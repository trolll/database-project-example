/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLBilling
Source Table          : TBLCategory
Date                  : 2023-10-04 18:57:42
*/


INSERT INTO [dbo].[TBLCategory] ([CategoryID], [CategoryName], [CategoryDesc], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1, '消耗道具', '消耗类道具', '1', '2008-01-09 12:35:13.000', NULL, NULL, '2008-04-02 10:36:42.390', 'CN11669', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLCategory] ([CategoryID], [CategoryName], [CategoryDesc], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (2, '时限道具', '有时间限制类道具', '1', '2008-01-09 12:35:13.000', NULL, NULL, '2008-02-22 17:16:54.607', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLCategory] ([CategoryID], [CategoryName], [CategoryDesc], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (3, '道具礼包', '道具组合礼包', '1', '2008-01-09 12:35:13.000', NULL, NULL, '2008-02-22 17:17:22.607', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLCategory] ([CategoryID], [CategoryName], [CategoryDesc], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (4, '网吧专用', '网吧用户特有道具', '1', '2008-01-11 11:27:07.000', NULL, NULL, '2008-02-22 17:18:22.577', 'CN12504', '210.72.232.228');
GO

