/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLBilling
Source Table          : TBLCode
Date                  : 2023-10-04 18:57:41
*/


INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (1, 0, '删除');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (1, 1, '显示商店');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (1, 2, '未显示商店');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (2, 0, 'Non Package');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (2, 1, 'Package');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 1, '普通');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 2, 'new');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 3, '普通，new');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 4, '打折');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 5, '普通,打折');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 6, 'new,打折');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 7, '普通,new,打折');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 8, '活动');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 9, '普通，活动');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 10, 'new，活动');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 11, '普通,new, 活动');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 12, '打折,活动');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 13, '普通, 打折, 活动');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 14, 'new, 打折, 活动');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (3, 15, '普通, new, 打折, 活动');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 0, 'None Err');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 1, '交易登录中收费出错');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 2, '交易登录中更新个人信息时出错');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 3, '交易购买中收费出错');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 4, '交易购买中更新个人信息时出错');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 5, '申请交易登录');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 6, '交易登录完毕');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 7, '3. 购买申请');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 8, '4. 购买完毕');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 9, '收领');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 10, '6. 收回并取消收费时出错');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (4, 11, '6. 回收并取消');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 0, 'Success');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 1, '系统报错');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 2, '共同 Invalid Parameter  参数不为空的时候');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 3, '共同 Invalid Parameter (svrid)  未登录服务器的时候');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 4, '共同 Invalid Account (status)  帐号状态不为 Y的时候');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 5, '共同 Invalid Coinlog (status)  该logseq 已被取消的时候');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 6, '共同 Invalid Coinlog (logseq)  该 logseq 不存在的时候');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 7, '充值 Invalid Parameter (pgid)  不是允许的结算方式的时候');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 8, '耗尽 Invalid Parameter (itemid)  未登录的产品时');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 9, '耗尽 Invalid Parameter (price) 金额 /件数与登录的价格不同的时候');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 10, '耗尽 Invalid Account (balance)  余额不足时');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (5, 11, '耗尽 Invalid Account (userid)  该ID无帐号时');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (6, 0, 'Gold->Silver Exchange');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (6, 1, 'Silver->Gold Exchange');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (7, 0, '报错');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (7, 1, '登录交易');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (7, 2, '交易登录完毕');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (7, 3, '申请交易');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (7, 4, '交易完毕');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (7, 5, '收领完毕(交易完毕)');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (7, 6, '回收并取消完毕');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (8, 0, '显示公告');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (8, 1, '不显示');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (8, 2, '商城首页中发公告');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (9, 0, '尝试订单');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (9, 1, '订单失败');
GO

INSERT INTO [dbo].[TBLCode] ([CodeType], [Code], [CodeDesc]) VALUES (9, 2, '订单成功');
GO

