/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLBilling
Source Table          : R2_COUPON_TYPE
Date                  : 2023-10-04 18:57:43
*/


INSERT INTO [dbo].[R2_COUPON_TYPE] ([COUPON_TYPE_NO], [COUPON_TYPE]) VALUES (1, '新手礼品');
GO

INSERT INTO [dbo].[R2_COUPON_TYPE] ([COUPON_TYPE_NO], [COUPON_TYPE]) VALUES (2, '生日礼品');
GO

INSERT INTO [dbo].[R2_COUPON_TYPE] ([COUPON_TYPE_NO], [COUPON_TYPE]) VALUES (3, '回归礼品');
GO

INSERT INTO [dbo].[R2_COUPON_TYPE] ([COUPON_TYPE_NO], [COUPON_TYPE]) VALUES (4, '租赁卡');
GO

INSERT INTO [dbo].[R2_COUPON_TYPE] ([COUPON_TYPE_NO], [COUPON_TYPE]) VALUES (24, '测试');
GO

