/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLBilling
Source Table          : TBLNewItemList
Date                  : 2023-10-04 18:57:42
*/


INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (21, 1, 1, '0', '2008-02-28 11:15:38.030', '', '127.0.0.1', '2014-08-12 11:20:16.340', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (22, 2, 5, '0', '2008-02-28 11:15:38.090', '', '127.0.0.1', '2014-03-30 21:13:50.263', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (39, 3, 5, '0', '2008-03-28 19:11:56.577', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.787', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (40, 4, 5, '0', '2008-05-01 10:06:53.373', 'CN12504', '210.72.232.228', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1, 11, 1, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 15:04:04.933', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (2, 12, 1, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 19:39:24.570', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (3, 13, 2, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 19:39:24.587', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (4, 14, 1, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-30 07:29:19.600', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (5, 15, 6, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-30 07:29:19.820', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (6, 16, 3, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-30 07:29:19.693', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (7, 17, 2, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-16 10:26:29.510', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (8, 18, 2, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-03 13:08:09.723', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (9, 19, 8, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 15:04:04.993', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10, 20, 9, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 15:04:05.010', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (38, 25, 4, '0', '2008-03-28 19:11:56.577', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.740', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (32, 26, 10, '0', '2008-03-26 16:19:27.947', 'CN12504', '210.72.232.228', '2008-04-30 07:29:20.007', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (36, 31, 3, '0', '2008-03-28 19:11:56.560', 'CN12504', '210.72.232.228', '2012-07-24 14:09:49.107', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (289, 33, 2, '0', '2013-11-26 16:03:58.890', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.093', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (226, 39, 6, '0', '2010-02-10 17:16:13.933', 'CN99019', '10.34.118.165', '2010-02-24 09:55:13.687', 'CN99019', '10.34.118.165');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (202, 40, 2, '0', '2009-10-27 14:54:51.320', 'CN12654', '202.108.36.125', '2012-02-22 14:55:12.270', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (133, 41, 1, '0', '2009-04-24 09:26:36.037', 'CN12654', '10.34.115.185', '2009-07-16 19:34:45.740', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (134, 42, 4, '0', '2009-04-24 09:26:36.350', 'CN12654', '10.34.115.185', '2012-03-13 11:12:37.080', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (135, 43, 8, '0', '2009-04-24 09:26:36.647', 'CN12654', '10.34.115.185', '2010-02-24 09:55:13.687', 'CN99019', '10.34.118.165');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (136, 44, 4, '0', '2009-04-24 09:26:37.303', 'CN12654', '10.34.115.185', '2012-02-22 14:55:12.270', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (137, 45, 3, '0', '2009-04-24 09:26:37.600', 'CN12654', '10.34.115.185', '2012-02-22 14:55:12.270', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (233, 46, 1, '0', '2012-02-24 10:24:17.380', 'CN90053', '61.152.133.230', '2012-03-09 16:07:18.820', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (138, 48, 3, '0', '2009-04-24 09:26:37.913', 'CN12654', '10.34.115.185', '2009-07-16 19:34:45.740', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (272, 50, 1, '0', '2012-10-29 13:31:05.687', '', '61.152.133.230', '2013-01-29 10:37:16.763', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (143, 51, 4, '0', '2009-05-05 09:52:12.250', 'CN12654', '10.34.115.182', '2014-02-11 10:33:36.513', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (297, 52, 5, '0', '2014-01-28 10:47:08.907', 'CN90053', '222.128.29.241', '2014-02-11 10:33:36.513', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (140, 54, 2, '0', '2009-05-05 09:52:11.953', 'CN12654', '10.34.115.182', '2009-06-23 12:17:21.530', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (141, 55, 3, '0', '2009-05-05 09:52:12.047', 'CN12654', '10.34.115.182', '2009-06-23 12:17:21.530', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (142, 56, 4, '0', '2009-05-05 09:52:12.157', 'CN12654', '10.34.115.182', '2009-06-23 12:17:21.530', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (144, 57, 3, '0', '2009-05-05 09:52:12.343', 'CN12654', '10.34.115.182', '2012-07-24 19:55:50.547', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (148, 58, 5, '0', '2009-05-05 09:52:12.750', 'CN12654', '10.34.115.182', '2009-09-22 06:44:25.360', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (292, 61, 3, '0', '2013-12-03 10:42:09.310', 'CN90053', '222.128.29.241', '2014-02-04 10:55:52.577', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (258, 62, 4, '0', '2012-07-24 19:55:50.593', '', '61.152.133.230', '2012-08-27 17:59:17.810', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (145, 63, 2, '0', '2009-05-05 09:52:12.453', 'CN12654', '10.34.115.182', '2009-09-22 06:44:25.360', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (146, 64, 3, '0', '2009-05-05 09:52:12.550', 'CN12654', '10.34.115.182', '2009-09-22 06:44:25.360', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (147, 65, 4, '0', '2009-05-05 09:52:12.657', 'CN12654', '10.34.115.182', '2009-09-22 06:44:25.360', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (227, 66, 3, '0', '2010-02-10 17:16:13.967', 'CN99019', '10.34.118.165', '2013-12-03 10:42:09.263', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (210, 67, 7, '0', '2009-12-24 17:28:25.570', 'CN99019', '202.108.36.125', '2009-12-29 09:31:51.140', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (217, 68, 7, '0', '2009-12-24 17:53:03.667', 'CN99019', '202.108.36.125', '2009-12-24 18:22:34.657', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (203, 83, 1, '0', '2009-12-23 18:37:38.793', 'CN99019', '218.25.29.4', '2009-12-24 17:16:27.793', 'CN99019', '218.25.29.4');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (220, 115, 1, '0', '2009-12-29 10:23:10.517', 'CN99019', '202.108.36.125', '2009-12-29 11:32:45.157', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (30, 125, 8, '0', '2008-03-26 16:19:27.930', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.913', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (31, 127, 9, '0', '2008-03-26 16:19:27.930', 'CN12504', '210.72.232.228', '2008-03-26 19:39:24.633', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (41, 129, 5, '0', '2008-05-01 10:06:53.543', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (42, 130, 6, '0', '2008-05-01 10:06:53.717', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (46, 131, 7, '0', '2008-05-01 10:06:54.373', 'CN12504', '210.72.232.228', '2008-05-01 10:06:54.373', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (47, 132, 7, '0', '2008-05-01 10:06:54.543', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (48, 133, 8, '0', '2008-05-01 10:06:54.700', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (49, 134, 10, '0', '2008-05-01 10:06:54.873', 'CN12504', '210.72.232.228', '2008-05-28 07:38:09.857', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (43, 135, 2, '0', '2008-05-01 10:06:53.887', 'CN12504', '210.72.232.228', '2008-06-11 06:57:39.050', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (44, 136, 3, '0', '2008-05-01 10:06:54.043', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (45, 137, 4, '0', '2008-05-01 10:06:54.217', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (54, 138, 5, '0', '2008-05-07 07:49:13.100', 'KR10643', '210.72.232.136', '2008-05-28 07:38:09.857', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (78, 142, 8, '0', '2008-06-11 06:57:39.270', 'CN12504', '210.72.232.170', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (63, 143, 3, '0', '2008-05-28 07:38:10.293', 'CN11582', '210.72.232.131', '2008-06-11 06:57:39.050', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (64, 144, 6, '0', '2008-05-28 07:38:10.340', 'CN11582', '210.72.232.131', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (69, 145, 9, '0', '2008-06-04 07:03:55.347', 'CN12504', '210.72.232.170', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (70, 147, 10, '0', '2008-06-04 07:03:55.393', 'CN12504', '210.72.232.170', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (71, 148, 7, '0', '2008-06-04 07:03:55.440', 'CN12504', '210.72.232.170', '2008-09-09 07:09:17.977', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (72, 149, 8, '0', '2008-06-04 07:03:55.487', 'CN12504', '210.72.232.170', '2008-09-09 07:09:17.977', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (73, 150, 9, '0', '2008-06-04 07:03:55.533', 'CN12504', '210.72.232.170', '2008-09-09 07:09:17.977', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (74, 151, 10, '0', '2008-06-04 07:03:55.580', 'CN12504', '210.72.232.170', '2008-09-09 07:09:17.977', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (77, 152, 3, '0', '2008-06-11 06:57:39.207', 'CN12504', '210.72.232.170', '2014-09-24 17:57:14.577', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (299, 153, 2, '0', '2014-02-11 10:33:36.607', 'CN90053', '222.128.29.241', '2014-02-11 10:39:06.077', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (84, 154, 6, '0', '2008-09-09 07:07:02.280', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (85, 155, 7, '0', '2008-09-09 07:07:02.280', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (86, 156, 5, '0', '2008-09-09 07:07:02.280', 'CN12654', '210.72.232.170', '2008-09-27 09:53:08.560', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (93, 158, 5, '0', '2008-09-23 07:04:31.267', 'CN12654', '210.72.232.170', '2008-11-18 06:59:18.313', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (98, 159, 10, '0', '2008-09-23 07:04:31.280', 'CN12654', '210.72.232.170', '2008-11-18 06:59:18.313', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (99, 160, 1, '0', '2008-09-23 07:04:31.297', 'CN12654', '210.72.232.170', '2009-03-17 08:07:33.013', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (100, 161, 2, '0', '2008-09-23 07:04:31.297', 'CN12654', '210.72.232.170', '2009-03-17 08:07:33.013', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (101, 163, 8, '0', '2008-09-23 07:04:31.297', 'CN12654', '210.72.232.170', '2009-05-05 09:52:11.720', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (97, 165, 5, '0', '2008-09-27 09:53:08.840', 'CN12504', '210.72.232.225', '2008-10-14 07:03:09.943', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (102, 171, 10, '0', '2008-09-23 07:04:31.297', 'CN12654', '210.72.232.170', '2009-05-05 09:52:11.720', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (83, 178, 5, '0', '2008-09-09 07:07:02.263', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (105, 181, 9, '0', '2008-10-14 07:03:05.867', 'CN12654', '210.72.232.170', '2009-05-05 09:52:11.720', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (120, 185, 8, '0', '2008-11-18 06:59:14.047', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (121, 186, 9, '0', '2008-11-18 06:59:14.047', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (232, 192, 2, '1', '2012-02-22 14:55:12.350', 'CN90053', '61.152.133.230', '2016-04-21 12:27:31.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (122, 193, 9, '0', '2008-12-23 11:17:30.360', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (123, 194, 10, '0', '2008-12-23 11:17:30.487', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (124, 195, 11, '0', '2008-12-23 11:17:30.580', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (125, 196, 12, '0', '2008-12-23 11:17:30.673', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (126, 197, 13, '0', '2008-12-23 11:17:30.783', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (127, 198, 14, '0', '2008-12-23 11:17:30.877', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (128, 199, 15, '0', '2008-12-23 11:17:30.970', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (129, 200, 16, '0', '2008-12-23 11:17:31.080', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (130, 201, 17, '0', '2008-12-23 11:17:31.190', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (131, 202, 18, '0', '2008-12-23 11:17:31.313', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (132, 223, 5, '0', '2009-04-16 07:15:46.440', 'CN12654', '10.34.115.182', '2014-05-13 18:43:36.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (176, 294, 4, '0', '2009-08-25 09:07:01.530', 'CN12654', '10.34.223.138', '2009-10-21 19:13:32.200', 'CN12654', '10.34.118.198');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (177, 299, 5, '0', '2009-08-25 09:07:01.580', 'CN12654', '10.34.223.138', '2009-10-21 19:13:32.200', 'CN12654', '10.34.118.198');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (164, 303, 1, '0', '2009-07-16 19:34:45.880', 'CN12654', '10.34.223.29', '2009-08-25 09:07:00.813', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (173, 307, 1, '0', '2009-08-25 09:07:01.140', 'CN12654', '10.34.223.138', '2009-10-27 14:54:50.773', 'CN12654', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (165, 312, 2, '0', '2009-07-16 19:34:46.003', 'CN12654', '10.34.223.29', '2009-08-25 09:07:00.813', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (174, 313, 5, '0', '2009-08-25 09:07:01.203', 'CN12654', '10.34.223.138', '2010-02-10 17:16:13.810', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (166, 318, 3, '0', '2009-07-16 19:34:46.130', 'CN12654', '10.34.223.29', '2009-08-25 09:07:00.813', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (167, 324, 4, '0', '2009-07-16 19:34:46.240', 'CN12654', '10.34.223.29', '2009-08-25 09:07:00.813', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (175, 325, 6, '0', '2009-08-25 09:07:01.250', 'CN12654', '10.34.223.138', '2010-02-10 17:16:13.810', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (183, 329, 6, '0', '2009-09-22 06:44:25.737', 'CN12654', '10.34.118.198', '2010-02-04 15:42:14.437', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (184, 330, 7, '0', '2009-09-22 06:44:25.813', 'CN12654', '10.34.118.198', '2010-02-04 15:42:14.437', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (193, 341, 8, '0', '2009-09-22 08:51:15.980', 'CN12654', '10.34.118.198', '2010-02-10 17:16:13.810', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (192, 353, 7, '0', '2009-09-22 08:51:15.917', 'CN12654', '10.34.118.198', '2010-02-10 17:16:13.810', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (254, 359, 4, '0', '2012-07-24 13:05:14.217', '', '61.152.133.230', '2012-07-24 19:55:50.547', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (218, 360, 1, '0', '2009-12-29 09:31:51.220', 'CN99019', '202.108.36.125', '2009-12-29 10:23:10.470', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (219, 361, 1, '0', '2012-03-13 11:12:37.130', 'CN90053', '61.152.133.230', '2014-07-10 19:21:34.780', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (229, 373, 3, '0', '2012-02-22 14:55:12.333', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.200', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (230, 374, 2, '0', '2012-02-22 14:55:12.333', 'CN90053', '61.152.133.230', '2012-04-05 15:30:20.640', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (228, 375, 2, '0', '2012-02-22 14:55:12.317', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.200', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (237, 376, 3, '0', '2012-04-05 15:10:21.280', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (239, 377, 5, '0', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (238, 378, 4, '0', '2012-04-05 15:10:21.297', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (249, 379, 4, '0', '2012-07-20 22:46:52.640', 'CN90053', '61.152.133.230', '2013-04-27 11:00:22.907', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (278, 394, 3, '0', '2013-01-29 10:37:16.873', 'CN90053', '61.152.133.230', '2013-11-05 10:47:45.687', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (243, 397, 4, '0', '2012-07-20 21:42:39.420', 'CN90053', '61.152.133.230', '2012-08-28 11:57:16.450', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (244, 398, 4, '0', '2012-07-20 21:42:39.420', 'CN90053', '61.152.133.230', '2012-09-28 10:48:40.857', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (268, 412, 4, '0', '2012-10-11 21:21:32.060', '', '61.152.133.230', '2012-10-29 13:31:05.623', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (269, 418, 5, '0', '2012-10-11 21:21:32.077', '', '61.152.133.230', '2013-11-26 10:34:58.390', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (242, 423, 4, '0', '2012-07-20 21:42:39.420', 'CN90053', '61.152.133.230', '2013-07-02 10:59:07.450', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (264, 426, 6, '0', '2012-10-08 11:33:29.297', '', '61.152.133.230', '2012-12-24 12:12:40.293', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (259, 436, 5, '0', '2012-09-28 10:48:40.903', '', '61.152.133.230', '2013-11-05 10:47:45.687', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (283, 437, 4, '0', '2013-02-06 12:14:50.497', 'CN90053', '61.152.133.230', '2014-09-10 17:48:10.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (284, 438, 5, '0', '2013-02-06 12:14:50.513', 'CN90053', '61.152.133.230', '2014-09-10 17:48:10.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (260, 439, 4, '0', '2012-09-28 10:48:40.903', '', '61.152.133.230', '2014-05-13 18:43:36.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (300, 440, 1, '0', '2014-08-12 11:20:16.577', 'CN90053', '124.205.178.150', '2015-09-01 11:50:41.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (317, 477, 4, '0', '2014-10-08 11:42:12.670', 'CN90053', '124.205.178.150', '2014-10-27 16:44:06.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (389, 590, 5, '0', '2015-06-23 13:54:58.683', 'CN90053', '114.112.126.218', '2015-06-23 14:39:16.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (351, 595, 5, '0', '2015-03-03 10:40:40.437', 'CN90053', '124.205.178.150', '2015-03-23 11:10:50.250', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (304, 613, 5, '0', '2014-09-10 17:48:10.543', 'CN90053', '124.205.178.150', '2015-09-20 15:56:03.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (305, 614, 3, '0', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150', '2015-02-12 16:41:28.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (310, 615, 3, '0', '2014-09-11 12:45:10.140', 'CN90053', '124.205.178.150', '2014-10-27 16:44:06.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (318, 627, 5, '0', '2014-10-08 11:42:12.670', 'CN90053', '124.205.178.150', '2014-10-27 16:44:06.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (321, 643, 3, '0', '2014-10-27 16:44:06.577', 'CN90053', '124.205.178.150', '2014-11-05 12:15:41.840', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (312, 644, 4, '0', '2014-09-23 21:14:48.280', 'CN90053', '124.205.178.150', '2014-11-05 12:15:41.840', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (313, 645, 5, '0', '2014-09-23 21:14:48.293', 'CN90053', '124.205.178.150', '2015-01-25 12:39:47.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (325, 648, 4, '0', '2014-11-05 12:15:41.903', 'CN90053', '124.205.178.150', '2015-01-25 12:39:47.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (324, 649, 3, '0', '2014-11-05 12:15:41.890', 'CN90053', '124.205.178.150', '2014-12-22 12:20:53.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (328, 655, 1, '0', '2014-11-21 15:34:29.983', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.217', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (366, 663, 4, '0', '2015-03-30 19:01:57.560', 'CN90053', '124.205.178.150', '2015-09-20 15:56:03.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (330, 664, 2, '0', '2014-12-30 12:31:46.873', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (334, 667, 3, '1', '2015-01-25 12:39:47.623', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.530', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (335, 670, 4, '1', '2015-01-25 12:39:47.637', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.543', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (340, 679, 4, '0', '2015-02-12 16:41:28.653', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.670', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (356, 680, 5, '0', '2015-03-23 11:10:50.373', 'CN90053', '124.205.178.150', '2015-03-23 20:18:37.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (374, 681, 3, '0', '2015-05-29 09:08:03.123', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (397, 682, 3, '0', '2015-08-25 14:43:36.623', 'CN90053', '114.112.126.218', '2015-09-08 11:45:18.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (398, 683, 4, '0', '2015-08-25 14:43:36.640', 'CN90053', '114.112.126.218', '2015-09-08 11:45:18.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (399, 684, 5, '0', '2015-08-25 14:43:36.640', 'CN90053', '114.112.126.218', '2015-09-01 11:50:41.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (384, 685, 5, '0', '2015-06-15 16:27:36.107', 'CN90053', '114.112.126.218', '2015-06-23 13:54:58.590', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (361, 686, 4, '0', '2015-03-23 20:18:37.780', 'CN90053', '124.205.178.150', '2015-05-29 09:31:40.873', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (341, 687, 4, '0', '2015-02-12 16:41:28.653', 'CN90053', '124.205.178.150', '2015-06-01 20:42:46.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (379, 688, 5, '0', '2015-05-29 09:31:40.903', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (346, 689, 5, '0', '2015-02-23 14:49:36.233', 'CN90053', '124.205.178.150', '2015-03-03 10:40:40.340', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (371, 691, 5, '0', '2015-04-27 16:32:28.047', 'CN90053', '124.205.178.210', '2015-05-11 12:18:20.623', 'CN90053', '124.205.178.210');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (392, 692, 3, '0', '2015-07-11 17:10:58.297', 'CN90053', '114.112.126.218', '2015-07-21 10:21:55.450', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (393, 693, 4, '0', '2015-07-11 17:10:58.310', 'CN90053', '114.112.126.218', '2015-07-21 10:21:55.450', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (394, 694, 5, '0', '2015-07-11 17:10:58.327', 'CN90053', '114.112.126.218', '2015-07-27 17:28:41.717', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (400, 699, 1, '1', '2015-09-01 11:50:41.200', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (405, 708, 3, '0', '2015-09-01 12:55:34.750', 'CN90053', '114.112.126.218', '2015-09-14 12:25:50.340', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (407, 730, 5, '1', '2015-09-14 12:25:50.387', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.543', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (412, 811, 4, '0', '2015-09-20 15:56:03.200', 'CN90053', '114.112.126.218', '2015-09-21 13:34:29.590', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (421, 812, 5, '0', '2015-09-22 15:39:54.750', 'CN90053', '114.112.126.218', '2015-09-22 16:07:08.733', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (416, 813, 4, '0', '2015-09-21 13:34:29.653', 'CN90053', '114.112.126.218', '2015-09-22 15:39:54.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (430, 817, 3, '0', '2015-09-22 17:48:39.547', 'CN90053', '114.112.126.218', '2015-10-13 11:21:27.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (431, 818, 4, '0', '2015-09-22 17:48:39.560', 'CN90053', '114.112.126.218', '2015-10-13 11:21:27.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (425, 819, 4, '0', '2015-09-22 16:07:08.763', 'CN90053', '114.112.126.218', '2015-09-22 16:17:47.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (426, 820, 5, '0', '2015-09-22 16:07:08.763', 'CN90053', '114.112.126.218', '2015-09-22 16:17:47.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (441, 821, 5, '0', '2015-11-09 15:08:09.797', 'CN90053', '114.112.126.218', '2015-11-16 17:50:15.373', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (436, 824, 5, '0', '2015-09-30 06:15:17.810', 'CN90053', '114.112.126.218', '2015-10-13 11:21:27.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (442, 1055, 13, '1', '2018-07-13 18:42:42.523', 'CN12654', '210.72.232.131', '2018-07-13 18:42:42.523', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (443, 1056, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (444, 1057, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (445, 1058, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (446, 1059, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (447, 1060, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (916, 1061, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (895, 1062, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (896, 1063, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (897, 1064, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (898, 1065, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (899, 1066, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (900, 1067, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (901, 1068, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (902, 1069, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (903, 1070, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (904, 1071, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (905, 1072, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (906, 1073, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (907, 1074, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (908, 1075, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (909, 1076, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (910, 1077, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (911, 1078, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (912, 1079, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (913, 1080, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (914, 1081, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (915, 1082, 168, '1', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (468, 10001, 1, '0', '2008-02-28 11:15:38.030', '', '127.0.0.1', '2014-08-12 11:20:16.340', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (469, 10002, 5, '0', '2008-02-28 11:15:38.090', '', '127.0.0.1', '2014-03-30 21:13:50.263', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (486, 10003, 5, '0', '2008-03-28 19:11:56.577', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.787', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (487, 10004, 5, '0', '2008-05-01 10:06:53.373', 'CN12504', '210.72.232.228', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (448, 10011, 1, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 15:04:04.933', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (449, 10012, 1, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 19:39:24.570', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (450, 10013, 2, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 19:39:24.587', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (451, 10014, 1, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-30 07:29:19.600', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (452, 10015, 6, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-30 07:29:19.820', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (453, 10016, 3, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-30 07:29:19.693', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (454, 10017, 2, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-16 10:26:29.510', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (455, 10018, 2, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-04-03 13:08:09.723', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (456, 10019, 8, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 15:04:04.993', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (457, 10020, 9, '0', '2008-01-09 14:13:54.000', NULL, NULL, '2008-03-26 15:04:05.010', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (485, 10025, 4, '0', '2008-03-28 19:11:56.577', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.740', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (479, 10026, 10, '0', '2008-03-26 16:19:27.947', 'CN12504', '210.72.232.228', '2008-04-30 07:29:20.007', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (483, 10031, 3, '0', '2008-03-28 19:11:56.560', 'CN12504', '210.72.232.228', '2012-07-24 14:09:49.107', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (736, 10033, 2, '0', '2013-11-26 16:03:58.890', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.093', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (673, 10039, 6, '0', '2010-02-10 17:16:13.933', 'CN99019', '10.34.118.165', '2010-02-24 09:55:13.687', 'CN99019', '10.34.118.165');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (649, 10040, 2, '0', '2009-10-27 14:54:51.320', 'CN12654', '202.108.36.125', '2012-02-22 14:55:12.270', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (580, 10041, 1, '0', '2009-04-24 09:26:36.037', 'CN12654', '10.34.115.185', '2009-07-16 19:34:45.740', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (581, 10042, 4, '0', '2009-04-24 09:26:36.350', 'CN12654', '10.34.115.185', '2012-03-13 11:12:37.080', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (582, 10043, 8, '0', '2009-04-24 09:26:36.647', 'CN12654', '10.34.115.185', '2010-02-24 09:55:13.687', 'CN99019', '10.34.118.165');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (583, 10044, 4, '0', '2009-04-24 09:26:37.303', 'CN12654', '10.34.115.185', '2012-02-22 14:55:12.270', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (584, 10045, 3, '0', '2009-04-24 09:26:37.600', 'CN12654', '10.34.115.185', '2012-02-22 14:55:12.270', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (680, 10046, 1, '0', '2012-02-24 10:24:17.380', 'CN90053', '61.152.133.230', '2012-03-09 16:07:18.820', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (585, 10048, 3, '0', '2009-04-24 09:26:37.913', 'CN12654', '10.34.115.185', '2009-07-16 19:34:45.740', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (719, 10050, 1, '0', '2012-10-29 13:31:05.687', '', '61.152.133.230', '2013-01-29 10:37:16.763', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (590, 10051, 4, '0', '2009-05-05 09:52:12.250', 'CN12654', '10.34.115.182', '2014-02-11 10:33:36.513', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (744, 10052, 5, '0', '2014-01-28 10:47:08.907', 'CN90053', '222.128.29.241', '2014-02-11 10:33:36.513', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (587, 10054, 2, '0', '2009-05-05 09:52:11.953', 'CN12654', '10.34.115.182', '2009-06-23 12:17:21.530', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (588, 10055, 3, '0', '2009-05-05 09:52:12.047', 'CN12654', '10.34.115.182', '2009-06-23 12:17:21.530', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (589, 10056, 4, '0', '2009-05-05 09:52:12.157', 'CN12654', '10.34.115.182', '2009-06-23 12:17:21.530', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (591, 10057, 3, '0', '2009-05-05 09:52:12.343', 'CN12654', '10.34.115.182', '2012-07-24 19:55:50.547', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (595, 10058, 5, '0', '2009-05-05 09:52:12.750', 'CN12654', '10.34.115.182', '2009-09-22 06:44:25.360', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (739, 10061, 3, '0', '2013-12-03 10:42:09.310', 'CN90053', '222.128.29.241', '2014-02-04 10:55:52.577', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (705, 10062, 4, '0', '2012-07-24 19:55:50.593', '', '61.152.133.230', '2012-08-27 17:59:17.810', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (592, 10063, 2, '0', '2009-05-05 09:52:12.453', 'CN12654', '10.34.115.182', '2009-09-22 06:44:25.360', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (593, 10064, 3, '0', '2009-05-05 09:52:12.550', 'CN12654', '10.34.115.182', '2009-09-22 06:44:25.360', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (594, 10065, 4, '0', '2009-05-05 09:52:12.657', 'CN12654', '10.34.115.182', '2009-09-22 06:44:25.360', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (674, 10066, 3, '0', '2010-02-10 17:16:13.967', 'CN99019', '10.34.118.165', '2013-12-03 10:42:09.263', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (657, 10067, 7, '0', '2009-12-24 17:28:25.570', 'CN99019', '202.108.36.125', '2009-12-29 09:31:51.140', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (664, 10068, 7, '0', '2009-12-24 17:53:03.667', 'CN99019', '202.108.36.125', '2009-12-24 18:22:34.657', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (650, 10083, 1, '0', '2009-12-23 18:37:38.793', 'CN99019', '218.25.29.4', '2009-12-24 17:16:27.793', 'CN99019', '218.25.29.4');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (667, 10115, 1, '0', '2009-12-29 10:23:10.517', 'CN99019', '202.108.36.125', '2009-12-29 11:32:45.157', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (477, 10125, 8, '0', '2008-03-26 16:19:27.930', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.913', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (478, 10127, 9, '0', '2008-03-26 16:19:27.930', 'CN12504', '210.72.232.228', '2008-03-26 19:39:24.633', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (488, 10129, 5, '0', '2008-05-01 10:06:53.543', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (489, 10130, 6, '0', '2008-05-01 10:06:53.717', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (493, 10131, 7, '0', '2008-05-01 10:06:54.373', 'CN12504', '210.72.232.228', '2008-05-01 10:06:54.373', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (494, 10132, 7, '0', '2008-05-01 10:06:54.543', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (495, 10133, 8, '0', '2008-05-01 10:06:54.700', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (496, 10134, 10, '0', '2008-05-01 10:06:54.873', 'CN12504', '210.72.232.228', '2008-05-28 07:38:09.857', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (490, 10135, 2, '0', '2008-05-01 10:06:53.887', 'CN12504', '210.72.232.228', '2008-06-11 06:57:39.050', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (491, 10136, 3, '0', '2008-05-01 10:06:54.043', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (492, 10137, 4, '0', '2008-05-01 10:06:54.217', 'CN12504', '210.72.232.228', '2008-06-04 07:03:55.097', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (501, 10138, 5, '0', '2008-05-07 07:49:13.100', 'KR10643', '210.72.232.136', '2008-05-28 07:38:09.857', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (525, 10142, 8, '0', '2008-06-11 06:57:39.270', 'CN12504', '210.72.232.170', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (510, 10143, 3, '0', '2008-05-28 07:38:10.293', 'CN11582', '210.72.232.131', '2008-06-11 06:57:39.050', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (511, 10144, 6, '0', '2008-05-28 07:38:10.340', 'CN11582', '210.72.232.131', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (516, 10145, 9, '0', '2008-06-04 07:03:55.347', 'CN12504', '210.72.232.170', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (517, 10147, 10, '0', '2008-06-04 07:03:55.393', 'CN12504', '210.72.232.170', '2008-09-23 07:04:35.923', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (518, 10148, 7, '0', '2008-06-04 07:03:55.440', 'CN12504', '210.72.232.170', '2008-09-09 07:09:17.977', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (519, 10149, 8, '0', '2008-06-04 07:03:55.487', 'CN12504', '210.72.232.170', '2008-09-09 07:09:17.977', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (520, 10150, 9, '0', '2008-06-04 07:03:55.533', 'CN12504', '210.72.232.170', '2008-09-09 07:09:17.977', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (521, 10151, 10, '0', '2008-06-04 07:03:55.580', 'CN12504', '210.72.232.170', '2008-09-09 07:09:17.977', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (524, 10152, 3, '0', '2008-06-11 06:57:39.207', 'CN12504', '210.72.232.170', '2014-09-24 17:57:14.577', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (746, 10153, 2, '0', '2014-02-11 10:33:36.607', 'CN90053', '222.128.29.241', '2014-02-11 10:39:06.077', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (531, 10154, 6, '0', '2008-09-09 07:07:02.280', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (532, 10155, 7, '0', '2008-09-09 07:07:02.280', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (533, 10156, 5, '0', '2008-09-09 07:07:02.280', 'CN12654', '210.72.232.170', '2008-09-27 09:53:08.560', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (540, 10158, 5, '0', '2008-09-23 07:04:31.267', 'CN12654', '210.72.232.170', '2008-11-18 06:59:18.313', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (545, 10159, 10, '0', '2008-09-23 07:04:31.280', 'CN12654', '210.72.232.170', '2008-11-18 06:59:18.313', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (546, 10160, 1, '0', '2008-09-23 07:04:31.297', 'CN12654', '210.72.232.170', '2009-03-17 08:07:33.013', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (547, 10161, 2, '0', '2008-09-23 07:04:31.297', 'CN12654', '210.72.232.170', '2009-03-17 08:07:33.013', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (548, 10163, 8, '0', '2008-09-23 07:04:31.297', 'CN12654', '210.72.232.170', '2009-05-05 09:52:11.720', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (544, 10165, 5, '0', '2008-09-27 09:53:08.840', 'CN12504', '210.72.232.225', '2008-10-14 07:03:09.943', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (549, 10171, 10, '0', '2008-09-23 07:04:31.297', 'CN12654', '210.72.232.170', '2009-05-05 09:52:11.720', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (530, 10178, 5, '0', '2008-09-09 07:07:02.263', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (552, 10181, 9, '0', '2008-10-14 07:03:05.867', 'CN12654', '210.72.232.170', '2009-05-05 09:52:11.720', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (567, 10185, 8, '0', '2008-11-18 06:59:14.047', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (568, 10186, 9, '0', '2008-11-18 06:59:14.047', 'CN12654', '210.72.232.170', '2009-04-24 09:26:35.680', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (679, 10192, 2, '1', '2012-02-22 14:55:12.350', 'CN90053', '61.152.133.230', '2016-04-21 12:27:31.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (569, 10193, 9, '0', '2008-12-23 11:17:30.360', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (570, 10194, 10, '0', '2008-12-23 11:17:30.487', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (571, 10195, 11, '0', '2008-12-23 11:17:30.580', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (572, 10196, 12, '0', '2008-12-23 11:17:30.673', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (573, 10197, 13, '0', '2008-12-23 11:17:30.783', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (574, 10198, 14, '0', '2008-12-23 11:17:30.877', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (575, 10199, 15, '0', '2008-12-23 11:17:30.970', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (576, 10200, 16, '0', '2008-12-23 11:17:31.080', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (577, 10201, 17, '0', '2008-12-23 11:17:31.190', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (578, 10202, 18, '0', '2008-12-23 11:17:31.313', 'KR13076', '210.72.232.170', '2009-04-16 07:15:46.287', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (579, 10223, 5, '0', '2009-04-16 07:15:46.440', 'CN12654', '10.34.115.182', '2014-05-13 18:43:36.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (623, 10294, 4, '0', '2009-08-25 09:07:01.530', 'CN12654', '10.34.223.138', '2009-10-21 19:13:32.200', 'CN12654', '10.34.118.198');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (624, 10299, 5, '0', '2009-08-25 09:07:01.580', 'CN12654', '10.34.223.138', '2009-10-21 19:13:32.200', 'CN12654', '10.34.118.198');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (611, 10303, 1, '0', '2009-07-16 19:34:45.880', 'CN12654', '10.34.223.29', '2009-08-25 09:07:00.813', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (620, 10307, 1, '0', '2009-08-25 09:07:01.140', 'CN12654', '10.34.223.138', '2009-10-27 14:54:50.773', 'CN12654', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (612, 10312, 2, '0', '2009-07-16 19:34:46.003', 'CN12654', '10.34.223.29', '2009-08-25 09:07:00.813', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (621, 10313, 5, '0', '2009-08-25 09:07:01.203', 'CN12654', '10.34.223.138', '2010-02-10 17:16:13.810', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (613, 10318, 3, '0', '2009-07-16 19:34:46.130', 'CN12654', '10.34.223.29', '2009-08-25 09:07:00.813', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (614, 10324, 4, '0', '2009-07-16 19:34:46.240', 'CN12654', '10.34.223.29', '2009-08-25 09:07:00.813', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (622, 10325, 6, '0', '2009-08-25 09:07:01.250', 'CN12654', '10.34.223.138', '2010-02-10 17:16:13.810', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (630, 10329, 6, '0', '2009-09-22 06:44:25.737', 'CN12654', '10.34.118.198', '2010-02-04 15:42:14.437', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (631, 10330, 7, '0', '2009-09-22 06:44:25.813', 'CN12654', '10.34.118.198', '2010-02-04 15:42:14.437', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (640, 10341, 8, '0', '2009-09-22 08:51:15.980', 'CN12654', '10.34.118.198', '2010-02-10 17:16:13.810', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (639, 10353, 7, '0', '2009-09-22 08:51:15.917', 'CN12654', '10.34.118.198', '2010-02-10 17:16:13.810', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (701, 10359, 4, '0', '2012-07-24 13:05:14.217', '', '61.152.133.230', '2012-07-24 19:55:50.547', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (665, 10360, 1, '0', '2009-12-29 09:31:51.220', 'CN99019', '202.108.36.125', '2009-12-29 10:23:10.470', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (666, 10361, 1, '0', '2012-03-13 11:12:37.130', 'CN90053', '61.152.133.230', '2014-07-10 19:21:34.780', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (676, 10373, 3, '0', '2012-02-22 14:55:12.333', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.200', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (677, 10374, 2, '0', '2012-02-22 14:55:12.333', 'CN90053', '61.152.133.230', '2012-04-05 15:30:20.640', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (675, 10375, 2, '0', '2012-02-22 14:55:12.317', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.200', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (684, 10376, 3, '0', '2012-04-05 15:10:21.280', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (686, 10377, 5, '0', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (685, 10378, 4, '0', '2012-04-05 15:10:21.297', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (696, 10379, 4, '0', '2012-07-20 22:46:52.640', 'CN90053', '61.152.133.230', '2013-04-27 11:00:22.907', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (725, 10394, 3, '0', '2013-01-29 10:37:16.873', 'CN90053', '61.152.133.230', '2013-11-05 10:47:45.687', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (690, 10397, 4, '0', '2012-07-20 21:42:39.420', 'CN90053', '61.152.133.230', '2012-08-28 11:57:16.450', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (691, 10398, 4, '0', '2012-07-20 21:42:39.420', 'CN90053', '61.152.133.230', '2012-09-28 10:48:40.857', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (715, 10412, 4, '0', '2012-10-11 21:21:32.060', '', '61.152.133.230', '2012-10-29 13:31:05.623', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (716, 10418, 5, '0', '2012-10-11 21:21:32.077', '', '61.152.133.230', '2013-11-26 10:34:58.390', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (689, 10423, 4, '0', '2012-07-20 21:42:39.420', 'CN90053', '61.152.133.230', '2013-07-02 10:59:07.450', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (711, 10426, 6, '0', '2012-10-08 11:33:29.297', '', '61.152.133.230', '2012-12-24 12:12:40.293', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (706, 10436, 5, '0', '2012-09-28 10:48:40.903', '', '61.152.133.230', '2013-11-05 10:47:45.687', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (730, 10437, 4, '0', '2013-02-06 12:14:50.497', 'CN90053', '61.152.133.230', '2014-09-10 17:48:10.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (731, 10438, 5, '0', '2013-02-06 12:14:50.513', 'CN90053', '61.152.133.230', '2014-09-10 17:48:10.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (707, 10439, 4, '0', '2012-09-28 10:48:40.903', '', '61.152.133.230', '2014-05-13 18:43:36.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (747, 10440, 1, '0', '2014-08-12 11:20:16.577', 'CN90053', '124.205.178.150', '2015-09-01 11:50:41.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (764, 10477, 4, '0', '2014-10-08 11:42:12.670', 'CN90053', '124.205.178.150', '2014-10-27 16:44:06.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (836, 10590, 5, '0', '2015-06-23 13:54:58.683', 'CN90053', '114.112.126.218', '2015-06-23 14:39:16.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (798, 10595, 5, '0', '2015-03-03 10:40:40.437', 'CN90053', '124.205.178.150', '2015-03-23 11:10:50.250', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (751, 10613, 5, '0', '2014-09-10 17:48:10.543', 'CN90053', '124.205.178.150', '2015-09-20 15:56:03.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (752, 10614, 3, '0', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150', '2015-02-12 16:41:28.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (757, 10615, 3, '0', '2014-09-11 12:45:10.140', 'CN90053', '124.205.178.150', '2014-10-27 16:44:06.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (765, 10627, 5, '0', '2014-10-08 11:42:12.670', 'CN90053', '124.205.178.150', '2014-10-27 16:44:06.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (768, 10643, 3, '0', '2014-10-27 16:44:06.577', 'CN90053', '124.205.178.150', '2014-11-05 12:15:41.840', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (759, 10644, 4, '0', '2014-09-23 21:14:48.280', 'CN90053', '124.205.178.150', '2014-11-05 12:15:41.840', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (760, 10645, 5, '0', '2014-09-23 21:14:48.293', 'CN90053', '124.205.178.150', '2015-01-25 12:39:47.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (772, 10648, 4, '0', '2014-11-05 12:15:41.903', 'CN90053', '124.205.178.150', '2015-01-25 12:39:47.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (771, 10649, 3, '0', '2014-11-05 12:15:41.890', 'CN90053', '124.205.178.150', '2014-12-22 12:20:53.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (775, 10655, 1, '0', '2014-11-21 15:34:29.983', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.217', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (813, 10663, 4, '0', '2015-03-30 19:01:57.560', 'CN90053', '124.205.178.150', '2015-09-20 15:56:03.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (777, 10664, 2, '0', '2014-12-30 12:31:46.873', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (781, 10667, 3, '1', '2015-01-25 12:39:47.623', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.530', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (782, 10670, 4, '1', '2015-01-25 12:39:47.637', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.543', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (787, 10679, 4, '0', '2015-02-12 16:41:28.653', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.670', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (803, 10680, 5, '0', '2015-03-23 11:10:50.373', 'CN90053', '124.205.178.150', '2015-03-23 20:18:37.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (821, 10681, 3, '0', '2015-05-29 09:08:03.123', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (844, 10682, 3, '0', '2015-08-25 14:43:36.623', 'CN90053', '114.112.126.218', '2015-09-08 11:45:18.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (845, 10683, 4, '0', '2015-08-25 14:43:36.640', 'CN90053', '114.112.126.218', '2015-09-08 11:45:18.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (846, 10684, 5, '0', '2015-08-25 14:43:36.640', 'CN90053', '114.112.126.218', '2015-09-01 11:50:41.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (831, 10685, 5, '0', '2015-06-15 16:27:36.107', 'CN90053', '114.112.126.218', '2015-06-23 13:54:58.590', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (808, 10686, 4, '0', '2015-03-23 20:18:37.780', 'CN90053', '124.205.178.150', '2015-05-29 09:31:40.873', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (788, 10687, 4, '0', '2015-02-12 16:41:28.653', 'CN90053', '124.205.178.150', '2015-06-01 20:42:46.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (826, 10688, 5, '0', '2015-05-29 09:31:40.903', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (793, 10689, 5, '0', '2015-02-23 14:49:36.233', 'CN90053', '124.205.178.150', '2015-03-03 10:40:40.340', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (818, 10691, 5, '0', '2015-04-27 16:32:28.047', 'CN90053', '124.205.178.210', '2015-05-11 12:18:20.623', 'CN90053', '124.205.178.210');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (839, 10692, 3, '0', '2015-07-11 17:10:58.297', 'CN90053', '114.112.126.218', '2015-07-21 10:21:55.450', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (840, 10693, 4, '0', '2015-07-11 17:10:58.310', 'CN90053', '114.112.126.218', '2015-07-21 10:21:55.450', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (841, 10694, 5, '0', '2015-07-11 17:10:58.327', 'CN90053', '114.112.126.218', '2015-07-27 17:28:41.717', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (847, 10699, 1, '1', '2015-09-01 11:50:41.200', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.513', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (852, 10708, 3, '0', '2015-09-01 12:55:34.750', 'CN90053', '114.112.126.218', '2015-09-14 12:25:50.340', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (854, 10730, 5, '1', '2015-09-14 12:25:50.387', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.543', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (859, 10811, 4, '0', '2015-09-20 15:56:03.200', 'CN90053', '114.112.126.218', '2015-09-21 13:34:29.590', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (868, 10812, 5, '0', '2015-09-22 15:39:54.750', 'CN90053', '114.112.126.218', '2015-09-22 16:07:08.733', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (863, 10813, 4, '0', '2015-09-21 13:34:29.653', 'CN90053', '114.112.126.218', '2015-09-22 15:39:54.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (877, 10817, 3, '0', '2015-09-22 17:48:39.547', 'CN90053', '114.112.126.218', '2015-10-13 11:21:27.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (878, 10818, 4, '0', '2015-09-22 17:48:39.560', 'CN90053', '114.112.126.218', '2015-10-13 11:21:27.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (872, 10819, 4, '0', '2015-09-22 16:07:08.763', 'CN90053', '114.112.126.218', '2015-09-22 16:17:47.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (873, 10820, 5, '0', '2015-09-22 16:07:08.763', 'CN90053', '114.112.126.218', '2015-09-22 16:17:47.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (888, 10821, 5, '0', '2015-11-09 15:08:09.797', 'CN90053', '114.112.126.218', '2015-11-16 17:50:15.373', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (883, 10824, 5, '0', '2015-09-30 06:15:17.810', 'CN90053', '114.112.126.218', '2015-10-13 11:21:27.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (889, 11055, 13, '1', '2018-07-13 18:42:42.523', 'CN12654', '210.72.232.131', '2018-07-13 18:42:42.523', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (890, 11056, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (891, 11057, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (892, 11058, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (893, 11059, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLNewItemList] ([ItemSeq], [GoldItemID], [OrderNO], [Status], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (894, 11060, 13, '1', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

