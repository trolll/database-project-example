/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLBilling
Source Table          : TBLBestItemList
Date                  : 2023-10-04 18:57:42
*/


INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1, 1, '0', 4, '2008-01-09 14:13:42.000', NULL, NULL, '2013-11-05 10:47:45.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (2, 2, '0', 1, '2008-01-09 14:13:42.000', NULL, NULL, '2014-03-30 21:13:51.483', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (3, 3, '0', 8, '2008-01-09 14:13:42.000', NULL, NULL, '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (4, 4, '0', 4, '2008-01-09 14:13:42.000', NULL, NULL, '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (5, 5, '0', 3, '2008-01-09 14:13:42.000', NULL, NULL, '2008-03-26 15:04:04.900', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (6, 6, '0', 6, '2008-01-09 14:13:42.000', NULL, NULL, '2008-03-07 18:21:33.830', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (7, 7, '0', 7, '2008-01-09 14:13:42.000', NULL, NULL, '2008-03-07 18:21:33.847', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (8, 8, '0', 1, '2008-01-09 14:13:42.000', NULL, NULL, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (9, 9, '0', 10, '2008-01-09 14:13:42.000', NULL, NULL, '2008-04-16 10:26:28.887', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10, 10, '0', 9, '2008-01-09 14:13:42.000', NULL, NULL, '2008-03-07 18:21:33.863', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (48, 14, '0', 10, '2008-04-30 07:29:19.507', 'KR10643', '210.72.232.170', '2008-04-30 07:29:19.507', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (30, 18, '0', 7, '2008-03-26 16:19:27.807', 'CN12504', '210.72.232.228', '2008-04-03 13:08:08.850', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (20, 19, '0', 8, '2008-02-28 11:15:37.090', '', '127.0.0.1', '2012-02-22 14:58:13.233', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (26, 20, '0', 9, '2008-03-26 16:19:27.790', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.460', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (27, 21, '0', 10, '2008-03-26 16:19:27.790', 'CN12504', '210.72.232.228', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (29, 22, '0', 6, '2008-03-26 16:19:27.807', 'CN12504', '210.72.232.228', '2008-04-03 13:08:08.677', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (23, 25, '0', 3, '2008-03-26 16:19:27.760', 'CN12504', '210.72.232.228', '2012-02-22 14:55:12.397', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (24, 26, '0', 7, '2008-03-26 16:19:27.777', 'CN12504', '210.72.232.228', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (172, 33, '0', 7, '2013-11-26 16:03:58.937', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (178, 35, '0', 8, '2013-11-26 16:03:58.983', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (21, 36, '0', 11, '2008-02-28 11:15:37.140', '', '127.0.0.1', '2008-05-28 07:38:10.387', '', '127.0.0.1');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (174, 37, '0', 2, '2013-11-26 16:03:58.950', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (176, 38, '0', 1, '2013-11-26 16:03:58.967', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (106, 42, '0', 3, '2012-02-22 14:58:13.297', 'CN90053', '61.152.133.230', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (82, 43, '0', 1, '2009-08-04 13:42:48.327', 'CN12654', '10.34.223.138', '2009-08-10 17:38:00.233', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (242, 46, '0', 9, '2014-10-08 11:42:12.810', 'CN90053', '124.205.178.150', '2015-09-08 11:10:12.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (22, 48, '0', 12, '2008-02-28 11:15:37.200', '', '127.0.0.1', '2008-05-28 07:38:10.387', '', '127.0.0.1');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (124, 50, '0', 1, '2012-07-20 21:42:39.483', 'CN90053', '61.152.133.230', '2013-01-29 10:37:16.950', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (188, 51, '0', 2, '2014-01-28 10:47:09.000', 'CN90053', '222.128.29.241', '2014-02-11 10:33:36.700', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (189, 52, '0', 8, '2014-01-28 10:47:09.013', 'CN90053', '222.128.29.241', '2015-09-08 11:10:12.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (175, 58, '0', 4, '2013-11-26 16:03:58.967', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (128, 61, '0', 3, '2012-07-20 21:42:39.513', 'CN90053', '61.152.133.230', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (152, 66, '0', 8, '2012-11-22 10:42:33.700', '', '61.152.133.230', '2015-01-25 12:39:47.653', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (38, 126, '0', 8, '2008-04-02 09:54:20.230', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.413', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (57, 129, '0', 10, '2008-05-01 10:06:51.357', 'CN12504', '210.72.232.228', '2008-09-09 07:09:18.523', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (58, 134, '0', 10, '2008-05-01 10:06:51.513', 'CN12504', '210.72.232.228', '2008-08-13 07:21:14.357', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (49, 135, '0', 3, '2008-05-01 10:06:50.027', 'CN12504', '210.72.232.228', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (187, 152, '0', 5, '2013-11-28 22:15:00.437', 'CN90053', '222.128.29.241', '2013-11-30 14:05:55.717', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (60, 157, '0', 2, '2008-08-13 07:21:09.297', 'CN12654', '210.72.232.170', '2008-09-23 07:04:36.437', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (62, 158, '0', 3, '2008-09-23 07:04:31.187', 'CN12654', '210.72.232.170', '2013-11-28 19:07:07.890', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (182, 177, '0', 6, '2013-11-26 20:30:31.920', 'CN90053', '222.128.29.241', '2014-09-18 14:19:06.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (61, 178, '0', 2, '2008-09-09 07:07:01.920', 'CN12654', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (184, 185, '1', 4, '2013-11-28 22:15:00.420', 'CN90053', '222.128.29.241', '2016-04-21 12:27:31.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (186, 186, '1', 5, '2013-11-28 22:15:00.437', 'CN90053', '222.128.29.241', '2016-04-21 12:27:31.687', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (72, 193, '0', 4, '2008-12-23 11:17:32.767', 'KR13076', '210.72.232.170', '2013-12-03 10:42:09.373', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (73, 194, '0', 12, '2008-12-23 11:17:32.877', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (74, 195, '0', 13, '2008-12-23 11:17:32.970', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (75, 196, '0', 14, '2008-12-23 11:17:33.080', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (76, 197, '0', 15, '2008-12-23 11:17:33.173', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (77, 198, '0', 16, '2008-12-23 11:17:33.297', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (78, 203, '0', 17, '2008-12-23 11:17:33.390', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (79, 204, '0', 8, '2008-12-23 11:17:33.487', 'KR13076', '210.72.232.170', '2012-03-13 11:12:37.190', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (80, 206, '0', 19, '2008-12-23 11:17:33.593', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (81, 208, '0', 20, '2008-12-23 11:17:33.690', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (91, 308, '0', 8, '2009-08-04 13:42:49.733', 'CN12654', '10.34.223.138', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (89, 325, '0', 6, '2009-08-04 13:42:49.500', 'CN12654', '10.34.223.138', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (114, 326, '0', 6, '2012-04-05 15:10:21.403', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (112, 345, '0', 4, '2012-04-05 15:10:21.390', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (108, 361, '0', 1, '2012-03-13 11:12:37.220', 'CN90053', '61.152.133.230', '2014-02-11 10:33:36.700', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (96, 373, '0', 5, '2012-02-22 14:55:12.457', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (95, 375, '0', 4, '2012-02-22 14:55:12.443', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (113, 376, '0', 5, '2012-04-05 15:10:21.403', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (123, 377, '0', 8, '2012-04-05 15:30:20.780', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (115, 378, '0', 7, '2012-04-05 15:10:21.403', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (132, 379, '0', 8, '2012-07-20 21:42:39.530', 'CN90053', '61.152.133.230', '2012-10-08 12:01:49.747', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (131, 394, '0', 7, '2012-07-20 21:42:39.530', 'CN90053', '61.152.133.230', '2012-10-08 12:01:49.747', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (130, 397, '0', 7, '2012-07-20 21:42:39.530', 'CN90053', '61.152.133.230', '2012-07-20 22:23:09.480', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (151, 398, '0', 7, '2012-10-11 21:21:32.170', '', '61.152.133.230', '2012-11-22 10:42:33.653', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (164, 407, '0', 8, '2013-05-07 11:00:22.093', 'CN90053', '222.128.26.192', '2013-08-12 14:34:21.530', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (163, 413, '0', 7, '2013-05-07 11:00:22.063', 'CN90053', '222.128.26.192', '2013-11-05 10:47:45.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (167, 414, '0', 10, '2013-11-05 10:47:45.840', 'CN90053', '222.128.29.241', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (142, 418, '0', 1, '2012-07-20 22:36:22.200', 'CN90053', '61.152.133.230', '2013-07-02 10:59:07.577', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (168, 419, '0', 9, '2013-11-05 10:47:45.857', 'CN90053', '222.128.29.241', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (133, 423, '0', 10, '2012-07-20 21:42:39.547', 'CN90053', '61.152.133.230', '2013-11-26 16:03:58.903', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (129, 426, '0', 1, '2012-07-20 21:42:39.513', 'CN90053', '61.152.133.230', '2013-11-05 10:47:45.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (162, 436, '0', 7, '2013-04-27 11:00:23.170', 'CN90053', '222.128.26.192', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (143, 437, '0', 8, '2012-09-28 10:48:40.997', '', '61.152.133.230', '2013-11-26 16:03:58.903', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (144, 438, '0', 9, '2012-09-28 10:48:41.013', '', '61.152.133.230', '2013-11-26 16:03:58.903', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (161, 439, '0', 8, '2013-04-27 11:00:23.170', 'CN90053', '222.128.26.192', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (190, 440, '0', 1, '2014-08-12 11:20:17.810', 'CN90053', '124.205.178.150', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (240, 540, '1', 7, '2014-10-08 11:42:12.793', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.700', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (250, 592, '0', 4, '2014-11-05 12:15:41.983', 'CN90053', '124.205.178.150', '2014-12-22 12:20:54.123', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (255, 608, '0', 10, '2014-11-05 12:15:42.013', 'CN90053', '124.205.178.150', '2014-11-21 15:34:30.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (192, 612, '0', 2, '2014-09-10 14:16:57.310', 'CN90053', '124.205.178.150', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (195, 613, '0', 4, '2014-09-10 17:48:10.590', 'CN90053', '124.205.178.150', '2015-02-12 16:41:28.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (196, 614, '0', 5, '2014-09-10 17:48:10.590', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (197, 615, '0', 4, '2014-09-10 17:48:10.607', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (206, 616, '0', 9, '2014-09-10 20:31:24.733', 'CN90053', '124.205.178.150', '2014-09-18 12:13:08.750', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (205, 617, '0', 9, '2014-09-10 20:31:24.733', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (204, 618, '0', 8, '2014-09-10 20:31:24.717', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (272, 622, '0', 7, '2014-12-02 10:53:29.420', 'CN90053', '124.205.178.150', '2014-12-22 12:20:54.123', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (245, 629, '1', 6, '2014-10-08 11:42:12.827', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.700', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (212, 631, '0', 6, '2014-09-18 14:19:06.090', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (213, 634, '0', 7, '2014-09-18 14:19:06.090', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (216, 640, '0', 10, '2014-09-18 14:19:06.123', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (225, 648, '0', 6, '2014-09-28 15:55:48.483', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (221, 650, '0', 2, '2014-09-28 15:55:48.483', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (234, 651, '0', 3, '2014-09-28 16:48:41.450', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (233, 652, '0', 8, '2014-09-28 16:48:41.437', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (229, 653, '0', 4, '2014-09-28 16:48:41.403', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (232, 654, '0', 7, '2014-09-28 16:48:41.437', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (260, 656, '0', 5, '2014-11-21 15:34:30.077', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (265, 657, '0', 10, '2014-11-21 15:34:30.123', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (262, 658, '0', 7, '2014-11-21 15:34:30.107', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (237, 659, '0', 9, '2014-09-28 18:21:29.560', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (263, 660, '0', 8, '2014-11-21 15:34:30.107', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (261, 661, '0', 4, '2014-11-21 15:34:30.090', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (273, 663, '0', 6, '2014-12-26 19:30:00.233', 'CN90053', '124.205.178.150', '2015-04-27 16:32:28.153', 'CN90053', '58.30.128.109');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (274, 664, '0', 1, '2014-12-30 12:29:04.937', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (284, 665, '0', 1, '2015-01-25 12:41:07.937', 'CN90053', '124.205.178.150', '2015-02-09 12:38:40.983', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (302, 667, '1', 3, '2015-03-17 14:32:37.310', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.653', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (283, 669, '0', 7, '2015-01-25 12:39:47.780', 'CN90053', '124.205.178.150', '2015-09-08 11:10:12.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (294, 670, '1', 2, '2015-02-09 12:39:10.560', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.653', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (295, 678, '0', 1, '2015-02-12 16:41:28.750', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (296, 680, '0', 2, '2015-02-12 16:41:28.780', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (297, 681, '0', 3, '2015-02-12 16:41:28.797', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (298, 682, '0', 4, '2015-02-12 16:41:28.797', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (299, 683, '0', 3, '2015-02-12 16:41:28.810', 'CN90053', '124.205.178.150', '2015-06-01 20:42:46.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (300, 684, '0', 1, '2015-02-12 16:41:28.810', 'CN90053', '124.205.178.150', '2015-05-29 17:25:49.717', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (305, 685, '0', 2, '2015-05-29 09:08:03.250', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (306, 689, '0', 1, '2015-05-29 17:25:49.733', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (303, 691, '0', 1, '2015-04-27 16:32:28.340', 'CN90053', '124.205.178.210', '2015-04-28 11:20:59.890', 'CN90053', '124.205.178.210');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (313, 723, '1', 8, '2015-09-08 11:10:12.200', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.717', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (314, 727, '1', 9, '2015-09-08 11:10:12.217', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.717', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (315, 729, '1', 10, '2015-09-08 11:10:12.233', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.730', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (316, 731, '1', 1, '2015-09-14 12:32:46.217', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.640', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (317, 821, '0', 1, '2015-11-09 15:08:09.873', 'CN90053', '114.112.126.218', '2015-11-16 17:50:15.530', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (318, 1055, '1', 13, '2018-07-13 18:42:42.523', 'CN12654', '210.72.232.131', '2018-07-13 18:42:42.523', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (319, 1056, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (320, 1057, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (321, 1058, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (322, 1059, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (323, 1060, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (647, 1061, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (648, 1062, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (649, 1063, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (650, 1064, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (651, 1065, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (652, 1066, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (653, 1067, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (654, 1068, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (655, 1069, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (656, 1070, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (657, 1071, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (658, 1072, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (659, 1073, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (660, 1074, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (661, 1075, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (662, 1076, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (663, 1077, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (664, 1078, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (665, 1079, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (666, 1080, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (667, 1081, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (668, 1082, '1', 168, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (669, 1083, '1', 13, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (670, 1084, '1', 13, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (671, 1085, '1', 13, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (672, 1086, '1', 13, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (673, 1087, '1', 13, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (674, 1088, '1', 13, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (675, 1089, '1', 13, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (324, 10001, '0', 4, '2008-01-09 14:13:42.000', NULL, NULL, '2013-11-05 10:47:45.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (325, 10002, '0', 1, '2008-01-09 14:13:42.000', NULL, NULL, '2014-03-30 21:13:51.483', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (326, 10003, '0', 8, '2008-01-09 14:13:42.000', NULL, NULL, '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (327, 10004, '0', 4, '2008-01-09 14:13:42.000', NULL, NULL, '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (328, 10005, '0', 3, '2008-01-09 14:13:42.000', NULL, NULL, '2008-03-26 15:04:04.900', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (329, 10006, '0', 6, '2008-01-09 14:13:42.000', NULL, NULL, '2008-03-07 18:21:33.830', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (330, 10007, '0', 7, '2008-01-09 14:13:42.000', NULL, NULL, '2008-03-07 18:21:33.847', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (331, 10008, '0', 1, '2008-01-09 14:13:42.000', NULL, NULL, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (332, 10009, '0', 10, '2008-01-09 14:13:42.000', NULL, NULL, '2008-04-16 10:26:28.887', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (333, 10010, '0', 9, '2008-01-09 14:13:42.000', NULL, NULL, '2008-03-07 18:21:33.863', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (371, 10014, '0', 10, '2008-04-30 07:29:19.507', 'KR10643', '210.72.232.170', '2008-04-30 07:29:19.507', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (353, 10018, '0', 7, '2008-03-26 16:19:27.807', 'CN12504', '210.72.232.228', '2008-04-03 13:08:08.850', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (343, 10019, '0', 8, '2008-02-28 11:15:37.090', '', '127.0.0.1', '2012-02-22 14:58:13.233', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (349, 10020, '0', 9, '2008-03-26 16:19:27.790', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.460', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (350, 10021, '0', 10, '2008-03-26 16:19:27.790', 'CN12504', '210.72.232.228', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (352, 10022, '0', 6, '2008-03-26 16:19:27.807', 'CN12504', '210.72.232.228', '2008-04-03 13:08:08.677', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (346, 10025, '0', 3, '2008-03-26 16:19:27.760', 'CN12504', '210.72.232.228', '2012-02-22 14:55:12.397', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (347, 10026, '0', 7, '2008-03-26 16:19:27.777', 'CN12504', '210.72.232.228', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (495, 10033, '0', 7, '2013-11-26 16:03:58.937', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (501, 10035, '0', 8, '2013-11-26 16:03:58.983', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (344, 10036, '0', 11, '2008-02-28 11:15:37.140', '', '127.0.0.1', '2008-05-28 07:38:10.387', '', '127.0.0.1');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (497, 10037, '0', 2, '2013-11-26 16:03:58.950', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (499, 10038, '0', 1, '2013-11-26 16:03:58.967', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (429, 10042, '0', 3, '2012-02-22 14:58:13.297', 'CN90053', '61.152.133.230', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (405, 10043, '0', 1, '2009-08-04 13:42:48.327', 'CN12654', '10.34.223.138', '2009-08-10 17:38:00.233', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (565, 10046, '0', 9, '2014-10-08 11:42:12.810', 'CN90053', '124.205.178.150', '2015-09-08 11:10:12.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (345, 10048, '0', 12, '2008-02-28 11:15:37.200', '', '127.0.0.1', '2008-05-28 07:38:10.387', '', '127.0.0.1');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (447, 10050, '0', 1, '2012-07-20 21:42:39.483', 'CN90053', '61.152.133.230', '2013-01-29 10:37:16.950', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (511, 10051, '0', 2, '2014-01-28 10:47:09.000', 'CN90053', '222.128.29.241', '2014-02-11 10:33:36.700', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (512, 10052, '0', 8, '2014-01-28 10:47:09.013', 'CN90053', '222.128.29.241', '2015-09-08 11:10:12.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (498, 10058, '0', 4, '2013-11-26 16:03:58.967', 'CN90053', '222.128.29.241', '2013-11-30 18:11:51.140', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (451, 10061, '0', 3, '2012-07-20 21:42:39.513', 'CN90053', '61.152.133.230', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (475, 10066, '0', 8, '2012-11-22 10:42:33.700', '', '61.152.133.230', '2015-01-25 12:39:47.653', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (361, 10126, '0', 8, '2008-04-02 09:54:20.230', 'CN12504', '210.72.232.228', '2008-04-30 07:29:19.413', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (380, 10129, '0', 10, '2008-05-01 10:06:51.357', 'CN12504', '210.72.232.228', '2008-09-09 07:09:18.523', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (381, 10134, '0', 10, '2008-05-01 10:06:51.513', 'CN12504', '210.72.232.228', '2008-08-13 07:21:14.357', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (372, 10135, '0', 3, '2008-05-01 10:06:50.027', 'CN12504', '210.72.232.228', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (510, 10152, '0', 5, '2013-11-28 22:15:00.437', 'CN90053', '222.128.29.241', '2013-11-30 14:05:55.717', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (383, 10157, '0', 2, '2008-08-13 07:21:09.297', 'CN12654', '210.72.232.170', '2008-09-23 07:04:36.437', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (385, 10158, '0', 3, '2008-09-23 07:04:31.187', 'CN12654', '210.72.232.170', '2013-11-28 19:07:07.890', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (505, 10177, '0', 6, '2013-11-26 20:30:31.920', 'CN90053', '222.128.29.241', '2014-09-18 14:19:06.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (384, 10178, '0', 2, '2008-09-09 07:07:01.920', 'CN12654', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (507, 10185, '1', 4, '2013-11-28 22:15:00.420', 'CN90053', '222.128.29.241', '2016-04-21 12:27:31.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (509, 10186, '1', 5, '2013-11-28 22:15:00.437', 'CN90053', '222.128.29.241', '2016-04-21 12:27:31.687', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (395, 10193, '0', 4, '2008-12-23 11:17:32.767', 'KR13076', '210.72.232.170', '2013-12-03 10:42:09.373', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (396, 10194, '0', 12, '2008-12-23 11:17:32.877', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (397, 10195, '0', 13, '2008-12-23 11:17:32.970', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (398, 10196, '0', 14, '2008-12-23 11:17:33.080', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (399, 10197, '0', 15, '2008-12-23 11:17:33.173', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (400, 10198, '0', 16, '2008-12-23 11:17:33.297', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (401, 10203, '0', 17, '2008-12-23 11:17:33.390', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (402, 10204, '0', 8, '2008-12-23 11:17:33.487', 'KR13076', '210.72.232.170', '2012-03-13 11:12:37.190', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (403, 10206, '0', 19, '2008-12-23 11:17:33.593', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (404, 10208, '0', 20, '2008-12-23 11:17:33.690', 'KR13076', '210.72.232.170', '2009-08-04 13:42:48.220', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (414, 10308, '0', 8, '2009-08-04 13:42:49.733', 'CN12654', '10.34.223.138', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (412, 10325, '0', 6, '2009-08-04 13:42:49.500', 'CN12654', '10.34.223.138', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (437, 10326, '0', 6, '2012-04-05 15:10:21.403', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (435, 10345, '0', 4, '2012-04-05 15:10:21.390', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (431, 10361, '0', 1, '2012-03-13 11:12:37.220', 'CN90053', '61.152.133.230', '2014-02-11 10:33:36.700', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (419, 10373, '0', 5, '2012-02-22 14:55:12.457', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (418, 10375, '0', 4, '2012-02-22 14:55:12.443', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (436, 10376, '0', 5, '2012-04-05 15:10:21.403', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (446, 10377, '0', 8, '2012-04-05 15:30:20.780', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (438, 10378, '0', 7, '2012-04-05 15:10:21.403', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.450', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (455, 10379, '0', 8, '2012-07-20 21:42:39.530', 'CN90053', '61.152.133.230', '2012-10-08 12:01:49.747', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (454, 10394, '0', 7, '2012-07-20 21:42:39.530', 'CN90053', '61.152.133.230', '2012-10-08 12:01:49.747', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (453, 10397, '0', 7, '2012-07-20 21:42:39.530', 'CN90053', '61.152.133.230', '2012-07-20 22:23:09.480', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (474, 10398, '0', 7, '2012-10-11 21:21:32.170', '', '61.152.133.230', '2012-11-22 10:42:33.653', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (487, 10407, '0', 8, '2013-05-07 11:00:22.093', 'CN90053', '222.128.26.192', '2013-08-12 14:34:21.530', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (486, 10413, '0', 7, '2013-05-07 11:00:22.063', 'CN90053', '222.128.26.192', '2013-11-05 10:47:45.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (490, 10414, '0', 10, '2013-11-05 10:47:45.840', 'CN90053', '222.128.29.241', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (465, 10418, '0', 1, '2012-07-20 22:36:22.200', 'CN90053', '61.152.133.230', '2013-07-02 10:59:07.577', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (491, 10419, '0', 9, '2013-11-05 10:47:45.857', 'CN90053', '222.128.29.241', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (456, 10423, '0', 10, '2012-07-20 21:42:39.547', 'CN90053', '61.152.133.230', '2013-11-26 16:03:58.903', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (452, 10426, '0', 1, '2012-07-20 21:42:39.513', 'CN90053', '61.152.133.230', '2013-11-05 10:47:45.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (485, 10436, '0', 7, '2013-04-27 11:00:23.170', 'CN90053', '222.128.26.192', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (466, 10437, '0', 8, '2012-09-28 10:48:40.997', '', '61.152.133.230', '2013-11-26 16:03:58.903', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (467, 10438, '0', 9, '2012-09-28 10:48:41.013', '', '61.152.133.230', '2013-11-26 16:03:58.903', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (484, 10439, '0', 8, '2013-04-27 11:00:23.170', 'CN90053', '222.128.26.192', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (513, 10440, '0', 1, '2014-08-12 11:20:17.810', 'CN90053', '124.205.178.150', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (563, 10540, '1', 7, '2014-10-08 11:42:12.793', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.700', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (573, 10592, '0', 4, '2014-11-05 12:15:41.983', 'CN90053', '124.205.178.150', '2014-12-22 12:20:54.123', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (578, 10608, '0', 10, '2014-11-05 12:15:42.013', 'CN90053', '124.205.178.150', '2014-11-21 15:34:30.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (515, 10612, '0', 2, '2014-09-10 14:16:57.310', 'CN90053', '124.205.178.150', '2014-09-10 17:48:10.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (518, 10613, '0', 4, '2014-09-10 17:48:10.590', 'CN90053', '124.205.178.150', '2015-02-12 16:41:28.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (519, 10614, '0', 5, '2014-09-10 17:48:10.590', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (520, 10615, '0', 4, '2014-09-10 17:48:10.607', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (529, 10616, '0', 9, '2014-09-10 20:31:24.733', 'CN90053', '124.205.178.150', '2014-09-18 12:13:08.750', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (528, 10617, '0', 9, '2014-09-10 20:31:24.733', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (527, 10618, '0', 8, '2014-09-10 20:31:24.717', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (595, 10622, '0', 7, '2014-12-02 10:53:29.420', 'CN90053', '124.205.178.150', '2014-12-22 12:20:54.123', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (568, 10629, '1', 6, '2014-10-08 11:42:12.827', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.700', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (535, 10631, '0', 6, '2014-09-18 14:19:06.090', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (536, 10634, '0', 7, '2014-09-18 14:19:06.090', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (539, 10640, '0', 10, '2014-09-18 14:19:06.123', 'CN90053', '124.205.178.150', '2014-09-28 11:03:45.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (548, 10648, '0', 6, '2014-09-28 15:55:48.483', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (544, 10650, '0', 2, '2014-09-28 15:55:48.483', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (557, 10651, '0', 3, '2014-09-28 16:48:41.450', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (556, 10652, '0', 8, '2014-09-28 16:48:41.437', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (552, 10653, '0', 4, '2014-09-28 16:48:41.403', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (555, 10654, '0', 7, '2014-09-28 16:48:41.437', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (583, 10656, '0', 5, '2014-11-21 15:34:30.077', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (588, 10657, '0', 10, '2014-11-21 15:34:30.123', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (585, 10658, '0', 7, '2014-11-21 15:34:30.107', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (560, 10659, '0', 9, '2014-09-28 18:21:29.560', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (586, 10660, '0', 8, '2014-11-21 15:34:30.107', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.327', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (584, 10661, '0', 4, '2014-11-21 15:34:30.090', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (596, 10663, '0', 6, '2014-12-26 19:30:00.233', 'CN90053', '124.205.178.150', '2015-04-27 16:32:28.153', 'CN90053', '58.30.128.109');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (597, 10664, '0', 1, '2014-12-30 12:29:04.937', 'CN90053', '124.205.178.150', '2015-01-06 10:27:49.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (607, 10665, '0', 1, '2015-01-25 12:41:07.937', 'CN90053', '124.205.178.150', '2015-02-09 12:38:40.983', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (625, 10667, '1', 3, '2015-03-17 14:32:37.310', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.653', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (606, 10669, '0', 7, '2015-01-25 12:39:47.780', 'CN90053', '124.205.178.150', '2015-09-08 11:10:12.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (617, 10670, '1', 2, '2015-02-09 12:39:10.560', 'CN90053', '124.205.178.150', '2016-04-21 12:27:31.653', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (618, 10678, '0', 1, '2015-02-12 16:41:28.750', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (619, 10680, '0', 2, '2015-02-12 16:41:28.780', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (620, 10681, '0', 3, '2015-02-12 16:41:28.797', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (621, 10682, '0', 4, '2015-02-12 16:41:28.797', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (622, 10683, '0', 3, '2015-02-12 16:41:28.810', 'CN90053', '124.205.178.150', '2015-06-01 20:42:46.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (623, 10684, '0', 1, '2015-02-12 16:41:28.810', 'CN90053', '124.205.178.150', '2015-05-29 17:25:49.717', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (628, 10685, '0', 2, '2015-05-29 09:08:03.250', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (629, 10689, '0', 1, '2015-05-29 17:25:49.733', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (626, 10691, '0', 1, '2015-04-27 16:32:28.340', 'CN90053', '124.205.178.210', '2015-04-28 11:20:59.890', 'CN90053', '124.205.178.210');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (636, 10723, '1', 8, '2015-09-08 11:10:12.200', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.717', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (637, 10727, '1', 9, '2015-09-08 11:10:12.217', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.717', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (638, 10729, '1', 10, '2015-09-08 11:10:12.233', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.730', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (639, 10731, '1', 1, '2015-09-14 12:32:46.217', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.640', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (640, 10821, '0', 1, '2015-11-09 15:08:09.873', 'CN90053', '114.112.126.218', '2015-11-16 17:50:15.530', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (641, 11055, '1', 13, '2018-07-13 18:42:42.523', 'CN12654', '210.72.232.131', '2018-07-13 18:42:42.523', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (642, 11056, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (643, 11057, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (644, 11058, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (645, 11059, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLBestItemList] ([ItemSeq], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (646, 11060, '1', 13, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

