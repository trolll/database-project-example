/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLBilling
Source Table          : TBLGoldItem
Date                  : 2023-10-04 18:57:42
*/


INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (3, 1588, '破封之咒', '', '用于解除被封印物品的封印状态,根据被封印物品强化程度的高低,需要不同数量的破封之咒.

双击破封之咒后,左键点击要解除封印的物品,即可解封.', 30, 30, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:23.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (4, 1589, '浓缩治疗药水', '', '能够大量恢复自身生命值的药剂,*1但只能在各大陆的村镇中才可实现购买*9.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 2, 2, '1', '0', '0', 0, 10, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:33.170', 'CN13117', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (5, 1590, '传送记忆列表(A)', '', '在20天内,*1可额外增加传送之咒中10个记录点*9.
双击鼠标左键使用.使用后可按*1T键*9查询剩余时间.', 30, 30, '1', '0', '0', 0, 1, 480, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-18 13:57:41.293', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (6, 1591, '传送记忆列表(B)', '', '在20天内,*1可额外增加传送之咒中20个记录点*9.
双击鼠标左键使用.使用后可按*1T键*9查询剩余时间.', 90, 75, '1', '0', '0', 0, 1, 480, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.920', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (8, 1593, '公会之力', '', '使用这个的话，在所属公会状态下能受到技能树的优惠', 200, 200, '1', '0', '0', 0, 1, 2, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:41.653', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (9, 1594, '地下城入场券(12小时)', '', '双击左键后使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期12小时.
使用后可按*1T键*9查询剩余时间.', 35, 35, '1', '0', '0', 0, 1, 12, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.763', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10, 1595, '哈德斯守护(50%)', '', '当角色最后死亡后,可50%恢复最后一次死亡时所失去的经验值.
死亡后,双击鼠标左键使用.', 24, 20, '1', '0', '0', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.810', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11, 1596, '哈德斯守护(100%)', '', '当角色最后死亡后,可*1100%恢复*9最后一次死亡时所失去的经验值.
死亡后,*1双击鼠标左键使用*9.', 60, 50, '1', '0', '0', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:37.827', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (12, 1597, '戒指解封之咒(A)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 80, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (13, 1598, '戒指解封之咒(B)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 80, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (14, 1599, '项链解封之咒', '', '使用后开启装备栏中的项链栏,从而可佩戴项链.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 80, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.203', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (15, 1600, '腰带解封之咒', '', '使用后开启装备栏中的腰带栏,从而可佩戴腰带.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 80, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.157', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (16, 1601, '披风解封之咒', '', '使用后开启装备栏中的披风栏,从而可佩戴披风.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 80, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.110', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (17, 1617, '拜师申请券', '', '请您到各大陆的村镇中找*1<师徒>管理员*9协助您使用拜师申请券,使用后可恢复1次拜师申请.', 100, 100, '1', '0', '0', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.687', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (18, 1618, '德拉克召唤书(30天)', '', '使用后可召唤超级德拉克,同时可随时收回.

召唤出的德拉克可在地下城内骑乘.
双击鼠标左键使用,*1购买后开始计时间*9,有效时间30天.', 580, 580, '1', '0', '0', 30, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.623', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (19, 1619, '传送之咒', '', '聊天窗口中输入*1／记忆(空格)XXX*9可记忆角色当前地点(10个,吉内亚岛除外).

直接输入*1／记忆*9可查询已保存的记录点或删除记录点,但无法进行传送.

双击本道具选择记录点后可进行传送,每次传送*1消耗1个传送之咒*9.', 5, 3, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:26.857', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (20, 1620, '变身之咒', '', '双击使用可变化成怪物形象,同时可拥有怪物部分攻击属性.随着等级提升可选择变化的怪物逐渐增多.*1变身持续时间20-30分钟*9.

双击变身之咒但不变身,可令使用者恢复原貌.恢复原貌也将消耗1个变身之咒.', 8, 8, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (21, 1623, '米泰欧斯的守护', '', '强化装备失败时,行囊内如有米泰欧斯的守护,*1可一定几率的自动保护强化失败的装备*9.

无论保护成功或失败,*1米泰欧斯的守护都会消失*9.', 80, 80, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:22.703', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (22, 1624, '玛尔斯的守护', '', '当角色死亡时，玛尔斯的守护会随机自动保护*11件物品不掉落*9.', 0, 0, '1', '0', '2', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:25.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (23, 1625, '公会仓库', '', '公会仓激活后,会创建全体公会成员共同使用的公会仓库。有效时间30天.
公会成员均可购买并激活公会公会仓库.
只有公会长有设置密码的权利.', 150, 150, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:39.030', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (24, 1626, '高级公会仓库', '', '除公会仓库功能外,公会长还可赋予指定会员权限,让他可使用仓库.
有效时间30天.
只有公会长有设置密码的权利.', 250, 250, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.967', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (27, 409, '银币', '', '银铸造的钱币.', 0, 0, '1', '0', '0', 0, 50000, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:41.483', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (28, 1619, '移动之咒', '', '可以移动到你所想移动的地方的魔法之咒。', 0, 0, '1', '0', '0', 0, 50, 0, '2008-01-09 12:59:08.000', NULL, NULL, '2014-08-12 12:06:41.530', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (29, 1620, '变换之咒', '', '可以变成你所喜欢的怪物的魔法之咒。', 0, 0, '1', '0', '0', 0, 50, 0, '2008-01-09 12:59:08.000', NULL, NULL, '2014-08-12 12:06:41.577', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (30, 50006, '1111111', '', '神秘礼包包含：
英雄箱子X1、
银币掉率增幅之咒(1.5倍)X1、基础精华X40', 990, 990, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.467', 'CN90053', '61.152.124.194');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (31, 50007, 'NO2不能用的包代码', '', '66级吉伽近战变身礼包：吉伽梅西战士(30日)月光精灵战士V(7日)   月光复仇者（加强）30日', 2880, 2880, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:31.200', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (33, 50009, '[秒杀]精华感恩礼包', '', '[秒杀]精华感恩礼包：含
*4含基础精华x50,
强化精华x5
德拉克神秘戒指x1*9
[秒杀]精华感恩礼包额外奖励：
*4祝福强化精华x1*9

秒杀感恩时间：
*111月26日16:00-20:00
11月28日19:00-21:00
11月30日14:00-18:00*9', 1410, 1280, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:30.670', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (36, 50012, '[新手学园专属]礼包', '', '[新手学园专属]礼包：礼包含装备解封之咒(30日)，旅行者之书(30日)X1，神秘德拉克戒指(30日)X1， 增幅之咒X1，辨识之书(30日)X1
温馨提示：装备解封之咒同时解封5个装备槽，之前使用当中的解封之咒时间也被注销，已新开启的时间为准。', 1030, 260, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:30.060', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (39, 50015, 'NO3不能用的包代码', '', '萝莉近战变身礼包：    修炼少女V(30日)      公主变身（加强）(30日)月光精灵战士V(7日) ', 2920, 2920, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:31.153', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (41, 50017, '2', '', '*1贤者智慧LV3（30日）*9 1个 
*4贤者智慧LV3 攻击速度和移动速度效果最强*9
变身强化精华LV3（30日）1个
幸运精华LV3（30日）1个', 960, 880, '2', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:29.607', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (43, 50019, '不可使用1', '', '内含芭芭莉安,暗黑法师,赫利肖舍里斯3个变身项链.原价7740G,限时惊爆价2460G', 7740, 2460, '3', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.403', 'CN90053', '222.128.26.192');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (46, 50022, '至尊猎人敏捷精华LV3礼包', '', '至尊猎人敏捷精华LV3礼包含
*1猎人敏捷精华LV3(30日）*9 1个 
*4猎人敏捷精华LV3 攻击速度和移动速度效果最强*9
变身强化精华LV3（30日）1个
幸运精华LV3（30日）1个
', 960, 880, '2', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:28.403', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (52, 50028, '至尊贤者智慧精华LV3礼包', '', '至尊贤者智慧精华LV3礼包含
*1贤者智慧LV3（30日）*9 1个 
*4贤者智慧LV3 攻击速度和移动速度效果最强*9
变身强化精华LV3（30日）1个
幸运精华LV3（30日）1个', 960, 880, '2', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:28.200', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (53, 50029, '变身强化精华LV3礼包[7天]', '', '变身强化精华Lv3,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+90，MP值+90', 100, 100, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.403', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (54, 50030, '英雄力量精华礼包[7天]', '', '英雄力量精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后力量+6,攻击速度/移动速度小幅度提高', 100, 100, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (55, 50031, '贤者智慧精华礼包[7天]', '', '贤者敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后智慧+6,攻击速度/移动速度小幅度提高', 100, 100, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (56, 50032, '猎人敏捷精华礼包[7天]', '', '猎人敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后敏捷+6,攻击速度/移动速度小幅度提高', 100, 100, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (59, 50035, '幸运精华LV3礼包[30天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 320, 320, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.747', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (60, 50036, '变身强化精华LV3礼包[30天]', '', '变身强化精华Lv3,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+90，MP值+90', 320, 320, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.793', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (61, 50037, '斗神祝福礼包', '', '购买后出现在金币道具栏内,
超级命中药水 10个
超级重击药水  5个
防御药水     10个
超级加速药水 10个', 120, 80, '3', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:26.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (69, 50045, 'package item-45', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:35.717', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (70, 50046, 'package item-46', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 0, 0, '2', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.263', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (71, 50047, 'package item-47', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.920', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (72, 50048, 'package item-48', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.983', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (73, 50049, 'package item-49', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.030', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (74, 50050, 'package item-50', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.077', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (75, 50051, 'package item-51', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.140', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (76, 50052, 'package item-52', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.187', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (77, 50053, 'package item-53', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.263', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (78, 50054, 'package item-54', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.310', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (79, 50055, 'package item-55', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.357', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (80, 50056, 'package item-56', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.420', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (81, 50057, 'package item-57', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.467', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (82, 50058, 'package item-58', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.513', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (83, 50059, '圣诞礼包', '', 'test', 0, 0, '4', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.623', 'CN13117', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (84, 50060, 'package item-60', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.577', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (85, 50061, 'package item-61', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.623', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (86, 50062, 'package item-62', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.670', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (87, 50063, 'package item-63', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.733', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (88, 50064, 'package item-64', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.780', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (89, 50065, 'package item-65', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.827', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (90, 50066, 'package item-66', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.890', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (91, 50067, 'package item-67', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.937', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (92, 50068, 'package item-68', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:40.983', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (93, 50069, 'package item-69', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:41.060', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (94, 50070, 'package item-70', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:41.107', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (95, 50071, 'package item-71', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:41.170', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (96, 50072, 'package item-72', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:41.217', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (97, 50073, 'package item-73', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:41.263', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (98, 50074, 'package item-74', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:41.310', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (99, 50075, 'package item-75', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:41.373', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (100, 50076, 'package item-76', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:41.420', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (101, 50077, 'package item-77', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.670', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (102, 50078, 'package item-78', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.607', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (103, 50079, 'package item-79', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 0, 0, '1', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:36.577', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (104, 50080, '超强新手礼包', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 0, 0, '1', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.577', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (105, 50081, '测试礼包1', '', 'test', 110, 110, '1', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.717', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (106, 50082, 'package item-82', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.670', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (107, 50083, 'package item-83', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.717', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (108, 50084, 'package item-84', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.763', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (109, 50085, 'package item-85', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.827', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (110, 50086, 'package item-86', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.873', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (111, 50087, '礼包', '', '深深礼包', 8, 7, '3', '1', '0', 1, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.873', 'CN12835', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (112, 50088, 'package item-88', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.967', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (113, 50089, 'package item-89', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.780', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (114, 50090, '礼包三1', '', '包含:基础精华x1,强化精华x1', 0, 0, '3', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.827', 'CN99019', '10.34.118.160');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (115, 50091, 'pack-1', '', '元旦礼包内含：项链解封之咒、戒指解封之咒（A）、戒指解封之咒（B）、腰带解封之咒、披风解封之咒、德拉克神秘戒指14天、浓缩治疗药水（100个）、传送之咒（20个）、超级加速药水（20个）、防御药水（20个）、疾速药水（20个）、米泰欧斯的守护（3个）。所有解封之咒的有效期都为30天。', 0, 0, '4', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:33.217', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (116, 50092, 'package item-92', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.170', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (117, 50093, 'package item-93', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.217', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (118, 50094, 'package item-94', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.280', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (119, 50095, 'package item-95', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.327', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (120, 50096, 'package item-96', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.390', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (121, 50097, 'package item-97', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.450', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (122, 50098, 'package item-98', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.513', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (123, 50099, 'package item-99', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 2000, 2000, '5', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:39.560', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (124, 63, '+0 兽人鳞甲', '', 'asdfg', 0, 0, '1', '0', '0', 0, 1, 0, '2008-03-03 16:18:51.930', '', '127.0.0.1', '2014-08-12 12:06:39.077', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (125, 1618, '德拉克召唤书(7天)', '', '使用后可召唤超级德拉克,同时可随时收回.

召唤出的德拉克可在地下城内骑.

双击鼠标左键使用,*1购买后开始计时间*9,有效时间7天. 

德拉克召唤书购买后不可进行出售，丢弃和存入仓库等操作.', 180, 180, '1', '0', '0', 7, 1, 0, '2008-03-17 15:05:15.037', 'CN12504', '210.72.232.228', '2014-08-12 13:19:36.890', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (126, 1618, '德拉克召唤书(14天)', '', '使用后可召唤超级德拉克,同时可随时收回.

召唤出的德拉克可在地下城内骑.

双击鼠标左键使用,*1购买后开始计时间*9,有效时间7天. 

德拉克召唤书购买后不可进行出售，丢弃和存入仓库等操作.', 300, 300, '1', '0', '0', 14, 1, 0, '2008-03-17 15:11:30.990', 'CN12504', '210.72.232.228', '2014-08-12 12:06:31.607', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (127, 1594, '地下城入场券(14天)', '', '双击左键使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期14天.
使用后可按*1T键*9查询剩余时间.', 180, 180, '1', '0', '0', 0, 1, 336, '2008-03-26 14:19:24.603', 'CN12504', '210.72.232.228', '2014-08-12 12:06:33.513', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (128, 1594, '地下城入场券(7天)', '', '双击左键后使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期7天.
使用后可按*1T键*9查询剩余时间', 270, 270, '1', '0', '0', 0, 1, 168, '2008-04-02 09:34:48.800', 'CN12504', '210.72.232.228', '2014-08-12 12:06:36.077', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (129, 1482, '神之假面', '', '可以重新设定角色外貌及性别的神奇石像.

*1必须在角色登陆画面时使用,需在角色选择画面点击右下方的"变更角色信息"按钮后调整人物性别、外貌及发型*9.

该道具为*1绑定道具.不可进行出售、丢弃及存入仓库等操作*9.', 300, 300, '1', '0', '1', 0, 1, 0, '2008-05-01 09:06:35.830', 'CN12504', '210.72.232.228', '2014-08-12 12:06:25.013', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (130, 1483, '角色更名许可证', '', '可以重新修改角色名称的道具.

*1必须在角色登陆画面时使用，需在角色选择画面点击右下方的"变更角色信息"按钮后修改角色名称*9.

该道具为*1绑定道具,不可进行出售、丢弃及存入仓库等操作*9.', 800, 400, '1', '0', '1', 0, 1, 0, '2008-05-01 09:07:09.173', 'CN12504', '210.72.232.228', '2014-08-12 12:06:24.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (131, 1596, '哈德斯守护', '', '当角色死亡后,在村镇中的*1<哈德斯的传教士> 阿尔特莫*9附近复活,点击该NPC,他会帮助您使用哈德斯的守护来恢复您损失的经验值.

不同级别恢复经验值所需的哈德斯守护的*1数量不一,14级以下无法使用该物品*9.', 50, 30, '1', '0', '1', 0, 1, 0, '2008-05-01 09:09:40.647', 'CN12504', '210.72.232.228', '2014-08-12 12:06:23.670', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (138, 1594, '地下城入场券(6小时)', '', '双击左键使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期6小时.
使用后可按*1T键*9查询剩余时间.', 25, 25, '1', '0', '0', 0, 1, 6, '2008-05-07 07:33:56.330', 'KR10643', '210.72.232.136', '2014-08-12 12:06:33.373', 'CN12654', '10.34.118.153');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (140, 707, '冰糖', '', 'test', 0, 0, '1', '0', '0', 0, 1, 0, '2008-05-20 16:48:04.180', 'CN12504', '210.72.232.131', '2014-08-12 12:06:32.513', 'CN12504', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (141, 500, '牛奶', '', 'test', 0, 0, '1', '0', '0', 0, 1, 0, '2008-05-20 16:48:22.180', 'CN12504', '210.72.232.131', '2014-08-12 12:06:38.467', 'CN12504', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (142, 1485, '角色移动许可证', '', '该物品需要到各村镇的*1NPC <活动>向导*9来帮您完成该道具的使用.

金币物品栏内拥有角色移动许可证的用户,需要根据<活动>向导的提示输入要将该角色移动至的游戏帐号.

要移动至的帐号内需要有空余的角色位置.

请谨慎输入要移动至的游戏帐号.

该道具为绑定道具,不可进行出售、丢弃及存入仓库等操作.

*4温馨提示:
温馨提示
角色移动到新帐号内，被封印的物品将被重新封印;
角色移动只能在同一服务器内进行.
仓库内物品无法转移(如帐号下只有1个角色,转移后原帐号内角色的仓库物品会被保留).
公会会长无法移动角色.*9', 1000, 1000, '4', '0', '1', 0, 1, 0, '2008-05-28 07:09:47.340', 'CN12504', '210.72.232.228', '2014-08-12 12:44:06.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (143, 1484, '公会信息变更许可证', '', '公会长需要到各村镇的*1NPC <活动>向导*9来帮您完成该道具的使用。

该道具可委任其他公会成员(骑士)担任公会长，*1按C键直接右键点击要委任的公会成员，但该公会成员只有在线的情况下可以委任成功*9。

公会长变更完成公会名称后，包括*1公会长在内所有公会成员会与服务器断开连接*9。

攻城战期间或公会处于战争、联盟状态均无法进行变更。

该道具为绑定道具，不可进行出售、丢弃及存入仓库等操作。', 1500, 600, '1', '0', '1', 0, 1, 0, '2008-05-28 07:09:47.040', 'CN12504', '210.72.232.228', '2014-08-12 12:06:25.107', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (144, 1825, '增幅之咒', '', '双击左键使用可获得*1 1.3倍经验状态。持续时间3小时。*9

双击后开始计时，离线计时不停止。可按T键盘查询剩余时间。', 28, 14, '1', '0', '0', 0, 1, 3, '2008-05-28 07:09:46.840', 'CN12504', '210.72.232.228', '2014-08-12 12:06:35.967', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (145, 2041, '北京奥运祝福礼花', '', '祝福北京成功举办2008奥运会游戏烟花.

双击该道具会释放礼花,呈现出*4祝福 beijing 2008*9的图案.

同时,会*4100%恢复饱腹度.', 1, 1, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:16.060', 'CN12504', '210.72.232.228', '2014-08-12 12:06:24.793', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (146, 2042, '北京奥运助威礼花', '', '北京奥运助威礼花', 1, 1, '1', '0', '0', 0, 1, 0, '2008-06-04 07:03:15.873', 'CN12504', '210.72.232.228', '2014-08-12 12:06:38.560', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (147, 2043, '胜利礼花', '', '代表胜利的游戏烟花.

双击该道具会释放礼花,呈现出*4V*9图案.

同时,会*4100%恢复饱腹度。', 2, 2, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:15.967', 'CN12504', '210.72.232.228', '2014-08-12 12:06:23.110', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (148, 2044, 'ABC', '', 'I Love China游戏烟花.

双击该道具会释放礼花,呈现出*4I Love China*9图案.
同时,会*4100%恢复饱腹度.', 1, 1, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:16.157', 'CN12504', '210.72.232.228', '2014-08-12 12:06:24.857', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (149, 2045, '爱之礼花', '', '表达爱意的游戏烟花.

双击该道具会释放礼花,呈现出*4两个心型*9图案.

同时,会*4100%恢复饱腹度.', 2, 2, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:16.250', 'CN12504', '210.72.232.228', '2014-08-12 12:06:24.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (150, 2046, 'World礼花', '', '奥运口号的游戏烟花.

双击该道具会释放礼花,呈现出*4One world One dream*9图案.

同时,会*4100%恢复饱腹度.', 1, 1, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:16.343', 'CN12504', '210.72.232.228', '2014-08-12 12:06:23.763', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (151, 2050, 'I Love China标志', '', 'I Love China特殊标志.

双击该道具会在角色头顶呈现*4I Love China*9的标志.

持续时间24小时.', 10, 10, '1', '0', '0', 0, 1, 0, '2008-06-04 07:03:16.437', 'CN12504', '210.72.232.228', '2009-12-28 18:35:20.537', 'CN13117', '10.34.118.176');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (152, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4毁灭之戒、必杀之戒、吸收戒指、灿烂的力量、敏捷、智慧戒指等稀有戒指物品，还有武器强化卷轴及防具强化卷轴(普通、祝福)、芒果、魔法增加剂等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 80, 80, '4', '0', '1', 0, 1, 0, '2008-06-11 06:57:02.697', 'CN12504', '210.72.232.228', '2014-08-12 12:44:06.060', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (153, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 175, 140, '1', '0', '0', 0, 5, 0, '2008-06-18 07:02:20.180', 'CN12504', '210.72.232.228', '2014-08-12 12:06:37.763', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (157, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 300, 225, '1', '0', '0', 0, 10, 0, '2008-07-02 07:04:58.050', 'CN12504', '210.72.232.228', '2014-08-12 12:06:37.717', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (158, 40609, '基础精华', '', '该道具是属性强化的基础精华,购买后可与NPC<属性强化>助手进行交换,获得不同属性的精华物品.

基础精华是强化属性最基本的物品.

成功购买后,基础精华会出现在您的*1属性强化栏内.', 16, 16, '1', '0', '0', 0, 1, 0, '2008-09-02 07:26:12.827', 'CN12654', '210.72.232.131', '2014-08-12 12:06:33.123', 'CN13117', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (159, 40610, '强化精华', '', '强化精华可对除活动精华外所有精华物品进行强化,拥有一定成功或失败的几率.

强化成功后精华物品属性将大幅度提高.

成功购买后,强化精华会出现在您的*1属性强化栏内.', 35, 35, '1', '0', '0', 0, 1, 0, '2008-09-02 07:26:12.920', 'CN12654', '210.72.232.131', '2014-08-12 12:06:36.670', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (160, 2416, '疾速药水', '', '可提升移动速度的神奇药剂,*1效果强于加速药水*9.

移动速度提升效果持续时间30分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 4, 4, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.483', 'CN12654', '210.72.232.131', '2014-08-12 12:06:22.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (161, 2417, '突击药水', '', '双击使用可提升攻击速度,*1效果强于勇气药剂*9.

*1仅限骑士*9使用,攻击速度提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.
', 8, 5, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.390', 'CN12654', '210.72.232.131', '2014-08-12 12:06:25.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (162, 2418, '超级敏捷药水', '', '双击使用可将敏捷提升3.

敏捷提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 4, 4, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.297', 'CN12654', '210.72.232.131', '2014-08-12 12:06:24.200', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (163, 2419, '超级命中药水', '', '双击使用可将命中提升3.

命中提升效果持续时间5分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 4, 4, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.220', 'CN12654', '210.72.232.131', '2014-08-12 12:06:27.060', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (164, 2420, '超级重击药水', '', '双击使用可将暴击率提升.

暴击率提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 5, 5, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.123', 'CN12654', '210.72.232.131', '2014-08-12 12:06:24.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (165, 2421, '防御药水', '', '双击使用可将防御力提升3.

防御提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 5, 5, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.030', 'CN12654', '210.72.232.131', '2014-08-12 12:06:27.013', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (166, 2422, '超级智慧药水', '', '双击使用可将智力提升3.

智力提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 2, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:10.953', 'CN12654', '210.72.232.131', '2014-08-12 12:06:23.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (167, 2423, '超级加速药水', '', '双击使用可提升移动速度.

*1移动速度效果强于超级加速秘药*9,效果持续时间为40秒.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 2, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:10.860', 'CN12654', '210.72.232.131', '2014-08-12 12:06:25.997', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (168, 2424, '超级力量药水', '', '双击使用可将力量提升3.

力量提升效果持续时间10分钟

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 2, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:10.763', 'CN12654', '210.72.232.131', '2014-08-12 12:06:26.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (169, 2425, '负重之药水 Lv1', '', '双击使用可将负重提升500.

负重提升效果持续时间20分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 3, 3, '1', '0', '0', 0, 1, 0, '2008-09-02 07:26:10.670', 'CN12654', '210.72.232.131', '2014-08-12 12:06:29.497', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (170, 2426, '负重之药水 Lv2', '', '双击使用可将负重提升1000.

负重提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 5, 5, '1', '0', '0', 0, 1, 0, '2008-09-02 07:26:10.593', 'CN12654', '210.72.232.131', '2014-08-12 12:06:31.513', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (171, 2427, '净化药水', '', '使用后可解除角色身上一切附加魔法效果及特殊药水效果.

爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 5, 5, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:10.500', 'CN12654', '210.72.232.131', '2014-08-12 12:06:22.110', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (172, 2428, '死亡弓箭', '', 'test', 1, 1, '1', '0', '0', 0, 1000, 0, '2008-09-02 07:26:10.407', 'CN12654', '210.72.232.131', '2014-08-12 12:06:38.263', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (173, 2429, '生命之戒', '', 'test', 1, 1, '1', '0', '0', 7, 1, 0, '2008-09-02 07:26:10.327', 'CN12654', '210.72.232.131', '2014-08-12 12:06:38.357', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (174, 2434, '恢复戒指', '', '佩戴后HP恢复+1,MP恢复+1,需要解封戒指栏.购买后开始计时.', 80, 80, '1', '0', '0', 7, 1, 0, '2008-09-02 07:26:10.233', 'CN12654', '210.72.232.131', '2014-08-12 12:06:19.657', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (175, 2430, '神力之戒', '', '佩戴后负重增加1500,需要解封戒指栏.购买后开始计时.', 60, 60, '1', '0', '1', 7, 1, 0, '2008-09-02 07:26:10.140', 'CN12654', '210.72.232.131', '2014-08-12 12:06:25.763', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (176, 2431, '辩识之书', '', 'test', 20, 20, '1', '0', '0', 30, 1, 0, '2008-09-02 07:26:10.047', 'CN12654', '210.72.232.131', '2014-08-12 12:06:38.077', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (177, 2432, '旅行者之书', '', '双击旅行者之书,可将您瞬间传送到当前位置最近的城镇.购买后开始计时,有效期30天.', 60, 60, '4', '0', '1', 30, 1, 0, '2008-09-02 07:26:09.903', 'CN12654', '210.72.232.131', '2014-08-12 13:17:53.420', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (179, 2445, '芒果糖', '', 'test', 1, 1, '1', '0', '0', 0, 1, 0, '2008-09-23 07:03:54.940', 'CN12504', '210.72.232.225', '2014-08-12 12:06:38.187', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (180, 2446, '芒果汁', '', 'test', 1, 1, '1', '0', '0', 0, 1, 0, '2008-09-23 07:03:55.080', 'CN12504', '210.72.232.225', '2014-08-12 12:06:38.123', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (181, 2440, '雷克斯魔女项链(30天)', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9', 800, 800, '1', '0', '1', 30, 1, 0, '2008-10-14 07:02:45.977', 'CN12504', '210.72.232.225', '2014-08-12 12:06:25.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (182, 1825, '增幅之咒（2倍）（3小时）', '', '加打怪时获得的经验.该效果可以被更高倍数"经验增幅之咒"的效果替换.经验增加效果与时间不累积. ', 35, 35, '1', '0', '0', 0, 1, 3, '2008-10-21 07:00:58.553', 'CN12504', '210.72.232.131', '2014-08-12 12:20:22.903', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (183, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 175, 175, '1', '0', '0', 0, 5, 0, '2008-10-21 07:00:57.790', 'CN12504', '210.72.232.131', '2014-08-12 12:06:37.670', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (184, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 300, 300, '1', '0', '0', 0, 10, 0, '2008-10-21 07:00:57.897', 'CN12504', '210.72.232.131', '2014-08-12 12:06:37.623', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (185, 40609, '基础精华', '', '该道具是属性强化的基础精华,购买后可与NPC<属性强化>助手进行交换,获得不同属性的精华物品.

基础精华是强化属性最基本的物品.

成功购买后,基础精华会出现在您的*1属性强化栏内.', 20, 20, '1', '0', '1', 0, 1, 0, '2008-10-21 07:00:57.990', 'CN12504', '210.72.232.131', '2014-08-12 12:06:24.170', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (186, 40610, '强化精华', '', '强化精华可对除活动精华外所有精华物品进行强化,拥有一定成功或失败的几率.

强化成功后精华物品属性将大幅度提高.

成功购买后,强化精华会出现在您的*1属性强化栏内.', 50, 50, '1', '0', '1', 0, 1, 0, '2008-10-21 07:00:58.460', 'CN12504', '210.72.232.131', '2014-08-12 12:06:26.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (187, 1599, '项链解封之咒(3天)', '', '使用后开启装备栏中的项链栏,从而可佩戴项链.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 5, 5, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:58.777', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.733', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (188, 1597, '戒指解封之咒(A-3天)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 5, 5, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:58.980', 'CN12504', '210.72.232.225', '2014-08-12 12:06:38.030', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (189, 1598, '戒指解封之咒(B-3天)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.
', 5, 5, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:59.073', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.983', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (190, 1600, '腰带解封之咒(3天)', '', '使用后开启装备栏中的腰带栏,从而可佩戴腰带.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 5, 5, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:59.170', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.920', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (191, 1601, '披风解封之咒(3天)', '', '使用后开启装备栏中的披风栏,从而可佩戴披风.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.
', 5, 5, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:59.263', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.873', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (192, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4毁灭之戒、必杀之戒、吸收戒指、灿烂的力量、敏捷、智慧戒指等稀有戒指物品，还有武器强化卷轴及防具强化卷轴(普通、祝福)、芒果、魔法增加剂等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 360, 360, '4', '0', '1', 0, 5, 0, '2008-12-02 08:22:42.523', 'CN12654', '210.72.232.131', '2014-08-12 12:44:06.450', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (193, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 30, 30, '1', '0', '0', 0, 1, 0, '2008-12-22 16:02:23.253', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.780', 'KR13076', '10.34.114.101');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (194, 1623, '米泰欧斯的守护', '', '强化装备失败时,行囊内如有米泰欧斯的守护,*1可一定几率的自动保护强化失败的装备*9.

无论保护成功或失败,*1米泰欧斯的守护都会消失*9.', 64, 64, '1', '0', '0', 0, 1, 0, '2008-12-22 16:02:25.100', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.827', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (195, 2440, '雷克斯魔女项链', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9', 9, 9, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:34.597', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.890', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (200, 2434, '恢复戒指', '', '佩戴后HP恢复+1,MP恢复+1,需要解封戒指栏.购买后开始计时.', 1, 1, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:38.567', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.090', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (201, 2430, '神力之戒', '', '佩戴后负重增加1500,需要解封戒指栏.购买后开始计时.', 1, 1, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:38.880', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.153', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (202, 2432, '旅行者之书', '', '双击旅行者之书,可将您瞬间传送到当前位置最近的城镇.购买后开始计时,有效期30天.', 1, 1, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:39.487', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.200', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (203, 2033, '月光复仇者项链(14天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+75 MP+45 负重+500*9.', 0, 0, '1', '0', '0', 14, 1, 0, '2008-12-22 16:02:40.690', 'CN12504', '210.72.232.225', '2014-08-12 12:06:35.827', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (204, 2039, '锥锋复仇者项链(14天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+75 MP+45 负重+500*9.', 0, 0, '1', '0', '0', 14, 1, 0, '2008-12-22 16:02:41.050', 'CN12504', '210.72.232.225', '2014-08-12 12:06:35.873', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (205, 1594, '地下城入场券(3小时)', '', '双击左键使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期3小时.
使用后可按*1T键*9查询剩余时间.', 12, 12, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:41.380', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.623', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (206, 1597, '戒指解封之咒(A)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 1, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:41.723', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.263', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (207, 1598, '戒指解封之咒(B)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 1, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:42.347', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.310', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (208, 1599, '项链解封之咒', '', '使用后开启装备栏中的项链栏,从而可佩戴项链.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 1, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:42.707', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.357', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (209, 1600, '腰带解封之咒', '', '使用后开启装备栏中的腰带栏,从而可佩戴腰带.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 1, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:43.037', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.420', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (210, 1601, '披风解封之咒', '', '使用后开启装备栏中的披风栏,从而可佩戴披风.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 1, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:43.707', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.467', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (211, 1590, '传送记忆列表(A)', '', '在20天内,*1可额外增加传送之咒中10个记录点*9.
双击鼠标左键使用.使用后可按*1T键*9查询剩余时间.', 1, 1, '1', '0', '0', 0, 1, 2, '2008-12-22 16:02:44.113', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.513', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (212, 1618, '德拉克召唤法书', '', '使用后可召唤超级德拉克,同时可随时收回.

召唤出的德拉克可在地下城内骑.

双击鼠标左键使用,*1购买后开始计时间*9,有效时间7天. 

德拉克召唤书购买后不可进行出售，丢弃和存入仓库等操作.', 3, 3, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:44.533', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.560', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (213, 2817, '温馨宝箱钥匙', '', '开启温馨宝箱的神秘钥匙钥匙', 300, 300, '1', '0', '0', 0, 1, 0, '2009-01-13 08:08:40.577', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.327', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (214, 1952, '[活动] +4 双手剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:12.623', 'CN12654', '210.72.232.225', '2014-08-12 12:06:36.123', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (215, 1956, '[活动] +4 猎枪', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-13 11:15:50.920', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.217', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (216, 1960, '[活动]+4 十字剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-13 11:15:50.437', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.280', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (217, 1964, '[活动]+4 拳剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:14.153', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.373', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (218, 1953, '[活动] +5 双手剑', '', '', 0, 0, '4', '0', '0', 14, 1, 0, '2009-03-17 08:06:14.437', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.030', 'KR13076', '10.34.65.17');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (219, 1957, '[活动] +5 猎枪', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:14.687', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.420', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (220, 1961, '[活动]+5 十字剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:14.967', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.483', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (221, 1965, '[活动]+5 拳剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:15.233', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.530', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (222, 58, '黑皮甲', '', 'test ', 0, 0, '1', '0', '0', 30, 1, 30, '2009-03-13 13:41:27.217', 'CN11582', '59.108.20.1', '2014-08-12 12:06:36.170', 'CN11582', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (223, 2074, '增幅之咒(2倍)(3小时)', '', '打怪时获得的经验提高2倍.该效果可以替换掉低倍数"经验增幅之咒"的效果.经验增加效果与时间不累积.', 35, 35, '4', '0', '1', 0, 1, 3, '2009-04-16 07:11:18.513', 'CN12654', '10.34.115.185', '2014-08-12 13:01:59.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (224, 40600, '幸运精华 Lv1[7天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:17.983', 'CN12654', '10.34.115.185', '2014-08-12 12:06:27.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (225, 40603, '变身强化精华 Lv1[7天]', '', '变身强化精华Lv1,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+30，MP值+30', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:16.293', 'CN12654', '10.34.115.185', '2014-08-12 12:06:25.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (226, 40606, '英雄力量精华[7天]', '', '英雄力量精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后力量+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:16.840', 'CN12654', '10.34.115.185', '2014-08-12 12:06:25.857', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (227, 40607, '猎人敏捷精华[7天]', '', '猎人敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后敏捷+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:17.107', 'CN12654', '10.34.115.185', '2014-08-12 12:06:25.670', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (228, 40608, '贤者智慧精华[7天]', '', '贤者敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后智慧+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:17.387', 'CN12654', '10.34.115.185', '2014-08-12 12:06:27.107', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (229, 2050, 'I Love China标志', '', 'I Love China特殊标志.

双击该道具会在角色头顶呈现*4I Love China*9的标志.

持续时间24小时.', 10, 10, '1', '0', '1', 0, 1, 0, '2009-04-27 12:58:53.303', 'CN12654', '10.34.115.182', '2014-08-12 12:06:23.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (230, 40601, '幸运精华 Lv2 [7天]', '', '购买后出现在属性强化栏内，仅能佩戴于特殊精华第一栏，携带后物品掉落几率提升', 0, 0, '1', '0', '2', 7, 1, 0, '2009-05-05 09:19:49.960', 'CN12654', '10.34.118.195', '2014-08-12 12:06:23.907', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (231, 40602, '幸运精华 Lv3[7天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 7, 1, 0, '2009-05-05 09:19:49.680', 'CN12654', '10.34.118.195', '2014-08-12 12:06:27.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (232, 40604, '变身强化精华 Lv2[7天]', '', '变身强化精华Lv2,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+60，MP值+60', 0, 0, '1', '0', '2', 7, 1, 0, '2009-05-05 09:19:49.413', 'CN12654', '10.34.118.195', '2014-08-12 12:06:24.107', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (233, 40605, '变身强化精华 Lv3[7天]', '', '变身强化精华Lv3,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+90，MP值+90', 0, 0, '1', '0', '2', 7, 1, 0, '2009-05-05 09:19:49.147', 'CN12654', '10.34.118.195', '2014-08-12 12:06:25.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (234, 40600, '幸运精华 Lv1[30天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:45.117', 'CN12654', '10.34.118.195', '2014-08-12 12:06:21.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (235, 40601, '幸运精华 Lv2[30天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:48.883', 'CN12654', '10.34.118.195', '2014-08-12 12:06:27.200', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (236, 40602, '幸运精华 Lv3[30天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:48.617', 'CN12654', '10.34.118.195', '2014-08-12 12:06:22.407', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (237, 40603, '变身强化精华 Lv1[30天]', '', '变身强化精华Lv1,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+30，MP值+30', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:48.337', 'CN12654', '10.34.118.195', '2014-08-12 12:06:23.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (238, 40604, '变身强化精华 Lv2[30天]', '', '变身强化精华Lv2,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+60，MP值+60', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:48.070', 'CN12654', '10.34.118.195', '2014-08-12 12:06:26.403', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (239, 40605, '变身强化精华 Lv3[30天]', '', '变身强化精华Lv3,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+90，MP值+90', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:47.803', 'CN12654', '10.34.118.195', '2014-08-12 12:06:23.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (240, 40606, '英雄力量精华[30天]', '', '英雄力量精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后力量+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:47.540', 'CN12654', '10.34.118.195', '2014-08-12 12:06:22.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (241, 40607, '猎人敏捷精华[30天]', '', '猎人敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后敏捷+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:47.273', 'CN12654', '10.34.118.195', '2014-08-12 12:06:21.907', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (242, 40608, '贤者智慧精华[30天]', '', '贤者敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后智慧+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:47.010', 'CN12654', '10.34.118.195', '2014-08-12 12:06:22.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (243, 2033, '月光复仇者项链', '', '月光复仇者项链(30天)', 1280, 1280, '1', '0', '0', 30, 1, 0, '2009-07-07 17:07:41.753', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.920', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (244, 40604, '变身强化精华 Lv2(14天）', '', '变身强化精华Lv2,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+60，MP值+60', 10, 10, '1', '0', '0', 14, 1, 0, '2009-06-23 12:17:09.437', 'CN12654', '10.34.115.182', '2014-08-12 12:06:31.310', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (245, 2996, '印谱诺项链(II)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+50 MP+30 负重+300', 0, 0, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:46.177', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.763', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (249, 2440, '雷克斯魔女项链(1天)', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9', 0, 0, '1', '0', '0', 1, 1, 0, '2009-09-22 08:08:44.350', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.670', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (250, 2440, '雷克斯魔女项链(1天)', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9', 0, 0, '1', '0', '0', 1, 1, 0, '2009-09-22 08:08:43.833', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.513', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (251, 3016, '公主项链(普通)', '', '封印着公主灵魂的项链(*1需要解封项链栏*9),佩带后将获得公主般华丽的样貌，且拥有超快的攻击速度.

购买后开始计时间.

变身为公主,*1HP+50 MP+30 负重+300*9', 45, 45, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:39.723', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.420', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (252, 2788, '威尔斯沃琪项链', '', '封印着威尔斯沃琪灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为威尔斯沃琪,*1HP+100 MP+60 负重+750*9', 45, 45, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:39.567', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.373', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (253, 2794, '科普提恩雷肯项链', '', '封印着科普提恩雷肯灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+100 MP+60 负重+750*9', 45, 45, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:39.880', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.467', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (254, 2996, '印谱诺项链(II)(1天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+75 MP+45 负重+500*9', 140, 140, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:56.343', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.827', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (255, 2996, '印谱诺项链(II)(7天)', '', '"封印着印谱诺灵魂的项链(*1需要解封项链栏*9)强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+75 MP+45 负重+500*9', 450, 450, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:55.110', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.497', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (256, 2996, '印谱诺项链(II)(30天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+75 MP+45 负重+500*9
', 1260, 1260, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:53.893', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.607', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (257, 2997, '印谱诺项链(III)(1天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+100 MP+60 负重+750*9
', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:53.097', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.873', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (258, 2997, '印谱诺项链(III)(7天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+100 MP+60 负重+750*9
', 540, 540, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:52.300', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.157', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (259, 2997, '印谱诺项链(III)(30天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+100 MP+60 负重+750*9
', 1510, 1510, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:51.503', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.110', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (260, 2998, '印谱诺项链(IV)(1天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+150 MP+90 负重+1250*9
', 200, 200, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:50.267', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.920', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (263, 2440, '雷克斯魔女项链(1天)', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9"
', 80, 80, '4', '0', '0', 1, 1, 0, '2009-07-16 19:26:47.470', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.670', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (264, 2999, '雷克斯魔女项链(II)(1天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9)强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+75 MP+45 负重+500*9"
', 140, 140, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:46.220', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.967', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (265, 2999, '雷克斯魔女项链(II)(7天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9)强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+75 MP+45 负重+500*9"', 450, 450, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:45.003', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.903', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (266, 2999, '雷克斯魔女项链(II)(30天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+75 MP+45 负重+500*9"', 1260, 1260, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:43.767', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.670', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (267, 3000, '雷克斯魔女项链(III)(1天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+100 MP+60 负重+750*9"', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:42.987', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.030', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (268, 3000, '雷克斯魔女项链(III)(7天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+100 MP+60 负重+750*9"', 540, 540, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:42.190', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.360', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (269, 3000, '雷克斯魔女项链(III)(30天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+100 MP+60 负重+750*9"', 1510, 1510, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:41.410', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (270, 3001, '雷克斯魔女项链(IV)(1天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+150 MP+90 负重+1250*9"', 200, 200, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:39.580', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.077', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (271, 3001, '雷克斯魔女项链(IV)(7天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+150 MP+90 负重+1250*9"', 650, 650, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:37.767', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.203', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (272, 3001, '雷克斯魔女项链(IV)(30天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+150 MP+90 负重+1250*9"', 1810, 1810, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:36.970', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (274, 3002, '嗜血的吸血鬼项链(II)(1天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9)强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+75MP+45 负重+500*9"', 140, 140, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:34.097', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.123', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (275, 3002, '嗜血的吸血鬼项链(II)(7天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+75MP+45 负重+500*9"', 450, 450, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:33.313', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.060', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (276, 3002, '嗜血的吸血鬼项链(II)(30天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+75MP+45 负重+500*9"', 1260, 1260, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:32.533', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.997', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (277, 3003, '嗜血的吸血鬼项链(III)(1天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+100MP+60 负重+750*9"', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:31.737', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.170', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (278, 3003, '嗜血的吸血鬼项链(III)(7天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+100MP+60 负重+750*9"', 540, 540, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:30.940', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (279, 3003, '嗜血的吸血鬼项链(III)(30天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+100MP+60 负重+750*9"', 1510, 1510, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:29.707', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.547', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (280, 3004, '嗜血的吸血鬼项链(IV)(1天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+150MP+90 负重+1250*9"', 200, 200, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:28.923', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.217', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (281, 3004, '嗜血的吸血鬼项链(IV)(7天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+150MP+90 负重+1250*9"', 650, 650, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:28.143', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.407', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (282, 3004, '嗜血的吸血鬼项链(IV)(30天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+150MP+90 负重+1250*9"', 1810, 1810, '1', '0', '0', 30, 1, 0, '2009-07-16 19:26:27.360', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.453', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (284, 3005, '圣甲战魔项链(II)(1天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+75 MP+45 负重+500', 140, 140, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:24.567', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.280', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (285, 3005, '圣甲战魔项链(II)(7天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+75 MP+45 负重+500', 450, 450, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:23.330', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.857', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (286, 3005, '圣甲战魔项链(II)(30天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+75 MP+45 负重+500', 1260, 1260, '1', '0', '0', 30, 1, 0, '2009-07-16 19:26:22.550', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.763', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (287, 3006, '圣甲战魔项链(III)(1天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+100 MP+60 负重+750', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:21.770', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.357', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (288, 3006, '圣甲战魔项链(III)(7天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+100 MP+60 负重+750', 540, 540, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:20.550', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.733', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (289, 3006, '圣甲战魔项链(III)(30天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+100 MP+60 负重+750', 1510, 1510, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:18.847', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (290, 3007, '圣甲战魔项链(IV)(1天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+150 MP+90 负重+1250', 200, 200, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:18.080', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.403', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (291, 3007, '圣甲战魔项链(IV)(7天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+150 MP+90 负重+1250', 650, 650, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:17.300', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.640', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (293, 2033, '月光复仇者项链(1天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+75 MP+45 负重+500*9.', 140, 140, '4', '0', '0', 1, 1, 0, '2009-07-16 19:26:15.300', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.467', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (295, 3008, '月光复仇者项链(改)(1天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+100 MP+60 负重+750*9.', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:13.317', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.513', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (296, 3008, '月光复仇者项链(改)(7天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+100 MP+60 负重+750*9.', 540, 540, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:10.457', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.107', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (297, 3008, '月光复仇者项链(改)(30天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+100 MP+60 负重+750*9.', 1510, 1510, '1', '0', '0', 30, 1, 0, '2009-07-16 19:26:09.237', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.310', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (298, 2039, '锥锋复仇者项链(1天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+75 MP+45 负重+500*9.', 140, 140, '4', '0', '0', 1, 1, 0, '2009-07-16 19:26:08.033', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.560', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (300, 3010, '锥锋复仇者项链(改)(1天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+100 MP+60 负重+750*9.', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:06.503', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.607', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (301, 3010, '锥锋复仇者项链(改)(7天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+100 MP+60 负重+750*9.', 540, 540, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:05.720', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.217', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (302, 3010, '锥锋复仇者项链(改)(30天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+100 MP+60 负重+750*9.', 1510, 1510, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:04.533', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.170', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (303, 3016, '公主项链(普通)(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+50 MP+30 负重+300*9"
', 80, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:03.753', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.560', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (306, 2782, '公主项链(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+75 MP+45 负重+500*9"
', 140, 140, '1', '0', '0', 1, 1, 0, '2009-07-16 19:26:00.597', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.670', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (307, 2782, '公主项链(7天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+75 MP+45 负重+500*9"', 450, 450, '1', '0', '1', 7, 1, 0, '2009-07-16 19:25:59.830', 'CN12654', '10.34.223.29', '2014-08-12 12:06:25.903', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (308, 2782, '公主项链(30天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+75 MP+45 负重+500*9"', 1260, 1260, '1', '0', '1', 30, 1, 0, '2009-07-16 19:25:58.677', 'CN12654', '10.34.223.29', '2014-08-12 12:06:25.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (309, 3014, '公主项链(改)(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+100 MP+60 负重+750*9"
', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:57.630', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.717', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (310, 3014, '公主项链(改)(7天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+100 MP+60 负重+750*9"
', 540, 540, '1', '0', '0', 7, 1, 0, '2009-07-16 19:25:55.113', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.403', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (311, 3014, '公主项链(改)(30天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+100 MP+60 负重+750*9"', 1510, 1510, '2', '0', '0', 30, 1, 0, '2009-07-16 19:25:54.660', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.357', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (312, 2788, '威尔斯沃琪项链(1天)', '', '"封印着威尔斯沃琪的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴, 佩带后将获得恐怖的远程攻击能力.
购买后开始计时间.

变身为威尔斯沃琪,*1HP+100 MP+60 负重+750*9"
', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:53.207', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.763', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (315, 3012, '威尔斯沃琪项链(改)(1天)', '', '"封印着威尔斯沃琪的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得恐怖的远程攻击能力.

购买后开始计时间.

变身为威尔斯沃琪,*1HP+150 MP+90 负重+1250*9"
', 200, 200, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:51.160', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.810', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (318, 2794, '科普提恩雷肯项链(1天)', '', '"封印着科普提恩雷肯项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+100 MP+60 负重+750*9"', 160, 160, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:49.440', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.873', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (321, 3013, '科普提恩雷肯项链(改)(1天)', '', '"封印着科普提恩雷肯项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+150 MP+90 负重+1250*9"', 200, 200, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:47.677', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.920', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (322, 3013, '科普提恩雷肯项链(改)(7天)', '', '"封印着科普提恩雷肯项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+150 MP+90 负重+1250*9"', 650, 650, '1', '0', '0', 7, 1, 0, '2009-07-16 19:25:47.220', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.450', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (323, 3013, '科普提恩雷肯项链(改)(30天)', '', '"封印着科普提恩雷肯项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+150 MP+90 负重+1250*9"', 1810, 1810, '3', '0', '0', 30, 1, 0, '2009-07-16 19:25:46.427', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.263', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (327, 2800, '英雄箱子', '', '英雄遗落的箱子*9.

宝箱内含*4 +2Lv3生命精华,++2Lv3灵魂精华,++2Lv3破坏精华,++2Lv3守护精华,++2Lv3熟练精华等物品,包括从普通,稀有,史诗到传说级别的极品精华,及10个基础精华.

打开宝箱,可随机获得其中一件道具
*9.
', 0, 0, '1', '0', '0', 0, 1, 0, '2009-08-04 13:41:22.640', 'CN12654', '10.34.223.138', '2014-08-12 12:06:21.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (328, 2072, 'NG_test1', '', '双击左键使用可获得*1 2倍掉率状态。持续时间3天。*9 

双击后开始计时，离线计时不停止。可按T键盘查询剩余时间。', 200000, 200000, '1', '0', '0', 0, 1, 3, '2009-08-04 13:41:23.220', 'CN12654', '10.34.223.138', '2014-08-12 12:06:30.107', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (329, 3372, '高级副本入场券(一层)', '', '双击左键使用后,可进入高级副本1层.
*只有1～40级之间的角色才可进入

道具使用时间为6小时.
*该道具有效期为7天,请在购买后7天内进行使用

使用后可按*1T键*9查询剩余时间.', 140, 140, '4', '0', '1', 30, 1, 6, '2009-08-10 17:42:37.710', 'CN12654', '10.34.223.138', '2014-08-12 13:01:59.060', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (330, 3373, '高级副本入场券(二层)(6小时)', '', '双击左键使用后,可进入高级副本2层.
*只有41级以上的角色才能进入

道具使用时间为6小时.
*该道具有效期为7天,请在购买后7天内进行使用

使用后可按*1T键*9查询剩余时间.', 200, 200, '4', '0', '1', 30, 1, 6, '2009-08-10 17:42:37.897', 'CN12654', '10.34.223.138', '2014-08-12 13:17:53.123', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (331, 3420, '可爱公主项链(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱公主,*1HP+50 MP+30 负重+300*9"
', 80, 80, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:41.163', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.123', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (334, 3426, '可爱公主(II)项链(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱公主,*1HP+75 MP+45 负重+500*9"', 140, 140, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:59.587', 'CN12654', '10.34.223.138', '2014-08-12 12:06:33.013', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (335, 3426, '修炼少女(II)项链(7天)', '', '"封印着修炼少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为修炼少女,*1HP+75 MP+45 负重+500*9"', 450, 450, '1', '0', '0', 7, 1, 0, '2009-08-10 17:42:59.757', 'CN12654', '10.34.223.138', '2014-08-12 12:06:28.700', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (336, 3426, '修炼少女(II)项链(30天)', '', '"封印着修炼少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为修炼少女,*1HP+75 MP+45 负重+500*9"', 1260, 1260, '1', '0', '0', 30, 1, 0, '2009-08-10 17:42:40.007', 'CN12654', '10.34.223.138', '2014-08-12 12:06:28.950', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (337, 3427, '可爱公主(III)项链(1天)', '', '封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱公主,*1HP+100 MP+60 负重+750*9"', 160, 160, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:42.413', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.013', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (338, 3427, '修炼少女(III)项链(7天)', '', '封印着修炼少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为修炼少女,*1HP+100 MP+60 负重+750*9"', 540, 540, '1', '0', '0', 7, 1, 0, '2009-08-10 17:42:42.240', 'CN12654', '10.34.223.138', '2014-08-12 12:06:20.000', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (346, 3423, '美少女(II)项链(1天)', '', '"封印着美少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为美少女,*1HP+75 MP+45 负重+500*9"', 140, 140, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:39.837', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.217', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (347, 3423, '东瀛少女(II)项链(7天)', '', '"封印着东瀛少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为东瀛少女,*1HP+75 MP+45 负重+500*9"', 450, 450, '1', '0', '0', 7, 1, 0, '2009-08-10 17:42:39.680', 'CN12654', '10.34.223.138', '2014-08-12 12:06:28.810', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (348, 3423, '东瀛少女(II)项链(30天)', '', '"封印着东瀛少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为东瀛少女,*1HP+75 MP+45 负重+500*9"', 1260, 1260, '2', '0', '0', 30, 1, 0, '2009-08-10 17:42:39.507', 'CN12654', '10.34.223.138', '2014-08-12 12:06:28.560', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (349, 3424, '美少女(III)项链(1天)', '', '"封印着美少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为美少女,*1HP+100 MP+60 负重+750*9"', 160, 160, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:39.350', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.263', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (350, 3424, '东瀛少女(III)项链(7天)', '', '"封印着东瀛少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为东瀛少女,*1HP+100 MP+60 负重+750*9"', 540, 540, '1', '0', '0', 7, 1, 0, '2009-08-10 17:42:38.943', 'CN12654', '10.34.223.138', '2014-08-12 12:06:19.953', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (351, 3424, '东瀛少女(III)项链(30天)', '', '"封印着东瀛少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为东瀛少女,*1HP+100 MP+60 负重+750*9"', 1510, 1510, '2', '0', '0', 30, 1, 0, '2009-08-10 17:42:38.790', 'CN12654', '10.34.223.138', '2014-08-12 12:06:19.907', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (352, 3425, '美少女(IV)项链(1天)', '', '"封印着美少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为美少女,*1HP+150 MP+90 负重+1250*9"', 200, 200, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:38.383', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.310', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (355, 2735, '高级服务礼券', '', '使用道具，将获得高级服务NPC', 0, 0, '1', '0', '0', 0, 1, 6, '2009-09-22 08:08:22.100', 'CN12654', '10.34.223.138', '2014-08-12 12:06:32.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (356, 3372, '高级副本入场券(一层)', '', '进入高级副本的凭证', 1, 1, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:24.927', 'CN12654', '10.34.118.177', '2014-08-12 12:06:33.467', 'CN12654', '10.34.118.177');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (357, 3422, '官方入场卷', '', '练功房入场券', 1, 1, '2', '1', '0', 7, 1, 6, '2009-09-22 08:08:24.757', 'CN12654', '10.34.118.177', '2014-08-12 12:06:33.420', 'CN12654', '10.34.118.177');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (358, 3422, '练功专用券', '', '双击左键使用后,可进入练功房。

道具使用时间为6小时.
*该道具有效期为7天,请在购买后7天内进行使用

使用后可按*1T键*9查询剩余时间.', 1, 1, '2', '0', '0', 7, 1, 6, '2009-09-22 08:08:21.710', 'CN12654', '10.34.223.138', '2014-08-12 12:06:33.263', 'CN12654', '10.34.118.170');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (362, 2070, '银币掉落增幅之咒(1.5倍)', '', '银币掉落增加到 1.5倍', 0, 0, '1', '0', '2', 0, 1, 3, '2010-02-01 14:20:58.307', 'CN99019', '10.34.118.180', '2014-08-12 12:06:26.903', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (363, 2069, '掉率增幅之咒(1.5倍)', '', '掉率增幅加至1.5倍', 0, 0, '1', '0', '2', 0, 1, 3, '2010-02-01 14:20:57.870', 'CN99019', '10.34.118.180', '2014-08-12 12:06:24.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (365, 1509, '柠檬(网吧)', '', '', 10, 10, '1', '0', '0', 0, 1, 10, '2010-02-09 10:44:35.037', 'CN99019', '10.34.118.165', '2014-08-12 12:06:33.077', 'CN99019', '10.34.118.165');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (367, 2735, '高级服务礼券', '', '使用道具, 将获得使用高级服务NPC的权限', 0, 0, '2', '0', '0', 0, 1, 168, '2010-02-09 14:47:36.837', 'CN99019', '10.34.118.165', '2014-08-12 12:06:32.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (368, 3372, '高级副本入场券(一层)(3天)', '', '高级副本入场券(一层) 效果持续时间3天', 600, 600, '4', '0', '1', 30, 1, 72, '2010-02-09 16:12:33.643', 'CN99019', '10.34.118.165', '2014-08-12 13:01:57.920', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (369, 3373, '高级副本入场券(二层)(30天) ', '', '高级副本入场券(二层) 效果持续时间30天', 1470, 1470, '4', '0', '1', 30, 1, 720, '2010-02-09 16:12:33.753', 'CN99019', '10.34.118.165', '2014-08-12 13:01:58.670', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (370, 3372, '高级副本入场券(一层)(30天)', '', '高级副本入场券(一层) 效果持续时间30天', 1050, 1050, '4', '0', '1', 30, 1, 720, '2010-02-09 16:12:33.690', 'CN99019', '10.34.118.165', '2014-08-12 13:19:36.840', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (371, 3373, '高级副本入场券(二层)(3天)', '', '高级副本入场券(二层) 效果持续时间3天', 840, 840, '4', '0', '1', 30, 1, 72, '2010-02-09 16:12:33.567', 'CN99019', '10.34.118.165', '2014-08-12 13:01:59.437', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (372, 2915, '礼花A', '', '342355', 8, 7, '3', '1', '0', 3, 1, 1, '2012-03-09 14:19:29.667', 'CN12835', '202.108.36.125', '2014-08-12 12:06:32.920', 'CN12835', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (380, 4871, '1元礼包测试', '', '', 100, 10, '1', '1', '0', 0, 1, 0, '2012-07-03 14:59:22.013', '', '61.152.133.230', '2014-08-12 12:06:32.200', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (381, 4875, '祝 v(＾O＾)v祝爆竹', '', '祝 v(＾O＾)v祝爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.607', '', '61.152.133.230', '2014-08-12 12:06:32.060', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (382, 4874, '祝爆竹', '', '', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.560', '', '61.152.133.230', '2014-08-12 12:06:29.967', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (383, 4876, '祝福の槍爆竹', '', '', 10, 10, '4', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.297', '', '61.152.133.230', '2014-08-12 12:06:32.107', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (384, 4872, 'T爆竹', '', 'T爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.513', '', '61.152.133.230', '2014-08-12 12:06:29.903', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (385, 4873, 'Y爆竹', '', 'Y爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.483', '', '61.152.133.230', '2014-08-12 12:06:29.857', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (386, 4868, '笑脸爆竹', '', '笑脸爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.437', '', '61.152.133.230', '2014-08-12 12:06:31.997', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (387, 4869, '招手的爆竹', '', '招手的爆竹', 0, 0, '4', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.390', '', '61.152.133.230', '2014-08-12 12:06:31.903', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (388, 4870, '生气的爆竹', '', '生气的爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.343', '', '61.152.133.230', '2014-08-12 12:06:31.950', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (389, 4871, 'P爆竹', '', '', 100, 10, '4', '0', '0', 0, 10, 0, '2012-07-20 18:47:10.217', '', '61.152.133.230', '2014-08-12 12:06:31.857', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (390, 1616, '冠军德拉克', '', '', 0, 0, '1', '0', '0', 30, 1, 0, '2012-07-03 16:50:14.717', '', '61.152.133.230', '2014-08-12 12:06:32.153', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (391, 40616, '英雄力量精华 Lv2[30天]', '', '英雄力量精华 Lv2[30天]', 0, 0, '1', '0', '0', 30, 1, 0, '2012-07-20 18:47:10.170', '', '61.152.133.230', '2014-08-12 12:06:30.873', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (392, 4871, 'P爆竹', '', '', 10, 10, '1', '0', '0', 0, 10, 0, '2012-07-20 18:47:09.983', '', '61.152.133.230', '2014-08-12 12:06:31.717', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (393, 2915, '礼花A', '', '', 10, 10, '1', '0', '0', 0, 10, 0, '2012-07-20 18:47:09.937', '', '61.152.133.230', '2014-08-12 12:06:31.670', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (395, 3389, '多能力之药水', '', '使用时，HP/MP上限提高100，持续时间17分钟，免疫驱散魔法道具及驱散技能', 8, 8, '2', '0', '1', 0, 1, 0, '2012-07-20 08:57:23.687', 'CN90053', '61.152.133.230', '2014-08-12 12:06:27.153', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (396, 3394, '负重之咒 I', '', '使用后负重上限提升500，与负重之秘药可重叠使用.持续时间 5分钟', 1, 1, '1', '0', '0', 0, 1, 0, '2012-07-20 10:02:07.683', 'CN90053', '61.152.133.230', '2014-08-18 13:57:41.340', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (397, 5388, '吉伽梅西战士-NG', '', '', 990, 990, '2', '0', '0', 7, 1, 0, '2012-07-20 12:18:54.153', 'CN90053', '61.152.133.230', '2014-08-12 12:06:31.060', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (400, 3396, '负重之咒 III', '', '使用后负重上限提升1500，与负重之秘药可重叠使用.持续时间 5分钟', 3, 3, '1', '0', '0', 0, 1, 0, '2012-07-20 18:47:09.327', 'CN90053', '61.152.133.230', '2014-08-12 12:06:21.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (401, 2431, '辨识之书', '', '爽辨识之术后点击未鉴定物品，可随时随地不限次数的鉴定物品，购买后开始计时，有效期30天', 350, 350, '2', '0', '0', 30, 1, 0, '2012-07-20 17:33:30.530', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (405, 4156, '月光精灵魔法师(IV)项链', '', '装着月光精灵魔法师灵魂的项链(需要解封项链栏，60级以上），HP+150 MP+90 负重+1250', 750, 750, '1', '0', '1', 7, 1, 0, '2012-07-20 18:47:09.013', 'CN90053', '61.152.133.230', '2014-08-12 12:13:06.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (406, 4156, '月光精灵魔法师(IV)项链(7日)', '', '装着月光精灵魔法师灵魂的项链(需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 750, 750, '1', '0', '1', 7, 1, 0, '2012-07-20 18:47:08.780', 'CN90053', '61.152.133.230', '2014-08-12 12:06:23.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (411, 4152, '月光精灵战士(IV)项链(7日)', '', '装着月光精灵魔法师战士的项链(需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 750, 750, '1', '0', '1', 7, 1, 0, '2012-07-20 18:47:08.560', 'CN90053', '61.152.133.230', '2014-08-12 12:06:24.653', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (412, 4150, '月光精灵战士(V)项链(7日)', '', '装着月光精灵魔法师战士的近战攻击变身项链(需要解封项链栏，63级以上），HP+250 MP+150 负重+1750', 990, 990, '1', '0', '0', 7, 1, 0, '2012-07-20 18:47:08.437', 'CN90053', '61.152.133.230', '2014-08-12 13:17:53.280', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (413, 4150, '月光精灵战士(V)项链(7日)', '', '装着月光精灵魔法师战士的近战攻击变身项链(需要解封项链栏，63级以上），HP+200 MP+120 负重+1750
', 990, 990, '1', '0', '1', 7, 1, 0, '2012-07-20 18:47:08.390', 'CN90053', '61.152.133.230', '2014-08-12 13:17:53.217', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (422, 4152, '月光精灵战士(IV)项链(30日)', '', '装着月光精灵魔法师战士的项链(需要解封项链栏，60级以上），HP+150 MP+90 负重+1250', 1810, 1810, '1', '0', '1', 30, 1, 0, '2012-07-20 18:47:08.000', 'CN90053', '61.152.133.230', '2014-08-12 12:06:24.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (427, 3009, '月光复仇者项链(加强)(7日)', '', '封印着月光复仇者灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 750, 750, '1', '0', '0', 7, 1, 0, '2012-07-20 18:47:07.700', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (428, 3009, '月光复仇者项链(加强)(30日)', '', '封印着月光复仇者灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 2320, 1810, '2', '0', '0', 30, 1, 0, '2012-07-20 18:47:07.640', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (429, 3011, '锥锋复仇者项链(加强)(7日)', '', '封印着锥锋复仇者灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 750, 750, '2', '0', '0', 7, 1, 0, '2012-07-20 18:47:07.607', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.703', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (430, 3011, '锥锋复仇者项链(加强)(30日)', '', '封印着锥锋复仇者灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250', 2300, 1810, '1', '0', '0', 30, 1, 0, '2012-07-20 18:47:07.560', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.763', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (431, 2072, 'TESTNO', '', '活动礼包，购买后可获得增幅之咒(2倍)[3天]X2，负重之咒 III X20', 1740, 520, '4', '0', '0', 0, 1, 0, '2012-07-20 18:47:07.437', 'CN90053', '61.152.133.230', '2014-08-12 12:06:31.467', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (432, 2072, '掉率增幅之咒(2倍)[3天]', '', '活动礼包，购买后可获得增幅之咒(2倍)[3天]X2，负重之咒 III X20', 0, 0, '1', '0', '0', 0, 1, 72, '2012-07-20 18:58:44.593', 'CN90053', '61.152.133.230', '2014-08-12 12:06:31.420', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (433, 1825, '增幅之咒(3天)', '', '狩猎获得经验2倍', 20000, 20000, '1', '0', '0', 0, 1, 72, '2012-07-20 22:00:13.623', 'CN90053', '61.152.133.230', '2014-08-12 12:06:31.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (434, 2074, 'NG_test2', '', '活动限时发售道具，
打怪时获得的经验提高2倍.该效果可以替换掉低倍数"经验增幅之咒"的效果.经验增加效果与时间不累积.
', 200000, 200000, '4', '0', '0', 0, 1, 72, '2012-07-20 22:00:13.577', 'CN90053', '61.152.133.230', '2014-08-12 12:06:30.153', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (435, 5397, 'NGItem_01_TEST', '', '拥有吉伽梅西战士灵魂的项链近战(需要解封项链栏，66级以上），HP+250 MP+150 负重+1750', 200, 200, '2', '0', '0', 30, 1, 0, '2012-07-24 12:58:48.030', '', '61.152.133.230', '2014-08-12 12:06:30.920', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (441, 4160, '[体验]修炼少女V(1天)', '', '装有修炼少女灵魂的项链(需要解封项链栏，63级以上），HP+250 MP+150 负重+1750', 0, 0, '1', '0', '0', 1, 1, 0, '2013-11-20 16:47:12.060', 'CN90053', '222.128.29.241', '2014-08-12 12:06:30.607', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (442, 4151, '[体验]月光精灵魔法师V(1日）', '', '装着月光精灵魔法师灵魂的项链(需要解封项链栏，63级以上），HP+250 MP+150 负重+1750', 0, 0, '1', '0', '0', 1, 1, 0, '2013-11-20 16:47:11.967', 'CN90053', '222.128.29.241', '2014-08-12 12:06:30.560', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (443, 40619, '英雄力量精华 Lv3', '', 'TEST', 0, 0, '1', '0', '0', 30, 1, 0, '2014-01-11 15:06:12.187', 'CN90053', '222.128.29.241', '2014-08-12 12:06:30.200', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (444, 40619, '英雄力量精华 Lv3', '', '', 0, 0, '1', '0', '0', 30, 1, 0, '2014-04-22 19:21:20.857', 'CN90053', '124.205.178.150', '2014-08-12 12:06:29.763', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (445, 40620, '猎人敏捷精华 Lv3', '', '', 0, 0, '1', '0', '0', 30, 1, 0, '2014-06-03 10:57:40.187', 'CN90053', '124.205.178.150', '2014-08-12 12:06:29.700', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (446, 40621, '贤者智慧精华 Lv3', '', '', 0, 0, '1', '0', '0', 30, 1, 0, '2014-04-22 19:21:20.483', 'CN90053', '124.205.178.150', '2014-08-12 12:06:29.653', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (447, 6713, '至尊英雄力量精华 Lv3[30日]', '', '战争狂持有过的精华
力量+6 负重+360
攻击速度大幅度提高
移动速度大幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2014-04-22 19:21:20.077', 'CN90053', '124.205.178.150', '2014-08-12 12:06:28.247', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (448, 6715, '至尊贤者智慧精华 Lv3[30日]', '', '蕴藏着贤者智慧精华
智力+6 MP+24
攻击速度大幅度提高
移动速度大幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2014-05-13 15:06:57.857', 'CN90053', '124.205.178.150', '2014-08-12 12:06:28.013', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (449, 6717, '至尊猎人敏捷精华 Lv3[30日]', '', '蕴藏着猎人敏捷的精华
敏捷+6 远距离命中率+2
攻击速度小幅度提高
移动速度小幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2014-05-13 15:06:56.983', 'CN90053', '124.205.178.150', '2014-08-12 12:06:28.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (450, 4178, '赫利肖舍里斯项链(30日)', '', '封印着赫利肖舍里斯灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 1810, 1810, '1', '0', '1', 30, 1, 0, '2014-06-03 15:58:28.733', 'CN90053', '124.205.178.150', '2014-08-12 12:06:28.450', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (451, 40609, '[竞猜-德国]基础精华', '', '[竞猜-德国]基础精华', 100, 88, '4', '0', '0', 0, 5, 0, '2014-07-10 19:35:26.967', 'CN90053', '124.205.178.150', '2014-08-12 12:06:21.360', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (453, 7788, '征服德拉克箱子(7日)', '', 'test', 20000, 20000, '2', '0', '1', 0, 1, 0, '2014-09-05 22:38:01.903', 'CN90053', '124.205.178.150', '2014-09-05 22:38:01.903', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (456, 5556, '天然芒果汁', '', '感觉全身的力量变的越来越强大.\n\n力量 +3\n敏捷 +3\n智力 +3', 20000, 20000, '1', '0', '2', 0, 1, 0, '2014-09-07 20:20:14.200', 'CN90053', '61.152.133.230', '2014-09-07 20:20:14.200', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (457, 5571, '天然芒果糖', '', '甜蜜的气息环绕在身边\n\n提供已升华的魔法盾, 石化皮肤\n加速,增加负重, 神圣铠甲\n祝福光环状态.', 20000, 20000, '1', '0', '2', 0, 1, 0, '2014-09-07 20:20:14.450', 'CN90053', '61.152.133.230', '2014-09-07 20:20:14.450', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (460, 5574, '英雄礼花', '', '使用后，*2力量+2，暴击几率增加，近距离命中率增加。*9，效果时间30分钟', 10, 10, '2', '0', '1', 0, 1, 0, '2014-09-13 12:24:30.827', 'CN90053', '61.152.133.230', '2014-09-13 12:24:30.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (461, 5575, '猎人礼花', '', '使用后，*2敏捷+2，暴击几率增加，远距离命中率增加。*9，效果时间30分钟', 8, 8, '2', '0', '1', 0, 1, 0, '2014-09-13 12:24:45.983', 'CN90053', '61.152.133.230', '2014-09-13 12:24:45.983', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (462, 5576, '贤者礼花', '', '使用后，*2智力+2，MP最大值增加，魔法命中率增加。*9，效果时间30分钟', 8, 8, '2', '0', '1', 0, 1, 0, '2014-09-13 12:25:00.950', 'CN90053', '61.152.133.230', '2014-09-13 12:25:00.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (466, 5896, '神佑之粉（武器)(1天)', '', '缠绕着神圣气息的粉末。\n使用后一定时间内自身携带的所有武器受到神圣气息的保护，角色死亡时武器不会掉落。', 880, 880, '2', '0', '1', 0, 1, 24, '2014-09-10 17:11:29.340', 'CN90053', '61.152.133.230', '2014-09-10 17:11:29.340', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (472, 5913, '免罪符', '', '可*6免杀人犯罪名*9的道具。在杀人犯状态下使用时，可解除杀人犯状态。', 60, 60, '2', '0', '1', 0, 1, 0, '2014-09-10 17:19:48.967', 'CN90053', '61.152.133.230', '2014-09-10 17:19:48.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (503, 6222, '[附魔Ⅱ]暗黑法师(Lv60)项链(7天)', '', '*1*1[远程]*9*9蕴含暗黑法师灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 991, 991, '2', '0', '1', 7, 1, 0, '2014-09-10 18:18:07.920', 'CN90053', '61.152.133.230', '2014-09-10 18:18:07.920', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (538, 6651, '经验增幅之咒(3倍)(6小时)', '', '打怪时获得的经验提高*33倍*9。持续*36小时*9。', 150, 150, '2', '0', '1', 0, 1, 6, '2014-09-10 17:10:33.983', 'CN90053', '61.152.133.230', '2014-09-10 17:10:33.983', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (540, 6653, '经验增幅之咒(4倍)(6小时)', '', '打怪时获得的经验提高*34倍*9。持续*36小时*9。', 220, 220, '2', '0', '1', 0, 1, 6, '2014-09-10 17:11:29.513', 'CN90053', '61.152.133.230', '2014-09-10 17:11:29.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (592, 7626, '高级礼花箱子', '', '装有*3各10个*9*1英雄/猎人/贤者的礼花*9的箱子。', 180, 180, '2', '0', '1', 30, 1, 0, '2014-09-10 12:51:26.310', 'CN90053', '61.152.133.230', '2014-09-10 12:51:26.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (612, 7791, '神秘德拉克幸运箱子', '', '开启箱子可随机获得*1神秘德拉克戒指10日、14日、21日、30日*1的其中一枚
', 220, 80, '2', '0', '1', 30, 1, 0, '2014-09-10 12:44:04.547', 'CN90053', '61.152.133.230', '2014-09-10 12:44:04.547', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (613, 7791, '神秘德拉克幸运箱子', '', '开启箱子可随机获得*1神秘德拉克戒指10日、14日、21日、30日*1的其中一枚，可合成奇美拉和格雷芬特的指环。
', 220, 80, '1', '0', '1', 30, 1, 0, '2014-09-10 17:38:22.547', 'CN90053', '124.205.178.150', '2014-09-10 17:38:22.547', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (614, 7631, '变身项链符文 Ⅲ 综合箱子(30天)', '', '装有*1力量III、敏捷III、智力III、HPIII、MPIII*95枚变身项链符文及*3 3个*9符文还原符、2个除槽炼金符的箱子。有效期30日。', 401, 401, '2', '0', '1', 30, 1, 0, '2014-09-11 12:41:22.153', 'CN90053', '124.205.178.150', '2014-09-11 12:41:22.153', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (629, 5912, '格林汉特的守护', '', '强化装备失败时,背包内如有格林汉特的守护,可以一定几率保护强化失败的装备。因保护气息不太稳定，强化等级会降1级', 680, 680, '2', '0', '1', 0, 1, 0, '2014-09-16 19:38:11.123', 'CN90053', '124.205.178.150', '2014-09-16 19:38:11.123', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (640, 6556, '阿努比斯(Lv60)项链(30天)', '', '*1[近战]*9装着阿努比斯灵魂的近战攻击变身项链(需要解封项链栏，*260级以上*9)，*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 1810, 1481, '2', '0', '1', 30, 1, 0, '2014-09-18 14:14:16.700', 'CN90053', '124.205.178.150', '2014-09-18 14:14:16.700', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (641, 1594, '地下城入场券(30天)', '', '双击使用后，可进入地下城，快捷键"T"可确认剩余持续时间。', 360, 60, '3', '0', '1', 0, 1, 720, '2014-09-18 16:40:08.653', 'CN90053', '124.205.178.150', '2014-09-18 16:40:08.653', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (644, 7637, '腰带符文 Ⅱ 狩猎箱子(30天)', '', '装有*1负重 II、减伤 II*92枚腰带符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9，30天内不使用的话，箱子将自动删除。', 153, 103, '1', '0', '1', 30, 1, 0, '2014-11-05 11:42:27.560', 'CN90053', '124.205.178.150', '2014-11-05 11:42:27.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (645, 7636, '腰带符文 Ⅱ 攻击箱子(30天)', '', '装有*1所有攻击力 II、所有命中率 II*92枚腰带符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子，有效期*430日*930天内不使用的话，箱子将自动删除。', 203, 153, '4', '0', '1', 30, 1, 0, '2014-11-05 11:45:34.483', 'CN90053', '124.205.178.150', '2014-11-05 11:45:34.483', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (646, 7642, '戒指符文 Ⅱ 狩猎箱子(30天)', '', '装有*1防御 II、减伤 II、HP恢复 II、MP恢复 II*94枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*930天内不使用的话，箱子将自动删除。', 154, 104, '4', '0', '1', 30, 1, 0, '2014-11-05 11:56:28.233', 'CN90053', '124.205.178.150', '2014-11-05 11:56:28.233', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (647, 7641, '戒指符文 Ⅱ 暴击箱子(30天)', '', '装有*1暴击率 II、暴击伤害 II*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*930天内不使用的话，箱子将自动删除。', 254, 194, '4', '0', '1', 30, 1, 0, '2014-11-05 11:57:50.543', 'CN90053', '124.205.178.150', '2014-11-05 11:57:50.543', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (648, 7640, '戒指符文 Ⅱ 攻击箱子(30天)', '', '装有*1所有伤害 II、所有命中率 II*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9，30天内不使用的话，箱子将自动删除。', 204, 124, '4', '0', '1', 30, 1, 0, '2014-11-05 12:01:10.543', 'CN90053', '124.205.178.150', '2014-11-05 12:01:10.543', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (649, 7634, '德拉克戒指符文 Ⅳ 综合箱子(30天)', '', '装有蛛网术抵抗IV、麻痹抵抗IV、减速抵抗IV、药水破坏抵抗IV、陨石术抵抗IV5枚德拉克指环符文及3个符文还原符、2个除槽炼金符的箱子。有效期30日。', 202, 92, '4', '0', '1', 30, 1, 0, '2014-11-05 12:02:18.263', 'CN90053', '124.205.178.150', '2014-11-05 12:02:18.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (663, 7626, '高级礼花箱子', '', '高级礼花箱子: 装有*210个英雄/猎人/贤者的礼花*9的箱子。箱子中物品*6可交易*9，可以跟不同职业分享使用。', 260, 180, '3', '0', '1', 30, 1, 0, '2014-12-30 13:38:58.090', 'CN90053', '124.205.178.150', '2014-12-30 13:38:58.090', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (667, 50001, '[暴击]首饰符文礼包(30天)', '', '[暴击]首饰符文礼包(30天)
礼包内符文箱子原价*11614GOLD*9，礼包价*4701GOLD*9。
礼包内含有：*4 戒指符文 Ⅱ 暴击箱子x1,变身项链符文 Ⅲ 综合箱子x1*9,腰带符文 Ⅱ 攻击箱子(30天),神秘德拉克幸运箱子x1共5个,德拉克戒指符文 Ⅳ 综合箱子x1,打开所有箱子物品共计，*314枚*9符文、*312个*9符文还原符、*38个*9除槽炼金符、*31枚*9神秘德拉克指环。', 1614, 701, '1', '1', '1', 0, 1, 0, '2015-01-23 20:56:50.590', 'CN90053', '124.205.178.150', '2015-01-23 20:56:50.590', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (669, 50042, '至尊英雄力量精华LV3礼包', '', '至尊英雄力量精华含
*1英雄力量精华LV3(30日)*9 1个 
*4英雄力量精华LV3 攻击速度和移动速度效果最强*9
变身强化精华LV3（30日）1个
幸运精华LV3（30日）1个', 880, 880, '3', '1', '1', 0, 1, 0, '2015-01-25 12:10:17.607', 'CN90053', '124.205.178.150', '2015-01-25 12:10:17.607', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (670, 50038, '[攻击]首饰符文礼包(30日)', '', '[攻击]首饰符文礼包(30天)
礼包内符文箱子原价*11484GOLD*9，礼包价*4631GOLD*9。
礼包内含有：*4 戒指符文 Ⅱ 攻击箱子x1,变身项链符文 Ⅲ 综合箱子x1,腰带符文 Ⅱ 攻击箱子(30天)*9,神秘德拉克幸运箱子x1共5个,德拉克戒指符文 Ⅳ 综合箱子x1,打开所有箱子物品共计，*314枚*9符文、*312个*9符文还原符、*38个*9除槽炼金符、*31枚*9神秘德拉克指环。', 1484, 631, '1', '1', '1', 0, 1, 0, '2015-01-25 12:17:27.887', 'CN90053', '124.205.178.150', '2015-01-25 12:17:27.887', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (671, 7624, '[EVNT]高级补给箱子', '', '装有*3100个芒果糖，100个芒果汁和10个防御提升卷轴*9的箱子。', 20000, 20000, '4', '0', '2', 30, 1, 0, '2015-03-20 17:29:06.483', 'CN90053', '124.205.178.150', '2015-03-20 17:29:06.483', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (672, 6654, '[EVNT]经验增幅之咒（4.5倍）', '', '打怪时获得的*2经验提高4.5倍*9。', 20000, 20000, '4', '0', '2', 0, 1, 6, '2015-03-20 17:32:50.153', 'CN90053', '124.205.178.150', '2015-03-20 17:32:50.153', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (680, 2884, '陈.音库柏斯(Lv60)项链(30天)', '', '*1*1[近战]*9*9变身为陈.音库柏斯（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 1810, 1410, '1', '0', '1', 30, 1, 0, '2015-06-30 10:41:20.547', 'CN90053', '114.112.126.218', '2015-06-30 10:41:20.547', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (690, 1980, '嗜血的吸血鬼(Lv1)项链(7天)', '', '*1*1[近战]*9*9变身为嗜血的吸血鬼（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 281, 281, '1', '0', '1', 7, 1, 0, '2015-09-01 11:51:11.733', 'CN90053', '114.112.126.218', '2015-09-01 11:51:11.733', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (691, 1980, '嗜血的吸血鬼(Lv1)项链(30天)', '', '*1*1[近战]*9*9变身为嗜血的吸血鬼（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 880, 551, '1', '0', '1', 30, 1, 0, '2015-09-01 11:51:20.810', 'CN90053', '114.112.126.218', '2015-09-01 11:51:20.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (692, 1981, '印谱诺(Lv1)项链(7天)', '', '*1*1[近战]*9*9变身为印谱诺（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 281, 281, '1', '0', '1', 7, 1, 0, '2015-09-01 11:50:52.590', 'CN90053', '114.112.126.218', '2015-09-01 11:50:52.590', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (693, 1981, '印谱诺(Lv1)项链(30天)', '', '*1*1[近战]*9*9变身为印谱诺（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 880, 551, '1', '0', '1', 30, 1, 0, '2015-09-01 11:51:04.090', 'CN90053', '114.112.126.218', '2015-09-01 11:51:04.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (694, 2027, '圣甲战魔(Lv1)项链(7天)', '', '*1*1[远程]*9*9变身为圣甲战魔（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9
', 281, 281, '1', '0', '1', 7, 1, 0, '2015-09-01 11:51:27.390', 'CN90053', '114.112.126.218', '2015-09-01 11:51:27.390', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (695, 2027, '圣甲战魔(Lv1)项链(30天)', '', '*1*1[远程]*9*9变身为圣甲战魔（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 880, 551, '1', '0', '1', 30, 1, 0, '2015-09-01 11:51:34.700', 'CN90053', '114.112.126.218', '2015-09-01 11:51:34.700', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (696, 4155, '月光精灵战士(Lv1)项链(7天)', '', '*1*1*1[近战]*9*9*9变身为月光精灵战士（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 281, 281, '1', '0', '1', 7, 1, 0, '2015-09-01 11:51:41.090', 'CN90053', '114.112.126.218', '2015-09-01 11:51:41.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (697, 4155, '月光精灵战士(Lv1)项链(30天)', '', '*1*1*1[近战]*9*9*9变身为月光精灵战士（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 800, 551, '1', '0', '1', 30, 1, 0, '2015-09-01 11:51:57.903', 'CN90053', '114.112.126.218', '2015-09-01 11:51:57.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (699, 1602, '装备解封之咒(5个槽)', '', '使用后*2开启装备栏中5个被封印窗口*9，从而可佩戴*6两个戒指，项链，腰带和披风*9。', 200, 1, '3', '0', '1', 0, 1, 720, '2015-09-01 11:49:21.810', 'CN90053', '114.112.126.218', '2015-09-01 11:49:21.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (700, 2033, '月光复仇者(Lv54)项链(7天)', '', '*1*1[近战]*9*9变身为月光复仇者（需要解封项链栏，*254级以上*9），*3HP+75 MP+45 攻击速度和移动速度提升 负重+500*9', 450, 381, '1', '0', '1', 7, 1, 0, '2015-09-01 12:46:32.297', 'CN90053', '114.112.126.218', '2015-09-01 12:46:32.297', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (701, 2039, '锥锋复仇者(Lv54)项链(7天)', '', '*1*1[远程]*9*9变身为锥锋复仇者（需要解封项链栏，*254级以上*9），*3HP+75 MP+45 攻击速度和移动速度提升 负重+500*9', 450, 381, '1', '0', '1', 7, 1, 0, '2015-09-01 12:47:00.903', 'CN90053', '114.112.126.218', '2015-09-01 12:47:00.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (702, 2884, '陈.音库柏斯(Lv60)项链(7天)', '', '*1*1[近战]*9*9变身为陈.音库柏斯（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 650, 551, '1', '0', '1', 7, 1, 0, '2015-09-01 12:47:38.090', 'CN90053', '114.112.126.218', '2015-09-01 12:47:38.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (703, 2884, '陈.音库柏斯(Lv60)项链(30天)', '', '*1*1[近战]*9*9变身为陈.音库柏斯（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 1810, 1251, '1', '0', '1', 30, 1, 0, '2015-09-01 12:48:03.280', 'CN90053', '114.112.126.218', '2015-09-01 12:48:03.280', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (704, 3012, '威尔斯沃琪(Lv60)项链(7天)', '', '*1*1[远程]*9*9变身为威尔斯沃琪（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 650, 551, '1', '0', '1', 7, 1, 0, '2015-09-01 12:48:28.607', 'CN90053', '114.112.126.218', '2015-09-01 12:48:28.607', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (705, 3012, '威尔斯沃琪(Lv60)项链(30天)', '', '*1*1[远程]*9*9变身为威尔斯沃琪（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 1810, 1251, '1', '0', '1', 30, 1, 0, '2015-09-01 12:49:06.700', 'CN90053', '114.112.126.218', '2015-09-01 12:49:06.700', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (706, 4148, '芭芭莉安(Lv63)项链(30天)', '', '*1*1[近战]*9*9装有着芭芭莉安灵魂的项链 变身为暗黑法师(需要解封项链栏，*263级以上*9)，*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9', 2100, 1351, '1', '0', '1', 30, 1, 0, '2015-09-01 12:49:31.327', 'CN90053', '114.112.126.218', '2015-09-01 12:49:31.327', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (707, 4149, '暗黑法师(Lv63)项链(30天)', '', '*1*1[远程]*9*9装有着暗黑法师灵魂的项链 变身为暗黑法师(需要解封项链栏，*263级以上*9)，*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9', 2100, 1351, '1', '0', '1', 30, 1, 0, '2015-09-01 12:49:47.310', 'CN90053', '114.112.126.218', '2015-09-01 12:49:47.310', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (708, 4150, '[进击]月光精灵战士(Lv63)项链(30天)', '', '*1*1[近战]*9*9装着进击的月光精灵战士灵魂的近战攻击项链，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 1691, '3', '0', '1', 30, 1, 0, '2015-09-01 12:51:07.390', 'CN90053', '114.112.126.218', '2015-09-01 12:51:07.390', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (709, 4151, '[进击]月光精灵魔法师(Lv63)项链(30天)', '', '*1*1[远程]*9*9装着进击的月光精灵魔法师灵魂的远程攻击变身项链(需要解封项链栏，*263级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 1691, '3', '0', '1', 30, 1, 0, '2015-09-01 12:51:29.373', 'CN90053', '114.112.126.218', '2015-09-01 12:51:29.373', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (710, 4160, '[进击]修炼少女 (Lv63) 项链(30天)', '', '*1*1[近战]*9*9装着进击的修炼少女的近战攻击项链(需要解封项链栏，*263级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 1691, '3', '0', '1', 30, 1, 0, '2015-09-01 12:52:06.153', 'CN90053', '114.112.126.218', '2015-09-01 12:52:06.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (712, 4161, '[进击]东瀛少女 (Lv63) 项链(30天)', '', '*1*1[近战]*9*9装着进击的修炼少女灵魂的近战攻击项链(需要解封项链栏，*263级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 1691, '3', '0', '1', 30, 1, 0, '2015-09-01 12:52:37.810', 'CN90053', '114.112.126.218', '2015-09-01 12:52:37.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (713, 5397, '吉伽梅西战士(Lv66)项链(30天)', '', '*1*1[近战]*9*9拥有吉伽梅西战士灵魂的项链，近战(需要解封项链栏，*266级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 2610, 1551, '1', '0', '1', 30, 1, 0, '2015-09-01 12:52:52.077', 'CN90053', '114.112.126.218', '2015-09-01 12:52:52.077', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (714, 5398, '吉伽梅西弓箭手(Lv66)项链(30天)', '', '*1*1[远程]*9*9拥有吉伽梅西弓箭手灵魂的项链，远程(需要解封项链栏，*266级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 1551, '1', '0', '1', 30, 1, 0, '2015-09-01 12:53:30.090', 'CN90053', '114.112.126.218', '2015-09-01 12:53:30.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (719, 7608, '[礼包用]幸运精华箱子中级', '', '[礼包用]幸运精华箱子中级(30天)', 20000, 20000, '1', '0', '2', 30, 1, 0, '2015-09-07 16:39:35.140', 'CN90053', '114.112.126.218', '2015-09-07 16:39:35.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (720, 6717, '[礼包用]猎人敏捷精华LV3', '', '[礼包用]猎人敏捷精华LV3(30天)', 20000, 20000, '1', '0', '2', 30, 1, 0, '2015-09-07 17:11:51.153', 'CN90053', '114.112.126.218', '2015-09-07 17:11:51.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (721, 40605, '[礼包用]变身强化精华LV3', '', '[礼包用]变身强化精华LV3（30天）', 0, 0, '1', '0', '2', 30, 1, 0, '2015-09-07 17:43:28.357', 'CN90053', '114.112.126.218', '2015-09-07 17:43:28.357', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (723, 50004, '[中级幸运]猎人精华礼包', '', '[中级幸运]猎人精华礼包含
*1猎人敏捷精华LV3(30日)*9 1个 
*2变身强化精华LV3（30日）*9 1个
*3幸运精华箱子中级 (30天)*9 1个
(最高可获得*4幸运精华 Lv6*9)', 1380, 960, '3', '1', '1', 0, 1, 0, '2015-09-07 18:27:11.467', 'CN90053', '114.112.126.218', '2015-09-07 18:27:11.467', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (726, 6713, '[礼包用]英雄力量精华LV3', '', '[礼包用]英雄力量精华LV3(30天)', 20000, 20000, '1', '0', '2', 30, 1, 0, '2015-09-07 19:22:14.467', 'CN90053', '114.112.126.218', '2015-09-07 19:22:14.467', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (727, 50014, '[中级幸运]英雄精华礼包', '', '[中级幸运]英雄精华礼包含
*1英雄力量精华LV3(30日)*9 1个 
*2变身强化精华LV3（30日）*9 1个
*3幸运精华箱子中级 (30天)*9 1个
(最高可获得*4幸运精华 Lv6*9', 1380, 960, '3', '1', '1', 0, 1, 0, '2015-09-07 19:29:26.920', 'CN90053', '114.112.126.218', '2015-09-07 19:29:26.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (728, 6715, '[礼包用]贤者智慧精华LV3', '', '[礼包用]贤者智慧精华LV3（30天）', 20000, 20000, '1', '0', '2', 30, 1, 0, '2015-09-08 10:57:02.373', 'CN90053', '114.112.126.218', '2015-09-08 10:57:02.373', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (729, 50003, '[中级幸运]贤者精华礼包', '', '[中级幸运]贤者精华礼包含
*1贤者智慧精华LV3(30日)*9 1个 
*2变身强化精华LV3（30日）*9 1个
*3幸运精华箱子中级 (30天)*9 1个
(最高可获得*4幸运精华 Lv6*9)', 1380, 960, '3', '1', '1', 0, 1, 0, '2015-09-08 11:04:53.810', 'CN90053', '114.112.126.218', '2015-09-08 11:04:53.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (730, 3353, '混沌点券充值卡 700P', '', '充值混沌点数的物品*1700 点*9,可以在各个城镇的*2混沌积分商人*9处购买*3战利品*9及其他重要道具。', 35, 35, '2', '0', '1', 0, 1, 0, '2015-09-14 12:24:22.653', 'CN90053', '114.112.126.218', '2015-09-14 12:24:22.653', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (731, 3363, '混沌点券充值卡 4000P', '', '充值混沌点数的物品*14000 点*9 充值,可以在各个城镇的*2混沌积分商人*9处购买*3战利品*9及其他重要道具。', 200, 195, '3', '0', '1', 0, 1, 0, '2015-09-14 12:31:16.547', 'CN90053', '114.112.126.218', '2015-09-14 12:31:16.547', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (785, 6199, '[附魔I]阿努比斯(Lv60)项链(7天)', '', '*1*1[近战]*9*9蕴含阿努比斯灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 991, 761, '1', '0', '1', 7, 1, 0, '2015-09-19 21:01:29.577', 'CN90053', '114.112.126.218', '2015-09-19 21:01:29.577', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (786, 6199, '[附魔I]阿努比斯(Lv60)项链(30天)', '', '*1*1[近战]*9*9蕴含阿努比斯灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 2181, 1361, '1', '0', '1', 30, 1, 0, '2015-09-19 21:06:00.123', 'CN90053', '114.112.126.218', '2015-09-19 21:06:00.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (787, 6222, '[附魔Ⅱ]暗黑法师(Lv60)项链(7天)', '', '*1*1[远程]*9*9蕴含暗黑法师灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 991, 961, '1', '0', '1', 7, 1, 0, '2015-09-19 21:09:23.327', 'CN90053', '114.112.126.218', '2015-09-19 21:09:23.327', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (788, 6222, '[附魔Ⅱ]暗黑法师(Lv60)项链(30天)', '', '*1*1[远程]*9*9蕴含暗黑法师灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 2381, 1471, '1', '0', '1', 30, 1, 0, '2015-09-19 21:10:37.840', 'CN90053', '114.112.126.218', '2015-09-19 21:10:37.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (789, 6196, '[附魔Ⅱ]忍者少女(Lv60)项链(7天)', '', '*1*1[近战]*9*9赋予特殊能力的忍者少女变身项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 991, 871, '1', '0', '1', 7, 1, 0, '2015-09-19 21:13:43.263', 'CN90053', '114.112.126.218', '2015-09-19 21:13:43.263', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (790, 6196, '[附魔Ⅱ]忍者少女(Lv60)项链(30天)', '', '*1*1[近战]*9*9赋予特殊能力的忍者少女变身项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 2381, 1471, '1', '0', '1', 30, 1, 0, '2015-09-19 21:14:45.890', 'CN90053', '114.112.126.218', '2015-09-19 21:14:45.890', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (791, 6206, '[附魔I]阿修罗(Lv63)项链(7天)', '', '*1*1[近战]*9*9赋予特殊能力的阿修罗变身项链。拥有*263级*9的变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 1171, 961, '1', '0', '1', 7, 1, 0, '2015-09-20 13:33:49.670', 'CN90053', '114.112.126.218', '2015-09-20 13:33:49.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (792, 6206, '[附魔I]阿修罗(Lv63)项链(30天)', '', '*1*1[近战]*9*9赋予特殊能力的阿修罗变身项链。拥有*263级*9的变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 2371, 1461, '1', '0', '1', 30, 1, 0, '2015-09-20 13:34:33.170', 'CN90053', '114.112.126.218', '2015-09-20 13:34:33.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (793, 6203, '[附魔Ⅱ]春丽(Lv63)项链(7天)', '', '*1*1[近战]*9*9赋予特殊能力的春丽变身项链。拥有*263级*9的变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 1271, 1071, '1', '0', '1', 7, 1, 0, '2015-09-20 13:36:09.827', 'CN90053', '114.112.126.218', '2015-09-20 13:36:09.827', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (794, 6203, '[附魔Ⅱ]春丽(Lv63)项链(30天)', '', '*1*1[近战]*9*9赋予特殊能力的春丽变身项链。拥有*263级*9的变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 2571, 1571, '1', '0', '1', 30, 1, 0, '2015-09-20 13:37:11.140', 'CN90053', '114.112.126.218', '2015-09-20 13:37:11.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (795, 6226, '[附魔Ⅲ]吉伽梅西弓箭手(Lv63)项链(7天)', '', '*1*1[远程]*9*9蕴含吉伽梅西弓箭手灵魂的项链。拥有*263级*9变身能力值*4力量+3 敏捷+3 智力+3*9', 1581, 1281, '2', '0', '1', 7, 1, 0, '2015-09-20 13:39:42.013', 'CN90053', '114.112.126.218', '2015-09-20 13:39:42.013', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (796, 6226, '[附魔Ⅲ]吉伽梅西弓箭手(Lv63)项链(30天)', '', '*1*1[远程]*9*9蕴含吉伽梅西弓箭手灵魂的项链。拥有*263级*9变身能力值*4力量+3 敏捷+3 智力+3*9', 2781, 1681, '2', '0', '1', 30, 1, 0, '2015-09-20 13:41:05.827', 'CN90053', '114.112.126.218', '2015-09-20 13:41:05.827', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (799, 6227, '[附魔I]赫利肖舍里斯变身(Lv66)项链(7天)', '', '*1*1[远程]*9*9蕴含赫利肖舍里斯灵魂的项链。拥有*266级*9变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 1481, 951, '2', '0', '1', 7, 1, 0, '2015-09-20 14:07:13.280', 'CN90053', '114.112.126.218', '2015-09-20 14:07:13.280', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (800, 6227, '[附魔I]赫利肖舍里斯变身(Lv66)项链(30天)', '', '*1*1[远程]*9*9蕴含赫利肖舍里斯灵魂的项链。拥有*266级*9变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 2781, 1661, '2', '0', '1', 30, 1, 0, '2015-09-20 14:08:09.297', 'CN90053', '114.112.126.218', '2015-09-20 14:08:09.297', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (801, 6213, '[附魔I]武士(Lv66)项链(7天)', '', '*1*1[近战]*9*9赋予特殊能力的武士变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 1481, 951, '1', '0', '1', 7, 1, 0, '2015-09-20 14:33:57.140', 'CN90053', '114.112.126.218', '2015-09-20 14:33:57.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (802, 6213, '[附魔I]武士(Lv66)项链(30天)', '', '*1*1[近战]*9*9赋予特殊能力的武士变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 2781, 1661, '1', '0', '1', 30, 1, 0, '2015-09-20 14:34:59.123', 'CN90053', '114.112.126.218', '2015-09-20 14:34:59.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (803, 7537, '女巫(Lv66)项链(30天)', '', '*1[近战]*9蕴含女巫的变身项链。赋予*266级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。', 2881, 1551, '2', '0', '1', 30, 1, 0, '2015-09-20 14:39:07.233', 'CN90053', '114.112.126.218', '2015-09-20 14:39:07.233', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (808, 6558, '角斗士(Lv69)项链(30天)', '', '*1*1[近战]*9*9变身为角斗士（需要解封项链栏，*269级以上*9），*3HP+300 MP+180 攻击速度和移动速度提升 负重+2000*9', 2981, 1751, '1', '0', '1', 30, 1, 0, '2015-09-20 15:38:59.373', 'CN90053', '114.112.126.218', '2015-09-20 15:38:59.373', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (810, 6559, '地狱领主(Lv72)项链(30天)', '', '[近战]蕴含地狱领主灵魂的项链。赋予72级变身HP/MP/负重/攻击速度/移动速度等能力值的变身项链。', 3581, 2051, '1', '0', '1', 30, 1, 0, '2015-09-20 15:41:52.797', 'CN90053', '114.112.126.218', '2015-09-20 15:41:52.797', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (811, 7539, '吉伽梅西弓箭手(Lv69)项链（30天）', '', '[远程]蕴含吉伽梅西弓箭手的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。', 2981, 1751, '1', '0', '1', 30, 1, 0, '2015-09-22 11:53:22.107', 'CN90053', '114.112.126.218', '2015-09-22 11:53:22.107', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (813, 7540, '[附魔Ⅱ]修道士(Lv69)项链(30天)', '', '*1[近战]*9蕴含修道士的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 3381, 1971, '2', '0', '1', 30, 1, 0, '2015-09-22 12:15:57.467', 'CN90053', '114.112.126.218', '2015-09-22 12:15:57.467', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (814, 7540, '[附魔Ⅱ]修道士(Lv69)(7天)', '', '*1[近战]*9蕴含修道士的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 1671, 1471, '2', '0', '1', 7, 1, 0, '2015-09-22 17:18:08.750', 'CN90053', '114.112.126.218', '2015-09-22 17:18:08.750', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (815, 7542, '[附魔Ⅱ]女王骑士(Lv69)项链(7天)', '', '*1[近战]*9蕴含女王骑士的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 1671, 1471, '2', '0', '1', 7, 1, 0, '2015-09-22 17:23:25.920', 'CN90053', '114.112.126.218', '2015-09-22 17:23:25.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (816, 7542, '[附魔Ⅱ]女王骑士(Lv69)项链(30天) ', '', '*1[近战]*9蕴含女王骑士的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 3381, 1971, '2', '0', '1', 30, 1, 0, '2015-09-22 17:27:03.607', 'CN90053', '114.112.126.218', '2015-09-22 17:27:03.607', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (817, 7822, 'test_[进击][附魔I]火枪手(Lv75', '', '', 1991, 1591, '2', '0', '1', 13, 1, 0, '2015-12-04 16:14:01.873', 'CN90053', '114.112.126.218', '2015-12-04 16:14:01.873', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (818, 2073, 'test_银币掉落增幅之咒(2倍)', '', '双及使用后，狩猎获得银币量为原由银币的*22倍*9，快捷键"T"可确认使用状态', 263, 161, '2', '0', '1', 0, 1, 24, '2015-12-04 18:33:37.030', 'CN90053', '114.112.126.218', '2015-12-04 18:33:37.030', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (819, 3395, '[test]负重之咒 II', '', '使用后*2负重上升1000*9，与负重之秘药可重叠使用。持续时间 *45分钟*9', 2, 2, '2', '0', '1', 0, 1, 0, '2015-12-04 18:53:40.140', 'CN90053', '114.112.126.218', '2015-12-04 18:53:40.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (820, 7041, '[test][附魔Ⅲ]狂战士(Lv75)项链', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 2581, 2581, '2', '0', '1', 30, 1, 0, '2015-12-04 18:58:06.153', 'CN90053', '114.112.126.218', '2015-12-04 18:58:06.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (821, 7607, '[test]幸运精华箱子高级 (7天)', '', '装有幸运精华的箱子。开启箱子时，可随机获得*1幸运精华Lv6、Lv7*9其中的一个。*47日*9。', 210, 99, '2', '0', '1', 7, 1, 0, '2015-12-04 19:03:52.077', 'CN90053', '114.112.126.218', '2015-12-04 19:03:52.077', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (842, 6205, '[附魔Ⅲ]炼金术士(Lv60)(30天)', '', '*1[近战]*9赋予特殊能力的炼金术士变身项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3481, 1581, '2', '0', '1', 30, 1, 0, '2016-03-22 11:43:04.200', 'CN90053', '114.112.126.218', '2016-03-22 11:43:04.200', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (843, 6207, '[附魔Ⅲ]女巫(Lv63)(30天)', '', '*1[近战]*9蕴含女巫灵魂的项链。拥有*263级*9变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 1681, '2', '0', '1', 30, 1, 0, '2016-03-22 11:43:04.140', 'CN90053', '114.112.126.218', '2016-03-22 11:43:04.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (844, 7038, '[附魔Ⅲ]恶魔追猎者(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔追猎者变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3881, 1881, '2', '0', '1', 30, 1, 0, '2016-03-22 11:43:04.093', 'CN90053', '114.112.126.218', '2016-03-22 11:43:04.093', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (845, 7039, '[附魔Ⅲ]大力神(Lv69)(30天)', '', '*1[近战]*9赋予特殊能力的大力神变身项链。拥有*269级*9的变身能力值*3HP+300 MP+180 攻击速度和移动速度提升 负重+2000*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4081, 2081, '2', '0', '1', 30, 1, 0, '2016-03-22 11:43:04.030', 'CN90053', '114.112.126.218', '2016-03-22 11:43:04.030', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (868, 2073, '银币掉落增幅之咒(2倍)(1天)', '', '双及使用后，狩猎获得银币量为原由银币的*22倍*9，快捷键"T"可确认使用状态', 280, 68, '2', '0', '1', 0, 1, 24, '2016-04-21 12:27:53.890', 'CN90053', '114.112.126.218', '2016-04-21 12:27:53.890', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (877, 7789, '[活动]征服德拉克箱子(30日)', '', '装有*12个德拉克神秘戒指*9、*11个封印的奇美拉卷轴、封印的格雷芬特卷轴*9的箱子。有效期限为*430天*9', 880, 240, '2', '0', '1', 30, 1, 0, '2016-04-21 16:10:29.217', 'CN90053', '114.112.126.218', '2016-04-21 16:10:29.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (878, 6660, '[活动]经验增幅道具宝箱（中级）', '', '装着*1经验增幅之咒*9的箱子。打开可随机获得*33倍~6倍*9其中的一个。，人品爆棚可获得*46小时*9的*36倍*9经验增幅之咒', 380, 178, '2', '0', '1', 0, 1, 0, '2016-04-21 16:11:44.123', 'CN90053', '114.112.126.218', '2016-04-21 16:11:44.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (879, 2073, '银币掉落增幅之咒(2倍)(1天)', '', '双及使用后，狩猎获得银币量为原由银币的*22倍*9，快捷键"T"可确认使用状态', 280, 68, '2', '0', '1', 0, 1, 24, '2016-04-21 16:12:52.310', 'CN90053', '114.112.126.218', '2016-04-21 16:12:52.310', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (881, 7816, '[进击Ⅱ][附魔Ⅱ]名妓黄真伊(Lv66)(30天)', '', '*1[近战]*9蕴含名妓黄真伊灵魂的项链,赋予超强近战攻击能力。拥有*266级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 4381, 2491, '2', '0', '1', 30, 1, 0, '2016-04-28 10:02:34.310', 'CN90053', '114.112.126.218', '2016-04-28 10:02:34.310', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (882, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)(30天)', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+3 敏捷+3 智力+3*9', 4081, 2181, '2', '0', '1', 30, 1, 0, '2016-04-28 10:02:42.543', 'CN90053', '114.112.126.218', '2016-04-28 10:02:42.543', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (883, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 2381, '2', '0', '1', 30, 1, 0, '2016-04-28 10:02:50.780', 'CN90053', '114.112.126.218', '2016-04-28 10:02:50.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (884, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4481, 2581, '2', '0', '1', 30, 1, 0, '2016-04-28 10:02:57.403', 'CN90053', '114.112.126.218', '2016-04-28 10:02:57.403', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (885, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 4581, 2691, '2', '0', '1', 30, 1, 0, '2016-04-28 10:03:05.920', 'CN90053', '114.112.126.218', '2016-04-28 10:03:05.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (886, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5900, 2991, '2', '0', '1', 30, 1, 0, '2016-04-28 10:03:12.390', 'CN90053', '114.112.126.218', '2016-04-28 10:03:12.390', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (887, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5400, 2781, '2', '0', '1', 30, 1, 0, '2016-04-28 10:04:33.950', 'CN90053', '114.112.126.218', '2016-04-28 10:04:33.950', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (890, 2800, '英雄箱子', '', '开启英雄箱子可随机获得+0Lv4精华或基础精华10个。', 2900, 1280, '2', '0', '1', 0, 1, 0, '2016-05-03 12:25:00.403', 'CN90053', '114.112.126.218', '2016-05-03 12:25:00.403', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (891, 7816, '[进击Ⅱ][附魔Ⅱ]名妓黄真伊(Lv66)(30天)', '', '*1[近战]*9蕴含名妓黄真伊灵魂的项链,赋予超强近战攻击能力。拥有*266级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 4800, 2491, '2', '0', '1', 30, 1, 0, '2016-05-24 10:48:46.793', 'CN90053', '114.112.126.218', '2016-05-24 10:48:46.793', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (892, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)(30天)', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+3 敏捷+3 智力+3*9', 4200, 2181, '2', '0', '1', 30, 1, 0, '2016-05-24 10:50:52.187', 'CN90053', '114.112.126.218', '2016-05-24 10:50:52.187', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (893, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4600, 2381, '2', '0', '1', 30, 1, 0, '2016-05-24 10:52:51.590', 'CN90053', '114.112.126.218', '2016-05-24 10:52:51.590', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (894, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5000, 2581, '2', '0', '1', 30, 1, 0, '2016-05-24 10:54:56.623', 'CN90053', '114.112.126.218', '2016-05-24 10:54:56.623', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (895, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5200, 2691, '2', '0', '1', 30, 1, 0, '2016-05-24 10:58:01.030', 'CN90053', '114.112.126.218', '2016-05-24 10:58:01.030', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (896, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5800, 2991, '2', '0', '1', 30, 1, 0, '2016-05-24 11:01:12.250', 'CN90053', '114.112.126.218', '2016-05-24 11:01:12.250', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (897, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5400, 2781, '2', '0', '1', 30, 1, 0, '2016-05-24 11:02:43.047', 'CN90053', '114.112.126.218', '2016-05-24 11:02:43.047', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (898, 7643, '戒指符文 Ⅲ 攻击箱子(30天)', '', '装有*1所有伤害 III、所有命中率 III*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 404, 204, '1', '0', '1', 30, 1, 0, '2016-05-24 11:04:39.140', 'CN90053', '114.112.126.218', '2016-05-24 11:04:39.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (899, 7644, '戒指符文 Ⅲ 暴击箱子(30天)', '', '装有*1暴击率 III、暴击伤害 III*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 504, 284, '2', '0', '1', 30, 1, 0, '2016-05-24 12:03:58.170', 'CN90053', '114.112.126.218', '2016-05-24 12:03:58.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (900, 7638, '腰带符文 Ⅲ 攻击箱子(30天)', '', '装有*1所有攻击力 III、所有命中率 III*92枚腰带符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 403, 183, '2', '0', '1', 30, 1, 0, '2016-05-24 11:08:03.000', 'CN90053', '114.112.126.218', '2016-05-24 11:08:03.000', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (901, 6548, '[活动]除槽炼金符[10个]', '', '装有10个除槽炼金符的箱子。开启箱子，可获得*110个除槽炼金符*9。', 200, 38, '2', '0', '1', 0, 1, 0, '2016-06-07 11:09:42.937', 'CN90053', '106.38.71.30', '2016-06-07 11:09:42.937', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (902, 6549, '[活动]符文还原符[10个]', '', '装有10个符文还原符的箱子。开启箱子，可获得*110个符文还原符*9。', 100, 28, '2', '0', '1', 0, 1, 0, '2016-06-07 11:10:50.640', 'CN90053', '106.38.71.30', '2016-06-07 11:10:50.640', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (903, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 320, '2', '0', '1', 0, 5, 0, '2016-06-07 11:12:38.793', 'CN90053', '106.38.71.30', '2016-06-07 11:12:38.793', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (904, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-06-07 11:14:04.013', 'CN90053', '106.38.71.30', '2016-06-07 11:14:04.013', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (905, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-06-07 11:15:10.780', 'CN90053', '106.38.71.30', '2016-06-07 11:15:10.780', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (906, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-06-07 11:16:15.903', 'CN90053', '106.38.71.30', '2016-06-07 11:16:15.903', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (907, 6638, '[限时]战靴专用符文箱子（中级）[5个]', '', '装有*45*9个*1战靴专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-06-07 11:17:23.310', 'CN90053', '106.38.71.30', '2016-06-07 11:17:23.310', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (908, 6640, '[限时]披风专用符文箱子（中级）[5个]', '', '装有*45*9个*1披风专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 280, '2', '0', '1', 0, 5, 0, '2016-06-07 11:18:38.793', 'CN90053', '106.38.71.30', '2016-06-07 11:18:38.793', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (909, 6642, '[限时]辅助装备专用符文箱子（中级)[5个]', '', '装有*45*9个*1辅助装备专用符文*9的箱子。。开启有几率获得*6IV级符文*9。', 1900, 280, '2', '0', '1', 0, 5, 0, '2016-06-07 11:19:54.623', 'CN90053', '106.38.71.30', '2016-06-07 11:19:54.623', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (910, 7609, '幸运精华箱子高级 (30天)', '', '装有幸运精华的箱子。开启箱子时，可随机获得*1幸运精华Lv6、Lv7*9其中的一个。*430日*9。', 562, 392, '2', '0', '1', 30, 1, 0, '2016-06-28 10:54:54.293', 'CN90053', '106.38.71.30', '2016-06-28 10:54:54.293', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (911, 7633, '变身项链符文 V 综合箱子(30天)', '', '装有*1力量V、敏捷V、智力V、HPV、MPV*95种符文及3个符文还原符、2个除槽炼金符的箱子。有效期30日。', 1600, 801, '2', '0', '1', 30, 1, 0, '2016-06-28 10:55:37.543', 'CN90053', '106.38.71.30', '2016-06-28 10:55:37.543', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (912, 7816, '[进击Ⅱ][附魔Ⅱ]名妓黄真伊(Lv66)(30天)', '', '*1[近战]*9蕴含名妓黄真伊灵魂的项链,赋予超强近战攻击能力。拥有*266级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 4900, 2491, '2', '0', '1', 30, 1, 0, '2016-06-28 10:59:04.263', 'CN90053', '106.38.71.30', '2016-06-28 10:59:04.263', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (913, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)(30天)', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+3 敏捷+3 智力+3*9', 4200, 2181, '2', '0', '1', 30, 1, 0, '2016-06-28 10:59:10.420', 'CN90053', '106.38.71.30', '2016-06-28 10:59:10.420', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (914, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4700, 2381, '2', '0', '1', 30, 1, 0, '2016-06-28 11:29:20.530', 'CN90053', '106.38.71.30', '2016-06-28 11:29:20.530', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (915, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4481, 2581, '2', '0', '1', 30, 1, 0, '2016-06-28 11:02:09.327', 'CN90053', '106.38.71.30', '2016-06-28 11:02:09.327', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (916, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5200, 2691, '2', '0', '1', 30, 1, 0, '2016-06-28 11:03:32.013', 'CN90053', '106.38.71.30', '2016-06-28 11:03:32.013', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (917, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 6000, 2991, '1', '0', '1', 30, 1, 0, '2016-06-28 11:04:50.857', 'CN90053', '106.38.71.30', '2016-06-28 11:04:50.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (918, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5400, 2781, '2', '0', '1', 30, 1, 0, '2016-06-28 11:06:07.483', 'CN90053', '106.38.71.30', '2016-06-28 11:06:07.483', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (919, 7789, '[活动]征服德拉克箱子(30日)', '', '装有*12个德拉克神秘戒指*9、*11个封印的奇美拉卷轴、封印的格雷芬特卷轴*9的箱子。有效期限为*430天*9', 880, 240, '2', '0', '1', 30, 1, 0, '2016-07-21 12:57:10.543', 'CN90053', '106.38.71.30', '2016-07-21 12:57:10.543', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (920, 6660, '[活动]经验增幅道具宝箱（中级）', '', '装着*1经验增幅之咒*9的箱子。打开可随机获得*33倍~6倍*9其中的一个。，人品爆棚可获得*46小时*9的*36倍*9经验增幅之咒', 380, 178, '2', '0', '1', 0, 1, 0, '2016-07-21 13:02:27.780', 'CN90053', '106.38.71.30', '2016-07-21 13:02:27.780', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (921, 78, '银币掉落增幅之咒(2倍)', ' ', 'test', 20, 20, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:23.860', ' ', ' ');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (924, 2073, '银币掉落增幅之咒(2倍)', '', '使用后，狩猎获得的银币提高至2倍。', 150, 69, '2', '0', '1', 0, 1, 24, '2016-08-08 20:05:13.513', 'CN90053', '106.38.71.30', '2016-08-08 20:05:13.513', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (925, 6548, '[活动]除槽炼金符[10个]', '', '装有10个除槽炼金符的箱子。开启箱子，可获得*110个除槽炼金符*9。', 200, 38, '2', '0', '1', 0, 1, 0, '2016-08-09 10:57:26.623', 'CN90053', '106.38.71.30', '2016-08-09 10:57:26.623', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (926, 6549, '[活动]符文还原符[10个]', '', '装有10个符文还原符的箱子。开启箱子，可获得*110个符文还原符*9。', 100, 28, '2', '0', '1', 0, 1, 0, '2016-08-09 10:59:00.733', 'CN90053', '106.38.71.30', '2016-08-09 10:59:00.733', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (927, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 320, '2', '0', '1', 0, 1, 0, '2016-08-09 11:00:05.857', 'CN90053', '106.38.71.30', '2016-08-09 11:00:05.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (928, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 1, 0, '2016-08-09 11:01:08.730', 'CN90053', '106.38.71.30', '2016-08-09 11:01:08.730', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (929, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 1, 0, '2016-08-09 11:02:21.420', 'CN90053', '106.38.71.30', '2016-08-09 11:02:21.420', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (930, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 1, 0, '2016-08-09 11:03:20.390', 'CN90053', '106.38.71.30', '2016-08-09 11:03:20.390', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (931, 6638, '[限时]战靴专用符文箱子（中级）[5个]', '', '装有*45*9个*1战靴专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-08-09 11:04:39.187', 'CN90053', '106.38.71.30', '2016-08-09 11:04:39.187', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (932, 6640, '[限时]披风专用符文箱子（中级）[5个]', '', '装有*45*9个*1披风专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 280, '2', '0', '1', 0, 5, 0, '2016-08-09 11:09:02.577', 'CN90053', '106.38.71.30', '2016-08-09 11:09:02.577', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (933, 6642, '[限时]辅助装备专用符文箱子（中级)[5个]', '', '装有*45*9个*1辅助装备专用符文*9的箱子。。开启有几率获得*6IV级符文*9。', 1900, 280, '2', '0', '1', 0, 5, 0, '2016-08-09 11:10:19.483', 'CN90053', '106.38.71.30', '2016-08-09 11:10:19.483', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (934, 7618, '[回归礼包] 破坏者的精华箱子', '', '装有所有不稳定精华(破坏/守护/生命/熟练/灵魂)的箱子。  有效期限：14天。箱子*214天*9内不开启，将自动删除。', 880, 380, '2', '0', '1', 14, 1, 0, '2016-08-09 11:13:18.123', 'CN90053', '106.38.71.30', '2016-08-09 11:13:18.123', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (935, 7619, '[回归礼包] 骑士回归津贴', '', '为了回归的骑士而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 1780, '2', '0', '1', 7, 1, 0, '2016-08-09 11:14:57.857', 'CN90053', '106.38.71.30', '2016-08-09 11:14:57.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (936, 7620, '[回归礼包] 游侠回归津贴', '', '为了回归的游侠而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 1580, '2', '0', '1', 7, 1, 0, '2016-08-09 11:42:03.373', 'CN90053', '106.38.71.30', '2016-08-09 11:42:03.373', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (937, 7621, '[回归礼包] 刺客回归津贴', '', '为了回归的刺客而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 1780, '2', '0', '1', 7, 1, 0, '2016-08-09 11:43:03.357', 'CN90053', '106.38.71.30', '2016-08-09 11:43:03.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (938, 7622, '[回归礼包] 精灵回归津贴', '', '为了回归的精灵而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 1580, '2', '0', '1', 7, 1, 0, '2016-08-09 11:19:27.483', 'CN90053', '106.38.71.30', '2016-08-09 11:19:27.483', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (939, 7623, '[回归礼包] 召唤师回归津贴', '', '为了回归的召唤师而准备的津贴。开启时，可获得7件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 1780, '2', '0', '1', 7, 1, 0, '2016-08-09 11:21:18.327', 'CN90053', '106.38.71.30', '2016-08-09 11:21:18.327', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (940, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 320, '2', '0', '1', 0, 5, 0, '2016-08-09 11:51:52.000', 'CN90053', '106.38.71.30', '2016-08-09 11:51:52.000', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (941, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-08-09 11:52:45.450', 'CN90053', '106.38.71.30', '2016-08-09 11:52:45.450', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (942, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-08-09 11:53:49.937', 'CN90053', '106.38.71.30', '2016-08-09 11:53:49.937', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (943, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-08-09 11:54:46.670', 'CN90053', '106.38.71.30', '2016-08-09 11:54:46.670', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (945, 1602, '装备解封之咒', '', '使用后开启装备栏中5个被封印窗口,从而可佩戴两个戒指,项链,腰带和披风.双击鼠标左键后使用,有效时间30天.', 200, 69, '1', '0', '1', 0, 1, 720, '2016-08-17 13:58:57.780', 'CN90053', '106.38.71.30', '2016-08-17 13:58:57.780', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (946, 6211, '[附魔Ⅲ]女海盗船长(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的女海盗船长变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 1981, '2', '0', '1', 30, 1, 0, '2016-08-23 10:48:49.763', 'CN90053', '106.38.71.30', '2016-08-23 10:48:49.763', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (947, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 2381, '2', '0', '1', 30, 1, 0, '2016-08-23 10:50:00.093', 'CN90053', '106.38.71.30', '2016-08-23 10:50:00.093', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (948, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5081, 2581, '2', '0', '1', 30, 1, 0, '2016-08-23 10:51:23.513', 'CN90053', '106.38.71.30', '2016-08-23 10:51:23.513', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (949, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5281, 2691, '2', '0', '1', 30, 1, 0, '2016-08-23 10:52:32.687', 'CN90053', '106.38.71.30', '2016-08-23 10:52:32.687', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (950, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5881, 2991, '2', '0', '1', 30, 1, 0, '2016-08-23 10:55:21.043', 'CN90053', '106.38.71.30', '2016-08-23 10:55:21.043', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (951, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5481, 2781, '2', '0', '1', 30, 1, 0, '2016-08-23 10:56:37.013', 'CN90053', '106.38.71.30', '2016-08-23 10:56:37.013', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (952, 7810, '[进击Ⅱ][附魔Ⅱ]女佣变身(Lv81)(30天)', '', '*1[近战]*9蕴含女佣灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 7081, 3591, '2', '0', '1', 30, 1, 0, '2016-08-23 11:01:47.670', 'CN90053', '106.38.71.30', '2016-08-23 11:01:47.670', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (953, 7837, '[进击Ⅱ][附魔Ⅲ]暮色黑寡妇(Lv84)(30天)', '', '*1[远程]*9蕴含暮色黑寡妇灵魂的项链,赋予超强远程打击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 3791, '2', '0', '1', 30, 1, 0, '2016-08-23 11:03:23.593', 'CN90053', '106.38.71.30', '2016-08-23 11:03:23.593', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (954, 7840, '[进击Ⅱ][附魔Ⅲ]暴走机车女(Lv84)(30天)', '', '*1[近战]*9蕴含暴走机车女灵魂的项链,赋予超强近战攻击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 3791, '2', '0', '1', 30, 1, 0, '2016-08-23 11:04:41.687', 'CN90053', '106.38.71.30', '2016-08-23 11:04:41.687', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (955, 7843, '[进击Ⅲ][附魔Ⅲ]暗黑侍卫(Lv87)(30天)', '', '*1[近战]*9蕴含暗黑侍卫灵魂的项链,赋予超强近战攻击能力。拥有*287级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅲ*9额外属性。', 8281, 4191, '2', '0', '1', 30, 1, 0, '2016-08-23 11:07:14.390', 'CN90053', '106.38.71.30', '2016-08-23 11:07:14.390', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (956, 7643, '戒指符文 Ⅲ 攻击箱子(30天)', '', '装有*1所有伤害 III、所有命中率 III*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 404, 204, '2', '0', '1', 30, 1, 0, '2016-08-23 11:08:33.297', 'CN90053', '106.38.71.30', '2016-08-23 11:08:33.297', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (957, 7644, '戒指符文 Ⅲ 暴击箱子(30天)', '', '装有*1暴击率 III、暴击伤害 III*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 504, 284, '2', '0', '1', 30, 1, 0, '2016-08-23 11:09:43.357', 'CN90053', '106.38.71.30', '2016-08-23 11:09:43.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (958, 7638, '腰带符文 Ⅲ 攻击箱子(30天)', '', '装有*1所有攻击力 III、所有命中率 III*92枚腰带符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 403, 183, '2', '0', '1', 30, 1, 0, '2016-08-23 11:10:47.280', 'CN90053', '106.38.71.30', '2016-08-23 11:10:47.280', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (959, 2800, '英雄箱子', '', '英雄遗落的箱子。宝箱内含*4+0Lv4生命精华,+0Lv4灵魂精华,+0Lv4破坏精华,+0Lv4守护精华,+0Lv4熟练精华等物品,包括从普通,稀有,史诗到传说级别的极品精华,及10个基础精华. 打开宝箱,可随机获得其中一件道具 *9。', 2900, 1280, '2', '0', '1', 30, 1, 0, '2016-08-23 11:11:52.827', 'CN90053', '106.38.71.30', '2016-08-23 11:11:52.827', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (960, 1602, '装备解封之咒', '', '使用可解封装备栏。', 200, 69, '1', '0', '1', 0, 1, 720, '2016-08-23 11:26:31.810', 'CN90053', '106.38.71.30', '2016-08-23 11:26:31.810', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (961, 2800, '英雄箱子', '', '英雄遗落的箱子。宝箱内含*4+0Lv4生命精华,+0Lv4灵魂精华,+0Lv4破坏精华,+0Lv4守护精华,+0Lv4熟练精华等物品,包括从普通,稀有,史诗到传说级别的极品精华,及10个基础精华. 打开宝箱,可随机获得其中一件道具 *9。', 2900, 1280, '2', '0', '1', 0, 1, 0, '2016-08-23 11:36:05.153', 'CN90053', '106.38.71.30', '2016-08-23 11:36:05.153', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (963, 7534, '[附魔Ⅱ]巴德(Lv66)项链', '', '*1[近战]*9蕴含巴德的变身项链。拥有*266级*9变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 1881, 1881, '2', '0', '1', 30, 1, 0, '2016-09-27 12:54:19.373', 'CN90053', '106.38.71.30', '2016-09-27 12:54:19.373', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (964, 8007, '[附魔Ⅱ]东瀛少女(Lv69)(30天)', '', '*1[近战]*9蕴含东瀛少女灵魂的项链。拥有*269级*9变身能力值*3HP+300 MP+180 攻击速度和移动速度提升 负重+2000*9以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 4100, 2081, '2', '0', '1', 30, 1, 0, '2016-09-27 12:56:23.890', 'CN90053', '106.38.71.30', '2016-09-27 12:56:23.890', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (965, 7497, '海僧主教(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的海僧主教变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9', 4100, 2051, '2', '0', '1', 30, 1, 0, '2016-09-27 12:58:34.543', 'CN90053', '106.38.71.30', '2016-09-27 12:58:34.543', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (966, 7498, '风精灵(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的风精灵变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9', 4500, 2251, '2', '0', '1', 30, 1, 0, '2016-09-27 13:02:29.810', 'CN90053', '106.38.71.30', '2016-09-27 13:02:29.810', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (967, 7499, '蛮幽(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的蛮幽变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9', 4900, 2451, '2', '0', '1', 30, 1, 0, '2016-09-27 13:05:03.640', 'CN90053', '106.38.71.30', '2016-09-27 13:05:03.640', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (968, 7500, '哨兵(Lv81)(30天)', '', '*1[近战]*9赋予特殊能力的哨兵变身项链。拥有*281级*9的变身能力值*3HP+500 MP+300 攻击速度和移动速度提升 负重+3000*9', 5300, 2651, '2', '0', '1', 30, 1, 0, '2016-09-27 13:47:40.217', 'CN90053', '106.38.71.30', '2016-09-27 13:47:40.217', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (969, 6548, '[活动]除槽炼金符[10个]', '', '装有10个除槽炼金符的箱子。开启箱子，可获得*110个除槽炼金符*9。', 200, 38, '2', '0', '1', 0, 1, 0, '2016-09-27 13:09:23.763', 'CN90053', '106.38.71.30', '2016-09-27 13:09:23.763', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (970, 6549, '[活动]符文还原符[10个]', '', '装有10个符文还原符的箱子。开启箱子，可获得*110个符文还原符*9。', 100, 28, '2', '0', '1', 0, 1, 0, '2016-09-27 13:10:56.357', 'CN90053', '106.38.71.30', '2016-09-27 13:10:56.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (971, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 320, '2', '0', '1', 0, 5, 0, '2016-09-27 13:13:41.843', 'CN90053', '106.38.71.30', '2016-09-27 13:13:41.843', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (972, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-09-27 13:15:23.077', 'CN90053', '106.38.71.30', '2016-09-27 13:15:23.077', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (973, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-09-27 13:17:33.170', 'CN90053', '106.38.71.30', '2016-09-27 13:17:33.170', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (974, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-09-27 13:20:22.513', 'CN90053', '106.38.71.30', '2016-09-27 13:20:22.513', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (975, 6638, '[限时]战靴专用符文箱子（中级）[5个]', '', '装有*45*9个*1战靴专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-09-27 13:22:14.687', 'CN90053', '106.38.71.30', '2016-09-27 13:22:14.687', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (976, 6640, '[限时]披风专用符文箱子（中级）[5个]', '', '装有*45*9个*1披风专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 280, '2', '0', '1', 0, 5, 0, '2016-09-27 13:24:19.810', 'CN90053', '106.38.71.30', '2016-09-27 13:24:19.810', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (977, 6642, '[限时]辅助装备专用符文箱子（中级)[5个]', '', '装有*45*9个*1辅助装备专用符文*9的箱子。。开启有几率获得*6IV级符文*9。', 1900, 280, '2', '0', '1', 0, 5, 0, '2016-09-27 13:26:24.670', 'CN90053', '106.38.71.30', '2016-09-27 13:26:24.670', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (978, 7627, '装备守护神宝箱', '', '"装备守护神宝箱 装有8种装备掉落保护道具的宝箱。*11个神佑之粉（武器）(3日）、1个神佑之粉（武器）(5日）、1个神佑之粉(3日）、1个神佑之粉(5日）、1个[归属]玛尔斯的守护、
1个[可交易]玛尔斯的守护、3个[归属]玛尔斯的守护、3个[可交易]玛尔斯的守护*9中随机获得其中一个。"', 1760, 1588, '2', '0', '1', 0, 1, 0, '2016-09-27 13:28:52.843', 'CN90053', '106.38.71.30', '2016-09-27 13:28:52.843', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (979, 7627, '装备守护神宝箱', '', '"装备守护神宝箱 装有8种装备掉落保护道具的宝箱。*11个神佑之粉（武器）(3日）、1个神佑之粉（武器）(5日）、1个神佑之粉(3日）、1个神佑之粉(5日）、1个[归属]玛尔斯的守护、
1个[可交易]玛尔斯的守护、3个[归属]玛尔斯的守护、3个[可交易]玛尔斯的守护*9中随机获得其中一个。"', 1760, 1588, '2', '0', '1', 0, 1, 0, '2016-10-11 17:45:41.467', 'CN90053', '106.38.71.30', '2016-10-11 17:45:41.467', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (980, 7789, '[活动]征服德拉克箱子(30日)', '', '装有*12个德拉克神秘戒指*9、*11个封印的奇美拉卷轴、封印的格雷芬特卷轴*9的箱子。有效期限为*430天*9', 880, 240, '2', '0', '1', 30, 1, 0, '2016-10-20 11:12:00.733', 'CN90053', '106.38.71.30', '2016-10-20 11:12:00.733', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (981, 6660, '[活动]经验增幅道具宝箱（中级）', '', '装着*1经验增幅之咒*9的箱子。打开可随机获得*33倍~6倍*9其中的一个。，人品爆棚可获得*46小时*9的*36倍*9经验增幅之咒', 380, 178, '2', '0', '1', 0, 1, 0, '2016-10-20 11:13:57.140', 'CN90053', '106.38.71.30', '2016-10-20 11:13:57.140', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (982, 2073, '银币掉落增幅之咒(2倍)(1天)', '', '双及使用后，狩猎获得银币量为原由银币的*22倍*9，快捷键"T"可确认使用状态', 280, 68, '2', '0', '1', 0, 1, 24, '2016-10-20 11:15:35.890', 'CN90053', '106.38.71.30', '2016-10-20 11:15:35.890', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (983, 7609, '[活动]幸运精华箱子高级(30天)', '', '装有幸运精华的箱子。开启箱子时，可随机获得*1幸运精华Lv6、Lv7*9其中的一个。*430日*9。', 562, 392, '2', '0', '1', 30, 1, 0, '2016-10-20 11:18:10.200', 'CN90053', '106.38.71.30', '2016-10-20 11:18:10.200', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (984, 7816, '[进击Ⅱ][附魔Ⅱ]名妓黄真伊(Lv66)(30天)', '', '*1[近战]*9蕴含名妓黄真伊灵魂的项链,赋予超强近战攻击能力。拥有*266级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 5900, 2491, '2', '0', '1', 30, 1, 0, '2016-10-27 11:05:00.827', 'CN90053', '106.38.71.30', '2016-10-27 11:05:00.827', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (985, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)(30天)', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+3 敏捷+3 智力+3*9', 4300, 2181, '2', '0', '1', 30, 1, 0, '2016-10-27 11:09:18.670', 'CN90053', '106.38.71.30', '2016-10-27 11:09:18.670', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (986, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4700, 2381, '2', '0', '1', 30, 1, 0, '2016-10-27 11:12:49.747', 'CN90053', '106.38.71.30', '2016-10-27 11:12:49.747', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (987, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5100, 2581, '2', '0', '1', 30, 1, 0, '2016-10-27 11:19:27.543', 'CN90053', '106.38.71.30', '2016-10-27 11:19:27.543', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (988, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5300, 2691, '2', '0', '1', 30, 1, 0, '2016-10-27 11:24:17.500', 'CN90053', '106.38.71.30', '2016-10-27 11:24:17.500', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (989, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 6000, 2991, '2', '0', '1', 30, 1, 0, '2016-10-27 11:26:12.140', 'CN90053', '106.38.71.30', '2016-10-27 11:26:12.140', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (990, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5500, 2781, '2', '0', '1', 30, 1, 0, '2016-10-27 11:30:02.593', 'CN90053', '106.38.71.30', '2016-10-27 11:30:02.593', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (991, 2800, '英雄箱子', '', '英雄遗落的箱子。宝箱内含*4+0Lv4生命精华,+0Lv4灵魂精华,+0Lv4破坏精华,+0Lv4守护精华,+0Lv4熟练精华等物品,包括从普通,稀有,史诗到传说级别的极品精华,及10个基础精华. 打开宝箱,可随机获得其中一件道具。*9', 2900, 1380, '2', '0', '1', 0, 1, 0, '2016-11-17 10:56:45.810', 'CN90053', '106.38.71.30', '2016-11-17 10:56:45.810', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (992, 7627, '装备守护神宝箱', '', '"装备守护神宝箱 装有8种装备掉落保护道具的宝箱。*11个神佑之粉（武器）(3日）、1个神佑之粉（武器）(5日）、1个神佑之粉(3日）、1个神佑之粉(5日）、1个[归属]玛尔斯的守护、
1个[可交易]玛尔斯的守护、3个[归属]玛尔斯的守护、3个[可交易]玛尔斯的守护*9中随机获得其中一个。', 1760, 1588, '1', '0', '1', 0, 1, 0, '2016-11-17 10:57:58.950', 'CN90053', '106.38.71.30', '2016-11-17 10:57:58.950', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (993, 6211, '[附魔Ⅲ]女海盗船长(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的女海盗船长变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 1981, '2', '0', '1', 30, 1, 0, '2016-11-24 10:58:47.280', 'CN90053', '106.38.71.30', '2016-11-24 10:58:47.280', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (994, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 2381, '2', '0', '1', 30, 1, 0, '2016-11-24 11:00:30.030', 'CN90053', '106.38.71.30', '2016-11-24 11:00:30.030', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (995, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5081, 2581, '2', '0', '1', 30, 1, 0, '2016-11-24 11:01:35.827', 'CN90053', '106.38.71.30', '2016-11-24 11:01:35.827', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (996, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5281, 2691, '2', '0', '1', 30, 1, 0, '2016-11-24 11:04:02.060', 'CN90053', '106.38.71.30', '2016-11-24 11:04:02.060', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (997, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5881, 2991, '2', '0', '1', 30, 1, 0, '2016-11-24 11:05:31.060', 'CN90053', '106.38.71.30', '2016-11-24 11:05:31.060', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (998, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5481, 2781, '2', '0', '1', 30, 1, 0, '2016-11-24 11:06:48.437', 'CN90053', '106.38.71.30', '2016-11-24 11:06:48.437', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (999, 7810, '[进击Ⅱ][附魔Ⅱ]女佣变身(Lv81)(30天)', '', '*1[近战]*9蕴含女佣灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 7081, 3581, '2', '0', '1', 30, 1, 0, '2016-11-24 11:08:12.107', 'CN90053', '106.38.71.30', '2016-11-24 11:08:12.107', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1000, 7828, '[进击Ⅱ][附魔Ⅲ]粉嫩护士(Lv81)(30天)', '', '*1[近战]*9蕴含粉嫩护士灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值 负重+2250 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7300, 3691, '2', '0', '1', 30, 1, 0, '2016-11-24 11:09:40.000', 'CN90053', '106.38.71.30', '2016-11-24 11:09:40.000', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1001, 7837, '[附魔Ⅲ]暮色黑寡妇(Lv84)项链', '', '*1[远程]*9蕴含暮色黑寡妇灵魂的项链,赋予超强远程打击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 3791, '2', '0', '1', 30, 1, 0, '2016-11-24 11:11:37.920', 'CN90053', '106.38.71.30', '2016-11-24 11:11:37.920', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1002, 7840, '[进击Ⅱ][附魔Ⅲ]暴走机车女(Lv84)(30天)', '', '*1[近战]*9蕴含暴走机车女灵魂的项链,赋予超强近战攻击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 3791, '2', '0', '1', 30, 1, 0, '2016-11-24 11:12:57.497', 'CN90053', '106.38.71.30', '2016-11-24 11:12:57.497', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1003, 7843, '[进击Ⅲ][附魔Ⅲ]暗黑侍卫(Lv87)(30天)', '', '*1[近战]*9蕴含暗黑侍卫灵魂的项链,赋予超强近战攻击能力。拥有*287级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅲ*9额外属性。', 8281, 4191, '2', '0', '1', 30, 1, 0, '2016-11-24 11:14:21.593', 'CN90053', '106.38.71.30', '2016-11-24 11:14:21.593', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1007, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)项链', '', '123456', 4300, 2681, '2', '0', '1', 30, 1, 0, '2016-12-06 14:52:04.373', 'CN90053', '106.38.71.30', '2016-12-06 14:52:04.373', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1008, 6211, '[附魔Ⅲ]女海盗船长(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的女海盗船长变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 1981, '2', '0', '1', 30, 1, 0, '2016-12-06 23:04:57.293', 'CN90053', '106.38.71.30', '2016-12-06 23:04:57.293', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1009, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 2381, '2', '0', '1', 30, 1, 0, '2016-12-06 23:06:14.373', 'CN90053', '106.38.71.30', '2016-12-06 23:06:14.373', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1010, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5081, 2581, '2', '0', '1', 30, 1, 0, '2016-12-06 23:07:51.797', 'CN90053', '106.38.71.30', '2016-12-06 23:07:51.797', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1011, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5281, 2691, '2', '0', '1', 30, 1, 0, '2016-12-06 23:10:10.047', 'CN90053', '106.38.71.30', '2016-12-06 23:10:10.047', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1012, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5881, 2991, '2', '0', '1', 30, 1, 0, '2016-12-06 23:19:12.170', 'CN90053', '106.38.71.30', '2016-12-06 23:19:12.170', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1013, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5481, 2781, '2', '0', '1', 30, 1, 0, '2016-12-06 23:20:54.357', 'CN90053', '106.38.71.30', '2016-12-06 23:20:54.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1014, 7810, '[进击Ⅱ][附魔Ⅱ]女佣变身(Lv81)(30天)', '', '*1[近战]*9蕴含女佣灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 7081, 3591, '2', '0', '1', 30, 1, 0, '2016-12-06 23:22:02.170', 'CN90053', '106.38.71.30', '2016-12-06 23:22:02.170', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1015, 7828, '[进击Ⅱ][附魔Ⅲ]粉嫩护士(Lv81)(30天)', '', '*1[近战]*9蕴含粉嫩护士灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值 负重+2250 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7381, 3691, '2', '0', '1', 30, 1, 0, '2016-12-06 23:25:15.857', 'CN90053', '106.38.71.30', '2016-12-06 23:25:15.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1016, 7837, '[进击Ⅱ][附魔Ⅲ]暮色黑寡妇(Lv84)(30天)', '', '*1[远程]*9蕴含暮色黑寡妇灵魂的项链,赋予超强远程打击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 3791, '2', '0', '1', 30, 1, 0, '2016-12-06 23:29:43.950', 'CN90053', '106.38.71.30', '2016-12-06 23:29:43.950', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1017, 7840, '[进击Ⅱ][附魔Ⅲ]暴走机车女(Lv84)(30天)', '', '*1[近战]*9蕴含暴走机车女灵魂的项链,赋予超强近战攻击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 3791, '2', '0', '1', 30, 1, 0, '2016-12-06 23:30:51.450', 'CN90053', '106.38.71.30', '2016-12-06 23:30:51.450', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1018, 7843, '[进击Ⅲ][附魔Ⅲ]暗黑侍卫(Lv87)(30天)', '', '*1[近战]*9蕴含暗黑侍卫灵魂的项链,赋予超强近战攻击能力。拥有*287级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅲ*9额外属性。', 8281, 4191, '2', '0', '1', 30, 1, 0, '2016-12-06 23:32:04.030', 'CN90053', '106.38.71.30', '2016-12-06 23:32:04.030', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1019, 7618, '[回归礼包] 破坏者的精华箱子', '', '装有所有不稳定精华(破坏/守护/生命/熟练/灵魂)的箱子。  有效期限：14天。箱子*214天*9内不开启，将自动删除。', 880, 380, '2', '0', '1', 14, 1, 0, '2016-12-06 23:33:15.500', 'CN90053', '106.38.71.30', '2016-12-06 23:33:15.500', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1020, 7619, '[回归礼包] 骑士回归津贴', '', '为了回归的骑士而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 1780, '2', '0', '1', 7, 1, 0, '2016-12-06 23:34:15.780', 'CN90053', '106.38.71.30', '2016-12-06 23:34:15.780', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1021, 7620, '[回归礼包] 游侠回归津贴', '', '为了回归的游侠而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 1580, '2', '0', '1', 7, 1, 0, '2016-12-06 23:35:09.200', 'CN90053', '106.38.71.30', '2016-12-06 23:35:09.200', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1022, 7621, '[回归礼包] 刺客回归津贴', '', '为了回归的刺客而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 1780, '2', '0', '1', 7, 1, 0, '2016-12-06 23:36:16.797', 'CN90053', '106.38.71.30', '2016-12-06 23:36:16.797', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1023, 7622, '[回归礼包] 精灵回归津贴', '', '为了回归的精灵而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 1580, '2', '0', '1', 7, 1, 0, '2016-12-06 23:37:28.047', 'CN90053', '106.38.71.30', '2016-12-06 23:37:28.047', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1024, 7623, '[回归礼包] 召唤师回归津贴', '', '为了回归的召唤师而准备的津贴。开启时，可获得7件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 1780, '2', '0', '1', 7, 1, 0, '2016-12-06 23:38:22.950', 'CN90053', '106.38.71.30', '2016-12-06 23:38:22.950', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1025, 1602, '装备解封之咒(5个槽）', '', '使用后*2开启装备栏中5个被封印窗口*9，从而可佩戴*6两个戒指，项链，腰带和披风*9。', 200, 1, '2', '0', '1', 0, 1, 720, '2016-12-13 13:42:57.700', 'CN90053', '106.38.71.30', '2016-12-13 13:42:57.700', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1026, 6211, '[附魔Ⅲ]女海盗船长(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的女海盗船长变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 1981, '2', '0', '1', 30, 1, 0, '2016-12-20 11:01:14.450', 'CN90053', '106.38.71.30', '2016-12-20 11:01:14.450', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1027, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 2381, '1', '0', '1', 30, 1, 0, '2016-12-20 11:04:12.077', 'CN90053', '106.38.71.30', '2016-12-20 11:04:12.077', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1028, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5081, 2581, '2', '0', '1', 30, 1, 0, '2016-12-20 11:07:54.513', 'CN90053', '106.38.71.30', '2016-12-20 11:07:54.513', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1029, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5281, 2691, '2', '0', '1', 30, 1, 0, '2016-12-20 11:11:49.640', 'CN90053', '106.38.71.30', '2016-12-20 11:11:49.640', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1030, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5881, 2991, '2', '0', '1', 30, 1, 0, '2016-12-20 11:14:11.187', 'CN90053', '106.38.71.30', '2016-12-20 11:14:11.187', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1031, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5481, 2781, '2', '0', '1', 30, 1, 0, '2016-12-20 11:17:49.717', 'CN90053', '106.38.71.30', '2016-12-20 11:17:49.717', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1032, 7810, '[进击Ⅱ][附魔Ⅱ]女佣变身(Lv81)(30天)', '', '*1[近战]*9蕴含女佣灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 7081, 3591, '2', '0', '1', 30, 1, 0, '2016-12-20 11:20:04.730', 'CN90053', '106.38.71.30', '2016-12-20 11:20:04.730', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1033, 7828, '[进击Ⅱ][附魔Ⅲ]粉嫩护士(Lv81)(30天)', '', '*1[近战]*9蕴含粉嫩护士灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值 负重+2250 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7300, 3691, '2', '0', '1', 30, 1, 0, '2016-12-20 11:23:33.060', 'CN90053', '106.38.71.30', '2016-12-20 11:23:33.060', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1034, 7837, '[进击Ⅱ][附魔Ⅲ]暮色黑寡妇(Lv84)(30天)', '', '*1[远程]*9蕴含暮色黑寡妇灵魂的项链,赋予超强远程打击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 3791, '2', '0', '1', 30, 1, 0, '2016-12-20 11:27:08.343', 'CN90053', '106.38.71.30', '2016-12-20 11:27:08.343', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1035, 7840, '[进击Ⅱ][附魔Ⅲ]暴走机车女(Lv84)(30天)', '', '*1[近战]*9蕴含暴走机车女灵魂的项链,赋予超强近战攻击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 3791, '2', '0', '1', 30, 1, 0, '2016-12-20 11:30:48.170', 'CN90053', '106.38.71.30', '2016-12-20 11:30:48.170', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1036, 7843, '[进击Ⅲ][附魔Ⅲ]暗黑侍卫(Lv87)(30天)', '', '*1[近战]*9蕴含暗黑侍卫灵魂的项链,赋予超强近战攻击能力。拥有*287级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅲ*9额外属性。', 8281, 4191, '2', '0', '1', 30, 1, 0, '2016-12-20 11:35:30.827', 'CN90053', '106.38.71.30', '2016-12-20 11:35:30.827', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1037, 6548, '[活动]除槽炼金符[10个]', '', '装有10个除槽炼金符的箱子。开启箱子，可获得*110个除槽炼金符*9。', 200, 38, '2', '0', '1', 0, 1, 0, '2016-12-20 11:40:06.077', 'CN90053', '106.38.71.30', '2016-12-20 11:40:06.077', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1038, 6549, '[活动]符文还原符[10个]', '', '装有10个符文还原符的箱子。开启箱子，可获得*110个符文还原符*9。', 100, 28, '2', '0', '1', 0, 1, 0, '2016-12-20 11:42:58.000', 'CN90053', '106.38.71.30', '2016-12-20 11:42:58.000', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1039, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 320, '2', '0', '1', 0, 5, 0, '2016-12-20 11:44:31.030', 'CN90053', '106.38.71.30', '2016-12-20 11:44:31.030', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1040, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-12-20 11:46:57.310', 'CN90053', '106.38.71.30', '2016-12-20 11:46:57.310', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1041, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-12-20 11:48:47.857', 'CN90053', '106.38.71.30', '2016-12-20 11:48:47.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1042, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-12-20 11:50:01.357', 'CN90053', '106.38.71.30', '2016-12-20 11:50:01.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1043, 6638, '[限时]战靴专用符文箱子（中级）[5个]', '', '装有*45*9个*1战靴专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 300, '2', '0', '1', 0, 5, 0, '2016-12-20 11:53:00.280', 'CN90053', '106.38.71.30', '2016-12-20 11:53:00.280', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1044, 6640, '[限时]披风专用符文箱子（中级）[5个]', '', '装有*45*9个*1披风专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 280, '2', '0', '1', 0, 5, 0, '2016-12-20 11:54:36.577', 'CN90053', '106.38.71.30', '2016-12-20 11:54:36.577', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1045, 6642, '[限时]辅助装备专用符文箱子（中级)[5个]', '', '装有*45*9个*1辅助装备专用符文*9的箱子。。开启有几率获得*6IV级符文*9。', 1900, 280, '2', '0', '1', 0, 5, 0, '2016-12-20 11:56:50.140', 'CN90053', '106.38.71.30', '2016-12-20 11:56:50.140', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1048, 1602, 'test装备解封之咒', '', 'test', 1230, 123, '2', '0', '1', 0, 1, 720, '2016-12-30 16:45:07.333', 'CN90053', '10.40.11.102', '2016-12-30 16:45:07.333', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1049, 5556, '天然芒果汁', '', '', 123, 12, '2', '0', '1', 1, 1, 1, '2016-12-30 17:03:05.270', 'CN90053', '10.40.11.102', '2016-12-30 17:03:05.270', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1050, 241, '属性精华卡片', '', '', 125, 123, '2', '0', '1', 1, 1, 1, '2016-12-30 17:14:56.980', 'CN90053', '10.40.11.102', '2016-12-30 17:42:31.280', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1051, 5556, '天然芒果汁', '', '', 124, 123, '2', '0', '1', 1, 1, 1, '2016-12-30 17:25:07.010', 'CN90053', '10.40.11.102', '2016-12-30 17:25:07.010', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1052, 5556, '天然芒果汁', '', '', 124, 123, '2', '0', '1', 1, 1, 1, '2016-12-30 17:44:21.110', 'CN90053', '10.40.11.102', '2016-12-30 17:44:21.110', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1053, 5571, '天然芒果糖', '', '', 1111, 123, '2', '0', '1', 1, 1, 1, '2016-12-30 17:50:23.530', 'CN90053', '10.40.11.102', '2016-12-30 17:50:23.530', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1054, 1594, 'test地下城入场券', '', 'test', 1230, 123, '2', '0', '1', 0, 1, 720, '2016-12-30 17:53:35.040', 'CN90053', '10.40.11.102', '2016-12-30 17:53:35.040', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1055, 7948, '翡翠钥匙', '', '可用于开启黄金宝箱', 150, 150, '1', '0', '1', 0, 1, 0, '2018-07-13 18:42:42.523', 'CN12654', '210.72.232.131', '2018-07-13 18:42:42.523', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1056, 8456, '至尊神秘箱子', '', '开启后可获得英雄箱子*1，基础精华*50，银币掉率增幅之咒（2倍）（1天），强化精华*2', 1588, 1588, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1057, 7628, '强化守护神宝箱', '', '装有7种强化守护道具和神圣强化卷轴的宝箱，开启后可随机获得其中一种', 560, 560, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1058, 6718, '武器强化宝箱', '', '装有神圣武器强化卷轴，发光的武器强化卷轴和武器强化卷轴（祝福）的箱子，开启后可随机获得其中一种', 499, 499, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1059, 6719, '防具强化宝箱', '', '装有神圣防具强化卷轴，发光的防具强化卷轴和防具强化卷轴（祝福）的箱子，开启后可随机获得其中一种', 399, 399, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1060, 3368, '混沌点券充值卡 21000P', '', '充值混沌点数的物品，可以在各个城镇的混沌积分商人处购买战利品及其他重要道具', 995, 995, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1061, 6204, '[附魔Ⅱ]巴德(Lv63)项链', '', '[近战]赋予特殊能力的巴德变身项链。拥有63级的变身能力值额外大幅提升附魔能力以及力量+2敏捷+2智力+2', 1571, 1571, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1062, 6208, '[附魔I]地狱领主(Lv63)项链', '', '[近战]蕴含地狱领主灵魂的项链。拥有63级变身能力值力量 +1敏捷 +1智力 +1', 1461, 1461, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1063, 6215, '[附魔I]角斗士(Lv66)项链', '', '*1[近战]*9蕴含角斗士的变身项链。赋予66级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+1 敏捷+1 智力+1', 1661, 1661, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1064, 6229, '[附魔Ⅱ]暗黑法师(Lv66)项链', '', '[远程]蕴含暗黑法师灵魂的项链。拥有66级变身能力值力量 +2敏捷 +2智力 +2', 1871, 1871, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1065, 7526, '[附魔Ⅱ]忍者少女(Lv63)项链', '', '*1[近战]*9蕴含忍者少女的变身项链。赋予63级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+2 敏捷+2 智力+2', 1571, 1571, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1066, 7527, '[附魔I]火花(Lv63)项链', '', '*1[近战]*9蕴含火花的变身项链。赋予63级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+1 敏捷+1 智力+1', 1461, 1461, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1067, 7528, '[附魔Ⅲ]炼金术士(Lv63)项链', '', '*1[近战]*9蕴含炼金术士的变身项链。赋予63级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 1881, 1881, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1068, 7529, '[附魔Ⅲ]阿斯摩太(Lv63)项链', '', '*1[近战]*9蕴含阿斯摩太的变身项链。赋予63级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 1781, 1781, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1069, 7530, '[附魔I]阿努比斯(Lv63)项链', '', '*1[近战]*9蕴含阿努比斯的变身项链。赋予63级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+1 敏捷+1 智力+1', 1461, 1461, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1070, 7535, '[附魔I]阿修罗(Lv66)项链', '', '*1[近战]*9蕴含阿修罗的变身项链。赋予66级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+1 敏捷+1 智力+1', 1661, 1661, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1071, 7536, '[附魔Ⅲ]月光精灵法师(Lv66)项链', '', '*1[近战]*9蕴含月光精灵法师的变身项链。赋予66级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 1881, 1881, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1072, 7541, '[附魔Ⅲ]女海盗船长(Lv69)项链', '', '*1[近战]*9蕴含女海盗船长的变身项链。赋予69级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 2081, 2081, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1073, 7543, '[附魔I]武士(Lv69)项链', '', '*1[近战]*9蕴含武士的变身项链。赋予69级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+1 敏捷+1 智力+1', 1861, 1861, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1074, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)项链', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予69级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 2081, 2081, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1075, 7545, '[附魔Ⅲ]恶魔追猎者(Lv69)项链', '', '*1[近战]*9蕴含恶魔追猎者的变身项链。赋予69级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 2181, 2181, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1076, 7547, '[附魔Ⅲ]大力神(Lv72)项链', '', '[近战]蕴含大力神的变身项链。赋予72级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 2381, 2381, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1077, 8007, '[附魔Ⅱ]东瀛少女(Lv69)项链', '', '[近战]蕴含东瀛少女灵魂的项链,赋予超强近战攻击能力。拥有69级变身能力值 以及附魔进阶属性力量+2 敏捷+2 智力+2', 1971, 1971, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1078, 8008, '[进击][附魔I]修炼少女(Lv75)项链', '', '[近战]蕴含修炼少女灵魂的项链,赋予超强近战攻击能力。拥有75级变身能力值 以及附魔进阶属性力量+1 敏捷+1 智力+1，同时拥有进击额外属性。', 2581, 2581, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1079, 8537, '[附魔III] 阿尔忒弥斯变身项链(CN)', '', '[近战]蕴含阿尔忒弥的变身项链。赋予90级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 4591, 4591, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1080, 7549, '[附魔Ⅲ]恶魔小丑(Lv75)', '', '[近战]蕴含恶魔小丑的变身项链。赋予75级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 2691, 2691, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1081, 7550, '[附魔Ⅲ]狂战士(Lv78)', '', '[近战]蕴含狂战士的变身项链。赋予78级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 2881, 2881, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1082, 7551, '[附魔Ⅲ]女神咖莉(Lv81)', '', '[近战]蕴含女神咖莉的变身项链。赋予81级变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性力量+3 敏捷+3 智力+3', 3181, 3181, '2', '0', '1', 30, 1, 0, '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102', '2019-08-15 16:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1083, 6631, '[限时]武器专用符文箱子（高级）[1个]', '', '装有武器专用符文的箱子,开启后有几率获得V级符文', 120, 120, '1', '0', '1', 0, 1, 0, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1084, 6633, '[限时]头盔专用符文箱子（高级）[1个]', '', '装有头盔专用符文的箱子,开启后有几率获得V级符文', 100, 100, '1', '0', '1', 0, 1, 0, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1085, 6635, '[限时]盔甲专用符文箱子（高级）[1个]', '', '装有盔甲专用符文的箱子,开启后有几率获得V级符文', 120, 120, '1', '0', '1', 0, 1, 0, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1086, 6637, '[限时]护手专用符文箱子（高级）[1个]', '', '装有护手专用符文的箱子,开启后有几率获得V级符文', 100, 100, '1', '0', '1', 0, 1, 0, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1087, 6639, '[限时]战靴专用符文箱子（高级）[1个]', '', '装有战靴专用符文的箱子,开启后有几率获得V级符文', 100, 100, '1', '0', '1', 0, 1, 0, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1088, 6641, '[限时]披风专用符文箱子（高级）[1个]', '', '装有披风专用符文的箱子,开启后有几率获得V级符文', 120, 120, '1', '0', '1', 0, 1, 0, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (1089, 6643, '[限时]辅助装备专用符文箱子（高级）[1个]', '', '装有辅助装备专用符文的箱子,开启后有几率获得V级符文', 100, 100, '1', '0', '1', 0, 1, 0, '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102', '2020-01-10 14:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10003, 1588, '破封之咒', '', '用于解除被封印物品的封印状态,根据被封印物品强化程度的高低,需要不同数量的破封之咒.

双击破封之咒后,左键点击要解除封印的物品,即可解封.', 30, 15, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:23.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10004, 1589, '浓缩治疗药水', '', '能够大量恢复自身生命值的药剂,*1但只能在各大陆的村镇中才可实现购买*9.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 2, 1, '1', '0', '0', 0, 10, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:33.170', 'CN13117', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10005, 1590, '传送记忆列表(A)', '', '在20天内,*1可额外增加传送之咒中10个记录点*9.
双击鼠标左键使用.使用后可按*1T键*9查询剩余时间.', 30, 15, '1', '0', '0', 0, 1, 480, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-18 13:57:41.293', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10006, 1591, '传送记忆列表(B)', '', '在20天内,*1可额外增加传送之咒中20个记录点*9.
双击鼠标左键使用.使用后可按*1T键*9查询剩余时间.', 90, 37, '1', '0', '0', 0, 1, 480, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.920', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10008, 1593, '公会之力', '', '使用这个的话，在所属公会状态下能受到技能树的优惠', 200, 100, '1', '0', '0', 0, 1, 2, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:41.653', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10009, 1594, '地下城入场券(12小时)', '', '双击左键后使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期12小时.
使用后可按*1T键*9查询剩余时间.', 35, 17, '1', '0', '0', 0, 1, 12, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.763', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10010, 1595, '哈德斯守护(50%)', '', '当角色最后死亡后,可50%恢复最后一次死亡时所失去的经验值.
死亡后,双击鼠标左键使用.', 24, 10, '1', '0', '0', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.810', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10011, 1596, '哈德斯守护(100%)', '', '当角色最后死亡后,可*1100%恢复*9最后一次死亡时所失去的经验值.
死亡后,*1双击鼠标左键使用*9.', 60, 25, '1', '0', '0', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:37.827', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10012, 1597, '戒指解封之咒(A)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 40, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10013, 1598, '戒指解封之咒(B)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 40, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10014, 1599, '项链解封之咒', '', '使用后开启装备栏中的项链栏,从而可佩戴项链.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 40, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.203', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10015, 1600, '腰带解封之咒', '', '使用后开启装备栏中的腰带栏,从而可佩戴腰带.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 40, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.157', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10016, 1601, '披风解封之咒', '', '使用后开启装备栏中的披风栏,从而可佩戴披风.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 80, 40, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.110', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10017, 1617, '拜师申请券', '', '请您到各大陆的村镇中找*1<师徒>管理员*9协助您使用拜师申请券,使用后可恢复1次拜师申请.', 100, 50, '1', '0', '0', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.687', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10018, 1618, '德拉克召唤书(30天)', '', '使用后可召唤超级德拉克,同时可随时收回.

召唤出的德拉克可在地下城内骑乘.
双击鼠标左键使用,*1购买后开始计时间*9,有效时间30天.', 580, 290, '1', '0', '0', 30, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.623', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10019, 1619, '传送之咒', '', '聊天窗口中输入*1／记忆(空格)XXX*9可记忆角色当前地点(10个,吉内亚岛除外).

直接输入*1／记忆*9可查询已保存的记录点或删除记录点,但无法进行传送.

双击本道具选择记录点后可进行传送,每次传送*1消耗1个传送之咒*9.', 5, 1, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:26.857', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10020, 1620, '变身之咒', '', '双击使用可变化成怪物形象,同时可拥有怪物部分攻击属性.随着等级提升可选择变化的怪物逐渐增多.*1变身持续时间20-30分钟*9.

双击变身之咒但不变身,可令使用者恢复原貌.恢复原貌也将消耗1个变身之咒.', 8, 4, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:21.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10021, 1623, '米泰欧斯的守护', '', '强化装备失败时,行囊内如有米泰欧斯的守护,*1可一定几率的自动保护强化失败的装备*9.

无论保护成功或失败,*1米泰欧斯的守护都会消失*9.', 80, 40, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:22.703', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10022, 1624, '玛尔斯的守护', '', '当角色死亡时，玛尔斯的守护会随机自动保护*11件物品不掉落*9.', 0, 0, '1', '0', '2', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:25.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10023, 1625, '公会仓库', '', '公会仓激活后,会创建全体公会成员共同使用的公会仓库。有效时间30天.
公会成员均可购买并激活公会公会仓库.
只有公会长有设置密码的权利.', 150, 75, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:39.030', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10024, 1626, '高级公会仓库', '', '除公会仓库功能外,公会长还可赋予指定会员权限,让他可使用仓库.
有效时间30天.
只有公会长有设置密码的权利.', 250, 125, '1', '0', '0', 0, 1, 720, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:38.967', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10027, 409, '银币', '', '银铸造的钱币.', 0, 0, '1', '0', '0', 0, 50000, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:41.483', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10028, 1619, '移动之咒', '', '可以移动到你所想移动的地方的魔法之咒。', 0, 0, '1', '0', '0', 0, 50, 0, '2008-01-09 12:59:08.000', NULL, NULL, '2014-08-12 12:06:41.530', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10029, 1620, '变换之咒', '', '可以变成你所喜欢的怪物的魔法之咒。', 0, 0, '1', '0', '0', 0, 50, 0, '2008-01-09 12:59:08.000', NULL, NULL, '2014-08-12 12:06:41.577', '', '');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10030, 50006, '1111111', '', '神秘礼包包含：
英雄箱子X1、
银币掉率增幅之咒(1.5倍)X1、基础精华X40', 990, 495, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.467', 'CN90053', '61.152.124.194');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10031, 50007, 'NO2不能用的包代码', '', '66级吉伽近战变身礼包：吉伽梅西战士(30日)月光精灵战士V(7日)   月光复仇者（加强）30日', 2880, 1440, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:31.200', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10033, 50009, '[秒杀]精华感恩礼包', '', '[秒杀]精华感恩礼包：含
*4含基础精华x50,
强化精华x5
德拉克神秘戒指x1*9
[秒杀]精华感恩礼包额外奖励：
*4祝福强化精华x1*9

秒杀感恩时间：
*111月26日16:00-20:00
11月28日19:00-21:00
11月30日14:00-18:00*9', 1410, 640, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:30.670', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10036, 50012, '[新手学园专属]礼包', '', '[新手学园专属]礼包：礼包含装备解封之咒(30日)，旅行者之书(30日)X1，神秘德拉克戒指(30日)X1， 增幅之咒X1，辨识之书(30日)X1
温馨提示：装备解封之咒同时解封5个装备槽，之前使用当中的解封之咒时间也被注销，已新开启的时间为准。', 1030, 130, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:30.060', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10039, 50015, 'NO3不能用的包代码', '', '萝莉近战变身礼包：    修炼少女V(30日)      公主变身（加强）(30日)月光精灵战士V(7日) ', 2920, 1460, '4', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:31.153', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10041, 50017, '2', '', '*1贤者智慧LV3（30日）*9 1个 
*4贤者智慧LV3 攻击速度和移动速度效果最强*9
变身强化精华LV3（30日）1个
幸运精华LV3（30日）1个', 960, 440, '2', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:29.607', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10043, 50019, '不可使用1', '', '内含芭芭莉安,暗黑法师,赫利肖舍里斯3个变身项链.原价7740G,限时惊爆价2460G', 7740, 1230, '3', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.403', 'CN90053', '222.128.26.192');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10046, 50022, '至尊猎人敏捷精华LV3礼包', '', '至尊猎人敏捷精华LV3礼包含
*1猎人敏捷精华LV3(30日）*9 1个 
*4猎人敏捷精华LV3 攻击速度和移动速度效果最强*9
变身强化精华LV3（30日）1个
幸运精华LV3（30日）1个
', 960, 440, '2', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:28.403', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10052, 50028, '至尊贤者智慧精华LV3礼包', '', '至尊贤者智慧精华LV3礼包含
*1贤者智慧LV3（30日）*9 1个 
*4贤者智慧LV3 攻击速度和移动速度效果最强*9
变身强化精华LV3（30日）1个
幸运精华LV3（30日）1个', 960, 440, '2', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:28.200', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10053, 50029, '变身强化精华LV3礼包[7天]', '', '变身强化精华Lv3,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+90，MP值+90', 100, 50, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.403', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10054, 50030, '英雄力量精华礼包[7天]', '', '英雄力量精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后力量+6,攻击速度/移动速度小幅度提高', 100, 50, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10055, 50031, '贤者智慧精华礼包[7天]', '', '贤者敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后智慧+6,攻击速度/移动速度小幅度提高', 100, 50, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10056, 50032, '猎人敏捷精华礼包[7天]', '', '猎人敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后敏捷+6,攻击速度/移动速度小幅度提高', 100, 50, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10059, 50035, '幸运精华LV3礼包[30天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 320, 160, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.747', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10060, 50036, '变身强化精华LV3礼包[30天]', '', '变身强化精华Lv3,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+90，MP值+90', 320, 160, '1', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:27.793', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10061, 50037, '斗神祝福礼包', '', '购买后出现在金币道具栏内,
超级命中药水 10个
超级重击药水  5个
防御药水     10个
超级加速药水 10个', 120, 40, '3', '1', '1', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:26.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10083, 50059, '圣诞礼包', '', 'test', 0, 0, '4', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.623', 'CN13117', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10104, 50080, '超强新手礼包', '', '<GUIText ver=2>
<text no=0> 
<p>
我是本镇的镇长
在此生活已达60多年了

我的职责是协助玩家创建公会，不过不知道能坚持多久。。。年纪大了，最近身体越来越衰弱了。

*3骑士才能创建公会。

*9其它角色只能加入公会。
</p>
</text> 


<text no=1>
<p>
已经有志同道合的人，想抛弃他们吗?
</p>
</text>

<text no=2>
<p>
恩。。。你不是骑士
只有骑士血统才能成为公会长

不好意思。
</p>
</text>


</GUIText>', 0, 0, '1', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.577', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10105, 50081, '测试礼包1', '', 'test', 110, 55, '1', '1', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.717', 'CN99019', '10.34.118.161');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10111, 50087, '礼包', '', '深深礼包', 8, 3, '3', '1', '0', 1, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.873', 'CN12835', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10114, 50090, '礼包三1', '', '包含:基础精华x1,强化精华x1', 0, 0, '3', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:32.827', 'CN99019', '10.34.118.160');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10115, 50091, 'pack-1', '', '元旦礼包内含：项链解封之咒、戒指解封之咒（A）、戒指解封之咒（B）、腰带解封之咒、披风解封之咒、德拉克神秘戒指14天、浓缩治疗药水（100个）、传送之咒（20个）、超级加速药水（20个）、防御药水（20个）、疾速药水（20个）、米泰欧斯的守护（3个）。所有解封之咒的有效期都为30天。', 0, 0, '4', '0', '0', 0, 1, 0, '2008-01-11 15:24:00.000', NULL, NULL, '2014-08-12 12:06:33.217', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10124, 63, '+0 兽人鳞甲', '', 'asdfg', 0, 0, '1', '0', '0', 0, 1, 0, '2008-03-03 16:18:51.930', '', '127.0.0.1', '2014-08-12 12:06:39.077', 'CN11582', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10125, 1618, '德拉克召唤书(7天)', '', '使用后可召唤超级德拉克,同时可随时收回.

召唤出的德拉克可在地下城内骑.

双击鼠标左键使用,*1购买后开始计时间*9,有效时间7天. 

德拉克召唤书购买后不可进行出售，丢弃和存入仓库等操作.', 180, 90, '1', '0', '0', 7, 1, 0, '2008-03-17 15:05:15.037', 'CN12504', '210.72.232.228', '2014-08-12 13:19:36.890', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10126, 1618, '德拉克召唤书(14天)', '', '使用后可召唤超级德拉克,同时可随时收回.

召唤出的德拉克可在地下城内骑.

双击鼠标左键使用,*1购买后开始计时间*9,有效时间7天. 

德拉克召唤书购买后不可进行出售，丢弃和存入仓库等操作.', 300, 150, '1', '0', '0', 14, 1, 0, '2008-03-17 15:11:30.990', 'CN12504', '210.72.232.228', '2014-08-12 12:06:31.607', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10127, 1594, '地下城入场券(14天)', '', '双击左键使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期14天.
使用后可按*1T键*9查询剩余时间.', 180, 90, '1', '0', '0', 0, 1, 336, '2008-03-26 14:19:24.603', 'CN12504', '210.72.232.228', '2014-08-12 12:06:33.513', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10128, 1594, '地下城入场券(7天)', '', '双击左键后使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期7天.
使用后可按*1T键*9查询剩余时间', 270, 135, '1', '0', '0', 0, 1, 168, '2008-04-02 09:34:48.800', 'CN12504', '210.72.232.228', '2014-08-12 12:06:36.077', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10129, 1482, '神之假面', '', '可以重新设定角色外貌及性别的神奇石像.

*1必须在角色登陆画面时使用,需在角色选择画面点击右下方的"变更角色信息"按钮后调整人物性别、外貌及发型*9.

该道具为*1绑定道具.不可进行出售、丢弃及存入仓库等操作*9.', 300, 150, '1', '0', '1', 0, 1, 0, '2008-05-01 09:06:35.830', 'CN12504', '210.72.232.228', '2014-08-12 12:06:25.013', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10130, 1483, '角色更名许可证', '', '可以重新修改角色名称的道具.

*1必须在角色登陆画面时使用，需在角色选择画面点击右下方的"变更角色信息"按钮后修改角色名称*9.

该道具为*1绑定道具,不可进行出售、丢弃及存入仓库等操作*9.', 800, 200, '1', '0', '1', 0, 1, 0, '2008-05-01 09:07:09.173', 'CN12504', '210.72.232.228', '2014-08-12 12:06:24.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10131, 1596, '哈德斯守护', '', '当角色死亡后,在村镇中的*1<哈德斯的传教士> 阿尔特莫*9附近复活,点击该NPC,他会帮助您使用哈德斯的守护来恢复您损失的经验值.

不同级别恢复经验值所需的哈德斯守护的*1数量不一,14级以下无法使用该物品*9.', 50, 15, '1', '0', '1', 0, 1, 0, '2008-05-01 09:09:40.647', 'CN12504', '210.72.232.228', '2014-08-12 12:06:23.670', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10138, 1594, '地下城入场券(6小时)', '', '双击左键使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期6小时.
使用后可按*1T键*9查询剩余时间.', 25, 12, '1', '0', '0', 0, 1, 6, '2008-05-07 07:33:56.330', 'KR10643', '210.72.232.136', '2014-08-12 12:06:33.373', 'CN12654', '10.34.118.153');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10140, 707, '冰糖', '', 'test', 0, 0, '1', '0', '0', 0, 1, 0, '2008-05-20 16:48:04.180', 'CN12504', '210.72.232.131', '2014-08-12 12:06:32.513', 'CN12504', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10141, 500, '牛奶', '', 'test', 0, 0, '1', '0', '0', 0, 1, 0, '2008-05-20 16:48:22.180', 'CN12504', '210.72.232.131', '2014-08-12 12:06:38.467', 'CN12504', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10142, 1485, '角色移动许可证', '', '该物品需要到各村镇的*1NPC <活动>向导*9来帮您完成该道具的使用.

金币物品栏内拥有角色移动许可证的用户,需要根据<活动>向导的提示输入要将该角色移动至的游戏帐号.

要移动至的帐号内需要有空余的角色位置.

请谨慎输入要移动至的游戏帐号.

该道具为绑定道具,不可进行出售、丢弃及存入仓库等操作.

*4温馨提示:
温馨提示
角色移动到新帐号内，被封印的物品将被重新封印;
角色移动只能在同一服务器内进行.
仓库内物品无法转移(如帐号下只有1个角色,转移后原帐号内角色的仓库物品会被保留).
公会会长无法移动角色.*9', 1000, 500, '4', '0', '1', 0, 1, 0, '2008-05-28 07:09:47.340', 'CN12504', '210.72.232.228', '2014-08-12 12:44:06.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10143, 1484, '公会信息变更许可证', '', '公会长需要到各村镇的*1NPC <活动>向导*9来帮您完成该道具的使用。

该道具可委任其他公会成员(骑士)担任公会长，*1按C键直接右键点击要委任的公会成员，但该公会成员只有在线的情况下可以委任成功*9。

公会长变更完成公会名称后，包括*1公会长在内所有公会成员会与服务器断开连接*9。

攻城战期间或公会处于战争、联盟状态均无法进行变更。

该道具为绑定道具，不可进行出售、丢弃及存入仓库等操作。', 1500, 300, '1', '0', '1', 0, 1, 0, '2008-05-28 07:09:47.040', 'CN12504', '210.72.232.228', '2014-08-12 12:06:25.107', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10144, 1825, '增幅之咒', '', '双击左键使用可获得*1 1.3倍经验状态。持续时间3小时。*9

双击后开始计时，离线计时不停止。可按T键盘查询剩余时间。', 28, 7, '1', '0', '0', 0, 1, 3, '2008-05-28 07:09:46.840', 'CN12504', '210.72.232.228', '2014-08-12 12:06:35.967', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10145, 2041, '北京奥运祝福礼花', '', '祝福北京成功举办2008奥运会游戏烟花.

双击该道具会释放礼花,呈现出*4祝福 beijing 2008*9的图案.

同时,会*4100%恢复饱腹度.', 1, 1, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:16.060', 'CN12504', '210.72.232.228', '2014-08-12 12:06:24.793', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10146, 2042, '北京奥运助威礼花', '', '北京奥运助威礼花', 1, 0, '1', '0', '0', 0, 1, 0, '2008-06-04 07:03:15.873', 'CN12504', '210.72.232.228', '2014-08-12 12:06:38.560', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10147, 2043, '胜利礼花', '', '代表胜利的游戏烟花.

双击该道具会释放礼花,呈现出*4V*9图案.

同时,会*4100%恢复饱腹度。', 2, 1, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:15.967', 'CN12504', '210.72.232.228', '2014-08-12 12:06:23.110', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10148, 2044, 'ABC', '', 'I Love China游戏烟花.

双击该道具会释放礼花,呈现出*4I Love China*9图案.
同时,会*4100%恢复饱腹度.', 1, 1, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:16.157', 'CN12504', '210.72.232.228', '2014-08-12 12:06:24.857', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10149, 2045, '爱之礼花', '', '表达爱意的游戏烟花.

双击该道具会释放礼花,呈现出*4两个心型*9图案.

同时,会*4100%恢复饱腹度.', 2, 1, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:16.250', 'CN12504', '210.72.232.228', '2014-08-12 12:06:24.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10150, 2046, 'World礼花', '', '奥运口号的游戏烟花.

双击该道具会释放礼花,呈现出*4One world One dream*9图案.

同时,会*4100%恢复饱腹度.', 1, 1, '1', '0', '1', 0, 10, 0, '2008-06-04 07:03:16.343', 'CN12504', '210.72.232.228', '2014-08-12 12:06:23.763', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10151, 2050, 'I Love China标志', '', 'I Love China特殊标志.

双击该道具会在角色头顶呈现*4I Love China*9的标志.

持续时间24小时.', 10, 5, '1', '0', '0', 0, 1, 0, '2008-06-04 07:03:16.437', 'CN12504', '210.72.232.228', '2009-12-28 18:35:20.537', 'CN13117', '10.34.118.176');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10152, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4毁灭之戒、必杀之戒、吸收戒指、灿烂的力量、敏捷、智慧戒指等稀有戒指物品，还有武器强化卷轴及防具强化卷轴(普通、祝福)、芒果、魔法增加剂等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 80, 40, '4', '0', '1', 0, 1, 0, '2008-06-11 06:57:02.697', 'CN12504', '210.72.232.228', '2014-08-12 12:44:06.060', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10153, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 175, 70, '1', '0', '0', 0, 5, 0, '2008-06-18 07:02:20.180', 'CN12504', '210.72.232.228', '2014-08-12 12:06:37.763', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10157, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 300, 112, '1', '0', '0', 0, 10, 0, '2008-07-02 07:04:58.050', 'CN12504', '210.72.232.228', '2014-08-12 12:06:37.717', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10158, 40609, '基础精华', '', '该道具是属性强化的基础精华,购买后可与NPC<属性强化>助手进行交换,获得不同属性的精华物品.

基础精华是强化属性最基本的物品.

成功购买后,基础精华会出现在您的*1属性强化栏内.', 16, 8, '1', '0', '0', 0, 1, 0, '2008-09-02 07:26:12.827', 'CN12654', '210.72.232.131', '2014-08-12 12:06:33.123', 'CN13117', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10159, 40610, '强化精华', '', '强化精华可对除活动精华外所有精华物品进行强化,拥有一定成功或失败的几率.

强化成功后精华物品属性将大幅度提高.

成功购买后,强化精华会出现在您的*1属性强化栏内.', 35, 17, '1', '0', '0', 0, 1, 0, '2008-09-02 07:26:12.920', 'CN12654', '210.72.232.131', '2014-08-12 12:06:36.670', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10160, 2416, '疾速药水', '', '可提升移动速度的神奇药剂,*1效果强于加速药水*9.

移动速度提升效果持续时间30分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 4, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.483', 'CN12654', '210.72.232.131', '2014-08-12 12:06:22.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10161, 2417, '突击药水', '', '双击使用可提升攻击速度,*1效果强于勇气药剂*9.

*1仅限骑士*9使用,攻击速度提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.
', 8, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.390', 'CN12654', '210.72.232.131', '2014-08-12 12:06:25.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10162, 2418, '超级敏捷药水', '', '双击使用可将敏捷提升3.

敏捷提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 4, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.297', 'CN12654', '210.72.232.131', '2014-08-12 12:06:24.200', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10163, 2419, '超级命中药水', '', '双击使用可将命中提升3.

命中提升效果持续时间5分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 4, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.220', 'CN12654', '210.72.232.131', '2014-08-12 12:06:27.060', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10164, 2420, '超级重击药水', '', '双击使用可将暴击率提升.

暴击率提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 5, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.123', 'CN12654', '210.72.232.131', '2014-08-12 12:06:24.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10165, 2421, '防御药水', '', '双击使用可将防御力提升3.

防御提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 5, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:11.030', 'CN12654', '210.72.232.131', '2014-08-12 12:06:27.013', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10166, 2422, '超级智慧药水', '', '双击使用可将智力提升3.

智力提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 2, 1, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:10.953', 'CN12654', '210.72.232.131', '2014-08-12 12:06:23.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10167, 2423, '超级加速药水', '', '双击使用可提升移动速度.

*1移动速度效果强于超级加速秘药*9,效果持续时间为40秒.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 2, 1, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:10.860', 'CN12654', '210.72.232.131', '2014-08-12 12:06:25.997', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10168, 2424, '超级力量药水', '', '双击使用可将力量提升3.

力量提升效果持续时间10分钟

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 2, 1, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:10.763', 'CN12654', '210.72.232.131', '2014-08-12 12:06:26.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10169, 2425, '负重之药水 Lv1', '', '双击使用可将负重提升500.

负重提升效果持续时间20分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 3, 1, '1', '0', '0', 0, 1, 0, '2008-09-02 07:26:10.670', 'CN12654', '210.72.232.131', '2014-08-12 12:06:29.497', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10170, 2426, '负重之药水 Lv2', '', '双击使用可将负重提升1000.

负重提升效果持续时间10分钟.

死亡后提升效果消失.爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 5, 2, '1', '0', '0', 0, 1, 0, '2008-09-02 07:26:10.593', 'CN12654', '210.72.232.131', '2014-08-12 12:06:31.513', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10171, 2427, '净化药水', '', '使用后可解除角色身上一切附加魔法效果及特殊药水效果.

爆裂法杖的攻击会造成该药剂的损失.

双击使用.或放入快捷栏中.按对应的快捷键使用.', 5, 2, '1', '0', '1', 0, 1, 0, '2008-09-02 07:26:10.500', 'CN12654', '210.72.232.131', '2014-08-12 12:06:22.110', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10172, 2428, '死亡弓箭', '', 'test', 1, 0, '1', '0', '0', 0, 1000, 0, '2008-09-02 07:26:10.407', 'CN12654', '210.72.232.131', '2014-08-12 12:06:38.263', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10173, 2429, '生命之戒', '', 'test', 1, 0, '1', '0', '0', 7, 1, 0, '2008-09-02 07:26:10.327', 'CN12654', '210.72.232.131', '2014-08-12 12:06:38.357', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10174, 2434, '恢复戒指', '', '佩戴后HP恢复+1,MP恢复+1,需要解封戒指栏.购买后开始计时.', 80, 40, '1', '0', '0', 7, 1, 0, '2008-09-02 07:26:10.233', 'CN12654', '210.72.232.131', '2014-08-12 12:06:19.657', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10175, 2430, '神力之戒', '', '佩戴后负重增加1500,需要解封戒指栏.购买后开始计时.', 60, 30, '1', '0', '1', 7, 1, 0, '2008-09-02 07:26:10.140', 'CN12654', '210.72.232.131', '2014-08-12 12:06:25.763', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10176, 2431, '辩识之书', '', 'test', 20, 10, '1', '0', '0', 30, 1, 0, '2008-09-02 07:26:10.047', 'CN12654', '210.72.232.131', '2014-08-12 12:06:38.077', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10177, 2432, '旅行者之书', '', '双击旅行者之书,可将您瞬间传送到当前位置最近的城镇.购买后开始计时,有效期30天.', 60, 30, '4', '0', '1', 30, 1, 0, '2008-09-02 07:26:09.903', 'CN12654', '210.72.232.131', '2014-08-12 13:17:53.420', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10179, 2445, '芒果糖', '', 'test', 1, 0, '1', '0', '0', 0, 1, 0, '2008-09-23 07:03:54.940', 'CN12504', '210.72.232.225', '2014-08-12 12:06:38.187', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10180, 2446, '芒果汁', '', 'test', 1, 0, '1', '0', '0', 0, 1, 0, '2008-09-23 07:03:55.080', 'CN12504', '210.72.232.225', '2014-08-12 12:06:38.123', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10181, 2440, '雷克斯魔女项链(30天)', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9', 800, 400, '1', '0', '1', 30, 1, 0, '2008-10-14 07:02:45.977', 'CN12504', '210.72.232.225', '2014-08-12 12:06:25.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10182, 1825, '增幅之咒（2倍）（3小时）', '', '加打怪时获得的经验.该效果可以被更高倍数"经验增幅之咒"的效果替换.经验增加效果与时间不累积. ', 35, 17, '1', '0', '0', 0, 1, 3, '2008-10-21 07:00:58.553', 'CN12504', '210.72.232.131', '2014-08-12 12:20:22.903', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10183, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 175, 87, '1', '0', '0', 0, 5, 0, '2008-10-21 07:00:57.790', 'CN12504', '210.72.232.131', '2014-08-12 12:06:37.670', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10184, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 300, 150, '1', '0', '0', 0, 10, 0, '2008-10-21 07:00:57.897', 'CN12504', '210.72.232.131', '2014-08-12 12:06:37.623', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10185, 40609, '基础精华', '', '该道具是属性强化的基础精华,购买后可与NPC<属性强化>助手进行交换,获得不同属性的精华物品.

基础精华是强化属性最基本的物品.

成功购买后,基础精华会出现在您的*1属性强化栏内.', 20, 10, '1', '0', '1', 0, 1, 0, '2008-10-21 07:00:57.990', 'CN12504', '210.72.232.131', '2014-08-12 12:06:24.170', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10186, 40610, '强化精华', '', '强化精华可对除活动精华外所有精华物品进行强化,拥有一定成功或失败的几率.

强化成功后精华物品属性将大幅度提高.

成功购买后,强化精华会出现在您的*1属性强化栏内.', 50, 25, '1', '0', '1', 0, 1, 0, '2008-10-21 07:00:58.460', 'CN12504', '210.72.232.131', '2014-08-12 12:06:26.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10187, 1599, '项链解封之咒(3天)', '', '使用后开启装备栏中的项链栏,从而可佩戴项链.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 5, 2, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:58.777', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.733', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10188, 1597, '戒指解封之咒(A-3天)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 5, 2, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:58.980', 'CN12504', '210.72.232.225', '2014-08-12 12:06:38.030', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10189, 1598, '戒指解封之咒(B-3天)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.
', 5, 2, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:59.073', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.983', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10190, 1600, '腰带解封之咒(3天)', '', '使用后开启装备栏中的腰带栏,从而可佩戴腰带.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 5, 2, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:59.170', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.920', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10191, 1601, '披风解封之咒(3天)', '', '使用后开启装备栏中的披风栏,从而可佩戴披风.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.
', 5, 2, '1', '0', '0', 0, 1, 72, '2008-11-04 07:20:59.263', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.873', 'CN12654', '210.72.232.131');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10192, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4毁灭之戒、必杀之戒、吸收戒指、灿烂的力量、敏捷、智慧戒指等稀有戒指物品，还有武器强化卷轴及防具强化卷轴(普通、祝福)、芒果、魔法增加剂等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 360, 180, '4', '0', '1', 0, 5, 0, '2008-12-02 08:22:42.523', 'CN12654', '210.72.232.131', '2014-08-12 12:44:06.450', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10193, 1621, '黄金密匙', '', '黄金密匙是开启黄金宝箱的*4唯一道具*9.

该物品需要到各村镇的*4<黄金宝箱>欧谱诺*9来协助您打开宝箱.

黄金宝箱内含*4武器强化卷轴及防具强化卷轴(普通、祝福、诅咒)、变身法杖等物品*9.


温馨提示:
*4被诅咒的物品使用解咒药水可恢复成正常状态；
黄金宝箱会在D级(包括D级)以上的怪物中掉落。*9', 30, 15, '1', '0', '0', 0, 1, 0, '2008-12-22 16:02:23.253', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.780', 'KR13076', '10.34.114.101');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10194, 1623, '米泰欧斯的守护', '', '强化装备失败时,行囊内如有米泰欧斯的守护,*1可一定几率的自动保护强化失败的装备*9.

无论保护成功或失败,*1米泰欧斯的守护都会消失*9.', 64, 32, '1', '0', '0', 0, 1, 0, '2008-12-22 16:02:25.100', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.827', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10195, 2440, '雷克斯魔女项链', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9', 9, 4, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:34.597', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.890', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10200, 2434, '恢复戒指', '', '佩戴后HP恢复+1,MP恢复+1,需要解封戒指栏.购买后开始计时.', 1, 0, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:38.567', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.090', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10201, 2430, '神力之戒', '', '佩戴后负重增加1500,需要解封戒指栏.购买后开始计时.', 1, 0, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:38.880', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.153', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10202, 2432, '旅行者之书', '', '双击旅行者之书,可将您瞬间传送到当前位置最近的城镇.购买后开始计时,有效期30天.', 1, 0, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:39.487', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.200', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10203, 2033, '月光复仇者项链(14天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+75 MP+45 负重+500*9.', 0, 0, '1', '0', '0', 14, 1, 0, '2008-12-22 16:02:40.690', 'CN12504', '210.72.232.225', '2014-08-12 12:06:35.827', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10204, 2039, '锥锋复仇者项链(14天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+75 MP+45 负重+500*9.', 0, 0, '1', '0', '0', 14, 1, 0, '2008-12-22 16:02:41.050', 'CN12504', '210.72.232.225', '2014-08-12 12:06:35.873', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10205, 1594, '地下城入场券(3小时)', '', '双击左键使用后,可进入R2大陆内所有的地下城3层以上的地图.有效期3小时.
使用后可按*1T键*9查询剩余时间.', 12, 6, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:41.380', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.623', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10206, 1597, '戒指解封之咒(A)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 0, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:41.723', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.263', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10207, 1598, '戒指解封之咒(B)', '', '使用后开启装备栏中的戒指栏,从而可佩戴1个戒指.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 0, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:42.347', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.310', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10208, 1599, '项链解封之咒', '', '使用后开启装备栏中的项链栏,从而可佩戴项链.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 0, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:42.707', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.357', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10209, 1600, '腰带解封之咒', '', '使用后开启装备栏中的腰带栏,从而可佩戴腰带.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 0, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:43.037', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.420', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10210, 1601, '披风解封之咒', '', '使用后开启装备栏中的披风栏,从而可佩戴披风.

双击左键后使用.

使用后可按*1T键*9查询剩余时间.', 1, 0, '1', '0', '0', 0, 1, 3, '2008-12-22 16:02:43.707', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.467', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10211, 1590, '传送记忆列表(A)', '', '在20天内,*1可额外增加传送之咒中10个记录点*9.
双击鼠标左键使用.使用后可按*1T键*9查询剩余时间.', 1, 0, '1', '0', '0', 0, 1, 2, '2008-12-22 16:02:44.113', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.513', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10212, 1618, '德拉克召唤法书', '', '使用后可召唤超级德拉克,同时可随时收回.

召唤出的德拉克可在地下城内骑.

双击鼠标左键使用,*1购买后开始计时间*9,有效时间7天. 

德拉克召唤书购买后不可进行出售，丢弃和存入仓库等操作.', 3, 1, '1', '0', '0', 1, 1, 0, '2008-12-22 16:02:44.533', 'CN12504', '210.72.232.225', '2014-08-12 12:06:37.560', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10213, 2817, '温馨宝箱钥匙', '', '开启温馨宝箱的神秘钥匙钥匙', 300, 150, '1', '0', '0', 0, 1, 0, '2009-01-13 08:08:40.577', 'CN12504', '210.72.232.225', '2014-08-12 12:06:36.327', 'CN12504', '210.72.232.225');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10214, 1952, '[活动] +4 双手剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:12.623', 'CN12654', '210.72.232.225', '2014-08-12 12:06:36.123', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10215, 1956, '[活动] +4 猎枪', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-13 11:15:50.920', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.217', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10216, 1960, '[活动]+4 十字剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-13 11:15:50.437', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.280', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10217, 1964, '[活动]+4 拳剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:14.153', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.373', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10218, 1953, '[活动] +5 双手剑', '', '', 0, 0, '4', '0', '0', 14, 1, 0, '2009-03-17 08:06:14.437', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.030', 'KR13076', '10.34.65.17');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10219, 1957, '[活动] +5 猎枪', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:14.687', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.420', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10220, 1961, '[活动]+5 十字剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:14.967', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.483', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10221, 1965, '[活动]+5 拳剑', '', '', 0, 0, '1', '0', '0', 14, 1, 0, '2009-03-17 08:06:15.233', 'CN12654', '10.34.117.27', '2014-08-12 12:06:36.530', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10222, 58, '黑皮甲', '', 'test ', 0, 0, '1', '0', '0', 30, 1, 30, '2009-03-13 13:41:27.217', 'CN11582', '59.108.20.1', '2014-08-12 12:06:36.170', 'CN11582', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10223, 2074, '增幅之咒(2倍)(3小时)', '', '打怪时获得的经验提高2倍.该效果可以替换掉低倍数"经验增幅之咒"的效果.经验增加效果与时间不累积.', 35, 17, '4', '0', '1', 0, 1, 3, '2009-04-16 07:11:18.513', 'CN12654', '10.34.115.185', '2014-08-12 13:01:59.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10224, 40600, '幸运精华 Lv1[7天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:17.983', 'CN12654', '10.34.115.185', '2014-08-12 12:06:27.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10225, 40603, '变身强化精华 Lv1[7天]', '', '变身强化精华Lv1,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+30，MP值+30', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:16.293', 'CN12654', '10.34.115.185', '2014-08-12 12:06:25.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10226, 40606, '英雄力量精华[7天]', '', '英雄力量精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后力量+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:16.840', 'CN12654', '10.34.115.185', '2014-08-12 12:06:25.857', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10227, 40607, '猎人敏捷精华[7天]', '', '猎人敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后敏捷+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:17.107', 'CN12654', '10.34.115.185', '2014-08-12 12:06:25.670', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10228, 40608, '贤者智慧精华[7天]', '', '贤者敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后智慧+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 7, 1, 0, '2009-04-16 07:11:17.387', 'CN12654', '10.34.115.185', '2014-08-12 12:06:27.107', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10229, 2050, 'I Love China标志', '', 'I Love China特殊标志.

双击该道具会在角色头顶呈现*4I Love China*9的标志.

持续时间24小时.', 10, 5, '1', '0', '1', 0, 1, 0, '2009-04-27 12:58:53.303', 'CN12654', '10.34.115.182', '2014-08-12 12:06:23.467', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10230, 40601, '幸运精华 Lv2 [7天]', '', '购买后出现在属性强化栏内，仅能佩戴于特殊精华第一栏，携带后物品掉落几率提升', 0, 0, '1', '0', '2', 7, 1, 0, '2009-05-05 09:19:49.960', 'CN12654', '10.34.118.195', '2014-08-12 12:06:23.907', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10231, 40602, '幸运精华 Lv3[7天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 7, 1, 0, '2009-05-05 09:19:49.680', 'CN12654', '10.34.118.195', '2014-08-12 12:06:27.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10232, 40604, '变身强化精华 Lv2[7天]', '', '变身强化精华Lv2,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+60，MP值+60', 0, 0, '1', '0', '2', 7, 1, 0, '2009-05-05 09:19:49.413', 'CN12654', '10.34.118.195', '2014-08-12 12:06:24.107', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10233, 40605, '变身强化精华 Lv3[7天]', '', '变身强化精华Lv3,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+90，MP值+90', 0, 0, '1', '0', '2', 7, 1, 0, '2009-05-05 09:19:49.147', 'CN12654', '10.34.118.195', '2014-08-12 12:06:25.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10234, 40600, '幸运精华 Lv1[30天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:45.117', 'CN12654', '10.34.118.195', '2014-08-12 12:06:21.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10235, 40601, '幸运精华 Lv2[30天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:48.883', 'CN12654', '10.34.118.195', '2014-08-12 12:06:27.200', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10236, 40602, '幸运精华 Lv3[30天]', '', '购买后出现在属性强化栏内,仅能佩带于特殊精华第一栏,携带后物品掉落几率提升.', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:48.617', 'CN12654', '10.34.118.195', '2014-08-12 12:06:22.407', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10237, 40603, '变身强化精华 Lv1[30天]', '', '变身强化精华Lv1,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+30，MP值+30', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:48.337', 'CN12654', '10.34.118.195', '2014-08-12 12:06:23.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10238, 40604, '变身强化精华 Lv2[30天]', '', '变身强化精华Lv2,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+60，MP值+60', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:48.070', 'CN12654', '10.34.118.195', '2014-08-12 12:06:26.403', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10239, 40605, '变身强化精华 Lv3[30天]', '', '变身强化精华Lv3,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+90，MP值+90', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:47.803', 'CN12654', '10.34.118.195', '2014-08-12 12:06:23.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10240, 40606, '英雄力量精华[30天]', '', '英雄力量精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后力量+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:47.540', 'CN12654', '10.34.118.195', '2014-08-12 12:06:22.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10241, 40607, '猎人敏捷精华[30天]', '', '猎人敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后敏捷+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:47.273', 'CN12654', '10.34.118.195', '2014-08-12 12:06:21.907', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10242, 40608, '贤者智慧精华[30天]', '', '贤者敏捷精华,购买后出现在属性强化栏内,仅能佩带于特殊精华第三栏,携带后智慧+6,攻击速度/移动速度小幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2009-05-05 09:19:47.010', 'CN12654', '10.34.118.195', '2014-08-12 12:06:22.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10243, 2033, '月光复仇者项链', '', '月光复仇者项链(30天)', 1280, 640, '1', '0', '0', 30, 1, 0, '2009-07-07 17:07:41.753', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.920', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10244, 40604, '变身强化精华 Lv2(14天）', '', '变身强化精华Lv2,购买后出现在属性强化栏内,仅能佩带于特殊精华第二栏,携带后变身状态下HP+60，MP值+60', 10, 5, '1', '0', '0', 14, 1, 0, '2009-06-23 12:17:09.437', 'CN12654', '10.34.115.182', '2014-08-12 12:06:31.310', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10245, 2996, '印谱诺项链(II)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+50 MP+30 负重+300', 0, 0, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:46.177', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.763', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10249, 2440, '雷克斯魔女项链(1天)', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9', 0, 0, '1', '0', '0', 1, 1, 0, '2009-09-22 08:08:44.350', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.670', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10250, 2440, '雷克斯魔女项链(1天)', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9', 0, 0, '1', '0', '0', 1, 1, 0, '2009-09-22 08:08:43.833', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.513', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10251, 3016, '公主项链(普通)', '', '封印着公主灵魂的项链(*1需要解封项链栏*9),佩带后将获得公主般华丽的样貌，且拥有超快的攻击速度.

购买后开始计时间.

变身为公主,*1HP+50 MP+30 负重+300*9', 45, 22, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:39.723', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.420', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10252, 2788, '威尔斯沃琪项链', '', '封印着威尔斯沃琪灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为威尔斯沃琪,*1HP+100 MP+60 负重+750*9', 45, 22, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:39.567', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.373', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10253, 2794, '科普提恩雷肯项链', '', '封印着科普提恩雷肯灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+100 MP+60 负重+750*9', 45, 22, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:39.880', 'CN12654', '10.34.223.29', '2014-08-12 12:06:35.467', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10254, 2996, '印谱诺项链(II)(1天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+75 MP+45 负重+500*9', 140, 70, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:56.343', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.827', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10255, 2996, '印谱诺项链(II)(7天)', '', '"封印着印谱诺灵魂的项链(*1需要解封项链栏*9)强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+75 MP+45 负重+500*9', 450, 225, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:55.110', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.497', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10256, 2996, '印谱诺项链(II)(30天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+75 MP+45 负重+500*9
', 1260, 630, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:53.893', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.607', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10257, 2997, '印谱诺项链(III)(1天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+100 MP+60 负重+750*9
', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:53.097', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.873', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10258, 2997, '印谱诺项链(III)(7天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+100 MP+60 负重+750*9
', 540, 270, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:52.300', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.157', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10259, 2997, '印谱诺项链(III)(30天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+100 MP+60 负重+750*9
', 1510, 755, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:51.503', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.110', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10260, 2998, '印谱诺项链(IV)(1天)', '', '封印着印谱诺灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得他那超快的攻击速度.

购买后开始计时间.

变身为印谱诺,*1HP+150 MP+90 负重+1250*9
', 200, 100, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:50.267', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.920', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10263, 2440, '雷克斯魔女项链(1天)', '', '封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+50 MP+30 负重+300*9"
', 80, 40, '4', '0', '0', 1, 1, 0, '2009-07-16 19:26:47.470', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.670', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10264, 2999, '雷克斯魔女项链(II)(1天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9)强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+75 MP+45 负重+500*9"
', 140, 70, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:46.220', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.967', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10265, 2999, '雷克斯魔女项链(II)(7天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9)强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+75 MP+45 负重+500*9"', 450, 225, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:45.003', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.903', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10266, 2999, '雷克斯魔女项链(II)(30天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+75 MP+45 负重+500*9"', 1260, 630, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:43.767', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.670', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10267, 3000, '雷克斯魔女项链(III)(1天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+100 MP+60 负重+750*9"', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:42.987', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.030', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10268, 3000, '雷克斯魔女项链(III)(7天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+100 MP+60 负重+750*9"', 540, 270, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:42.190', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.360', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10269, 3000, '雷克斯魔女项链(III)(30天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+100 MP+60 负重+750*9"', 1510, 755, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:41.410', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10270, 3001, '雷克斯魔女项链(IV)(1天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+150 MP+90 负重+1250*9"', 200, 100, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:39.580', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.077', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10271, 3001, '雷克斯魔女项链(IV)(7天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+150 MP+90 负重+1250*9"', 650, 325, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:37.767', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.203', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10272, 3001, '雷克斯魔女项链(IV)(30天)', '', '"封印着雷克斯魔女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为雷克斯魔女,*1HP+150 MP+90 负重+1250*9"', 1810, 905, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:36.970', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10274, 3002, '嗜血的吸血鬼项链(II)(1天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9)强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+75MP+45 负重+500*9"', 140, 70, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:34.097', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.123', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10275, 3002, '嗜血的吸血鬼项链(II)(7天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+75MP+45 负重+500*9"', 450, 225, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:33.313', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.060', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10276, 3002, '嗜血的吸血鬼项链(II)(30天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+75MP+45 负重+500*9"', 1260, 630, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:32.533', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.997', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10277, 3003, '嗜血的吸血鬼项链(III)(1天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+100MP+60 负重+750*9"', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:31.737', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.170', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10278, 3003, '嗜血的吸血鬼项链(III)(7天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+100MP+60 负重+750*9"', 540, 270, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:30.940', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10279, 3003, '嗜血的吸血鬼项链(III)(30天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+100MP+60 负重+750*9"', 1510, 755, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:29.707', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.547', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10280, 3004, '嗜血的吸血鬼项链(IV)(1天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+150MP+90 负重+1250*9"', 200, 100, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:28.923', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.217', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10281, 3004, '嗜血的吸血鬼项链(IV)(7天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+150MP+90 负重+1250*9"', 650, 325, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:28.143', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.407', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10282, 3004, '嗜血的吸血鬼项链(IV)(30天)', '', '"封印着嗜血的吸血鬼女王灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为嗜血的吸血鬼,*1HP+150MP+90 负重+1250*9"', 1810, 905, '1', '0', '0', 30, 1, 0, '2009-07-16 19:26:27.360', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.453', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10284, 3005, '圣甲战魔项链(II)(1天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+75 MP+45 负重+500', 140, 70, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:24.567', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.280', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10285, 3005, '圣甲战魔项链(II)(7天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+75 MP+45 负重+500', 450, 225, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:23.330', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.857', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10286, 3005, '圣甲战魔项链(II)(30天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+75 MP+45 负重+500', 1260, 630, '1', '0', '0', 30, 1, 0, '2009-07-16 19:26:22.550', 'CN12654', '10.34.223.29', '2014-08-12 12:06:28.763', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10287, 3006, '圣甲战魔项链(III)(1天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+100 MP+60 负重+750', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:21.770', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.357', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10288, 3006, '圣甲战魔项链(III)(7天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+100 MP+60 负重+750', 540, 270, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:20.550', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.733', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10289, 3006, '圣甲战魔项链(III)(30天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+100 MP+60 负重+750', 1510, 755, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:18.847', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10290, 3007, '圣甲战魔项链(IV)(1天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+150 MP+90 负重+1250', 200, 100, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:18.080', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.403', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10291, 3007, '圣甲战魔项链(IV)(7天)', '', '封印着圣甲战魔灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩戴后将获得他那恐怖的远程攻击能力.

购买后开始计时.

变身为圣甲战魔,*1HP+150 MP+90 负重+1250', 650, 325, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:17.300', 'CN12654', '10.34.223.29', '2014-08-12 12:06:20.640', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10293, 2033, '月光复仇者项链(1天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+75 MP+45 负重+500*9.', 140, 70, '4', '0', '0', 1, 1, 0, '2009-07-16 19:26:15.300', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.467', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10295, 3008, '月光复仇者项链(改)(1天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+100 MP+60 负重+750*9.', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:13.317', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.513', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10296, 3008, '月光复仇者项链(改)(7天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+100 MP+60 负重+750*9.', 540, 270, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:10.457', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.107', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10297, 3008, '月光复仇者项链(改)(30天)', '', '封印着月光复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有疯狂的近战能力.

购买后开始计时.

变身为月光复仇者,*1HP+100 MP+60 负重+750*9.', 1510, 755, '1', '0', '0', 30, 1, 0, '2009-07-16 19:26:09.237', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.310', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10298, 2039, '锥锋复仇者项链(1天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+75 MP+45 负重+500*9.', 140, 70, '4', '0', '0', 1, 1, 0, '2009-07-16 19:26:08.033', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.560', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10300, 3010, '锥锋复仇者项链(改)(1天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+100 MP+60 负重+750*9.', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:06.503', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.607', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10301, 3010, '锥锋复仇者项链(改)(7天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+100 MP+60 负重+750*9.', 540, 270, '1', '0', '0', 7, 1, 0, '2009-07-16 19:26:05.720', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.217', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10302, 3010, '锥锋复仇者项链(改)(30天)', '', '封印着锥锋复仇者灵魂的项链(需要解封项链栏),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩戴后拥有恐怖的远程攻击能力.

购买后开始计时.

变身为锥锋复仇者,*1HP+100 MP+60 负重+750*9.', 1510, 755, '2', '0', '0', 30, 1, 0, '2009-07-16 19:26:04.533', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.170', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10303, 3016, '公主项链(普通)(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+50 MP+30 负重+300*9"
', 80, 40, '2', '0', '0', 1, 1, 0, '2009-07-16 19:26:03.753', 'CN12654', '10.34.223.29', '2014-08-12 12:06:33.560', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10306, 2782, '公主项链(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+75 MP+45 负重+500*9"
', 140, 70, '1', '0', '0', 1, 1, 0, '2009-07-16 19:26:00.597', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.670', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10307, 2782, '公主项链(7天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+75 MP+45 负重+500*9"', 450, 225, '1', '0', '1', 7, 1, 0, '2009-07-16 19:25:59.830', 'CN12654', '10.34.223.29', '2014-08-12 12:06:25.903', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10308, 2782, '公主项链(30天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+75 MP+45 负重+500*9"', 1260, 630, '1', '0', '1', 30, 1, 0, '2009-07-16 19:25:58.677', 'CN12654', '10.34.223.29', '2014-08-12 12:06:25.717', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10309, 3014, '公主项链(改)(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+100 MP+60 负重+750*9"
', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:57.630', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.717', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10310, 3014, '公主项链(改)(7天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+100 MP+60 负重+750*9"
', 540, 270, '1', '0', '0', 7, 1, 0, '2009-07-16 19:25:55.113', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.403', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10311, 3014, '公主项链(改)(30天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱的公主,*1HP+100 MP+60 负重+750*9"', 1510, 755, '2', '0', '0', 30, 1, 0, '2009-07-16 19:25:54.660', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.357', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10312, 2788, '威尔斯沃琪项链(1天)', '', '"封印着威尔斯沃琪的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴, 佩带后将获得恐怖的远程攻击能力.
购买后开始计时间.

变身为威尔斯沃琪,*1HP+100 MP+60 负重+750*9"
', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:53.207', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.763', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10315, 3012, '威尔斯沃琪项链(改)(1天)', '', '"封印着威尔斯沃琪的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得恐怖的远程攻击能力.

购买后开始计时间.

变身为威尔斯沃琪,*1HP+150 MP+90 负重+1250*9"
', 200, 100, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:51.160', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.810', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10318, 2794, '科普提恩雷肯项链(1天)', '', '"封印着科普提恩雷肯项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+100 MP+60 负重+750*9"', 160, 80, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:49.440', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.873', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10321, 3013, '科普提恩雷肯项链(改)(1天)', '', '"封印着科普提恩雷肯项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+150 MP+90 负重+1250*9"', 200, 100, '2', '0', '0', 1, 1, 0, '2009-07-16 19:25:47.677', 'CN12654', '10.34.223.29', '2014-08-12 12:06:34.920', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10322, 3013, '科普提恩雷肯项链(改)(7天)', '', '"封印着科普提恩雷肯项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+150 MP+90 负重+1250*9"', 650, 325, '1', '0', '0', 7, 1, 0, '2009-07-16 19:25:47.220', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.450', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10323, 3013, '科普提恩雷肯项链(改)(30天)', '', '"封印着科普提恩雷肯项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上的勇士才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为科普提恩雷肯,*1HP+150 MP+90 负重+1250*9"', 1810, 905, '3', '0', '0', 30, 1, 0, '2009-07-16 19:25:46.427', 'CN12654', '10.34.223.29', '2014-08-12 12:06:29.263', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10327, 2800, '英雄箱子', '', '英雄遗落的箱子*9.

宝箱内含*4 +2Lv3生命精华,++2Lv3灵魂精华,++2Lv3破坏精华,++2Lv3守护精华,++2Lv3熟练精华等物品,包括从普通,稀有,史诗到传说级别的极品精华,及10个基础精华.

打开宝箱,可随机获得其中一件道具
*9.
', 0, 0, '1', '0', '0', 0, 1, 0, '2009-08-04 13:41:22.640', 'CN12654', '10.34.223.138', '2014-08-12 12:06:21.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10328, 2072, 'NG_test1', '', '双击左键使用可获得*1 2倍掉率状态。持续时间3天。*9 

双击后开始计时，离线计时不停止。可按T键盘查询剩余时间。', 200000, 100000, '1', '0', '0', 0, 1, 3, '2009-08-04 13:41:23.220', 'CN12654', '10.34.223.138', '2014-08-12 12:06:30.107', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10329, 3372, '高级副本入场券(一层)', '', '双击左键使用后,可进入高级副本1层.
*只有1～40级之间的角色才可进入

道具使用时间为6小时.
*该道具有效期为7天,请在购买后7天内进行使用

使用后可按*1T键*9查询剩余时间.', 140, 70, '4', '0', '1', 30, 1, 6, '2009-08-10 17:42:37.710', 'CN12654', '10.34.223.138', '2014-08-12 13:01:59.060', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10330, 3373, '高级副本入场券(二层)(6小时)', '', '双击左键使用后,可进入高级副本2层.
*只有41级以上的角色才能进入

道具使用时间为6小时.
*该道具有效期为7天,请在购买后7天内进行使用

使用后可按*1T键*9查询剩余时间.', 200, 100, '4', '0', '1', 30, 1, 6, '2009-08-10 17:42:37.897', 'CN12654', '10.34.223.138', '2014-08-12 13:17:53.123', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10331, 3420, '可爱公主项链(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱公主,*1HP+50 MP+30 负重+300*9"
', 80, 40, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:41.163', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.123', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10334, 3426, '可爱公主(II)项链(1天)', '', '"封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱公主,*1HP+75 MP+45 负重+500*9"', 140, 70, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:59.587', 'CN12654', '10.34.223.138', '2014-08-12 12:06:33.013', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10335, 3426, '修炼少女(II)项链(7天)', '', '"封印着修炼少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为修炼少女,*1HP+75 MP+45 负重+500*9"', 450, 225, '1', '0', '0', 7, 1, 0, '2009-08-10 17:42:59.757', 'CN12654', '10.34.223.138', '2014-08-12 12:06:28.700', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10336, 3426, '修炼少女(II)项链(30天)', '', '"封印着修炼少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为修炼少女,*1HP+75 MP+45 负重+500*9"', 1260, 630, '1', '0', '0', 30, 1, 0, '2009-08-10 17:42:40.007', 'CN12654', '10.34.223.138', '2014-08-12 12:06:28.950', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10337, 3427, '可爱公主(III)项链(1天)', '', '封印着可爱的公主灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为可爱公主,*1HP+100 MP+60 负重+750*9"', 160, 80, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:42.413', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.013', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10338, 3427, '修炼少女(III)项链(7天)', '', '封印着修炼少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为修炼少女,*1HP+100 MP+60 负重+750*9"', 540, 270, '1', '0', '0', 7, 1, 0, '2009-08-10 17:42:42.240', 'CN12654', '10.34.223.138', '2014-08-12 12:06:20.000', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10346, 3423, '美少女(II)项链(1天)', '', '"封印着美少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为美少女,*1HP+75 MP+45 负重+500*9"', 140, 70, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:39.837', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.217', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10347, 3423, '东瀛少女(II)项链(7天)', '', '"封印着东瀛少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为东瀛少女,*1HP+75 MP+45 负重+500*9"', 450, 225, '1', '0', '0', 7, 1, 0, '2009-08-10 17:42:39.680', 'CN12654', '10.34.223.138', '2014-08-12 12:06:28.810', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10348, 3423, '东瀛少女(II)项链(30天)', '', '"封印着东瀛少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有54级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为东瀛少女,*1HP+75 MP+45 负重+500*9"', 1260, 630, '2', '0', '0', 30, 1, 0, '2009-08-10 17:42:39.507', 'CN12654', '10.34.223.138', '2014-08-12 12:06:28.560', 'CN90047', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10349, 3424, '美少女(III)项链(1天)', '', '"封印着美少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为美少女,*1HP+100 MP+60 负重+750*9"', 160, 80, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:39.350', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.263', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10350, 3424, '东瀛少女(III)项链(7天)', '', '"封印着东瀛少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为东瀛少女,*1HP+100 MP+60 负重+750*9"', 540, 270, '1', '0', '0', 7, 1, 0, '2009-08-10 17:42:38.943', 'CN12654', '10.34.223.138', '2014-08-12 12:06:19.953', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10351, 3424, '东瀛少女(III)项链(30天)', '', '"封印着东瀛少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有57级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为东瀛少女,*1HP+100 MP+60 负重+750*9"', 1510, 755, '2', '0', '0', 30, 1, 0, '2009-08-10 17:42:38.790', 'CN12654', '10.34.223.138', '2014-08-12 12:06:19.907', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10352, 3425, '美少女(IV)项链(1天)', '', '"封印着美少女灵魂的项链(*1需要解封项链栏*9),强烈的灵魂诅咒使得只有60级以上才能佩戴,佩带后将获得她那超快的攻击速度.

购买后开始计时间.

变身为美少女,*1HP+150 MP+90 负重+1250*9"', 200, 100, '2', '0', '0', 1, 1, 0, '2009-08-10 17:42:38.383', 'CN12654', '10.34.223.138', '2014-08-12 12:06:35.310', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10355, 2735, '高级服务礼券', '', '使用道具，将获得高级服务NPC', 0, 0, '1', '0', '0', 0, 1, 6, '2009-09-22 08:08:22.100', 'CN12654', '10.34.223.138', '2014-08-12 12:06:32.310', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10356, 3372, '高级副本入场券(一层)', '', '进入高级副本的凭证', 1, 0, '2', '0', '0', 7, 1, 0, '2009-09-22 08:08:24.927', 'CN12654', '10.34.118.177', '2014-08-12 12:06:33.467', 'CN12654', '10.34.118.177');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10357, 3422, '官方入场卷', '', '练功房入场券', 1, 0, '2', '1', '0', 7, 1, 6, '2009-09-22 08:08:24.757', 'CN12654', '10.34.118.177', '2014-08-12 12:06:33.420', 'CN12654', '10.34.118.177');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10358, 3422, '练功专用券', '', '双击左键使用后,可进入练功房。

道具使用时间为6小时.
*该道具有效期为7天,请在购买后7天内进行使用

使用后可按*1T键*9查询剩余时间.', 1, 0, '2', '0', '0', 7, 1, 6, '2009-09-22 08:08:21.710', 'CN12654', '10.34.223.138', '2014-08-12 12:06:33.263', 'CN12654', '10.34.118.170');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10362, 2070, '银币掉落增幅之咒(1.5倍)', '', '银币掉落增加到 1.5倍', 0, 0, '1', '0', '2', 0, 1, 3, '2010-02-01 14:20:58.307', 'CN99019', '10.34.118.180', '2014-08-12 12:06:26.903', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10363, 2069, '掉率增幅之咒(1.5倍)', '', '掉率增幅加至1.5倍', 0, 0, '1', '0', '2', 0, 1, 3, '2010-02-01 14:20:57.870', 'CN99019', '10.34.118.180', '2014-08-12 12:06:24.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10365, 1509, '柠檬(网吧)', '', '', 10, 5, '1', '0', '0', 0, 1, 10, '2010-02-09 10:44:35.037', 'CN99019', '10.34.118.165', '2014-08-12 12:06:33.077', 'CN99019', '10.34.118.165');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10367, 2735, '高级服务礼券', '', '使用道具, 将获得使用高级服务NPC的权限', 0, 0, '2', '0', '0', 0, 1, 168, '2010-02-09 14:47:36.837', 'CN99019', '10.34.118.165', '2014-08-12 12:06:32.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10368, 3372, '高级副本入场券(一层)(3天)', '', '高级副本入场券(一层) 效果持续时间3天', 600, 300, '4', '0', '1', 30, 1, 72, '2010-02-09 16:12:33.643', 'CN99019', '10.34.118.165', '2014-08-12 13:01:57.920', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10369, 3373, '高级副本入场券(二层)(30天) ', '', '高级副本入场券(二层) 效果持续时间30天', 1470, 735, '4', '0', '1', 30, 1, 720, '2010-02-09 16:12:33.753', 'CN99019', '10.34.118.165', '2014-08-12 13:01:58.670', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10370, 3372, '高级副本入场券(一层)(30天)', '', '高级副本入场券(一层) 效果持续时间30天', 1050, 525, '4', '0', '1', 30, 1, 720, '2010-02-09 16:12:33.690', 'CN99019', '10.34.118.165', '2014-08-12 13:19:36.840', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10371, 3373, '高级副本入场券(二层)(3天)', '', '高级副本入场券(二层) 效果持续时间3天', 840, 420, '4', '0', '1', 30, 1, 72, '2010-02-09 16:12:33.567', 'CN99019', '10.34.118.165', '2014-08-12 13:01:59.437', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10372, 2915, '礼花A', '', '342355', 8, 3, '3', '1', '0', 3, 1, 1, '2012-03-09 14:19:29.667', 'CN12835', '202.108.36.125', '2014-08-12 12:06:32.920', 'CN12835', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10380, 4871, '1元礼包测试', '', '', 100, 5, '1', '1', '0', 0, 1, 0, '2012-07-03 14:59:22.013', '', '61.152.133.230', '2014-08-12 12:06:32.200', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10381, 4875, '祝 v(＾O＾)v祝爆竹', '', '祝 v(＾O＾)v祝爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.607', '', '61.152.133.230', '2014-08-12 12:06:32.060', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10382, 4874, '祝爆竹', '', '', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.560', '', '61.152.133.230', '2014-08-12 12:06:29.967', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10383, 4876, '祝福の槍爆竹', '', '', 10, 5, '4', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.297', '', '61.152.133.230', '2014-08-12 12:06:32.107', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10384, 4872, 'T爆竹', '', 'T爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.513', '', '61.152.133.230', '2014-08-12 12:06:29.903', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10385, 4873, 'Y爆竹', '', 'Y爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.483', '', '61.152.133.230', '2014-08-12 12:06:29.857', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10386, 4868, '笑脸爆竹', '', '笑脸爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.437', '', '61.152.133.230', '2014-08-12 12:06:31.997', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10387, 4869, '招手的爆竹', '', '招手的爆竹', 0, 0, '4', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.390', '', '61.152.133.230', '2014-08-12 12:06:31.903', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10388, 4870, '生气的爆竹', '', '生气的爆竹', 0, 0, '1', '0', '0', 0, 10, 0, '2012-07-03 14:59:22.343', '', '61.152.133.230', '2014-08-12 12:06:31.950', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10389, 4871, 'P爆竹', '', '', 100, 5, '4', '0', '0', 0, 10, 0, '2012-07-20 18:47:10.217', '', '61.152.133.230', '2014-08-12 12:06:31.857', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10390, 1616, '冠军德拉克', '', '', 0, 0, '1', '0', '0', 30, 1, 0, '2012-07-03 16:50:14.717', '', '61.152.133.230', '2014-08-12 12:06:32.153', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10391, 40616, '英雄力量精华 Lv2[30天]', '', '英雄力量精华 Lv2[30天]', 0, 0, '1', '0', '0', 30, 1, 0, '2012-07-20 18:47:10.170', '', '61.152.133.230', '2014-08-12 12:06:30.873', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10392, 4871, 'P爆竹', '', '', 10, 5, '1', '0', '0', 0, 10, 0, '2012-07-20 18:47:09.983', '', '61.152.133.230', '2014-08-12 12:06:31.717', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10393, 2915, '礼花A', '', '', 10, 5, '1', '0', '0', 0, 10, 0, '2012-07-20 18:47:09.937', '', '61.152.133.230', '2014-08-12 12:06:31.670', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10395, 3389, '多能力之药水', '', '使用时，HP/MP上限提高100，持续时间17分钟，免疫驱散魔法道具及驱散技能', 8, 4, '2', '0', '1', 0, 1, 0, '2012-07-20 08:57:23.687', 'CN90053', '61.152.133.230', '2014-08-12 12:06:27.153', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10396, 3394, '负重之咒 I', '', '使用后负重上限提升500，与负重之秘药可重叠使用.持续时间 5分钟', 1, 0, '1', '0', '0', 0, 1, 0, '2012-07-20 10:02:07.683', 'CN90053', '61.152.133.230', '2014-08-18 13:57:41.340', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10397, 5388, '吉伽梅西战士-NG', '', '', 990, 495, '2', '0', '0', 7, 1, 0, '2012-07-20 12:18:54.153', 'CN90053', '61.152.133.230', '2014-08-12 12:06:31.060', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10400, 3396, '负重之咒 III', '', '使用后负重上限提升1500，与负重之秘药可重叠使用.持续时间 5分钟', 3, 1, '1', '0', '0', 0, 1, 0, '2012-07-20 18:47:09.327', 'CN90053', '61.152.133.230', '2014-08-12 12:06:21.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10401, 2431, '辨识之书', '', '爽辨识之术后点击未鉴定物品，可随时随地不限次数的鉴定物品，购买后开始计时，有效期30天', 350, 175, '2', '0', '0', 30, 1, 0, '2012-07-20 17:33:30.530', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10405, 4156, '月光精灵魔法师(IV)项链', '', '装着月光精灵魔法师灵魂的项链(需要解封项链栏，60级以上），HP+150 MP+90 负重+1250', 750, 375, '1', '0', '1', 7, 1, 0, '2012-07-20 18:47:09.013', 'CN90053', '61.152.133.230', '2014-08-12 12:13:06.687', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10406, 4156, '月光精灵魔法师(IV)项链(7日)', '', '装着月光精灵魔法师灵魂的项链(需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 750, 375, '1', '0', '1', 7, 1, 0, '2012-07-20 18:47:08.780', 'CN90053', '61.152.133.230', '2014-08-12 12:06:23.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10411, 4152, '月光精灵战士(IV)项链(7日)', '', '装着月光精灵魔法师战士的项链(需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 750, 375, '1', '0', '1', 7, 1, 0, '2012-07-20 18:47:08.560', 'CN90053', '61.152.133.230', '2014-08-12 12:06:24.653', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10412, 4150, '月光精灵战士(V)项链(7日)', '', '装着月光精灵魔法师战士的近战攻击变身项链(需要解封项链栏，63级以上），HP+250 MP+150 负重+1750', 990, 495, '1', '0', '0', 7, 1, 0, '2012-07-20 18:47:08.437', 'CN90053', '61.152.133.230', '2014-08-12 13:17:53.280', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10413, 4150, '月光精灵战士(V)项链(7日)', '', '装着月光精灵魔法师战士的近战攻击变身项链(需要解封项链栏，63级以上），HP+200 MP+120 负重+1750
', 990, 495, '1', '0', '1', 7, 1, 0, '2012-07-20 18:47:08.390', 'CN90053', '61.152.133.230', '2014-08-12 13:17:53.217', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10422, 4152, '月光精灵战士(IV)项链(30日)', '', '装着月光精灵魔法师战士的项链(需要解封项链栏，60级以上），HP+150 MP+90 负重+1250', 1810, 905, '1', '0', '1', 30, 1, 0, '2012-07-20 18:47:08.000', 'CN90053', '61.152.133.230', '2014-08-12 12:06:24.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10427, 3009, '月光复仇者项链(加强)(7日)', '', '封印着月光复仇者灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 750, 375, '1', '0', '0', 7, 1, 0, '2012-07-20 18:47:07.700', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.810', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10428, 3009, '月光复仇者项链(加强)(30日)', '', '封印着月光复仇者灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 2320, 905, '2', '0', '0', 30, 1, 0, '2012-07-20 18:47:07.640', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.860', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10429, 3011, '锥锋复仇者项链(加强)(7日)', '', '封印着锥锋复仇者灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 750, 375, '2', '0', '0', 7, 1, 0, '2012-07-20 18:47:07.607', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.703', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10430, 3011, '锥锋复仇者项链(加强)(30日)', '', '封印着锥锋复仇者灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250', 2300, 905, '1', '0', '0', 30, 1, 0, '2012-07-20 18:47:07.560', 'CN90053', '61.152.133.230', '2014-08-12 12:06:19.763', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10431, 2072, 'TESTNO', '', '活动礼包，购买后可获得增幅之咒(2倍)[3天]X2，负重之咒 III X20', 1740, 260, '4', '0', '0', 0, 1, 0, '2012-07-20 18:47:07.437', 'CN90053', '61.152.133.230', '2014-08-12 12:06:31.467', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10432, 2072, '掉率增幅之咒(2倍)[3天]', '', '活动礼包，购买后可获得增幅之咒(2倍)[3天]X2，负重之咒 III X20', 0, 0, '1', '0', '0', 0, 1, 72, '2012-07-20 18:58:44.593', 'CN90053', '61.152.133.230', '2014-08-12 12:06:31.420', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10433, 1825, '增幅之咒(3天)', '', '狩猎获得经验2倍', 20000, 10000, '1', '0', '0', 0, 1, 72, '2012-07-20 22:00:13.623', 'CN90053', '61.152.133.230', '2014-08-12 12:06:31.357', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10434, 2074, 'NG_test2', '', '活动限时发售道具，
打怪时获得的经验提高2倍.该效果可以替换掉低倍数"经验增幅之咒"的效果.经验增加效果与时间不累积.
', 200000, 100000, '4', '0', '0', 0, 1, 72, '2012-07-20 22:00:13.577', 'CN90053', '61.152.133.230', '2014-08-12 12:06:30.153', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10435, 5397, 'NGItem_01_TEST', '', '拥有吉伽梅西战士灵魂的项链近战(需要解封项链栏，66级以上），HP+250 MP+150 负重+1750', 200, 100, '2', '0', '0', 30, 1, 0, '2012-07-24 12:58:48.030', '', '61.152.133.230', '2014-08-12 12:06:30.920', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10441, 4160, '[体验]修炼少女V(1天)', '', '装有修炼少女灵魂的项链(需要解封项链栏，63级以上），HP+250 MP+150 负重+1750', 0, 0, '1', '0', '0', 1, 1, 0, '2013-11-20 16:47:12.060', 'CN90053', '222.128.29.241', '2014-08-12 12:06:30.607', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10442, 4151, '[体验]月光精灵魔法师V(1日）', '', '装着月光精灵魔法师灵魂的项链(需要解封项链栏，63级以上），HP+250 MP+150 负重+1750', 0, 0, '1', '0', '0', 1, 1, 0, '2013-11-20 16:47:11.967', 'CN90053', '222.128.29.241', '2014-08-12 12:06:30.560', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10443, 40619, '英雄力量精华 Lv3', '', 'TEST', 0, 0, '1', '0', '0', 30, 1, 0, '2014-01-11 15:06:12.187', 'CN90053', '222.128.29.241', '2014-08-12 12:06:30.200', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10444, 40619, '英雄力量精华 Lv3', '', '', 0, 0, '1', '0', '0', 30, 1, 0, '2014-04-22 19:21:20.857', 'CN90053', '124.205.178.150', '2014-08-12 12:06:29.763', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10445, 40620, '猎人敏捷精华 Lv3', '', '', 0, 0, '1', '0', '0', 30, 1, 0, '2014-06-03 10:57:40.187', 'CN90053', '124.205.178.150', '2014-08-12 12:06:29.700', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10446, 40621, '贤者智慧精华 Lv3', '', '', 0, 0, '1', '0', '0', 30, 1, 0, '2014-04-22 19:21:20.483', 'CN90053', '124.205.178.150', '2014-08-12 12:06:29.653', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10447, 6713, '至尊英雄力量精华 Lv3[30日]', '', '战争狂持有过的精华
力量+6 负重+360
攻击速度大幅度提高
移动速度大幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2014-04-22 19:21:20.077', 'CN90053', '124.205.178.150', '2014-08-12 12:06:28.247', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10448, 6715, '至尊贤者智慧精华 Lv3[30日]', '', '蕴藏着贤者智慧精华
智力+6 MP+24
攻击速度大幅度提高
移动速度大幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2014-05-13 15:06:57.857', 'CN90053', '124.205.178.150', '2014-08-12 12:06:28.013', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10449, 6717, '至尊猎人敏捷精华 Lv3[30日]', '', '蕴藏着猎人敏捷的精华
敏捷+6 远距离命中率+2
攻击速度小幅度提高
移动速度小幅度提高', 0, 0, '1', '0', '2', 30, 1, 0, '2014-05-13 15:06:56.983', 'CN90053', '124.205.178.150', '2014-08-12 12:06:28.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10450, 4178, '赫利肖舍里斯项链(30日)', '', '封印着赫利肖舍里斯灵魂的项链（需要解封项链栏，60级以上），HP+150 MP+90 负重+1250
', 1810, 905, '1', '0', '1', 30, 1, 0, '2014-06-03 15:58:28.733', 'CN90053', '124.205.178.150', '2014-08-12 12:06:28.450', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10451, 40609, '[竞猜-德国]基础精华', '', '[竞猜-德国]基础精华', 100, 44, '4', '0', '0', 0, 5, 0, '2014-07-10 19:35:26.967', 'CN90053', '124.205.178.150', '2014-08-12 12:06:21.360', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10453, 7788, '征服德拉克箱子(7日)', '', 'test', 20000, 10000, '2', '0', '1', 0, 1, 0, '2014-09-05 22:38:01.903', 'CN90053', '124.205.178.150', '2014-09-05 22:38:01.903', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10456, 5556, '天然芒果汁', '', '感觉全身的力量变的越来越强大.\n\n力量 +3\n敏捷 +3\n智力 +3', 20000, 10000, '1', '0', '2', 0, 1, 0, '2014-09-07 20:20:14.200', 'CN90053', '61.152.133.230', '2014-09-07 20:20:14.200', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10457, 5571, '天然芒果糖', '', '甜蜜的气息环绕在身边\n\n提供已升华的魔法盾, 石化皮肤\n加速,增加负重, 神圣铠甲\n祝福光环状态.', 20000, 10000, '1', '0', '2', 0, 1, 0, '2014-09-07 20:20:14.450', 'CN90053', '61.152.133.230', '2014-09-07 20:20:14.450', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10460, 5574, '英雄礼花', '', '使用后，*2力量+2，暴击几率增加，近距离命中率增加。*9，效果时间30分钟', 10, 5, '2', '0', '1', 0, 1, 0, '2014-09-13 12:24:30.827', 'CN90053', '61.152.133.230', '2014-09-13 12:24:30.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10461, 5575, '猎人礼花', '', '使用后，*2敏捷+2，暴击几率增加，远距离命中率增加。*9，效果时间30分钟', 8, 4, '2', '0', '1', 0, 1, 0, '2014-09-13 12:24:45.983', 'CN90053', '61.152.133.230', '2014-09-13 12:24:45.983', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10462, 5576, '贤者礼花', '', '使用后，*2智力+2，MP最大值增加，魔法命中率增加。*9，效果时间30分钟', 8, 4, '2', '0', '1', 0, 1, 0, '2014-09-13 12:25:00.950', 'CN90053', '61.152.133.230', '2014-09-13 12:25:00.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10466, 5896, '神佑之粉（武器)(1天)', '', '缠绕着神圣气息的粉末。\n使用后一定时间内自身携带的所有武器受到神圣气息的保护，角色死亡时武器不会掉落。', 880, 440, '2', '0', '1', 0, 1, 24, '2014-09-10 17:11:29.340', 'CN90053', '61.152.133.230', '2014-09-10 17:11:29.340', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10472, 5913, '免罪符', '', '可*6免杀人犯罪名*9的道具。在杀人犯状态下使用时，可解除杀人犯状态。', 60, 30, '2', '0', '1', 0, 1, 0, '2014-09-10 17:19:48.967', 'CN90053', '61.152.133.230', '2014-09-10 17:19:48.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10503, 6222, '[附魔Ⅱ]暗黑法师(Lv60)项链(7天)', '', '*1*1[远程]*9*9蕴含暗黑法师灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 991, 495, '2', '0', '1', 7, 1, 0, '2014-09-10 18:18:07.920', 'CN90053', '61.152.133.230', '2014-09-10 18:18:07.920', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10538, 6651, '经验增幅之咒(3倍)(6小时)', '', '打怪时获得的经验提高*33倍*9。持续*36小时*9。', 150, 75, '2', '0', '1', 0, 1, 6, '2014-09-10 17:10:33.983', 'CN90053', '61.152.133.230', '2014-09-10 17:10:33.983', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10540, 6653, '经验增幅之咒(4倍)(6小时)', '', '打怪时获得的经验提高*34倍*9。持续*36小时*9。', 220, 110, '2', '0', '1', 0, 1, 6, '2014-09-10 17:11:29.513', 'CN90053', '61.152.133.230', '2014-09-10 17:11:29.513', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10592, 7626, '高级礼花箱子', '', '装有*3各10个*9*1英雄/猎人/贤者的礼花*9的箱子。', 180, 90, '2', '0', '1', 30, 1, 0, '2014-09-10 12:51:26.310', 'CN90053', '61.152.133.230', '2014-09-10 12:51:26.310', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10612, 7791, '神秘德拉克幸运箱子', '', '开启箱子可随机获得*1神秘德拉克戒指10日、14日、21日、30日*1的其中一枚
', 220, 40, '2', '0', '1', 30, 1, 0, '2014-09-10 12:44:04.547', 'CN90053', '61.152.133.230', '2014-09-10 12:44:04.547', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10613, 7791, '神秘德拉克幸运箱子', '', '开启箱子可随机获得*1神秘德拉克戒指10日、14日、21日、30日*1的其中一枚，可合成奇美拉和格雷芬特的指环。
', 220, 40, '1', '0', '1', 30, 1, 0, '2014-09-10 17:38:22.547', 'CN90053', '124.205.178.150', '2014-09-10 17:38:22.547', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10614, 7631, '变身项链符文 Ⅲ 综合箱子(30天)', '', '装有*1力量III、敏捷III、智力III、HPIII、MPIII*95枚变身项链符文及*3 3个*9符文还原符、2个除槽炼金符的箱子。有效期30日。', 401, 200, '2', '0', '1', 30, 1, 0, '2014-09-11 12:41:22.153', 'CN90053', '124.205.178.150', '2014-09-11 12:41:22.153', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10629, 5912, '格林汉特的守护', '', '强化装备失败时,背包内如有格林汉特的守护,可以一定几率保护强化失败的装备。因保护气息不太稳定，强化等级会降1级', 680, 340, '2', '0', '1', 0, 1, 0, '2014-09-16 19:38:11.123', 'CN90053', '124.205.178.150', '2014-09-16 19:38:11.123', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10640, 6556, '阿努比斯(Lv60)项链(30天)', '', '*1[近战]*9装着阿努比斯灵魂的近战攻击变身项链(需要解封项链栏，*260级以上*9)，*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 1810, 740, '2', '0', '1', 30, 1, 0, '2014-09-18 14:14:16.700', 'CN90053', '124.205.178.150', '2014-09-18 14:14:16.700', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10641, 1594, '地下城入场券(30天)', '', '双击使用后，可进入地下城，快捷键"T"可确认剩余持续时间。', 360, 30, '3', '0', '1', 0, 1, 720, '2014-09-18 16:40:08.653', 'CN90053', '124.205.178.150', '2014-09-18 16:40:08.653', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10644, 7637, '腰带符文 Ⅱ 狩猎箱子(30天)', '', '装有*1负重 II、减伤 II*92枚腰带符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9，30天内不使用的话，箱子将自动删除。', 153, 51, '1', '0', '1', 30, 1, 0, '2014-11-05 11:42:27.560', 'CN90053', '124.205.178.150', '2014-11-05 11:42:27.560', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10645, 7636, '腰带符文 Ⅱ 攻击箱子(30天)', '', '装有*1所有攻击力 II、所有命中率 II*92枚腰带符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子，有效期*430日*930天内不使用的话，箱子将自动删除。', 203, 76, '4', '0', '1', 30, 1, 0, '2014-11-05 11:45:34.483', 'CN90053', '124.205.178.150', '2014-11-05 11:45:34.483', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10646, 7642, '戒指符文 Ⅱ 狩猎箱子(30天)', '', '装有*1防御 II、减伤 II、HP恢复 II、MP恢复 II*94枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*930天内不使用的话，箱子将自动删除。', 154, 52, '4', '0', '1', 30, 1, 0, '2014-11-05 11:56:28.233', 'CN90053', '124.205.178.150', '2014-11-05 11:56:28.233', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10647, 7641, '戒指符文 Ⅱ 暴击箱子(30天)', '', '装有*1暴击率 II、暴击伤害 II*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*930天内不使用的话，箱子将自动删除。', 254, 97, '4', '0', '1', 30, 1, 0, '2014-11-05 11:57:50.543', 'CN90053', '124.205.178.150', '2014-11-05 11:57:50.543', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10648, 7640, '戒指符文 Ⅱ 攻击箱子(30天)', '', '装有*1所有伤害 II、所有命中率 II*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9，30天内不使用的话，箱子将自动删除。', 204, 62, '4', '0', '1', 30, 1, 0, '2014-11-05 12:01:10.543', 'CN90053', '124.205.178.150', '2014-11-05 12:01:10.543', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10649, 7634, '德拉克戒指符文 Ⅳ 综合箱子(30天)', '', '装有蛛网术抵抗IV、麻痹抵抗IV、减速抵抗IV、药水破坏抵抗IV、陨石术抵抗IV5枚德拉克指环符文及3个符文还原符、2个除槽炼金符的箱子。有效期30日。', 202, 46, '4', '0', '1', 30, 1, 0, '2014-11-05 12:02:18.263', 'CN90053', '124.205.178.150', '2014-11-05 12:02:18.263', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10663, 7626, '高级礼花箱子', '', '高级礼花箱子: 装有*210个英雄/猎人/贤者的礼花*9的箱子。箱子中物品*6可交易*9，可以跟不同职业分享使用。', 260, 90, '3', '0', '1', 30, 1, 0, '2014-12-30 13:38:58.090', 'CN90053', '124.205.178.150', '2014-12-30 13:38:58.090', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10667, 50001, '[暴击]首饰符文礼包(30天)', '', '[暴击]首饰符文礼包(30天)
礼包内符文箱子原价*11614GOLD*9，礼包价*4701GOLD*9。
礼包内含有：*4 戒指符文 Ⅱ 暴击箱子x1,变身项链符文 Ⅲ 综合箱子x1*9,腰带符文 Ⅱ 攻击箱子(30天),神秘德拉克幸运箱子x1共5个,德拉克戒指符文 Ⅳ 综合箱子x1,打开所有箱子物品共计，*314枚*9符文、*312个*9符文还原符、*38个*9除槽炼金符、*31枚*9神秘德拉克指环。', 1614, 350, '1', '1', '1', 0, 1, 0, '2015-01-23 20:56:50.590', 'CN90053', '124.205.178.150', '2015-01-23 20:56:50.590', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10669, 50042, '至尊英雄力量精华LV3礼包', '', '至尊英雄力量精华含
*1英雄力量精华LV3(30日)*9 1个 
*4英雄力量精华LV3 攻击速度和移动速度效果最强*9
变身强化精华LV3（30日）1个
幸运精华LV3（30日）1个', 880, 440, '3', '1', '1', 0, 1, 0, '2015-01-25 12:10:17.607', 'CN90053', '124.205.178.150', '2015-01-25 12:10:17.607', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10670, 50038, '[攻击]首饰符文礼包(30日)', '', '[攻击]首饰符文礼包(30天)
礼包内符文箱子原价*11484GOLD*9，礼包价*4631GOLD*9。
礼包内含有：*4 戒指符文 Ⅱ 攻击箱子x1,变身项链符文 Ⅲ 综合箱子x1,腰带符文 Ⅱ 攻击箱子(30天)*9,神秘德拉克幸运箱子x1共5个,德拉克戒指符文 Ⅳ 综合箱子x1,打开所有箱子物品共计，*314枚*9符文、*312个*9符文还原符、*38个*9除槽炼金符、*31枚*9神秘德拉克指环。', 1484, 315, '1', '1', '1', 0, 1, 0, '2015-01-25 12:17:27.887', 'CN90053', '124.205.178.150', '2015-01-25 12:17:27.887', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10671, 7624, '[EVNT]高级补给箱子', '', '装有*3100个芒果糖，100个芒果汁和10个防御提升卷轴*9的箱子。', 20000, 10000, '4', '0', '2', 30, 1, 0, '2015-03-20 17:29:06.483', 'CN90053', '124.205.178.150', '2015-03-20 17:29:06.483', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10672, 6654, '[EVNT]经验增幅之咒（4.5倍）', '', '打怪时获得的*2经验提高4.5倍*9。', 20000, 10000, '4', '0', '2', 0, 1, 6, '2015-03-20 17:32:50.153', 'CN90053', '124.205.178.150', '2015-03-20 17:32:50.153', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10680, 2884, '陈.音库柏斯(Lv60)项链(30天)', '', '*1*1[近战]*9*9变身为陈.音库柏斯（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 1810, 705, '1', '0', '1', 30, 1, 0, '2015-06-30 10:41:20.547', 'CN90053', '114.112.126.218', '2015-06-30 10:41:20.547', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10690, 1980, '嗜血的吸血鬼(Lv1)项链(7天)', '', '*1*1[近战]*9*9变身为嗜血的吸血鬼（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 281, 140, '1', '0', '1', 7, 1, 0, '2015-09-01 11:51:11.733', 'CN90053', '114.112.126.218', '2015-09-01 11:51:11.733', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10691, 1980, '嗜血的吸血鬼(Lv1)项链(30天)', '', '*1*1[近战]*9*9变身为嗜血的吸血鬼（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 880, 275, '1', '0', '1', 30, 1, 0, '2015-09-01 11:51:20.810', 'CN90053', '114.112.126.218', '2015-09-01 11:51:20.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10692, 1981, '印谱诺(Lv1)项链(7天)', '', '*1*1[近战]*9*9变身为印谱诺（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 281, 140, '1', '0', '1', 7, 1, 0, '2015-09-01 11:50:52.590', 'CN90053', '114.112.126.218', '2015-09-01 11:50:52.590', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10693, 1981, '印谱诺(Lv1)项链(30天)', '', '*1*1[近战]*9*9变身为印谱诺（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 880, 275, '1', '0', '1', 30, 1, 0, '2015-09-01 11:51:04.090', 'CN90053', '114.112.126.218', '2015-09-01 11:51:04.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10694, 2027, '圣甲战魔(Lv1)项链(7天)', '', '*1*1[远程]*9*9变身为圣甲战魔（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9
', 281, 140, '1', '0', '1', 7, 1, 0, '2015-09-01 11:51:27.390', 'CN90053', '114.112.126.218', '2015-09-01 11:51:27.390', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10695, 2027, '圣甲战魔(Lv1)项链(30天)', '', '*1*1[远程]*9*9变身为圣甲战魔（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 880, 275, '1', '0', '1', 30, 1, 0, '2015-09-01 11:51:34.700', 'CN90053', '114.112.126.218', '2015-09-01 11:51:34.700', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10696, 4155, '月光精灵战士(Lv1)项链(7天)', '', '*1*1*1[近战]*9*9*9变身为月光精灵战士（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 281, 140, '1', '0', '1', 7, 1, 0, '2015-09-01 11:51:41.090', 'CN90053', '114.112.126.218', '2015-09-01 11:51:41.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10697, 4155, '月光精灵战士(Lv1)项链(30天)', '', '*1*1*1[近战]*9*9*9变身为月光精灵战士（需要解封项链栏，*21级以上*9），*3HP+50 MP+30 攻击速度和移动速度提升 负重+300*9', 800, 275, '1', '0', '1', 30, 1, 0, '2015-09-01 11:51:57.903', 'CN90053', '114.112.126.218', '2015-09-01 11:51:57.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10699, 1602, '装备解封之咒(5个槽)', '', '使用后*2开启装备栏中5个被封印窗口*9，从而可佩戴*6两个戒指，项链，腰带和披风*9。', 200, 0, '3', '0', '1', 0, 1, 720, '2015-09-01 11:49:21.810', 'CN90053', '114.112.126.218', '2015-09-01 11:49:21.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10700, 2033, '月光复仇者(Lv54)项链(7天)', '', '*1*1[近战]*9*9变身为月光复仇者（需要解封项链栏，*254级以上*9），*3HP+75 MP+45 攻击速度和移动速度提升 负重+500*9', 450, 190, '1', '0', '1', 7, 1, 0, '2015-09-01 12:46:32.297', 'CN90053', '114.112.126.218', '2015-09-01 12:46:32.297', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10701, 2039, '锥锋复仇者(Lv54)项链(7天)', '', '*1*1[远程]*9*9变身为锥锋复仇者（需要解封项链栏，*254级以上*9），*3HP+75 MP+45 攻击速度和移动速度提升 负重+500*9', 450, 190, '1', '0', '1', 7, 1, 0, '2015-09-01 12:47:00.903', 'CN90053', '114.112.126.218', '2015-09-01 12:47:00.903', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10702, 2884, '陈.音库柏斯(Lv60)项链(7天)', '', '*1*1[近战]*9*9变身为陈.音库柏斯（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 650, 275, '1', '0', '1', 7, 1, 0, '2015-09-01 12:47:38.090', 'CN90053', '114.112.126.218', '2015-09-01 12:47:38.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10703, 2884, '陈.音库柏斯(Lv60)项链(30天)', '', '*1*1[近战]*9*9变身为陈.音库柏斯（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 1810, 625, '1', '0', '1', 30, 1, 0, '2015-09-01 12:48:03.280', 'CN90053', '114.112.126.218', '2015-09-01 12:48:03.280', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10704, 3012, '威尔斯沃琪(Lv60)项链(7天)', '', '*1*1[远程]*9*9变身为威尔斯沃琪（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 650, 275, '1', '0', '1', 7, 1, 0, '2015-09-01 12:48:28.607', 'CN90053', '114.112.126.218', '2015-09-01 12:48:28.607', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10705, 3012, '威尔斯沃琪(Lv60)项链(30天)', '', '*1*1[远程]*9*9变身为威尔斯沃琪（需要解封项链栏，*260级以上*9），*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9', 1810, 625, '1', '0', '1', 30, 1, 0, '2015-09-01 12:49:06.700', 'CN90053', '114.112.126.218', '2015-09-01 12:49:06.700', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10706, 4148, '芭芭莉安(Lv63)项链(30天)', '', '*1*1[近战]*9*9装有着芭芭莉安灵魂的项链 变身为暗黑法师(需要解封项链栏，*263级以上*9)，*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9', 2100, 675, '1', '0', '1', 30, 1, 0, '2015-09-01 12:49:31.327', 'CN90053', '114.112.126.218', '2015-09-01 12:49:31.327', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10707, 4149, '暗黑法师(Lv63)项链(30天)', '', '*1*1[远程]*9*9装有着暗黑法师灵魂的项链 变身为暗黑法师(需要解封项链栏，*263级以上*9)，*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9', 2100, 675, '1', '0', '1', 30, 1, 0, '2015-09-01 12:49:47.310', 'CN90053', '114.112.126.218', '2015-09-01 12:49:47.310', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10708, 4150, '[进击]月光精灵战士(Lv63)项链(30天)', '', '*1*1[近战]*9*9装着进击的月光精灵战士灵魂的近战攻击项链，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 845, '3', '0', '1', 30, 1, 0, '2015-09-01 12:51:07.390', 'CN90053', '114.112.126.218', '2015-09-01 12:51:07.390', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10709, 4151, '[进击]月光精灵魔法师(Lv63)项链(30天)', '', '*1*1[远程]*9*9装着进击的月光精灵魔法师灵魂的远程攻击变身项链(需要解封项链栏，*263级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 845, '3', '0', '1', 30, 1, 0, '2015-09-01 12:51:29.373', 'CN90053', '114.112.126.218', '2015-09-01 12:51:29.373', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10710, 4160, '[进击]修炼少女 (Lv63) 项链(30天)', '', '*1*1[近战]*9*9装着进击的修炼少女的近战攻击项链(需要解封项链栏，*263级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 845, '3', '0', '1', 30, 1, 0, '2015-09-01 12:52:06.153', 'CN90053', '114.112.126.218', '2015-09-01 12:52:06.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10712, 4161, '[进击]东瀛少女 (Lv63) 项链(30天)', '', '*1*1[近战]*9*9装着进击的修炼少女灵魂的近战攻击项链(需要解封项链栏，*263级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 845, '3', '0', '1', 30, 1, 0, '2015-09-01 12:52:37.810', 'CN90053', '114.112.126.218', '2015-09-01 12:52:37.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10713, 5397, '吉伽梅西战士(Lv66)项链(30天)', '', '*1*1[近战]*9*9拥有吉伽梅西战士灵魂的项链，近战(需要解封项链栏，*266级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 2610, 775, '1', '0', '1', 30, 1, 0, '2015-09-01 12:52:52.077', 'CN90053', '114.112.126.218', '2015-09-01 12:52:52.077', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10714, 5398, '吉伽梅西弓箭手(Lv66)项链(30天)', '', '*1*1[远程]*9*9拥有吉伽梅西弓箭手灵魂的项链，远程(需要解封项链栏，*266级以上*9)，*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9', 3280, 775, '1', '0', '1', 30, 1, 0, '2015-09-01 12:53:30.090', 'CN90053', '114.112.126.218', '2015-09-01 12:53:30.090', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10719, 7608, '[礼包用]幸运精华箱子中级', '', '[礼包用]幸运精华箱子中级(30天)', 20000, 10000, '1', '0', '2', 30, 1, 0, '2015-09-07 16:39:35.140', 'CN90053', '114.112.126.218', '2015-09-07 16:39:35.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10720, 6717, '[礼包用]猎人敏捷精华LV3', '', '[礼包用]猎人敏捷精华LV3(30天)', 20000, 10000, '1', '0', '2', 30, 1, 0, '2015-09-07 17:11:51.153', 'CN90053', '114.112.126.218', '2015-09-07 17:11:51.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10721, 40605, '[礼包用]变身强化精华LV3', '', '[礼包用]变身强化精华LV3（30天）', 0, 0, '1', '0', '2', 30, 1, 0, '2015-09-07 17:43:28.357', 'CN90053', '114.112.126.218', '2015-09-07 17:43:28.357', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10723, 50004, '[中级幸运]猎人精华礼包', '', '[中级幸运]猎人精华礼包含
*1猎人敏捷精华LV3(30日)*9 1个 
*2变身强化精华LV3（30日）*9 1个
*3幸运精华箱子中级 (30天)*9 1个
(最高可获得*4幸运精华 Lv6*9)', 1380, 480, '3', '1', '1', 0, 1, 0, '2015-09-07 18:27:11.467', 'CN90053', '114.112.126.218', '2015-09-07 18:27:11.467', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10726, 6713, '[礼包用]英雄力量精华LV3', '', '[礼包用]英雄力量精华LV3(30天)', 20000, 10000, '1', '0', '2', 30, 1, 0, '2015-09-07 19:22:14.467', 'CN90053', '114.112.126.218', '2015-09-07 19:22:14.467', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10727, 50014, '[中级幸运]英雄精华礼包', '', '[中级幸运]英雄精华礼包含
*1英雄力量精华LV3(30日)*9 1个 
*2变身强化精华LV3（30日）*9 1个
*3幸运精华箱子中级 (30天)*9 1个
(最高可获得*4幸运精华 Lv6*9', 1380, 480, '3', '1', '1', 0, 1, 0, '2015-09-07 19:29:26.920', 'CN90053', '114.112.126.218', '2015-09-07 19:29:26.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10728, 6715, '[礼包用]贤者智慧精华LV3', '', '[礼包用]贤者智慧精华LV3（30天）', 20000, 10000, '1', '0', '2', 30, 1, 0, '2015-09-08 10:57:02.373', 'CN90053', '114.112.126.218', '2015-09-08 10:57:02.373', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10729, 50003, '[中级幸运]贤者精华礼包', '', '[中级幸运]贤者精华礼包含
*1贤者智慧精华LV3(30日)*9 1个 
*2变身强化精华LV3（30日）*9 1个
*3幸运精华箱子中级 (30天)*9 1个
(最高可获得*4幸运精华 Lv6*9)', 1380, 480, '3', '1', '1', 0, 1, 0, '2015-09-08 11:04:53.810', 'CN90053', '114.112.126.218', '2015-09-08 11:04:53.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10730, 3353, '混沌点券充值卡 700P', '', '充值混沌点数的物品*1700 点*9,可以在各个城镇的*2混沌积分商人*9处购买*3战利品*9及其他重要道具。', 35, 17, '2', '0', '1', 0, 1, 0, '2015-09-14 12:24:22.653', 'CN90053', '114.112.126.218', '2015-09-14 12:24:22.653', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10731, 3363, '混沌点券充值卡 4000P', '', '充值混沌点数的物品*14000 点*9 充值,可以在各个城镇的*2混沌积分商人*9处购买*3战利品*9及其他重要道具。', 200, 97, '3', '0', '1', 0, 1, 0, '2015-09-14 12:31:16.547', 'CN90053', '114.112.126.218', '2015-09-14 12:31:16.547', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10785, 6199, '[附魔I]阿努比斯(Lv60)项链(7天)', '', '*1*1[近战]*9*9蕴含阿努比斯灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 991, 380, '1', '0', '1', 7, 1, 0, '2015-09-19 21:01:29.577', 'CN90053', '114.112.126.218', '2015-09-19 21:01:29.577', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10786, 6199, '[附魔I]阿努比斯(Lv60)项链(30天)', '', '*1*1[近战]*9*9蕴含阿努比斯灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 2181, 680, '1', '0', '1', 30, 1, 0, '2015-09-19 21:06:00.123', 'CN90053', '114.112.126.218', '2015-09-19 21:06:00.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10787, 6222, '[附魔Ⅱ]暗黑法师(Lv60)项链(7天)', '', '*1*1[远程]*9*9蕴含暗黑法师灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 991, 480, '1', '0', '1', 7, 1, 0, '2015-09-19 21:09:23.327', 'CN90053', '114.112.126.218', '2015-09-19 21:09:23.327', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10788, 6222, '[附魔Ⅱ]暗黑法师(Lv60)项链(30天)', '', '*1*1[远程]*9*9蕴含暗黑法师灵魂的项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 2381, 735, '1', '0', '1', 30, 1, 0, '2015-09-19 21:10:37.840', 'CN90053', '114.112.126.218', '2015-09-19 21:10:37.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10789, 6196, '[附魔Ⅱ]忍者少女(Lv60)项链(7天)', '', '*1*1[近战]*9*9赋予特殊能力的忍者少女变身项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 991, 435, '1', '0', '1', 7, 1, 0, '2015-09-19 21:13:43.263', 'CN90053', '114.112.126.218', '2015-09-19 21:13:43.263', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10790, 6196, '[附魔Ⅱ]忍者少女(Lv60)项链(30天)', '', '*1*1[近战]*9*9赋予特殊能力的忍者少女变身项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 2381, 735, '1', '0', '1', 30, 1, 0, '2015-09-19 21:14:45.890', 'CN90053', '114.112.126.218', '2015-09-19 21:14:45.890', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10791, 6206, '[附魔I]阿修罗(Lv63)项链(7天)', '', '*1*1[近战]*9*9赋予特殊能力的阿修罗变身项链。拥有*263级*9的变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 1171, 480, '1', '0', '1', 7, 1, 0, '2015-09-20 13:33:49.670', 'CN90053', '114.112.126.218', '2015-09-20 13:33:49.670', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10792, 6206, '[附魔I]阿修罗(Lv63)项链(30天)', '', '*1*1[近战]*9*9赋予特殊能力的阿修罗变身项链。拥有*263级*9的变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 2371, 730, '1', '0', '1', 30, 1, 0, '2015-09-20 13:34:33.170', 'CN90053', '114.112.126.218', '2015-09-20 13:34:33.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10793, 6203, '[附魔Ⅱ]春丽(Lv63)项链(7天)', '', '*1*1[近战]*9*9赋予特殊能力的春丽变身项链。拥有*263级*9的变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 1271, 535, '1', '0', '1', 7, 1, 0, '2015-09-20 13:36:09.827', 'CN90053', '114.112.126.218', '2015-09-20 13:36:09.827', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10794, 6203, '[附魔Ⅱ]春丽(Lv63)项链(30天)', '', '*1*1[近战]*9*9赋予特殊能力的春丽变身项链。拥有*263级*9的变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+2 敏捷+2 智力+2*9', 2571, 785, '1', '0', '1', 30, 1, 0, '2015-09-20 13:37:11.140', 'CN90053', '114.112.126.218', '2015-09-20 13:37:11.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10795, 6226, '[附魔Ⅲ]吉伽梅西弓箭手(Lv63)项链(7天)', '', '*1*1[远程]*9*9蕴含吉伽梅西弓箭手灵魂的项链。拥有*263级*9变身能力值*4力量+3 敏捷+3 智力+3*9', 1581, 640, '2', '0', '1', 7, 1, 0, '2015-09-20 13:39:42.013', 'CN90053', '114.112.126.218', '2015-09-20 13:39:42.013', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10796, 6226, '[附魔Ⅲ]吉伽梅西弓箭手(Lv63)项链(30天)', '', '*1*1[远程]*9*9蕴含吉伽梅西弓箭手灵魂的项链。拥有*263级*9变身能力值*4力量+3 敏捷+3 智力+3*9', 2781, 840, '2', '0', '1', 30, 1, 0, '2015-09-20 13:41:05.827', 'CN90053', '114.112.126.218', '2015-09-20 13:41:05.827', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10799, 6227, '[附魔I]赫利肖舍里斯变身(Lv66)项链(7天)', '', '*1*1[远程]*9*9蕴含赫利肖舍里斯灵魂的项链。拥有*266级*9变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 1481, 475, '2', '0', '1', 7, 1, 0, '2015-09-20 14:07:13.280', 'CN90053', '114.112.126.218', '2015-09-20 14:07:13.280', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10800, 6227, '[附魔I]赫利肖舍里斯变身(Lv66)项链(30天)', '', '*1*1[远程]*9*9蕴含赫利肖舍里斯灵魂的项链。拥有*266级*9变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 2781, 830, '2', '0', '1', 30, 1, 0, '2015-09-20 14:08:09.297', 'CN90053', '114.112.126.218', '2015-09-20 14:08:09.297', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10801, 6213, '[附魔I]武士(Lv66)项链(7天)', '', '*1*1[近战]*9*9赋予特殊能力的武士变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 1481, 475, '1', '0', '1', 7, 1, 0, '2015-09-20 14:33:57.140', 'CN90053', '114.112.126.218', '2015-09-20 14:33:57.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10802, 6213, '[附魔I]武士(Lv66)项链(30天)', '', '*1*1[近战]*9*9赋予特殊能力的武士变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+1 敏捷+1 智力+1*9', 2781, 830, '1', '0', '1', 30, 1, 0, '2015-09-20 14:34:59.123', 'CN90053', '114.112.126.218', '2015-09-20 14:34:59.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10803, 7537, '女巫(Lv66)项链(30天)', '', '*1[近战]*9蕴含女巫的变身项链。赋予*266级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。', 2881, 775, '2', '0', '1', 30, 1, 0, '2015-09-20 14:39:07.233', 'CN90053', '114.112.126.218', '2015-09-20 14:39:07.233', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10808, 6558, '角斗士(Lv69)项链(30天)', '', '*1*1[近战]*9*9变身为角斗士（需要解封项链栏，*269级以上*9），*3HP+300 MP+180 攻击速度和移动速度提升 负重+2000*9', 2981, 875, '1', '0', '1', 30, 1, 0, '2015-09-20 15:38:59.373', 'CN90053', '114.112.126.218', '2015-09-20 15:38:59.373', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10810, 6559, '地狱领主(Lv72)项链(30天)', '', '[近战]蕴含地狱领主灵魂的项链。赋予72级变身HP/MP/负重/攻击速度/移动速度等能力值的变身项链。', 3581, 1025, '1', '0', '1', 30, 1, 0, '2015-09-20 15:41:52.797', 'CN90053', '114.112.126.218', '2015-09-20 15:41:52.797', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10811, 7539, '吉伽梅西弓箭手(Lv69)项链（30天）', '', '[远程]蕴含吉伽梅西弓箭手的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。', 2981, 875, '1', '0', '1', 30, 1, 0, '2015-09-22 11:53:22.107', 'CN90053', '114.112.126.218', '2015-09-22 11:53:22.107', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10813, 7540, '[附魔Ⅱ]修道士(Lv69)项链(30天)', '', '*1[近战]*9蕴含修道士的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 3381, 985, '2', '0', '1', 30, 1, 0, '2015-09-22 12:15:57.467', 'CN90053', '114.112.126.218', '2015-09-22 12:15:57.467', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10814, 7540, '[附魔Ⅱ]修道士(Lv69)(7天)', '', '*1[近战]*9蕴含修道士的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 1671, 735, '2', '0', '1', 7, 1, 0, '2015-09-22 17:18:08.750', 'CN90053', '114.112.126.218', '2015-09-22 17:18:08.750', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10815, 7542, '[附魔Ⅱ]女王骑士(Lv69)项链(7天)', '', '*1[近战]*9蕴含女王骑士的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 1671, 735, '2', '0', '1', 7, 1, 0, '2015-09-22 17:23:25.920', 'CN90053', '114.112.126.218', '2015-09-22 17:23:25.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10816, 7542, '[附魔Ⅱ]女王骑士(Lv69)项链(30天) ', '', '*1[近战]*9蕴含女王骑士的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 3381, 985, '2', '0', '1', 30, 1, 0, '2015-09-22 17:27:03.607', 'CN90053', '114.112.126.218', '2015-09-22 17:27:03.607', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10817, 7822, 'test_[进击][附魔I]火枪手(Lv75', '', '', 1991, 795, '2', '0', '1', 13, 1, 0, '2015-12-04 16:14:01.873', 'CN90053', '114.112.126.218', '2015-12-04 16:14:01.873', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10818, 2073, 'test_银币掉落增幅之咒(2倍)', '', '双及使用后，狩猎获得银币量为原由银币的*22倍*9，快捷键"T"可确认使用状态', 263, 80, '2', '0', '1', 0, 1, 24, '2015-12-04 18:33:37.030', 'CN90053', '114.112.126.218', '2015-12-04 18:33:37.030', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10819, 3395, '[test]负重之咒 II', '', '使用后*2负重上升1000*9，与负重之秘药可重叠使用。持续时间 *45分钟*9', 2, 1, '2', '0', '1', 0, 1, 0, '2015-12-04 18:53:40.140', 'CN90053', '114.112.126.218', '2015-12-04 18:53:40.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10820, 7041, '[test][附魔Ⅲ]狂战士(Lv75)项链', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 2581, 1290, '2', '0', '1', 30, 1, 0, '2015-12-04 18:58:06.153', 'CN90053', '114.112.126.218', '2015-12-04 18:58:06.153', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10821, 7607, '[test]幸运精华箱子高级 (7天)', '', '装有幸运精华的箱子。开启箱子时，可随机获得*1幸运精华Lv6、Lv7*9其中的一个。*47日*9。', 210, 49, '2', '0', '1', 7, 1, 0, '2015-12-04 19:03:52.077', 'CN90053', '114.112.126.218', '2015-12-04 19:03:52.077', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10842, 6205, '[附魔Ⅲ]炼金术士(Lv60)(30天)', '', '*1[近战]*9赋予特殊能力的炼金术士变身项链。拥有*260级*9变身能力*3HP+150 MP+90 攻击速度和移动速度提升 负重+1250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3481, 790, '2', '0', '1', 30, 1, 0, '2016-03-22 11:43:04.200', 'CN90053', '114.112.126.218', '2016-03-22 11:43:04.200', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10843, 6207, '[附魔Ⅲ]女巫(Lv63)(30天)', '', '*1[近战]*9蕴含女巫灵魂的项链。拥有*263级*9变身能力值*3HP+200 MP+120 攻击速度和移动速度提升 负重+1500*9及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 840, '2', '0', '1', 30, 1, 0, '2016-03-22 11:43:04.140', 'CN90053', '114.112.126.218', '2016-03-22 11:43:04.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10844, 7038, '[附魔Ⅲ]恶魔追猎者(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔追猎者变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3881, 940, '2', '0', '1', 30, 1, 0, '2016-03-22 11:43:04.093', 'CN90053', '114.112.126.218', '2016-03-22 11:43:04.093', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10845, 7039, '[附魔Ⅲ]大力神(Lv69)(30天)', '', '*1[近战]*9赋予特殊能力的大力神变身项链。拥有*269级*9的变身能力值*3HP+300 MP+180 攻击速度和移动速度提升 负重+2000*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4081, 1040, '2', '0', '1', 30, 1, 0, '2016-03-22 11:43:04.030', 'CN90053', '114.112.126.218', '2016-03-22 11:43:04.030', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10868, 2073, '银币掉落增幅之咒(2倍)(1天)', '', '双及使用后，狩猎获得银币量为原由银币的*22倍*9，快捷键"T"可确认使用状态', 280, 34, '2', '0', '1', 0, 1, 24, '2016-04-21 12:27:53.890', 'CN90053', '114.112.126.218', '2016-04-21 12:27:53.890', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10877, 7789, '[活动]征服德拉克箱子(30日)', '', '装有*12个德拉克神秘戒指*9、*11个封印的奇美拉卷轴、封印的格雷芬特卷轴*9的箱子。有效期限为*430天*9', 880, 120, '2', '0', '1', 30, 1, 0, '2016-04-21 16:10:29.217', 'CN90053', '114.112.126.218', '2016-04-21 16:10:29.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10878, 6660, '[活动]经验增幅道具宝箱（中级）', '', '装着*1经验增幅之咒*9的箱子。打开可随机获得*33倍~6倍*9其中的一个。，人品爆棚可获得*46小时*9的*36倍*9经验增幅之咒', 380, 89, '2', '0', '1', 0, 1, 0, '2016-04-21 16:11:44.123', 'CN90053', '114.112.126.218', '2016-04-21 16:11:44.123', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10879, 2073, '银币掉落增幅之咒(2倍)(1天)', '', '双及使用后，狩猎获得银币量为原由银币的*22倍*9，快捷键"T"可确认使用状态', 280, 34, '2', '0', '1', 0, 1, 24, '2016-04-21 16:12:52.310', 'CN90053', '114.112.126.218', '2016-04-21 16:12:52.310', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10881, 7816, '[进击Ⅱ][附魔Ⅱ]名妓黄真伊(Lv66)(30天)', '', '*1[近战]*9蕴含名妓黄真伊灵魂的项链,赋予超强近战攻击能力。拥有*266级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 4381, 1245, '2', '0', '1', 30, 1, 0, '2016-04-28 10:02:34.310', 'CN90053', '114.112.126.218', '2016-04-28 10:02:34.310', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10882, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)(30天)', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+3 敏捷+3 智力+3*9', 4081, 1090, '2', '0', '1', 30, 1, 0, '2016-04-28 10:02:42.543', 'CN90053', '114.112.126.218', '2016-04-28 10:02:42.543', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10883, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 1190, '2', '0', '1', 30, 1, 0, '2016-04-28 10:02:50.780', 'CN90053', '114.112.126.218', '2016-04-28 10:02:50.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10884, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4481, 1290, '2', '0', '1', 30, 1, 0, '2016-04-28 10:02:57.403', 'CN90053', '114.112.126.218', '2016-04-28 10:02:57.403', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10885, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 4581, 1345, '2', '0', '1', 30, 1, 0, '2016-04-28 10:03:05.920', 'CN90053', '114.112.126.218', '2016-04-28 10:03:05.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10886, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5900, 1495, '2', '0', '1', 30, 1, 0, '2016-04-28 10:03:12.390', 'CN90053', '114.112.126.218', '2016-04-28 10:03:12.390', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10887, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5400, 1390, '2', '0', '1', 30, 1, 0, '2016-04-28 10:04:33.950', 'CN90053', '114.112.126.218', '2016-04-28 10:04:33.950', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10890, 2800, '英雄箱子', '', '开启英雄箱子可随机获得+0Lv4精华或基础精华10个。', 2900, 640, '2', '0', '1', 0, 1, 0, '2016-05-03 12:25:00.403', 'CN90053', '114.112.126.218', '2016-05-03 12:25:00.403', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10891, 7816, '[进击Ⅱ][附魔Ⅱ]名妓黄真伊(Lv66)(30天)', '', '*1[近战]*9蕴含名妓黄真伊灵魂的项链,赋予超强近战攻击能力。拥有*266级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 4800, 1245, '2', '0', '1', 30, 1, 0, '2016-05-24 10:48:46.793', 'CN90053', '114.112.126.218', '2016-05-24 10:48:46.793', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10892, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)(30天)', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+3 敏捷+3 智力+3*9', 4200, 1090, '2', '0', '1', 30, 1, 0, '2016-05-24 10:50:52.187', 'CN90053', '114.112.126.218', '2016-05-24 10:50:52.187', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10893, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4600, 1190, '2', '0', '1', 30, 1, 0, '2016-05-24 10:52:51.590', 'CN90053', '114.112.126.218', '2016-05-24 10:52:51.590', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10894, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5000, 1290, '2', '0', '1', 30, 1, 0, '2016-05-24 10:54:56.623', 'CN90053', '114.112.126.218', '2016-05-24 10:54:56.623', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10895, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5200, 1345, '2', '0', '1', 30, 1, 0, '2016-05-24 10:58:01.030', 'CN90053', '114.112.126.218', '2016-05-24 10:58:01.030', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10896, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5800, 1495, '2', '0', '1', 30, 1, 0, '2016-05-24 11:01:12.250', 'CN90053', '114.112.126.218', '2016-05-24 11:01:12.250', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10897, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5400, 1390, '2', '0', '1', 30, 1, 0, '2016-05-24 11:02:43.047', 'CN90053', '114.112.126.218', '2016-05-24 11:02:43.047', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10898, 7643, '戒指符文 Ⅲ 攻击箱子(30天)', '', '装有*1所有伤害 III、所有命中率 III*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 404, 102, '1', '0', '1', 30, 1, 0, '2016-05-24 11:04:39.140', 'CN90053', '114.112.126.218', '2016-05-24 11:04:39.140', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10899, 7644, '戒指符文 Ⅲ 暴击箱子(30天)', '', '装有*1暴击率 III、暴击伤害 III*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 504, 142, '2', '0', '1', 30, 1, 0, '2016-05-24 12:03:58.170', 'CN90053', '114.112.126.218', '2016-05-24 12:03:58.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10900, 7638, '腰带符文 Ⅲ 攻击箱子(30天)', '', '装有*1所有攻击力 III、所有命中率 III*92枚腰带符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 403, 91, '2', '0', '1', 30, 1, 0, '2016-05-24 11:08:03.000', 'CN90053', '114.112.126.218', '2016-05-24 11:08:03.000', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10901, 6548, '[活动]除槽炼金符[10个]', '', '装有10个除槽炼金符的箱子。开启箱子，可获得*110个除槽炼金符*9。', 200, 19, '2', '0', '1', 0, 1, 0, '2016-06-07 11:09:42.937', 'CN90053', '106.38.71.30', '2016-06-07 11:09:42.937', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10902, 6549, '[活动]符文还原符[10个]', '', '装有10个符文还原符的箱子。开启箱子，可获得*110个符文还原符*9。', 100, 14, '2', '0', '1', 0, 1, 0, '2016-06-07 11:10:50.640', 'CN90053', '106.38.71.30', '2016-06-07 11:10:50.640', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10903, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 160, '2', '0', '1', 0, 5, 0, '2016-06-07 11:12:38.793', 'CN90053', '106.38.71.30', '2016-06-07 11:12:38.793', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10904, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-06-07 11:14:04.013', 'CN90053', '106.38.71.30', '2016-06-07 11:14:04.013', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10905, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-06-07 11:15:10.780', 'CN90053', '106.38.71.30', '2016-06-07 11:15:10.780', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10906, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-06-07 11:16:15.903', 'CN90053', '106.38.71.30', '2016-06-07 11:16:15.903', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10907, 6638, '[限时]战靴专用符文箱子（中级）[5个]', '', '装有*45*9个*1战靴专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-06-07 11:17:23.310', 'CN90053', '106.38.71.30', '2016-06-07 11:17:23.310', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10908, 6640, '[限时]披风专用符文箱子（中级）[5个]', '', '装有*45*9个*1披风专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 140, '2', '0', '1', 0, 5, 0, '2016-06-07 11:18:38.793', 'CN90053', '106.38.71.30', '2016-06-07 11:18:38.793', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10909, 6642, '[限时]辅助装备专用符文箱子（中级)[5个]', '', '装有*45*9个*1辅助装备专用符文*9的箱子。。开启有几率获得*6IV级符文*9。', 1900, 140, '2', '0', '1', 0, 5, 0, '2016-06-07 11:19:54.623', 'CN90053', '106.38.71.30', '2016-06-07 11:19:54.623', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10910, 7609, '幸运精华箱子高级 (30天)', '', '装有幸运精华的箱子。开启箱子时，可随机获得*1幸运精华Lv6、Lv7*9其中的一个。*430日*9。', 562, 196, '2', '0', '1', 30, 1, 0, '2016-06-28 10:54:54.293', 'CN90053', '106.38.71.30', '2016-06-28 10:54:54.293', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10911, 7633, '变身项链符文 V 综合箱子(30天)', '', '装有*1力量V、敏捷V、智力V、HPV、MPV*95种符文及3个符文还原符、2个除槽炼金符的箱子。有效期30日。', 1600, 400, '2', '0', '1', 30, 1, 0, '2016-06-28 10:55:37.543', 'CN90053', '106.38.71.30', '2016-06-28 10:55:37.543', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10912, 7816, '[进击Ⅱ][附魔Ⅱ]名妓黄真伊(Lv66)(30天)', '', '*1[近战]*9蕴含名妓黄真伊灵魂的项链,赋予超强近战攻击能力。拥有*266级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 4900, 1245, '2', '0', '1', 30, 1, 0, '2016-06-28 10:59:04.263', 'CN90053', '106.38.71.30', '2016-06-28 10:59:04.263', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10913, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)(30天)', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+3 敏捷+3 智力+3*9', 4200, 1090, '2', '0', '1', 30, 1, 0, '2016-06-28 10:59:10.420', 'CN90053', '106.38.71.30', '2016-06-28 10:59:10.420', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10914, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4700, 1190, '2', '0', '1', 30, 1, 0, '2016-06-28 11:29:20.530', 'CN90053', '106.38.71.30', '2016-06-28 11:29:20.530', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10915, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4481, 1290, '2', '0', '1', 30, 1, 0, '2016-06-28 11:02:09.327', 'CN90053', '106.38.71.30', '2016-06-28 11:02:09.327', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10916, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5200, 1345, '2', '0', '1', 30, 1, 0, '2016-06-28 11:03:32.013', 'CN90053', '106.38.71.30', '2016-06-28 11:03:32.013', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10917, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 6000, 1495, '1', '0', '1', 30, 1, 0, '2016-06-28 11:04:50.857', 'CN90053', '106.38.71.30', '2016-06-28 11:04:50.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10918, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5400, 1390, '2', '0', '1', 30, 1, 0, '2016-06-28 11:06:07.483', 'CN90053', '106.38.71.30', '2016-06-28 11:06:07.483', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10919, 7789, '[活动]征服德拉克箱子(30日)', '', '装有*12个德拉克神秘戒指*9、*11个封印的奇美拉卷轴、封印的格雷芬特卷轴*9的箱子。有效期限为*430天*9', 880, 120, '2', '0', '1', 30, 1, 0, '2016-07-21 12:57:10.543', 'CN90053', '106.38.71.30', '2016-07-21 12:57:10.543', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10920, 6660, '[活动]经验增幅道具宝箱（中级）', '', '装着*1经验增幅之咒*9的箱子。打开可随机获得*33倍~6倍*9其中的一个。，人品爆棚可获得*46小时*9的*36倍*9经验增幅之咒', 380, 89, '2', '0', '1', 0, 1, 0, '2016-07-21 13:02:27.780', 'CN90053', '106.38.71.30', '2016-07-21 13:02:27.780', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10921, 78, '银币掉落增幅之咒(2倍)', ' ', 'test', 20, 10, '1', '0', '1', 0, 1, 0, '2008-01-09 12:27:34.000', NULL, NULL, '2014-08-12 12:06:23.860', ' ', ' ');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10924, 2073, '银币掉落增幅之咒(2倍)', '', '使用后，狩猎获得的银币提高至2倍。', 150, 34, '2', '0', '1', 0, 1, 24, '2016-08-08 20:05:13.513', 'CN90053', '106.38.71.30', '2016-08-08 20:05:13.513', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10925, 6548, '[活动]除槽炼金符[10个]', '', '装有10个除槽炼金符的箱子。开启箱子，可获得*110个除槽炼金符*9。', 200, 19, '2', '0', '1', 0, 1, 0, '2016-08-09 10:57:26.623', 'CN90053', '106.38.71.30', '2016-08-09 10:57:26.623', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10926, 6549, '[活动]符文还原符[10个]', '', '装有10个符文还原符的箱子。开启箱子，可获得*110个符文还原符*9。', 100, 14, '2', '0', '1', 0, 1, 0, '2016-08-09 10:59:00.733', 'CN90053', '106.38.71.30', '2016-08-09 10:59:00.733', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10927, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 160, '2', '0', '1', 0, 1, 0, '2016-08-09 11:00:05.857', 'CN90053', '106.38.71.30', '2016-08-09 11:00:05.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10928, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 1, 0, '2016-08-09 11:01:08.730', 'CN90053', '106.38.71.30', '2016-08-09 11:01:08.730', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10929, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 1, 0, '2016-08-09 11:02:21.420', 'CN90053', '106.38.71.30', '2016-08-09 11:02:21.420', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10930, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 1, 0, '2016-08-09 11:03:20.390', 'CN90053', '106.38.71.30', '2016-08-09 11:03:20.390', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10931, 6638, '[限时]战靴专用符文箱子（中级）[5个]', '', '装有*45*9个*1战靴专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-08-09 11:04:39.187', 'CN90053', '106.38.71.30', '2016-08-09 11:04:39.187', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10932, 6640, '[限时]披风专用符文箱子（中级）[5个]', '', '装有*45*9个*1披风专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 140, '2', '0', '1', 0, 5, 0, '2016-08-09 11:09:02.577', 'CN90053', '106.38.71.30', '2016-08-09 11:09:02.577', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10933, 6642, '[限时]辅助装备专用符文箱子（中级)[5个]', '', '装有*45*9个*1辅助装备专用符文*9的箱子。。开启有几率获得*6IV级符文*9。', 1900, 140, '2', '0', '1', 0, 5, 0, '2016-08-09 11:10:19.483', 'CN90053', '106.38.71.30', '2016-08-09 11:10:19.483', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10934, 7618, '[回归礼包] 破坏者的精华箱子', '', '装有所有不稳定精华(破坏/守护/生命/熟练/灵魂)的箱子。  有效期限：14天。箱子*214天*9内不开启，将自动删除。', 880, 190, '2', '0', '1', 14, 1, 0, '2016-08-09 11:13:18.123', 'CN90053', '106.38.71.30', '2016-08-09 11:13:18.123', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10935, 7619, '[回归礼包] 骑士回归津贴', '', '为了回归的骑士而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 890, '2', '0', '1', 7, 1, 0, '2016-08-09 11:14:57.857', 'CN90053', '106.38.71.30', '2016-08-09 11:14:57.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10936, 7620, '[回归礼包] 游侠回归津贴', '', '为了回归的游侠而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 790, '2', '0', '1', 7, 1, 0, '2016-08-09 11:42:03.373', 'CN90053', '106.38.71.30', '2016-08-09 11:42:03.373', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10937, 7621, '[回归礼包] 刺客回归津贴', '', '为了回归的刺客而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 890, '2', '0', '1', 7, 1, 0, '2016-08-09 11:43:03.357', 'CN90053', '106.38.71.30', '2016-08-09 11:43:03.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10938, 7622, '[回归礼包] 精灵回归津贴', '', '为了回归的精灵而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 790, '2', '0', '1', 7, 1, 0, '2016-08-09 11:19:27.483', 'CN90053', '106.38.71.30', '2016-08-09 11:19:27.483', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10939, 7623, '[回归礼包] 召唤师回归津贴', '', '为了回归的召唤师而准备的津贴。开启时，可获得7件高级装备*9。有效期限：*47天*9。箱子*214天*9内不开启，将自动删除。', 5600, 890, '2', '0', '1', 7, 1, 0, '2016-08-09 11:21:18.327', 'CN90053', '106.38.71.30', '2016-08-09 11:21:18.327', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10940, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 160, '2', '0', '1', 0, 5, 0, '2016-08-09 11:51:52.000', 'CN90053', '106.38.71.30', '2016-08-09 11:51:52.000', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10941, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-08-09 11:52:45.450', 'CN90053', '106.38.71.30', '2016-08-09 11:52:45.450', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10942, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-08-09 11:53:49.937', 'CN90053', '106.38.71.30', '2016-08-09 11:53:49.937', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10943, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-08-09 11:54:46.670', 'CN90053', '106.38.71.30', '2016-08-09 11:54:46.670', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10945, 1602, '装备解封之咒', '', '使用后开启装备栏中5个被封印窗口,从而可佩戴两个戒指,项链,腰带和披风.双击鼠标左键后使用,有效时间30天.', 200, 34, '1', '0', '1', 0, 1, 720, '2016-08-17 13:58:57.780', 'CN90053', '106.38.71.30', '2016-08-17 13:58:57.780', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10946, 6211, '[附魔Ⅲ]女海盗船长(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的女海盗船长变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 990, '2', '0', '1', 30, 1, 0, '2016-08-23 10:48:49.763', 'CN90053', '106.38.71.30', '2016-08-23 10:48:49.763', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10947, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 1190, '2', '0', '1', 30, 1, 0, '2016-08-23 10:50:00.093', 'CN90053', '106.38.71.30', '2016-08-23 10:50:00.093', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10948, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5081, 1290, '2', '0', '1', 30, 1, 0, '2016-08-23 10:51:23.513', 'CN90053', '106.38.71.30', '2016-08-23 10:51:23.513', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10949, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5281, 1345, '2', '0', '1', 30, 1, 0, '2016-08-23 10:52:32.687', 'CN90053', '106.38.71.30', '2016-08-23 10:52:32.687', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10950, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5881, 1495, '2', '0', '1', 30, 1, 0, '2016-08-23 10:55:21.043', 'CN90053', '106.38.71.30', '2016-08-23 10:55:21.043', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10951, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5481, 1390, '2', '0', '1', 30, 1, 0, '2016-08-23 10:56:37.013', 'CN90053', '106.38.71.30', '2016-08-23 10:56:37.013', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10952, 7810, '[进击Ⅱ][附魔Ⅱ]女佣变身(Lv81)(30天)', '', '*1[近战]*9蕴含女佣灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 7081, 1795, '2', '0', '1', 30, 1, 0, '2016-08-23 11:01:47.670', 'CN90053', '106.38.71.30', '2016-08-23 11:01:47.670', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10953, 7837, '[进击Ⅱ][附魔Ⅲ]暮色黑寡妇(Lv84)(30天)', '', '*1[远程]*9蕴含暮色黑寡妇灵魂的项链,赋予超强远程打击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 1895, '2', '0', '1', 30, 1, 0, '2016-08-23 11:03:23.593', 'CN90053', '106.38.71.30', '2016-08-23 11:03:23.593', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10954, 7840, '[进击Ⅱ][附魔Ⅲ]暴走机车女(Lv84)(30天)', '', '*1[近战]*9蕴含暴走机车女灵魂的项链,赋予超强近战攻击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 1895, '2', '0', '1', 30, 1, 0, '2016-08-23 11:04:41.687', 'CN90053', '106.38.71.30', '2016-08-23 11:04:41.687', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10955, 7843, '[进击Ⅲ][附魔Ⅲ]暗黑侍卫(Lv87)(30天)', '', '*1[近战]*9蕴含暗黑侍卫灵魂的项链,赋予超强近战攻击能力。拥有*287级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅲ*9额外属性。', 8281, 2095, '2', '0', '1', 30, 1, 0, '2016-08-23 11:07:14.390', 'CN90053', '106.38.71.30', '2016-08-23 11:07:14.390', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10956, 7643, '戒指符文 Ⅲ 攻击箱子(30天)', '', '装有*1所有伤害 III、所有命中率 III*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 404, 102, '2', '0', '1', 30, 1, 0, '2016-08-23 11:08:33.297', 'CN90053', '106.38.71.30', '2016-08-23 11:08:33.297', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10957, 7644, '戒指符文 Ⅲ 暴击箱子(30天)', '', '装有*1暴击率 III、暴击伤害 III*92枚戒指符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 504, 142, '2', '0', '1', 30, 1, 0, '2016-08-23 11:09:43.357', 'CN90053', '106.38.71.30', '2016-08-23 11:09:43.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10958, 7638, '腰带符文 Ⅲ 攻击箱子(30天)', '', '装有*1所有攻击力 III、所有命中率 III*92枚腰带符文及*13个符文还原符*9、*12个除槽炼金符*9的箱子。有效期*430日*9。', 403, 91, '2', '0', '1', 30, 1, 0, '2016-08-23 11:10:47.280', 'CN90053', '106.38.71.30', '2016-08-23 11:10:47.280', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10959, 2800, '英雄箱子', '', '英雄遗落的箱子。宝箱内含*4+0Lv4生命精华,+0Lv4灵魂精华,+0Lv4破坏精华,+0Lv4守护精华,+0Lv4熟练精华等物品,包括从普通,稀有,史诗到传说级别的极品精华,及10个基础精华. 打开宝箱,可随机获得其中一件道具 *9。', 2900, 640, '2', '0', '1', 30, 1, 0, '2016-08-23 11:11:52.827', 'CN90053', '106.38.71.30', '2016-08-23 11:11:52.827', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10960, 1602, '装备解封之咒', '', '使用可解封装备栏。', 200, 34, '1', '0', '1', 0, 1, 720, '2016-08-23 11:26:31.810', 'CN90053', '106.38.71.30', '2016-08-23 11:26:31.810', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10961, 2800, '英雄箱子', '', '英雄遗落的箱子。宝箱内含*4+0Lv4生命精华,+0Lv4灵魂精华,+0Lv4破坏精华,+0Lv4守护精华,+0Lv4熟练精华等物品,包括从普通,稀有,史诗到传说级别的极品精华,及10个基础精华. 打开宝箱,可随机获得其中一件道具 *9。', 2900, 640, '2', '0', '1', 0, 1, 0, '2016-08-23 11:36:05.153', 'CN90053', '106.38.71.30', '2016-08-23 11:36:05.153', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10963, 7534, '[附魔Ⅱ]巴德(Lv66)项链', '', '*1[近战]*9蕴含巴德的变身项链。拥有*266级*9变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 3700, 940, '2', '0', '1', 30, 1, 0, '2016-09-27 12:54:19.373', 'CN90053', '106.38.71.30', '2016-09-27 12:54:19.373', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10964, 8007, '[附魔Ⅱ]东瀛少女(Lv69)(30天)', '', '*1[近战]*9蕴含东瀛少女灵魂的项链。拥有*269级*9变身能力值*3HP+300 MP+180 攻击速度和移动速度提升 负重+2000*9以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9', 4100, 1040, '2', '0', '1', 30, 1, 0, '2016-09-27 12:56:23.890', 'CN90053', '106.38.71.30', '2016-09-27 12:56:23.890', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10965, 7497, '海僧主教(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的海僧主教变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9', 4100, 1025, '2', '0', '1', 30, 1, 0, '2016-09-27 12:58:34.543', 'CN90053', '106.38.71.30', '2016-09-27 12:58:34.543', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10966, 7498, '风精灵(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的风精灵变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9', 4500, 1125, '2', '0', '1', 30, 1, 0, '2016-09-27 13:02:29.810', 'CN90053', '106.38.71.30', '2016-09-27 13:02:29.810', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10967, 7499, '蛮幽(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的蛮幽变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9', 4900, 1225, '2', '0', '1', 30, 1, 0, '2016-09-27 13:05:03.640', 'CN90053', '106.38.71.30', '2016-09-27 13:05:03.640', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10968, 7500, '哨兵(Lv81)(30天)', '', '*1[近战]*9赋予特殊能力的哨兵变身项链。拥有*281级*9的变身能力值*3HP+500 MP+300 攻击速度和移动速度提升 负重+3000*9', 5300, 1325, '2', '0', '1', 30, 1, 0, '2016-09-27 13:47:40.217', 'CN90053', '106.38.71.30', '2016-09-27 13:47:40.217', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10969, 6548, '[活动]除槽炼金符[10个]', '', '装有10个除槽炼金符的箱子。开启箱子，可获得*110个除槽炼金符*9。', 200, 19, '2', '0', '1', 0, 1, 0, '2016-09-27 13:09:23.763', 'CN90053', '106.38.71.30', '2016-09-27 13:09:23.763', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10970, 6549, '[活动]符文还原符[10个]', '', '装有10个符文还原符的箱子。开启箱子，可获得*110个符文还原符*9。', 100, 14, '2', '0', '1', 0, 1, 0, '2016-09-27 13:10:56.357', 'CN90053', '106.38.71.30', '2016-09-27 13:10:56.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10971, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 160, '2', '0', '1', 0, 5, 0, '2016-09-27 13:13:41.843', 'CN90053', '106.38.71.30', '2016-09-27 13:13:41.843', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10972, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-09-27 13:15:23.077', 'CN90053', '106.38.71.30', '2016-09-27 13:15:23.077', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10973, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-09-27 13:17:33.170', 'CN90053', '106.38.71.30', '2016-09-27 13:17:33.170', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10974, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-09-27 13:20:22.513', 'CN90053', '106.38.71.30', '2016-09-27 13:20:22.513', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10975, 6638, '[限时]战靴专用符文箱子（中级）[5个]', '', '装有*45*9个*1战靴专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-09-27 13:22:14.687', 'CN90053', '106.38.71.30', '2016-09-27 13:22:14.687', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10976, 6640, '[限时]披风专用符文箱子（中级）[5个]', '', '装有*45*9个*1披风专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 140, '2', '0', '1', 0, 5, 0, '2016-09-27 13:24:19.810', 'CN90053', '106.38.71.30', '2016-09-27 13:24:19.810', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10977, 6642, '[限时]辅助装备专用符文箱子（中级)[5个]', '', '装有*45*9个*1辅助装备专用符文*9的箱子。。开启有几率获得*6IV级符文*9。', 1900, 140, '2', '0', '1', 0, 5, 0, '2016-09-27 13:26:24.670', 'CN90053', '106.38.71.30', '2016-09-27 13:26:24.670', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10978, 7627, '装备守护神宝箱', '', '"装备守护神宝箱 装有8种装备掉落保护道具的宝箱。*11个神佑之粉（武器）(3日）、1个神佑之粉（武器）(5日）、1个神佑之粉(3日）、1个神佑之粉(5日）、1个[归属]玛尔斯的守护、
1个[可交易]玛尔斯的守护、3个[归属]玛尔斯的守护、3个[可交易]玛尔斯的守护*9中随机获得其中一个。"', 1760, 794, '2', '0', '1', 0, 1, 0, '2016-09-27 13:28:52.843', 'CN90053', '106.38.71.30', '2016-09-27 13:28:52.843', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10979, 7627, '装备守护神宝箱', '', '"装备守护神宝箱 装有8种装备掉落保护道具的宝箱。*11个神佑之粉（武器）(3日）、1个神佑之粉（武器）(5日）、1个神佑之粉(3日）、1个神佑之粉(5日）、1个[归属]玛尔斯的守护、
1个[可交易]玛尔斯的守护、3个[归属]玛尔斯的守护、3个[可交易]玛尔斯的守护*9中随机获得其中一个。"', 1760, 794, '2', '0', '1', 0, 1, 0, '2016-10-11 17:45:41.467', 'CN90053', '106.38.71.30', '2016-10-11 17:45:41.467', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10980, 7789, '[活动]征服德拉克箱子(30日)', '', '装有*12个德拉克神秘戒指*9、*11个封印的奇美拉卷轴、封印的格雷芬特卷轴*9的箱子。有效期限为*430天*9', 880, 120, '2', '0', '1', 30, 1, 0, '2016-10-20 11:12:00.733', 'CN90053', '106.38.71.30', '2016-10-20 11:12:00.733', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10981, 6660, '[活动]经验增幅道具宝箱（中级）', '', '装着*1经验增幅之咒*9的箱子。打开可随机获得*33倍~6倍*9其中的一个。，人品爆棚可获得*46小时*9的*36倍*9经验增幅之咒', 380, 89, '2', '0', '1', 0, 1, 0, '2016-10-20 11:13:57.140', 'CN90053', '106.38.71.30', '2016-10-20 11:13:57.140', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10982, 2073, '银币掉落增幅之咒(2倍)(1天)', '', '双及使用后，狩猎获得银币量为原由银币的*22倍*9，快捷键"T"可确认使用状态', 280, 34, '2', '0', '1', 0, 1, 24, '2016-10-20 11:15:35.890', 'CN90053', '106.38.71.30', '2016-10-20 11:15:35.890', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10983, 7609, '[活动]幸运精华箱子高级(30天)', '', '装有幸运精华的箱子。开启箱子时，可随机获得*1幸运精华Lv6、Lv7*9其中的一个。*430日*9。', 562, 196, '2', '0', '1', 30, 1, 0, '2016-10-20 11:18:10.200', 'CN90053', '106.38.71.30', '2016-10-20 11:18:10.200', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10984, 7816, '[进击Ⅱ][附魔Ⅱ]名妓黄真伊(Lv66)(30天)', '', '*1[近战]*9蕴含名妓黄真伊灵魂的项链,赋予超强近战攻击能力。拥有*266级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 5900, 1245, '2', '0', '1', 30, 1, 0, '2016-10-27 11:05:00.827', 'CN90053', '106.38.71.30', '2016-10-27 11:05:00.827', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10985, 7544, '[附魔Ⅲ]亡灵巫师(Lv69)(30天)', '', '*1[近战]*9蕴含亡灵巫师的变身项链。赋予*269级*9变身HP/MP/负重/攻击速度/移动速度等能力值的的变身项链。附魔进阶属性*3力量+3 敏捷+3 智力+3*9', 4300, 1090, '2', '0', '1', 30, 1, 0, '2016-10-27 11:09:18.670', 'CN90053', '106.38.71.30', '2016-10-27 11:09:18.670', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10986, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4700, 1190, '2', '0', '1', 30, 1, 0, '2016-10-27 11:12:49.747', 'CN90053', '106.38.71.30', '2016-10-27 11:12:49.747', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10987, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5100, 1290, '2', '0', '1', 30, 1, 0, '2016-10-27 11:19:27.543', 'CN90053', '106.38.71.30', '2016-10-27 11:19:27.543', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10988, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5300, 1345, '2', '0', '1', 30, 1, 0, '2016-10-27 11:24:17.500', 'CN90053', '106.38.71.30', '2016-10-27 11:24:17.500', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10989, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 6000, 1495, '2', '0', '1', 30, 1, 0, '2016-10-27 11:26:12.140', 'CN90053', '106.38.71.30', '2016-10-27 11:26:12.140', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10990, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5500, 1390, '2', '0', '1', 30, 1, 0, '2016-10-27 11:30:02.593', 'CN90053', '106.38.71.30', '2016-10-27 11:30:02.593', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10991, 2800, '英雄箱子', '', '英雄遗落的箱子。宝箱内含*4+0Lv4生命精华,+0Lv4灵魂精华,+0Lv4破坏精华,+0Lv4守护精华,+0Lv4熟练精华等物品,包括从普通,稀有,史诗到传说级别的极品精华,及10个基础精华. 打开宝箱,可随机获得其中一件道具。*9', 2900, 690, '2', '0', '1', 0, 1, 0, '2016-11-17 10:56:45.810', 'CN90053', '106.38.71.30', '2016-11-17 10:56:45.810', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10992, 7627, '装备守护神宝箱', '', '"装备守护神宝箱 装有8种装备掉落保护道具的宝箱。*11个神佑之粉（武器）(3日）、1个神佑之粉（武器）(5日）、1个神佑之粉(3日）、1个神佑之粉(5日）、1个[归属]玛尔斯的守护、
1个[可交易]玛尔斯的守护、3个[归属]玛尔斯的守护、3个[可交易]玛尔斯的守护*9中随机获得其中一个。', 1760, 794, '1', '0', '1', 0, 1, 0, '2016-11-17 10:57:58.950', 'CN90053', '106.38.71.30', '2016-11-17 10:57:58.950', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10993, 6211, '[附魔Ⅲ]女海盗船长(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的女海盗船长变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 990, '2', '0', '1', 30, 1, 0, '2016-11-24 10:58:47.280', 'CN90053', '106.38.71.30', '2016-11-24 10:58:47.280', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10994, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 1190, '2', '0', '1', 30, 1, 0, '2016-11-24 11:00:30.030', 'CN90053', '106.38.71.30', '2016-11-24 11:00:30.030', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10995, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5081, 1290, '2', '0', '1', 30, 1, 0, '2016-11-24 11:01:35.827', 'CN90053', '106.38.71.30', '2016-11-24 11:01:35.827', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10996, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5281, 1345, '2', '0', '1', 30, 1, 0, '2016-11-24 11:04:02.060', 'CN90053', '106.38.71.30', '2016-11-24 11:04:02.060', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10997, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5881, 1495, '2', '0', '1', 30, 1, 0, '2016-11-24 11:05:31.060', 'CN90053', '106.38.71.30', '2016-11-24 11:05:31.060', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10998, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5481, 1390, '2', '0', '1', 30, 1, 0, '2016-11-24 11:06:48.437', 'CN90053', '106.38.71.30', '2016-11-24 11:06:48.437', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10999, 7810, '[进击Ⅱ][附魔Ⅱ]女佣变身(Lv81)(30天)', '', '*1[近战]*9蕴含女佣灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 7081, 1790, '2', '0', '1', 30, 1, 0, '2016-11-24 11:08:12.107', 'CN90053', '106.38.71.30', '2016-11-24 11:08:12.107', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11000, 7828, '[进击Ⅱ][附魔Ⅲ]粉嫩护士(Lv81)(30天)', '', '*1[近战]*9蕴含粉嫩护士灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值 负重+2250 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7300, 1845, '2', '0', '1', 30, 1, 0, '2016-11-24 11:09:40.000', 'CN90053', '106.38.71.30', '2016-11-24 11:09:40.000', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11001, 7837, '[附魔Ⅲ]暮色黑寡妇(Lv84)项链', '', '*1[远程]*9蕴含暮色黑寡妇灵魂的项链,赋予超强远程打击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 1895, '2', '0', '1', 30, 1, 0, '2016-11-24 11:11:37.920', 'CN90053', '106.38.71.30', '2016-11-24 11:11:37.920', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11002, 7840, '[进击Ⅱ][附魔Ⅲ]暴走机车女(Lv84)(30天)', '', '*1[近战]*9蕴含暴走机车女灵魂的项链,赋予超强近战攻击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 1895, '2', '0', '1', 30, 1, 0, '2016-11-24 11:12:57.497', 'CN90053', '106.38.71.30', '2016-11-24 11:12:57.497', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11003, 7843, '[进击Ⅲ][附魔Ⅲ]暗黑侍卫(Lv87)(30天)', '', '*1[近战]*9蕴含暗黑侍卫灵魂的项链,赋予超强近战攻击能力。拥有*287级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅲ*9额外属性。', 8281, 2095, '2', '0', '1', 30, 1, 0, '2016-11-24 11:14:21.593', 'CN90053', '106.38.71.30', '2016-11-24 11:14:21.593', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11007, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)项链', '', '123456', 4300, 1340, '2', '0', '1', 30, 1, 0, '2016-12-06 14:52:04.373', 'CN90053', '106.38.71.30', '2016-12-06 14:52:04.373', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11008, 6211, '[附魔Ⅲ]女海盗船长(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的女海盗船长变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 990, '2', '0', '1', 30, 1, 0, '2016-12-06 23:04:57.293', 'CN90053', '106.38.71.30', '2016-12-06 23:04:57.293', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11009, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 1190, '2', '0', '1', 30, 1, 0, '2016-12-06 23:06:14.373', 'CN90053', '106.38.71.30', '2016-12-06 23:06:14.373', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11010, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5081, 1290, '2', '0', '1', 30, 1, 0, '2016-12-06 23:07:51.797', 'CN90053', '106.38.71.30', '2016-12-06 23:07:51.797', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11011, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5281, 1345, '2', '0', '1', 30, 1, 0, '2016-12-06 23:10:10.047', 'CN90053', '106.38.71.30', '2016-12-06 23:10:10.047', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11012, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5881, 1495, '2', '0', '1', 30, 1, 0, '2016-12-06 23:19:12.170', 'CN90053', '106.38.71.30', '2016-12-06 23:19:12.170', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11013, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5481, 1390, '2', '0', '1', 30, 1, 0, '2016-12-06 23:20:54.357', 'CN90053', '106.38.71.30', '2016-12-06 23:20:54.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11014, 7810, '[进击Ⅱ][附魔Ⅱ]女佣变身(Lv81)(30天)', '', '*1[近战]*9蕴含女佣灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 7081, 1795, '2', '0', '1', 30, 1, 0, '2016-12-06 23:22:02.170', 'CN90053', '106.38.71.30', '2016-12-06 23:22:02.170', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11015, 7828, '[进击Ⅱ][附魔Ⅲ]粉嫩护士(Lv81)(30天)', '', '*1[近战]*9蕴含粉嫩护士灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值 负重+2250 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7381, 1845, '2', '0', '1', 30, 1, 0, '2016-12-06 23:25:15.857', 'CN90053', '106.38.71.30', '2016-12-06 23:25:15.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11016, 7837, '[进击Ⅱ][附魔Ⅲ]暮色黑寡妇(Lv84)(30天)', '', '*1[远程]*9蕴含暮色黑寡妇灵魂的项链,赋予超强远程打击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 1895, '2', '0', '1', 30, 1, 0, '2016-12-06 23:29:43.950', 'CN90053', '106.38.71.30', '2016-12-06 23:29:43.950', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11017, 7840, '[进击Ⅱ][附魔Ⅲ]暴走机车女(Lv84)(30天)', '', '*1[近战]*9蕴含暴走机车女灵魂的项链,赋予超强近战攻击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 1895, '2', '0', '1', 30, 1, 0, '2016-12-06 23:30:51.450', 'CN90053', '106.38.71.30', '2016-12-06 23:30:51.450', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11018, 7843, '[进击Ⅲ][附魔Ⅲ]暗黑侍卫(Lv87)(30天)', '', '*1[近战]*9蕴含暗黑侍卫灵魂的项链,赋予超强近战攻击能力。拥有*287级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅲ*9额外属性。', 8281, 2095, '2', '0', '1', 30, 1, 0, '2016-12-06 23:32:04.030', 'CN90053', '106.38.71.30', '2016-12-06 23:32:04.030', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11019, 7618, '[回归礼包] 破坏者的精华箱子', '', '装有所有不稳定精华(破坏/守护/生命/熟练/灵魂)的箱子。  有效期限：14天。箱子*214天*9内不开启，将自动删除。', 880, 190, '2', '0', '1', 14, 1, 0, '2016-12-06 23:33:15.500', 'CN90053', '106.38.71.30', '2016-12-06 23:33:15.500', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11020, 7619, '[回归礼包] 骑士回归津贴', '', '为了回归的骑士而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 890, '2', '0', '1', 7, 1, 0, '2016-12-06 23:34:15.780', 'CN90053', '106.38.71.30', '2016-12-06 23:34:15.780', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11021, 7620, '[回归礼包] 游侠回归津贴', '', '为了回归的游侠而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 790, '2', '0', '1', 7, 1, 0, '2016-12-06 23:35:09.200', 'CN90053', '106.38.71.30', '2016-12-06 23:35:09.200', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11022, 7621, '[回归礼包] 刺客回归津贴', '', '为了回归的刺客而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 890, '2', '0', '1', 7, 1, 0, '2016-12-06 23:36:16.797', 'CN90053', '106.38.71.30', '2016-12-06 23:36:16.797', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11023, 7622, '[回归礼包] 精灵回归津贴', '', '为了回归的精灵而准备的津贴。开启时，可获得*17件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 790, '2', '0', '1', 7, 1, 0, '2016-12-06 23:37:28.047', 'CN90053', '106.38.71.30', '2016-12-06 23:37:28.047', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11024, 7623, '[回归礼包] 召唤师回归津贴', '', '为了回归的召唤师而准备的津贴。开启时，可获得7件高级装备*9。有效期限：*47天*9。箱子*27天*9内不开启，将自动删除。', 5600, 890, '2', '0', '1', 7, 1, 0, '2016-12-06 23:38:22.950', 'CN90053', '106.38.71.30', '2016-12-06 23:38:22.950', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11025, 1602, '装备解封之咒(5个槽）', '', '使用后*2开启装备栏中5个被封印窗口*9，从而可佩戴*6两个戒指，项链，腰带和披风*9。', 200, 1, '2', '0', '1', 0, 1, 720, '2016-12-13 13:42:57.700', 'CN90053', '106.38.71.30', '2016-12-13 13:42:57.700', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11026, 6211, '[附魔Ⅲ]女海盗船长(Lv66)(30天)', '', '*1[近战]*9赋予特殊能力的女海盗船长变身项链。拥有*266级*9的变身能力值*3HP+250 MP+150 攻击速度和移动速度提升 负重+1750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 3681, 990, '2', '0', '1', 30, 1, 0, '2016-12-20 11:01:14.450', 'CN90053', '106.38.71.30', '2016-12-20 11:01:14.450', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11027, 7040, '[附魔Ⅲ]恶魔小丑(Lv72)(30天)', '', '*1[近战]*9赋予特殊能力的恶魔小丑变身项链。拥有*272级*9的变身能力值*3HP+350 MP+210 攻击速度和移动速度提升 负重+2250*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 4281, 1190, '1', '0', '1', 30, 1, 0, '2016-12-20 11:04:12.077', 'CN90053', '106.38.71.30', '2016-12-20 11:04:12.077', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11028, 7041, '[附魔Ⅲ]狂战士(Lv75)(30天)', '', '*1[近战]*9赋予特殊能力的狂战士变身项链。拥有*275级*9的变身能力值*3HP+400 MP+240 攻击速度和移动速度提升 负重+2500*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5081, 1290, '2', '0', '1', 30, 1, 0, '2016-12-20 11:07:54.513', 'CN90053', '106.38.71.30', '2016-12-20 11:07:54.513', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11029, 7822, '[进击][附魔I]男爵火枪手(Lv75)(30天)', '', '*1[远程]*9蕴含男爵火枪手灵魂的项链,赋予超强远程打击能力。拥有*275级*9变身能力值以及附魔进阶属性*3力量+1 敏捷+1 智力+1*9，同时拥有*4进击*9额外属性。', 5281, 1345, '2', '0', '1', 30, 1, 0, '2016-12-20 11:11:49.640', 'CN90053', '106.38.71.30', '2016-12-20 11:11:49.640', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11030, 7825, '[进击][附魔Ⅱ]惊艳女狙击手(Lv78)(30天)', '', '*1[远程]*9蕴含惊艳女狙击手灵魂的项链,赋予超强远程打击能力。拥有*278级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击*9额外属性。', 5881, 1495, '2', '0', '1', 30, 1, 0, '2016-12-20 11:14:11.187', 'CN90053', '106.38.71.30', '2016-12-20 11:14:11.187', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11031, 7042, '[附魔Ⅲ]女神咖莉(Lv78)(30天)', '', '*1[近战]*9赋予特殊能力的女神咖莉变身项链。拥有*278级*9的变身能力值*3HP+450 MP+270 攻击速度和移动速度提升 负重+2750*9以及附魔进阶属性*4力量+3 敏捷+3 智力+3*9', 5481, 1390, '2', '0', '1', 30, 1, 0, '2016-12-20 11:17:49.717', 'CN90053', '106.38.71.30', '2016-12-20 11:17:49.717', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11032, 7810, '[进击Ⅱ][附魔Ⅱ]女佣变身(Lv81)(30天)', '', '*1[近战]*9蕴含女佣灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值以及附魔进阶属性*3力量+2 敏捷+2 智力+2*9，同时拥有*4进击Ⅱ*9额外属性。', 7081, 1795, '2', '0', '1', 30, 1, 0, '2016-12-20 11:20:04.730', 'CN90053', '106.38.71.30', '2016-12-20 11:20:04.730', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11033, 7828, '[进击Ⅱ][附魔Ⅲ]粉嫩护士(Lv81)(30天)', '', '*1[近战]*9蕴含粉嫩护士灵魂的项链,赋予超强近战攻击能力。拥有*281级*9变身能力值 负重+2250 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7300, 1845, '2', '0', '1', 30, 1, 0, '2016-12-20 11:23:33.060', 'CN90053', '106.38.71.30', '2016-12-20 11:23:33.060', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11034, 7837, '[进击Ⅱ][附魔Ⅲ]暮色黑寡妇(Lv84)(30天)', '', '*1[远程]*9蕴含暮色黑寡妇灵魂的项链,赋予超强远程打击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 1895, '2', '0', '1', 30, 1, 0, '2016-12-20 11:27:08.343', 'CN90053', '106.38.71.30', '2016-12-20 11:27:08.343', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11035, 7840, '[进击Ⅱ][附魔Ⅲ]暴走机车女(Lv84)(30天)', '', '*1[近战]*9蕴含暴走机车女灵魂的项链,赋予超强近战攻击能力。拥有*284级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅱ*9额外属性。', 7481, 1895, '2', '0', '1', 30, 1, 0, '2016-12-20 11:30:48.170', 'CN90053', '106.38.71.30', '2016-12-20 11:30:48.170', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11036, 7843, '[进击Ⅲ][附魔Ⅲ]暗黑侍卫(Lv87)(30天)', '', '*1[近战]*9蕴含暗黑侍卫灵魂的项链,赋予超强近战攻击能力。拥有*287级*9变身能力值 以及附魔进阶属性*3力量+3 敏捷+3 智力+3*9，同时拥有*4进击Ⅲ*9额外属性。', 8281, 2095, '2', '0', '1', 30, 1, 0, '2016-12-20 11:35:30.827', 'CN90053', '106.38.71.30', '2016-12-20 11:35:30.827', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11037, 6548, '[活动]除槽炼金符[10个]', '', '装有10个除槽炼金符的箱子。开启箱子，可获得*110个除槽炼金符*9。', 200, 19, '2', '0', '1', 0, 1, 0, '2016-12-20 11:40:06.077', 'CN90053', '106.38.71.30', '2016-12-20 11:40:06.077', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11038, 6549, '[活动]符文还原符[10个]', '', '装有10个符文还原符的箱子。开启箱子，可获得*110个符文还原符*9。', 100, 14, '2', '0', '1', 0, 1, 0, '2016-12-20 11:42:58.000', 'CN90053', '106.38.71.30', '2016-12-20 11:42:58.000', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11039, 6630, '[限时]武器专用符文箱子（中级）[5个]', '', '装有*45*9个*1武器专用符文*9的箱子。开启有几率获得获得*6IV级符文*9。', 1900, 160, '2', '0', '1', 0, 5, 0, '2016-12-20 11:44:31.030', 'CN90053', '106.38.71.30', '2016-12-20 11:44:31.030', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11040, 6632, '[限时]头盔专用符文箱子（中级）[5个]', '', '装有*45*9个*1头盔专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-12-20 11:46:57.310', 'CN90053', '106.38.71.30', '2016-12-20 11:46:57.310', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11041, 6634, '[限时]盔甲专用符文箱子（中级）[5个]', '', '装有*45*9个*1盔甲专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-12-20 11:48:47.857', 'CN90053', '106.38.71.30', '2016-12-20 11:48:47.857', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11042, 6636, '[限时]护手专用符文箱子（中级）[5个]', '', '装有*45*9个*1护手专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-12-20 11:50:01.357', 'CN90053', '106.38.71.30', '2016-12-20 11:50:01.357', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11043, 6638, '[限时]战靴专用符文箱子（中级）[5个]', '', '装有*45*9个*1战靴专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 150, '2', '0', '1', 0, 5, 0, '2016-12-20 11:53:00.280', 'CN90053', '106.38.71.30', '2016-12-20 11:53:00.280', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11044, 6640, '[限时]披风专用符文箱子（中级）[5个]', '', '装有*45*9个*1披风专用符文*9的箱子。开启有几率获得*6IV级符文*9。', 1900, 140, '2', '0', '1', 0, 5, 0, '2016-12-20 11:54:36.577', 'CN90053', '106.38.71.30', '2016-12-20 11:54:36.577', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11045, 6642, '[限时]辅助装备专用符文箱子（中级)[5个]', '', '装有*45*9个*1辅助装备专用符文*9的箱子。。开启有几率获得*6IV级符文*9。', 1900, 140, '2', '0', '1', 0, 5, 0, '2016-12-20 11:56:50.140', 'CN90053', '106.38.71.30', '2016-12-20 11:56:50.140', 'CN90053', '106.38.71.30');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11048, 1602, 'test装备解封之咒', '', 'test', 1230, 61, '2', '0', '1', 0, 1, 720, '2016-12-30 16:45:07.333', 'CN90053', '10.40.11.102', '2016-12-30 16:45:07.333', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11049, 5556, '天然芒果汁', '', '', 123, 6, '2', '0', '1', 1, 1, 1, '2016-12-30 17:03:05.270', 'CN90053', '10.40.11.102', '2016-12-30 17:03:05.270', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11050, 241, '属性精华卡片', '', '', 125, 61, '2', '0', '1', 1, 1, 1, '2016-12-30 17:14:56.980', 'CN90053', '10.40.11.102', '2016-12-30 17:42:31.280', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11051, 5556, '天然芒果汁', '', '', 124, 61, '2', '0', '1', 1, 1, 1, '2016-12-30 17:25:07.010', 'CN90053', '10.40.11.102', '2016-12-30 17:25:07.010', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11052, 5556, '天然芒果汁', '', '', 124, 61, '2', '0', '1', 1, 1, 1, '2016-12-30 17:44:21.110', 'CN90053', '10.40.11.102', '2016-12-30 17:44:21.110', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11053, 5571, '天然芒果糖', '', '', 1111, 61, '2', '0', '1', 1, 1, 1, '2016-12-30 17:50:23.530', 'CN90053', '10.40.11.102', '2016-12-30 17:50:23.530', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11054, 1594, 'test地下城入场券', '', 'test', 1230, 61, '2', '0', '1', 0, 1, 720, '2016-12-30 17:53:35.040', 'CN90053', '10.40.11.102', '2016-12-30 17:53:35.040', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11055, 7948, '翡翠钥匙', '', '可用于开启黄金宝箱', 150, 75, '1', '0', '1', 0, 1, 0, '2018-07-13 18:42:42.523', 'CN12654', '210.72.232.131', '2018-07-13 18:42:42.523', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11056, 8456, '至尊神秘箱子', '', '开启后可获得英雄箱子*1，基础精华*50，银币掉率增幅之咒（2倍）（1天），强化精华*2', 1588, 794, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11057, 7628, '强化守护神宝箱', '', '装有7种强化守护道具和神圣强化卷轴的宝箱，开启后可随机获得其中一种', 560, 280, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11058, 6718, '武器强化宝箱', '', '装有神圣武器强化卷轴，发光的武器强化卷轴和武器强化卷轴（祝福）的箱子，开启后可随机获得其中一种', 499, 249, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11059, 6719, '防具强化宝箱', '', '装有神圣防具强化卷轴，发光的防具强化卷轴和防具强化卷轴（祝福）的箱子，开启后可随机获得其中一种', 399, 199, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

INSERT INTO [dbo].[TBLGoldItem] ([GoldItemID], [IID], [ItemName], [ItemImage], [ItemDesc], [OriginalGoldPrice], [GoldPrice], [ItemCategory], [IsPackage], [Status], [AvailablePeriod], [Count], [PracticalPeriod], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11060, 3368, '混沌点券充值卡 21000P', '', '充值混沌点数的物品，可以在各个城镇的混沌积分商人处购买战利品及其他重要道具', 995, 497, '1', '0', '1', 0, 1, 0, '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102', '2018-09-27 10:00:00.000', 'CN90053', '10.40.11.102');
GO

