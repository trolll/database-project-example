/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLBilling
Source Table          : TBLRecommendItemList
Date                  : 2023-10-04 18:57:43
*/


INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (2, 0, 1, '0', 1, '2008-01-09 14:18:57.000', NULL, NULL, '2013-04-27 11:00:23.203', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (3, 0, 2, '0', 3, '2008-01-09 14:18:57.000', NULL, NULL, '2013-11-28 22:15:00.467', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (4, 0, 3, '0', 3, '2008-01-09 14:18:57.000', NULL, NULL, '2008-05-01 10:06:55.997', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (5, 0, 4, '0', 8, '2008-01-09 14:18:57.000', NULL, NULL, '2008-11-04 08:36:58.290', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (6, 0, 5, '0', 10, '2008-01-09 14:18:57.000', NULL, NULL, '2008-04-30 07:29:20.507', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (7, 0, 6, '0', 9, '2008-01-09 14:18:57.000', NULL, NULL, '2008-03-07 18:21:34.020', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (8, 0, 7, '0', 7, '2008-01-09 14:18:57.000', NULL, NULL, '2008-02-25 16:10:25.560', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (9, 0, 8, '0', 1, '2008-01-09 14:18:57.000', NULL, NULL, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (10, 0, 9, '0', 8, '2008-01-09 14:18:57.000', NULL, NULL, '2008-02-25 16:10:25.560', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (11, 0, 10, '0', 1, '2008-01-09 14:18:57.000', NULL, NULL, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (12, 0, 11, '0', 9, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-03 13:08:13.770', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (13, 0, 12, '0', 1, '2008-01-09 14:21:10.000', NULL, NULL, '2008-03-26 15:04:05.040', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (14, 0, 13, '0', 2, '2008-01-09 14:21:10.000', NULL, NULL, '2008-03-26 15:04:05.057', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (15, 0, 14, '0', 1, '2008-01-09 14:21:10.000', NULL, NULL, '2008-09-09 07:09:19.083', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (16, 0, 15, '0', 8, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-30 07:29:20.413', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (17, 0, 16, '0', 10, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-02 09:54:20.417', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (18, 0, 17, '0', 8, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-16 10:26:33.370', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (19, 0, 18, '0', 4, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-03 13:08:12.817', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (20, 0, 19, '0', 7, '2008-01-09 14:21:10.000', NULL, NULL, '2008-09-09 07:09:19.083', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (21, 0, 20, '0', 7, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-30 07:29:20.367', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (28, 0, 21, '0', 6, '2008-03-26 16:19:28.070', 'CN12504', '210.72.232.228', '2008-04-30 07:29:20.320', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (29, 0, 22, '0', 7, '2008-03-26 16:19:28.070', 'CN12504', '210.72.232.228', '2008-04-03 13:08:13.410', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (22, 0, 25, '0', 10, '2008-03-26 16:19:28.027', 'CN12504', '210.72.232.228', '2008-11-04 08:36:58.290', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (24, 0, 26, '0', 9, '2008-03-26 16:19:28.040', 'CN12504', '210.72.232.228', '2008-11-04 08:36:58.290', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (219, 0, 33, '0', 4, '2013-11-28 19:07:08.013', 'CN90053', '222.128.29.241', '2013-11-28 22:15:00.467', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (235, 0, 34, '0', 4, '2014-04-29 12:59:38.123', 'CN90053', '124.205.178.150', '2014-05-16 15:27:27.453', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (221, 0, 35, '0', 1, '2013-11-30 14:05:55.810', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (213, 0, 37, '0', 2, '2013-11-26 16:03:59.047', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (225, 0, 38, '0', 4, '2013-11-30 14:05:55.857', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (155, 0, 39, '0', 4, '2012-07-24 13:05:14.437', '', '61.152.133.230', '2012-07-24 19:55:50.700', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (209, 0, 40, '0', 4, '2013-11-05 10:47:46.013', 'CN90053', '222.128.29.241', '2014-04-29 12:59:37.247', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (101, 0, 41, '0', 5, '2009-06-23 12:17:29.060', 'CN12654', '10.34.223.29', '2014-05-16 15:27:27.453', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (214, 0, 42, '0', 5, '2013-11-26 16:03:59.060', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (108, 0, 43, '0', 1, '2009-08-04 13:42:49.983', 'CN12654', '10.34.223.138', '2010-01-26 11:37:05.607', 'CN99019', '10.34.118.178');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (210, 0, 44, '0', 5, '2013-11-05 10:47:46.013', 'CN90053', '222.128.29.241', '2014-04-29 12:59:37.247', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (102, 0, 45, '0', 3, '2009-06-23 12:17:29.187', 'CN12654', '10.34.223.29', '2014-04-29 12:59:37.247', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (239, 0, 46, '0', 4, '2014-05-16 15:27:27.873', 'CN90053', '124.205.178.150', '2014-09-16 20:39:50.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (103, 0, 48, '0', 10, '2009-06-23 12:17:29.310', 'CN12654', '10.34.223.29', '2009-07-16 19:34:51.457', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (181, 0, 50, '0', 5, '2012-10-29 13:31:05.903', '', '61.152.133.230', '2012-11-06 10:37:56.077', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (160, 0, 52, '0', 5, '2012-07-24 19:55:50.750', '', '61.152.133.230', '2014-09-16 20:39:50.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (156, 0, 57, '0', 5, '2012-07-24 13:05:14.437', '', '61.152.133.230', '2012-07-24 19:55:50.700', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (215, 0, 58, '0', 3, '2013-11-26 16:03:59.060', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (165, 0, 61, '0', 3, '2012-08-27 17:59:18.090', '', '61.152.133.230', '2013-11-05 10:47:45.950', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (161, 0, 62, '0', 5, '2012-07-24 19:55:50.763', '', '61.152.133.230', '2012-08-27 17:59:18.030', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (228, 0, 63, '0', 3, '2013-12-25 21:13:23.013', 'CN90053', '222.128.29.241', '2013-12-27 19:01:02.610', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (229, 0, 64, '0', 4, '2013-12-25 21:13:23.013', 'CN90053', '222.128.29.241', '2013-12-27 19:01:02.610', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (220, 0, 66, '0', 3, '2013-11-28 19:07:08.030', 'CN90053', '222.128.29.241', '2014-09-16 20:39:50.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (125, 0, 115, '0', 1, '2009-12-29 10:23:11.360', 'CN99019', '202.108.36.125', '2009-12-29 11:32:46.190', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (39, 0, 129, '0', 10, '2008-05-01 10:06:57.137', 'CN12504', '210.72.232.228', '2008-09-09 07:09:19.083', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (37, 0, 134, '0', 7, '2008-05-01 10:06:56.810', 'CN12504', '210.72.232.228', '2008-11-04 08:36:58.290', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (34, 0, 135, '0', 4, '2008-05-01 10:06:56.323', 'CN12504', '210.72.232.228', '2008-09-09 07:09:19.083', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (44, 0, 138, '0', 5, '2008-05-07 07:49:13.583', 'KR10643', '210.72.232.136', '2009-06-23 12:17:27.687', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (50, 0, 154, '0', 7, '2008-09-09 07:07:02.340', 'CN12654', '210.72.232.170', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (51, 0, 155, '0', 7, '2008-09-09 07:07:02.340', 'CN12654', '210.72.232.170', '2012-02-22 14:55:12.550', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (52, 0, 156, '0', 7, '2008-09-09 07:07:02.357', 'CN12654', '210.72.232.170', '2009-09-22 06:44:26.440', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (63, 0, 165, '0', 10, '2008-11-04 09:50:58.090', 'CN11582', '59.108.20.1', '2009-03-17 08:07:37.390', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (49, 0, 178, '0', 1, '2008-09-09 07:07:02.340', 'CN12654', '210.72.232.170', '2009-07-16 19:34:51.457', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (62, 0, 181, '0', 10, '2008-11-04 09:50:58.040', 'CN11582', '59.108.20.1', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (60, 0, 185, '0', 1, '2008-11-04 09:50:57.947', 'CN11582', '59.108.20.1', '2013-11-30 14:05:55.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (61, 0, 186, '0', 2, '2008-11-04 09:50:57.993', 'CN11582', '59.108.20.1', '2013-11-30 14:05:55.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (54, 0, 187, '0', 1, '2008-11-04 08:34:18.023', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (55, 0, 188, '0', 2, '2008-11-04 08:34:18.040', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (56, 0, 189, '0', 3, '2008-11-04 08:34:18.040', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (57, 0, 190, '0', 4, '2008-11-04 08:34:18.040', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (58, 0, 191, '0', 5, '2008-11-04 08:34:18.057', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (82, 0, 201, '0', 18, '2008-12-23 11:17:36.203', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (81, 0, 203, '0', 17, '2008-12-23 11:17:36.063', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (80, 0, 204, '0', 16, '2008-12-23 11:17:35.940', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (75, 0, 206, '0', 11, '2008-12-23 11:17:35.220', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (76, 0, 207, '0', 12, '2008-12-23 11:17:35.313', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (77, 0, 208, '0', 13, '2008-12-23 11:17:35.640', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (78, 0, 209, '0', 14, '2008-12-23 11:17:35.737', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (79, 0, 210, '0', 15, '2008-12-23 11:17:35.843', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (83, 0, 211, '0', 19, '2008-12-23 11:17:36.313', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (74, 0, 212, '0', 10, '2008-12-23 11:17:35.080', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (93, 0, 223, '0', 10, '2009-04-16 07:15:50.690', 'CN12654', '10.34.115.182', '2009-06-23 12:17:27.687', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (104, 0, 303, '0', 1, '2009-07-16 19:34:51.583', 'CN12654', '10.34.223.29', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (109, 0, 310, '0', 2, '2009-08-04 13:42:50.123', 'CN12654', '10.34.223.138', '2012-02-22 14:55:12.550', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (105, 0, 312, '0', 2, '2009-07-16 19:34:51.707', 'CN12654', '10.34.223.29', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (110, 0, 316, '0', 5, '2009-08-04 13:42:50.233', 'CN12654', '10.34.223.138', '2012-04-05 15:10:21.437', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (106, 0, 318, '0', 3, '2009-07-16 19:34:51.833', 'CN12654', '10.34.223.29', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (111, 0, 322, '0', 6, '2009-08-04 13:42:50.357', 'CN12654', '10.34.223.138', '2012-04-05 15:10:21.437', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (107, 0, 324, '0', 4, '2009-07-16 19:34:51.957', 'CN12654', '10.34.223.29', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (112, 0, 325, '0', 6, '2009-08-04 13:42:50.467', 'CN12654', '10.34.223.138', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (142, 0, 326, '0', 8, '2012-04-05 15:10:21.530', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (121, 0, 329, '0', 9, '2009-09-22 06:44:26.923', 'CN12654', '10.34.118.198', '2009-12-29 09:31:52.140', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (122, 0, 330, '0', 10, '2009-09-22 06:44:26.970', 'CN12654', '10.34.118.198', '2009-12-29 09:31:52.140', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (234, 0, 359, '0', 3, '2014-04-29 12:59:37.903', 'CN90053', '124.205.178.150', '2014-05-13 18:43:36.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (123, 0, 360, '0', 1, '2009-12-29 09:31:52.220', 'CN99019', '202.108.36.125', '2009-12-29 10:23:11.313', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (124, 0, 361, '0', 5, '2012-12-24 12:12:40.497', 'CN90053', '61.152.133.230', '2014-07-10 19:21:37.857', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (134, 0, 373, '0', 9, '2012-02-22 14:55:12.647', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.437', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (128, 0, 375, '0', 3, '2012-02-22 14:55:12.600', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.437', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (138, 0, 376, '0', 4, '2012-04-05 15:10:21.500', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (139, 0, 378, '0', 5, '2012-04-05 15:10:21.513', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (170, 0, 379, '0', 5, '2012-08-28 11:57:16.640', '', '61.152.133.230', '2012-10-29 13:31:05.843', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (186, 0, 394, '0', 5, '2012-11-06 10:37:56.140', '', '61.152.133.230', '2012-12-24 12:12:40.450', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (146, 0, 397, '0', 5, '2012-07-20 21:42:39.607', 'CN90053', '61.152.133.230', '2012-08-28 11:57:16.593', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (147, 0, 398, '0', 4, '2012-07-20 21:42:39.623', 'CN90053', '61.152.133.230', '2012-07-20 22:23:09.543', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (231, 0, 407, '0', 2, '2013-12-27 21:23:13.077', 'CN90053', '222.128.29.241', '2013-12-28 13:55:29.700', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (205, 0, 413, '0', 5, '2013-05-07 11:00:22.203', 'CN90053', '222.128.26.192', '2013-11-05 10:47:45.950', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (149, 0, 418, '0', 2, '2012-07-20 21:42:39.623', 'CN90053', '61.152.133.230', '2014-09-10 20:31:24.747', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (151, 0, 419, '0', 8, '2012-07-20 21:42:39.640', 'CN90053', '61.152.133.230', '2012-07-20 22:23:09.543', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (148, 0, 423, '0', 1, '2012-07-20 21:42:39.623', 'CN90053', '61.152.133.230', '2014-09-10 20:31:24.747', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (150, 0, 424, '0', 6, '2012-07-20 21:42:39.640', 'CN90053', '61.152.133.230', '2012-07-20 22:36:22.200', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (191, 0, 426, '0', 4, '2013-01-04 14:48:04.530', 'CN90053', '61.152.133.230', '2013-11-05 10:47:45.950', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (194, 0, 436, '0', 3, '2013-02-06 12:14:50.687', 'CN90053', '61.152.133.230', '2013-02-19 10:27:09.340', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (200, 0, 437, '0', 6, '2013-04-27 11:00:23.267', 'CN90053', '222.128.26.192', '2013-05-07 11:00:22.140', 'CN90053', '222.128.26.192');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (176, 0, 438, '0', 7, '2012-10-08 12:01:49.827', '', '61.152.133.230', '2013-05-07 11:00:22.140', 'CN90053', '222.128.26.192');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (171, 0, 439, '0', 2, '2012-09-28 10:48:41.140', '', '61.152.133.230', '2013-02-19 10:27:09.340', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (277, 0, 477, '0', 5, '2014-12-02 10:53:29.560', 'CN90053', '124.205.178.150', '2015-01-27 11:14:12.170', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (240, 0, 494, '0', 2, '2014-09-10 20:31:24.747', 'CN90053', '124.205.178.150', '2014-09-11 12:45:10.233', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (290, 0, 618, '0', 5, '2015-01-06 10:27:50.233', 'CN90053', '124.205.178.150', '2015-06-15 16:27:36.297', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (269, 0, 619, '0', 5, '2014-11-05 12:15:42.090', 'CN90053', '124.205.178.150', '2015-03-20 18:49:33.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (268, 0, 622, '0', 4, '2014-11-05 12:15:42.077', 'CN90053', '124.205.178.150', '2015-08-25 14:43:36.857', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (241, 0, 625, '0', 2, '2014-09-10 20:31:24.763', 'CN90053', '124.205.178.150', '2015-03-20 18:49:33.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (264, 0, 626, '0', 3, '2014-10-08 11:42:12.967', 'CN90053', '124.205.178.150', '2015-03-20 18:49:33.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (243, 0, 627, '0', 1, '2014-09-11 12:45:10.247', 'CN90053', '124.205.178.150', '2015-03-20 18:49:33.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (246, 0, 630, '0', 1, '2014-09-16 20:39:50.920', 'CN90053', '124.205.178.150', '2014-11-05 12:15:42.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (250, 0, 631, '0', 2, '2014-09-16 21:02:10.420', 'CN90053', '124.205.178.150', '2014-11-05 12:15:42.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (251, 0, 634, '0', 3, '2014-09-16 21:02:10.437', 'CN90053', '124.205.178.150', '2014-11-05 12:15:42.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (257, 0, 648, '0', 4, '2014-09-28 14:29:28.590', 'CN90053', '124.205.178.150', '2014-09-28 15:42:20.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (274, 0, 650, '0', 5, '2014-11-21 15:31:38.483', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (273, 0, 651, '0', 4, '2014-11-21 15:31:38.483', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (272, 0, 652, '0', 3, '2014-11-21 15:31:38.467', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (253, 0, 653, '0', 2, '2014-11-21 15:31:38.467', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (252, 0, 654, '0', 1, '2014-11-21 15:31:38.437', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (258, 0, 655, '0', 1, '2014-09-28 18:21:29.670', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (279, 0, 656, '0', 2, '2014-12-22 12:20:54.483', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (280, 0, 657, '0', 3, '2014-12-22 12:20:54.500', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (281, 0, 658, '0', 4, '2014-12-22 12:20:54.500', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (286, 0, 659, '0', 5, '2014-12-22 12:48:56.107', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (259, 0, 661, '0', 2, '2014-09-28 18:21:29.687', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (300, 0, 663, '0', 5, '2015-02-09 12:38:41.250', 'CN90053', '124.205.178.150', '2015-02-12 16:41:28.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (295, 0, 664, '0', 5, '2015-01-27 11:14:12.250', 'CN90053', '124.205.178.150', '2015-02-09 12:38:41.153', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (350, 0, 667, '0', 4, '2015-09-23 12:36:32.047', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (349, 0, 670, '0', 3, '2015-09-23 12:36:32.030', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (301, 0, 671, '0', 1, '2015-02-12 16:41:28.937', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (302, 0, 672, '0', 2, '2015-02-12 16:41:28.967', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (303, 0, 673, '0', 3, '2015-02-12 16:41:28.967', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (304, 0, 674, '0', 5, '2015-02-12 16:41:29.030', 'CN90053', '124.205.178.150', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (305, 0, 675, '0', 5, '2015-02-12 16:41:29.030', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (307, 0, 676, '0', 3, '2015-03-20 18:49:33.420', 'CN90053', '124.205.178.150', '2015-08-25 14:43:36.857', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (309, 0, 677, '0', 4, '2015-03-20 18:49:33.437', 'CN90053', '124.205.178.150', '2015-07-11 17:10:58.500', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (310, 0, 678, '0', 3, '2015-03-20 18:49:33.437', 'CN90053', '124.205.178.150', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (306, 0, 679, '0', 1, '2015-03-20 18:49:33.403', 'CN90053', '124.205.178.150', '2015-08-25 14:43:36.857', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (317, 0, 681, '0', 1, '2015-05-29 09:08:03.390', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (311, 0, 684, '0', 1, '2015-04-20 14:30:12.310', 'CN90053', '58.30.128.109', '2015-05-11 12:18:20.937', 'CN90053', '124.205.178.210');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (326, 0, 685, '0', 4, '2015-08-25 14:43:36.937', 'CN90053', '114.112.126.218', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (324, 0, 686, '0', 2, '2015-08-25 14:43:36.920', 'CN90053', '114.112.126.218', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (313, 0, 687, '0', 1, '2015-04-27 16:32:28.733', 'CN90053', '124.205.178.210', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (314, 0, 688, '0', 3, '2015-04-27 16:32:28.763', 'CN90053', '124.205.178.210', '2015-06-23 13:54:58.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (315, 0, 689, '0', 5, '2015-04-27 16:32:28.780', 'CN90053', '124.205.178.210', '2015-06-23 13:54:58.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (316, 0, 690, '0', 5, '2015-04-27 16:32:28.780', 'CN90053', '124.205.178.210', '2015-05-11 12:18:20.937', 'CN90053', '124.205.178.210');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (322, 0, 695, '0', 5, '2015-07-11 17:10:58.560', 'CN90053', '114.112.126.218', '2015-07-21 10:21:55.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (321, 0, 696, '0', 4, '2015-07-11 17:10:58.547', 'CN90053', '114.112.126.218', '2015-07-21 10:21:55.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (372, 0, 731, '1', 1, '2016-04-21 12:27:31.793', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.793', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (341, 0, 794, '0', 5, '2015-09-22 15:39:55.250', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (331, 0, 798, '0', 5, '2015-09-20 15:56:03.513', 'CN90053', '114.112.126.218', '2015-09-21 13:34:29.797', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (359, 0, 802, '0', 4, '2015-10-13 11:21:28.263', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (336, 0, 803, '0', 2, '2015-09-21 13:34:29.873', 'CN90053', '114.112.126.218', '2015-09-23 12:36:31.937', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (329, 0, 805, '0', 3, '2015-09-20 15:56:03.497', 'CN90053', '114.112.126.218', '2015-09-22 15:39:55.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (330, 0, 806, '0', 4, '2015-09-20 15:56:03.513', 'CN90053', '114.112.126.218', '2015-09-22 15:39:55.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (328, 0, 810, '0', 1, '2015-09-20 15:56:03.497', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (327, 0, 812, '0', 1, '2015-09-20 15:56:03.480', 'CN90053', '114.112.126.218', '2015-09-22 15:39:55.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (339, 0, 813, '0', 2, '2015-09-22 15:39:55.233', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (340, 0, 815, '0', 3, '2015-09-22 15:39:55.250', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (345, 0, 816, '0', 4, '2015-09-22 17:48:39.750', 'CN90053', '114.112.126.218', '2015-09-23 12:36:31.937', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (347, 0, 817, '0', 1, '2015-09-23 12:36:32.000', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (348, 0, 818, '0', 2, '2015-09-23 12:36:32.013', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (355, 0, 829, '0', 5, '2015-09-30 06:15:18.310', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (363, 0, 833, '0', 4, '2016-02-04 12:17:28.857', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (364, 0, 834, '0', 5, '2016-02-04 12:17:28.873', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (362, 0, 836, '0', 3, '2016-02-04 12:17:28.843', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (361, 0, 837, '0', 2, '2016-02-04 12:17:28.827', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (360, 0, 838, '0', 1, '2016-02-04 12:17:28.810', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (369, 0, 839, '0', 5, '2016-03-22 11:44:28.060', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.763', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (367, 0, 841, '0', 3, '2016-03-22 11:44:28.047', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.763', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (376, 0, 842, '1', 3, '2016-04-21 12:27:31.827', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.827', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (375, 0, 843, '1', 2, '2016-04-21 12:27:31.810', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (377, 0, 844, '1', 4, '2016-04-21 12:27:31.840', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (368, 0, 845, '1', 5, '2016-03-22 11:44:28.060', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.857', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (365, 0, 856, '0', 1, '2016-03-22 11:44:28.030', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.763', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (366, 0, 857, '0', 2, '2016-03-22 11:44:28.047', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.763', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (379, 0, 10001, '0', 1, '2008-01-09 14:18:57.000', NULL, NULL, '2013-04-27 11:00:23.203', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (380, 0, 10002, '0', 3, '2008-01-09 14:18:57.000', NULL, NULL, '2013-11-28 22:15:00.467', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (381, 0, 10003, '0', 3, '2008-01-09 14:18:57.000', NULL, NULL, '2008-05-01 10:06:55.997', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (382, 0, 10004, '0', 8, '2008-01-09 14:18:57.000', NULL, NULL, '2008-11-04 08:36:58.290', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (383, 0, 10005, '0', 10, '2008-01-09 14:18:57.000', NULL, NULL, '2008-04-30 07:29:20.507', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (384, 0, 10006, '0', 9, '2008-01-09 14:18:57.000', NULL, NULL, '2008-03-07 18:21:34.020', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (385, 0, 10007, '0', 7, '2008-01-09 14:18:57.000', NULL, NULL, '2008-02-25 16:10:25.560', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (386, 0, 10008, '0', 1, '2008-01-09 14:18:57.000', NULL, NULL, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (387, 0, 10009, '0', 8, '2008-01-09 14:18:57.000', NULL, NULL, '2008-02-25 16:10:25.560', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (388, 0, 10010, '0', 1, '2008-01-09 14:18:57.000', NULL, NULL, NULL, NULL, NULL);
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (389, 0, 10011, '0', 9, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-03 13:08:13.770', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (390, 0, 10012, '0', 1, '2008-01-09 14:21:10.000', NULL, NULL, '2008-03-26 15:04:05.040', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (391, 0, 10013, '0', 2, '2008-01-09 14:21:10.000', NULL, NULL, '2008-03-26 15:04:05.057', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (392, 0, 10014, '0', 1, '2008-01-09 14:21:10.000', NULL, NULL, '2008-09-09 07:09:19.083', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (393, 0, 10015, '0', 8, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-30 07:29:20.413', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (394, 0, 10016, '0', 10, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-02 09:54:20.417', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (395, 0, 10017, '0', 8, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-16 10:26:33.370', 'CN12504', '210.72.232.228');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (396, 0, 10018, '0', 4, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-03 13:08:12.817', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (397, 0, 10019, '0', 7, '2008-01-09 14:21:10.000', NULL, NULL, '2008-09-09 07:09:19.083', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (398, 0, 10020, '0', 7, '2008-01-09 14:21:10.000', NULL, NULL, '2008-04-30 07:29:20.367', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (405, 0, 10021, '0', 6, '2008-03-26 16:19:28.070', 'CN12504', '210.72.232.228', '2008-04-30 07:29:20.320', 'KR10643', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (406, 0, 10022, '0', 7, '2008-03-26 16:19:28.070', 'CN12504', '210.72.232.228', '2008-04-03 13:08:13.410', 'KR10643', '210.72.232.136');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (399, 0, 10025, '0', 10, '2008-03-26 16:19:28.027', 'CN12504', '210.72.232.228', '2008-11-04 08:36:58.290', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (401, 0, 10026, '0', 9, '2008-03-26 16:19:28.040', 'CN12504', '210.72.232.228', '2008-11-04 08:36:58.290', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (596, 0, 10033, '0', 4, '2013-11-28 19:07:08.013', 'CN90053', '222.128.29.241', '2013-11-28 22:15:00.467', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (612, 0, 10034, '0', 4, '2014-04-29 12:59:38.123', 'CN90053', '124.205.178.150', '2014-05-16 15:27:27.453', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (598, 0, 10035, '0', 1, '2013-11-30 14:05:55.810', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (590, 0, 10037, '0', 2, '2013-11-26 16:03:59.047', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (602, 0, 10038, '0', 4, '2013-11-30 14:05:55.857', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (532, 0, 10039, '0', 4, '2012-07-24 13:05:14.437', '', '61.152.133.230', '2012-07-24 19:55:50.700', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (586, 0, 10040, '0', 4, '2013-11-05 10:47:46.013', 'CN90053', '222.128.29.241', '2014-04-29 12:59:37.247', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (478, 0, 10041, '0', 5, '2009-06-23 12:17:29.060', 'CN12654', '10.34.223.29', '2014-05-16 15:27:27.453', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (591, 0, 10042, '0', 5, '2013-11-26 16:03:59.060', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (485, 0, 10043, '0', 1, '2009-08-04 13:42:49.983', 'CN12654', '10.34.223.138', '2010-01-26 11:37:05.607', 'CN99019', '10.34.118.178');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (587, 0, 10044, '0', 5, '2013-11-05 10:47:46.013', 'CN90053', '222.128.29.241', '2014-04-29 12:59:37.247', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (479, 0, 10045, '0', 3, '2009-06-23 12:17:29.187', 'CN12654', '10.34.223.29', '2014-04-29 12:59:37.247', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (616, 0, 10046, '0', 4, '2014-05-16 15:27:27.873', 'CN90053', '124.205.178.150', '2014-09-16 20:39:50.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (480, 0, 10048, '0', 10, '2009-06-23 12:17:29.310', 'CN12654', '10.34.223.29', '2009-07-16 19:34:51.457', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (558, 0, 10050, '0', 5, '2012-10-29 13:31:05.903', '', '61.152.133.230', '2012-11-06 10:37:56.077', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (537, 0, 10052, '0', 5, '2012-07-24 19:55:50.750', '', '61.152.133.230', '2014-09-16 20:39:50.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (533, 0, 10057, '0', 5, '2012-07-24 13:05:14.437', '', '61.152.133.230', '2012-07-24 19:55:50.700', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (592, 0, 10058, '0', 3, '2013-11-26 16:03:59.060', 'CN90053', '222.128.29.241', '2013-12-28 18:15:55.997', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (542, 0, 10061, '0', 3, '2012-08-27 17:59:18.090', '', '61.152.133.230', '2013-11-05 10:47:45.950', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (538, 0, 10062, '0', 5, '2012-07-24 19:55:50.763', '', '61.152.133.230', '2012-08-27 17:59:18.030', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (605, 0, 10063, '0', 3, '2013-12-25 21:13:23.013', 'CN90053', '222.128.29.241', '2013-12-27 19:01:02.610', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (606, 0, 10064, '0', 4, '2013-12-25 21:13:23.013', 'CN90053', '222.128.29.241', '2013-12-27 19:01:02.610', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (597, 0, 10066, '0', 3, '2013-11-28 19:07:08.030', 'CN90053', '222.128.29.241', '2014-09-16 20:39:50.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (502, 0, 10115, '0', 1, '2009-12-29 10:23:11.360', 'CN99019', '202.108.36.125', '2009-12-29 11:32:46.190', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (416, 0, 10129, '0', 10, '2008-05-01 10:06:57.137', 'CN12504', '210.72.232.228', '2008-09-09 07:09:19.083', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (414, 0, 10134, '0', 7, '2008-05-01 10:06:56.810', 'CN12504', '210.72.232.228', '2008-11-04 08:36:58.290', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (411, 0, 10135, '0', 4, '2008-05-01 10:06:56.323', 'CN12504', '210.72.232.228', '2008-09-09 07:09:19.083', 'CN12654', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (421, 0, 10138, '0', 5, '2008-05-07 07:49:13.583', 'KR10643', '210.72.232.136', '2009-06-23 12:17:27.687', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (427, 0, 10154, '0', 7, '2008-09-09 07:07:02.340', 'CN12654', '210.72.232.170', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (428, 0, 10155, '0', 7, '2008-09-09 07:07:02.340', 'CN12654', '210.72.232.170', '2012-02-22 14:55:12.550', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (429, 0, 10156, '0', 7, '2008-09-09 07:07:02.357', 'CN12654', '210.72.232.170', '2009-09-22 06:44:26.440', 'CN12654', '10.34.223.138');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (440, 0, 10165, '0', 10, '2008-11-04 09:50:58.090', 'CN11582', '59.108.20.1', '2009-03-17 08:07:37.390', 'KR13076', '59.108.20.1');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (426, 0, 10178, '0', 1, '2008-09-09 07:07:02.340', 'CN12654', '210.72.232.170', '2009-07-16 19:34:51.457', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (439, 0, 10181, '0', 10, '2008-11-04 09:50:58.040', 'CN11582', '59.108.20.1', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (437, 0, 10185, '0', 1, '2008-11-04 09:50:57.947', 'CN11582', '59.108.20.1', '2013-11-30 14:05:55.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (438, 0, 10186, '0', 2, '2008-11-04 09:50:57.993', 'CN11582', '59.108.20.1', '2013-11-30 14:05:55.793', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (431, 0, 10187, '0', 1, '2008-11-04 08:34:18.023', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (432, 0, 10188, '0', 2, '2008-11-04 08:34:18.040', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (433, 0, 10189, '0', 3, '2008-11-04 08:34:18.040', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (434, 0, 10190, '0', 4, '2008-11-04 08:34:18.040', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (435, 0, 10191, '0', 5, '2008-11-04 08:34:18.057', 'KR13076', '210.72.232.170', '2008-11-04 09:50:57.620', 'KR13076', '210.72.232.170');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (459, 0, 10201, '0', 18, '2008-12-23 11:17:36.203', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (458, 0, 10203, '0', 17, '2008-12-23 11:17:36.063', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (457, 0, 10204, '0', 16, '2008-12-23 11:17:35.940', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (452, 0, 10206, '0', 11, '2008-12-23 11:17:35.220', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (453, 0, 10207, '0', 12, '2008-12-23 11:17:35.313', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (454, 0, 10208, '0', 13, '2008-12-23 11:17:35.640', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (455, 0, 10209, '0', 14, '2008-12-23 11:17:35.737', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (456, 0, 10210, '0', 15, '2008-12-23 11:17:35.843', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (460, 0, 10211, '0', 19, '2008-12-23 11:17:36.313', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (451, 0, 10212, '0', 10, '2008-12-23 11:17:35.080', 'KR13076', '210.72.232.170', '2009-04-16 07:15:49.613', 'CN12654', '10.34.117.27');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (470, 0, 10223, '0', 10, '2009-04-16 07:15:50.690', 'CN12654', '10.34.115.182', '2009-06-23 12:17:27.687', 'CN12654', '10.34.115.182');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (481, 0, 10303, '0', 1, '2009-07-16 19:34:51.583', 'CN12654', '10.34.223.29', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (486, 0, 10310, '0', 2, '2009-08-04 13:42:50.123', 'CN12654', '10.34.223.138', '2012-02-22 14:55:12.550', 'CN99019', '10.34.118.179');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (482, 0, 10312, '0', 2, '2009-07-16 19:34:51.707', 'CN12654', '10.34.223.29', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (487, 0, 10316, '0', 5, '2009-08-04 13:42:50.233', 'CN12654', '10.34.223.138', '2012-04-05 15:10:21.437', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (483, 0, 10318, '0', 3, '2009-07-16 19:34:51.833', 'CN12654', '10.34.223.29', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (488, 0, 10322, '0', 6, '2009-08-04 13:42:50.357', 'CN12654', '10.34.223.138', '2012-04-05 15:10:21.437', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (484, 0, 10324, '0', 4, '2009-07-16 19:34:51.957', 'CN12654', '10.34.223.29', '2009-08-04 13:42:49.857', 'CN12654', '10.34.223.29');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (489, 0, 10325, '0', 6, '2009-08-04 13:42:50.467', 'CN12654', '10.34.223.138', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (519, 0, 10326, '0', 8, '2012-04-05 15:10:21.530', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (498, 0, 10329, '0', 9, '2009-09-22 06:44:26.923', 'CN12654', '10.34.118.198', '2009-12-29 09:31:52.140', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (499, 0, 10330, '0', 10, '2009-09-22 06:44:26.970', 'CN12654', '10.34.118.198', '2009-12-29 09:31:52.140', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (611, 0, 10359, '0', 3, '2014-04-29 12:59:37.903', 'CN90053', '124.205.178.150', '2014-05-13 18:43:36.827', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (500, 0, 10360, '0', 1, '2009-12-29 09:31:52.220', 'CN99019', '202.108.36.125', '2009-12-29 10:23:11.313', 'CN99019', '202.108.36.125');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (501, 0, 10361, '0', 5, '2012-12-24 12:12:40.497', 'CN90053', '61.152.133.230', '2014-07-10 19:21:37.857', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (511, 0, 10373, '0', 9, '2012-02-22 14:55:12.647', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.437', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (505, 0, 10375, '0', 3, '2012-02-22 14:55:12.600', 'CN90053', '61.152.133.230', '2012-04-05 15:10:21.437', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (515, 0, 10376, '0', 4, '2012-04-05 15:10:21.500', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (516, 0, 10378, '0', 5, '2012-04-05 15:10:21.513', 'CN90053', '61.152.133.230', '2012-07-20 21:42:39.560', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (547, 0, 10379, '0', 5, '2012-08-28 11:57:16.640', '', '61.152.133.230', '2012-10-29 13:31:05.843', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (563, 0, 10394, '0', 5, '2012-11-06 10:37:56.140', '', '61.152.133.230', '2012-12-24 12:12:40.450', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (523, 0, 10397, '0', 5, '2012-07-20 21:42:39.607', 'CN90053', '61.152.133.230', '2012-08-28 11:57:16.593', '', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (524, 0, 10398, '0', 4, '2012-07-20 21:42:39.623', 'CN90053', '61.152.133.230', '2012-07-20 22:23:09.543', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (608, 0, 10407, '0', 2, '2013-12-27 21:23:13.077', 'CN90053', '222.128.29.241', '2013-12-28 13:55:29.700', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (582, 0, 10413, '0', 5, '2013-05-07 11:00:22.203', 'CN90053', '222.128.26.192', '2013-11-05 10:47:45.950', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (526, 0, 10418, '0', 2, '2012-07-20 21:42:39.623', 'CN90053', '61.152.133.230', '2014-09-10 20:31:24.747', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (528, 0, 10419, '0', 8, '2012-07-20 21:42:39.640', 'CN90053', '61.152.133.230', '2012-07-20 22:23:09.543', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (525, 0, 10423, '0', 1, '2012-07-20 21:42:39.623', 'CN90053', '61.152.133.230', '2014-09-10 20:31:24.747', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (527, 0, 10424, '0', 6, '2012-07-20 21:42:39.640', 'CN90053', '61.152.133.230', '2012-07-20 22:36:22.200', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (568, 0, 10426, '0', 4, '2013-01-04 14:48:04.530', 'CN90053', '61.152.133.230', '2013-11-05 10:47:45.950', 'CN90053', '222.128.29.241');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (571, 0, 10436, '0', 3, '2013-02-06 12:14:50.687', 'CN90053', '61.152.133.230', '2013-02-19 10:27:09.340', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (577, 0, 10437, '0', 6, '2013-04-27 11:00:23.267', 'CN90053', '222.128.26.192', '2013-05-07 11:00:22.140', 'CN90053', '222.128.26.192');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (553, 0, 10438, '0', 7, '2012-10-08 12:01:49.827', '', '61.152.133.230', '2013-05-07 11:00:22.140', 'CN90053', '222.128.26.192');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (548, 0, 10439, '0', 2, '2012-09-28 10:48:41.140', '', '61.152.133.230', '2013-02-19 10:27:09.340', 'CN90053', '61.152.133.230');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (654, 0, 10477, '0', 5, '2014-12-02 10:53:29.560', 'CN90053', '124.205.178.150', '2015-01-27 11:14:12.170', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (617, 0, 10494, '0', 2, '2014-09-10 20:31:24.747', 'CN90053', '124.205.178.150', '2014-09-11 12:45:10.233', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (667, 0, 10618, '0', 5, '2015-01-06 10:27:50.233', 'CN90053', '124.205.178.150', '2015-06-15 16:27:36.297', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (646, 0, 10619, '0', 5, '2014-11-05 12:15:42.090', 'CN90053', '124.205.178.150', '2015-03-20 18:49:33.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (645, 0, 10622, '0', 4, '2014-11-05 12:15:42.077', 'CN90053', '124.205.178.150', '2015-08-25 14:43:36.857', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (618, 0, 10625, '0', 2, '2014-09-10 20:31:24.763', 'CN90053', '124.205.178.150', '2015-03-20 18:49:33.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (641, 0, 10626, '0', 3, '2014-10-08 11:42:12.967', 'CN90053', '124.205.178.150', '2015-03-20 18:49:33.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (620, 0, 10627, '0', 1, '2014-09-11 12:45:10.247', 'CN90053', '124.205.178.150', '2015-03-20 18:49:33.357', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (623, 0, 10630, '0', 1, '2014-09-16 20:39:50.920', 'CN90053', '124.205.178.150', '2014-11-05 12:15:42.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (627, 0, 10631, '0', 2, '2014-09-16 21:02:10.420', 'CN90053', '124.205.178.150', '2014-11-05 12:15:42.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (628, 0, 10634, '0', 3, '2014-09-16 21:02:10.437', 'CN90053', '124.205.178.150', '2014-11-05 12:15:42.030', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (634, 0, 10648, '0', 4, '2014-09-28 14:29:28.590', 'CN90053', '124.205.178.150', '2014-09-28 15:42:20.967', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (651, 0, 10650, '0', 5, '2014-11-21 15:31:38.483', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (650, 0, 10651, '0', 4, '2014-11-21 15:31:38.483', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (649, 0, 10652, '0', 3, '2014-11-21 15:31:38.467', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (630, 0, 10653, '0', 2, '2014-11-21 15:31:38.467', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (629, 0, 10654, '0', 1, '2014-11-21 15:31:38.437', 'CN90053', '124.205.178.150', '2014-12-02 10:53:29.500', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (635, 0, 10655, '0', 1, '2014-09-28 18:21:29.670', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (656, 0, 10656, '0', 2, '2014-12-22 12:20:54.483', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (657, 0, 10657, '0', 3, '2014-12-22 12:20:54.500', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (658, 0, 10658, '0', 4, '2014-12-22 12:20:54.500', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (663, 0, 10659, '0', 5, '2014-12-22 12:48:56.107', 'CN90053', '124.205.178.150', '2015-01-06 10:27:50.140', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (636, 0, 10661, '0', 2, '2014-09-28 18:21:29.687', 'CN90053', '124.205.178.150', '2014-10-08 11:42:12.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (677, 0, 10663, '0', 5, '2015-02-09 12:38:41.250', 'CN90053', '124.205.178.150', '2015-02-12 16:41:28.873', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (672, 0, 10664, '0', 5, '2015-01-27 11:14:12.250', 'CN90053', '124.205.178.150', '2015-02-09 12:38:41.153', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (727, 0, 10667, '0', 4, '2015-09-23 12:36:32.047', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (726, 0, 10670, '0', 3, '2015-09-23 12:36:32.030', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (678, 0, 10671, '0', 1, '2015-02-12 16:41:28.937', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (679, 0, 10672, '0', 2, '2015-02-12 16:41:28.967', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (680, 0, 10673, '0', 3, '2015-02-12 16:41:28.967', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (681, 0, 10674, '0', 5, '2015-02-12 16:41:29.030', 'CN90053', '124.205.178.150', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (682, 0, 10675, '0', 5, '2015-02-12 16:41:29.030', 'CN90053', '124.205.178.150', '2015-02-23 14:37:27.950', 'CN90053', '124.205.178.150');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (684, 0, 10676, '0', 3, '2015-03-20 18:49:33.420', 'CN90053', '124.205.178.150', '2015-08-25 14:43:36.857', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (686, 0, 10677, '0', 4, '2015-03-20 18:49:33.437', 'CN90053', '124.205.178.150', '2015-07-11 17:10:58.500', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (687, 0, 10678, '0', 3, '2015-03-20 18:49:33.437', 'CN90053', '124.205.178.150', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (683, 0, 10679, '0', 1, '2015-03-20 18:49:33.403', 'CN90053', '124.205.178.150', '2015-08-25 14:43:36.857', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (694, 0, 10681, '0', 1, '2015-05-29 09:08:03.390', 'CN90053', '114.112.126.218', '2015-06-01 20:42:46.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (688, 0, 10684, '0', 1, '2015-04-20 14:30:12.310', 'CN90053', '58.30.128.109', '2015-05-11 12:18:20.937', 'CN90053', '124.205.178.210');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (703, 0, 10685, '0', 4, '2015-08-25 14:43:36.937', 'CN90053', '114.112.126.218', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (701, 0, 10686, '0', 2, '2015-08-25 14:43:36.920', 'CN90053', '114.112.126.218', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (690, 0, 10687, '0', 1, '2015-04-27 16:32:28.733', 'CN90053', '124.205.178.210', '2015-09-20 15:56:03.433', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (691, 0, 10688, '0', 3, '2015-04-27 16:32:28.763', 'CN90053', '124.205.178.210', '2015-06-23 13:54:58.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (692, 0, 10689, '0', 5, '2015-04-27 16:32:28.780', 'CN90053', '124.205.178.210', '2015-06-23 13:54:58.920', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (693, 0, 10690, '0', 5, '2015-04-27 16:32:28.780', 'CN90053', '124.205.178.210', '2015-05-11 12:18:20.937', 'CN90053', '124.205.178.210');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (699, 0, 10695, '0', 5, '2015-07-11 17:10:58.560', 'CN90053', '114.112.126.218', '2015-07-21 10:21:55.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (698, 0, 10696, '0', 4, '2015-07-11 17:10:58.547', 'CN90053', '114.112.126.218', '2015-07-21 10:21:55.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (749, 0, 10731, '1', 1, '2016-04-21 12:27:31.793', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.793', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (718, 0, 10794, '0', 5, '2015-09-22 15:39:55.250', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (708, 0, 10798, '0', 5, '2015-09-20 15:56:03.513', 'CN90053', '114.112.126.218', '2015-09-21 13:34:29.797', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (736, 0, 10802, '0', 4, '2015-10-13 11:21:28.263', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (713, 0, 10803, '0', 2, '2015-09-21 13:34:29.873', 'CN90053', '114.112.126.218', '2015-09-23 12:36:31.937', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (706, 0, 10805, '0', 3, '2015-09-20 15:56:03.497', 'CN90053', '114.112.126.218', '2015-09-22 15:39:55.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (707, 0, 10806, '0', 4, '2015-09-20 15:56:03.513', 'CN90053', '114.112.126.218', '2015-09-22 15:39:55.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (705, 0, 10810, '0', 1, '2015-09-20 15:56:03.497', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (704, 0, 10812, '0', 1, '2015-09-20 15:56:03.480', 'CN90053', '114.112.126.218', '2015-09-22 15:39:55.170', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (716, 0, 10813, '0', 2, '2015-09-22 15:39:55.233', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (717, 0, 10815, '0', 3, '2015-09-22 15:39:55.250', 'CN90053', '114.112.126.218', '2016-02-04 12:17:28.780', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (722, 0, 10816, '0', 4, '2015-09-22 17:48:39.750', 'CN90053', '114.112.126.218', '2015-09-23 12:36:31.937', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (724, 0, 10817, '0', 1, '2015-09-23 12:36:32.000', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (725, 0, 10818, '0', 2, '2015-09-23 12:36:32.013', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (732, 0, 10829, '0', 5, '2015-09-30 06:15:18.310', 'CN90053', '114.112.126.218', '2015-10-13 11:21:28.217', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (740, 0, 10833, '0', 4, '2016-02-04 12:17:28.857', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (741, 0, 10834, '0', 5, '2016-02-04 12:17:28.873', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (739, 0, 10836, '0', 3, '2016-02-04 12:17:28.843', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (738, 0, 10837, '0', 2, '2016-02-04 12:17:28.827', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (737, 0, 10838, '0', 1, '2016-02-04 12:17:28.810', 'CN90053', '114.112.126.218', '2016-03-22 11:44:27.967', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (746, 0, 10839, '0', 5, '2016-03-22 11:44:28.060', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.763', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (744, 0, 10841, '0', 3, '2016-03-22 11:44:28.047', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.763', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (753, 0, 10842, '1', 3, '2016-04-21 12:27:31.827', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.827', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (752, 0, 10843, '1', 2, '2016-04-21 12:27:31.810', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.810', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (754, 0, 10844, '1', 4, '2016-04-21 12:27:31.840', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.840', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (745, 0, 10845, '1', 5, '2016-03-22 11:44:28.060', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.857', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (742, 0, 10856, '0', 1, '2016-03-22 11:44:28.030', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.763', 'CN90053', '114.112.126.218');
GO

INSERT INTO [dbo].[TBLRecommendItemList] ([ItemSeq], [CategoryID], [GoldItemID], [Status], [OrderNO], [RegistDate], [RegistAdmin], [RegistIP], [UpdateDate], [UpdateAdmin], [UpdateIP]) VALUES (743, 0, 10857, '0', 2, '2016-03-22 11:44:28.047', 'CN90053', '114.112.126.218', '2016-04-21 12:27:31.763', 'CN90053', '114.112.126.218');
GO

