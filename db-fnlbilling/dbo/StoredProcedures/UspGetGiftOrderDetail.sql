
CREATE PROCEDURE [dbo].[UspGetGiftOrderDetail] 
	 @pGiftType		TINYINT
	 , @pOrderID	VARCHAR(20)
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	
	IF @pGiftType = 1	-- 老馆 备概 沥焊绰 免仿窍瘤 臼绰促.
		RETURN(0)
		
	IF @pGiftType = 0
	BEGIN
		-- system 瘤鞭		
		SELECT
			TOP 1			
			T2.mGmCharNm mNm
			, 0 mSvrNo	-- 矫胶袍 瘤鞭俊绰 辑滚 锅龋甫 馆券窍瘤 臼绰促.
			, T2.mMsg mGiftMsg
			, mLimitedDay =
				CASE WHEN mLimitedDate = '2079-01-01' THEN 0
					ELSE DATEDIFF(DD, GETDATE(), mLimitedDate)
				END 
		FROM dbo.TBLSysOrderList T1 
			INNER JOIN dbo.TBLSysOrder T2
				ON T1.mSysID = T2.mSysID		
		WHERE T1.mSysOrderID = 	CONVERT(INT, @pOrderID)		
			AND T1.mLimitedDate > GETDATE() 
	END 	
	
	IF @pGiftType = 2	-- 急拱 皋技瘤
	BEGIN	
	
		SELECT 
			TOP 1
			mNm
			, mSvrNo
			, mGiftMsg
			, 0 mLimitedDay
		FROM dbo.TBLWebOrder
		WHERE mWebOrderID = @pOrderID		
	END 

	RETURN(0)

GO

