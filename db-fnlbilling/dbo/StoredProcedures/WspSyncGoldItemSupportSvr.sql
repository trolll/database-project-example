CREATE PROCEDURE dbo.WspSyncGoldItemSupportSvr     
    @inputdata VARCHAR(8000),
    @ReturnValue INT OUTPUT
AS
BEGIN
    SET NOCOUNT ON    
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    create table #tmptbl(GoldItemID bigint,mSvrNo smallint);
    if @@ERROR <> 0
    begin
        set @ReturnValue=0
        return
    end

    declare   @ch   as   varchar(100)
    declare   @StrSeprate  as char(1)
    declare   @StrSeprate2  as char(1)
    declare   @GoldItemID  as varchar(100)
    declare   @mSvrNo as varchar(100)
    declare   @sep2index as int
        
    set @StrSeprate=';'
    set @StrSeprate2=','
  
    while(@inputdata<>'')   
    begin   
        set   @ch=left(@inputdata,charindex(@StrSeprate,@inputdata,1)-1)   

        set @sep2index = charindex(@StrSeprate2,@ch,1);
        set @GoldItemID=left(@ch,@sep2index-1)
        set @mSvrNo=stuff(@ch,1,@sep2index,'')
        print 'GoldItemID=' + @GoldItemID + ' and mSvrNo=' + @mSvrNo
        set @inputdata=stuff(@inputdata,1,charindex(@StrSeprate,@inputdata,1),'')   
        insert into #tmptbl(GoldItemID,mSvrNo) values(@GoldItemID,@mSvrNo)
        if @@ERROR <> 0
        begin
            set @ReturnValue=0
            return
        end
    end   

    delete from dbo.TBLGoldItemSupportSvr where 
    GoldItemID in (select GoldItemID from #tmptbl)
    and convert(varchar,GoldItemID)+','+convert(varchar,mSvrNo) not in 
    (select convert(varchar,GoldItemID)+','+convert(varchar,mSvrNo) from #tmptbl);
    if @@ERROR <> 0
    begin
        set @ReturnValue=0
        return
    end

    insert into dbo.TBLGoldItemSupportSvr select GoldItemID,mSvrNo from #tmptbl where convert(varchar,GoldItemID)+','+convert(varchar,mSvrNo) not in 
    (select convert(varchar,GoldItemID)+','+convert(varchar,mSvrNo) from dbo.TBLGoldItemSupportSvr);
    if @@ERROR <> 0
    begin
        set @ReturnValue=0
        return
    end

    set @ReturnValue=1
END

GO

