CREATE PROCEDURE dbo.UPDATE_COUPON_ITEM
	@pMasterSeq		INT
	, @pItemNo		INT
	, @pItemCnt		INT	
	, @pOp			BIT = 0	
	, @pAvailablePeriod	int = 0
	, @pPracticalPeriod	int = 0
	, @pStatus	tinyint = 1
	, @pBindingType	tinyint = 0
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF ( @pOp = 1 )
	BEGIN
	
		IF EXISTS(	SELECT *
					FROM dbo.R2_ADD_ITEM_LIST 
					WHERE PRODUCT_NO = @pMasterSeq
							AND ITEM_NO = @pItemNo )
		BEGIN
			
			DELETE dbo.R2_ADD_ITEM_LIST
			WHERE PRODUCT_NO = @pMasterSeq
							AND ITEM_NO = @pItemNo
			IF @@ERROR <> 0 OR @@ROWCOUNT = 0
			BEGIN
				RETURN(1)
			END 
			
			RETURN(0)
		END 				

		INSERT INTO dbo.R2_ADD_ITEM_LIST(PRODUCT_NO,ITEM_NO,ITEM_CNT
			,AvailablePeriod
			,PracticalPeriod
			,mStatus
			,mBindingType)
		VALUES(
			@pMasterSeq,
			@pItemNo,
			@pItemCnt,
			@pAvailablePeriod,
			@pPracticalPeriod,
			@pStatus,
			@pBindingType
		)

		IF @@ERROR <> 0 OR @@ROWCOUNT = 0
		BEGIN
			RETURN(1)
		END 	
	
		RETURN(0)
	END 
		
	IF EXISTS(	SELECT *
				FROM dbo.R2_COUPON_ITEM_LIST 
				WHERE MASTER_SEQ = @pMasterSeq
						AND ITEM_NO = @pItemNo )
	BEGIN
		
		DELETE dbo.R2_COUPON_ITEM_LIST
		WHERE MASTER_SEQ = @pMasterSeq
						AND ITEM_NO = @pItemNo
		IF @@ERROR <> 0 OR @@ROWCOUNT = 0
		BEGIN
			RETURN(1)
		END 
		
		RETURN(0)
	END 				
	
	INSERT INTO dbo.R2_COUPON_ITEM_LIST(MASTER_SEQ,ITEM_NO,ITEM_CNT
		,AvailablePeriod
		,PracticalPeriod
		,mStatus
		,mBindingType)
	VALUES(
		@pMasterSeq,
		@pItemNo,
		@pItemCnt,
		@pAvailablePeriod,
		@pPracticalPeriod,
		@pStatus,
		@pBindingType
	)	
	

	IF @@ERROR <> 0 OR @@ROWCOUNT = 0
	BEGIN
		RETURN(1)
	END 
	
	RETURN(0)

GO

