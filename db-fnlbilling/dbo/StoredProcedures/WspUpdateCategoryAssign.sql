CREATE PROCEDURE dbo.WspUpdateCategoryAssign  	
	@CategoryID BIGINT,
	@GoldItemID BIGINT,
	@Status CHAR(1),
	@OrderNO INT,
	@UpdateAdmin NVARCHAR(20),
	@UpdateIP NVARCHAR(19),
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    if(exists(select 1 from TBLCategoryAssign where GoldItemID=@GoldItemID))
	begin
	    update TBLCategoryAssign set
	        CategoryID=@CategoryID,
	        Status=@Status,
	        OrderNO=@OrderNO,
	        UpdateDate=getdate(),
	        UpdateAdmin=@UpdateAdmin,
	        UpdateIP=@UpdateIP
	    where GoldItemID = @GoldItemID
	    IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
	        BEGIN
	            set @ReturnValue=0
	        END
	    ELSE
	        BEGIN
	            set @ReturnValue=1
	        END
	end
	else
	begin
	    insert into TBLCategoryAssign (CategoryID,GoldItemID,Status,OrderNO,RegistDate,RegistAdmin,RegistIP,UpdateDate,UpdateAdmin,UpdateIP) 
	     values(@CategoryID,@GoldItemID,@Status,@OrderNO,getdate(),@UpdateAdmin,@UpdateIP,getdate(),@UpdateAdmin,@UpdateIP)
		
	    IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
	        BEGIN
	            set @ReturnValue=0
	        END
	    ELSE
	        BEGIN
	            set @ReturnValue=1
	        END
	end

GO

