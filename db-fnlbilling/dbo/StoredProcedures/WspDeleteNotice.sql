CREATE PROCEDURE dbo.WspDeleteNotice  	
	@NoticeSeq BIGINT,
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	update TBLNotice set status='2',UpdateDate=getdate() where NoticeSeq = @NoticeSeq
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
        BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END

GO

