

CREATE  PROCEDURE dbo.WspInsertPCCafeItemList  	
	@ItemSeq BIGINT,
	@GoldItemID BIGINT,
	@Status CHAR(1),
	@OrderNO INT,
	@RegistAdmin NVARCHAR(20),
	@RegistIP NVARCHAR(19),
	@UpdateAdmin NVARCHAR(20),
	@UpdateIP NVARCHAR(19),
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    if(exists(select 1 from TBLPCCafeItemList where GoldItemID=@GoldItemID))
    begin

        update TBLPCCafeItemList set
            Status=@Status,
            OrderNO=@OrderNO,
            UpdateDate=getdate(),
            UpdateAdmin=@UpdateAdmin,
            UpdateIP=@UpdateIP
        where GoldItemID=@GoldItemID
        
        IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
            BEGIN
                set @ReturnValue=0
            END
        ELSE
            BEGIN
                set @ReturnValue=1
            END
    end
	else
	begin   

        insert into TBLPCCafeItemList
        (ItemSeq,GoldItemID,Status,OrderNO,RegistDate,RegistAdmin,RegistIP,UpdateDate,UpdateAdmin,UpdateIP
        ) values (
        @ItemSeq,@GoldItemID,@Status,@OrderNO,getdate(),@RegistAdmin,@RegistIP,getdate(),@UpdateAdmin,@UpdateIP
        )
        
        IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
            BEGIN
                set @ReturnValue=0
            END
        ELSE
            BEGIN
                set @ReturnValue=1
            END
    end

GO

