CREATE PROCEDURE dbo.WspAllDeleteRecommendItemList  	
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	update TBLRecommendItemList set status='0',UpdateDate=getdate() where status<>'0'
	
	IF @@ERROR <> 0
        BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END

GO

