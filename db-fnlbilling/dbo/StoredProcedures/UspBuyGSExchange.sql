CREATE PROCEDURE dbo.UspBuyGSExchange
	@pExchangeID	BIGINT,
	@pBmUserNo		INT,
	@pBmNo			INT,
	@pBmNm			NVARCHAR(12),
	@pSvrNo			SMALLINT,
	@pBillingExchangeID	NVARCHAR(20),
	@pBillingReturnCode	INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE @aErr INT, @aRowCnt INT
	SELECT @aErr = 0, @aRowCnt = 0
	
	IF NOT EXISTS( SELECT *
					FROM dbo.TBLGSExchange
					WHERE ExchangeID = @pExchangeID
						AND Status = '2' )
	BEGIN
		RETURN(2) -- 거래 등록할 수 없는 상태(거래 진행중 상태가 아닌경우) 
	END 
		 
	IF @pBillingExchangeID IS NULL
		OR @pBillingExchangeID = N''
	BEGIN
		
		UPDATE dbo.TBLGSExchange
		SET 
			BmUserNo = @pBmUserNo,
			BmNo = @pBmNo,
			BmNm = @pBmNm,
			Status = '3'	-- 구매 실행 신청
		WHERE ExchangeID = @pExchangeID
			AND Status = '2' 
			AND mSvrNo = @pSvrNo
		
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErr <> 0 OR @aRowCnt <= 0
		BEGIN
			RETURN(1) -- db error
		END 

		RETURN(0)	
	END 	
		
	UPDATE dbo.TBLGSExchange
	SET 
		BmUserNo = @pBmUserNo,
		BmNo = @pBmNo,
		BmNm = @pBmNm,
		Status = '3',
		BmBillingExchangeID = @pBillingExchangeID,	-- 구매시 빌링 ID
		BillingReturnCode = @pBillingReturnCode		
	WHERE ExchangeID = @pExchangeID
		AND Status = '2' 
		AND mSvrNo = @pSvrNo
	
	SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErr <> 0 OR @aRowCnt <= 0
	BEGIN
		RETURN(1) -- db error
	END 

	RETURN(0)

GO

