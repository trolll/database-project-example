

CREATE  PROCEDURE dbo.WspGetOrderListByUserID
	@pUserId varchar(20)
	,@pSvrNo	SMALLINT
	,@pPageNum	INT
	,@pPageSize	INT
    ,@pPageCount INT output
    ,@pRecordCount INT output
AS
BEGIN

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @TotalCount INT

    DECLARE @pUserNo INT
    SELECT @pUserNo=mUserNo from FNLAccount.dbo.TBLUser where mUserId=@pUserId

	SELECT @TotalCount = COUNT(*) 
	FROM dbo.TBLOrderList
	WHERE mUserNo = @pUserNo
		AND mSrvNo = @pSvrNo
		AND OrderStatus = N'2'

	DECLARE @SQL NVARCHAR(1000)
	DECLARE @PARAMS NVARCHAR(100)

	SET @SQL = ''
	SET @SQL = @SQL + N''
	SET @SQL = @SQL + N' SELECT TOP ' + STR(@pPageSize) + N' '	
	SET @SQL = @SQL + N'	o.OrderID '
	SET @SQL = @SQL + N'	, o.mNm '
	SET @SQL = @SQL + N'	, o.GoldItemID,g.ItemName'
	SET @SQL = @SQL + N'	, o.Count '
	SET @SQL = @SQL + N'	, o.UseGold '
	SET @SQL = @SQL + N'	, o.OrderDate '
	SET @SQL = @SQL + N' FROM dbo.TBLOrderList o,dbo.TBLGoldItem g'
	SET @SQL = @SQL + N' WHERE o.GoldItemID=g.GoldItemID and mUserNo = @UserNo '
	SET @SQL = @SQL + N'	AND mSrvNo = @SvrNo '
	SET @SQL = @SQL + N'	AND OrderStatus = N''2'' '
	SET @SQL = @SQL + N'	AND OrderID NOT IN '
	SET @SQL = @SQL + N'	('
	SET @SQL = @SQL + N'		SELECT TOP ' + STR(@pPageSize * (@pPageNum -1)) + N' '
	SET @SQL = @SQL + N'			OrderID '
	SET @SQL = @SQL + N'		FROM  dbo.TBLOrderList '
	SET @SQL = @SQL + N'		WHERE mUserNo = @UserNo '
	SET @SQL = @SQL + N'			AND mSrvNo = @SvrNo '
	SET @SQL = @SQL + N'			AND OrderStatus = N''2'' '
	SET @SQL = @SQL + N'		ORDER BY OrderID DESC'
	SET @SQL = @SQL + N'	)'
	SET @SQL = @SQL + N' ORDER BY OrderID DESC'
	SET @PARAMS = '@UserNo INT, @SvrNo SMALLINT'

	EXEC SP_EXECUTESQL @SQL, @PARAMS, @UserNo = @pUserNo, @SvrNo = @pSvrNo
    SELECT @pRecordCount = @TotalCount
    SELECT @pPageCount = ceiling(1.0*@TotalCount/@pPageSize)
END

GO

