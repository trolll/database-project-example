

CREATE PROCEDURE dbo.UspGetGoldItem
	@pSvrNo	SMALLINT	-- 瘤盔辑滚
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 1000	-- 1000俺肺 力茄

	SELECT 
		T1.GoldItemID
		, OrderNo
		, IID
		, ItemName
		, ItemDesc
		, CONVERT(SMALLINT, ItemCateGory) ItemCateGory
		, CategoryID
		, IsPackage
		, OriginalGoldPrice
		, GoldPrice
		, AvailablePeriod
		, Count
		, IsPCCafeItem
		, Status
		, PracticalPeriod
	FROM (
		SELECT 
			T1.GoldItemID
			, OrderNo = 
				CASE WHEN T3.GoldItemID IS NOT NULL THEN T3.OrderNO
					ELSE ISNULL(T2.OrderNO, 32000)
				END 
			, T1.IID
			, T1.ItemName
			, T1.ItemDesc
			, ItemCateGory =
				CASE 
					WHEN ASCII(T1.ItemCategory) >= 65 THEN CONVERT(SMALLINT, ASCII(T1.ItemCategory) - ASCII(N'A') + 10)
					ELSE CONVERT(SMALLINT, T1.ItemCategory)
				END 	
			, T2.CategoryID
			, CONVERT(TINYINT, T1.IsPackage) AS IsPackage			
			, T1.OriginalGoldPrice
			, T1.GoldPrice
			, T1.AvailablePeriod
			, T1.Count
			, IsPCCafeItem =							-- PC规 傈侩 酒捞袍 咯何 					
				CASE WHEN T3.GoldItemID IS NOT NULL THEN 1
					ELSE 0
				END
			, Status =
				CASE WHEN T3.GoldItemID IS NOT NULL THEN CONVERT(TINYINT, T3.Status) 
					ELSE CONVERT(TINYINT, T1.Status)
				END	
			, T1.PracticalPeriod					
		FROM dbo.TBLGoldItem T1
			INNER JOIN dbo.TBLCategoryAssign T2
				ON T1.GoldItemID = T2.GoldItemID			
			LEFT OUTER JOIN dbo.TBLPCCafeItemList T3
				ON T1.GoldItemID = T3.GoldItemID	
		WHERE T1.Status IN( N'1', N'2' )				-- 菩虐瘤 备己 酒捞袍鳖瘤 葛滴 殿废秦 初绰促.				
	) T1 INNER JOIN dbo.TBLGoldItemSupportSvr T2
		ON T1.GoldItemID = T2.GoldItemID
	WHERE T2.mSvrNo = @pSvrNo	
	ORDER BY IsPCCafeItem ASC, OrderNo ASC

GO

