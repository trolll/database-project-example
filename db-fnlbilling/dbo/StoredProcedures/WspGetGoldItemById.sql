

CREATE PROCEDURE dbo.WspGetGoldItemById  	
	@GoldItemID BIGINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	select GoldItemID,ItemName,IID,ItemImage,ItemDesc,OriginalGoldPrice,GoldPrice,
    ItemCategory,IsPackage,AvailablePeriod,Count,PracticalPeriod,Status 
    from TBLGoldItem 
    where GoldItemID=@GoldItemID

GO

