

CREATE  PROCEDURE dbo.WspGetCategoryById  	
	@CategoryID BIGINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	select CategoryID,CategoryName,CategoryDesc ,Status
	from TBLCategory t where t.CategoryID = @CategoryID

GO

