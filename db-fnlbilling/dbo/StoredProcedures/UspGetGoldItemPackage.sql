CREATE PROCEDURE dbo.UspGetGoldItemPackage
	@pSvrNo	SMALLINT	-- 지원서버
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 500 -- 최대 제한 갯수 500개
	
	SELECT 
		T3.GoldItemID
		, T3.PackageItemID
		, T3.Count	
	FROM dbo.TBLPackageItem T3
		INNER JOIN dbo.TBLGoldItemSupportSvr T4
			ON T3.GoldItemID = T4.GoldItemID
		INNER JOIN dbo.TBLGoldItem T5
			ON T4.GoldItemID = T5.GoldItemID
	WHERE PackageItemID IN (
		SELECT T1.GoldItemID
		FROM dbo.TBLGoldItem T1
			INNER JOIN dbo.TBLGoldItemSupportSvr T2
				ON T1.GoldItemID = T2.GoldItemID
		WHERE T2.mSvrNo = @pSvrNo
			AND	T1.IsPackage = 1
			AND T1.Status IN ( N'1', N'2' )  )
		AND T4.mSvrNo = @pSvrNo
		AND T3.Status IN (N'1', N'2')
		AND T5.Status IN (N'1', N'2')
	ORDER BY T3.PackageItemID

GO

