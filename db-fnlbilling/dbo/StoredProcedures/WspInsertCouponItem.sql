
CREATE PROCEDURE dbo.WspInsertCouponItem  
	        @masterSeq INT,
	        @itemNo INT,
	        @itemCnt INT,
	        @regDate NVARCHAR(10),
	        @condType INT,
	        @condValue NVARCHAR(10),
	        @orderNum INT
	AS		
		INSERT INTO R2_COUPON_ITEM_LIST 
		    (MASTER_SEQ,ITEM_NO,ITEM_CNT,REG_DATE,COND_TYPE,COND_VALUE,ORDER_NUM)
		VALUES (@masterSeq,@itemNo,@itemCnt,@regDate,@condType,@condValue,@orderNum)

GO

