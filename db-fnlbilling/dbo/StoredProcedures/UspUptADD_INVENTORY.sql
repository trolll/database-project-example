/******************************************************************************
**		Name: UspUptADD_INVENTORY
**		Desc: 何啊辑厚胶 沥焊甫 诀单捞甫
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/ 
CREATE PROCEDURE dbo.UspUptADD_INVENTORY
	@pUserID VARCHAR(20)
	, @aUserNo INT
AS
	SET NOCOUNT ON
	
	DECLARE @aErr INT, @aRowCnt INT

	SELECT @aErr = 0, @aRowCnt = 0

	UPDATE T2
	SET
		MEMBERID = @pUserID
	FROM dbo.R2_ADD_INVENTORY_20101101_CHECK T1
		INNER JOIN dbo.R2_ADD_INVENTORY T2
			ON T1.INVENTORY_NO = T2.INVENTORY_NO
	WHERE T1.mUserNO =  @aUserNo	

	SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT
		
	IF @aErr <> 0
	BEGIN
		RETURN(1) -- db error
	END 

	RETURN(0)

GO

