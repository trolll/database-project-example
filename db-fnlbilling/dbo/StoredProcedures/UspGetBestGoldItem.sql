

CREATE PROCEDURE dbo.UspGetBestGoldItem
	@pSvrNo	SMALLINT	-- 瘤盔辑滚
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 10
	
	SELECT 
		T1.GoldItemID		
	FROM dbo.TBLBestItemList T1
		INNER JOIN dbo.TBLGoldItem T2
			ON T1.GoldItemID = T2.GoldItemID
		INNER JOIN dbo.TBLGoldItemSupportSvr T3
			ON T1.GoldItemID = T3.GoldItemID
	WHERE T3.mSvrNo = @pSvrNo
		AND T2.Status = N'1'	-- 钎矫 亲格父 畴免 
		AND T1.Status = N'1'
	ORDER BY T1.OrderNO ASC

GO

