




CREATE PROCEDURE [dbo].[AP_INDEX_TBL_HISTORY]  	
AS
	-- TblPc_history
	CREATE INDEX NC_TblPc_history_1	ON TblPc_history(mNo)
	CREATE INDEX NC_TblPc_history_2	ON TblPc_history(mOwner)
	CREATE INDEX NC_TblPc_history_3	ON TblPc_history(mNm)
	
	-- TblPcStore	
	CREATE INDEX NC_TblPcStore_history_1	ON TblPcStore_history(mUserNo)
	CREATE INDEX NC_TblPcStore_history_2	ON TblPcStore_history(mItemNo, mStatus, mCnt)
	
	-- TblPcInventory_history
	CREATE INDEX NC_TblPcInventory_history_1	ON TblPcInventory_history(mPcNo)
	CREATE INDEX NC_TblPcInventory_history_2	ON TblPcInventory_history(mItemNo, mStatus, mCnt)
	CREATE INDEX NC_TblPcInventory_history_3	ON TblPcInventory_history(mSerialNo)

	-- TblGuildStore
	CREATE INDEX [NC_TblGuildStore_history_1] ON [dbo].[TblGuildStore_history](mGuildNo)
	CREATE INDEX [NC_TblGuildStore_history_2] ON [dbo].[TblGuildStore_history](mItemNo, mStatus, mCnt)

	--20070118
	CREATE INDEX [NC_TblCastleTowerStone_historyGuildNo] ON [dbo].[TblCastleTowerStone_history] ([mGuildNo]) 	
	CREATE INDEX [NC_TblPcState_historyNo] ON [dbo].[TblPcState_history]([mNo]) 
	CREATE INDEX [NC_TblGuildMember_historyGuildNo] ON [dbo].[TblGuildMember_history]([mGuildNo], [mPcNo]) 
	CREATE INDEX [NC_TblGuild_historyGuildNo] ON [dbo].[TblGuild_history]([mGuildNo],[mGuildNm]) 
	
	CREATE INDEX [NC_TblBanquetHall_history] ON [dbo].[TblBanquetHall_history]([mOwnerPcNo])
	CREATE INDEX [NC_TblGuildAgitAuction_history] ON [dbo].[TblGuildAgitAuction_history]([mCurBidGID])
	CREATE INDEX [NC_TblGuildAgit_history] ON [dbo].[TblGuildAgit_history]([mOwnerGID])
	
	-- 20080108
	CREATE INDEX [NC_TblGuildStorePassword_history] ON [dbo].[TblGuildStorePassword_history]([mGuildNo])
	CREATE INDEX [NC_TblPcGoldItemEffect_history] ON [dbo].[TblPcGoldItemEffect_history]([mPcNo])
	CREATE INDEX [NC_TblGuildGoldItemEffect_history] ON [dbo].[TblGuildGoldItemEffect_history]([mGuildNo])

	-- 20120810
	CREATE INDEX NC_TblPcBead_history_mOwnerSerialNo ON dbo.TblPcBead_history (mOwnerSerialNo)
	CREATE INDEX NC_TblPcBead_history_mOwnerSerialNo_mBeadIndex ON dbo.TblPcBead_history (mOwnerSerialNo, mBeadIndex)
	CREATE INDEX NC_TblPcBead_history_mOwnerSerialNo_mItemNo ON dbo.TblPcBead_history (mOwnerSerialNo, mItemNo)

GO

