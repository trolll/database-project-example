CREATE PROCEDURE [dbo].[AP_MOVE_TBL_HISTORY]    
AS    
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
 SET NOCOUNT ON    
     
 -- prev history table rename    
 -- EXEC dbo.AP_CYCLE_TBL_HISTORY    
    
 select mRegDate,mNo,mHp,mIsOpen    
 into dbo.TblCastleGate_history    
 from FNLGame1156.dbo.TblCastleGate     
    
 select mRegDate,mChgDate,mIsTower,mPlace,mGuildNo,mTaxBuy,mTaxBuyMax,mTaxHunt,mTaxHuntMax,mTaxGamble,mTaxGambleMax,mAsset    
 into dbo.TblCastleTowerStone_history    
 from FNLGame1156.dbo.TblCastleTowerStone     
    
 select mRegDate,mGuildNo,mPlace,mNode,mPcNo    
 into dbo.TblGkill_history    
 from FNLGame1156.dbo.TblGkill     
    
 select mRegDate,mGuildNo,mGuildSeqNo,mGuildNm,mGuildMsg,mGuildMark,mRewardExp    
 into dbo.TblGuild_history    
 from FNLGame1156.dbo.TblGuild     
    
 select mRegDate,mGuildNo    
 into dbo.TblGuildAss_history    
 from FNLGame1156.dbo.TblGuildAss     
    
 select mRegDate,mGuildAssNo,mGuildNo    
 into dbo.TblGuildAssMem_history    
 from FNLGame1156.dbo.TblGuildAssMem     
    
 select mRegDate,mGuildNo,mPcNo,mGuildGrade,mNickNm    
 into dbo.TblGuildMember_history      
 from FNLGame1156.dbo.TblGuildMember     
    
 select mRegDate,mOwner,mSlot,mNo,mNm,mClass,mSex,mHead,mFace,mBody,mHomeMapNo,mHomePosX,mHomePosY,mHomePosZ,mDelDate    
 into dbo.TblPc_history    
 from FNLGame1156.dbo.TblPc     
    
 select mRegDate,mOwner,mSlot,mSerialNo    
 into dbo.TblPcEquip_history    
 from FNLGame1156.dbo.TblPcEquip     
    
 select mPcNo,mQuestNo,mValue    
 into dbo.TblPcQuest_history    
 from FNLGame1156.dbo.TblPcQuest     
    
 -- 2007.05.15    
 select mNo,mLevel,mExp,mHpAdd,mHp,mMpAdd,mMp,mMapNo,mPosX,mPosY,mPosZ,mStomach,mIp,mLoginTm,mLogoutTm,mTotUseTm,mPkCnt,mChaotic, mPartyMemCntLevel,mDiscipleJoinCount,mLostExp    
 into dbo.TblPcState_history    
 from FNLGame1156.dbo.TblPcState     
     
 -- 2008.01.08    
 SELECT mRegDate, mSerialNo, mUserNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse, mIsSeizure, mApplyAbnItemNo, mApplyAbnItemEndDate,mOwner, mPracticalPeriod, mBindingType, mRestoreCnt, mHoleCount    
 into dbo.TblPcStore_history    
 FROM FNLGame1156.dbo.TblPcStore     
     
 SELECT mRegDate, mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse, mIsSeizure, mApplyAbnItemNo, mApplyAbnItemEndDate,mOwner, mPracticalPeriod, mBindingType, mRestoreCnt, mHoleCount    
 into dbo.TblPcInventory_history    
 FROM FNLGame1156.dbo.TblPcInventory     
    
 -- 2007.06.26    
 SELECT mTerritory, mBanquetHallNo, mBanquetHallType, mOwnerPcNo, mRegDate, mLeftMin    
 into dbo.TblBanquetHall_history    
 FROM FNLGame1156.dbo.TblBanquetHall    
     
 SELECT mTerritory, mGuildAgitNo, mCurBidGID, mCurBidMoney, mRegDate    
 into dbo.TblGuildAgitAuction_history    
 FROM FNLGame1156.dbo.TblGuildAgitAuction    
    
 SELECT mTerritory, mGuildAgitNo, mOwnerGID, mGuildAgitName, mRegDate, mLeftMin, mIsSelling, mSellingMoney,mBuyingMoney    
 into dbo.TblGuildAgit_history    
 FROM FNLGame1156.dbo.TblGuildAgit    
     
 -- 2008.01.08    
 SELECT mRegDate, mGuildNo, mPassword, mGrade    
 INTO dbo.TblGuildStorePassword_history     
 FROM FNLGame1156.dbo.TblGuildStorePassword    
      
 SELECT mRegDate, mSerialNo, mGuildNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse, mIsSeizure, mApplyAbnItemNo, mApplyAbnItemEndDate, mOwner, mGrade, mPracticalPeriod, mBindingType, mRestoreCnt, mHoleCount    
 INTO dbo.TblGuildStore_history     
 FROM FNLGame1156.dbo.TblGuildStore    
     
 SELECT mRegDate, mGuildNo, mItemType, mParmA, mEndDate, mItemNo    
 INTO dbo.TblGuildGoldItemEffect_history     
 FROM FNLGame1156.dbo.TblGuildGoldItemEffect    
     
 SELECT mRegDate, mPcNo, mItemType, mParmA, mEndDate, mItemNo    
 INTO dbo.TblPcGoldItemEffect_history    
 FROM FNLGame1156.dbo.TblPcGoldItemEffect    
      
 -- 2009.06.15    
 SELECT mGID, mGuildMoney, mRegDate    
 INTO dbo.TblGuildAccount_history    
 FROM FNLGame1156.dbo.TblGuildAccount    
    
     
 -- 2011.03.02    
 SELECT    
  mRegDate, mCnsmNo, mCategoryNo, mPrice, mTradeEndDate, mPcNo, mRegCnt, mCurCnt, mItemNo    
 INTO dbo.TblConsignment_history     
 FROM FNLGame1156.dbo.TblConsignment    
     
 SELECT    
  mPcNo, mBalance    
 INTO dbo.TblConsignmentAccount_history     
 FROM FNLGame1156.dbo.TblConsignmentAccount    
     
 SELECT    
  mCnsmNo, mRegDate, mSerialNo, mEndDate, mIsConfirm, mStatus, mCntUse    
  , mOwner, mPracticalPeriod, mBindingType, mRestoreCnt    
 INTO dbo.TblConsignmentItem_history     
 FROM FNLGame1156.dbo.TblConsignmentItem      
     
 -- 2012.08.10    
 SELECT mRegDate, mOwnerSerialNo, mBeadIndex, mItemNo, mEndDate, mCntUse    
 INTO dbo.TblPcBead_history    
 FROM FNLGame1156.dbo.TblPcBead    
    
 -- prev history table drop    
 EXEC AP_CYCLE_TBL_HISTORY_DROP_TABLE    
       
 -- index create    
 EXEC dbo.AP_INDEX_TBL_HISTORY

GO

