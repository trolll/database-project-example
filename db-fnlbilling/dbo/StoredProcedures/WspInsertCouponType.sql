
CREATE PROCEDURE dbo.WspInsertCouponType 
	        @couponTypeNo INT,
	        @couponType NVARCHAR(100)
	AS				
		INSERT INTO R2_COUPON_TYPE
		    (COUPON_TYPE_NO,COUPON_TYPE)
		VALUES ( @couponTypeNo,@couponType)

GO

