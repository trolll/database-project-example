CREATE PROCEDURE dbo.WspGetGoldItemsOfNonPackage  	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
		
	SELECT GoldItemID,ItemName,IID,ItemImage,ItemDesc,OriginalGoldPrice,GoldPrice,
		ItemCategory,IsPackage,AvailablePeriod,Count,PracticalPeriod,Status 
	FROM dbo.TBLGoldItem 
	WHERE isPackage=N'0' and status IN( N'1', N'2')
	ORDER BY GoldItemID ASC

GO

