CREATE PROCEDURE dbo.WspGetGoldItemSimpleBySelectStatus
  @selectStatus INT
AS
 SET NOCOUNT ON
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

 ---- get new update list
 IF @selectStatus = 3
             BEGIN
  SELECT
   T1.GoldItemID
   , T1.IID
   , T1.ItemName
   , T1.ItemDesc
   , T1.ItemCategory
   , T1.IsPackage   
   , T1.OriginalGoldPrice
   , T1.GoldPrice
   , T1.AvailablePeriod
   , T1.Count
   , T1.Status
   , T1.PracticalPeriod     
  FROM dbo.TBLGoldItem T1
           WHERE T1.UpdateDate > getDate() - 7
  ORDER BY updateDate DESC
      END
 ---- get new add list
 ELSE IF @selectStatus = 2
     BEGIN
  SELECT
   T1.GoldItemID
   , T1.IID
   , T1.ItemName
   , T1.ItemDesc
   , T1.ItemCategory
   , T1.IsPackage   
   , T1.OriginalGoldPrice
   , T1.GoldPrice
   , T1.AvailablePeriod
   , T1.Count
   , T1.Status
   , T1.PracticalPeriod     
  FROM dbo.TBLGoldItem T1
           WHERE T1.RegistDate > getDate() - 7
  ORDER BY RegistDate DESC
     END
 ELSE
     BEGIN
  SELECT
   T1.GoldItemID
   , T1.IID
   , T1.ItemName
   , T1.ItemDesc
   , T1.ItemCategory
   , T1.IsPackage   
   , T1.OriginalGoldPrice
   , T1.GoldPrice
   , T1.AvailablePeriod
   , T1.Count
   , T1.Status
   , T1.PracticalPeriod     
  FROM dbo.TBLGoldItem T1
  ORDER BY updateDate DESC
     END

GO

