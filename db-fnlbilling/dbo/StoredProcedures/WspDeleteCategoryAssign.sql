CREATE PROCEDURE dbo.WspDeleteCategoryAssign  	
	@CategoryID BIGINT,
	@GoldItemID BIGINT,
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	update TBLCategoryAssign set status = '0',UpdateDate=getdate() where
		CategoryID=@CategoryID and GoldItemID = @GoldItemID
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
        BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END

GO

