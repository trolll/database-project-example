





CREATE  PROCEDURE dbo.UspUptOrderStatus
	@pOrderID	BIGINT
	,@pBillingOrderID	nvarchar(20)
	,@pBillingReturnCode	int
	,@pOrderStatus NCHAR(1)
AS
	SET NOCOUNT ON
	
	DECLARE @aErr INT, @aRowCnt INT

	SELECT @aErr = 0, @aRowCnt = 0
	
	UPDATE dbo.TBLOrderList
	SET OrderStatus = @pOrderStatus,
		BillingOrderID = @pBillingOrderID,
		BillingReturnCode =  @pBillingReturnCode
	WHERE OrderID = @pOrderID
	SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT
		
	IF @aErr <> 0 OR @aRowCnt <= 0
	BEGIN
		RETURN(1) -- db error
	END 

	RETURN(0)

GO

