/******************************************************************************  
**  Name: UspGetGiftOrderItem
**  Desc: 急拱窃俊辑 酒捞袍 茫扁
**  
**                
**  Return values:  
**   饭内靛悸
**   
**                
**  Author: 
**  Date:
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**  2016-06-14  沥柳龋	   mRestoreCnt 拿烦阑 啊廉柯促.(酒捞袍 楷厘冉荐)
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetGiftOrderItem] 
	 @pGiftType		TINYINT
	 , @pOrderID	VARCHAR(20)
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
		
	IF @pGiftType = 0
	BEGIN
		-- system 瘤鞭		
		SELECT
			TOP 1			
			mUserNo
			, mSvrNo
			, mItemID
			, mCnt
			, mPracticalPeriod
			, mAvailablePeriod
			, mItemStatus
			, mBindingType			
			, mRestoreCnt			-- 酒捞袍 楷厘冉荐甫 眉农窍扁 困秦 荤侩
		FROM dbo.TBLSysOrderList 	
		WHERE mSysOrderID = 	CONVERT(INT, @pOrderID)	
			AND mLimitedDate > GETDATE()
		
		RETURN	
	END 	
	

	SELECT 
		T1.mUserNo			
		, T1.mSvrNo
		, T1.GoldItemID mItemID		-- GoldItem 
		, T1.Count		mCnt
		, T2.PracticalPeriod mPracticalPeriod
		, T2.AvailablePeriod mAvailablePeriod
		, 1 mItemStatus				-- 酒捞袍 惑怕(1: 老馆)
		, 0 mBindingType			-- 蓖加 鸥涝(0:蓖加 酒捞袍 鸥涝捞 酒聪促)		
		, 0	mRestoreCnt				-- mRestoreCnt绰 system 瘤鞭父 眉农茄促.
	FROM dbo.TBLWebOrderList T1
		INNER JOIN dbo.TBLGoldItem T2
			ON T1.GoldItemID = T2.GoldItemID
	WHERE T1.mWebOrderID = @pOrderID

GO

