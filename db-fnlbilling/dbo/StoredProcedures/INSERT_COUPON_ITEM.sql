CREATE PROCEDURE dbo.INSERT_COUPON_ITEM
	@pCouponStr	VARCHAR(30)
	, @pMasterSeq	INT
	, @pSvrNo		INT
	, @pCharNo	INT
	, @pCharNm	VARCHAR(12)
	, @pMemberID	VARCHAR(20)
	, @pOp		BIT = 0		-- 0 : coupon, 1 : add-product
	, @pItemGiveLimite	TINYINT	 =	NULL	-- next drop parameter 
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
	
	DECLARE @aErr INT,
			@aRowCnt INT
			
	SELECT @aErr = 0, @aRowCnt = 0
	
	--------------------------------------------
	-- 부가 서비스 정보 등록 
	--------------------------------------------	
	IF @pOp = 1 
	BEGIN
	
		DECLARE @INVENTORY_NO	INT
		SET @INVENTORY_NO = CONVERT(INT, @pCouponStr)
		
		IF @@ERROR <> 0 
		BEGIN
			RETURN(1)	-- convert db error 
		END 
		
		
		IF EXISTS(  SELECT	*
					FROM dbo.R2_ADD_ITEM_GIVE_LIMIT
					WHERE SVR_NO = @pSvrNo
						AND PRODUCT_NO = @pMasterSeq )
		BEGIN
			RETURN(1)	-- 지급 제한 걸려 있는 서버이다.		
		END 		
		
		
		IF NOT EXISTS (	SELECT *
						FROM dbo.R2_ADD_INVENTORY T1
							INNER JOIN dbo.R2_ADD_PRODUCT T2 
								ON T1.PRODUCT_NO = T2.PRODUCT_NO
						WHERE INVENTORY_NO = @INVENTORY_NO
								AND MEMBERID = @pMemberID 
								AND INVENTORY_STATUS = 'A' )
		BEGIN
			RETURN(1) -- 아이템이 존재하지 않는다.	
		END 			
		
		
		UPDATE dbo.R2_ADD_INVENTORY
		SET
			SVR_NO = @pSvrNo,
			CHAR_NM = @pCharNm,
			USE_DATE = GETDATE(),	-- 아이템 등록일
			INVENTORY_STATUS = 'B'
		WHERE INVENTORY_NO = @INVENTORY_NO 
			AND MEMBERID = @pMemberID 
			AND INVENTORY_STATUS = 'A'
			
		
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT			
		IF @aErr <> 0 OR @aRowCnt <= 0 
		BEGIN
			RETURN(1) -- db error
		END			
	
		RETURN(0)
	END 
		
	--------------------------------------------
	-- 쿠폰 정보 
	--------------------------------------------				
	IF EXISTS( SELECT *
				FROM dbo.R2_COUPON_ITEM_GIVE_LIMIT
				WHERE SVR_NO = @pSvrNo
					AND MASTER_SEQ = @pMasterSeq )
	BEGIN
		RETURN(1)	-- 지급 제한 서버이다.		
	END 	
	
	
	IF NOT EXISTS(	SELECT *
				FROM dbo.R2_COUPON_LIST T1
					INNER JOIN dbo.R2_COUPON_MASTER T2
						ON  T1.MASTER_SEQ = T2.MASTER_SEQ
				WHERE COUPON_STR = @pCouponStr 
						AND MEMBERID = @pMemberID
						AND ITEM_GIVE_YN ='Y'
						AND ITEM_USE_DATE IS NULL )
	BEGIN
		RETURN(1)	-- 등록할 쿠폰이 존재하지 않는다.
	END			
	
	
	IF EXISTS(	SELECT *
				FROM dbo.R2_COUPON_ITEM_LOG
				WHERE COUPON_STR = @pCouponStr )
	BEGIN
		RETURN(2)	-- 쿠폰을 이미 사용하였다.
	END	
	
	BEGIN TRAN
	
		UPDATE DBO.R2_COUPON_LIST
		SET
			ITEM_USE_DATE = GETDATE()
		WHERE COUPON_STR = @pCouponStr 
				AND MEMBERID = @pMemberID
				AND ITEM_USE_DATE IS NULL
			
	
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT			
		IF @aErr <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1) -- db error( 또는 이미 받았음)
		END					
				
		
		INSERT INTO dbo.R2_COUPON_ITEM_LOG
		VALUES(
			@pMasterSeq,
			@pCouponStr,
			@pMemberID,
			@pSvrNo,
			@pCharNo,
			@pCharNm,	
			GETDATE()	
		)
	
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT			
		IF @aErr <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1) -- db error
		END	
	
	COMMIT TRAN
	RETURN(0)

GO

