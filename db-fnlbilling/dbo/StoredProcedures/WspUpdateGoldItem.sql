CREATE PROCEDURE dbo.WspUpdateGoldItem 
	@GoldItemID BIGINT,
	@ItemName NVARCHAR(40),
	@ItemImage NVARCHAR(100),
	@ItemDesc NVARCHAR(500),
	@OriginalGoldPrice INT,
	@GoldPrice INT,
	@AvailablePeriod INT,
	@Count INT,
	@IID INT,
	@PracticalPeriod INT,
	@ItemCategory CHAR(1),
	@IsPackage CHAR(1),
	@Status CHAR(1),
	@UpdateAdmin NVARCHAR(20),
	@UpdateIP NVARCHAR(19),
	@ReturnValue INT OUTPUT 	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	if(exists(select 1 from dbo.TBLGoldItem where GoldItemID=@GoldItemID))
    begin
    	update TBLGoldItem set
	        ItemName=@ItemName,
	        ItemImage=@ItemImage,
	        ItemDesc=@ItemDesc,
	        OriginalGoldPrice=@OriginalGoldPrice,
	        GoldPrice=@GoldPrice,
	        AvailablePeriod=@AvailablePeriod,
	        Count=@Count,
	        IID=@IID,
	        PracticalPeriod=@PracticalPeriod,
	        ItemCategory=@ItemCategory,
	        IsPackage=@IsPackage,
	        Status=@Status,
	        UpdateDate=getdate(),
	        UpdateAdmin=@UpdateAdmin,
	        UpdateIP=@UpdateIP
	    where GoldItemID=@GoldItemID
		
		IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
	        BEGIN
	            set @ReturnValue=0
	        END
	    ELSE
	        BEGIN
	            set @ReturnValue=1
	        END
    end
    else
    begin
		insert into TBLGoldItem (GoldItemID,ItemName,ItemImage,ItemDesc,OriginalGoldPrice,GoldPrice,ItemCategory,IsPackage,Status,AvailablePeriod,Count,PracticalPeriod,RegistDate,RegistAdmin,RegistIP,UpdateDate,UpdateAdmin,UpdateIP,IID) 
		values	(@GoldItemID,@ItemName,@ItemImage,@ItemDesc,@OriginalGoldPrice,@GoldPrice,@ItemCategory,@IsPackage,@Status,@AvailablePeriod,@Count,@PracticalPeriod,getdate(),@UpdateAdmin,@UpdateIP,getdate(),@UpdateAdmin,@UpdateIP,@IID)
	
		IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
	        BEGIN
	            set @ReturnValue=0
	        END
	    ELSE
	        BEGIN
	            set @ReturnValue=1
	        END  
    end

GO

