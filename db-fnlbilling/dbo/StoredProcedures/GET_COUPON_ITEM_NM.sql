CREATE PROCEDURE [dbo].[GET_COUPON_ITEM_NM]
	@pMasterSeq		INT
	, @pOp				BIT = 0		-- 0 : Coupon, 1 : add item
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF (@pOp = 1)
	BEGIN
		SELECT 
			T1.ITEM_NO,
			T1.ITEM_CNT,
			T1.REG_DATE,		
			T2.IName,
			T1.AvailablePeriod,
			T1.PracticalPeriod,
			T1.mStatus,
			T1.mBindingType			
		FROM dbo.R2_ADD_ITEM_LIST T1
			LEFT OUTER JOIN FNLParm.dbo.DT_ITEM T2
			ON T1.ITEM_NO = T2.IID	
		WHERE T1.PRODUCT_NO = @pMasterSeq			
		
		RETURN(0)
	END 
	
	
	SELECT 
		T1.ITEM_NO,
		T1.ITEM_CNT,
		T1.REG_DATE,		
		T2.IName,
		T1.AvailablePeriod,
		T1.PracticalPeriod,
		T1.mStatus,
		T1.mBindingType					
	FROM dbo.R2_COUPON_ITEM_LIST T1
		LEFT OUTER JOIN FNLParm.dbo.DT_ITEM T2
		ON T1.ITEM_NO = T2.IID	
	WHERE T1.MASTER_SEQ = @pMasterSeq

GO

