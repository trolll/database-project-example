

CREATE  PROCEDURE dbo.WspInsertCategory  	
	@CategoryID BIGINT,
	@CategoryName NVARCHAR(30),
	@CategoryDesc NVARCHAR(50),
	@Status CHAR(1),
	@RegistAdmin NVARCHAR(20),
	@RegistIP NVARCHAR(19),
	@UpdateAdmin NVARCHAR(20),
	@UpdateIP NVARCHAR(19),
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	insert into TBLCategory
	(CategoryID,CategoryName,CategoryDesc,Status,RegistDate,RegistAdmin,RegistIP,UpdateDate,UpdateAdmin,UpdateIP) 
    values		(@CategoryID,@CategoryName,@CategoryDesc,@Status,getdate(),@RegistAdmin,@RegistIP,getdate(),@UpdateAdmin,@UpdateIP)
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
        BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END

GO

