
CREATE  PROCEDURE dbo.WspGetGSExchangeByUserId
	@pRUserId varchar(20)
    ,@pBUserid varchar(20)
	,@pSvrNo	SMALLINT
    ,@pExchangeType char(1)
    ,@pExchangeDate varchar(10)
	,@pPageNum	INT
	,@pPageSize	INT
    ,@pPageCount INT output
    ,@pRecordCount INT output
AS
BEGIN

	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @TotalCount INT
    DECLARE @strWhere NVARCHAR(1000)
    SET @strWhere = 'mSvrNo = ' + STR(@pSvrNo)
    IF @pRUserId is not null and @pRUserId<>''
    BEGIN
    SET @strWhere = @strWhere + ' and RmUserNo = (SELECT mUserNo from FNLAccount.dbo.TBLUser where mUserId=''' + @pRUserId + ''')'
    END

	IF @pBUserid is not null and @pBUserid<>''
    BEGIN
    	SET @strWhere = @strWhere + ' and BmUserNo = (SELECT mUserNo from FNLAccount.dbo.TBLUser where mUserId=''' + @pBUserid + ''')'
    END

    IF @pExchangeType is not null and @pExchangeType<>''
    BEGIN
    	SET @strWhere = @strWhere + ' and ExchangeType = ''' + @pExchangeType + ''''
    END

    IF @pExchangeDate is not null and @pExchangeDate<>''
    BEGIN
    	SET @strWhere = @strWhere + ' and convert(varchar,ExchangeDate,23) = N''' + @pExchangeDate + ''''
    END

	DECLARE @SQLCOUNT NVARCHAR(1000)
    SET @SQLCOUNT = N'SELECT @TotalCount = COUNT(*) FROM dbo.TBLGSExchange WHERE ' + @strWhere
	EXECUTE sp_executesql @SQLCOUNT, N'@TotalCount INT OUTPUT', @TotalCount = @TotalCount OUTPUT
	

	DECLARE @SQL NVARCHAR(1000)

	SET @SQL = ''
	SET @SQL = @SQL + N''
	SET @SQL = @SQL + N' SELECT TOP ' + STR(@pPageSize) + N' '	
	SET @SQL = @SQL + N'	ExchangeID '
	SET @SQL = @SQL + N'	, RmUserNo,RmNo,RmNm,BmUserNo,BmNo,BmNm,ExChangeType,RegDate,ExchangeDate,Status,ExchangeGold,ExchangeSilver,Commission '
	SET @SQL = @SQL + N' FROM dbo.TBLGSExchange '
	SET @SQL = @SQL + N' WHERE ' + @strWhere
	SET @SQL = @SQL + N'	AND ExchangeID NOT IN '
	SET @SQL = @SQL + N'	('
	SET @SQL = @SQL + N'		SELECT TOP ' + STR(@pPageSize * (@pPageNum -1)) + N' '
	SET @SQL = @SQL + N'			ExchangeID '
	SET @SQL = @SQL + N'		FROM  dbo.TBLGSExchange '
	SET @SQL = @SQL + N'		WHERE ' + @strWhere
	SET @SQL = @SQL + N'		ORDER BY ExchangeID DESC'
	SET @SQL = @SQL + N'	)'
	SET @SQL = @SQL + N' ORDER BY ExchangeID DESC'

	EXEC SP_EXECUTESQL @SQL
    SELECT @pRecordCount = @TotalCount
    SELECT @pPageCount = ceiling(1.0*@TotalCount/@pPageSize)
END

GO

