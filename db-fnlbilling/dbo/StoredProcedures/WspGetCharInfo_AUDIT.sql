












/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2011-3-17 14:09:43 ******/

/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2011-3-3 16:58:03 ******/

/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2009-12-15 18:17:28 ******/

/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2009-7-16 7:49:38 ******/
/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2009-6-10 9:19:25 ******/  
  
  
/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2009-4-15 8:59:17 ******/  
  
/****** Object:  Stored Procedure dbo.WspGetCharInfo_AUDIT    Script Date: 2009-2-25 14:50:34 ******/  
  
  
  
  
  
  
  
CREATE        PROCEDURE [dbo].[WspGetCharInfo_AUDIT]   
 @mNm CHAR(12), -- ????  
 @mBackupYear INT,  
 @mBackupMonth INT,  
 @mBackupDay INT  
AS  
 SET NOCOUNT ON  
  
 DECLARE @mDate DATETIME  
 DECLARE @Sql  NVARCHAR(3000)  
 DECLARE @mTable1 NVARCHAR(50)  
 DECLARE @mTable2 NVARCHAR(50)  
 DECLARE @mBackupDate DATETIME  
   
 SET @mDate = GETDATE()  
 SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)  
  
 IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )  
 BEGIN  
  SET @mTable1 = N'TblPc_history'  
  SET @mTable2 = N'TblPcState_history'  
 END  
 ELSE  
 BEGIN  
  SET @mTable1 = N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
  SET @mTable2 = N'TblPcState_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)   
 END  
  
  
 SET @Sql =   
 'SELECT  
  p.mNm, -- ????  
  u.mUserId, -- ???  
  p.mClass, -- ???    
  (SELECT COUNT(*) FROM [FNLAccount].[dbo].[TblUserBlock] AS b  
   WHERE p.mOwner = b.mUserNo) AS mBlock, -- 0?? ? ??, ???? = ???  
  (SELECT COUNT(*) FROM [FNLAccount].[dbo].[TblUserBlockHistory] AS bh  
   WHERE p.mOwner = bh.mUserNo) AS mBlockHistory, -- mBlock? 0??? ??? 0?? ? ??, ???? = ??[?? ?? ??]  
  p.mRegDate, -- ??? ???  
  s.mLevel, -- ??  
  s.mExp, -- ???  
  s.mPosX, s.mPosY, s.mPosZ, -- ??  
  s.mHp, -- HP  
  s.mMp, -- MP  
  s.mChaotic, -- ??  
  -- mGuildNm, -- ???  
  s.mLoginTm, -- ?? ?? ?? ??  
  s.mLogoutTm, -- ?? ?? ???? ??  
  s.mIp, -- ?? ?? ?? IP  
  p.mDelDate, -- ??? ???  
  p.mOwner, -- ??? ?? ??  
  p.mNo -- ??? ??  
 FROM  '  
  + @mTable1 + ' AS p WITH (NOLOCK)  
 INNER JOIN   
  [FNLAccount].[dbo].[TblUser]  AS u  
 ON  
  p.mOwner = u.mUserNo  
 INNER JOIN  '    
  + @mTable2 +  '  AS s WITH (NOLOCK)  
 ON  
  p.mNo = s.mNo  
  
 WHERE    
  p.mNm = @mNm  
  AND p.mDelDate IS NULL'   
   
 EXEC sp_executesql @Sql, N'@mNm  CHAR(12), @mBackupDate DATETIME', @mNm = @mNm, @mBackupDate = @mBackupDate  
  
 SET NOCOUNT OFF

GO

