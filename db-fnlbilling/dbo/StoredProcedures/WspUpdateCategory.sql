

CREATE  PROCEDURE dbo.WspUpdateCategory  	
	@CategoryID BIGINT,
	@CategoryName NVARCHAR(30),
	@CategoryDesc NVARCHAR(50),
	@Status CHAR(1),
	@UpdateAdmin NVARCHAR(20),
	@UpdateIP NVARCHAR(19),
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	update TBLCategory set
        CategoryName=@CategoryName,
        CategoryDesc=@CategoryDesc,
        Status=@Status,
        UpdateDate=getdate(),
        UpdateAdmin=@UpdateAdmin,
        UpdateIP=@UpdateIP
    where CategoryID=@CategoryID
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
        BEGIN 
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END

GO

