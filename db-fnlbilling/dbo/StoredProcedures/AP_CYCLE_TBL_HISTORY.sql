




CREATE  PROCEDURE [dbo].[AP_CYCLE_TBL_HISTORY]
AS
	SET NOCOUNT ON
	
	DECLARE @obj_tbl_name TABLE (
		mSeq		INT NOT NULL IDENTITY(1,1),
		mObjName	VARCHAR(100) NOT NULL
	)
	
	DECLARE @objID	INT,
			@mSeq	INT,
			@mObjName	VARCHAR(100),
			@mOldObjName	VARCHAR(100)
				
	
	-- Audit target table insert
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblCastleGate_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblCastleTowerStone_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGkill_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuild_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildAss_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildAssMem_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildMember_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblPc_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblPcEquip_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblPcInventory_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblPcStore_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblPcQuest_history');
	
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblPcState_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblBanquetHall_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildAgitAuction_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildAgit_history');
	
	-- 2008.01.08
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildGoldItemEffect_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblPcGoldItemEffect_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildStore_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildStorePassword_history');
	
	-- 2009.06.16
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblGuildAccount_history');
	
	-- 2011.03.02
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblConsignment_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblConsignmentAccount_history');
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblConsignmentItem_history');
	
	-- 2012.08.10
	INSERT INTO @obj_tbl_name(mObjName) VALUES('TblPcBead_history');	

	SELECT @objID = COUNT(*)
	FROM @obj_tbl_name
	
	WHILE( @objID <> 0 )
	BEGIN
		
		SELECT 
			@mObjName = mObjName
		FROM @obj_tbl_name
		WHERE mSeq = @objID
		
		IF @@ROWCOUNT <= 0 
			BREAK
	
		SET @mOldObjName  = RTRIM(@mObjName) +  '_' +  convert(char(8),dateadd(dd,-1,getdate()),112)   		
		EXEC sp_rename @mObjName, @mOldObjName		
		
		SET @objID = @objID - 1
	END

GO

