
CREATE PROCEDURE dbo.WspGetRecommendGoldItem  	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 20
	
	SELECT
		T1.GoldItemID
		, T1.ItemSeq
		, T1.CategoryID
		, T2.GoldItemID
		, T2.IID
		, T2.ItemName
		, T2.ItemDesc
		, T2.ItemCategory
		, T2.IsPackage			
		, T2.OriginalGoldPrice
		, T2.GoldPrice
		, T2.AvailablePeriod
		, T2.Count
		, T2.Status
		, T2.PracticalPeriod	
	FROM dbo.TBLRecommendItemList T1
		INNER JOIN dbo.TBLGoldItem T2
			ON T1.GoldItemID = T2.GoldItemID
	WHERE  T1.status in ('1','2')
	ORDER BY T1.OrderNO ASC

GO

