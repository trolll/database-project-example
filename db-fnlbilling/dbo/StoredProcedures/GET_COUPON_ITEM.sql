CREATE PROCEDURE dbo.GET_COUPON_ITEM
	@pMasterSeq		INT,		-- 쿠폰 마스터 정보 OR 상품 번호
	@pOp			BIT = 0		-- 쿠폰 정보 요청 
AS 
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF @pOp = 0 
	BEGIN
		SELECT 
			T2.ITEM_NO,
			T2.ITEM_CNT,
			T2.AvailablePeriod,
			T2.PracticalPeriod,
			T2.mStatus,
			T2.mBindingType
		FROM dbo.R2_COUPON_MASTER AS T1
			INNER JOIN dbo.R2_COUPON_ITEM_LIST AS T2
			ON T1.MASTER_SEQ = T2.MASTER_SEQ
		WHERE  T1.MASTER_SEQ = @pMasterSeq			
				AND T1.USE_LIMITDATE >= GETDATE()			
				AND T1.ITEM_GIVE_YN = 'Y'	-- 아이템 지급
				
		RETURN(0)
	END
	
	-- 2007.09.19
	SELECT 
			ITEM_NO,
			ITEM_CNT,
			AvailablePeriod,
			PracticalPeriod,
			mStatus,
			mBindingType			
	FROM dbo.R2_ADD_ITEM_LIST
	WHERE PRODUCT_NO = @pMasterSeq

GO

