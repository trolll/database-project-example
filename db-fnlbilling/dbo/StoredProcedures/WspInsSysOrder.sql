CREATE PROCEDURE dbo.WspInsSysOrder  
    @pGmCharNm varchar(12),
    @pGmUserNo int,
    @pMsg varchar(200),
    @pOutSysID	BIGINT	OUTPUT	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0, @pOutSysID = 0   
	
	
	INSERT INTO [dbo].[TBLSysOrder] ([mGmCharNm], [mGmUserNo], [mMsg])
	SELECT @pGmCharNm, @pGmUserNo, @pMsg
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT

	SET @pOutSysID = @@IDENTITY

	IF @aErrNo <> 0 
		RETURN(1)

    RETURN(0)

GO

