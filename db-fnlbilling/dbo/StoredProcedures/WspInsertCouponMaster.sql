
CREATE PROCEDURE dbo.WspInsertCouponMaster  
	        @masterSeq INT,
	        @title NVARCHAR(100),
	        @couponCnt INT,
	        @regLimitDate NVARCHAR(10),
	        @useLimitDate NVARCHAR(10),
	        @regDate NVARCHAR(10),
	        @memo NVARCHAR(1000),
	        @couponTypeNo INT,
	        @regPerson NVARCHAR(10)
	AS		
		INSERT INTO R2_COUPON_MASTER
		    (MASTER_SEQ,TITLE,COUPONCNT,REG_LIMITDATE,USE_LIMITDATE,REGDATE,MEMO,
		      STATUS,ITEM_GIVE_LIMIT_YN,COUPON_TYPE_NO,REG_PERSON,GAME_USE_CNT,WEB_ACTIVE_CNT,ITEM_GIVE_YN)
		VALUES ( @masterSeq,@title,@couponCnt,@regLimitDate,@useLimitDate,@regDate,@memo,
		         'A','Y',@couponTypeNo,@regPerson,0,0,'Y')

GO

