
CREATE  PROCEDURE [dbo].[WspDeleteBestItemList]
	@ItemSeq BIGINT,
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	

	UPDATE dbo.TBLBestItemList 
	SET
		status = N'0' 
	WHERE ItemSeq = @ItemSeq
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo <> 0 OR @aRowCnt <> 1
	BEGIN
	    set @ReturnValue=0
	END
	ELSE
	BEGIN
	    set @ReturnValue=1
	END

GO

