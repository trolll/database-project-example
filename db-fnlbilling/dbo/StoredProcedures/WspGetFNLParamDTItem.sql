

CREATE  PROCEDURE dbo.WspGetFNLParamDTItem  	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	select iid,iname,case when itermofvalidity>30 then 30 else itermofvalidity end  as itermofvalidity,imaxstack 
	from FNLParm.dbo.DT_Item 
	where iischarge=1

GO

