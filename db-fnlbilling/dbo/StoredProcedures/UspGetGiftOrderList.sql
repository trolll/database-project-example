CREATE PROCEDURE dbo.UspGetGiftOrderList
	@pSvrNo	SMALLINT
	, @pUserNo	INT
	, @pRowCnt	INT	=	30
	, @pTotalCnt	INT		OUTPUT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		
	
	SET ROWCOUNT @pRowCnt
	
	SET @pTotalCnt = 0
	
	-- total count
	SELECT 
		@pTotalCnt = SUM(mCnt)
	FROM (
		SELECT 
			COUNT(*) mCnt
		FROM dbo.TBLWebOrderList
		WHERE mUserNo = @pUserNo
			AND mSvrNo = @pSvrNo
			AND mStatus = 0
		UNION ALL
		SELECT 
			COUNT(*) mCnt
		FROM dbo.TBLSysOrderList 
		WHERE mUserNo = @pUserNo
			AND mSvrNo IN (@pSvrNo, 0)
			AND mStatus = 0
			AND mLimitedDate > GETDATE()  ) T1	
	
	-- top 10
	SELECT 
		*
	FROM (		
		SELECT 
			T1.mOrderType mGiftType		-- 1 : 老馆 备概, 2 : 急拱 备概
			, T1.mWebOrderID mOrderID
			, T1.mRegDate
			, T1.GoldItemID mItemID
			, T1.Count mCnt
			, T2.PracticalPeriod mPracticalPeriod
			, T2.AvailablePeriod mAvailablePeriod
			, 1 mItemStatus				-- 酒捞袍 惑怕(1: 老馆)
			, 0 mBindingType			-- 蓖加 鸥涝(0:蓖加 酒捞袍 鸥涝捞 酒聪促)
		FROM dbo.TBLWebOrderList T1
			INNER JOIN dbo.TBLGoldItem T2
				ON T1.GoldItemID = T2.GoldItemID	
		WHERE T1.mUserNo = @pUserNo
			AND T1.mSvrNo = @pSvrNo
			AND T1.mStatus = 0
		UNION ALL
		SELECT 
			0 mGiftType	-- 0 System 瘤鞭
			, CONVERT(VARCHAR(20), mSysOrderID) mOrderID
			, mRegDate
			, mItemID
			, mCnt
			, mPracticalPeriod
			, mAvailablePeriod
			, mItemStatus	-- 惑怕 
			, mBindingType
		FROM dbo.TBLSysOrderList 
		WHERE mUserNo = @pUserNo
			AND mSvrNo IN (@pSvrNo, 0)
			AND mStatus = 0	
			AND mLimitedDate > GETDATE() )	T3
	ORDER BY mRegDate DESC

GO

