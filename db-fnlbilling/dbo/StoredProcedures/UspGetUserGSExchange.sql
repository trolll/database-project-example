CREATE PROCEDURE dbo.UspGetUserGSExchange
	@pSvrNo	SMALLINT,
	@pUserNo INT	
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SET ROWCOUNT 5
	
	SELECT 
		ExchangeID,
		BmUserNo,
		BmNo,
		BmNm,
		ExchangeType,
		RegDate,
		Status,
		ExchangeGold,
		ExchangeSilver,
		Commission		
	FROM dbo.TBLGSExchange
	WHERE RmUserNo = @pUserNo
			AND mSvrNo = @pSvrNo
			AND Status IN (N'2', N'4')
	ORDER BY RegDate DESC	
			
	RETURN(0)

GO

