CREATE PROCEDURE dbo.WspGetCouponItemList
		@masterSeq int
	as	
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED					
		select a.COUPON_TYPE_NO, b.REG_DATE, a.USE_LIMITDATE, b.item_no, c.INAME, c.IDESC, b.ITEM_CNT, b.COND_TYPE, b.COND_VALUE
		from R2_COUPON_MASTER a, R2_COUPON_ITEM_LIST b,FNLParm.dbo.dt_item c
			where a.MASTER_SEQ = b.MASTER_SEQ and c.IID = b.ITEM_NO and a.MASTER_SEQ = @masterSeq
			order by b.order_num

GO

