CREATE PROCEDURE dbo.WspGetOrderList  	
	@pUserNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		OrderID
		, mNm
		, mSrvNo
		, T1.GoldItemID
		, T2.ItemName
		, T1.Count
		, UseGold
		, OrderDate
		, OrderIP
		, BillingOrderID
		, BillingReturnCode
	FROM dbo.TBLOrderList T1
		INNER JOIN dbo.TblGoldItem T2
			ON T1.GoldItemID = T2.GoldItemID
	WHERE mUserNo = @pUserNo
		AND  OrderStatus = N'2'
	 ORDER BY OrderID DESC

GO

