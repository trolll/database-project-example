CREATE PROCEDURE dbo.UspRegGiftOrderEnd
	 @pGiftType		TINYINT
	 , @pOrderID	VARCHAR(20)
	 , @pSvrNo		SMALLINT
	 , @pUserNo		INT
	 , @pNo			INT
	 , @pNm			VARCHAR(12)
	 , @pStatus		TINYINT		-- 惑怕蔼(2, 9) 
	 , @pReceiptOrderID BIGINT = NULL
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aUseGold INT

	SELECT @aErrNo = 0, @aRowCnt = 0  , @aUseGold =0 
	
	IF @pStatus NOT IN (2, 9)	
		RETURN(3)	-- 舅荐 绝绰 惑怕蔼捞促.	
		

	-- 林巩矫 备概 啊拜阑 啊廉柯促.(捞 窜拌俊辑 TBLWebOrderList 抛捞喉俊 ORDERID啊 扁废等促)
	SELECT
		@aUseGold = UseGold
	FROM DBO.TBLWebOrderList
	WHERE mWebOrderID = @pOrderID
	
	IF @aUseGold IS NULL OR 
		@aUseGold = 0
	BEGIN
		SET @aUseGold=0
	END
		
	
	BEGIN TRAN
	
		-- 搬力 沥焊 惑怕 盎脚 
		IF @pReceiptOrderID IS NOT NULL AND
			@pReceiptOrderID > 0 
		BEGIN
			-- Status 2 : success
			-- 1 : error 
			UPDATE dbo.TBLOrderList
			SET OrderStatus = 
				CASE
					WHEN @pStatus = 2 THEN  N'2'	-- 林巩 己傍
					ELSE N'3'	-- 牢亥俊 持促啊 角菩 坷幅 馆券
				END
				, UseGold = @aUseGold	-- 昆 备概矫 且牢 捻迄 利侩栏肺 牢窍咯 备概矫 啊拜 函版
			WHERE OrderID = @pReceiptOrderID
			
			SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT

			IF @aErrNo <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(1)	-- SQL Error
			END
				
			IF @aRowCnt <= 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(2)	-- None row data
			END
		END
	
	
		IF @pGiftType IN( 1	, 2 )
		BEGIN
			UPDATE dbo.TBLWebOrderList
			SET
				mReceiptDate = GETDATE()
				, mReceiptOrderID = @pReceiptOrderID
				, mStatus =	@pStatus	
				, mReceiptPcNo = @pNo
				, mRecepitPcNm = @pNm			
			WHERE mWebOrderID = @pOrderID
				AND mUserNo = @pUserNo
				AND mSvrNo = @pSvrNo
				AND mStatus = 1
		
			SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
			
			IF @aErrNo <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(1)	-- SQL Error
			END
				
			IF @aRowCnt <= 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(2)	-- None row data
			END
			
			COMMIT TRAN
			RETURN(0)
		END
		
		UPDATE dbo.TBLSysOrderList
		SET
			mReceiptDate = GETDATE()
			, mStatus =	@pStatus	
			, mReceiptPcNo = @pNo
			, mRecepitPcNm = @pNm				
		WHERE mSysOrderID = CONVERT(BIGINT, @pOrderID)
			AND mUserNo = @pUserNo
			AND mSvrNo IN (@pSvrNo, 0)
			AND mStatus = 1

		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT

		IF @aErrNo <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)	-- SQL Error
		END
			
		IF @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(2)	-- None row data
		END
	
	COMMIT TRAN
	RETURN(0)

GO

