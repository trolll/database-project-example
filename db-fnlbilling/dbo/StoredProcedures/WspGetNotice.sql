CREATE PROCEDURE dbo.WspGetNotice          

AS

         SET NOCOUNT ON

         SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

         

         SELECT 

                   NoticeSeq

                   , Title

                   , Notice

                   , RegistDate

                          ,status       

         FROM dbo.TBLNotice

         WHERE Status in (N'1',N'2')

         ORDER BY NoticeSeq DESC

GO

