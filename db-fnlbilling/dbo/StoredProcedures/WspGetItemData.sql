CREATE PROCEDURE [dbo].[WspGetItemData] 
	@ITEMNAME NVARCHAR(40) 
AS		
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT
		 a.IID as iid
		, a.IName as iname
		, a.IType as itype
		, a.IDesc as idesc
		, ISNULL(b.goldprice, 0) as goldprice
	FROM 
		[FNLParm].[dbo].[dt_item] a WITH (NOLOCK) 
		LEFT outer join [FNLBilling].[dbo].[tblgolditem] b WITH (NOLOCK) ON a.iid = b.iid
	WHERE 			
		a.INAME like '%' + @ITEMNAME + '%'
		and a.IID < 50000
		and a.itype < 21

GO

