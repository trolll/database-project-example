
CREATE PROCEDURE dbo.WspDeleteGoldItem  	
	@GoldItemID BIGINT,
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if(exists(select 1 from dbo.TBLGoldItem where GoldItemID = @GoldItemID and status='0'))
begin
    declare @strstatus char(1)
    if(exists(select 1 from FNLParm.dbo.DT_ITEM d,dbo.TBLGoldItem g where d.iid=g.iid and g.GoldItemID = @GoldItemID and d.iischarge='1'))
        begin
           set @strstatus='1'
        end
    else
        begin
           set @strstatus='2'
        end

    update dbo.TBLGoldItem
	set status=@strstatus,UpdateDate=getdate()
	where GoldItemID = @GoldItemID
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
       BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END
end
else
begin	
	update dbo.TBLGoldItem
	set status='0',UpdateDate=getdate()
	where GoldItemID = @GoldItemID
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
       BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END
end

GO

