












/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2011-3-17 14:09:44 ******/

/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2011-3-3 16:58:04 ******/

/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2009-12-15 18:17:28 ******/

/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2009-7-16 7:49:38 ******/

/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2009-6-10 9:19:25 ******/  
  
  
/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2009-4-15 8:59:17 ******/  
  
/****** Object:  Stored Procedure dbo.WspListSilverRankByAccount_AUDIT    Script Date: 2009-2-25 14:50:35 ******/  
  
  
  
  
  
  
  
  
CREATE PROCEDURE [dbo].[WspListSilverRankByAccount_AUDIT]  
 @mBackupYear INT,  
 @mBackupMonth INT,  
 @mBackupDay INT  
AS  
 SET NOCOUNT ON  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   
  
 DECLARE @mDate DATETIME  
 DECLARE @Sql  NVARCHAR(3000)  
 DECLARE @mTable1 NVARCHAR(50)  
 DECLARE @mTable2 NVARCHAR(50)  
 DECLARE @mBackupDate DATETIME  
   
 SET @mDate = GETDATE()  
 SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)  
  
 IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )  
 BEGIN  
  SET @mTable1 = N'TblPc_history'  
  SET @mTable2 = N'TblPcInventory_history'  
 END  
 ELSE  
 BEGIN  
  SET @mTable1 = N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
  SET @mTable2 = N'TblPcInventory_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)   
 END  
  
 SET  @Sql =    
 'SELECT  
  TOP 20  
  u.mUserId, SUM(CONVERT(BIGINT,mCnt)) AS mSilver   
 FROM  
  [FnlAccount].[dbo].[TblUser] AS u  
 INNER JOIN  
  ' + @mTable1+  ' AS p   
 ON  
  u.mUserNo = p.mOwner  
 INNER JOIN  
  ' + @mTable2 + ' AS i   
 ON  
  p.mNo = i.mPcNo  
 WHERE  
  i.mItemNo = 409  
 GROUP BY  
  u.mUserId  
   
 ORDER BY  
  mSilver DESC'     
  
 EXEC sp_executesql @Sql, N'@mBackupDate DATETIME', @mBackupDate = @mBackupDate  
  
 SET NOCOUNT OFF

GO

