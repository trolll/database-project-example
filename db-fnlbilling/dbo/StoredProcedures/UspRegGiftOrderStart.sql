CREATE PROCEDURE dbo.UspRegGiftOrderStart	
	 @pGiftType		TINYINT
	 , @pOrderID	VARCHAR(20)
	 , @pSvrNo		SMALLINT
	 , @pUserNo		INT
	 , @pNo			INT
	 , @pNm			VARCHAR(12)
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	
	IF @pGiftType IN( 1	, 2 )
	BEGIN
		UPDATE dbo.TBLWebOrderList
		SET
			mReceiptDate = GETDATE()
			, mStatus =	1	-- 荐飞 夸珚 惑怕 函版		
			, mReceiptPcNo = @pNo
			, mRecepitPcNm = @pNm
		WHERE mWebOrderID = @pOrderID
			AND mUserNo = @pUserNo
			AND mSvrNo = @pSvrNo
			AND mStatus = 0
	
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo <> 0 
			RETURN(1)	-- SQL Error
			
		IF @aRowCnt <= 0 
			RETURN(2)	-- None row data
		
		RETURN(0)
	END
	
	UPDATE dbo.TBLSysOrderList
	SET
		mReceiptDate = GETDATE()
		, mStatus =	1	-- 荐飞 夸珚 惑怕 函版		
		, mReceiptPcNo = @pNo
		, mRecepitPcNm = @pNm		
	WHERE mSysOrderID = CONVERT(BIGINT, @pOrderID)
		AND mUserNo = @pUserNo
		AND mSvrNo IN (@pSvrNo, 0)
		AND mStatus = 0

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT

	IF @aErrNo <> 0 
		RETURN(1)	-- SQL Error
		
	IF @aRowCnt <= 0 
		RETURN(2)	-- None row data
	
	RETURN(0)

GO

