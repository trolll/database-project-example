

CREATE  PROCEDURE dbo.WspGetCategory  	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		CategoryID,
		CategoryName,CategoryDesc,Status
	FROM dbo.TBLCategory where status in (N'1',N'2') order by categoryID ASC

GO

