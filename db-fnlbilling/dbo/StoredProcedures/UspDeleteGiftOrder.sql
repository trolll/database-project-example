/*************************************************************************************************************
** 2011-03-03 菩摹
*************************************************************************************************************/
/******************************************************************************
**		Name: UspDeleteGiftOrder
**		Desc: 急拱 沥焊 昏力 钦聪促. 
**
**		Status : 0 : 荐飞措扁, 1 : 荐飞 夸没, 2 : 荐飞 肯丰, 3 : 券阂/秒家 , 4 : 瘤鞭 秒家(GM), 5: 昏力 9 : 坷幅
**		Auth: JUDY
**		Date: 2010.08.12
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**               
*******************************************************************************/
CREATE PROCEDURE dbo.UspDeleteGiftOrder  	
	 @pGiftType		TINYINT
	 , @pOrderID	VARCHAR(20)
	 , @pSvrNo		SMALLINT
	 , @pUserNo		INT
	 , @pNo			INT
	 , @pNm			VARCHAR(12)
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	
	IF @pGiftType IN( 1	, 2 )
	BEGIN
		UPDATE dbo.TBLWebOrderList
		SET
			mReceiptDate = GETDATE()
			, mStatus =	5	-- 昏力 惑怕 函版	
			, mReceiptPcNo = @pNo
			, mRecepitPcNm = @pNm
		WHERE mWebOrderID = @pOrderID
			AND mUserNo = @pUserNo
			AND mSvrNo = @pSvrNo
			AND mStatus = 0
	
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo <> 0 
			RETURN(1)	-- SQL Error
			
		IF @aRowCnt <= 0 
			RETURN(2)	-- None row data
		
		RETURN(0)
	END
	
	UPDATE dbo.TBLSysOrderList
	SET
		mReceiptDate = GETDATE()
		, mStatus =	5	-- 昏力 惑怕 函版
		, mReceiptPcNo = @pNo
		, mRecepitPcNm = @pNm		
	WHERE mSysOrderID = CONVERT(BIGINT, @pOrderID)
		AND mUserNo = @pUserNo
		AND mSvrNo IN (@pSvrNo, 0)
		AND mStatus = 0

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT

	IF @aErrNo <> 0 
		RETURN(1)	-- SQL Error
		
	IF @aRowCnt <= 0 
		RETURN(2)	-- None row data
	
	RETURN(0)

GO

