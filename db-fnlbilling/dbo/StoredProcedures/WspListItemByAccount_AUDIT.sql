












/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2011-3-17 14:09:43 ******/

/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2011-3-3 16:58:03 ******/

/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2009-12-15 18:17:28 ******/

/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2009-7-16 7:49:38 ******/

/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2009-6-10 9:19:25 ******/  
  
  
/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2009-4-15 8:59:17 ******/  
  
/****** Object:  Stored Procedure dbo.WspListItemByAccount_AUDIT    Script Date: 2009-2-25 14:50:35 ******/  
  
  
CREATE PROCEDURE [dbo].[WspListItemByAccount_AUDIT]  
 @mUserId VARCHAR(20), -- ???  
 @mBackupYear INT,  
 @mBackupMonth INT,  
 @mBackupDay INT  
AS  
 SET NOCOUNT ON  
  
 DECLARE @mDate DATETIME  
 DECLARE @Sql  NVARCHAR(3000)  
 DECLARE @mTable1 NVARCHAR(50)  
 DECLARE @mTable2 NVARCHAR(50)  
 DECLARE @mTable3 NVARCHAR(50)  
 DECLARE @mBackupDate DATETIME  
   
 SET @mDate = GETDATE()  
 SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)  
  
 IF @mBackupDate < '2007-04-13'  
 BEGIN  
  IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )  
  BEGIN  
   SET @mTable1 = N'TblPc_history'  
   SET @mTable2 = N'TblPcItem_History'  
  END  
  ELSE  
  BEGIN  
   SET @mTable1 = N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
   SET @mTable2 = N'TblPcItem_History_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
  END  
    
  SET @Sql=  
  '(SELECT  
   p.mNm -- ???  
   , null as mSlot -- ??  
   , ii.IName -- ??? ??  
   , i.mStatus -- ??  
   , DATEDIFF(DAY, GETDATE(), i.mEndDate) AS mEndDate -- ????  
   , i.mSerialNo -- ???  
   , i.mCnt -- ??  
   , i.mItemNo -- TID  
   , i.mIsSeizure -- ????  
  FROM '  
    + @mTable1 +' AS p WITH (NOLOCK)  
  INNER JOIN  
   [FNLAccount].[dbo].[TblUser] AS u  
  ON  
   p.mOwner = u.mUserNo   
  INNER JOIN '  
   + @mTable2 + ' AS i WITH (NOLOCK)  
  ON  
   i.mOwner = p.mNo   
  INNER JOIN  
   [FnlParm].[dbo].[DT_Item] AS ii    
  ON  
   i.mItemNo = ii.IID  
  WHERE  
   u.mUserId = @mUserId  
   AND p.mDelDate IS NULL  
  UNION ALL  
  SELECT  
   ''??'' AS mNm -- ???  
   , null as mSlot -- ??  
   , ii.IName -- ??? ??  
   , i.mStatus -- ??  
   , DATEDIFF(DAY, GETDATE(), i.mEndDate) AS mEndDate -- ????  
   , i.mSerialNo -- ???  
   , i.mCnt -- ??  
   , i.mItemNo -- TID  
   , i.mIsSeizure -- ????  
  FROM  
   ' + @mTable2+ ' AS i WITH (NOLOCK)  
  INNER JOIN  
   [FNLAccount].[dbo].[TblUser] AS u  
  ON  
   i.mUserNo = u.mUserNo  
  INNER JOIN  
   [FnlParm].[dbo].[DT_Item] AS ii  
  ON  
   i.mItemNo = ii.IID  
  WHERE  
     
   u.mUserId = @mUserId  
  )  
  '  
 END  
 ELSE  
 BEGIN  
  IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )  
  BEGIN  
   SET @mTable1 = N'TblPc_history'  
   SET @mTable2 = N'TblPcInventory_history'  
   SET @mTable3 = N'TblPcStore_history'  
  END  
  ELSE  
  BEGIN  
   SET @mTable1 = N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
   SET @mTable2 = N'TblPcInventory_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
   SET @mTable3 = N'TblPcStore_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
  END  
    
  SET @Sql=  
  '(SELECT  
   p.mNm -- ???  
   , null as mSlot -- ??  
   , ii.IName -- ??? ??  
   , i.mStatus -- ??  
   , DATEDIFF(DAY, GETDATE(), i.mEndDate) AS mEndDate -- ????  
   , i.mSerialNo -- ???  
   , i.mCnt -- ??  
   , i.mItemNo -- TID  
   , i.mIsSeizure -- ????  
   , i.mOwner --ADDED BY MENGDAN IN 20080604  
  FROM '  
    + @mTable1 +' AS p   
  INNER JOIN  
   [FNLAccount].[dbo].[TblUser] AS u  
  ON  
   p.mOwner = u.mUserNo   
  INNER JOIN '  
   + @mTable2 + ' AS i   
  ON  
   i.mPcNo = p.mNo   
  INNER JOIN  
   [FnlParm].[dbo].[DT_Item] AS ii    
  ON  
   i.mItemNo = ii.IID  
  WHERE  
   u.mUserId = @mUserId  
   AND p.mDelDate IS NULL  
  UNION ALL  
  SELECT  
   ''??'' AS mNm -- ???  
   , null as mSlot -- ??  
   , ii.IName -- ??? ??  
   , i.mStatus -- ??  
   , DATEDIFF(DAY, GETDATE(), i.mEndDate) AS mEndDate -- ????  
   , i.mSerialNo -- ???  
   , i.mCnt -- ??  
   , i.mItemNo -- TID  
   , i.mIsSeizure -- ????  
   , i.mOwner --ADDED BY MENGDAN IN 20080604  
  FROM  
   ' + @mTable3+ ' AS i   
  INNER JOIN  
   [FNLAccount].[dbo].[TblUser] AS u  
  ON  
   i.mUserNo = u.mUserNo  
  INNER JOIN  
   [FnlParm].[dbo].[DT_Item] AS ii  
  ON  
   i.mItemNo = ii.IID  
  WHERE     
   u.mUserId = @mUserId  
  )  
  '  
   
 END   
   
 EXEC sp_executesql @Sql, N'@mUserId VARCHAR(20), @mBackupDate DATETIME', @mUserId = @mUserId, @mBackupDate = @mBackupDate

GO

