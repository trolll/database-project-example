
CREATE PROCEDURE [dbo].[WspGetItemList] 
		@itemType1  INT, 
		@itemType2  INT, 
		@itemType3  INT, 
		@itemType4  INT, 
		@itemType5  INT, 
		@itemType6  INT 	
	AS		
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		SELECT 
			a.IID iid, 
			a.IName as iname,
			a.IType as itype,
			a.IDesc as idesc,
			ISNULL(b.goldprice, 0) as goldprice
		FROM 	[FNLParm].[dbo].[dt_item] a WITH (NOLOCK) 
			LEFT outer join [FNLBilling].[dbo].[tblgolditem] b WITH (NOLOCK)
		ON a.iid = b.iid
		WHERE 
			IType in (@itemType1,@itemType2,@itemType3,@itemType4,@itemType5,@itemType6);

GO

