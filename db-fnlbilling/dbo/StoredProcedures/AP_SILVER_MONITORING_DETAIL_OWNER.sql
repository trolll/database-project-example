








/******************************************************************************  
**  Name: AP_SILVER_MONITORING_DETAIL_OWNER  
**  Desc: 10盒付促 10盒傈苞 拌沥 家蜡狼 角滚樊 厚背 (玫父 角滚 捞惑老 矫 扁废)
**  
**  Auth: 捞柳急  
**  Date: 2012-04-06
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**        
*******************************************************************************/  
CREATE PROC [dbo].[AP_SILVER_MONITORING_DETAIL_OWNER]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'u' AND name = 'TblDiffByOwner')
	BEGIN
		DROP TABLE dbo.TblDiffByOwner
	END

	SELECT mOwner, SUM(CONVERT(BIGINT,ISNULL(mCnt,0))) AS mCnt INTO dbo.TblDiffByOwner
	FROM (
		SELECT T1.mOwner, SUM(CONVERT(BIGINT,ISNULL(T2.mCnt,0))) AS mCnt
		FROM FNLGame1133.dbo.TblPc AS T1
			INNER JOIN FNLGame1133.dbo.TblPcInventory AS T2
		ON T1.mNo = T2.mPcNo
		WHERE T1.mNo > 1
		AND T2.mItemNo = 409
		GROUP BY T1.mOwner
		UNION ALL
		SELECT mUserNo AS mOwner , mCnt
		FROM FNLGame1133.dbo.TblPcStore
		WHERE mItemNo = 409
	) AS T
	GROUP BY mOwner

	INSERT INTO dbo.TBL_SILVER_LOG_DETAIL_OWNER
	SELECT
		GETDATE() mRegDate
	  , T1.mOwner  
	  , T1.mCnt  
	  , ( ISNULL(T1.mCnt,0) - ISNULL(T2.mCnt,0) ) mDiffCnt
	FROM dbo.TblDiffByOwner T1
		LEFT OUTER JOIN dbo.TBL_SILVER_LOG_CURR_DETAIL_OWNER T2  
	ON T1.mOwner = T2.mOwner
	WHERE ( ISNULL(T1.mCnt,0) - ISNULL(T2.mCnt,0) ) >= 10000000 --瞒捞樊捞 玫父 角滚 捞惑.

	IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'u' AND name = 'TBL_SILVER_LOG_CURR_DETAIL_OWNER')
	BEGIN
		DROP TABLE dbo.TBL_SILVER_LOG_CURR_DETAIL_OWNER
	END

	SELECT
		mOwner
		, mCnt INTO dbo.TBL_SILVER_LOG_CURR_DETAIL_OWNER
	FROM dbo.TblDiffByOwner 
END

GO

