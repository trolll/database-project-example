

CREATE PROCEDURE [dbo].[WspInsSysOrderList]  	
    @pAvailablePeriod int,
    @pCnt int,
    @pItemID int,
    @pPracticalPeriod int,
    @pSvrNo smallint,
    @pSysID bigint,
    @pUserID VARCHAR(20),
    @pBindingType		TINYINT,	-- 官牢爹 鸥涝
    @pLimitedDay		TINYINT,	-- 瘤鞭 力茄老
    @pItemStatus		TINYINT		-- 酒捞袍 惑怕 
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT, 
			@aUserNo	INT,
			@aLimitedDate	SMALLDATETIME

	SELECT @aErrNo = 0, @aRowCnt = 0, @aUserNo = 0

	SELECT
		@aUserNo = mUserNo
	FROM FNLAccount.dbo.TblUser  
	WHERE mUserID = @pUserID

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
		RETURN(1)
		
	IF @pLimitedDay = 0 
		SET @aLimitedDate = '2079-01-01'
	ELSE
		SET @aLimitedDate = DATEADD(DD, @pLimitedDay, GETDATE()) 
			
	INSERT INTO [dbo].[TBLSysOrderList] ([mAvailablePeriod], [mCnt], [mItemID], [mPracticalPeriod], 
		[mSvrNo], [mSysID], [mUserNo], mBindingType, mLimitedDate, mItemStatus )
	VALUES( 
		@pAvailablePeriod
		, @pCnt
		, @pItemID
		, @pPracticalPeriod
		, @pSvrNo
		, @pSysID
		, @aUserNo
		, @pBindingType
		, @aLimitedDate 
		, @pItemStatus )
	
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
	IF @aErrNo <> 0 
		RETURN(1)

    RETURN(0)

GO

