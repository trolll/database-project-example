CREATE PROCEDURE dbo.UspRegWebToGameOrder  	
	@pWebOrderID	VARCHAR(20)
	,@pBillingOrderID	VARCHAR(20)
	,@pUserNo	INT
	,@pSvrNo	SMALLINT
	,@pNo	INT
	,@pNm	VARCHAR(12)
	,@pGoldItemID	BIGINT
	,@pCount	INT
	,@pUseGold	INT
	
	,@pGiftGiveUserNo	INT	= NULL
	,@pGiftGiveSvrNo	SMALLINT	= NULL
	,@pGiftGiveNo		INT	= NULL
	,@pGiftGiveNm		VARCHAR(12)	= NULL
	,@pGiftMsg			VARCHAR(200)	= NULL
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aGiftType	SMALLINT,
			@aGoldPrice	INT
						

	SELECT @aErrNo = 0, @aRowCnt = 0, @aGiftType = 1	-- web order type
	SELECT @aGoldPrice = 0
	
	IF @pGiftGiveUserNo IS NOT NULL
		 SET @aGiftType = 2	-- gift type
		 		 
	SELECT 
		TOP 1 
			@aGoldPrice = T1.GoldPrice	
	FROM dbo.TBLGoldItem T1
		INNER JOIN dbo.TBLGoldItemSupportSvr T2
			ON T1.GoldItemID = T2.GoldItemID
	WHERE T2.GoldItemID = @pGoldItemID
			AND T2.mSvrNo = @pSvrNo
			AND T1.Status =  N'1'			
	
	IF @@ROWCOUNT <> 1
		RETURN(2)	-- 지원되지 않은 골드 아이템 정보
		 	
	BEGIN TRAN
	
		INSERT INTO dbo.TBLWebOrderList(mWebOrderID,mBillingOrderID,mUserNo,mSvrNo,mNo,mNm,
			GoldItemID,Count,UseGold,mOrderType,mStatus)
		VALUES(
			@pWebOrderID
			, @pBillingOrderID
			, @pUserNo
			, @pSvrNo
			, @pNo
			, @pNm
			, @pGoldItemID
			, @pCount
			, @pUseGold
			, @aGiftType
			, 0
		)
	
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)	-- SQL Error	
		END
	
		IF @pGiftGiveUserNo IS NOT NULL
		BEGIN
		
			INSERT INTO dbo.TBLWebOrder(mWebOrderID,mUserNo,mSvrNo,mNo,mNm,mGiftMsg)
			VALUES(
				@pWebOrderID
				, @pGiftGiveUserNo
				, @pGiftGiveSvrNo
				, @pGiftGiveNo
				, @pGiftGiveNm
				, @pGiftMsg		
			)	
		
		
			SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
			
			IF @aErrNo <> 0 OR @aRowCnt <= 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(1)	-- SQL Error	
			END		
			
		END 
	
	COMMIT TRAN
    RETURN(0)

GO

