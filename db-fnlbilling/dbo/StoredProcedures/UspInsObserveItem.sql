










CREATE PROCEDURE [dbo].[UspInsObserveItem]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @TBL_TOT TABLE(	
		mItemNO		INT		NOT NULL,
		mPos		TINYINT	NOT NULL,
		mStatus		TINYINT	NOT NULL,
		mCnt		BIGINT	NOT NULL		
	)
	
	DECLARE @TBL_EXP_INVEN TABLE(
		mItemNO		INT		NOT NULL,
		mStatus		TINYINT	NOT NULL,
		mCnt		BIGINT	NOT NULL		
	)
	
	DECLARE @TBL_EXP_STORE TABLE(
		mItemNO		INT		NOT NULL,
		mStatus		TINYINT	NOT NULL,
		mCnt		BIGINT	NOT NULL		
	)
	
	DECLARE @TBL_EXP_INVEN_EQUIP TABLE(
		mItemNO		INT		NOT NULL,
		mStatus		TINYINT	NOT NULL,
		mCnt		BIGINT	NOT NULL		
	)	
	
	-------------------------------------------------------------------------
	-- 旮办〈 雿办澊韮€ 鞝滉卑	
	-------------------------------------------------------------------------	
	TRUNCATE TABLE dbo.TblObserveItem
	TRUNCATE TABLE dbo.TblNonActiveUser
	TRUNCATE TABLE dbo.TblNonActiveChar	
	TRUNCATE TABLE dbo.TblObserveItemDetail

	-------------------------------------------------------------------------
	-- 牍勴櫆靹表檾 瓿勳爼/旌愲Ν韯?於旍稖
	-------------------------------------------------------------------------
	BEGIN	
		INSERT INTO dbo.TblNonActiveUser
		SELECT *
		FROM FNLStatistics.dbo.TblNonActiveUser

		-- 靷牅霅?旌愲Ν韯?於旉皜 
		INSERT INTO dbo.TblNonActiveChar(mNo, mNm)
		SELECT 
			mNo,
			mNm
		FROM dbo.TblPc_history
		WHERE mDelDate IS NOT NULL
				OR mNo IN(0,1)

		-- 牍勴櫆靹彪悳 瓿勳爼 	
		INSERT INTO dbo.TblNonActiveChar(mNo, mNm)
		SELECT 
			mNo,
			mNm
		FROM dbo.TblNonActiveUser T1
			INNER JOIN dbo.TblPc_history T2
				ON T1.mUserNo = T2.mOwner
		WHERE mNo NOT IN (
			SELECT  
				mNo
			FROM dbo.TblNonActiveChar
		)		
	END
		 
	-------------------------------------------------------------------------
	-- 齑?鞎勳澊韰?鞝曤炒 於旍稖
	-------------------------------------------------------------------------	
	BEGIN
		INSERT INTO @TBL_TOT
		SELECT
			mItemNo
			, mPos
			, mStatus
			, mCnt
		FROM (
				SELECT 
					mItemNo
					,1 mPos
					,mStatus
					,SUM(CONVERT(BIGINT,mCnt)) AS mCnt
				FROM dbo.TblPcInventory_history	-- equip + inventory
				GROUP BY mItemNo, mStatus
				UNION ALL
				SELECT 
					mItemNo
					,2 mPos
					,mStatus
					,SUM(CONVERT(BIGINT,mCnt)) AS mCnt
				FROM dbo.TblPcStore_history
				GROUP BY mItemNo, mStatus
				UNION ALL
				SELECT 
					mItemNo
					,3 mPos
					, mStatus
					,SUM(CONVERT(BIGINT, mCnt)) mCnt	
				FROM dbo.TblGuildStore_history
				GROUP BY mItemNo, mStatus								
				UNION ALL
				SELECT 
					409 mItemNo
					, 4 mPos
					, 1 mStatus		-- 靸來儨臧?氤€瓴?
					, ISNULL(SUM(CONVERT(BIGINT, mAsset)),0) mCnt	
				FROM dbo.TblCastleTowerStone_history
				UNION ALL
				SELECT 
					409 mItemNo
					, 5 mPos
					, 1 mStatus		-- 靸來儨臧?氤€瓴?
					, ISNULL(SUM(CONVERT(BIGINT, mGuildMoney)),0) mCnt	
				FROM dbo.TblGuildAccount_history
			) T1
		OPTION(MAXDOP  1)
	END 

 	-------------------------------------------------------------------------
	-- 牍勴櫆靹?旌愲Ν韯?鞚鸽菠韱犽Μ 鞎勳澊韰?鞝曤炒 於旍稖 
	-------------------------------------------------------------------------	
	BEGIN
		INSERT INTO @TBL_EXP_INVEN
		SELECT 
			mItemNo
			, mStatus
			, SUM(CONVERT(BIGINT,mCnt)) AS mCnt
		FROM dbo.TblNonActiveChar T1
				INNER JOIN dbo.TblPcInventory_history T2
				ON T1.mNo = T2.mPcNo
		GROUP BY mItemNo, mStatus		
		OPTION(MAXDOP  1)			
	END 

 	-------------------------------------------------------------------------
	-- 牍勴櫆靹?瓿勳爼鞚?彀疥碃 鞎勳澊韰?鞝曤炒 於旍稖 
	-------------------------------------------------------------------------	
	BEGIN
		INSERT INTO @TBL_EXP_STORE
		SELECT 
			mItemNo
			, mStatus	
			, SUM(CONVERT(BIGINT,mCnt)) AS mCnt
		FROM dbo.TblNonActiveUser T1
				INNER JOIN dbo.TblPcStore_history T2
				ON T1.mUserNo = T2.mUserNo
		GROUP BY mItemNo, mStatus	
		OPTION(MAXDOP  1)			
	END 	

 	-------------------------------------------------------------------------
	-- 鞛レ癌 鞎勳澊韰?鞝曤炒
	-------------------------------------------------------------------------	
	BEGIN
		INSERT INTO @TBL_EXP_INVEN_EQUIP
		SELECT 
			T2.mItemNo
			, T2.mStatus
			, SUM(CONVERT(BIGINT,mCnt)) AS mCnt
		FROM dbo.TblPcEquip_history T1
			INNER JOIN dbo.TblPcInventory_history T2
				ON T1.mSerialNo = T2.mSerialNo
		WHERE T1.mOwner NOT IN (
			SELECT mNo
			FROM dbo.TblNonActiveChar	)
		GROUP BY mItemNo, mStatus
		OPTION(MAXDOP  1)
	END 	
		
 	-------------------------------------------------------------------------
	-- 鞎勳澊韰?韱店硠 於旍稖 
	-------------------------------------------------------------------------		
	BEGIN
		-- Each Status, Pos
		-- 靾橂焿 = Total - 牍勴櫆靹?旌愲Ν韯?鞎勳澊韰?鞝曤炒 - 牍勴櫆靹?瓿勳爼彀疥碃 - 鞛レ癌 鞎勳澊韰?鞝曤炒
		INSERT INTO dbo.TblObserveItemDetail
		SELECT 
			T1.mItemNo
			, T1.mStatus
			, T1.mPos
			, ( T1.mCnt - ISNULL(T2.mCnt, 0) - ISNULL(T3.mCnt, 0)  - ISNULL(T4.mCnt, 0) ) AS mCnt			
		FROM @TBL_TOT T1
			LEFT OUTER JOIN @TBL_EXP_INVEN T2
				ON T1.mItemNo = T2.mItemNo
					AND T1.mStatus = T2.mStatus
					AND T1.mPos = 1
			LEFT OUTER JOIN @TBL_EXP_STORE T3
				ON T1.mItemNo = T3.mItemNo
					AND T1.mStatus = T3.mStatus
					AND T1.mPos = 2
			LEFT OUTER JOIN @TBL_EXP_INVEN_EQUIP T4	
				ON T1.mItemNo = T4.mItemNo
					AND T1.mStatus = T4.mStatus
					AND T1.mPos = 1					
		OPTION(MAXDOP  1)
		
		-- 鞛レ癌 靾橂焿 鞛呺牓
		INSERT INTO dbo.TblObserveItemDetail
		SELECT
			mItemNo
			, mStatus
			, 0 -- mEquipPos ( 0 : Equipment inventory )
			, mCnt
		FROM @TBL_EXP_INVEN_EQUIP					
	END 

	
	BEGIN
		INSERT INTO dbo.TblObserveItem
		SELECT
			mItemNo, SUM(mCnt)
		FROM dbo.TblObserveItemDetail
		GROUP BY mItemNo
		OPTION(MAXDOP  1)			
	END

GO

