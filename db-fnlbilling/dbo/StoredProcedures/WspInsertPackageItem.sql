CREATE PROCEDURE dbo.WspInsertPackageItem  	
	@PackageItemID BIGINT,
	@GoldItemID BIGINT,
	@Count INT,
	@Status CHAR(1),
	@RegistAdmin NVARCHAR(20),
	@RegistIP NVARCHAR(19),
	@UpdateAdmin NVARCHAR(20),
	@UpdateIP NVARCHAR(19),
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	if(exists(select 1 from dbo.TBLPackageItem where PackageItemID=@PackageItemID and GoldItemID = @GoldItemID))
    begin
        update TBLPackageItem set
            Count=@Count,
            Status=@Status,
            UpdateDate=getdate(),
            UpdateAdmin=@UpdateAdmin,
            UpdateIP=@UpdateIP
        where PackageItemID=@PackageItemID and GoldItemID = @GoldItemID
        
        IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
            BEGIN
                set @ReturnValue=0
            END
        ELSE
            BEGIN
                set @ReturnValue=1
            END
    end
    else
    begin
        insert into TBLPackageItem 
         (PackageItemID,GoldItemID,Count,Status,RegistDate,RegistAdmin,RegistIP,UpdateDate,UpdateAdmin,UpdateIP) values 
         (@PackageItemID,@GoldItemID,@Count,@Status,getdate(),@RegistAdmin,@RegistIP,getdate(),@UpdateAdmin,@UpdateIP)
        
        IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
            BEGIN
                set @ReturnValue=0
            END
        ELSE
            BEGIN
                set @ReturnValue=1
            END
    end

GO

