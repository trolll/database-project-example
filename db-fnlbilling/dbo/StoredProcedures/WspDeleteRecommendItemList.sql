

CREATE  PROCEDURE dbo.WspDeleteRecommendItemList  	
	@ItemSeq BIGINT,
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	update TBLRecommendItemList set
        status='0' 
    where ItemSeq=@ItemSeq
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
        BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END

GO

