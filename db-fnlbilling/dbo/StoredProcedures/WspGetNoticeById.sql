

CREATE  PROCEDURE dbo.WspGetNoticeById  	
	@NoticeSeq BIGINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	select NoticeSeq,Title,Notice,Status,RegistDate 
	from TBLNotice t 
	where t.NoticeSeq=@NoticeSeq

GO

