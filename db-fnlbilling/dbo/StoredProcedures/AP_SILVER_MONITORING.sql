








CREATE PROCEDURE [dbo].[AP_SILVER_MONITORING]
AS

	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	


	DECLARE @Diff TABLE 
	(
		mPos		TINYINT
		, mCnt	BIGINT
	)

	INSERT INTO @Diff
	SELECT 
		mPos
		, mCnt
	FROM (
		SELECT 
			0 mPos
			, SUM(CONVERT(BIGINT, mCnt)) mCnt
		FROM FNLGame1133.dbo.TblPcInventory 
		WHERE mPcNo > 1
			AND mItemNo = 409
		UNION ALL
		SELECT 
			1 mPos
			, SUM(CONVERT(BIGINT, mCnt)) mCnt
		FROM FNLGame1133.dbo.TblPcStore 
		WHERE mItemNo = 409
		UNION ALL
		SELECT 
			2 mPos
			, SUM(CONVERT(BIGINT, mCnt)) mCnt
		FROM FNLGame1133.dbo.TblGuildStore
		WHERE mItemNo = 409
		UNION ALL
		SELECT
			3 mPos
			, SUM(CONVERT(BIGINT, mGuildMoney)) mCnt	
		FROM FNLGame1133.dbo.TblGuildAccount
		UNION ALL
		SELECT
			4 mPos
			, SUM(CONVERT(BIGINT, mAsset)) mCnt	
		FROM FNLGame1133.dbo.TblCastleTowerStone
		) T1


	INSERT INTO dbo.TBL_SILVER_LOG
	SELECT 
		GETDATE() mRegDate
		, T1.mPos
		, T1.mCnt
		, ( T1.mCnt - T2.mCnt ) mDiffCnt
	FROM @Diff T1
		LEFT OUTER JOIN dbo.TBL_SILVER_LOG_CURR T2
			ON T1.mPos = T2.mPos


	TRUNCATE TABLE DBO.TBL_SILVER_LOG_CURR

	INSERT INTO dbo.TBL_SILVER_LOG_CURR
	SELECT 
		mPos
		, mCnt
	FROM @Diff

GO

