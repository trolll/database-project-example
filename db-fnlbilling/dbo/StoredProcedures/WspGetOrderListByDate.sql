CREATE PROCEDURE dbo.WspGetOrderListByDate
	@startdt varchar(8)
	,@enddt varchar(8)
AS
BEGIN
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
   
    SELECT o.OrderDate,o.BillingOrderID,o.OrderID,u.mUserID,o.mNm,o.mSrvNo,
           o.GoldItemID as iid,o.Count,o.UseGold,o.RemainGold
    FROM dbo.TBLOrderList o
    LEFT OUTER JOIN FNLAccount.dbo.TBLUser u
    on o.mUserNo=u.mUserNo
    WHERE OrderStatus = N'2'
    AND OrderDate>convert(datetime,@startdt)
    AND OrderDate<dateadd(d,1,convert(datetime,@enddt))
    ORDER BY orderid
END

GO

