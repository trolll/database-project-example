
CREATE  PROCEDURE dbo.WspGetMainNotice  	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
	 top 1
		NoticeSeq
		, Title
		, Notice
		, RegistDate
                          ,status	
	FROM dbo.TBLNotice
	WHERE Status = N'3'			
	ORDER BY NoticeSeq DESC

GO

