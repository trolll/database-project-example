CREATE PROCEDURE dbo.GET_COUPON_ITEM_INVENTORY
	@pMemberID			VARCHAR(20)
	, @pOp				BIT = 0		-- 쿠폰 정보 요청
	, @pGiveLimitSvrNo	SMALLINT
AS 
	SET NOCOUNT ON  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
		
	DECLARE @v_pMemberID VARCHAR(20)
	SET @v_pMemberID = @pMemberID		
	
	-------------------------------------------------------
	-- 부가서비스 
	-------------------------------------------------------	
	IF @pOp = 1 
	BEGIN		
			
		SELECT 
			T1.PRODUCT_NO AS MASTER_SEQ,
			T2.PRODUCT_NAME AS TITLE,
			CONVERT(VARCHAR(30), INVENTORY_NO) AS COUPON_STR	
		FROM dbo.R2_ADD_INVENTORY T1
			INNER JOIN (
				SELECT PRODUCT_NO, PRODUCT_NAME
				FROM dbo.R2_ADD_PRODUCT
				WHERE PRODUCT_NO NOT IN (	
					SELECT PRODUCT_NO
					FROM dbo.R2_ADD_ITEM_GIVE_LIMIT
					WHERE SVR_NO = @pGiveLimitSvrNo	
				) ) T2
				ON T1.PRODUCT_NO = T2.PRODUCT_NO					
		WHERE T1.MEMBERID = @v_pMemberID
				AND T1.INVENTORY_STATUS = 'A'			
	
		RETURN(0)

	END 
	
	
	-------------------------------------------------------
	-- 쿠폰
	-------------------------------------------------------		
	SELECT C.MASTER_SEQ, C.TITLE, A.COUPON_STR
	FROM R2_COUPON_LIST A
		INNER JOIN (
			SELECT 
				MASTER_SEQ
				, TITLE
				, USE_LIMITDATE
				, ITEM_GIVE_YN
			FROM dbo.R2_COUPON_MASTER
			WHERE MASTER_SEQ NOT IN (	
				SELECT MASTER_SEQ
				FROM dbo.R2_COUPON_ITEM_GIVE_LIMIT	
				WHERE SVR_NO = @pGiveLimitSvrNo)
					AND USE_LIMITDATE >= GETDATE()        -- 이용 제한 체크
					AND ITEM_GIVE_YN = 'Y'				-- 아이템 지급	 
					 ) C
			ON A.MASTER_SEQ = C.MASTER_SEQ
	WHERE A.MEMBERID = @v_pMemberID
		AND A.ITEM_USE_DATE IS NULL

	RETURN(0)

GO

