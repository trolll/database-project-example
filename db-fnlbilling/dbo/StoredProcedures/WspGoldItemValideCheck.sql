CREATE PROCEDURE dbo.WspGoldItemValideCheck
	@IID INT,
	@IsPackage NCHAR(1),	 
	@AvailablePeriod INT,
	@Count INT,	 	
	@PracticalPeriod INT,
	@Status NCHAR(1)
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aITermOfValidity	INT, 
			@aIMaxStack INT,
			@aIsCharge SMALLINT,
			@aIsPracticalPeriod	INT
			

	SELECT @aErrNo = 0, @aRowCnt = 0, @aITermOfValidity = 0,  @aIMaxStack = 0 , @aIsCharge = 0
	SELECT @aIsPracticalPeriod = 0;
	
		
	SELECT 
		@aITermOfValidity = ITermOfValidity
		, @aIMaxStack = IMaxStack
		, @aIsCharge = IIsCharge
		, @aIsPracticalPeriod = mIsPracticalPeriod
	FROM FNLParmdbo.DT_ITEM
	WHERE IID = @IID

	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0
		RETURN(1)	-- db error
		
	IF @aRowCnt <= 0  
		RETURN(2)	-- none data
		
	-- 상점에 오픈하면서 비유료 아이템 인 경우	
	IF @Status = N'1' AND @aIsCharge = 0
	BEGIN
		RETURN(7)
	END 		

	-- Package ITEM 
	IF @IsPackage = N'1'	
	BEGIN		
		IF @IID <= 50000
			RETURN(3)	-- package 상품 정보가 아니다.
			
		IF @AvailablePeriod > 0 
			RETURN(3)	-- 유효 기간 오류
			
		IF @Count <= 0
			RETURN(5)	-- 아이템 갯수 오류
				
		IF @PracticalPeriod > 0
			RETURN(6)	-- 효과 지속 시간 오류 							
	END  			

	-- 1. 기간제 아이템 체크 			
	IF @aITermOfValidity > 0 
	BEGIN
		-- 무제한 속성 체크
		IF @aITermOfValidity = 10000 AND @AvailablePeriod > 0
			RETURN(8)	-- 무제한 속성이 아니다..( 무조건 @AvailablePeriod = 0으로 무제한 속성이 체크 되어야 한다.
		
		-- 유효기간 오류이다.
		IF (@aITermOfValidity > 0 AND @aITermOfValidity < 10000) AND (@AvailablePeriod > 30 OR  @AvailablePeriod < 0 )	
			RETURN(9)   -- 	
	END 
	ELSE
	BEGIN			
		-- 기간제 아이템이 아니다.
		IF @AvailablePeriod > 0 OR @AvailablePeriod < 0 
			RETURN(10)	-- 기간제 아이템이 아니나 기간제 정보가 체크 되어 있다.
	END 
	
	
	-- 2. 스택형 아이템
	-- Stack Item
	IF @aIMaxStack = 0 AND @Count > 1	-- 비 스택형 아이템이다.
		RETURN(11)	-- Stack 오류
	
	
	IF @Count <= 0
		RETURN(11)	-- Stack 값이 설정되어 있지 않다. 	


	-- 3. 효과 시간 체크
	-- 효과 시간 설정 아이템이 아닌데..입력되었다.	
	IF @aIsPracticalPeriod = 0 AND @PracticalPeriod > 0 
		RETURN(12)	-- 효과 지속 시간 에러	

	-- 효과 지속시간
	IF ( @aIsPracticalPeriod = 1 )
		 AND ( @PracticalPeriod < 0  OR @PracticalPeriod > (24*30) )
	BEGIN		 
		RETURN(12)	-- 효과 지속 시간 에러	
	END

    RETURN(0)

GO

