CREATE PROCEDURE dbo.UspGetOrderListByWeb  	
	@pUserNo	INT
	, @pPageNum	INT
	, @pPageSize	INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aLastOrderID BIGINT,
			@RowCount BIGINT
			
	SELECT @aLastOrderID = 0, @RowCount = 0

	-- prev rowcount
	SET @RowCount = (@pPageNum-1)*@pPageSize + 1;

	SET ROWCOUNT @RowCount

	-- last order id
	SELECT 
		@aLastOrderID = OrderID
	FROM dbo.TBLOrderList
	WHERE  mUserNo = @pUserNo
			AND OrderStatus = N'2'
			AND mOrderType IN(1,3)
	ORDER BY OrderID DESC

	-- order list
	SET ROWCOUNT @pPageSize

	SELECT 
		T1.OrderID 		
		, T1.mNm 
		, T1.mSrvNo
		, T1.GoldItemID 
		, T1.Count 
		, T1.UseGold
		, T1.OrderDate 
		, T2.IID
		, T2.ItemName
	FROM dbo.TBLOrderList T1
		INNER JOIN dbo.TBLGoldItem T2
			ON T1.GoldItemID = T2.GoldItemID
	WHERE  mUserNo = @pUserNo
			AND OrderStatus = N'2'
			AND mOrderType IN(1,3)
			AND OrderID <= @aLastOrderID
	ORDER BY OrderID DESC

GO

