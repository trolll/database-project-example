
CREATE PROCEDURE dbo.UspGetGSExchange
	@pExchangeType	CHAR(1),
	@pSvrNo		SMALLINT,
	@pLimitHour	TINYINT = 24,	-- Hour ( ex, 24 Hour)
	@pCount		INT = 1000, 		
	@pExchangeID	BIGINT = NULL			
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aSQL NVARCHAR(1000)
	
	SET ROWCOUNT @pCount 

	SET @aSQL = N''	
	SET @aSQL = @aSQL +	N' SELECT '
	SET @aSQL = @aSQL +	N'		ExchangeID '	
	SET @aSQL = @aSQL +	N'		,RmUserNo '	
	SET @aSQL = @aSQL +	N'		,RmNo '	
	SET @aSQL = @aSQL +	N'		,RmNm '	
	SET @aSQL = @aSQL +	N'		,ExchangeType '	
	SET @aSQL = @aSQL +	N'		,RegDate '	
	SET @aSQL = @aSQL +	N'		,ExchangeGold '	
	SET @aSQL = @aSQL +	N'		,ExchangeSilver '	
	SET @aSQL = @aSQL +	N'		,Commission '	
	SET @aSQL = @aSQL +	N'		,RmBillingExchangeID '	
	SET @aSQL = @aSQL +	N'	FROM dbo.TBLGSExchange '	
	SET @aSQL = @aSQL +	N'	WHERE '	
	SET @aSQL = @aSQL +	N'		mSvrNo = @pSvrNo '
	SET @aSQL = @aSQL +	N'		AND ExchangeType = @pExchangeType '
	SET @aSQL = @aSQL +	N'		AND Status = ''2'''		-- 芭贰殿废 肯丰	
	SET @aSQL = @aSQL +	N'		AND RegDate > DATEADD(HH, -@pLimitHour, GETDATE()) '		
	IF @pExchangeID IS NOT NULL
	BEGIN
		SET @aSQL = @aSQL +	N'			AND ExchangeID < @pExchangeID '	
	END 
	SET @aSQL = @aSQL +	N'	ORDER BY RegDate DESC '	

	EXEC sp_executesql  @aSQL, N' @pExchangeType CHAR(1), @pSvrNo SMALLINT, @pExchangeID BIGINT, @pLimitHour	TINYINT', @pExchangeType = @pExchangeType, @pSvrNo = @pSvrNo, @pExchangeID = @pExchangeID, @pLimitHour = @pLimitHour

GO

