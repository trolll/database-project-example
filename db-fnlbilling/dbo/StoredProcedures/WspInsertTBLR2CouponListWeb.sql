
CREATE  PROCEDURE [dbo].[WspInsertTBLR2CouponListWeb]
		@couponStr nvarchar(30),
		@gameUserId nvarchar(20),
		@svrNo int,
	             @charNo int,
	             @charNm nvarchar(20),
	             @returnValue int output
	AS	
			
	              declare @masterSeq int  
	              select @masterSeq = master_seq from r2_coupon_list where coupon_str = @couponStr             
	              	if @masterSeq <> 0
		            	begin
					       insert into r2_coupon_list_web(
				                          master_seq,
				                          coupon_str,
				                          game_user_id,
				                          svrNo,
				                          charno,
				                          charnm
				                     ) values(
				                          @masterSeq,
				                          @couponStr,
				                          @gameUserId,
				                          @svrNo,
				                          @charNo,
				                          @charNm
				                      )	
					        if @@error <> 0
				                       begin
				                          set @returnValue = 0
				                           return @returnValue
				                       end
				            else
				                       begin
				                            set @returnValue = 1
				                             return @returnValue
				                       end
				    	end
					else
						begin
                                                       set @returnValue = 2
							return @returnValue
						end

GO

