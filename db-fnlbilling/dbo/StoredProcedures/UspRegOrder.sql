CREATE   PROCEDURE dbo.UspRegOrder
	@pUserNo	int
	,@pNo	int
	,@pNm	nvarchar(12)
	,@pIDCID	smallint
	,@pSrvNo	smallint
	,@pGoldItemID	bigint
	,@pCount	smallint
	,@pUseGold	int
	,@pRemainGold	int
	,@pOrderIP	nvarchar(19)
	,@pBillingOrderID	nvarchar(20)
	,@pBillingReturnCode	int
	,@pClass	tinyint
	,@pLevel	smallint
	,@pPcbangLv	smallint
	,@pOrderType	tinyint		-- 0 System 지급, 1 : 일반 구매, 2 : 선물 구매, 3: 게임내 구매
	,@pOrderID	BIGINT	OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aGoldPrice	INT
	SELECT @pOrderID = 0, @aGoldPrice = 0

	SELECT 
		TOP 1 
			@aGoldPrice = T1.GoldPrice	
	FROM dbo.TBLGoldItem T1
		INNER JOIN dbo.TBLGoldItemSupportSvr T2
			ON T1.GoldItemID = T2.GoldItemID
	WHERE T2.GoldItemID = @pGoldItemID
			AND T2.mSvrNo = @pSrvNo
			AND T1.Status IN( N'1',  N'2')		-- 판매 종료 상품도 구매된 상품이라면, 구매 가능하도록 변경
	
	IF @@ROWCOUNT <> 1
		RETURN(2)	-- 지원되지 않은 골드 아이템 정보
		
	-- 웹 구매, 선물 구매 타입은 금액 정보를 0으로 넘겨준다.	
	IF @pOrderType IN (1,2)
	BEGIN
		SET @pUseGold = @aGoldPrice	* @pCount
	END	
		
	IF @pOrderType =  3
	BEGIN	 
		IF ( @aGoldPrice * @pCount ) <> @pUseGold
			RETURN(2)	-- 금액 정보가 일치 않는다.
	END 	
	
	INSERT INTO dbo.TBLOrderList(
		mUserNo
		,mNo
		,mNm
		,IDCID
		,mSrvNo
		,GoldItemID
		,Count
		,UseGold
		,RemainGold		
		,OrderStatus
		,OrderIP
		,BillingOrderID
		,BillingReturnCode
		,mClass, mLevel, mPcbangLv, mOrderType
	) VALUES(
		@pUserNo
		,@pNo
		,@pNm
		,@pIDCID
		,@pSrvNo
		,@pGoldItemID
		,@pCount
		,@pUseGold
		,@pRemainGold
		,N'1'	-- 주문 시도
		,@pOrderIP
		,@pBillingOrderID
		,@pBillingReturnCode
		,@pClass
		,@pLevel
		,@pPcbangLv
		,@pOrderType
	)

	SET @pOrderID = @@IDENTITY

	IF @@ERROR <> 0
		RETURN (1) -- db error
	
	RETURN(0)

GO

