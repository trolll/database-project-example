

CREATE  PROCEDURE dbo.WspGetPCCafeGoldItemById  	
	@ItemSeq BIGINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	

    select t.ItemSeq,t.Status,t1.GoldItemID,t1.ItemName,t1.IID,t1.ItemImage,t1.ItemDesc,t1.OriginalGoldPrice,
    t1.GoldPrice,t1.ItemCategory,t1.IsPackage,t1.AvailablePeriod,t1.Count,t1.PracticalPeriod
    from TBLPCCafeItemList t ,TBLGoldItem t1 
    where	t.ItemSeq=@ItemSeq and t.GoldItemID=t1.GoldItemID and t.status=N'1'

GO

