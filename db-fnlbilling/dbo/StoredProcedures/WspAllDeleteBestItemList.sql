CREATE PROCEDURE [dbo].[WspAllDeleteBestItemList]
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	

	UPDATE dbo.TBLBestItemList SET status = '0',UpdateDate=getdate() where status<>'0' 	
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo <> 0
	BEGIN
	    set @ReturnValue=0
	END
	ELSE
	BEGIN
	    set @ReturnValue=1
	END

GO

