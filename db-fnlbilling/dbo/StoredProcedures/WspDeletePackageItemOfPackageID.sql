CREATE PROCEDURE dbo.WspDeletePackageItemOfPackageID  	
	@pPackageItemID	BIGINT
AS
	SET NOCOUNT ON			

	DELETE dbo.TBLPackageItem
	WHERE  PackageItemID = @pPackageItemID

GO

