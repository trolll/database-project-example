

CREATE PROCEDURE dbo.UspRegRmGSExchange
	@pRmUserNo		INT,
	@pRmNo			INT,
	@pRmNm			NVARCHAR(12),
	@pIDCID			SMALLINT,
	@pSvrNo			SMALLINT,
	@pExchangeType	CHAR(1),
	@pExchangeGold	INT,
	@pExchangeSilver	INT,
	@pCommission	INT,
	@pExchangeID	BIGINT OUTPUT	
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET @pExchangeID = 0
		
	IF (	SELECT COUNT(*) 
			FROM dbo.TBLGSExchange
			WHERE mSvrNo = @pSvrNo
					AND RmUserNo = @pRmUserNo
					AND Status IN('2','4') ) >= 5
	BEGIN
		RETURN(2)	-- 殿废冉荐 檬苞
	END
		
	INSERT INTO dbo.TBLGSExchange(RmUserNo,	RmNo,	RmNm,	
		IDCID,	mSvrNo,	ExchangeType,	ExchangeGold,	ExchangeSilver,	
		Commission )
	VALUES(
		@pRmUserNo,
		@pRmNo,
		@pRmNm,
		@pIDCID,
		@pSvrNo,
		@pExchangeType,
		@pExchangeGold,
		@pExchangeSilver,
		@pCommission
	)
	
	SET @pExchangeID = @@IDENTITY	
	
	IF @@ERROR <> 0 
	BEGIN
		RETURN (1)
	END 
	
	RETURN(0)

GO

