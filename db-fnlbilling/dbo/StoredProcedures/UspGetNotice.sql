




CREATE PROCEDURE dbo.UspGetNotice
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		TOP 30 
		NoticeSeq
		, Title
		, Notice
		, RegistDate	
	FROM dbo.TBLNotice
	WHERE Status = N'1'			
	ORDER BY NoticeSeq DESC

GO

