







CREATE  PROC [dbo].[DB_BACKUP] 
        @DBNAME         varCHAR(16), 
        @path           varchar(32) = 'D:\Works\', 
        @gen            int = 3 
AS 
  begin 
        declare @dev_name       varchar(64) 
        declare @dev_name_del   varchar(64) 
        declare @physical_name  varchar(64) 
        declare @description    varchar(32) 
        
        if ( select count(*) from master.dbo.sysdatabases where name = @dbname ) = 0 
          begin 
                select 'Database "' + @DBNAME + '" does not exist.' 
                return 
          end 

        select @description = @DBNAME  + ' , ' + CONVERT(CHAR(20),GETDATE()) 
        
        --select @dev_name = @@SERVERNAME + '.' + @DBNAME + ' ' + convert(char(10),getdate() , 120 ) 
        --select @dev_name_del = @@SERVERNAME + '.' + @DBNAME + ' ' + convert(char(10),getdate() - @gen , 120 ) 
        select @dev_name = 'nanfang' + '.' + @DBNAME + ' ' + convert(char(10),getdate() , 120 ) 
        select @dev_name_del = 'nanfang' + '.' + @DBNAME + ' ' + convert(char(10),getdate() - @gen , 120 ) 
        

	select @physical_name = rtrim(upper(@PATH)) + rtrim(upper(@dev_name)) + '.BAK' 
        select @description = 'Daily Backup , ' + @DBNAME  + '   ' + CONVERT(CHAR(20),GETDATE()) 
        
        if ( select count(*) from master.dbo.sysdevices where name = rtrim(@dev_name_del) ) > 0 
                EXEC sp_dropdevice @dev_name_del,'delfile' 

        if ( select count(*) from master.dbo.sysdevices where name = rtrim(@dev_name) ) = 0 
                EXEC sp_addumpdevice 'disk' , @dev_name , @physical_name 

        BACKUP DATABASE @DBNAME TO @dev_name WITH INIT , NAME = @description , NOSKIP , NOFORMAT 

end

GO

