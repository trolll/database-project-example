

CREATE  PROCEDURE dbo.WspGetGoldItemsOfPackage  	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	select GoldItemID,ItemName,IID,ItemImage,ItemDesc,OriginalGoldPrice,GoldPrice,
		ItemCategory,IsPackage,AvailablePeriod,Count,PracticalPeriod,Status 
	from TBLGoldItem t where isPackage=N'1' and status in ('1','2')
	order by GoldItemID

GO

