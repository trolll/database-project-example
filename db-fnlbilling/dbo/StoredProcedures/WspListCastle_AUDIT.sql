












/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2011-3-17 14:09:43 ******/

/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2011-3-3 16:58:03 ******/

/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2009-12-15 18:17:28 ******/

/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2009-7-16 7:49:38 ******/



/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2009-6-10 9:19:25 ******/  
  
  
/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2009-4-15 8:59:17 ******/  
  
/****** Object:  Stored Procedure dbo.WspListCastle_AUDIT    Script Date: 2009-2-25 14:50:35 ******/  
  
  
  
  
  
CREATE    PROCEDURE [dbo].[WspListCastle_AUDIT]  
 @mBackupYear INT,  
 @mBackupMonth INT,  
 @mBackupDay INT  
AS  
 SET NOCOUNT ON  
   
 DECLARE @mDate DATETIME  
 DECLARE @Sql  NVARCHAR(3000)  
 DECLARE @mTable1 NVARCHAR(50)  
 DECLARE @mTable2 NVARCHAR(50)  
 DECLARE @mTable3 NVARCHAR(50)  
 DECLARE @mTable4 NVARCHAR(50)  
 DECLARE @mBackupDate DATETIME  
  
 SET @mDate = GETDATE()  
 SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)  
  
 IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )  
 BEGIN  
  SET @mTable1 = N'TblPc_history'  
  SET @mTable2 = N'TblGuildMember_history'  
  SET @mTable3 = N'TblCastleTowerStone_history'  
  SET @mTable4 = N'TblGuild_history'  
 END  
 ELSE  
 BEGIN  
  SET @mTable1 = N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
  SET @mTable2 = N'TblGuildMember_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
  SET @mTable3 = N'TblCastleTowerStone_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
  SET @mTable4 = N'TblGuild_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
 END  
  
 SET  @Sql =   
 'SELECT  
  (select mPlaceNm from [FNLParm].dbo.TblPlace   pl where c.mPlace=pl.mplaceno) as mplace  
  , g.mGuildNm  
  , (  
   SELECT p.mNm  
   FROM ' + @mTable1 + ' AS p WITH (NOLOCK)  
   INNER JOIN ' + @mTable2 + ' AS gm WITH (NOLOCK)  
   ON p.mNo = gm.mPcNo  
   WHERE gm.mGuildNo = c.mGuildNo AND gm.mGuildGrade = 0  
    ) AS mGuildMasterNm  
  , (  
   SELECT COUNT(*)  
   FROM ' + @mTable2 + ' AS gm WITH (NOLOCK)  
   WHERE gm.mGuildNo = c.mGuildNo  
    ) AS mGuildTotCnt  
  , c.mTaxBuy  
  , c.mTaxHunt  
  , c.mTaxGamble  
  , c.mAsset  
  
 FROM   
  ' + @mTable3 + ' AS c WITH (NOLOCK)  
 INNER JOIN  
  ' + @mTable4 + ' AS g WITH (NOLOCK)  
 ON  
  c.mGuildNo = g.mGuildNo'  
  
 EXEC sp_executesql @Sql, N'@mBackupDate DATETIME', @mBackupDate = @mBackupDate  
  
 SET NOCOUNT OFF

GO

