CREATE PROCEDURE dbo.WspDeletePackageItem  	
	@PackageItemID BIGINT,
	@GoldItemID BIGINT,
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	update TBLPackageItem set status = '0',UpdateDate=getdate() where
		PackageItemID=@PackageItemID and GoldItemID = @GoldItemID
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
        BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END

GO

