

CREATE  PROCEDURE dbo.WspGetCategoryAssign  	
	@CategoryID BIGINT,
	@GoldItemID BIGINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
    select CategoryID,GoldItemID,Status,OrderNO from TBLCategoryAssign 
    where CategoryID = @CategoryID	and GoldItemID = @GoldItemID

GO

