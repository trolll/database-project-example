CREATE PROCEDURE dbo.UspGetGSExchangeOfExchangeID
	@pExchangeID	BIGINT
	, @pStatus		CHAR(1) 
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		RmUserNo 
		,RmNo 
		,RmNm 
		,ExchangeType 
		,RegDate 
		,ExchangeGold 
		,ExchangeSilver 
		,Commission 	
		,RmBillingExchangeID
		,mSvrNo 
	FROM dbo.TBLGSExchange 
	WHERE ExchangeID = @pExchangeID 
		AND Status = @pStatus

GO

