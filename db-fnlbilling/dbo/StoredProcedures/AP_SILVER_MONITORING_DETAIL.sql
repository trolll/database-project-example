








/******************************************************************************
**		Name: AP_SILVER_MONITORING_DETAIL
**		Desc: 归父 角滚 捞惑 瞒捞啊 沥焊 荐笼
**
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      20120326	捞柳急				弥檬积己
**		20120717	捞柳急				芒绊 殿.. 角滚啊 茄扒捞 酒聪扼, 滴扒捞 乐绢 炼牢矫 拌魂捞 捞惑窍霸凳. MAX 1扒栏肺 函版
*******************************************************************************/ 
CREATE PROC [dbo].[AP_SILVER_MONITORING_DETAIL]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'u' AND name = 'TblDiff')
	BEGIN
		DROP TABLE dbo.TblDiff
	END

	SELECT
		mPos
		, mOwner
		, SUM(mCnt) AS mCnt INTO dbo.TblDiff
	FROM (
		SELECT
			0 AS mPos
			, mPcNo AS mOwner
			, mCnt AS mCnt
		FROM FNLGame1133.dbo.TblPcInventory   
		WHERE mPcNo > 1
		AND mItemNo = 409
		UNION ALL
		SELECT
			1 AS mPos
			, mUserNo AS mOwner
			, mCnt AS mCnt
		FROM FNLGame1133.dbo.TblPcStore 
		WHERE mItemNo = 409
		UNION ALL
		SELECT
			2 AS mPos
			, mGuildNo AS mOwner
			, mCnt AS mCnt
		FROM FNLGame1133.dbo.TblGuildStore  
		WHERE mItemNo = 409  
		UNION ALL
		SELECT
			3 AS mPos
			, mGID AS mOwner
			, mGuildMoney AS mCnt
		FROM FNLGame1133.dbo.TblGuildAccount
		UNION ALL
		SELECT
			4 AS mPos
			, mGuildNo AS mOwner
			, mAsset AS mCnt
		FROM FNLGame1133.dbo.TblCastleTowerStone
	) T1
	GROUP BY mPos, mOwner
	
	INSERT INTO dbo.TBL_SILVER_LOG_DETAIL
	SELECT
		GETDATE() mRegDate
	  , T1.mPos 
	  , T1.mOwner  
	  , T1.mCnt  
	  , ( ISNULL(T1.mCnt,0) - ISNULL(T2.mCnt,0) ) mDiffCnt
	FROM dbo.TblDiff T1
		LEFT OUTER JOIN dbo.TBL_SILVER_LOG_CURR_DETAIL T2  
	ON T1.mPos = T2.mPos
	AND T1.mOwner = T2.mOwner
	WHERE ( ISNULL(T1.mCnt,0) - ISNULL(T2.mCnt,0) ) >= 1000000 --瞒捞樊捞 归父 角滚 捞惑.

	IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'u' AND name = 'TBL_SILVER_LOG_CURR_DETAIL')
	BEGIN
		DROP TABLE dbo.TBL_SILVER_LOG_CURR_DETAIL
	END

	SELECT
		mPos 
		, mOwner
		, mCnt INTO dbo.TBL_SILVER_LOG_CURR_DETAIL
	FROM dbo.TblDiff 
END

GO

