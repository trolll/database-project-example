
CREATE  PROCEDURE [dbo].[WspUpdateTBLR2CouponMaster]	
		@couponStr nvarchar(20),
	    @returnValue INT OUTPUT
	AS	
			
	    declare @masterSeq int	
	    select @masterSeq = master_seq from r2_coupon_list where coupon_str = @couponStr               
	    if @masterSeq <> 0
	        begin
	          	update r2_coupon_master
	                set web_active_cnt = web_active_cnt+1 
	            	where 
	                	master_seq = @masterSeq
	           	if @@error <> 0
	              begin
	                  set @returnValue = 0
	                  return @returnValue
	              end
	           	else
	              begin
                          set @returnValue = 1
	                  return @returnValue
	              end
	    	end
	     	else
				begin
                                        set @returnValue = 2
					return @returnValue
				end

GO

