

--add by xuwei 2009-02-02
--鎭㈠宸茬粡鍒犻櫎鐨勫叕鍛婁簨椤癸紙status璁剧疆涓?锛?
CREATE PROCEDURE dbo.WspResetNotice
 @NoticeSeq BIGINT,
 @ReturnValue INT OUTPUT
AS
 SET NOCOUNT ON 
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

 update TBLNotice set status='1',UpdateDate=getdate() where NoticeSeq = @NoticeSeq
 IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
        BEGIN
            set @ReturnValue=0
        END
    ELSE
        BEGIN
            set @ReturnValue=1
        END

GO

