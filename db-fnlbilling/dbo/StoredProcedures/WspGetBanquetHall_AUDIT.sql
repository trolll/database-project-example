












/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2011-3-17 14:09:43 ******/

/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2011-3-3 16:58:03 ******/

/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2009-12-15 18:17:27 ******/

/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2009-7-16 7:49:38 ******/

/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2009-6-10 9:19:25 ******/


/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2009-4-15 8:59:17 ******/

/****** Object:  Stored Procedure dbo.WspGetBanquetHall_AUDIT    Script Date: 2009-2-25 14:50:34 ******/






CREATE         PROCEDURE [dbo].[WspGetBanquetHall_AUDIT]	
	@mBackupYear INT,
	@mBackupMonth INT,
	@mBackupDay INT
AS
	SET NOCOUNT ON

	DECLARE @mDate	DATETIME
	DECLARE @Sql		NVARCHAR(3000)
	DECLARE @mTable1	NVARCHAR(50)
	DECLARE @mTable2	NVARCHAR(50)
	DECLARE @mBackupDate DATETIME
	
	SET @mDate = GETDATE()
	SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)

	IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )
	BEGIN
		SET @mTable1	= N'TblPc_history'
		SET @mTable2	= N'TblBanquetHall_history'
	END
	ELSE
	BEGIN
		SET @mTable1	= N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)
		SET @mTable2	= N'TblBanquetHall_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)	
	END


	SET	@Sql = 
	'SELECT
		p.mNo,
		p.mNm,
		p.mClass,
		s.mTerritory ,
		s.mBanquetHallNo,
		s.mBanquetHallType,
		s.mOwnerPcNo,
		s.mRegDate,
		s.mLeftMin				
	FROM  '
		+ @mTable1 + ' AS p WITH (NOLOCK)
	INNER JOIN  '		
		+ @mTable2 +  '  AS s WITH (NOLOCK)
	ON
		p.mNo = s.mOwnerPcNo'	
	
	EXEC sp_executesql @Sql

GO

