/******************************************************************************
**		Name: UspGetOrderList 
**		Desc: 구매 목록 정보 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      2007-10-02	soundkey			페이지 방식으로 수정
**		2007-10-05	soundkey			끝페이지판별위한 수정 @PageSize+1
**		2008-03-19	JUDY				페이징 기능 버그 수정
**		20080506	JUDY				Item ID와 골드 아이템 이름 반환
**		20080611	JUDY				골드 아이템의 속성 카테고리 정보 반환
**		20080724	JUDY				거래내역에서 선물함 제외
**		20090420	JUDY				paging error
*******************************************************************************/ 
CREATE  PROCEDURE dbo.UspGetOrderList
	@pUserNo	INT
	, @pSvrNo	SMALLINT
	, @pPageNum	INT
	, @pPageSize	INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aLastOrderID BIGINT,
			@RowCount BIGINT,
			@PageCount INT
			
	SELECT @aLastOrderID = 0, @RowCount = 0

	-- prev rowcount
	SET @RowCount = (@pPageNum-1)*@pPageSize + 1;
	SET @PageCount = @pPageSize+1;

	SET ROWCOUNT @RowCount

	-- last order id
	SELECT 
		@aLastOrderID = OrderID
	FROM dbo.TBLOrderList
	WHERE  mUserNo = @pUserNo
			AND mSrvNo = @pSvrNo
			AND OrderStatus = N'2'
			AND mOrderType IN(1,3)
	ORDER BY OrderID DESC

	-- order list
	SET ROWCOUNT @PageCount

	SELECT 
		T1.OrderID 
		, T1.mNm 
		, T1.GoldItemID 
		, T1.Count 
		, T1.UseGold 
		, T1.OrderDate 
		, T2.IID
		, T2.ItemName
		, CONVERT(SMALLINT, T2.ItemCateGory) ItemCateGory
	FROM dbo.TBLOrderList T1
		INNER JOIN dbo.TBLGoldItem T2
			ON T1.GoldItemID = T2.GoldItemID
	WHERE  mUserNo = @pUserNo
			AND mSrvNo = @pSvrNo
			AND OrderStatus = N'2'
			AND OrderID <= @aLastOrderID
			AND mOrderType IN(1,3)
	ORDER BY OrderID DESC

GO

