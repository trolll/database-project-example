CREATE    PROCEDURE dbo.UspUptTradeGSExchange
	@pExchangeID	BIGINT
	,@pStatus		CHAR(1)	-- 상태값
	,@pSvrNo		SMALLINT
	,@pErrorCode	INT	 = 0 
	,@pErrorStep	SMALLINT = 0
	,@pRmBillingExchangeID	NVARCHAR(20) = NULL -- 등록시 빌링 id
	,@pBmBillingExchangeID	NVARCHAR(20) = NULL -- 구매시 빌링 id
	,@pBillingReturnCode	INT = NULL	-- 빌링 리턴 코드
AS
	SET NOCOUNT ON

	DECLARE @aErr INT, @aRowCnt INT
	SELECT @aErr = 0, @aRowCnt = 0

	-- 등록시(구매/등록 id 가 없을 경우)
	IF  ( @pRmBillingExchangeID IS NULL
			OR @pRmBillingExchangeID = N'' ) AND 
		( @pBmBillingExchangeID IS NULL
			OR @pBmBillingExchangeID = N'' )	
	BEGIN	
		UPDATE dbo.TBLGSExchange
		SET 
			ExchangeDate = GETDATE()	
			, Status = @pStatus
			, ErrorCode = @pErrorCode
			, ErrorStep = @pErrorStep		
		WHERE ExchangeID = @pExchangeID	
				AND mSvrNo = @pSvrNo
		
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT
		IF @aErr <> 0 OR @aRowCnt <= 0
		BEGIN
			RETURN(1) -- db error
		END 
				
		RETURN(0)				
	END
	
	-- 등록시
	IF  @pRmBillingExchangeID IS NOT NULL
	BEGIN	
		UPDATE dbo.TBLGSExchange
		SET 
			ExchangeDate = GETDATE()	
			, Status = @pStatus
			, ErrorCode = @pErrorCode
			, ErrorStep = @pErrorStep
			, RmBillingExchangeID = @pRmBillingExchangeID
			, BillingReturnCode = @pBillingReturnCode				
		WHERE ExchangeID = @pExchangeID	
				AND mSvrNo = @pSvrNo
				
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT
		IF @aErr <> 0 OR @aRowCnt <= 0
		BEGIN
			RETURN(1) -- db error
		END 
				
		RETURN(0)				
	END
	
	-- 구매시
	IF  @pBmBillingExchangeID IS NOT NULL
	BEGIN	
		UPDATE dbo.TBLGSExchange
		SET 
			ExchangeDate = GETDATE()	
			, Status = @pStatus
			, ErrorCode = @pErrorCode
			, ErrorStep = @pErrorStep
			, BmBillingExchangeID = @pBmBillingExchangeID
			, BillingReturnCode = @pBillingReturnCode						
		WHERE ExchangeID = @pExchangeID	
				AND mSvrNo = @pSvrNo
		
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT
		IF @aErr <> 0 OR @aRowCnt <= 0
		BEGIN
			RETURN(1) -- db error
		END 
				
		RETURN(0)				
	END	

	RETURN(0)

GO

