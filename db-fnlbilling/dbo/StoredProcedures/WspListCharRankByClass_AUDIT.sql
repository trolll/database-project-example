












/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2011-3-17 14:09:43 ******/

/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2011-3-3 16:58:03 ******/

/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2009-12-15 18:17:28 ******/

/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2009-7-16 7:49:38 ******/

/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2009-6-10 9:19:25 ******/


/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2009-4-15 8:59:17 ******/

/****** Object:  Stored Procedure dbo.WspListCharRankByClass_AUDIT    Script Date: 2009-2-25 14:50:35 ******/






CREATE        PROCEDURE [dbo].[WspListCharRankByClass_AUDIT]
	@mClass TINYINT, -- ??? (0:??, 1:???, 2:??)
	@mBackupYear INT,
	@mBackupMonth INT,
	@mBackupDay INT
AS
	SET NOCOUNT ON

	DECLARE @mBackupDate  DATETIME
	DECLARE @mDate	DATETIME
	DECLARE @Sql		NVARCHAR(3000)
	DECLARE @mPcTable	NVARCHAR(50)
	DECLARE @mItemTable	NVARCHAR(50)


	SET @mDate = GETDATE()
	SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)

	IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )
	BEGIN
		SET @mPcTable	= N'TblPc_history'
		SET @mItemTable	= N'TblPcState_history'
	END
	ELSE
	BEGIN
		SET @mPcTable	= N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)
		SET @mItemTable	= N'TblPcState_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)
	END

	SET	@Sql = 
	'SELECT
		TOP 20 mNm, mLevel
	FROM
		' + @mPcTable +  ' AS p WITH (NOLOCK)
	INNER JOIN
		' + @mItemTable + ' AS s WITH (NOLOCK)
	ON
		p.mNo = s.mNo
	WHERE
		mClass = @mClass
		AND p.mDelDate IS NULL
	ORDER BY
		s.mLevel DESC, mExp DESC'


	EXEC sp_executesql @Sql, N'@mClass TINYINT, @mBackupDate DATETIME', @mClass = @mClass, @mBackupDate = @mBackupDate
	
	SET NOCOUNT OFF

GO

