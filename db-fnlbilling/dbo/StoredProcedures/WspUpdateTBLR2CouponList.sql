
CREATE  PROCEDURE [dbo].[WspUpdateTBLR2CouponList]
		@gameUserId nvarchar(20),
		@couponStr nvarchar(30),
		@returnValue INT OUTPUT
	AS				
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED           
		update r2_coupon_list set status = 'A' , memberid = @gameUserId,regdate=getdate() where coupon_str = @couponStr	
		if @@error <> 0
			begin
				set @returnValue = 0
				return @returnValue
			end
		else
			begin
				set @returnValue = 1
				return @returnValue
			end

GO

