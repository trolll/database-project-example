CREATE PROCEDURE dbo.WspGetCouponMaster	
		@itemNo int,
		@beginDate nvarchar(8),
		@endDate nvarchar(8),
		@couponTypeNo int
	as
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		declare @sql nvarchar(3000)
		set @sql = 'SELECT a.MASTER_SEQ, a.TITLE, a.COUPONCNT, a.RELATION_PRODUCT_NO, a.REG_LIMITDATE, a.USE_LIMITDATE, a.REGDATE, MEMO,
				   a.STATUS, a.ONEJUMIN_MULTIID_YN, a.BILLING_USER_USE_YN, a.ITEM_GIVE_YN, a.ITEM_GIVE_LIMIT_YN, a.COUPON_TYPE_NO,
				   a.REG_PERSON, (select count(m.MASTER_SEQ) from R2_COUPON_ITEM_LOG m where m.MASTER_SEQ = a.MASTER_SEQ ) as GAME_USE_CNT,
				   a.WEB_ACTIVE_CNT, d.INAME
			FROM R2_COUPON_MASTER a , R2_COUPON_ITEM_LIST c, FNLParm.dbo.dt_item d
			WHERE a.MASTER_SEQ = c.MASTER_SEQ and c.ORDER_NUM = 1 and d.IID = c.ITEM_NO'
		if @itemNo is not null and @itemNo <> ''
			set @sql = @sql + ' and c.ITEM_NO = convert(int,@itemNo)'
		if @beginDate is not null and @beginDate <> ''
			set @sql = @sql + ' and a.REGDATE >= @beginDate'
		if @endDate is not null and @endDate <> ''
			set @sql = @sql + ' and a.REGDATE <= @endDate'
		if @couponTypeNo = '4'
			set @sql = @sql + ' and a.COUPON_TYPE_NO not in (1,2,3)'
		else if @couponTypeNo is not null and @couponTypeNo <> ''
			set @sql = @sql + ' and a.COUPON_TYPE_NO = convert(int,@couponTypeNo)'			
		set @sql = @sql + ' ORDER BY a.RegDate desc,a.MASTER_SEQ desc'
		exec sp_executesql @sql,N'@itemNo int, @beginDate nvarchar(8), @endDate nvarchar(8), @couponTypeNo int',@itemNo,@beginDate,@endDate,@couponTypeNo

GO

