












/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2011-3-17 14:09:43 ******/

/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2011-3-3 16:58:04 ******/

/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2009-12-15 18:17:28 ******/

/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2009-7-16 7:49:38 ******/


/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2009-6-10 9:19:25 ******/  
  
  
/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2009-4-15 8:59:17 ******/  
  
/****** Object:  Stored Procedure dbo.WspListItemByTid_AUDIT    Script Date: 2009-2-25 14:50:35 ******/  
  
  
  
  
CREATE PROCEDURE [dbo].[WspListItemByTid_AUDIT]  
 @mBackupYear INT,  
 @mBackupMonth INT,  
 @mBackupDay INT,  
   
 @mItemNo INT -- TID  
AS  
 SET NOCOUNT ON   
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
  
 DECLARE @mDate DATETIME  
 DECLARE @Sql  NVARCHAR(3000)  
 DECLARE @mTable1 NVARCHAR(50)  
 DECLARE @mTable2 NVARCHAR(50)  
 DECLARE @mTable3 NVARCHAR(50)  
 DECLARE @mTable4 NVARCHAR(50)  
 DECLARE @mTable5 NVARCHAR(50)  
 DECLARE @mBackupDate DATETIME  
   
 SET @mDate = GETDATE()  
 SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)  
  
 IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )  
 BEGIN  
  SET @mTable1 = N'TblPcInventory_history'  
  SET @mTable2 = N'TblPcStore_history'  
  SET @mTable3 = N'TblPc_history'  
  SET @mTable4 = N'TblGuildStore_history'  
  SET @mTable5 = N'TblGuild_history'    
 END  
 ELSE  
 BEGIN  
  SET @mTable1 = N'TblPcInventory_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)  
  SET @mTable2 = N'TblPcStore_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)   
  SET @mTable3 = N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)   
  SET @mTable4 = N'TblGuildStore_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)   
  SET @mTable5 = N'TblGuild_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)       
 END  
   
 SET @Sql = '    
  SELECT   
   mSerialNo  
   , mNm  
   , T1.mRegDate  
   , T2.mUserId  
   , NULL mGuildNm  
  FROM (  
   SELECT  
    i.mSerialNo  
    , p.mNm  
    , i.mRegDate  
    , p.mOwner  
   FROM   
    ' + @mTable1 + '  i  
    INNER JOIN ' + @mTable3 + ' p  
     ON i.mPcNo = p.mNo  
   WHERE  
    i.mItemNo = @mItemNo  
    AND i.mPcNo > 1  
    AND p.mDelDate IS NULL  
   UNION ALL  
   SELECT   
    mSerialNo,  
    ''Store'' AS mNm,  
    mRegDate,  
    mUserNo   
   FROM   
    ' + @mTable2 + ' s  
   WHERE mItemNo = @mItemNo  
  ) T1  
   INNER JOIN FNLAccount.dbo.TblUser T2  
    ON T1.mOwner = T2.mUserNo   
  UNION ALL  
  SELECT   
   mSerialNo  
   , ''GuildStore'' mNm  
   , T1.mRegDate  
   , NULL mUserId  
   , T2.mGuildNm  
  FROM ' + @mTable4 + ' T1  
   INNER JOIN '+ @mTable5 + ' T2  
    ON T1.mGuildNo = T2.mGuildNo  
  WHERE mItemNo = @mItemNo '   
  
 EXEC sp_executesql @Sql, N'@mItemNo INT ', @mItemNo = @mItemNo

GO

