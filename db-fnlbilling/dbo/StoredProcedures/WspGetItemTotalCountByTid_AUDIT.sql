












/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2011-3-17 14:09:43 ******/

/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2011-3-3 16:58:03 ******/

/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2009-12-15 18:17:28 ******/

/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2009-7-16 7:49:38 ******/

/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2009-6-10 9:19:25 ******/


/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2009-4-15 8:59:17 ******/

/****** Object:  Stored Procedure dbo.WspGetItemTotalCountByTid_AUDIT    Script Date: 2009-2-25 14:50:35 ******/




----------------------------------------------------------------------------------------------------------------
-- CREATE SCRIPT 
----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[WspGetItemTotalCountByTid_AUDIT]
	@mBackupYear INT,
	@mBackupMonth INT,
	@mBackupDay INT,
	
	@mItemNo INT, -- TID
	@mTotCnt INT OUTPUT -- ? ??
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


	DECLARE @mDate	DATETIME
	DECLARE @Sql		NVARCHAR(3000)
	DECLARE @mTable1	NVARCHAR(50)
	DECLARE @mTable2	NVARCHAR(50)
	DECLARE @mTable3	NVARCHAR(50)
	DECLARE @mTable4	NVARCHAR(50)
	DECLARE @mTable5	NVARCHAR(50)
	
	DECLARE @mBackupDate DATETIME
	
	SET @mDate = GETDATE()
	SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)

	IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )
	BEGIN
		SET @mTable1	= N'TblPcInventory_history'
		SET @mTable2	= N'TblPcStore_history'
		SET @mTable3	= N'TblPc_history'
		SET @mTable4	= N'TblGuildStore_history'
		SET @mTable5	= N'TblGuild_history'
	END
	ELSE
	BEGIN
		SET @mTable1	= N'TblPcInventory_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)
		SET @mTable2	= N'TblPcStore_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)	
		SET @mTable3	= N'TblPc_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)	
		SET @mTable4	= N'TblGuildStore_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)	
		SET @mTable5	= N'TblGuild_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)	
	END


	SET @Sql = ' 
		SELECT @mTotCnt = SUM(mTot) 
		FROM(
			SELECT 
				COUNT(*) AS mTot
			FROM 
				' + @mTable1 + ' T1
				INNER JOIN ' + @mTable3 + ' T2
					ON T1.mPcNo = T2.mNo	
			WHERE 
				T1.mPcNo > 1				
				AND T2.mDelDate IS NULL
				AND mItemNo = @mItemNo 
			UNION ALL
			SELECT COUNT(*) AS mTot
			FROM ' + @mTable2 + '
			WHERE mItemNo = @mItemNo
			UNION ALL
			SELECT COUNT(*) AS mTot
			FROM ' + @mTable4 + '
			WHERE mItemNo = @mItemNo
		) T1 '
		
	EXEC sp_executesql @Sql, N'@mTotCnt INT OUTPUT, @mItemNo INT ', @mTotCnt OUTPUT, @mItemNo = @mItemNo

GO

