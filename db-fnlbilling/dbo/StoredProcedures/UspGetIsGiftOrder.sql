CREATE PROCEDURE dbo.UspGetIsGiftOrder 	
	@pSvrNo	SMALLINT
	, @pUserNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		
	
	IF EXISTS(	
		SELECT 
			TOP 1
			mOrderType mGiftType					
		FROM dbo.TBLWebOrderList T1
		WHERE mUserNo = @pUserNo
			AND mSvrNo = @pSvrNo
			AND mStatus = 0
		UNION ALL
		SELECT 
			TOP 1
			0 mGiftType	-- 0 System 瘤鞭
		FROM dbo.TBLSysOrderList
		WHERE mUserNo = @pUserNo
			AND mSvrNo IN (@pSvrNo, 0)	
			AND mStatus = 0	
			AND mLimitedDate > GETDATE())
	BEGIN
		RETURN (0)
	END 
	
	RETURN(1)

GO

