
CREATE PROCEDURE dbo.WspUpdateCouponMaster  
	        @masterSeq INT,
	        @title NVARCHAR(100),
	        @couponCnt INT,
	        @regLimitDate NVARCHAR(10),
	        @useLimitDate NVARCHAR(10),
	        @memo NVARCHAR(1000),
	        @couponTypeNo INT
	AS		
		DELETE FROM R2_COUPON_ITEM_GIVE_LIMIT
		WHERE  MASTER_SEQ = @masterSeq
		DELETE FROM R2_COUPON_ITEM_LIST 
		WHERE  MASTER_SEQ = @masterSeq
		UPDATE R2_COUPON_MASTER SET
		      TITLE=@title,
		      COUPONCNT=@couponCnt,
		      REG_LIMITDATE=@regLimitDate,
		      USE_LIMITDATE=@useLimitDate,
		      MEMO=@memo,
		      COUPON_TYPE_NO=@couponTypeNo
		WHERE  MASTER_SEQ = @masterSeq

GO

