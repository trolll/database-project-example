












/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2011-9-26 13:19:20 ******/


/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2011-3-17 14:09:43 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2011-3-3 16:58:03 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2010-3-23 12:38:32 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2009-12-15 18:17:28 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2009-11-17 7:36:23 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2009-9-22 10:40:29 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2009-7-16 7:49:38 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2009-6-10 9:19:25 ******/


/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2009-4-15 8:59:17 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildAgit_AUDIT    Script Date: 2009-2-25 14:50:35 ******/






CREATE         PROCEDURE [dbo].[WspGetGuildAgit_AUDIT]	
	@mBackupYear INT,
	@mBackupMonth INT,
	@mBackupDay INT
AS
	SET NOCOUNT ON

	DECLARE @mDate	DATETIME
	DECLARE @Sql		NVARCHAR(3000)
	DECLARE @mTable1	NVARCHAR(50)
	DECLARE @mTable2	NVARCHAR(50)
	DECLARE @mBackupDate DATETIME
	
	SET @mDate = GETDATE()
	SET @mBackupDate = CAST(STR(@mBackupYear) + '-' + STR(@mBackupMonth) + '-' + STR(@mBackupDay) AS DATETIME)

	IF (CONVERT(NVARCHAR(8), @mBackupDate, 112) =  CONVERT(NVARCHAR(8), @mDate, 112) )
	BEGIN
		SET @mTable1	= N'TblGuild_history'
		SET @mTable2	= N'TblGuildAgit_history'
	END
	ELSE
	BEGIN
		SET @mTable1	= N'TblGuild_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)
		SET @mTable2	= N'TblGuildAgit_history_' + CONVERT(NVARCHAR(8), @mBackupDate, 112)	
	END


	SET	@Sql = 
	'SELECT
		p.mGuildNo,
		p.mGuildNm ,
		s.mTerritory,
		s.mGuildAgitNo,
		s.mGuildAgitName,
		s.mOwnerGID,
		s.mRegDate,
		s.mLeftMin,
		s.mIsSelling,
		s.mSellingMoney,
		s.mBuyingMoney						
	FROM  '
		+ @mTable1 + ' AS p WITH (NOLOCK)
	INNER JOIN  '		
		+ @mTable2 +  '  AS s WITH (NOLOCK)
	ON
		p.mGuildNo = s.mOwnerGID '	
	
	EXEC sp_executesql @Sql

GO

