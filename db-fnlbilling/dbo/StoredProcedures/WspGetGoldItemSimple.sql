CREATE PROCEDURE dbo.WspGetGoldItemSimple  	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
			T1.GoldItemID
			, T1.IID
			, T1.ItemName
			, T1.ItemDesc
			, T1.ItemCategory
			, T1.IsPackage			
			, T1.OriginalGoldPrice
			, T1.GoldPrice
			, T1.AvailablePeriod
			, T1.Count
			, T1.Status
			, T1.PracticalPeriod					
		FROM dbo.TBLGoldItem T1
-- 		WHERE T1.Status IN( N'1', N'2' )
		ORDER BY GoldItemID ASC

GO

