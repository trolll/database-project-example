
CREATE  PROCEDURE dbo.WspUpdateNotice  	
	@NoticeSeq BIGINT,
	@Title NVARCHAR(50),
	@Notice NVARCHAR(3000),
	@Status CHAR(1),
	@UpdateAdmin NVARCHAR(20),
	@UpdateIP NVARCHAR(19),
	@ReturnValue INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  if(exists(select 1 from dbo.TBLNotice where noticeseq=@NoticeSeq))
  begin
    	update TBLNotice set
            Title=@Title,
            Notice=@Notice,
            Status=@Status,
            UpdateDate=getdate(),
            UpdateAdmin=@UpdateAdmin,
            UpdateIP=@UpdateIP
        where NoticeSeq=@NoticeSeq
	
    	 IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
            BEGIN
                set @ReturnValue=0
            END
        ELSE
            BEGIN
                set @ReturnValue=1
            END
    end
    else
    begin
        insert into TBLNotice
    	 (NoticeSeq,Title,Notice,Status,RegistDate,RegistAdmin,RegistIP,UpdateDate,UpdateAdmin,UpdateIP) values
    	 (@NoticeSeq,@Title,@Notice,@Status,getdate(),@UpdateAdmin,@UpdateIP,getdate(),@UpdateAdmin,@UpdateIP)
	
    	  IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
            BEGIN
                set @ReturnValue=0
            END
        ELSE
            BEGIN
                set @ReturnValue=1
            END
    end

GO

