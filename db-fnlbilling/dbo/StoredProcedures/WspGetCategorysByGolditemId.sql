

CREATE  PROCEDURE dbo.WspGetCategorysByGolditemId
	@GoldItemID BIGINT  	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
    select t1.CategoryID,t1.CategoryName,t1.CategoryDesc,t1.Status
    from TBLCategoryAssign t ,TBLCategory t1 
    where t.GoldItemID = @GoldItemID and t.CategoryID=t1.CategoryID and t.status=N'1' and t1.status=N'1'
    order by t.CategoryID desc

GO

