CREATE TABLE [dbo].[TBLPackageItem] (
    [GoldItemID]    BIGINT        NOT NULL,
    [PackageItemID] BIGINT        NOT NULL,
    [Status]        NCHAR (1)     NOT NULL,
    [Count]         INT           CONSTRAINT [DF_TBLPackageItem_Count] DEFAULT (1) NOT NULL,
    [RegistDate]    DATETIME      CONSTRAINT [DF_TBLPackageItem_RegistDate] DEFAULT (getdate()) NOT NULL,
    [RegistAdmin]   NVARCHAR (20) NULL,
    [RegistIP]      NVARCHAR (19) NULL,
    [UpdateDate]    DATETIME      NULL,
    [UpdateAdmin]   NVARCHAR (20) NULL,
    [UpdateIP]      NVARCHAR (19) NULL,
    CONSTRAINT [CL_PKTBLPackageItem] PRIMARY KEY CLUSTERED ([PackageItemID] ASC, [GoldItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLPackageItem_Status] CHECK ([Status] = N'2' or ([Status] = N'1' or [Status] = N'0'))
);


GO

CREATE NONCLUSTERED INDEX [NC_TBLPackageItem_01]
    ON [dbo].[TBLPackageItem]([GoldItemID] ASC) WITH (FILLFACTOR = 90);


GO

