CREATE TABLE [dbo].[TBLOrderList] (
    [OrderID]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [mUserNo]           INT           NOT NULL,
    [mNo]               INT           NOT NULL,
    [mNm]               NVARCHAR (12) NOT NULL,
    [IDCID]             SMALLINT      NOT NULL,
    [mSrvNo]            SMALLINT      NOT NULL,
    [GoldItemID]        BIGINT        NOT NULL,
    [Count]             INT           NOT NULL,
    [UseGold]           INT           CONSTRAINT [DF_TBLOrderList_UseGold] DEFAULT (0) NOT NULL,
    [RemainGold]        INT           CONSTRAINT [DF_TBLOrderList_RemainGold] DEFAULT (0) NOT NULL,
    [OrderDate]         DATETIME      CONSTRAINT [DF_TBLOrderList_OrderDate] DEFAULT (getdate()) NULL,
    [OrderStatus]       NCHAR (1)     CONSTRAINT [DF_TBLOrderList_OrderStatus] DEFAULT ('1') NULL,
    [OrderIP]           NVARCHAR (19) NULL,
    [BillingOrderID]    NVARCHAR (20) NULL,
    [BillingReturnCode] INT           NULL,
    [mClass]            TINYINT       CONSTRAINT [DF_TBLOrderList_mClass] DEFAULT (0) NOT NULL,
    [mLevel]            SMALLINT      CONSTRAINT [DF_TBLOrderList_mLevel] DEFAULT (0) NOT NULL,
    [mPcbangLv]         SMALLINT      CONSTRAINT [DF_TBLOrderList_mPcbangLv] DEFAULT (0) NOT NULL,
    [mOrderType]        TINYINT       CONSTRAINT [DF_TBLOrderList_mOrderType] DEFAULT (3) NOT NULL,
    CONSTRAINT [UNC_PKTBLOrderList_1] PRIMARY KEY NONCLUSTERED ([OrderID] DESC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLOrderList_mOrderType] CHECK ([mOrderType] = 3 or ([mOrderType] = 2 or ([mOrderType] = 1 or [mOrderType] = 0))),
    CONSTRAINT [CK_TBLOrderList_OrderStatus] CHECK ([OrderStatus] = N'3' or ([OrderStatus] = N'0' or ([OrderStatus] = N'2' or [OrderStatus] = N'1')))
);


GO

CREATE UNIQUE CLUSTERED INDEX [UCL_TBLOrderList]
    ON [dbo].[TBLOrderList]([mUserNo] ASC, [OrderStatus] ASC, [mOrderType] ASC, [mSrvNo] ASC, [OrderID] DESC) WITH (FILLFACTOR = 90);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TBLOrderList_2]
    ON [dbo].[TBLOrderList]([mUserNo] ASC, [OrderStatus] ASC, [mOrderType] ASC, [mSrvNo] ASC, [OrderID] DESC) WITH (FILLFACTOR = 90);


GO

