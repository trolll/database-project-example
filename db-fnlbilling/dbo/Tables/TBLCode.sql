CREATE TABLE [dbo].[TBLCode] (
    [CodeType] SMALLINT       NOT NULL,
    [Code]     INT            NOT NULL,
    [CodeDesc] NVARCHAR (100) NOT NULL,
    CONSTRAINT [CL_PKTBLCode] PRIMARY KEY CLUSTERED ([CodeType] ASC, [Code] ASC) WITH (FILLFACTOR = 90)
);


GO

