CREATE TABLE [dbo].[TBL_BIP_LOG] (
    [SVRID]      SMALLINT      NULL,
    [GAMEID]     VARCHAR (10)  NULL,
    [USERID]     VARCHAR (32)  NULL,
    [CHARID]     VARCHAR (20)  NULL,
    [ACCESSIP]   VARCHAR (15)  NULL,
    [PCBANG]     CHAR (1)      NULL,
    [JOINCODE]   CHAR (1)      NULL,
    [LOGINDATE]  SMALLDATETIME NULL,
    [LOGOUTDATE] SMALLDATETIME NULL,
    [LOGINLV]    SMALLINT      NULL,
    [LOGOUTLV]   SMALLINT      NULL,
    [PLAYTIME]   INT           NULL,
    [NEWIDYN]    BIT           CONSTRAINT [DF__TBL_BIP_L__NEWID__7B9B496D] DEFAULT (0) NULL
);


GO

