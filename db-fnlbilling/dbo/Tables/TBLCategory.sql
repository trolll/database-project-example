CREATE TABLE [dbo].[TBLCategory] (
    [CategoryID]   SMALLINT      NOT NULL,
    [CategoryName] NVARCHAR (30) NOT NULL,
    [CategoryDesc] NVARCHAR (50) NULL,
    [Status]       NCHAR (1)     NOT NULL,
    [RegistDate]   DATETIME      CONSTRAINT [DF_TBLCategory_RegistDate] DEFAULT (getdate()) NOT NULL,
    [RegistAdmin]  NVARCHAR (20) NULL,
    [RegistIP]     NVARCHAR (19) NULL,
    [UpdateDate]   DATETIME      NULL,
    [UpdateAdmin]  NVARCHAR (20) NULL,
    [UpdateIP]     NVARCHAR (19) NULL,
    CONSTRAINT [CL_PKTBLCategory] PRIMARY KEY CLUSTERED ([CategoryID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLCategory_CategoryID] CHECK ([CategoryID] >= 1 and [CategoryID] <= 4),
    CONSTRAINT [CK_TBLCategory_Status] CHECK ([Status] = N'2' or ([Status] = N'1' or [Status] = N'0'))
);


GO

