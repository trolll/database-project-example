CREATE TABLE [dbo].[R2_COUPON_ITEM_LIST] (
    [MASTER_SEQ]      INT            NOT NULL,
    [ITEM_NO]         INT            NOT NULL,
    [ITEM_CNT]        INT            NOT NULL,
    [REG_DATE]        SMALLDATETIME  CONSTRAINT [DF_R2_COUPON_ITEM_LIST_REG_DATE] DEFAULT (getdate()) NOT NULL,
    [COND_TYPE]       INT            NULL,
    [COND_VALUE]      NVARCHAR (100) NULL,
    [ORDER_NUM]       SMALLINT       NULL,
    [AvailablePeriod] INT            CONSTRAINT [CK_R2_COUPON_ITEM_LIST_AvailablePeriod] DEFAULT (0) NOT NULL,
    [PracticalPeriod] INT            CONSTRAINT [DF_R2_COUPON_ITEM_LIST_PracticalPeriod] DEFAULT (0) NOT NULL,
    [mStatus]         TINYINT        CONSTRAINT [DF_R2_COUPON_ITEM_LIST_mStatus] DEFAULT (1) NOT NULL,
    [mBindingType]    TINYINT        CONSTRAINT [DF_R2_COUPON_ITEM_LIST_mBindingType] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_R2_COUPON_ITEM_LIST] PRIMARY KEY CLUSTERED ([MASTER_SEQ] ASC, [ITEM_NO] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_R2_COUPON_ITEM_LIST_mBindingType] CHECK ([mBindingType] = 2 or ([mBindingType] = 1 or [mBindingType] = 0)),
    CONSTRAINT [CK_R2_COUPON_ITEM_LIST_mStatus] CHECK ([mStatus] = 2 or ([mStatus] = 1 or [mStatus] = 0))
);


GO

