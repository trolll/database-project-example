CREATE TABLE [dbo].[TBLGoldItem] (
    [GoldItemID]        BIGINT         NOT NULL,
    [IID]               INT            NOT NULL,
    [ItemName]          NVARCHAR (40)  NOT NULL,
    [ItemImage]         NVARCHAR (100) NULL,
    [ItemDesc]          NVARCHAR (500) NOT NULL,
    [OriginalGoldPrice] INT            NOT NULL,
    [GoldPrice]         INT            NOT NULL,
    [ItemCategory]      NCHAR (1)      NOT NULL,
    [IsPackage]         NCHAR (1)      NOT NULL,
    [Status]            NCHAR (1)      NOT NULL,
    [AvailablePeriod]   INT            CONSTRAINT [DF_TBLGoldItem_AvailablePeriod] DEFAULT (0) NOT NULL,
    [Count]             INT            CONSTRAINT [DF_TBLGoldItem_Count] DEFAULT (1) NOT NULL,
    [PracticalPeriod]   INT            CONSTRAINT [DF_TBLGoldItem_PracticalPeriod] DEFAULT (0) NOT NULL,
    [RegistDate]        DATETIME       CONSTRAINT [DF_TBLGoldItem_RegistDate] DEFAULT (getdate()) NOT NULL,
    [RegistAdmin]       NVARCHAR (20)  NULL,
    [RegistIP]          NVARCHAR (19)  NULL,
    [UpdateDate]        DATETIME       NULL,
    [UpdateAdmin]       NVARCHAR (20)  NULL,
    [UpdateIP]          NVARCHAR (19)  NULL,
    CONSTRAINT [CL_PKTBLGoldItem] PRIMARY KEY CLUSTERED ([GoldItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLGoldItem_DiffPrice] CHECK ([OriginalGoldPrice] >= [GoldPrice]),
    CONSTRAINT [CK_TBLGoldItem_GoldPrice] CHECK ([GoldPrice] >= 0),
    CONSTRAINT [CK_TBLGoldItem_IsPackage] CHECK ([IsPackage] = N'1' or [IsPackage] = N'0'),
    CONSTRAINT [CK_TBLGoldItem_ItemCategory] CHECK ([ItemCategory] >= N'1' and [ItemCategory] <= N'F'),
    CONSTRAINT [CK_TBLGoldItem_OriginalGoldPrice] CHECK ([OriginalGoldPrice] >= 0),
    CONSTRAINT [CK_TBLGoldItem_Status] CHECK ([Status] = N'2' or ([Status] = N'1' or [Status] = N'0'))
);


GO

CREATE NONCLUSTERED INDEX [NC_TBLGoldItem_1]
    ON [dbo].[TBLGoldItem]([IID] ASC) WITH (FILLFACTOR = 90);


GO

