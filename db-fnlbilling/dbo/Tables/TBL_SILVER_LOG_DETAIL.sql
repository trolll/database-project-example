CREATE TABLE [dbo].[TBL_SILVER_LOG_DETAIL] (
    [mRegDate] DATETIME NULL,
    [mPos]     TINYINT  NULL,
    [mOwner]   INT      NULL,
    [mCnt]     BIGINT   NULL,
    [mDiffCnt] BIGINT   NULL
);


GO

