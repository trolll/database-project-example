CREATE TABLE [dbo].[TBLWebOrderList] (
    [mRegDate]        SMALLDATETIME CONSTRAINT [DF_TBLWebOrderList_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mWebOrderID]     VARCHAR (20)  NOT NULL,
    [mBillingOrderID] VARCHAR (20)  NULL,
    [mUserNo]         INT           NOT NULL,
    [mSvrNo]          SMALLINT      NOT NULL,
    [mNo]             INT           NOT NULL,
    [mNm]             VARCHAR (12)  NOT NULL,
    [GoldItemID]      BIGINT        NOT NULL,
    [Count]           INT           CONSTRAINT [DF_TBLWebOrderList_Count] DEFAULT (0) NOT NULL,
    [UseGold]         INT           NOT NULL,
    [mOrderType]      TINYINT       CONSTRAINT [DF_TBLWebOrderList_mOrderType] DEFAULT (1) NOT NULL,
    [mStatus]         TINYINT       CONSTRAINT [DF_TBLWebOrderList_mStatus] DEFAULT (0) NOT NULL,
    [mReceiptDate]    SMALLDATETIME NULL,
    [mReceiptOrderID] BIGINT        NULL,
    [mReceiptPcNo]    INT           NULL,
    [mRecepitPcNm]    VARCHAR (12)  NULL,
    CONSTRAINT [UNC_PK_TBLWebOrderList] PRIMARY KEY NONCLUSTERED ([mWebOrderID] DESC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLWebOrderList_mOrderType] CHECK ([mOrderType] = 2 or [mOrderType] = 1),
    CONSTRAINT [CK_TBLWebOrderList_mStatus] CHECK ([mStatus] = 9 or ([mStatus] = 4 or ([mStatus] = 3 or ([mStatus] = 2 or ([mStatus] = 1 or [mStatus] = 0))))),
    CONSTRAINT [FK_TBLWebOrderList_TBLGoldItem] FOREIGN KEY ([GoldItemID]) REFERENCES [dbo].[TBLGoldItem] ([GoldItemID])
);


GO

CREATE CLUSTERED INDEX [CL_TBLWebOrderList_1]
    ON [dbo].[TBLWebOrderList]([mUserNo] ASC, [mSvrNo] ASC, [mStatus] ASC) WITH (FILLFACTOR = 90);


GO

CREATE NONCLUSTERED INDEX [NC_TBLWebOrderList_2]
    ON [dbo].[TBLWebOrderList]([mReceiptOrderID] DESC) WITH (FILLFACTOR = 90);


GO

