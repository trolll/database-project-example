CREATE TABLE [dbo].[T_TBLOrderList] (
    [OrderID]           BIGINT        IDENTITY (1, 1) NOT NULL,
    [mUserNo]           INT           NOT NULL,
    [mNo]               INT           NOT NULL,
    [mNm]               NVARCHAR (12) NOT NULL,
    [IDCID]             SMALLINT      NOT NULL,
    [mSrvNo]            SMALLINT      NOT NULL,
    [GoldItemID]        BIGINT        NOT NULL,
    [Count]             INT           NOT NULL,
    [UseGold]           INT           NOT NULL,
    [RemainGold]        INT           NOT NULL,
    [OrderDate]         DATETIME      NULL,
    [OrderStatus]       NCHAR (1)     NULL,
    [OrderIP]           NVARCHAR (19) NULL,
    [BillingOrderID]    NVARCHAR (20) NULL,
    [BillingReturnCode] INT           NULL,
    [mClass]            TINYINT       NOT NULL,
    [mLevel]            SMALLINT      NOT NULL,
    [mPcbangLv]         SMALLINT      NOT NULL,
    [mOrderType]        TINYINT       NOT NULL
);


GO

CREATE CLUSTERED INDEX [UCL_T_TBLOrderList]
    ON [dbo].[T_TBLOrderList]([mUserNo] ASC, [OrderStatus] ASC, [mSrvNo] ASC, [OrderID] DESC);


GO

CREATE NONCLUSTERED INDEX [UNC_PKT_TBLOrderList_1]
    ON [dbo].[T_TBLOrderList]([OrderID] DESC);


GO

CREATE NONCLUSTERED INDEX [UNC_T_TBLOrderList_2]
    ON [dbo].[T_TBLOrderList]([mUserNo] ASC, [OrderStatus] ASC, [mSrvNo] ASC, [OrderID] DESC);


GO

