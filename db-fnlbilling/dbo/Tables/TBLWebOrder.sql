CREATE TABLE [dbo].[TBLWebOrder] (
    [mWebOrderID] VARCHAR (20)  NOT NULL,
    [mUserNo]     INT           NOT NULL,
    [mSvrNo]      SMALLINT      NOT NULL,
    [mNo]         INT           NOT NULL,
    [mNm]         VARCHAR (12)  NOT NULL,
    [mGiftMsg]    VARCHAR (200) NULL,
    CONSTRAINT [UCL_PK_TBLWebOrder_1] PRIMARY KEY CLUSTERED ([mWebOrderID] DESC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_TBLWebOrder_TBLWebOrderList] FOREIGN KEY ([mWebOrderID]) REFERENCES [dbo].[TBLWebOrderList] ([mWebOrderID])
);


GO

