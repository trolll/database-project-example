CREATE TABLE [dbo].[R2_COUPON_MASTER] (
    [MASTER_SEQ]          INT            NOT NULL,
    [TITLE]               VARCHAR (100)  NOT NULL,
    [COUPONCNT]           INT            NULL,
    [RELATION_PRODUCT_NO] INT            NULL,
    [REG_LIMITDATE]       DATETIME       NULL,
    [USE_LIMITDATE]       DATETIME       NULL,
    [REGDATE]             DATETIME       NULL,
    [MEMO]                VARCHAR (1000) NULL,
    [STATUS]              CHAR (1)       NULL,
    [ONEJUMIN_MULTIID_YN] CHAR (1)       NULL,
    [BILLING_USER_USE_YN] CHAR (1)       NULL,
    [ITEM_GIVE_YN]        CHAR (1)       NULL,
    [ITEM_GIVE_LIMIT_YN]  CHAR (1)       CONSTRAINT [DF__R2_COUPON__ITEM___6477ECF3] DEFAULT ('Y') NOT NULL,
    [AUTH_CHECK_YN]       CHAR (1)       NULL,
    [COUPON_TYPE_NO]      INT            NULL,
    [REG_PERSON]          NVARCHAR (20)  NULL,
    [GAME_USE_CNT]        INT            NULL,
    [WEB_ACTIVE_CNT]      INT            NULL,
    CONSTRAINT [PK_R2_COUPON_MASTER] PRIMARY KEY CLUSTERED ([MASTER_SEQ] ASC) WITH (FILLFACTOR = 90)
);


GO

