CREATE TABLE [dbo].[TBLSysOrder] (
    [mRegDate]  SMALLDATETIME CONSTRAINT [DF_TBLSysOrder_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSysID]    BIGINT        IDENTITY (1, 1) NOT NULL,
    [mGmUserNo] INT           NOT NULL,
    [mGmCharNm] VARCHAR (12)  NOT NULL,
    [mMsg]      VARCHAR (200) NULL,
    CONSTRAINT [UCL_PK_TBLSysOrder] PRIMARY KEY CLUSTERED ([mSysID] DESC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [NC_TBLSysOrder_1]
    ON [dbo].[TBLSysOrder]([mGmUserNo] ASC) WITH (FILLFACTOR = 90);


GO

CREATE NONCLUSTERED INDEX [NC_TBLSysOrder_2]
    ON [dbo].[TBLSysOrder]([mRegDate] ASC) WITH (FILLFACTOR = 90);


GO

