CREATE TABLE [dbo].[TblSysEvent] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF_TblSysEvent_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSysID]   BIGINT        NOT NULL,
    [mUserNo]  INT           NOT NULL,
    CONSTRAINT [UCL_PK_TblSysEvent] PRIMARY KEY CLUSTERED ([mSysID] DESC, [mUserNo] ASC) WITH (FILLFACTOR = 90)
);


GO

