CREATE TABLE [dbo].[T_TBLSilver] (
    [mSvrNo]  SMALLINT      NOT NULL,
    [mUserno] INT           NOT NULL,
    [mNo]     INT           NOT NULL,
    [mNm]     NVARCHAR (12) NOT NULL,
    [mSilver] BIGINT        NOT NULL,
    [mType]   TINYINT       NOT NULL
);


GO

