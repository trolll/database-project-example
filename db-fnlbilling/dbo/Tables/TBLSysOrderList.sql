CREATE TABLE [dbo].[TBLSysOrderList] (
    [mRegDate]         SMALLDATETIME CONSTRAINT [DF_TBLSysOrderList_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSysOrderID]      BIGINT        IDENTITY (1, 1) NOT NULL,
    [mSysID]           BIGINT        NOT NULL,
    [mUserNo]          INT           NOT NULL,
    [mSvrNo]           SMALLINT      NOT NULL,
    [mItemID]          INT           NOT NULL,
    [mCnt]             INT           NOT NULL,
    [mAvailablePeriod] INT           CONSTRAINT [DF_TBLSysOrderList_mAvailablePeriod] DEFAULT (0) NOT NULL,
    [mPracticalPeriod] INT           CONSTRAINT [DF_TBLSysOrderList_PracticalPeriod] DEFAULT (0) NOT NULL,
    [mStatus]          TINYINT       CONSTRAINT [DF_TBLSysOrderList_mStatus] DEFAULT (0) NOT NULL,
    [mReceiptDate]     SMALLDATETIME NULL,
    [mReceiptPcNo]     INT           NULL,
    [mRecepitPcNm]     VARCHAR (12)  NULL,
    [mBindingType]     TINYINT       CONSTRAINT [DF_TBLSysOrderList_mBindingType] DEFAULT (0) NOT NULL,
    [mLimitedDate]     SMALLDATETIME CONSTRAINT [DF_TBLSysOrderList_mLimitedDate] DEFAULT ('2079-01-01') NOT NULL,
    [mItemStatus]      TINYINT       CONSTRAINT [DF_TBLSysOrderList_mItemStatus] DEFAULT (1) NOT NULL,
    [mRestoreCnt]      TINYINT       CONSTRAINT [DF_TBLSysOrderList_mRestoreCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UNC_PK_TBLSysOrderList_1] PRIMARY KEY NONCLUSTERED ([mSysOrderID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLSysOrderList_mBindingType] CHECK ([mBindingType] = 2 or ([mBindingType] = 1 or [mBindingType] = 0)),
    CONSTRAINT [CK_TBLSysOrderList_mItemStatus] CHECK ([mItemStatus] = 2 or ([mItemStatus] = 1 or [mItemStatus] = 0)),
    CONSTRAINT [FK_TBLSysOrderList_TBLSysOrder] FOREIGN KEY ([mSysID]) REFERENCES [dbo].[TBLSysOrder] ([mSysID])
);


GO

CREATE NONCLUSTERED INDEX [NC_TBLSysOrderList_3]
    ON [dbo].[TBLSysOrderList]([mItemID] ASC) WITH (FILLFACTOR = 90);


GO

CREATE NONCLUSTERED INDEX [NC_TBLSysOrderList_2]
    ON [dbo].[TBLSysOrderList]([mSysID] ASC) WITH (FILLFACTOR = 90);


GO

CREATE CLUSTERED INDEX [CL_TBLSysOrderList]
    ON [dbo].[TBLSysOrderList]([mUserNo] ASC, [mSvrNo] ASC, [mStatus] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'楷厘冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TBLSysOrderList', @level2type = N'COLUMN', @level2name = N'mRestoreCnt';


GO

