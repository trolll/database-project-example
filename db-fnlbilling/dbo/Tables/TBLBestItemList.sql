CREATE TABLE [dbo].[TBLBestItemList] (
    [ItemSeq]     BIGINT        NOT NULL,
    [GoldItemID]  BIGINT        NOT NULL,
    [Status]      NCHAR (1)     NOT NULL,
    [OrderNO]     SMALLINT      NOT NULL,
    [RegistDate]  DATETIME      CONSTRAINT [DF_TBLBestItemList_RegistDate] DEFAULT (getdate()) NOT NULL,
    [RegistAdmin] NVARCHAR (20) NULL,
    [RegistIP]    NVARCHAR (19) NULL,
    [UpdateDate]  DATETIME      NULL,
    [UpdateAdmin] NVARCHAR (20) NULL,
    [UpdateIP]    NVARCHAR (19) NULL,
    CONSTRAINT [CL_TBLBestItemList] PRIMARY KEY CLUSTERED ([GoldItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLBestItemList_Status] CHECK ([Status] = N'2' or ([Status] = N'1' or [Status] = N'0'))
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [NC_TBLBestItemList]
    ON [dbo].[TBLBestItemList]([ItemSeq] DESC) WITH (FILLFACTOR = 90);


GO

