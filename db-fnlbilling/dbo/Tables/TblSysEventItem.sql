CREATE TABLE [dbo].[TblSysEventItem] (
    [mSysID]           BIGINT  NOT NULL,
    [mItemID]          INT     NOT NULL,
    [mCnt]             INT     NOT NULL,
    [mAvailablePeriod] INT     CONSTRAINT [DF_TblSysEventItem_mAvailablePeriod] DEFAULT (0) NOT NULL,
    [mPracticalPeriod] INT     CONSTRAINT [DF_TblSysEventItem_PracticalPeriod] DEFAULT (0) NOT NULL,
    [mStatus]          TINYINT CONSTRAINT [DF_TblSysEventItem_mStatus] DEFAULT (0) NOT NULL,
    [mBindingType]     TINYINT CONSTRAINT [DF_TblSysEventItem_mBindingType] DEFAULT (0) NOT NULL,
    [mItemStatus]      TINYINT CONSTRAINT [DF_TblSysEventItem_mItemStatus] DEFAULT (1) NOT NULL
);


GO

CREATE UNIQUE CLUSTERED INDEX [CL_TblSysEventItem]
    ON [dbo].[TblSysEventItem]([mSysID] ASC, [mItemID] ASC) WITH (FILLFACTOR = 90);


GO

