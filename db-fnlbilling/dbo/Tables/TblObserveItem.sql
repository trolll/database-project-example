CREATE TABLE [dbo].[TblObserveItem] (
    [mItemNo] INT    NOT NULL,
    [mCnt]    BIGINT NOT NULL,
    PRIMARY KEY CLUSTERED ([mItemNo] ASC) WITH (FILLFACTOR = 90)
);


GO

