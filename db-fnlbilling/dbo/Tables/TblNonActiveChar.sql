CREATE TABLE [dbo].[TblNonActiveChar] (
    [mNo] INT          NOT NULL,
    [mNm] VARCHAR (12) NOT NULL,
    CONSTRAINT [PK_CL_TblNonActiveChar] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

