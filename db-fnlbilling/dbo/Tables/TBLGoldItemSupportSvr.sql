CREATE TABLE [dbo].[TBLGoldItemSupportSvr] (
    [GoldItemID] BIGINT   NOT NULL,
    [mSvrNo]     SMALLINT NOT NULL,
    CONSTRAINT [UNC_PK_TBLGoldItemSupportSvr_1] PRIMARY KEY NONCLUSTERED ([GoldItemID] ASC, [mSvrNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_TBLGoldItemSupportSvr_TBLGoldItem] FOREIGN KEY ([GoldItemID]) REFERENCES [dbo].[TBLGoldItem] ([GoldItemID]) ON DELETE CASCADE
);


GO

CREATE NONCLUSTERED INDEX [NC_TBLGoldItemSupportSvr_2]
    ON [dbo].[TBLGoldItemSupportSvr]([GoldItemID] ASC) WITH (FILLFACTOR = 90);


GO

CREATE CLUSTERED INDEX [CL_TBLGoldItemSupportSvr]
    ON [dbo].[TBLGoldItemSupportSvr]([mSvrNo] ASC) WITH (FILLFACTOR = 90);


GO

