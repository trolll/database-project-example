CREATE TABLE [dbo].[TBLPCCafeItemList] (
    [ItemSeq]     BIGINT        NOT NULL,
    [GoldItemID]  BIGINT        NOT NULL,
    [Status]      NCHAR (1)     NOT NULL,
    [OrderNO]     SMALLINT      NOT NULL,
    [RegistDate]  DATETIME      CONSTRAINT [DF_TBLPCCafeItemList_RegistDate] DEFAULT (getdate()) NOT NULL,
    [RegistAdmin] NVARCHAR (20) NULL,
    [RegistIP]    NVARCHAR (19) NULL,
    [UpdateDate]  DATETIME      NULL,
    [UpdateAdmin] NVARCHAR (20) NULL,
    [UpdateIP]    NVARCHAR (19) NULL,
    CONSTRAINT [CL_PKTBLPCCafeItemList] PRIMARY KEY CLUSTERED ([GoldItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLPCCafeItemList_Status] CHECK ([Status] = N'2' or ([Status] = N'1' or [Status] = N'0'))
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [NC_TBLPCCafeItemList]
    ON [dbo].[TBLPCCafeItemList]([ItemSeq] ASC) WITH (FILLFACTOR = 90);


GO

