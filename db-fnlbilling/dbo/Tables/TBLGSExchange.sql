CREATE TABLE [dbo].[TBLGSExchange] (
    [ExchangeID]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [RmUserNo]            INT           NOT NULL,
    [RmNo]                INT           NOT NULL,
    [RmNm]                NVARCHAR (12) NOT NULL,
    [BmUserNo]            INT           NULL,
    [BmNo]                INT           NULL,
    [BmNm]                NVARCHAR (12) NULL,
    [IDCID]               SMALLINT      NOT NULL,
    [mSvrNo]              SMALLINT      NOT NULL,
    [ExchangeType]        CHAR (1)      NOT NULL,
    [RegDate]             DATETIME      CONSTRAINT [DF_TBLGSExchange_RegDate] DEFAULT (getdate()) NOT NULL,
    [ExchangeDate]        DATETIME      NULL,
    [Status]              CHAR (1)      CONSTRAINT [DF_TBLGSExchange_Status] DEFAULT ('1') NOT NULL,
    [ExchangeGold]        INT           NOT NULL,
    [ExchangeSilver]      INT           NOT NULL,
    [Commission]          INT           NOT NULL,
    [RmBillingExchangeID] NVARCHAR (20) NULL,
    [BmBillingExchangeID] NVARCHAR (20) NULL,
    [BillingReturnCode]   INT           NULL,
    [ErrorCode]           BIGINT        CONSTRAINT [DF_TBLGSExchange_ErrorCode] DEFAULT (0) NOT NULL,
    [ErrorStep]           SMALLINT      CONSTRAINT [DF_TBLGSExchange_ErrorStep] DEFAULT (0) NOT NULL,
    CONSTRAINT [NC_PKTBLGSExchange_1] PRIMARY KEY NONCLUSTERED ([ExchangeID] DESC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLGSExchange_Commission] CHECK ([Commission] >= 0),
    CONSTRAINT [CK_TBLGSExchange_ExchangeGold] CHECK ([ExchangeGold] > 0),
    CONSTRAINT [CK_TBLGSExchange_ExchangeSilver] CHECK ([ExchangeSilver] > 0),
    CONSTRAINT [CK_TBLGSExchange_ExchangeType] CHECK ([ExchangeType] >= '0' and [ExchangeType] <= '1'),
    CONSTRAINT [CK_TBLGSExchange_Status] CHECK ([Status] >= '0' and [Status] <= '6')
);


GO

CREATE NONCLUSTERED INDEX [NC_TBLGSExchange_2]
    ON [dbo].[TBLGSExchange]([RmUserNo] DESC, [mSvrNo] DESC, [RegDate] DESC, [Status] DESC) WITH (FILLFACTOR = 90);


GO

CREATE CLUSTERED INDEX [CL_TBLGSExchange]
    ON [dbo].[TBLGSExchange]([mSvrNo] DESC, [ExchangeType] DESC, [RegDate] DESC) WITH (FILLFACTOR = 90);


GO

