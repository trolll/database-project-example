CREATE TABLE [dbo].[TBLNewItemList] (
    [ItemSeq]     BIGINT        NOT NULL,
    [GoldItemID]  BIGINT        NOT NULL,
    [OrderNO]     SMALLINT      NULL,
    [Status]      NCHAR (1)     NOT NULL,
    [RegistDate]  DATETIME      CONSTRAINT [DF_TBLNewItemList_RegistDate] DEFAULT (getdate()) NOT NULL,
    [RegistAdmin] NVARCHAR (20) NULL,
    [RegistIP]    NVARCHAR (19) NULL,
    [UpdateDate]  DATETIME      NULL,
    [UpdateAdmin] NVARCHAR (20) NULL,
    [UpdateIP]    NVARCHAR (19) NULL,
    CONSTRAINT [CL_TBLNewItemList] PRIMARY KEY CLUSTERED ([GoldItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLNewItemList_Status] CHECK ([Status] = N'2' or ([Status] = N'1' or [Status] = N'0'))
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [NC_TBLNewItemList]
    ON [dbo].[TBLNewItemList]([ItemSeq] ASC) WITH (FILLFACTOR = 90);


GO

