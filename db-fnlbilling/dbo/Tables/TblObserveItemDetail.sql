CREATE TABLE [dbo].[TblObserveItemDetail] (
    [mItemNo]   INT     NOT NULL,
    [mStatus]   TINYINT NOT NULL,
    [mEquipPos] TINYINT NOT NULL,
    [mCnt]      BIGINT  NOT NULL,
    CONSTRAINT [UCL_PKTblObserveItemDetail] PRIMARY KEY CLUSTERED ([mItemNo] ASC, [mStatus] ASC, [mEquipPos] ASC) WITH (FILLFACTOR = 90)
);


GO

