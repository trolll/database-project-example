CREATE TABLE [dbo].[T_TBLGSExchange] (
    [ExchangeID]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [RmUserNo]            INT           NOT NULL,
    [RmNo]                INT           NOT NULL,
    [RmNm]                NVARCHAR (12) NOT NULL,
    [BmUserNo]            INT           NULL,
    [BmNo]                INT           NULL,
    [BmNm]                NVARCHAR (12) NULL,
    [IDCID]               SMALLINT      NOT NULL,
    [mSvrNo]              SMALLINT      NOT NULL,
    [ExchangeType]        CHAR (1)      NOT NULL,
    [RegDate]             DATETIME      NOT NULL,
    [ExchangeDate]        DATETIME      NULL,
    [Status]              CHAR (1)      NOT NULL,
    [ExchangeGold]        INT           NOT NULL,
    [ExchangeSilver]      INT           NOT NULL,
    [Commission]          INT           NOT NULL,
    [RmBillingExchangeID] NVARCHAR (20) NULL,
    [BmBillingExchangeID] NVARCHAR (20) NULL,
    [BillingReturnCode]   INT           NULL,
    [ErrorCode]           BIGINT        NOT NULL,
    [ErrorStep]           SMALLINT      NOT NULL
);


GO

