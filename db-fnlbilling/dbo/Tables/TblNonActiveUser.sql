CREATE TABLE [dbo].[TblNonActiveUser] (
    [mUserNo] INT          NOT NULL,
    [mUserID] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_CL_TblNonActiveUser] PRIMARY KEY CLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 90)
);


GO

