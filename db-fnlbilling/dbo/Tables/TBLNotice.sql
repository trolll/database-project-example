CREATE TABLE [dbo].[TBLNotice] (
    [NoticeSeq]   BIGINT          NOT NULL,
    [Title]       NVARCHAR (50)   NOT NULL,
    [Notice]      NVARCHAR (3000) NOT NULL,
    [Status]      NCHAR (1)       NOT NULL,
    [RegistDate]  DATETIME        CONSTRAINT [DF_TBLNotice_RegistDate] DEFAULT (getdate()) NOT NULL,
    [RegistAdmin] NVARCHAR (20)   NULL,
    [RegistIP]    NVARCHAR (19)   NULL,
    [UpdateDate]  DATETIME        NULL,
    [UpdateAdmin] NVARCHAR (20)   NULL,
    [UpdateIP]    NVARCHAR (19)   NULL,
    CONSTRAINT [CL_PKTBLNotice] PRIMARY KEY CLUSTERED ([NoticeSeq] DESC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TBLNotice_Status] CHECK ([Status] = N'3' or ([Status] = N'2' or [Status] = N'1'))
);


GO

