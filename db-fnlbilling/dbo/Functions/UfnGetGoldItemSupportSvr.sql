
CREATE FUNCTION DBO.UfnGetGoldItemSupportSvr
  (@pGoldItem BIGINT) 
RETURNS VARCHAR(8000)
AS
BEGIN
	DECLARE @aStrSupportSvr VARCHAR(8000)
	SET @aStrSupportSvr = ''

	SELECT @aStrSupportSvr= convert(varchar, mSvrNo) + ',' + @aStrSupportSvr
	FROM dbo.TBLGoldItemSupportSvr
	WHERE GoldItemID = @pGoldItem;

    RETURN @aStrSupportSvr;
END

GO

