CREATE TABLE [dbo].[TblPcInventoryOpHistory] (
    [mRegDate]         DATETIME      DEFAULT (getdate()) NOT NULL,
    [gmId]             CHAR (20)     NOT NULL,
    [mItemNo]          INT           NOT NULL,
    [mCnt]             INT           NOT NULL,
    [mStatus]          TINYINT       NOT NULL,
    [mIsConfirm]       BIT           NOT NULL,
    [mPracticalPeriod] INT           DEFAULT (0) NOT NULL,
    [mPcNo]            INT           NOT NULL,
    [mUserId]          CHAR (20)     NOT NULL,
    [gmIp]             CHAR (15)     NOT NULL,
    [mSerialNo]        BIGINT        NOT NULL,
    [mOp]              CHAR (3)      NOT NULL,
    [mEndDate]         SMALLDATETIME DEFAULT (getdate()) NOT NULL
);


GO

