CREATE TABLE [dbo].[R2_ADMIN_LOG] (
    [ID]         INT            IDENTITY (1, 1) NOT NULL,
    [USERID]     VARCHAR (7)    NOT NULL,
    [REGDATE]    VARCHAR (14)   NOT NULL,
    [PROGID]     VARCHAR (100)  NOT NULL,
    [SERVERID]   VARCHAR (5)    NOT NULL,
    [PCNAME]     VARCHAR (100)  NOT NULL,
    [PROGNAME]   VARCHAR (50)   NULL,
    [USERNAME]   NVARCHAR (50)  NULL,
    [SERVERNAME] VARCHAR (20)   NULL,
    [PARAM]      VARCHAR (4000) NULL,
    CONSTRAINT [PK_R2_ADMIN_LOG] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [IX_R2_ADMIN_LOG_2]
    ON [dbo].[R2_ADMIN_LOG]([REGDATE] ASC, [SERVERID] ASC, [PCNAME] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [IX_R2_ADMIN_LOG_1]
    ON [dbo].[R2_ADMIN_LOG]([PROGID] ASC, [REGDATE] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [IX_R2_ADMIN_LOG]
    ON [dbo].[R2_ADMIN_LOG]([USERID] ASC, [REGDATE] ASC) WITH (FILLFACTOR = 80);


GO

