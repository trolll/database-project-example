CREATE  PROCEDURE  dbo.WspListGmItem
	@mUserId CHAR(20) ,
	@gmId CHAR(20) ,
	@gmIp CHAR(15) ,
	@mItemNo INT ,
	@mSerialNo BIGINT ,
	--MODIFIED BY MENGDAN
	@mValiday INT ,
	@sdate DATETIME ,
	@edate DATETIME ,
	@method VARCHAR(10)
	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	IF @method = 'userid'
	BEGIN
		SELECT h.mRegDate,
		       h.gmId,
		       h.mItemNo,
		       h.mCnt,
		       h.mStatus,
		       h.mIsConfirm,
		       h.mPracticalPeriod,
		       h.mUserId,
		       h.gmIp,
		       h.mSerialNo,
		       h.mPcNo,
		       h.mOp,
		       i.IName,
		       --ADDED BY MENGDAN	
		       DATEDIFF(DAY, GETDATE(), h.mEndDate) intvalidday
		  FROM dbo.TblPcInventoryOpHistory h
		       inner join [FNLParm].[dbo].[dt_item] i
		  ON  h.mItemNo = i.IID
		 WHERE h.mUserId=@mUserId and h.mRegDate between @sdate and @edate
		 ORDER BY h.mRegDate DESC
	END
	
	IF @method = 'gmid'
	BEGIN
		SELECT h.mRegDate,
		       h.gmId,
		       h.mItemNo,
		       h.mCnt,
		       h.mStatus,
		       h.mIsConfirm,
		       h.mPracticalPeriod,
		       h.mUserId,
		       h.gmIp,
		       h.mSerialNo,
		       h.mPcNo,
		       h.mOp,
		       i.IName,
		       --ADDED BY MENGDAN	
		       DATEDIFF(DAY, GETDATE(), h.mEndDate) intvalidday
		  FROM dbo.TblPcInventoryOpHistory h
		       inner join [FNLParm].[dbo].[dt_item] i
		  ON  h.mItemNo = i.IID
		 WHERE h.gmId=@gmId and h.mRegDate between @sdate and @edate
		 ORDER BY h.mRegDate DESC
	END
	IF @method = 'ip'
	BEGIN
		SELECT h.mRegDate,
		       h.gmId,
		       h.mItemNo,
		       h.mCnt,
		       h.mStatus,
		       h.mIsConfirm,
		       h.mPracticalPeriod,
		       h.mUserId,
		       h.gmIp,
		       h.mSerialNo,
		       h.mPcNo,
		       h.mOp,
		       i.IName,
		       --ADDED BY MENGDAN	
		       DATEDIFF(DAY, GETDATE(), h.mEndDate) intvalidday
		  FROM dbo.TblPcInventoryOpHistory h
		       inner join [FNLParm].[dbo].[dt_item] i
		  ON  h.mItemNo = i.IID
		 WHERE h.gmIp=@gmIp and h.mRegDate between @sdate and @edate
		 ORDER BY h.mRegDate DESC
	END
	IF @method = 'itemtid'
	BEGIN
		SELECT h.mRegDate,
		       h.gmId,
		       h.mItemNo,
		       h.mCnt,
		       h.mStatus,
		       h.mIsConfirm,
		       h.mPracticalPeriod,
		       h.mUserId,
		       h.gmIp,
		       h.mSerialNo,
		       h.mPcNo,
		       h.mOp,
		       i.IName,
		       --ADDED BY MENGDAN	
		       DATEDIFF(DAY, GETDATE(), h.mEndDate) intvalidday
		  FROM dbo.TblPcInventoryOpHistory h
		       inner join [FNLParm].[dbo].[dt_item] i
		  ON  h.mItemNo = i.IID
		 WHERE h.mItemNo=@mItemNo and h.mRegDate between @sdate and @edate
		 ORDER BY h.mRegDate DESC
	END
	IF @method = 'serialno'
	BEGIN
		SELECT h.mRegDate,
		       h.gmId,
		       h.mItemNo,
		       h.mCnt,
		       h.mStatus,
		       h.mIsConfirm,
		       h.mPracticalPeriod,
		       h.mUserId,
		       h.gmIp,
		       h.mSerialNo,
		       h.mPcNo,
		       h.mOp,
		       i.IName,
		       --ADDED BY MENGDAN	
		       DATEDIFF(DAY, GETDATE(), h.mEndDate) intvalidday
		  FROM dbo.TblPcInventoryOpHistory h
		       inner join [FNLParm].[dbo].[dt_item] i
		  ON  h.mItemNo = i.IID
		 WHERE h.mSerialNo=@mSerialNo and h.mRegDate between @sdate and @edate
		 ORDER BY h.mRegDate DESC
	END
	IF @method = 'validay'
	BEGIN
		SELECT h.mRegDate,
		       h.gmId,
		       h.mItemNo,
		       h.mCnt,
		       h.mStatus,
		       h.mIsConfirm,
		       h.mPracticalPeriod,
		       h.mUserId,
		       h.gmIp,
		       h.mSerialNo,
		       h.mPcNo,
		       h.mOp,
		       i.IName,
		       --ADDED BY MENGDAN	
		       DATEDIFF(DAY, GETDATE(), h.mEndDate) intvalidday
		  FROM dbo.TblPcInventoryOpHistory h
		       inner join [FNLParm].[dbo].[dt_item] i
		  ON  h.mItemNo = i.IID
			--MODIFIED BY MENGDAN
		 WHERE DATEDIFF(DAY, GETDATE(), h.mEndDate)=@mValiday and h.mRegDate between @sdate and @edate
		 ORDER BY h.mRegDate DESC
	END

GO

