CREATE PROCEDURE  dbo.WspInsertGmItem
	@gmId CHAR(20) ,
	@mItemNo INT ,
	@mCnt INT ,
	@mStatus TINYINT ,
	@mIsConfirm BIT ,
	--MODIFIED BY MENGDAN
	@mEndDay SMALLDATETIME ,
	@mNo INT ,
	@userId CHAR(20) ,
	@gmIp CHAR(15) ,
	@serialNo BIGINT ,
	@op CHAR(3) ,
	@mReturnCode SMALLINT OUTPUT 
AS
	SET NOCOUNT ON
	BEGIN
		BEGIN TRANSACTION
	 	INSERT INTO 
			TblPcInventoryOpHistory
			(mRegDate, 
			 gmId, 
			 mItemNo, 
			 mCnt, 
			 mStatus, 
			 mIsConfirm,
			 mPracticalPeriod,
			 mPcNo,
			 mUserId,
			 gmIp,
			 mSerialNo,
			 mOp,
			 mEndDate) 
	 	VALUES 
			( GETDATE(), 
			 @gmId,
			 @mItemNo,
			 @mCnt,
			 @mStatus,
			 @mIsConfirm,
			 0,
			 @mNo,
			 @userId,
			 @gmIp,
			 @serialNo,
			 @op,
			 @mEndDay)
		IF @@ERROR <> 0
			ROLLBACK TRANSACTION
		ELSE
			COMMIT TRANSACTION
		SET @mReturnCode = @@ERROR
	END

GO

