CREATE TABLE [dbo].[TblUser] (
    [mRegDate]             DATETIME  CONSTRAINT [DF__TblUser__mRegDate] DEFAULT (getdate()) NOT NULL,
    [mUserAuth]            TINYINT   CONSTRAINT [DF__TblUser__mUserAuth] DEFAULT (1) NOT NULL,
    [mUserNo]              INT       IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mUserId]              CHAR (20) NOT NULL,
    [mUserPswd]            CHAR (20) NOT NULL,
    [mCertifiedKey]        INT       CONSTRAINT [DF__TblUser__mCertifiedKey] DEFAULT (0) NULL,
    [mIp]                  CHAR (15) CONSTRAINT [DF__TblUser__mIp] DEFAULT ('') NOT NULL,
    [mLoginTm]             DATETIME  NULL,
    [mLogoutTm]            DATETIME  NULL,
    [mTotUseTm]            INT       CONSTRAINT [DF__TblUser__mTotUseTm] DEFAULT (0) NOT NULL,
    [mWorldNo]             SMALLINT  CONSTRAINT [DF__TblUser__mWorldNo] DEFAULT (0) NOT NULL,
    [mDelDate]             DATETIME  CONSTRAINT [DF__TblUser__DelDate] DEFAULT ('1900-01-01') NOT NULL,
    [mPcBangLv]            INT       CONSTRAINT [DF__TblUser__mPcBangLv] DEFAULT (1) NOT NULL,
    [mSecKeyTableUse]      TINYINT   CONSTRAINT [DF__TblUser__mSecKey] DEFAULT (0) NOT NULL,
    [mUseMacro]            SMALLINT  CONSTRAINT [DF_TblUser_mUseMacro] DEFAULT (0) NOT NULL,
    [mIpEX]                BIGINT    CONSTRAINT [DF_TblUser_mIpEX] DEFAULT (0) NOT NULL,
    [mJoinCode]            CHAR (1)  CONSTRAINT [DF_Tbluser_mJoinCode] DEFAULT ('N') NOT NULL,
    [mLoginChannelID]      CHAR (1)  CONSTRAINT [DF_Tbluser_mLoginChannelID] DEFAULT ('N') NOT NULL,
    [mTired]               CHAR (1)  CONSTRAINT [DF_Tbluser_mTired] DEFAULT ('N') NOT NULL,
    [mChnSID]              CHAR (33) CONSTRAINT [DF_Tbluser_mChnSID] DEFAULT ('') NOT NULL,
    [mNewId]               BIT       CONSTRAINT [DF_Tbluser_mNewId] DEFAULT (0) NOT NULL,
    [mLoginSvrType]        TINYINT   CONSTRAINT [DF_TblUser_mLoginSvrType] DEFAULT (0) NOT NULL,
    [mAccountGuid]         INT       CONSTRAINT [DF_TblUser_mAccountGuid] DEFAULT (0) NOT NULL,
    [mNormalLimitTime]     INT       CONSTRAINT [DF_TblUser_mNormalLimitTime] DEFAULT ((162000)) NOT NULL,
    [mPcBangLimitTime]     INT       CONSTRAINT [DF_TblUser_mPcBangLimitTime] DEFAULT ((54000)) NOT NULL,
    [mIsMovingToBattleSvr] BIT       CONSTRAINT [TblUser_mIsMovingToBattleSvr] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblUser] PRIMARY KEY CLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK__TblUser__mUserAuth] CHECK (0 <= [mUserAuth] and [mUserAuth] <= 9),
    CONSTRAINT [CK__TblUser__mUserId] CHECK (1 <= datalength(rtrim([mUserId]))),
    CONSTRAINT [CK__TblUser__mUserNo] CHECK (0 < [mUserNo]),
    CONSTRAINT [CK__TblUser__mUserPswd] CHECK (2 <= datalength(rtrim([mUserPswd])))
);


GO

CREATE NONCLUSTERED INDEX [NC_TblUser_mIp]
    ON [dbo].[TblUser]([mIp] ASC) WITH (FILLFACTOR = 80);


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblUserUserId]
    ON [dbo].[TblUser]([mUserId] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [IxTblUserWorldNo]
    ON [dbo].[TblUser]([mWorldNo] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [IxTblUsermIpEX]
    ON [dbo].[TblUser]([mIpEX] ASC, [mWorldNo] ASC, [mPcBangLv] ASC) WITH (FILLFACTOR = 80);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Æ¯È­¼­¹ö°ü·Ã ÀÏ¹Ý Á¦ÇÑ ½Ã°£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUser', @level2type = N'COLUMN', @level2name = N'mNormalLimitTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'硅撇辑滚肺 捞悼吝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUser', @level2type = N'COLUMN', @level2name = N'mIsMovingToBattleSvr';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Æ¯È­¼­¹ö°ü·Ã PC¹æ Á¦ÇÑ ½Ã°£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUser', @level2type = N'COLUMN', @level2name = N'mPcBangLimitTime';


GO

