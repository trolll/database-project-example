CREATE TABLE [dbo].[TblBeforeRegWebzenTransfer] (
    [mRegDate]      SMALLDATETIME CONSTRAINT [DF_TblBeforeRegWebzenTransfer_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mUserGuid]     INT           NOT NULL,
    [mUserNm]       VARCHAR (50)  NOT NULL,
    [mAccountGuid]  INT           NOT NULL,
    [mAccountID]    VARCHAR (16)  NOT NULL,
    [mHanAccountID] VARCHAR (20)  NOT NULL,
    [mIp]           VARCHAR (15)  NULL,
    CONSTRAINT [UCL_PK_TblBeforeRegWebzenTransfer_mAccountGuid] PRIMARY KEY CLUSTERED ([mAccountGuid] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblBeforeRegWebzenTransfer_mAccountID]
    ON [dbo].[TblBeforeRegWebzenTransfer]([mAccountID] ASC) WITH (FILLFACTOR = 80);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblBeforeRegWebzenTransfer_mHanAccountID]
    ON [dbo].[TblBeforeRegWebzenTransfer]([mHanAccountID] ASC) WITH (FILLFACTOR = 80);


GO

