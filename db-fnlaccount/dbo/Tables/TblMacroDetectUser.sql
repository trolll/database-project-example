CREATE TABLE [dbo].[TblMacroDetectUser] (
    [mUserID]   VARCHAR (20)  NOT NULL,
    [mRegDate]  DATETIME      CONSTRAINT [DF_TblMacroDetectUser_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mMemo]     VARCHAR (100) NULL,
    [mAttachId] VARCHAR (20)  NOT NULL
);


GO

CREATE CLUSTERED INDEX [CL_TblMacroDetectUser_mRegDate]
    ON [dbo].[TblMacroDetectUser]([mRegDate] ASC) WITH (FILLFACTOR = 80);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblMacroDetectUser_mUserID]
    ON [dbo].[TblMacroDetectUser]([mUserID] ASC) WITH (FILLFACTOR = 80);


GO

