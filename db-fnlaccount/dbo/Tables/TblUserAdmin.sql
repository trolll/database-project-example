CREATE TABLE [dbo].[TblUserAdmin] (
    [mUserNo] INT       NOT NULL,
    [mUserId] CHAR (20) NOT NULL,
    CONSTRAINT [PkTblUserAdminUserNo] PRIMARY KEY CLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80)
);


GO

