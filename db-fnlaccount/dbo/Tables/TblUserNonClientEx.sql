CREATE TABLE [dbo].[TblUserNonClientEx] (
    [mRegDate]      SMALLDATETIME CONSTRAINT [DF_TblUserNonClientEx_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mUserNo]       INT           NOT NULL,
    [mBrokenMaxCnt] INT           NOT NULL,
    [mBrokenCnt]    INT           NOT NULL,
    [mLastUpdate]   SMALLDATETIME CONSTRAINT [DF_TblUserNonClientEx_mLastUpdate] DEFAULT (getdate()) NOT NULL,
    [mTotCnt]       BIGINT        CONSTRAINT [DF_TblUserNonClientEx_mTotCnt] DEFAULT (1) NOT NULL,
    CONSTRAINT [UNC_PK_TblUserNonClientEx_1] PRIMARY KEY NONCLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [Fk_TblUserNonClientEx_mUserNo] FOREIGN KEY ([mUserNo]) REFERENCES [dbo].[TblUser] ([mUserNo])
);


GO

CREATE CLUSTERED INDEX [CL_TblUserNonClientEx]
    ON [dbo].[TblUserNonClientEx]([mLastUpdate] DESC) WITH (FILLFACTOR = 80);


GO

