CREATE TABLE [dbo].[TblEventUser] (
    [mRegDate] DATETIME     CONSTRAINT [DF__TblEventU__mRegD__733B0D96] DEFAULT (getdate()) NOT NULL,
    [mUserID]  VARCHAR (20) NOT NULL,
    CONSTRAINT [NC_PK_TblEventUser] PRIMARY KEY NONCLUSTERED ([mUserID] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE CLUSTERED INDEX [CL_TblEventUser_mRegDate]
    ON [dbo].[TblEventUser]([mRegDate] ASC) WITH (FILLFACTOR = 80);


GO

