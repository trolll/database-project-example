CREATE TABLE [dbo].[TblUserBlockHistory] (
    [mRegDate]      DATETIME        CONSTRAINT [DF__TblUserBl__mRegD__3DE82FB7] DEFAULT (getdate()) NOT NULL,
    [mSeqNo]        BIGINT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mUserNo]       INT             NOT NULL,
    [mType]         INT             NOT NULL,
    [mEndDate]      DATETIME        NOT NULL,
    [mReasonCode]   SMALLINT        NOT NULL,
    [mReason]       CHAR (200)      NOT NULL,
    [mReasonFile]   CHAR (200)      NULL,
    [mAttachUserID] VARCHAR (50)    NOT NULL,
    [mDetachUserID] VARCHAR (50)    NULL,
    [mDetachReason] CHAR (200)      NULL,
    [mDeatchDate]   DATETIME        NULL,
    [mcomment]      NVARCHAR (1000) NULL,
    CONSTRAINT [UNC_PkTblUserBlockHistory_mSeqNo] PRIMARY KEY NONCLUSTERED ([mSeqNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE CLUSTERED INDEX [CL_TblUserBlockHistory_mUserNo]
    ON [dbo].[TblUserBlockHistory]([mUserNo] ASC, [mType] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [NC_TblUserBlockHistory_mEndDate]
    ON [dbo].[TblUserBlockHistory]([mEndDate] DESC, [mRegDate] DESC) WITH (FILLFACTOR = 80);


GO

