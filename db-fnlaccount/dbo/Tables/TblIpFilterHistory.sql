CREATE TABLE [dbo].[TblIpFilterHistory] (
    [mRegDate]     DATETIME     DEFAULT (getdate()) NOT NULL,
    [mIpStx]       BIGINT       NOT NULL,
    [mIpEtx]       BIGINT       NOT NULL,
    [mDesc]        CHAR (100)   NULL,
    [mOperator]    VARCHAR (30) NULL,
    [mDel_yn]      CHAR (1)     DEFAULT ('N') NOT NULL,
    [mDelDate]     DATETIME     NULL,
    [mDelOperator] VARCHAR (30) NULL
);


GO

