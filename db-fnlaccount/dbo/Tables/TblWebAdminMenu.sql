CREATE TABLE [dbo].[TblWebAdminMenu] (
    [mMenuNo]   INT           NOT NULL,
    [mPageName] VARCHAR (100) NOT NULL,
    [mMenuName] VARCHAR (100) NOT NULL,
    [mOrder]    SMALLINT      CONSTRAINT [DF_TblWebAdminMenu_mOrder] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_TblWebAdminMenu] PRIMARY KEY CLUSTERED ([mMenuNo] ASC) WITH (FILLFACTOR = 80)
);


GO

