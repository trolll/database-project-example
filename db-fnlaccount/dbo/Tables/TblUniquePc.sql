CREATE TABLE [dbo].[TblUniquePc] (
    [mRegdate] DATETIME  CONSTRAINT [DF_TblUniquePc_mRegdate] DEFAULT (getdate()) NOT NULL,
    [mNm]      CHAR (12) NOT NULL,
    [mSvrNo]   SMALLINT  NOT NULL,
    CONSTRAINT [UCL_TblUniquePc_mNm] PRIMARY KEY CLUSTERED ([mNm] ASC) WITH (FILLFACTOR = 80)
);


GO

