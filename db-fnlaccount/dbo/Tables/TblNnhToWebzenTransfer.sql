CREATE TABLE [dbo].[TblNnhToWebzenTransfer] (
    [mRegDate]       SMALLDATETIME CONSTRAINT [DF_TblNnhToWebzenTransfer_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mUserNo]        INT           NOT NULL,
    [mUserID]        VARCHAR (20)  NOT NULL,
    [mUserName]      VARCHAR (20)  NOT NULL,
    [mHanGameUserID] VARCHAR (20)  NOT NULL,
    [mIp]            VARCHAR (15)  NULL,
    CONSTRAINT [UCL_PK_TblNnhToWebzenTransfer_mUserNo] PRIMARY KEY CLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblNnhToWebzenTransfer_mUserID]
    ON [dbo].[TblNnhToWebzenTransfer]([mUserID] ASC) WITH (FILLFACTOR = 80);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblNnhToWebzenTransfer_mHanGameUserID]
    ON [dbo].[TblNnhToWebzenTransfer]([mHanGameUserID] ASC) WITH (FILLFACTOR = 80);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblNnhToWebzenTransfer_mUserName]
    ON [dbo].[TblNnhToWebzenTransfer]([mUserName] ASC) WITH (FILLFACTOR = 80);


GO

