CREATE TABLE [dbo].[X_TblRoleItemLog] (
    [id]               INT           IDENTITY (1, 1) NOT NULL,
    [before_mItemNo]   INT           NULL,
    [before_mSerialNo] INT           NULL,
    [srvNo]            INT           NULL,
    [mPcNo]            INT           NULL,
    [after_mItemNo]    INT           NULL,
    [after_mSerialNo]  INT           NULL,
    [created_at]       DATETIME      NULL,
    [updated_at]       DATETIME      NULL,
    [tablename]        NVARCHAR (50) NULL,
    [is_leg]           INT           NULL
);


GO

