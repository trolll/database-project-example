CREATE TABLE [dbo].[TblTemplate] (
    [mTemplateNo] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mTemplateId] SMALLINT       NULL,
    [mTitle]      VARCHAR (50)   NOT NULL,
    [mContent]    VARCHAR (1000) NOT NULL,
    [mUserId]     VARCHAR (50)   NULL,
    [mRegDate]    DATETIME       CONSTRAINT [DF_TblTemplate_mRegDate] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_TblTemplate] PRIMARY KEY CLUSTERED ([mTemplateNo] ASC) WITH (FILLFACTOR = 80)
);


GO

