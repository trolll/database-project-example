CREATE TABLE [dbo].[TblUserState] (
    [mUserNo]         INT NOT NULL,
    [mPShopOpeningTm] INT NULL,
    CONSTRAINT [UCL_TblUserState_mUserNo] PRIMARY KEY CLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80)
);


GO

