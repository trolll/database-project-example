CREATE TABLE [dbo].[Member] (
    [mUserId]   VARCHAR (50)  NOT NULL,
    [mUserPswd] VARCHAR (50)  NULL,
    [Superpwd]  VARCHAR (50)  NULL,
    [Cash]      INT           DEFAULT ((0)) NULL,
    [email]     VARCHAR (255) NULL,
    [tgzh]      VARCHAR (255) NULL,
    [uid]       INT           IDENTITY (1, 1) NOT NULL,
    [klq]       INT           DEFAULT ((0)) NULL,
    [ylq]       INT           DEFAULT ((0)) NULL,
    [auth]      INT           DEFAULT ((0)) NULL,
    [mSum]      VARCHAR (255) DEFAULT ((0)) NULL,
    [isadmin]   INT           DEFAULT ((0)) NULL,
    [isdl]      INT           DEFAULT ((0)) NULL,
    [dlmoney]   INT           DEFAULT ((0)) NULL
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [mmuserid]
    ON [dbo].[Member]([mUserId] ASC);


GO

