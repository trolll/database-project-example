CREATE TABLE [dbo].[TblCode] (
    [mCodeType] SMALLINT     NOT NULL,
    [mCode]     SMALLINT     NOT NULL,
    [mCodeDesc] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TblCode] PRIMARY KEY CLUSTERED ([mCodeType] DESC, [mCode] ASC) WITH (FILLFACTOR = 80)
);


GO

