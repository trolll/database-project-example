CREATE TABLE [dbo].[TblIpFilter] (
    [mRegDate] DATETIME   CONSTRAINT [DF_TblIpFilt_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mIpStx]   BIGINT     NOT NULL,
    [mIpEtx]   BIGINT     NOT NULL,
    [mDesc]    CHAR (100) NULL,
    CONSTRAINT [PkTblIpFilter] PRIMARY KEY CLUSTERED ([mIpStx] ASC, [mIpEtx] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK__TblIpFilt__mDesc__0F975522] CHECK (1 <= datalength(rtrim([mDesc]))),
    CONSTRAINT [CK__TblIpFilt__mIpEt__108B795B] CHECK (0 < [mIpEtx] and [mIpEtx] <= 255255255255),
    CONSTRAINT [CK__TblIpFilt__mIpSt__117F9D94] CHECK (0 < [mIpStx] and [mIpStx] <= 255255255255)
);


GO

