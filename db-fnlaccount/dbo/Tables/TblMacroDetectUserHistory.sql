CREATE TABLE [dbo].[TblMacroDetectUserHistory] (
    [mUserID]      VARCHAR (20) NOT NULL,
    [mRegDate]     DATETIME     DEFAULT (getdate()) NOT NULL,
    [mOperator]    VARCHAR (30) NULL,
    [mDel_yn]      CHAR (1)     DEFAULT ('N') NOT NULL,
    [mDelDate]     DATETIME     NULL,
    [mDelOperator] VARCHAR (30) NULL
);


GO

