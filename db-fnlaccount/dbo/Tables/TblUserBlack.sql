CREATE TABLE [dbo].[TblUserBlack] (
    [mRegDate] DATETIME      CONSTRAINT [DF__TblUserBl__mRegD__7BE56230] DEFAULT (getdate()) NOT NULL,
    [mUserId]  CHAR (20)     NOT NULL,
    [mDesc]    VARCHAR (200) NULL,
    CONSTRAINT [PkTblUserBlack] PRIMARY KEY CLUSTERED ([mUserId] ASC) WITH (FILLFACTOR = 80),
    CHECK (2 <= datalength(rtrim([mUserId])))
);


GO

