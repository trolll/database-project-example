CREATE TABLE [dbo].[TblUserNonClient] (
    [mRegDate]               SMALLDATETIME CONSTRAINT [DF_TblUserNonClient_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mUserNo]                INT           NOT NULL,
    [mLastDiffNonCltKeyDate] SMALLDATETIME CONSTRAINT [DF_TblUserNonClient_mLastDiffNonCltKeyDate] DEFAULT (getdate()) NOT NULL,
    [mCltKeyCnt]             BIGINT        CONSTRAINT [DF_TblUserNonClient_mCltKeyCnt] DEFAULT (1) NOT NULL,
    CONSTRAINT [NC_PKTblUserNonClient_1] PRIMARY KEY NONCLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [Fk_TblUserNonClient_mUserNo] FOREIGN KEY ([mUserNo]) REFERENCES [dbo].[TblUser] ([mUserNo])
);


GO

CREATE CLUSTERED INDEX [CL_TblUserNonClient]
    ON [dbo].[TblUserNonClient]([mLastDiffNonCltKeyDate] DESC) WITH (FILLFACTOR = 80);


GO

