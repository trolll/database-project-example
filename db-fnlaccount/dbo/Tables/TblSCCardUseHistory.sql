CREATE TABLE [dbo].[TblSCCardUseHistory] (
    [mSeqNo]          INT      IDENTITY (1, 1) NOT NULL,
    [mRegDate]        DATETIME NULL,
    [mUserNo]         INT      NULL,
    [mSecKeyTableUse] TINYINT  NULL
);


GO

CREATE CLUSTERED INDEX [CL_TblSCCardUseHistory_mUserNo]
    ON [dbo].[TblSCCardUseHistory]([mUserNo] ASC) WITH (FILLFACTOR = 80);


GO

