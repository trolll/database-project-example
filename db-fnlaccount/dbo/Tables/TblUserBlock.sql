CREATE TABLE [dbo].[TblUserBlock] (
    [mRegDate]       DATETIME   CONSTRAINT [DF__TblUserBl__mRegD__3A179ED3] DEFAULT (getdate()) NOT NULL,
    [mUserNo]        INT        NOT NULL,
    [mCertify]       DATETIME   NULL,
    [mBoard]         DATETIME   NULL,
    [mChat]          INT        NULL,
    [mCertifyReason] CHAR (200) NULL,
    CONSTRAINT [NC_PK_TblUserBlock] PRIMARY KEY NONCLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblUserBlockTblUser] FOREIGN KEY ([mUserNo]) REFERENCES [dbo].[TblUser] ([mUserNo])
);


GO

CREATE CLUSTERED INDEX [CL_TblUserBlock_mRegDate]
    ON [dbo].[TblUserBlock]([mRegDate] DESC) WITH (FILLFACTOR = 80);


GO

