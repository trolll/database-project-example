CREATE TABLE [dbo].[TblUseMacro] (
    [mRegDate]     DATETIME CONSTRAINT [DF_TblUseMacro_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mUserNo]      INT      NOT NULL,
    [mUseMacroCnt] INT      NOT NULL,
    [mUptDate]     DATETIME CONSTRAINT [DF_TblUseMacro_mUptDate] DEFAULT (getdate()) NOT NULL
);


GO

CREATE CLUSTERED INDEX [CL_TblUseMacro_mUserNo]
    ON [dbo].[TblUseMacro]([mUserNo] ASC) WITH (FILLFACTOR = 80);


GO

