CREATE TABLE [dbo].[TblUserFilterSet] (
    [mRegDate] DATETIME      NOT NULL,
    [mUserId]  VARCHAR (20)  NOT NULL,
    [mNm]      CHAR (20)     NOT NULL,
    [mLogType] VARCHAR (200) NOT NULL,
    [mReason]  VARCHAR (200) NOT NULL,
    [mOutput]  VARCHAR (200) NOT NULL
);


GO

