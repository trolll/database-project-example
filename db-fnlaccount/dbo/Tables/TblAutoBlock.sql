CREATE TABLE [dbo].[TblAutoBlock] (
    [Seq]      INT       IDENTITY (1, 1) NOT NULL,
    [mUserno]  INT       NOT NULL,
    [mUserid]  CHAR (20) NOT NULL,
    [mWorldNo] SMALLINT  NOT NULL,
    [Dates]    CHAR (8)  NULL,
    [mNm]      CHAR (12) NULL
);


GO

