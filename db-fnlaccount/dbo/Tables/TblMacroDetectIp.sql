CREATE TABLE [dbo].[TblMacroDetectIp] (
    [mIp]       CHAR (15)     NOT NULL,
    [mRegDate]  DATETIME      CONSTRAINT [DF_TblMacroDetectIp_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mMemo]     VARCHAR (100) NULL,
    [mAttachId] VARCHAR (20)  NOT NULL
);


GO

CREATE CLUSTERED INDEX [CL_TblMacroDetectIp_mRegDate]
    ON [dbo].[TblMacroDetectIp]([mRegDate] ASC) WITH (FILLFACTOR = 80);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblMacroDetectIp_mIp]
    ON [dbo].[TblMacroDetectIp]([mIp] ASC) WITH (FILLFACTOR = 80);


GO

