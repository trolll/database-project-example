CREATE TABLE [dbo].[TblDeathCnt] (
    [mRegDate]    DATETIME CONSTRAINT [DF_TblDeathCnt_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]      SMALLINT NOT NULL,
    [mCntHunt]    BIGINT   NOT NULL,
    [mCntTBattle] BIGINT   NOT NULL,
    [mCntSuicide] BIGINT   NOT NULL,
    [mCntPK]      BIGINT   NOT NULL,
    [mCntSiege]   BIGINT   NOT NULL,
    [mCntEtc]     BIGINT   NOT NULL,
    [mCntMon]     BIGINT   NOT NULL
);


GO

CREATE CLUSTERED INDEX [CL_TblDeathCnt]
    ON [dbo].[TblDeathCnt]([mSvrNo] ASC, [mRegDate] ASC) WITH (FILLFACTOR = 80);


GO

