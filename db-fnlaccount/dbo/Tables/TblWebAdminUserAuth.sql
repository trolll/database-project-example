CREATE TABLE [dbo].[TblWebAdminUserAuth] (
    [mMenuNo] INT NOT NULL,
    [mUserNo] INT NOT NULL,
    CONSTRAINT [IX_TblWebAdminUserAuth] UNIQUE CLUSTERED ([mMenuNo] ASC, [mUserNo] ASC) WITH (FILLFACTOR = 80)
);


GO

