CREATE TABLE [dbo].[TblUserDeleted] (
    [mRegDate] DATETIME     CONSTRAINT [DF__TblUserDe__mRegD__09A971A2] DEFAULT (getdate()) NOT NULL,
    [mUserNo]  INT          NOT NULL,
    [mUserId]  CHAR (20)    NULL,
    [mMemo]    VARCHAR (50) NULL,
    CONSTRAINT [PkTblUserDeleted] PRIMARY KEY CLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80)
);


GO

