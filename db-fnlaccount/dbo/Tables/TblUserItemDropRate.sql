CREATE TABLE [dbo].[TblUserItemDropRate] (
    [mUserNo]       INT           NOT NULL,
    [mItemDrop]     TINYINT       CONSTRAINT [DF_TblUserItemDropRate_mItemDrop] DEFAULT (25) NOT NULL,
    [mPKDrop]       TINYINT       CONSTRAINT [DF_TblUserItemDropRate_mPKDrop] DEFAULT (2) NOT NULL,
    [mRandomFlag]   BIT           CONSTRAINT [DF_TblUserItemDropRate_mRandomFlag] DEFAULT (1) NOT NULL,
    [mRandomValue]  TINYINT       NOT NULL,
    [mSetCount]     SMALLINT      CONSTRAINT [DF_TblUserItemDropRate_mSetCount] DEFAULT (0) NOT NULL,
    [mRegDate]      SMALLDATETIME CONSTRAINT [DF_TblUserItemDropRate_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mRenewalDate]  SMALLDATETIME CONSTRAINT [DF_TblUserItemDropRate_mRenewalDate] DEFAULT (getdate()) NOT NULL,
    [mCheckRegWeek] SMALLDATETIME CONSTRAINT [DF_TblUserItemDropRate_mCheckRegWeek] DEFAULT (getdate()) NOT NULL,
    [mPShopTm]      INT           NULL,
    [mTotalTm]      INT           NULL,
    CONSTRAINT [UCL_TblUserItemDropRate_mUserNo] PRIMARY KEY CLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_ITEMDROP_RANGE] CHECK ([mItemDrop] <= 100 and [mItemDrop] >= 0),
    CONSTRAINT [CK_PSHOPTM_RANGE] CHECK ([mPShopTm] >= 0),
    CONSTRAINT [CK_RANDOMVALUE_RANGE] CHECK ([mRandomValue] <= 100 and [mRandomValue] >= 0),
    CONSTRAINT [CK_TOTALTM_RANGE] CHECK ([mTotalTm] >= 0)
);


GO

