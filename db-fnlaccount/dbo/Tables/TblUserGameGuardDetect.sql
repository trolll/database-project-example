CREATE TABLE [dbo].[TblUserGameGuardDetect] (
    [mRegDate]          SMALLDATETIME    CONSTRAINT [DF_TblUserGameGuardDetect_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mUserNo]           INT              NOT NULL,
    [mSvrNo]            INT              NOT NULL,
    [mPcNo]             INT              NOT NULL,
    [mNm]               VARCHAR (12)     NOT NULL,
    [mTotCnt]           BIGINT           CONSTRAINT [DF_TblUserGameGuardDetect_mTotCnt] DEFAULT (1) NOT NULL,
    [mIsDecryptSuccess] BIT              CONSTRAINT [DF_TblUserGameGuardDetect_mIsDecryptSuccess] DEFAULT (0) NOT NULL,
    [mLastGameGuardMsg] VARBINARY (1024) NULL,
    [mLastUpdate]       SMALLDATETIME    CONSTRAINT [DF_TblUserGameGuardDetect_mLastUpdate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [UNC_PK_TblUserGameGuardDetect] PRIMARY KEY NONCLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [Fk_TblUserGameGuardDetect_TblUser] FOREIGN KEY ([mUserNo]) REFERENCES [dbo].[TblUser] ([mUserNo])
);


GO

CREATE CLUSTERED INDEX [CL_TblUserGameGuardDetect_1]
    ON [dbo].[TblUserGameGuardDetect]([mLastUpdate] DESC) WITH (FILLFACTOR = 80);


GO

