CREATE TABLE [dbo].[TblUserCb] (
    [mRegDate] DATETIME  CONSTRAINT [DF__TblUserCb__mRegD__3D14070F] DEFAULT (getdate()) NOT NULL,
    [mUserId]  CHAR (20) NOT NULL,
    CONSTRAINT [PkTblUserCb] PRIMARY KEY CLUSTERED ([mUserId] ASC) WITH (FILLFACTOR = 80),
    CHECK (2 <= datalength(rtrim([mUserId])))
);


GO

