CREATE TABLE [dbo].[TblUserMemo] (
    [mMemoNo]       INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mUserNo]       VARCHAR (20)  NULL,
    [mManageUserNo] VARCHAR (20)  NULL,
    [mMemoText]     VARCHAR (100) NOT NULL,
    [mRegDate]      DATETIME      CONSTRAINT [DF_TblUserMemo_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PkTblUserMemo] PRIMARY KEY CLUSTERED ([mMemoNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [IxTblUserMemoManageUserNoUserNo]
    ON [dbo].[TblUserMemo]([mManageUserNo] ASC, [mUserNo] ASC) WITH (FILLFACTOR = 80);


GO

