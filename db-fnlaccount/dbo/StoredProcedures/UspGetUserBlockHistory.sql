
CREATE PROCEDURE dbo.UspGetUserBlockHistory
	@pUserID	VARCHAR(20)
	,@pResult	INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE @aRowCount INT
			,@aUserNO	INT
	SELECT @aRowCount = 0, @aUserNO = 0, @pResult =0
		
	SELECT 
		TOP 1
		@aUserNO = mUserNO
		,@aRowCount = mUseMacro
	FROM dbo.TblUser				
	WHERE mUserID = @pUserID	

	IF 	@aUserNO = 0 OR @aUserNO IS NULL
	BEGIN
		SET @pResult = 1	-- non user 
		RETURN(1)		
	END 

	IF @aRowCount > 0
	BEGIN
		SET @pResult = 2	-- auto user
		RETURN(1)		
	END 

		
	SELECT 
		@aRowCount = COUNT(*)
	FROM dbo.TblUserBlock
	WHERE mUserNo = @aUserNO
			AND mCertify >= GETDATE()
	
	IF @aRowCount > 0	
	BEGIN
		SET @pResult = 3	-- Certifiy block user 
		RETURN(1)		
	END 	
		
	SELECT 
		@aRowCount = COUNT(*)
	FROM dbo.TblUserBlockHistory
	WHERE mUserNo = @aUserNO
		AND mRegDate > DATEADD( MM, -6, GETDATE())	
		AND mReasonCode IN (10, 14) -- 诈骗, 非公式
		AND ( ( mType >= 7 AND mType <= 13)	OR ( mType >= 18 AND mType <= 23) ) -- 账户停止
		AND mDetachUserID IS NULL
		
	IF @aRowCount > 0 
	BEGIN
		SET @pResult = 3	-- block history
		RETURN(1)		
	END 	
		
	RETURN(0)

GO

