

CREATE         PROCEDURE [dbo].[WspApplyChatPenalty]	
	@mUserId VARCHAR(20), -- ?? ??? ???
	@mType INT, -- ????		
	@mReasonCode SMALLINT, -- ????
	@mPenaltyHour INT, -- ??? ??? ??
	@mReason CHAR(200), -- ??????
	@mReasonFile CHAR(200), -- ??????
	@mAttachUserID varchar(50), -- ??? ??? ??? ID
@mComment nvarchar(1000)
AS
	SET NOCOUNT ON

	DECLARE @mUserNo INT
	DECLARE @mChat INT
	DECLARE @mEndDate DATETIME

	SELECT @mUserNo = mUserNo FROM [dbo].[TblUser] WHERE mUserId = @mUserId
	SELECT @mChat = @mPenaltyHour * 60
	SELECT @mEndDate = DATEADD(HOUR, @mPenaltyHour, GETDATE())


	BEGIN TRANSACTION

	IF (SELECT COUNT(*) FROM [dbo].[TblUserBlock] WHERE mUserNo = @mUserNo) = 0
	BEGIN	
		INSERT INTO
			[dbo].[TblUserBlock]
			(mRegDate, mUserNo, mChat, mCertifyReason)
		VALUES
			(GETDATE(), @mUserNo, @mChat, @mReason)	
	
	END
	ELSE
	BEGIN
		UPDATE 
			[dbo].[TblUserBlock]
		SET
			mRegDate = GETDATE(),
			mChat = @mChat,
			mCertifyReason = @mReason
		WHERE
			mUserNo = @mUserNo					
	END	
	
	-- ???	
	IF @@ERROR <> 0
		GOTO ON_ERROR

	INSERT INTO
		[dbo].[TblUserBlockHistory]
		(mRegDate, mUserNo, mType, mEndDate, mReasonCode, mReason, mReasonFile, mAttachUserID,mComment)
	VALUES
		(GETDATE(), @mUserNo, @mType, @mEndDate, @mReasonCode, @mReason, @mReasonFile, @mAttachUserID,@mComment)

	-- ???	
	IF @@ERROR <> 0
		GOTO ON_ERROR

	COMMIT TRANSACTION
	RETURN 0

ON_ERROR:
	ROLLBACK TRANSACTION
	RETURN @@ERROR

/*
	中国专用 结束
*/

/*修改中国专用sp　为alter,红色为注释不运行不部分*/

GO

