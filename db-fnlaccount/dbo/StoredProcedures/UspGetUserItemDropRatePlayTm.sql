/******************************************************************************
**		Name: UspGetUserItemDropRatePlayTm
**		Desc: 蜡历狼 角力 敲饭捞 矫埃 掘扁
**
**		Auth: 沥备柳
**		Date: 10.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetUserItemDropRatePlayTm]
	@pUserNo			INT
	,	@pRealPlayTm	INT	OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE		@aLoginTm		SMALLDATETIME
	DECLARE		@aPShopTm		INT
	DECLARE		@aTotalTm		INT

	SET		@pRealPlayTm = 0

	-- 泅犁鳖瘤 敲饭捞 茄 矫埃 / 俺牢惑痢 矫埃 / 肺弊牢 窍咯 泅犁鳖瘤 柳青茄 矫埃阑 傈眉 矫埃俊 歹茄促.
	SELECT @aTotalTm = TDrop.mTotalTm, @aPShopTm = TDrop.mPShopTm, @aLoginTm = TUser.mLoginTm
	FROM dbo.TblUserItemDropRate	TDrop
	INNER JOIN dbo.TblUser			TUser ON TDrop.mUserNo = TUser.mUserNo  
	WHERE TDrop.mUserNo = @pUserNo

	SET	@aTotalTm = @aTotalTm + DATEDIFF( minute, @aLoginTm, GetDate())

	-- 角力 敲饭捞茄 矫埃
	SET @pRealPlayTm = @aTotalTm - @aPShopTm
	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error 
	END
	
	RETURN(0)

GO

