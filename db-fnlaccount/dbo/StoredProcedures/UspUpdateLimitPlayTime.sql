/******************************************************************************
**		Name: UspUpdateLimitPlayTime
**		Desc: Æ¯È­¼­¹ö ÇÃ·¹ÀÌ Á¦ÇÑ½Ã°£ ¼³Á¤
**			  
**		Auth: °ø ¼® ±Ô
**		Date: 2014.08.20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateLimitPlayTime]
	 @pUserNo	INT,
	 @pNormalLimitTime	INT,
	 @pPcBangLimitTime	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	UPDATE dbo.TblUser 
	SET mNormalLimitTime = @pNormalLimitTime,
		mPcBangLimitTime = @pPcBangLimitTime
	WHERE mUserNo = @pUserNo
	
	IF (@@ROWCOUNT = 0 OR @@ERROR <> 0)
	BEGIN
		RETURN(1)
	END
		
	RETURN(0)

GO

