-----------------------------------------------------------------
-- PROCEDURE
-----------------------------------------------------------------
CREATE   PROCEDURE [dbo].[UspGetSameIpUser]
	@mCnt INT	--  悼老酒捞乔 割疙捞惑
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT B.mUserNo, B.mWorldNo FROM 
	(
		SELECT 	
			mIPEX 
		FROM dbo.TblUser
		WHERE mWorldNo >  0 	-- 柯扼牢 荤侩磊父
			and	mIPEX > 1000	-- 肋给等 ip 力寇
		GROUP BY 	mIPEX
		HAVING COUNT(*) >= @mCnt
	) A	JOIN dbo.TblUser B 
			ON  A.mIPEX = B.mIPEX
	WHERE mWorldNo >  0

GO

