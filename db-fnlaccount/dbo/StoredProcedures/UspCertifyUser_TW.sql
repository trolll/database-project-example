/******************************************************************************  
**  File: UspCertifyUser.sql  
**  Name: UspCertifyUser_TW  
**  Desc: 荤侩磊 牢刘  
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  20110123 dmbkh  辑滚焙捞 悼老窍搁 IP 林家 眉农 绝捞 牢刘俊 己傍茄促.(辑滚焙 悼老 咯何绰 霸烙辑滚俊辑 眉农)
**                  辑滚焙捞 促福搁 肺弊牢 阂啊(霸烙辑滚俊辑 贸府)
**                  辑滚焙捞 悼老且 锭 扁粮 蜡历绰 亲惑 KICK等促(霸烙辑滚俊辑 贸府)
*******************************************************************************/   
CREATE  PROCEDURE [dbo].[UspCertifyUser_TW]
	 @pUserId			VARCHAR(20)
	,@pUserPswd			VARCHAR(20)
	,@pIp				CHAR(15)
	,@pIpEX			BIGINT
	,@pCertifiedKey		INT			-- DB狼 RAND()绰 float骨肺 寇何俊辑 林磊.
	,@pIsEqualRsc		BIT
	,@pPcBangLv			INT			-- EPcBangLv.
	,@pUserNo			INT				OUTPUT
	,@pWorldNo			SMALLINT		OUTPUT
	,@pErrNoStr			VARCHAR(50)		OUTPUT
	,@pCertify			DATETIME		OUTPUT	-- 牢刘block捞 辆丰登绰 老矫.
	,@pCertifyReason	VARCHAR(200)	OUTPUT	-- 牢刘block矫 荤蜡.
	,@pSecKeyTableUse	TINYINT	OUTPUT	-- 焊救虐抛捞喉 荤侩咯何
	,@pUserAuth			TINYINT	OUTPUT
	,@pIsAddUser		BIT	= 1
	,@pIsPwdCheck		BIT = 0
	,@pJoinCode			VARCHAR(1) = 'N'     -- pc规 炼牢内靛
    ,@pLoginChannelID	CHAR(1) = 'N'        -- 盲澄傅 ID
    ,@pTired      		CHAR(1) = 'N'        -- 吝刀规瘤 措惑磊衬?
    ,@pChnSID      		CHAR(33) = ''        -- 吝惫脚盒刘锅龋
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE	@aErrNo		INT,
			@aNewUserNo	INT;

	SELECT	@aErrNo		= 0, @aNewUserNo = 0;
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'
	

	-- 涝仿蔼狼 菩胶况靛肺 函版 茄促. 
	IF ( @pIsAddUser = 1 )
	BEGIN
		IF NOT EXISTS(	SELECT * 
						FROM dbo.TblUser 
						WHERE mUserId= @pUserId)
		BEGIN
			INSERT INTO dbo.TblUser([mUserId], [mUserPswd], [mJoinCode], mLoginChannelID, mTired, mChnSID, mNewId)	
			VALUES(@pUserId, 0x00, @pJoinCode, @pLoginChannelID, @pTired, @pChnSID, 1)
			
			IF @@ERROR <> 0
				RETURN(1) -- db error 

			
			SET @aNewUserNo = SCOPE_IDENTITY();
			
			UPDATE dbo.TblUser
			SET mUserPswd = [dbo].[UFN_MD5_ENCODEVALUE]( @pUserId, @pUserPswd, @aNewUserNo)
			WHERE mUserID = @pUserId;

			IF @@ERROR <> 0
				RETURN(1) -- db error 			
			
		END
	END 
	
	DECLARE	@aIp		CHAR(15)
	DECLARE @aPswd		VARCHAR(20),
			@aPswdCheck	BINARY(16);
	DECLARE @aLoginTm	DATETIME
	DECLARE @aKey		INT
	DECLARE @aUserNo	INT
	
	SELECT 
		   @aUserNo = a.[mUserNo],
		   @pUserAuth=a.[mUserAuth], @aPswd=a.[mUserPswd], @pUserNo=a.[mUserNo], @pWorldNo=a.[mWorldNo], 
		   @aIp=a.[mIp], @aLoginTm=a.[mLoginTm], @aKey=a.[mCertifiedKey], 
		   @pCertify=b.[mCertify], @pCertifyReason=RTRIM(b.[mCertifyReason]),
		   @pSecKeyTableUse=a.[mSecKeyTableUse]
		FROM dbo.TblUser  AS a 
			LEFT OUTER JOIN dbo.TblUserBlock AS b  
			ON a.[mUserNo] = b.[mUserNo] 
		WHERE (a.[mUserId] = @pUserId) 
				AND ( a.[mDelDate] = '1900-01-01' )	
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoUserNotExistId3'
		GOTO LABEL_END
	 END		
	  
	-- 2008.02.11 judy
	IF ( @pIsPwdCheck = 1 )
	BEGIN
		SET @aPswdCheck = [dbo].[UFN_MD5_ENCODEVALUE]( @pUserId, @pUserPswd, @aUserNo )
		IF ( RTRIM(@aPswdCheck) <> RTRIM(@aPswd) )
		BEGIN
			SET  @aErrNo	= 1
			SET	 @pErrNoStr	= 'eErrNoUserDiffPswd'
			GOTO LABEL_END					
		END 
	END 
		  
	IF(0 = @pUserAuth)
	 BEGIN
		SET  @aErrNo	= 3
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid'
		GOTO LABEL_END	 
	 END
	 
	-- 20060723(milkji):老馆牢捞搁 resource啊 沥犬秦具父 沥惑捞促.
	IF((1 = @pUserAuth) AND (0 = @pIsEqualRsc))
	 BEGIN
		SET  @aErrNo	= 92
		SET	 @pErrNoStr	= 'eErrNoRscKeyNotEqual'
		GOTO LABEL_END		 
	 END
	 
	IF((1 < @pUserAuth) AND (@aIp <> @pIp))
	 BEGIN
		SET  @aErrNo	= 93
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid'
		GOTO LABEL_END		 
	 END	 
	 
	IF(GETDATE() < @pCertify)
	 BEGIN
		SET  @aErrNo	= 9
		SET	 @pErrNoStr	= 'eErrNoUserBlocked'
		GOTO LABEL_END			
	 END	 

	IF(0 < @pWorldNo)
	 BEGIN
		SET  @aErrNo = 4		

	-- 20110124 dmbkh --------------------
	-- 促弗 辑滚焙捞扼搁 公炼扒 救凳(盲澄辑滚俊辑 阜澜)	
	-- 鞍篮 辑滚焙捞扼搁 咯扁鳖瘤 坷霸 登绊 IP 林家 眉农 绝捞 eErrNoUserChkAlreadyLogined肺 贸府
	--------------------------------------		

			SET	 @pErrNoStr	= 'eErrNoUserChkAlreadyLogined'
			GOTO LABEL_END1	-- 悼老 IP狼 版快父 荤侩磊 沥焊甫 盎脚茄促.

		--IF(@aIp = @pIp)
		-- BEGIN
		--	SET	 @pErrNoStr	= 'eErrNoUserChkAlreadyLogined'
		--	GOTO LABEL_END1	-- 悼老 IP狼 版快父 荤侩磊 沥焊甫 盎脚茄促.
		-- END
		--ELSE
		-- BEGIN
		--	SET	 @pErrNoStr	= 'eErrNoUserLoginAnother'
		--	GOTO LABEL_END
		-- END
	 END
	 	 
LABEL_END1:		 
	UPDATE dbo.TblUser 
	SET 
		[mCertifiedKey]=@pCertifiedKey, 
		[mIp]=@pIp, 		
		[mPcBangLv]=@pPcBangLv,
		[mIpEX] = @pIpEX
		, mJoinCode = @pJoinCode
		, mLoginChannelID = @pLoginChannelID
		, mTired = @pTired
		, mChnSID = @pChnSID
	FROM dbo.TblUser 
	WHERE ( [mUserNo] = @aUserNo ) 
			AND ([mDelDate]  = '1900-01-01')	
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 5
		GOTO LABEL_END	 
	END	 

LABEL_END:	
	
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

