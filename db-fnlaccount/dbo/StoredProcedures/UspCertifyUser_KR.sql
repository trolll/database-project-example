/******************************************************************************    
**  File: UspCertifyUser.sql    
**  Name: UspCertifyUser    
**  Desc: 荤侩磊 牢刘    
**    
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:  Author:    Description:    
**  -------- --------   ---------------------------------------    
**  20110123 dmbkh  辑滚焙捞 悼老窍搁 IP 林家 眉农 绝捞 牢刘俊 己傍茄促.(辑滚焙 悼老 咯何绰 霸烙辑滚俊辑 眉农)  
**                  辑滚焙捞 促福搁 肺弊牢 阂啊(霸烙辑滚俊辑 贸府)  
**                  辑滚焙捞 悼老且 锭 扁粮 蜡历绰 亲惑 KICK等促(霸烙辑滚俊辑 贸府)  
**  20101104 JUDY   昆哩 欺喉府寂 函版栏肺 昆哩 拌沥 KEY甫 历厘茄促.     
**  20160921 nsm	硅撇 辑滚肺 捞悼 矫 worldno 沥焊啊 巢酒乐绢档 mIsMovingToBattleSvr 沥焊俊 蝶扼 贸府   
*******************************************************************************/     
CREATE  PROCEDURE [dbo].[UspCertifyUser_KR]    
 @pUserId   VARCHAR(20)    
 ,@pUserPswd   VARCHAR(20)    
 ,@pIp    CHAR(15)    
 ,@pIpEX   BIGINT    
 ,@pCertifiedKey  INT   -- DB狼 RAND()绰 float骨肺 寇何俊辑 林磊.    
 ,@pIsEqualRsc  BIT    
 ,@pPcBangLv   INT   -- EPcBangLv.    
 ,@pUserNo   INT    OUTPUT    
 ,@pWorldNo   SMALLINT  OUTPUT    
 ,@pErrNoStr   VARCHAR(50)  OUTPUT    
 ,@pCertify   DATETIME  OUTPUT -- 牢刘block捞 辆丰登绰 老矫.    
 ,@pCertifyReason VARCHAR(200) OUTPUT -- 牢刘block矫 荤蜡.    
 ,@pSecKeyTableUse TINYINT OUTPUT -- 焊救虐抛捞喉 荤侩咯何    
 ,@pUserAuth   TINYINT OUTPUT    
 ,@pIsAddUser  BIT = 1    
 ,@pIsPwdCheck  BIT = 0    
 ,@pJoinCode   VARCHAR(1) = 'N'     -- pc规 炼牢内靛    
 ,@pLoginChannelID CHAR(1) = 'N'        -- 盲澄傅 ID    
 ,@pTired        CHAR(1) = 'N'        -- 吝刀规瘤 措惑磊衬?    
 ,@pChnSID        CHAR(33) = ''        -- 吝惫脚盒刘锅龋    
 ,@pAccountGuid  INT -- 昆哩 GUID    
AS    
 SET NOCOUNT ON;     
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
     
 DECLARE @aErrNo  INT    
 SET  @aErrNo  = 0    
 SET  @pErrNoStr = 'eErrNoSqlInternalError'    
     
 IF ( @pIsAddUser = 1 )    
 BEGIN    
  IF NOT EXISTS( SELECT *     
      FROM dbo.TblUser     
      WHERE mUserId= @pUserId)    
  BEGIN    
      
   IF (@pAccountGuid IS NULL) OR (@pAccountGuid = 0)    
   BEGIN    
    SET  @aErrNo = 3    
    SET  @pErrNoStr = 'eErrNoAuthInvalid'    
    GOTO LABEL_END       
   END    
         
       
   INSERT INTO dbo.TblUser([mUserId], [mUserPswd],[mJoinCode], mLoginChannelID, mTired, mChnSID, mNewId, mAccountGuid)  -- 脚痹 拌沥 积己    
   VALUES(@pUserId, 'nhngames', @pJoinCode, @pLoginChannelID, @pTired, @pChnSID, 1, @pAccountGuid)    
  END    
 END     
     
 DECLARE @aIp  CHAR(15)    
 DECLARE @aPswd  VARCHAR(20)    
 DECLARE @aLoginTm DATETIME    
 DECLARE @aKey  INT    
 DECLARE @aUserNo INT    
    , @aAccountGUID INT -- 昆哩 拌沥 GUID    
 
 -- 20160921(nsm)
 DECLARE @aIsMovingToBattleSvr	BIT
 
     
 SELECT     
     @aUserNo = a.[mUserNo],    
     @pUserAuth=a.[mUserAuth], @aPswd=a.[mUserPswd], @pUserNo=a.[mUserNo], @pWorldNo=a.[mWorldNo],     
     @aIp=a.[mIp], @aLoginTm=a.[mLoginTm], @aKey=a.[mCertifiedKey],     
     @pCertify=b.[mCertify], @pCertifyReason=RTRIM(b.[mCertifyReason]),    
     @pSecKeyTableUse=a.[mSecKeyTableUse], @aAccountGUID = mAccountGuid 
     -- 20160921(nsm)
     ,@aIsMovingToBattleSvr = a.[mIsMovingToBattleSvr]
  FROM dbo.TblUser  AS a     
   LEFT OUTER JOIN dbo.TblUserBlock AS b      
   ON a.[mUserNo] = b.[mUserNo]     
  WHERE (a.[mUserId] = @pUserId)     
    AND ( a.[mDelDate] = '1900-01-01' )     
 IF(1 <> @@ROWCOUNT)    
  BEGIN    
  SET  @aErrNo = 1    
  SET  @pErrNoStr = 'eErrNoUserNotExistId3'    
  GOTO LABEL_END    
  END      
       
 -- 2008.02.11 judy    
 IF ( @pIsPwdCheck = 1 )    
 BEGIN    
  IF ( RTRIM(@pUserPswd) <> RTRIM(@aPswd) )    
  BEGIN    
   SET  @aErrNo = 1    
   SET  @pErrNoStr = 'eErrNoUserDiffPswd'    
   GOTO LABEL_END         
  END     
 END     
        
 IF(0 = @pUserAuth)    
  BEGIN    
  SET  @aErrNo = 3    
  SET  @pErrNoStr = 'eErrNoAuthInvalid'    
  GOTO LABEL_END      
  END    
      
 -- 20060723(milkji):老馆牢捞搁 resource啊 沥犬秦具父 沥惑捞促.    
 IF((1 = @pUserAuth) AND (0 = @pIsEqualRsc))    
  BEGIN    
  SET  @aErrNo = 92    
  SET  @pErrNoStr = 'eErrNoRscKeyNotEqual'    
  GOTO LABEL_END       
  END    
      
 IF((1 < @pUserAuth) AND (@aIp <> @pIp))    
  BEGIN    
  SET  @aErrNo = 93    
  SET  @pErrNoStr = 'eErrNoAuthInvalid'    
  GOTO LABEL_END       
  END      
      
 IF(GETDATE() < @pCertify)    
  BEGIN    
  SET  @aErrNo = 9    
  SET  @pErrNoStr = 'eErrNoUserBlocked'    
  GOTO LABEL_END       
  END      
      
 --20101104 JUDY : 昆哩狼 GUID 内靛啊 乐绢具父 牢刘捞 啊瓷窍促.    
 IF (@aAccountGUID  IS NULL OR @aAccountGUID = 0 )    
  BEGIN    
  SET  @aErrNo = 94    
  SET  @pErrNoStr = 'eErrNoAuthInvalid'    
  GOTO LABEL_END       
  END      
    
 --20101104 JUDY : 牢刘GUID啊 撇府促.    
 --老馆 荤侩磊狼 版快 荤侩磊 沥焊啊 撇副版快 馆券 贸府 茄促.    
 IF ( @pUserAuth = 1)  AND (@aAccountGUID  <> @pAccountGuid )    
  BEGIN    
  SET  @aErrNo = 95    
  SET  @pErrNoStr = 'eErrNoAuthInvalid'    
  GOTO LABEL_END       
  END      
    
-- 20160921(nsm)
-- IF(0 < @pWorldNo)  
IF( (0 < @pWorldNo) AND (0 = @aIsMovingToBattleSvr) )
BEGIN    
 SET  @aErrNo = 4  
  
 -- 20110124 dmbkh --------------------  
 -- 促弗 辑滚焙捞扼搁 公炼扒 救凳(盲澄辑滚俊辑 阜澜)   
 -- 鞍篮 辑滚焙捞扼搁 咯扁鳖瘤 坷霸 登绊 IP 林家 眉农 绝捞 eErrNoUserChkAlreadyLogined肺 贸府  
 --------------------------------------    
 SET  @pErrNoStr = 'eErrNoUserChkAlreadyLogined'    
 GOTO LABEL_END1  
  
 --IF(@aIp = @pIp)    
 --BEGIN    
 -- SET  @pErrNoStr = 'eErrNoUserChkAlreadyLogined'    
 -- GOTO LABEL_END1 -- 悼老 IP狼 版快父 荤侩磊 沥焊甫 盎脚茄促.    
 --END    
 --ELSE    
 --BEGIN    
 -- SET  @pErrNoStr = 'eErrNoUserLoginAnother'    
 -- GOTO LABEL_END    
 --END     
   
END    
        
LABEL_END1:       
  UPDATE dbo.TblUser     
  SET     
   [mCertifiedKey]=@pCertifiedKey,     
   [mIp]=@pIp,       
   [mPcBangLv]=@pPcBangLv,    
   [mIpEX] = @pIpEX    
   , mJoinCode = @pJoinCode    
   , mLoginChannelID = @pLoginChannelID    
   , mTired = @pTired    
   , mChnSID = @pChnSID      
  FROM dbo.TblUser     
  WHERE ( [mUserNo] = @aUserNo )     
    AND ([mDelDate]  = '1900-01-01')     
      
  IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)    
  BEGIN    
   SET  @aErrNo = 5    
   GOTO LABEL_END      
  END      
    
LABEL_END:     
     
 SET NOCOUNT OFF     
 RETURN(@aErrNo)

GO

