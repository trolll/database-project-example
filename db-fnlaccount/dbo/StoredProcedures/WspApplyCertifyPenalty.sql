CREATE	PROCEDURE [dbo].[WspApplyCertifyPenalty]	
	@mUserId CHAR(20), -- 阂樊 捞侩磊 拌沥疙
	@mType INT, -- 贸国规过		
	@mReasonCode SMALLINT, -- 贸国荤蜡
	@mPenaltyHour INT, -- 力犁甫 啊窍绰 矫埃
	@mReason CHAR(200), -- 贸国包访皋葛
	@mReasonFile CHAR(200), -- 贸国包访颇老
	@mAttachUserID VARCHAR(20),
	@mComment nvarchar(1000)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @mUserNo INT
	DECLARE @mCertify DATETIME

	SELECT @mUserNo = mUserNo FROM [dbo].[TblUser] WHERE mUserId = @mUserId
	SELECT @mCertify = CASE WHEN @mType IN(13, 14) THEN 
				'7777-07-07 00:00:00'
			ELSE
				DATEADD(HOUR, @mPenaltyHour, GETDATE())
			END 
					
	
	BEGIN TRANSACTION

		IF (SELECT COUNT(*) FROM [dbo].[TblUserBlock] WHERE mUserNo = @mUserNo) = 0
		BEGIN	
			INSERT INTO
				[dbo].[TblUserBlock]
				(mRegDate, mUserNo, mCertify, mCertifyReason)
			VALUES
				(GETDATE(), @mUserNo, @mCertify, @mReason)	
		
		END
		ELSE
		BEGIN
			UPDATE 
				[dbo].[TblUserBlock]
			SET
				mRegDate = GETDATE(),
				mCertify = @mCertify,
				mCertifyReason = @mReason
			WHERE
				mUserNo = @mUserNo					
		END	
		
		-- 俊矾?
		IF @@ERROR <> 0
			GOTO ON_ERROR
		
		INSERT INTO
			[dbo].[TblUserBlockHistory]
			(mRegDate, mUserNo, mType, mEndDate, mReasonCode, mReason, mReasonFile, mAttachUserID, mComment)
		VALUES
			(GETDATE(), @mUserNo, @mType, @mCertify, @mReasonCode, @mReason, @mReasonFile, @mAttachUserID, @mComment)

		-- 俊矾?
		IF @@ERROR <> 0
			GOTO ON_ERROR

	COMMIT TRANSACTION
	RETURN 0

ON_ERROR:
	ROLLBACK TRANSACTION
	RETURN @@ERROR

GO

