

--FNLAccount
CREATE         PROCEDURE [dbo].[WspListBlockHistory]
	@mStartYear SMALLINT,
	@mStartMonth SMALLINT,
	@mStartDay SMALLINT,
	@mEndYear SMALLINT,
	@mEndMonth SMALLINT,
	@mEndDay SMALLINT,
	@attachCat VARCHAR(20),
	@attachCount VARCHAR(20),
	@attachId VARCHAR(20)
AS
	SET NOCOUNT ON

	DECLARE @mStartDate DATETIME
	DECLARE @mEndDate DATETIME

	SET @mStartDate = CAST(STR(@mStartYear) + '-' + STR(@mStartMonth) + '-' + STR(@mStartDay) AS DATETIME)
	SET @mEndDate = CAST(STR(@mEndYear) + '-' + STR(@mEndMonth) + '-' + STR(@mEndDay) AS DATETIME) + 1
	
	IF (@attachCat = 0) --查询处罚者分类为“全部”	
		SELECT
			b.mSeqNo, -- ??
			u.mUserId, -- ?? ??
			b.mRegDate, -- ???
			b.mType, -- ????
			b.mReasonCode, -- ????	
			--(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mAttachUserNo) mAttachUserId, -- ??? 
			b.mAttachUserId mAttachUserId,
			b.mReason, -- ??????
			--b.mDeatchDate, -- ?????
			--modified by mengdan 2008-12-2
			case
				when b.mDeatchDate is null and b.mEndDate<getdate()
					then b.mEndDate
				else 
					b.mDeatchDate
			end as mDeatchDate,	
			--(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mDetachUserNo) mDetachUserId, -- ??? 
			b.mDetachUserId mDetachUserId,
			b.mDetachReason, -- ??????
			b.mReasonFile ,-- ?? ??
	                           b.mcomment,
			 (SELECT c.mCodeDesc from dbo.TblCode c WHERE c.mCode=b.mType AND c.mCodeType=4) as mCodeDesc
		FROM 
			[dbo].[TblUserBlockHistory] As b
		INNER JOIN		[dbo].[TblUser] As u
		ON		b.mUserNo = u.mUserNo
		WHERE
			b.mRegdate BETWEEN @mStartDate AND @mEndDate
		ORDER BY		b.mRegdate ASC
	
	IF (@attachCat = 1) --查询处罚者分类为“系统”	
		IF(@attachCount = 0)--处罚Count为“全部”	
			SELECT
				b.mSeqNo, -- ??
				u.mUserId, -- ?? ??
				b.mRegDate, -- ???
				b.mType, -- ????
				b.mReasonCode, -- ????	
				--(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mAttachUserNo) mAttachUserId, -- ??? 
				b.mAttachUserId mAttachUserId,
				b.mReason, -- ??????
				--b.mDeatchDate, -- ?????
				--modified by mengdan 2008-12-2
				case
					when b.mDeatchDate is null and b.mEndDate<getdate()
						then b.mEndDate
					else 
						b.mDeatchDate
				end as mDeatchDate,	
				--(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mDetachUserNo) mDetachUserId, -- ??? 
				b.mDetachUserId mDetachUserId,
				b.mDetachReason, -- ??????
				b.mReasonFile ,-- ?? ??
		                           b.mcomment,
				 (SELECT c.mCodeDesc from dbo.TblCode c WHERE c.mCode=b.mType AND c.mCodeType=4) as mCodeDesc
			FROM 
				[dbo].[TblUserBlockHistory] As b
			INNER JOIN		[dbo].[TblUser] As u
			ON		b.mUserNo = u.mUserNo
			WHERE
				b.mRegdate BETWEEN @mStartDate AND @mEndDate
				AND b.mAttachUserID = '系统'
			ORDER BY		b.mRegdate ASC


		IF(@attachCount != 0)--处罚Count不为“全部”	
			SELECT
				b.mSeqNo, -- ??
				u.mUserId, -- ?? ??
				b.mRegDate, -- ???
				b.mType, -- ????
				b.mReasonCode, -- ????	
				--(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mAttachUserNo) mAttachUserId, -- ??? 
				b.mAttachUserId mAttachUserId,
				b.mReason, -- ??????
				--b.mDeatchDate, -- ?????
				--modified by mengdan 2008-12-2
				case
					when b.mDeatchDate is null and b.mEndDate<getdate()
						then b.mEndDate
					else 
						b.mDeatchDate
				end as mDeatchDate,	
				--(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mDetachUserNo) mDetachUserId, -- ??? 
				b.mDetachUserId mDetachUserId,
				b.mDetachReason, -- ??????
				b.mReasonFile ,-- ?? ??
		                           b.mcomment,
				 (SELECT c.mCodeDesc from dbo.TblCode c WHERE c.mCode=b.mType AND c.mCodeType=4) as mCodeDesc
			FROM 
				[dbo].[TblUserBlockHistory] As b
			INNER JOIN		[dbo].[TblUser] As u
			ON		b.mUserNo = u.mUserNo
			WHERE
				b.mRegdate BETWEEN @mStartDate AND @mEndDate
				AND b.mAttachUserID = '系统' 
			ORDER BY		b.mRegdate ASC


	IF (@attachCat = 2) --查询处罚者分类为“Admin”	
		SELECT
			b.mSeqNo, -- ??
			u.mUserId, -- ?? ??
			b.mRegDate, -- ???
			b.mType, -- ????
			b.mReasonCode, -- ????	
			--(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mAttachUserNo) mAttachUserId, -- ??? 
			b.mAttachUserId mAttachUserId,
			b.mReason, -- ??????
			--b.mDeatchDate, -- ?????
			--modified by mengdan 2008-12-2
			case
				when b.mDeatchDate is null and b.mEndDate<getdate()
					then b.mEndDate
				else 
					b.mDeatchDate
			end as mDeatchDate,	

			--(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mDetachUserNo) mDetachUserId, -- ??? 
			b.mDetachUserId mDetachUserId,
			b.mDetachReason, -- ??????
			b.mReasonFile ,-- ?? ??
	                           b.mcomment,
			 (SELECT c.mCodeDesc from dbo.TblCode c WHERE c.mCode=b.mType AND c.mCodeType=4) as mCodeDesc
		FROM 
			[dbo].[TblUserBlockHistory] As b
		INNER JOIN		[dbo].[TblUser] As u
		ON		b.mUserNo = u.mUserNo
		WHERE
			b.mRegdate BETWEEN @mStartDate AND @mEndDate
			AND b.mAttachUserID = @attachId
		ORDER BY		b.mRegdate ASC

GO

