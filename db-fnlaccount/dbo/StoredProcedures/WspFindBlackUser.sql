
CREATE   Procedure dbo.WspFindBlackUser
------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	SELECT a.[mUserId], a.[mDesc], b.[mWorldNo] 
		FROM TblUserBlack AS a WITH ( NOLOCK )
			LEFT OUTER JOIN TblUser AS b WITH (NOLOCK) 		
	ON(a.[mUserId] = b.[mUserId]And b.[mWorldNo] > 0)
	
	SET NOCOUNT OFF

GO

