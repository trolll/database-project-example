/******************************************************************************
**		Name: UspResetCheckUserItemDrop
**		Desc: 眉农 扁埃捞 瘤车栏骨肺 秦寸 沥焊甫 檬扁拳茄促.
**
**		Auth: 沥备柳
**		Date: 10.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspResetCheckUserItemDrop]
	@pUserNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- 老林老捞 瘤唱辑 眉农客 包访等 沥焊甸阑 檬扁拳 茄促.
	UPDATE dbo.TblUserItemDropRate
	SET	mCheckRegWeek = GetDate(), mPShopTm = 0, mTotalTm = 0
	WHERE mUserNo = @pUserNo

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error
	END

	RETURN(0)

GO

