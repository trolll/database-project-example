
CREATE PROCEDURE	dbo.UspCheckUseSecKeyTable
	@pUserNo		INT,				-- 사용자 계정번호
	@pCheckVal		TINYINT 		OUTPUT,	-- 보안키테이블 시스템 사용여부 (1:사용함/0:사용않함)
	@pErrNoStr		VARCHAR(50) 	OUTPUT
As
Begin
	SET NOCOUNT ON

	DECLARE	@aErrNo	INT
	SET		@aErrNo	= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'

	IF NOT EXISTS(	SELECT mUserNo 
			FROM dbo.TblUser WITH(NOLOCK) 
			WHERE mUserNo = @pUserNo)
	BEGIN
		SET	@aErrNo	= 1
		SET	@pErrNoStr	= 'eErrNoUserNotExistId4'
		GOTO LABEL_END
	END

	SELECT @pCheckVal = mSecKeyTableUse FROM dbo.TblUser WITH (NOLOCK) WHERE mUserNo = @pUserNo

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)
End

GO

