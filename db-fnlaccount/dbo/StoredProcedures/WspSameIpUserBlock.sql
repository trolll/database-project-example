CREATE PROCEDURE [dbo].[WspSameIpUserBlock]
	  @mS	BIGINT
	, @mE	BIGINT
	, @mDesc	CHAR(100)
AS
	SET NOCOUNT ON

	DECLARE	@aErrNo		INT

	IF (	SELECT COUNT(*) 
			FROM dbo.TblIpFilter 
			WHERE mIpStx <= @mS 
					AND @mS <= mIpEtx) > 0
	BEGIN
		RETURN(2)	-- 秦寸 抛捞喉俊 沥焊啊 捞固 粮犁茄促
	END 
	
	INSERT INTO dbo.TblIpFilter 
	VALUES(GETDATE(), @mS, @mE, @mDesc)	

	IF @@ERROR = 0
		RETURN(1)	-- DB Error
	
	RETURN(0)

GO

