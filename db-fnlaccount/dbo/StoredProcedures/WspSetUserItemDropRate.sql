/******************************************************************************
**		Name: WspSetUserItemDropRate
**		Desc: 蜡历狼 靛而啦/PK靛而啦 汲沥
**
**		Auth: 眠槛
**		Date: 10.06.08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspSetUserItemDropRate]
	@pUserID			VARCHAR(20)
	, @pItemDrop		TINYINT
	, @pPKDrop		TINYINT
	, @pRandomFlag	BIT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aUserNo	INT

	SELECT @aErrNo = 0, @aRowCnt = 0, @aUserNo = 0;
	
	SELECT 
		@aUserNo = mUserNo
	FROM dbo.TblUser
	WHERE mUserID = @pUserID
		AND mDelDate = '1900-01-01';
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	IF(@aErrNo <> 0)
	BEGIN
		RETURN(1)	-- db error
	END	
	
	
	UPDATE dbo.TblUserItemDropRate
	SET
		mItemDrop = @pItemDrop
		, mPKDrop = @pPKDrop
		, mRandomFlag = @pRandomFlag		
		, mRandomValue =  CASE WHEN @pRandomFlag = 1 THEN (RAND()*(100 - @pItemDrop)) + @pItemDrop
					ELSE @pItemDrop
				END 				
		, mRenewalDate = GETDATE()
	WHERE mUserNo = @aUserNo
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	
	IF @aErrNo <>  0
	BEGIN
		RETURN(1)	-- db error
	END	
	
	IF @aRowCnt = 0
	BEGIN
		INSERT INTO dbo.TblUserItemDropRate(mUserNo, mItemDrop, mPKDrop, mRandomFlag, mRandomValue, mSetCount, mRegDate, mRenewalDate, mCheckRegWeek, mPShopTm, mTotalTm) 
		VALUES (@aUserNo, @pItemDrop, @pPKDrop, @pRandomFlag, (RAND()*(100 - @pItemDrop)) + @pItemDrop, 1, GetDate(), GetDate(), GetDate(), 0, 0)
		
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
		BEGIN
			RETURN(1)	-- db error
		END					
	END 
	
	RETURN(0)

GO

