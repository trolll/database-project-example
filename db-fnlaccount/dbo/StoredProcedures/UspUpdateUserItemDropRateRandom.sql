CREATE PROCEDURE [dbo].[UspUpdateUserItemDropRateRandom]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE dbo.TblUserItemDropRate
	SET mRandomValue = (RAND()*(100 - mItemDrop)) + mItemDrop
	WHERE mRandomFlag = 1 
		AND (	mItemDrop NOT IN (100) OR mPKDrop NOT IN (0)	)
		AND mUserNo IN (
			SELECT mUserNo
			FROM dbo.TblUser WITH(INDEX=NC_TblUser_mLoginTm)
			WHERE mLoginTm > GETDATE()-7	-- 老林老付促 府鹤
	)

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error 
	END

	RETURN (0)

GO

