CREATE PROCEDURE [WspGetNonClientUser]  	
	@pBeginDate CHAR(10),
	@pEndDate CHAR(10),
	@pCnt BIGINT = 0
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aBeginDate SMALLDATETIME,
			@aEndDate SMALLDATETIME
			
	SELECT @aBeginDate = @pBeginDate + ' 00:00:00',
			@aEndDate = @pEndDate + ' 23:59:59';
	
	SELECT 
		T1.mRegDate
		, T1.mUserNo
		, T1.mLastDiffNonCltKeyDate
		, T1.mCltKeyCnt
		, T2.mUserId
		, mIsBlock = 
			CASE
				WHEN T3.mCertify IS NULL THEN 0
				WHEN T3.mCertify >= GETDATE() THEN 1
				ELSE 0
			END 
		, T2.mWorldNo
	FROM dbo.TblUserNonClient T1
		INNER JOIN dbo.TblUser T2
			ON T1.mUserNO = T2.mUserNo
		LEFT OUTER JOIN dbo.TblUserBlock T3
			ON T1.mUserNo = T3.mUserNo
	WHERE T1.mLastDiffNonCltKeyDate BETWEEN @aBeginDate AND @aEndDate
				AND mCltKeyCnt >= @pCnt

GO

