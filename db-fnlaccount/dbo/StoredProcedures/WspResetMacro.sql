CREATE PROCEDURE dbo.WspResetMacro  
	@pUserNo	INT
	, @pUseMacro	SMALLINT		
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	UPDATE dbo.TblUser
	SET
		mUseMacro = @pUseMacro
	WHERE mUserNo = @pUserNo
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo <> 0  OR @aRowCnt <= 0
		RETURN(1)

    RETURN(0)

GO

