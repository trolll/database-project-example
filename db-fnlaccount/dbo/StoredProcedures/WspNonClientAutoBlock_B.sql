CREATE Procedure [dbo].[WspNonClientAutoBlock_B]                      
@blockterm INT                      
as                      
set nocount on                       
declare @seq int, @maxseq int, @mUserid char(20), @mUserno int, @mWorldNo smallint                      
declare @charnm char(12)                      
select @seq = isnull(max(seq), 1) from tblAutoBlock                      
insert into tblAutoBlock                      
select a.mUserno, mUserid, mWorldNo, convert(char(8), getdate(), 112) as Dates, '' as mNm                      
from tbluser a with (nolock)                      
inner join TblUserNonClient b with (nolock)                      
on a.mUserNo = b.mUserNo                      
where mWorldNo> 0 and b.mLastDiffNonCltKeyDate < dateadd(mi, -@blockterm, getdate())                      
select @maxseq = max(seq) from tblAutoBlock                      
while (@seq < @maxseq)                      
begin                      
 select @mUserid = muserid, @mUserno = mUserno, @mWorldNo = mWorldNo from tblAutoBlock where seq = @seq                      
 exec dbo.WspApplyCertifyPenalty @mUserid,45,14,36500,'使用未?可的program',null,'KR10643','批量封停'                      
 -- gameWorld                      
      
      
    
if (@mWorldNo = 1164)      
 exec FNLGame1164.dbo.WspGetCharNm @mUserno, @mNm = @charnm OUTPUT 
if (@mWorldNo = 1167)      
 exec FNLGame1167.dbo.WspGetCharNm @mUserno, @mNm = @charnm OUTPUT
if (@mWorldNo = 1168)      
 exec FNLGame1168.dbo.WspGetCharNm @mUserno, @mNm = @charnm OUTPUT
if (@mWorldNo = 1169)      
 exec FNLGame1169.dbo.WspGetCharNm @mUserno, @mNm = @charnm OUTPUT
if (@mWorldNo = 1170)      
 exec FNLGame1170.dbo.WspGetCharNm @mUserno, @mNm = @charnm OUTPUT
if (@mWorldNo = 1171)      
 exec FNLGame1171.dbo.WspGetCharNm @mUserno, @mNm = @charnm OUTPUT 
if (@mWorldNo = 1172)      
 exec FNLGame1172.dbo.WspGetCharNm @mUserno, @mNm = @charnm OUTPUT 
 
 update tblAutoBlock set mNm = @charnm where mUserno  = @mUserno                      
--  select convert(varchar, @seq), @charnm, convert(varchar, @mUserno)                      
 set @seq = @seq + 1                      
end                      
set nocount off

GO

