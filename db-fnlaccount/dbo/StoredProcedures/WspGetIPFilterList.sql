
CREATE   PROCEDURE [dbo].[WspGetIPFilterList]
AS
	SET NOCOUNT ON

	SELECT
		  i.mRegDate
		, CAST(mIpStx as VARCHAR) AS mIpStx
		, CAST(mIpEtx as VARCHAR) AS mIpEtx
		, i.mDesc
		, mOperator
	FROM
		[dbo].[TblIpFilterHistory] AS i WITH (NOLOCK)
	WHERE mDel_yn = 'N' 
	ORDER BY mRegDate DESC
	SET NOCOUNT OFF

GO

