
CREATE  PROCEDURE [dbo].[WspAllowIpFilter]
	  @mS1	BIGINT
	, @mS2	BIGINT
	, @mS3	BIGINT
	, @mS4	BIGINT
	, @mE1	BIGINT
	, @mE2	BIGINT
	, @mE3	BIGINT
	, @mE4	BIGINT
	, @mDelOperator VARCHAR(30)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE	@mIpStart	BIGINT
	DECLARE	@mIpEnd	BIGINT
	DECLARE	@aErrNo		INT

	SET @mIpStart = (@mS1 * 1000000000) + (@mS2 * 1000000) + (@mS3 * 1000) + (@mS4)
	SET @mIpEnd  = (@mE1 * 1000000000) + (@mE2 * 1000000) + (@mE3 * 1000) + (@mE4)

	IF ( EXISTS (SELECT 1 FROM dbo.TblIpFilter WHERE mIpStx = @mIpStart AND  mIpEtx = @mIpEnd )) 

	BEGIN
		
		DELETE dbo.TblIpFilter
		WHERE mIpStx = @mIpStart AND mIpEtx = @mIpEnd
		
		UPDATE dbo.TblIpFilterHistory
		SET mDel_yn = 'Y' , mDelOperator = @mDelOperator ,mDelDate = getdate()
		WHERE mIpStx = @mIpStart AND mIpEtx = @mIpEnd AND mDel_yn = 'N'

		IF @@ERROR <> 0
		BEGIN
			RETURN(1)	-- db error
		END

		RETURN(0)	
	END

	RETURN(2)	-- ?? ??? ??. 

GO

