
CREATE         PROCEDURE [dbo].[WspGetBadUserDetailInfo2] 
	@mUserId VARCHAR(20)	-- 아이디
AS
	SET NOCOUNT ON

	SELECT
		b.mRegDate, -- 처벌일
		b.mEndDate, -- 처벌종료일
		b.mType, -- 처벌방법
		b.mReasonCode, -- 처벌사유
		(SELECT mUserId FROM [dbo].[tblUser] WHERE mUserNo = b.mAttachUserID) mAttachUserId, -- 처리자 
		b.mReason, -- 처벌관련메모
		b.mDeatchDate, -- 수동복구일
		b.mDetachReason, -- 복구관련메모
		b.mSeqNo, -- 관리번호
		b.mReasonFile -- 첨부파일
	FROM 
		[dbo].[TblUserBlockHistory] As b
	INNER JOIN
		[dbo].[TblUser] As u
	ON
		b.mUserNo = u.mUserNo
	WHERE
		u.mUserId = @mUserId
		AND b.mType IN ( 15, 16, 17 )

GO

