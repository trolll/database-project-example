/******************************************************************************
**		File: UspApplyChatPenalty.sql
**		Name: UspApplyChatPenalty
**		Desc: 盲泼 喉钒 贸府 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2019.08.23	捞铰窍				矫胶袍 力力矫 @mAttachUserNo甫 0栏肺 贸府登档废 荐沥
*******************************************************************************/ 
create PROCEDURE [dbo].[UspApplyChatPenalty]
	@mUserId VARCHAR(20), -- 阂樊 捞侩磊 拌沥疙
	@mType INT, -- 贸国规过		
	@mReasonCode SMALLINT, -- 贸国荤蜡
	@mPenaltyHour INT, -- 力犁甫 啊窍绰 矫埃
	@mReason CHAR(200), -- 贸国包访皋葛
	@mReasonFile CHAR(200), -- 贸国包访颇老
	@mAttachUserNo INT -- 力犁甫 啊窍绰 捞侩磊 ID
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @mUserNo INT
			, @mChat INT
			, @mEndDate DATETIME
			, @mAttachUserID VARCHAR(20)
			
	SELECT	@mChat = @mPenaltyHour * 60
			, @mEndDate = DATEADD(HOUR, @mPenaltyHour, GETDATE())
		
	SELECT 
		@mUserNo = mUserNo 
	FROM dbo.TblUser
	WHERE mUserId = @mUserId
	
	IF @@ROWCOUNT <= 0 
		RETURN(1)	-- non user id
		
	IF @mAttachUserID <> 0 -- 0捞搁 辑滚俊辑 磊悼栏肺 力力甫 啊沁促
	BEGIN
		SELECT 
			@mAttachUserID = mUserID
		FROM dbo.TblUser
		WHERE mUserNo = @mAttachUserNo
		
		IF @@ROWCOUNT <= 0 
			RETURN(1)	-- non user no	
	END
	ELSE
	BEGIN
		SET @mAttachUserID = '';
	END

	BEGIN TRANSACTION

		IF (SELECT COUNT(*) 
			FROM [dbo].[TblUserBlock] 
			WHERE mUserNo = @mUserNo) = 0
		BEGIN	
			INSERT INTO
				[dbo].[TblUserBlock]
				(mRegDate, mUserNo, mChat, mCertifyReason)
			VALUES
				(GETDATE(), @mUserNo, @mChat, @mReason)	
		
		END
		ELSE
		BEGIN
			UPDATE 
				[dbo].[TblUserBlock]
			SET
				mRegDate = GETDATE(),
				mChat = @mChat,
				mCertifyReason = @mReason
			WHERE
				mUserNo = @mUserNo					
		END	
	
		IF @@ERROR <> 0
			GOTO ON_ERROR


		INSERT INTO [dbo].[TblUserBlockHistory]
			(mRegDate, mUserNo, mType, mEndDate, mReasonCode, mReason, mReasonFile, mAttachUserID)
		VALUES
			(GETDATE(), @mUserNo, @mType, @mEndDate, @mReasonCode, @mReason, @mReasonFile, @mAttachUserID)

		IF @@ERROR <> 0
			GOTO ON_ERROR

	COMMIT TRANSACTION
	RETURN 0

ON_ERROR:
	ROLLBACK TRANSACTION
	RETURN @@ERROR

GO

