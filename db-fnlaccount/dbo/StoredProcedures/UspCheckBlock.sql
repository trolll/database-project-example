
CREATE PROCEDURE dbo.UspCheckBlock
	 @pUserNo					INT,
	 @pUserChatBlockApplyTime	INT,				-- chatting block  适用时间 
	 @pBlockType				TINYINT OUTPUT,		-- block  TYPE 
	 @pChatRemainderTime		INT OUTPUT,			-- chatting block的情况下chatting 剩余时间 
	 @pIp						BIGINT	= NULL
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aCertify	DATETIME
	DECLARE @aBoard		DATETIME
	DECLARE @aChat		INT
			
	SELECT  @pBlockType = 0
			, @pChatRemainderTime = 0
			
	
	---------------------------------------
	-- chatting block 
	---------------------------------------
	IF @pUserChatBlockApplyTime IS NOT NULL 
		AND @pUserChatBlockApplyTime > 0 
	BEGIN		
		UPDATE dbo.TblUserBlock
		SET mChat = 
			CASE 
				WHEN mChat IS NULL THEN  NULL
				WHEN mChat > @pUserChatBlockApplyTime THEN  mChat - @pUserChatBlockApplyTime
				WHEN mChat <= @pUserChatBlockApplyTime THEN 0
			END 
		WHERE mUserNo = @pUserNo
		
		IF @@ERROR <> 0  
		BEGIN
			RETURN(1)
		END	
	END 
	
	---------------------------
	-- PS 用户与否进行确认 
	---------------------------
	IF EXISTS( SELECT T1.mUserID
				FROM dbo.TblPsUser T1
					INNER JOIN dbo.TblUser T2					
						ON T1.mUserID = T2.mUserID
				WHERE T2.mUserNo = @pUserNo )
	BEGIN
		SET @pBlockType = 1		-- 认证block的原因，TYPE返还处理 
		RETURN(0)	
	END 

	---------------------------
	-- IP block 与否 Check 
	---------------------------
	IF @pIp	IS NOT NULL
	BEGIN
		
		IF (SELECT COUNT(*) 
			FROM dbo.TblIpFilter 
			WHERE mIpStx <= @pIp AND mIpEtx >= @pIp) > 0
		BEGIN
			SET @pBlockType = 1		-- 认证block的原因，TYPE返还处理 
			RETURN(0)			
		END 	

	END 
	
	---------------------------------------
	-- chatting block 
	---------------------------------------
	SELECT 
		@aCertify = mCertify,
		@aBoard = mBoard,
		@aChat = mChat		
	FROM dbo.TblUserBlock
	WHERE mUserNo = @pUserNo

	IF @@ROWCOUNT > 0
	BEGIN
		-- 认证 block 	
		IF @aCertify IS NOT NULL 
			AND @aCertify >= GETDATE()
		BEGIN
			SET @pBlockType = 1	
			RETURN(0)
		END
			
		-- chatting block
		IF @aChat IS NOT NULL 
			AND @aChat >= 0
		BEGIN
			SET @pBlockType = 2
			SET @pChatRemainderTime = @aChat
			RETURN(0)
		END	
	END 
	
	RETURN(0)

GO

