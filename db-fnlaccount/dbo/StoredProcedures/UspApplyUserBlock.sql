/******************************************************************************
**		File: UspApplyUserBlock.sql
**		Name: UspApplyUserBlock
**		Desc: 厚傍侥 荤侩磊肺 魄喊捞 登绢 拌沥 喉钒 
**
**		Auth: J.S.H
**		Date: 2007.08.13
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2007.12.14	JUDY				喉钒 荤蜡 殿废
**		2008.01.25	JUDY				BLOCKING 啊秦磊拌沥 殿废
**		2008.02.13	JUDY				吝惫苞狼 龋券己 蜡瘤甫 困秦辑 mAttachNo 拿烦沥焊 力芭 措矫 mAttachUserID 殿废	
**		2010-05-18	辫堡挤				康备力犁甫 狼固窍绰 朝楼 函版
*******************************************************************************/ 
CREATE PROCEDURE dbo.UspApplyUserBlock
	@pUserNo	INT,
	@pCertifyReason	VARCHAR(200) = NULL,
	@pBlockDay	INT	= NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aBlockEndday DATETIME
	SET @aBlockEndday = '7777-07-07 00:00:00';	-- 康备 力犁甫 狼固窍绰 朝楼. -> 扁粮俊绰 '2020-01-01'
		
	IF @pCertifyReason IS NULL
		SET @pCertifyReason = 'informal user'
	
	IF @pBlockDay IS NOT NULL
		SET @aBlockEndday = DATEADD(DAY, @pBlockDay, GETDATE())

	BEGIN TRANSACTION

		IF (SELECT COUNT(*) FROM [dbo].[TblUserBlock] WHERE mUserNo = @pUserNo) = 0
		BEGIN	
			INSERT INTO
				[dbo].[TblUserBlock]
				(mRegDate, mUserNo, mCertify, mCertifyReason)
			VALUES
				(GETDATE(), @pUserNo, @aBlockEndday, @pCertifyReason)	
		
		END
		ELSE
		BEGIN
			UPDATE 
				[dbo].[TblUserBlock]
			SET
				mRegDate = GETDATE(),
				mCertify = @aBlockEndday,
				mCertifyReason = @pCertifyReason
			WHERE
				mUserNo = @pUserNo					
		END	
	
		IF @@ERROR <> 0
			GOTO ON_ERROR

		INSERT INTO [dbo].[TblUserBlockHistory]
			(mRegDate, mUserNo, mType, mEndDate, mReasonCode, mReason, mReasonFile,  mAttachUserID)
		VALUES
			(GETDATE(), @pUserNo, 13, @aBlockEndday, 31, @pCertifyReason, NULL, 'NON-PC')

		IF @@ERROR <> 0
			GOTO ON_ERROR

	COMMIT TRANSACTION
	RETURN 0

ON_ERROR:
	ROLLBACK TRANSACTION
	RETURN @@ERROR

GO

