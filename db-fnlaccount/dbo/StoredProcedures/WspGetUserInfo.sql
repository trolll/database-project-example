CREATE PROCEDURE [dbo].[WspGetUserInfo]
	@mUserId VARCHAR(20)
	, @SearchType	TINYINT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aUserNo INT
	SELECT @aUserNo = -2147483648;
	
	IF @SearchType = 0
	BEGIN
		SELECT 
			TOP 1
				@aUserNo = mUserNo
		FROM dbo.TblUser
		WHERE mUserID = @mUserId
	END
	
	IF @SearchType = 1
	BEGIN
		-- 茄霸烙 拌沥 炼雀 
		SELECT 
			TOP 1
				@aUserNo = mUserNo
		FROM dbo.TblUserHanGame
		WHERE mUserID = @mUserId				
	END	
	
	IF @SearchType = 2
	BEGIN
		-- 昆哩 捞包 脚没磊 拌沥 炼雀
		SELECT 
			TOP 1
				@aUserNo = mUserNo
		FROM dbo.TblNnhToWebzenTransfer
		WHERE mUserName = @mUserId				
	END		
			
	EXEC Dbo.WspGetUserInfoA @aUserNo

GO

