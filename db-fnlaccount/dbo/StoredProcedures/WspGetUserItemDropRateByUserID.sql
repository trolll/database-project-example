/******************************************************************************
**		File: WspGetUserItemDropRateByUserID.sql
**		Name: WspGetUserItemDropRateByUserID
**		Desc: 力犁 沥焊甫 掘绢柯促.
**
**              
**		Auth: 眠槛
**		Date: 2010.06.08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[WspGetUserItemDropRateByUserID]
	@pUserID	VARCHAR(20)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT 
		T1.mUserID
		, T2.mUserNO
		, mItemDrop
		, mPKDrop
		, mRandomFlag
		, mRandomValue
		, mSetCount
		, T2.mRegDate
		, mRenewalDate
		, mCheckRegWeek
		, mPShopTm
		, mTotalTm		
	FROM dbo.TblUser T1
		INNER JOIN dbo.TblUserItemDropRate T2
				ON T1.mUserNo = T2.mUserNo
	WHERE mUserID = @pUserID

GO

