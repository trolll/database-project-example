

CREATE         PROCEDURE [dbo].[WspApplyBoardPenalty]	
	@mUserId CHAR(20), -- ?? ??? ???
	@mType INT, -- ????		
	@mReasonCode SMALLINT, -- ????
	@mPenaltyHour INT, -- ??? ??? ??
	@mReason CHAR(200), -- ??????
	@mReasonFile CHAR(200), -- ??????
	@mAttachUserID Varchar(50), -- ??? ??? ??? ID
             @mComment nvarchar(1000)
AS
	SET NOCOUNT ON

	DECLARE @mUserNo INT
	DECLARE @mBoard DATETIME

	SELECT @mUserNo = mUserNo FROM [dbo].[TblUser] WHERE mUserId = @mUserId
	SELECT @mBoard = DATEADD(HOUR, @mPenaltyHour, GETDATE())

	BEGIN TRANSACTION
	
	IF (SELECT COUNT(*) FROM [dbo].[TblUserBlock] WHERE mUserNo = @mUserNo) = 0
	BEGIN
		INSERT INTO
			[dbo].[TblUserBlock]
			(mRegDate, mUserNo, mBoard, mCertifyReason)
		VALUES
			(GETDATE(), @mUserNo, @mBoard, @mReason)			 
	END
	ELSE
	BEGIN
		UPDATE 
			[dbo].[TblUserBlock]
		SET
			mRegDate = GETDATE(),
			mBoard = @mBoard,
			mCertifyReason = @mReason
		WHERE
			mUserNo = @mUserNo			
	END

	-- ???	
	IF @@ERROR <> 0
		GOTO ON_ERROR

	INSERT INTO
		[dbo].[TblUserBlockHistory]
		(mRegDate, mUserNo, mType, mEndDate, mReasonCode, mReason, mReasonFile, mAttachUserID,mComment)
	VALUES
		(GETDATE(), @mUserNo, @mType, @mBoard, @mReasonCode, @mReason, @mReasonFile, @mAttachUserID,@mComment)

	-- ???
	IF @@ERROR <> 0
		GOTO ON_ERROR

	COMMIT TRANSACTION
	RETURN 0

ON_ERROR:
	ROLLBACK TRANSACTION
	RETURN @@ERROR

GO

