
CREATE      PROCEDURE [dbo].[WspAccountTransfer]
	@mSID 	VARCHAR(20),	-- 정보가 수정될 아이디  (후에 존재하지 않음) SID 가 DID로 변경
	@mDID	VARCHAR(20)	-- 삭제될 아이디 , 없으면 삭제 안됨, (후에 존재하는 ID)
AS
	SET NOCOUNT ON
	DECLARE @aErrNo	INT
	DECLARE @mReturn	INT
	DECLARE @mFlag		BIT

	SET @mFlag 	= 0
	SET @aErrNo 	= 0

	IF NOT EXISTS(SELECT * FROM TblUser WITH (NOLOCK) WHERE mUserId = @mSID)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END
	 END

	IF NOT EXISTS(SELECT  * FROM TblUser WITH (NOLOCK) WHERE mUserId = @mSID AND mWorldNo <= 0 )
 	 BEGIN
		SET @aErrNo = 2
		GOTO LABEL_END
	 END

	IF EXISTS (SELECT * FROM TblUser WITH (NOLOCK) WHERE mUserId = @mDID)
 	 BEGIN
		IF NOT EXISTS (SELECT * FROM TblUser WITH (NOLOCK) WHERE mUserId = @mDID AND mWorldNo  <= 0) 
		 BEGIN
			SET @aErrNo = 4
			GOTO LABEL_END
		 END
		
		exec @mReturn = UspDeleteUser @mDID

		IF (@mReturn <> 0)
		 BEGIN
			SET @aErrNo = 5
			GOTO LABEL_END
		 END
	 END
	
	SET @mFlag = 1

	BEGIN TRAN
	UPDATE TblUser SET mUserId = @mDID WHERE mUserId = @mSID

	IF @@ROWCOUNT <> 1
	BEGIN
		SET @aErrNo = 6
		GOTO LABEL_END
	END
	

LABEL_END:
	IF (1 = @mFlag)
	BEGIN
		IF (0 = @aErrNo)
			COMMIT TRAN
		ELSE
			ROLLBACK TRAN
	END 

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

