CREATE PROCEDURE [dbo].[WspDetachPenalty]	
	@mSeqNo BIGINT, -- 力犁 包府锅龋
	@mUserId VARCHAR(20), -- 阂樊 捞侩磊 拌沥疙
	@mDetachReason CHAR(200), -- 汗备包访皋葛
	@mDetachUserID VARCHAR(50) -- 力犁甫 啊窍绰 捞侩磊 ID
AS
	SET NOCOUNT ON

	DECLARE @mUserNo INT
			, @mUseMacro	INT
	
	SELECT @mUseMacro = 0 , @mUserNo = 0		

	SELECT 
		@mUserNo = mUserNo 
		, @mUseMacro = mUseMacro
	FROM [dbo].[TblUser] 
	WHERE mUserId = @mUserId

	BEGIN TRANSACTION

		IF (SELECT COUNT(*) FROM [dbo].[TblUserBlock] WHERE mUserNo = @mUserNo) = 1		
		BEGIN
			DELETE FROM
				[dbo].[TblUserBlock]
			WHERE
				mUserNo = @mUserNo

			IF @@ERROR <> 0
				GOTO ON_ERROR
		END
		
		
		UPDATE
			[dbo].[TblUserBlockHistory]
		SET
			mDetachUserID = @mDetachUserID
			, mDetachReason = @mDetachReason
			, mDeatchDate = GETDATE()
		WHERE
			mSeqNo = @mSeqNo
	
		IF @@ERROR <> 0
			GOTO ON_ERROR
			
		-- 厚橇荤 吧妨 乐栏搁, 鞍捞 秦力茄促. 	
		IF (@mUseMacro <> 0) 
		BEGIN
		
			UPDATE dbo.TblUser
			SET mUseMacro = 0
			WHERE mUserNo = @mUserNo
			
			IF @@ERROR <> 0
				GOTO ON_ERROR
		END 

	COMMIT TRANSACTION
	RETURN 0

ON_ERROR:
	ROLLBACK TRANSACTION
	RETURN @@ERROR

GO

