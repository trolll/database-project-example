



CREATE    PROCEDURE [dbo].[WspWriteUserMemoA]
	@mUserNo INT,
	@mMemoText VARCHAR(100) -- ?? ?? 50?
AS
	SET NOCOUNT ON

	BEGIN TRANSACTION

	INSERT INTO
		[dbo].[TblUserMemo]
		(mUserNo, mMemoText)
	VALUES
		(@mUserNo, @mMemoText)

	IF @@IDENTITY > 0 AND @@ERROR = 0
		BEGIN
			COMMIT TRANSACTION
			RETURN 0
		END
	ELSE
		BEGIN
			ROLLBACK TRANSACTION
			RETURN 1
		END

GO

