
CREATE PROCEDURE [dbo].[WspIpUserListByCount]
	  @count	INT
AS
	SET NOCOUNT ON

	
	BEGIN
		SELECT
			 mUserId,mip,mworldno
		FROM 
			TblUser  WITH (NOLOCK)
		 WHERE 
			mIp in (
		
				SELECT 
					mIp 
				FROM 
					TblUser WITH (NOLOCK)
					where mworldno>0   GROUP BY mIp HAVING count(mIp)>@count
			)  --and   mworldno>0 
			  order by mip
	END

GO

