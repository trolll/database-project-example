
CREATE       Procedure dbo.WspInsertBlackUser
	  @mUserId	VARCHAR(20)		-- 계정
	, @mDesc	VARCHAR(200)		-- 사유
	, @mErrorCode	SMALLINT OUTPUT	-- 에러코드
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	IF  ( SELECT COUNT (*) FROM TblUser WHERE mUserId = @mUserId) > 0
	BEGIN
		IF  ( SELECT COUNT (*) FROM TblUserBlack WHERE mUserId = @mUserId ) > 0
		BEGIN
			SET	@mErrorCode = 2
		END
		ELSE
		BEGIN
			SET	@mErrorCode = 1
			INSERT INTO TblUserBlack VALUES (GETDATE(), @mUserId, @mDesc)
		END
	END

	ELSE
		SET	@mErrorCode = -1
	
	SET NOCOUNT OFF

GO

