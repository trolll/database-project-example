
CREATE  PROCEDURE dbo.UspGetUserNo
	@pUserId		CHAR(20),
	@pUserNo		INT OUTPUT
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT,
			@aRowCnt	INT,
			@aAuthNo	INT
			
	SELECT	@aErrNo = 0, @aRowCnt = 0, @aAuthNo = 0
	
	SELECT 
		@pUserNo = mUserNo,
		@aAuthNo = mUserAuth
	FROM dbo.TblUser
	WHERE mUserId =  @pUserId
			AND mDelDate = '1900-01-01'
		
	SELECT  @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR
	IF(0 <> @aErrNo) OR (0 = @aRowCnt)
	BEGIN
		RETURN(1)	-- 현재 계정 정보가 존재하지 않다. 
	END	
	
	IF @aAuthNo = 0
	BEGIN
		SET @pUserNo = NULL
		RETURN(2)	-- 블럭된 사용자이거나 다른 정보의 유저일 경우 
	END 
		
	RETURN(0)

GO

