/******************************************************************************
**		Name: WspSetUserItemDropRateByUserNo
**		Desc: 蜡历狼 靛而啦/PK靛而啦 汲沥
**
**		Auth: 眠槛
**		Date: 10.06.08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspSetUserItemDropRateByUserNo]
	@pUserNo			VARCHAR(2000)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aUserNo	INT

	SELECT @aErrNo = 0, @aRowCnt = 0, @aUserNo = 0;
		
	
	UPDATE dbo.TblUserItemDropRate
	SET
		mItemDrop = 0		-- A 殿鞭
		, mPKDrop = 0		-- A 殿鞭
		, mRandomValue = 100		
		, mRenewalDate = GETDATE()
	WHERE mUserNo IN (	
		SELECT 
			CONVERT(INT, ELEMENT) mUserNo
		FROM dbo.UfnSplitSQL(@pUserNo,',')
	)
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	
	IF @aErrNo <>  0
	BEGIN
		RETURN(1)	-- db error
	END	

	RETURN(0)

GO

