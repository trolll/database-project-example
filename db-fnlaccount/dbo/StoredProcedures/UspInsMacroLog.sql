CREATE PROCEDURE dbo.UspInsMacroLog  	
	@pUserNo INT
	, @pMacroCnt	INT
AS
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0;   
	
	UPDATE dbo.TblUseMacro
	SET
		mUptDate = GETDATE()
	WHERE mUserNo = @pUserNo
		AND mUseMacroCnt = @pMacroCnt;
		
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;   
	IF (@aErrNo <> 0)
		RETURN(1);
	
	IF (@aRowCnt = 0)
	BEGIN	
		INSERT INTO  dbo.TblUseMacro(mUserNo,mUseMacroCnt)
		VALUES( @pUserNo, @pMacroCnt);

		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;   
		IF (@aErrNo <> 0)
			RETURN(1);				
	END 

    RETURN(0);

GO

