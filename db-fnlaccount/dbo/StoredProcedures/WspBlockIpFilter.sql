
CREATE PROCEDURE [dbo].[WspBlockIpFilter]
	  @mS1	BIGINT
	, @mS2	BIGINT
	, @mS3	BIGINT
	, @mS4	BIGINT
	, @mE1	BIGINT
	, @mE2	BIGINT
	, @mE3	BIGINT
	, @mE4	BIGINT
	, @mDesc	CHAR(100)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE @mIp NCHAR(15)
	DECLARE @mDClass TINYINT,
		    @mCClass TINYINT,
			@mPcbangIpCnt	INT
	
	DECLARE @mRowCont	INT
	DECLARE @mErrNo		INT
	DECLARE @mIPTable	TABLE(
		mSeq	INT IDENTITY(1,1),
		mIP		NVARCHAR(15)
	)
		
	SELECT	@mDClass = 0,
			@mRowCont = 0,
			@mErrNo = 0,
			@mCClass = @mS3,
			@mDClass = @mS4,
			@mPcbangIpCnt = 0
			
				
	DECLARE	@mIpStart	BIGINT
	DECLARE	@mIpEnd	BIGINT
	DECLARE	@aErrNo		INT


	----------------------------------------------------------------------------
	-- 존재 대여폭 체크
	----------------------------------------------------------------------------	
	SET @mIpStart = (@mS1 * 1000000000) + (@mS2 * 1000000) + (@mS3 * 1000) + (@mS4)
	SET @mIpEnd  = (@mE1 * 1000000000) + (@mE2 * 1000000) + (@mE3 * 1000) + (@mE4)
	
	SELECT @mPcbangIpCnt = COUNT(*) 
	FROM dbo.TblIpFilter 
	WHERE mIpStx <= @mIpStart AND @mIpStart <= mIpEtx
	
	IF @mPcbangIpCnt > 0 
	BEGIN
		RETURN(2)	
	END	
	
	SET @mPcbangIpCnt = 0	
						
	----------------------------------------------------------------------------
	-- C, D Class 체크
	----------------------------------------------------------------------------			
	WHILE(1>0)
	BEGIN		
		SET @mIp =	CONVERT(NVARCHAR, @mS1) + '.' 
		SET @mIp =	@mIp + CONVERT(NVARCHAR, @mS2) + '.'
		SET @mIp =	@mIp + CONVERT(NVARCHAR, @mCClass) + '.'
		SET @mIp =	@mIp + CONVERT(NVARCHAR, @mDClass)		

		INSERT @mIPTable(mIP)  VALUES(@mIp)
		
		IF ( @mCClass = @mE3 ) AND ( @mE4 = @mDClass )
		BEGIN
			BREAK	-- 종료			
		END
					
		IF ( @mCClass <= @mE3 ) AND ( @mDClass = 255 )
		BEGIN
			SET @mCClass =  @mCClass + 1
			SET @mDClass = 0
			CONTINUE
		END			
		
		IF ( @mCClass<= @mE3 ) AND ( @mDClass < 255 )
		BEGIN
			SET @mDClass =  @mDClass + 1
			CONTINUE
		END	
	END

	----------------------------------------------------------------------------
	-- PCBang IP여부 체크 
	-- D 클래스 IP 대역폭 크기만 확인한다. 
	----------------------------------------------------------------------------
	SELECT @mPcbangIpCnt = COUNT(CustIP)
	FROM FNLBilling.dbo.TblPayPcBangIp
	WHERE CustIP IN (
		SELECT 
			mIP
		FROM @mIPTable
	)
	
	IF @mPcbangIpCnt > 0
	BEGIN
		RETURN (1)
	END 

	----------------------------------------------------------------------------
	-- IP차단 대역폭 등록
	----------------------------------------------------------------------------
	IF (SELECT COUNT(*) 
		FROM TblIpFilter 
		WHERE mIpStx <= @mIpStart AND @mIpStart <= mIpEtx) = 0
	BEGIN


		INSERT INTO dbo.TblIpFilter 
		VALUES(GETDATE(), @mIpStart, @mIpEnd, @mDesc)

		IF @@ERROR <> 0
		BEGIN
			RETURN(1)	-- error
		END	

		RETURN(0)
	END -- IF (SELECT)
	ELSE
	BEGIN
		RETURN(2)
	END

	SET NOCOUNT OFF
	RETURN(0)

GO

