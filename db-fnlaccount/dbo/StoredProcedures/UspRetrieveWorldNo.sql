/******************************************************************************  
**  Name: UspRetrieveWorldNo  
**  Desc: WorldNo甫 府畔茄促.
**  
**                
**  Return values:  
**   亲惑 0
**                
**  Author: 辫碍龋  
**  Date: 2011-01-24  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspRetrieveWorldNo]  
	  @pUserId   VARCHAR(20)  
	  ,@pWorldNo   SMALLINT  OUTPUT  
AS
		SET NOCOUNT ON   
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

		SELECT   
		 @pWorldNo=a.[mWorldNo]
		FROM dbo.TblUser  AS a   
		WHERE (a.[mUserId] = @pUserId) AND ( a.[mDelDate] = '1900-01-01' );

		IF(1 <> @@ROWCOUNT)  
		BEGIN
			SET @pWorldNo = 0;
		END

		RETURN(0);

GO

