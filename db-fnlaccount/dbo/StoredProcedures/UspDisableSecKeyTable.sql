CREATE PROCEDURE	dbo.UspDisableSecKeyTable
	@pUserNo		INT,		-- 荤侩磊 拌沥锅龋
	@pErrNoStr 		VARCHAR(50) OUTPUT
AS
/******************************************************************************
**		File: UspDisableSecKeyTable.sql
**		Name: UspDisableSecKeyTable
**		Desc: SC 秦瘤 窍扁. 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2011.12.30	捞柳急				SC 墨靛 秦瘤矫 肺弊 眠啊
*******************************************************************************/ 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	SET LOCK_TIMEOUT	2000;

	DECLARE	@aErrNo	INT,
			@aRowCount	INT
			
	SELECT	@aRowCount = 0,
			@aErrNo	= 0,
			@pErrNoStr	= 'eErrNoSqlInternalError'


	IF NOT EXISTS(	SELECT mUserNo 
					FROM dbo.TblUser WITH (NOLOCK) 
					WHERE mUserNo = @pUserNo)
	BEGIN
		SET	@pErrNoStr	= 'eErrNoUserNotExistId4'
		RETURN(1)		-- 荤侩磊 沥焊啊 粮犁窍瘤 臼促.
	END
		
	--------------------------------------------------------
	-- SC 粮犁 咯何 眉农
	--------------------------------------------------------
	IF EXISTS(		SELECT mUserNo 
					FROM dbo.TblUserSecKeyTable WITH (NOLOCK) 
					WHERE mUserNo = @pUserNo )
	BEGIN
		SELECT	@aRowCount = 1
	END	


	BEGIN TRANSACTION

		--------------------------------------------------------
		-- SC 厚劝己老 函版 
		--------------------------------------------------------
		IF @aRowCount > 0
		BEGIN
			UPDATE dbo.TblUserSecKeyTable 
			SET mReleaseDate = getdate()		
			WHERE mUserNo = @pUserNo
		END
		ELSE
		BEGIN
			INSERT INTO dbo.TblUserSecKeyTable (mUserNo, mReleaseDate)
			VALUES(@pUserNo, getdate())
		END

		SELECT  @aErrNo = @@ERROR,
				@aRowCount = @@ROWCOUNT
		
		IF @aErrNo <> 0 OR @aRowCount <> 1
		BEGIN
			SET @pErrNoStr	= 'eErrSqlInternalError'				
			RETURN(@aErrNo)
		END			

		--------------------------------------------------------
		-- SC 厚劝己 
		--------------------------------------------------------
		UPDATE dbo.TblUser 
		SET mSecKeyTableUse = 0		
		WHERE mUserNo = @pUserNo

		SELECT  @aErrNo = @@ERROR,
				@aRowCount = @@ROWCOUNT
		
		IF @aErrNo <> 0 OR @aRowCount <> 1
		BEGIN
			SET @pErrNoStr	= 'eErrSqlInternalError'				
			RETURN(@aErrNo)
		END

		--------------------------------------------
		-- 2011.12.30 SC 墨靛 脚没矫 肺弊 眠啊 
		--------------------------------------------
		INSERT INTO dbo.TblSCCardUseHistory (mRegDate, mUserNo, mSecKeyTableUse)
		VALUES ( GETDATE(), @pUserNo, 0 )

		SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;

		IF (@aRowCount = 0 OR @aErrNo <> 0)
		BEGIN
			ROLLBACK TRAN
			RETURN(@aErrNo)
		END
	
	COMMIT TRAN
	RETURN(0)
End

GO

