CREATE PROCEDURE [dbo].[WspSameIpUser]
	@mYear 		INT, -- 八祸 斥档
	@mMonth	INT, -- 八祸 岿
	@mDay		INT, -- 八祸 老
	@mHour		INT, -- 八祸矫埃
	@mCnt		INT, -- 厚背 墨款飘
	@mFlag		INT, -- 敲贰弊
	@mSvrNo	INT  -- 辑滚 锅龋

AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@mStartDate	DATETIME
	DECLARE	@mRowCount	INT

	SET @mRowCount = 0
	SET @mStartDate = CAST(STR(@mYear) + '-' + STR(@mMonth) + '-' + STR(@mDay) + ' ' + STR(@mHour) + ':00:00'  AS DATETIME)	

	IF (@mFlag = 0)
	BEGIN
		IF (@mSvrNo = 0)
		 BEGIN
			SELECT 
				mIp, 
				COUNT(*) AS Cnt,
				0 AS	mPcNo
			FROM 
				dbo.TblUser T1 WITH (NOLOCK)
			WHERE
				mLoginTm >= @mStartDate 
			GROUP BY 
				mIp
			HAVING
				COUNT(*) >= @mCnt
			ORDER BY
				Cnt
			DESC
		 END
		ELSE
		 BEGIN
			SELECT 
				mIp, 
				COUNT(*) AS Cnt,
				0 AS	mPcNo
			FROM 
				dbo.TblUser
			WHERE
				mLoginTm >= @mStartDate 
					AND mWorldNo = @mSvrNo
			GROUP BY 
				mIp
			HAVING
				COUNT(*) >= @mCnt
			ORDER BY
				Cnt
			DESC
		 END
	END
	ELSE
	BEGIN
		IF (@mSvrNo = 0)
		 BEGIN
			SELECT 
				mIp, 
				COUNT(*) AS Cnt,
				mPcBangLv
			FROM 
				dbo.TblUser 
			WHERE
				mLoginTm >= @mStartDate 
					AND mWorldNo >  0
			GROUP BY 
				mIp,mPcBangLv
			HAVING
				COUNT(*) >= @mCnt
			ORDER BY
				Cnt
			DESC
		 END
		ELSE
		 BEGIN
			SELECT 
				mIp, 
				COUNT(*) AS Cnt,
				mPcBangLv
			FROM 
				dbo.TblUser 
			WHERE
				mLoginTm >= @mStartDate 
					AND mWorldNo = @mSvrNo
			GROUP BY 
				mIp,mPcBangLv
			HAVING
				COUNT(*) >= @mCnt
			ORDER BY
				Cnt
			DESC
		 END
	END

	SET @mRowCount = @@ROWCOUNT

	SET NOCOUNT OFF
	RETURN @mRowCount

GO

