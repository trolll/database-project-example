/******************************************************************************
**		File: WspGetUserItemDropRate.sql
**		Name: WspGetUserItemDropRate
**		Desc: 力犁 沥焊甫 掘绢柯促.
**
**              
**		Auth: 眠槛
**		Date: 2010.06.08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[WspGetUserItemDropRate]
	@pRandomFlag BIT
	, @pItemDropS TINYINT
	, @pItemDropE TINYINT
	, @pPKDropS TINYINT
	, @pPKDropE TINYINT
	, @pSetCountS SMALLINT
	, @pSetCountE SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT 
		T1.mUserID
		, T1.mUserNo
		, mItemDrop
		, mPKDrop
		, mRandomFlag
		, mRandomValue
		, mSetCount
		, T2.mRegDate
		, mRenewalDate
		, mCheckRegWeek
		, mPShopTm
		, mTotalTm
	FROM dbo.TblUserItemDropRate T2
		INNER LOOP JOIN dbo.TblUser T1
				ON T1.mUserNo = T2.mUserNo
	WHERE mRandomFlag = @pRandomFlag
		AND ( mItemDrop BETWEEN @pItemDropE AND @pItemDropS )
		AND ( mPKDrop BETWEEN @pPKDropS AND	 @pPKDropE )
		AND ( mSetCount BETWEEN @pSetCountS AND @pSetCountE )

GO

