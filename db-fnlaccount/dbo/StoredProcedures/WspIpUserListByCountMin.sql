CREATE  PROCEDURE dbo.WspIpUserListByCountMin
@count INT
AS
SET NOCOUNT ON

select z.mip, z.ltms, b.mLoginTm, b.mUserId, b.mworldno,
	case when c.mLastDiffNonCltKeyDate is null then 'Y' else 'N' end as isCtlKeyNull
from (
	select mip, LTms
	from (
		select mIp, left(LTm, 14) + 
		case 	when right(LTm, 1) = '1' then '0'
			when right(LTm, 1) = '3' then '2'
			when right(LTm, 1) = '5' then '4' 
			else right(LTm, 1) end + '0' as LTms
		from (
			select mIp, convert(char(15), mLoginTm, 120) as LTm
			FROM TblUser WITH (NOLOCK)
			where mworldno>0 
			GROUP BY mIp, convert(char(15), mLoginTm, 120)
 			HAVING count(mIp)> @count
		) x
	) y
	group by mip, LTms
) z
inner join tbluser b with (nolock)
on z.mip = b.mip
and convert(char(16), b.mLoginTm, 120) >= z.ltms
and convert(char(16), b.mLoginTm, 120) < convert(char(16), dateadd(mi, 20, convert(datetime, z.ltms)), 120)
left outer join TblUserNonClient c with (nolock)
on b.mUserNo = c.mUserNo
where b.mworldno > 0
order by z.mip, b.mLoginTm

GO

