CREATE PROCEDURE [dbo].[UspSetUserState]
	@pUserNo			INT
	,@pPShopOpeningTm	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE		@aErrNo		INT
				, @aRowCnt	INT;

	SELECT @aErrNo = 0, @aRowCnt = 0;

	UPDATE [dbo].[TblUserState]
	SET
		mPShopOpeningTm =
			mPShopOpeningTm + @pPShopOpeningTm
	WHERE
		mUserNo = @pUserNo					

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;

	IF @aErrNo <> 0
	BEGIN
		RETURN(1)	-- sql error 
	END

	IF @aRowCnt = 0
	BEGIN
		INSERT INTO 
			[dbo].[TblUserState]
			([mUserNo], [mPShopOpeningTm])
		VALUES
			(@pUserNo, @pPShopOpeningTm);

		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;

		IF @aErrNo <> 0
		BEGIN
			RETURN(1)	-- sql error 
		END
	END 

	RETURN(0);

GO

