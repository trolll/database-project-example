/******************************************************************************
**		Name: UspSetUserItemDropRate
**		Desc: 蜡历狼 靛而啦/PK靛而啦 汲沥
**
**		Auth: 沥备柳
**		Date: 10.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetUserItemDropRate]
	@pUserNo			INT
	,	@pItemDrop		TINYINT
	,	@pPKDrop		TINYINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE		@aItemDrop	TINYINT
	DECLARE		@aPKDrop	TINYINT
	DECLARE		@aCount		TINYINT

	SET @aItemDrop  = 0
	SET	@aPKDrop	= 0
	SET @aCount		= 0

	-- 泅犁 力犁等 蜡历 老 版快
	SELECT @aItemDrop = mItemDrop, @aPKDrop = mPKDrop
	FROM dbo.TblUserItemDropRate
	WHERE mUserNo = @pUserNo

	IF 0 = @@ROWCOUNT
	BEGIN
		-- 力犁 拌沥 眠啊
		INSERT INTO dbo.TblUserItemDropRate 
		VALUES (@pUserNo, @pItemDrop, @pPKDrop, 1, (RAND()*(100 - @pItemDrop)) + @pItemDrop, 1, GetDate(), GetDate(), GetDate(), 0, 0)
	END
	ELSE
	BEGIN
		-- A/A殿鞭老 版快父 促矫 汲沥阑 利侩茄促.
		IF @aItemDrop IN (100) AND @aPKDrop IN (0)
		BEGIN
			-- 靛而啦 汲沥 棺 眉农 何盒 檬扁拳
			RETURN(2)
		END
		ELSE
		BEGIN
			-- 捞固 力犁等 蜡历
			RETURN(1)
		END
	END

	RETURN(0)

GO

