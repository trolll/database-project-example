
CREATE  PROCEDURE [dbo].WspBlockIpFilterNonPc
	  @mS1	BIGINT
	, @mS2	BIGINT
	, @mS3	BIGINT
	, @mS4	BIGINT
	, @mE1	BIGINT
	, @mE2	BIGINT
	, @mE3	BIGINT
	, @mE4	BIGINT
	, @mDesc	CHAR(100)
	, @mOperator VARCHAR(30)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE @mIp NCHAR(15)
	DECLARE @mDClass TINYINT,
		    @mCClass TINYINT,
			@mPcbangIpCnt	INT
	
	DECLARE @mRowCont	INT
	DECLARE @mErrNo		INT
	DECLARE @mIPTable	TABLE(
		mSeq	INT IDENTITY(1,1),
		mIP		NVARCHAR(15)
	)
		
	SELECT	@mDClass = 0,
			@mRowCont = 0,
			@mErrNo = 0,
			@mCClass = @mS3,
			@mDClass = @mS4,
			@mPcbangIpCnt = 0
			
				
	DECLARE	@mIpStart	BIGINT
	DECLARE	@mIpEnd	BIGINT
	DECLARE	@aErrNo		INT


	----------------------------------------------------------------------------
	-- ?? ??? ??
	----------------------------------------------------------------------------	
	SET @mIpStart = (@mS1 * 1000000000) + (@mS2 * 1000000) + (@mS3 * 1000) + (@mS4)
	SET @mIpEnd  = (@mE1 * 1000000000) + (@mE2 * 1000000) + (@mE3 * 1000) + (@mE4)
	
	----------------------------------------------------------------------------
	-- IP?? ??? ??
	----------------------------------------------------------------------------
	IF (NOT EXISTS(SELECT 1 
		FROM TblIpFilter 
		WHERE mIpStx <= @mIpStart AND @mIpStart <= mIpEtx))
	BEGIN


		INSERT INTO dbo.TblIpFilter 
		VALUES(GETDATE(), @mIpStart, @mIpEnd, @mDesc)
	
		INSERT INTO dbo.TblIpFilterHistory(mRegDate,mIpStx,mIpEtx,mDesc,mOperator,mDel_yn) 
		VALUES(GETDATE(), @mIpStart, @mIpEnd, @mDesc, @mOperator, 'N')

		IF @@ERROR <> 0
		BEGIN
			RETURN(1)	-- error
		END	

		RETURN(0)
	END -- IF (SELECT)
	ELSE
	BEGIN
		RETURN(2)
	END

	SET NOCOUNT OFF
	RETURN(0)

GO

