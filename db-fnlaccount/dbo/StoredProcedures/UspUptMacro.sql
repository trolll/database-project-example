/******************************************************************************
**		File: UspUptMacro.sql
**		Name: UspUptMacro
**		Desc: 厚.橇.荤 坷翠 冉荐 诀单捞飘
**
**		Auth: chose96
**		Date: 2008.08.19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20090902	JUDY				厚橇荤 肺弊 眠啊 沥狼
*******************************************************************************/ 
CREATE PROCEDURE dbo.UspUptMacro
	  @pUserNo  		INT
	, @pUseMacroTimes	INT	-- 厚.橇.荤 坷翠冉荐
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aErrNo		INT
	SET @aErrNo = 0
	
	UPDATE dbo.TblUser
	SET 
		[mUseMacro] = @pUseMacroTimes
	WHERE ([mUserNo] = @pUserNo)

	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 250014	-- eErrNoCannotUpdateTblUser
		GOTO LABEL_END
	END

	-- 力犁 肺弊 扁废 
	EXEC dbo.UspInsMacroLog @pUserNo,  @pUseMacroTimes
	
LABEL_END:
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

