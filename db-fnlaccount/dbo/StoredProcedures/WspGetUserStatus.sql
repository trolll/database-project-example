
CREATE     PROCEDURE dbo.WspGetUserStatus
	@mSerialString VARCHAR(5000)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		u.mUserId, 
		(select top 1 mType from TblUserBlockHistory where mUserNo = u.mUserNo order by mSeqNo desc) as mType
		,(SELECT COUNT(*) FROM [dbo].[TblUserBlock] b
		 WHERE mUserNo = u.mUserNo AND ( mCertify > GETDATE()  OR mChat > 0 OR mBoard > GETDATE()    ) ) AS mBlock
		,(SELECT COUNT(*) FROM [dbo].[TblUserBlockHistory] bh
		 WHERE mUserNo = u.mUserNo) AS mBlockHistory		
	FROM [dbo].[TblUser] AS u
		INNER JOIN dbo.fn_SplitTSQL (@mSerialstring,',') a
			ON u.mUserId = a.element
	ORDER BY u.mUserId

GO

