
CREATE PROCEDURE [dbo].[WspCheckLogin]
	@mUserId	VARCHAR(20),	-- 아이디
	@mUserPswd	VARCHAR(20),	-- 비밀번호
	@mIp		CHAR(15)
AS
	SET NOCOUNT ON

	DECLARE @aUserPwd	VARCHAR(20)
	DECLARE @aIp		VARCHAR(15)
	DECLARE @aUserAuth	TINYINT

	SELECT		
		 @aUserPwd = mUserPswd
		,@aIp = mIp
		,@aUserAuth = mUserAuth
	FROM 
		[dbo].[TblUser] WITH(NOLOCK)
	WHERE
		mUserId = @mUserId

	IF @@ROWCOUNT <= 0 
	BEGIN
		RETURN(1)	-- 계정이 존재하지 않는다.
	END

	IF RTRIM(@mUserPswd) <> RTRIM(@aUserPwd)
	BEGIN
		RETURN(2)
	END
	
	IF RTRIM(@mIp) <> RTRIM(@aIp)
	BEGIN
		RETURN(3)
	END	

	IF @aUserAuth <= 1
	BEGIN
		RETURN(4)
	END	

	RETURN(0)	-- 인증 성공

GO

