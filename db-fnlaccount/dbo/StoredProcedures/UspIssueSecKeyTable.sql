CREATE PROCEDURE	dbo.UspIssueSecKeyTable
	@pUserNo	INT,		-- 荤侩磊 拌沥锅龋
	@pSecKey1 SMALLINT, @pSecKey2 SMALLINT, @pSecKey3 SMALLINT, @pSecKey4 SMALLINT, @pSecKey5 SMALLINT,
	@pSecKey6 SMALLINT, @pSecKey7 SMALLINT, @pSecKey8 SMALLINT, @pSecKey9 SMALLINT, @pSecKey10 SMALLINT,
	@pSecKey11 SMALLINT, @pSecKey12 SMALLINT, @pSecKey13 SMALLINT, @pSecKey14 SMALLINT, @pSecKey15 SMALLINT,
	@pSecKey16 SMALLINT, @pSecKey17 SMALLINT, @pSecKey18 SMALLINT, @pSecKey19 SMALLINT, @pSecKey20 SMALLINT,
	@pSecKey21 SMALLINT, @pSecKey22 SMALLINT, @pSecKey23 SMALLINT, @pSecKey24 SMALLINT, @pSecKey25 SMALLINT,
	@pSecKey26 SMALLINT, @pSecKey27 SMALLINT, @pSecKey28 SMALLINT, @pSecKey29 SMALLINT, @pSecKey30 SMALLINT,
	@pSecKey31 SMALLINT, @pSecKey32 SMALLINT, @pSecKey33 SMALLINT, @pSecKey34 SMALLINT, @pSecKey35 SMALLINT,
	@pSecKey36 SMALLINT, @pSecKey37 SMALLINT, @pSecKey38 SMALLINT, @pSecKey39 SMALLINT, @pSecKey40 SMALLINT,
	@pSecKey41 SMALLINT, @pSecKey42 SMALLINT, @pSecKey43 SMALLINT, @pSecKey44 SMALLINT, @pSecKey45 SMALLINT,
	@pSecKey46 SMALLINT, @pSecKey47 SMALLINT, @pSecKey48 SMALLINT, @pSecKey49 SMALLINT, @pSecKey50 SMALLINT,
	@pErrNoStr 	VARCHAR(50) OUTPUT
AS
/******************************************************************************
**		File: UspIssueSecKeyTable.sql
**		Name: UspIssueSecKeyTable
**		Desc: SC 墨靛 脚没 窍扁. ( SC墨靛甫 脚没窍看栏唱, 劝己拳甫 窍瘤 臼篮 惑怕 )
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2011.12.30	捞柳急				SC 墨靛 脚没矫 肺弊 眠啊
*******************************************************************************/ 
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;
	SET LOCK_TIMEOUT 2000;

	DECLARE	@aErrNo	INT
	DECLARE @aRowCount INT

	SET		@aErrNo	= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'
	SET		@aRowCount = 0

	IF NOT EXISTS(	SELECT mUserNo 
					FROM TblUser WITH (NOLOCK) 
					WHERE mUserNo = @pUserNo)
	BEGIN
		SET	@aErrNo	= 1
		SET	@pErrNoStr	= 'eErrNoUserNotExistId4'
		RETURN(@aErrNo)
	END

	DECLARE @aSysErrNo	INT
	BEGIN TRANSACTION

		IF EXISTS(	SELECT mUserNo 
					FROM TblUserSecKeyTable WITH (NOLOCK) 
					WHERE mUserNo = @pUserNo)
		BEGIN
			UPDATE dbo.TblUserSecKeyTable
			SET	mSecKey1=@pSecKey1, mSecKey2=@pSecKey2, mSecKey3=@pSecKey3, mSecKey4=@pSecKey4, mSecKey5=@pSecKey5, mSecKey6=@pSecKey6, mSecKey7=@pSecKey7, mSecKey8=@pSecKey8, mSecKey9=@pSecKey9, mSecKey10=@pSecKey10, 
				mSecKey11=@pSecKey11, mSecKey12=@pSecKey12, mSecKey13=@pSecKey13, mSecKey14=@pSecKey14, mSecKey15=@pSecKey15, mSecKey16=@pSecKey16, mSecKey17=@pSecKey17, mSecKey18=@pSecKey18, mSecKey19=@pSecKey19, mSecKey20=@pSecKey20, 
				mSecKey21=@pSecKey21, mSecKey22=@pSecKey22, mSecKey23=@pSecKey23, mSecKey24=@pSecKey24, mSecKey25=@pSecKey25, mSecKey26=@pSecKey26, mSecKey27=@pSecKey27, mSecKey28=@pSecKey28, mSecKey29=@pSecKey29, mSecKey30=@pSecKey30, 
				mSecKey31=@pSecKey31, mSecKey32=@pSecKey32, mSecKey33=@pSecKey33, mSecKey34=@pSecKey34, mSecKey35=@pSecKey35, mSecKey36=@pSecKey36, mSecKey37=@pSecKey37, mSecKey38=@pSecKey38, mSecKey39=@pSecKey39, mSecKey40=@pSecKey40, 
				mSecKey41=@pSecKey41, mSecKey42=@pSecKey42, mSecKey43=@pSecKey43, mSecKey44=@pSecKey44, mSecKey45=@pSecKey45, mSecKey46=@pSecKey46, mSecKey47=@pSecKey47, mSecKey48=@pSecKey48, mSecKey49=@pSecKey49, mSecKey50=@pSecKey50
			WHERE mUserNo = @pUserNo
		END
		ELSE
		BEGIN
			INSERT dbo.TblUserSecKeyTable VALUES(@pUserNo, 
				@pSecKey1, @pSecKey2, @pSecKey3, @pSecKey4, @pSecKey5, @pSecKey6, @pSecKey7, @pSecKey8, @pSecKey9, @pSecKey10, 
				@pSecKey11, @pSecKey12, @pSecKey13, @pSecKey14, @pSecKey15, @pSecKey16, @pSecKey17, @pSecKey18, @pSecKey19, @pSecKey20, 
				@pSecKey21, @pSecKey22, @pSecKey23, @pSecKey24, @pSecKey25, @pSecKey26, @pSecKey27, @pSecKey28, @pSecKey29, @pSecKey30, 
				@pSecKey31, @pSecKey32, @pSecKey33, @pSecKey34, @pSecKey35, @pSecKey36, @pSecKey37, @pSecKey38, @pSecKey39, @pSecKey40, 
				@pSecKey41, @pSecKey42, @pSecKey43, @pSecKey44, @pSecKey45, @pSecKey46, @pSecKey47, @pSecKey48, @pSecKey49, @pSecKey50,
				DEFAULT, DEFAULT
				)
		END
		
		SET @aSysErrNo = @@Error
		IF(@aSysErrNo <> 0 )
		BEGIN
			ROLLBACK TRANSACTION
			SET @aErrNo  = @aSysErrNo
			SET	@pErrNoStr	= 'eErrSqlInternalError'
			
			RETURN(@aErrNo)
		END	

		UPDATE TblUser 
		SET mSecKeyTableUse = 1 
		WHERE mUserNo = @pUserNo

		SET @aSysErrNo = @@Error
		IF(@aSysErrNo <> 0 )
		BEGIN
			ROLLBACK TRANSACTION
			SET @aErrNo  = @aSysErrNo
			SET	@pErrNoStr	= 'eErrSqlInternalError'
			
			RETURN(@aErrNo)
		END	
	
		--------------------------------------------
		-- 2011.12.30 SC 墨靛 脚没矫 肺弊 眠啊 
		--------------------------------------------
		INSERT INTO dbo.TblSCCardUseHistory (mRegDate, mUserNo, mSecKeyTableUse)
		VALUES ( GETDATE(), @pUserNo, 1 )

		SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;

		IF (@aRowCount = 0 OR @aErrNo <> 0)
		BEGIN
			ROLLBACK TRAN
			RETURN(@aErrNo)
		END

	COMMIT TRAN
	RETURN(0)
End

GO

