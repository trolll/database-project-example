/******************************************************************************  
**  Name: UspSetUserMovingToBattleSvr  
**  Desc: 硅撇 辑滚肺 捞悼吝牢 蜡历牢瘤 咯何 殿废
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**   
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetUserMovingToBattleSvr]  
	@pUserNo	INT     
AS  
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	DECLARE @aErrNo INT
	SET @aErrNo=0
	DECLARE @aRowCnt INT  
	SET @aRowCnt=0    
    
	UPDATE dbo.TblUser SET [mIsMovingToBattleSvr]=1 WHERE [mUserNo] = @pUserNo;
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
    IF( (0 <> @aErrNo) OR (1 <> @aRowCnt) )  
    BEGIN                              
		RETURN(@aErrNo);             
    END  
    
    RETURN(0);

GO

