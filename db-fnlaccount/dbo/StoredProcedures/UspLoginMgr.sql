
CREATE Procedure dbo.UspLoginMgr
	 @pUserId	CHAR(20)
	,@pUserPswd	CHAR(20)
	,@pIp		CHAR(15)
	,@pUserAuth	TINYINT	OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	DECLARE @aPswd		CHAR(20)
	DECLARE	@aUserNo	INT
	DECLARE @aIp			CHAR(15)
	
	SELECT 
		@aUserNo=[mUserNo], 
		@pUserAuth=[mUserAuth], 
		@aPswd=[mUserPswd], 
		@aIp=[mIp]
	FROM dbo.TblUser 
	WHERE ([mUserId] = @pUserId) 
			AND ([mDelDate] = '1900-01-01')
		
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END
	 END
	 
	IF(@pUserPswd <> @aPswd)
	 BEGIN
		SET  @aErrNo = 2
		GOTO LABEL_END	 
	 END
	 
	IF((@pUserAuth < 2) OR (@pIp <> @aIp))
	 BEGIN
		SET  @aErrNo = 3
		GOTO LABEL_END	 
	 END
 
	UPDATE dbo.TblUser 
	SET [mIp]=@pIp
	WHERE ([mUserNo] = @aUserNo) 
			AND ([mDelDate] = '1900-01-01')
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo = 5
		GOTO LABEL_END	 
	 END
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

