
CREATE     PROCEDURE [dbo].[WspListWebAdminMenuAuth]
	@mUserNo INT
AS
	SET NOCOUNT ON

	SELECT
		mMenuNo
		, mPageName
		, mMenuName
		, (SELECT COUNT(*) FROM [dbo].[TblWebAdminUserAuth] WITH (NOLOCK) WHERE mMenuNo = m.mMenuNo AND mUserNo = @mUserNo) AS mAccess
	FROM
		[dbo].[TblWebAdminMenu] AS m WITH (NOLOCK)
	ORDER BY
		mOrder ASC

GO

