CREATE PROCEDURE dbo.UspDeleteUser
	 @pUserId		VARCHAR(20)
	 ,@pMemo		VARCHAR(50)	= ''
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	
	DECLARE	@aErrNo		INT	
	SELECT	@aErrNo = 0;


	BEGIN TRAN
	
		DELETE dbo.TblPsUser
		WHERE mUserID = @pUserId
		IF(0 <> @@ERROR)
		BEGIN
			SET  @aErrNo = @@ERROR
			GOTO LABEL_END
		END
		
		DELETE dbo.TblEventUser
		WHERE mUserID = @pUserId
		IF(0 <> @@ERROR)
		BEGIN
			SET  @aErrNo = @@ERROR
			GOTO LABEL_END
		END		
		
		DELETE dbo.TblUserCb
		WHERE mUserID = @pUserId
		IF(0 <> @@ERROR)
		BEGIN
			SET  @aErrNo = @@ERROR
			GOTO LABEL_END
		END		
				
		DELETE dbo.TblMacroDetectUser
		WHERE mUserID = @pUserId
		IF(0 <> @@ERROR)
		BEGIN
			SET  @aErrNo = @@ERROR
			GOTO LABEL_END
		END			
		
		DELETE dbo.TblUserBlack
		WHERE mUserID = @pUserId
		IF(0 <> @@ERROR)
		BEGIN
			SET  @aErrNo = @@ERROR
			GOTO LABEL_END
		END				
		
		-- 昏力巩磊牢 ','绰 馆靛矫 TblInvalidString俊 殿废阑 窍咯 荤侩 给窍档废 秦具 茄促.
		INSERT dbo.TblUserDeleted( [mUserNo], [mUserId], [mMemo] ) 
		SELECT 
			[mUserNo]
			, [mUserId] 
			, @pMemo
		FROM dbo.TblUser
		WHERE ([mUserId] = @pUserId) 
					AND ([mDelDate] = '1900-01-01')
		IF(0 <> @@ERROR)
		BEGIN
			SET  @aErrNo = @@ERROR
			GOTO LABEL_END
		END
		 
		UPDATE dbo.TblUser 
		SET 	
			[mDelDate]=GETDATE(), 
			[mUserId]= ','+SUBSTRING(CONVERT(VARCHAR(100),[mUserNo]), 1, 19),	-- 扁粮 ID 函券俊辑 漂沥 巩磊凯肺 函券 贸府 茄促.	
			[mIP] = '0.0.0.0',
			[mIpEX] =  0
		WHERE ([mUserId] = @pUserId) 
		IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
		BEGIN
			SET  @aErrNo = 250014	-- eErrNoCannotUpdateTblUser
			GOTO LABEL_END
		END

LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

