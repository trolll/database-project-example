/******************************************************************************  
**  Name: UspLoginUser_KR  
**  Desc: 蜡历 肺弊牢  
**       
**    
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**  2010-12-15	辫堡挤		SC 荤侩 咯何甫 OUTPUT 栏肺 傈崔 罐绰促.  
**  2014-04-29	傍籍痹		漂拳辑滚 包访 力茄矫埃阑 OUTPUT 栏肺 傈崔 罐绰促. 
**  2016-09-21	nsm			硅撇 辑滚肺 捞悼 矫 worldno 沥焊啊 巢酒乐绢档 mIsMovingToBattleSvr 沥焊俊 蝶扼 贸府   
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspLoginUser_KR]  
       @pUserNo                    INT  
       ,@pCertifiedKey             INT   
       ,@pIp                       CHAR(15)  
       ,@pWorldNo                  SMALLINT  
       ,@pIpEX                     BIGINT  
       ,@pPcBangLvEX               INT  
       ,@pIsNonClt                 BIT                   -- 1 : NonClient 荤侩磊       
       ,@pUserId                   VARCHAR(20)    OUTPUT  
       ,@pUserAuth                 TINYINT        OUTPUT  
       ,@pErrNoStr                 VARCHAR(50)    OUTPUT  
       ,@pLeftChat                 INT            OUTPUT -- 盲泼静扁陛瘤巢篮矫埃.  
       ,@pEndBoard                 DATETIME       OUTPUT -- 霸矫魄静扁陛瘤啊辆丰登绰老矫.  
       ,@pPcBangLv                 INT            OUTPUT -- EPcBangLv.  
       ,@pUseMacro                 SMALLINT       OUTPUT -- 眠啊, 概农肺荤侩冉荐  
       ,@pIpEXUserNo               INT            OUTPUT  
       ,@pJoinCode                 CHAR(1)        OUTPUT -- 炼牢内靛眠啊  
       ,@pTired                    CHAR(1)        OUTPUT -- 吝刀规瘤眠啊  
       ,@pChnSID                   CHAR(33)       OUTPUT -- 吝惫脚盒刘锅龋眠啊  
       ,@pNewId                    BIT            OUTPUT -- 脚痹拌沥咯何眠啊  
       ,@pSvrInfo                  TINYINT        = 0    -- 立加茄辑滚鸥涝( 0 : 老馆辑滚, 1 : 墨坷胶海撇辑滚)  
       ,@pNewCertifiedKey          INT                   -- 函版且牢刘虐  
       ,@pNewCertifiedKeyOutput    INT            OUTPUT -- 角力函版等牢刘虐  
       ,@pAccountGUID              INT            OUTPUT -- 昆哩拌沥GUID  
       ,@pSecKeyState              TINYINT        OUTPUT -- SC 荤侩咯何 搬苞  
       ,@pNormalLimitTime          INT            OUTPUT -- 漂拳辑滚包访 老馆 力茄 矫埃  
       ,@pPcBangLimitTime          INT            OUTPUT -- 漂拳辑滚包访 PC规 力茄 矫埃      
AS  
       SET NOCOUNT ON        
       SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
       DECLARE      @aErrNo                INT,  
                    @aRowCount            INT  
                      
       SELECT       @aErrNo                = 0,  
                    @pErrNoStr             = 'eErrNoSqlInternalError',  
                    @aRowCount             = 0;  
         
         
       DECLARE      @aWorld                SMALLINT  
       DECLARE      @aIp                   CHAR(15)  
       DECLARE      @aKey                  INT  
       DECLARE      @aLoginTm              DATETIME  
       DECLARE      @aCertify              DATETIME  
       DECLARE      @aLastDiffNonCltUse    SMALLDATETIME  
       DECLARE      @aSvrInfo              TINYINT  
  	   -- 20160921(nsm)
	   DECLARE		@aIsMovingToBattleSvr	BIT
         
       SELECT   
                    @pUserId = RTRIM(a.[mUserId])  
                    , @pUserAuth = a.[mUserAuth]  
                    , @aWorld = a.[mWorldNo]  
                    , @aIp = a.[mIp]  
                    , @aKey = ISNULL(a.[mCertifiedKey],0)  
                    , @aLoginTm = a.[mLoginTm]  
                    , @pLeftChat = b.[mChat]  
                    , @pEndBoard = b.[mBoard]  
                    , @pPcBangLv = a.[mPcBangLv]  
                    , @aCertify = b.[mCertify]  
                    , @pUseMacro = a.[mUseMacro]  
                    , @pJoinCode = a.[mJoinCode]  
                    , @pTired = a.[mTired]  
                    , @pChnSID = a.[mChnSID]  
                    , @pNewId = a.[mNewId]  
                    , @aSvrInfo = a.[mLoginSvrType]  
                    , @pAccountGUID = a.[mAccountGuid]  
                    , @pSecKeyState = a.[mSecKeyTableUse]  
                    , @pNormalLimitTime = a.[mNormalLimitTime]  
                    , @pPcBangLimitTime = a.[mPcBangLimitTime]  
                    -- 20160921(nsm)
					, @aIsMovingToBattleSvr = a.[mIsMovingToBattleSvr]
       FROM   
                    dbo.TblUser  AS a   
                           LEFT OUTER JOIN dbo.TblUserBlock   AS b   
                    ON a.mUserNo = b.mUserNo  
       WHERE   
                    a.mUserNo = @pUserNo   
                    AND a.mDelDate = '1900-01-01';                                
         
       SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;     
       IF (@aRowCount = 0) OR (@aErrNo <> 0)  
       BEGIN  
             SET  @aErrNo  = 1  
             SET    @pErrNoStr   = 'eErrNoUserNotExistId4';  
             RETURN(@aErrNo)  
       END  
         
       -- 20100818 辫堡挤- 父距函版窍绰虐啊扁粮虐尔悼老茄版快虐甫函版茄促. (犬伏捞1/42撅捞扼辑-_-せ)  
       IF(@pNewCertifiedKey = @aKey)  
       BEGIN  
             -- 20100818 辫堡挤- 坷滚敲肺快规瘤甫困秦公炼扒澜荐肺父电促.  
             IF(0 < @pNewCertifiedKey)  
             BEGIN  
                    SET    @pNewCertifiedKey = @pNewCertifiedKey * -1;  
             END  
         
             -- 20100818 辫堡挤- 蔼捞澜荐捞扁俊距埃罚待茄剧荐甫 歹茄促.  
             SET    @pNewCertifiedKey = @pNewCertifiedKey + CONVERT(INT, RAND() * 100000000);  
               
             -- 20100818 辫堡挤- 弊贰档鞍栏搁何龋父函版矫挪促.  
             IF(@pNewCertifiedKey = @aKey)  
             BEGIN  
                    SET @pNewCertifiedKey = @pNewCertifiedKey * -1;  
             END  
       END  
         
       -- 20100818 辫堡挤- 牢刘俊己傍窍电角菩窍电蜡历啊粮犁窍绰版快牢刘虐甫函版茄促.  
       UPDATE dbo.TblUser SET [mCertifiedKey] = @pNewCertifiedKey WHERE  [mUserNo] = @pUserNo;  
       SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;  
       IF(@aRowCount = 0 OR @aErrNo <> 0)  
       BEGIN  
             SELECT  @pErrNoStr  = 'eErrSqlInternalError', @aErrNo = 8;                               
             RETURN(@aErrNo);             
       END  
         
       -- 20100818 辫堡挤- 角力肺函版等牢刘虐甫历厘茄促.  
       SET    @pNewCertifiedKeyOutput = @pNewCertifiedKey;  
         
       IF(@pCertifiedKey <> @aKey)  
       BEGIN  
             SET  @aErrNo  = 2;  
             SET    @pErrNoStr   = 'eErrNoUserDiffCertifiedKey';  
             RETURN(@aErrNo);      
       END  
         
  
       IF( GETDATE() < @aCertify )             -- 牢刘喉遏眠啊  
       BEGIN  
             SET  @aErrNo  = 9;  
             SET    @pErrNoStr   = 'eErrNoUserBlocked';  
             RETURN(@aErrNo);             
       END            
         
       IF(0 = @pUserAuth)  
       BEGIN  
             SET  @aErrNo  = 5;  
             SET    @pErrNoStr   = 'eErrNoAuthInvalid';  
             RETURN(@aErrNo);      
       END  
         
       IF(0 = @pAccountGUID)  
       BEGIN  
             SET  @aErrNo  = 11;  
             SET    @pErrNoStr   = 'eErrNoAuthInvalid';  
             RETURN(@aErrNo);      
       END      
         
       -- 2007.10.08 soundkey 茄拌沥寸茄俺IP 力茄阑困秦眠啊  
       -- @pIpEX 啊TblUser.mIpEX俊乐促搁秦寸UserNo甫馆券矫糯  
       IF( 0 < @pPcBangLvEX  )    -- @pPcBangLvEX 老沥捞惑捞搁  
       BEGIN  
             SELECT          
                    TOP 1          
                           @pIpEXUserNo = mUserNo   
             FROM   
                           dbo.TblUser   
             WHERE   
                           mIpEX = @pIpEX   
                                 AND mWorldNo > 0   
                                 AND mPcBangLv = @pPcBangLvEX;   
  
             SELECT @aRowCount = @@ROWCOUNT;  
  
              IF @aRowCount > 0  
             BEGIN  
                    SET    @aErrNo      = 10;  
                    SET    @pErrNoStr   = 'eErrNoUserCantDupLoginIp';  
                    RETURN(@aErrNo);      
             END            
       END  
         
       -- 2008.01.11 辫堡挤(稠努扼捞攫飘荤侩磊咯何)  
       IF(0<>@pIsNonClt)  
             SET @aLastDiffNonCltUse = GETDATE();  
       ELSE  
             SET @aLastDiffNonCltUse = NULL;  
  
	   -- 20160921(nsm)
  	   -- 肺弊牢 矫痢俊 [mIsMovingToBattleSvr] 沥焊 公炼扒 檬扁拳
	   UPDATE dbo.TblUser SET [mIsMovingToBattleSvr]=0 WHERE mUserNo=@pUserNo AND mDelDate='1900-01-01'; 
       -- IF(0 < @aWorld) 
  	   IF( (0 < @aWorld) AND (0 = @aIsMovingToBattleSvr) )
       BEGIN  
             -- 20081202(kslive) :  IP 客辑滚锅龋档悼老秦具瘤OK 促.  
             -- IF(@aIp = @pIp)  
             IF(@aIp = @pIp AND @aWorld = @pWorldNo)  
             BEGIN  
                      
                    SET  @aErrNo = 0;   -- mWorld > 0 AND 悼老IP 肺弊牢矫捞何盒篮0蔼栏肺Return  
                    --SET  @pErrNoStr   = 'eErrNoUserChkAlreadyLogined'                                     
             END  
             -- 20090430(kslive) : IP 客泅犁立加吝牢辑滚啊墨坷胶硅撇辑滚捞搁立加阑倾侩茄促.  
             ELSE IF(@aIp = @pIp AND @aSvrInfo = 1)  
             BEGIN  
                    SET @aErrNo = 0;  
             END  
             ELSE  
             BEGIN  
                    SET  @aErrNo = 7;  
                    SET    @pErrNoStr   = 'eErrNoUserLoginAnother';  
         END            
               
             RETURN(@aErrNo);                   
       END      
  
       -- login info update         
       UPDATE   
                    dbo.TblUser   
       SET   
                    [mIp]=@pIp  
                    , [mLoginTm]=GETDATE()  
                    , [mWorldNo]=@pWorldNo  
                    , [mIpEX] = @pIpEX    
                    , [mLoginSvrType] = @pSvrInfo  
       WHERE   
                    mUserNo = @pUserNo  
                    AND  mDelDate = '1900-01-01';  
         
       SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;  
       IF @aRowCount = 0 OR @aErrNo <> 0  
       BEGIN  
             SELECT  @pErrNoStr  = 'eErrSqlInternalError', @aErrNo = 8;  
             RETURN(@aErrNo);             
       END  
         
       IF(0<>@pIsNonClt)  
       BEGIN  
  
             EXEC @aErrNo = dbo.UspRegisterNonClientUser @pUserNo  
             IF @aErrNo <>0  
             BEGIN  
                    SELECT  @pErrNoStr  = 'eErrSqlInternalError', @aErrNo = 8;  
                    RETURN(@aErrNo);             
             END      
       END   
  
       -- 2009.01.22 JUDY  
       -- 烙矫利立加茄荤侩磊甸阑措惑栏肺急拱窃俊酒捞袍瘤鞭茄促.  
       EXEC FNLBilling.dbo.UspRegGiftEvent @pUserNo  
  
       RETURN(0);

GO

