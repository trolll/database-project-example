
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
CREATE    Procedure dbo.WspListBlackUser
------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	SELECT
		 a.[mUserId], a.[mDesc], b.[mWorldNo], p.[mDesc] AS SName
	FROM 
		TblUserBlack AS a 
	LEFT OUTER JOIN 
		TblUser AS b WITH (NOLOCK) 
	ON
		(a.[mUserId] = b.[mUserId])
	LEFT OUTER JOIN
		FNLParm.dbo.TblParmSvr AS p 
	ON
		b.mWorldNo = p.mWorldNo AND p.mType = 1
	
	SET NOCOUNT OFF

GO

