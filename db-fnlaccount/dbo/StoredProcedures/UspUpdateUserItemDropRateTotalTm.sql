/******************************************************************************
**		Name: UspUpdateUserItemDropRateTotalTm
**		Desc: 力犁等 蜡历狼 立加 矫埃 盎脚(肺弊酒眶 矫痢)
**
**		Auth: 沥备柳
**		Date: 10.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      10.04.28    沥备柳              靛而啦 力犁 等 蜡历狼 敲饭捞 矫埃 盎脚  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateUserItemDropRateTotalTm]
	 @pUserNo			INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aLoginTm	SMALLDATETIME
	DECLARE	@aLogoutTm	SMALLDATETIME
	DECLARE	@aTotalTm	INT
	
	SET	@aLoginTm = 0
	SET	@aLogoutTm = 0
	SET @aTotalTm = 0

	-- 肺弊酒眶 且 矫痢俊 敲饭捞 矫埃阑 穿利茄促.
	SELECT @aLoginTm = mLoginTm, @aLogoutTm = mLogoutTm
	FROM dbo.TblUser
	WHERE mUserNo = @pUserNo

	SET @aTotalTm = DATEDIFF(minute, @aLoginTm, @aLogoutTm)

	-- 敲饭捞矫埃 穿利
	UPDATE dbo.TblUserItemDropRate
	SET mTotalTm = mTotalTm + @aTotalTm
	WHERE mUserNo = @pUserNo

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error 
	END
	
	RETURN(0)

GO

