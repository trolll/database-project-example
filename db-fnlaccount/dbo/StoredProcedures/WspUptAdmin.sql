CREATE PROCEDURE [dbo].[WspUptAdmin]
	 @mUserId	VARCHAR(20)
	,@mUserPwd	VARCHAR(20)
	,@mUserAuth	TINYINT
	,@mIp		VARCHAR(15)
AS
	SET NOCOUNT ON
	
	DECLARE @aErr		INT,
			@aRowCnt	INT,
			@aUserNo	INT
	
	SELECT	@aErr = 0,@aRowCnt = 0, @aUserNo = 0
		
	IF RTRIM(@mUserPwd) = '' OR 
		LEN(RTRIM(@mUserPwd)) = 0
		SET @mUserPwd = NULL
			
	---------------------------------------------------------------------
	-- 荤侩磊 沥焊 粮犁咯何
	---------------------------------------------------------------------
	SELECT TOP 1		
		@aUserNo = mUserNo		
	FROM dbo.TblUser
	WHERE mUserId = @mUserId
	SELECT	@aErr = @@ERROR,	@aRowCnt = @@ROWCOUNT
	
	---------------------------------------------------------------------
	-- 荤侩磊 沥焊 盎脚 肚绰 殿废 
	---------------------------------------------------------------------
	BEGIN TRAN
	
		IF @aRowCnt > 0 
		BEGIN 
			
			IF @mUserPwd IS NULL
			BEGIN
			
				UPDATE dbo.TblUser
				SET 
					mUserAuth = @mUserAuth,
					mIp = @mIp
				WHERE mUserNo = @aUserNo
				
				SELECT	@aErr = @@ERROR,	@aRowCnt = @@ROWCOUNT		
				IF @aErr <> 0  OR @aRowCnt <> 1
				BEGIN
					ROLLBACK TRAN
					RETURN(@aErr)
				END	
			END
			ELSE
			BEGIN
				UPDATE dbo.TblUser
				SET 
					mUserAuth = @mUserAuth,
					mIp = @mIp,
					mUserPswd= @mUserPwd
				WHERE mUserNo = @aUserNo
				
				SELECT	@aErr = @@ERROR,	@aRowCnt = @@ROWCOUNT		
				IF @aErr <> 0  OR @aRowCnt <> 1
				BEGIN
					ROLLBACK TRAN
					RETURN(@aErr)
				END				
			
			END 
						
		END 
		ELSE
		BEGIN
		
			INSERT INTO dbo.TblUser(
				mUserId,
				mUserPswd,
				mUserAuth,
				mIp)
			VALUES(
				@mUserId,
				@mUserPwd,
				@mUserAuth,
				@mIp)
				
			SET @aUserNo = @@IDENTITY	

			SELECT	@aErr = @@ERROR,	@aRowCnt = @@ROWCOUNT		
			IF @aErr <> 0  OR @aRowCnt <> 1
			BEGIN
				ROLLBACK TRAN
				RETURN(@aErr)
			END

			INSERT INTO dbo.TblUserAdmin
			VALUES(
					@aUserNo
				,@mUserId
			)	
			
			SELECT	@aErr = @@ERROR,	@aRowCnt = @@ROWCOUNT		
			IF @aErr <> 0  OR @aRowCnt <> 1
			BEGIN
				ROLLBACK TRAN
				RETURN(@aErr)
			END			
		END
					

	COMMIT TRAN
	RETURN(0)

GO

