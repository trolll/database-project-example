CREATE  PROCEDURE	dbo.UspCheckSecKeyValue
	@pUserNo			INT,			-- 荤侩磊 拌沥锅龋
	@pSecKeyIdx1			TINYINT,		-- 焊救虐抛捞喉 牢郸胶 1
	@pSecKeySide1		TINYINT,		-- 焊救虐抛捞喉 牢郸胶蔼狼 规氢 1 (1:惑困2磊府/0:窍困2磊府)
	@pSecKeyIdx2			TINYINT,		-- 焊救虐抛捞喉 牢郸胶 2
	@pSecKeySide2		TINYINT,		-- 焊救虐抛捞喉 牢郸胶蔼狼 规氢 2 (1:惑困2磊府/0:窍困2磊府)
	@pUserAuthVal			INT,			-- 困狼 焊救沥焊甫 捞侩窍咯 荤侩磊啊 拌魂窍咯 焊辰 蔼
	@pCheckVal			TINYINT OUTPUT,	-- 焊救虐抛捞喉 牢刘 烹苞咯何 (1:己傍/0:角菩)
	@pErrNoStr			VARCHAR(50) OUTPUT
AS
/******************************************************************************
**		File: UspCheckSecKeyValue.sql
**		Name: UspCheckSecKeyValue
**		Desc: SC 墨靛 劝己拳 窍扁. 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2011.12.30	捞柳急				SC 墨靛 劝己拳 矫 肺弊 眠啊
*******************************************************************************/ 
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNoGlobal	INT
	DECLARE	@aErrNo	INT
	DECLARE	@pSecKeyVerify1	TINYINT	-- 焊救虐抛捞喉 牢郸胶 1狼 稠府坷幅 八免侩
	DECLARE	@pSecKeyVerify2	TINYINT	-- 焊救虐抛捞喉 牢郸胶 2狼 稠府坷幅 八免侩
	DECLARE	@pUserAuthValSide1	TINYINT	-- 困狼 焊救沥焊甫 捞侩窍咯 荤侩磊 蔼 吝 惑困 2磊府 蔼
	DECLARE	@pUserAuthValSide2	TINYINT	-- 困狼 焊救沥焊甫 捞侩窍咯 荤侩磊啊  吝 窍困 2磊府 蔼
	
	DECLARE @aRowCnt	INT,
			@aSecKeyTableUse	TINYINT

	SELECT	@aErrNo	= 0,
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@pCheckVal	= 0,
			@aRowCnt = 0,
			@aSecKeyTableUse = 0


	--------------------------------------------------------------------------
	-- 蜡历沥焊 犬牢
	--------------------------------------------------------------------------
	SELECT TOP 1 @aSecKeyTableUse = mSecKeyTableUse
	FROM dbo.TblUser WITH (NOLOCK) 
	WHERE mUserNo = @pUserNo

	SELECT	@aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT	
	IF @aErrNo <> 0 OR @aRowCnt <> 1
	BEGIN
		SET	@aErrNo	= 1
		SET	@pErrNoStr	= 'eErrNoUserNotExistId4'
		RETURN(@aErrNo)
	END

	--------------------------------------------------------------------------
	-- SC Key Table眉农 
	--------------------------------------------------------------------------
	IF NOT EXISTS(	SELECT mUserNo 
					FROM dbo.TblUserSecKeyTable WITH (NOLOCK) 
					WHERE mUserNo = @pUserNo)
	BEGIN
		SET	@aErrNo	= 2
		SET	@pErrNoStr	= 'eErrNoSecKeyTableNotExist '
		RETURN(@aErrNo)
	END

	--------------------------------------------------------------------------
	-- SC KEY 眉农 
	--------------------------------------------------------------------------
	IF (@pSecKeyIdx1 < 1 OR @pSecKeyIdx1 > 50 OR @pSecKeyIdx2 < 1 OR @pSecKeyIdx2 > 50)
	BEGIN
		SET	@aErrNo	= 3
		SET	@pErrNoStr	= 'eErrNoSecKeyIndexOutBound'
		RETURN(@aErrNo)
	END
	
	--------------------------------------------------------------------------
	-- SC 沥焊 犬牢
	--------------------------------------------------------------------------	
	DECLARE @aSecKeyVal1	SMALLINT
	DECLARE @aSecKeyVal2	SMALLINT
	DECLARE @aQueryStr		NVARCHAR(256)
	DECLARE @aParamStr		NVARCHAR(256)
	SET @aQueryStr =N'SELECT @aSecKeyTempVal1 = mSecKey' + LTRIM(STR(@pSecKeyIdx1)) + ', @aSecKeyTempVal2 = mSecKey' + LTRIM(STR(@pSecKeyIdx2))
	SET @aQueryStr = @aQueryStr + N' FROM dbo.TblUserSecKeyTable WITH(NOLOCK) WHERE mUserNo = ' + LTRIM(STR(@pUserNo))
	SET @aParamStr = N'@aSecKeyTempVal1 AS SMALLINT OUTPUT, @aSecKeyTempVal2 AS SMALLINT OUTPUT'
	EXEC sp_executesql  @aQueryStr, @aParamStr, @aSecKeyTempVal1 = @aSecKeyVal1 OUTPUT, @aSecKeyTempVal2 = @aSecKeyVal2 OUTPUT

	SET @aErrNoGlobal = @@Error
	IF(@aErrNoGlobal <> 0 )
	BEGIN
		SET	@aErrNo	= @aErrNoGlobal
		RETURN(@aErrNo)
	END

	IF (@pSecKeySide1 <> 0)
	BEGIN
		SET @aSecKeyVal1 = @aSecKeyVal1 / 100
		SET @pSecKeyVerify1 = 0
	END
	ELSE
	BEGIN
		SET @aSecKeyVal1 = @aSecKeyVal1 % 100
		SET @pSecKeyVerify1 = 1
	END

	IF (@pSecKeySide2 <> 0)
	BEGIN
		SET @aSecKeyVal2 = @aSecKeyVal2 / 100
		SET @pSecKeyVerify2 = 1
	END
	ELSE
	BEGIN
		SET @aSecKeyVal2 = @aSecKeyVal2 % 100
		SET @pSecKeyVerify2 = 0
	END

	IF (@pSecKeyVerify1 <> @pSecKeyVerify2)	-- 辑肺 规氢捞 促福瘤 臼栏搁 稠府利牢 坷幅肺 埃林 (20061121)
	BEGIN
		SET	@aErrNo	= 4
		SET	@pErrNoStr	= 'eErrNoSecKeyIndexOutBound'
		RETURN(@aErrNo)
	END
	
	SET @pUserAuthVal		= @pUserAuthVal % 65536
	SET @pUserAuthValSide1	= @pUserAuthVal / 256
	SET @pUserAuthValSide2	= @pUserAuthVal % 256

	IF (@pSecKeyVerify1 = 0)
	BEGIN
		IF ((@aSecKeyVal1 = @pUserAuthValSide1) AND (@aSecKeyVal2 = @pUserAuthValSide2))
		BEGIN
			SET @pCheckVal = 1		
		END
		ELSE
		BEGIN
			SET @pCheckVal = 0
		END
	END
	ELSE
	BEGIN
		IF ((@aSecKeyVal1 = @pUserAuthValSide2) AND (@aSecKeyVal2 = @pUserAuthValSide1))
		BEGIN
			SET @pCheckVal = 1		
		END
		ELSE
		BEGIN
			SET @pCheckVal = 0
		END
	END

	--------------------------------------------------------------------------
	-- SC 沥焊 函版
	--------------------------------------------------------------------------		
	IF @pCheckVal = 1 AND @aSecKeyTableUse <> 2	
	BEGIN
		DECLARE @aSysErrNo	INT

		BEGIN TRAN
		
			UPDATE dbo.TblUser 
			SET mSecKeyTableUse = 2				-- 柳青 惑怕 函版 
			WHERE mUserNo = @pUserNo
	
			SELECT @aErrNo = @@ERROR
			IF(@aErrNo <> 0 )
			BEGIN
				ROLLBACK TRAN
				RETURN(@aErrNo)
			END			

			UPDATE dbo.TblUserSecKeyTable
			SET mSetupDate = Getdate()			-- SC 墨靛 汲沥
			WHERE mUserNo = @pUserNo
				
			SELECT @aErrNo = @@ERROR
			IF(@aErrNo <> 0 )
			BEGIN
				ROLLBACK TRAN
				RETURN(@aErrNo)
			END

			--------------------------------------------
			-- 2011.12.30 SC 墨靛 劝己拳 矫 肺弊 眠啊 
			--------------------------------------------
			INSERT INTO dbo.TblSCCardUseHistory (mRegDate, mUserNo, mSecKeyTableUse)
			VALUES ( GETDATE(), @pUserNo, 2 )

			SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;

			IF (@aRowCnt = 0 OR @aErrNo <> 0)
			BEGIN
				ROLLBACK TRAN
				RETURN(@aErrNo)
			END
	
		COMMIT TRAN			
	END 

	RETURN(0)
End

GO

