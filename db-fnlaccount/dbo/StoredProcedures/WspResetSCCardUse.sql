CREATE          PROCEDURE [dbo].[WspResetSCCardUse]
	@mStr 		VARCHAR(20),	-- 蜡历 锅龋, 蜡历 ID
	@mFlag		INT		-- 敲贰弊
AS
/******************************************************************************
**		File: WspResetSCCardUse.sql
**		Name: WspResetSCCardUse
**		Desc: SCCard阑 荤侩 救窍骨肺 配臂茄促
**
**              
**		Return values:
**					ErrNo 2 : 蜡历啊 粮犁窍瘤 臼芭唱, SCCard Service甫 荤侩窍瘤 臼绰促.
**					ErrNo 1 : Update 等 Row狼 俺荐啊 茄俺啊 酒聪促. (0俺 Or 促荐)
**					ErrNo 0 : 己傍利栏肺 肯丰登菌促.
**              
**		Parameters:
**		Input							Output
**     ----------							-----------
**
**		Auth: 辫堡挤
**		Date: 2006.10.12
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2006.11.20	JUDY				劝己老矫 函版
**		2011.12.30	捞柳急				SC 墨靛 秦瘤矫 肺弊 眠啊
*******************************************************************************/
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@mUserNo	INT,
			@aErrNo		INT,
			@aFlag		BIT,
			@aRowCnt	INT

	SELECT	@aErrNo = 2,
			@aFlag  = 0,
			@mUserNo = 0,
			@aRowCnt = 0

	--------------------------------------------------------------------------------
	-- 荤侩磊 沥焊 粮犁咯何 犬牢
	--------------------------------------------------------------------------------
	IF( @mFlag = 1)
	BEGIN
		SET @mUserNo = CONVERT(INT, @mStr)
		
		IF NOT EXISTS(	SELECT * 
						FROM dbo.TblUser WITH(NOLOCK) 
						WHERE mUserNo = @mUserNo AND mSecKeyTableUse <> 0 )
		BEGIN
			RETURN(2)	-- 荤侩磊 沥焊啊 绝促.
		END						
	END
	ELSE
	BEGIN
		SELECT TOP 1
			@mUserNo = mUserNo
		FROM dbo.TblUser WITH(NOLOCK) 
		WHERE mUserId = @mStr AND mSecKeyTableUse <> 0 
		
		IF @@ROWCOUNT <= 0
		BEGIN
			RETURN(2)	-- 荤侩磊 沥焊啊 绝促.
		END						
	END

	--------------------------------------------------------------------------------
	-- SC Card 檬扁拳
	--------------------------------------------------------------------------------
	BEGIN TRAN
	
		UPDATE dbo.TblUserSecKeyTable 
		SET mReleaseDate = getdate()		
		WHERE mUserNo = @mUserNo
		
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;

		IF (@aRowCnt = 0 OR @aErrNo <> 0)
		BEGIN
			ROLLBACK TRAN
			RETURN(@aErrNo)
		END
					
	
		UPDATE dbo.TblUser 
		SET mSecKeyTableUse=0 
		WHERE mUserNo = @mUserNo 
		
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;

		IF (@aRowCnt = 0 OR @aErrNo <> 0)
		BEGIN
			ROLLBACK TRAN
			RETURN(@aErrNo)
		END

		--------------------------------------------
		-- 2011.12.30 SC 墨靛 秦瘤 矫 肺弊 眠啊 
		--------------------------------------------
		INSERT INTO dbo.TblSCCardUseHistory (mRegDate, mUserNo, mSecKeyTableUse)
		VALUES ( GETDATE(), @mUserNo, 0 )

		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;

		IF (@aRowCnt = 0 OR @aErrNo <> 0)
		BEGIN
			ROLLBACK TRAN
			RETURN(@aErrNo)
		END

	COMMIT TRAN
	RETURN(0)
END

GO

