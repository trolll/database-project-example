--FNLAccount
CREATE  PROCEDURE dbo.UspCertifyUser
	 @pUserId			VARCHAR(20)
	,@pUserPswd			VARCHAR(20)
	,@pIp				CHAR(15)
	,@pIpEX			BIGINT
	,@pCertifiedKey		INT			-- DB狼 RAND()绰 float骨肺 寇何俊辑 林磊.
	,@pIsEqualRsc		BIT
	,@pPcBangLv			INT			-- EPcBangLv.
	,@pUserNo			INT				OUTPUT
	,@pWorldNo			SMALLINT		OUTPUT
	,@pErrNoStr			VARCHAR(50)		OUTPUT
	,@pCertify			DATETIME		OUTPUT	-- 牢刘block捞 辆丰登绰 老矫.
	,@pCertifyReason	VARCHAR(200)	OUTPUT	-- 牢刘block矫 荤蜡.
	,@pSecKeyTableUse	TINYINT	OUTPUT	-- 焊救虐抛捞喉 荤侩咯何
	,@pUserAuth			TINYINT	OUTPUT
	,@pIsAddUser		BIT	= 1
	,@pIsPwdCheck		BIT = 0
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'
	
	IF ( @pIsAddUser = 1 )
	BEGIN
		IF NOT EXISTS(	SELECT * 
						FROM dbo.TblUser 
						WHERE mUserId= @pUserId)
		BEGIN
			INSERT INTO dbo.TblUser([mUserId], [mUserPswd])	
			VALUES(@pUserId, 'nhngames')
			
			-- added cash
			INSERT INTO dbo.Member([mUserId], [Cash])
			VALUES(@pUserId, 0)
		END
	END 
	
	DECLARE	@aIp		CHAR(15)
	DECLARE @aPswd		VARCHAR(20)
	DECLARE @aLoginTm	DATETIME
	DECLARE @aKey		INT
	DECLARE @aUserNo	INT
	
	SELECT 
		   @aUserNo = a.[mUserNo],
		   @pUserAuth=a.[mUserAuth], @aPswd=a.[mUserPswd], @pUserNo=a.[mUserNo], @pWorldNo=a.[mWorldNo], 
		   @aIp=a.[mIp], @aLoginTm=a.[mLoginTm], @aKey=a.[mCertifiedKey], 
		   @pCertify=b.[mCertify], @pCertifyReason=RTRIM(b.[mCertifyReason]),
		   @pSecKeyTableUse=a.[mSecKeyTableUse]
		FROM dbo.TblUser  AS a WITH (NOLOCK)
			LEFT OUTER JOIN dbo.TblUserBlock AS b  WITH (NOLOCK)
			ON a.[mUserNo] = b.[mUserNo] 
		WHERE (a.[mUserId] = @pUserId) 
				AND ( a.[mDelDate] = '1900-01-01' )	
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoUserNotExistId3'
		GOTO LABEL_END
	 END		
	  
	-- 2008.02.11 judy
	IF ( @pIsPwdCheck = 1 )
	BEGIN
		IF ( RTRIM(@pUserPswd) <> RTRIM(@aPswd) )
		BEGIN
			SET  @aErrNo	= 1
			SET	 @pErrNoStr	= 'eErrNoUserDiffPswd'
			GOTO LABEL_END					
		END 
	END 
		  
	IF(0 = @pUserAuth)
	 BEGIN
		SET  @aErrNo	= 3
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid'
		GOTO LABEL_END	 
	 END
	 
	-- 20060723(milkji):老馆牢捞搁 resource啊 沥犬秦具父 沥惑捞促.
	IF((1 = @pUserAuth) AND (0 = @pIsEqualRsc))
	 BEGIN
		SET  @aErrNo	= 92
		SET	 @pErrNoStr	= 'eErrNoRscKeyNotEqual'
		GOTO LABEL_END		 
	 END
	 
	IF((1 < @pUserAuth) AND (@aIp <> @pIp))
	 BEGIN
		SET  @aErrNo	= 93
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid'
		GOTO LABEL_END		 
	 END	 
	 
	IF(GETDATE() < @pCertify)
	 BEGIN
		SET  @aErrNo	= 9
		SET	 @pErrNoStr	= 'eErrNoUserBlocked'
		GOTO LABEL_END			
	 END	 

	IF(0 < @pWorldNo)
	 BEGIN
		SET  @aErrNo = 4
		
		IF(@aIp = @pIp)
		 BEGIN
			SET	 @pErrNoStr	= 'eErrNoUserChkAlreadyLogined'
			GOTO LABEL_END1	-- 悼老 IP狼 版快父 荤侩磊 沥焊甫 盎脚茄促.
		 END
		ELSE
		 BEGIN
			SET	 @pErrNoStr	= 'eErrNoUserLoginAnother'
			GOTO LABEL_END
		 END
	 END
	 	 
LABEL_END1:		 
	UPDATE dbo.TblUser 
	SET 
		[mCertifiedKey]=@pCertifiedKey, 
		[mIp]=@pIp, 		
		[mPcBangLv]=@pPcBangLv,
		[mIpEX] = @pIpEX
	FROM dbo.TblUser 
	WHERE ( [mUserNo] = @aUserNo ) 
			AND ([mDelDate]  = '1900-01-01')	
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 5
		GOTO LABEL_END	 
	END	 

LABEL_END:	
	
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

