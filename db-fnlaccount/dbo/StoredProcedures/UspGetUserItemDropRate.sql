/******************************************************************************
**		Name: UspGetUserItemDropRate
**		Desc: 蜡历狼 靛而啦/PK靛而啦阑 啊廉柯促.
**
**		Auth: 沥备柳
**		Date: 10.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetUserItemDropRate]
	@pUserNo		INT
	,	@pItemDrop	TINYINT	OUTPUT
	,	@pPKDrop	TINYINT OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET	@pItemDrop = 100
	SET	@pPKDrop = 0

	SELECT 
		@pItemDrop = mRandomValue, 
		@pPKDrop = mPKDrop
	FROM dbo.TblUserItemDropRate
	WHERE mUserNo = @pUserNo

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error
	END

	RETURN(0)

GO

