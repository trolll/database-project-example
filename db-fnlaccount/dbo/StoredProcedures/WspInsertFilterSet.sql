
CREATE PROCEDURE [dbo].[WspInsertFilterSet] 
	@mUserId VARCHAR(20),
	@mNm VARCHAR(20),
	@mLogType VARCHAR(200),
	@mReason VARCHAR(200),
	@mOutput VARCHAR(200),
	@mReturnCode SMALLINT OUTPUT 



AS
	SET NOCOUNT ON

	
	
		IF(SELECT COUNT(*) FROM dbo.TblUserFilterSet WHERE mUserId= @mUserId AND mNm=@mNm)>0
		BEGIN
			SET @mReturnCode = 2
		END
		ELSE
		BEGIN
			SET @mReturnCode = 1
			INSERT INTO dbo.TblUserFilterSet VALUES (getDate(),@mUserId,@mNm,@mLogType,@mReason,@mOutput)
		END
			
	
	
	
	SET NOCOUNT OFF

GO

