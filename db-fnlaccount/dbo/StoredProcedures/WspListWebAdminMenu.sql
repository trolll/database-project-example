
CREATE    PROCEDURE [dbo].[WspListWebAdminMenu]
	@mUserNo INT = NULL
AS
	SET NOCOUNT ON

	IF @mUserNo IS NULL
	-- mUserNo가 NULL값이면 전부 얻는다.

		SELECT
			mMenuNo, mPageName, mMenuName
		FROM
			[dbo].[TblWebAdminMenu] WITH (NOLOCK)
		ORDER BY
			mOrder ASC

	ELSE
	-- mUserNo가 NULL이 아니면 권한별로 얻는다

		SELECT
			m.mMenuNo, mPageName, mMenuName
		FROM
			[dbo].[TblWebAdminMenu] AS m WITH (NOLOCK)
		INNER JOIN
			[dbo].[TblWebAdminUserAuth] AS u WITH (NOLOCK)
		ON
			m.mMenuNo = u.mMenuNo
		WHERE
			u.mUserNo = @mUserNo
		ORDER BY
			mOrder ASC

GO

