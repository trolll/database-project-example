CREATE PROCEDURE dbo.UspGetUserID  	
	@pUserNo	INT
	, @pUserID	VARCHAR(20)	OUTPUT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	SELECT 
		@pUserID = mUserID
	FROM dbo.TblUser
	WHERE mUserNo =  @pUserNo
			AND mDelDate = '1900-01-01'
		
	SELECT  @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR
	IF(0 <> @aErrNo) OR (0 = @aRowCnt)
	BEGIN
		RETURN(1)	-- 泅犁 拌沥 沥焊啊 粮犁窍瘤 臼促. 
	END	

    RETURN(0)

GO

