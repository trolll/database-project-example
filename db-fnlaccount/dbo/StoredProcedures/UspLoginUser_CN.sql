CREATE PROCEDURE dbo.UspLoginUser_CN
	 @pUserNo		INT
	,@pCertifiedKey	INT 
	,@pIp			CHAR(15)
	,@pWorldNo		SMALLINT
	,@pIpEX			BIGINT
	,@pPcBangLvEX	INT
	,@pIsNonClt		BIT					-- 1 : NonClient 사용자 	
	,@pUserId		VARCHAR(20)	OUTPUT 
	,@pUserAuth		TINYINT		OUTPUT
	,@pErrNoStr		VARCHAR(50)	OUTPUT
	,@pLeftChat		INT			OUTPUT	-- 채팅 쓰기금지 남은 시간.
	,@pEndBoard		DATETIME	OUTPUT	-- 게시판 쓰기금지가 종료되는 일시.
	,@pPcBangLv		INT			OUTPUT	-- EPcBangLv.
	,@pUseMacro		SMALLINT	OUTPUT -- 추가, 매크로 사용 횟수
	,@pIpEXUserNo	INT			OUTPUT
	,@pJoinCode		CHAR(1)	OUTPUT  -- 조인코드 추가
	,@pTired  		CHAR(1)	OUTPUT  -- 중독방지 추가
	,@pChnSID 		CHAR(33)	OUTPUT  -- 중국신분증번호 추가
	,@pNewId  		BIT     	OUTPUT  -- 신규계정여부 추가
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo		INT,
			@aRowCount	INT
			
	SELECT	@aErrNo		= 0,
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@aRowCount = 0;
	
	
	DECLARE	@aWorld			SMALLINT
	DECLARE	@aIp			CHAR(15)
	DECLARE @aKey			INT
	DECLARE @aLoginTm		DATETIME
	DECLARE	@aCertify			DATETIME
	DECLARE @aLastDiffNonCltUse		SMALLDATETIME

	
	SELECT	
			@pUserId=RTRIM(a.[mUserId])
			, @pUserAuth=a.[mUserAuth]
			, @aWorld=a.[mWorldNo]
			, @aIp=a.[mIp]
			, @aKey=ISNULL(a.[mCertifiedKey],0)
			, @aLoginTm=a.[mLoginTm]
			, @pLeftChat=b.[mChat]
			, @pEndBoard=b.[mBoard]
			, @pPcBangLv=[mPcBangLv]
			, @aCertify=b.[mCertify]
			, @pUseMacro = mUseMacro			
			, @pJoinCode = mJoinCode			
			, @pTired = mTired
			, @pChnSID = mChnSID
			, @pNewId = mNewId
	FROM 
			dbo.TblUser  AS a 
				LEFT OUTER JOIN dbo.TblUserBlock   AS b 
			ON a.mUserNo = b.mUserNo
	WHERE 
			a.mUserNo = @pUserNo 
			AND a.mDelDate = '1900-01-01';					
	
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;	
	IF (@aRowCount = 0) OR (@aErrNo <> 0)
	BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoUserNotExistId4';
		RETURN(@aErrNo)
	END
	 
	IF(@pCertifiedKey <> @aKey)
	BEGIN
		SET  @aErrNo	= 2;
		SET	 @pErrNoStr	= 'eErrNoUserDiffCertifiedKey';
		RETURN(@aErrNo);	 
	END
	 

	IF( GETDATE() < @aCertify )		-- 인증 블락 추가 
	BEGIN
		SET  @aErrNo	= 9;
		SET	 @pErrNoStr	= 'eErrNoUserBlocked';
		RETURN(@aErrNo);		
	END		 
	
	IF(0 = @pUserAuth)
	BEGIN
		SET  @aErrNo	= 5;
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid';
		RETURN(@aErrNo);	 
	END
	
	-- 2007.10.08 soundkey 한계정당 한개IP 제한을 위해 추가
	-- @pIpEX 가 TblUser.mIpEX에 있다면 해당 UserNo를 반환시킴
	IF( 0 < @pPcBangLvEX  )	-- @pPcBangLvEX 일정이상이면
	BEGIN
		SELECT 	
			TOP 1		
				@pIpEXUserNo = mUserNo 
		FROM 
				dbo.TblUser 
		WHERE 
				mIpEX = @pIpEX 
					AND mWorldNo > 0 
					AND mPcBangLv = @pPcBangLvEX; 

		SELECT @aRowCount = @@ROWCOUNT;

		IF @aRowCount > 0
		BEGIN
			SET	@aErrNo	= 10;
			SET	@pErrNoStr	= 'eErrNoUserCantDupLoginIp';
			RETURN(@aErrNo);	 
		END		
	END
	
	-- 2008.01.11 김광섭(논클라이언트 사용자 여부)
	IF(0<>@pIsNonClt)
		SET @aLastDiffNonCltUse = GETDATE();
	ELSE
		SET @aLastDiffNonCltUse = NULL;


	IF(0  < @aWorld)
	BEGIN
		IF(@aIp = @pIp)
		BEGIN
			SET  @aErrNo = 0;	-- mWorld > 0 AND 동일 IP 로그인시 이 부분은 0값으로 Return
			--SET	 @pErrNoStr	= 'eErrNoUserChkAlreadyLogined'						
		END
		ELSE
		BEGIN
			SET  @aErrNo = 7;
			SET	 @pErrNoStr	= 'eErrNoUserLoginAnother';
		END		 
		
		RETURN(@aErrNo);			 
	END	 

	-- login info update 	
	UPDATE 
			dbo.TblUser 
	SET 
			[mIp]=@pIp
			, [mLoginTm]=GETDATE()
			, [mWorldNo]=@pWorldNo
			, [mIpEX] = @pIpEX			
	WHERE 
			mUserNo = @pUserNo
			AND  mDelDate = '1900-01-01';
	
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF @aRowCount = 0 OR @aErrNo <> 0
	BEGIN
		SELECT  @pErrNoStr	= 'eErrSqlInternalError', @aErrNo = 8;					 
		RETURN(@aErrNo);		
	END
	
	IF(0<>@pIsNonClt)
	BEGIN

		EXEC @aErrNo = dbo.UspRegisterNonClientUser @pUserNo
		IF @aErrNo <>0
		BEGIN
			SELECT  @pErrNoStr	= 'eErrSqlInternalError', @aErrNo = 8;					 
			RETURN(@aErrNo);		
		END	
	END 
				
	RETURN(0);

GO

