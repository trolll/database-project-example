/******************************************************************************
**		Name: UspGetAccountGUID
**		Desc: 拌沥 锅龋俊措茄 昆哩 拌沥 GUID甫 掘绢柯促.
**
**		Auth: JUDY
**		Date: 2008.07.15
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**               
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetAccountGUID  	
	@pUserNo	INT
	, @pAccountGUID INT	OUTPUT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0, @pAccountGUID = 0;   
	
	SELECT 
		@pAccountGUID = mAccountGuid
	FROM dbo.TblUser
	WHERE mUserNo =  @pUserNo
			AND mDelDate = '1900-01-01'
		
	SELECT  @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR
	IF(0 <> @aErrNo) OR (0 = @aRowCnt)
	BEGIN
		RETURN(1)	-- 泅犁 拌沥 沥焊啊 粮犁窍瘤 臼促. 
	END	

	IF (@pAccountGUID IS NULL) OR (@pAccountGUID = 0)
	BEGIN
		RETURN(2)	-- 牢刘等 荤侩磊啊 酒凑聪促.
	END

    RETURN(0)

GO

