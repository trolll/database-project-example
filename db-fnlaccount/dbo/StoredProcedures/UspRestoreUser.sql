
CREATE Procedure dbo.UspRestoreUser
	 @pUserNo	INT
	,@pUserId	CHAR(20)		-- 기존에 삭제된 ID가 재사용될 수 있다.
------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0	
	
	UPDATE dbo.TblUser 
	SET 
		[mDelDate]='1900-01-01',	
		[mUserId]=@pUserId
	WHERE ([mUserNo] = @pUserNo) 
			AND ([mDelDate] > '1900-01-01')

	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 250014	-- eErrNoCannotUpdateTblUser
		GOTO LABEL_END
	END

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

