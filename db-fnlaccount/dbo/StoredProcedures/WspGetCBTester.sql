
CREATE             PROCEDURE [dbo].[WspGetCBTester]
	@mUserId VARCHAR(20)	-- 아이디
AS
	SET NOCOUNT ON

	SELECT
		mUserId,
		mRegDate
	FROM 
		[dbo].[TblUserCb]
	WHERE
		mUserId = @mUserId

GO

