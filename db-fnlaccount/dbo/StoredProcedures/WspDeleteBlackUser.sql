
CREATE      Procedure dbo.WspDeleteBlackUser
	  @mUserId	VARCHAR(20)		-- 계정
	, @mErrorCode	SMALLINT OUTPUT	-- 에러코드
AS
	SET NOCOUNT ON

	IF ( SELECT COUNT (*) FROM TblUserBlack WHERE mUserId = @mUserId ) > 0
	BEGIN
		SET	@mErrorCode = 1
		DELETE 	FROM TblUserBlack WHERE mUserId = @mUserId
	END

	ELSE
	BEGIN
		SET	@mErrorCode = -1
	END

	SET NOCOUNT OFF

GO

