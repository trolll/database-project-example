CREATE PROCEDURE [dbo].[UspRegisterDeathCnt]
	 @pSvrNo		SMALLINT        -- 서버번호
	,@pCntHT		BIGINT          -- 몬스터사냥시 사망
    ,@pCntTB		BIGINT          -- 팀배틀시 사망
    ,@pCntSC		BIGINT          -- 자살
    ,@pCntPK		BIGINT          -- PK
    ,@pCntSG		BIGINT          -- 공성중 사망
    ,@pCntEtc		BIGINT          -- 기타
    ,@pCntMon		BIGINT          -- 죽은 몬스터 수

AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE 	@aRv INT,
				@aErrNo	INT
			
	SELECT @aRv = 0, @aErrNo = 0

	INSERT dbo.TblDeathCnt (mSvrNo, mCntHunt, mCntTBattle, mCntSuicide, mCntPK, mCntSiege, mCntEtc, mCntMon) 
	VALUES (@pSvrNo, @pCntHT, @pCntTB, @pCntSC, @pCntPK, @pCntSG, @pCntEtc, @pCntMon)
	
	SELECT @aRv = @@ROWCOUNT,  @aErrNo = @@ERROR
	
	IF @aErrNo <> 0 OR @aRv <= 0 
	BEGIN
		RETURN(1) -- db error 
	END

	RETURN (0)	-- non error

GO

