
CREATE PROCEDURE [dbo].[WspApplyPsUser]	
	@pUserID	VARCHAR(20)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF EXISTS(	SELECT mUserID
				FROM dbo.TblPsUser
				WHERE mUserID = @pUserID )
	BEGIN
		DELETE dbo.TblPsUser
		WHERE mUserID = @pUserID
	END 
	
	INSERT INTO dbo.TblPsUser(mUserID)
	VALUES( @pUserID )
	
	RETURN (0)

GO

