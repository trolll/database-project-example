CREATE PROCEDURE dbo.UspChkUniquePc  	
     @pCharNm   	CHAR(12)				--Char Name
    ,@pSvrNo		SMALLINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	---------------------------------------------------
	-- 중복 캐릭터명 존재여부 체크 
	---------------------------------------------------		
	IF EXISTS (	SELECT * 
				FROM dbo.TblUniquePc
				WHERE mNm = @pCharNm)
	BEGIN
		RETURN(1)	-- eErrNoCharAlreadyExistNm
	END
	 
	---------------------------------------------------
	-- 캐릭터명 등록 
	---------------------------------------------------
	INSERT INTO dbo.TblUniquePc(mNm, mSvrNo)
	VALUES(@pCharNm, @pSvrNo)

    IF(0 <> @@ERROR)
	BEGIN
		RETURN(2)	-- DB Err
	END
	 
    RETURN(0)

GO

