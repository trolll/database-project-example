
CREATE     PROCEDURE [dbo].[WspListSecKey]
	@mUserId	VARCHAR(20)
AS
	SET NOCOUNT ON

	SELECT 
		Sc.mSecKey1, Sc.mSecKey2, Sc.mSecKey3, Sc.mSecKey4, Sc.mSecKey5, Sc.mSecKey6, Sc.mSecKey7, Sc.mSecKey8,
		Sc.mSecKey9, Sc.mSecKey10, Sc.mSecKey11, Sc.mSecKey12, Sc.mSecKey13, Sc.mSecKey14, Sc.mSecKey15,
		Sc.mSecKey16, Sc.mSecKey17, Sc.mSecKey18, Sc.mSecKey19, Sc.mSecKey20, Sc.mSecKey21, Sc.mSecKey22,
		Sc.mSecKey23, Sc.mSecKey24, Sc.mSecKey25, Sc.mSecKey26, Sc.mSecKey27, Sc.mSecKey28, Sc.mSecKey29,
		Sc.mSecKey30, Sc.mSecKey31, Sc.mSecKey32, Sc.mSecKey33, Sc.mSecKey34, Sc.mSecKey35, Sc.mSecKey36,
		Sc.mSecKey37, Sc.mSecKey38, Sc.mSecKey39, Sc.mSecKey40, Sc.mSecKey41, Sc.mSecKey42, Sc.mSecKey43,
		Sc.mSecKey44, Sc.mSecKey45, Sc.mSecKey46, Sc.mSecKey47, Sc.mSecKey48, Sc.mSecKey49, Sc.mSecKey50,
		Sc.mSetupDate, Sc.mReleaseDate
	FROM
		TblUser AS Us WITH (NOLOCK)
	INNER JOIN
		TblUserSecKeyTable AS Sc WITH (NOLOCK)
	ON
		Us.mUserNo = Sc.mUserNo
	WHERE 
		Us.mUserId = @mUserId

GO

