
CREATE PROCEDURE [dbo].[UspLogoutUser]
	 @pUserNo	INT,
	 @pUserChatBlockApplyTime	INT,
	 @pUseMacro 	SMALLINT	
AS

	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	DECLARE @aDate	DATETIME
	SET		@aDate = GETDATE()
	DECLARE	@aPlay	INT
	
	SELECT @aPlay=DATEDIFF(mi,[mLoginTm],@aDate) 
	FROM dbo.TblUser WITH (NOLOCK)		
	WHERE [mUserNo] = @pUserNo  
			AND ([mWorldNo] > 0 ) 
			AND ([mDelDate] = '1900-01-01')

	IF  @@ROWCOUNT <> 1 
	BEGIN
		SET  @aErrNo = 1
		RETURN(@aErrNo) 	 
	END
	
	BEGIN TRAN
	
		UPDATE dbo.TblUser 
		SET 
			[mLogoutTm]=@aDate, 
			[mWorldNo]=-[mWorldNo], 
			[mTotUseTm]=[mTotUseTm]+@aPlay,
			mUseMacro = @pUseMacro
		WHERE [mUserNo] = @pUserNo  
				AND ([mWorldNo] > 0 )  
				AND ([mDelDate] = '1900-01-01')
		IF @@ERROR<>0 
		BEGIN
			ROLLBACK TRAN
			RETURN(2)	-- db error 
		END

		UPDATE dbo.TblUserBlock
		SET [mChat] = 
			CASE 
				WHEN [mChat] IS NULL THEN  NULL
				WHEN [mChat] > @pUserChatBlockApplyTime THEN  [mChat] - @pUserChatBlockApplyTime
				WHEN [mChat] <= @pUserChatBlockApplyTime THEN 0
			END 
		WHERE [mUserNo] = @pUserNo
		IF @@ERROR<>0 
		BEGIN
			ROLLBACK TRAN
			RETURN(3)	-- db error 
		END
				
	COMMIT TRAN
 	RETURN(0)

GO

