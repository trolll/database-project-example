/******************************************************************************
**		File: WspGetUserInfoA.sql
**		Name: WspGetUserInfoA
**		Desc: 荤侩磊 沥焊甫 掘绰促
**
**              
**		Auth: 辫己犁
**		Date: 2006.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2006.11.21	JUDY				荤侩磊 沥焊(鼻茄, IP)
**		2007.01.30	JUDY				力犁 沥焊 炼扒巩 眠啊
**		2007.08.27	JUDY				mUseMacro
**		2007.11.09	soundkey			mType add
**		2008.03.26	JUDY				稠努扼捞攫飘 沥焊
**		2008.03.31	JUDY				喉钒 荤侩磊 沥焊 函版
**		2009.07.08	JUDY				窃荐疙 函版 
**		2010.06.08	JUDY				Drop Rate/PK Drop Rate 眠啊
**		20101026	JUDy				昆哩捞包
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[WspGetUserInfoA]
	@mUserNo BIGINT	-- 蜡历锅龋
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		u.mRegdate, mUserAuth, u.mUserNo, u.mUserId, mCertifiedKey
		, u.mIp, mLoginTm, mLogoutTm, mTotUseTm, mWorldNo, mDelDate
		, lbh.mType
		, mBlock = 
			CASE  
				WHEN ub.mCertify > GETDATE() THEN 1
				WHEN ub.mChat > 0 THEN 1
				WHEN ub.mBoard > GETDATE() THEN 1
			ELSE 0 END
		, ISNULL(lbh.mType,0) AS mBlockHistory 
		, mUserAuth
		, u.mIp  
		, mSecKeyTableUse
		, mUseMacro
		, mUserAuth
		, c.mCodeDesc AS mUserAuthDesc		 
		, unc.mRegDate AS mFirstRegDate
		, unc.mLastDiffNonCltKeyDate
		, unc.mCltKeyCnt 
		, lbh.mReason
		, uidr.mItemDrop
		, uidr.mPKDrop	
		
		, nwt.mRegDate mTranRegDate
		, nwt.mUserName mWebZenID
		, nwt.mHanGameUserID mTranHanGameID
		
	FROM  dbo.TblUser AS u
		LEFT OUTER JOIN	dbo.TblCode AS c
			ON u.mUserAuth = c.mCode
				AND c.mCodeType = 1
		LEFT OUTER JOIN dbo.TblUserNonClient AS unc
			ON u.mUserNo = unc.mUserNo
		LEFT OUTER JOIN dbo.TblUserBlock AS ub
			ON u.mUserNo = ub.mUserNo		
		LEFT OUTER JOIN dbo.UfnGetLastBlockHistory( @mUserNo )  AS lbh
			ON u.mUserNo = lbh.mUserNo
		LEFT OUTER JOIN dbo.TblUserItemDropRate AS uidr
			ON u.mUserNo = uidr.mUserNo	
		LEFT OUTER JOIN dbo.TblNnhToWebzenTransfer AS nwt
			ON u.mUserNo = nwt.mUserNo	
	WHERE u.mUserNo = @mUserNo

GO

