/******************************************************************************
**		Name: UspResetUserItemDropRate
**		Desc: 蜡历狼 靛而啦/PK靛而啦 檬扁拳
**
**		Auth: 沥备柳
**		Date: 10.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspResetUserItemDropRate]
	@pUserNo			INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- 靛而啦 汲沥 饶 盎脚朝楼 函版
	UPDATE dbo.TblUserItemDropRate
	SET mItemDrop = 100, 
		mPKDrop = 0, 
		mRandomValue = 100, 
		mRandomFlag = 1, 
		mRenewalDate = GetDate(),
		mCheckRegWeek = GetDate(),
		mPShopTm = 0,
		mTotalTm = 0
	WHERE mUserNo = @pUserNo
	
	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error 
	END

	RETURN (0)

GO

