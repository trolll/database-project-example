----------------------------------------------------------------------------------------------------------------
-- CREATE SCRIPT 
----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE dbo.UspRegisterNonClientUser  	
	@pUserNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	UPDATE dbo.TblUserNonClient
	SET
		mLastDiffNonCltKeyDate = GETDATE()
		, mCltKeyCnt = mCltKeyCnt + 1
	WHERE mUserNo = @pUserNo
	
	SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF @aErrNo <> 0
	BEGIN	
		RETURN(1)	-- db error
	END 
	
	IF @aRowCnt = 0 
	BEGIN
		
		INSERT INTO dbo.TblUserNonClient(mUserNo)
		VALUES(@pUserNo)
		
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN	
			RETURN(1)	-- db error
		END 		
	
	END 

    RETURN(0)

GO

