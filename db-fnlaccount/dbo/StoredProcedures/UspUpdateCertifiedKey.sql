/******************************************************************************
**		Name: UspUpdateCertifiedKey
**		Desc: 牢刘虐父 函版矫挪促.
**
**		Auth: 辫 堡挤
**		Date: 2010-08-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010-10-28	辫堡挤				泅犁 PC 规 饭骇阑 倒妨林绰 扁瓷 眠啊
*******************************************************************************/
CREATE PROCEDURE dbo.UspUpdateCertifiedKey
	 @pUserNo			INT
	,@pCertifiedKey		INT
	,@pUserId			CHAR(20)	OUTPUT
	,@pUserAuth			TINYINT		OUTPUT
	,@pSecKeyTableUse	TINYINT		OUTPUT
	,@pPCBangLv			INT			OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT,
			@aRowCount	INT
			
	SELECT
		  @pUserId 			= [mUserId]
		, @pUserAuth		= [mUserAuth]
		, @pSecKeyTableUse 	= [mSecKeyTableUse]
		, @pPCBangLv		= [mPcBangLv]
	FROM	dbo.TblUser
	WHERE	[mUserNo] = @pUserNo;
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF(@aRowCount = 0)
	 BEGIN
		RETURN(1);
	 END

	UPDATE	dbo.TblUser 
	SET 
		[mLoginTm]		= GETDATE(),
		[mCertifiedKey] = @pCertifiedKey
	WHERE	[mUserNo] = @pUserNo;
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF @aRowCount = 0 OR @aErrNo <> 0
	 BEGIN
		RETURN(2);
	 END

	RETURN(0);

GO

