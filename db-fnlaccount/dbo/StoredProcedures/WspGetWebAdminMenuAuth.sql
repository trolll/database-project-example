
CREATE      PROCEDURE [dbo].[WspGetWebAdminMenuAuth]
	@mPageName VARCHAR(100),
	@mUserNo INT
AS
	SET NOCOUNT ON

	SELECT
		COUNT(*) AS mAccess
	FROM
		[dbo].[TblWebAdminUserAuth] AS u
	INNER JOIN
		[dbo].[TblWebAdminMenu] AS m
	ON
		u.mMenuNo = m.mMenuNo
	WHERE
		u.mUserNo = @mUserNo
		AND m.mPageName = @mPageName

GO

