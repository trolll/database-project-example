

CREATE       Procedure dbo.WspGetTemplateList
  @mTemplateId SMALLINT  


AS
SET NOCOUNT ON 

SELECT  mTemplateNo,mTemplateId, mTitle, mContent,mUserId,mRegDate 
 FROM FNLAccount.dbo.TblTemplate 
 WHERE mTemplateId=@mTemplateId 
 

SET NOCOUNT OFF

GO

