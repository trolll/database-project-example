--FNLAccount
CREATE PROCEDURE [dbo].[WspAutoDetachPenalty]                       
AS
SET NOCOUNT ON
                 
declare @now datetime
select @now = getdate() 
                  
BEGIN TRAN

   DELETE  FROM [dbo].[TblUserBlock] 
   FROM [dbo].[TblUserBlock] B,[dbo].[TblUserBlockHistory] H     
   WHERE 
        B.mUserNo = H.mUserNo 
        AND
        H.mDeatchDate is NUll  
        AND H.mEnddate<@now
   IF @@ERROR<>0
        ROLLBACK TRAN
		        
   UPDATE [dbo].[TblUserBlockHistory] 
   SET mDeatchDate=@now 
   WHERE mDeatchDate is NUll  
   AND mEnddate<@now
   IF @@ERROR<>0
        ROLLBACK TRAN

   COMMIT TRAN

GO

