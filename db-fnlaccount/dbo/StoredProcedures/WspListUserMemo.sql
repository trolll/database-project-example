
CREATE   PROCEDURE [dbo].[WspListUserMemo]
	@mUserNo INT -- 사용자 번호
AS
	SET NOCOUNT ON

	SELECT
		m.mMemoText
		, u.mUserId
		, m.mRegDate
	FROM
		[dbo].[TblUserMemo] AS m WITH (NOLOCK)
	INNER JOIN
		[dbo].[TblUser] AS u WITH (NOLOCK)
	ON
		m.mManageUserNo = u.mUserNo
	WHERE
		m.mUserNo = @mUserNo
	ORDER BY
		m.mRegDate DESC

GO

