/******************************************************************************
**		Name: UspGetUserID_GCPS
**		Desc: 拌沥 锅龋俊措茄 荤侩磊 酒捞叼甫 馆券茄促.(昆哩 捞包 措惑磊)
**
**		Auth: JUDY
**		Date: 2010.10.11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**               
*******************************************************************************/
CREATE PROCEDURE dbo.UspGetUserID_GCPS  	
	@pUserNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aAccountGuid INT,
			@aUserID VARCHAR(20);

	SELECT @aErrNo = 0, @aRowCnt = 0, @aUserID = NULL;  
	
	SELECT 
		@aUserID = mUserID
		, @aAccountGuid = mAccountGuid		-- 昆哩 拌沥 GUID
	FROM dbo.TblUser
	WHERE mUserNo =  @pUserNo
			AND mDelDate = '1900-01-01'
		
	SELECT  @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR
	IF(0 <> @aErrNo) OR (0 = @aRowCnt)
	BEGIN
		SELECT 1, @aUserID;	-- 泅犁 拌沥 沥焊啊 粮犁窍瘤 救促. 		
		
		RETURN;
	END	

	-- 捞包 脚没磊牢瘤 眉农 茄促. 
	IF SUBSTRING(@aUserID, 1, 1) = '@'
	BEGIN
		SELECT 2, @aUserID;	-- 茄霸烙俊辑 R2肺 捞包 脚没窍瘤 臼篮 荤侩磊捞促.
		
		RETURN; 
	END

	-- 捞包 脚没磊牢瘤 眉农 茄促. 
	IF @aAccountGuid = 0
	BEGIN
		SELECT 2, @aUserID;	-- 茄霸烙俊辑 R2肺 捞包 脚没窍瘤 臼篮 荤侩磊捞促.
		
		RETURN; 
	END

    SELECT 0, CONVERT(VARCHAR(20), @aAccountGuid);	-- 己傍

GO

