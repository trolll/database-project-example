

CREATE       Procedure dbo.WspGetTemplate
  @mTemplateId SMALLINT  
, @mTitle VARCHAR(50) 

AS
SET NOCOUNT ON 

SELECT  mTemplateNo,mTemplateId, mTitle, mContent,mUserId,mRegDate 
 FROM FNLAccount.dbo.TblTemplate 
 WHERE mTemplateId=@mTemplateId 
 AND mTitle=@mTitle

SET NOCOUNT OFF

GO

