CREATE PROC dbo.WspGetSCCardUseHistoryByUserId
(
	@pUserId	VARCHAR(20)
)
AS
/******************************************************************************
**		File: WspGetSCCardUseHistoryByUserId.sql
**		Name: WspGetSCCardUseHistoryByUserId
**		Desc: SC 墨靛 脚没, 劝己拳, 秦瘤 郴开 炼雀 (SC 荤侩 郴开 炼雀)
**
**		Auth: 2011.12.30
**		Date: 捞柳急
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		
*******************************************************************************/ 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT
		T1.mSecKeyTableUse
		, T1.mRegDate
	FROM dbo.TblSCCardUseHistory AS T1
		INNER JOIN dbo.TblUser AS T2
	ON T1.mUserNo = T2.mUserNo
	WHERE T2.mUserId = @pUserId
	ORDER BY T1.mRegDate

END

GO

