CREATE  PROCEDURE dbo.UspLoginUserEX2
	 @pUserNo		INT
	,@pCertifiedKey	INT 
	,@pIp			CHAR(15)
	,@pWorldNo		SMALLINT
	,@pIpEX			BIGINT
	,@pPcBangLvEX	INT
	,@pIsNonClt		BIT					-- 1 : NonClient ??? 	
	,@pUserId		VARCHAR(20)	OUTPUT 
	,@pUserAuth		TINYINT		OUTPUT
	,@pErrNoStr		VARCHAR(50)	OUTPUT
	,@pLeftChat		INT			OUTPUT	-- ?? ???? ?? ??.
	,@pEndBoard		DATETIME	OUTPUT	-- ??? ????? ???? ??.
	,@pPcBangLv		INT			OUTPUT	-- EPcBangLv.
	,@pUseMacro		SMALLINT	OUTPUT -- ??, ??? ?? ??
	,@pIpEXUserNo	INT			OUTPUT

AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo		INT,
			@aRowCount	INT
			
	SELECT	@aErrNo		= 0,
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@aRowCount = 0;
	
	
	DECLARE	@aWorld			SMALLINT
	DECLARE	@aIp			CHAR(15)
	DECLARE @aKey			INT
	DECLARE @aLoginTm		DATETIME
	DECLARE	@aCertify			DATETIME
	DECLARE @aLastDiffNonCltUse		SMALLDATETIME

	
	SELECT	
			@pUserId=RTRIM(a.[mUserId])
			, @pUserAuth=a.[mUserAuth]
			, @aWorld=a.[mWorldNo]
			, @aIp=a.[mIp]
			, @aKey=ISNULL(a.[mCertifiedKey],0)
			, @aLoginTm=a.[mLoginTm]
			, @pLeftChat=b.[mChat]
			, @pEndBoard=b.[mBoard]
			, @pPcBangLv=[mPcBangLv]
			, @aCertify=b.[mCertify]
			, @pUseMacro = mUseMacro			
	FROM 
			dbo.TblUser  AS a 
				LEFT OUTER JOIN dbo.TblUserBlock   AS b 
			ON a.mUserNo = b.mUserNo
	WHERE 
			a.mUserNo = @pUserNo 
			AND a.mDelDate = '1900-01-01';					
	
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;	
	IF (@aRowCount = 0) OR (@aErrNo <> 0)
	BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoUserNotExistId4';
		RETURN(@aErrNo)
	END
	 
	IF(@pCertifiedKey <> @aKey)
	BEGIN
		SET  @aErrNo	= 2;
		SET	 @pErrNoStr	= 'eErrNoUserDiffCertifiedKey';
		RETURN(@aErrNo);	 
	END
	 

	IF( GETDATE() < @aCertify )		-- ?? ?? ?? 
	BEGIN
		SET  @aErrNo	= 9;
		SET	 @pErrNoStr	= 'eErrNoUserBlocked';
		RETURN(@aErrNo);		
	END		 
	
	IF(0 = @pUserAuth)
	BEGIN
		SET  @aErrNo	= 5;
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid';
		RETURN(@aErrNo);	 
	END
	
	-- 2007.10.08 soundkey ???? ??IP ??? ?? ??
	-- @pIpEX ? TblUser.mIpEX? ??? ?? UserNo? ????
	IF( 0 < @pPcBangLvEX  )	-- @pPcBangLvEX ??????
	BEGIN
		SELECT 	
			TOP 1		
				@pIpEXUserNo = mUserNo 
		FROM 
				dbo.TblUser 
		WHERE 
				mIpEX = @pIpEX 
					AND mWorldNo > 0 
					AND mPcBangLv = @pPcBangLvEX; 

		SELECT @aRowCount = @@ROWCOUNT;

		IF @aRowCount > 0
		BEGIN
			SET	@aErrNo	= 10;
			SET	@pErrNoStr	= 'eErrNoUserCantDupLoginIp';
			RETURN(@aErrNo);	 
		END		
	END
	
	-- 2008.01.11 ???(?????? ??? ??)
	IF(0<>@pIsNonClt)
		SET @aLastDiffNonCltUse = GETDATE();
	ELSE
		SET @aLastDiffNonCltUse = NULL;


	IF(0  < @aWorld)
	BEGIN
		IF(@aIp = @pIp)
		BEGIN
			SET  @aErrNo = 0;	-- mWorld > 0 AND ?? IP ???? ? ??? 0??? Return
			--SET	 @pErrNoStr	= 'eErrNoUserChkAlreadyLogined'						
		END
		ELSE
		BEGIN
			SET  @aErrNo = 7;
			SET	 @pErrNoStr	= 'eErrNoUserLoginAnother';
		END		 
		
		RETURN(@aErrNo);			 
	END	 

	-- login info update 	
	UPDATE 
			dbo.TblUser 
	SET 
			[mIp]=@pIp
			, [mLoginTm]=GETDATE()
			, [mWorldNo]=@pWorldNo
			, [mIpEX] = @pIpEX			
	WHERE 
			mUserNo = @pUserNo
			AND  mDelDate = '1900-01-01';
	
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF @aRowCount = 0 OR @aErrNo <> 0
	BEGIN
		SELECT  @pErrNoStr	= 'eErrSqlInternalError', @aErrNo = 8;					 
		RETURN(@aErrNo);		
	END
	
	IF(0<>@pIsNonClt)
	BEGIN

		EXEC @aErrNo = dbo.UspRegisterNonClientUser @pUserNo
		IF @aErrNo <>0
		BEGIN
			SELECT  @pErrNoStr	= 'eErrSqlInternalError', @aErrNo = 8;					 
			RETURN(@aErrNo);		
		END	
	END 
				
	RETURN(0);

GO

