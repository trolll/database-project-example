/******************************************************************************
**		Name: UspUpdateUserItemDropRatePShopTm
**		Desc: 蜡历狼 俺牢惑痢 矫埃 穿利
**
**		Auth: 沥备柳
**		Date: 09.05.14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateUserItemDropRatePShopTm]
	@pUserNo				INT
	,	@pPShopOpeningTm	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- 拌沥捞 力犁等 坷配 蜡历啊 粮犁且 版快
	UPDATE dbo.TblUserItemDropRate
	SET mPShopTm = mPShopTm + @pPShopOpeningTm
	WHERE mUserNo = @pUserNo

	IF (@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error 
	END

	RETURN(0)

GO

