
/******************************************************************************
**		File: UspLoginUser.sql
**		Name: UspLoginUser
**		Desc: 유저 로그인
**			  1.오류코드는 CTrocLoginUser::__FetchUser()와 연결된다.	
**		
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2008.10.08	JUDY				중국 채널링 추가 지원
**		2008.12.02	김광섭			현재 접속중인 유저인 경우 접속 시도한 필드와 현재 접속중인 필드 서버가 동일한 경우에만 통과 시킨다.	
**		2009.01.22	JUDY				[임시]로그인 한 사용자를 대상으로 이벤트 아이템 지급
**		2009-04-30	김광섭			접속시 접속한 서버타입을 저장한다. (현재 접속중이지만 접속중인 서버가 카오스배틀 서버이면 접속을 허용한다.)
**		2010-08-18	김광섭			인증에 성공 혹은 실패시 항상 인증키를 변경하게 수정
**    	2010-12-15	김광섭			SC 사용 여부를 OUTPUT 으로 전달 받는다.
**		2016-09-21	nsm				배틀 서버로 이동 시 worldno 정보가 남아있어도 mIsMovingToBattleSvr 정보에 따라 처리   
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspLoginUser]
	 @pUserNo		INT
	,@pCertifiedKey	INT 
	,@pIp			CHAR(15)
	,@pWorldNo		SMALLINT
	,@pIpEX			BIGINT
	,@pPcBangLvEX	INT
	,@pIsNonClt		BIT					-- 1 : NonClient 사용자 	
	,@pUserId		VARCHAR(20)	OUTPUT 
	,@pUserAuth		TINYINT		OUTPUT
	,@pErrNoStr		VARCHAR(50)	OUTPUT
	,@pLeftChat		INT			OUTPUT	-- 채팅 쓰기금지 남은 시간.
	,@pEndBoard		DATETIME	OUTPUT	-- 게시판 쓰기금지가 종료되는 일시.
	,@pPcBangLv		INT			OUTPUT	-- EPcBangLv.
	,@pUseMacro		SMALLINT	OUTPUT -- 추가, 매크로 사용 횟수
	,@pIpEXUserNo	INT			OUTPUT
	,@pJoinCode		CHAR(1)	OUTPUT  -- 조인코드 추가
	,@pTired  		CHAR(1)	OUTPUT  -- 중독방지 추가
	,@pChnSID 		CHAR(33)	OUTPUT  -- 중국신분증번호 추가
	,@pNewId  		BIT     	OUTPUT  -- 신규계정여부 추가
	,@pSvrInfo		TINYINT				-- 접속한 서버 타입 ( 0 : 일반서버, 1 : 카오스 베틀 서버 )
	,@pNewCertifiedKey	INT				-- 변경할 인증키
	,@pNewCertifiedKeyOutput	INT	OUTPUT	-- 실제 변경된 인증키
	,@pSecKeyState	TINYINT		OUTPUT	-- SC 사용여부 결과
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo		INT,
			@aRowCount	INT
			
	SELECT	@aErrNo		= 0,
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@aRowCount = 0;
	
	
	DECLARE	@aWorld			SMALLINT
	DECLARE	@aIp			CHAR(15)
	DECLARE @aKey			INT
	DECLARE @aLoginTm		DATETIME
	DECLARE	@aCertify			DATETIME
	DECLARE @aLastDiffNonCltUse		SMALLDATETIME
	DECLARE	@aSvrInfo				TINYINT
  	   -- 20160921(nsm)
	DECLARE	@aIsMovingToBattleSvr	BIT

	
	SELECT	
			@pUserId=RTRIM(a.[mUserId])
			, @pUserAuth=a.[mUserAuth]
			, @aWorld=a.[mWorldNo]
			, @aIp=a.[mIp]
			, @aKey=ISNULL(a.[mCertifiedKey],0)
			, @aLoginTm=a.[mLoginTm]
			, @pLeftChat=b.[mChat]
			, @pEndBoard=b.[mBoard]
			, @pPcBangLv=[mPcBangLv]
			, @aCertify=b.[mCertify]
			, @pUseMacro = mUseMacro			
			, @pJoinCode = mJoinCode			
			, @pTired = mTired
			, @pChnSID = mChnSID
			, @pNewId = mNewId
			, @aSvrInfo	= mLoginSvrType
			, @pSecKeyState	= [mSecKeyTableUse]
            -- 20160921(nsm)
			, @aIsMovingToBattleSvr = a.[mIsMovingToBattleSvr]
	FROM 
			dbo.TblUser  AS a 
				LEFT OUTER JOIN dbo.TblUserBlock   AS b 
			ON a.mUserNo = b.mUserNo
	WHERE 
			a.mUserNo = @pUserNo 
			AND a.mDelDate = '1900-01-01';					
	
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;	
	IF (@aRowCount = 0) OR (@aErrNo <> 0)
	BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoUserNotExistId4';
		RETURN(@aErrNo)
	END
	
	-- 20100818 김광섭 - 만약 변경하는 키가 기존 키랑 동일한 경우 키를 변경한다. (확률이 1/42억 이라서 -_-ㅋ)
	IF(@pNewCertifiedKey = @aKey)
	BEGIN
		-- 20100818 김광섭 - 오버플로우 방지를 위해 무조건 음수로 만든다.
		IF(0 < @pNewCertifiedKey)
		 BEGIN
			SET	@pNewCertifiedKey = @pNewCertifiedKey * -1;
		 END
	
		-- 20100818 김광섭 - 값이 음수이기에 약간 랜덤한 양수를  더한다.
		SET	@pNewCertifiedKey = @pNewCertifiedKey + CONVERT(INT, RAND() * 100000000);
		
		-- 20100818 김광섭 - 그래도 같으면 부호만 변경시킨다.
		IF(@pNewCertifiedKey = @aKey)
		BEGIN
			SET @pNewCertifiedKey = @pNewCertifiedKey * -1;
		END
	END
	
	-- 20100818 김광섭 - 인증에 성공하든 실패하든 유저가 존재하는 경우 인증키를 변경한다.
	UPDATE	dbo.TblUser SET [mCertifiedKey] = @pNewCertifiedKey WHERE	[mUserNo] = @pUserNo;
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF(@aRowCount = 0 OR @aErrNo <> 0)
	BEGIN
		SELECT  @pErrNoStr	= 'eErrSqlInternalError', @aErrNo = 8;					 
		RETURN(@aErrNo);		
	END
	
	-- 20100818 김광섭 - 실제로 변경된 인증키를 저장한다.
	SET	@pNewCertifiedKeyOutput = @pNewCertifiedKey;
	 
	IF(@pCertifiedKey <> @aKey)
	BEGIN
		SET  @aErrNo	= 2;
		SET	 @pErrNoStr	= 'eErrNoUserDiffCertifiedKey';
		RETURN(@aErrNo);	 
	END
	 

	IF( GETDATE() < @aCertify )		-- 인증 블락 추가 
	BEGIN
		SET  @aErrNo	= 9;
		SET	 @pErrNoStr	= 'eErrNoUserBlocked';
		RETURN(@aErrNo);		
	END		 
	
	IF(0 = @pUserAuth)
	BEGIN
		SET  @aErrNo	= 5;
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid';
		RETURN(@aErrNo);	 
	END
	
	-- 2007.10.08 soundkey 한계정당 한개IP 제한을 위해 추가
	-- @pIpEX 가 TblUser.mIpEX에 있다면 해당 UserNo를 반환시킴
	IF( 0 < @pPcBangLvEX  )	-- @pPcBangLvEX 일정이상이면
	BEGIN
		SELECT 	
			TOP 1		
				@pIpEXUserNo = mUserNo 
		FROM 
				dbo.TblUser 
		WHERE 
				mIpEX = @pIpEX 
					AND mWorldNo > 0 
					AND mPcBangLv = @pPcBangLvEX; 

		SELECT @aRowCount = @@ROWCOUNT;

		IF @aRowCount > 0
		BEGIN
			SET	@aErrNo	= 10;
			SET	@pErrNoStr	= 'eErrNoUserCantDupLoginIp';
			RETURN(@aErrNo);	 
		END		
	END
	
	-- 2008.01.11 김광섭(논클라이언트 사용자 여부)
	IF(0<>@pIsNonClt)
		SET @aLastDiffNonCltUse = GETDATE();
	ELSE
		SET @aLastDiffNonCltUse = NULL;

		
	-- 20160921(nsm)
  	-- 로그인 시점에 [mIsMovingToBattleSvr] 정보 무조건 초기화
	UPDATE dbo.TblUser SET [mIsMovingToBattleSvr]=0 WHERE mUserNo=@pUserNo AND mDelDate='1900-01-01'; 
    -- IF(0 < @aWorld) 
  	IF( (0 < @aWorld) AND (0 = @aIsMovingToBattleSvr) )
	BEGIN
		-- 20081202(kslive) :  IP 와 서버 번호도 동일해야지 OK 다.
		-- IF(@aIp = @pIp)
		IF(@aIp = @pIp AND @aWorld = @pWorldNo)
		BEGIN
			
			SET  @aErrNo = 0;	-- mWorld > 0 AND 동일 IP 로그인시 이 부분은 0값으로 Return
			--SET	 @pErrNoStr	= 'eErrNoUserChkAlreadyLogined'						
		END
		-- 20090430(kslive) : IP 와 현재 접속중인 서버가 카오스배틀 서버이면 접속을 허용한다.
		ELSE IF(@aIp = @pIp AND @aSvrInfo = 1)
		BEGIN
			SET @aErrNo = 0;
		END
		ELSE
		BEGIN
			SET  @aErrNo = 7;
			SET	 @pErrNoStr	= 'eErrNoUserLoginAnother';
		END		 
		
		RETURN(@aErrNo);			 
	END	 

	-- login info update 	
	UPDATE 
			dbo.TblUser 
	SET 
			[mIp]=@pIp
			, [mLoginTm]=GETDATE()
			, [mWorldNo]=@pWorldNo
			, [mIpEX] = @pIpEX	
			, [mLoginSvrType] = @pSvrInfo
	WHERE 
			mUserNo = @pUserNo
			AND  mDelDate = '1900-01-01';
	
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF @aRowCount = 0 OR @aErrNo <> 0
	BEGIN
		SELECT  @pErrNoStr	= 'eErrSqlInternalError', @aErrNo = 8;					 
		RETURN(@aErrNo);		
	END
	
	IF(0<>@pIsNonClt)
	BEGIN

		EXEC @aErrNo = dbo.UspRegisterNonClientUser @pUserNo
		IF @aErrNo <>0
		BEGIN
			SELECT  @pErrNoStr	= 'eErrSqlInternalError', @aErrNo = 8;					 
			RETURN(@aErrNo);		
		END	
	END 

	-- 2009.01.22 JUDY
	-- 사용자들을 대상으로 선물함에 아이템 지급 한다.
	EXEC FNLBilling.dbo.UspRegGiftEvent @pUserNo					

	RETURN(0);

GO

