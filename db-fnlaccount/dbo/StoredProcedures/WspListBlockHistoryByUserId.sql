--fnlacount
CREATE   PROCEDURE [dbo].[WspListBlockHistoryByUserId]
	@userId  VARCHAR(20)
AS
	SET NOCOUNT ON
	SELECT 
		 (SELECT c.mCodeDesc from dbo.TblCode c WHERE c.mCode=bh.mType  AND c.mCodeType=4) as mCodeDesc, --????
		bh.mReason, --???譚
		convert(varchar(19),bh.mRegDate,20) AS mRegDate, --????
              	bh.mAttachUserID as mAttachUserNm ,--??諒
		bh.mDetachUserID, --榴檄
		bh.mDetachReason,--??覩凜
		--convert(varchar(19),bh.mDeatchDate,20) as mDeatchDate,--????
		--modified by mengdan 2008-12-2
		case
			when bh.mDeatchDate is null and bh.mEndDate<getdate()
				then convert(varchar(19),bh.mEndDate,20)
			else 
				convert(varchar(19),bh.mDeatchDate,20)
		end as mDeatchDate,		

		bh.mReasonFile as mReasonFile,--??
                           bh.mcomment,
		bh.mSeqNo
	FROM    dbo.TblUserBlockHistory As bh
	INNER JOIN dbo.TblUser As u
		ON u.mUserNo=bh.mUserNo
	WHERE u.mUserId = @userId

GO

