CREATE PROC dbo.WspCheckSCCardUseByUserId
(
	@pUserId	VARCHAR(20)
)
AS
/******************************************************************************
**		File: WspCheckSCCardUseByUserId.sql
**		Name: WspCheckSCCardUseByUserId
**		Desc: SC 脚没 咯何 眉农
**
**		Auth: 2011.12.30
**		Date: 捞柳急
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		
*******************************************************************************/
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	IF NOT EXISTS (
				SELECT *
				FROM dbo.TblUser
				WHERE mUserId = @pUserId
				AND mSecKeyTableUse <> 0
	)
	BEGIN
		RETURN 1; 
	END
	
	RETURN 0;
END

GO

