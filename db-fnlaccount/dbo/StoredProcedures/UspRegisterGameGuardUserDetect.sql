CREATE PROCEDURE dbo.UspRegisterGameGuardUserDetect  	
    @pUserNo int,
    @pLastGameGuardMsg VARBINARY(1024),
    @pIsDecryptSuccess BIT,
    @pNm varchar(12),
    @pPcNo int,
    @pSvrNo smallint
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	UPDATE dbo.TblUserGameGuardDetect
	SET    
		mLastGameGuardMsg = @pLastGameGuardMsg
		, mIsDecryptSuccess = @pIsDecryptSuccess
		, mLastUpdate = GETDATE()
		, mNm = @pNm
		, mPcNo = @pPcNo
		, mSvrNo = @pSvrNo
		, mTotCnt = mTotCnt + 1	-- 穿利冉荐
	WHERE  mUserNo = @pUserNo	

	SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF @aErrNo <> 0
	BEGIN	
		RETURN(1)	-- db error
	END 
	
	IF @aRowCnt <= 0 
	BEGIN
		INSERT INTO dbo.TblUserGameGuardDetect 
		(
			mUserNo
			, mLastGameGuardMsg
			, mLastUpdate
			, mNm
			, mPcNo
			, mSvrNo
			, mIsDecryptSuccess)
		VALUES( @pUserNo
			, @pLastGameGuardMsg
			, GETDATE()
			, @pNm
			, @pPcNo
			, @pSvrNo
			, @pIsDecryptSuccess)
	
	
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
		IF @aErrNo <> 0
		BEGIN	
			RETURN(1)	-- db error
		END 			
	END	
	
    RETURN(0)

GO

