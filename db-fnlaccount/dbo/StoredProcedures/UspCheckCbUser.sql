
CREATE Procedure dbo.UspCheckCbUser
	 @pUserId			CHAR(20)
------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0

	IF NOT EXISTS(SELECT * FROM TblUserCb WHERE [mUserId] = @pUserId)
	 BEGIN
		SET  @aErrNo	= 1
	 END

LABEL_END:	
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

