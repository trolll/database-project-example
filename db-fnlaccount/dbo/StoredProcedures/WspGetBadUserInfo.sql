
CREATE        PROCEDURE [dbo].[WspGetBadUserInfo]
	@mUserId VARCHAR(20)	-- 아이디
AS
	SET NOCOUNT ON

	SELECT
		u.mUserId, -- 계정
		-- 이름
		-- 주민등록번호
		-- 이메일
		--b.mCertify,
		--b.mBoard,
		b.mChat, -- 채팅
		(SELECT COUNT(*) FROM [dbo].[TblUserBlock] b
		 WHERE u.mUserNo = b.mUserNo AND GETDATE() < mCertify) AS mBlock, -- 0보다 클 경우, 계정상태 = 제재중
		(SELECT COUNT(*) FROM [dbo].[TblUserBlockHistory] bh
		 WHERE u.mUserNo = bh.mUserNo) AS mBlockHistory -- mBlock이 0이면서 이값이 0보다 클 경우, 계정상태 = 정상[제재 기록 있음]		
	FROM 
		[dbo].[TblUser] AS u
	INNER JOIN
		[dbo].[TblUserBlock] AS	b
	ON
		u.mUserNo = b.mUserNo 
	
	WHERE
		mUserId = @mUserId

GO

