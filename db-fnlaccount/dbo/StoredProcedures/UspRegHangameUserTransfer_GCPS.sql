/******************************************************************************
**		File: UspRegHangameUserTransfer_GCPS.sql
**		Name: UspRegHangameUserTransfer_GCPS
**		Desc: GCPS甫 烹窍咯 昆哩 捞包 脚没阑 殿废 茄促.  
**
**		Auth: 林叼
**		Date: 2010.10.10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:			Description:
**		--------	--------			---------------------------------------
**	   20101108		JUDY			拌沥GUID->拌沥ID 函版(涝仿牢厘 函版 救窃)
**     20101011	JUDY			康备 喉钒荤侩磊绰 捞包捞 阂啊瓷窍促. 
**	   20101018		JUDY			俺惯磊 夸备荐沥
**	   20101029		JUDY			捞固 牢刘等 拌沥篮 荤傈捞包 脚没 阂啊瓷栏肺 函版 
**	   20101029		JUDY			何啊辑厚胶 固荐飞磊 沥焊甫 盎脚 贸府 茄促.
**	   20101105		JUDY			拌沥GUID -> 拌沥ID 函版
*******************************************************************************/
CREATE PROCEDURE dbo.UspRegHangameUserTransfer_GCPS
	@pUserID VARCHAR(20)					-- 烹钦牢刘狼 拌沥 锅龋
	, @pUserName VARCHAR(20)				-- 烹钦牢刘狼 拌沥
	, @pHanGameUserID VARCHAR(20)		-- 茄霸烙 拌沥
	, @pIp VARCHAR(15)							-- 脚沥磊 IP
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;
	
	DECLARE @aUserNo	INT
		, @aRowCnt INT
		, @aErrNo INT
		, @aCertify DATETIME;
	SELECT @aUserNo = 0, @aRowCnt = 0, @aErrNo = 0, @aCertify = NULL;
	
	-- 2010.11.08 JUDY (捞包 脚没磊狼 input parameter狼 沥焊甫 函版 茄促. 昆哩GUID -> 昆哩 拌沥 函版)
	DECLARE @aAccountGuid	INT
		, @aAccountID	VARCHAR(20)
	SET @aAccountGuid = CONVERT(INT, @pUserID);
	SET @aAccountID = @pUserName;	-- 烹钦牢刘 拌沥 馆券
		
	-- 捞包 脚没 扁废捞 乐绰瘤 眉农 
	IF EXISTS(	SELECT * 
					FROM dbo.TblNnhToWebzenTransfer 
					WHERE mHanGameUserID = @pHanGameUserID  )
	BEGIN
		SET @aErrNo = 99; -- 捞固 捞包 肯丰等 拌沥捞促.
		GOTO T_END
	END
	
	-- 捞固 R2 捞侩 悼狼茄 拌沥俊 措秦辑 眉农 茄促. 
	IF EXISTS(	SELECT * 
					FROM dbo.TblUser 
					WHERE mUserID = @aAccountID  )
	BEGIN
		SET @aErrNo = 96; -- 捞固 捞侩吝牢 拌沥捞促.
		GOTO T_END
	END	
	
	-- 茄霸烙 拌沥 粮犁 眉农(眠饶, 拌沥 喉钒阑 眉农 茄促)
	SELECT 
		@aUserNo = T1.mUserNo
		, @aCertify = mCertify
	FROM dbo.TblUserHanGame T1
		LEFT OUTER JOIN dbo.TblUserBlock T2
			ON T1.mUserNo = T2.mUserNO
	WHERE mUserID = @pHanGameUserID
			AND mDelDate = '1900-01-01'	

	SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 98	-- R2甫 荤侩窍绰 茄霸烙 荤侩磊啊 酒聪促.
		GOTO T_END;
	END	
		
	IF 	@aErrNo <> 0
	BEGIN
		SET @aErrNo = 1		-- DB ERROR
		GOTO T_END;
	END	

	-- 康备 喉钒 眉农
	IF @aCertify =  '7777-07-07 00:00:00'
	BEGIN
		SET @aErrNo = 97		-- 喉钒 荤侩磊涝聪促. 喉钒 荤侩磊绰 捞悼 脚没 阂啊瓷钦聪促. 
		GOTO T_END;
	END	


	BEGIN TRANSACTION
		
		-- 殿废老, R2 拌沥 GUID, 蜡历 拌沥 锅龋, 蜡历 拌沥, 茄霸烙 拌沥, 殿废 IP
		INSERT INTO dbo.TblNnhToWebzenTransfer( mRegDate, mUserNo,mUserID,mUserName,mHanGameUserID,mIp)
		VALUES ( GETDATE(), @aUserNo, @pUserID, @pUserName, @pHanGameUserID, @pIp)
	
		SELECT @aErrNo = @@ERROR;	
		IF @aErrNo <> 0 
		BEGIN
			ROLLBACK TRANSACTION
			
			SET @aErrNo = 1;		-- DB ERROR
			GOTO T_END;
		END	
		
		-- 何啊辑厚胶 荤侩磊 沥焊 楷搬
		EXEC @aErrNo = FNLBILLING.dbo.UspUptADD_INVENTORY @aAccountID, @aUserNo
		IF @aErrNo <> 0 
			BEGIN
				ROLLBACK TRANSACTION
				
				SET @aErrNo = 1;		-- DB ERROR
				GOTO T_END;
		END			
	
		UPDATE dbo.TblUser
		SET
			mUserID = @aAccountID
			, mAccountGuid = @aAccountGuid
		WHERE mUserNo = @aUserNo
		
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
		IF @aErrNo <> 0 
		BEGIN
			ROLLBACK TRANSACTION
			
			SET @aErrNo = 1;		-- DB ERROR
			GOTO T_END;
		END		
			
	COMMIT TRANSACTION

T_END:
	SELECT @aErrNo, @aUserNo;

GO

