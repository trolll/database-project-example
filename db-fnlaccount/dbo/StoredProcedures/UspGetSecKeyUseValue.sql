
CREATE PROCEDURE	dbo.UspGetSecKeyUseValue
	@pUserNo		INT,		-- 사용자 계정번호
	@pSecKeyUseVal	INT OUTPUT,	-- 보안카드 활성화 정보
	@pErrNoStr 		VARCHAR(50) OUTPUT
As
Begin
	SET NOCOUNT ON
	
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@pSecKeyUseVal	= 0
			
	SELECT TOP 1 @pSecKeyUseVal = mSecKeyTableUse 
	FROM dbo.TblUser WITH (NOLOCK) 
	WHERE mUserNo = @pUserNo	

	SELECT	@pRowCnt = @@ROWCOUNT, 
			@pErr  = @@ERROR

	IF @pRowCnt = 0
	BEGIN 
		SET	@pErrNoStr	= 'eErrNoUserNotExistId4'
		RETURN(1)		-- 사용자 정보가 없다. 
	END 
	
	IF @pErr <> 0
	BEGIN 
		SET	@pErrNoStr	= 'eErrNoSqlInternalError'
		RETURN(@pErr)	-- SQL 내부 에러 
	END 
		
	RETURN(0)			-- None Error
End

GO

