
CREATE Procedure dbo.UspLogoutUserAll
	 @pWorldNo	SMALLINT	
----WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	DECLARE	@aDate		DATETIME
	SET		@aDate = GETDATE()
	
	UPDATE dbo.TblUser 
	SET 
		[mLogoutTm]=@aDate, 
		[mWorldNo]=-[mWorldNo], 
		[mTotUseTm]=[mTotUseTm]+DATEDIFF(mi,[mLoginTm],@aDate)
	WHERE ([mWorldNo] = @pWorldNo) 
			AND ([mDelDate] = '1900-01-01' )
	IF(0 <> @@ERROR)
	 BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END	 
	 END
	 	
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

