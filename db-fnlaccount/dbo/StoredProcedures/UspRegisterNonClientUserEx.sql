CREATE PROCEDURE dbo.UspRegisterNonClientUserEx  	
	@pUserNo	INT
	, @pBrokenMaxCnt	INT = 0
	, @pBrokenCnt	INT = 0
	, @pRandomMaxCnt	INT = 0		-- 夯挤 菩摹矫 滴俺狼 漠烦 沥焊 力芭
	, @pRandomCnt	INT = 0	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	UPDATE dbo.TblUserNonClientEx
	SET
		mBrokenMaxCnt = @pBrokenMaxCnt,
		mBrokenCnt = @pBrokenCnt,
		mLastUpdate = GETDATE(),
		mTotCnt = mTotCnt + 1
	WHERE mUserNo = @pUserNo
	
	SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
	IF @aErrNo <> 0
	BEGIN	
		RETURN(1)	-- db error
	END 
	
	IF @aRowCnt = 0 
	BEGIN
		
		INSERT INTO dbo.TblUserNonClientEx(mUserNo,mBrokenMaxCnt,mBrokenCnt)
		VALUES(@pUserNo,@pBrokenMaxCnt,@pBrokenCnt)
		
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN	
			RETURN(1)	-- db error
		END 		
	
	END 

    RETURN(0)

GO

