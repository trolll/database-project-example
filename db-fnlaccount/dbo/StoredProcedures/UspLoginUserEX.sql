CREATE  PROCEDURE dbo.UspLoginUserEX
	 @pUserNo		INT
	,@pCertifiedKey	INT 
	,@pIp			CHAR(15)
	,@pWorldNo		SMALLINT
	,@pIpEX			BIGINT
	,@pPcBangLvEX	INT
	,@pUserId		VARCHAR(20)	OUTPUT 
	,@pUserAuth		TINYINT		OUTPUT
	,@pErrNoStr		VARCHAR(50)	OUTPUT
	,@pLeftChat		INT			OUTPUT	-- 盲泼 静扁陛瘤 巢篮 矫埃.
	,@pEndBoard		DATETIME	OUTPUT	-- 霸矫魄 静扁陛瘤啊 辆丰登绰 老矫.
	,@pPcBangLv		INT			OUTPUT	-- EPcBangLv.
	,@pUseMacro		SMALLINT	OUTPUT -- 眠啊, 概农肺 荤侩 冉荐
	,@pIpEXUserNo	INT		OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo		INT,
			@aRowCount	INT
			
	SELECT	@aErrNo		= 0,
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@aRowCount = 0
	
	
	DECLARE	@aWorld		SMALLINT
	DECLARE	@aIp		CHAR(15)
	DECLARE @aKey		INT
	DECLARE @aLoginTm	DATETIME,
			@aCertify	DATETIME
	
	
	SELECT	@pUserId=RTRIM(a.[mUserId]), 
			@pUserAuth=a.[mUserAuth], 
			@aWorld=a.[mWorldNo], 
			@aIp=a.[mIp], 
			@aKey=ISNULL(a.[mCertifiedKey],0), 
			@aLoginTm=a.[mLoginTm], 
			@pLeftChat=b.[mChat], 
			@pEndBoard=b.[mBoard],
			@pPcBangLv=[mPcBangLv],
			@aCertify=b.[mCertify],
			@pUseMacro = mUseMacro			
	FROM dbo.TblUser  AS a 
			LEFT OUTER JOIN dbo.TblUserBlock   AS b 
				ON(a.[mUserNo] = b.[mUserNo])
	WHERE (a.[mUserNo] = @pUserNo) 
			AND (a.[mDelDate] = '1900-01-01')		
	
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR	
	IF @aRowCount = 0 OR @aErrNo <> 0
	BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoUserNotExistId4'
		RETURN(@aErrNo)
	END
	 
	IF(@pCertifiedKey <> @aKey)
	 BEGIN
		SET  @aErrNo	= 2
		SET	 @pErrNoStr	= 'eErrNoUserDiffCertifiedKey'
		RETURN(@aErrNo)	 
	 END
	 

	IF( GETDATE() < @aCertify )		-- 牢刘 喉遏 眠啊 
	BEGIN
		SET  @aErrNo	= 9
		SET	 @pErrNoStr	= 'eErrNoUserBlocked'
		RETURN(@aErrNo)		
	END		 
	
	IF(0 = @pUserAuth)
	 BEGIN
		SET  @aErrNo	= 5
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid'
		RETURN(@aErrNo)	 
	 END
	
	-- 2007.10.08 soundkey 茄拌沥寸 茄俺IP 力茄阑 困秦 眠啊
	-- @pIpEX 啊 TblUser.mIpEX俊 乐促搁 秦寸 UserNo甫 馆券矫糯
	IF( 0 < @pPcBangLvEX  )	-- @pPcBangLvEX 老沥捞惑捞搁
	BEGIN
		SELECT TOP 1 @pIpEXUserNo = mUserNo 
		FROM dbo.TblUser 
		WHERE mIpEX = @pIpEX 
			AND mWorldNo > 0 
			AND mPcBangLv = @pPcBangLvEX 
			
		SELECT @aRowCount = @@ROWCOUNT

		IF @aRowCount > 0
		BEGIN
			SET	@aErrNo	= 10
			SET	@pErrNoStr	= 'eErrNoUserCantDupLoginIp'
			RETURN(@aErrNo)	 
		END
	END
	
	IF(0  < @aWorld)
	BEGIN
		IF(@aIp = @pIp)
		BEGIN
			SET  @aErrNo = 0	-- mWorld > 0 AND 悼老 IP 肺弊牢矫 捞 何盒篮 0蔼栏肺 Return
			--SET	 @pErrNoStr	= 'eErrNoUserChkAlreadyLogined'						
		END
		ELSE
		BEGIN
			SET  @aErrNo = 7
			SET	 @pErrNoStr	= 'eErrNoUserLoginAnother'
		END		 
		RETURN(@aErrNo)			 
	END	 

	--------------------------------------------------
	-- 肺弊牢 沥焊 盎脚 
	--------------------------------------------------	
	UPDATE dbo.TblUser SET 
		[mIp]=@pIp, 
		[mLoginTm]=GETDATE(), 
		[mWorldNo]=@pWorldNo,
		[mIpEX] = @pIpEX
	WHERE ([mUserNo] = @pUserNo) 
			AND ([mDelDate] = '1900-01-01')
	
	SELECT @aRowCount = @@ROWCOUNT, @aErrNo = @@ERROR
	IF @aRowCount = 0 OR @aErrNo <> 0
	BEGIN
		SELECT  @pErrNoStr	= 'eErrSqlInternalError', @aErrNo = 8					 
		RETURN(@aErrNo)		
	END
				
	RETURN(0)

GO

