CREATE  PROCEDURE [dbo].[WspSameIpUserList]
	  @mIp	VARCHAR(20)
	, @Flag	INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @Flag = 0
	BEGIN
		SELECT 
			mRegDate, mUserAuth, mUserNo, mUserId, mIp, mLoginTm, mLogoutTm, mTotUseTm,
			mWorldNo, mPcBangLv 
		FROM dbo.TblUser
		WHERE mIp = @mIp
	END
	ELSE
	BEGIN
		SELECT 
			mRegDate, mUserAuth, mUserNo, mUserId, mIp, mLoginTm, mLogoutTm, mTotUseTm,
			mWorldNo, mPcBangLv 
		FROM dbo.TblUser
		WHERE mIp = @mIp 
				AND mWorldNo > 0
	END

GO

