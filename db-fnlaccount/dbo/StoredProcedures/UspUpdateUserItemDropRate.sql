/******************************************************************************
**		Name: UspUpdateUserItemDropRate
**		Desc: 蜡历狼 靛而啦/PK靛而啦 盎脚
**
**		Auth: 沥备柳
**		Date: 10.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateUserItemDropRate]
	@pUserNo			INT
	,	@pItemDrop		TINYINT
	,	@pPKDrop		TINYINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE		@aSetCount			SMALLINT
	DECLARE		@aDefaultItemDrop	TINYINT
	DECLARE		@aDefaultPKDrop		TINYINT
	
	-- 靛而啦 汲沥 饶 盎脚朝楼 函版
	UPDATE dbo.TblUserItemDropRate
	SET mItemDrop = @pItemDrop, 
		mPKDrop = @pPKDrop, 
		mRandomFlag = 1, 
		mRandomValue = (RAND()*(100 - @pItemDrop)) + @pItemDrop,
		mSetCount = 
				CASE 
					WHEN  mItemDrop = 100 AND mPKDrop = 0 
						THEN  mSetCount + 1
					ELSE 	mSetCount
				END, 		
		mRenewalDate = GetDate()
	WHERE mUserNo = @pUserNo
	
	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error 
	END

	RETURN (0)

GO

