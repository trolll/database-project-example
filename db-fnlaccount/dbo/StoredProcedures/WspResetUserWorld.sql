
CREATE PROCEDURE [dbo].[WspResetUserWorld]
	@mUserId VARCHAR(20)	-- 아이디
AS
	SET NOCOUNT ON

	UPDATE 
		TblUser
	SET
		mWorldNo = -mWorldNo
	WHERE
		mUserId = @mUserId AND mWorldNo > 0

	SET NOCOUNT OFF

GO

