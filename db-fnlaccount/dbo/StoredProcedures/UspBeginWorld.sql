CREATE PROCEDURE dbo.UspBeginWorld
	@pWorldNo	SMALLINT	 
------------------WITH ENCRYPTION
AS
	IF (0 = @pWorldNo)
		RETURN

	SET NOCOUNT ON		
	
	-- ?? ???? ? ??? ??? ??? ?? ???? ????.
	UPDATE dbo.TblUser WITH(ROWLOCK)
	SET [mCertifiedKey]=CONVERT(SMALLINT, ((RAND( (DATEPART(mm, GETDATE()) * 100000 )
           + (DATEPART(ss, GETDATE()) * 1000 )
           + DATEPART(ms, GETDATE()) ) * 10000)))	-- ? ???
	WHERE [mWorldNo] = -@pWorldNo 
		
	
	-- ??? ?? ??? ??
	UPDATE dbo.TblUser WITH(ROWLOCK)
	SET [mWorldNo]=-@pWorldNo,		-- ?? World? ???? ?? ?? 
	[mLogoutTm]=GETDATE(), 
	[mCertifiedKey]=CONVERT(SMALLINT, ((RAND( (DATEPART(mm, GETDATE()) * 100000 )
           + (DATEPART(ss, GETDATE()) * 1000 )
           + DATEPART(ms, GETDATE()) ) * 10000)))	-- ? ???
	WHERE [mWorldNo] = @pWorldNo
	
	RETURN(0)

GO

