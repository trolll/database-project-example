
CREATE       PROCEDURE [dbo].[WspGetSameIpUser1]
	  @mYear		INT
	, @mMonth		INT
	, @mDay			INT
	, @mHour		INT
	, @mIp			VARCHAR(20)
	, @mSvrNo		INT
AS
	SET NOCOUNT ON

	DECLARE	@mDate		DATETIME

	SET @mDate = CAST(STR(@mYear) + '-' + STR(@mMonth) + '-' + STR(@mDay) + ' ' + STR(@mHour) + ':00:00' AS DATETIME)

	IF (@mSvrNo =0 )
	 BEGIN
		SELECT
			  u.mUserId	--계정
			, b.mChat
			, (SELECT COUNT(*) FROM [dbo].[TblUserBlock] b 
			  WHERE u.mUserNo = b.mUserNo AND GETDATE() < mCertify) AS mBlock -- 0보다 크면 제재중이다
			, (SELECT COUNT(*) FROM [dbo].[TblUserBlockHistory] bh
			  WHERE u.mUserNo = bh.muserNo) AS mBlockHistory -- mBlock이 0 이면서 이 값이 0 보다 크면 제재 경우 있음
		FROM
			[dbo].[TblUser] AS u
		LEFT OUTER JOIN
			[dbo].[TblUserBlock] AS b
		ON
			u.mUserNo = b.mUserNo
		WHERE
			u.mLoginTm > @mDate AND mIp = @mIp
	
		
	 END
	ELSE
	 BEGIN
		SELECT
			  u.mUserId	--계정
			, b.mChat
			, (SELECT COUNT(*) FROM [dbo].[TblUserBlock] b 
			  WHERE u.mUserNo = b.mUserNo AND GETDATE() < mCertify) AS mBlock -- 0보다 크면 제재중이다
			, (SELECT COUNT(*) FROM [dbo].[TblUserBlockHistory] bh
			  WHERE u.mUserNo = bh.muserNo) AS mBlockHistory -- mBlock이 0 이면서 이 값이 0 보다 크면 제재 경우 있음
		FROM
			[dbo].[TblUser] AS u
		LEFT OUTER JOIN
			[dbo].[TblUserBlock] AS b
		ON
			u.mUserNo = b.mUserNo
		WHERE
			u.mLoginTm > @mDate AND mIp = @mIp AND  mWorldNo = @mSvrNo
	 END
	
	SET NOCOUNT OFF

GO

