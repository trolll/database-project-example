

CREATE   PROCEDURE [dbo].[WspListUserMemoA]
	@mUserId VARCHAR(20)
AS
	SET NOCOUNT ON

	SELECT
		m.mMemoText
		, m.mRegDate
	FROM
		[dbo].[TblUserMemo] AS m WITH (NOLOCK)

	WHERE
		m.mUserNo = (SELECT mUserNo FROM dbo.TblUser as u WHERE mUserId=@mUserId)
	ORDER BY
		m.mRegDate DESC

GO

