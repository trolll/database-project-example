--FnlAccount:

CREATE       Procedure dbo.WspInsertTemplate
  @mTemplateId SMALLINT  
, @mTitle VARCHAR(50) 
, @mContent VARCHAR(1000) 
, @mUserId VARCHAR(50)
, @mErrorCode SMALLINT OUTPUT
AS
SET NOCOUNT ON 

IF  ( SELECT COUNT (*) FROM TblTemplate WHERE mTemplateId = @mTemplateId AND 
mTitle=@mTitle) = 0
BEGIN
 SET @mErrorCode = 1
 INSERT INTO TblTemplate (mTemplateId,mTitle, mContent,mUserId) VALUES 
(@mTemplateId, @mTitle, @mContent, @mUserId)
END

ELSE
 SET @mErrorCode = -1

SET NOCOUNT OFF

GO

