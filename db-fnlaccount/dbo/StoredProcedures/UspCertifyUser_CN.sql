/******************************************************************************  
**  File: UspCertifyUser.sql  
**  Name: UspCertifyUser_CN  
**  Desc: 사용자 인증  
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  20110123 dmbkh  서버군이 동일하면 IP 주소 체크 없이 인증에 성공한다.(서버군 동일 여부는 게임서버에서 체크)
**                  서버군이 다르면 로그인 불가(게임서버에서 처리)
**                  서버군이 동일할 때 기존 유저는 항상 KICK된다(게임서버에서 처리)
**  20160921 nsm	배틀 서버로 이동 시 worldno 정보가 남아있어도 mIsMovingToBattleSvr 정보에 따라 처리  
*******************************************************************************/   
CREATE  PROCEDURE [dbo].[UspCertifyUser_CN]
	 @pUserId			VARCHAR(20)
	,@pUserPswd			VARCHAR(20)
	,@pIp				CHAR(15)
	,@pIpEX			BIGINT
	,@pCertifiedKey		INT			-- DB의 RAND()는 float므로 외부에서 주자.
	,@pIsEqualRsc		BIT
	,@pPcBangLv			INT			-- EPcBangLv.
	,@pUserNo			INT				OUTPUT
	,@pWorldNo			SMALLINT		OUTPUT
	,@pErrNoStr			VARCHAR(50)		OUTPUT
	,@pCertify			DATETIME		OUTPUT	-- 인증block이 종료되는 일시.
	,@pCertifyReason	VARCHAR(200)	OUTPUT	-- 인증block시 사유.
	,@pSecKeyTableUse	TINYINT	OUTPUT	-- 보안키테이블 사용여부
	,@pUserAuth			TINYINT	OUTPUT
	,@pIsAddUser		BIT	= 1
	,@pIsPwdCheck		BIT = 0
	,@pJoinCode			VARCHAR(1) = 'N'     -- pc방 조인코드
    ,@pLoginChannelID	CHAR(1) = 'N'        -- 채널링 ID
    ,@pTired      		CHAR(1) = 'N'        -- 중독방지 대상자냐?
    ,@pChnSID      		CHAR(33) = ''        -- 중국신분증번호
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'
	
	IF ( @pIsAddUser = 1 )
	BEGIN
		IF NOT EXISTS(	SELECT * 
						FROM dbo.TblUser 
						WHERE mUserId= @pUserId)
		BEGIN
			INSERT INTO dbo.TblUser([mUserId], [mUserPswd],[mJoinCode], mLoginChannelID, mTired, mChnSID, mNewId)	
			VALUES(@pUserId, 'nhngames', @pJoinCode, @pLoginChannelID, @pTired, @pChnSID, 1)
			
			-- added cash
			INSERT INTO dbo.Member([mUserId], [Cash])
			VALUES(@pUserId, 0)
		END
	END 
	
	DECLARE	@aIp		CHAR(15)
	DECLARE @aPswd		VARCHAR(20)
	DECLARE @aLoginTm	DATETIME
	DECLARE @aKey		INT
	DECLARE @aUserNo	INT
	-- 20160921(nsm)
	DECLARE @aIsMovingToBattleSvr	BIT
	
	SELECT 
		   @aUserNo = a.[mUserNo],
		   @pUserAuth=a.[mUserAuth], @aPswd=a.[mUserPswd], @pUserNo=a.[mUserNo], @pWorldNo=a.[mWorldNo], 
		   @aIp=a.[mIp], @aLoginTm=a.[mLoginTm], @aKey=a.[mCertifiedKey], 
		   @pCertify=b.[mCertify], @pCertifyReason=RTRIM(b.[mCertifyReason]),
		   @pSecKeyTableUse=a.[mSecKeyTableUse]
	FROM dbo.TblUser  AS a 
		LEFT OUTER JOIN dbo.TblUserBlock AS b  
		ON a.[mUserNo] = b.[mUserNo] 
	WHERE (a.[mUserId] = @pUserId) 
			AND ( a.[mDelDate] = '1900-01-01' )	
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoUserNotExistId3'
		GOTO LABEL_END
	 END		
	  
	-- 2008.02.11 judy
	IF ( @pIsPwdCheck = 1 )
	BEGIN
		IF ( RTRIM(@pUserPswd) <> RTRIM(@aPswd) )
		BEGIN
			SET  @aErrNo	= 1
			SET	 @pErrNoStr	= 'eErrNoUserDiffPswd'
			GOTO LABEL_END					
		END 
	END 
		  
	IF(0 = @pUserAuth)
	 BEGIN
		SET  @aErrNo	= 3
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid'
		GOTO LABEL_END	 
	 END
	 
	-- 20060723(milkji):일반인이면 resource가 정확해야만 정상이다.
	IF((1 = @pUserAuth) AND (0 = @pIsEqualRsc))
	 BEGIN
		SET  @aErrNo	= 92
		SET	 @pErrNoStr	= 'eErrNoRscKeyNotEqual'
		GOTO LABEL_END		 
	 END
	 
	IF((1 < @pUserAuth) AND (@aIp <> @pIp))
	 BEGIN
		SET  @aErrNo	= 93
		SET	 @pErrNoStr	= 'eErrNoAuthInvalid'
		GOTO LABEL_END		 
	 END	 
	 
	IF(GETDATE() < @pCertify)
	 BEGIN
		SET  @aErrNo	= 9
		SET	 @pErrNoStr	= 'eErrNoUserBlocked'
		GOTO LABEL_END			
	 END	 
	 
	-- 20160921(nsm)
	-- IF(0 < @pWorldNo)  
	IF( (0 < @pWorldNo) AND (0 = @aIsMovingToBattleSvr) )
	 BEGIN
		SET  @aErrNo = 4
		
		IF(@aIp = @pIp)
		 BEGIN
			SET	 @pErrNoStr	= 'eErrNoUserChkAlreadyLogined'
			GOTO LABEL_END1	-- 동일 IP의 경우만 사용자 정보를 갱신한다.
		 END
		ELSE
		 BEGIN
			SET	 @pErrNoStr	= 'eErrNoUserLoginAnother'
			GOTO LABEL_END
		 END
	 END
	 	 
LABEL_END1:		 
	UPDATE dbo.TblUser 
	SET 
		[mCertifiedKey]=@pCertifiedKey, 
		[mIp]=@pIp, 		
		[mPcBangLv]=@pPcBangLv,
		[mIpEX] = @pIpEX
		, mJoinCode = @pJoinCode
		, mLoginChannelID = @pLoginChannelID
		, mTired = @pTired
		, mChnSID = @pChnSID		
	FROM dbo.TblUser 
	WHERE ( [mUserNo] = @aUserNo ) 
			AND ([mDelDate]  = '1900-01-01')	
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 5
		GOTO LABEL_END	 
	END	 

LABEL_END:	
	
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

