CREATE    PROCEDURE [dbo].[checkuser]
as
	SET NOCOUNT ON

select muserid,mworldno,mloginchannelid
into ldhcheckuser
from tbluser with (nolock)
where mworldno > 0


declare @old_table_name varchar(200)
set @old_table_name ='ldhcheckuser'+convert(char(8),getdate(),112)+convert(char(2),getdate(),108)+convert(char(2),DATEPART(n,GETDATE()))

if exists (select name from dbo.sysobjects where id = object_id(N'ldhcheckuser') and OBJECTPROPERTY(id, N'IsUserTable') = 1  )
begin
exec sp_rename 'ldhcheckuser',@old_table_name
end

GO

