CREATE  PROCEDURE [dbo].[WspWriteUserMemo]
	@mUserNo INT = NULL,
	@mManageUserNo INT, 
	@mMemoText VARCHAR(100),
	@mArrayUserId VARCHAR(5000) = NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	IF @mUserNo IS NOT NULL
	BEGIN

		INSERT INTO
			[dbo].[TblUserMemo]
			(mUserNo, mManageUserNo, mMemoText)
		VALUES
			(@mUserNo, @mManageUserNo, @mMemoText)

		IF @@ERROR <> 0
			RETURN 1
	
		RETURN 0
	END
	
	INSERT INTO dbo.TblUserMemo(mUserNo, mManageUserNo, mMemoText, mRegDate)
	SELECT 
		u.mUserNo,
		@mManageUserNo ,
		@mMemoText,
		GETDATE()		
	FROM [dbo].[TblUser] AS u
		INNER JOIN dbo.fn_SplitTSQL(@mArrayUserId,',') a
			ON u.mUserId = a.element
	
	RETURN 0

GO

