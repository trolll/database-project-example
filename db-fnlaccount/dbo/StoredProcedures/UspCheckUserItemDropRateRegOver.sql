/******************************************************************************
**		Name: UspCheckUserItemDropRateRegOver
**		Desc: 蜡历啊 力犁等 殿废老肺 老林老捞 瘤车绰瘤 眉农
**
**		Auth: 沥备柳
**		Date: 10.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckUserItemDropRateRegOver]
	@pUserNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- 殿废老肺 何磐 老林老捞 瘤车绰瘤 犬牢
	DECLARE		@aStartTime		SMALLDATETIME
	DECLARE		@aDiff			INT	

	SELECT @aStartTime = mCheckRegWeek
	FROM dbo.TblUserItemDropRate
	WHERE mUserNo = @pUserNo

	SET @aDiff	= DATEDIFF(day, @aStartTime, GetDate())

	IF (@aDiff >= 7)
	BEGIN
		RETURN(1)
	END
	
	RETURN(0)

GO

