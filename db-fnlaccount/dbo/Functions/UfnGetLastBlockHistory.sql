CREATE FUNCTION dbo.UfnGetLastBlockHistory
( @aUserNo INT )
RETURNS TABLE
AS
RETURN

	SELECT TOP 1 
		mUserNo,		
		mType, 
		mRegDate,
		mReason
	FROM dbo.TblUserBlockHistory WITH(NOLOCK)
	WHERE mUserNo = @aUserNo
	ORDER BY mRegDate DESC

GO

