/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLAccount
Source Table          : TblTemplate
Date                  : 2023-10-04 18:57:39
*/


SET IDENTITY_INSERT [dbo].[TblTemplate] ON;

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (4, 0, '人物不可移动[处理完毕]', '您好，我们是R2运营部。
我们已将把您的角色传送到了附近安全的位置，为您
带来的不便我们深感抱歉。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'SC12281', '2008-01-23 10:56:00.530');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (5, 0, '投诉不良用户[信息正确受理]', '您好!我们是R2运营部
非常感谢您的举报，稍后我们将进行查证核实。
如确有不良现象，我们将对其进行制裁。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'SC12281', '2008-01-23 10:58:43.263');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (8, 0, '提交BUG[受理建议]', '您好!我们是R2运营部
非常感谢您提供的游戏建议，我们已把您提供的情况
如实记录并反馈给相关部门处理了！
R2运营部将竭诚为您服务，祝您游戏愉快。
', 'SC12281', '2008-01-23 11:03:17.530');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (10, 0, '不属于投诉箱的问题', '您好!我们是R2运营部
您提交的问题不属于投诉区受理范围,
请您到主页FAQ->1对1咨询中提交您的问题。
R2运营部将竭诚为您服务，祝您游戏愉快。
', 'SC12281', '2008-01-23 11:19:08.293');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (11, 0, '投诉不良用户[信息不全]', '您好!我们是R2运营部
请您提供服务器名称、角色名称、该玩家处在的具体地图区域以便我们的线上GM准确的核实。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'SC12273', '2008-02-19 15:05:56.563');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (12, 0, '人物不可移动[人物不在线]', '您好！我们是R2运营部。
查询目前您的角色已经不在线。
R2运营部竭诚为您服务，祝您游戏愉快。', 'SC12273', '2008-02-21 13:10:48.953');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (13, 0, '角色连续卡住', '您好，我们是R2运营部。经过我们技术部门调查，由
于使用了非法程序或游戏辅助程序才会出现卡号现
象，请您不要继续使用，如果由于使用辅助程序导致
再次卡号的，我们将不予进行处理，R2运营部将竭诚
为您服务，祝您游戏愉快。', 'SC12312', '2008-03-19 16:06:50.380');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (14, 0, '玩家无法提交1对1咨询', '您好，我们是R2运营部。
您可以在网页右上角的客服中心->FAQ提交相应问题
查询，如未搜索到，系统会自动提示在线咨询的链
接,请您登陆提交。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'SC12490', '2008-03-25 15:58:08.793');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (16, 0, '服务器太卡', '您好!我们是R2运营部
由于目前卡机现象的出现涉及网络环境、个人电脑配置等共六个环节，而其中有四个环节是网络游戏运营商暂时无法控制的。不过请您放心，我们也会有专人24小时监控服务器状况，并会不定期对服务器进行维护优化，以保证您可以正常游戏。同时，请您关注R2官方主页公告，如果是服务器登录系统维护或相关网络在调整，也将会有这样的情况。如果当时有大型活动在进行，那么可能是由于同一地图上聚集了较多的玩家，因而影响到游戏速度。您也可以关注一下您身边的其他玩家是否也有类似情况。另外请您不要在游戏中使用外挂程序，建议游戏时不要同时运行其他程序，以免因此而影响到您的正常游戏。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'SC12486', '2008-05-14 20:02:53.587');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (17, 0, '物品找回[非被盗类]', '您好!我们是R2运营部。非被盗类案件因牵涉玩家间纠纷，我们是无法介入处理的，请您谅解。R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90051', '2011-08-20 20:15:56.083');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (18, 0, '活动相关[已受理]', '  尊敬的玩家您好，我们是《R2》运营部门。您所反映的情况我们已经为您提交至有关部门进行处理，还请您耐心的等待。感谢您对《R2》的支持与关注，《R2》运营部门将竭诚为您服务。', 'CN90051', '2011-09-16 18:45:10.727');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (19, 0, '活动相关', '对于您提供的信息我们已经记录并提交至相关部门进行处理.还请您耐心等待。 R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-09-16 19:04:30.943');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (20, 0, '解封处理完毕', ' 尊敬的玩家您好，我们是《R2》运营部门。您的解封物品已经发放到您的背包中，请您到礼品箱查找。感谢您对《R2》的支持与关注，《R2》运营部门将竭诚为您服务。 ', 'CN90052', '2011-09-17 11:00:43.317');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (21, 0, '过了活动受理期', '您好!我们是R2运营部
已经过了活动受理期，我们无法为您查证，给您带来的不便我们深表歉意。R2运营部将竭诚为您服务，祝您游戏愉快。
', 'CN90052', '2011-09-17 12:38:49.310');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (22, 0, '耐心等待', '您好!我们是R2运营部。对于您提供的信息我们已经记录并提交至相关部门进行处理.还请您耐心等待。R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-09-17 20:30:37.810');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (24, 0, '扣留物品', '尊敬的玩家您好，我们是《R2》运营部门。您所反映的情况我们已经为您提交至有关部门进行处理，由于处理您所反映的问题需要一定的时间，因此还请您耐心的等待。感谢您对《R2》的支持与关注，《R2》运营部门将竭诚为您服务。', 'CN90052', '2011-09-20 14:03:29.380');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (25, 0, '举报外挂', '您好，我们是R2运营部。
非常感谢您的举报，稍后我们将进行查证核实。如确有不良现象，我们将对其进行制裁。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-09-22 10:33:39.537');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (26, 0, '工会图标', '尊敬的玩家您好，我们是《R2》运营部门。通过官网上传的工会图标均会在两个工作日内审核完毕，若超过该期限仍未显示，还请重新通过官网上传并确认上传图标的格式是否正确。
感谢您对《R2》的支持与关注，《R2》运营部门将竭诚为您服务。', 'CN90052', '2011-09-22 10:55:23.260');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (27, 0, '活动申诉期已过', '您好!我们是R2运营部。活动受理时间已过，我们无法为您查询，为您带来的不便，我们深表歉意。 R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-09-22 14:47:37.080');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (28, 0, '运气不好', '您好，我们是R2运营部。
建议您尝试换个区域尝试一下运气。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-09-24 20:20:39.407');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (29, 0, '账号被盗', '您好，我们是R2运营部。
请您去FAQ中提供被盗账号、服务器、角色名、被盗物品在游戏中的正确名称、被盗的时间，账号注册时的邮箱（如修改过邮箱需同时提交修改后的邮箱）注册姓名，注册身份证号码，注册身份证正反面扫描件以便我们进行查证拦截。同时建议您经常杀毒改密、不要与他人共享账号并且绑定密保卡以避免账号再次被盗。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-09-26 15:40:19.763');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (30, 0, '防沉迷', '您好，我们是R2运营部。
请您提供账号、服务器、角色名，账号注册时的邮箱（如修改过邮箱需同时提交修改后的邮箱）注册姓名，注册身份证号码，注册身份证正反面扫描件，游戏中防沉迷提示的清晰截图。以便我们尽快为您查证。R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-09-26 16:23:10.450');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (31, 0, '道具商城', '您好，我们是R2运营部。
游戏商城已经可以正常使用了，给您带来的不便我们深表歉意，请您见谅。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90051', '2011-09-27 18:37:40.593');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (32, 0, '盛世龙威 工会图标', '您好，我们是R2运营部。
盛世龙威服务器公会图标无法自助上传，建议公会会长通过FAQ在线咨询提交公会图标，我门将为您上传。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-10-02 21:31:27.470');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (33, 0, 'SC卡丢失', '您好，我们是R2运营部。
如果您需要解除密保的话您可以通过官网的FAQ提供您的账号，角色名，服务器，注册邮箱（如修改过邮箱需同时提交修改后的邮箱）注册姓名，注册身份证号码，清晰的注册身份证正反面扫描件。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-10-02 21:33:07.483');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (34, 0, '金币交易所出现问题', '您好！我们是R2运营部。
请您去在线FAQ提供帐号，服务器，角色名，和金币交易所相关问题截图，以便我们尽快为您处理。
R2运营部竭诚为您服务，祝您游戏愉快。', 'CN90052', '2011-10-12 22:16:42.753');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (35, 0, '金币交易无异常', '您好，我们是R2运营部。经过我们查证您的交易并无异常情况，请您再次进行核实。R2运营部将竭诚
为您服务，祝您游戏愉快。', 'CN90052', '2011-10-14 16:31:41.850');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (36, 0, '金币交易问题', '您好！我们是R2运营部。
请您详细提供具体的账号，服务器，角色名， 挂单具体时间（重要） ，挂单具体明细（重要）如多少金币兑换多少银币 兑换单数交易类型和抱错信息以便工作人员为您进行核实处理。
感谢您对R2的支持，祝您游戏愉快！', 'CN90051', '2011-10-15 12:55:10.743');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (37, 0, '充值问题', '您好！我们是R2运营部。
请您通过在线FAQ提供您的充值时间，账号，服务器，充值方式以及金额，同时提供您在官网的充值完整截图（截图需要完整显示：您的充值帐号，充值时间，充值金额，进入官网后，请您登陆您的帐号，页面左上角有帐号充值，点击进入，点击充值查询，请您提供该页面截图，截图需要完整显示：您的充值帐号，充值时间，充值金额，本次充值详情以及上次的充值详情。
。以便工作人员为您进行核实确认。
感谢您对R2的支持，祝您游戏愉快！', 'CN90052', '2011-10-16 13:04:27.227');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (38, 0, '金币不能购买物品', '您好!我们是R2运营部
请您登陆官网客服中心FAQ在线咨询提供您购买物品出现错误的截图（全屏的）以及您的帐号，服务器，角色名！
R2运营部将竭诚为您服务，祝您游戏愉快。
', 'CN90051', '2011-10-27 21:10:34.330');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (39, 0, '服务器无异常', '您好，经过技术人员核查，此时间段内服务器并无异常，角色死亡掉落物品是游戏的设定。建议您在今后的游戏中关闭一些不必要的辅助程序以确保网络的流畅。祝您游戏愉快~！
', 'CN90052', '2011-11-12 20:55:11.260');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (40, 0, '其他用户已认证或其他服务器认证问题', '您好!我们是R2运营部
该问题已经提交，且相关部门正在努力处理中，修复问题需要一定的时间，请您理解。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90051', '2012-01-30 11:04:29.940');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (41, 0, '仓库取不出来', '您好!我们是R2运营部。
仓库内取不出物品的问题，我们会尽快解决，并且仓库内物品暂时取不出来，不会有任何损失。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90047', '2012-02-23 21:38:18.257');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (42, 0, 'SC卡活动', '您好，我们是R2运营部。
您的问题我们已经记录，感谢您对我们活动的响应，再次建议您，经常更换密保卡以保证您账号的安全。
奖励正在统计抽取中，请耐心等待，祝您好运。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90051', '2013-06-18 11:00:40.607');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (43, 0, '55任务', '您好!我们是R2运营部
请您在官网客服中心在线咨询处提交您正在执行的任务列表和您点击任NPC后的对话截图（全部全屏）后通过附件形式上传并提交您的账号、角色名、服务器。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90051', '2013-06-25 20:50:55.340');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (44, 0, '55级任务', '您好!我们是R2运营部
请您在官网客服中心在线咨询处提交您正在执行的任务列表和您点击任NPC后的对话截图（全部全屏）后通过附件形式上传并提交您的账号、角色名、服务器。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90051', '2013-06-26 11:48:36.980');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (45, 0, '打不到东西', '您好，所有物品都有其珍惜性，也许您还没有打到掉落物品的怪物，我们也希望玩家能够多多通过游戏获得极品道具，祝您好运~！', 'CN90051', '2013-07-30 13:10:52.920');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (46, 0, '物品被扣押', '您好，我们是R2运营部。
请您在官网提交您的账号、角色名、服务器、注册邮箱和您注册的身份证照片，我们会帮您查询是什么原因导致的扣押。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90051', '2013-07-30 13:12:13.170');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (47, 0, '32天解封之咒', '您好!我们是R2运营部。
请您在官网客服中心提交账号、角色名、服务器、物品使用时的全屏截图和物品游戏内名称。我们会帮您查询。
R2运营部将竭诚为您服务，祝您游戏愉快。', 'CN90051', '2013-12-05 11:21:07.233');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (48, 0, '游戏闪退', '您好，关于您说的由于服务器的原因导致角色强制性下线的情况，我们已经记录，并已经反馈给相关部门进行核查了，如果服务器确实有问题，我们会及是处理解决。', 'CN90051', '2016-03-04 10:31:14.950');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (49, 0, '充值未到账', '您好，充值未到账请您不要着急，由于网络差异会导致导致时间的延误，如果24小时后还未到账，请您提供下您的充值时间，充值所在服务器，充值的角色ID，以及充值成功的截图，提交到官网客服中心，提交后我们会尽快为您查询处理。', 'CN90060', '2016-06-04 12:32:47.043');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (50, 0, '装备点碎了', '您好，您的心情我们十分理解，游戏中的道具物品强化失败会自行摧毁，这是游戏的设置，不在可控范围之内，我们也希望玩家强化能够多成功，多获得游戏内高强化道具，建议您可以换个时间或地点在进行尝试，祝您好运。', 'CN90060', '2016-07-06 13:43:19.640');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (51, 0, '打不过', '您好，请您不要着急，请您提供玩家具体名称和游戏中大概位置，我们会反馈相关工作人员进行核实。', 'CN90047', '2017-10-09 12:19:42.213');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (52, 0, '开箱子', '您好，您的心情我们十分理解，开启宝箱有机会获得宝箱内珍贵道具，此为几率问题，不在可控范围内。您可以考虑换个时间或地点尝试，我们也希望您能够多获得游戏内珍贵道具，祝您好运~！', 'CN90047', '2019-08-13 10:25:40.937');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (53, 0, '1.刺客冲级奖励', '您好，
您的问题已经记录并反馈给相关工作人员进行核实查证，如核实无误。符合活动条件预计次日18：00前为您发送，请注意查看礼品箱。祝您游戏愉快！', 'CN90047', '2019-09-25 17:34:39.403');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (54, 0, '2.游戏建议', '您好，
感谢您对本游戏提出的宝贵建议，
我们会积极反馈给相关工作人员，
在此祝您游戏愉快!', 'CN90047', '2019-09-25 17:37:34.520');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (55, 0, '3.账号被盗(第1次提交信息)', '您好，
请您提供被盗账号、服务器、角色名、被盗物品在游戏中的正确名称、被盗的时间.
同时建议您经常杀毒改密、不要与他人共享账号并且绑定密保卡以避免账号再次被盗。', 'CN90047', '2019-09-25 17:40:50.750');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (56, 0, '4.举报外挂', '您好，
非常感谢您的举报。举报信息需要提供相关截图，需要包含游戏完整界面截图，鼠标指向玩家，玩家头顶显示角色名称。按住键盘Print Screen/Sys Rq进行截图，聊天界面会显示截图，时间，再次按该键位截图，提供带有截图时间，玩家头顶角色名称的完整游戏界面截图。提供到官网客服中心，我们会反馈工作人员核实查看。 ', 'CN90047', '2019-09-25 17:42:48.003');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (57, 0, '5.取消密保卡', '您好，请您提供游戏账号，?角色名，所在服务器，手持注册身份证露脸照片，和身份证正面以及反面清晰照片，提交到官网客服中心，我们核实无误会为您取消密保卡，祝您游戏愉快！', 'CN90047', '2019-09-26 12:17:06.303');
GO

INSERT INTO [dbo].[TblTemplate] ([mTemplateNo], [mTemplateId], [mTitle], [mContent], [mUserId], [mRegDate]) VALUES (58, 0, '6.举报不良信息', '您好，
非常感谢您的举报。举报信息需要提供相关截图，需要包含游戏完整界面截图，按住键盘Print Screen/Sys Rq进行截图，聊天界面会显示截图，时间，再次按该键位截图，提供带有截图时间的截图到官网客服中心，我们会反馈工作人员核实查看。 ', 'CN90047', '2019-10-15 20:43:01.253');
GO

SET IDENTITY_INSERT [dbo].[TblTemplate] OFF;