/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLAccount
Source Table          : TblCode
Date                  : 2023-10-04 18:57:39
*/


INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 1, '扰乱秩序行为 (妨碍游戏正常进行');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 2, '辱骂及他人诽谤, 人格侮辱');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 3, '不适合的角色名');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 4, 'GM警告忽略 (妨碍游戏运营)');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 5, '聊天窗口发布重复信息');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 6, '现金交易及ID交易');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 7, '身份资料未到达');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 8, '公司诽谤及散布流言蜚语');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 9, ' Hacking');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 10, 'attachType欺诈');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 11, '虚假申请');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 12, '盗用他人身份证及个人信息不一致');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 13, '诈骗自称运营者');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 14, '使用未认可的Ｐrogram ');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 15, '商业性广告');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 16, '恶意攻击系统及Ｂug Ｐlay ');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 17, '网页 公告板中公布重复信息 ');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 18, '公告栏中辱骂及他人诽谤，侮辱人格的网页');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 19, '网页　商业性广告');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 20, '网页　公司诽谤及散布虚假事实');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 21, '诈称运营者及欺诈网页');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 22, '现金交易及ID交易的网页');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 23, '网页　散发未经许可的Program ');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 24, '网页  淫乱公告物');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 25, '网页  散布系统攻击内容及Ｂug恶用方法');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 26, '其它');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 27, '辱骂');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 28, '刷屏');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 29, '现金及帐号交易');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 30, '其它');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 31, '非法用户');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (5, 32, '本人认证适用状态下使用非法帐号');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 1, '公告栏停止使用1小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 2, '公告栏停止使用3小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 3, '公告栏停止使用5小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 4, '公告栏停止使用1天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 5, '公告栏停止使用3天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 6, '公告栏停止使用5　');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 7, 'ID停止使用１天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 8, 'ID停止使用3天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 9, 'ID停止使用５天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 10, 'ID停止使用10天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 11, 'ID停止使用15天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 12, 'ID停止使用30天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 13, '永久停止使用');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 14, '当事人ID申请制裁');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 15, 'Chatting停止使用 1小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 16, 'Chatting停止使用 3小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 17, 'Chatting停止使用 5小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 18, 'ID停止使用2天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 19, 'ID停止使用4天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 20, 'ID停止使用6天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 21, 'ID停止使用7天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 22, 'ID停止使用8天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 23, 'ID停止使用9天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 24, '禁止发贴1小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 25, '禁止发贴3小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 26, '禁止发贴5小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 27, '禁止发贴1天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 28, '禁止发贴3天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 29, '禁止发贴5天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 30, '禁止交谈1小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 31, '禁止交谈3小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 32, '禁止交谈5小时');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 33, '账号停用1天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 34, '账号停用2天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 35, '账号停用3天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 36, '账号停用4天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 37, '账号停用5天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 38, '账号停用6天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 39, '账号停用7天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 40, '账号停用8天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 41, '账号停用9天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 42, '账号停用10天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 43, '账号停用15天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 44, '账号停用30天');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (4, 45, '账号停用永久');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (3, 0, 'NotUse');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (3, 1, 'SecKeyIssue');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (3, 2, 'SecKeyActive');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (2, 0, 'PcBangLvInvalid');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (2, 1, 'PcBangLv1');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (2, 2, 'PcBangLv2');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (2, 3, 'PcBangLv3');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (2, 4, 'ePcBangLvNo');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 0, 'Block User');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 1, 'User');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 2, 'GM Level 7');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 3, 'GM Level 6');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 4, 'GM Level 5');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 5, 'GM Level 4');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 6, 'GM Level 3');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 7, 'GM Level 2');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 8, 'GM Level 1');
GO

INSERT INTO [dbo].[TblCode] ([mCodeType], [mCode], [mCodeDesc]) VALUES (1, 9, 'Supervisor Admin');
GO

