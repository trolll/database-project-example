CREATE FUNCTION [dbo].[UfnGetEndDate]
(
	 @pGetDate	DATETIME	-- 窃荐郴俊辑绰 厚犬沥利牢 GETDATE()甫 荤侩且 荐 绝促.
	,@pValidDay	INT
) RETURNS DATETIME
--------------WITH ENCRYPTION
AS
BEGIN
	DECLARE @aDate	DATETIME

	IF(10000 <= @pValidDay)
	 BEGIN
		SET	@aDate = '2079-01-01'
	 END
	ELSE
	 BEGIN
		SET @aDate = DATEADD(dd, @pValidDay, @pGetDate)
		SET @aDate = DATEADD(mi, -1, @aDate)	
	 END

	RETURN @aDate
END

GO

