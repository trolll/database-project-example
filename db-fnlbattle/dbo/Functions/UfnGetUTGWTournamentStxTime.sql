/******************************************************************************
**		File: 
**		Name: UfnGetUTGWTournamentStxTime
**		Desc: 配呈刚飘 矫累 矫埃阑 拌魂茄促.
**
**		Auth: 辫 堡挤
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    	2010-12-06	辫堡挤				岿 林扁狼 狼固啊 函版凳. (扁粮俊绰 1捞 捞锅崔捞瘤父 函版 饶 0 捞 捞锅崔烙)
*******************************************************************************/
CREATE FUNCTION [dbo].[UfnGetUTGWTournamentStxTime]
(
	  @pDate			DATETIME
	, @pPeriodMonth		INT
	, @pPeriodWeek		INT
	, @pDayOfTheWeek	INT
) RETURNS DATETIME
AS
BEGIN
	/**
		Warn.	秦寸 窃荐俊辑绰 @pDayOfTheWeek 客 SQL 郴何狼 夸老 鉴辑啊 悼老秦具茄促.
				泅犁 霸烙辑滚俊辑绰 老夸老何磐 矫累茄促. 窍瘤父 SQL 篮 郴何利栏肺 汲沥俊 蝶扼 函版捞 等促.
				馆靛矫 秦寸 窃荐甫 龋免窍绰 SP 俊辑绰 "SET DATEFIRST 7" 阑 龋免秦辑 郴何利栏肺 老夸老何磐 夸老捞 矫累登霸 函版秦具茄促.
				
				肚茄, 霸烙辑滚绰 夸老捞 0何磐 矫累捞瘤父 SQL 辑滚绰 1何磐 矫累茄促. 捞甫 嘎苗辑 秦寸 窃荐甫 龋免秦具茄促.
				秦寸 窃荐俊辑绰 @pDayOfTheWeek 牢磊啊 1 何磐 7鳖瘤狼 裹困甫 爱绊乐澜阑 啊沥茄促.
	*/

	DECLARE	@aRvDate			DATETIME;
	DECLARE	@aRvDayOfTheWeek	INT;
	
	/**
		1. 泅犁 朝楼俊辑 泅犁 老荐甫 猾促. (1岿 10老 版快 10老阑 猾促.)
			=> 瘤抄崔狼 付瘤阜 朝肺 汲沥等促.
		2. 秦寸 蔼俊 窍风甫 歹茄促.
			=> 泅犁 崔狼 1老肺 汲沥等促.
	*/
	SET	@aRvDate = DATEADD(dd, (DAY(@pDate) * -1) + 1, @pDate);
	
	-- 20101206(kslive) : 馆汗林扁狼 狼固啊 1 : 泅犁崔俊辑, 0 : 泅犁崔肺 函版茄促. 弊矾骨肺 DATAADD 俊辑 @pPeriodMonth 俊 啊皑绝捞 弊措肺 荤侩茄促.
	-- 馆汗林扁狼 崔阑 歹茄促. (1篮 泅犁 崔捞扁 锭巩俊 @pPeriodMonth 俊辑 1阑 猾父怒 歹茄促.
	SET	@aRvDate = DATEADD(mm, @pPeriodMonth, @aRvDate);
	
	-- 馆汗林扁狼 林甫 歹茄促. (1篮 霉林捞扁 锭巩俊 @pPeriodWeek 俊辑 1阑 猾父怒 歹茄促.
	SET	@aRvDate = DATEADD(wk, @pPeriodWeek - 1, @aRvDate);
	
	-- 馆汗 夸老苞 泅犁 备茄 夸老捞 促福搁 秦寸 夸老狼 朝楼肺 捞悼茄促.
	SET	@aRvDayOfTheWeek = DATEPART(dw, @aRvDate);
	IF(@aRvDayOfTheWeek <> @pDayOfTheWeek)
	 BEGIN
		IF(@aRvDayOfTheWeek < @pDayOfTheWeek)
		 BEGIN
			-- 馆汗 夸老捞 泅犁 备茄 朝楼狼 夸老焊促 奴 版快
			SET @aRvDate = DATEADD(dd, @pDayOfTheWeek - @aRvDayOfTheWeek, @aRvDate);
		 END
		ELSE
		 BEGIN
			-- 馆汗 夸老捞 泅犁 备茄 朝楼狼 夸老焊促 累篮 版快 
			SET @aRvDate = DATEADD(dd, -(@aRvDayOfTheWeek - @pDayOfTheWeek), @aRvDate);
		 END
	 END
	
	RETURN(@aRvDate);
END

GO

