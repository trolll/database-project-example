CREATE FUNCTION [dbo].[UfnGetEndDateByMin]
(
	 @pGetDate	DATETIME	-- 窃荐郴俊辑绰 厚犬沥利牢 GETDATE()甫 荤侩且 荐 绝促.
	,@pValidMin	INT
) RETURNS DATETIME
--------------WITH ENCRYPTION
AS
BEGIN
	DECLARE @aDate	DATETIME
	SET @aDate = DATEADD(mi, @pValidMin, @pGetDate)
	RETURN @aDate
END

GO

