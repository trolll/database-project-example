create FUNCTION [dbo].[fn_SplitTSQL]
  (@string varchar(8000), @separator CHAR(1) = N',') 
RETURNS TABLE
AS
RETURN
  SELECT
    n - LEN(REPLACE(LEFT(s, n), @separator, '')) + 1 AS pos,
    SUBSTRING(s, n,
      CHARINDEX(@separator, s + @separator, n) - n) AS element
  FROM (SELECT @string AS s) AS D
    JOIN dbo.Nums
      ON n <= LEN(s)
      AND SUBSTRING(@separator + s, n, 1) = @separator

GO

