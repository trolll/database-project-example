CREATE TABLE [dbo].[TblPetitionBoard] (
    [mPID]       BIGINT      IDENTITY (1, 1) NOT NULL,
    [mCategory]  INT         NOT NULL,
    [mRegDate]   DATETIME    NOT NULL,
    [mPcNo]      INT         NOT NULL,
    [mPosX]      FLOAT (53)  NOT NULL,
    [mPosY]      FLOAT (53)  NOT NULL,
    [mPosZ]      FLOAT (53)  NOT NULL,
    [mText]      CHAR (1000) NOT NULL,
    [mIpAddress] CHAR (20)   NOT NULL,
    [mIsFin]     BIT         CONSTRAINT [DF_TblPetitionBoard_mIsFin] DEFAULT ((0)) NOT NULL,
    [mFinDate]   DATETIME    CONSTRAINT [DF_TblPetitionBoard_mFinDate] DEFAULT (NULL) NULL,
    CONSTRAINT [PK_TblPetitionBoard] PRIMARY KEY CLUSTERED ([mPID] ASC, [mCategory] ASC, [mRegDate] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblPetitionBoard_1]
    ON [dbo].[TblPetitionBoard]([mRegDate] ASC, [mCategory] ASC, [mIsFin] ASC);


GO

CREATE NONCLUSTERED INDEX [NC_TblPetitionBoard_2]
    ON [dbo].[TblPetitionBoard]([mCategory] ASC, [mPcNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'柳沥辑', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'贸府 肯丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mFinDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'郴侩', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mText';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'柳沥辑 殿废 IP 林家', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mIpAddress';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Y 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mPosY';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'贸府 肯丰 咯何 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mIsFin';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Z 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mPosZ';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'柳沥辑 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mPID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'X 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mPosX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'柳沥辑 盒幅', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionBoard', @level2type = N'COLUMN', @level2name = N'mCategory';


GO

