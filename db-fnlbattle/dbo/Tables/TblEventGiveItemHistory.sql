CREATE TABLE [dbo].[TblEventGiveItemHistory] (
    [mSerialNo]        BIGINT  IDENTITY (1, 1) NOT NULL,
    [mEventID]         INT     NOT NULL,
    [mPos]             TINYINT NOT NULL,
    [mNo]              INT     NOT NULL,
    [IID]              INT     NOT NULL,
    [mCnt]             INT     NOT NULL,
    [mStatus]          TINYINT NOT NULL,
    [mAvaiable]        INT     NOT NULL,
    [mPracticalPeriod] INT     DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UNC_PK_TblEventGiveItemHistory] PRIMARY KEY NONCLUSTERED ([mSerialNo] ASC),
    CONSTRAINT [CK_TblEventGiveItemHistory_mPos] CHECK ([mPos]=(1) OR [mPos]=(0))
);


GO
ALTER TABLE [dbo].[TblEventGiveItemHistory] NOCHECK CONSTRAINT [CK_TblEventGiveItemHistory_mPos];


GO

CREATE CLUSTERED INDEX [CL_TblEventGiveItemHistory]
    ON [dbo].[TblEventGiveItemHistory]([mEventID] ASC, [mNo] ASC, [mPos] ASC);


GO

