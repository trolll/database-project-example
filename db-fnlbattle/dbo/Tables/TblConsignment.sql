CREATE TABLE [dbo].[TblConsignment] (
    [mRegDate]      SMALLDATETIME CONSTRAINT [DF_TblConsignment_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mCnsmNo]       BIGINT        IDENTITY (1, 1) NOT NULL,
    [mCategoryNo]   TINYINT       NOT NULL,
    [mPrice]        INT           NOT NULL,
    [mTradeEndDate] SMALLDATETIME NOT NULL,
    [mPcNo]         INT           NOT NULL,
    [mRegCnt]       INT           NOT NULL,
    [mCurCnt]       INT           NOT NULL,
    [mItemNo]       INT           NOT NULL,
    CONSTRAINT [UNC_PK_TblConsignment_mCnsmNo] PRIMARY KEY CLUSTERED ([mCnsmNo] ASC),
    CHECK ((0)<[mRegCnt]),
    CHECK ((0)<=[mCategoryNo] AND [mCategoryNo]<(14)),
    CHECK ((0)<=[mCurCnt])
);


GO

CREATE NONCLUSTERED INDEX [NC_TblConsignment_mPcNo]
    ON [dbo].[TblConsignment]([mPcNo] ASC);


GO

CREATE NONCLUSTERED INDEX [NC_TblConsignment_mCategoryNo_mTradeEndDate]
    ON [dbo].[TblConsignment]([mCategoryNo] ASC, [mTradeEndDate] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'俺寸 魄概 啊拜(角滚)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mPrice';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'困殴 魄概 抛捞喉', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废 俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mRegCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'魄概 辆丰 矫阿', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mTradeEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'巢篮 俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mCurCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'困殴 殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'魄概(殿废) PC 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'困殴 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mCnsmNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'墨抛绊府(DT_ITEM.IPShopItemType)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignment', @level2type = N'COLUMN', @level2name = N'mCategoryNo';


GO

