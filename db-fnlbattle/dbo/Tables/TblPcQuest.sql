CREATE TABLE [dbo].[TblPcQuest] (
    [mPcNo]      INT           NOT NULL,
    [mQuestNo]   INT           NOT NULL,
    [mValue]     INT           NOT NULL,
    [mBeginDate] SMALLDATETIME CONSTRAINT [DF_TblPcQuest_mBeginDate] DEFAULT (getdate()) NULL,
    [mEndDate]   SMALLDATETIME NULL,
    [mLimitTime] SMALLDATETIME NULL,
    CONSTRAINT [PK_CL_TblPcQuest] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mQuestNo] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblPcQuest_mQuestNo]
    ON [dbo].[TblPcQuest]([mQuestNo] ASC)
    INCLUDE([mValue], [mPcNo]);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'涅胶飘 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuest', @level2type = N'COLUMN', @level2name = N'mQuestNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'涅胶飘 柳青 肯丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuest', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'涅胶飘 矫累老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuest', @level2type = N'COLUMN', @level2name = N'mBeginDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'肯丰力茄 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuest', @level2type = N'COLUMN', @level2name = N'mLimitTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuest', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 涅胶飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuest';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'涅胶飘 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuest', @level2type = N'COLUMN', @level2name = N'mValue';


GO

