CREATE TABLE [dbo].[TblItemSerial] (
    [mSerialNo] BIGINT  IDENTITY (1, 1) NOT NULL,
    [mTm]       TINYINT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CL_TblItemSerial] PRIMARY KEY CLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'酒捞袍 矫府倔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblItemSerial';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付瘤阜 积己等 矫府倔 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblItemSerial', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'扁夯蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblItemSerial', @level2type = N'COLUMN', @level2name = N'mTm';


GO

