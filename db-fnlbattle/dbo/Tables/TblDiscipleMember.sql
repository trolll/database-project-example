CREATE TABLE [dbo].[TblDiscipleMember] (
    [mMaster]   INT      NOT NULL,
    [mDisciple] INT      NOT NULL,
    [mMemPoint] INT      CONSTRAINT [DF_TblDiscipleMember_mMemPoint] DEFAULT ((0)) NOT NULL,
    [mType]     TINYINT  CONSTRAINT [DF_TblDiscipleMember_mIsWait] DEFAULT ((3)) NOT NULL,
    [mRegDate]  DATETIME CONSTRAINT [DF_TblDiscipleMember_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [NC_PK_TblDiscipleMember_mDisciple] PRIMARY KEY NONCLUSTERED ([mDisciple] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE CLUSTERED INDEX [CL_TblDiscipleMember]
    ON [dbo].[TblDiscipleMember]([mMaster] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付胶磐 某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleMember', @level2type = N'COLUMN', @level2name = N'mMaster';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleMember', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'泅犁 糕滚 穿利器牢飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleMember', @level2type = N'COLUMN', @level2name = N'mMemPoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤力 某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleMember', @level2type = N'COLUMN', @level2name = N'mDisciple';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'荤力 糕滚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleMember';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'葛烙 郴俊辑狼 开且 (0:绝澜,1:荤何,2:力磊,3:措扁)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleMember', @level2type = N'COLUMN', @level2name = N'mType';


GO

