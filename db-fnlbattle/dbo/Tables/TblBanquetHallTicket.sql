CREATE TABLE [dbo].[TblBanquetHallTicket] (
    [mTicketSerialNo]  BIGINT        NOT NULL,
    [mTerritory]       INT           NOT NULL,
    [mBanquetHallType] INT           NOT NULL,
    [mBanquetHallNo]   INT           NOT NULL,
    [mOwnerPcNo]       INT           NOT NULL,
    [mRegDate]         SMALLDATETIME CONSTRAINT [DF_TblBanquetHallTicket_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mFromPcNm]        NVARCHAR (12) CONSTRAINT [DF_TblBanquetHallTicket_mFromPcNm] DEFAULT ('') NOT NULL,
    [mToPcNm]          NVARCHAR (12) CONSTRAINT [DF_TblBanquetHallTicket_mToPcNm] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_TblBanquetHallTicket] PRIMARY KEY CLUSTERED ([mTicketSerialNo] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'楷雀厘 萍南 矫府倔锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket', @level2type = N'COLUMN', @level2name = N'mTicketSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'楷雀厘 萍南', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'楷雀厘 鸥涝 ( 0: 家 楷雀厘 / 1 : 措 楷雀厘 )', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket', @level2type = N'COLUMN', @level2name = N'mBanquetHallType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康瘤锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'楷雀厘 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket', @level2type = N'COLUMN', @level2name = N'mBanquetHallNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'罐绰 某腐磐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket', @level2type = N'COLUMN', @level2name = N'mToPcNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'焊郴柯 捞狼 某腐磐 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHallTicket', @level2type = N'COLUMN', @level2name = N'mFromPcNm';


GO

