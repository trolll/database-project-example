CREATE TABLE [dbo].[TblPcSkillPackInventory] (
    [mRegDate] DATETIME      CONSTRAINT [DF_TblPcSkillPackInventory_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT           NOT NULL,
    [mSPID]    INT           NOT NULL,
    [mEndDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [UCL_PK_TblPcSkillPackInventory_mPcNo_mSPID] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mSPID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 胶懦 蒲 牢亥配府', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillPackInventory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillPackInventory', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡 PC 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillPackInventory', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillPackInventory', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦蒲ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillPackInventory', @level2type = N'COLUMN', @level2name = N'mSPID';


GO

