CREATE TABLE [dbo].[T_TblPc] (
    [mTransferSvrNo] SMALLINT  NOT NULL,
    [mSvrNo]         SMALLINT  NOT NULL,
    [mIsOverlap]     BIT       NOT NULL,
    [mOLDPcNo]       INT       NOT NULL,
    [mOLDPcNm]       CHAR (12) NOT NULL,
    [mTrasnsferLv]   TINYINT   DEFAULT ((10)) NOT NULL,
    [mRegDate]       DATETIME  NOT NULL,
    [mOwner]         INT       NOT NULL,
    [mSlot]          TINYINT   NOT NULL,
    [mNo]            INT       NOT NULL,
    [mNm]            CHAR (12) NOT NULL,
    [mIsMove]        BIT       NOT NULL
);


GO

CREATE UNIQUE CLUSTERED INDEX [UCL_T_TblPc]
    ON [dbo].[T_TblPc]([mSvrNo] ASC, [mOLDPcNo] ASC);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_T_TblPc_mSvrNo]
    ON [dbo].[T_TblPc]([mTransferSvrNo] ASC, [mNo] ASC);


GO

CREATE NONCLUSTERED INDEX [UNC_T_TblPc_mOwner]
    ON [dbo].[T_TblPc]([mOwner] ASC);


GO

