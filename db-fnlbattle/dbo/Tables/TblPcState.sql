CREATE TABLE [dbo].[TblPcState] (
    [mNo]                INT       NOT NULL,
    [mLevel]             SMALLINT  NOT NULL,
    [mExp]               BIGINT    NOT NULL,
    [mHpAdd]             INT       NOT NULL,
    [mHp]                INT       NOT NULL,
    [mMpAdd]             INT       NOT NULL,
    [mMp]                INT       NOT NULL,
    [mMapNo]             INT       NOT NULL,
    [mPosX]              REAL      NOT NULL,
    [mPosY]              REAL      NOT NULL,
    [mPosZ]              REAL      NOT NULL,
    [mStomach]           SMALLINT  CONSTRAINT [DF_TblPcState_mStomach] DEFAULT ((100)) NOT NULL,
    [mIp]                CHAR (15) NULL,
    [mLoginTm]           DATETIME  NULL,
    [mLogoutTm]          DATETIME  NULL,
    [mTotUseTm]          INT       CONSTRAINT [DF_TblPcState_mTotUseTm] DEFAULT ((0)) NOT NULL,
    [mPkCnt]             INT       CONSTRAINT [DF_TblPcState_mPkCnt] DEFAULT ((0)) NOT NULL,
    [mChaotic]           INT       CONSTRAINT [DF_TblPcState_mChaotic] DEFAULT ((0)) NOT NULL,
    [mPartyMemCntLevel]  INT       DEFAULT ((0)) NOT NULL,
    [mDiscipleJoinCount] INT       CONSTRAINT [DF__TblPcStat__mDiscipleJoinCount] DEFAULT ((3)) NOT NULL,
    [mLostExp]           BIGINT    CONSTRAINT [DF_TblPcState_mLostExp] DEFAULT ((0)) NOT NULL,
    [mIsLetterLimit]     BIT       CONSTRAINT [DF_TblPcState_mIsLetterLimit] DEFAULT ((0)) NOT NULL,
    [mFlag]              TINYINT   CONSTRAINT [DF_TblPcState_mFlag] DEFAULT ((0)) NOT NULL,
    [mIsPreventItemDrop] BIT       CONSTRAINT [DF_TblPcState_mIsPreventItemDrop] DEFAULT ((0)) NOT NULL,
    [mSkillTreePoint]    SMALLINT  CONSTRAINT [DF_TblPcState_mSkillTreePoint] DEFAULT ((0)) NOT NULL,
    [mRestExpGuild]      BIGINT    CONSTRAINT [DF_TblPcState_mRestExp] DEFAULT ((0)) NOT NULL,
    [mRestExpActivate]   BIGINT    CONSTRAINT [DF_TblPcState_mRestExpActivate] DEFAULT ((0)) NOT NULL,
    [mRestExpDeactivate] BIGINT    CONSTRAINT [DF_TblPcState_mRestExpDeactivate] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblPcState] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblPcState_mLevel] CHECK ((0)<=[mLevel] AND [mLevel]<=(100)),
    CONSTRAINT [FkTblPcStateTblPc] FOREIGN KEY ([mNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO
ALTER TABLE [dbo].[TblPcState] NOCHECK CONSTRAINT [CK_TblPcState_mLevel];


GO
ALTER TABLE [dbo].[TblPcState] NOCHECK CONSTRAINT [FkTblPcStateTblPc];


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡丰 酒捞袍俊 狼茄 劝己拳 等 绒侥 版氰摹 儡咯樊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mRestExpActivate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付瘤阜 敲饭捞 矫累 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mLoginTm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'硅绊悄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mStomach';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'己氢 荐摹', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mChaotic';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'醚 play 矫埃(盒 窜困)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mTotUseTm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Y 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mPosY';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'甘 NO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mMapNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥辟 荤噶 版氰摹 扁废', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mLostExp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mHp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'檬焊级 捞呕 咯何 巩狼咯何 (犬厘啊瓷 0 : False, 1 : true) ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mFlag';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版氰摹', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mExp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦飘府 器牢飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mSkillTreePoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'厚 劝己拳 绒侥 版氰摹 儡咯樊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mRestExpDeactivate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付瘤阜 立辟 IP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mIp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付瘤阜 敲饭捞 辆丰 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mLogoutTm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'X 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mPosX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Z 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mPosZ';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mPkCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'颇萍 弥措 糕滚荐 饭骇(0 : 20疙, 1 : 30疙)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mPartyMemCntLevel';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mMp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康备 刘啊等 MP烹.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mMpAdd';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤力葛烙 巢篮 啊涝冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mDiscipleJoinCount';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'祈瘤瘤 荐脚 力茄 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mIsLetterLimit';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康备 刘啊等 Hp烹.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mHpAdd';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 靛而 规瘤 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mIsPreventItemDrop';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'饭骇', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mLevel';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛胶懦俊 狼茄 绒侥 版氰摹 儡咯樊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mRestExpGuild';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 惑怕', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState';


GO

