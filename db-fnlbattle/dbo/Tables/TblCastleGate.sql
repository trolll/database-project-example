CREATE TABLE [dbo].[TblCastleGate] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF__TblCastle__mRegD__338B682C] DEFAULT (getdate()) NOT NULL,
    [mNo]      BIGINT        NOT NULL,
    [mHp]      INT           NOT NULL,
    [mIsOpen]  BIT           CONSTRAINT [DF__TblCastle__mIsOp__3573B09E] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblCastleGate] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90),
    CHECK ((0)<=[mHp]),
    CHECK ((0)<=[mHp]),
    CHECK ((0)<=[mHp]),
    CHECK ((0)<=[mHp]),
    CHECK ((0)<=[mHp]),
    CHECK ((0)<=[mHp])
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'己巩锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleGate', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleGate', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'己巩 凯赴 惑怕(1:曼)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleGate', @level2type = N'COLUMN', @level2name = N'mIsOpen';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleGate', @level2type = N'COLUMN', @level2name = N'mHp';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'己巩', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleGate';


GO

