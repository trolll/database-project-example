CREATE TABLE [dbo].[TblPetitionCheckState] (
    [mPcNo]     INT    NOT NULL,
    [mCategory] INT    NOT NULL,
    [mPID]      BIGINT NOT NULL,
    CONSTRAINT [PK_TblPetitionPcState] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mCategory] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'柳沥辑 惑怕', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionCheckState';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'TblPetitionBoard.mPID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionCheckState', @level2type = N'COLUMN', @level2name = N'mPID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionCheckState', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'盒幅 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPetitionCheckState', @level2type = N'COLUMN', @level2name = N'mCategory';


GO

