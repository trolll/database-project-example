CREATE TABLE [dbo].[TblGuildAccount] (
    [mGID]        INT           NOT NULL,
    [mGuildMoney] BIGINT        CONSTRAINT [DF_TblGuildAccount_mMoney] DEFAULT ((0)) NOT NULL,
    [mRegDate]    SMALLDATETIME CONSTRAINT [DF_TblGuildAccount_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblGuildAccount] PRIMARY KEY CLUSTERED ([mGID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAccount', @level2type = N'COLUMN', @level2name = N'mGID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAccount', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 拌谅', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAccount';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 赣聪', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAccount', @level2type = N'COLUMN', @level2name = N'mGuildMoney';


GO

