CREATE TABLE [dbo].[TblNpcGlobalValue] (
    [mNId]   INT          NOT NULL,
    [mName]  VARCHAR (20) NOT NULL,
    [mValue] INT          NOT NULL,
    CONSTRAINT [UCL_PK_TblNpcGlobalValue] PRIMARY KEY NONCLUSTERED ([mName] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblNpcGlobalValue', @level2type = N'COLUMN', @level2name = N'mName';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblNpcGlobalValue', @level2type = N'COLUMN', @level2name = N'mValue';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'NPC 傈开沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblNpcGlobalValue';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡聪农茄 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblNpcGlobalValue', @level2type = N'COLUMN', @level2name = N'mNId';


GO

