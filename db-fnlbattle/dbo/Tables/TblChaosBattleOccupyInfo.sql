CREATE TABLE [dbo].[TblChaosBattleOccupyInfo] (
    [mTerritory]        INT           NOT NULL,
    [mOccupySvrNo]      SMALLINT      NOT NULL,
    [mOccupyDate]       SMALLDATETIME NOT NULL,
    [mChaosBattleSvrNo] SMALLINT      NOT NULL,
    CONSTRAINT [UCL_TblChaosBattleOccupyInfo] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mChaosBattleSvrNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康瘤锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattleOccupyInfo', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattleOccupyInfo', @level2type = N'COLUMN', @level2name = N'mOccupySvrNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattleOccupyInfo', @level2type = N'COLUMN', @level2name = N'mOccupyDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'墨坷胶 硅撇 辑滚狼 痢飞 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattleOccupyInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'墨坷胶 硅撇 辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattleOccupyInfo', @level2type = N'COLUMN', @level2name = N'mChaosBattleSvrNo';


GO

