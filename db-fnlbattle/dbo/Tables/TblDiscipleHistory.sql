CREATE TABLE [dbo].[TblDiscipleHistory] (
    [mMaster]     INT          NOT NULL,
    [mNo]         INT          NOT NULL,
    [mDisciple]   INT          NOT NULL,
    [mDiscipleNm] VARCHAR (50) NOT NULL,
    [mRegDate]    DATETIME     CONSTRAINT [DF_TblDiscipleHistory_mRegDate] DEFAULT (getdate()) NOT NULL
);


GO

CREATE CLUSTERED INDEX [CL_TblDiscipleHistory]
    ON [dbo].[TblDiscipleHistory]([mMaster] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleHistory', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤力 某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleHistory', @level2type = N'COLUMN', @level2name = N'mDisciple';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'荤力 捞仿', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleHistory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤力 某腐磐 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleHistory', @level2type = N'COLUMN', @level2name = N'mDiscipleNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'捞仿 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleHistory', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付胶磐 某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDiscipleHistory', @level2type = N'COLUMN', @level2name = N'mMaster';


GO

