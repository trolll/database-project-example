CREATE TABLE [dbo].[TblDialog] (
    [mRegDate]  SMALLDATETIME  CONSTRAINT [DF_TblDialog_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mMId]      INT            NOT NULL,
    [mClick]    VARCHAR (6000) NOT NULL,
    [mDie]      VARCHAR (100)  NOT NULL,
    [mAttacked] VARCHAR (100)  NOT NULL,
    [mTarget]   VARCHAR (100)  NOT NULL,
    [mBear]     VARCHAR (100)  NOT NULL,
    [mGossip1]  VARCHAR (100)  NOT NULL,
    [mGossip2]  VARCHAR (100)  NOT NULL,
    [mGossip3]  VARCHAR (100)  NOT NULL,
    [mGossip4]  VARCHAR (100)  NOT NULL,
    CONSTRAINT [PK_TblDialog] PRIMARY KEY CLUSTERED ([mMId] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥檬肺 傍拜阑 寸且锭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mAttacked';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'怕绢朝锭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mBear';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啊奖 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mGossip2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'努腐沁阑 锭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mClick';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'磷菌阑 锭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mDie';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利阑 惯斑窍砍阑 锭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mTarget';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啊奖 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mGossip1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啊奖 3', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mGossip3';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啊奖 4', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mGossip4';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'措拳', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialog', @level2type = N'COLUMN', @level2name = N'mMId';


GO

