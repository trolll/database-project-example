CREATE TABLE [dbo].[TblBanquetHall] (
    [mTerritory]       INT           NOT NULL,
    [mBanquetHallType] INT           NOT NULL,
    [mBanquetHallNo]   INT           NOT NULL,
    [mOwnerPcNo]       INT           CONSTRAINT [DF_TblBanquetHall_mOwnerPcNo] DEFAULT ((0)) NOT NULL,
    [mRegDate]         SMALLDATETIME CONSTRAINT [DF_TblBanquetHall_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mLeftMin]         INT           CONSTRAINT [DF_TblBanquetHall_mLeftMin] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblBanquetHall] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mBanquetHallType] ASC, [mBanquetHallNo] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [IX_TblBanquetHall]
    ON [dbo].[TblBanquetHall]([mOwnerPcNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'儡咯 矫埃(盒)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHall', @level2type = N'COLUMN', @level2name = N'mLeftMin';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHall', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康配锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHall', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'楷雀厘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHall';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHall', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'楷雀厘 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHall', @level2type = N'COLUMN', @level2name = N'mBanquetHallNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'楷雀厘 鸥涝 ( 0: 家 楷雀厘 / 1 : 措 楷雀厘 )', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBanquetHall', @level2type = N'COLUMN', @level2name = N'mBanquetHallType';


GO

