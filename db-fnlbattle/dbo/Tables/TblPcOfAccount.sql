CREATE TABLE [dbo].[TblPcOfAccount] (
    [mRegDate]            SMALLDATETIME CONSTRAINT [DF_TblPcOfAccount_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mOwner]              INT           NOT NULL,
    [mDelDateOfPcDeleted] SMALLDATETIME CONSTRAINT [DF_TblPcOfAccount_mDelDateOfPcDeleted] DEFAULT (getdate()) NOT NULL,
    [mDelCntOfPc]         INT           CONSTRAINT [DF_TblPcOfAccount_mDelCntOfPc] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [CL_PKTblPcOfAccount] PRIMARY KEY CLUSTERED ([mOwner] ASC) WITH (FILLFACTOR = 80)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊(拌沥 锅龋)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcOfAccount', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'昏力 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcOfAccount', @level2type = N'COLUMN', @level2name = N'mDelCntOfPc';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcOfAccount', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付瘤阜 某腐磐 昏力 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcOfAccount', @level2type = N'COLUMN', @level2name = N'mDelDateOfPcDeleted';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 昏力 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcOfAccount';


GO

