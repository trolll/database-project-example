CREATE TABLE [dbo].[T_TblPcInventory] (
    [OrgmSerialNo]         BIGINT        NOT NULL,
    [mRegDate]             DATETIME      NOT NULL,
    [mSerialNo]            BIGINT        IDENTITY (2, 1) NOT NULL,
    [mPcNo]                INT           NOT NULL,
    [mItemNo]              INT           NOT NULL,
    [mEndDate]             SMALLDATETIME NOT NULL,
    [mIsConfirm]           BIT           NOT NULL,
    [mStatus]              TINYINT       NOT NULL,
    [mCnt]                 INT           NOT NULL,
    [mCntUse]              TINYINT       NOT NULL,
    [mIsSeizure]           BIT           NOT NULL,
    [mApplyAbnItemNo]      INT           NOT NULL,
    [mApplyAbnItemEndDate] SMALLDATETIME NOT NULL,
    [mOwner]               INT           NOT NULL,
    [mPracticalPeriod]     INT           NOT NULL,
    [mBindingType]         TINYINT       NOT NULL,
    [mRestoreCnt]          TINYINT       NOT NULL
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_T_TblPcInventory_mSerialNo]
    ON [dbo].[T_TblPcInventory]([mSerialNo] ASC);


GO

