CREATE TABLE [dbo].[TblPc] (
    [mRegDate]   DATETIME  CONSTRAINT [DF__TblPc__mRegDate__1E05700A] DEFAULT (getdate()) NOT NULL,
    [mOwner]     INT       NOT NULL,
    [mSlot]      TINYINT   NOT NULL,
    [mNo]        INT       IDENTITY (1, 1) NOT NULL,
    [mNm]        CHAR (12) NOT NULL,
    [mClass]     TINYINT   NOT NULL,
    [mSex]       TINYINT   CONSTRAINT [DF_TblPc_mSex] DEFAULT ((0)) NOT NULL,
    [mHead]      TINYINT   CONSTRAINT [DF_TblPc_mHead] DEFAULT ((0)) NOT NULL,
    [mFace]      TINYINT   CONSTRAINT [DF_TblPc_mFace] DEFAULT ((0)) NOT NULL,
    [mBody]      TINYINT   CONSTRAINT [DF_TblPc_mBody] DEFAULT ((0)) NOT NULL,
    [mHomeMapNo] INT       NOT NULL,
    [mHomePosX]  REAL      NOT NULL,
    [mHomePosY]  REAL      NOT NULL,
    [mHomePosZ]  REAL      NOT NULL,
    [mDelDate]   DATETIME  NULL,
    CONSTRAINT [PkTblPc] PRIMARY KEY NONCLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK__TblPc__mClass] CHECK ((0)<=[mClass] AND [mClass]<=(4)),
    CONSTRAINT [CK__TblPc__mNm__1FEDB87C] CHECK ((1)<=datalength(rtrim([mNm]))),
    CONSTRAINT [CK__TblPc__mNo__1EF99443] CHECK ((0)<=[mNo])
);


GO
ALTER TABLE [dbo].[TblPc] NOCHECK CONSTRAINT [CK__TblPc__mClass];


GO
ALTER TABLE [dbo].[TblPc] NOCHECK CONSTRAINT [CK__TblPc__mNm__1FEDB87C];


GO
ALTER TABLE [dbo].[TblPc] NOCHECK CONSTRAINT [CK__TblPc__mNo__1EF99443];


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblPcNm]
    ON [dbo].[TblPc]([mNm] ASC) WITH (FILLFACTOR = 90);


GO

CREATE CLUSTERED INDEX [IxTblPcOwner]
    ON [dbo].[TblPc]([mOwner] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'权 X 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mHomePosX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 拌沥 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'权 Y 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mHomePosY';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'浇吩锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mSlot';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'权 Z 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mHomePosZ';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'官叼 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mBody';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'甘 NO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mHomeMapNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'赣府 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mHead';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'倔奔 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mFace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'昏力老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mDelDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'努贰胶 ( 0 : 唱捞飘, 1 : 饭牢历, 2 : 郡橇, 3 : 绢泍脚, 4 : 辑葛呈 )', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mClass';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'己喊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPc', @level2type = N'COLUMN', @level2name = N'mSex';


GO

