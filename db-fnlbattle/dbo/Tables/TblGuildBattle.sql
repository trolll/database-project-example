CREATE TABLE [dbo].[TblGuildBattle] (
    [mRegDate]  DATETIME CONSTRAINT [DF__TblGuildB__mRegD__0C06BB60] DEFAULT (getdate()) NOT NULL,
    [mGuildNo1] INT      NOT NULL,
    [mGuildNo2] INT      NOT NULL,
    CONSTRAINT [PkTblGuildBattle] PRIMARY KEY CLUSTERED ([mGuildNo1] ASC, [mGuildNo2] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblGuildBattle_mGuildNo2]
    ON [dbo].[TblGuildBattle]([mGuildNo2] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattle', @level2type = N'COLUMN', @level2name = N'mGuildNo1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傈捧矫累 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattle', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 傈捧', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattle';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattle', @level2type = N'COLUMN', @level2name = N'mGuildNo2';


GO

