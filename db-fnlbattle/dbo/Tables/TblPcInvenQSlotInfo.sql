CREATE TABLE [dbo].[TblPcInvenQSlotInfo] (
    [mUpdateDate] DATETIME      CONSTRAINT [DF_TblPcInvenQSlotInfo_mUpdateDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]       INT           NOT NULL,
    [mInfo]       BINARY (8000) NULL,
    CONSTRAINT [UCL_PK_TblPcInvenQSlotInfo] PRIMARY KEY CLUSTERED ([mPcNo] ASC),
    CONSTRAINT [FK_TblChaosBattlePc_TblPcInvenQSlotInfo] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblChaosBattlePc] ([mNo])
);


GO
ALTER TABLE [dbo].[TblPcInvenQSlotInfo] NOCHECK CONSTRAINT [FK_TblChaosBattlePc_TblPcInvenQSlotInfo];


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 皋葛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcInvenQSlotInfo', @level2type = N'COLUMN', @level2name = N'mInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcInvenQSlotInfo', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcInvenQSlotInfo', @level2type = N'COLUMN', @level2name = N'mUpdateDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 牢亥浇吩 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcInvenQSlotInfo';


GO

