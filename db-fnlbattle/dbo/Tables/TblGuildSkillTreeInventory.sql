CREATE TABLE [dbo].[TblGuildSkillTreeInventory] (
    [mRegDate]     DATETIME      CONSTRAINT [DF_TblGuildSkillTreeInventory_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]     INT           NOT NULL,
    [mSTNIID]      INT           NOT NULL,
    [mEndDate]     SMALLDATETIME NOT NULL,
    [mCreatorPcNo] INT           NOT NULL,
    [mExp]         BIGINT        NOT NULL,
    CONSTRAINT [UCL_PK_TblGuildSkillTreeInventory_mGuildNo_mSTNIID] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mSTNIID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'父电PC锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mCreatorPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦飘府畴靛酒捞袍ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mSTNIID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 胶懦飘府 牢亥配府', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildSkillTreeInventory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'穿利 版氰摹', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mExp';


GO

