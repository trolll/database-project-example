CREATE TABLE [dbo].[TblUnitedGuildWarRewardInfo] (
    [mRewardDate]       SMALLDATETIME NOT NULL,
    [mRound]            INT           NOT NULL,
    [mRanking]          TINYINT       NOT NULL,
    [mRewardSvrNo]      SMALLINT      NOT NULL,
    [mAdvantage]        INT           NOT NULL,
    [mIsTournament]     BIT           NOT NULL,
    [mChaosBattleSvrNo] SMALLINT      NOT NULL,
    [mIsValid]          BIT           CONSTRAINT [DF_TblUnitedGuildWarRewardInfo_mIsValid] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [CL_PK_TblUnitedGuildWarRewardInfo] UNIQUE CLUSTERED ([mChaosBattleSvrNo] ASC, [mRanking] ASC, [mRound] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'焊惑辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarRewardInfo', @level2type = N'COLUMN', @level2name = N'mRewardSvrNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'驱琶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarRewardInfo', @level2type = N'COLUMN', @level2name = N'mAdvantage';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'扼款靛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarRewardInfo', @level2type = N'COLUMN', @level2name = N'mRound';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'鉴困', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarRewardInfo', @level2type = N'COLUMN', @level2name = N'mRanking';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'焊惑老磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarRewardInfo', @level2type = N'COLUMN', @level2name = N'mRewardDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡瓤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarRewardInfo', @level2type = N'COLUMN', @level2name = N'mIsValid';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1 :配呈刚飘 0 : 老馆', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarRewardInfo', @level2type = N'COLUMN', @level2name = N'mIsTournament';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'墨坷胶硅撇辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarRewardInfo', @level2type = N'COLUMN', @level2name = N'mChaosBattleSvrNo';


GO

