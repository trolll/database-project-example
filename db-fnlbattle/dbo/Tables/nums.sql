CREATE TABLE [dbo].[nums] (
    [n] INT IDENTITY (1, 1) NOT NULL,
    PRIMARY KEY CLUSTERED ([n] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'nums', @level2type = N'COLUMN', @level2name = N'n';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'Split 窃荐甫 困茄 鉴锅 抛捞喉', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'nums';


GO

