CREATE TABLE [dbo].[TblGuildRecruit] (
    [mRegDate]    DATETIME      CONSTRAINT [DF_TblGuildRecruit_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mEndDate]    DATETIME      NOT NULL,
    [mGuildNo]    INT           NOT NULL,
    [mIsPremium]  BIT           NOT NULL,
    [mIsMarkShow] BIT           NOT NULL,
    [mGuildMsg]   VARCHAR (500) CONSTRAINT [DF_TblGuildRecruit_mGuildMsg] DEFAULT (' ') NULL,
    CONSTRAINT [UNC_TblGuildRecruit_mGuildNo] PRIMARY KEY NONCLUSTERED ([mGuildNo] ASC)
);


GO

CREATE NONCLUSTERED INDEX [NL_TblGuildRecruit_mGuildNm]
    ON [dbo].[TblGuildRecruit]([mEndDate] DESC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'葛笼辆丰朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruit', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛葛笼矫胶袍狼 葛笼吝牢 辨靛 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruit';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 葛笼 傍绊 皋技瘤 (弥措 茄臂250磊)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruit', @level2type = N'COLUMN', @level2name = N'mGuildMsg';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruit', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 付农 钎矫 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruit', @level2type = N'COLUMN', @level2name = N'mIsMarkShow';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruit', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'橇府固决 葛笼 殿废 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruit', @level2type = N'COLUMN', @level2name = N'mIsPremium';


GO

