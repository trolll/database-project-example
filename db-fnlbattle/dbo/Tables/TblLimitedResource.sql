CREATE TABLE [dbo].[TblLimitedResource] (
    [mRegDate]      SMALLDATETIME CONSTRAINT [DF_TblLimitedResource_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mResourceType] INT           NOT NULL,
    [mMaxCnt]       INT           NOT NULL,
    [mRemainerCnt]  INT           NOT NULL,
    [mRandomVal]    FLOAT (53)    NOT NULL,
    [mUptDate]      SMALLDATETIME CONSTRAINT [DF_TblLimitedResource_mUptDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblLimitedResource_mResourceType] PRIMARY KEY CLUSTERED ([mResourceType] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'力茄等 府家胶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResource';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResource', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'府家胶 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResource', @level2type = N'COLUMN', @level2name = N'mResourceType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'力茄 俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResource', @level2type = N'COLUMN', @level2name = N'mMaxCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'巢篮 俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResource', @level2type = N'COLUMN', @level2name = N'mRemainerCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'犬伏蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResource', @level2type = N'COLUMN', @level2name = N'mRandomVal';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥辆 盎脚老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResource', @level2type = N'COLUMN', @level2name = N'mUptDate';


GO

