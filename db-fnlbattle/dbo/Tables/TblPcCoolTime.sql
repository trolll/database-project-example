CREATE TABLE [dbo].[TblPcCoolTime] (
    [mPcNo]          INT      NOT NULL,
    [mSID]           INT      NOT NULL,
    [mCoolTimeGroup] SMALLINT NOT NULL,
    [mRemainTime]    INT      NOT NULL,
    CONSTRAINT [UCL_TblPcCoolTime] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mCoolTimeGroup] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'酿鸥烙 瘤加 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcCoolTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PC 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcCoolTime', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酿鸥烙 弊缝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcCoolTime', @level2type = N'COLUMN', @level2name = N'mCoolTimeGroup';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酿 鸥烙 儡咯 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcCoolTime', @level2type = N'COLUMN', @level2name = N'mRemainTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcCoolTime', @level2type = N'COLUMN', @level2name = N'mSID';


GO

