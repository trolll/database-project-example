CREATE TABLE [dbo].[TblUnitedGuildWarTournamentInfo] (
    [mRegDate]      SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [mRound]        INT           NOT NULL,
    [mSvrNo]        SMALLINT      NOT NULL,
    [mGuildNo]      INT           NOT NULL,
    [mGuildNm]      CHAR (12)     NOT NULL,
    [mRanking]      TINYINT       NOT NULL,
    [mMatchSchTime] SMALLDATETIME NOT NULL,
    [mMatchTime]    SMALLDATETIME DEFAULT (NULL) NULL,
    [mState]        TINYINT       NOT NULL,
    [mGroup]        TINYINT       NOT NULL,
    [mGroupIndex]   TINYINT       NOT NULL,
    [mDepth]        TINYINT       NOT NULL,
    CONSTRAINT [NCL_TblUnitedGuildWarTournamentInfo] PRIMARY KEY NONCLUSTERED ([mRound] ASC, [mSvrNo] ASC, [mGuildNo] ASC, [mDepth] ASC)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblUnitedGuildWarTournamentInfo_mRound]
    ON [dbo].[TblUnitedGuildWarTournamentInfo]([mRound] DESC)
    INCLUDE([mDepth]);


GO

CREATE CLUSTERED INDEX [CL_TblUnitedGuildWarTournamentInfo_mRound]
    ON [dbo].[TblUnitedGuildWarTournamentInfo]([mRound] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Depth', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mDepth';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版扁 胶纳领 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mMatchSchTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瞒荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mRound';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版扁 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mMatchTime';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'配呈刚飘 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GroupIndex', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mGroupIndex';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'珐欧', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mRanking';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mGroup';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛疙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mGuildNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mSvrNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'State', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mState';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentInfo', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

