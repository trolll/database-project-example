CREATE TABLE [dbo].[TblGuildGoldItemEffect] (
    [mRegDate]  DATETIME      DEFAULT (getdate()) NOT NULL,
    [mGuildNo]  INT           NOT NULL,
    [mItemType] INT           NOT NULL,
    [mParmA]    FLOAT (53)    DEFAULT ((0)) NOT NULL,
    [mEndDate]  SMALLDATETIME NOT NULL,
    [mItemNo]   INT           CONSTRAINT [DF_TblGuildGoldItemEffect_mItemNo] DEFAULT ((409)) NOT NULL,
    CONSTRAINT [CL_PK_TblGuildGoldItemEffect] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mItemType] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 蜡丰酒捞袍', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildGoldItemEffect';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰朝磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍狼 何啊利牢 加己蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mParmA';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡 辨靛 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 蜡丰 酒捞袍 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mItemType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己 朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡丰 酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

