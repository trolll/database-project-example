CREATE TABLE [dbo].[TblQuickSupplicationComment] (
    [mRegDate] DATETIME      DEFAULT (getdate()) NOT NULL,
    [mQSID]    BIGINT        NOT NULL,
    [mComment] VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_CL_TblQuickSupplicationComment] PRIMARY KEY CLUSTERED ([mQSID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationComment', @level2type = N'COLUMN', @level2name = N'mQSID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 皋葛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationComment', @level2type = N'COLUMN', @level2name = N'mComment';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'脚绊窃 皋葛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationComment';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationComment', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

