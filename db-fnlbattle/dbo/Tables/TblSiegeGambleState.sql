CREATE TABLE [dbo].[TblSiegeGambleState] (
    [mRegDate]      DATETIME CONSTRAINT [DF__TblSiegeG__mRegD__61722E6D] DEFAULT (getdate()) NOT NULL,
    [mTerritory]    INT      NOT NULL,
    [mNo]           INT      NOT NULL,
    [mIsFinish]     BIT      NOT NULL,
    [mTotalMoney]   INT      NOT NULL,
    [mDividend]     INT      NOT NULL,
    [mOccupyGuild0] INT      CONSTRAINT [DF__TblSiegeG__mOccu__6542BF51] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild1] INT      CONSTRAINT [DF__TblSiegeG__mOccu__6636E38A] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild2] INT      CONSTRAINT [DF__TblSiegeG__mOccu__672B07C3] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild3] INT      CONSTRAINT [DF__TblSiegeG__mOccu__681F2BFC] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild4] INT      CONSTRAINT [DF__TblSiegeG__mOccu__69135035] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild5] INT      CONSTRAINT [DF__TblSiegeG__mOccu__6A07746E] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild6] INT      CONSTRAINT [DF__TblSiegeG__mOccu__6AFB98A7] DEFAULT ((0)) NOT NULL,
    [mIsKeep]       INT      CONSTRAINT [DF__TblSiegeG__mIsKe__6BEFBCE0] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblSiegeGambleState] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mNo] ASC) WITH (FILLFACTOR = 90),
    CHECK ((0)<[mTerritory] AND [mTerritory]<(5)),
    CHECK ((0)<[mTerritory] AND [mTerritory]<(5)),
    CHECK ((0)<[mTerritory] AND [mTerritory]<(5)),
    CHECK ((0)<[mTerritory] AND [mTerritory]<(5)),
    CHECK ((0)<[mTerritory] AND [mTerritory]<(5)),
    CHECK ((0)<[mTerritory] AND [mTerritory]<(5)),
    CHECK ((0)<=[mDividend]),
    CHECK ((0)<=[mDividend]),
    CHECK ((0)<=[mDividend]),
    CHECK ((0)<=[mDividend]),
    CHECK ((0)<=[mDividend]),
    CHECK ((0)<=[mDividend]),
    CHECK ((0)<=[mTotalMoney]),
    CHECK ((0)<=[mTotalMoney]),
    CHECK ((0)<=[mTotalMoney]),
    CHECK ((0)<=[mTotalMoney]),
    CHECK ((0)<=[mTotalMoney]),
    CHECK ((0)<=[mTotalMoney])
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mIsFinish';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 辨靛 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mOccupyGuild5';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 辨靛 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mOccupyGuild4';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SiegeGamble 雀瞒', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'硅寸陛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mDividend';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'bit窜困肺 荐己沁栏搁 曼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mIsKeep';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 辨靛 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mOccupyGuild6';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'醚 陛咀', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mTotalMoney';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傍己 矫累 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 辨靛 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mOccupyGuild1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 辨靛 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mOccupyGuild0';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'傍己铰何荤 泅炔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 辨靛 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mOccupyGuild3';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康瘤 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞 辨靛 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleState', @level2type = N'COLUMN', @level2name = N'mOccupyGuild2';


GO

