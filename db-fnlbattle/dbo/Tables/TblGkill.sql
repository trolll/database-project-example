CREATE TABLE [dbo].[TblGkill] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF__TblGkill__mRegDa__438CC5CB] DEFAULT (getdate()) NOT NULL,
    [mGuildNo] INT           NOT NULL,
    [mPlace]   INT           NOT NULL,
    [mNode]    INT           NOT NULL,
    [mPcNo]    INT           NOT NULL,
    CONSTRAINT [PK_TblGkill] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mPlace] ASC, [mNode] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FkTblGkillTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo])
);


GO
ALTER TABLE [dbo].[TblGkill] NOCHECK CONSTRAINT [FkTblGkillTblGuild];


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblGkillPcNo]
    ON [dbo].[TblGkill]([mPcNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'畴靛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGkill', @level2type = N'COLUMN', @level2name = N'mNode';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGkill', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瘤开 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGkill', @level2type = N'COLUMN', @level2name = N'mPlace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'build啊 矫累等 老矫', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGkill', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 胶懦', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGkill';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'build甫 夸没茄 PC锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGkill', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

