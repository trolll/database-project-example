CREATE TABLE [dbo].[TblPcRankingPoint] (
    [mRegDate]       DATETIME DEFAULT (getdate()) NOT NULL,
    [mPcNo]          INT      NOT NULL,
    [mContribution]  INT      DEFAULT ((0)) NOT NULL,
    [mGuardian]      INT      DEFAULT ((0)) NOT NULL,
    [mTeleportTower] INT      DEFAULT ((0)) NOT NULL,
    [mPVP]           INT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblPcRankingPoint] PRIMARY KEY CLUSTERED ([mPcNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcRankingPoint', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 珐欧 器牢飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcRankingPoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荐龋脚 痢飞', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcRankingPoint', @level2type = N'COLUMN', @level2name = N'mGuardian';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcRankingPoint', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PVP', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcRankingPoint', @level2type = N'COLUMN', @level2name = N'mPVP';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'捞悼器呕 痢飞', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcRankingPoint', @level2type = N'COLUMN', @level2name = N'mTeleportTower';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傈厘 扁咯档', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcRankingPoint', @level2type = N'COLUMN', @level2name = N'mContribution';


GO

