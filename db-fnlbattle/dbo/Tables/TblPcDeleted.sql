CREATE TABLE [dbo].[TblPcDeleted] (
    [mRegDate] DATETIME  CONSTRAINT [DF__TblPcDele__mRegD__269AB60B] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT       NOT NULL,
    [mPcNm]    CHAR (12) NULL,
    CONSTRAINT [PkTblPcDeleted] PRIMARY KEY CLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FkTblPcDeletedTblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO
ALTER TABLE [dbo].[TblPcDeleted] NOCHECK CONSTRAINT [FkTblPcDeletedTblPc];


GO

CREATE NONCLUSTERED INDEX [NC_TblPcDeleted_mPcNm]
    ON [dbo].[TblPcDeleted]([mPcNm] DESC) WITH (FILLFACTOR = 80);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'昏力老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcDeleted', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcDeleted', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 昏力', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcDeleted';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 捞抚(昏力傈)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcDeleted', @level2type = N'COLUMN', @level2name = N'mPcNm';


GO

