CREATE TABLE [dbo].[TblGuildRecruitMemberList] (
    [mRegDate] DATETIME      CONSTRAINT [DF_TblGuildRecruitMemberList_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT           NOT NULL,
    [mGuildNo] INT           NOT NULL,
    [mState]   TINYINT       NOT NULL,
    [mPcMsg]   VARCHAR (500) CONSTRAINT [DF_TblGuildRecruitMemberList_mPcMsgz] DEFAULT (' ') NULL,
    [mIsHide]  BIT           CONSTRAINT [DF_TblGuildRecruitMemberList_mIsRefusal] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UNC_TblGuildRecruitMemberList_mPcNo] PRIMARY KEY NONCLUSTERED ([mPcNo] ASC),
    CONSTRAINT [CK_TblGuildRecruitMemberList_mState] CHECK ((0)<=[mState] AND [mState]<=(5))
);


GO
ALTER TABLE [dbo].[TblGuildRecruitMemberList] NOCHECK CONSTRAINT [CK_TblGuildRecruitMemberList_mState];


GO

CREATE CLUSTERED INDEX [CL_TblGuildRecruitMemberList_mGuildNo]
    ON [dbo].[TblGuildRecruitMemberList]([mGuildNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PC锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitMemberList', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚没茄 辨靛锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitMemberList', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitMemberList', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚没磊 府胶飘 免仿 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitMemberList', @level2type = N'COLUMN', @level2name = N'mIsHide';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 葛笼阑 盔窍绰 某腐磐甸狼 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitMemberList';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'泅犁柳青惑怕', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitMemberList', @level2type = N'COLUMN', @level2name = N'mState';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚没磊狼 皋技瘤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitMemberList', @level2type = N'COLUMN', @level2name = N'mPcMsg';


GO

