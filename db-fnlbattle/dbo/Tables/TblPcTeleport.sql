CREATE TABLE [dbo].[TblPcTeleport] (
    [mNo]    INT       IDENTITY (1, 1) NOT NULL,
    [mPcNo]  INT       NOT NULL,
    [mName]  CHAR (20) NOT NULL,
    [mMapNo] INT       NOT NULL,
    [mPosX]  REAL      NOT NULL,
    [mPosY]  REAL      NOT NULL,
    [mPosZ]  REAL      NOT NULL,
    CONSTRAINT [PkTblPcTeleport] PRIMARY KEY NONCLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FkTblPcTeleportTblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO
ALTER TABLE [dbo].[TblPcTeleport] NOCHECK CONSTRAINT [FkTblPcTeleportTblPc];


GO

CREATE CLUSTERED INDEX [IxTblTblPcTeleportPcNo]
    ON [dbo].[TblPcTeleport]([mPcNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 炮饭器飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcTeleport';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'X 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcTeleport', @level2type = N'COLUMN', @level2name = N'mPosX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcTeleport', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'炮饭器飘 厘家 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcTeleport', @level2type = N'COLUMN', @level2name = N'mName';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Teleport ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcTeleport', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Y 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcTeleport', @level2type = N'COLUMN', @level2name = N'mPosY';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Z 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcTeleport', @level2type = N'COLUMN', @level2name = N'mPosZ';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'甘 NO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcTeleport', @level2type = N'COLUMN', @level2name = N'mMapNo';


GO

