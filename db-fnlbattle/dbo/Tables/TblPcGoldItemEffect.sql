CREATE TABLE [dbo].[TblPcGoldItemEffect] (
    [mRegDate]  DATETIME      DEFAULT (getdate()) NOT NULL,
    [mPcNo]     INT           NOT NULL,
    [mItemType] INT           NOT NULL,
    [mParmA]    FLOAT (53)    DEFAULT ((0)) NOT NULL,
    [mEndDate]  SMALLDATETIME NOT NULL,
    [mItemNo]   INT           CONSTRAINT [DF_TblPcGoldItemEffect_mItemNo] DEFAULT ((409)) NOT NULL,
    CONSTRAINT [CL_PK_TblPcGoldItemEffect] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mItemType] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡丰 酒捞袍 荤侩 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰 朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡丰 酒捞袍狼 何啊利牢 加己蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mParmA';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 蜡丰酒捞袍 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcGoldItemEffect';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡丰 酒捞袍 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcGoldItemEffect', @level2type = N'COLUMN', @level2name = N'mItemType';


GO

