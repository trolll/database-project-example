CREATE TABLE [dbo].[TblSiegeGambleTicket] (
    [mRegDate]   DATETIME CONSTRAINT [DF__TblSiegeG__mRegD__6ECC298B] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]  BIGINT   NOT NULL,
    [mTerritory] INT      NOT NULL,
    [mNo]        INT      NOT NULL,
    [mIsKeep]    INT      CONSTRAINT [DF__TblSiegeG__mIsKe__6FC04DC4] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblSiegeGambleTicket] PRIMARY KEY NONCLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE CLUSTERED INDEX [IX_TblSiegeGambleTicket]
    ON [dbo].[TblSiegeGambleTicket]([mTerritory] ASC, [mNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'bit窜困肺 荐己寝栏搁 曼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleTicket', @level2type = N'COLUMN', @level2name = N'mIsKeep';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傍己 矫累 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleTicket', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'傍己铰何荤 萍南', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleTicket';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傍己铰何 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleTicket', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康瘤 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleTicket', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 矫府倔 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeGambleTicket', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

