CREATE TABLE [dbo].[TblPcPopupGuide] (
    [mPcNo]    INT NOT NULL,
    [mGuideNo] INT NOT NULL,
    [mReadCnt] INT CONSTRAINT [DF_TblPcPopupGuide_mReadCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblPcPopupGuide_UnitedKey] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mGuideNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'犬牢茄 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcPopupGuide', @level2type = N'COLUMN', @level2name = N'mReadCnt';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐狼 扑诀 啊捞靛 犬牢咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcPopupGuide';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'扑诀啊捞靛 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcPopupGuide', @level2type = N'COLUMN', @level2name = N'mGuideNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcPopupGuide', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

