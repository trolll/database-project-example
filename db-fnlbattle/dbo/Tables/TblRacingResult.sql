CREATE TABLE [dbo].[TblRacingResult] (
    [mPlace]     INT           NOT NULL,
    [mStage]     INT           NOT NULL,
    [mWinnerNID] INT           NOT NULL,
    [mDividend]  FLOAT (53)    NOT NULL,
    [mRegDate]   SMALLDATETIME CONSTRAINT [DF_TblRacingResult_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblRacingResult] PRIMARY KEY CLUSTERED ([mPlace] ASC, [mStage] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'版林扁废', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingResult';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'硅寸陛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingResult', @level2type = N'COLUMN', @level2name = N'mDividend';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瘤开(FNLParm.dbo.TblPlace)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingResult', @level2type = N'COLUMN', @level2name = N'mPlace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'搬苞 殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingResult', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stage ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingResult', @level2type = N'COLUMN', @level2name = N'mStage';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'铰磊 某腐磐 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingResult', @level2type = N'COLUMN', @level2name = N'mWinnerNID';


GO

