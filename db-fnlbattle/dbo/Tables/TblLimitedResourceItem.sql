CREATE TABLE [dbo].[TblLimitedResourceItem] (
    [mResourceType] INT NOT NULL,
    [mIID]          INT NOT NULL,
    [mIsComsume]    BIT CONSTRAINT [DF_TblLimitedResourceItem_mIsComsume] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblLimitedResourceItem] PRIMARY KEY CLUSTERED ([mResourceType] ASC, [mIID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'府家胶 力茄 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResourceItem', @level2type = N'COLUMN', @level2name = N'mResourceType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResourceItem', @level2type = N'COLUMN', @level2name = N'mIID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家戈 啊瓷茄瘤 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResourceItem', @level2type = N'COLUMN', @level2name = N'mIsComsume';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'府家胶 力茄 酒捞袍 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedResourceItem';


GO

