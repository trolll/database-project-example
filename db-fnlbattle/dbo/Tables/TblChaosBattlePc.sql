CREATE TABLE [dbo].[TblChaosBattlePc] (
    [mRegDate]      DATETIME      CONSTRAINT [DF_TblChaosBattlePc_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mOwner]        INT           NOT NULL,
    [mNo]           INT           IDENTITY (2, 1) NOT NULL,
    [mFieldSvrNo]   SMALLINT      NOT NULL,
    [mFieldSvrPcNo] INT           NOT NULL,
    [mNm]           CHAR (12)     NOT NULL,
    [mChgNmDate]    SMALLDATETIME NULL,
    [mLoginTm]      DATETIME      CONSTRAINT [DF_TblChaosBattlePc_mLoginTm] DEFAULT (getdate()) NOT NULL,
    [mLogoutTm]     DATETIME      NULL,
    [mTotUseTm]     INT           CONSTRAINT [DF_TblChaosBattlePc__mTotUseTm] DEFAULT ((0)) NOT NULL,
    [mDelDate]      SMALLDATETIME NULL,
    CONSTRAINT [NCL_TblChaosBattlePc] PRIMARY KEY NONCLUSTERED ([mNo] ASC),
    CONSTRAINT [CK_TblChaosBattlePc_mNm] CHECK ((1)<=datalength(rtrim([mNm]))),
    CONSTRAINT [CK_TblChaosBattlePc_mNo] CHECK ((0)<=[mNo])
);


GO
ALTER TABLE [dbo].[TblChaosBattlePc] NOCHECK CONSTRAINT [CK_TblChaosBattlePc_mNm];


GO
ALTER TABLE [dbo].[TblChaosBattlePc] NOCHECK CONSTRAINT [CK_TblChaosBattlePc_mNo];


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblChaosBattlePc_mFieldSvrNo_mFieldSvrPcNo]
    ON [dbo].[TblChaosBattlePc]([mFieldSvrNo] ASC, [mFieldSvrPcNo] ASC);


GO

CREATE CLUSTERED INDEX [CL_TblChaosBattlePc_mOwner]
    ON [dbo].[TblChaosBattlePc]([mOwner] ASC);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblChaosBattlePc_mFieldSvrNo_mNm]
    ON [dbo].[TblChaosBattlePc]([mFieldSvrNo] ASC, [mNm] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'肺弊酒眶矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mLogoutTm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'鞘靛辑滚 某腐磐锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mFieldSvrPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'墨坷胶硅撇 某腐磐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'拌沥锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'醚 敲饭捞矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mTotUseTm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐疙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐疙 函版朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mChgNmDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'肺弊牢矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mLoginTm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'鞘靛辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mFieldSvrNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'昏力老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblChaosBattlePc', @level2type = N'COLUMN', @level2name = N'mDelDate';


GO

