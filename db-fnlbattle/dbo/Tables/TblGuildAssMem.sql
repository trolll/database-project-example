CREATE TABLE [dbo].[TblGuildAssMem] (
    [mRegDate]    SMALLDATETIME CONSTRAINT [DF__TblGuildA__mRegD__58DCE86C] DEFAULT (getdate()) NOT NULL,
    [mGuildAssNo] INT           NOT NULL,
    [mGuildNo]    INT           NOT NULL,
    CONSTRAINT [PkTblGuildAssMem] PRIMARY KEY NONCLUSTERED ([mGuildNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FkTblGuildAssMemTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo]),
    CONSTRAINT [FkTblGuildAssMemTblGuildAss] FOREIGN KEY ([mGuildAssNo]) REFERENCES [dbo].[TblGuildAss] ([mGuildNo])
);


GO
ALTER TABLE [dbo].[TblGuildAssMem] NOCHECK CONSTRAINT [FkTblGuildAssMemTblGuild];


GO
ALTER TABLE [dbo].[TblGuildAssMem] NOCHECK CONSTRAINT [FkTblGuildAssMemTblGuildAss];


GO

CREATE CLUSTERED INDEX [IxTblGuildAssMemGuildAssNo]
    ON [dbo].[TblGuildAssMem]([mGuildAssNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 楷钦 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAssMem', @level2type = N'COLUMN', @level2name = N'mGuildAssNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAssMem', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 楷钦 糕滚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAssMem';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAssMem', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

