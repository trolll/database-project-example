CREATE TABLE [dbo].[TblDepository] (
    [mDepositoryNo]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [mRegNo]               BIGINT        NOT NULL,
    [mRegDate]             DATETIME      CONSTRAINT [DF__TblDeposi__mRegD__34FEAF52] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]            BIGINT        NOT NULL,
    [mNo]                  INT           NOT NULL,
    [mItemNo]              INT           NOT NULL,
    [mEndDate]             SMALLDATETIME NOT NULL,
    [mIsConfirm]           BIT           NOT NULL,
    [mStatus]              TINYINT       NOT NULL,
    [mCnt]                 INT           NOT NULL,
    [mCntUse]              TINYINT       NOT NULL,
    [mIsSeizure]           BIT           NOT NULL,
    [mApplyAbnItemNo]      INT           NOT NULL,
    [mApplyAbnItemEndDate] SMALLDATETIME NOT NULL,
    [mOwner]               INT           NOT NULL,
    [mPracticalPeriod]     INT           NOT NULL,
    CONSTRAINT [NC_PK_TblDepository_1] PRIMARY KEY NONCLUSTERED ([mDepositoryNo] ASC),
    CONSTRAINT [FK_TblDepository_TblDepositoryOwner_mRegNo] FOREIGN KEY ([mRegNo]) REFERENCES [dbo].[TblDepositoryOwner] ([mRegNo])
);


GO
ALTER TABLE [dbo].[TblDepository] NOCHECK CONSTRAINT [FK_TblDepository_TblDepositoryOwner_mRegNo];


GO

CREATE NONCLUSTERED INDEX [NC_TblDepository_2]
    ON [dbo].[TblDepository]([mItemNo] ASC);


GO

CREATE CLUSTERED INDEX [CL_TblDepository_mRegNo]
    ON [dbo].[TblDepository]([mRegNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利侩等 酒捞袍 辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mApplyAbnItemEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤侩 啊瓷茄 俺荐( 0 : 荤侩阂啊)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mCntUse';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瓤苞 瘤加矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mPracticalPeriod';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'拘幅咯何 ( 0 : 拘幅X , 1 : 拘幅O )', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mIsSeizure';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'矫府倔锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'焊包窃 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mDepositoryNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 惑怕(0:历林, 1:焊烹, 2:绵汗, 3:罚待)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mStatus';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡丰 酒捞袍 焊包窃 家蜡 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mRegNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'[檬扁拳] 焊包窃 - 胶乔靛 檬扁拳 矫, 蜡丰 酒捞袍 汗备甫 困茄 吝埃 抛捞喉', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'犬牢 咯何( 犬牢且 荐 乐绰 酒捞袍)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mIsConfirm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利侩等 酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepository', @level2type = N'COLUMN', @level2name = N'mApplyAbnItemNo';


GO

