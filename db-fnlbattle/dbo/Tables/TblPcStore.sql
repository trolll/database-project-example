CREATE TABLE [dbo].[TblPcStore] (
    [mRegDate]             DATETIME      DEFAULT (getdate()) NOT NULL,
    [mSerialNo]            BIGINT        NOT NULL,
    [mUserNo]              INT           NOT NULL,
    [mItemNo]              INT           NOT NULL,
    [mEndDate]             SMALLDATETIME NOT NULL,
    [mIsConfirm]           BIT           NOT NULL,
    [mStatus]              TINYINT       NOT NULL,
    [mCnt]                 INT           NOT NULL,
    [mCntUse]              TINYINT       DEFAULT ((0)) NOT NULL,
    [mIsSeizure]           BIT           DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemNo]      INT           DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemEndDate] SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [mOwner]               INT           DEFAULT ((0)) NOT NULL,
    [mPracticalPeriod]     INT           CONSTRAINT [DF_TblPcStore_mPracticalPeriod] DEFAULT ((0)) NOT NULL,
    [mBindingType]         TINYINT       CONSTRAINT [DF_TblPcStore_mBindingType] DEFAULT ((0)) NOT NULL,
    [mRestoreCnt]          TINYINT       CONSTRAINT [DF_TblPcStore_mRestoreCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_NC_TblPcStore_1] PRIMARY KEY NONCLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 90),
    CHECK ((0)<[mCnt]),
    CHECK ((0)<[mCnt]),
    CHECK ((0)<[mCnt]),
    CHECK ((0)<[mCnt]),
    CHECK ((0)<[mCnt]),
    CHECK ((0)<[mCnt]),
    CONSTRAINT [CK_TblPcStore_mBindingType] CHECK ([mBindingType]=(2) OR ([mBindingType]=(1) OR [mBindingType]=(0))),
    CONSTRAINT [CK_TblPcStore_mUserNo] CHECK ([mUserNo]>(1))
);


GO
ALTER TABLE [dbo].[TblPcStore] NOCHECK CONSTRAINT [CK_TblPcStore_mBindingType];


GO
ALTER TABLE [dbo].[TblPcStore] NOCHECK CONSTRAINT [CK_TblPcStore_mUserNo];


GO

CREATE NONCLUSTERED INDEX [NC_TblPcStore_2]
    ON [dbo].[TblPcStore]([mItemNo] ASC, [mCnt] ASC);


GO

CREATE CLUSTERED INDEX [CL_TblPcStore]
    ON [dbo].[TblPcStore]([mUserNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 惑怕(0:历林, 1:焊烹, 2:绵汗, 3:罚待)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mStatus';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'芒绊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 矫府倔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瓤苞 瘤加矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mPracticalPeriod';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤侩 啊瓷茄 冉荐( 0 : 荤侩阂啊)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mCntUse';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤侩磊 酒捞叼 PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mUserNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'拘幅咯何 ( 0 : 拘幅X, 1 : 拘幅O )', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mIsSeizure';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'官牢爹 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mBindingType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利侩等 酒捞袍 辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mApplyAbnItemEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 拌沥 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡瓤扁埃 雀汗 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mRestoreCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利侩等 酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mApplyAbnItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'犬牢 咯何( 犬牢且 荐 乐绰 酒捞袍)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcStore', @level2type = N'COLUMN', @level2name = N'mIsConfirm';


GO

