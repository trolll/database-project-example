CREATE TABLE [dbo].[TblPcAbnormal] (
    [mPcNo]       INT     NOT NULL,
    [mParmNo]     INT     NOT NULL,
    [mLeftTime]   INT     NOT NULL,
    [mAbParmNo]   INT     CONSTRAINT [DF_TblPcAbnormal_mAbParmNo] DEFAULT ((0)) NOT NULL,
    [mRestoreCnt] TINYINT CONSTRAINT [DF_TblPcAbnormal_mRestoreCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblPcAbnormal] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mParmNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_TblChaosBattlePc_TblPcAbnormal] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblChaosBattlePc] ([mNo])
);


GO
ALTER TABLE [dbo].[TblPcAbnormal] NOCHECK CONSTRAINT [FK_TblChaosBattlePc_TblPcAbnormal];


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡瓤扁埃 雀汗 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcAbnormal', @level2type = N'COLUMN', @level2name = N'mRestoreCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'惑怕捞惑(檬)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcAbnormal', @level2type = N'COLUMN', @level2name = N'mLeftTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'惑怕捞惑喊 绊蜡蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcAbnormal', @level2type = N'COLUMN', @level2name = N'mAbParmNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcAbnormal', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'惑怕捞惑 内靛. DT_Abnormal 曼炼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcAbnormal', @level2type = N'COLUMN', @level2name = N'mParmNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 惑怕捞惑', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcAbnormal';


GO

