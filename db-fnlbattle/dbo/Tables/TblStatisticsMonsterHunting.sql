CREATE TABLE [dbo].[TblStatisticsMonsterHunting] (
    [mRegDate]    DATETIME DEFAULT (getdate()) NOT NULL,
    [MID]         INT      NOT NULL,
    [mHuntingCnt] BIGINT   NULL,
    CONSTRAINT [CL_PKTblStatisticsMonsterHunting] PRIMARY KEY CLUSTERED ([MID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐 荤成 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsMonsterHunting', @level2type = N'COLUMN', @level2name = N'mHuntingCnt';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'[烹拌]阁胶磐 荤成 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsMonsterHunting';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsMonsterHunting', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsMonsterHunting', @level2type = N'COLUMN', @level2name = N'MID';


GO

