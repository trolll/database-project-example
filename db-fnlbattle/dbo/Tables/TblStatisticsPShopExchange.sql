CREATE TABLE [dbo].[TblStatisticsPShopExchange] (
    [mRegDate]        DATETIME DEFAULT (getdate()) NOT NULL,
    [mItemNo]         INT      NOT NULL,
    [mBuyCount]       BIGINT   NULL,
    [mSellCount]      BIGINT   NULL,
    [mBuyTotalPrice]  BIGINT   NULL,
    [mSellTotalPrice] BIGINT   NULL,
    [mBuyMinPrice]    BIGINT   NULL,
    [mSellMinPrice]   BIGINT   NULL,
    [mBuyMaxPrice]    BIGINT   NULL,
    [mSellMaxPrice]   BIGINT   NULL,
    [mItemStatus]     SMALLINT CONSTRAINT [DF_TblStatisticsPShopExchange_mItemStatus] DEFAULT ((0)) NOT NULL,
    [mTradeType]      SMALLINT CONSTRAINT [DF_TblStatisticsPShopExchange_mTradeType] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblStatisticsPShopExchange_mItemNo_mItemStatus_mTradeType] PRIMARY KEY CLUSTERED ([mItemNo] ASC, [mItemStatus] ASC, [mTradeType] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥绊备概啊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mBuyMaxPrice';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'[烹拌] 俺牢惑痢阑 烹秦 芭贰等 酒捞袍郴开', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥绊概档啊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mSellMaxPrice';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'醚概档咀', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mSellTotalPrice';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥历备概啊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mBuyMinPrice';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥历概档啊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mSellMinPrice';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'概档樊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mSellCount';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'醚备概咀', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mBuyTotalPrice';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 惑怕(0:历林, 1:焊烹, 2:绵汗, 3:罚待)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mItemStatus';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'背券鸥涝(0:俺牢芭贰, 1:困殴惑痢芭贰)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mTradeType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'备概樊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsPShopExchange', @level2type = N'COLUMN', @level2name = N'mBuyCount';


GO

