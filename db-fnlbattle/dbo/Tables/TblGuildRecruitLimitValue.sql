CREATE TABLE [dbo].[TblGuildRecruitLimitValue] (
    [mGuildNo]  INT      NOT NULL,
    [mClass]    TINYINT  NOT NULL,
    [mMinLevel] SMALLINT NOT NULL,
    [mMinChao]  SMALLINT NOT NULL,
    CONSTRAINT [UCL_TblGuildRecruitLimitValue_UnitedKey] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mClass] ASC),
    CONSTRAINT [CK_TblGuildRecruitLimitValue_mClass] CHECK ((0)<=[mClass] AND [mClass]<=(4)),
    CONSTRAINT [CK_TblGuildRecruitLimitValue_mMinChao] CHECK ((-30000)<=[mMinChao] AND [mMinChao]<=(30000)),
    CONSTRAINT [CK_TblGuildRecruitLimitValue_mMinLevel] CHECK ((1)<=[mMinLevel] AND [mMinLevel]<=(100))
);


GO
ALTER TABLE [dbo].[TblGuildRecruitLimitValue] NOCHECK CONSTRAINT [CK_TblGuildRecruitLimitValue_mClass];


GO
ALTER TABLE [dbo].[TblGuildRecruitLimitValue] NOCHECK CONSTRAINT [CK_TblGuildRecruitLimitValue_mMinChao];


GO
ALTER TABLE [dbo].[TblGuildRecruitLimitValue] NOCHECK CONSTRAINT [CK_TblGuildRecruitLimitValue_mMinLevel];


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitLimitValue', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛葛笼矫胶袍狼 葛笼吝牢 辨靛 啊涝 弥家 炼扒', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitLimitValue';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啊涝 弥家 Chaotic', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitLimitValue', @level2type = N'COLUMN', @level2name = N'mMinChao';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'努贰胶 侥喊 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitLimitValue', @level2type = N'COLUMN', @level2name = N'mClass';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啊涝 弥家 Level', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildRecruitLimitValue', @level2type = N'COLUMN', @level2name = N'mMinLevel';


GO

