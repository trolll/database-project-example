CREATE TABLE [dbo].[TblQuickSupplicationReply] (
    [mRegDate] DATETIME       DEFAULT (getdate()) NOT NULL,
    [mQSID]    BIGINT         NOT NULL,
    [mReply]   VARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_CL_TblQuickSupplicationReply] PRIMARY KEY CLUSTERED ([mQSID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationReply', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationReply', @level2type = N'COLUMN', @level2name = N'mQSID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'脚绊窃 览翠', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationReply';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 览翠', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationReply', @level2type = N'COLUMN', @level2name = N'mReply';


GO

