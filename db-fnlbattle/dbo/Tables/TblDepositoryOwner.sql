CREATE TABLE [dbo].[TblDepositoryOwner] (
    [mRegDate] SMALLDATETIME NOT NULL,
    [mRegNo]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [mOwner]   INT           NOT NULL,
    [mPos]     TINYINT       NOT NULL,
    [mPcNo]    INT           NULL,
    [mPcNm]    VARCHAR (12)  NULL,
    [mItemCnt] INT           NOT NULL,
    CONSTRAINT [NC_PK_TblDepositoryOwner_1] PRIMARY KEY NONCLUSTERED ([mRegNo] ASC),
    CONSTRAINT [CK_TblDepository_mPos] CHECK ([mPos]=(2) OR ([mPos]=(1) OR [mPos]=(0)))
);


GO
ALTER TABLE [dbo].[TblDepositoryOwner] NOCHECK CONSTRAINT [CK_TblDepository_mPos];


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblDepositoryOwner_2]
    ON [dbo].[TblDepositoryOwner]([mRegDate] ASC, [mOwner] ASC, [mPos] ASC, [mPcNo] ASC);


GO

CREATE CLUSTERED INDEX [CL_TblDepositoryOwner]
    ON [dbo].[TblDepositoryOwner]([mOwner] ASC, [mPos] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'困摹', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepositoryOwner', @level2type = N'COLUMN', @level2name = N'mPos';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐疙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepositoryOwner', @level2type = N'COLUMN', @level2name = N'mPcNm';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'[檬扁拳] 焊包窃 家蜡磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepositoryOwner';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepositoryOwner', @level2type = N'COLUMN', @level2name = N'mItemCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡丰 酒捞袍 焊包窃 家蜡 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepositoryOwner', @level2type = N'COLUMN', @level2name = N'mRegNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepositoryOwner', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepositoryOwner', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDepositoryOwner', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

