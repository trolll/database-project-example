CREATE TABLE [dbo].[TblGuildStore] (
    [mRegDate]             DATETIME      DEFAULT (getdate()) NOT NULL,
    [mSerialNo]            BIGINT        NOT NULL,
    [mGuildNo]             INT           NOT NULL,
    [mItemNo]              INT           NOT NULL,
    [mEndDate]             SMALLDATETIME NOT NULL,
    [mIsConfirm]           BIT           NOT NULL,
    [mStatus]              TINYINT       NOT NULL,
    [mCnt]                 INT           NOT NULL,
    [mCntUse]              TINYINT       DEFAULT ((0)) NOT NULL,
    [mIsSeizure]           BIT           DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemNo]      INT           DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemEndDate] SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [mOwner]               INT           NOT NULL,
    [mGrade]               TINYINT       NOT NULL,
    [mPracticalPeriod]     INT           DEFAULT ((0)) NOT NULL,
    [mBindingType]         TINYINT       CONSTRAINT [DF_TblGuildStore_mBindingType] DEFAULT ((0)) NOT NULL,
    [mRestoreCnt]          TINYINT       CONSTRAINT [DF_TblGuildStore_mRestoreCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [NC_PK_TblGuildStore_1] PRIMARY KEY NONCLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblGuildStore_1] CHECK ((0)<[mCnt]),
    CONSTRAINT [CK_TblGuildStore_2] CHECK ((0)<=[mGrade] AND [mGrade]<(2)),
    CONSTRAINT [CK_TblGuildStore_mBindingType] CHECK ([mBindingType]=(2) OR ([mBindingType]=(1) OR [mBindingType]=(0))),
    CONSTRAINT [FkTblGuildStoremGuildNo] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[TblGuildStore] NOCHECK CONSTRAINT [CK_TblGuildStore_1];


GO
ALTER TABLE [dbo].[TblGuildStore] NOCHECK CONSTRAINT [CK_TblGuildStore_2];


GO
ALTER TABLE [dbo].[TblGuildStore] NOCHECK CONSTRAINT [CK_TblGuildStore_mBindingType];


GO
ALTER TABLE [dbo].[TblGuildStore] NOCHECK CONSTRAINT [FkTblGuildStoremGuildNo];


GO

CREATE NONCLUSTERED INDEX [NC_TblGuildStore_2]
    ON [dbo].[TblGuildStore]([mItemNo] ASC, [mCnt] ASC);


GO

CREATE CLUSTERED INDEX [CL_TblGuildStore_1]
    ON [dbo].[TblGuildStore]([mGuildNo] ASC, [mGrade] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'俺荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤侩 啊瓷茄 冉荐( 0 : 荤侩阂啊)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mCntUse';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'拘幅 咯何 ( 0 : 拘幅X, 1 : 拘幅O )', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mIsSeizure';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'犬牢 咯何( 犬牢且 荐 乐绰 酒捞袍)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mIsConfirm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 惑怕', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mStatus';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'官牢爹 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mBindingType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡瓤扁埃 雀汗 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mRestoreCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 矫府倔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利侩等 酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mApplyAbnItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'利侩等 酒捞袍 辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mApplyAbnItemEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 拌沥 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 芒绊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'该败柳 酒捞袍狼 芒绊 饭骇', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mGrade';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瓤苞 瘤加矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStore', @level2type = N'COLUMN', @level2name = N'mPracticalPeriod';


GO

