CREATE TABLE [dbo].[TblGuildAgitAuction] (
    [mTerritory]   INT           NOT NULL,
    [mGuildAgitNo] INT           NOT NULL,
    [mCurBidGID]   INT           CONSTRAINT [DF_TblGuildAgitAuction_mCurBidGID] DEFAULT ((0)) NOT NULL,
    [mCurBidMoney] BIGINT        CONSTRAINT [DF_TblGuildAgitAuction_mCurBidMoney] DEFAULT ((0)) NOT NULL,
    [mRegDate]     SMALLDATETIME CONSTRAINT [DF_TblGuildAgitAuction_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblGuildAgitAuction] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mGuildAgitNo] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [IX_TblGuildAgitAuction]
    ON [dbo].[TblGuildAgitAuction]([mCurBidGID] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'泅犁 官牢靛等 赣聪', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitAuction', @level2type = N'COLUMN', @level2name = N'mCurBidMoney';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康配 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitAuction', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'泅犁 官牢靛等 辨靛 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitAuction', @level2type = N'COLUMN', @level2name = N'mCurBidGID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 版概', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitAuction';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitAuction', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 酒瘤飘 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitAuction', @level2type = N'COLUMN', @level2name = N'mGuildAgitNo';


GO

