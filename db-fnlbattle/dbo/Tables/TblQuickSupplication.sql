CREATE TABLE [dbo].[TblQuickSupplication] (
    [mQSID]         BIGINT         NOT NULL,
    [mSupplication] VARCHAR (1000) NOT NULL,
    [mPosX]         FLOAT (53)     NOT NULL,
    [mPosY]         FLOAT (53)     NOT NULL,
    [mPosZ]         FLOAT (53)     NOT NULL,
    [mIp]           CHAR (15)      NOT NULL,
    CONSTRAINT [PK_CL_TblQuickSupplication] PRIMARY KEY CLUSTERED ([mQSID] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 郴侩', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplication', @level2type = N'COLUMN', @level2name = N'mSupplication';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Z 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplication', @level2type = N'COLUMN', @level2name = N'mPosZ';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'X 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplication', @level2type = N'COLUMN', @level2name = N'mPosX';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'IP 林家', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplication', @level2type = N'COLUMN', @level2name = N'mIp';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'脚绊窃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplication';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplication', @level2type = N'COLUMN', @level2name = N'mQSID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Y 谅钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplication', @level2type = N'COLUMN', @level2name = N'mPosY';


GO

