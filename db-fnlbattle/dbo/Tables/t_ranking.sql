CREATE TABLE [dbo].[t_ranking] (
    [mPcNo]          INT           NOT NULL,
    [mContribution]  INT           NOT NULL,
    [mGuardian]      INT           NOT NULL,
    [mTeleportTower] INT           NOT NULL,
    [mPVP]           INT           NOT NULL,
    [mFieldSvrNo]    SMALLINT      NOT NULL,
    [mNm]            CHAR (12)     NOT NULL,
    [mChgNmDate]     SMALLDATETIME NULL
);


GO

CREATE CLUSTERED INDEX [ucl_t_ranking_mpcno]
    ON [dbo].[t_ranking]([mPcNo] ASC);


GO

CREATE NONCLUSTERED INDEX [nc_t_ranking_mTeleportTower]
    ON [dbo].[t_ranking]([mTeleportTower] DESC);


GO

CREATE NONCLUSTERED INDEX [nc_t_ranking_mGuardian]
    ON [dbo].[t_ranking]([mGuardian] DESC);


GO

CREATE NONCLUSTERED INDEX [nc_t_ranking_mContribution]
    ON [dbo].[t_ranking]([mContribution] DESC);


GO

CREATE NONCLUSTERED INDEX [nc_t_ranking_mPVP]
    ON [dbo].[t_ranking]([mPVP] DESC);


GO

