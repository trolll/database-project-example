CREATE TABLE [dbo].[TblStatisticsItemByMonster] (
    [mRegDate]    DATETIME DEFAULT (getdate()) NOT NULL,
    [MID]         INT      NOT NULL,
    [mItemNo]     INT      NOT NULL,
    [mCreate]     BIGINT   NULL,
    [mDelete]     BIGINT   NULL,
    [mItemStatus] SMALLINT CONSTRAINT [DF_TblStatisticsItemByMonster_mItemStatus] DEFAULT ((0)) NOT NULL,
    [mModType]    SMALLINT CONSTRAINT [DF_TblStatisticsItemByMonster_mModType] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PKTblStatisticsItemByMonster_MID_mItemNo_mItemStatus_mModType] PRIMARY KEY CLUSTERED ([MID] ASC, [mItemNo] ASC, [mItemStatus] ASC, [mModType] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByMonster', @level2type = N'COLUMN', @level2name = N'mCreate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'昏力 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByMonster', @level2type = N'COLUMN', @level2name = N'mDelete';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 惑怕(0:历林, 1:焊烹, 2:绵汗, 3:罚待)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByMonster', @level2type = N'COLUMN', @level2name = N'mItemStatus';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'函版鸥涝(0:叼弃飘, 1:困殴惑痢殿废厚, 2:困殴惑痢 备概荐荐丰)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByMonster', @level2type = N'COLUMN', @level2name = N'mModType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByMonster', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'[烹拌]阁胶磐 靛酚 酒捞袍 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByMonster';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByMonster', @level2type = N'COLUMN', @level2name = N'MID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByMonster', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

