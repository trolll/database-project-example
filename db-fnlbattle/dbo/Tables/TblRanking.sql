CREATE TABLE [dbo].[TblRanking] (
    [mRegDate]  SMALLDATETIME NOT NULL,
    [mRanking]  SMALLINT      NOT NULL,
    [mRankType] SMALLINT      NOT NULL,
    [mPcNo]     INT           NOT NULL,
    [mPcNm]     CHAR (12)     NOT NULL,
    [mSvrNo]    SMALLINT      NOT NULL,
    [mPoint]    BIGINT        NOT NULL
);


GO

CREATE NONCLUSTERED INDEX [NC_TblRanking_mPcNo]
    ON [dbo].[TblRanking]([mPcNo] ASC);


GO

CREATE NONCLUSTERED INDEX [CL_TblRanking]
    ON [dbo].[TblRanking]([mRankType] ASC, [mRanking] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'珐欧鸥涝 ( 0 = 傈厘 扁咯档, 1 = 荐龋脚 痢飞, 2 = 捞悼器呕 痢飞, 3 = PVP )', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRanking', @level2type = N'COLUMN', @level2name = N'mRankType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'珐欧', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRanking', @level2type = N'COLUMN', @level2name = N'mRanking';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRanking', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRanking', @level2type = N'COLUMN', @level2name = N'mSvrNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'珐欧 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRanking';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐疙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRanking', @level2type = N'COLUMN', @level2name = N'mPcNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'器牢飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRanking', @level2type = N'COLUMN', @level2name = N'mPoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRanking', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

