CREATE TABLE [dbo].[TblPcEquip] (
    [mRegDate]  DATETIME CONSTRAINT [DF__TblPcEqui__mRegD__2F2FFC0C] DEFAULT (getdate()) NOT NULL,
    [mOwner]    INT      NOT NULL,
    [mSlot]     INT      NOT NULL,
    [mSerialNo] BIGINT   NOT NULL,
    CONSTRAINT [PkTblPcEquip] PRIMARY KEY CLUSTERED ([mOwner] ASC, [mSlot] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FkTblPcEquipTblPcInventory] FOREIGN KEY ([mSerialNo]) REFERENCES [dbo].[TblPcInventory] ([mSerialNo]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[TblPcEquip] NOCHECK CONSTRAINT [FkTblPcEquipTblPcInventory];


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblPcEquip]
    ON [dbo].[TblPcEquip]([mSerialNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'浇吩 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcEquip', @level2type = N'COLUMN', @level2name = N'mSlot';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'厘馒老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcEquip', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcEquip', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 酒捞袍 厘馒', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcEquip';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 矫府倔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcEquip', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

