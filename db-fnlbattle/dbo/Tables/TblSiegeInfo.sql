CREATE TABLE [dbo].[TblSiegeInfo] (
    [mRegDate]   DATETIME CONSTRAINT [DF_TblSiegeInfo_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mChgDate]   DATETIME CONSTRAINT [DF_TblSiegeInfo_mChgDate] DEFAULT ('2000-01-01 00:00') NOT NULL,
    [mTerritory] INT      NOT NULL,
    [mIsSiege]   INT      CONSTRAINT [DF_TblSiegeInfo_mIsSiege] DEFAULT ((2)) NOT NULL,
    CONSTRAINT [IX_TblSiegeInfo] UNIQUE NONCLUSTERED ([mTerritory] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'傍己', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傍己 咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeInfo', @level2type = N'COLUMN', @level2name = N'mIsSiege';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痢飞瘤开', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeInfo', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'函版老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeInfo', @level2type = N'COLUMN', @level2name = N'mChgDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblSiegeInfo', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

