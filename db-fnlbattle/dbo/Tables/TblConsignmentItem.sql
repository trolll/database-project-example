CREATE TABLE [dbo].[TblConsignmentItem] (
    [mCnsmNo]          BIGINT   NOT NULL,
    [mRegDate]         DATETIME NOT NULL,
    [mSerialNo]        BIGINT   NOT NULL,
    [mEndDate]         DATETIME NOT NULL,
    [mIsConfirm]       BIT      NOT NULL,
    [mStatus]          TINYINT  NOT NULL,
    [mCntUse]          TINYINT  NOT NULL,
    [mOwner]           INT      NOT NULL,
    [mPracticalPeriod] INT      NOT NULL,
    [mBindingType]     TINYINT  NOT NULL,
    [mRestoreCnt]      TINYINT  NOT NULL,
    CONSTRAINT [UCL_FK_TblConsignmentItem_mCnsmNo] PRIMARY KEY CLUSTERED ([mCnsmNo] ASC)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [NC_TblConsignmentItem_mSerialNo]
    ON [dbo].[TblConsignmentItem]([mSerialNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'官牢爹 鸥涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mBindingType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤侩 啊瓷茄 冉荐( 0 : 荤侩阂啊)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mCntUse';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'困殴 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mCnsmNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'犬牢 咯何( 犬牢且 荐 乐绰 酒捞袍)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mIsConfirm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡瓤扁埃 雀汗 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mRestoreCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 拌沥 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mOwner';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瓤苞 瘤加矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mPracticalPeriod';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'困殴 魄概 酒捞袍 抛捞喉', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 惑怕', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mStatus';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 矫府倔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentItem', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

