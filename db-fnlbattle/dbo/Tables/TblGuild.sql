CREATE TABLE [dbo].[TblGuild] (
    [mRegDate]          DATETIME      CONSTRAINT [DF__TblGuild__mRegDa__04659998] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]          INT           IDENTITY (1, 1) NOT NULL,
    [mGuildSeqNo]       INT           CONSTRAINT [DF_TblGuild_mGuildSeqNo] DEFAULT ((0)) NOT NULL,
    [mGuildNm]          CHAR (12)     NOT NULL,
    [mGuildMsg]         CHAR (60)     NULL,
    [mGuildMark]        BINARY (1784) NULL,
    [mRewardExp]        INT           CONSTRAINT [DF_TblGuild_mRewardExp] DEFAULT ((0)) NOT NULL,
    [mGuildMarkUptDate] SMALLDATETIME CONSTRAINT [DF_TblGuild_mUptDate] DEFAULT (getdate()) NOT NULL,
    [mCastleDfnsLv]     SMALLINT      CONSTRAINT [DF_TblGuild_mCastleDfnsLv] DEFAULT ((0)) NOT NULL,
    [mSpotDfnsLv]       SMALLINT      CONSTRAINT [DF_TblGuild_mSpotDfnsLv] DEFAULT ((0)) NOT NULL,
    [mSkillTreePoint]   SMALLINT      CONSTRAINT [DF_TblGuild_mSkillTreePoint] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblGuild] PRIMARY KEY CLUSTERED ([mGuildNo] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblGuild_mGuildMarkUptDate]
    ON [dbo].[TblGuild]([mGuildMarkUptDate] DESC) WITH (FILLFACTOR = 90);


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblGuildNm]
    ON [dbo].[TblGuild]([mGuildNm] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 付农 函版 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mGuildSeqNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 付农 盎脚老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mGuildMarkUptDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版氰摹', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mRewardExp';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶铺 规绢 驱琶 饭骇', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mSpotDfnsLv';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 皋矫瘤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mGuildMsg';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'己 规绢 驱琶 饭骇', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mCastleDfnsLv';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mGuildNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦飘府 器牢飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mSkillTreePoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 付农', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mGuildMark';


GO

