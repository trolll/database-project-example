CREATE TABLE [dbo].[TblLetter] (
    [mRegDate]  DATETIME      CONSTRAINT [DF__TblLetter__mRegD__3414ACBA] DEFAULT (getdate()) NOT NULL,
    [mSerialNo] BIGINT        NOT NULL,
    [mFromPcNm] CHAR (12)     NOT NULL,
    [mTitle]    CHAR (30)     NOT NULL,
    [mMsg]      VARCHAR (512) NOT NULL,
    [mToPcNm]   CHAR (12)     NOT NULL,
    CONSTRAINT [PkTblLetter] PRIMARY KEY CLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'祈瘤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLetter';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLetter', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'矫府倔锅龋 (TblPcInventory)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLetter', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'惯脚磊 某腐磐 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLetter', @level2type = N'COLUMN', @level2name = N'mFromPcNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'力格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLetter', @level2type = N'COLUMN', @level2name = N'mTitle';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'皋矫瘤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLetter', @level2type = N'COLUMN', @level2name = N'mMsg';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荐脚磊 某腐磐 酒捞叼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLetter', @level2type = N'COLUMN', @level2name = N'mToPcNm';


GO

