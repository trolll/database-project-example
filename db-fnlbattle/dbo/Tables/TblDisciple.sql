CREATE TABLE [dbo].[TblDisciple] (
    [mMaster]      INT      NOT NULL,
    [mCurPoint]    INT      CONSTRAINT [DF_TblDisciple_mCurPoint] DEFAULT ((0)) NOT NULL,
    [mMaxCurPoint] INT      CONSTRAINT [DF_TblDisciple_mMaxPoint] DEFAULT ((0)) NOT NULL,
    [mRegDate]     DATETIME CONSTRAINT [DF_TblDisciple_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mDecDate]     DATETIME CONSTRAINT [DF_TblDisciple_mDecDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [CL_PK_TblDisciple] PRIMARY KEY CLUSTERED ([mMaster] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDisciple', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'泅犁 儡咯器牢飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDisciple', @level2type = N'COLUMN', @level2name = N'mCurPoint';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'荤力', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDisciple';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'泅犁鳖瘤狼 弥措 儡咯器牢飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDisciple', @level2type = N'COLUMN', @level2name = N'mMaxCurPoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付瘤阜 器牢飘 皑家老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDisciple', @level2type = N'COLUMN', @level2name = N'mDecDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付胶磐 某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDisciple', @level2type = N'COLUMN', @level2name = N'mMaster';


GO

