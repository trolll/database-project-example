CREATE TABLE [dbo].[TblGuildStorePassword] (
    [mRegDate]  DATETIME CONSTRAINT [DF_TblGuildStorePassword_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]  INT      NOT NULL,
    [mPassword] CHAR (8) NOT NULL,
    [mGrade]    TINYINT  NOT NULL,
    CONSTRAINT [CL_PK_TblGuildStorePassword] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mGrade] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [CK_TblGuildStorePassword_mGrade] CHECK ((0)<=[mGrade] AND [mGrade]<(2)),
    CONSTRAINT [FkTblGuildStorePasswordmGuildNo] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[TblGuildStorePassword] NOCHECK CONSTRAINT [CK_TblGuildStorePassword_mGrade];


GO
ALTER TABLE [dbo].[TblGuildStorePassword] NOCHECK CONSTRAINT [FkTblGuildStorePasswordmGuildNo];


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 芒绊 菩胶况靛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStorePassword';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStorePassword', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStorePassword', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'菩胶况靛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStorePassword', @level2type = N'COLUMN', @level2name = N'mPassword';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'芒绊 饭骇 1, 芒绊 饭骇 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildStorePassword', @level2type = N'COLUMN', @level2name = N'mGrade';


GO

