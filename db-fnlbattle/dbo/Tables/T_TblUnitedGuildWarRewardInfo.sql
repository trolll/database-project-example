CREATE TABLE [dbo].[T_TblUnitedGuildWarRewardInfo] (
    [mRewardDate]       SMALLDATETIME NOT NULL,
    [mRound]            INT           NOT NULL,
    [mRanking]          TINYINT       NOT NULL,
    [mRewardSvrNo]      INT           NOT NULL,
    [mAdvantage]        INT           NOT NULL,
    [mIsTournament]     BIT           NOT NULL,
    [mChaosBattleSvrNo] SMALLINT      NOT NULL,
    [mIsValid]          BIT           NOT NULL
);


GO

