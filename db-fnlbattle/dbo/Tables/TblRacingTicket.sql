CREATE TABLE [dbo].[TblRacingTicket] (
    [mSerialNo] BIGINT NOT NULL,
    [mPlace]    INT    NOT NULL,
    [mStage]    INT    NOT NULL,
    [mNID]      INT    NOT NULL,
    CONSTRAINT [PK_TblRacingTicket] PRIMARY KEY CLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公措 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingTicket', @level2type = N'COLUMN', @level2name = N'mStage';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瘤开(FNLParm.dbo.TblPlace)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingTicket', @level2type = N'COLUMN', @level2name = N'mPlace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NPC ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingTicket', @level2type = N'COLUMN', @level2name = N'mNID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'版林厘钎', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingTicket';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'版林厘萍南 矫府倔 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblRacingTicket', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

