CREATE TABLE [dbo].[TblGuildAgitTicket] (
    [mTicketSerialNo] BIGINT        NOT NULL,
    [mTerritory]      INT           NOT NULL,
    [mGuildAgitNo]    INT           NOT NULL,
    [mOwnerGID]       INT           NOT NULL,
    [mRegDate]        SMALLDATETIME CONSTRAINT [DF_TblGuildAgitTicket_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mFromPcNm]       NVARCHAR (12) CONSTRAINT [DF_TblGuildAgitTicket_mFromPcNm] DEFAULT ('') NOT NULL,
    [mToPcNm]         NVARCHAR (12) CONSTRAINT [DF_TblGuildAgitTicket_mToPcNm] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_TblGuildAgitTicket] PRIMARY KEY CLUSTERED ([mTicketSerialNo] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'焊郴柯 捞狼 某腐磐 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitTicket', @level2type = N'COLUMN', @level2name = N'mFromPcNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 酒瘤飘 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitTicket', @level2type = N'COLUMN', @level2name = N'mGuildAgitNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitTicket', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康配 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitTicket', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 辨靛 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitTicket', @level2type = N'COLUMN', @level2name = N'mOwnerGID';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 酒瘤飘 萍南', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitTicket';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'罐绰 捞狼 某腐磐 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgitTicket', @level2type = N'COLUMN', @level2name = N'mToPcNm';


GO

