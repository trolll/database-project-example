CREATE TABLE [dbo].[TblGuildAss] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF__TblGuildA__mRegD__550C5788] DEFAULT (getdate()) NOT NULL,
    [mGuildNo] INT           NOT NULL,
    CONSTRAINT [PkTblGuildAss] PRIMARY KEY CLUSTERED ([mGuildNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FkTblGuildAssTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo])
);


GO
ALTER TABLE [dbo].[TblGuildAss] NOCHECK CONSTRAINT [FkTblGuildAssTblGuild];


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAss', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 楷钦', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAss';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'楷钦厘狼 辨靛 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAss', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

