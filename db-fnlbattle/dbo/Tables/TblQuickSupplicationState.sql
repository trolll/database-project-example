CREATE TABLE [dbo].[TblQuickSupplicationState] (
    [mRegDate]  DATETIME DEFAULT (getdate()) NOT NULL,
    [mQSID]     BIGINT   IDENTITY (1, 1) NOT NULL,
    [mPcNo]     INT      NOT NULL,
    [mCategory] TINYINT  NOT NULL,
    [mStatus]   TINYINT  DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_NC_TblQuickSupplicationState] PRIMARY KEY NONCLUSTERED ([mQSID] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE CLUSTERED INDEX [CL_TblQuickSupplicationState_mPcNo]
    ON [dbo].[TblQuickSupplicationState]([mPcNo] ASC, [mStatus] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationState', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0 : 殿废肯丰, 1 : 贸府 肯丰, 2: 蜡历犬牢 肯丰', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationState', @level2type = N'COLUMN', @level2name = N'mStatus';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'脚绊 PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationState', @level2type = N'COLUMN', @level2name = N'mQSID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 PK ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationState', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'脚绊窃 惑怕', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationState';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0 : 某腐磐 捞悼 阂啊, 1 : 阂樊捞侩磊 脚绊, 2: 滚弊 脚绊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblQuickSupplicationState', @level2type = N'COLUMN', @level2name = N'mCategory';


GO

