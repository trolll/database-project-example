CREATE TABLE [dbo].[TblPcSkillTreeInventory] (
    [mRegDate] DATETIME      CONSTRAINT [DF_TblPcSkillTreeInventory_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT           NOT NULL,
    [mSTNIID]  INT           NOT NULL,
    [mEndDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [UCL_PK_TblPcSkillTreeInventory_mPcNo_mSTNIID] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mSTNIID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡 PC 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶懦飘府畴靛酒捞袍ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mSTNIID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillTreeInventory', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 胶懦飘府 牢亥配府', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcSkillTreeInventory';


GO

