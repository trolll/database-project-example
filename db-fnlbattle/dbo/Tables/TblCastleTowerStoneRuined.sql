CREATE TABLE [dbo].[TblCastleTowerStoneRuined] (
    [mPlace] INT NOT NULL,
    CONSTRAINT [UCL_PK_TblCastleTowerStoneRuined] PRIMARY KEY CLUSTERED ([mPlace] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'企倾拳等 胶铺狼 瘤开锅龋 府胶飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStoneRuined';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'企倾拳等 胶铺狼 瘤开锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStoneRuined', @level2type = N'COLUMN', @level2name = N'mPlace';


GO

