CREATE TABLE [dbo].[TblStatisticsItemByCase] (
    [mRegDate]          DATETIME DEFAULT (getdate()) NOT NULL,
    [mItemNo]           INT      NOT NULL,
    [mMerchantCreate]   BIGINT   NULL,
    [mMerchantDelete]   BIGINT   NULL,
    [mReinforceCreate]  BIGINT   NULL,
    [mReinforceDelete]  BIGINT   NULL,
    [mCraftingCreate]   BIGINT   NULL,
    [mCraftingDelete]   BIGINT   NULL,
    [mPcUseDelete]      BIGINT   NULL,
    [mNpcUseDelete]     BIGINT   NULL,
    [mNpcCreate]        BIGINT   NULL,
    [mMonsterDrop]      BIGINT   NULL,
    [mGSExchangeCreate] BIGINT   NULL,
    [mGSExchangeDelete] BIGINT   NULL,
    CONSTRAINT [CL_PKTblStatisticsItemByCase] PRIMARY KEY CLUSTERED ([mItemNo] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'榜靛 芭贰肺 牢茄 角滚 瞒皑', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mGSExchangeDelete';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'榜靛 芭贰肺 牢茄 角滚 积己', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mGSExchangeCreate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NPC 积己 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mNpcCreate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐 靛而 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mMonsterDrop';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'NPC 捞侩 昏力 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mNpcUseDelete';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'力累 昏力 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mCraftingDelete';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PC 捞侩 昏力 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mPcUseDelete';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒捞袍 ID(Parameter DB)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mItemNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'[烹拌]酒捞袍 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'力累 积己 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mCraftingCreate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'碍拳 昏力 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mReinforceDelete';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'惑痢 昏力荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mMerchantDelete';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'碍拳 积己 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mReinforceCreate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'惑痢 积己 荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStatisticsItemByCase', @level2type = N'COLUMN', @level2name = N'mMerchantCreate';


GO

