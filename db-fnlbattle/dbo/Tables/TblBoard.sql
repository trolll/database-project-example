CREATE TABLE [dbo].[TblBoard] (
    [mRegDate]  DATETIME      NOT NULL,
    [mBoardId]  TINYINT       NOT NULL,
    [mBoardNo]  INT           IDENTITY (1, 1) NOT NULL,
    [mFromPcNm] CHAR (12)     NOT NULL,
    [mTitle]    CHAR (30)     NOT NULL,
    [mMsg]      VARCHAR (512) NOT NULL,
    CONSTRAINT [PkTblBoard] PRIMARY KEY CLUSTERED ([mBoardId] ASC, [mBoardNo] ASC) WITH (FILLFACTOR = 90),
    CHECK ((0)<[mBoardNo]),
    CHECK ((0)<[mBoardNo]),
    CHECK ((0)<[mBoardNo]),
    CHECK ((0)<[mBoardNo]),
    CHECK ((0)<=[mBoardId]),
    CHECK ((0)<=[mBoardId]),
    CHECK ((0)<=[mBoardId]),
    CHECK ((0)<=[mBoardId]),
    CONSTRAINT [CK__TblBoard__mBoard__457442E6] CHECK ((0)<=[mBoardId]),
    CONSTRAINT [CK__TblBoard__mBoard__4668671F] CHECK ((0)<[mBoardNo])
);


GO
ALTER TABLE [dbo].[TblBoard] NOCHECK CONSTRAINT [CK__TblBoard__mBoard__457442E6];


GO
ALTER TABLE [dbo].[TblBoard] NOCHECK CONSTRAINT [CK__TblBoard__mBoard__4668671F];


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBoard', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'霸矫魄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBoard';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'霸矫魄 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBoard', @level2type = N'COLUMN', @level2name = N'mBoardNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'霸矫魄 辆幅 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBoard', @level2type = N'COLUMN', @level2name = N'mBoardId';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'力格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBoard', @level2type = N'COLUMN', @level2name = N'mTitle';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'霸矫磊疙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBoard', @level2type = N'COLUMN', @level2name = N'mFromPcNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'皋矫瘤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblBoard', @level2type = N'COLUMN', @level2name = N'mMsg';


GO

