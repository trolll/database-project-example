CREATE TABLE [dbo].[TblUnitedGuildWarTournamentStxInfo] (
    [mRegDate]               SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [mSvrNo]                 SMALLINT      NOT NULL,
    [mUTGWStxDate]           SMALLDATETIME NOT NULL,
    [mUTGWTournamentStxDate] SMALLDATETIME NOT NULL,
    [mUTGWTournamentEtxDate] SMALLDATETIME NOT NULL,
    [mPeriodMonth]           SMALLINT      NOT NULL,
    [mPeriodWeek]            SMALLINT      NOT NULL,
    [mDayOfTheWeek]          SMALLINT      NOT NULL,
    [mIngDay]                SMALLINT      NOT NULL,
    CONSTRAINT [UCL_TblUnitedGuildWarTournamentStxInfo] PRIMARY KEY CLUSTERED ([mSvrNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'mUTGWTournamentStxDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mUTGWTournamentStxDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'柳青扁埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mIngDay';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'促澜 配呈刚飘 矫累 沥焊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'mUTGWStxDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mUTGWStxDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配呈刚飘 矫累 夸老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mDayOfTheWeek';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配呈刚飘 馆汗 林扁 (岿)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mPeriodMonth';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑滚锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mSvrNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配呈刚飘 馆汗 林扁 (林)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mPeriodWeek';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'mUTGWTournamentEtxDate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mUTGWTournamentEtxDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'朝楼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblUnitedGuildWarTournamentStxInfo', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

