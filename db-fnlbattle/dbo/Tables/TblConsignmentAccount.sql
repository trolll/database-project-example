CREATE TABLE [dbo].[TblConsignmentAccount] (
    [mPcNo]    INT    NOT NULL,
    [mBalance] BIGINT NOT NULL,
    CONSTRAINT [UCL_PK_TblConsignmentAccount_mPcNo] PRIMARY KEY CLUSTERED ([mPcNo] ASC),
    CHECK ((0)<=[mBalance])
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 PcNo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentAccount', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'儡咀', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentAccount', @level2type = N'COLUMN', @level2name = N'mBalance';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'困殴 魄概 沥魂陛 拌谅', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblConsignmentAccount';


GO

