CREATE TABLE [dbo].[TblGuildMember] (
    [mRegDate]    DATETIME  CONSTRAINT [DF__TblGuildM__mRegD__07420643] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]    INT       NOT NULL,
    [mPcNo]       INT       NOT NULL,
    [mGuildGrade] TINYINT   NOT NULL,
    [mNickNm]     CHAR (16) NULL,
    CONSTRAINT [PkTblGuildMember] PRIMARY KEY NONCLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FkTblGuildMemberTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo]),
    CONSTRAINT [FkTblGuildMemberTblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO
ALTER TABLE [dbo].[TblGuildMember] NOCHECK CONSTRAINT [FkTblGuildMemberTblGuild];


GO
ALTER TABLE [dbo].[TblGuildMember] NOCHECK CONSTRAINT [FkTblGuildMemberTblPc];


GO

CREATE CLUSTERED INDEX [IX_TblGuildMember]
    ON [dbo].[TblGuildMember]([mGuildNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildMember', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildMember', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildMember', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛盔 葱匙烙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildMember', @level2type = N'COLUMN', @level2name = N'mNickNm';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 糕滚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildMember';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛盔 殿鞭', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildMember', @level2type = N'COLUMN', @level2name = N'mGuildGrade';


GO

