CREATE TABLE [dbo].[TblCastleTowerStone] (
    [mRegDate]      SMALLDATETIME CONSTRAINT [DF__TblCastle__mRegD__7BD11EEE] DEFAULT (getdate()) NOT NULL,
    [mChgDate]      SMALLDATETIME CONSTRAINT [DF__TblCastle__mChgD__7CC54327] DEFAULT (getdate()) NOT NULL,
    [mIsTower]      BIT           CONSTRAINT [DF_TblCastleTowerStone_mIsTower] DEFAULT ((0)) NOT NULL,
    [mPlace]        INT           NOT NULL,
    [mGuildNo]      INT           NOT NULL,
    [mTaxBuy]       INT           NOT NULL,
    [mTaxBuyMax]    INT           NOT NULL,
    [mTaxHunt]      INT           NOT NULL,
    [mTaxHuntMax]   INT           NOT NULL,
    [mTaxGamble]    INT           NOT NULL,
    [mTaxGambleMax] INT           NOT NULL,
    [mAsset]        BIGINT        NOT NULL,
    [mAssetBuy]     BIGINT        CONSTRAINT [DF_TblCastleTowerStone_mAssetBuy] DEFAULT ((0)) NOT NULL,
    [mAssetHunt]    BIGINT        CONSTRAINT [DF_TblCastleTowerStone_mAssetHunt] DEFAULT ((0)) NOT NULL,
    [mAssetGamble]  BIGINT        CONSTRAINT [DF_TblCastleTowerStone_mAssetGamble] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [CK__TblCastle__mAsse__037240B6] CHECK ((0)<=[mAsset]),
    CONSTRAINT [CK__TblCastle__mTaxB__7DB96760] CHECK ((0)<=[mTaxBuy]),
    CONSTRAINT [CK__TblCastle__mTaxB__7EAD8B99] CHECK ((0)<=[mTaxBuyMax]),
    CONSTRAINT [CK__TblCastle__mTaxG__0189F844] CHECK ((0)<=[mTaxGamble]),
    CONSTRAINT [CK__TblCastle__mTaxG__027E1C7D] CHECK ((0)<=[mTaxGambleMax]),
    CONSTRAINT [CK__TblCastle__mTaxH__0095D40B] CHECK ((0)<=[mTaxHuntMax]),
    CONSTRAINT [CK__TblCastle__mTaxH__7FA1AFD2] CHECK ((0)<=[mTaxHunt]),
    CONSTRAINT [FkTblCastleTowerStoneTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo])
);


GO
ALTER TABLE [dbo].[TblCastleTowerStone] NOCHECK CONSTRAINT [CK__TblCastle__mAsse__037240B6];


GO
ALTER TABLE [dbo].[TblCastleTowerStone] NOCHECK CONSTRAINT [CK__TblCastle__mTaxB__7DB96760];


GO
ALTER TABLE [dbo].[TblCastleTowerStone] NOCHECK CONSTRAINT [CK__TblCastle__mTaxB__7EAD8B99];


GO
ALTER TABLE [dbo].[TblCastleTowerStone] NOCHECK CONSTRAINT [CK__TblCastle__mTaxG__0189F844];


GO
ALTER TABLE [dbo].[TblCastleTowerStone] NOCHECK CONSTRAINT [CK__TblCastle__mTaxG__027E1C7D];


GO
ALTER TABLE [dbo].[TblCastleTowerStone] NOCHECK CONSTRAINT [CK__TblCastle__mTaxH__0095D40B];


GO
ALTER TABLE [dbo].[TblCastleTowerStone] NOCHECK CONSTRAINT [CK__TblCastle__mTaxH__7FA1AFD2];


GO
ALTER TABLE [dbo].[TblCastleTowerStone] NOCHECK CONSTRAINT [FkTblCastleTowerStoneTblGuild];


GO

CREATE CLUSTERED INDEX [IxTblCastleTowerStonePlace]
    ON [dbo].[TblCastleTowerStone]([mPlace] ASC) WITH (FILLFACTOR = 90);


GO

CREATE NONCLUSTERED INDEX [IxTblCastleTowerStoneGuildNo]
    ON [dbo].[TblCastleTowerStone]([mGuildNo] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瘤开', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mPlace';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥措 荤成 技陛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mTaxHuntMax';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤成栏肺何磐 掘绢柳 荐涝 醚钦', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mAssetHunt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'True : 己, False : 痢飞啊瓷茄 胶铺(瘤开)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mIsTower';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥措 档冠 技陛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mTaxGambleMax';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'档冠 技陛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mTaxGamble';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'档冠栏肺何磐 掘绢柳 荐涝 醚钦', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mAssetGamble';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'穿利等 荐涝', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mAsset';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'傍己 痢飞泅炔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'备涝 技陛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mTaxBuy';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荤成 技陛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mTaxHunt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'林牢捞 官诧 老矫', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mChgDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'惑痢栏肺何磐 掘绢柳 荐涝 醚钦', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mAssetBuy';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥措 备涝 技陛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mTaxBuyMax';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCastleTowerStone', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

