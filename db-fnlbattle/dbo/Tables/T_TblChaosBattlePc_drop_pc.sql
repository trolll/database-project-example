CREATE TABLE [dbo].[T_TblChaosBattlePc_drop_pc] (
    [mRegDate]      DATETIME      NOT NULL,
    [mOwner]        INT           NOT NULL,
    [mNo]           INT           NOT NULL,
    [mFieldSvrNo]   SMALLINT      NOT NULL,
    [mFieldSvrPcNo] INT           NOT NULL,
    [mNm]           CHAR (12)     NOT NULL,
    [mChgNmDate]    SMALLDATETIME NULL,
    [mLoginTm]      DATETIME      NOT NULL,
    [mLogoutTm]     DATETIME      NULL,
    [mTotUseTm]     INT           NOT NULL,
    [mDelDate]      SMALLDATETIME NULL
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [UNC_TblChaosBattlePc_drop_pc]
    ON [dbo].[T_TblChaosBattlePc_drop_pc]([mNo] ASC);


GO

