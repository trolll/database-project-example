CREATE TABLE [dbo].[TblDialogScript] (
    [mRegDate]    SMALLDATETIME  CONSTRAINT [DF_TblDialogScript_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mMId]        INT            NOT NULL,
    [mScriptText] VARCHAR (8000) CONSTRAINT [DF_TblDialogScript_mScript] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_TblDialogScript] PRIMARY KEY CLUSTERED ([mMId] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [IX_TblDialogScript]
    ON [dbo].[TblDialogScript]([mMId] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialogScript', @level2type = N'COLUMN', @level2name = N'mMId';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'措拳 胶农赋飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialogScript';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialogScript', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'胶农赋飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblDialogScript', @level2type = N'COLUMN', @level2name = N'mScriptText';


GO

