CREATE TABLE [dbo].[TblPcQuestCondition] (
    [mPcNo]      INT NOT NULL,
    [mQuestNo]   INT NOT NULL,
    [mMonsterNo] INT NOT NULL,
    [mCnt]       INT NOT NULL,
    [mMaxCnt]    INT NOT NULL,
    CONSTRAINT [UCL_PK_TblPcQuestCondition_mPcNo] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mQuestNo] ASC, [mMonsterNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'阁胶磐锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuestCondition', @level2type = N'COLUMN', @level2name = N'mMonsterNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'弥措荐樊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuestCondition', @level2type = N'COLUMN', @level2name = N'mMaxCnt';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'某腐磐 涅胶飘炼扒', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuestCondition';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'涅胶飘 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuestCondition', @level2type = N'COLUMN', @level2name = N'mQuestNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuestCondition', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'荐樊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcQuestCondition', @level2type = N'COLUMN', @level2name = N'mCnt';


GO

