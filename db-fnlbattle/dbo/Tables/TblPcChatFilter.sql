CREATE TABLE [dbo].[TblPcChatFilter] (
    [mRegDate]        SMALLDATETIME CONSTRAINT [DF_TblPcChatFilter_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mOwnerPcNo]      INT           CONSTRAINT [DF_TblPcChatFilter_mOwnerPcNo] DEFAULT ((0)) NOT NULL,
    [mChatFilterPcNo] INT           CONSTRAINT [DF_TblPcChatFilter_mChatFilterPcNo] DEFAULT ((0)) NOT NULL,
    [mChatFilterPcNm] CHAR (12)     CONSTRAINT [DF_TblPcChatFilter_mChatFilterPcNm] DEFAULT ('') NOT NULL,
    CONSTRAINT [UCL_PK_TblPcChatFilter_mOwnerPcNo_mChatFilterPcNo_mChatFilterPcNm] PRIMARY KEY CLUSTERED ([mOwnerPcNo] ASC, [mChatFilterPcNo] ASC, [mChatFilterPcNm] ASC),
    CONSTRAINT [FkTblPcChatFilterTblPc] FOREIGN KEY ([mOwnerPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO
ALTER TABLE [dbo].[TblPcChatFilter] NOCHECK CONSTRAINT [FkTblPcChatFilterTblPc];


GO

CREATE NONCLUSTERED INDEX [NCL_TblPcChatFilter_mChatFilterPcNm_mChatFilterPcNo]
    ON [dbo].[TblPcChatFilter]([mChatFilterPcNm] ASC, [mChatFilterPcNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瞒窜 某腐磐 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcChatFilter', @level2type = N'COLUMN', @level2name = N'mChatFilterPcNm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcChatFilter', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'瞒窜 某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcChatFilter', @level2type = N'COLUMN', @level2name = N'mChatFilterPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'盲泼 瞒窜', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcChatFilter';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcChatFilter', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

