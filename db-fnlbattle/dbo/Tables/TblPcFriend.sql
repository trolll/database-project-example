CREATE TABLE [dbo].[TblPcFriend] (
    [mRegDate]    SMALLDATETIME CONSTRAINT [DF__TblPcFrie__mRegD__6DD739FB] DEFAULT (getdate()) NOT NULL,
    [mOwnerPcNo]  INT           NOT NULL,
    [mFriendPcNo] INT           NOT NULL,
    [mFriendPcNm] CHAR (12)     NOT NULL,
    CONSTRAINT [PK_CL_TblPcFriend] PRIMARY KEY CLUSTERED ([mOwnerPcNo] ASC, [mFriendPcNo] ASC, [mFriendPcNm] ASC),
    CONSTRAINT [FkTblPcFriendTblPc] FOREIGN KEY ([mOwnerPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO
ALTER TABLE [dbo].[TblPcFriend] NOCHECK CONSTRAINT [FkTblPcFriendTblPc];


GO

CREATE NONCLUSTERED INDEX [NC_TblPcFriend_mFriendPcNm]
    ON [dbo].[TblPcFriend]([mFriendPcNm] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcFriend', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcFriend', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'模备', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcFriend';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'模备 某腐磐 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcFriend', @level2type = N'COLUMN', @level2name = N'mFriendPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'模备 某腐磐 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcFriend', @level2type = N'COLUMN', @level2name = N'mFriendPcNm';


GO

