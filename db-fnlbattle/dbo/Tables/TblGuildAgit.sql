CREATE TABLE [dbo].[TblGuildAgit] (
    [mTerritory]     INT           NOT NULL,
    [mGuildAgitNo]   INT           NOT NULL,
    [mOwnerGID]      INT           CONSTRAINT [DF_TblGuildAgit_mOwnerGID] DEFAULT ((0)) NOT NULL,
    [mGuildAgitName] NVARCHAR (50) CONSTRAINT [DF_TblGuildAgit_mGuildAgitName] DEFAULT ('') NOT NULL,
    [mRegDate]       SMALLDATETIME CONSTRAINT [DF_TblGuildAgit_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mLeftMin]       INT           CONSTRAINT [DF_TblGuildAgit_mLeftMin] DEFAULT ((0)) NOT NULL,
    [mIsSelling]     TINYINT       CONSTRAINT [DF_TblGuildAgit_mIsSelling] DEFAULT ((0)) NOT NULL,
    [mSellingMoney]  BIGINT        CONSTRAINT [DF_TblGuildAgit_mSellingMoney] DEFAULT ((0)) NOT NULL,
    [mBuyingMoney]   BIGINT        CONSTRAINT [DF_TblGuildAgit_mBuyingMoney] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblGuildAgit] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mGuildAgitNo] ASC) WITH (FILLFACTOR = 90)
);


GO

CREATE NONCLUSTERED INDEX [IX_TblGuildAgit]
    ON [dbo].[TblGuildAgit]([mOwnerGID] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'魄概陛咀', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mSellingMoney';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 酒瘤飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 酒瘤飘 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mGuildAgitNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'康配锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mTerritory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 酒瘤飘 捞抚', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mGuildAgitName';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'魄概咯何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mIsSelling';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊 辨靛 ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mOwnerGID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'儡咯矫埃(盒)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mLeftMin';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'备涝陛咀', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildAgit', @level2type = N'COLUMN', @level2name = N'mBuyingMoney';


GO

