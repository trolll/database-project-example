CREATE TABLE [dbo].[TblScriptExValue] (
    [mNo]  INT NOT NULL,
    [mVal] INT NOT NULL,
    CONSTRAINT [PK_TblScriptExternValue] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'犬厘 胶农赋飘', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblScriptExValue';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蔼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblScriptExValue', @level2type = N'COLUMN', @level2name = N'mVal';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'漂沥 悼累 锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblScriptExValue', @level2type = N'COLUMN', @level2name = N'mNo';


GO

