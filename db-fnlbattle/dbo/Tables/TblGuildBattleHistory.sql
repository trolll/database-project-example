CREATE TABLE [dbo].[TblGuildBattleHistory] (
    [mGuildNo1]      INT      NOT NULL,
    [mGuildNo2]      INT      NOT NULL,
    [mStxDate]       DATETIME NOT NULL,
    [mEtxDate]       DATETIME NOT NULL,
    [mWinnerGuildNo] INT      NOT NULL
);


GO

CREATE NONCLUSTERED INDEX [IxTblGuildBattleHistory]
    ON [dbo].[TblGuildBattleHistory]([mGuildNo1] ASC, [mGuildNo2] ASC) WITH (FILLFACTOR = 90);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattleHistory', @level2type = N'COLUMN', @level2name = N'mGuildNo2';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'铰府辨靛锅龋. 0捞搁 公铰何', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattleHistory', @level2type = N'COLUMN', @level2name = N'mWinnerGuildNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辨靛 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattleHistory', @level2type = N'COLUMN', @level2name = N'mGuildNo1';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辆丰 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattleHistory', @level2type = N'COLUMN', @level2name = N'mEtxDate';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辨靛 傈捧 捞仿', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattleHistory';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'矫累 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuildBattleHistory', @level2type = N'COLUMN', @level2name = N'mStxDate';


GO

