CREATE PROCEDURE [dbo].[UspGetRacingResultList]
	 @pPlace		INT
	,@pStage		INT
	,@pFetchPrev		TINYINT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF (@pStage <= 0)
	BEGIN
		SELECT TOP 1 @pStage = mStage
		FROM dbo.TblRacingResult
		WHERE mPlace = @pPlace
		ORDER BY mPlace, mStage DESC

		SET @pFetchPrev = 1	-- 啊厘 弥脚 郴侩何磐 捞傈狼 郴侩阑 啊廉客具 窃
	END

	IF (@pFetchPrev <> 0)
	BEGIN
		-- 漂沥 瘤开, 雀瞒锅龋狼 捞傈狼 N俺狼 搬苞 啊廉咳
		SELECT TOP 10 mPlace, mStage, mWinnerNID, mDividend
		FROM dbo.TblRacingResult
		WHERE mPlace = @pPlace AND mStage <= @pStage
		ORDER BY mPlace, mStage DESC
	END
	ELSE
	BEGIN
		-- 漂沥 瘤开, 雀瞒锅龋狼 捞饶狼 N俺狼 搬苞 啊廉咳
		SELECT TOP 10 mPlace, mStage, mWinnerNID, mDividend
		FROM dbo.TblRacingResult
		WHERE mPlace = @pPlace AND mStage >= @pStage
		ORDER BY mPlace, mStage ASC
	END

	SET NOCOUNT OFF

GO

