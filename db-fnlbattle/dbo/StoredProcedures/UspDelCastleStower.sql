CREATE PROCEDURE [dbo].[UspDelCastleStower]
	 @pPlaceNo		INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	

	DELETE dbo.TblCastleTowerStone 	
	WHERE ([mPlace] = @pPlaceNo)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END
	 
	RETURN(0)

GO

