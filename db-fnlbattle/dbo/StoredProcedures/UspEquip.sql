CREATE PROCEDURE [dbo].[UspEquip]
	 @pPcNo			INT		-- 1捞搁 阁胶磐促. 八荤侩烙.
	,@pSerial		BIGINT
	,@pWhere		INT			-- CPcEquip::ePosMax.
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
			
	DECLARE	@aCol		NVARCHAR(50)
	IF(0 = @pWhere)
		SET @aCol	= '[mWeapon]'
	ELSE IF(1 = @pWhere)
		SET @aCol	= '[mShield]'
	ELSE IF(2 = @pWhere)
		SET @aCol	= '[mArmor]'
	ELSE IF(3 = @pWhere)
		SET @aCol	= '[mRing1]'
	ELSE IF(4 = @pWhere)
		SET @aCol	= '[mRing2]'
	ELSE IF(5 = @pWhere)
		SET @aCol	= '[mAmulet]'
	ELSE IF(6 = @pWhere)
		SET @aCol	= '[mBoot]'
	ELSE IF(7 = @pWhere)
		SET @aCol	= '[mGlove]'
	ELSE IF(8 = @pWhere)
		SET @aCol	= '[mCap]'
	ELSE IF(9 = @pWhere)
		SET @aCol	= '[mBelt]'
	ELSE IF(10 = @pWhere)
		SET @aCol	= '[mCloak]'
	ELSE
	 BEGIN
		SET	@aErrNo	= 1
		GOTO LABEL_END
	 END
	
	DECLARE	@aSql		NVARCHAR(500)
	IF EXISTS(SELECT * FROM TblPcEquip WHERE (@pPcNo = [mOwner]))
	 BEGIN
		SET @aSql	= N'UPDATE dbo.TblPcEquip SET ' + @aCol + '=' + CAST(@pSerial AS NVARCHAR(20)) 
					+ ' WHERE ' + CAST(@pPcNo AS NVARCHAR(20)) + '=[mOwner]'
	 END
	ELSE
	 BEGIN
		SET @aSql	= N'INSERT dbo.TblPcEquip([mOwner],' + @aCol + ') VALUES(' 
					+ CAST(@pPcNo AS NVARCHAR(20)) + ',' + CAST(@pSerial AS NVARCHAR(20)) + ')'
	 END
	 
	EXEC @aErrNo=sp_executesql @aSql
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

