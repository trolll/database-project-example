/******************************************************************************  
**  File: 
**  Name: UspSetGuildSkillTreePoint  
**  Desc: Guild狼 胶懦飘府 器牢飘 技泼
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.28 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetGuildSkillTreePoint]
	 @pGuildNo		INT,
	@pPoint	SMALLINT 
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	update TblGuild
	set mSkillTreePoint = @pPoint
	where mGuildNo = @pGuildNo

	if @@error <> 0
	begin
		return 1
	end

	return 0

GO

