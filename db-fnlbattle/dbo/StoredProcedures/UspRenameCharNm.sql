CREATE PROCEDURE [dbo].[UspRenameCharNm]
	@pPcNo		INT,		-- 家蜡 某腐磐 锅龋
	@pUserNo	INT,		-- 拌沥 锅龋  
	@pOrgPcNm	CHAR(12),	-- 捞傈 某腐磐疙 
	@pNewPcNm	CHAR(12),	-- 函版且 某腐磐 捞抚
    @pSerialNo	BIGINT,		-- 酒捞袍 矫府倔 沥焊
	@pItemNo	INT			-- 酒捞袍 锅龋 	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE	@aErrNo		INT
			,@aRowCnt	INT 
			,@aNewPcNo	INT
			,@aOldPcNm	CHAR(12)
			,@aPlace		INT
			,@aGkillNode	INT
			,@aGuildNo	INT
			,@aDiscipleNo	INT
			,@aDiscipleType	TINYINT
	
	-- @aPlace啊 -1 捞搁 胶懦阑 呼靛茄利绝绰 某腐磐促
	SELECT  @aErrNo = 0, @aRowCnt = 0, @aNewPcNo = 0, @aPlace = -1, @aGuildNo = 0, @aDiscipleNo =0
	
	-----------------------------------------------
	-- 吝汗 某腐磐疙 粮犁咯何 眉农 
	-----------------------------------------------
	IF EXISTS( SELECT * 
				FROM dbo.TblPc
				WHERE mNm = @pNewPcNm )
	BEGIN
		SET @aErrNo = 2
		GOTO T_END
	END 
	
	-----------------------------------------------
	-- 捞傈 某腐疙苞 悼老咯何 眉农 
	-----------------------------------------------	
	SELECT 
		@aOldPcNm = T1.mNm
		, @aDiscipleNo = ISNULL(T2.mMaster, 0)
		, @aDiscipleType = ISNULL(T2.mType,0)
		, @aGuildNo = ISNULL(T3.mGuildNo, 0)
		, @aPlace = ISNULL(T4.mPlace, -1)
		, @aGkillNode = ISNULL(T4.mNode, -1)
	FROM dbo.TblPc T1
		LEFT OUTER JOIN dbo.TblDiscipleMember T2
			ON T1.mNo = T2.mDisciple 
		LEFT OUTER JOIN dbo.TblGuildMember T3
			ON T1.mNo = T3.mPcNo
		LEFT OUTER JOIN dbo.TblGkill T4
			ON T1.mNo = T4.mPcNo
	WHERE mNo = @pPcNo
	
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
	IF @aErrNo <> 0  OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END
	END 	
	
	IF @aOldPcNm <> @pOrgPcNm
	BEGIN
		SET @aErrNo = 3		-- 捞傈 某腐疙苞 悼老窍瘤 臼促. 
		GOTO T_END	
	END 
			
	BEGIN TRAN
					
		---------------------------------------------------
		-- 荤侩茄 酒捞袍 昏力
		---------------------------------------------------				
		DELETE dbo.TblPcInventory
		WHERE mPcNo = @pPcNo 
			AND	mSerialNo = @pSerialNo
			AND mItemNo = @pItemNo	
			
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1
			GOTO T_TRAN	 
		END 		
				
		---------------------------------------------------
		-- 某腐磐疙 函版 
		---------------------------------------------------
		UPDATE dbo.TblPc
		SET
			mNm = @pNewPcNm
		WHERE mOwner = @pUserNo
				AND mNo = @pPcNo
				
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			SET @aErrNo = 1
			GOTO T_TRAN
		END		
		
		---------------------------------------------------
		-- 捞傈 某腐磐疙 粮犁窍瘤 臼档废 函版(捞傈 某腐磐疙篮 昏力 朝楼甫 殿废) 
		---------------------------------------------------
		INSERT INTO dbo.TblPc( mOwner, mSlot, mNm, mClass,mHomeMapNo, mHomePosX, mHomePosY, mHomePosZ, mDelDate)
		VALUES(	0, 0,@pOrgPcNm, 0, 0, 0, 0, 0, GETDATE() )
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1
			GOTO T_TRAN
		END

		SET @aNewPcNo = @@IDENTITY
	 
		---------------------------------------------------
		-- 某腐磐 惑怕
		---------------------------------------------------
		INSERT INTO dbo.TblPcState(mNo, mLevel, mExp, mHpAdd, mHp, mMpAdd, mMp, mMapNo, mPosX, mPosY, mPosZ)
		VALUES(@aNewPcNo, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1
			GOTO T_TRAN
		END 
		
T_TRAN:
	IF(@aErrNo <> 0)
	 BEGIN
		ROLLBACK TRAN
	 END
	ELSE
	 BEGIN		
		COMMIT TRAN
	 END

T_END:
	SELECT	@aErrNo, @aGuildNo, @aPlace, @aGkillNode, @aDiscipleNo, @aDiscipleType

GO

