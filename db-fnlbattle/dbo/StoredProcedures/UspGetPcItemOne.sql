/******************************************************************************  
**  Name: UspGetPcItemOne  
**  Desc: 酒捞袍 茄 俺 沥焊 掘绢坷扁
**  
**                
**  Return values:  
**   饭内靛悸
**   
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcItemOne]
 @pSerialNo BIGINT
AS
	 SET NOCOUNT ON  ;
	 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  ;

	 SELECT   
	  mSerialNo  
	  , mItemNo  
	  , DATEDIFF(mi,GETDATE(),mEndDate) mEndDate     
	  , mCnt  
	  , mIsConfirm  
	  , mStatus  
	  , mCntUse  
	  , mIsSeizure  
	  , mApplyAbnItemNo  
	  , DATEDIFF(mi,GETDATE(),mApplyAbnItemEndDate) mApplyAbnItemEndDate   
	  , mOwner  
	  , mPracticalPeriod  
	  , mBindingType  
	  , mRestoreCnt  
	 FROM dbo.TblPcInventory WITH(INDEX(PK_NC_TblPcInventory_1), NOLOCK )  
	  WHERE  mSerialNo = @pSerialNo		
		 AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())  
		 AND mCnt > 0 ;

GO

