CREATE  PROCEDURE [dbo].[UspSendBanquetHallTicketToGuild]
	 @pSerial		BIGINT		-- 楷雀厘 檬没厘 锅龋.
	,@pToGuildNm	VARCHAR(12)
	,@pStatus		TINYINT
	,@pTerritory		INT		-- 康瘤锅龋
	,@pBanquetHallType	INT		-- 楷雀厘鸥涝
	,@pBanquetHallNo	INT		-- 楷雀厘锅龋
	,@pOwnerPcNo		INT		-- 家蜡磊
	,@pFromPcNm		VARCHAR(12)	-- 焊郴绰 荤恩 捞抚
    ,@pSendTicket   	INT		-- 焊郴绰 酒捞袍 捞抚
    ,@pRecvTicket   	INT		-- 罐绰 酒捞袍 捞抚
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE 	@aRowCnt 	INT,
				@aErrNo		INT
			
	SELECT @aErrNo = 0, @aRowCnt = 0

	DECLARE	@aFromPcNo		INT
	DECLARE	@aFromGuildNo		INT
	DECLARE	@aFromGuildAssNo	INT
	DECLARE	@aToGuildNo		INT
	DECLARE	@aToGuildAssNo		INT

	DECLARE @aSn	BIGINT
	DECLARE @aPcNo	INT
	DECLARE	@aEnd	DATETIME,
				@aBeginCount	INT,
				@aEndCount	INT
	DECLARE	@aBanquetHallTicket1	INT	-- 楷雀厘 檬没厘(辨靛)
	DECLARE	@aBanquetHallTicket2	INT	-- 罐篮 楷雀厘 檬没厘
				
	DECLARE	@aGuildMember	TABLE (
					mSeq		INT	 IDENTITY(1,1)	 NOT NULL,
					mPcNo		INT	 NOT NULL
				)

	SELECT		 @aEnd = dbo.UfnGetEndDate(GETDATE(), 7)
				,@aBeginCount = 0
				,@aEndCount = 0
				,@aSn = 0
				,@aPcNo = 0
				,@aBanquetHallTicket1 = @pSendTicket	-- 楷雀厘 檬没厘(辨靛) 锅龋 (窍靛内爹 登绢乐澜)
                ,@aBanquetHallTicket2 = @pRecvTicket	-- 楷雀厘 檬没厘 锅龋 (窍靛内爹 登绢乐澜)

	--------------------------------------------------------------------------	
	--辨靛 沥焊 眉农 
	--------------------------------------------------------------------------
	SELECT 
		@aToGuildNo = mGuildNo
	FROM dbo.TblGuild 
	WHERE mGuildNm = @pToGuildNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(2)		-- 2 : 罐绰 辨靛啊 粮犁窍瘤 臼澜
	END

	--------------------------------------------------------------------------	
	-- 2007.10.09 soundkey 辨靛楷钦盔牢瘤 眉农
	--------------------------------------------------------------------------
	-- From PcNo 掘绢坷扁
	SELECT
		@aFromPcNo = mNo
	FROM dbo.TblPc
	WHERE mNm = @pFromPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(4)	-- From PcNo 掘绢坷扁 角菩
	END

	-- From GuildNo 掘绢坷扁
	SELECT
		@aFromGuildNo = mGuildNo
	FROM dbo.TblGuildMember 
	WHERE mPcNo = @aFromPcNo

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(6)	-- 辨靛糕滚啊 酒丛
	END
	
	-- 楷钦辨靛牢瘤 八荤
	IF @aFromGuildNo <> @aToGuildNo	-- 夯牢捞 加茄 辨靛啊 酒聪扼搁 楷钦辨靛牢瘤 八荤
	BEGIN
		-- From GuildAssNo 掘绢坷扁
		SELECT
			@aFromGuildAssNo = mGuildAssNo
		FROM dbo.TblGuildAssMem
		WHERE mGuildNo = @aFromGuildNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			RETURN(6)	--辨靛楷钦糕滚啊 酒丛
		END

		-- To GuildAssNo 掘绢坷扁
		SELECT
			@aToGuildAssNo = mGuildAssNo
		FROM dbo.TblGuildAssMem
		WHERE mGuildNo = @aToGuildNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			RETURN(6)	-- 辨靛楷钦糕滚啊 酒丛
		END

		-- 鞍篮 辨靛楷钦捞 酒聪扼搁 俊矾贸府
		IF @aFromGuildAssNo <> @aToGuildAssNo 
		BEGIN
			RETURN(8)	-- 鞍篮 辨靛楷钦捞 酒丛
		END
		
	END

	--------------------------------------------------------------------------	
	--楷雀厘 檬措厘 力芭
	--------------------------------------------------------------------------
	DELETE dbo.TblPcInventory 
	WHERE mSerialNo = @pSerial
 			 AND mItemNo = @aBanquetHallTicket1

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(3)		-- 3 : 楷雀厘 檬没厘捞 粮犁窍瘤 臼澜
	END
		 
	--------------------------------------------------------------------------	
	--泅犁 辨靛盔 沥焊甫 掘绰促
	--------------------------------------------------------------------------
	INSERT INTO @aGuildMember
	SELECT mPcNo
	FROM dbo.TblGuildMember 
	WHERE  mGuildNo = @aToGuildNo
	
	SET @aEndCount = @@ROWCOUNT	

	--------------------------------------------------------------------------	
	--楷雀厘 檬没厘 傈价
	--------------------------------------------------------------------------	
	WHILE(@aEndCount <>0)
	BEGIN
		SELECT 
			TOP 1	@aPcNo = mPcNo
		FROM @aGuildMember
		WHERE mSeq = @aBeginCount+1

		EXEC @aSn =  dbo.UspGetItemSerial 
		IF @aSn <= 0
		BEGIN
			CONTINUE	 
		END			

		IF @aPcNo > 0 
		BEGIN
			INSERT dbo.TblPcInventory(mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
				VALUES(@aSn, @aPcNo, @aBanquetHallTicket2, @aEnd, 0, @pStatus, 1, 0)
			IF(0 = @@ERROR)
			BEGIN
			
				INSERT dbo.TblBanquetHallTicket (mTicketSerialNo, mTerritory, mBanquetHallType, mBanquetHallNo, mOwnerPcNo, mFromPcNm, mToPcNm)
					VALUES (@aSn,@pTerritory, @pBanquetHallType, @pBanquetHallNo, @pOwnerPcNo, @pFromPcNm, N'')
				IF(0 = @@ERROR)
				BEGIN
					SELECT @aPcNo, @aSn
				END
			END
		END

		SET @aEndCount = @aEndCount - 1
		SET @aBeginCount = @aBeginCount + 1
	END

	RETURN(0)

GO

