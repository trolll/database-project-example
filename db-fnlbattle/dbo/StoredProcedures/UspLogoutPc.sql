CREATE PROCEDURE [dbo].[UspLogoutPc]
	 @pUserNo	INT
	,@pPcNo		INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	DECLARE @aLogin	DATETIME
	SELECT 
		@aLogin=b.[mLoginTm] 
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
		ON(a.[mNo] = b.[mNo])
	WHERE (a.[mOwner]=@pUserNo) 
		AND (a.[mNo]=@pPcNo) 
		AND (a.[mDelDate] IS NULL)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END	 	 
	END
	
	DECLARE @aDate	DATETIME
	SET		@aDate = GETDATE()	
	DECLARE @aHour	INT
	SET		@aHour = DATEDIFF(mi, @aLogin, @aDate)
	
	UPDATE dbo.TblPcState 
	SET 
		[mLogoutTm]=@aDate, 
		[mTotUseTm]=[mTotUseTm]+@aHour
	WHERE ([mNo]=@pPcNo)
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo = 2
		GOTO LABEL_END	 
	 END

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

