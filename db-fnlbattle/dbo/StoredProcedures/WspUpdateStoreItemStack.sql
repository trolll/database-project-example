CREATE PROCEDURE [dbo].[WspUpdateStoreItemStack]  	
	@pSerial		BIGINT
	,@pLeftCnt		TINYINT
AS
	SET NOCOUNT ON	
		
	UPDATE dbo.TblPcStore
	SET 
		mCnt = @pLeftCnt
	WHERE  mSerialNo = @pSerial
	
	IF @@ERROR<>0 OR @@ROWCOUNT <> 1
	BEGIN
		RETURN(1)	 
	END	
	
	RETURN(0)

GO

