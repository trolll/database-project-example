--------------------------------
-- 按眉疙 : WspListCharRankByClass
-- 汲疙 : 努贰胶 喊 饭骇 鉴困甫 掘绰促
-- 累己磊 : 辫己犁
-- 累己老 : 2006.04.16
-- 荐沥磊 : 
-- 荐沥老 : 
--------------------------------
CREATE     PROCEDURE [dbo].[WspListCharRankByClass]
	@mClass TINYINT -- 努贰胶 (0:扁荤, 1:饭牢历, 2:郡橇)
AS
	SET NOCOUNT ON

	SELECT
		TOP 20 mNm, mLevel
	FROM
		[dbo].[TblPc] AS p WITH (NOLOCK)
	INNER JOIN
		[dbo].[TblPcState] AS s WITH (NOLOCK)
	ON
		p.mNo = s.mNo
	WHERE
		mClass = @mClass
		AND p.mDelDate IS NULL
	ORDER BY
		s.mLevel DESC, mExp DESC

GO

