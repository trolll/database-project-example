CREATE PROCEDURE [dbo].[UspDeleteDisciple]
	@pMaster	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT
	DECLARE @aCount INT
	SET @aRv = 0

	IF NOT EXISTS(SELECT mMaster FROM TblDisciple WHERE mMaster  = @pMaster)
	BEGIN
		RETURN(3)		-- 粮犁窍瘤 臼绰 葛烙
	END 

	SELECT @aCount = COUNT(mMaster) 
	FROM dbo.TblDiscipleMember 
	WHERE mMaster = @pMaster	
	IF @aCount > 1
	BEGIN
		RETURN(2)		-- 付胶磐甫 力寇茄 葛烙 糕滚啊 葛滴 呕硼秦具 窃
	END

	BEGIN TRAN
	
		DELETE dbo.TblDiscipleMember 
		WHERE mMaster = @pMaster		-- 单捞磐 部烙 规瘤侩
		
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1) -- DB ERROR
		END
		
		DELETE dbo.TblDiscipleHistory 
		WHERE mMaster = @pMaster		-- 单捞磐 部烙 规瘤侩

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1) -- DB ERROR
		END
		
		DELETE dbo.TblDisciple 
		WHERE mMaster = @pMaster

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1) -- DB ERROR
		END
		
	COMMIT TRAN

	RETURN(0) -- non error

GO

