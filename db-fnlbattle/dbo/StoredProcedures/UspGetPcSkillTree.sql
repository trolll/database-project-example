/******************************************************************************  
**  File: 
**  Name: UspGetPcSkillTree  
**  Desc: PC狼 胶懦飘府畴靛酒捞袍甸阑 肺靛茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcSkillTree]
	 @pPcNo		INT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	IF @pPcNo = 1 OR @pPcNo = 0 
	BEGIN
		RETURN(0)	-- NPC, 滚妨柳 酒捞袍
	END
	
	SELECT 
		mSTNIID,
		DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
	FROM dbo.TblPcSkillTreeInventory WITH(NOLOCK)
	WHERE 	mPcNo = @pPcNo
				AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY mSTNIID ASC

GO

