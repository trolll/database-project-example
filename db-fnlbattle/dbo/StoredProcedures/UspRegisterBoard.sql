CREATE Procedure [dbo].[UspRegisterBoard]
	 @pBoardId		TINYINT
	,@pPcNm			CHAR(12)
	,@pTitle		CHAR(30)
	,@pMsg			VARCHAR(512)
	,@pRegDate		DATETIME	OUTPUT
	,@pBoardNo		INT			OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	SET @pRegDate = GETDATE()
	INSERT TblBoard([mRegDate], [mBoardId], [mFromPcNm], [mTitle], [mMsg])
		VALUES(@pRegDate, @pBoardId, @pPcNm, @pTitle, @pMsg)
	SET @pBoardNo = @@IDENTITY
	IF(0 <> @@ERROR)
	 BEGIN
		SET	@aErrNo	= 1	 
	 END
			
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

