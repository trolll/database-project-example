CREATE PROCEDURE [dbo].[UspInsLimitedResource]
	@pResourceType	INT,
	@pMaxCnt		INT,
	@pRemainerCnt	INT,
	@pRandomVal		FLOAT
AS	
	SET NOCOUNT ON
	
	IF EXISTS(	SELECT *	
				FROM dbo.TblLimitedResource	
				WHERE mResourceType = @pResourceType )
	BEGIN
		RETURN(1)
	END				

	INSERT INTO dbo.TblLimitedResource
	VALUES(
		GETDATE(),
		@pResourceType,
		@pMaxCnt,
		@pRemainerCnt,
		@pRandomVal,
		GETDATE()
	)
		
	IF @@ERROR <> 0  OR @@ROWCOUNT <= 0
	BEGIN
		RETURN(1)	-- db error
	END
			
	RETURN(0)

GO

