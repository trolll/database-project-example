CREATE PROCEDURE [dbo].[UspGetPcDetail]
	 @pPcNo		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT RTRIM(a.[mNm]) AS mNm, a.[mClass], a.[mSex], a.[mHead], a.[mFace], a.[mBody], 
		   b.[mLevel], b.[mHpAdd], b.[mHp], b.[mMpAdd], b.[mMp], b.[mExp], b.[mStomach],
		   b.[mMapNo], b.[mPosX], b.[mPosY], b.[mPosZ],
		   a.[mHomeMapNo], a.[mHomePosX], a.[mHomePosY], a.[mHomePosZ],
		   c.[mGuildNo], RTRIM(c.[mNickNm]), c.[mGuildGrade], b.[mPkCnt], b.[mChaotic], 
		   ISNULL(d.[mMaster], 0) As mDiscipleNo, ISNULL(d.[mType], 0) As mDiscipleType, 
		   b.[mIsLetterLimit], b.mIsPreventItemDrop, b.[mFlag]
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
			ON(a.[mNo] = b.[mNo]) 
		LEFT JOIN dbo.TblGuildMember AS c 
			ON(a.[mNo] = c.[mPcNo])
		LEFT JOIN dbo.TblDiscipleMember AS d
			ON(a.[mNo] = d.[mDisciple])
	WHERE (a.[mNo]=@pPcNo) 
		AND ([mDelDate] IS NULL)

GO

