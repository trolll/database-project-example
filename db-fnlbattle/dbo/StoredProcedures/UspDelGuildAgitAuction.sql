CREATE Procedure [dbo].[UspDelGuildAgitAuction]
	 @pTerritory		INT			-- 康瘤锅龋
	,@pGuildAgitNo		INT OUTPUT		-- 辨靛酒瘤飘锅龋 [免仿]
	,@pOwnerGID		INT 			-- 家蜡辨靛
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	SET		@pGuildAgitNo = 0

	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		DECLARE @aIsSelling		CHAR
		SELECT @pGuildAgitNo = mGuildAgitNo, @aIsSelling = mIsSelling FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID
		IF (@aIsSelling = 0)
		BEGIN
			-- 概阿吝捞 酒丛 (坷幅酒丛)
			GOTO LABEL_END_LAST			 
		END

		BEGIN TRAN

		-- 概阿夸没 秒家
		UPDATE TblGuildAgit SET mIsSelling = 0 WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo AND mOwnerGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 

		-- 父距 版概沥焊啊 乐栏搁 陛咀 券阂
		IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo AND mCurBidGID <> 0 AND mCurBidMoney > 0)
		BEGIN
			DECLARE @aCurBidGID INT
			DECLARE @aCurBidMoney BIGINT
			SELECT @aCurBidGID = mCurBidGID, @aCurBidMoney = mCurBidMoney FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

			-- 辨靛酒瘤飘 涝蔓措陛 馆券
			UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney + @aCurBidMoney WHERE mGID = @aCurBidGID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END	 

			-- 辨靛酒瘤飘 涝蔓沥焊 檬扁拳			
			UPDATE TblGuildAgitAuction SET mCurBidGID = 0, mCurBidMoney = 0 WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END	 
		END
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		SET @aErrNo = 2			-- 2 : 秦寸 辨靛酒瘤飘啊 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END
	

LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

