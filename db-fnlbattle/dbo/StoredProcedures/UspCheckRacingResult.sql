CREATE PROCEDURE [dbo].[UspCheckRacingResult]
	 @pSerialNo		BIGINT
	,@pPlace		INT		OUTPUT
	,@pStage		INT		OUTPUT
	,@pResult		TINYINT	OUTPUT
	,@pDividend		FLOAT		OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	DECLARE	@aNID			INT
	DECLARE	@aWinnerNID		INT
	SET		@pPlace		= 0
	SET		@pStage		= 0
	SET		@pResult		= 0	-- 0:角菩 / 1:己傍 / 2:DRAW
	SET		@pDividend		= 0.0

	SELECT @pPlace = mPlace, @pStage = mStage, @aNID = mNID 	FROM TblRacingTicket WHERE mSerialNo = @pSerialNo
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @pResult	= 2	-- 坷幅牢 版快俊档 老窜 DRAW 肺 埃林
	 END
	ELSE
	 BEGIN
		SELECT @aWinnerNID = mWinnerNID, @pDividend = mDividend FROM TblRacingResult WHERE mPlace = @pPlace AND mStage = @pStage
		IF(1 <> @@ROWCOUNT)
		 BEGIN
			SET @pResult	= 2		-- 坷幅牢 版快俊档 版快俊档 老窜 DRAW肺 埃林
		 END
		ELSE
		 BEGIN
			IF (@aWinnerNID = @aNID)
			 BEGIN
				SET @pResult	= 1 	-- 铰磊乐澜
			 END
			ELSE IF (@aWinnerNID = 0)
			 BEGIN
				SET @pResult	= 2	-- DRAW 烙
			 END
		 END
	 END
	 
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

