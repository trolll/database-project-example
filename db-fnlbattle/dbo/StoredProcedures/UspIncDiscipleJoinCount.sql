CREATE PROCEDURE [dbo].[UspIncDiscipleJoinCount]
	 @pPcNo	INT
	,@pIncNum	INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo INT

	UPDATE dbo.TblPcState 
	SET 
		@pIncNum = 
			mDiscipleJoinCount = mDiscipleJoinCount + @pIncNum 
	WHERE mNo = @pPcNo
	
	SET @aErrNo = @@ERROR
	IF @aErrNo <> 0
	BEGIN
		RETURN(1) -- db error		
	END
	
	RETURN(0)

GO

