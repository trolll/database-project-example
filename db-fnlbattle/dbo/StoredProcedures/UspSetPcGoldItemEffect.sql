CREATE PROCEDURE [dbo].[UspSetPcGoldItemEffect]
	  @pPcNo		INT
	, @pItemType	INT
	, @pParmA		FLOAT
	, @pValidHour	INT	-- Hour
	, @pItemId		INT	
	, @pLeftTime	INT	OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF(@pPcNo < 2)
	BEGIN		
		RETURN(1)	-- NPC啊 蜡丰酒捞袍阑 荤侩且 荐 绝促.
	END

	DECLARE	@aEndDay	SMALLDATETIME
	SELECT @aEndDay = dbo.UfnGetEndDateByHour( GETDATE(), @pValidHour)
		, @pLeftTime= DATEDIFF(mi, GETDATE(), @aEndDay)

	IF EXISTS(	SELECT *
				FROM dbo.TblPcGoldItemEffect 
					WHERE mPcNo = @pPcNo 
						AND mItemType = @pItemType)
	BEGIN
		UPDATE dbo.TblPcGoldItemEffect
		SET 
			  mRegDate = GETDATE()
			, mParmA = @pParmA
			, mEndDate = @aEndDay
			, mItemNo = @pItemId
		WHERE mPcNo = @pPcNo 
				AND mItemType = @pItemType
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblPcGoldItemEffect (mPcNo, mItemType, mParmA, mEndDate, mItemNo) 
		VALUES(
			@pPcNo
			, @pItemType
			, @pParmA
			, @aEndDay
			, @pItemId)
	END
	
	IF(@@ERROR <>0 )
	BEGIN
		RETURN(2)	-- DB Err
	END
	
	RETURN(0)

GO

