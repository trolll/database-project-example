/******************************************************************************
**		Name: UspUpdateRecruitMemberHide
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩窍绰 纳腐磐甫 芭何惑怕肺 官槽促.
**
**		Auth: 沥柳龋
**		Date: 2009.08.11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateRecruitMemberHide]
	@pPcNo		INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	----------------------------------------------
	-- 辨靛 府捻福泼 脚没牢 沥焊 诀单捞飘
	----------------------------------------------
	UPDATE dbo.TblGuildRecruitMemberList
	SET	mIsHide = 1
	WHERE [mPcNo] = @pPcNo
	
	IF(0<>@@ERROR)
	BEGIN
		RETURN(1)
	END
		
	RETURN(0)

GO

