CREATE PROCEDURE [dbo].[UspDecDisciplePointByRateAll]
	 @pDecRate		INT	-- 皑家且 器牢飘 (%)
	,@pMinPoint		INT	-- 倾侩 啊瓷茄 弥家 器牢飘
	,@pExpLev1		INT	-- 饭骇喊 版氰摹 (饭骇1)
	,@pExpLev2		INT	-- 饭骇喊 版氰摹 (饭骇2)
	,@pExpLev3		INT	-- 饭骇喊 版氰摹 (饭骇3)
	,@pExpLev4		INT	-- 饭骇喊 版氰摹 (饭骇4)
	,@pExpLev5		INT	-- 饭骇喊 版氰摹 (饭骇5)
	,@pExpSumLev1	INT	-- 饭骇喊 版氰摹 钦拌 (饭骇1)
	,@pExpSumLev2	INT	-- 饭骇喊 版氰摹 钦拌 (饭骇2)
	,@pExpSumLev3	INT	-- 饭骇喊 版氰摹 钦拌 (饭骇3)
	,@pExpSumLev4	INT	-- 饭骇喊 版氰摹 钦拌 (饭骇4)
	,@pExpSumLev5	INT	-- 饭骇喊 版氰摹 钦拌 (饭骇5)
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aCurDate DATETIME
	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aDecPoint INT,
			@aMaxPoint INT	
						
	SELECT	@aErrNo = 0, @aCurDate = GETDATE(), @aRowCnt = 0,
			@aMaxPoint = 0


	UPDATE dbo.TblDisciple 
	SET 
		mCurPoint = 
			CASE
				WHEN (mCurPoint - T2.mDescPoint) < @pMinPoint THEN @pMinPoint
				ELSE
					 (mCurPoint - T2.mDescPoint)
			END									
		, mDecDate = @aCurDate 		
	FROM dbo.TblDisciple T1
		INNER JOIN (			
			SELECT 
				mMaster,
				mDescPoint = 
					CASE 					
						WHEN mCurPoint < @pExpSumLev1 
							THEN @pExpLev1 * @pDecRate / 100
						WHEN mCurPoint >= @pExpSumLev1 AND mCurPoint <= @pExpSumLev2 
							THEN @pExpLev2 * @pDecRate / 100
						WHEN mCurPoint >= @pExpSumLev2 AND mCurPoint <= @pExpSumLev3 
							THEN @pExpLev3 * @pDecRate / 100
						WHEN mCurPoint >= @pExpSumLev3 AND mCurPoint <= @pExpSumLev4 
							THEN @pExpLev4 * @pDecRate / 100
						WHEN mCurPoint > @pExpSumLev4 
							THEN @pExpLev5 * @pDecRate / 100
					END						
			FROM dbo.TblDisciple
			WHERE mCurPoint > @pMinPoint 
				AND DATEDIFF(HH, mDecDate, @aCurDate) >= 24				
		) T2
			ON	T1.mMaster = T2.mMaster
	WHERE T1.mCurPoint > @pMinPoint 
		AND DATEDIFF(HH, T1.mDecDate, @aCurDate) >= 24

	SELECT @aErrNo = @@ERROR
	
	IF @aErrNo <> 0
		RETURN (1)
	
	RETURN(0)

GO

