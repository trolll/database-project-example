/******************************************************************************
**		Name: UspCheckPcQuestCnt
**		Desc: 弥措 柳青 涅胶飘 荐甫 啊廉柯促.
**
**		Auth: 沥柳龋
**		Date: 2010-02-23
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckPcQuestCnt]
	 @pPcNo			INT 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
			count(*) AS mCnt
    FROM dbo.TblPcQuest 
    WHERE mPcNo = @pPcNo 
		AND mValue < 99

GO

