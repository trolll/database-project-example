CREATE Procedure [dbo].[UspRegGuildAgitAuction]
	 @pTerritory		INT			-- 康瘤锅龋
	,@pOwnerGID		INT			-- 家蜡辨靛
	,@pSellingMoney	BIGINT			-- 概阿陛咀
	,@pGuildAgitNo		INT OUTPUT		-- 辨靛酒瘤飘锅龋 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	SET		@pGuildAgitNo = 0

	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID AND mLeftMin > 0)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		-- 概阿可记 劝己拳 / 概阿陛咀 技泼

		SELECT @pGuildAgitNo = mGuildAgitNo FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID

		BEGIN TRAN
		
		UPDATE TblGuildAgit SET mIsSelling = 1, mSellingMoney = @pSellingMoney  WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID AND mLeftMin > 0
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		SET @aErrNo = 2		-- 2 : 辨靛啊 家蜡茄 酒瘤飘啊 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END


LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

