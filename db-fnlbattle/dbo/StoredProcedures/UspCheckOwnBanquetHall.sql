CREATE Procedure [dbo].[UspCheckOwnBanquetHall]	
	 @pTerritory		INT OUTPUT		-- 康瘤锅龋 [涝仿/免仿]
	,@pOwnerPcNo		INT				-- 荤侩磊锅龋
	,@pIsOwnBanquetHall	TINYINT OUTPUT	-- 辨靛酒瘤飘 家蜡咯何 [免仿]
	,@pBanquetHallType	INT OUTPUT		-- 楷雀厘鸥涝 [免仿]
	,@pBanquetHallNo	INT OUTPUT		-- 楷雀厘锅龋 [免仿]
	,@pLeftMin		INT OUTPUT			-- 父丰盒 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT,
			@aRowCnt	INT
			
	SELECT	@aErrNo = 0,
			@aRowCnt = 0

	SET		@pIsOwnBanquetHall = 0
	SET		@pBanquetHallType = 0
	SET		@pBanquetHallNo = 0
	SET		@pLeftMin = 0

	IF (@pOwnerPcNo = 0)
	BEGIN
		RETURN(0)
	END
	
	IF (@pTerritory <= 0)	-- 角力 康瘤锅龋绰 1何磐烙	
	BEGIN
		-- 葛电 康瘤 八祸
		SELECT 
			@pTerritory = mTerritory, 
			@pIsOwnBanquetHall = 1, 
			@pBanquetHallType = mBanquetHallType, 
			@pBanquetHallNo = mBanquetHallNo, 
			@pLeftMin = mLeftMin 
		FROM dbo.TblBanquetHall WITH (NOLOCK) 
		WHERE mOwnerPcNo = @pOwnerPcNo
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo<> 0
		BEGIN
			RETURN(1)		-- 1: DB 郴何利牢 坷幅
		END

		RETURN(0)		
	END
	
	
	SELECT 
		@pTerritory = mTerritory, 
		@pIsOwnBanquetHall = 1, 
		@pBanquetHallType = mBanquetHallType, 
		@pBanquetHallNo = mBanquetHallNo, 
		@pLeftMin = mLeftMin 
	FROM dbo.TblBanquetHall WITH (NOLOCK) 
	WHERE mTerritory = @pTerritory 
		AND mOwnerPcNo = @pOwnerPcNo
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo<> 0
	BEGIN
		RETURN(1)		-- 1: DB 郴何利牢 坷幅
	END

	RETURN(0)

GO

