/******************************************************************************
**		Name: UspGetChatFilterRegistMe
**		Desc: 
**		Test:			
**		Auth: 巢己葛
**		Date: 2014/03/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetChatFilterRegistMe]
	 @pPcNo	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		b.mOwnerPcNo	
	FROM dbo.TblPc AS a
		INNER JOIN dbo.TblPcChatFilter AS b
			ON a.mNm = b.mChatFilterPcNm
					AND a.mNo = b.mChatFilterPcNo
	WHERE a.mNo = @pPcNo
			AND a.mDelDate IS NULL

GO

