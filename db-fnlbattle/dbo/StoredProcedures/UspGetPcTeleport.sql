CREATE PROCEDURE [dbo].[UspGetPcTeleport]
	 @pPcNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		mNo, 
		RTRIM(mName), 
		mMapNo, 
		mPosX, 
		mPosY, 
		mPosZ
	FROM dbo.TblPcTeleport
	WHERE mPcNo = @pPcNo
	ORDER BY mName ASC

GO

