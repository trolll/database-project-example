CREATE PROCEDURE [dbo].[UspGetLetterInfo]
	 @pSerial		BIGINT				-- 祈瘤瘤 锅龋.
	,@pFromPcNm		CHAR(12)	OUTPUT
	,@pTitle		CHAR(30)	OUTPUT
	,@pToPcNm		CHAR(12)	OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0	
	SELECT 
		@pFromPcNm=[mFromPcNm], 
		@pTitle=[mTitle], 
		@pToPcNm=[mToPcNm] 
	FROM dbo.TblLetter 
	WHERE [mSerialNo]=@pSerial
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END	 
	 END
	 
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

