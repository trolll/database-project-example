CREATE PROCEDURE [dbo].[UspGetRacingTicketInfo]
	 @pSerialNo		BIGINT
	,@pPlace		INT		OUTPUT
	,@pStage		INT		OUTPUT
	,@pNID			INT		OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	SET		@pPlace		= 0
	SET		@pStage		= 0
	SET		@pNID			= 0	-- 0 : NID狼 檬扁蔼 (绝澜)

	SELECT @pPlace = mPlace, @pStage = mStage, @pNID = mNID FROM dbo.TblRacingTicket WHERE mSerialNo = @pSerialNo

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

