/******************************************************************************  
**  File: 
**  Name: UspDelPcSkillPack  
**  Desc: PC狼 胶懦蒲 窍唱甫 昏力茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDelPcSkillPack]
	@pPcNo		INT,
	@pSPID	INT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, 滚妨柳 酒捞袍

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	DELETE FROM dbo.TblPcSkillPackInventory
	WHERE mPcNo = @pPcNo AND mSPID = @pSPID
	IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- SQL Error
		END

	RETURN @aErrNo

GO

