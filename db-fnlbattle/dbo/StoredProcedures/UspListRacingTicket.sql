CREATE PROCEDURE [dbo].[UspListRacingTicket]
	 @pPcNo		INT
AS 
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT	 a.mSerialNo
			,a.mPlace
			,a.mStage
			,a.mNID
	FROM dbo.TblRacingTicket  AS a 
		INNER JOIN dbo.TblPcInventory    AS b 
		ON a.mSerialNo = b.mSerialNo
	WHERE b.mPcNo = @pPcNo		
			AND b.mEndDate > GETDATE() 

	SET NOCOUNT OFF

GO

