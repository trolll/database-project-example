CREATE Procedure [dbo].[UspDelGuildAgitTicket]
	 @pTicketSerialNo	BIGINT			-- 萍南矫府倔锅龋
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0


	-- 楷雀厘 涝厘萍南 昏力
	IF EXISTS(SELECT mTicketSerialNo FROM dbo.TblGuildAgitTicket WHERE mTicketSerialNo = @pTicketSerialNo)
	BEGIN
		BEGIN TRAN

		DELETE dbo.TblGuildAgitTicket WHERE mTicketSerialNo = @pTicketSerialNo
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END
	ELSE
	BEGIN
		SET @aErrNo = 2			-- 2 : 萍南捞 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END


LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

