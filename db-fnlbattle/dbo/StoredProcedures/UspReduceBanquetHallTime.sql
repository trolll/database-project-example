CREATE Procedure [dbo].[UspReduceBanquetHallTime]	-- 漂沥 楷雀厘狼 矫埃 皑家矫糯 (秦寸 楷雀厘 沥焊 绝栏搁 积己)
	 @pTerritory		INT			-- 康瘤锅龋
	,@pBanquetHallType	INT			-- 楷雀厘鸥涝 (0::家.楷雀厘/1:措.楷雀厘)
	,@pBanquetHallNo	INT			-- 楷雀厘锅龋
	,@pReduceMin		INT OUTPUT		-- 皑家矫懦 盒 [涝仿/免仿]
	,@pOwnerPcNo		INT OUTPUT		-- 家蜡荤侩磊 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF EXISTS(SELECT mOwnerPcNo FROM TblBanquetHall WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mBanquetHallType = @pBanquetHallType AND mBanquetHallNo = @pBanquetHallNo)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		BEGIN TRAN

		-- 父丰盒 皑家
		UPDATE TblBanquetHall SET mLeftMin = mLeftMin - ISNULL(@pReduceMin, 0) WHERE mTerritory = @pTerritory AND mBanquetHallType = @pBanquetHallType AND mBanquetHallNo = @pBanquetHallNo
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 

		-- 楷雀厘 沥焊甫 掘绢咳
		SELECT @pReduceMin = mLeftMin, @pOwnerPcNo = mOwnerPcNo FROM TblBanquetHall WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mBanquetHallType = @pBanquetHallType AND mBanquetHallNo = @pBanquetHallNo
		IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜 (火涝)

		SET	@pReduceMin=0
		SET	@pOwnerPcNo=0

		BEGIN TRAN

		INSERT TblBanquetHall (mTerritory, mBanquetHallType, mBanquetHallNo) VALUES(@pTerritory, @pBanquetHallType, @pBanquetHallNo)
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END


LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

