--------------------------------
-- 按眉疙 : WspGetDelCharInfoByCharacter
-- 汲疙 : 某腐磐疙栏肺 昏力等 某腐磐 沥焊甫 啊廉柯促
-- 累己磊 : 捞快籍
-- 累己老 : 2006.04.02
-- 荐沥磊 : 捞快籍
-- 荐沥老 : 2006.04.08
--------------------------------
CREATE         PROCEDURE [dbo].[WspGetDelCharInfoByCharacter]	
	@mNm CHAR(12) -- 某腐磐疙
AS
	SET NOCOUNT ON
	
	SELECT
		d.mPcNm,--某腐疙
		p.mNm, -- 昏力登辑 函版等 某腐磐疙( ,+PcNo 技泼)
		u.mUserId, -- 拌沥疙
		u.mUserNo, --拌沥锅龋
		p.mClass, -- 努贰胶
		p.mDelDate, -- 某腐磐 昏力老
		-- 辑滚疙 甸绢啊具窃		
		p.mRegDate, -- 某腐磐 积己老
		s.mLevel, -- 昏力 寸矫 饭骇
		s.mExp, -- 版氰摹
		s.mPosX, s.mPosY, s.mPosZ, -- 困摹
		s.mHp, -- HP
		s.mMp, -- MP
		s.mChaotic, -- 己氢
		g.mGuildNm, -- 辨靛疙		
		s.mIp, -- 昏力 寸矫 辑滚 立加 IP
		p.mNo
	FROM
		[dbo].[TblPcDeleted] AS d WITH (NOLOCK)
	INNER JOIN
		[dbo].[TblPc] AS p WITH(NOLOCK)
	ON
		d.mPcNo = p.mNo
	INNER JOIN
		[FNLAccount].[dbo].[TblUser] AS u --WITH (NOLOCK)
		--OPENQUERY(A55077, 'SELECT * FROM [FNLAccount].[dbo].[TblUser] WITH (NOLOCK)') AS u
	ON
		p.mOwner = u.mUserNo
	INNER JOIN		
		[dbo].[TblPcState] AS s WITH (NOLOCK)
	ON
		p.mNo = s.mNo
	Left OUTER JOIN
		[dbo].[TblGuild] As g WITH (NOLOCK)
	ON
		g.mGuildNo = (SELECT mGuildNo From [dbo].[TblGuildMember]
			      WHERE mPcNo = p.mNo)
	WHERE		
		d.mPcNm = @mNm

GO

