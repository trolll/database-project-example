-----------------------------
-- UspSetLimitedResource
-----------------------------
CREATE PROCEDURE [dbo].[UspSetLimitedResource]
	@pResourceType	INT
AS
	SET NOCOUNT ON
	
	DECLARE @aRemainerCnt	INT,
			@aRandomVal		FLOAT,
			@aCurrRandomVal FLOAT	
	
	SELECT	@aRemainerCnt = 0,
			@aRandomVal = 0,
			@aCurrRandomVal = 0
		
	SELECT 
		@aRemainerCnt = mRemainerCnt,
		@aRandomVal = mRandomVal
	FROM dbo.TblLimitedResource
	WHERE mResourceType = @pResourceType
	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0  
	BEGIN
		RETURN (1)	-- db error
	END 

	SELECT @aCurrRandomVal = RAND() * 100		-- 100% 犬伏
	IF CONVERT(BIGINT,(@aCurrRandomVal*10000)) > CONVERT(BIGINT, (@aRandomVal*10000))
	BEGIN
		RETURN(2)	-- 犬伏俊 固 器窃.
	END  
		
	IF @aRemainerCnt <= 0
	BEGIN
		RETURN(3)	-- 儡咯 肮荐啊 绝促.
	END

	UPDATE dbo.TblLimitedResource
	SET 
		@aRemainerCnt = mRemainerCnt = mRemainerCnt - 1,
		mUptDate = GETDATE()
	WHERE mResourceType = @pResourceType
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0  
	BEGIN
		RETURN (1)	-- db error
	END 		

	IF @aRemainerCnt < 0 
	BEGIN
		RETURN(3)	-- 儡咯 肮荐啊 绝促.
	END
	
	RETURN(0)	-- 惯青

GO

