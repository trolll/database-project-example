CREATE PROCEDURE [dbo].[UspSetPcLostExp]
	  @pPcNo		INT
	, @pExp		INT
AS
	SET NOCOUNT ON		
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			

	-- 颊角 版氰摹啊 1焊促 累阑荐 绝促.
	IF(@pExp < 1)
	BEGIN
		RETURN(1)
	END

	-- PcNo 绰 2锅 何磐 矫累茄促.
	IF(@pPcNo < 2)
	BEGIN
		RETURN(2)
	END

	IF EXISTS(	SELECT * 
				FROM dbo.TblPc 
				WHERE mNo = @pPcNo 
					AND mDelDate IS NULL)
	BEGIN

		UPDATE dbo.TblPcState 
		SET mLostExp = @pExp 
		WHERE mNo = @pPcNo

		IF(@@ERROR <> 0)
		BEGIN
			RETURN(3)	-- DB Error
		END
		
		RETURN(0)
 	END

	RETURN(4)	-- non character

GO

