CREATE PROCEDURE [dbo].[UspUpdateMpHp]
	 @pPcNo			INT
	,@pHp			SMALLINT
	,@pMp			SMALLINT
AS
	SET NOCOUNT ON			
	
	DECLARE	@aErrNo		INT,
			@aRowCnt	INT
			
	SELECT 	@aErrNo = 0	, 	@aRowCnt = 0	

	UPDATE dbo.TblPcState
	SET
		mHpAdd = mHpAdd + @pHp,
		mMpAdd = mMpAdd + @pMp
	WHERE mNo = @pPcNo
	SELECT 	@aErrNo = @@ERROR, 	@aRowCnt = @@ROWCOUNT

	IF @aErrNo <> 0
	BEGIN
		RETURN(1)	
	END
	
	IF @aRowCnt <= 0
	BEGIN
		RETURN(2)
	END 

	RETURN(0)

GO

