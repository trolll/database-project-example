CREATE Procedure [dbo].[UspGetGuildAgitInfo]			-- 漂沥 辨靛啊 漂沥 康瘤俊 酒瘤飘甫 家蜡窍绊 乐栏搁 秦寸 沥焊 馆券
	 @pOwnerGID		INT				-- 辨靛锅龋
	,@pTerritory		INT OUTPUT			-- 康瘤锅龋 [涝仿/免仿]
	,@pGuildAgitNo		INT OUTPUT			-- 辨靛酒瘤飘锅龋 [免仿]
	,@pGuildAgitName	NVARCHAR(50) OUTPUT	-- 辨靛酒瘤飘捞抚 [免仿]
	,@pIsSelling		TINYINT OUTPUT		-- 魄概咯何
	,@pLeftMin		INT OUTPUT			-- 父丰盒 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	SET		@pGuildAgitNo = 0
	SET		@pGuildAgitName = 0
	SET		@pIsSelling = 0
	SET		@pLeftMin = 0
	
	IF (@pTerritory <= 0)	-- 角力 康瘤锅龋绰 1何磐烙
	BEGIN
		-- 葛电 康瘤 八祸
		IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH (NOLOCK) WHERE mOwnerGID = @pOwnerGID)
		BEGIN
			SELECT @pTerritory = mTerritory, @pGuildAgitNo = mGuildAgitNo, @pGuildAgitName = mGuildAgitName, @pIsSelling = mIsSelling, @pLeftMin = mLeftMin FROM TblGuildAgit WITH (NOLOCK) WHERE mOwnerGID = @pOwnerGID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1: DB 郴何利牢 坷幅
			END	 
		END
		ELSE
		BEGIN
			SET	@aErrNo = 2		-- 2 : 辨靛啊 家蜡茄 酒瘤飘啊 绝澜
		END
	END
	ELSE
	BEGIN
		-- 漂沥 康瘤 八祸
		IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID)
		BEGIN
			SELECT @pTerritory = mTerritory, @pGuildAgitNo = mGuildAgitNo, @pGuildAgitName = mGuildAgitName, @pIsSelling = mIsSelling, @pLeftMin = mLeftMin FROM TblGuildAgit WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
			END	 
		END
		ELSE
		BEGIN
			SET	@aErrNo = 2		-- 2 : 辨靛啊 家蜡茄 酒瘤飘啊 绝澜
		END
	END
	
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

