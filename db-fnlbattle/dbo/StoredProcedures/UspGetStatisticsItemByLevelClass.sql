/******************************************************************************  
**  Name: UspGetStatisticsItemByLevelClass  
**  Desc: 酒捞袍 烹拌 抛捞喉 肺靛
**  
**  Auth:   
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  辫碍龋      mItemStatus,  mCnsmRegFee, mCnsmBuyFee 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetStatisticsItemByLevelClass]  
AS  
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
  
	SELECT   
		mItemNo  
		, mMerchantCreate  
		, mMerchantDelete  
		, mReinforceCreate  
		, mReinforceDelete  
		, mCraftingCreate  
		, mCraftingDelete  
		, mPcUseDelete  
		, mNpcUseDelete  
		, mNpcCreate  
		, mMonsterDrop  
		, mGSExchangeCreate   
		, mGSExchangeDelete  
		, mLevel  
		, mClass    
		, mItemStatus
		, mCnsmRegFee
		, mCnsmBuyFee
	FROM   
		dbo.TblStatisticsItemByLevelClass;

GO

