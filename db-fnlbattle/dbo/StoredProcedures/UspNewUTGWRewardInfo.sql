/******************************************************************************  
**  Name: UspNewUTGWRewardInfo  
**  Desc: 烹钦辨靛傈 辑滚焊惑 沥焊甫 货肺 积己茄促.
**  
**  Auth: 沥柳宽  
**  Date: 2013-05-13  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  
**  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspNewUTGWRewardInfo]
	@pRound					INT
	, @pRanking				INT  
	, @pRewardSvrNo			SMALLINT
	, @pAdvantage			INT
	, @pIsTournament		BIT  
	, @pChaosBattleSvrNo	SMALLINT
AS
	SET NOCOUNT ON  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
   
	INSERT INTO dbo.TblUnitedGuildWarRewardInfo
	(
		[mRewardDate], [mRound], [mRanking], [mRewardSvrNo], [mAdvantage], [mIsTournament], [mChaosBattleSvrNo]
	)
	VALUES  
	(
		GETDATE(), @pRound, @pRanking, @pRewardSvrNo, @pAdvantage, @pIsTournament, @pChaosBattleSvrNo
	)
   
	IF(0 <> @@ERROR)  
	BEGIN  
		RETURN(1)	-- DB ERROR
	END  
    
	RETURN(0)

GO

