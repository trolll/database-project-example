--------------------------------
-- 按眉疙 : [WspGetGuildAsset]
-- 汲疙 : 辨靛疙栏肺 泅犁 辨靛啊 家蜡茄 康瘤狼 儡绊甫 掘绢柯促.
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.07.12
-- 荐沥磊 : 
-- 荐沥老 : 
-- 荐沥郴侩 : 
--------------------------------
CREATE       PROCEDURE [dbo].[WspGetGuildAsset]
	@mGuildNm VARCHAR(12) -- 辨靛 捞抚
AS
	SET NOCOUNT ON

	SELECT
		p.mAsset
	FROM
		[dbo].[TblCastleTowerStone] AS p		WITH ( NOLOCK )
	INNER	JOIN
		[dbo].[TblGuild]	AS g				WITH ( NOLOCK )
	ON	
		g.mGuildNo = p.mGuildNo
	WHERE
		g.mGuildNm = @mGuildNm

	SET NOCOUNT OFF

GO

