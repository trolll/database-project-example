/******************************************************************************
**		Name: WspListConsignmentItemByPc
**		Desc: 
**
**		Auth: JUDY
**		Date: 2010-12-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspListConsignmentItemByPc]
	@mCharNm VARCHAR(12)	-- 某腐磐疙
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	SET ROWCOUNT 1000;		-- 弥措 啊廉棵 荐 乐绰 酒捞袍 荐甫 力茄 

	SELECT 
		 T1.mNm		-- 家瘤磊
		,T1.mNo		-- 某腐磐 锅龋
		,T4.IName		-- 酒捞袍疙
		,DATEDIFF(DAY, GETDATE(), T3.mEndDate) AS mEndDate	-- 蜡瓤扁埃
		,T3.mSerialNo
		,T2.mCurCnt
		,T2.mItemNo
		,0 mIsSeizure
		,T3.mRegDate	-- 嚼垫老磊
		,T3.mPracticalPeriod
		,T3.mBindingType
		,T3.mRestoreCnt
		, T2.mCnsmNo
		, mRegCnt
		, mCurCnt
		, mPrice
		, mTradeEndDate		
	FROM dbo.TblPc T1 
		INNER JOIN dbo.TblConsignment  T2 
			ON T1.mNo = T2.mPcNo
		INNER JOIN dbo.TblConsignmentItem  T3
			ON T2.mCnsmNo = T3.mCnsmNo					
		LEFT OUTER JOIN FNLParm.dbo.DT_Item T4
			ON T2.mItemNo = T4.IID	
	WHERE T1.mNm = @mCharNm
		AND T1.mDelDate IS NULL

GO

