/******************************************************************************  
**  Name: UspSetStatisticsPShopExchange  
**  Desc: 俺牢惑痢 抛捞喉俊 烹拌郴开阑 涝仿  
**  
**  Auth: 沥备柳  
**  Date: 09.05.07  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:         Author:    Description:  
**  --------      --------   ---------------------------------------  
**   2010.12.07.  辫碍龋      mItemStatus,  mTradeType 眠啊 
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetStatisticsPShopExchange]  
   @pItemNo    INT  
 , @pExchangeCntBuy  BIGINT  
 , @pExchangeCntSell  BIGINT  
 , @pTotalPriceBuy  BIGINT  
 , @pTotalPriceSell  BIGINT  
 , @pMinPriceBuy   BIGINT  
 , @pMinPriceSell  BIGINT  
 , @pMaxPriceBuy   BIGINT  
 , @pMaxPriceSell  BIGINT  
 , @pItemStatus SMALLINT
 , @pTradeType SMALLINT
AS  
 SET NOCOUNT ON  ;
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
   
 DECLARE  @aErrNo  INT  ;
 SET @aErrNo = 0  ;
  
 INSERT INTO dbo.TblStatisticsPShopExchange  
  ([mItemNo], [mBuyCount], [mSellCount], [mBuyTotalPrice], [mSellTotalPrice],   
  [mBuyMinPrice], [mSellMinPrice], [mBuyMaxPrice], [mSellMaxPrice]
  , mItemStatus, mTradeType)  
 VALUES  
  (@pItemNo, @pExchangeCntBuy, @pExchangeCntSell, @pTotalPriceBuy, @pTotalPriceSell,   
  @pMinPriceBuy, @pMinPriceSell, @pMaxPriceBuy, @pMaxPriceSell
  , @pItemStatus, @pTradeType);
         
 IF @@ERROR <> 0  
 BEGIN
  SET @aErrNo = 2  ;
 END
                
 RETURN(@aErrNo)  ;

GO

