CREATE PROCEDURE [dbo].[UspGetStatisticsItemByCase]
AS
	SET NOCOUNT ON

	SELECT 
		mItemNo
		, mMerchantCreate
		, mMerchantDelete
		, mReinforceCreate
		, mReinforceDelete
		, mCraftingCreate
		, mCraftingDelete
		, mPcUseDelete
		, mNpcUseDelete
		, mNpcCreate
		, mMonsterDrop
		, mGSExchangeCreate	
		, mGSExchangeDelete
	FROM 
		dbo.TblStatisticsItemByCase;

GO

