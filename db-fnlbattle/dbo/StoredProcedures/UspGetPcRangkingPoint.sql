/******************************************************************************
**		File: 
**		Name: UspGetPcRangkingPoint
**		Desc: 漂沥 PC狼 珐欧 单捞磐甫 掘绰促.
**
**		Auth: 辫 堡挤
**		Date: 2009-03-23
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcRangkingPoint]
	  @pPcNo	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		  mContribution
		, mGuardian
		, mTeleportTower
		, mPVP
	FROM	dbo.TblPcRankingPoint
	WHERE	mPcNo = @pPcNo

GO

