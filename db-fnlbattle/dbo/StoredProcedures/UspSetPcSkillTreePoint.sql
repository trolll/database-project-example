/******************************************************************************  
**  File: 
**  Name: UspSetPcSkillTreePoint  
**  Desc: PC狼 胶懦飘府 器牢飘 技泼
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.28 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetPcSkillTreePoint]
	 @pPcNo		INT,
	@pPoint	SMALLINT 
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	update TblPcState
	set mSkillTreePoint = @pPoint
	where mNo = @pPcNo

	if @@error <> 0
	begin
		return 1
	end

	return 0

GO

