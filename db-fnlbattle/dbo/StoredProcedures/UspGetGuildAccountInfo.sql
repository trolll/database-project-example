CREATE Procedure [dbo].[UspGetGuildAccountInfo]		-- 辨靛拌谅俊 捣 免陛
	 @pGID			INT			-- 家蜡辨靛
	,@pGuildMoney		BIGINT OUTPUT	-- 辨靛磊陛 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF EXISTS(SELECT mGID FROM TblGuildAccount WITH(NOLOCK) WHERE mGID = @pGID)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		SELECT @pGuildMoney = mGuildMoney FROM TblGuildAccount WITH(NOLOCK) WHERE mGID = @pGID
		GOTO LABEL_END_LAST
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		BEGIN TRAN

		INSERT TblGuildAccount (mGID, mGuildMoney) VALUES (@pGID, 0)
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END

		SET @pGuildMoney = 0
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

