CREATE PROCEDURE [dbo].[UspListCastleStower]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.

	SELECT a.[mPlace], a.[mGuildNo], a.[mTaxBuy], a.[mTaxBuyMax], a.[mTaxHunt], a.[mTaxHuntMax], 
		   a.[mTaxGamble], a.[mTaxGambleMax], a.[mAsset], a.[mChgDate], a.[mAssetBuy], a.[mAssetHunt],
		   a.[mAssetGamble]
		FROM TblCastleTowerStone AS a

	SET NOCOUNT OFF

GO

