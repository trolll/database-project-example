CREATE PROCEDURE [dbo].[WspReplyQuickSupplication]
	  @pQSID		BIGINT
	, @pPcNo		INT
	, @pCate		TINYINT
	, @pReply		VARCHAR(1000)
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF NOT EXISTS ( SELECT mQSID 
					FROM dbo.TblQuickSupplicationState 
					WHERE mQSID = @pQSID 
						AND mPcNo = @pPcNo 
						AND mStatus = 0
						AND mCategory = @pCate  )
	 BEGIN
		-- 翠函捞 崔府瘤 臼篮 龙巩捞 粮犁 窍瘤 臼绰促.
		RETURN(1)
	 END

	DECLARE	@aErrNo	INT
	SET @aErrNo = 0	

	BEGIN TRAN
	
		INSERT INTO dbo.TblQuickSupplicationReply (mQSID, mReply) 
		VALUES (
			@pQSID
			, @pReply
		)
		
		IF @@ERROR <> 0
		BEGIN		
			SET @aErrNo = 2
			GOTO T_END
		END

		UPDATE dbo.TblQuickSupplicationState 
		SET mStatus = 1 
		WHERE mQSID = @pQSID 
			AND mPcNo = @pPcNo 
			AND mCategory = @pCate 
			AND mStatus = 0 
			
		IF @@ERROR <> 0
		BEGIN
			SET @aErrNo = 3
			GOTO T_END
		END
		
	T_END:
		IF(0 = @aErrNo)
		 BEGIN 
			COMMIT TRAN
		 END
		ELSE
		 BEGIN
			ROLLBACK TRAN
		 END

	RETURN(@aErrNo)

GO

