CREATE Procedure [dbo].[UspGetBanquetHallList]		-- 漂沥 康瘤狼 傈眉 楷雀厘 府胶飘 掘扁
	 @pTerritory		INT			-- 康瘤锅龋
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	-- 楷雀厘 府胶飘 掘扁
	SELECT 
		 mTerritory
		,mBanquetHallType
		,mBanquetHallNo
		,mOwnerPcNo
		,ISNULL((CASE mOwnerPcNo
			WHEN 0 THEN N'<NONE>'
			ELSE (SELECT mNm FROM TblPc WITH (NOLOCK) WHERE mNo = mOwnerPcNo)
			END), N'<FAIL>') As mOwnerPcName
		,mLeftMin 
	FROM TblBanquetHall
	WHERE mTerritory = @pTerritory

	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo = 1	-- 1: DB 郴何利牢 坷幅
	END
	
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

