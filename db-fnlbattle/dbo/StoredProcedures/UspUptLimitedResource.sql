CREATE PROCEDURE [dbo].[UspUptLimitedResource]
	@pResourceType	INT,
	@pRandomVal		FLOAT
AS
	SET NOCOUNT ON
	
	UPDATE dbo.TblLimitedResource
	SET mRandomVal = @pRandomVal
	WHERE mResourceType = @pResourceType
	
	IF @@ERROR <> 0  OR @@ROWCOUNT <= 0
	BEGIN
		RETURN(1)	-- db error
	END

	RETURN(0)

GO

