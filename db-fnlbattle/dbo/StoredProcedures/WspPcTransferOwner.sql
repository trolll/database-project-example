CREATE PROCEDURE [dbo].[WspPcTransferOwner]
	@pPcNo	INT
	, @pPcNm	CHAR(12)
	, @pUserID	VARCHAR(20)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
	
	DECLARE	@aErrNo		INT,
			@aRowCnt	INT,
			@aOwnerNo	INT	
			
	SELECT @aOwnerNo = 0, @aErrNo = 0, @aRowCnt = 0
		
	SELECT
		TOP 1
		@aOwnerNo = mUserNo
	FROM FNLAccount.dbo.TblUser
	WHERE mUserID = @pUserID
		AND mDelDate = '1900-01-01' 						
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(1)	-- not exists account
	END 
	
	IF (	SELECT COUNT(*)
			FROM dbo.TblPc
			WHERE mOwner = @aOwnerNo
				AND mDelDate IS NULL ) >= 3
	BEGIN
		RETURN(2)	-- 家蜡 拌沥 沥焊 檬苞 
	END 
		
	UPDATE dbo.TblPc
	SET 
		mOwner = @aOwnerNo
	WHERE mNo = @pPcNo
		AND mDelDate IS NULL
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(3)	-- db update error
	END 	
	
	RETURN(0)

GO

