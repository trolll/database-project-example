CREATE    PROCEDURE [dbo].[WspDeleteStoreItem]
	@mNo INT, 
	@mCnt BIGINT -- 荐樊
AS
	SET NOCOUNT ON

	SET XACT_ABORT ON
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRANSACTION

	IF (SELECT mCnt FROM [dbo].[TblPcStore] WHERE mUserNo = @mNo) - @mCnt < 1

		BEGIN
			-- 昏力
			DELETE FROM
				[dbo].[TblPcStore]
			WHERE
				mUserNo = @mNo
		END

	ELSE

		BEGIN
			-- 荐樊 皑家
			UPDATE
				[dbo].[TblPcStore]
			SET
				mCnt = mCnt - @mCnt
			WHERE
				mUserNo = @mNo
		END

	IF @@ERROR = 0
		BEGIN
			COMMIT TRANSACTION
			RETURN 0
		END
	ELSE
		BEGIN
			ROLLBACK TRANSACTION
			RETURN 1
		END

GO

