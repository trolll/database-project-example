CREATE   PROCEDURE [dbo].[WspUpdateCharExp]	
	@mExp BIGINT, -- 版氰摹
	@mNm CHAR(12) -- 某腐磐疙
AS
	SET NOCOUNT ON

	UPDATE dbo.TblPcState
	SET
		mExp = @mExp 
	WHERE mNo = (SELECT mNo From [dbo].[TblPc] WHERE mNm = @mNm)			

	IF @@ERROR <> 0
		RETURN(1)
		
	RETURN(0)

GO

