CREATE Procedure [dbo].[UspGetBanquetHallInfo]		-- 漂沥 荤侩磊啊 漂沥 康瘤俊 楷雀厘阑 家蜡窍绊 乐栏搁 秦寸 沥焊 馆券
	 @pOwnerPcNo		INT			-- 荤侩磊锅龋
	,@pTerritory		INT OUTPUT		-- 康瘤锅龋 [涝仿/免仿]
	,@pBanquetHallType	INT OUTPUT		-- 楷雀厘鸥涝 (0:家.楷雀厘 / 1:措.楷雀厘) [免仿]
	,@pBanquetHallNo	INT OUTPUT		-- 楷雀厘锅龋 [免仿]
	,@pLeftMin		INT OUTPUT		-- 父丰盒 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF (@pTerritory <= 0)	-- 角力 康瘤锅龋绰 1何磐烙
	BEGIN
		-- 葛电 康瘤 八祸
		IF EXISTS(SELECT mOwnerPcNo FROM TblBanquetHall WITH (NOLOCK) WHERE mOwnerPcNo = @pOwnerPcNo)
		BEGIN
			SELECT @pTerritory = mTerritory, @pBanquetHallType = mBanquetHallType, @pBanquetHallNo = mBanquetHallNo, @pLeftMin = mLeftMin FROM TblBanquetHall WITH (NOLOCK) WHERE mOwnerPcNo = @pOwnerPcNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1: DB 郴何利牢 坷幅
			END	 
		END
		ELSE
		BEGIN
			SET	@aErrNo = 2		-- 2 : 荤侩磊啊 家蜡茄 楷雀厘捞 绝澜
		END
	END
	ELSE
	BEGIN
		-- 漂沥 康瘤 八祸
		IF EXISTS(SELECT mOwnerPcNo FROM TblBanquetHall WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerPcNo = @pOwnerPcNo)
		BEGIN
			SELECT @pTerritory = mTerritory, @pBanquetHallType = mBanquetHallType, @pBanquetHallNo = mBanquetHallNo, @pLeftMin = mLeftMin FROM TblBanquetHall WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerPcNo = @pOwnerPcNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
			END	 
		END
		ELSE
		BEGIN
			SET	@aErrNo = 2		-- 2 : 荤侩磊啊 家蜡茄 楷雀厘捞 绝澜
		END
	END
	
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

