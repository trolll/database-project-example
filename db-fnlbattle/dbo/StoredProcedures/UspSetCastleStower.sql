CREATE PROCEDURE [dbo].[UspSetCastleStower]
	 @pPlaceNo	INT
	,@pGuildNo	INT
	,@pIsNew	BIT	-- 曼捞搁 林牢捞 函版灯促.
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	UPDATE dbo.TblCastleTowerStone 
	SET 
		[mChgDate]=CASE @pIsNew WHEN 0 THEN [mChgDate] ELSE GETDATE() END,
		[mGuildNo]=@pGuildNo, 
		[mAssetBuy]=0, 
		[mAssetHunt]=0, 
		[mAssetGamble]=0
	WHERE ([mPlace]=@pPlaceNo)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		SET @aErrNo	= 1
	END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

