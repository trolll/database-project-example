/******************************************************************************
**		Name: WspGetCharInfo
**		Desc: 墨坷胶 硅撇 辑滚 某腐磐 沥焊 炼雀
**
**		Auth: 辫籍玫
**		Date: 20090429
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE	PROCEDURE [dbo].[WspGetCharInfo]	
	@mNm CHAR(12) -- 某腐磐疙
	,@mFieldSvrNo SMALLINT -- 鞘靛 辑滚 锅龋
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		c.mNm, -- 某腐磐疙
		u.mUserId, -- 拌沥疙
		p.mClass, -- 努贰胶		
		(
			SELECT 
				COUNT(*) 
			FROM [FNLAccount].[dbo].[TblUserBlock] b
			WHERE mUserNo = p.mOwner AND ( mCertify > GETDATE()  OR mChat > 0 OR mBoard > GETDATE())  ) AS mBlock, 		
		(
			SELECT 
				COUNT(*) 
			FROM [FNLAccount].[dbo].[TblUserBlockHistory] bh			
			WHERE mUserNo = p.mOwner) AS mBlockHistory, 
		c.mRegDate, -- 某腐磐 积己老
		s.mLevel, -- 饭骇
		s.mExp, -- 版氰摹
		s.mPosX, s.mPosY, s.mPosZ, -- 困摹
		s.mHp, -- HP
		s.mMp, -- MP
		s.mChaotic, -- 己氢
		g.mGuildNm, -- 辨靛疙
		s.mLoginTm, -- 弥辟 辑滚 立加 老矫
		s.mLogoutTm, -- 弥辟 辑滚 肺弊酒眶 老矫
		s.mIp, -- 弥辟 辑滚 立加 IP
		p.mDelDate, -- 某腐磐 昏力老
		c.mOwner, -- 某腐磐 拌沥 锅龋
		c.mNo, -- 某腐磐 锅龋
		s.mIsPreventItemDrop	-- 酒捞袍 靛而 规瘤 咯何 
	FROM
		dbo.TblChaosBattlePc AS c		
		LEFT OUTER JOIN [dbo].[TblPc] AS p
			ON c.mNo = p.mNo
		LEFT OUTER JOIN	[dbo].[TblPcState] AS s 
			ON	c.mNo = s.mNo
		INNER JOIN	[FNLAccount].[dbo].[TblUser] AS u
			ON	c.mOwner = u.mUserNo			
		LEFT OUTER JOIN [dbo].[TblGuildMember] gm
			ON	c.mNo = gm.mPcNo
		Left OUTER JOIN	[dbo].[TblGuild] As g 
			ON	g.mGuildNo = gm.mGuildNo
	WHERE		
		c.mNm = @mNm
		AND c.mFieldSvrNo = @mFieldSvrNo

GO

