CREATE  PROCEDURE [dbo].[WspGetItemCntByAllTID] 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		  dt.mName   	AS IName
		, dt.mItemNo	 AS IID
		, COUNT(p.mItemNo) AS Cnt
	FROM 
		dbo.TblPcInventory p 
		RIGHT OUTER JOIN [FNLParm].dbo.TblMonitorItem dt 
			ON	p.mItemNo = dt.mItemNo 	
	GROUP BY  dt.mName, dt.mItemNo	
	ORDER BY dt.mItemNo ASC

GO

