CREATE PROCEDURE [dbo].[UspLoginPc]
	 @pUserNo	INT
	,@pPcNo		INT
	,@pIp		CHAR(15)
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	IF NOT EXISTS(	SELECT * 
					FROM dbo.TblPc WITH(NOLOCK)
					WHERE ([mOwner]=@pUserNo) 
							AND ([mNo]=@pPcNo) 
							AND ([mDelDate] IS NULL))
	BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END	 	 
	END
	 
	UPDATE dbo.TblPcState 
	SET 
		[mIp]=@pIp, 
		[mLoginTm]=GETDATE() 
	WHERE ([mNo]=@pPcNo)
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 2
		GOTO LABEL_END	 
	END

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

