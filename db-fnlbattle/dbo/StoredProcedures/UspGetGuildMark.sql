CREATE PROCEDURE [dbo].[UspGetGuildMark]
	 @pDate	SMALLDATETIME
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		mGuildNo,
		mGuildSeqNo,
		mGuildMark
	FROM dbo.TblGuild
	WHERE mGuildMarkUptDate >= @pDate

GO

