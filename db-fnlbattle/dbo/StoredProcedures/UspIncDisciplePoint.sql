CREATE PROCEDURE [dbo].[UspIncDisciplePoint]
	 @pMaster		INT
	,@pIncPoint		INT
	,@pMaxPoint		INT
	,@pCurPoint		INT OUTPUT	-- 泅犁 儡咯器牢飘
	,@pMaxCurPoint	INT OUTPUT	-- 泅犁鳖瘤狼 弥措 儡咯器牢飘
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT
	SET @aRv = 0
	SET @pCurPoint = 0
	SET @pMaxCurPoint = 0

	SELECT 
		TOP 1 
		@pCurPoint = mCurPoint, 
		@pMaxCurPoint = mMaxCurPoint 
	FROM dbo.TblDisciple 
	WHERE mMaster = @pMaster
	
	IF @@ROWCOUNT <= 0
	BEGIN
		RETURN(2)	-- 粮犁窍瘤 臼绰 葛烙
	END

	-- 泅犁 儡咯器牢飘 + 眠啊 器牢飘啊 啊瓷茄 弥措器牢飘焊促 奴瘤 眉农
	IF (@pCurPoint + @pIncPoint > @pMaxPoint)
	BEGIN
		SET @pCurPoint = @pMaxPoint
	END
	ELSE
	BEGIN
		SET @pCurPoint = @pCurPoint + @pIncPoint
	END

	-- 泅犁鳖瘤狼 弥措 儡咯器牢飘啊 馆康瞪 泅犁 儡咯器牢飘焊促 累篮瘤 眉农
	IF (@pCurPoint > @pMaxCurPoint)
	BEGIN
		SET @pMaxCurPoint = @pCurPoint
	END

	UPDATE dbo.TblDisciple 
	SET 
		mCurPoint = @pCurPoint, 
		mMaxCurPoint = @pMaxCurPoint 
	WHERE mMaster = @pMaster
	IF @@ERROR > 0
	BEGIN
		RETURN(1)		
	END
	
	RETURN(0)

GO

