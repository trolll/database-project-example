CREATE Procedure [dbo].[UspReduceGuildAgitTime]	-- 漂沥 辨靛酒瘤飘狼 矫埃 皑家矫糯 (秦寸 辨靛葛烙 沥焊 绝栏搁 积己)
	 @pTerritory		INT			-- 康瘤锅龋
	,@pGuildAgitNo		INT			-- 辨靛酒瘤飘锅龋
	,@pReduceMin		INT OUTPUT		-- 皑家矫懦 盒 [涝仿/免仿]
	,@pOwnerGID		INT OUTPUT		-- 家蜡辨靛 [免仿]
	,@pIsSelling		CHAR OUTPUT		-- 概阿陛咀 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		BEGIN TRAN

		-- 父丰盒 皑家
		UPDATE TblGuildAgit SET mLeftMin = mLeftMin - ISNULL(@pReduceMin, 0) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 

		-- 辨靛葛烙 沥焊甫 掘绢咳
		SELECT @pReduceMin = mLeftMin, @pOwnerGID = mOwnerGID, @pIsSelling = mIsSelling FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
		IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜 (火涝)

		SET	@pReduceMin=0
		SET	@pOwnerGID=0
		SET	@pIsSelling=0

		BEGIN TRAN

		INSERT TblGuildAgit (mTerritory, mGuildAgitNo) VALUES(@pTerritory, @pGuildAgitNo)
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END

	IF NOT EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
	BEGIN
		INSERT TblGuildAgitAuction (mTerritory, mGuildAgitNo) VALUES (@pTerritory, @pGuildAgitNo)
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END
	

LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

