CREATE PROCEDURE [dbo].[UspSendGuildAgitTicketToGuild]
	 @pSerial		BIGINT		-- 辨靛酒瘤飘 檬没厘 锅龋.
	,@pToGuildNm	VARCHAR(12)
	,@pStatus		TINYINT
	,@pTerritory	INT		-- 康瘤锅龋
	,@pGuildAgitNo	INT		-- 辨靛酒瘤飘锅龋
	,@pOwnerGID		INT		-- 家蜡辨靛
	,@pFromPcNm		VARCHAR(12)	-- 焊郴绰 荤恩 捞抚
    ,@pSendTicket  	INT		-- 焊郴绰 酒捞袍 捞抚
    ,@pRecvTicket  	INT		-- 罐绰 酒捞袍 捞抚
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE 	@aRowCnt 	INT,
				@aErrNo		INT
		
	SELECT @aErrNo = 0, @aRowCnt = 0

	DECLARE	@aToGuildNo	INT
	DECLARE 	@aSn	BIGINT
	DECLARE 	@aPcNo	INT
	DECLARE	@aEnd	DATETIME,
				@aBeginCount	INT,
				@aEndCount	INT
	DECLARE	@aGuildAgitTicket1	INT	-- 辨靛窍快胶 檬没厘(辨靛)
	DECLARE	@aGuildAgitTicket2	INT	-- 罐篮 辨靛窍快胶 檬没厘
				
	DECLARE	@aGuildMember	TABLE (
					mSeq		INT	 IDENTITY(1,1)	 NOT NULL,
					mPcNo		INT	 NOT NULL
				)

	SELECT		 @aEnd = dbo.UfnGetEndDate(GETDATE(), 7)
				,@aBeginCount = 0
				,@aEndCount = 0
				,@aSn = 0
				,@aPcNo = 0
				,@aGuildAgitTicket1 = @pSendTicket	-- 辨靛酒瘤飘 檬没厘(辨靛) 锅龋 (窍靛内爹 登绢乐澜)
                ,@aGuildAgitTicket2 = @pRecvTicket	-- 罐篮 辨靛酒瘤飘 檬没厘 锅龋 (窍靛内爹 登绢乐澜)

	--------------------------------------------------------------------------	
	--辨靛 沥焊 眉农 
	--------------------------------------------------------------------------
	SELECT 
		@aToGuildNo=mGuildNo
	FROM dbo.TblGuild 
	WHERE mGuildNm = @pToGuildNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(2)		-- 2 : 罐绰 辨靛啊 粮犁窍瘤 臼澜
	 END

	--------------------------------------------------------------------------	
	--辨靛酒瘤飘 檬没厘 力芭
	--------------------------------------------------------------------------
	DELETE dbo.TblPcInventory 
	WHERE mSerialNo = @pSerial
			 AND mItemNo = @aGuildAgitTicket1

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(3)		-- 3 : 辨靛酒瘤飘 檬没厘捞 粮犁窍瘤 臼澜
	END
		 
	--------------------------------------------------------------------------	
	--泅犁 辨靛盔 沥焊甫 掘绰促
	--------------------------------------------------------------------------
	INSERT INTO @aGuildMember
	SELECT [mPcNo] 
	FROM dbo.TblGuildMember 
	WHERE  [mGuildNo] = @aToGuildNo
	
	SET @aEndCount = @@ROWCOUNT	

	--------------------------------------------------------------------------	
	--辨靛酒瘤飘 檬没厘 傈价
	--------------------------------------------------------------------------	
	WHILE(@aEndCount <>0)
	BEGIN

		SELECT 
			TOP 1	@aPcNo = mPcNo
		FROM @aGuildMember
		WHERE mSeq = @aBeginCount+1

		EXEC @aSn =  dbo.UspGetItemSerial 
		IF @aSn <= 0
		BEGIN
			CONTINUE	 
		END			

		IF @aPcNo > 0 
		BEGIN
			INSERT dbo.TblPcInventory(mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
				VALUES(@aSn, @aPcNo, @aGuildAgitTicket2, @aEnd, 0, @pStatus, 1, 0)

			IF(0 = @@ERROR)
			BEGIN
				INSERT dbo.TblGuildAgitTicket (mTicketSerialNo, mTerritory, mGuildAgitNo, mOwnerGID, mFromPcNm, mToPcNm)
				VALUES (@aSn,@pTerritory, @pGuildAgitNo, @pOwnerGID, @pFromPcNm, N'')
				IF(0 = @@ERROR)
				BEGIN
					SELECT @aPcNo, @aSn
				END
			END
		END

		SET @aEndCount = @aEndCount - 1
		SET @aBeginCount = @aBeginCount + 1
	END

	RETURN(0)

GO

