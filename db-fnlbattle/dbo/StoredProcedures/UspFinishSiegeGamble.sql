CREATE PROCEDURE [dbo].[UspFinishSiegeGamble]
	 @pTerritory	INT
	,@pNo			INT	--雀瞒.
	,@pTotalMoney	INT	--醚穿利陛咀.	 
	,@pOccupyGuild0	INT
	,@pOccupyGuild1	INT
	,@pOccupyGuild2	INT
	,@pOccupyGuild3	INT
	,@pOccupyGuild4	INT
	,@pOccupyGuild5	INT
	,@pOccupyGuild6	INT
	,@pIsKeep		INT
	,@pDividend		INT	OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	
	SET		@pDividend	= 0
	
	IF(0 <> @@TRANCOUNT) ROLLBACK
	SET XACT_ABORT ON
	BEGIN TRAN	
	
	--硅寸咀 = (醚 穿利陛咀 * 90%) / 寸梅磊荐
	--10%绰 荐荐丰烙.
	DECLARE	@aTotCnt	INT
	DECLARE @aOkCnt		INT	
	SELECT @aTotCnt=COUNT(*) FROM TblSiegeGambleTicket WHERE ([mTerritory]=@pTerritory) AND ([mNo]=@pNo)
	SELECT @aOkCnt=COUNT(*) FROM TblSiegeGambleTicket WHERE ([mTerritory]=@pTerritory) AND ([mNo]=@pNo) AND ([mIsKeep]
=@pIsKeep)
	IF(0 < @aOkCnt)
	 BEGIN
		SET @pDividend = (@pTotalMoney*0.9) / @aOkCnt
	 END
	
	UPDATE TblSiegeGambleState 
			SET [mIsFinish]=1, [mDividend]=@pDividend, [mIsKeep]=@pIsKeep,
				[mOccupyGuild0]=@pOccupyGuild0, [mOccupyGuild1]=@pOccupyGuild1, 
				[mOccupyGuild2]=@pOccupyGuild2, [mOccupyGuild3]=@pOccupyGuild3,
				[mOccupyGuild4]=@pOccupyGuild4, [mOccupyGuild5]=@pOccupyGuild5,
				[mOccupyGuild6]=@pOccupyGuild6
			WHERE ([mTerritory] = @pTerritory) AND ([mNo] = @pNo)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END			 
	END			

	SET @pNo = @pNo + 1
	INSERT TblSiegeGambleState([mTerritory],[mNo],[mIsFinish],[mTotalMoney],[mDividend]) 
		VALUES(@pTerritory, @pNo, 0, 0, 0)
	IF(0 <> @@ERROR)
	 BEGIN
			SET @aErrNo = 2
			GOTO LABEL_END			 
	 END			

LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

