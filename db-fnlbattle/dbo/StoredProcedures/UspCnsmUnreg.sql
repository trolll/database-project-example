/******************************************************************************  
**  Name: UspCnsmUnreg  
**  Desc: 困殴 秒家 or 扁埃 父丰等 巴 雀荐
**  
**                
**  Return values:  
**   0 : 累诀 贸府 己傍  
**   > 0 : SQL Error  
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmUnreg]
 @pCnsmSn  BIGINT	-- 困殴 矫府倔 锅龋
 ,@pItemNo INT		-- 酒捞袍 锅龋(困殴 单捞磐客 老摹 咯何 犬牢)
 ,@pIsStack BIT		-- 胶琶屈牢瘤 咯何
 ,@pPcNo	INT
 ,@pCatetory TINYINT OUTPUT
 ,@pItemSerial BIGINT OUTPUT	-- 雀荐等 酒捞袍Sn, 扁粮巴俊 捍钦登菌促搁 措惑Sn
 ,@pResultCnt  INT OUTPUT		-- 雀荐等 俺荐 or 捍钦 搬苞 弥辆俺荐
 ,@pCurCnt INT OUTPUT -- 雀荐等 俺荐
 ,@pPrice INT OUTPUT -- 俺寸 魄概 啊拜
AS  
	SET NOCOUNT ON   ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;

	DECLARE @aPcNo					INT;
	DECLARE @aCurCnt				INT	;

	DECLARE @aRegDate			DATETIME;
	DECLARE @aSerialNo			BIGINT	;
	DECLARE @aItemNo			INT		;	
	DECLARE @aEndDate			DATETIME;
	DECLARE @aIsConfirm			BIT	;
	DECLARE @aStatus			TINYINT;
	DECLARE @aCntUse			TINYINT;
	DECLARE @aOwner				INT;
	DECLARE @aPracticalPeriod	INT;
	DECLARE @aBindingType		TINYINT;
	DECLARE @aRestoreCnt		TINYINT;

	SELECT @aRowCnt = 0, @aErrNo= 0  ;
	SELECT   
		@aPcNo		= [mPcNo],
		@aCurCnt	= [mCurCnt],
		@pCatetory	= [mCategoryNo],
		@aItemNo	= mItemNo,
		@pPrice = mPrice
	FROM dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmSn;   

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 1;  -- 困殴 酒捞袍捞 粮犁窍瘤 臼嚼聪促.(TblConsignment)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 郴何 坷幅
		END
		GOTO T_END ;  
	END  

	SET @pCurCnt = @aCurCnt;

	-- PC 锅龋 犬牢(夯牢捞 殿废茄 巴牢瘤 犬牢)
	IF @aPcNo <> @pPcNo
	BEGIN  
		SET @aErrNo = 2 ;-- 夯牢捞 殿废茄 酒捞袍捞 酒凑聪促.
		GOTO T_END   ;
	END

	-- 酒捞袍 锅龋啊 老摹窍绰瘤 犬牢
	IF @aItemNo <> @pItemNo
	BEGIN
		SET @aErrNo = 3; -- 酒捞袍ID啊 老摹窍瘤 臼嚼聪促.
		GOTO T_END;
	END

	SELECT
		@aRegDate			= mRegDate,
		@aSerialNo			= mSerialNo,		
		@aEndDate			= mEndDate,
		@aIsConfirm			= mIsConfirm,
		@aStatus			= mStatus,
		@aCntUse			= mCntUse,
		@aOwner				= mOwner,
		@aPracticalPeriod	= mPracticalPeriod,
		@aBindingType		= mBindingType,
		@aRestoreCnt		= mRestoreCnt
	FROM dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmSn;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 4 ; -- 困殴 酒捞袍捞 粮犁窍瘤 臼嚼聪促.(TblConsignmentItem)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100 ; -- DB 郴何 坷幅
		END
		GOTO T_END ;  
	END  


	-- 胶琶屈捞 酒囱 版快
	IF @pIsStack = 0
	BEGIN
		SET @pItemSerial = @aSerialNo;
		SET @pResultCnt = @aCurCnt;

		BEGIN TRAN;

		-- 酒捞袍 昏力
		DELETE dbo.TblConsignmentItem
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 5; -- 酒捞袍 昏力 角菩(TblConsignmentItem)
			GOTO T_END   ;
		END

		DELETE dbo.TblConsignment
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 6 ;-- 酒捞袍 昏力 角菩(TblConsignment)
			GOTO T_END   ;
		END

		-- 牢亥配府俊 酒捞袍 眠啊
		INSERT dbo.TblPcInventory(
			[mSerialNo],  
			[mPcNo],   
			[mItemNo],   
			[mEndDate],   
			[mIsConfirm],   
			[mStatus],   
			[mCnt],   
			[mCntUse],  
			[mOwner],  
			[mPracticalPeriod], 
			mBindingType )  
		VALUES(  
			@pItemSerial,  
			@pPcNo,   
			@aItemNo,   
			@aEndDate,   
			@aIsConfirm,   
			@aStatus,   
			@aCurCnt,   
			@aCntUse,  
			@aOwner,  
			@aPracticalPeriod, 
			@aBindingType) ;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 7; -- 牢亥配府 酒捞袍 眠啊 角菩
			GOTO T_END  ; 
		END
		COMMIT TRAN
		GOTO T_END
	END


	-- 胶琶屈老 版快

	DECLARE @aCntTarget  INT  ;
	DECLARE @aIsSeizure  BIT ; 
	SET  @aCntTarget = 0  ;   

	SELECT   
		@aCntTarget=ISNULL([mCnt],0),   
		@pItemSerial=[mSerialNo],   
		@aIsSeizure=ISNULL([mIsSeizure],0)  
	FROM dbo.TblPcInventory
	WHERE mPcNo		=  @pPcNo
	AND mItemNo		= @aItemNo   
	AND mIsConfirm	= @aIsConfirm   
	AND mStatus		= @aStatus   
	AND mOwner		= @aOwner    
	AND mPracticalPeriod	= @aPracticalPeriod  
	AND mBindingType		= @aBindingType
	AND (mCnt + @aCurCnt) <= 1000000000  ;
	  
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0  
	BEGIN  
		SET @aErrNo = 100 ; -- DB 郴何 坷幅  
		GOTO T_END  ;    
	END

	-- 胶琶矫虐妨绰 措惑捞 拘幅登绢 乐栏搁 魄概 秒家甫 给茄促.
	-- 弊成 矫府倔 货肺 父甸绢辑 魄概 秒家啊 啊瓷窍档废 且 荐档 乐变 茄单...
	IF @aCntTarget<> 0 AND @aIsSeizure <> 0  
	BEGIN  
		SET @aErrNo = 8; -- 酒捞袍捞 拘幅登绢 乐嚼聪促.
		GOTO T_END   ;   
	END


	BEGIN TRAN;
	-- @aCurCnt俊 掘绢柯 俺荐父怒阑 哗辑 诀单捞飘茄 搬苞啊 0捞搁 DELETE甫 茄促.
	-- DELETE俊 角菩且 版快 俊矾
	DECLARE @aResultCnt INT;
	SET @aResultCnt = 0;

		UPDATE TblConsignment
		SET @aResultCnt = mCurCnt = mCurCnt - @aCurCnt
		WHERE  mCnsmNo  =@pCnsmSn;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 9 ; -- 昏力且 荐 绝嚼聪促(TblConsignment)
			GOTO T_END   ;
		END
		IF @aResultCnt <> 0
		BEGIN
			ROLLBACK TRAN;
			SET @aErrNo = 10 ; -- 昏力且 荐 绝嚼聪促(TblConsignment)
			GOTO T_END   ;
		END

	-- 酒捞袍 昏力
	DELETE dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmSn;
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT;  
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 11 ; -- 昏力且 荐 绝嚼聪促(TblConsignmentItem)
		GOTO T_END ;  
	END

	DELETE dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmSn;
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 12;  -- 昏力且 荐 绝嚼聪促(TblConsignment)
		GOTO T_END   ;
	END

	IF @aCntTarget = 0 -- 穿利且 酒捞袍捞 绝阑 版快
	BEGIN
		SET @pResultCnt = @aCurCnt;
		EXEC @pItemSerial =  dbo.UspGetItemSerial  ;-- 脚痹 矫府倔 掘扁   
		IF @pItemSerial <= 0  
		BEGIN  
			ROLLBACK TRAN  ;
			SET @aErrNo = 13;  -- 酒捞袍 矫府倔 积己 角菩
			GOTO T_END    ;
		END	

		-- 牢亥配府俊 酒捞袍 眠啊
		INSERT dbo.TblPcInventory(
			[mSerialNo],  
			[mPcNo],   
			[mItemNo],   
			[mEndDate],   
			[mIsConfirm],   
			[mStatus],   
			[mCnt],   
			[mCntUse],  
			[mOwner],  
			[mPracticalPeriod], 
			mBindingType )  
		VALUES(  
			@pItemSerial,  
			@pPcNo,   
			@aItemNo,   
			@aEndDate,   
			@aIsConfirm,   
			@aStatus,   
			@aCurCnt,   
			@aCntUse,  
			@aOwner,  
			@aPracticalPeriod, 
			@aBindingType)  ;
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF  @aErrNo <> 0 OR  @aRowCnt = 0  
		BEGIN
			ROLLBACK TRAN  ;
			SET @aErrNo = 14 ; -- 牢亥配府俊 酒捞袍 眠啊 角菩
			GOTO T_END;
		END
	END
	ELSE -- 穿利且 酒捞袍捞 乐阑 版快
	BEGIN
		UPDATE dbo.TblPcInventory
		SET @pResultCnt = mCnt = mCnt + @aCurCnt
		WHERE mSerialNo = @pItemSerial;
		IF  @aErrNo <> 0 OR  @aRowCnt = 0  
		BEGIN
			ROLLBACK TRAN  ;
			SET @aErrNo = 15;  -- 牢亥配府俊 酒捞袍 眠啊 角菩
			GOTO T_END;
		END
		IF 1000000000 < @pResultCnt
		BEGIN
			ROLLBACK TRAN ; 
			SET @aErrNo = 16;  -- 牢亥配府俊 酒捞袍 眠啊 角菩
			GOTO T_END;
		END
	END

T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN ; 

T_END:     
	SET NOCOUNT OFF  ; 
	RETURN(@aErrNo) ;

GO

