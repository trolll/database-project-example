CREATE PROCEDURE [dbo].[UspGetGkill]
	 @pGuildNo	INT
	,@pPlace	INT		-- EPlace.
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	-- 漂沥瘤开捞搁 guild锅龋甫 舅荐 绝促.
	IF(0 = @pPlace)
	BEGIN
		SELECT a.[mGuildNo], a.[mNode], a.[mPcNo], a.[mRegDate], b.[mNm] 
		FROM dbo.TblGkill AS a 
				INNER JOIN dbo.TblPc AS b 
		ON(a.[mPcNo] = b.[mNo]) 
		WHERE (a.[mGuildNo]=@pGuildNo) 
			AND (a.[mPlace]=@pPlace)
	 
	END
	ELSE
	 BEGIN
		SELECT a.[mGuildNo], a.[mNode], a.[mPcNo], a.[mRegDate], b.[mNm] 
		FROM dbo.TblGkill AS a 			INNER JOIN dbo.TblPc AS b 
		ON(a.[mPcNo] = b.[mNo]) 
		WHERE a.[mPlace] = @pPlace
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

