CREATE PROCEDURE [dbo].[UspNewGuildAss]
	 @pGuildNo	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	IF(0 <> @@TRANCOUNT) ROLLBACK
	SET XACT_ABORT ON
	BEGIN TRAN	
		
	INSERT INTO TblGuildAss([mGuildNo]) VALUES(@pGuildNo)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END		
	 END
	 
	INSERT INTO TblGuildAssMem([mGuildAssNo], [mGuildNo]) VALUES(@pGuildNo, @pGuildNo)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo = 2
		GOTO LABEL_END		
	 END	 
	 
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

