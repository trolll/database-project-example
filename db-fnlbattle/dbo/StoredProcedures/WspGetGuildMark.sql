--------------------------------
-- 按眉疙 : WspGetGuildMark
-- 汲疙 : 辨靛 付农(Binary) 单捞磐甫 掘绢柯促
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.07.11
-- 荐沥磊 : 
-- 荐沥老 : --------------------------------

CREATE     PROCEDURE [dbo].[WspGetGuildMark] 
	@mGuildNm	CHAR(12)		-- 辨靛 捞抚

AS
	SET NOCOUNT ON

	SELECT
		mGuildMark
	FROM	
		TblGuild
	WITH ( NOLOCK ) 
	WHERE
		mGuildNm = @mGuildNm
		
	SET NOCOUNT OFF

GO

