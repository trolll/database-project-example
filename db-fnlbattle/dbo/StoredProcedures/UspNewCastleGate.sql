CREATE PROCEDURE [dbo].[UspNewCastleGate]
	 @pNo		BIGINT
	,@pHp		INT
	,@pIsOpen	BIT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	INSERT TblCastleGate([mNo], [mHp], [mIsOpen]) VALUES(@pNo, @pHp, @pIsOpen)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo	= 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

