/******************************************************************************
**		File: 
**		Name: UspGetNextUnitedGuildWarTournamentTime
**		Desc: 促澜 配呈刚飘 矫累 沥焊甫 掘绰促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetNextUnitedGuildWarTournamentTime]
	  @pSvrNo			SMALLINT	-- 烹钦 辨靛 措傈 辑滚 锅龋
	, @pPeriodMonth		SMALLINT	-- 配呈刚飘 馆汗 林扁 (岿)
	, @pPeriodWeek		SMALLINT	-- 配呈刚飘 馆汗 林扁 (林)
	, @pDayOfTheWeek	SMALLINT	-- 配呈刚飘 矫累 夸老
	, @pStxHour			SMALLINT	-- 配呈刚飘 矫累 矫
	, @pStxMin			SMALLINT	-- 配呈刚飘 矫累 盒
	, @pIngDay			SMALLINT	-- 柳青扁埃
	, @pIngHour			SMALLINT	-- 柳青矫埃
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aUTGWStxDate			SMALLDATETIME,
			@aUTGWTournamentStxDate	SMALLDATETIME,
			@aUTGWTournamentEtxDate	SMALLDATETIME,
			@aCurrentDate			DATETIME,	
			@aCurrentDateString		CHAR(40);
	
	SELECT
		  @aUTGWStxDate 			= [mUTGWStxDate]
		, @aUTGWTournamentStxDate	= [mUTGWTournamentStxDate]
		, @aUTGWTournamentEtxDate	= [mUTGWTournamentEtxDate]
	FROM	dbo.TblUnitedGuildWarTournamentStxInfo
	WHERE	[mSvrNo] = @pSvrNo
				AND [mPeriodMonth] = @pPeriodMonth
					AND [mPeriodWeek] = @pPeriodWeek
						AND [mDayOfTheWeek] = @pDayOfTheWeek
							AND [mIngDay]	= @pIngDay
						
	-- 单捞磐啊 绝芭唱 配呈刚飘 辆丰矫痢焊促 泅犁 矫埃捞 奴 版快 货肺 父电促.
	IF((0 = @@ROWCOUNT) OR (@aUTGWTournamentEtxDate < GETDATE()))
	 BEGIN
		
		-- SQL 狼 夸老狼 矫累痢阑 老夸老肺 函版茄促.
		SET DATEFIRST 7;
		
		-- 泅犁 朝楼俊 矫埃阑 配呈刚飘 矫累矫埃栏肺 嘎冕促.
		SET	@aCurrentDate = GETDATE();
		SET	@aCurrentDateString = CAST(YEAR(@aCurrentDate) AS CHAR(4)) + '-' + CAST(MONTH(@aCurrentDate) AS CHAR(2)) + '-' + CAST(DAY(@aCurrentDate) AS CHAR(2)) + ' ' +
								CAST(@pStxHour AS CHAR(2)) + ':' + CAST(@pStxMin AS CHAR(2));
		SET	@aCurrentDate = CAST(@aCurrentDateString AS DATETIME);								
		
		-- 配呈刚飘 矫累矫埃苞 老矫甫 掘绰促.
		SET	@aUTGWTournamentStxDate = dbo.UfnGetUTGWTournamentStxTime(@aCurrentDate, @pPeriodMonth, @pPeriodWeek, @pDayOfTheWeek);
		
		IF(@aUTGWTournamentStxDate <= GETDATE())
		 BEGIN
			-- 岿 林扁俊 + 1 阑 秦辑配呈刚飘 矫累矫埃苞 老矫甫 掘绰促. 
			SET	@aUTGWTournamentStxDate = dbo.UfnGetUTGWTournamentStxTime(@aCurrentDate, @pPeriodMonth + 1, @pPeriodWeek, @pDayOfTheWeek);
		 END
		
		BEGIN TRAN;
		
			-- 促弗 林扁狼 配呈刚飘 矫累 沥焊啊 乐栏搁 秦寸 沥焊甫 昏力茄促.
			DELETE	dbo.TblUnitedGuildWarTournamentStxInfo WHERE [mSvrNo] = @pSvrNo;
			IF(0 <> @@ERROR)
			 BEGIN
				ROLLBACK TRAN;
				RETURN(1);	--DB ERROR
			 END
		
			-- 矫累 矫埃苞 辆丰矫埃阑 汲沥茄促.
			SELECT	@aUTGWStxDate  = GETDATE(), @aUTGWTournamentEtxDate = DATEADD(hh, @pIngHour, DATEADD(dd, @pIngDay - 1, @aUTGWTournamentStxDate));
			
			-- 货肺款 配呈刚飘 沥焊甫 涝仿茄促.
			INSERT INTO dbo.TblUnitedGuildWarTournamentStxInfo 
			([mSvrNo], [mUTGWStxDate], [mUTGWTournamentStxDate], [mUTGWTournamentEtxDate], [mPeriodMonth], [mPeriodWeek], [mDayOfTheWeek], [mIngDay]) 
			VALUES 
			(@pSvrNo, @aUTGWStxDate, @aUTGWTournamentStxDate,  @aUTGWTournamentEtxDate, @pPeriodMonth, @pPeriodWeek, @pDayOfTheWeek, @pIngDay);
			
			IF(0 <> @@ERROR)
			 BEGIN
				ROLLBACK TRAN;
				RETURN(2);	-- DB ERROR
			 END
			 
		COMMIT TRAN;
		
		
	 END
	
	SELECT	@aUTGWStxDate, @aUTGWTournamentStxDate, @aUTGWTournamentEtxDate;
	RETURN(0);

GO

