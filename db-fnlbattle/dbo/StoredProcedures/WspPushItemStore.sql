CREATE PROCEDURE [dbo].[WspPushItemStore]
	@pUserNo		INT				-- 某腐磐甫 家蜡茄 荤侩磊锅龋.(芒绊绰 某腐磐啊 傍蜡窃)
	,@pItemNo		INT				-- 酒捞袍 锅龋 	
	,@pCnt			INT				-- 积己且 肮荐
	,@pStatus		TINYINT			-- 惑怕
	,@pIsSeizure	BIT				-- 拘幅咯何 
	,@pIsConfirm	BIT				-- 犬牢 咯何 
	,@pIsStack		BIT
	,@pValidDay		INT				-- 蜡瓤老.(窜困:老)
	,@pTargetSn		BIGINT OUTPUT	-- 烹钦等 SN锅龋.
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
		 	
	DECLARE	@aTargetSn		BIGINT	
	DECLARE	@aCntTarget		INT
	SET		@aCntTarget = 0		
	DECLARE	@aIsSeizure		BIT	
	
	-- 捣.
	IF(@pItemNo = 409)
	 BEGIN
		SET		@pIsConfirm = 1
		SET		@pStatus = 1
	 END
	
	-- 父丰老 
	DECLARE	@aEndDay	SMALLDATETIME
	SET		@aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)
		
	
	IF @pIsStack = 1	
	BEGIN
		-- 胶琶屈狼 版快 穿利且 酒捞袍阑 茫绰促.
		SELECT 
			@aCntTarget=ISNULL(mCnt,0), 
			@aTargetSn=mSerialNo, 
			@aIsSeizure=ISNULL(mIsSeizure,0)
		FROM dbo.TblPcStore
		WHERE mUserNo = @pUserNo				AND mItemNo = @pItemNo				AND mIsConfirm = @pIsConfirm				AND mStatus = @pStatus

		IF(0 <> @@ERROR)
		BEGIN
			RETURN(4)
		END
			
		IF(0 <> @aIsSeizure)
		BEGIN
			RETURN(5)	 
		END	
	END
	
	--------------------------------------------------------------
	-- 酒捞袍阑 积己窍芭唱 穿利茄促. 
	--------------------------------------------------------------
	IF @aTargetSn > 0
	BEGIN
		-- 穿利且 措惑 粮犁 
		SET @pTargetSn = @aTargetSn
		
		UPDATE dbo.TblPcStore 
		SET 
			mCnt = mCnt + @pCnt
		WHERE mSerialNo = @aTargetSn
				AND mItemNo = @pItemNo
		IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
		BEGIN
			RETURN(6)
		END		
				
		RETURN(0)
	END 
	
	EXEC @pTargetSn = dbo.UspGetItemSerial 
	IF @pTargetSn <= 0 
	BEGIN
		RETURN(7)
	END 	
			
	-- 芒绊狼 脚痹 酒捞袍 殿废 
	INSERT INTO dbo.TblPcStore (mSerialNo, mUserNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
	VALUES(
		@pTargetSn,
		@pUserNo, 
		@pItemNo, 
		@aEndDay, 
		@pIsConfirm, 
		@pStatus, 
		@pCnt, 
		0	-- UsedCount (积己篮 0栏肺 技泼 茄促)
	)	
	
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(8)
	END
	
	RETURN(0)

GO

