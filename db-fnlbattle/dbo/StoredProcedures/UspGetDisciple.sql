CREATE PROCEDURE [dbo].[UspGetDisciple]
	 @pMaster		INT
	,@pMasterNm		VARCHAR(12) OUTPUT
	,@pMasterLevel		INT OUTPUT
	,@pCurPoint		INT OUTPUT
	,@pMaxCurPoint	INT OUTPUT
	,@pRegDate		DATETIME OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET @pMasterNm = ''
	SET @pMasterLevel = 0
	SET @pCurPoint = 0
	SET @pMaxCurPoint = 0
	SET @pRegDate='1900-01-01 00:00:00'

	SELECT @pMasterNm = ISNULL(b.[mNm], '<NONE>'), 
		@pMasterLevel = ISNULL(c.[mLevel], 0), 
		@pCurPoint = a.[mCurPoint], 
		@pMaxCurPoint = a.[mMaxCurPoint], 
		@pRegDate = a.[mRegDate]
	FROM TblDisciple As a 
		JOIN TblPc As b 
			ON a.[mMaster] = b.[mNo]
		JOIN TblPcState As c 
			ON a.[mMaster] = c.[mNo]
	WHERE a.[mMaster] = @pMaster

GO

