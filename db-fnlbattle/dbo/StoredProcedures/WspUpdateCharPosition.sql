--------------------------------
-- 按眉疙 : WspUpdateCharPosition
-- 汲疙 : 某腐磐 困摹甫 函版茄促
-- 累己磊 : 捞快籍
-- 累己老 : 2006.04.09
-- 荐沥磊 : 辫己犁
-- 荐沥老 : 2006.06.06
-- 荐沥郴侩 : 飘罚黎记 荤侩
--------------------------------
CREATE    PROCEDURE [dbo].[WspUpdateCharPosition]	
	@mPosX REAL, -- 困摹X
	@mPosY REAL, -- 困摹Y
	@mPosZ REAL, -- 困摹Z
	@mNm CHAR(12) -- 某腐磐疙
AS
	SET NOCOUNT ON

	BEGIN TRANSACTION
	
	UPDATE
		[dbo].[TblPcState]
	SET
		mPosX = @mPosX,
		mPosY = @mPosY,
		mPosZ = @mPosZ
	WHERE		
		mNo = (SELECT mNo From [dbo].[TblPc] WHERE mNm = @mNm)			


	IF @@ERROR = 0
	BEGIN
		COMMIT TRANSACTION
		RETURN 0
	END
	ELSE
	BEGIN
		ROLLBACK TRANSACTION
		RETURN @@ERROR
	END

GO

