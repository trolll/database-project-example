CREATE PROCEDURE [dbo].[UspSetGuildMsg]
	 @pGuildNo		INT
	,@pGuildMsg		CHAR(60)
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
		
	UPDATE dbo.TblGuild 
	SET 
		[mGuildMsg]=@pGuildMsg
	WHERE ([mGuildNo]=@pGuildNo )
	IF(0 <> @@ERROR) OR (1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

