/******************************************************************************
**		File: UspGetListFromStore.sql
**		Name: UspGetListFromStore
**		Desc: 芒绊 酒捞袍阑 啊廉柯促.
**
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2006.11.29	JUDY				WHERE 炼扒屈 何盒 荐沥
**		2007.05.07	JUDY				酒捞袍 盒府 利侩
**		2007.10.01	soundkey			家蜡磊 沥焊 眠啊
**		2008.01.07	JUDY				mPracticalPeriod 瓤苞 瘤加 矫埃 眠啊
**		2009.04.17	JUDY				牢郸胶 腮飘 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetListFromStore]
	 @pOwner		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SET ROWCOUNT 300	

	SELECT 
		[mSerialNo], 
		[mItemNo], 
		[mIsConfirm], 
		[mStatus], 
		[mCnt], 
		[mCntUse],
		[mOwner],	-- 家蜡磊 沥焊 眠啊. 
		[mPracticalPeriod]	-- 瓤苞瘤加矫埃 眠啊
	FROM dbo.TblPcStore WITH(INDEX(CL_TblPcStore), NOLOCK )
	WHERE mUserNo = @pOwner
			AND mEndDate >  CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY mItemNo

GO

