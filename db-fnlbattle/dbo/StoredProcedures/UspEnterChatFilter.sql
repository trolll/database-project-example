/******************************************************************************
**		Name: UspEnterChatFilter
**		Desc: 盲泼瞒窜沥焊 殿废
**		Test:			
**		Auth: 巢己葛
**		Date: 2014/03/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspEnterChatFilter]
	 @pOwnerPcNo	INT
	,@pPcNm			CHAR(15)
	,@pPcNo			INT			OUTPUT
	,@pErrNoStr		VARCHAR(50)	OUTPUT
	,@pLastLogoutTm	DATETIME	OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo				INT,
			@aChatFilterPcNm	CHAR(12)
			
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'
	SET		@aChatFilterPcNm = NULL
	
	SELECT 
		@pPcNo=a.[mNo], 
		@pLastLogoutTm=b.[mLogoutTm] 
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
			ON a.mNo = b.mNo
	WHERE a.mNm = @pPcNm
			AND a.mDelDate IS NULL
	IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
	BEGIN
		SET	 @pErrNoStr	= 'eErrNoCharNotExist'
		RETURN(1)
	END

	SELECT TOP 1 
		@aChatFilterPcNm = mChatFilterPcNm
	FROM dbo.TblPcChatFilter
	WHERE mOwnerPcNo = @pOwnerPcNo
			 AND mChatFilterPcNo = @pPcNo						 
	IF (@@ROWCOUNT > 0) AND (@aChatFilterPcNm = @pPcNm)
	BEGIN
		SET	 @pErrNoStr	= 'eErrNoCharAlreadyRegister'
		RETURN(2) 	
	END 

	BEGIN TRAN
	
		IF @aChatFilterPcNm IS NOT NULL		-- duplicate pc number 
		BEGIN		
		
			UPDATE dbo.TblPcChatFilter	
			SET 
				mChatFilterPcNo = mChatFilterPcNo			
			WHERE mOwnerPcNo = @pOwnerPcNo
				 AND mChatFilterPcNo = @pPcNo
				 
			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(3)
			END 	 
		END 
		
		INSERT INTO dbo.TblPcChatFilter([mOwnerPcNo], [mChatFilterPcNo], [mChatFilterPcNm])
		VALUES(@pOwnerPcNo,  @pPcNo, @pPcNm)		
		
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(3)
		END 	 

	COMMIT TRAN
	RETURN(0)

GO

