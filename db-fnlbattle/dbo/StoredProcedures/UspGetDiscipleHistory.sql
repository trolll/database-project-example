CREATE PROCEDURE [dbo].[UspGetDiscipleHistory]
	 @pMaster		INT
	,@pNo			INT
	,@pFetchPrev		TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF (@pNo <= 0)
	BEGIN
		SELECT TOP 1 @pNo = mNo
		FROM dbo.TblDiscipleHistory
		WHERE mMaster = @pMaster
		ORDER BY mMaster, mNo DESC

		SET @pFetchPrev = 1	-- 啊厘 弥脚 郴侩何磐 捞傈狼 郴侩阑 啊廉客具 窃
	END

	IF (@pFetchPrev <> 0)
	BEGIN
		-- 漂沥 瘤开, 雀瞒锅龋狼 捞傈狼 N俺狼 搬苞 啊廉咳
		SELECT TOP 7 mMaster, mNo, mDisciple, RTRIM(mDiscipleNm), mRegDate
		FROM dbo.TblDiscipleHistory
		WHERE mMaster = @pMaster AND mNo <= @pNo
		ORDER BY mMaster, mNo DESC
	END
	ELSE
	BEGIN
		-- 漂沥 瘤开, 雀瞒锅龋狼 捞饶狼 N俺狼 搬苞 啊廉咳
		SELECT TOP 7 mMaster, mNo, mDisciple, RTRIM(mDiscipleNm), mRegDate
		FROM dbo.TblDiscipleHistory
		WHERE mMaster = @pMaster AND mNo >= @pNo
		ORDER BY mMaster, mNo ASC
	END

	SET NOCOUNT OFF

GO

