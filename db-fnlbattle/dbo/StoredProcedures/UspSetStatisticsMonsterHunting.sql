CREATE PROCEDURE [dbo].[UspSetStatisticsMonsterHunting]
	  @pMID			INT
	, @pHuntingCnt	BIGINT
AS
	SET NOCOUNT ON
	
	DECLARE @aErrNo INT
	SET @aErrNo = 0
	
	INSERT INTO dbo.TblStatisticsMonsterHunting ([MID], [mHuntingCnt]) 
	VALUES (@pMID, @pHuntingCnt)
		
	IF(@@ERROR <> 0)
		SET @aErrNo = 2 -- DB Error

	
	RETURN(@aErrNo)

GO

