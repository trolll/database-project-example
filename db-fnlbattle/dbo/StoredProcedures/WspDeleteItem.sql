CREATE    PROCEDURE [dbo].[WspDeleteItem]
	@mSerialNo BIGINT, -- 矫府倔
	@mCnt BIGINT, -- 荐樊
	@mPos		CHAR(1)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @mPos = 'i' 
	BEGIN
		IF(	SELECT mCnt 
			FROM dbo.TblPcInventory 
			WHERE mSerialNo = @mSerialNo) - @mCnt < 1
		BEGIN
			DELETE FROM dbo.TblPcInventory
			WHERE mSerialNo = @mSerialNo
		END
		ELSE
		BEGIN			
			UPDATE dbo.TblPcInventory 
			SET	mCnt = mCnt - @mCnt
			WHERE mSerialNo = @mSerialNo
		END
	END
	
	IF @mPos = 's' 
	BEGIN
		IF(	SELECT mCnt 
			FROM dbo.TblPcStore 
			WHERE mSerialNo = @mSerialNo) - @mCnt < 1
		BEGIN
			DELETE FROM dbo.TblPcStore
			WHERE mSerialNo = @mSerialNo
		END
		ELSE
		BEGIN			
			UPDATE dbo.TblPcStore 
			SET	mCnt = mCnt - @mCnt
			WHERE mSerialNo = @mSerialNo
		END	
	END 
	
	IF @mPos = 'g' 
	BEGIN
		IF(	SELECT mCnt 
			FROM dbo.TblGuildStore 
			WHERE mSerialNo = @mSerialNo) - @mCnt < 1
		BEGIN
			DELETE FROM dbo.TblGuildStore
			WHERE mSerialNo = @mSerialNo
		END
		ELSE
		BEGIN			
			UPDATE dbo.TblGuildStore 
			SET	mCnt = mCnt - @mCnt
			WHERE mSerialNo = @mSerialNo
		END	
	END 	
	
	IF @@ERROR <> 0
	BEGIN			
		RETURN(1)
	END

	RETURN(0)

GO

