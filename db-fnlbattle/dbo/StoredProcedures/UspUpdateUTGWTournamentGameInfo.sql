/******************************************************************************
**		Name: UspUpdateUTGWTournamentGameInfo
**		Desc: 配呈刚飘 霸烙 沥焊甫 盎脚茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateUTGWTournamentGameInfo]
	  @pRound			[INT]
	, @pSvrNo			[SMALLINT]
	, @pGuildNo			[INT]
	, @pGroup			[TINYINT]
	, @pGroupIndex		[TINYINT]
	, @pDepth			[TINYINT]
	, @pIsWin			[BIT]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aState	TINYINT;
	
	IF(0 <> @pIsWin)
	 BEGIN
		SET	@aState = 1;	-- 铰府
	 END
	ELSE
	 BEGIN
		SET	@aState = 0;	-- 铰府
	 END
	
	UPDATE	dbo.TblUnitedGuildWarTournamentInfo	
	SET 	[mMatchTime] = GETDATE(), [mState] = @aState 
	WHERE	[mRound] = @pRound 
		AND [mSvrNo] = @pSvrNo 
		AND [mGuildNo] = @pGuildNo 
		AND [mGroup] = @pGroup 
		AND [mGroupIndex] = @pGroupIndex 
		AND [mDepth] = @pDepth;
	
	IF(0 = @@ROWCOUNT)
	 BEGIN
		RETURN(1);
	 END
	
	RETURN(0);

GO

