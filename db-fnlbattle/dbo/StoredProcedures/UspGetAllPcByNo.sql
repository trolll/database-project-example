CREATE Procedure [dbo].[UspGetAllPcByNo]
	 @pmNo			INT
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	SELECT 
		TblPc.mNo, 
		TblPc.mNm, 
		TblPc.mClass, 
		TblPc.mHead, 
		TblPc.mFace, 
		TblPc.mBody, 
		TblPc.mHomeMapNo, 
		TblPc.mHomePosX, 
		TblPc.mHomePosY, 
		TblPc.mHomePosZ, 
		TblPcState.mLevel, 
		TblPcState.mExp, 
		TblPcState.mHp, 
		TblPcState.mMp, 
		TblPcState.mStomach
	FROM dbo.TblPc  WITH(NOLOCK)
		INNER JOIN dbo.TblPcState  WITH(NOLOCK)
		ON  TblPc.mNo = TblPcState.mNo
	WHERE TblPc.mNo = @pmNo
	
	IF(1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

