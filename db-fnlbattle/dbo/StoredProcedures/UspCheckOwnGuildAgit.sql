CREATE Procedure [dbo].[UspCheckOwnGuildAgit]
	 @pTerritory		INT OUTPUT			-- 康瘤锅龋 [涝仿/免仿]
	,@pOwnerGID		INT						-- 辨靛锅龋
	,@pIsOwnGuildAgit	TINYINT OUTPUT		-- 辨靛酒瘤飘 家蜡咯何 [免仿]
	,@pGuildAgitNo		INT OUTPUT			-- 辨靛酒瘤飘锅龋 [免仿]
	,@pLeftMin		INT OUTPUT				-- 父丰盒 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	SET		@pIsOwnGuildAgit = 0
	SET		@pGuildAgitNo = 0
	SET		@pLeftMin = 0

	IF (@pOwnerGID = 0)
	BEGIN
		RETURN(0)
	END

	IF (@pTerritory <= 0)	-- 角力 康瘤锅龋绰 1何磐烙
	BEGIN
		
		SELECT 
			@pTerritory = mTerritory, 
			@pIsOwnGuildAgit = 1, 
			@pGuildAgitNo = mGuildAgitNo, 
			@pLeftMin = mLeftMin 
		FROM dbo.TblGuildAgit WITH (NOLOCK) 
		WHERE mOwnerGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(1)		-- 1: DB 郴何利牢 坷幅
		END	 

		RETURN(0)
	END

	SELECT 
		@pTerritory = mTerritory, 
		@pIsOwnGuildAgit = 1, 
		@pGuildAgitNo = mGuildAgitNo, 
		@pLeftMin = mLeftMin 
	FROM dbo.TblGuildAgit WITH (NOLOCK) 
	WHERE mTerritory = @pTerritory 
		AND mOwnerGID = @pOwnerGID
		
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)		-- 1: DB 郴何利牢 坷幅
	END	 

	RETURN(0)

GO

