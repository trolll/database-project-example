CREATE PROCEDURE [dbo].[UspRenameGuildNm]
	@pGuildNo		INT,		-- 辨靛锅龋
	@pPcNo			INT,		-- 函版窍妨绊 窍绰 某腐磐 锅龋
	@pOrgGuildNm	CHAR(12),	-- 捞傈 辨靛 捞抚
	@pNewGuildNm	CHAR(12),	-- 函版且 辨靛 捞抚
    @pSerialNo		BIGINT,		-- 酒捞袍 矫府倔 沥焊
    @pItemNo		INT			-- 酒捞袍 锅龋 	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE	@aErrNo		INT
			,@aRowCnt	INT
			,@aNewPcNo	INT
			
	SELECT  @aErrNo = 0, @aRowCnt = 0, @aNewPcNo = 0
	

	-----------------------------------------------
	-- 吝汗 辨靛疙 粮犁咯何 
	-----------------------------------------------
	IF EXISTS( SELECT * 
				FROM dbo.TblGuild
				WHERE mGuildNm = @pNewGuildNm )
	BEGIN
		RETURN(2)			
	END 
	
	-----------------------------------------------
	-- 函版磊 辨靛付胶磐 咯何 眉农 
	-----------------------------------------------	
	IF NOT EXISTS( SELECT *
					FROM dbo.TblGuildMember
					WHERE mGuildNo = @pGuildNo
							AND mPcNo = @pPcNo
							AND mGuildGrade = 0 )
	BEGIN
		RETURN(3)			
	END 	
	
	-----------------------------------------------
	-- 傈犁惑怕 咯何 
	-----------------------------------------------		
	IF EXISTS(  SELECT *
				FROM dbo.TblGuildBattle
				WHERE mGuildNo1 = @pGuildNo ) 
	BEGIN
		RETURN(4)			
	END 

	IF EXISTS(  SELECT *
				FROM dbo.TblGuildBattle
				WHERE mGuildNo2 = @pGuildNo ) 
	BEGIN
		RETURN(4)			
	END 
	
	-----------------------------------------------
	-- 辨靛 楷钦 咯何
	-----------------------------------------------		
	IF EXISTS(	SELECT *
				FROM dbo.TblGuildAssMem
				WHERE mGuildNo = @pGuildNo )
	BEGIN
		RETURN(5)			
	END 	
	
	-----------------------------------------------
	-- 荐己 辨靛 
	-----------------------------------------------		
	IF EXISTS(	SELECT *
				FROM dbo.TblCastleTowerStone
				WHERE mGuildNo = @pGuildNo )
	BEGIN
		RETURN(6)			
	END 	

	-----------------------------------------------
	-- 辨靛疙 函版
	-----------------------------------------------	
	BEGIN TRAN		

		DELETE dbo.TblPcInventory
		WHERE mPcNo = @pPcNo 
			AND	mSerialNo = @pSerialNo
			AND mItemNo = @pItemNo
				
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END		
	
		UPDATE dbo.TblGuild
		SET
			mGuildNm = @pNewGuildNm
		WHERE mGuildNo = @pGuildNo
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END		
	
	COMMIT TRAN
	RETURN(0)

GO

