/******************************************************************************
**		Name: UspDeleteGuild
**		Desc: 辨靛 昏力
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20061208	JUDY				力距 炼扒 眠啊
**		20091029	JUDY				辨靛 葛笼 咯何 犬牢
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteGuild]
	 @pGuildNo	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	--------------------------------------	
	-- 辨靛楷钦 备己盔捞芭唱, 备己 付胶磐绰 辨靛 秦眉啊 阂啊瓷 
	--------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblGuildAssMem 
				WHERE mGuildNo = @pGuildNo OR mGuildAssNo = @pGuildNo )
	BEGIN
		RETURN(3)				
	END

	--------------------------------------	
	-- 辨靛 傈捧 惑炔 眉农 
	--------------------------------------
	IF EXISTS(	SELECT mGuildNo1 mGuildNo
				FROM dbo.TblGuildBattle 
				WHERE mGuildNo1 = @pGuildNo 
				UNION ALL
				SELECT mGuildNo2 mGuildNo
				FROM dbo.TblGuildBattle 
				WHERE mGuildNo2 = @pGuildNo  )
	BEGIN
		RETURN(4)				
	END

	--------------------------------------	
	-- 己 家蜡茄 辨靛绰 秦眉啊 阂啊瓷 
	--------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblCastleTowerStone
				WHERE mGuildNo = @pGuildNo )
	BEGIN
		RETURN(6)				
	END
	
	--------------------------------------	
	-- 辨靛 葛笼吝捞扼搁 呕硼 阂啊. 
	--------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblGuildRecruit 
				WHERE mGuildNo = @pGuildNo
					AND mEndDate >= GETDATE() )
	BEGIN
		RETURN(5)				
	END	
	

	BEGIN TRAN	
	
		DELETE dbo.TblGuildMember 
		WHERE [mGuildNo] = @pGuildNo
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- SQL Error
			GOTO LABEL_END		
		END	

		DELETE dbo.TblGuild 
		WHERE [mGuildNo] = @pGuildNo
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 2		-- SQL Error
			GOTO LABEL_END		
		END		
	 
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN

		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

