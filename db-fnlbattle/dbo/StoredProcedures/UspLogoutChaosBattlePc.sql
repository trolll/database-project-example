/******************************************************************************
**		File: 
**		Name: UspLogoutChaosBattlePc
**		Desc: 墨坷胶硅撇 辑滚俊辑 肺弊酒眶茄促.
**
**		Auth: 辫 堡挤
**		Date: 2009-02-05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspLogoutChaosBattlePc]
	@pNo			INT
	, @pUserNo		INT	
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	DECLARE	@aErrNo		INT;
	DECLARE	@aRowCnt	INT;
	
	UPDATE	dbo.TblChaosBattlePc
	SET
		  [mLogoutTm]	= GETDATE()
		, [mTotUseTm]	= [mTotUseTm] + DATEDIFF(mi, [mLoginTm], GETDATE())
	WHERE	[mNo] = @pNo
				AND [mOwner] = @pUserNo;
				
	SELECT	@aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	
	IF(0 <> @aErrNo)
	 BEGIN
		RETURN(1);		-- DB Error;
	 END
	 
	IF(1 <> @aRowCnt)
	 BEGIN
		RETURN(2);	-- Invalid RowCount
	 END
	 
	RETURN(0);

GO

