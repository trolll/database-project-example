/******************************************************************************
**		Name: UspSelectPcQuestTimeLimit
**		Desc: 葛电 矫埃力茄 涅胶飘 八荤
**
**		Auth: 沥柳龋
**		Date: 2010-02-17
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSelectPcQuestTimeLimit]
     @pPcNo			INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT 
			mQuestNo,
			mLimitTime = DATEDIFF(ss,GETDATE(),mLimitTime)
    FROM dbo.TblPcQuest 
    WHERE mPcNo = @pPcNo 
		AND mValue < 99
		AND mLimitTime IS NOT NULL

GO

