--------------------------------
-- 按眉疙 : WspListCastle
-- 汲疙 : 辑滚 郴 己 格废阑 掘绰促
-- 累己磊 : 辫己犁
-- 累己老 : 2006.04.08
-- 荐沥磊 : 
-- 荐沥老 : 
--------------------------------
CREATE PROCEDURE [dbo].[WspListCastleA]

AS
	SET NOCOUNT ON

	DECLARE @pIsKeep	INT

	SELECT
		
		c.mPlace
		, g.mGuildNm
		, (
			SELECT p.mNm
			FROM [dbo].[TblPc] AS p WITH (NOLOCK)
			INNER JOIN [dbo].[TblGuildMember] AS gm WITH (NOLOCK)
			ON p.mNo = gm.mPcNo
			WHERE gm.mGuildNo = c.mGuildNo AND gm.mGuildGrade = 0
		  ) AS mGuildMasterNm
		, (
			SELECT COUNT(*)
			FROM [dbo].[TblGuildMember] AS gm WITH (NOLOCK)
			WHERE gm.mGuildNo = c.mGuildNo
		  ) AS mGuildTotCnt
		, c.mTaxBuy
		, c.mTaxHunt
		, c.mTaxGamble
		, c.mAsset
		, c.mGuildNo
				


	FROM
		[dbo].[TblCastleTowerStone] AS c WITH (NOLOCK)
	INNER JOIN
		[dbo].[TblGuild] AS g WITH (NOLOCK)
	ON
		c.mGuildNo = g.mGuildNo

	ORDER BY
		c.mPlace
	ASC

GO

