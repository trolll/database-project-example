/******************************************************************************
**		Name: UspCreateChaosBattlePc
**		Desc: 墨坷胶硅撇 DB 俊 货肺款 PC 沥焊甫 眠啊茄促.
**
**		Auth: 辫 堡挤
**		Date: 2009-02-05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2009.08.10	kslive				拌沥捞傈 贸府 眠啊
**		2009.04.29	JUDY				某腐磐 昏力 扁瓷 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCreateChaosBattlePc]
	  @pFieldSvrNo	INT
	, @pFieldPcNo	INT
	, @pFieldNm		CHAR(12)
	, @pOwner		INT
	, @pPcNo		INT	OUTPUT
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	DECLARE	@aErrNo	INT
			, @aPcNo	INT
			, @aRowCnt	INT
			, @aFieldSvrPcNo	INT
			, @aOwner			INT

	SELECT  @aPcNo = 0, @aErrNo = 0, @aRowCnt = 0, @aFieldSvrPcNo = 0, @aOwner = 0;
	
	-- FieldSvrNo, FieldSvrPcNo 肺 促矫 茄锅 八祸茄促.
	SELECT
		  @aFieldSvrPcNo	= [mFieldSvrPcNo]
		, @aPcNo			= [mNo]
		, @aOwner			= [mOwner]
	FROM	dbo.TblChaosBattlePc
	WHERE	[mFieldSvrNo]	= @pFieldSvrNo
				AND [mFieldSvrPcNo]	= @pFieldPcNo;
				
	SELECT	@aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
	
	BEGIN TRAN
	
		IF(0 < @aRowCnt)
		 BEGIN
			-- UspLoginChaosBattlePc 俊辑 mOwner 啊 促弗版快 UspCreateChaosBattlePc 甫 龋免茄促. (咯扁辑绰 家蜡磊父 函版茄促.)
			UPDATE	dbo.TblChaosBattlePc
			SET
				[mOwner] = @pOwner
			WHERE	[mNo] = @aPcNo;
			
			SET	@pPcNo = @aPcNo;
			
			SELECT	@aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
			IF((0 <> @aErrNo) OR (1 <> @aRowCnt))
			 BEGIN
				SET		@aErrNo = 3;
				GOTO	Label_End;
			 END
		 END
		ELSE
		 BEGIN
			-- PC 沥焊啊 粮犁窍瘤 臼栏骨肺 货肺 积己茄促.
			INSERT INTO dbo.TblChaosBattlePc 
			([mOwner], [mFieldSvrNo], [mFieldSvrPcNo], [mNm]) 
			VALUES 
				(@pOwner, @pFieldSvrNo, @pFieldPcNo, @pFieldNm);
				
			SELECT	@aErrNo = @@ERROR;
				
			IF(0 <> @aErrNo)
			BEGIN
				SET		@aErrNo = 1;
				GOTO	Label_End;
			END
			
			SET @pPcNo = @@IDENTITY;
			
			-- 珐欧 沥焊档 积己茄促.
			INSERT INTO dbo.TblPcRankingPoint
				([mPcNo])
			VALUES
				(@pPcNo);
				
			SELECT	@aErrNo = @@ERROR;
				
			IF(0 <> @aErrNo)
			BEGIN
				SET		@aErrNo = 2;
				GOTO	Label_End;
			END
		 END
	
Label_End:
	IF(0 <> @aErrNo)
	 BEGIN
		ROLLBACK TRAN;
	 END
	ELSE
	 BEGIN
		COMMIT TRAN;
	 END
	 
	RETURN(@aErrNo);

GO

