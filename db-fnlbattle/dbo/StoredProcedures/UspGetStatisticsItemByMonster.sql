/******************************************************************************  
**  Name: UspGetStatisticsItemByMonster  
**  Desc: 阁胶磐, NPC 烹拌 抛捞喉 肺靛
**  
**  Auth:   
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  辫碍龋      mItemStatus,  mModType 眠啊
*******************************************************************************/  
CREATE Procedure [dbo].[UspGetStatisticsItemByMonster]  
AS  
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   

	SELECT   
		MID  
		,mItemNo  
		,mCreate  
		,mDelete
		,mItemStatus
		,mModType
	FROM dbo.TblStatisticsItemByMonster

GO

