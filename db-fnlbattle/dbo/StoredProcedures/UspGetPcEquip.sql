CREATE  PROCEDURE [dbo].[UspGetPcEquip]
	 @pPcNo		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			
	
	SELECT 
		a.mSlot, 
		a.mSerialNo,
		 b.mItemNo
	FROM dbo.TblPcEquip AS a 
		INNER JOIN dbo.TblPcInventory AS b 
		ON a.mSerialNo = b.mSerialNo
	WHERE a.mOwner = @pPcNo
			AND b.mEndDate >= GETDATE() 
			AND b.mIsSeizure = 0
			
	SET NOCOUNT OFF

GO

