CREATE PROCEDURE [dbo].[WspGetInfoPcItem] 
	@mSerial	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		mPcNo,
		mItemNo,
		mIsConfirm,
		mStatus,
		mCnt,
		mCntUse,
		mIsSeizure,
		mEndDate,
		mBindingType
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @mSerial

GO

