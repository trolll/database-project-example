/******************************************************************************
**		Name: UspGetListAbnormal
**		Desc: 
**
**		Auth:
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 2009-05-09		Author:	辫 辈挤
**		Description: SELECT 亲格 眠啊. (mRestoreCnt)
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspGetListAbnormal]
	 @pPcNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- time()甫 荤侩窍骨肺 扁霖扁埃捞 '1970-1-1 0:0:0'捞促.
	SELECT 
		  [mParmNo]
		, [mLeftTime]
		, [mAbParmNo]
		, [mRestoreCnt]
	FROM dbo.TblPcAbnormal
	WHERE mPcNo=@pPcNo;

GO

