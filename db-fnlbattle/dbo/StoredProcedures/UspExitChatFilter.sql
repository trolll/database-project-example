/******************************************************************************
**		Name: UspExitChatFilter
**		Desc: 盲泼瞒窜沥焊 昏力
**		Test:			
**		Auth: 巢己葛
**		Date: 2014/03/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspExitChatFilter]
	 @pOwnerPcNo		INT
	 ,@pPcNo			INT
	 ,@pChatFilterPcNm	CHAR(12)	-- 傈崔牢磊 沥焊 函版 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DELETE dbo.TblPcChatFilter
	WHERE (mOwnerPcNo=@pOwnerPcNo) 
			AND ( mChatFilterPcNm = @pChatFilterPcNm ) 
	IF(1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END
		
	RETURN(0)

GO

