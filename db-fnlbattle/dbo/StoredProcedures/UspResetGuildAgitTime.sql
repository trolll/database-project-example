CREATE PROCEDURE [dbo].[UspResetGuildAgitTime]
	 @pTerritory	INT		-- 康瘤锅龋
	,@pOwnerGID		INT		-- 家蜡辨靛
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	-- 辨靛啊 秦寸 康瘤俊 辨靛酒瘤飘甫 啊瘤绊 乐绰瘤 眉农
	IF NOT EXISTS(	SELECT mOwnerGID 
					FROM dbo.TblGuildAgit WITH (NOLOCK) 
					WHERE mTerritory = @pTerritory 
						AND mOwnerGID = @pOwnerGID)
	BEGIN
		RETURN(2)			-- 2 : 辨靛酒瘤飘啊 粮犁窍瘤 臼澜
	END

	-- 辨靛酒瘤飘 措咯扁埃 檬扁拳
	UPDATE dbo.TblGuildAgit 
	SET mLeftMin = 0 
	WHERE mTerritory = @pTerritory 
		AND mOwnerGID = @pOwnerGID
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)	-- db error
	END	 
	
	
	RETURN(0)

GO

