CREATE Procedure [dbo].[UspGetGuildAgitList]		-- 辨靛葛烙 府胶飘 掘扁
	 @pTerritory		INT			-- 康瘤锅龋
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	SELECT 
		 mTerritory
		,mGuildAgitNo
		,mOwnerGID
		,ISNULL((CASE mOwnerGID
			WHEN 0 THEN N'<NONE>'
			ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = mOwnerGID)
			END), N'<FAIL>') As mOwnerGuildName
		,mGuildAgitName
		,mLeftMin 
	FROM TblGuildAgit WITH(NOLOCK) 
	WHERE mTerritory = @pTerritory

	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo = 1	-- 1: DB 郴何利牢 坷幅
	END
	
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

