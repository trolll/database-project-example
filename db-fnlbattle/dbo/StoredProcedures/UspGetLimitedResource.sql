CREATE PROCEDURE [dbo].[UspGetLimitedResource]
AS
	SET NOCOUNT ON
	
	SELECT 
		mRegDate,
		mResourceType,
		mMaxCnt,
		mRemainerCnt,
		mRandomVal,
		mUptDate	
	FROM dbo.TblLimitedResource

GO

