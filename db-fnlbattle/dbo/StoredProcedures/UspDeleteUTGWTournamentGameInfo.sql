/******************************************************************************
**		Name: UspDeleteUTGWTournamentGameInfo
**		Desc: 配呈刚飘 霸烙 沥焊甫 昏力茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteUTGWTournamentGameInfo]
	  @pRound		INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aRound	INT;
	
	-- 秦寸 沥焊啊 甸绢乐绰 Round 傈眉甫 瘤款促.
	DELETE	dbo.TblUnitedGuildWarTournamentInfo	
	WHERE [mRound] = @pRound;

GO

