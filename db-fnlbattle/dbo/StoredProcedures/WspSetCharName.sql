CREATE PROCEDURE [dbo].[WspSetCharName]
	@pNo			INT,			-- 某腐磐 锅龋
	@pOrgPcNm		CHAR(12),	-- 函版傈 捞抚
	@pChgPcNm		CHAR(12)	-- 函版饶 捞抚
AS
BEGIN
	SET NOCOUNT ON
	SET LOCK_TIMEOUT 2000
		
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErr = 0
		
	------------------------------------------------------------------------------		
	-- 函版且 某腐磐 粮犁咯何 眉农
	------------------------------------------------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblPc WITH(NOLOCK)
				WHERE mNm = @pChgPcNm)
	BEGIN			
		RETURN(1)					-- 函版且 某腐磐啊 粮犁茄促.
	END
		
	------------------------------------------------------------------------------		
	-- 某腐磐疙 函版
	------------------------------------------------------------------------------
	BEGIN TRAN
	
		UPDATE dbo.TblPc
		SET mNm = @pChgPcNm
		WHERE mNo =@pNo 
		
		SELECT	@pRowCnt = @@ROWCOUNT, 
				@pErr  = @@ERROR			

		IF @pRowCnt = 0
		BEGIN
			ROLLBACK TRAN			-- 某腐磐啊 粮犁窍瘤 臼芭唱, 盎脚等 沥焊啊 绝促
			RETURN(2)			
		END 

		IF @pErr <> 0
		BEGIN
			ROLLBACK TRAN			
			RETURN(@pErr)			-- SQL System Error		
		END
	
	COMMIT TRAN
	RETURN(0)
END

GO

