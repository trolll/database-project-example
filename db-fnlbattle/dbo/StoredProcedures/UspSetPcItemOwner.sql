CREATE PROCEDURE [dbo].[UspSetPcItemOwner]
	  @pSerial		BIGINT		-- 豪牢 棺 秦力甫 寸且 酒捞袍狼 矫府倔锅龋
	, @pIsSeal	BIT		-- 豪牢 咯何(1 捞搁 豪牢, 0捞搁 秦力)
	, @pOwner	INT	OUTPUT	-- 酒捞袍 家蜡林 锅龋.
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	@aItemOwner INT	
	SELECT	@pOwner = 0, @aItemOwner = 0

	SELECT
	 	  @pOwner 	= T2.mOwner 	--(T2 绰 TblPc捞促. TblUser 狼 mUserNo捞促.)
		, @aItemOwner 	= T1.mOwner	--(TblPcInventory 狼 mOwner捞促.) 
	FROM dbo.TblPcInventory T1
		INNER JOIN dbo.TblPc T2
			ON T2.mNo = T1.mPcNo 
				AND T2.mDelDate IS NULL		
	WHERE mSerialNo = @pSerial

	IF(@pIsSeal = 1)	
	BEGIN
		-- 家蜡且 拌沥狼 沥焊啊 乐绊 酒捞袍狼 家蜡磊啊 绝绰 版快俊 豪牢矫挪促.
		IF(@pOwner <> 0 AND @aItemOwner = 0) 
		BEGIN
			-- 豪牢矫挪促. (Auto commit)
			UPDATE dbo.TblPcInventory
			SET mOwner = @pOwner
			WHERE mSerialNo = @pSerial
			
			IF(@@ERROR <> 0)
			BEGIN
				RETURN(2) -- DB Err
			END
		END
		ELSE
		BEGIN
			-- 豪牢阑 窍绰单 家蜡林 沥焊啊 绝芭唱 豪牢酒捞袍捞 酒聪促.
			RETURN(3)
		END
	END
	ELSE
	BEGIN
		-- 酒捞袍狼 泅 家蜡林 沥焊啊 乐绢具 豪牢阑 秦力 茄促.
		IF(@aItemOwner <> 0)
		BEGIN
			-- 豪牢 秦力 矫挪促. (Auto commit)
			UPDATE dbo.TblPcInventory
			SET mOwner = 0
			WHERE mSerialNo = @pSerial
			
			IF(@@ERROR <> 0)
			 BEGIN
				RETURN(4) -- DB Err
			 END
		END
		ELSE
		BEGIN			
			RETURN(5)	-- 豪牢 酒捞袍牢单 家蜡林啊 绝促?
		END
	 END

	RETURN(0)

GO

