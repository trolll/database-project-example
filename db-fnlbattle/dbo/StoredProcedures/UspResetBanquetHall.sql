CREATE Procedure [dbo].[UspResetBanquetHall]		-- 漂沥 楷雀厘狼 沥焊甫 府悸
	 @pTerritory		INT			-- 康瘤锅龋
	,@pBanquetHallType	INT			-- 楷雀厘鸥涝 (0:家.楷雀厘/1:措.楷雀厘)
	,@pBanquetHallNo	INT			-- 楷雀厘锅龋
	,@pLeftMin		INT			-- 父丰盒
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	BEGIN TRAN

	-- 楷雀厘 沥焊 府悸
	UPDATE TblBanquetHall 
	SET mOwnerPcNo = 0, mLeftMin = @pLeftMin, mRegDate = getdate()
	WHERE mTerritory = @pTerritory AND mBanquetHallType = @pBanquetHallType AND mBanquetHallNo = @pBanquetHallNo
	
	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END			 
	END	 
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

