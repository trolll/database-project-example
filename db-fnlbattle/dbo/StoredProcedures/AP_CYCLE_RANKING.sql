/******************************************************************************
**		Name: AP_CYCLE_RANKING
**		Desc: RANKING 魂沥
**				AM 6:00	, PM 6:00 SQL JOB AGENT
**				傈厘 扁咯档, 荐龋脚 痢飞, 捞悼器呕 痢飞, PVP 珐欧 葛滴 焊蜡器牢飘啊 
**				10器牢飘 捞惑何磐 免仿
**				捞抚 函版矫 珐农俊 7老埃 给 甸绢埃促.
**		Auth: JUDY
**		Date: 2009.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      2009.04.29	林叼				昏力等 某腐磐 力芭         
*******************************************************************************/
CREATE PROCEDURE [dbo].[AP_CYCLE_RANKING]  	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	if exists ( select * from sysobjects where name ='t_ranking' and type = 'u')
	begin
		drop table t_ranking
	end 

	select T1.mPcNo, mContribution, mGuardian, mTeleportTower, mPVP
		, T2.mFieldSvrNo
		, T2.mNm
		, T2.mChgNmDate
	into dbo.t_ranking	
	from dbo.TblPcRankingPoint t1
		inner join dbo.TblChaosBattlePc t2
			on t1.mPcNo = t2.mNo
	where mDelDate IS NULL			

	create clustered index ucl_t_ranking_mpcno on dbo.t_ranking( mPcNo )
	create index nc_t_ranking_mContribution on dbo.t_ranking( mContribution DESC )
	create index nc_t_ranking_mGuardian on dbo.t_ranking( mGuardian DESC )
	create index nc_t_ranking_mTeleportTower on dbo.t_ranking( mTeleportTower DESC )
	create index nc_t_ranking_mPVP on dbo.t_ranking( mPVP DESC )
	
	-- 捞傈 珐欧 沥焊甫 檬扁拳 茄促.
	truncate table TblRanking

	INSERT INTO dbo.TblRanking
	SELECT 
		TOP 100
		
		GETDATE() mRegDate
		, mRank mRanking
		, 1 mRankType	
		, mPcNo
		, mNm mPcNm
		, SvrNo
		, mContribution mPoint
	FROM (
		SELECT 
			TOP 1000
			mPcNo	
			, mNm
			, mFieldSvrNo SvrNo
			, mContribution
			, mRank = (
				SELECT COUNT(DISTINCT mContribution)
				FROM dbo.t_ranking WITH (INDEX = nc_t_ranking_mContribution, TABLOCKX)
				WHERE mContribution >= T1.mContribution
					AND mContribution > = 10
					AND (  mChgNmDate <= DATEADD(DD, -7, GETDATE()) OR mChgNmDate IS NULL )
			 )
		FROM dbo.t_ranking  T1 WITH (INDEX = nc_t_ranking_mContribution, TABLOCKX)
		WHERE mContribution >= 10
			AND (  mChgNmDate <= DATEADD(DD, -7, GETDATE()) OR mChgNmDate IS NULL )	
		ORDER BY mRank ASC ) T1
	WHERE mRank <= 100
	ORDER BY mRank ASC

	INSERT INTO dbo.TblRanking
	SELECT 
		TOP 100
		
		GETDATE() mRegDate
		, mRank mRanking
		, 2 mRankType	
		, mPcNo
		, mNm mPcNm
		, SvrNo
		, mGuardian mPoint
	FROM (
		SELECT 
			TOP 1000
			mPcNo	
			, mNm
			, mFieldSvrNo SvrNo
			, mGuardian
			, mRank = (
				SELECT COUNT(DISTINCT mGuardian)
				FROM dbo.t_ranking WITH (INDEX = nc_t_ranking_mGuardian, TABLOCKX)
				WHERE mGuardian >= T1.mGuardian
					AND mGuardian > = 10
					AND (  mChgNmDate <= DATEADD(DD, -7, GETDATE()) OR mChgNmDate IS NULL )
			 )
		FROM dbo.t_ranking  T1 WITH (INDEX = nc_t_ranking_mGuardian, TABLOCKX)
		WHERE mGuardian >= 10
			AND (  mChgNmDate <= DATEADD(DD, -7, GETDATE()) OR mChgNmDate IS NULL )	
		ORDER BY mRank ASC ) T1
	WHERE mRank <= 100
	ORDER BY mRank ASC

	INSERT INTO dbo.TblRanking
	SELECT 
		TOP 100
		
		GETDATE() mRegDate
		, mRank mRanking
		, 3 mRankType	
		, mPcNo
		, mNm mPcNm
		, SvrNo
		, mTeleportTower mPoint
	FROM (
		SELECT 
			TOP 1000
			mPcNo	
			, mNm
			, mFieldSvrNo SvrNo
			, mTeleportTower
			, mRank = (
				SELECT COUNT(DISTINCT mTeleportTower)
				FROM dbo.t_ranking WITH (INDEX = nc_t_ranking_mTeleportTower, TABLOCKX)
				WHERE mTeleportTower >= T1.mTeleportTower
					AND mTeleportTower > = 10
					AND (  mChgNmDate <= DATEADD(DD, -7, GETDATE()) OR mChgNmDate IS NULL )
			 )
		FROM dbo.t_ranking  T1 WITH (INDEX = nc_t_ranking_mTeleportTower, TABLOCKX)
		WHERE mTeleportTower >= 10
			AND (  mChgNmDate <= DATEADD(DD, -7, GETDATE()) OR mChgNmDate IS NULL )	
		ORDER BY mRank ASC ) T1
	WHERE mRank <= 100
	ORDER BY mRank ASC

	INSERT INTO dbo.TblRanking
	SELECT 
		TOP 100
		
		GETDATE() mRegDate
		, mRank mRanking
		, 4 mRankType	
		, mPcNo
		, mNm mPcNm
		, SvrNo
		, mPVP mPoint
	FROM (
		SELECT 
			TOP 1000
			mPcNo	
			, mNm
			, mFieldSvrNo SvrNo
			, mPVP
			, mRank = (
				SELECT COUNT(DISTINCT mPVP)
				FROM dbo.t_ranking WITH (INDEX = nc_t_ranking_mPVP, TABLOCKX)
				WHERE mPVP >= T1.mPVP
					AND mPVP > = 10
					AND (  mChgNmDate <= DATEADD(DD, -7, GETDATE()) OR mChgNmDate IS NULL )
			 )
		FROM dbo.t_ranking  T1 WITH (INDEX = nc_t_ranking_mPVP, TABLOCKX)
		WHERE mPVP >= 10
			AND (  mChgNmDate <= DATEADD(DD, -7, GETDATE()) OR mChgNmDate IS NULL )	
		ORDER BY mRank ASC ) T1
	WHERE mRank <= 100
	ORDER BY mRank ASC

GO

