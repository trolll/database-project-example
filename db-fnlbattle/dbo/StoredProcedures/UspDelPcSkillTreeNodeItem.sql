/******************************************************************************  
**  File: 
**  Name: UspDelPcSkillTreeNodeItem  
**  Desc: PC狼 胶懦飘府畴靛酒捞袍 窍唱甫 昏力茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDelPcSkillTreeNodeItem]
	@pPcNo		INT,
	@pSTNIID	INT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF @pPcNo = 1 OR @pPcNo = 0 
	BEGIN
		RETURN(0)	-- NPC, 滚妨柳 酒捞袍
	END

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	IF 0 = @pSTNIID 
		BEGIN
			DELETE FROM dbo.TblPcSkillTreeInventory
			WHERE mPcNo = @pPcNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1		-- SQL Error
			END
		END
	ELSE
		BEGIN
			DELETE FROM dbo.TblPcSkillTreeInventory
			WHERE mPcNo = @pPcNo AND mSTNIID = @pSTNIID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1		-- SQL Error
			END
		END

	RETURN @aErrNo

GO

