CREATE PROCEDURE [dbo].[UspUptItemDate]  	
	@pMI	INT			-- 楷厘 矫埃(盒)
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	BEGIN TRAN
		
		-- inventory
		UPDATE dbo.TblPcInventory
		SET mEndDate
				= DATEADD(mi, @pMI, mEndDate)
		WHERE mEndDate < CONVERT(SMALLDATETIME, '2079-01-01')
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END

		-- store
		UPDATE dbo.TblPcStore
		SET mEndDate
				= DATEADD(mi, @pMI, mEndDate)
		WHERE mEndDate < CONVERT(SMALLDATETIME, '2079-01-01')
			
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END		
		
		-- Guild Store
		UPDATE dbo.TblGuildStore
		SET mEndDate
				= DATEADD(mi, @pMI, mEndDate)
		WHERE mEndDate < CONVERT(SMALLDATETIME, '2079-01-01')
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END				
		
		-- Pc Gold Item Effect
		UPDATE dbo.TblPcGoldItemEffect
		SET mEndDate
				= DATEADD(mi, @pMI, mEndDate)
				
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END					
		
		-- Giold Gold Item Effect
		UPDATE dbo.TblGuildGoldItemEffect
		SET mEndDate
				= DATEADD(mi, @pMI, mEndDate)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END				
					
	COMMIT TRAN					
	RETURN(0)

GO

