CREATE         PROCEDURE [dbo].[WspGetPetitionEx]
	@mFlag		INT,	-- 敲贰弊蔼 (0捞搁 固贸府 格废, 1 捞搁 葛电格废)
	@in_begin_date	CHAR(8),
	@in_end_date	CHAR(8)

AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @mFlag = 0
	BEGIN
		SELECT 
			TOP 1000
			  pe.mRegDate
			, pe.mText
			, pe.mIsFin
			, p.mNm
			, ca.mContent
			, pe.mPID
			, pe.mIpAddress
			, de.mPcNm
		FROM
			[dbo].[TblPetitionBoard]AS pe
		INNER JOIN
			dbo.TblChaosBattlePc AS p
		ON
			pe.mPcNo = p.mNo
		FULL OUTER JOIN
			[dbo].[TblPcDeleted] AS de
		ON
			p.mDelDate IS NOT NULL AND p.mNo = de.mPcNo 
		INNER JOIN
			[FNLParm].[dbo].[TblPetitionCategory] AS ca
		ON
			pe.mCategory = ca.mCategory
		WHERE
			pe.mRegDate >= CONVERT(DATETIME, @in_begin_date )
				AND
			pe.mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_End_Date ) )
				AND
			pe.mIsFin = 0
		ORDER BY
 			pe.mRegDate
		DESC
	END
	
	ELSE
	BEGIN
		SELECT 
			TOP 1000
			  pe.mRegDate
			, pe.mText
			, pe.mIsFin
			, p.mNm
			, ca.mContent
			, pe.mPID
			, pe.mIpAddress
			, de.mPcNm
		FROM
			[dbo].[TblPetitionBoard]AS pe
		INNER JOIN
			[dbo].TblChaosBattlePc AS p
		ON
			pe.mPcNo = p.mNo
		FULL OUTER JOIN
			[dbo].[TblPcDeleted] AS de
		ON
			p.mDelDate IS NOT NULL AND p.mNo = de.mPcNo 
		INNER JOIN
			[FNLParm].[dbo].[TblPetitionCategory] AS ca
		ON
			pe.mCategory = ca.mCategory
		WHERE
			pe.mRegDate >= CONVERT(DATETIME, @in_begin_date )
				AND
			pe.mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_End_Date ) )

		ORDER BY
 			pe.mRegDate
		DESC
	END

	SET NOCOUNT OFF

GO

