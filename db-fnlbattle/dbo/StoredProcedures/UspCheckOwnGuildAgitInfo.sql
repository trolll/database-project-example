CREATE PROCEDURE [dbo].[UspCheckOwnGuildAgitInfo]
	@pTerritory	INT,
	@pGuildAgitNo	INT,
	@pGuildNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	IF EXISTS ( SELECT * 
				FROM dbo.TblGuildAgit 
				WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo AND mOwnerGID = @pGuildNo)
	 BEGIN
		RETURN(0);	-- 郴 辨靛 家蜡狼 酒瘤飘捞促.
	 END
	ELSE
	 BEGIN
		RETURN(1);	-- 郴 辨靛 家蜡狼 酒瘤飘啊 酒聪促.
	 END

GO

