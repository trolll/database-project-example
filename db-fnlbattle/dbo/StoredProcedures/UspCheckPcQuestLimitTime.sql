/******************************************************************************
**		Name: UspCheckPcQuestLimitTime
**		Desc: 涅胶飘 矫埃力茄 眉农
**
**		Auth: 沥柳龋
**		Date: 2010-02-12
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspCheckPcQuestLimitTime]
	  @pPcNo		INT
	 ,@pQuestNo		INT
	 ,@pLimitTime	INT	OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @pLimitTime = 0;
			
    SELECT 
			@pLimitTime = 
					CASE WHEN mLimitTime IS NULL THEN 0 
						ELSE DATEDIFF(ss,GETDATE(),  mLimitTime )	
					END 
    FROM dbo.TblPcQuest
    WHERE mPcNo = @pPcNo 
		AND mQuestNo = @pQuestNo

	RETURN(0)

GO

