CREATE PROCEDURE [dbo].[WspSetGuildName]
	 @pGuildNo		INT
	,@pGuildNm		CHAR(12)	
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000	-- 2檬 捞郴俊 累诀捞 场唱瘤 臼栏搁 1222 坷幅甫 馆券
	
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErr = 0
		
		
	--------------------------------------------------------------------------
	-- 吝汗等 辨靛 粮犁咯何 眉农 
	--------------------------------------------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblGuild WITH(NOLOCK)
				WHERE mGuildNm = @pGuildNm )
	BEGIN		
		RETURN(1)				-- 捞固 殿废等 辨靛疙捞促.
	END

	--------------------------------------------------------------------------
	-- 辨靛疙 函版 
	--------------------------------------------------------------------------			
	BEGIN TRAN
			
		UPDATE dbo.TblGuild	
		SET 	
			mGuildNm = @pGuildNm
		WHERE  [mGuildNo] = @pGuildNo

		SELECT	@pRowCnt = @@ROWCOUNT, 
				@pErr  = @@ERROR	

		IF @pRowCnt = 0
		BEGIN				
			ROLLBACK TRAN		-- 辨靛啊 粮犁窍瘤 臼绰促.		
			RETURN(2)
		END
		
		IF @pErr <> 0
		BEGIN				
			ROLLBACK TRAN			
			RETURN(@pErr)		-- System Error.
		END

	COMMIT TRAN
	RETURN(0)	-- NOT ERROR

GO

