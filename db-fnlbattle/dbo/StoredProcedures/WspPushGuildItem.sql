CREATE PROCEDURE [dbo].[WspPushGuildItem]  
	@pGuildNo INT, -- 辨靛 锅龋
	@pItemNo INT, -- 酒捞袍 锅龋 (TID)
	@pStatus TINYINT, -- 酒捞袍 惑怕
	@pCnt INT, -- 酒捞袍 荐樊
	@pIsConfirm BIT, -- 犬牢咯何
	@pGuildGrade	TINYINT = 0,	-- 芒绊 饭骇 1肺 殿废
	@pTargetSn BIGINT OUTPUT,
	@pValidDay INT = NULL, -- 蜡瓤扁埃	
	@pPraticalHour INT = NULL,
	@pBindingType	TINYINT = 0		-- BIDING TYPE OFF
AS
	SET NOCOUNT ON
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
		 	
	DECLARE @pCntUse TINYINT
	DECLARE @pIsStack BIT,
			@pIsPracticalPeriod BIT

	SELECT @pTargetSn = 0, @pIsPracticalPeriod = 0
	

	IF @pValidDay IS NULL
		SELECT
			@pValidDay = ITermOfValidity
			, @pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
		FROM
			FNLParm.[dbo].[DT_Item]
		WHERE
			IID = @pItemNo
	ELSE
		SELECT
			@pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
		FROM
			FNLParm.[dbo].[DT_Item]			
		WHERE
			IID = @pItemNo
	

	-- 绝绰 TID
	IF @pValidDay IS NULL OR @pIsStack IS NULL
		RETURN (2)
		 	
	DECLARE	@aTargetSn		BIGINT	
	DECLARE	@aCntTarget		INT
	SET		@aCntTarget = 0		
	DECLARE	@aIsSeizure		BIT	
	
	-- 捣.
	IF(@pItemNo = 409)
	 BEGIN
		SET		@pIsConfirm = 1
		SET		@pStatus = 1
	 END
	
	-- 父丰老 
	DECLARE	@aEndDay	SMALLDATETIME
	SET		@aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)
		
	-- 瓤苞 瘤加 矫埃
	IF @pIsPracticalPeriod = 1 AND ( @pPraticalHour IS NULL OR @pPraticalHour <= 0)
		RETURN 3
	
	IF @pPraticalHour > (30*24) 
		RETURN 4
		
	IF @pPraticalHour IS NULL
		SET @pPraticalHour = 0
			
	
	IF @pIsStack = 1	
	BEGIN
		-- 胶琶屈狼 版快 穿利且 酒捞袍阑 茫绰促.
		SELECT 
			@aCntTarget=ISNULL(mCnt,0), 
			@aTargetSn=mSerialNo, 
			@aIsSeizure=ISNULL(mIsSeizure,0)
		FROM dbo.TblGuildStore
		WHERE mGuildNo = @pGuildNo
				AND mItemNo = @pItemNo
				AND mIsConfirm = @pIsConfirm
				AND mStatus = @pStatus

		IF(0 <> @@ERROR)
		BEGIN
			RETURN(4)
		END
			
		IF(0 <> @aIsSeizure)
		BEGIN
			RETURN(5)	 
		END	
	END
	
	--------------------------------------------------------------
	-- 酒捞袍阑 积己窍芭唱 穿利茄促. 
	--------------------------------------------------------------
	IF @aTargetSn > 0
	BEGIN
		-- 穿利且 措惑 粮犁 
		SET @pTargetSn = @aTargetSn
		
		UPDATE dbo.TblGuildStore 
		SET 
			mCnt = mCnt + @pCnt
		WHERE mSerialNo = @aTargetSn
				AND mItemNo = @pItemNo
		IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
		BEGIN
			RETURN(6)
		END		
				
		RETURN(0)
	END 
	
	EXEC @pTargetSn = dbo.UspGetItemSerial 
	IF @pTargetSn <= 0 
	BEGIN
		RETURN(7)
	END 	
			
	-- 芒绊狼 脚痹 酒捞袍 殿废 
	INSERT INTO dbo.TblGuildStore (mSerialNo, mGuildNo, mItemNo, mEndDate, mIsConfirm, 
		mStatus, mCnt, mCntUse, mGrade, mOwner, mPracticalPeriod, mBindingType )
	VALUES(
		@pTargetSn,
		@pGuildNo, 
		@pItemNo, 
		@aEndDay, 
		@pIsConfirm, 
		@pStatus, 
		@pCnt, 
		0	-- UsedCount (积己篮 0栏肺 技泼 茄促)
		, @pGuildGrade
		, 0
		, @pPraticalHour
		, @pBindingType
	)	
	
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(8)
	END
	
	RETURN(0)

GO

