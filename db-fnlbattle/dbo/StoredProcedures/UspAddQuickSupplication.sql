CREATE PROCEDURE [dbo].[UspAddQuickSupplication]
	  @pPcNo		INT
	, @pCate		TINYINT
	, @pSupplication	VARCHAR(1000)
	, @pPosX		FLOAT
	, @pPosY		FLOAT
	, @pPosZ		FLOAT
	, @pIp			VARCHAR(20)
AS
	SET NOCOUNT ON		
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	DECLARE @aQSID		BIGINT
	DECLARE	@aTime		DATETIME
	SELECT	@aErrNo = 0, @aQSID = 0, @aTime = '1900-01-01 00:00:00'

	-- 翠函殿废捞 登绢乐瘤 臼篮 龙巩捞 乐绰版快 犁 殿废捞 阂啊瓷窍促.
	IF EXISTS ( SELECT mQSID 
				FROM dbo.TblQuickSupplicationState 
				WHERE mPcNo = @pPcNo 
						AND mStatus < 2 )
	BEGIN		
		SET @aErrNo = 1	-- 殿废等 龙巩捞 乐促.
		GOTO T_RETURN
	END

	BEGIN TRAN
		
		SET @aTime = GETDATE()
			
		-- 脚绊窃俊 殿废茄促.
		INSERT INTO dbo.TblQuickSupplicationState (mRegDate, mPcNo, mCategory) 
		VALUES (
			@aTime
			, @pPcNo
			, @pCate
		)
		
		IF(@@ERROR <> 0)
		BEGIN			
			SET @aErrNo = 2	-- DB Insert Error
			GOTO T_END
		END
		
		SELECT  @aQSID = @@IDENTITY

	
		-- 脚绊 郴侩阑 殿废 茄促. 			
		INSERT  INTO dbo.TblQuickSupplication (mQSID, mSupplication, mPosX, mPosY, mPosZ, mIp)
		VALUES (@aQSID, @pSupplication, @pPosX, @pPosY, @pPosZ, @pIp)
		
		IF(@@ERROR <> 0)
		BEGIN
			SET @aErrNo = 3
			GOTO T_END
		END
			
T_END:
	IF ( @aErrNo = 0)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN

	 
T_RETURN:
	SELECT @aErrNo, @aQSID, @aTime

GO

