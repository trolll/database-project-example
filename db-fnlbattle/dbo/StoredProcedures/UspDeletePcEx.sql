/******************************************************************************
**		File: UspDeletePcEx.sql
**		Name: UspDeletePcEx
**		Desc: 某腐磐 昏力
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      20061107	milkji				guild啊涝咯何 犬牢矫 坷幅惯积 措厚 & NOLOCK 眠啊.
**		20061110	judy				PC 八祸 何盒 BEGIN TRAN 困摹 捞悼(飘罚黎记阑 陋霸 蜡瘤窍扁 困窃)
**		20061208	JUDY				某腐磐 昏力俊 蝶福绰 包访 单捞鸥 昏力
**		20070705	b4nfter			荤力 葛烙 啊涝登绢 乐栏搁 费归 贸府
**		20080131	JUDY				某腐磐 昏力 冉荐 力茄 扁瓷 眠啊	
**		20080226	JUDY				荤力 葛烙 眉农
**		20100121  dmbkh			困殴魄概 眉农 扁瓷 眠啊
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspDeletePcEx]
	 @pOwner		INT
	,@pPcNo			INT
	,@pNm			CHAR(12)	OUTPUT
	,@pGuildNo		INT			OUTPUT	-- 啊涝茄 辨靛锅龋. 绝栏搁 TGuildNoDef(0)
	,@pGuildGrade	TINYINT		OUTPUT	-- EGuildGrade. 啊涝茄 辨靛啊 乐阑 锭父 狼固 乐澜.
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
			, @aDelFlag	INT
			, @aRowCnt	INT

	SELECT	@aErrNo = 0, @aRowCnt = 0, @aDelFlag = 1

	--------------------------------------------------------------- 
	-- 某腐磐 眉农 
	---------------------------------------------------------------			 
	SELECT 
		@pNm=[mNm] 
	FROM dbo.TblPc
	WHERE ([mOwner]=@pOwner) 
			AND ([mNo]=@pPcNo) 
			AND ([mDelDate] IS NULL)
	IF(1 <> @@ROWCOUNT)
	BEGIN 
		RETURN(1)
	END
	 
	--------------------------------------------------------------- 
	-- 某腐磐 昏力 眉农 
	-- 昏力矫埃 24矫埃 捞郴, 昏力 冉荐 力茄 5雀
	---------------------------------------------------------------			 
	SELECT 
		@aDelFlag =
		CASE 
			WHEN DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE()) <= 24 
				AND mDelCntOfPc > 4 THEN 5	-- max 荐摹
			WHEN DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE()) <= 24 
				THEN mDelCntOfPc	
			WHEN DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE()) > 24 	
				THEN 1	-- Delete Clear
		END  
	FROM dbo.TblPcOfAccount
	WHERE mOwner = @pOwner
	
	IF ( @aDelFlag >= 5 )
	BEGIN
		RETURN(7)	-- 昏力 冉荐 檬苞
	END 
	 
	--------------------------------------------------------------- 
	-- 辨靛 啊涝 沥焊 眉农 
	---------------------------------------------------------------			 
	SELECT 
		@pGuildNo=[mGuildNo], 
		@pGuildGrade=[mGuildGrade] 
	FROM dbo.TblGuildMember
	WHERE [mPcNo] = @pPcNo
	IF(0 <> @@ROWCOUNT)
	BEGIN
		RETURN(2)
	END	 
	
	 ---------------------------------------------------------------   
	 -- 困殴魄概 殿废 眉农 (2011-01-21:dmbkh)
	 ---------------------------------------------------------------          
		IF EXISTS(SELECT mCnsmNo
			FROM dbo.TblConsignment
			WHERE mPcNo = @pPcNo)
	BEGIN
		RETURN(8)
	END

	 ---------------------------------------------------------------   
	 -- 困殴魄概 沥魂陛 眉农 (2011-01-21:dmbkh)
	 ---------------------------------------------------------------          	
	DECLARE @aCnsmBalance BIGINT
	SELECT @aCnsmBalance=mBalance
	FROM dbo.TblConsignmentAccount
	WHERE mPcNo=@pPcNo
	
	IF @aCnsmBalance IS NOT NULL AND @aCnsmBalance > 0
	BEGIN
		RETURN(8)
	END
	    	
	
	--------------------------------------------------------------- 
	-- 荤力葛烙 啊涝 沥焊 眉农 (2007-07-05:b4nfter) 
	---------------------------------------------------------------			 
	-- 1:荤何,2:力磊
	IF EXISTS(	SELECT mMaster 
					FROM dbo.TblDiscipleMember
					WHERE mDisciple = @pPcNo
							AND mType BETWEEN 1 AND 2 )
	BEGIN
		RETURN(6)
	END	
		
	BEGIN TRAN
	
		--------------------------------------------------------------
		-- 荤力 葛烙 措惑 昏力(措扁, 绝澜)
		--------------------------------------------------------------
		DELETE dbo.TblDiscipleMember
		WHERE mDisciple = @pPcNo
		IF( @@ERROR <> 0)
		BEGIN
			SET  @aErrNo = 3
			GOTO LABEL_END
		END			
		
	
	   	--------------------------------------------------------------- 
		-- 模备 格废 昏力
		---------------------------------------------------------------			
		DELETE dbo.TblPcFriend
		WHERE mOwnerPcNo = @pPcNo
		IF( @@ERROR <> 0)
		BEGIN
			SET  @aErrNo = 3
			GOTO LABEL_END
		END	
			
	   	--------------------------------------------------------------- 
		-- 某腐磐 昏力 沥焊 殿废
		---------------------------------------------------------------			
		INSERT dbo.TblPcDeleted([mPcNo], [mPcNm]) 
		SELECT @pPcNo, @pNm
		IF( @@ERROR <> 0)
		BEGIN
			SET  @aErrNo = 4
			GOTO LABEL_END
		END
	   	
	   	--------------------------------------------------------------- 
		-- 茄锅 昏力等 捞抚阑 犁荤侩且 荐 乐促.(2006-4-13:milkji)
		---------------------------------------------------------------
		UPDATE dbo.TblPc 
		SET 
			[mDelDate]=GETDATE(), 
			[mNm]=','+CONVERT(CHAR(11),[mNo])
		WHERE ([mNo]=@pPcNo) 
			AND ([mDelDate] IS NULL)
		IF( @@ERROR <> 0)
		BEGIN
			SET  @aErrNo = 5
			GOTO LABEL_END
		END

	   	--------------------------------------------------------------- 
		-- 昏力 冉荐 力茄 扁瓷 眠啊
		---------------------------------------------------------------			
		UPDATE dbo.TblPcOfAccount
		SET
			mDelDateOfPcDeleted = 				
				CASE 
					WHEN DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE() ) <= 24 THEN mDelDateOfPcDeleted
					ELSE
						GETDATE()
				END 
			, mDelCntOfPc =
				CASE 
					WHEN  DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE() ) <= 24 THEN mDelCntOfPc+1
					ELSE
						1
				END 							
		WHERE mOwner = @pOwner
		
		
		SELECT	@aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		IF( @@ERROR <> 0)
		BEGIN
			SET  @aErrNo = 5
			GOTO LABEL_END
		END		

		IF ( @aRowCnt = 0 )
		BEGIN	-- 积己 沥焊啊 绝促搁..		
			INSERT INTO dbo.TblPcOfAccount(mOwner)	VALUES(@pOwner)
				
			IF( @@ERROR <> 0)
			BEGIN
				SET  @aErrNo = 6
				GOTO LABEL_END
			END				
		END 

LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

