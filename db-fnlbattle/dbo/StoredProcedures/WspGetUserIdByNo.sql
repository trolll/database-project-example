CREATE     PROCEDURE [dbo].[WspGetUserIdByNo] 
	@mNo	INT
AS
	SET NOCOUNT ON

	SELECT
		mUserNo
	FROM
		[dbo].[TblPcStore]
	WITH ( NOLOCK )
	WHERE
		mUserNo = @mNo

	SET NOCOUNT OFF

GO

