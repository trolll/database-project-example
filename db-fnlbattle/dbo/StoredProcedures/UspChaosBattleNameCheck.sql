/******************************************************************************
**		Name: UspChaosBattleNameCheck
**		Desc: 墨坷胶硅撇 PC 肺弊牢傈 肺弊牢阑 夸没茄 PC狼 捞抚俊 措茄 贸府甫 茄促. (霸烙辑滚俊辑 捞抚阑 函版窍绰 版快 墨坷胶硅撇俊辑绰 舅 荐 绝栏骨肺)
**
**		Auth: 辫 堡挤
**		Date: 2009-08-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspChaosBattleNameCheck]
	  @pFieldSvrNo	INT
	, @pFieldPcNo	INT
	, @pFieldNm		CHAR(12)
	, @pOwner		INT
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	DECLARE		@aErrNo			INT;
	DECLARE		@aRowCnt		INT;
	DECLARE		@aFieldPcNo		INT
				, @aNo			INT;
	
	SELECT	@aErrNo = 0, @aRowCnt = 0;
	
	SELECT
		  @aFieldPcNo	= mFieldSvrPcNo
		  , @aNo = [mNo]
	FROM	dbo.TblChaosBattlePc
	WHERE	[mFieldSvrNo] = @pFieldSvrNo 
				AND [mNm] = @pFieldNm;

	SELECT	@aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;
	
	IF(0 < @aRowCnt)
	 BEGIN
		-- 秦寸 辑滚俊 秦寸 捞抚捞 粮犁茄促搁, 泅犁 肺弊牢阑 夸没茄 PC客 悼老茄瘤 眉农茄促.
		IF(@aFieldPcNo <> @pFieldPcNo)
		 BEGIN
			-- 墨坷胶硅撇 DB 俊 秦寸 捞抚阑 家蜡茄 PC 啊 肺弊牢阑 夸没茄 PC 客 促弗 版快. (悼老茄 捞抚捞扼绊 秦辑 捞傈 PC啊 昏力灯促绊 杭 荐 绝促.)
			-- 迭洒 捞抚阑 函版且父茄霸 绝绢辑 昏力傍侥俊 蝶弗 捞抚栏肺 函版茄促. (蜡聪农且 荐 乐绰 捞抚)
			-- mChgNmDate 甫 刚 固贰肺 汲沥窍绰 捞蜡绰 秦寸 珐欧俊 眠啊窍瘤 臼栏妨绰 狼档烙. 秦寸 PC啊 肺弊牢阑 窍搁 促矫 捞抚捞 汲沥登绊 mChgNmDate 档 函版瞪 巴烙
			UPDATE	dbo.TblChaosBattlePc
			SET
				[mNm]		= ',' + CONVERT(CHAR(11), @aFieldPcNo)
				, [mChgNmDate]= DATEADD(YEAR, 20, GETDATE())
			WHERE	[mNo]	= @aNo;
			
			SELECT	@aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR
			IF((0 <> @aErrNo) OR (1 <> @aRowCnt))
			 BEGIN
				SET	@aErrNo = 10;
			 END
		 END
	 END
	
	RETURN(@aErrNo);

GO

