CREATE  PROCEDURE [dbo].[UspPushItemToGuildStore]
	 @pSn			BIGINT
	,@pCnt			INT		-- 历厘且 俺荐.
	,@pGuildNo		INT		-- 酒捞袍阑 傍蜡且 辨靛锅龋.
	,@pIsStack		BIT
	,@pGrade			TINYINT		
	,@pTargetSn		BIGINT OUTPUT	-- 烹钦等 SN锅龋.
AS
	SET NOCOUNT ON		
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	@aRowCnt			INT
	DECLARE	@aErrNo				INT
	DECLARE @aCnt				INT
	DECLARE @aItemNo			INT
	DECLARE @aIsConfirm			BIT
	DECLARE @aStatus			TINYINT
	DECLARE	@aCntUse			TINYINT
	DECLARE	@aPcNo				INT
	DECLARE	@aEndDate			SMALLDATETIME
	DECLARE @aLeftCnt			INT
	DECLARE @aRegDate			DATETIME
	DECLARE @aOwner				INT
	DECLARE @aPracticalPeriod	INT
	DECLARE	@aBindingType		TINYINT
	DECLARE	@aRestoreCnt		TINYINT
			

	SELECT @aRowCnt = 0, @aErrNo= 0

	------------------------------------------
	-- 牢亥配府 酒捞袍 眉农 
	------------------------------------------
	SELECT 
		@aLeftCnt=[mCnt],   
		@aCnt=[mCnt], 
		@aItemNo=[mItemNo], 
		@aIsConfirm=[mIsConfirm], 
		@aStatus=[mStatus], 
		@aCntUse=[mCntUse],
		@aPcNo=[mPcNo], 
		@aEndDate=[mEndDate],
		@aRegDate = [mRegDate],
		@aOwner = [mOwner],
		@aPracticalPeriod = [mPracticalPeriod],
		@aBindingType = mBindingType,
		@aRestoreCnt = mRestoreCnt	
	FROM dbo.TblPcInventory WITH(INDEX=PK_NC_TblPcInventory_1)
	WHERE  mSerialNo  =@pSn 
			AND mIsSeizure = 0

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo<> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END	
	END
	
	SET @aCnt = @aCnt - @pCnt
	IF @aCnt < 0
	BEGIN
		SET @aErrNo = 2
		GOTO T_END	
	END
	
	-- 官牢爹 眉农 (蓖加 惑怕老 版快 捞悼捞 例措 阂啊) 
	IF @aBindingType IN (1,2) 
	BEGIN
		SET @aErrNo = 11
		GOTO T_END	
	END
	 
	 
	SET @pTargetSn = @pSn
	
	------------------------------------------
	-- 胶琶屈捞 酒聪搁 弊成 函版父 窍搁等促.
	------------------------------------------	
	IF @pIsStack = 0
	BEGIN

		IF(1 <> @pCnt)
		BEGIN
			SET @aErrNo = 12	-- 胶琶屈捞 酒聪扼搁 肮荐绰 公炼扒 1捞绢具 茄促. 
			GOTO T_END		 
		END
	 
		BEGIN TRAN

			-- 芒绊肺 酒捞袍 捞悼 
			INSERT dbo.TblGuildStore(	
				[mRegDate],			
				[mSerialNo],
				[mGuildNo], 
				[mItemNo], 
				[mEndDate], 
				[mIsConfirm], 
				[mStatus], 
				[mCnt], 
				[mCntUse],
				[mOwner],
				[mGrade],
				[mPracticalPeriod], 
				[mBindingType],
				[mRestoreCnt])
			VALUES(
				@aRegDate,
				@pSn,
				@pGuildNo, 
				@aItemNo, 
				@aEndDate, 
				@aIsConfirm, 
				@aStatus, 
				@aLeftCnt, 
				@aCntUse,
				@aOwner,
				@pGrade,
				@aPracticalPeriod, 
				@aBindingType,
				@aRestoreCnt)		

			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF @aErrNo<> 0 OR @aRowCnt <= 0 
			BEGIN
				ROLLBACK TRAN
				SET @aErrNo = 3
				GOTO T_END	
			END

			-- 牢亥配府 酒捞袍 沥焊 昏力 
			DELETE dbo.TblPcInventory
			WHERE mSerialNo = @pSn

			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF @aErrNo<> 0 OR @aRowCnt <= 0 
			BEGIN
				ROLLBACK TRAN
				SET @aErrNo = 3
				GOTO T_END	
			END

		COMMIT TRAN
		GOTO T_END
	 END
	 
	------------------------------------------
	-- 胶琶屈篮 扁粮俊 悼老茄 巴捞 乐栏搁 穿利窍绊, 绝栏搁 泅犁狼 巴阑 函版茄促.	
	------------------------------------------	
	DECLARE	@aTargetSn		BIGINT	
	DECLARE	@aCntTarget		INT
	DECLARE	@aIsSeizure		BIT
	SET		@aCntTarget = 0			
		
	SELECT 
		@aCntTarget=ISNULL([mCnt],0), 
		@aTargetSn=[mSerialNo], 
		@aIsSeizure=ISNULL([mIsSeizure],0)
	FROM dbo.TblGuildStore WITH(INDEX=CL_TblGuildStore_1)
	WHERE mGuildNo =  @pGuildNo 
		AND mItemNo = @aItemNo 
		AND mIsConfirm = @aIsConfirm 
		AND mStatus =@aStatus 
		AND mOwner = @aOwner 
		AND mGrade = @pGrade 
		AND mPracticalPeriod = @aPracticalPeriod
		AND mBindingType = @aBindingType

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo<> 0
	BEGIN
		SET @aErrNo = 4
		GOTO T_END			 
	END
		
	IF @aIsSeizure <> 0
	BEGIN
		SET @aErrNo = 5
		GOTO T_END			 
	END	
	 

	------------------------------------------
	-- 酒捞袍 捞悼 
	------------------------------------------	
	BEGIN TRAN
		 	
		IF @aCntTarget = 0	-- 穿利且 巴捞 绝栏搁		
		BEGIN		
			IF @aCnt < 1		-- 葛滴甫 芒绊俊 该板促搁 
			BEGIN	
				
				-- 芒绊 殿废
				INSERT INTO dbo.TblGuildStore(
					[mRegDate],
					[mSerialNo],
					[mGuildNo], 
					[mItemNo], 
					[mEndDate], 
					[mIsConfirm], 
					[mStatus], 
					[mCnt], 
					[mCntUse],
					[mOwner],
					[mGrade],
					[mPracticalPeriod], 
					[mBindingType],
					[mRestoreCnt])
				VALUES(
					@aRegDate,
					@pSn,
					@pGuildNo,
					@aItemNo,
					@aEndDate,
					@aIsConfirm,
					@aStatus,
					@aLeftCnt,
					@aCntUse,
					@aOwner,
					@pGrade,
					@aPracticalPeriod, 
					@aBindingType,
					@aRestoreCnt)	
				
				SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo<> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErrNo= 6
					GOTO T_ERR
				END
	
				-- 牢亥配府俊 阶咯 乐绰 沥焊 昏力 
				DELETE dbo.TblPcInventory 
				WHERE mSerialNo = @pSn 
	
				SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo<> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErrNo= 6
					GOTO T_ERR	 
				END

			END
			ELSE
			BEGIN		-- 老何父 芒绊俊 该板促搁

				UPDATE dbo.TblPcInventory 
				SET 
					mCnt =mCnt - @pCnt 
				WHERE  mSerialNo  = @pSn

				SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo<> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErrNo= 7
					GOTO T_ERR			 
				END

				EXEC @pTargetSn =  dbo.UspGetItemSerial 
				IF @pTargetSn <= 0
				BEGIN
					SET @aErrNo= 7
					GOTO T_ERR	
				END 								

				INSERT dbo.TblGuildStore(				
					[mSerialNo],
					[mGuildNo], 
					[mItemNo], 
					[mEndDate], 
					[mIsConfirm], 
					[mStatus], 
					[mCnt], 
					[mCntUse],
					[mOwner],
					[mGrade],
					[mPracticalPeriod], 
					[mBindingType],
					[mRestoreCnt])
				VALUES(
					@pTargetSn,
					@pGuildNo, 
					@aItemNo, 
					@aEndDate, 
					@aIsConfirm, 
					@aStatus, 
					@pCnt, 
					@aCntUse,
					@aOwner,
					@pGrade,
					@aPracticalPeriod, 
					@aBindingType,
					@aRestoreCnt)	
				IF(0 <> @@ERROR)
				
				SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo<> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErrNo= 8
					GOTO T_ERR			 
				END		

			 END
		END
		ELSE
		BEGIN 			

			 -- 穿利且 巴捞 乐栏搁
			SET @pTargetSn = @aTargetSn

			UPDATE dbo.TblGuildStore 
			SET 
				mCnt = mCnt + @pCnt 
			WHERE  mSerialNo = @aTargetSn

			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF @aErrNo<> 0 OR 	@aRowCnt <= 0
			BEGIN
				SET @aErrNo= 7
				GOTO T_ERR			 
			END	 
			
			IF @aCnt < 1
			BEGIN -- 葛电 酒捞袍阑 波陈促搁 牢亥配府俊辑 昏力窃.
				DELETE dbo.TblPcInventory 
				WHERE  mSerialNo = @pSn

				SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo<> 0 OR @aRowCnt <= 0
				BEGIN
					SET @aErrNo= 10
					GOTO T_ERR				 
				END
			END
			ELSE
			BEGIN -- 老何 酒捞袍父 波陈栏搁 波辰 父怒 扁粮巴阑 皑家矫糯.
				UPDATE dbo.TblPcInventory 
				SET 
					mCnt = mCnt - @pCnt 
				WHERE mSerialNo = @pSn

				SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo<> 0 OR 	@aRowCnt <= 0
				BEGIN
					SET @aErrNo= 9
					GOTO T_ERR			 
				END	 
			END
		 END

T_ERR:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
T_END:			
			
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

