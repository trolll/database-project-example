/******************************************************************************
**		Name: UspUpdatePcQuestCondition
**		Desc: 某腐磐 涅胶飘 炼扒 诀单捞飘
**
**		Auth: 沥柳龋
**		Date: 2010-02-04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePcQuestCondition]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pMonNo   		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    UPDATE dbo.TblPcQuestCondition 
	SET mCnt = mCnt + 1     -- 1究 刘啊
	WHERE mPcNo = @pPcNo 
			AND mQuestNo = @pQuestNo
            AND mMonsterNo = @pMonNo
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(2)
	END
	 		 
	RETURN(0)

GO

