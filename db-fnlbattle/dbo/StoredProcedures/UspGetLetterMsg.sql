CREATE  PROCEDURE [dbo].[UspGetLetterMsg]
	 @pSerial		BIGINT				-- 祈瘤瘤 锅龋.
	,@pMsg			VARCHAR(2048) OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			
	
	DECLARE	 @aRowCnt		INT
				,@aErrNo		INT

	SELECT @aRowCnt = 0, @aErrNo = 0

	-----------------------------------------------------
	-- 祈瘤 郴侩 掘扁
	-----------------------------------------------------
	SELECT 
		@pMsg = mMsg
	FROM dbo.TblLetter
	WHERE mSerialNo = @pSerial
	
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0
	BEGIN
		SET  @aErrNo = 1
		GOTO T_END	 
	END
	 
	-----------------------------------------------------
	-- 祈瘤 惑怕 函版 
	-- eUnusedLetterItemID:721:荤侩救茄 祈瘤.
	-- eKnownLetterItemID :724:荤侩茄 祈瘤.
	-----------------------------------------------------
	UPDATE dbo.TblPcInventory 
	SET 
		mIsConfirm = 1 
	WHERE mSerialNo = @pSerial

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0
	BEGIN 
		SET  @aErrNo = 2
		GOTO T_END	 
	END

T_END:	
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

