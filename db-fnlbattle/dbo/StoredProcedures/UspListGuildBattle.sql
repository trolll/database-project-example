CREATE PROCEDURE [dbo].[UspListGuildBattle]
	 @pGuildNo		INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SELECT [mGuildNo1], [mGuildNo2]
	FROM dbo.TblGuildBattle
	WHERE ([mGuildNo1] = @pGuildNo) 
		OR ([mGuildNo2] = @pGuildNo)
	SET NOCOUNT OFF

GO

