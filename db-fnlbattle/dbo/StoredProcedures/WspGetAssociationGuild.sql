--------------------------------
-- 按眉疙 : WspGetAssociationGuild
-- 汲疙 : 楷钦 辨靛狼 格废阑 啊廉柯促
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.07.11
-- 荐沥磊 : 
-- 荐沥老 : 
--------------------------------

CREATE    PROCEDURE [dbo].[WspGetAssociationGuild] 
	@mGuildNo	SMALLINT	-- 辨靛 侥喊 锅龋
AS
	SET NOCOUNT ON

	SELECT 
		mGuildAssNo 
	FROM 
		TblGuildAssMem 
	WITH ( NOLOCK )
	WHERE 
		mGuildNo = @mGuildNo

	SET NOCOUNT OFF

GO

