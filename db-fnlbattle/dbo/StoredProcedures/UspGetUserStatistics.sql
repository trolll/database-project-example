CREATE PROCEDURE [dbo].[UspGetUserStatistics]
	 @pActiveUserCnt	INT		OUTPUT
	,@pActiveMoney		BIGINT	OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SELECT TOP 1 @pActiveUserCnt=[mActiveUserCnt], @pActiveMoney=[mActiveMoney]
		FROM TblStatisticsUser ORDER BY [mRegDate] DESC
	IF(0 = @@ROWCOUNT)
	 BEGIN
		SET	@pActiveUserCnt	= 0
		SET	@pActiveMoney	= 0
	 END
	SET NOCOUNT OFF

GO

