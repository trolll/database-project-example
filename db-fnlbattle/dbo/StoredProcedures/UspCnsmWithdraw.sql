/******************************************************************************  
**  Name: UspCnsmWithdraw  
**  Desc: 困殴魄概 沥魂陛 茫扁
**  
**                
**  Return values:  
**   0 : 累诀 贸府 己傍  
**   > 0 : SQL Error  
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmWithdraw]
 @pPcNo				INT
 ,@pMoneySn			BIGINT	-- 捣 Sn
 ,@pCnt				INT		-- 牢免咀荐
 ,@pMoneyItemNo		INT		-- 捣 酒捞袍 锅龋
 ,@pStatus			TINYINT		-- 捣 酒捞袍 锅龋
 ,@pMoneySnNew		BIGINT OUTPUT	-- 捣 Sn(捣捞 货肺 积板阑 版快)
 ,@pResultMoneyCnt	INT OUTPUT		-- 搬苞 捣 咀荐
 ,@pResultBalance	BIGINT OUTPUT	-- 搬苞 沥魂陛
AS  

	 SET NOCOUNT ON  ;
	 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  ;


	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;


	DECLARE @aCurMoneyCnt INT;
	SELECT 	@aCurMoneyCnt = ISNULL(mCnt, 0)
	FROM dbo.TblPcInventory
	WHERE mPcNo=@pPcNo AND mSerialNo=@pMoneySn AND mIsSeizure = 0;

	-- 呈公 腹捞 茫栏妨绊 窍绰瘤 眉农
	IF 1000000000 < (@pCnt+@aCurMoneyCnt) OR @pCnt < 1
	BEGIN
		SET @aErrNo = 1 ;-- 茫栏妨绰 咀荐啊 何利例钦聪促.
		GOTO T_END   ;		
	END
	
	DECLARE @aBalance BIGINT
	SELECT @aBalance = mBalance
	FROM dbo.TblConsignmentAccount
	WHERE  mPcNo = @pPcNo;
	IF @aBalance IS NULL OR @aBalance < @pCnt
	BEGIN
		SET @aErrNo = 1; -- 茫栏妨绰 咀荐啊 何利例钦聪促.
		GOTO T_END;
	END
	
	

	BEGIN TRAN

	-- 沥魂陛 皑家
	UPDATE dbo.TblConsignmentAccount
	SET @pResultBalance = mBalance = mBalance - @pCnt
	WHERE mPcNo = @pPcNo;
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0
	BEGIN
		ROLLBACK TRAN;
		SET @aErrNo = 2 ;-- 沥魂陛 皑家 角菩
		GOTO T_END   ;		
	END

	IF 0 = @pMoneySn
	BEGIN
		EXEC @pMoneySnNew =  dbo.UspGetItemSerial;  -- 脚痹 矫府倔 掘扁   
		IF @pMoneySnNew <= 0  
		BEGIN  
			ROLLBACK TRAN  ;
			SET @aErrNo = 3 ;-- 矫府倔 积己 角菩
			GOTO T_END;    
		END	

		declare @aEndDay datetime;
		SELECT   @aEndDay = dbo.UfnGetEndDate(GETDATE(), 10000)  ;

		-- 牢亥配府俊 酒捞袍 眠啊
		INSERT dbo.TblPcInventory(
			[mSerialNo],  
			[mPcNo],   
			[mItemNo],   
			[mEndDate],   
			[mIsConfirm],   
			[mStatus],   
			[mCnt],   
			[mCntUse],  
			[mOwner],  
			[mPracticalPeriod], 
			mBindingType )  
		VALUES(  
			@pMoneySnNew,  
			@pPcNo,   
			@pMoneyItemNo,--@aItemNo,   
			@aEndDay,--@aEndDate,
			1, --@aIsConfirm,   
			@pStatus, --@aStatus,   
			@pCnt,   
			0, --@aCntUse,  
			0, --@aOwner,  
			0, --@aPracticalPeriod, 
			0) --@aBindingType);
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT;  
		IF  @aErrNo <> 0 OR  @aRowCnt <= 0  
		BEGIN
			ROLLBACK TRAN  ;
			SET @aErrNo = 4 ;-- 酒捞袍 眠啊 角菩
			GOTO T_END;
		END
		SET @pResultMoneyCnt = @pCnt;
	END
	ELSE
	BEGIN
		SET @pMoneySnNew = @pMoneySn;

		-- 家瘤陛俊 眠啊
		UPDATE dbo.TblPcInventory
		SET @pResultMoneyCnt = mCnt = mCnt + @pCnt
		WHERE mPcNo=@pPcNo AND mSerialNo=@pMoneySn AND mIsSeizure = 0;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0
		BEGIN
			ROLLBACK TRAN;
			SET @aErrNo = 5; -- 牢亥配府 诀单捞飘 角菩
			GOTO T_END  ; 		
		END
		IF 1000000000 < @pResultMoneyCnt
		BEGIN
			ROLLBACK TRAN;
			SET @aErrNo = 6; -- 牢亥配府 儡咀捞 呈公 农促
			GOTO T_END   ;		
		END
	END


T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN  ;

T_END:     
	SET NOCOUNT OFF  ; 
	RETURN(@aErrNo) ;

GO

