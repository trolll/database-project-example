CREATE PROCEDURE [dbo].[UspGetGuildStorePassword]
	 @pGuildNo	INT
		
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		  RTRIM(mPassword)
		, mGrade
	FROM dbo.TblGuildStorePassword
	WHERE mGuildNo = @pGuildNo

GO

