CREATE PROCEDURE [dbo].[WspUptPcIsItemDrop]
	@pPcNo	INT
	, @pIsPreventItemDrop	BIT		-- item On/Off Setup	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   

		
	UPDATE dbo.TblPcState
	SET 
		mIsPreventItemDrop = @pIsPreventItemDrop
	WHERE  mNo = @pPcNo
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT

	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(1)
	END
	
    RETURN(0)

GO

