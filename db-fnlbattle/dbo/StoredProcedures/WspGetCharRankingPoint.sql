/******************************************************************************
**		Name: WspGetCharRankingPoint
**		Desc: 某腐磐 珐欧 器牢飘甫 炼雀茄促
**
**		Auth: 辫籍玫
**		Date: 2009-04-29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetCharRankingPoint]
	@mFieldSvrNo			INT,	-- 鞘靛 辑滚 锅龋
	@mFieldSvrPcNo			INT		-- 鞘靛 辑滚 PC锅龋
AS
	SET NOCOUNT ON			-- 汲疙 : 搬苞 饭内靛 悸阑 馆券 救 矫挪促.
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT mContribution, mGuardian, mTeleportTower, mPVP
	FROM dbo.TblChaosBattlePc T1
		INNER JOIN dbo.TblPcRankingPoint T2
			ON T1.mNo = T2.mPcNo
	WHERE mFieldSvrNo = @mFieldSvrNo
		AND mFieldSvrPcNo = @mFieldSvrPcNo

GO

