CREATE PROCEDURE [dbo].[UspBuildGkill]
	 @pGuildNo	INT
	,@pPlace	INT			-- EPlace.
	,@pNode		INT			-- EGkillNode
	,@pPcNo		INT
	,@pBuildStx	SMALLDATETIME OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SET @pBuildStx = GETDATE()	
	-------------------------------------------------------------------------------------
	-- PC俊 殿废等 辨靛 胶懦 粮犁咯何 眉农 
	-------------------------------------------------------------------------------------	
	IF EXISTS(	SELECT *
				FROM dbo.TblGkill 
				WHERE mPcNo = @pPcNo )
	BEGIN
		RETURN(2)	--	秦寸 PC俊 殿废等 辨靛 胶懦沥焊啊 粮犁茄促.	
	END 				
		
	-------------------------------------------------------------------------------------
	-- 辨靛 烘靛 沥焊 殿废
	-------------------------------------------------------------------------------------	
	INSERT TblGkill([mRegDate], [mGuildNo], [mPlace], [mNode], [mPcNo])
			VALUES(@pBuildStx, @pGuildNo, @pPlace, @pNode, @pPcNo)
	IF(@@ERROR <> 0)
	 BEGIN
		RETURN(1)	--  秦寸 辨靛 烘靛 沥焊 眉农 
	 END

	SET NOCOUNT OFF
	RETURN(0)

GO

