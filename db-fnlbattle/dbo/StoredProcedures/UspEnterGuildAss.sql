CREATE PROCEDURE [dbo].[UspEnterGuildAss]
	 @pGuildAssNo	INT
	,@pGuildNo		INT	
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	 
	INSERT INTO dbo.TblGuildAssMem([mGuildAssNo], [mGuildNo]) VALUES(@pGuildAssNo,  @pGuildNo)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END		
	 END	

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

