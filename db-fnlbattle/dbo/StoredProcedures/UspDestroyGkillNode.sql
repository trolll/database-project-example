CREATE PROCEDURE [dbo].[UspDestroyGkillNode]
	 @pGuildNo	INT
	,@pPlace	INT		-- EPlace.
	,@pNode		INT		-- EGkillNode.
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	DELETE dbo.TblGkill 
	WHERE ([mGuildNo]=@pGuildNo) 
		AND ([mPlace]=@pPlace) 
		AND ([mNode]=@pNode)
	 
	IF((@@ERROR <> 0) OR (0 = @@ROWCOUNT))
	 BEGIN
		SET @aErrNo = 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

