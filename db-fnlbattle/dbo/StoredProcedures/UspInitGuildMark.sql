CREATE PROCEDURE [dbo].[UspInitGuildMark]
	@pGuildNo		INT			-- 辨靛锅龋
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErr = 0
		

	UPDATE dbo.TblGuild
	SET mGuildSeqNo = 0,
		mGuildMark = NULL
	WHERE mGuildNo = @pGuildNo 
	
	SELECT	@pRowCnt = @@ROWCOUNT, 
			@pErr  = @@ERROR			

	IF @pRowCnt = 0
	BEGIN
		RETURN(1)				-- 辨靛啊 粮犁窍瘤 臼芭唱, 函版登瘤 臼疽促.
	END 
	
	IF @pErr <> 0 
	BEGIN
		RETURN(@pErr)			-- SQL System Error		
	END

	RETURN(0)
END

GO

