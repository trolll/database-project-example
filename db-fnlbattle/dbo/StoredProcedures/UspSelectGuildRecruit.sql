/******************************************************************************
**		Name: UspSelectGuildRecruit
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩吝牢 辨靛 沥焊甫 傈何 掘绢柯促.
**			  (辑滚 肺靛矫 棵扼棵锭)
**
**		Auth: 辫锐档, 沥柳龋
**		Date: 2009.07.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSelectGuildRecruit]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		TOP 100 
			T1.mRegDate,
			T1.mEndDate,	
			T1.mGuildNo,
			RTRIM(T2.mGuildNm),
			T1.mIsPremium,
			T1.mIsMarkShow,
			T1.mGuildMsg
	FROM
		dbo.TblGuildRecruit T1
			INNER JOIN dbo.TblGuild T2
				ON T1.mGuildNo = T2.mGuildNo			
	WHERE T1.mEndDate >= GETDATE()		
	ORDER BY T1.mIsPremium DESC, T1.mRegDate ASC;

GO

