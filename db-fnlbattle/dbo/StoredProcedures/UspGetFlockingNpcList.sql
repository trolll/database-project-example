/******************************************************************************
**		File: UspGetFlockingNpcList.sql
**		Name: UspGetFlockingNpcList
**		Desc: 笼窜青悼 NPC 府胶飘 啊廉咳
**
**		Auth: 炔霖老
**		Date: 2006-10-30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE	[dbo].[UspGetFlockingNpcList]
	@pMasterPcNo		INT,		-- 林牢捞 登绰 PC狼 锅龋
	@pErrNoStr 		VARCHAR(50) OUTPUT		-- 俊矾巩磊凯 馆券
As
Begin
	SET NOCOUNT ON
	
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErrNoStr	= 'eErrNoSqlInternalError'
	
	SELECT mSlaveNpcNo
	FROM dbo.TblFlockingNpc WITH (NOLOCK) 
	WHERE mMasterPcNo = @pMasterPcNo	

	SELECT	@pRowCnt = @@ROWCOUNT, 
			@pErr  = @@ERROR

	IF @pRowCnt = 0
	BEGIN 
		SET	@pErrNoStr	= 'eErrNoUserNotExistId4'
		RETURN(1)		-- 荤侩磊 沥焊啊 绝促. 
	END 
	
	IF @pErr <> 0
	BEGIN 
		SET	@pErrNoStr	= 'eErrNoSqlInternalError'
		RETURN(@pErr)	-- SQL 郴何 俊矾 
	END 
		
	RETURN(0)			-- None Error
End

GO

