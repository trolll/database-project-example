CREATE Procedure [dbo].[UspSetBanquetHall]
	 @pTerritory		INT			-- 康瘤锅龋
	,@pBanquetHallType	INT			-- 楷雀厘辆幅 (0:家.楷雀厘 / 1:措.楷雀厘)
	,@pBanquetHallNo	INT			-- 楷雀厘锅龋
	,@pOwnerPcNo		INT			-- 家蜡荤侩磊 (夸没)
	,@pLeftMin		INT			-- 父丰盒
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	-- 措咯厚绰 固府 荤侩磊俊霸辑 瞒皑登绰巴阑 焊厘秦具 窃

	-- 楷雀厘 措咯夸没 荤侩磊狼 促弗 楷雀厘捞 绝绰瘤 犬牢
	IF EXISTS(SELECT mOwnerPcNo FROM TblBanquetHall WITH(NOLOCK) WHERE mOwnerPcNo = @pOwnerPcNo)
	BEGIN
		SET @aErrNo = 2		-- 2 : 捞固 楷雀厘 家蜡吝
		GOTO LABEL_END_LAST			 
	END

	-- 家蜡鼻 眉农
	IF EXISTS(SELECT mOwnerPcNo FROM TblBanquetHall WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mBanquetHallType = @pBanquetHallType AND mBanquetHallNo = @pBanquetHallNo)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		-- 家蜡荤侩磊啊 粮犁窍绰瘤 眉农
		DECLARE @pCurOwnerPcNo	INT
		SELECT @pCurOwnerPcNo = mOwnerPcNo FROM TblBanquetHall WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mBanquetHallType = @pBanquetHallType AND mBanquetHallNo = @pBanquetHallNo
		IF (@pCurOwnerPcNo = 0)
		BEGIN
			-- 家蜡林 绝栏骨肺 家蜡鼻 技泼
			BEGIN TRAN
			
			-- 家蜡鼻 技泼
			UPDATE TblBanquetHall SET mOwnerPcNo = @pOwnerPcNo, mLeftMin = @pLeftMin, mRegDate = getdate() WHERE mTerritory = @pTerritory AND mBanquetHallType = @pBanquetHallType AND mBanquetHallNo = @pBanquetHallNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END	 
		END
		ELSE
		BEGIN
			-- 家蜡林啊 乐栏骨肺 家蜡鼻 函版 阂啊瓷
			SET @aErrNo = 3		-- 3 : 家蜡鼻 函版 阂啊瓷
			GOTO LABEL_END_LAST			 
		END
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		BEGIN TRAN

		-- 家蜡鼻 技泼
		INSERT TblBanquetHall (mTerritory, mBanquetHallType, mBanquetHallNo, mOwnerPcNo, mLeftMin) VALUES(@pTerritory, @pBanquetHallType, @pBanquetHallNo, @pOwnerPcNo, @pLeftMin)
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

