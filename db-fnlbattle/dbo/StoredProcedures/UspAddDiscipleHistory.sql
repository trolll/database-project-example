-- change 
CREATE PROCEDURE [dbo].[UspAddDiscipleHistory]
	 @pMaster		INT
	,@pDisciple		INT
	,@pDiscipleNm		VARCHAR(32)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE 	@aRv INT,
				@aErrNo	INT,
				@aHistoryNo	INT
			
	SELECT @aRv = 0, @aErrNo = 0, @aHistoryNo = 0

	SELECT
		@aHistoryNo = ISNULL(MAX(mNo),0)
	FROM dbo.TblDiscipleHistory 
	WHERE mMaster = @pMaster
	

	INSERT dbo.TblDiscipleHistory (mMaster, mNo, mDisciple, mDiscipleNm) 
	VALUES (@pMaster, @aHistoryNo+1, @pDisciple, @pDiscipleNm)
	
	SELECT @aRv = @@ROWCOUNT,  @aErrNo = @@ERROR
	
	IF @aErrNo <> 0 OR @aRv <= 0 
	BEGIN
		RETURN(1) -- db error 
	END

	RETURN (0)	-- non error

GO

