CREATE PROCEDURE [dbo].[UspListBoard]
	 @pBoardId		TINYINT
	,@pIsNext		INT
	,@pBoardNo		INT		-- 器窃烙.
	,@pCnt			INT	
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE @aSql		NVARCHAR(512)
	DECLARE @aWhere	NVARCHAR(100)

	SET @aWhere 	= N' @aBoardId		TINYINT, @aBoardNo		INT '	
	SET @aSql = N'SELECT TOP ' + CAST(@pCnt AS VARCHAR(10)) + ' [mRegDate], [mBoardNo], RTRIM([mFromPcNm]), RTRIM([mTitle]) ' +  CHAR(10) +
				N'FROM TblBoard ' +  CHAR(10) +
				N'WHERE ([mBoardId] = @aBoardId ) '+  CHAR(10) 
				

	IF(0 <> @pIsNext)
 	BEGIN
		SET @aSql = @aSql + 	N'AND ([mBoardNo] <= @aBoardNo ) ' +  CHAR(10) +
							N'ORDER BY [mBoardNo] DESC'	+  CHAR(10)
	END
	ELSE
	BEGIN
		DECLARE	@aCnt	INT

		SELECT @aCnt=COUNT(*) 
		FROM dbo.TblBoard 
		WHERE ([mBoardId]=@pBoardId) 
				AND ([mBoardNo]=@pBoardNo)

		IF(@pCnt < @aCnt)
		BEGIN
			SET @aSql = @aSql + N'AND ([mBoardNo] >= @aBoardNo ) ' +  CHAR(10) +
								N'ORDER BY [mBoardNo] ASC' +  CHAR(10)
		END
		ELSE
		BEGIN
			SET @aSql = @aSql + N'ORDER BY [mBoardNo] DESC'		 	 
		END

	 END				

	EXEC sp_executesql	@aSql, @aWhere, @aBoardId = @pBoardId, 	@aBoardNo = @pBoardNo

GO

