CREATE PROCEDURE [dbo].[UspUpdateQuest]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pValue		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	DECLARE @aValue		INT
	SET		@aErrNo = 0
	
	SELECT @aValue=[mValue]
	FROM dbo.TblPcQuest 
	WHERE ([mPcNo]=@pPcNo) 
		AND ([mQuestNo]=@pQuestNo)	-- 1捞搁 滚妨柳 巴捞促.
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)
	END
			
	IF(@aValue IS NULL)
	BEGIN
	
		IF (@pValue=99)	-- Quest Start, End
		BEGIN
			INSERT INTO dbo.TblPcQuest([mPcNo], [mQuestNo], [mValue],[mBeginDate],[mEndDate])
				VALUES(@pPcNo, @pQuestNo, @pValue, GETDATE(), GETDATE())
				
			IF(0 <> @@ERROR)
			BEGIN
				RETURN(2)
			END
			
			RETURN(0)
		END
		
		
		IF (@pValue <> 0)
		BEGIN		
			INSERT INTO dbo.TblPcQuest([mPcNo], [mQuestNo], [mValue])
				VALUES(@pPcNo, @pQuestNo, @pValue)
				
			IF(0 <> @@ERROR)
			BEGIN
				RETURN(2)
			END	
			
			RETURN(0)	
		END
	
		RETURN(0)
	END


	IF(@pValue = 0)
	BEGIN
		DELETE dbo.TblPcQuest 
		WHERE [mPcNo]=@pPcNo 
				AND [mQuestNo]=@pQuestNo
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(2)
		END
		
		RETURN(0)
	END

	-- Value change check 
	IF(@aValue <> @pValue)
	BEGIN
		
		IF ( @pValue = 99 )	-- Quest 荐青 辆丰矫 付瘤阜 辆丰老阑 殿废 茄促. 
		BEGIN
		
			UPDATE dbo.TblPcQuest 
			SET [mValue]=@pValue
				, mEndDate = GETDATE()	  
			WHERE [mPcNo]=@pPcNo 
				AND [mQuestNo]=@pQuestNo
			
			IF(0 <> @@ERROR)
			BEGIN
				RETURN(3)
			END		
						
			RETURN(0)
		END 
	
		UPDATE dbo.TblPcQuest 
		SET [mValue]=@pValue 
		WHERE [mPcNo]=@pPcNo 
			AND [mQuestNo]=@pQuestNo
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(3)
		END
		 		 
		RETURN(0)
	END

	RETURN(0)

GO

