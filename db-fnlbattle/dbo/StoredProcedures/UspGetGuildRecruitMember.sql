/******************************************************************************
**		Name: UspGetGuildRecruitMember
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩吝牢 某腐磐 沥焊甫 掘绢柯促.
**
**		Auth: 辫锐档, 沥柳龋
**		Date: 2009.07.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetGuildRecruitMember]
	@pPcNo		INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		T1.mRegDate,
		mGuildNo,
		mState,
		mPcMsg,
		RTRIM(T2.mNm) mPcNm,
		T3.mLevel mPcLv,
		T2.mClass mPcClass
	FROM
		dbo.TblGuildRecruitMemberList T1
			INNER JOIN dbo.TblPc T2
				ON T1.mPcNo = T2.mNo 
			INNER JOIN dbo.TblPcState T3
				ON T2.mNo = T3.mNo
	WHERE T1.[mPcNo] = @pPcNo
		AND T2.mDelDate IS NULL

GO

