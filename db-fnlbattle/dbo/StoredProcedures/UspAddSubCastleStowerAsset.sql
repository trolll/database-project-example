CREATE PROCEDURE [dbo].[UspAddSubCastleStowerAsset]
	 @pPlace	INT
	,@pVal		INT
	,@pBuy		BIGINT
	,@pHunt		BIGINT
	,@pGamble	BIGINT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	
	
	UPDATE dbo.TblCastleTowerStone 
	SET 
		[mAsset]=[mAsset]+@pVal, 
		[mAssetBuy]=@pBuy, 
		[mAssetHunt]=@pHunt, 
		[mAssetGamble]=@pGamble
	WHERE ([mPlace] = @pPlace)	
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo	= 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

