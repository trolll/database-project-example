CREATE PROCEDURE [dbo].[UspSetDiscipleJoinCount]  	
	 @pPcNo	INT
	,@pSetNum	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo INT

	UPDATE dbo.TblPcState 
	SET  mDiscipleJoinCount = @pSetNum
	WHERE mNo = @pPcNo
	
	SET @aErrNo = @@ERROR
	IF @aErrNo <> 0
	BEGIN
		RETURN(1) -- db error		
	END
	
	RETURN(0)

GO

