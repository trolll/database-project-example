/******************************************************************************
**		Name: UspUptLimitedResourceItem
**		Desc: 何劝 包访 沥焊甫 啊廉柯促
**
**		Auth: 炼 技泅
**		Date: 2009-07-27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      2010-07-26  沥柳龋              mRemainerCnt 眉农 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUptLimitedResourceItem]
	@pIsInc	BIT,
	@pType	INT,
	@pCnt	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF 0 = @pIsInc	-- 酒捞袍阑 掘阑锭
	BEGIN
		DECLARE @aCnt INT
		SELECT @aCnt = 0

		SELECT @aCnt = mRemainerCnt
		FROM dbo.TblLimitedResource
		WHERE mResourceType = @pType

		IF (0 <> @@ERROR) OR (0 = @@ROWCOUNT)
		BEGIN
			RETURN(2)
		END

		IF 0 >= @aCnt
		BEGIN
			RETURN(1)	-- 泅犁 巢篮 府家胶 力茄 酒捞袍 肮荐啊 绝促
		END
	END
	
	IF NOT EXISTS(	SELECT *
					FROM dbo.TblLimitedResourceItem
					WHERE mResourceType = @pType)
	BEGIN
		RETURN(2)
	END 				

	IF 0 < @pIsInc
	BEGIN
		UPDATE dbo.TblLimitedResource
		SET mRemainerCnt = 
			CASE 
				WHEN mMaxCnt < (mRemainerCnt + @pCnt) THEN mMaxCnt
				ELSE mRemainerCnt + @pCnt
			END
		WHERE mResourceType = @pType
	END
	ELSE
	BEGIN
		UPDATE dbo.TblLimitedResource
		SET mRemainerCnt = 
			CASE
				WHEN (mRemainerCnt - @pCnt) < 0 THEN 0
				ELSE mRemainerCnt - @pCnt
			END
		WHERE mResourceType = @pType
	END
	
	IF (0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		RETURN(2)
	END

	RETURN(0)

GO

