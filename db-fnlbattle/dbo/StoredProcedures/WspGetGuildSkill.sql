--------------------------------
-- 按眉疙 : WspGetGuildSkill
-- 汲疙 : 辨靛疙栏肺 泅犁 辨靛扁 呼靛 茄 辨靛 胶懦格废阑 掘绢柯促
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.07.12
-- 荐沥磊 : 
-- 荐沥老 : 
-- 荐沥郴侩 : 
--------------------------------
CREATE       PROCEDURE [dbo].[WspGetGuildSkill]
	@mGuildNm VARCHAR(12) -- 辨靛 捞抚
AS
	SET NOCOUNT ON

	SELECT
	  g.mRegDate
	, g.mPlace
	, g.mNode
	, p.mNm

	FROM	
		[dbo].[TblGuild] as gu	WITH ( NOLOCK )
	INNER	JOIN
		[dbo].[TblGKill]	as g	WITH ( NOLOCK )	
	ON
		g.mGuildNo = gu.mGuildNo
	INNER	JOIN
		[dbo].[TblPc] as p	WITH ( NOLOCK )
	ON
		p.mNo = g.mPcNo
	WHERE
		gu.mGuildNm = @mGuildNm
	ORDER 	BY
		g.mPcNo

	
	SET NOCOUNT OFF

GO

