CREATE PROCEDURE [dbo].[UspBattleGuild]
	 @pGuildNo1	INT
	,@pGuildNo2	INT	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	DECLARE	@aGuildNo1	INT
	DECLARE	@aGuildNo2	INT
	
	IF(@pGuildNo1 < @pGuildNo2)
	 BEGIN
		SET	@aGuildNo1	= @pGuildNo1
		SET	@aGuildNo2	= @pGuildNo2
	 END
	ELSE
	 BEGIN
		SET	@aGuildNo1	= @pGuildNo2
		SET	@aGuildNo2	= @pGuildNo1	 
	 END

	-- 辨靛 蜡瓤己 眉农 
	IF NOT EXISTS(  SELECT * 
					FROM dbo.TblGuild
					WHERE mGuildNo = @aGuildNo1 )
	BEGIN
		RETURN(1)	-- db error 	
	END 

	IF NOT EXISTS(  SELECT * 
					FROM dbo.TblGuild
					WHERE mGuildNo = @aGuildNo2 )
	BEGIN
		RETURN(1)	-- db error 	
	END 
	
	-- 辨靛 傈捧 沥焊 眉农 
	IF EXISTS(	SELECT *
				FROM dbo.TblGuildBattle
				WHERE mGuildNo1 = @aGuildNo1 AND  mGuildNo2 = @aGuildNo2)
	BEGIN
		SET @aErrNo = 2		-- 捞固 辨靛 傈捧 沥焊啊 殿废登绢 乐促
		RETURN(@aErrNo)
	END 

	INSERT INTO TblGuildBattle(	[mGuildNo1], [mGuildNo2]) 
	VALUES(	
			@aGuildNo1, 
			@aGuildNo2
	)
	
	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo = 1		-- SQL Error
		RETURN(@aErrNo)
	END	
	
	RETURN(0)

GO

