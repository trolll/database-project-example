CREATE  PROCEDURE [dbo].[UspGetPcItem]
	 @pPcNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET ROWCOUNT 160	

	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, 滚妨柳 酒捞袍

	SELECT 
		mSerialNo
		, mItemNo
		, mEndDate
		, mCnt
		, mIsConfirm
		, mStatus
		, mCntUse
		, mIsSeizure
		, mApplyAbnItemNo
		, mApplyAbnItemEndDate		-- mi 
		, mOwner
		, mPracticalPeriod
		, mBindingType
		, mRestoreCnt
	FROm (
		SELECT 
			mSerialNo
			, mItemNo
			, DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
			, mCnt
			, mIsConfirm
			, mStatus
			, mCntUse
			, mIsSeizure
			, mApplyAbnItemNo
			, DATEDIFF(mi,GETDATE(),mApplyAbnItemEndDate) mApplyAbnItemEndDate
			, mOwner
			, mPracticalPeriod
			, mBindingType
			, mRestoreCnt
			, mOrderNo =
				CASE 
					WHEN mItemNo = 1136 THEN 1
					WHEN mItemNo = 1135 THEN 2
					WHEN mItemNo = 722  THEN 3
					WHEN mItemNo = 724  THEN 4
					ELSE 
						0
				END			
		FROM dbo.TblPcInventory WITH(INDEX(CL_TblPcInventory), NOLOCK )
		WHERE 	mPcNo = @pPcNo
					AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
					AND mCnt > 0 ) T1
	ORDER BY mOrderNo, mSerialNo ASC

GO

