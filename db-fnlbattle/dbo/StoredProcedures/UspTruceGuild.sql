CREATE PROCEDURE [dbo].[UspTruceGuild]
	 @pGuildNo1	INT
	,@pGuildNo2	INT	
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	DECLARE	@aGuildNo1	INT
	DECLARE	@aGuildNo2	INT
	IF(@pGuildNo1 < @pGuildNo2)
	 BEGIN
		SET	@aGuildNo1	= @pGuildNo1
		SET	@aGuildNo2	= @pGuildNo2
	 END
	ELSE
	 BEGIN
		SET	@aGuildNo1	= @pGuildNo2
		SET	@aGuildNo2	= @pGuildNo1	 
	 END	

	DECLARE	@aRegDate	DATETIME
	
	SELECT @aRegDate=[mRegDate] 
	FROM dbo.TblGuildBattle 
	WHERE ([mGuildNo1]=@aGuildNo1) 
		AND ([mGuildNo2]=@aGuildNo2)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END

	BEGIN TRAN	

		DELETE dbo.TblGuildBattle 
		WHERE ([mGuildNo1]=@aGuildNo1) AND ([mGuildNo2]=@aGuildNo2)
		IF(1 <> @@ROWCOUNT)
		BEGIN
			SET @aErrNo = 2
			GOTO LABEL_END		 
		END
		 
		INSERT INTO dbo.TblGuildBattleHistory([mGuildNo1], [mGuildNo2], [mStxDate], [mEtxDate], [mWinnerGuildNo]) 
									VALUES(@aGuildNo1,  @aGuildNo2,  @aRegDate,	 GETDATE(),	 0)
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 2
			GOTO LABEL_END		 
		END	
									
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN	
		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

