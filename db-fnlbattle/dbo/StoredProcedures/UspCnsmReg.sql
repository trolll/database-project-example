/******************************************************************************  
**  Name: UspCnsmReg  
**  Desc: 困殴 酒捞袍 殿废
**  
**                
**  Return values:  
**   0 : 累诀 贸府 己傍  
**   > 0 : SQL Error  
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmReg]
  @pSn		BIGINT	-- 殿废且 酒捞袍 矫府倔 锅龋
 ,@pCnt		INT		-- 殿废且 俺荐
 ,@pIsStack	BIT		-- 胶琶屈牢瘤 咯何(1捞搁 胶琶)
 ,@pFeeItemSn	BIGINT	-- 荐荐丰 酒捞袍
 ,@pFeeItemCnt	INT		-- 荐荐丰 酒捞袍 俺荐
 ,@pSlotCnt		INT		-- 殿废 啊瓷茄 浇吩 荐
 ,@pCatetory	TINYINT -- 墨抛绊府
 ,@pPrice		INT		-- 俺寸 魄概 啊拜
 ,@pPeriodMinute	INT	-- 魄概 扁埃(盒窜困)
 ,@pCnsmSn			BIGINT OUTPUT -- 殿废 己傍矫 困殴 锅龋
 ,@pTradeLeftMin	INT OUTPUT -- 巢篮 魄概 扁埃(盒窜困)
AS  
	SET NOCOUNT ON   ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  ;
 
	DECLARE @aRowCnt   INT  ;
	DECLARE @aErrNo    INT  ;

	DECLARE @aCnt    INT  ;
	DECLARE @aItemNo   INT  ;
	DECLARE @aIsConfirm   BIT ; 
	DECLARE @aStatus   TINYINT;  
	DECLARE @aCntUse   TINYINT ; 
	DECLARE @aPcNo    INT  ;
	DECLARE @aEndDate   SMALLDATETIME  ;
	DECLARE @aLeftCnt   INT  ;
	DECLARE @aRegDate   DATETIME  ;
	DECLARE @aOwner    INT  ;
	DECLARE @aPracticalPeriod INT  ;
	DECLARE @aBindingType  TINYINT ; 
	DECLARE @aRestoreCnt  TINYINT  ;

	IF(@pCnt < 1 OR 100 < @pCnt)
	BEGIN  
		SET @aErrNo = 1; -- 何利例茄 俺荐涝聪促.
		GOTO T_END  ; 
	END  

	IF(@pSlotCnt < 1)
	BEGIN  
		SET @aErrNo = 2; -- 殿废 啊瓷茄 浇吩捞 何利例钦聪促.
		GOTO T_END ;  
	END  

	-- 俺寸 啊拜捞 10撅阑 逞栏搁 救等促
	IF(@pPrice < 100 OR 1000000000 < @pPrice)
	BEGIN  
		SET @aErrNo = 3; -- 魄概 啊拜捞 何利例钦聪促.
		GOTO T_END   ;
	END  

	IF(@pPeriodMinute < 3 OR 60*24*5 < @pPeriodMinute)
	BEGIN  
		SET @aErrNo = 4; -- 魄概 扁埃捞 何利例钦聪促.
		GOTO T_END   ;
	END 

	SET @pTradeLeftMin = @pPeriodMinute;

	SELECT @aRowCnt = 0, @aErrNo= 0  ;
	------------------------------------------  
	-- 牢亥配府 酒捞袍 眉农   
	------------------------------------------  
	SELECT   
		@aLeftCnt=[mCnt],     
		@aCnt=[mCnt],   
		@aItemNo=[mItemNo],   
		@aIsConfirm=[mIsConfirm],   
		@aStatus=[mStatus],   
		@aCntUse=[mCntUse],  
		@aPcNo=[mPcNo],   
		@aEndDate=[mEndDate],  
		@aRegDate = [mRegDate],  
		@aOwner = [mOwner],  
		@aPracticalPeriod = [mPracticalPeriod],  
		@aBindingType = mBindingType,  
		@aRestoreCnt = mRestoreCnt  
	FROM dbo.TblPcInventory
	WHERE  mSerialNo  =@pSn   
	AND mIsSeizure = 0  ;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		SET @aErrNo = 5; -- 牢亥配府俊 酒捞袍捞 粮犁窍瘤 臼嚼聪促.
		GOTO T_END   ;
	END  

	SET @aCnt = @aCnt - @pCnt ; 
	IF @aCnt < 0  
	BEGIN  
		SET @aErrNo = 6; -- 牢亥配府俊 酒捞袍 俺荐啊 何练钦聪促.
		GOTO T_END   ;
	END  

	-- 官牢爹 眉农 (蓖加 惑怕老 版快 捞悼捞 例措 阂啊)   
	IF @aBindingType IN (1,2)   
	BEGIN  
		SET @aErrNo = 7 ;-- 蓖加酒捞袍涝聪促.
		GOTO T_END   ;
	END  

	-- 荐荐丰 酒捞袍 俺荐 眉农
	DECLARE @aFeeItemCnt INT;
	SELECT @aFeeItemCnt=ISNULL([mCnt],0)
	FROM dbo.TblPcInventory
	WHERE  mSerialNo  =@pFeeItemSn  
	AND mIsSeizure = 0 	;
	
	IF(@aFeeItemCnt < @pFeeItemCnt)
	BEGIN
		SET @aErrNo = 8 ;-- 荐荐丰 酒捞袍捞 何练钦聪促.  
		GOTO T_END;
	END
	-- @aFeeItemCnt啊 0 捞搁 酒捞袍阑 昏力窍绊 弊犯瘤 臼栏搁 @pFeeItemCnt父怒 俺荐 皑家矫难具 等促
	SET @aFeeItemCnt = @aFeeItemCnt - @pFeeItemCnt;



	-- 殿废等 困殴 格废 俺荐 眉农(急青 眉农)
	DECLARE @aRegCnt INT;
	DECLARE @aExpectedIncome BIGINT;
	SELECT @aRegCnt = COUNT(*), @aExpectedIncome = ISNULL(SUM(CAST(mPrice AS BIGINT)*mCurCnt),0) 
	FROM dbo.TblConsignment
	WHERE mPcNo = @aPcNo;
	
	-- 捞固 殿废茄 俺荐啊 弥措 浇吩 俺荐客 鞍芭唱 歹 农搁 救凳
	IF(@pSlotCnt <= @aRegCnt)
	BEGIN
		SET @aErrNo = 9; -- 殿废 啊瓷茄 浇吩捞 何练钦聪促.
		GOTO T_END;
	END

	-- 抗惑 魄概 措陛苞 沥魂陛狼 钦捞 8000撅阑 逞栏搁 救 等促.
	BEGIN
		DECLARE @aBalance BIGINT;
		SELECT @aBalance = ISNULL(mBalance, 0) 
		FROM dbo.TblConsignmentAccount
		WHERE mPcNo=@aPcNo;
		IF 800000000000 < (@aExpectedIncome + @aBalance + CAST(@pPrice AS BIGINT) * @pCnt)
		BEGIN
			SET @aErrNo = 10; -- 魄概 措陛阑 沥魂陛 拌谅啊 荐侩且 荐 绝嚼聪促.
			GOTO T_END;
		END
	END


	IF(@pIsStack = 0 and 1 <> @pCnt)  
	BEGIN  
		SET @aErrNo = 11; -- 胶琶屈捞 酒聪搁 俺荐绰 1捞绢具 邓聪促.
		GOTO T_END;
	END

	BEGIN TRAN;

	-- 困殴 殿废
	INSERT dbo.TblConsignment(
		mRegDate			,			
		mCategoryNo			,
		mPrice				,
		mTradeEndDate		,
		mPcNo				,
		mRegCnt				,
		mCurCnt				,
		mItemNo)
	VALUES(
		getdate(),			
		@pCatetory,
		@pPrice,
		DATEADD(minute, @pPeriodMinute, getdate()),
		@aPcNo,
		@pCnt,
		@pCnt,
		@aItemNo);
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN  
		SET @aErrNo = 12; -- 殿废且 荐 绝嚼聪促(TblConsignment)
		GOTO T_END   ;
	END

	SET @pCnsmSn = SCOPE_IDENTITY();

	INSERT dbo.TblConsignmentItem(
		mCnsmNo,
		mRegDate,
		mSerialNo,		-- 扁粮 矫府倔 蜡瘤
		mEndDate,
		mIsConfirm,
		mStatus,
		mCntUse,
		mOwner,
		mPracticalPeriod,
		mBindingType,
		mRestoreCnt)
	VALUES(
		@pCnsmSn,
		@aRegDate,
		@pSn,
		@aEndDate,
		@aIsConfirm,
		@aStatus,
		@aCntUse,
		@aOwner,
		@aPracticalPeriod,
		@aBindingType,
		@aRestoreCnt);
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN  
		SET @aErrNo = 13; -- 殿废且 荐 绝嚼聪促(TblConsignmentItem)
		GOTO T_END   ;
	END
	
	-- 荐荐丰 瞒皑, 巢篮霸 乐栏搁 俺荐 皑家, 巢篮霸 绝栏搁 DELETE
	IF @aFeeItemCnt < 1
	BEGIN
		DELETE dbo.TblPcInventory  
		WHERE mSerialNo = @pFeeItemSn and mCnt = @pFeeItemCnt;
		-- 瘤况柳 肺快狼 mCnt啊 @pFeeItemCnt客 鞍瘤 臼促搁 费归捞促
	END
	ELSE
	BEGIN
		UPDATE dbo.TblPcInventory  
		SET mCnt = mCnt - @pFeeItemCnt
		WHERE mSerialNo = @pFeeItemSn;
	END
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN  
		SET @aErrNo = 14 ;-- 荐荐丰 瞒皑俊 角菩沁嚼聪促. 
		GOTO T_END   ;
	END

	-- 殿废等 困殴 格废 俺荐 眉农
	SELECT @aRegCnt = COUNT(*) FROM dbo.TblConsignment
	WHERE mPcNo = @aPcNo;
	IF(@pSlotCnt < @aRegCnt)
	BEGIN
		ROLLBACK TRAN;
		SET @aErrNo = 15; -- 浇吩 俺荐啊 檬苞灯嚼聪促.
		GOTO T_END;
	END


	IF @pIsStack = 0
	BEGIN
		-- 胶琶屈捞 酒聪搁 牢亥配府 酒捞袍 沥焊 昏力   
		DELETE dbo.TblPcInventory  
		WHERE mSerialNo = @pSn  ;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN  ;
			SET @aErrNo = 16 ;-- 牢亥配府 酒捞袍 昏力 角菩
			GOTO T_END   ;
		END
	END
	ELSE -- 胶琶屈捞搁
	BEGIN
		IF @aCnt < 1	-- 傈何 魄概且 版快
		BEGIN		
			-- 牢亥配府俊 阶咯 乐绰 沥焊 昏力
			DELETE dbo.TblPcInventory   
			WHERE mSerialNo = @pSn and mCnt = @pCnt;
			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
			IF @aErrNo<> 0 OR  @aRowCnt = 0  
			BEGIN  
				ROLLBACK TRAN
				SET @aErrNo = 16; -- 牢亥配府 酒捞袍 昏力 角菩
				GOTO T_END    ;
			END			
			-- 瘤况柳 肺快狼 mCnt啊 pCnt客 鞍瘤 臼栏搁 费归秦具 等促.
		END
		ELSE -- 老何父 魄概且 版快
		BEGIN
			UPDATE dbo.TblPcInventory   
			SET   
			mCnt =mCnt - @pCnt   
			WHERE  mSerialNo  = @pSn;
			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
			IF @aErrNo<> 0 OR  @aRowCnt = 0  
			BEGIN
				ROLLBACK TRAN  ;
				SET @aErrNo = 16; -- 牢亥配府 酒捞袍 昏力 角菩
				GOTO T_END      ;
			END
		END
	END


T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN ; 

T_END:     
	SET NOCOUNT OFF   ;
	RETURN(@aErrNo) ;

GO

