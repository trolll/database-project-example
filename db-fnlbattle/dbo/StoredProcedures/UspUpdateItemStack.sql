CREATE Procedure [dbo].[UspUpdateItemStack]
	 @pPcNo			INT
	,@pSerial		BIGINT
	,@pLeftCnt		TINYINT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
		
	UPDATE dbo.TblPcInventory 
	SET 
		mCnt = @pLeftCnt
	WHERE mPcNo = @pPcNo
		AND mSerialNo = @pSerial
	IF @@ERROR<>0 OR @@ROWCOUNT <> 1
	BEGIN
		RETURN(1)	 
	END	
	
	RETURN(0)

GO

