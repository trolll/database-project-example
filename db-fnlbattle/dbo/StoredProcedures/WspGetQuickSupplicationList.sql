CREATE PROCEDURE [dbo].[WspGetQuickSupplicationList]  	
	@mYear SMALLINT
	, @mMonth SMALLINT
	, @mDay SMALLINT
AS
	SET NOCOUNT ON 

	DECLARE @mStart DATETIME
	DECLARE @mEnd DATETIME
	
	SET @mStart = CAST(STR(@mYear) + '-' + STR(@mMonth) + '-' + STR(@mDay) + ' 00:00:00' AS DATETIME)
	SET @mEnd = DATEADD(MI, 1, CAST(STR(@mYear) + '-' + STR(@mMonth) + '-' + STR(@mDay) + ' 23:58:59' AS DATETIME))
	
	SELECT	
		T1.mRegDate
		,T1.mQSID
		,T2.mSupplication
		,T2.mIp
		,T1.mStatus
		,T1.mCategory
		,T1.mPcNo
		, p.mNm
	FROM dbo.TblQuickSupplicationState T1
		INNER JOIN dbo.TblQuickSupplication T2
			ON T1.mQSID = T2.mQSID 
				AND T1.MRegDate BETWEEN @mStart AND @mEnd
		INNER JOIN dbo.TblPc AS p 
			ON T1.mPcNo = p.mNo
	ORDER BY T1.MRegDate DESC

GO

