CREATE PROCEDURE [dbo].[UspGetRacingNextStage]
	 @pPlace	INT
	,@pStage	INT OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SELECT TOP 1 @pStage = mStage 
	FROM TblRacingResult 
	WHERE mPlace = @pPlace
	ORDER BY [mPlace],[mStage] DESC
	IF (1 <> @@ROWCOUNT)
	BEGIN
		SET @pStage = 1
	END
	ELSE
	BEGIN
		SET @pStage = @pStage + 1
	END

	SET NOCOUNT OFF

GO

