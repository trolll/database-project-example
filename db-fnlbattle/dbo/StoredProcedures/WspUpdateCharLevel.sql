--------------------------------
-- 按眉疙 : WspUpdateCharLevel
-- 汲疙 : 某腐磐 饭骇阑 函版茄促
-- 累己磊 : 捞快籍
-- 累己老 : 2006.04.09
-- 荐沥磊 : 辫己犁
-- 荐沥老 : 2006.06.06
-- 荐沥郴侩 : 飘罚黎记 荤侩
--------------------------------
CREATE           PROCEDURE [dbo].[WspUpdateCharLevel]	
	@mLevel SMALLINT, -- 饭骇
	@mNm CHAR(12) -- 某腐磐疙
AS
	SET NOCOUNT ON
	
	BEGIN TRANSACTION

	UPDATE
		[dbo].[TblPcState]
	SET
		mLevel = @mLevel 
		, mExp = 0
	WHERE		
		mNo = (SELECT mNo From [dbo].[TblPc] WHERE mNm = @mNm)			

	IF @@ERROR = 0
	BEGIN
		COMMIT TRANSACTION
		RETURN 0
	END
	ELSE
	BEGIN
		ROLLBACK TRANSACTION
		RETURN @@ERROR
	END

GO

