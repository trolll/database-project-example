/******************************************************************************
**		Name: UspListCastleStowerRuined
**		Desc: 企倾拳等 胶铺 瘤开 府胶飘
**
**		Auth: 辫碍龋
**		Date: 2009.10.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspListCastleStowerRuined]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	
	SELECT mPlace 
	FROM dbo.TblCastleTowerStoneRuined

GO

