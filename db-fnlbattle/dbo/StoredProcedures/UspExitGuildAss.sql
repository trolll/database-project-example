CREATE PROCEDURE [dbo].[UspExitGuildAss]
	 @pGuildNo	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	-- 辨靛楷钦厘捞 呕硼绰 泅钦秦眉捞骨肺 UspDeleteGuildAss()甫 龋免秦扼.
	IF EXISTS(	SELECT * 
				FROM dbo.TblGuildAss 
				WHERE ([mGuildNo]=@pGuildNo) )
	 BEGIN
		SET	@aErrNo	= 1
		GOTO LABEL_END
	 END
	
	DELETE dbo.TblGuildAssMem 
	WHERE ([mGuildNo]=@pGuildNo)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		SET @aErrNo = 2
		GOTO LABEL_END		
	END	

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

