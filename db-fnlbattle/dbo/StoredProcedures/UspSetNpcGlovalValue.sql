CREATE PROCEDURE [dbo].[UspSetNpcGlovalValue]
	@pId		INT,
	@pValue	INT
AS

SET NOCOUNT ON

	IF EXISTS(SELECT * FROM TblNpcGlobalValue WHERE @pId = [mNId])
	BEGIN
		UPDATE TblNpcGlobalValue 
		SET mValue = @pValue 
		WHERE @pId = [mNId]

		RETURN(0)
	END

	RETURN(1)

GO

