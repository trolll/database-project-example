CREATE PROCEDURE [dbo].[UspListGuildBattleNm]
	 @pGuildNo		INT
AS
	SET NOCOUNT ON	
	
	SELECT 
		@pGuildNo AS mGuildNo1,
		T2.mGuildNo AS mGuildNo2,
		RTRIM(T2.mGuildNm) AS mGuildNm
	FROM (	
		SELECT 
			CASE [mGuildNo1]
				WHEN @pGuildNo THEN [mGuildNo2]
				ELSE [mGuildNo1]
			END AS mTargetGuildNo
		FROM dbo.TblGuildBattle WITH(NOLOCK)
		WHERE ([mGuildNo1] = @pGuildNo) 
			OR ([mGuildNo2] = @pGuildNo)
	) T1 INNER JOIN dbo.TblGuild T2  WITH(NOLOCK) 
		On T1.mTargetGuildNo = T2.mGuildNo
		
	SET NOCOUNT OFF

GO

