CREATE PROCEDURE [dbo].[UspGetRacingResult]
	 @pPlace		INT
	,@pStage		INT
	,@pWinnerNID		INT		OUTPUT
	,@pDividend		FLOAT		OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	

	SET @pWinnerNID	= 0
	SET @pDividend	= 0
	
	SELECT @pWinnerNID = mWinnerNID, @pDividend = mDividend
	FROM dbo.TblRacingResult
	WHERE mPlace = @pPlace AND mStage = @pStage

	SET NOCOUNT OFF

GO

