CREATE PROCEDURE [dbo].[UspDecDiscipleJoinCount]
	 @pPcNo	INT
	,@pDecNum	INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT
	DECLARE @aErrNo INT,
			@aRowCnt INT
	DECLARE @aDiscipleJoin INT

	SELECT @aErrNo = 0, @aRv = 0, @aRowCnt = 0


	IF NOT EXISTS(	SELECT mNo 
					FROM dbo.TblPcState 
					WHERE mNo = @pPcNo 
						AND mDiscipleJoinCount - @pDecNum >= 0)
	BEGIN
		RETURN(0)
	END	
	
	UPDATE dbo.TblPcState 
	SET @pDecNum =
			mDiscipleJoinCount = mDiscipleJoinCount - @pDecNum 
	WHERE mNo = @pPcNo
	
	SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR
	IF @aErrNo <> 0 OR @aRowCnt <= 0
	BEGIN
		RETURN(1)
	END 

	RETURN(0)

GO

