/******************************************************************************
**		Name: WspListPcByAccount
**		Desc: 墨坷胶 硅撇 辑滚 拌沥 家蜡 某腐磐 炼雀
**
**		Auth: 辫籍玫
**		Date: 20090428
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspListPcByAccount]
	@mUserId VARCHAR(20) -- 拌沥疙
	,@mFieldSvrNo	SMALLINT		-- 鞘靛 辑滚 锅龋
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		p.mRegdate
		, p.mNm
		, IsConnect =
			CASE
				WHEN s.mLoginTm >= s.mLogoutTm THEN '柯扼牢'
				WHEN s.mLoginTm < s.mLogoutTm THEN '坷橇扼牢'
				ELSE '坷橇扼牢'
			END
		, p.mNo
		, s.mLevel
		, ISNULL(s.mLoginTm, CAST(STR('2000')+'-'+STR('01')+'-'+STR('01') AS DATETIME)) AS mLoginTm
		, ISNULL(s.mLogoutTm, CAST(STR('2000')+'-'+STR('01')+'-'+STR('01') AS DATETIME)) AS mLogoutTm
		, ISNULL(s.mIp, 0) AS mIp
	FROM
		dbo.TblChaosBattlePc AS p
	INNER JOIN
		[FNLAccount].[dbo].[TblUser] AS u
	ON
		p.mOwner = u.mUserNo
	LEFT OUTER JOIN
		[dbo].[TblPcState] AS s
	ON
		p.mNo = s.mNo
	WHERE
		u.mUserId = @mUserId
		AND p.mFieldSvrNo = @mFieldSvrNo
	ORDER BY
		p.mNo

GO

