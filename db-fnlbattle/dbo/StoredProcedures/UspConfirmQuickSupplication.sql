CREATE PROCEDURE [dbo].[UspConfirmQuickSupplication]
	  @pQSID		BIGINT
	, @pPcNo		INT
	, @pCate		TINYINT
AS
	SET NOCOUNT ON			
		
	IF NOT EXISTS(	SELECT * 
					FROM dbo.TblQuickSupplicationState 
					WHERE mQSID = @pQSID 
						AND mPcNo = @pPcNo 
						AND mStatus = 1
						AND mCategory = @pCate )
	 BEGIN		
		RETURN(1)	 -- 犬牢且 郴侩捞 绝促.		
	 END 
	 
	 
	UPDATE dbo.TblQuickSupplicationState 
	SET mStatus = 2 
	WHERE mQSID = @pQSID
	IF(@@ERROR <> 0)
	BEGIN		
		RETURN(2)	-- 惑怕 盎脚 角菩
 	END

	RETURN(0)

GO

