 /******************************************************************************
**		Name: UspSetCastleStowerRuined
**		Desc: 胶铺狼 企倾 咯何 惑怕甫 函版茄促.
**
**		Auth: 辫碍龋
**		Date: 2009.10.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspSetCastleStowerRuined]
	 @pPlace		INT
	,@pIsRuined		BIT		-- 企倾拳 惑怕 咯何?
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	IF(0 = @pIsRuined)
	BEGIN
		DELETE FROM dbo.TblCastleTowerStoneRuined 
		WHERE mPlace=@pPlace
	END 
	ELSE
	BEGIN		
		IF NOT EXISTS ( SELECT * FROM dbo.TblCastleTowerStoneRuined WHERE mPlace = @pPlace )
		BEGIN
			INSERT INTO dbo.TblCastleTowerStoneRuined(mPlace)
			VALUES(@pPlace)
		END
	END

	SELECT @aErrNo = @@ERROR;

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

