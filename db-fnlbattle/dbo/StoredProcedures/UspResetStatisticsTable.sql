/******************************************************************************
**		Name: dbo.UspResetStatisticsTable
**		Desc: 烹拌 抛捞喉 檬扁拳
**
**		Auth: 辫 堡挤
**		Date: 2007-07-02
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20080827	JUDY				檬扁拳 矫 己瓷 巩力 惯积栏肺 牢窍咯  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspResetStatisticsTable]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DELETE DBO.TblStatisticsItemByCase WITH(TABLOCK)
	DELETE DBO.TblStatisticsMonsterHunting WITH(TABLOCK)
	DELETE DBO.TblStatisticsItemByMonster WITH(TABLOCK)
	DELETE DBO.TblStatisticsItemByLevelClass WITH(TABLOCK)
	DELETE DBO.TblStatisticsPShopExchange WITH(TABLOCK)

GO

