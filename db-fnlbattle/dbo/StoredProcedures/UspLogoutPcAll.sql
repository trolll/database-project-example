CREATE PROCEDURE [dbo].[UspLogoutPcAll]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	DECLARE @aDate	DATETIME
	SET		@aDate = GETDATE()	
	
	UPDATE dbo.TblPcState 
	SET 
		[mLogoutTm]=@aDate, 
		[mTotUseTm]=[mTotUseTm]+DATEDIFF(mi,[mLoginTm],@aDate)
	WHERE ([mLogoutTm] < [mLoginTm])
	IF(0 <> @@ERROR)
	BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END	 
	END		

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

