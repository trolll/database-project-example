/******************************************************************************  
**  Name: WspSetItemSeizureConsignment  
**  Desc: 酒捞袍 拘幅
**  
**                
**  Return values:  
**   0 : 累诀 贸府 己傍  
**   > 0 : SQL Error  
**   
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[WspSetItemSeizureConsignment]
	@pCnsmNo BIGINT -- 困殴 殿废 锅龋
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;

	DECLARE @aPcNo					INT;
	DECLARE @aCurCnt				INT	;

	DECLARE @aRegDate			DATETIME;
	DECLARE @aSerialNo			BIGINT	;
	DECLARE @aItemNo			INT		;	
	DECLARE @aEndDate			DATETIME;
	DECLARE @aIsConfirm			BIT	;
	DECLARE @aStatus			TINYINT;
	DECLARE @aCntUse			TINYINT;
	DECLARE @aOwner				INT;
	DECLARE @aPracticalPeriod	INT;
	DECLARE @aBindingType		TINYINT;
	DECLARE @aRestoreCnt		TINYINT;

	SELECT @aRowCnt = 0, @aErrNo= 0  ;
	SELECT   
		@aPcNo		= [mPcNo],
		@aCurCnt	= [mCurCnt],
		@aItemNo	= [mItemNo]
	FROM dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmNo;   

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 1;  -- 困殴 酒捞袍捞 粮犁窍瘤 臼嚼聪促.(TblConsignment)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 郴何 坷幅
		END
		GOTO T_END ;  
	END  

	SELECT
		@aRegDate			= mRegDate,
		@aSerialNo			= mSerialNo,		
		@aEndDate			= mEndDate,
		@aIsConfirm			= mIsConfirm,
		@aStatus			= mStatus,
		@aCntUse			= mCntUse,
		@aOwner				= mOwner,
		@aPracticalPeriod	= mPracticalPeriod,
		@aBindingType		= mBindingType,
		@aRestoreCnt		= mRestoreCnt
	FROM dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmNo;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 4 ; -- 困殴 酒捞袍捞 粮犁窍瘤 臼嚼聪促.(TblConsignmentItem)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100 ; -- DB 郴何 坷幅
		END
		GOTO T_END ;  
	END  


	BEGIN TRAN;

	-- 酒捞袍 昏力
	DELETE dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmNo;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 5; -- 酒捞袍 昏力 角菩(TblConsignmentItem)
		GOTO T_END   ;
	END

	DELETE dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmNo and mCurCnt = @aCurCnt;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 6 ;-- 酒捞袍 昏力 角菩(TblConsignment)
		GOTO T_END   ;
	END

	-- 牢亥配府俊 酒捞袍 眠啊(殿废茄 荤恩俊霸 拘幅等 惑怕肺 眠啊)
	INSERT dbo.TblPcInventory(
		[mSerialNo],  
		[mPcNo],   
		[mItemNo],   
		[mEndDate],   
		[mIsConfirm],   
		[mStatus],   
		[mCnt],   
		[mCntUse],  
		[mOwner],  
		[mPracticalPeriod], 
		[mBindingType],
		[mIsSeizure])  
	VALUES(  
		@aSerialNo,  
		@aPcNo,   
		@aItemNo,   
		@aEndDate,   
		@aIsConfirm,   
		@aStatus,   
		@aCurCnt,   
		@aCntUse,  
		@aOwner,  
		@aPracticalPeriod, 
		@aBindingType,
		1) ;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 7; -- 牢亥配府 酒捞袍 眠啊 角菩
		GOTO T_END  ; 
	END
	
	-- TODO: 霸烙 肺弊 巢扁扁	
	
	COMMIT TRAN
	GOTO T_END


T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN ; 

T_END:     
	SET NOCOUNT OFF  ; 
	RETURN(@aErrNo) ;

GO

