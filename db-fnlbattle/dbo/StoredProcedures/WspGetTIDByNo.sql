CREATE     PROCEDURE [dbo].[WspGetTIDByNo] 
	@mNo	INT
AS
	SET NOCOUNT ON

	SELECT
		mItemNo
	FROM
		[dbo].[TblPcStore]
	WITH ( NOLOCK )
	WHERE
		mUserNo = @mNo

	SET NOCOUNT OFF

GO

