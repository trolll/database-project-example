/******************************************************************************
**		Name: UspCheckQuickSupplication
**		Desc: 脚绊窃狼 郴侩阑 啊廉柯促.
**
**		Auth: 辫堡挤
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2012-04-10	傍籍痹				矾矫酒 巩磊凯 坷幅肺 牢秦 mSupplication RTRIM
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckQuickSupplication]
	  @pPcNo		INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 1

	DECLARE		@aErrNo	INT
	DECLARE		@aQSID	BIGINT
	DECLARE		@aText	VARCHAR(1000)
	DECLARE		@aStatus	TINYINT
	DECLARE		@aCate	TINYINT
	DECLARE		@aDate	DATETIME

	SELECt	@aErrNo = 0, @aQSID = 0

	IF NOT EXISTS (	SELECT * 
					FROM TblQuickSupplicationState 
					WHERE mPcNo = @pPcNo 
						AND mStatus < 2 )
	BEGIN		
		SET @aErrNo = 1	-- PC 啊 犬牢窍瘤 臼篮 龙巩捞 绝促.
		GOTO T_END
	END

	SELECT
		  @aDate =   T1.mRegDate
		, @aQSID = T1.mQSID
		, @aText = T2.mSupplication
		, @aStatus = T1.mStatus
		, @aCate = T1.mCategory
	FROM dbo.TblQuickSupplicationState		T1
		INNER JOIN	dbo.TblQuickSupplication		T2
			ON T1.mQSID = T2.mQSID
	WHERE mPcNo = @pPcNo 
		AND mStatus < 2 
	ORDER BY T1.mQSID DESC

T_END:
	SELECT	@aErrNo, @aDate, @aQSID, RTRIM(@aText), @aStatus, @aCate

GO

