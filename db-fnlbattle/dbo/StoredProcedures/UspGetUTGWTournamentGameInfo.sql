/******************************************************************************
**		Name: UspGetUTGWTournamentGameInfo
**		Desc: 配呈刚飘 霸烙 沥焊甫 肺靛茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010-03-31	辫堡挤				辨靛疙 RTRIM 眠啊.
**		2010-04-26	辫堡挤				免仿 鉴辑 函版
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetUTGWTournamentGameInfo]
	  @pStxTime			[SMALLDATETIME]	-- 矫累 矫埃
	, @pEtxTime			[SMALLDATETIME]	-- 辆丰 矫埃
	, @pIsShowInfo		[BIT]			-- 版扁 郴侩阑 焊咯林扁 困茄 侩档肺 肺靛窍绰巴牢啊?
	, @pRowCount		[INT]	OUTPUT	-- @@ROWCOUNT OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aStxDate	[SMALLDATETIME];
	DECLARE	@aRound		[INT];
	DECLARE	@aMinDepth	[TINYINT];
	
	SET @pRowCount = 0;
	
	-- 泅犁 啊厘 臭篮 雀瞒 沥焊甫 掘绰促.
	SELECT	
		TOP 1
		@aMinDepth	= MIN([mDepth]),
		@aStxDate 	= MAX([mMatchSchTime]),
		@aRound		= MAX([mRound])
	FROM	dbo.TblUnitedGuildWarTournamentInfo
	GROUP BY [mDepth], [mMatchSchTime], [mRound]
	ORDER BY [mRound] DESC;
	
	IF(0 <> @@ROWCOUNT)
	 BEGIN
		-- 泅犁 啊厘 臭篮 雀瞒狼 矫累 矫埃捞 泅犁 配呈刚飘 矫累 矫埃荤捞俊 粮犁窍绊 秦寸 配呈刚飘 版扁啊 辆丰登瘤 臼芭唱 郴侩阑 焊咯林扁 困茄 侩档扼搁 配呈刚飘 沥焊甫 肺靛茄促.
		IF((0 <>  @pIsShowInfo) OR (@pStxTime <= @aStxDate AND @aStxDate <= @pEtxTime AND @aMinDepth <> 0))
		 BEGIN
			SELECT
				  [mRound]
				, [mSvrNo]
				, [mGuildNo]
				, RTRIM([mGuildNm])
				, [mRanking]
				, [mMatchSchTime]
				, [mMatchTime]
				, [mState]
				, [mGroup]
				, [mGroupIndex]
				, [mDepth]
			FROM	dbo.TblUnitedGuildWarTournamentInfo
			WHERE	[mRound] = @aRound
			ORDER BY [mDepth] DESC, CASE WHEN mMatchTime IS NULL THEN '1900-01-01' ELSE mMatchTime END ASC;
			
			SET @pRowCount = @@ROWCOUNT;
		 END
	 END

GO

