/******************************************************************************
**		Name: UspRestoreItemTermOfValidity
**		Desc: 酒捞袍狼 蜡瓤扁埃阑 刘啊 矫挪促.
**
**		Auth: 辫 堡挤
**		Date: 2009-04-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: Author:	
**		Description: 
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspRestoreItemTermOfValidity]
	   @pPcNo		INT
	 , @pSerial		BIGINT
	 , @pValidity	INT
	 , @pIsOver		BIT
	 , @pEndDate	INT	OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
			, @aRowCnt	INT
			, @aEndDate SMALLDATETIME
			, @aDate	SMALLDATETIME
			
	UPDATE	dbo.TblPcInventory	
	SET
		  @aEndDate = mEndDate	= dbo.UfnGetEndDate(CASE WHEN 1 = @pIsOver THEN GETDATE() ELSE mEndDate END, @pValidity)
		, mRestoreCnt	= mRestoreCnt + 1
	WHERE	mPcNo = @pPcNo
				AND mSerialNo = @pSerial
	
	SELECT	@aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	
	IF(0 <> @aErrNo)
	 BEGIN
		RETURN(1);	-- DB Error
	 END
	 
	IF(1 <> @aRowCnt)
	 BEGIN
		RETURN(2);
	 END

	SELECT @pEndDate = DATEDIFF(mi,GETDATE(),@aEndDate)	
	 
	RETURN(0);

GO

