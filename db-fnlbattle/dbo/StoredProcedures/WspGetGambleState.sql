-----------------------------------------------------------------------------------------------------------------
-- SP		: WspGetGambleState
-- DESC		: 辆丰登瘤 臼篮 傍己 铰何荤狼 沥焊甫 啊廉柯促.
-- 积己老磊	: 2006-10-19
-- 积己磊	: 辫堡挤
-----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[WspGetGambleState]

AS
	SET NOCOUNT ON
	
	SELECT 
		mRegDate, mTerritory, mIsFinish, mNo, mTotalMoney, mDividend, mOccupyGuild0, mOccupyGuild1, mOccupyGuild2, mOccupyGuild3, mOccupyGuild4,
		mOccupyGuild5, mOccupyGuild6, mIsKeep
	FROM 
		TblSiegeGambleState WITH (NOLOCK)
	GROUP BY
		mRegDate, mTerritory, mIsFinish, mNo, mTotalMoney, mDividend, mOccupyGuild0, mOccupyGuild1, mOccupyGuild2, mOccupyGuild3, mOccupyGuild4,
		mOccupyGuild5, mOccupyGuild6, mIsKeep	
	HAVING 
		mIsFinish = 0
	ORDER BY
		mTerritory

GO

