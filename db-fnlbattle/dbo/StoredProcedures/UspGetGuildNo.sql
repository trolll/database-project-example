CREATE PROCEDURE [dbo].[UspGetGuildNo]
	 @pGuildNm	CHAR(12)
	,@pGuildNo	INT			OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SELECT @pGuildNo=[mGuildNo] 
	FROM dbo.TblGuild WHERE [mGuildNm] = @pGuildNm
	IF(0 = @@ROWCOUNT)
	BEGIN
		SET @pGuildNo = 0
	END

	SET NOCOUNT OFF

GO

