CREATE PROCEDURE [dbo].[UspAddChaoticAndPkCnt]
	 @pPcNo			INT
	,@pChaotic		INT
	,@pPkCnt		INT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	
	UPDATE dbo.TblPcState 
	SET 
		[mChaotic]=@pChaotic, 
		[mPkCnt]=@pPkCnt
	WHERE [mNo] = @pPcNo
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END	

	RETURN(0)

GO

