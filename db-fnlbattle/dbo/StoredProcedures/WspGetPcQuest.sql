CREATE  PROCEDURE [dbo].[WspGetPcQuest] 
	@pPcNo	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
	
	SELECT 
		T1.mQuestNo
		, T1.mValue
		, T1.mBeginDate
		, T1.mEndDate		
		, T2.mQuestNm
		, T2.mQuestDesc	-- Quest name,,,,
	FROM dbo.TblPcQuest T1
		INNER JOIN [FNLParm].dbo.TblQuest T2
			ON T1.mQuestNo = T2.mQuestNo
	WHERE T1.mPcNo = @pPcNo

GO

