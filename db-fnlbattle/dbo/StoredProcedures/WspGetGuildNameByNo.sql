--------------------------------
-- 按眉疙 : WspGetGuildNameByNo
-- 汲疙 : 辨靛狼 捞抚阑 啊廉柯促
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.07.11
-- 荐沥磊 : 
-- 荐沥老 : 
--------------------------------

CREATE    PROCEDURE [dbo].[WspGetGuildNameByNo] 
	@mGuildNo		SMALLINT	-- 辨靛锅龋
AS
	SET NOCOUNT ON

	SELECT
		mGuildNm
	FROM
		TblGuild
	WITH (NOLOCK)
	WHERE
		mGuildNo = @mGuildNo

	SET NOCOUNT OFF

GO

