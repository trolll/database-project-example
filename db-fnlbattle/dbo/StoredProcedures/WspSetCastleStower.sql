-----------------------------------------------------------------------------------------------------------------
-- SP		: WspSetCastleStower
-- DESC		: 胶铺狼 痢飞辨靛甫 函版茄促.(扁粮俊 痢飞磊啊 酒囱 货肺款 痢飞磊肺) // UspSetCastleStower阑 荤侩
-- 积己老磊	: 2006-10-18
-- 积己磊	: 辫堡挤
-- ErrNo	: UspGetCastleStower 狼 ErrNo 曼炼
-- ErrNo 5	: 辨靛疙俊 秦寸窍绰 辨靛啊 粮犁窍瘤 臼绰促
-----------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[WspSetCastleStower]
	 @pPlaceNo	INT				-- 瘤开 锅龋
	,@pGuildNm	VARCHAR(20)		-- 辨靛 捞抚
AS
	SET NOCOUNT ON
	
	DECLARE	@aErrNo		INT
	DECLARE @pGuildNo	INT
	
	SELECT @aErrNo = 2, @pGuildNo = 0
	SELECT @pGuildNo = mGuildNo FROM TblGuild WITH(NOLOCK) WHERE mGuildNm = @pGuildNm	
	
	IF(@pGuildNo <> 0)
	 BEGIN
		EXEC @aErrNo = UspSetCastleStower @pPlaceNo, @pGuildNo, 1
	 END
	
	
	RETURN(@aErrNo)

GO

