CREATE PROCEDURE [dbo].[UspGetDiscipleJoinCount]
	 @pPcNo	INT
	,@pJoinCount	INT OUTPUT
AS
	SET NOCOUNT ON	

	SET @pJoinCount = 0

	SELECT @pJoinCount = mDiscipleJoinCount 
	FROM dbo.TblPcState WITH(NOLOCK) 
	WHERE mNo = @pPcNo

	SET NOCOUNT OFF

GO

