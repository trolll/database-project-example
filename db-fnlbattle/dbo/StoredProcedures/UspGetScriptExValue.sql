CREATE PROCEDURE [dbo].[UspGetScriptExValue]
	@pNo		INT,
	@pVal		INT OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	IF EXISTS(SELECT * FROM TblScriptExValue WHERE mNo = @pNo)
	BEGIN
		SELECT @pVal = mVal FROM TblScriptExValue WHERE mNo = @pNo
	END
	ELSE
	BEGIN
		SELECT @pVal = 0
	END

	SET NOCOUNT OFF

GO

