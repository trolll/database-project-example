CREATE   PROCEDURE [dbo].[UspSetSiegeInfo]
	@pTerritory	INT,
	@pIsSiege	INT

AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo		INT

	IF NOT EXISTS(	SELECT * 
					FROM dbo.TblSiegeInfo 
					WHERE [mTerritory] = @pTerritory )
	BEGIN
		RETURN(2)	-- 粮犁窍瘤 臼绰 瘤开	
	END 
	 	 
	UPDATE dbo.TblSiegeInfo 
	SET [mChgDate] = 
		CASE		
			WHEN @pIsSiege = 1 THEN GetDate()
			ELSE	[mChgDate]
		END,
			[mIsSiege] = @pIsSiege
	WHERE [mTerritory] = @pTerritory
	
	
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)	-- DB 郴何利 坷幅
	END
	
	RETURN(0)  -- OK

GO

