CREATE PROCEDURE [dbo].[UspListPcInfo]
	 @pUserNo	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		a.mNo,			-- 某腐磐 锅龋
		a.mNm,			-- 某腐磐 捞抚
		b.mLevel,		-- 某腐磐 饭骇
		a.mRegDate,		-- 积己老 		
		c.mGuildGrade,
		d.mGuildNm,
		b.mTotUseTm		
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
			ON a.mNo = b.mNo
		LEFT OUTER JOIN dbo.TblGuildMember AS c
			ON a.mNo = c.mPcNo	
		LEFT OUTER JOIN dbo.TblGuild AS d
			ON c.mGuildNo = d.mGuildNo
	WHERE a.mOwner = @pUserNo
			AND mDelDate IS NULL

GO

