CREATE Procedure [dbo].[UspUpdateExp]
	 @pPcNo			INT
	,@pLevel		SMALLINT	 
	,@pExp			BIGINT	--
	,@pPkCnt		INT
	,@pChaotic		INT
AS
	SET NOCOUNT ON	

	DECLARE	 @aRowCnt		INT
	DECLARE  @aErr			INT

	SELECT  @aRowCnt = 0, @aErr = 0
		
	-- ID啊 悼老窍搁 坷幅(2601)捞 惯积窃.
	UPDATE dbo.TblPcState 
	SET 
		[mLevel]=@pLevel
		, [mExp]=@pExp
		, [mPkCnt]=@pPkCnt
		, [mChaotic]=@pChaotic
	WHERE @pPcNo = [mNo]
	
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT	
	
	IF (@aErr <> 0)
		RETURN(1)	-- db error 
		
	IF (@aRowCnt <= 0)	
		RETURN(1)	-- none row 
	
	RETURN(0)

GO

