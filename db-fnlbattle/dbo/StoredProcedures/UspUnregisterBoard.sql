CREATE  Procedure [dbo].[UspUnregisterBoard]
	 @pPcNm			CHAR(12)
	,@pBoardId		TINYINT
	,@pBoardNo		INT
	,@pIsGM			BIT	-- 款康磊 牢瘤 眉农
	,@pErrNoStr		VARCHAR(50)	OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'

	IF (@pIsGM = 0)		-- 老馆牢捞搁
	BEGIN	
		DELETE dbo.TblBoard 
		WHERE ([mBoardNo]=@pBoardNo) 
				AND ([mFromPcNm]=@pPcNm) 
				AND ([mBoardId]=@pBoardId)
		IF(1 <> @@ROWCOUNT)
		 BEGIN
			SET	@aErrNo	= 2
			SET	 @pErrNoStr	= 'eErrNoBoardNotExist'
			GOTO LABEL_END		 
		 END	
	END
	ELSE IF (@pIsGM = 1)	-- 款康磊捞搁 捞抚厚背绝捞 霸矫拱 昏力
	BEGIN
		DELETE dbo.TblBoard 
		WHERE ([mBoardNo]=@pBoardNo) 
				AND ([mBoardId]=@pBoardId)
		IF(1 <> @@ROWCOUNT)
		 BEGIN
			SET	@aErrNo	= 2
			SET	 @pErrNoStr	= 'eErrNoBoardNotExist'
			GOTO LABEL_END		 
		 END	
		

	END
			
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

