CREATE PROCEDURE [dbo].[UspSetGuildGoldItemEffect]
	  @pGuildNo		INT
	, @pItemType	INT
	, @pParmA		FLOAT
	, @pValidHour	INT	-- Hour
	, @pItemId		INT	
	, @pLeftTime	INT	OUTPUT
	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aEndDay	SMALLDATETIME
	SELECT @aEndDay = dbo.UfnGetEndDateByHour( GETDATE(), @pValidHour)
		, @pLeftTime= DATEDIFF(mi, GETDATE(), @aEndDay)
	
	IF EXISTS(	SELECT *
				FROM dbo.TblGuildGoldItemEffect  
				WHERE mGuildNo= @pGuildNo	
					AND mItemType = @pItemType)
	BEGIN
		UPDATE dbo.TblGuildGoldItemEffect 
		SET 
			  mRegDate = GETDATE()
			, mParmA = @pParmA
			, mEndDate = @aEndDay
			, mItemNo = @pItemId
		WHERE mGuildNo = @pGuildNo 
			AND mItemType = @pItemType
			
		IF(@@ERROR <>0)
		BEGIN
			RETURN(3)
		END
		
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblGuildGoldItemEffect (mGuildNo, mItemType, mParmA, mEndDate, mItemNo) 
		VALUES(@pGuildNo, @pItemType, @pParmA, @aEndDay, @pItemId)

		IF(@@ERROR <>0 )
		BEGIN
			RETURN(2)	-- DB Err
		END
	END
	
	RETURN(0)

GO

