CREATE PROCEDURE [dbo].[UspListLetter]
	 @pPcNo		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			
	
	SELECT 
		a.mSerialNo, 
		a.mTitle, 
		a.mFromPcNm, 
		a.mToPcNm
	FROM dbo.TblLetter AS a 
		INNER JOIN TblPcInventory AS b 
		ON a.mSerialNo  = b.mSerialNo
	WHERE b.mPcNo =@pPcNo
			AND b.mItemNo IN (722, 724)
	
	SET NOCOUNT OFF

GO

