/******************************************************************************  
**  Name: UspSetStatisticsItemByMonster  
**  Desc: 阁胶磐, NPC 烹拌 单捞磐 历厘
**  
**  Auth:   
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  辫碍龋      mItemStatus,  mModType 眠啊
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetStatisticsItemByMonster]  
   @pMID   INT  
 , @pItemNo  INT  
 , @pCreate  BIGINT  
 , @pDelete  BIGINT  
 , @pItemStatus SMALLINT
 , @pModType SMALLINT
AS  
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
   
	DECLARE @aErrNo INT  ;
	SET @aErrNo = 0  ;

	INSERT INTO dbo.TblStatisticsItemByMonster
		([MID], [mItemNo], [mCreate], [mDelete]
		 , mItemStatus, mModType)   
	VALUES 
		(@pMID, @pItemNo, @pCreate, @pDelete
		 , @pItemStatus, @pModType)  ;
    
	IF(@@ERROR <> 0) 
	BEGIN 
		SET @aErrNo = 2; -- DB Error  
	END
   
	RETURN(@aErrNo)  ;

GO

