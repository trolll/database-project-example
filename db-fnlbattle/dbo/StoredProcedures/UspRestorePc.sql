CREATE PROCEDURE [dbo].[UspRestorePc]
	 @pPcNo		INT
	,@pPcNm		CHAR(12)		-- 扁粮俊 昏力等 某腐磐疙捞 犁荤侩瞪 荐 乐促.
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0	
	
	UPDATE dbo.TblPc 
	SET 
		[mDelDate]=NULL, 
		[mNm]=@pPcNm
	WHERE ([mNo]=@pPcNo) 
		AND ([mDelDate] IS NOT NULL)
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 40007	--eErrNoNotExistTblPc
		GOTO LABEL_END
	END
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

