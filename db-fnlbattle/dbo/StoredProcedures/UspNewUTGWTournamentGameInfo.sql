/******************************************************************************
**		File: 
**		Name: UspNewUTGWTournamentGameInfo
**		Desc: 配呈刚飘 霸烙 沥焊甫 货肺 积己茄促.
**
**		Auth: 辫 堡挤
**		Date: 2010-01-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspNewUTGWTournamentGameInfo]
	  @pStxTime			[SMALLDATETIME]
	, @pSvrNo			[SMALLINT]
	, @pGuildNo			[INT]
	, @pGuildNm			[CHAR](12)
	, @pRanking			[TINYINT]
	, @pGroup			[TINYINT]
	, @pGroupIndex		[TINYINT]
	, @pDepth			[TINYINT]
	, @pIsNewRound		[BIT]
	, @pRoundNew		[INT]
	, @pRound			[INT]		OUTPUT
	
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aRound			INT;
	DECLARE	@aGroupIndex	TINYINT;
	
	IF(0 <> @pIsNewRound)
	 BEGIN
		-- 货肺款 扼款靛 锅龋甫 汲沥茄促. (啊厘 奴 扼款靛 锅龋俊辑 1阑 歹茄促.)
		SELECT	
			TOP 1
			@pRound = [mRound] + 1,
			@aRound = @pRound
		FROM	dbo.TblUnitedGuildWarTournamentInfo
		ORDER BY [mRound] DESC;
		
		-- 父距 抛捞喉捞 厚绢乐促搁 扼款靛 锅龋甫  1肺 汲沥茄促.
		IF(0 = @@ROWCOUNT)
		 BEGIN
			SELECT	@pRound = 1, @aRound = @pRound;
		 END
	 END
	ELSE
	 BEGIN
		SELECT @aRound = @pRoundNew, @pRound = @aRound;
	 END
	 
	IF EXISTS 
		(
			SELECT 
				* 
			FROM dbo.TblUnitedGuildWarTournamentInfo 
			WHERE	[mRound] = @aRound
						AND [mDepth] = @pDepth
							AND [mGroup] = @pGroup
								AND [mGroupIndex] = @pGroupIndex
		)
	 BEGIN
		IF(0 < @pGroupIndex)
		 BEGIN
			SET @aGroupIndex = 0;
		 END
		ELSE
		 BEGIN
			SET @aGroupIndex = 1;
		 END
	 END
	ELSE
	 BEGIN
		SET	@aGroupIndex = @pGroupIndex;
	 END
	
	INSERT INTO dbo.TblUnitedGuildWarTournamentInfo 
	([mRound], [mSvrNo], [mGuildNo], [mGuildNm], [mRanking], [mMatchSchTime], [mState], [mGroup], [mGroupIndex], [mDepth])
	VALUES
	(@aRound, @pSvrNo, @pGuildNo, @pGuildNm, @pRanking, @pStxTime, 2, @pGroup, @pGroupIndex, @pDepth);	 -- 版扁 搬苞绰 FnlApp::eStateIng 肺 汲沥茄促. (柳青 趣篮 措扁吝)
	
	IF(0 <> @@ERROR)
	 BEGIN
		RETURN(1);		-- DB ERROR;
	 END
	 
	RETURN(0);

GO

