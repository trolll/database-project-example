----------------------------------------------------------------------------------------------------------------
-- ALTER SCRIPT 
----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[UspEnterDisciple]
	 @pMaster	INT
	,@pDisciple	INT
	,@pType	TINYINT	-- 葛烙 郴俊辑狼 开且 (0:绝澜 / 1:荤何 / 2:力磊 / 3:措扁)
	,@pJoinCount	INT OUTPUT	-- 巢篮 啊涝冉荐 馆券
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT
	DECLARE @aErrNo INT
	DECLARE @aMaster INT
	DECLARE @aMasterLevel INT
	DECLARE @aLevel INT
	DECLARE @aJoinCount INT
	SET @aRv = 0
	SET @aMasterLevel = 0
	SET @aLevel = 0
	SET @aJoinCount = 0
	SET @pJoinCount = 0

	-- 葛烙啊涝阑 盔窍绰 荤侩磊狼 PC沥焊啊 粮犁窍绰瘤 犬牢
	SELECT 
		@aLevel = mLevel,  
		@aJoinCount = mDiscipleJoinCount 
	FROM dbo.TblPcState T1
		INNER JOIN dbo.TblPc T2
			ON T1.mNo = T2.mNO
	WHERE T1.mNo = @pDisciple
			AND T2.mDelDate IS NULL
	
	IF @@ROWCOUNT <= 0
	BEGIN
		RETURN(2)  -- 粮犁窍瘤 臼绰 荤侩磊 PC沥焊
	END

	-- 葛烙狼 付胶磐狼 PC沥焊啊 粮犁窍绰瘤 犬牢
	SELECT 
		@aMasterLevel = mLevel 
	FROM dbo.TblPcState 
	WHERE mNo = @pMaster
	IF @@ROWCOUNT <= 0
	BEGIN
		RETURN(6)  -- 粮犁窍瘤 臼绰 付胶磐 PC沥焊
	END
	
	IF (@pMaster = @pDisciple)
	BEGIN
		RETURN(3)		-- 捞固 促弗 葛烙俊 啊涝登绢 乐澜			
	END	
	
	-- 捞固 措扁磊肺 沥侥栏肺 啊涝窍绰 版快
	SELECT @aMaster = mMaster 
	FROM dbo.TblDiscipleMember 
	WHERE mDisciple = @pDisciple	
	IF @@ROWCOUNT > 0
	BEGIN
		IF (@aMaster <> @pMaster)
		BEGIN
			RETURN(3)		-- 捞固 促弗 葛烙俊 啊涝登绢 乐澜			
		END

		-- 荤侩磊狼 付胶磐狼 饭骇焊促 臭瘤 臼篮瘤 眉农
		IF(@aMasterLevel <= @aLevel)
		BEGIN			
			DELETE dbo.TblDiscipleMember 
			WHERE mDisciple = @pDisciple	-- 沥焊啊 巢酒乐扒 酒聪扒 瘤框
			
			RETURN(4)		-- 啊涝 啊瓷茄 饭骇 檬苞
		END

		-- 巢篮 啊涝啊瓷 冉荐 眉农
		IF (@aJoinCount <= 0)
		BEGIN			
			DELETE dbo.TblDiscipleMember 
			WHERE mDisciple = @pDisciple	-- 沥焊啊 巢酒乐扒 酒聪扒 瘤框
			
			RETURN(5)		-- 啊涝啊瓷 冉荐 檬苞
		END

		BEGIN TRAN

			UPDATE dbo.TblDiscipleMember 
			SET mType = @pType 
			WHERE mDisciple = @pDisciple
			
			SET @aErrNo = @@ERROR
			IF @aErrNo <> 0
			BEGIN
				ROLLBACK TRAN
				RETURN(1)
			END

			UPDATE dbo.TblPcState 
			SET @pJoinCount =
					mDiscipleJoinCount = mDiscipleJoinCount - 1 
			WHERE mNo = @pDisciple
			
			SET @aErrNo = @@ERROR
			IF @aErrNo <> 0
			BEGIN
				ROLLBACK TRAN
				RETURN(1)
			END

		COMMIT TRAN
		RETURN(0)
	END


	-- 贸澜 啊涝窍咯 措扁磊肺 殿废窍绰 版快
	-- 荤侩磊狼 付胶磐狼 饭骇焊促 臭瘤 臼篮瘤 眉农
	IF(@aMasterLevel <= @aLevel)
	BEGIN
		RETURN(4)		-- 啊涝 啊瓷茄 饭骇 檬苞
	END

	-- 巢篮 啊涝啊瓷 冉荐 眉农
	IF (@aJoinCount <= 0)
	BEGIN
		RETURN(5)		-- 啊涝啊瓷 冉荐 檬苞		
	END

	INSERT dbo.TblDiscipleMember (mMaster, mDisciple, mType) 
	VALUES(@pMaster, @pDisciple, @pType)
	SET @aErrNo = @@ERROR
	IF @aErrNo <> 0
	BEGIN
		RETURN(1)	-- db error 
	END

	RETURN(0)

GO

