--------------------------------
-- 按眉疙 : WspGetAssociationGuildName
-- 汲疙 : 楷钦 辨靛狼 格废阑 啊廉柯促
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.07.11
-- 荐沥磊 : 
-- 荐沥老 : 
--------------------------------


CREATE    PROCEDURE [dbo].[WspGetAssociationGuildName] 
	@mAssNo		SMALLINT	-- 楷钦辨靛 牢侥锅龋
AS
	SET NOCOUNT ON

	SELECT
		mGuildNo
	FROM
		TblGuildAssMem 
	WITH (NOLOCK)
	WHERE
		mGuildAssNo = @mAssNo
		
	SET NOCOUNT OFF

GO

