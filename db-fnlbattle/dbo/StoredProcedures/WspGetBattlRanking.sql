/******************************************************************************
**		Name: WspGetBattlRanking
**		Desc: 款康砒俊辑 墨坷胶 硅撇 珐欧 沥焊甫 佬绰促
**
**		Auth: 辫籍玫
**		Date: 2009-04-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetBattlRanking]
	@pRankType SMALLINT		-- 珐农 蜡屈 1:扁咯档, 2:荐龋脚, 3:捞悼器呕, 4:pvp
AS
	SET NOCOUNT ON			-- 汲疙 : 搬苞 饭内靛 悸阑 馆券 救 矫挪促.
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT mRegDate, mRanking, mRankType, mPcNo, mPcNm, mSvrNo, mPoint
	FROM dbo.TblRanking
	WHERE mRankType = @pRankType
	ORDER BY mRanking ASC

GO

