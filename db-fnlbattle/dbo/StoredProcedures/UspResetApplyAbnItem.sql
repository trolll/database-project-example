CREATE  PROCEDURE [dbo].[UspResetApplyAbnItem]
	 @pPcNo		INT				-- 家蜡磊
	,@pSerialNo		BIGINT		-- 格钎 酒捞袍狼 矫府倔锅龋
AS
	SET NOCOUNT ON		
	
	UPDATE dbo.TblPcInventory 
	SET 
		mApplyAbnItemNo = 0 
	WHERE mPcNo = @pPcNo 
			AND mSerialNo = @pSerialNo
	
	IF (@@ERROR <> 0)
	BEGIN
		RETURN(1)	-- db error
	END
	
	RETURN(0)	-- non error

GO

