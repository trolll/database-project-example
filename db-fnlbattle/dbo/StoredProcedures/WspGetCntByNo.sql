CREATE     PROCEDURE [dbo].[WspGetCntByNo] 
	@mNo	INT	
AS
	SET NOCOUNT ON

	SELECT
		mCnt
	FROM
		[dbo].[TblPcStore]
	WITH ( NOLOCK )
	WHERE
		mUserNo = @mNo

	SET NOCOUNT OFF

GO

