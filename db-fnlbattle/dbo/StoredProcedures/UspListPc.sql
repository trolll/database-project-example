CREATE PROCEDURE [dbo].[UspListPc]
	 @pUserNo	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SELECT 
		a.[mSlot], 
		a.[mNo]
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
		ON(a.[mNo] = b.[mNo])
	WHERE (a.[mOwner]=@pUserNo) 
		AND ([mDelDate] IS NULL)
	ORDER BY a.[mSlot]
	SET NOCOUNT OFF

GO

