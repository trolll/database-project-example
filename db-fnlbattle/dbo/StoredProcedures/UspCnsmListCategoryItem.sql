/******************************************************************************  
**  Name: UspCnsmListCategoryItem  
**  Desc: 墨抛绊府 茄 俺 弥辟 殿废 府胶飘
**  
**                
**   
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmListCategoryItem]
 @pCategory		TINYINT			-- 墨抛绊府 锅龋
 ,@pPageNo		INT				-- 夸没 其捞瘤
 ,@pCntPerPage	INT			-- 其捞瘤寸 酒捞袍荐
 ,@pRealPageNo	INT OUTPUT	-- 搬苞悸狼 角力 其捞瘤 锅龋啊 馆券等促.
AS  
	 SET NOCOUNT ON  ;
	 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  ;

	-- 弥辟俊 殿废等 巴捞 刚历 焊捞档废 茄促.
	declare @aCurDate smalldatetime;
	SET @aCurDate = getdate();

	-- 傈眉 肺快 墨款飘
	declare @aTotalRow INT;
	select @aTotalRow = count(*)
	from dbo.TblConsignment a
	where a.mCategoryNo = @pCategory and @aCurDate < a.mTradeEndDate;
	

	-- 傈眉 其捞瘤荐 拌魂
	declare @aTotalPageNo INT;
	SET @aTotalPageNo = (@aTotalRow / @pCntPerPage);
	IF 0 < (@aTotalRow % @pCntPerPage)
	BEGIN
		SET @aTotalPageNo = @aTotalPageNo + 1;
	END

	IF 0 = @aTotalPageNo
	BEGIN
		SET @aTotalPageNo = 1;
	END
	
	SET @pRealPageNo = @pPageNo;
	IF @pRealPageNo <= 0 
	BEGIN
		SET @pRealPageNo = 1;
	END
	IF @aTotalPageNo < @pRealPageNo
	BEGIN
		SET @pRealPageNo = @aTotalPageNo;
	END	
	
	
	/*
	-- 鞘夸茄 饭内靛狼 矫累苞 场 锅龋 拌魂
	DECLARE @aStartRow INT;
	DECLARE @aEndRow INT;
	SET @aStartRow = (@pRealPageNo-1)*@pCntPerPage;
	IF @aStartRow < 0
	BEGIN
		SET @aStartRow = 0;
	END
	SET @aEndRow = @aStartRow + @pCntPerPage + 1;	

	WITH CategoryList AS
	(
		select 
			a.mCnsmNo AS mCnsmNo, a.mCategoryNo AS mCategoryNo, a.mPrice AS mPrice, 
				CASE WHEN a.mTradeEndDate <= @aCurDate
				THEN 0 ELSE
					DATEDIFF(minute, getdate(), a.mTradeEndDate) END AS mLeftMinutes, a.mPcNo AS mPcNo, a.mRegCnt AS mRegCnt, 
				a.mCurCnt AS mCurCnt, a.mItemNo AS mItemNo,
				b.mStatus AS mStatus, b.mCntUse AS mCntUse, b.mOwner AS mOwner, b.mPracticalPeriod AS mPracticalPeriod,
				count(*) as RowNumber
				--ROW_NUMBER() OVER(order by a.mCnsmNo desc) AS RowNumber
		from dbo.TblConsignment a, dbo.TblConsignment a2 inner join TblConsignmentItem b on a.mCnsmNo = b.mCnsmNo
		where a.mCategoryNo = @pCategory 
			and @aCurDate < a.mTradeEndDate
			and a.mCnsmNo < b.mCnsmNo
		
	)
	select 
		mCnsmNo, mCategoryNo, mPrice, 
		mLeftMinutes, mPcNo, mRegCnt, mCurCnt, mItemNo,
			mStatus, mCntUse, mOwner, mPracticalPeriod
	FROM CategoryList
	WHERE @aStartRow < RowNumber AND RowNumber < @aEndRow;	
	*/
	
	
   DECLARE @lastKeyValue BIGINT;

	DECLARE @numberToIgnore int;
    SET @numberToIgnore = (@pRealPageNo-1) * @pCntPerPage;
    
    
    IF @numberToIgnore > 0
	BEGIN
	
		SET ROWCOUNT @numberToIgnore;
		
		SELECT
            @lastKeyValue = [UniqueValue]
        FROM
        (
			SELECT
               cnsm.[mCnsmNo] AS [UniqueValue]
            FROM
                dbo.TblConsignment cnsm
            WHERE
				cnsm.[mCategoryNo] = @pCategory
				and
                cnsm.[mTradeEndDate] > @aCurDate
         ) AS Derived
          ORDER BY           
           [UniqueValue] DESC
	END	
		
	SET ROWCOUNT @pCntPerPage;	
	
	SELECT
        Derived.[UniqueValue],
		Derived.[mCategoryNo], 
		Derived.[mPrice], 
		Derived.[mLeftMinutes], 
		Derived.[mPcNo], 
		Derived.[mRegCnt], 
		Derived.[mCurCnt], 
		Derived.[mItemNo],
		Derived.[mStatus], 
		Derived.[mCntUse], 
		Derived.[mOwner], 
		Derived.[mPracticalPeriod]
	FROM
	(	
		select 
			a.mCnsmNo AS [UniqueValue], 
			a.mCategoryNo AS mCategoryNo, 
			a.mPrice AS mPrice, 
			CASE WHEN a.mTradeEndDate <= @aCurDate
			THEN 0 ELSE
				DATEDIFF(minute, getdate(), a.mTradeEndDate) END AS mLeftMinutes, 
			a.mPcNo AS mPcNo, 
			a.mRegCnt AS mRegCnt, 
			a.mCurCnt AS mCurCnt, 
			a.mItemNo AS mItemNo,
			b.mStatus AS mStatus, 
			b.mCntUse AS mCntUse, 
			b.mOwner AS mOwner, 
			b.mPracticalPeriod AS mPracticalPeriod
		from dbo.TblConsignment a 
			inner join TblConsignmentItem b on a.mCnsmNo = b.mCnsmNo
		where a.mCategoryNo = @pCategory 
			and @aCurDate < a.mTradeEndDate						
	 ) AS Derived
	WHERE
    (
        @lastKeyValue IS NULL
    )
    OR
    (
		[UniqueValue] < @lastKeyValue	        
    )    
    ORDER BY
        [UniqueValue] DESC

    SET ROWCOUNT 0

GO

