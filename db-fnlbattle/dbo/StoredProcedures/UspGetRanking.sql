/******************************************************************************
**		Name: UspGetRanking
**		Desc: 珐欧 沥焊 馆券
**
**		Auth: JUDY
**		Date: 2009.03.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**               
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetRanking]  	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		mRanking, mPcNo, RTRIM(mPcNm) mPcNm, mSvrNo, mPoint
	FROM dbo.TblRanking
	WHERE mRankType = 1	-- mContribution
	ORDER BY mRanking ASC 
	

	SELECT 
		mRanking, mPcNo, RTRIM(mPcNm) mPcNm, mSvrNo, mPoint
	FROM dbo.TblRanking
	WHERE mRankType = 2	-- mGuardian

	SELECT 
		mRanking, mPcNo, RTRIM(mPcNm) mPcNm, mSvrNo, mPoint
	FROM dbo.TblRanking
	WHERE mRankType = 3	-- mTeleportTower

	SELECT 
		mRanking, mPcNo, RTRIM(mPcNm) mPcNm, mSvrNo, mPoint
	FROM dbo.TblRanking
	WHERE mRankType = 4	-- mPVP

GO

