CREATE PROCEDURE [dbo].[UspPopItem]
	 @pPcNo			INT					-- 酒捞袍 脚 家蜡磊.
	,@pSerial		BIGINT
	,@pCnt			INT					-- 滚副 俺荐.
	,@pCachingCnt	INT					-- 滚副 俺荐甫 器窃茄 caching等 啊皑 俺荐.
	,@pIsSpend		TINYINT				-- 曼捞搁 荤扼咙.
	,@pSerialNew	BIGINT OUTPUT		-- 老何父 滚赴版快 滚妨柳 酒捞袍狼 SN.
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
		
	DECLARE	@aErrNo		INT
	DECLARE @aRowCnt	INT
	DECLARE @aCnt		INT
	DECLARE @aIsSeizure	BIT
	DECLARE @aItemNo		INT
	DECLARE @aEndDate	SMALLDATETIME
	DECLARE @aIsConfirm	BIT
	DECLARE @aStatus		TINYINT
	DECLARE 	@pRegDate 	DATETIME
	DECLARE @aOwner		INT
	DECLARE @aPracticalPeriod	INT,
			@aBindingType	TINYINT
			
	-----------------------------------------------
	-- 函荐 檬扁拳
	-----------------------------------------------
	SELECT 		@aErrNo = 0,	@aRowCnt = 0
	
	
	-----------------------------------------------
	-- 扁粮 酒捞袍 沥焊 犬牢(矫府倔 酒捞袍 沥焊 犬牢, 拘幅, 瞒皑 酒捞袍 犬牢)
	-----------------------------------------------
	SELECT 
		@aCnt= mCnt, 
		@aIsSeizure = mIsSeizure, 
		@aItemNo = mItemNo, 
		@aEndDate = mEndDate, 
		@aIsConfirm= mIsConfirm, 
		@aStatus= mStatus,
		@aOwner = mOwner,
		@aPracticalPeriod = mPracticalPeriod,
		@aBindingType = mBindingType
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pSerial
	
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt = 0
	BEGIN
		RETURN(1)			-- ERR : 1 , 贸府等 单捞鸥啊 绝芭唱, SQL INTERNAL ERROR		
	END		
	IF(0 <> @aIsSeizure)
	BEGIN
		RETURN(11)			-- ERR : 11 ,  拘幅等 酒捞袍篮 牢亥配府俊辑 波尘 荐 绝促
	END
	SET @aCnt = @aCnt + @pCachingCnt
	IF(@aCnt < 0)
	BEGIN
		RETURN(2)			-- ERR : 2 ,  波尘荐 乐绰 酒捞袍捞 粮犁窍瘤 臼绰促.
	END

	-- 官牢爹 坷幅 眉农 
	IF	@pIsSpend = 0	-- true : 家葛己??
		AND	@aBindingType IN ( 1, 2)
	BEGIN
		RETURN(12)			
	END		
		

	--------------------------------------------------------------
	-- 矫府倔 沥焊甫 掘绰促.
	--------------------------------------------------------------
	IF ( @aCnt > 0 AND @pIsSpend = 0 )
	BEGIN
		SET @pRegDate = GETDATE()
		EXEC @pSerialNew =  dbo.UspGetItemSerial 
		IF @pSerialNew <= 0
		BEGIN
			RETURN(7)			
		END
	END		
	ELSE
	BEGIN		
		SET @pSerialNew = @pSerial
	END
	
	--------------------------------------------------------------
	-- 酒捞袍阑 捞悼 
	--------------------------------------------------------------
	BEGIN TRAN
		------------------------------------------------------
		-- 儡咯肮荐 绝促.
		------------------------------------------------------
		IF(@aCnt < 1)	
		BEGIN
			IF(0 <> @pIsSpend)	-- 荤扼瘤绰 酒捞袍 捞搁 
			BEGIN
				DELETE dbo.TblPcInventory 
				WHERE mSerialNo = @pSerial
				SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo <> 0 OR 	@aRowCnt = 0
				BEGIN
					ROLLBACK TRAN
					RETURN(3)				
				END
			END
			ELSE
			BEGIN
				UPDATE dbo.TblPcInventory
				SET 
					mPcNo = @pPcNo,
					mCnt = @pCnt 
				WHERE mSerialNo  = @pSerial 
	
				SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo <> 0 OR 	@aRowCnt = 0
				BEGIN
					ROLLBACK TRAN
					RETURN(4)				
				END					 
			END
			
			COMMIT TRAN
			RETURN(0)	
		END
		------------------------------------------------------
		-- 儡咯肮荐 粮犁.
		------------------------------------------------------
		UPDATE dbo.TblPcInventory 
		SET 
			mCnt = @aCnt 
		WHERE mSerialNo  = @pSerial 
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN(5)				
		END
		IF (@pIsSpend = 0)
		BEGIN
			INSERT INTO dbo.TblPcInventory([mSerialNo], [mPcNo], [mItemNo], [mEndDate], [mIsConfirm], [mStatus], [mCnt], [mOwner], [mPracticalPeriod], mBindingType )
			VALUES(
				@pSerialNew,
				@pPcNo, 
				@aItemNo, 
				@aEndDate, 
				@aIsConfirm, 
				@aStatus, 
				@pCnt,
				@aOwner,
				@aPracticalPeriod,
				@aBindingType
				)
			
			SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF @aErrNo <> 0 OR 	@aRowCnt = 0
			BEGIN
				ROLLBACK TRAN
				RETURN(6)				
			END		
		END 
	
	COMMIT TRAN
	RETURN(0)

GO

