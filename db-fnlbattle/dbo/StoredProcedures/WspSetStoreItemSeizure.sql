CREATE PROCEDURE [dbo].[WspSetStoreItemSeizure]
	@mNo INT, -- 芒绊锅龋
	@mIsSeizure BIT -- 拘幅咯何
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


	UPDATE dbo.TblPcStore
	SET
		mIsSeizure = @mIsSeizure
	WHERE mUserNo = @mNo

	IF @@ERROR <> 0
		RETURN 1
	
	RETURN 0

GO

