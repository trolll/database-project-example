CREATE PROCEDURE [dbo].[WspGetGuildInfo]
	@mGuildNm CHAR(12) 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		g.mGuildNm, 
		g.mGuildNo, 
		p.mNm, 
		(SELECT COUNT(*) FROM [dbo].[TblGuildMember]
		 WHERE mGuildNo = g.mGuildNo) mMemberCnt, 
		g.mRegDate, 
		c.mPlace, 
		g.mGuildSeqNo,
		gsp1.mPassword AS mLev1Passworld,		
		gsp2.mPassword AS mLev2Passworld
	FROM
		[dbo].[TblGuild]  AS g 
		INNER JOIN [dbo].[TblGuildMember] AS gm 
			ON	g.mGuildNo = gm.mGuildNo	
		INNER JOIN [dbo].[TblPc] AS p 
			ON	gm.mPcNo = p.mNo
		LEFT OUTER JOIN [dbo].[TblCastleTowerStone] AS c 
			ON	g.mGuildNo = c.mGuildNo
		LEFT OUTER JOIN dbo.TblGuildStorePassword AS gsp1
			ON  g.mGuildNo = gsp1.mGuildNo
				AND gsp1.mGrade = 0	-- 芒绊 饭骇 1
		LEFT OUTER JOIN dbo.TblGuildStorePassword AS gsp2
			ON  g.mGuildNo = gsp2.mGuildNo
				AND gsp2.mGrade = 1	-- 芒绊 饭骇 2				
	WHERE
		g.mGuildNm = @mGuildNm
			AND gm.mGuildGrade = 0

GO

