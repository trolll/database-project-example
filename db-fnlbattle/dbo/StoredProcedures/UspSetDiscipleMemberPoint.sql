CREATE PROCEDURE [dbo].[UspSetDiscipleMemberPoint]
	 @pDisciple	INT	-- 力磊狼 PC锅龋
	,@pMemPoint	INT	-- 力磊狼 器牢飘
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE @aMaster INT
	
	-- 荤侩磊狼 PC沥焊啊 粮犁窍绰瘤 犬牢
	IF NOT EXISTS(	SELECT mNo 
					FROM dbo.TblPc 
					WHERE mNo = @pDisciple )
	BEGIN
		RETURN(2)		-- 粮犁窍瘤 臼绰 荤侩磊 PC沥焊
	END

	-- 葛烙糕滚俊 力磊狼 沥焊啊 粮犁窍绰瘤 犬牢
	IF NOT EXISTS(	SELECT mMaster 
					FROM dbo.TblDiscipleMember 
					WHERE mDisciple = @pDisciple )
	BEGIN
		RETURN(3)	-- 粮犁窍瘤 臼绰 力磊沥焊
	END

	UPDATE dbo.TblDiscipleMember 
	SET mMemPoint = @pMemPoint 
	WHERE mDisciple = @pDisciple
	IF @@ERROR > 0
	BEGIN
		RETURN(1)		-- DB俊矾 惯积
	END
	
	RETURN(0)

GO

