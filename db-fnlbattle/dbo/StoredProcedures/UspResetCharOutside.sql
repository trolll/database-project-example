CREATE PROCEDURE [dbo].[UspResetCharOutside]
	@aPcNo	INT,        -- 家蜡 某腐磐 锅龋
	@pUserNo INT,		-- 拌沥 沥焊  
    @pSex   TINYINT,	-- 函版等 己喊     
	@pHead	TINYINT,	-- 赣府
    @pFace  TINYINT,    -- 倔奔   
    @pSerialNo	BIGINT,	-- 酒捞袍 矫府倔 沥焊
    @pItemNo INT		-- 酒捞袍 锅龋 
AS
	SET NOCOUNT ON			

	DECLARE	@aErrNo		INT
			,@aRowCnt	INT
			
	SELECT  @aErrNo = 0, @aRowCnt = 0
	
	-----------------------------------------------
	-- 寇葛 沥焊 函版 
	-----------------------------------------------		
	BEGIN TRAN
	
		UPDATE dbo.TblPc
		SET
			mSex = @pSex,
			mHead = @pHead,
			mFace = @pFace
		WHERE mOwner = @pUserNo		
				AND mNo = @aPcNo
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END		
	
		DELETE dbo.TblPcInventory
		WHERE mPcNo = @aPcNo 
			AND	mSerialNo = @pSerialNo
			AND mItemNo = @pItemNo
				
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END					

	COMMIT TRAN	
	RETURN(0)

GO

