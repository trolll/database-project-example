CREATE PROCEDURE [dbo].[UspCancelQuickSupplication]
	  @pPcNo		INT
	, @pQSID		BIGINT
	, @pCate		TINYINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo INT
	SET @aErrNo = 0
	
	IF NOT EXISTS (	SELECT *
				FROM dbo.TblQuickSupplicationState 
				WHERE mQSID = @pQSID 
					AND mPcNo = @pPcNo 
					AND mCategory = @pCate 
					AND mStatus = 0 ) 					
	BEGIN		
		RETURN(1)	-- 秒家且 荐 乐绰 脚绊郴侩捞 绝嚼聪促.		
	END 
	
	
	BEGIN TRAN
	
		DELETE dbo.TblQuickSupplicationState 
		WHERE mQSID = @pQSID
		
		IF(@@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN 
			RETURN(2)
		END

		DELETE dbo.TblQuickSupplication 
		WHERE mQSID = @pQSID
		
		IF(@@ERROR <> 0)
  		BEGIN
			ROLLBACK TRAN 
			RETURN(2)
		END

	COMMIT TRAN
	RETURN(0)

GO

