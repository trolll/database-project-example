-----------------------------
-- PROCEDURES
-----------------------------
CREATE PROCEDURE [dbo].[UspGetFriend]
	 @pPcNo	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		[mFriendPcNo] =
			CASE 
				WHEN a.[mFriendPcNm] <> c.[mNm] THEN 0
				WHEN a.[mFriendPcNo] <> c.mNo THEN 0
				WHEN c.[mNm] IS NULL THEN 0
				ELSE c.mNo	-- new pc number
			END,
		RTRIM(a.[mFriendPcNm]), 
		ISNULL(b.[mLogoutTm], GETDATE())
	FROM dbo.TblPcFriend AS a 
		LEFT OUTER JOIN dbo.TblPc c
			ON ( a.mFriendPcNm = c.mNm)		
		LEFT JOIN dbo.TblPcState AS b 
			ON ( c.mNo = b.mNo )
	WHERE mOwnerPcNo = @pPcNo
	

	SET NOCOUNT OFF

GO

