CREATE    PROCEDURE [dbo].[WspGetItemTotalCountByTid]
	@mItemNo INT, -- TID
	@mTotCnt INT OUTPUT -- 醚 肮荐
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @mTotCnt = SUM(mTot)
	FROM(
		SELECT COUNT(*) AS mTot
		FROM dbo.TblPcInventory T1
			INNER JOIN dbo.TblPc T2
				ON T1.mPcNo = T2.mNo
		WHERE T1.mPcNo > 1
			AND mDelDate IS NULL
			AND mItemNo = @mItemNo
		UNION ALL
		SELECT COUNT(*) AS mTot
		FROM dbo.TblPcStore	
		WHERE mItemNo = @mItemNo
		UNION ALL
		SELECT COUNT(*) AS mTot
		FROM dbo.TblGuildStore	
		WHERE mItemNo = @mItemNo		
	) T1

GO

