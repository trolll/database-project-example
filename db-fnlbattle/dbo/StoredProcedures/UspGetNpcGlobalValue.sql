CREATE PROCEDURE [dbo].[UspGetNpcGlobalValue]
 AS

SET NOCOUNT ON

	SELECT mNid, mName, mValue
	FROM TblNpcGlobalValue

GO

