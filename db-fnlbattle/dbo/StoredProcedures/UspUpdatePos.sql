CREATE Procedure [dbo].[UspUpdatePos]
	 @pPcNo			INT
	,@pHp			INT
	,@pMp			INT
	,@pMapNo		INT
	,@pPosX			REAL
	,@pPosY			REAL
	,@pPosZ			REAL
	,@pStomach		SMALLINT	 
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	-- ID啊 悼老窍搁 坷幅(2601)捞 惯积窃.
	UPDATE dbo.TblPcState 
	SET [mHp]=@pHp, [mMp]=@pMp, [mMapNo]=@pMapNo, [mPosX]=@pPosX, [mPosY]=@pPosY, 
	  [mPosZ]=@pPosZ, [mStomach]=@pStomach
	WHERE [mNo] = @pPcNo
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END	 
	 END	

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

