CREATE Procedure [dbo].[UspBidGuildAgitAuction]			-- 辨靛酒瘤飘 版概 涝蔓 (版概 啊瓷茄 辨靛俊 茄窃)
	 @pTerritory		INT				-- 康瘤锅龋
	,@pGuildAgitNo		INT				-- 辨靛酒瘤飘锅龋
	,@pBidGID		INT OUTPUT			-- 涝蔓辨靛 [涝仿/免仿]
	,@pBidMoney		BIGINT OUTPUT		-- 涝蔓陛咀 [涝仿/免仿]
	,@pCurBidGuildName	NVARCHAR(50) OUTPUT	-- 泅犁涝蔓辨靛 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	SET		@pCurBidGuildName = ''

	-- 措咯陛 粮犁咯何 眉农
	IF NOT EXISTS(SELECT mGID FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pBidGID)
	BEGIN
		SET @aErrNo = 2			-- 2 : 辨靛拌谅啊 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END

	-- 措咯陛 面盒咯何 眉农
	DECLARE @aCurGuildMoney	BIGINT
	SELECT @aCurGuildMoney = mGuildMoney FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pBidGID
	IF (@aCurGuildMoney < @pBidMoney)
	BEGIN
		SET @aErrNo = 3			-- 3 : 辨靛磊陛捞 何练
		GOTO LABEL_END_LAST			 
	END

	-- 泅犁 辨靛酒瘤飘甫 焊蜡窍绊 乐绰 版快绰 涝蔓阂啊
	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mOwnerGID = @pBidGID)
	BEGIN
		SET @aErrNo = 4			-- 4 : 捞固 促弗 辨靛酒瘤飘甫 焊蜡窍绊 乐澜
		GOTO LABEL_END_LAST			 
	END

	-- 促弗 辨靛酒瘤飘俊 涝蔓 吝牢 版快绰 涝蔓阂啊
	IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH(NOLOCK) WHERE mCurBidGID = @pBidGID)
	BEGIN
		SET @aErrNo = 8			-- 8 : 吝汗 涝蔓烙
		GOTO LABEL_END_LAST			 
	END

	-- 辨靛酒瘤飘 涝蔓 贸府
	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		DECLARE @aCurBidGID	INT
		DECLARE @aCurBidMoney	BIGINT
		DECLARE @aIsSelling		TINYINT

		SELECT @aCurBidGID = mOwnerGID, @aCurBidMoney = mSellingMoney, @aIsSelling = mIsSelling FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

		-- 泅犁 辨靛酒瘤飘啊 家蜡林啊 乐绊 魄概吝捞 酒囱 版快俊绰 涝蔓 阂啊
		IF (@aCurBidGID <> 0 AND @aIsSelling = 0)
		BEGIN
			SET @aErrNo = 9			-- 9 : 泅犁 辨靛酒瘤飘啊 魄概吝捞 酒丛
			GOTO LABEL_END_LAST			 
		END

		-- 泅犁 家蜡辨靛啊 力矫茄 涝蔓陛咀苞 厚背
		IF (@aCurBidMoney >= @pBidMoney)
		BEGIN
			-- 涝蔓陛咀捞 魄概陛咀焊促 累澜
			SET @pBidMoney = @aCurBidMoney
			SET @pBidGID = @aCurBidGID
			SET @pCurBidGuildName = ISNULL((CASE @aCurBidGID
				WHEN 0 THEN N'<NONE>'
				ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = @aCurBidGID)
				END), N'<FAIL>')
			SET @aErrNo = 5			-- 5 : 家蜡辨靛焊促 涝蔓陛咀捞 累澜
			GOTO LABEL_END_LAST			 
		END

		-- 泅犁 秦寸 辨靛酒瘤飘俊 版概啊 柳青吝牢 涝蔓陛咀苞 厚背
		IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
		BEGIN
			SELECT @aCurBidGID = mCurBidGID, @aCurBidMoney = mCurBidMoney FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			IF (@aCurBidMoney >= @pBidMoney AND @aCurBidGID <> 0)
			BEGIN
				-- 涝蔓陛咀捞 魄概陛咀焊促 累澜
				SET @pBidMoney = @aCurBidMoney
				SET @pBidGID = @aCurBidGID
				SET @pCurBidGuildName = ISNULL((CASE @aCurBidGID
					WHEN 0 THEN N'<NONE>'
					ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = @aCurBidGID)
					END), N'<FAIL>')
				SET @aErrNo = 6		-- 6 : 版概涝蔓 吝牢 促弗 辨靛焊促 涝蔓陛咀捞 累澜
				GOTO LABEL_END_LAST			 
			END
		END

		-- 涝蔓啊瓷 (己傍)

		BEGIN TRAN

		-- 辨靛拌谅俊辑 捣 瞒皑
		UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney - @pBidMoney WHERE mGID = @pBidGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 

		-- 辨靛酒瘤飘 涝蔓沥焊 技泼
		IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
		BEGIN
			SELECT @aCurBidGID = mCurBidGID, @aCurBidMoney = mCurBidMoney FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

			-- 扁粮 涝蔓陛俊辑 技陛力寇 (泅犁 3%)
			SET @aCurBidMoney = (@aCurBidMoney * (100 - 3)) / 100

			-- 扁粮 涝蔓辨靛狼 涝蔓措陛 馆券
			UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney + @aCurBidMoney WHERE mGID = @aCurBidGID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END

			-- 货肺款 涝蔓沥焊 技泼
			UPDATE TblGuildAgitAuction SET mCurBidGID = @pBidGID, mCurBidMoney = @pBidMoney, mRegDate = GETDATE()  WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END	 

			-- 涝蔓辨靛 捞抚 技泼
			SET @pCurBidGuildName = ISNULL((CASE @pBidGID
				WHEN 0 THEN N'<NONE>'
				ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = @pBidGID)
				END), N'<FAIL>')
		END
		ELSE
		BEGIN
			-- 货肺款 涝蔓沥焊 技泼
			INSERT TblGuildAgitAuction (mTerritory, mGuildAgitNo, mCurBidGID, mCurBidMoney) VALUES (@pTerritory, @pGuildAgitNo, @pBidGID, @pBidMoney)
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END

			-- 涝蔓辨靛 捞抚 技泼
			SET @pCurBidGuildName = ISNULL((CASE @pBidGID
				WHEN 0 THEN N'<NONE>'
				ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = @pBidGID)
				END), N'<FAIL>')
		END
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		SET @aErrNo = 7		-- 7 : 秦寸 康瘤俊 涝蔓且 辨靛酒瘤飘啊 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

