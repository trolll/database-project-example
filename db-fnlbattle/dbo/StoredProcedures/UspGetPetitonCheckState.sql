CREATE PROCEDURE [dbo].[UspGetPetitonCheckState]
	@mPcNo		INT		-- PC锅龋
--WITH ENCRYPTION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	BEGIN TRAN

		-- 柳沥辑 惑怕甫 掘绢坷扁 傈俊 犬牢肯丰等 惑怕 吝 2老捞 瘤抄 巴甸阑 昏力 窃
		DELETE dbo.TblPetitionCheckState
		FROM dbo.TblPetitionCheckState A JOIN dbo.TblPetitionBoard B ON A.mPID = B.mPID
		WHERE A.mPcNo = @mPcNo AND B.mIsFin = CAST(1 AS BIT) AND B.mFinDate < DATEADD(dd, -2, GetDate())
		
		-- 沥府等 柳沥辑 惑怕甫 掘绢咳
		SELECT A.mCategory, B.mIsFin
		FROM dbo.TblPetitionCheckState A JOIN dbo.TblPetitionBoard  B ON A.mPID = B.mPID
		WHERE A.mPcNo = @mPcNo

	IF @@ERROR = 0
	BEGIN
		COMMIT TRAN
	END
	ELSE
	BEGIN
		SET	@aErrNo = 1	-- 矫胶袍 坷幅
		ROLLBACK TRAN
	END

	SET NOCOUNT OFF
	RETURN(@aErrNo)
END

GO

