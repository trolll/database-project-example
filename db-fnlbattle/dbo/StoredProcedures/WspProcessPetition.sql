--------------------------------
-- 按眉疙 : WspProcessPetition
-- 汲疙 : 柳沥阑 贸府茄促
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.07.21
-- 荐沥磊 : 
-- 荐沥老 : 
--------------------------------
CREATE      PROCEDURE [dbo].[WspProcessPetition]
	@mPID BIGINT -- PID

AS
	SET NOCOUNT ON

	BEGIN TRANSACTION

	UPDATE
		[dbo].[TblPetitionBoard]
	SET
		mIsFin = 1,
		mFinDate    = GetDate()
		
	WHERE
		mPID  = @mPID AND mFinDate IS NULL

	IF @@ERROR = 0
		BEGIN
			COMMIT TRANSACTION
			RETURN 0
		END
	ELSE
		BEGIN
			ROLLBACK TRANSACTION
			RETURN 1
		END

	SET NOCOUNT OFF

GO

