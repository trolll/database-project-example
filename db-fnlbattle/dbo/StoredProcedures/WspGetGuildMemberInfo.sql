--------------------------------
-- 按眉疙 : WspGetGuildMemberInfo
-- 汲疙 : 辨靛盔 沥焊甫 啊廉柯促
-- 累己磊 : 捞快籍
-- 累己老 : 2006.04.09
-- 荐沥磊 :
-- 荐沥老 :
--------------------------------
CREATE        PROCEDURE [dbo].[WspGetGuildMemberInfo]	
	@mGuildNm CHAR(12) -- 辨靛疙
AS
	SET NOCOUNT ON
	
	SELECT
		p.mNm, -- 某腐磐疙		
		s.mLevel, -- 饭骇
		p.mClass, -- 努贰胶
		gm.mNickNm, -- 龋莫
		gm.mGuildGrade -- 流鞭
		
	FROM
		[dbo].[TblGuildMember] AS gm WITH (NOLOCK)
	INNER JOIN
		[dbo].[TblGuild] AS g WITH (NOLOCK)
	ON
		gm.mGuildNo = g.mGuildNo
	INNER JOIN
		[dbo].[TblPc] AS p WITH (NOLOCK)
	ON
		gm.mPcNo = p.mNo
	INNER JOIN
		[dbo].[TblPcState] AS s WITH (NOLOCK)
	ON	
		gm.mPcNo = s.mNo
	WHERE
		g.mGuildNm = @mGuildNm		
	Order By
		gm.mGuildGrade,	
		s.mLevel DESC

GO

