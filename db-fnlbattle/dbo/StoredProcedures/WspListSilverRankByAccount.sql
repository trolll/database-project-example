CREATE  PROCEDURE [dbo].[WspListSilverRankByAccount]
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		TOP 20
		item.mUserId, (ISNULL(cnt1, 0) + ISNULL(cnt2, 0)) AS mSilver
	FROM
		(SELECT
			u.mUserId
			, sum(mCnt) cnt2
		FROM 
			FNLAccount.dbo.TblUser u
			INNER JOIN dbo.TblPcStore s
				ON	u.mUserNo = s.mUserNo
		WHERE	s.mItemNo = 409
		GROUP BY u.mUserId
		) store
	FULL OUTER JOIN	
		(SELECT
			u.mUserId
			, SUM(mCnt) AS cnt1
		FROM
			FNLAccount.dbo.TblUser u
			INNER JOIN dbo.TblPc p 
				ON u.mUserNo = p.mOwner 
					AND p.mDelDate IS NULL
			INNER JOIN	dbo.TblPcInventory i
				ON 	p.mNo = i.mPcNo 
		WHERE 	i.mItemNo = 409
		GROUP BY u.mUserId	
		) AS item	
	ON  store.mUserId = item.mUserId	
	ORDER BY mSilver DESC

GO

