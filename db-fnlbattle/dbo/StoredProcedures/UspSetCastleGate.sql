CREATE PROCEDURE [dbo].[UspSetCastleGate]
	 @pNo		BIGINT
	,@pHp		INT
	,@pIsOpen	BIT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	UPDATE dbo.TblCastleGate 
	SET 
		[mHp]=@pHp, 
		[mIsOpen]=@pIsOpen 
	WHERE [mNo] = @pNo
	IF(0 = @@ROWCOUNT)
	BEGIN
		SET @aErrNo	= 1
	END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

