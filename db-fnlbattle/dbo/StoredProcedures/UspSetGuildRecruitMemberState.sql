/******************************************************************************
**		Name: UspSetGuildRecruitMemberState
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩窍绰 某腐磐狼 惑怕甫 函版茄促.
**
**		Auth: 沥柳龋
**		Date: 2009.08.11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetGuildRecruitMemberState]
	@pPcNo		INT,
	@pState		TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	----------------------------------------------
	-- 辨靛 府捻福泼 脚没牢 沥焊 诀单捞飘
	----------------------------------------------
	UPDATE dbo.TblGuildRecruitMemberList
	SET	mState = @pState
	WHERE [mPcNo] = @pPcNo
	
	IF(0<>@@ERROR)
	BEGIN
		RETURN(1)
	END
		
	RETURN(0)

GO

