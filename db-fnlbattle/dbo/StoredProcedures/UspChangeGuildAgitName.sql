CREATE Procedure [dbo].[UspChangeGuildAgitName]
	 @pTerritory		INT			-- 康瘤锅龋
	,@pGuildAgitNo		INT OUTPUT		-- 辨靛酒瘤飘锅龋 [免仿]
	,@pOwnerGID		INT 			-- 家蜡辨靛
	,@pGuildAgitName	NVARCHAR(50)		-- 辨靛酒瘤飘捞抚
	,@pBuyingMoney	BIGINT			-- 捞抚函版陛咀
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	SET		@pGuildAgitNo = 0

	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		-- 捞抚函版磊陛 粮犁咯何 眉农
		IF NOT EXISTS(SELECT mGID FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pOwnerGID)
		BEGIN
			SET @aErrNo = 2			-- 2 : 辨靛拌谅啊 粮犁窍瘤 臼澜
			GOTO LABEL_END_LAST			 
		END
	
		-- 捞抚函版磊陛 面盒咯何 眉农
		DECLARE @aCurGuildMoney	BIGINT
		SELECT @aCurGuildMoney = mGuildMoney FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pOwnerGID
		IF (@aCurGuildMoney < @pBuyingMoney)
		BEGIN
			SET @aErrNo = 3			-- 3 : 辨靛磊陛捞 何练
			GOTO LABEL_END_LAST			 
		END

		SELECT @pGuildAgitNo = mGuildAgitNo FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID

		BEGIN TRAN

		-- 辨靛拌谅俊辑 捞抚函版磊陛 皑家
		UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney - @pBuyingMoney WHERE mGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1			-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 

		-- 辨靛捞抚 函版
		UPDATE TblGuildAgit SET mGuildAgitName = @pGuildAgitName WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo AND mOwnerGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1			-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		SET @aErrNo = 4				-- 4 : 秦寸 辨靛酒瘤飘啊 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END
	

LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

