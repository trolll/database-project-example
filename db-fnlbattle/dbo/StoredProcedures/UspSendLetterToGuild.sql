CREATE   Procedure [dbo].[UspSendLetterToGuild]
	 @pSerial		BIGINT		-- 祈瘤瘤 锅龋.
	,@pFromPcNm	CHAR(12)
	,@pToGuildNm	CHAR(12)
	,@pTitle			CHAR(30)
	,@pMsg			VARCHAR(2048)
	,@pStatus		TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		
	
	DECLARE	@aErrNo			INT
	DECLARE	@aFromPcNo		INT
	DECLARE	@aFromGuildNo		INT
	DECLARE	@aFromGuildAssNo	INT
	DECLARE	@aToGuildNo		INT
	DECLARE	@aToGuildAssNo		INT

	DECLARE 	@aSn			BIGINT
	DECLARE 	@aPcNo			INT
	DECLARE	@aEnd	DATETIME,
				@aBeginCount	INT,
				@aEndCount	INT,
				@aRowCnt		INT
				
	DECLARE	@aGuildMember	TABLE (
					mSeq		INT	 IDENTITY(1,1)	 NOT NULL,
					mPcNo		INT	 NOT NULL
				)

	SELECT		@aEnd = dbo.UfnGetEndDate(GETDATE(), 10000)
				,@aErrNo		= 0
				,@aBeginCount = 1
				,@aEndCount = 0
				,@aSn = 0
				,@aPcNo = 0

	--------------------------------------------------------------------------	
	--辨靛 沥焊 眉农 
	--------------------------------------------------------------------------
	SELECT 
		@aToGuildNo = mGuildNo
	FROM dbo.TblGuild
	WHERE mGuildNm = @pToGuildNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(1)
	 END

	--------------------------------------------------------------------------	
	-- 2007.10.09 soundkey 辨靛楷钦盔牢瘤 眉农
	--------------------------------------------------------------------------
	-- From PcNo 掘绢坷扁
	SELECT
		@aFromPcNo = mNo
	FROM dbo.TblPc
	WHERE mNm = @pFromPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(4)	-- From PcNo 掘绢坷扁 角菩
	END

	-- From GuildNo 掘绢坷扁
	SELECT
		@aFromGuildNo = mGuildNo
	FROM dbo.TblGuildMember 
	WHERE mPcNo = @aFromPcNo

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(6)	-- 辨靛糕滚啊 酒丛
	END
	
	-- 楷钦辨靛牢瘤 八荤
	IF @aFromGuildNo <> @aToGuildNo	-- 夯牢捞 加茄 辨靛啊 酒聪扼搁 楷钦辨靛牢瘤 八荤
	BEGIN
		-- From GuildAssNo 掘绢坷扁
		SELECT
			@aFromGuildAssNo = mGuildAssNo
		FROM dbo.TblGuildAssMem
		WHERE mGuildNo = @aFromGuildNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			RETURN(6)	--辨靛楷钦糕滚啊 酒丛
		END

		-- To GuildAssNo 掘绢坷扁
		SELECT
			@aToGuildAssNo = mGuildAssNo
		FROM dbo.TblGuildAssMem
		WHERE mGuildNo = @aToGuildNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			RETURN(6)	-- 辨靛楷钦糕滚啊 酒丛
		END

		-- 鞍篮 辨靛楷钦捞 酒聪扼搁 俊矾贸府
		IF @aFromGuildAssNo <> @aToGuildAssNo 
		BEGIN
			RETURN(8)	-- 鞍篮 辨靛楷钦捞 酒丛
		END
		
	END


	--------------------------------------------------------------------------	
	--祈瘤瘤 力芭
	--------------------------------------------------------------------------
	DELETE dbo.TblPcInventory 
	WHERE mSerialNo = @pSerial
			 AND mItemNo = 725

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(2)
	END
		 
	--------------------------------------------------------------------------	
	--泅犁 辨靛盔 沥焊甫 掘绰促
	--------------------------------------------------------------------------
	INSERT INTO @aGuildMember
	SELECT mPcNo
	FROM dbo.TblGuildMember 
	WHERE mGuildNo = @aToGuildNo
	
	SET @aEndCount = @@ROWCOUNT	

	--------------------------------------------------------------------------	
	--祈瘤瘤 傈价
	--------------------------------------------------------------------------	
	WHILE(@aEndCount <> 0)
	BEGIN
		SELECT 
			TOP 1	@aPcNo = mPcNo
		FROM @aGuildMember
		WHERE mSeq = @aBeginCount		

		IF @aPcNo > 0 
		BEGIN

			-- 酒捞袍 矫府倔 掘扁
			EXEC @aSn =  dbo.UspGetItemSerial 
			IF @aSn <= 0
			BEGIN
				CONTINUE
			END 	

			-- PC侩 酒捞袍 殿废
			INSERT dbo.TblPcInventory(mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
				VALUES(@aSn, @aPcNo, 724, @aEnd, 0, @pStatus, 1, 0)
			IF(0 = @@ERROR)
			BEGIN

				INSERT dbo.TblLetter(mSerialNo, mFromPcNm, mTitle, mMsg, mToPcNm) 
					VALUES(@aSn, @pFromPcNm, @pTitle, @pMsg, @pToGuildNm)
				IF(0 = @@ERROR)
				BEGIN
					SELECT @aPcNo, @aSn
				END
			 END
		END

		SET @aEndCount = @aEndCount - 1
		SET @aBeginCount = @aBeginCount + 1
	END

	RETURN(0)

GO

