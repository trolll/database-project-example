CREATE PROCEDURE [dbo].[UspSetGuildMemberGrade]
	  @pGuildNo	INT
	, @pPcNo		INT
	, @pLv		TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	 @aRowCnt		INT
	DECLARE  @aErr			INT

	SELECT  @aRowCnt = 0, @aErr = 0
	
	
	UPDATE dbo.TblGuildMember 
	SET mGuildGrade = @pLv 
	WHERE mGuildNo = @pGuildNo 
		AND mPcNo = @pPcNo
		
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT	

	IF (@aErr <> 0)
		RETURN(2)	-- db error 
		
	IF (@aRowCnt <= 0)	
		RETURN(1)	-- none row 
	
	RETURN(0)

GO

