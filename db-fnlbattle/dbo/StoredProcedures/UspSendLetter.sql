CREATE Procedure [dbo].[UspSendLetter]
	 @pSerial		BIGINT		-- 祈瘤瘤 锅龋.
	,@pFromPcNm		CHAR(12)
	,@pToPcNm		CHAR(12)
	,@pTitle		CHAR(30)
	,@pMsg			VARCHAR(2048)
	,@pErrNoStr		VARCHAR(50)	OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		

	DECLARE	@aErrNo				INT,
			@aRowCnt			INT,
			@aToPcNo			INT,
			@aLetterLimit		BIT,
			@aToGuildNo			INT,
			@aFromGuildNo		INT,
			@aFromPcNo			INT,
			@aFromGuildAssNo	INT,
			@aToGuildAssNo		INT


	SELECT	@aErrNo = 0, @pErrNoStr = 'eErrNoSqlInternalError', 
			@aRowCnt = 0, @aToPcNo = 0, @aLetterLimit = 0,
			@aToGuildNo = 0,@aFromGuildNo = 0,@aFromPcNo = 0,
			@aFromGuildAssNo = 0,@aToGuildAssNo = 0


	-- 罐绰 荤恩 某腐磐 犬牢
	SELECT 
		@aToPcNo = T1.mNo
		, @aLetterLimit = T2.mIsLetterLimit
		, @aToGuildNo = ISNULL(T3.mGuildNo, 0)
		, @aToGuildAssNo = ISNULL(T4.mGuildAssNo, 0)
	FROM dbo.TblPc T1
		INNER JOIN dbo.TblPcState T2 
				ON T1.mNo = T2.mNo	
		LEFT OUTER JOIN dbo.TblGuildMember T3
			ON T1.mNo = T3.mPcNo	
		LEFT OUTER JOIN dbo.TblGuildAssMem T4
			ON T3.mGuildNo = T4.mGuildNo
	WHERE T1.mNm = @pToPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SELECT @aErrNo	= 1, @pErrNoStr	= 'eErrNoCharNotExist'
		GOTO T_END
	END

	-- 焊郴绰 荤恩 某腐磐沥焊 拳变
	SELECT
		@aFromPcNo = mNo
		, @aFromGuildNo = ISNULL(T2.mGuildNo, 0)
		, @aFromGuildAssNo = ISNULL(mGuildAssNo,0)
	FROM dbo.TblPc T1
		LEFT OUTER JOIN dbo.TblGuildMember T2
			ON T1.mNo = T2.mPcNo
		LEFT OUTER JOIN dbo.TblGuildAssMem T3
			ON T2.mGuildNo = T3.mGuildNo
	WHERE mNm = @pFromPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SELECT @aErrNo	= 1, @pErrNoStr	= 'eErrNoCharNotExist'
		GOTO T_END
	END

	-- 夯牢捞 加茄 辨靛啊 酒聪扼搁 楷钦辨靛牢瘤 八荤
	-- 鞍篮 辨靛楷钦捞 酒聪扼搁 俊矾贸府
	IF ( @aLetterLimit = 1 )
		AND (
			( ( @aFromGuildNo <> 0 AND  @aToGuildNo <> 0 ) 
				AND (@aFromGuildNo <> @aToGuildNo )  )			
			OR ( ( @aFromGuildAssNo <> 0 AND  @aToGuildAssNo <> 0 ) 
				AND ( @aFromGuildAssNo <> @aToGuildAssNo )  )			
			OR ( @aToGuildNo = 0 )
			OR ( @aFromGuildNo = 0 )
		)
	BEGIN
		SELECT @aErrNo	= 4, @pErrNoStr	= 'eErrNoRefuseTheLetter'
		GOTO T_END
	END 


	BEGIN TRAN
	
		-----------------------------------------------
		-- 祈瘤 沥焊 殿废
		-----------------------------------------------
		INSERT dbo.TblLetter(mSerialNo, mFromPcNm, mTitle, mMsg, mToPcNm) 
			VALUES(@pSerial, @pFromPcNm, @pTitle, @pMsg, @pToPcNm)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN
			SELECT @aErrNo	= 2, @pErrNoStr	= 'eErrNoLetterCannotSend'
			GOTO T_ERR
		END
		 
		-----------------------------------------------
		-- eUnusedLetterItemID:721:荤侩救茄 祈瘤.
		-- eKnownLetterItemID :724:荤侩茄 祈瘤.
		-- 佬菌绰瘤 咯何绰 confirm蔼栏肺 茄促.
		-----------------------------------------------
		UPDATE dbo.TblPcInventory 
		SET 
			mItemNo =724, 	-- 荤侩茄 祈瘤
			mIsConfirm = 0, 
			mPcNo = @aToPcNo 
		WHERE mSerialNo = @pSerial
				AND mItemNo = 721
	
		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN
			SELECT @aErrNo	= 3, @pErrNoStr	= 'eErrNoItemNotExist'
			GOTO T_ERR	 
		END		 

T_ERR:		
	IF @aErrNo = 0
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
	
T_END:	
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

