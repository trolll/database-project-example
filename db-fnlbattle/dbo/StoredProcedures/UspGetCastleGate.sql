CREATE PROCEDURE [dbo].[UspGetCastleGate]
	 @pNo		BIGINT
	,@pHp		INT		OUTPUT
	,@pIsOpen	BIT		OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.

	SELECT 
		@pHp=[mHp], 
		@pIsOpen=[mIsOpen] 
	FROM dbo.TblCastleGate 
	WHERE [mNo] = @pNo
	IF(0 = @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

