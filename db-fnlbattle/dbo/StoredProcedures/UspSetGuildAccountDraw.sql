CREATE Procedure [dbo].[UspSetGuildAccountDraw]	-- 辨靛拌谅俊 捣 免陛
	 @pGID			INT			-- 家蜡辨靛
	,@pGuildMoney		BIGINT OUTPUT	-- 免陛陛咀 [涝仿/免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF EXISTS(SELECT mGID FROM TblGuildAccount WITH(NOLOCK) WHERE mGID = @pGID)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		DECLARE @aGuildMoney	BIGINT
		SELECT @aGuildMoney = mGuildMoney FROM TblGuildAccount WITH(NOLOCK) WHERE mGID = @pGID
		IF (@aGuildMoney >= @pGuildMoney)
		BEGIN
			-- 辨靛拌谅俊 免陛且 咀荐 乐澜

			BEGIN TRAN
	
			UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney - @pGuildMoney WHERE mGID = @pGID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END	 

			SELECT @pGuildMoney = mGuildMoney FROM TblGuildAccount WHERE mGID = @pGID
		END
		ELSE
		BEGIN
			-- 辨靛拌谅俊 儡陛何练

			SET @aErrNo = 2		-- 2 : 辨靛拌谅 儡陛何练
			GOTO LABEL_END_LAST			 
		END
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		BEGIN TRAN

		INSERT TblGuildAccount (mGID, mGuildMoney) VALUES (@pGID, 0)
		IF(0 <> @@ERROR)
		BEGIN
			ROLLBACK TRAN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END_LAST
		END

		COMMIT TRAN
		SET @aErrNo = 3			-- 3 : 辨靛拌谅啊 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

