CREATE PROCEDURE [dbo].[UspSetLetterLimit]
	 @pIsOn			BIT
	 ,@pPcNo		INT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	


	UPDATE dbo.TblPcState 
	SET 
		[mIsLetterLimit]=@pIsOn
	WHERE [mNo] = @pPcNo
	
	IF(0 = @@ROWCOUNT)
	BEGIN
		SET @aErrNo	= 1 -- PC啊 粮犁窍瘤 臼澜
	END
	
	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo	= 2	-- DB Err
	END	

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

