CREATE  PROCEDURE [dbo].[UspResetCachingItem]
	 @pPcNo			INT
	,@pSerial		BIGINT
	,@pCachingCnt	INT			-- 啊皑蔼.(溜, 澜荐档 啊瓷)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			
	
	DECLARE	 @aRowCnt		INT
				,@aErrNo		INT
	DECLARE	 @aCnt	INT

	SELECT		@aErrNo = 0,
				@aCnt = 0,
				@aRowCnt = 0
		
	---------------------------------------
	-- 牢亥配府 肮荐 眉农 
	---------------------------------------
	SELECT 
		@aCnt = mCnt
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pSerial
			AND mPcNo = @pPcNo
		
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END			
	END
	
	---------------------------------------
	--  reset饶 俺荐绰 弥家茄 1俺 捞惑捞绢具 茄促. 
	---------------------------------------
	SET @aCnt = @aCnt + @pCachingCnt
	IF @aCnt < 1
	BEGIN
		SET @aErrNo = 2
		GOTO T_END			
	END

	---------------------------------------
	--  肮荐 府悸
	---------------------------------------
	UPDATE dbo.TblPcInventory 
	SET 
		mCnt = @aCnt 
	WHERE mSerialNo = @pSerial

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 3
		GOTO T_END			
	END		

T_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

