CREATE PROCEDURE [dbo].[UspGetReplyQuickSupplication]
	@pQSID		BIGINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo	INT
	DECLARE	@aReply	VARCHAR(1000)

	SET @aErrNo = 0

	IF NOT EXISTS (	SELECT * 
					FROM dbo.TblQuickSupplicationReply 
					WHERE mQSID = @pQSID)
	BEGIN
		SET @aErrNo = 1
		GOTO T_END
	END	
	
	SELECT @aReply = mReply
	FROM dbo.TblQuickSupplicationReply
	WHERE mQSID = @pQSID

T_END:
	SELECT	@aErrNo, @aReply

GO

