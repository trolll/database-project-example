CREATE PROCEDURE [dbo].[UspExitGuild]
	 @pGuildNo	INT
	,@pPcNo		INT	
--WITH ENCRYPTION
AS
	SET NOCOUNT ON
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	IF EXISTS(	SELECT * 
				FROM dbo.TblGuildMember WITH(NOLOCK)
				WHERE ([mGuildNo]=@pGuildNo) 
						AND ([mPcNo]=@pPcNo) 
						AND ([mGuildGrade]) = 0)
	BEGIN
		SET	@aErrNo	= 1
		GOTO LABEL_END
	END
	
	DELETE dbo.TblGuildMember 
	WHERE ([mGuildNo]=@pGuildNo) 
			AND ([mPcNo]=@pPcNo) 
	IF(1 <> @@ROWCOUNT)
	BEGIN
		SET @aErrNo = 2
		GOTO LABEL_END		
	END	
	
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

