/******************************************************************************
**		Name: WspCnsmUnReg
**		Desc:  困殴惑痢 秒家(款康砒俊辑绰 拘幅矫 困殴 惑痢狼 酒捞袍阑 昏力窍咯 牢亥配府俊 犁 殿废 贸府 茄促)
**
**		Auth: JUDY
**		Date: 2011-01-05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspCnsmUnReg]
	@pCnsmSn  BIGINT	-- 困殴 矫府倔 锅龋	
AS
	SET NOCOUNT ON   ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;

	DECLARE @aPcNo		INT
				, @aItemNo	INT
				, @aIsStack		SMALLINT
				, @pCatetory TINYINT 
				,@pItemSerial BIGINT 	-- 雀荐等 酒捞袍Sn, 扁粮巴俊 捍钦登菌促搁 措惑Sn
				,@pResultCnt  INT 		-- 雀荐等 俺荐 or 捍钦 搬苞 弥辆俺荐
				,@pCurCnt INT  -- 雀荐等 俺荐
				,@pPrice INT  -- 俺寸 魄概 啊拜				

	SELECT @aRowCnt = 0, @aErrNo= 0  ;
	
	SELECT   
		@aPcNo		= [mPcNo],
		@aItemNo	= mItemNo,
		@aIsStack = IMaxStack	
	FROM dbo.TblConsignment T1
		INNER JOIN FNLParm.dbo.DT_ITEM T2
			ON T1.mItemNo = T2.IID
	WHERE  mCnsmNo  =@pCnsmSn;   	
	
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 1;  -- 困殴 酒捞袍捞 粮犁窍瘤 臼嚼聪促.(TblConsignment)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 郴何 坷幅
		END
		RETURN(@aErrNo);
	END  
	
	EXEC @aErrNo = dbo.WspCnsmUnregister @pCnsmSn, @aItemNo, @aIsStack, @aPcNo, @pCatetory OUTPUT, @pItemSerial OUTPUT, @pResultCnt OUTPUT,  @pCurCnt OUTPUT, @pPrice OUTPUT

GO

