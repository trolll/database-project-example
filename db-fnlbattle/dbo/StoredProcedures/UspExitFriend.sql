CREATE PROCEDURE [dbo].[UspExitFriend]
	 @pOwnerPcNo	INT
	 ,@pPcNo	INT
	,@pFriendPcNm	CHAR(12)	-- 傈崔牢磊 沥焊 函版 
--WITH ENCRYPTION
AS
	SET NOCOUNT ON
	
	DELETE dbo.TblPcFriend 
	WHERE (mOwnerPcNo=@pOwnerPcNo) 
			AND ( mFriendPcNm = @pFriendPcNm ) 
	IF(1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END
		
	RETURN(0)

GO

