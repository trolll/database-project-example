/******************************************************************************
**		File: 
**		Name: UspSetChaosBattleOccupyInfo
**		Desc: 墨坷胶 硅撇 辑滚狼 痢飞 沥焊甫 汲沥茄促.
**
**		Auth: 辫 堡挤
**		Date: 2009-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetChaosBattleOccupyInfo]
	  @pSvrNo				SMALLINT
	, @pTerritory			INT
	, @pChaosBattleSvrNo	SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT;
	DECLARE	@aRowCnt	INT;
	
	UPDATE	dbo.TblChaosBattleOccupyInfo 
	SET 
		[mOccupySvrNo] = @pSvrNo, 
		[mOccupyDate] = GETDATE() 
	WHERE 
		[mTerritory] = @pTerritory 
			AND [mChaosBattleSvrNo] = @pChaosBattleSvrNo;
	
	SELECT	@aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	
	IF(@aErrNo <> 0)
	BEGIN
		RETURN(1);	-- DB Error
	END
	
	IF(@aRowCnt = 0)
	BEGIN
		INSERT INTO 
			dbo.TblChaosBattleOccupyInfo ([mTerritory], [mOccupySvrNo], [mOccupyDate], [mChaosBattleSvrNo]) 
		VALUES 
			(@pTerritory, @pSvrNo, GETDATE(), @pChaosBattleSvrNo);
		
		SELECT	@aErrNo = @@ERROR;
		
		IF(@aErrNo <> 0)
		 BEGIN
			RETURN(2);	-- DB Error
		 END
	END
	
	RETURN(0);

GO

