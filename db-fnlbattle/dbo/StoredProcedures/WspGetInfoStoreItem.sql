CREATE       PROCEDURE [dbo].[WspGetInfoStoreItem] 
	@mSerialNo	BIGINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		mUserNo,
		mItemNo,
		mIsConfirm,
		mStatus,
		mCnt,
		mCntUse,
		mIsSeizure
	FROM dbo.TblPcStore
	WHERE mSerialNo = @mSerialNo

GO

