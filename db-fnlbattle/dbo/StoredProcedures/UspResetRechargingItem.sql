CREATE  PROCEDURE [dbo].[UspResetRechargingItem]
	 @pPcNo			INT
	,@pSerial		BIGINT
	,@pCachingCnt	INT			-- 啊皑蔼.(溜, 澜荐档 啊瓷)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		
	
	DECLARE	 @aRowCnt		INT
				,@aErrNo		INT
				,@aCntUse		INT

	SELECT		@aErrNo = 0,	@aRowCnt = 0, @aCntUse = 0

	SELECT 
		@aCntUse = mCntUse
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pSerial

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END			
	END
	
	SET @aCntUse = @aCntUse + @pCachingCnt
	IF @aCntUse < 0
	BEGIN
		SET @aErrNo = 2
		GOTO T_END			
	END

	UPDATE dbo.TblPcInventory 
	SET 
		mCntUse = @aCntUse 
	WHERE mSerialNo = @pSerial

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 3
		GOTO T_END			
	END		

T_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

