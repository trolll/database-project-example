CREATE Procedure [dbo].[UspAddGuildAgitTime]
	 @pTerritory		INT			-- 康瘤锅龋
	,@pOwnerGID		INT			-- 家蜡辨靛
	,@pBuyingMoney	BIGINT			-- 措咯陛咀
	,@pGuildAgitNo		INT OUTPUT		-- 辨靛酒瘤飘锅龋
	,@pLeftMin		INT OUTPUT		-- 父丰盒 (眠啊) [涝仿/免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	SET 		@pGuildAgitNo = 0

	-- 措咯陛 粮犁咯何 眉农
	IF NOT EXISTS(SELECT mGID FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pOwnerGID)
	BEGIN
		SET @aErrNo = 2			-- 2 : 辨靛拌谅啊 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END

	-- 措咯陛 面盒咯何 眉农
	DECLARE @aCurGuildMoney	BIGINT
	SELECT @aCurGuildMoney = mGuildMoney FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pOwnerGID
	IF (@aCurGuildMoney < @pBuyingMoney)
	BEGIN
		SET @aErrNo = 3			-- 3 : 辨靛磊陛捞 何练
		GOTO LABEL_END_LAST			 
	END

	-- 辨靛酒瘤飘 措咯扁埃 楷厘
	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID)
	BEGIN
		-- 捞固 牢胶畔胶啊 粮犁

		BEGIN TRAN

		-- 辨靛拌谅俊辑 捣 瞒皑
		UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney - @pBuyingMoney WHERE mGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 

		-- 父丰盒 刘啊
		UPDATE TblGuildAgit SET mLeftMin = mLeftMin + @pLeftMin WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO LABEL_END			 
		END	 

		SELECT @pGuildAgitNo = mGuildAgitNo, @pLeftMin = mLeftMin FROM TblGuildAgit WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		SET @aErrNo = 4			-- 4 : 秦寸 辨靛酒瘤飘啊 粮犁窍瘤 臼澜
		GOTO LABEL_END_LAST			 
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

