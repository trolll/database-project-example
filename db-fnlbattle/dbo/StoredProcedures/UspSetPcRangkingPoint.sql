/******************************************************************************
**		File: 
**		Name: UspSetPcRangkingPoint
**		Desc: 漂沥 PC狼 珐欧馆康 器牢飘甫 函版茄促.
**
**		Auth: 辫 堡挤
**		Date: 2009-03-23
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcRangkingPoint]
	  @pPcNo			INT
	, @pContribution	INT
	, @pGuardian		INT
	, @pTeleportTower	INT
	, @pPVP				INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT;
	DECLARE	@aRowCnt	INT;

	UPDATE	dbo.TblPcRankingPoint	
	SET 
		  mContribution = @pContribution
		, mGuardian		= @pGuardian
		, mTeleportTower= @pTeleportTower
		, mPVP			= @pPVP
	WHERE	mPcNo = @pPcNo;
	
	SELECT	@aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	
	IF((0 <> @aErrNo) OR (1 <> @aRowCnt))
	 BEGIN
		RETURN(1);
	 END
	
	RETURN(0);

GO

