CREATE PROCEDURE [dbo].[UspGetDiscipleMember]
	@pMaster	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT a.[mDisciple], RTRIM(b.[mNm]), c.[mLevel], b.[mClass], a.[mMemPoint], a.[mType], ISNULL(c.[mLogoutTm], getdate())
	FROM dbo.TblDiscipleMember As a 
		JOIN dbo.TblPc As b 
			ON a.[mDisciple] = b.[mNo]
		JOIN dbo.TblPcState As c 
			ON a.[mDisciple] = c.[mNo]
	WHERE mMaster = @pMaster
	ORDER BY a.[mRegDate] ASC
		
	SET NOCOUNT OFF

GO

