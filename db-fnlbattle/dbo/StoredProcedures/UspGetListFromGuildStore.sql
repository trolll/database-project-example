/******************************************************************************
**		Name: UspGetListFromGuildStore
**		Desc: 辨靛 芒绊 府胶飘甫 啊廉柯促.
**
**		Auth:  辫 堡挤
**		Date:  2008-01-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2009.04.17	JUDY				牢郸胶 腮飘 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetListFromGuildStore]
	  @pOwnerGuild	INT
	, @pGrade	TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 300		


	SELECT 
		[mSerialNo], 
		[mItemNo], 
		[mIsConfirm], 
		[mStatus], 
		[mCnt], 
		[mCntUse],
		[mOwner],
		[mPracticalPeriod]
	FROM dbo.TblGuildStore WITH(INDEX(CL_TblGuildStore_1), NOLOCK )
	WHERE mGuildNo = @pOwnerGuild 
			AND mGrade = @pGrade
			AND mEndDate >  CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY mItemNo

GO

