/******************************************************************************  
**  File: 
**  Name: UspAddGuildSkillTreeNodeItem
**  Desc: 辨靛 胶懦飘府畴靛酒捞袍 窍唱甫 眠啊茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspAddGuildSkillTreeNodeItem]
	@pGuildNo		INT,
	@pSTNIID		INT,
	@pCreatorPcNo	INT,
	@pValidSec		INT,	 -- 864000000檬(10000老)捞搁 公茄措, 2020-01-01 00:00:00 俊 父丰登绰 巴栏肺 距加茄促
	@pLeftMin	INT OUTPUT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

--	DECLARE	@aErrNo		INT
--	SET		@aErrNo		= 0	

	-- 捞固 乐促搁 父丰 朝楼父 技泼窍绊 眠啊 己傍栏肺 埃林茄促
	declare @aSTNIID INT
	select @aSTNIID=mSTNIID from TblGuildSkillTreeInventory
	where mGuildNo=@pGuildNo and mSTNIID = @pSTNIID

	DECLARE	 @aEndDay		SMALLDATETIME
	IF 864000000 = @pValidSec
		BEGIN
		SELECT	 @aEndDay = dbo.UfnGetEndDate(GETDATE(), 864000000/(3600 * 24))
		END
	ELSE
		BEGIN
		SELECT	 @aEndDay = dateadd(second, @pValidSec, GETDATE())
		END

	if @aSTNIID is not NULL
	begin
		update TblGuildSkillTreeInventory	
		set mEndDate = @aEndDay,
			mCreatorPcNo = @pCreatorPcNo,
			mExp = 0
		where mGuildNo=@pGuildNo and mSTNIID = @pSTNIID
		if @@error <> 0
		begin
			return 1 -- DB 俊矾
		end
		return 0
	end
	
	insert into TblGuildSkillTreeInventory
	values(default, @pGuildNo, @pSTNIID, @aEndDay, @pCreatorPcNo, 0)
	if @@error <> 0
	begin
		return 1 -- DB 俊矾
	end

	SET @pLeftMin = DATEDIFF(mi,GETDATE(), @aEndDay)

	return 0

GO

