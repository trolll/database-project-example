/******************************************************************************
**		File: UspBeginWorld.sql
**		Name: UspBeginWorld
**		Desc: 鞘靛辑滚啊 矫累灯促. 檬扁拳殿阑 茄促.
**
**		Warn: 1.父丰等 item阑 昏力茄促. 
**			:   (厘厚殿阑 CASCADE甫 捞侩秦辑 昏力啊 登档废 茄促.)
**			: 1.阁胶磐啊 家蜡茄 item档 昏力甫 茄促.
**			: 1.login吝牢 PC甫 logout矫挪促.
**
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2008.05.07	JUDY				扁埃力 酒捞袍 汗备 扁瓷 眠啊
**		2008.05.16	JUDY				辆丰 酒捞袍 沥府	
**		2008.08.01	JUDY				捞亥飘 扁埃 辆丰 窃荐 贸府 扁瓷 坷幅.
**		2009.01.21	JUDY				譬醋1瞒(捞亥飘 酒捞袍 啊廉坷绰 吝... 牢郸胶啊 捞惑窍霸 鸥芭唱,, 促弗 巩力啊 惯积茄促.) 
**		2009.04.23	JUDY				捞亥飘 酒捞袍 扁埃 楷厘 眠啊
**		2010.04.06	JUDY				老老 涅胶飘 檬扁拳 茄促.
**		2010.07.29	沥柳龋				豪牢等 带怜 焊碍
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspBeginWorld]
	@pSvrNo	SMALLINT
	, @pIsItemRecovery BIT
	, @pItemRecoveryTgTm	INT		-- Recovery Item Target interval time 
	, @pItemRecoveryTm	INT			-- Recovery item default time
	, @pItemExpireDD	SMALLINT = 0	-- 酒捞袍 昏力甫 困茄 昏力老( 泅犁老 -5老傈 辆丰酒捞袍 昏力)
AS
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;
	
	DECLARE @aLastSupportMinute INT,
					@aErr				INT,
					@aStep				SMALLINT,
					@aItemExpireDate	SMALLDATETIME,
					@aCurrDate			SMALLDATETIME,
					@aArrExpireItem		VARCHAR(8000),
					@aSupportTime		INT,
					@aLastTime			INT,
					@aQuestNoStr		VARCHAR(3000),
					@aEventQuestNoStr	VARCHAR(3000)
		
	DECLARE @aItemNo			TABLE (
				mItemNo		INT
			)				
			
	SELECT  @aLastSupportMinute = 0
			, @aErr =0
			, @aStep = 0 -- 磊悼 楷厘 累诀
			, @aItemExpireDate = DATEADD(DD, -@pItemExpireDD, GETDATE() )
			, @aCurrDate = GETDATE()
			, @aArrExpireItem = ''
			, @aSupportTime = 0
			, @aLastTime = 0
			, @aQuestNoStr = ''		
	
	-- Event Expire item 
	-- @aCurrDate -> @aItemExpireDate 函版
	EXEC FNLParm.dbo.UspGetExpireEventItem @pSvrNo, @aItemExpireDate, @aArrExpireItem OUTPUT		
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 2	-- 付瘤阜 辑滚 瘤盔 辑滚 沥焊 掘扁 角菩
		GOTO TRN_END	
	END 
	
	-- 老老 涅胶飘 檬扁拳 沥焊甫 掘绢柯促.
	-- 檬扁拳 秦具瞪 涅胶飘 锅龋甫 备茄促.
	EXEC FNLParm.dbo.UspGetExpireQuest @pSvrNo, @aCurrDate, @aQuestNoStr OUTPUT, @aSupportTime OUTPUT, @aLastTime OUTPUT
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 1	-- sql error
		GOTO TRN_END	
	END 	
	
	-- 肯丰等 捞亥飘 涅胶飘 沥焊甫 掘绢柯促.
	-- 昏力 秦具瞪 涅胶飘 锅龋甫 备茄促.
	EXEC FNLParm.dbo.UspGetExpireEventQuest @pSvrNo, @aEventQuestNoStr OUTPUT
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 1	-- sql error
		GOTO TRN_END	
	END 		


	INSERT INTO @aItemNo(mItemNo)
	SELECT CONVERT(INT, element) mObjID
	FROM  dbo.fn_SplitTSQL(  @aArrExpireItem, ',' )
	WHERE LEN(RTRIM(element)) > 0
	
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 2	-- 付瘤阜 辑滚 瘤盔 辑滚 沥焊 掘扁 角菩
		GOTO TRN_END	
	END 		
	
	IF @pItemExpireDD > 0
		SET @pItemExpireDD = -@pItemExpireDD		-- 扁埃 辆丰 贸府甫 困秦辑.
		
	IF @pIsItemRecovery = 1
	BEGIN
	
		EXEC @aLastSupportMinute = FNLParm.dbo.UspGetParmSvrLastSupportDate @pSvrNo
		SELECT @aErr = @@ERROR
		
		IF @aErr <> 0 
		BEGIN
			SET @aErr = 2	-- 付瘤阜 辑滚 瘤盔 辑滚 沥焊 掘扁 角菩
			GOTO TRN_END
		END
							
		IF @aLastSupportMinute >= @pItemRecoveryTgTm
		BEGIN	
			SET @aLastSupportMinute = @aLastSupportMinute + @pItemRecoveryTm	-- 扁夯 楷厘 坷瞒 矫埃
			
			EXEC @aErr = dbo.UspUptItemDate @aLastSupportMinute		-- 扁埃力 酒捞袍 磊悼 楷厘
			
			IF @aErr <> 0 
			BEGIN
				SET @aErr = 3	-- 扁埃力 酒捞袍 磊悼 楷厘 角菩
				GOTO TRN_END
			END

		END 
		ELSE
			SET @aLastSupportMinute = 0	-- 磊悼 焊惑捞 捞风绢瘤瘤 臼绰促搁 焊惑 矫埃 努府绢 矫难霖促.

	END 

	/*
	-- 瞒捞啊 24矫埃 捞惑捞搁 公炼扒 檬扁拳茄促
	-- 涅胶飘 檬扁拳 矫埃 6矫(辑滚俊 窍靛内爹 登绢乐澜)
	IF 24 <= @aSupportTime OR @aLastTime < 6
	BEGIN
		-- 檬扁拳
		EXEC @aErr = dbo.UspPopPcQuestConditionAll @aQuestNoStr		-- 涅胶飘 昏力
		IF @aErr <> 0 
		BEGIN
			SET @aErr = 1	-- sql error
			GOTO TRN_END
		END	
	END
	*/
	
	BEGIN TRAN
		
		SET @aStep = 1	-- 酒捞袍 盎脚 累诀

		-- 1. Inventory, Store, GuildStroe 扁埃 辆丰 酒捞袍 昏力 棺 胶琶 辆丰等 酒捞袍 昏力
		-- 1. Event 辆丰 酒捞袍 昏力
		DELETE dbo.TblPcInventory
		WHERE mEndDate <= @aItemExpireDate	
			OR mCnt < 1		
			OR mPcNo IN(0,1)	
			OR mItemNo IN (				
				SELECT mItemNO
				FROM @aItemNo				
			)	
			
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		

		
		DELETE dbo.TblPcStore
		WHERE mEndDate <= @aItemExpireDate
			OR mCnt < 1
			OR mItemNo In (				
				SELECT mItemNO
				FROM @aItemNo							
			)					
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		
		
		DELETE dbo.TblGuildStore
		WHERE mEndDate <= @aItemExpireDate
			OR mCnt < 1
			OR mItemNo In (				
				SELECT mItemNO
				FROM @aItemNo					
			)		
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		
				
		
		-- 肺弊酒眶 矫埃 犁 汲沥 		
		UPDATE dbo.TblPcState 
		SET 
			mLogoutTm = GETDATE() 
		WHERE mLogoutTm < mLoginTm
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		

		-- 粮犁窍瘤 臼绰 辨靛窍快胶  檬措厘沥焊 力芭
		DELETE dbo.TblGuildAgitTicket 
		WHERE NOT EXISTS(	SELECT mSerialNo 
							FROM dbo.TblPcInventory 
							WHERE mSerialNo = mTicketSerialNo)

		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		

		-- 粮犁窍瘤 臼绰 圈  檬措厘沥焊 力芭
		DELETE dbo.TblBanquetHallTicket 
		WHERE NOT EXISTS(	SELECT mSerialNo 
							FROM dbo.TblPcInventory 
							WHERE mSerialNo = mTicketSerialNo	)
							
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		
						
		-- 粮犁窍瘤 臼绰 辨靛啊 涝蔓茄 辨靛窍快胶 版概沥焊 檬扁拳
		UPDATE dbo.TblGuildAgitAuction
		SET		
			mCurBidGID = 0, mCurBidMoney = 0, mRegDate = getdate()
		WHERE mCurBidGID <> 0 
				AND (NOT EXISTS(
								SELECT mGuildNo 
								FROM dbo.TblGuild 
								WHERE mGuildNo = mCurBidGID))
								
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END										

		-- 粮犁窍瘤 臼绰 辨靛啊 啊瘤绊 乐绰 辨靛窍快胶 沥焊 檬扁拳
		UPDATE dbo.TblGuildAgit
		SET 
			mOwnerGID = 0, 
			mGuildAgitName = '', 
			mRegDate = getdate(), 
			mLeftMin = 4320, 
			mIsSelling = 0, 
			mSellingMoney = 100000, 
			mBuyingMoney = 0
		WHERE mOwnerGID <> 0 
				AND (NOT EXISTS(
								SELECT mGuildNo 
								FROM dbo.TblGuild 
								WHERE mGuildNo = mOwnerGID))

		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END			

		-- 扁茄捞 瘤抄 眠啊加己 酒捞袍 锅龋 檬扁拳
		UPDATE dbo.TblPcInventory
		SET mApplyAbnItemNo = 0
		WHERE mApplyAbnItemEndDate < GETDATE()
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END					
		
		-- 昏力等 祈瘤瘤 沥焊 昏力 
		DELETE dbo.TblLetter 
		WHERE mSerialNo NOT IN (
			SELECT mSerialNo
			FROM dbo.TblPcInventory
			WHERE mItemNo IN (721,722,724,725)
			UNION ALL
			SELECT mSerialNo
			FROM dbo.TblPcStore
			WHERE mItemNo IN (721,722,724,725)
		)
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END					
		
		-- 父丰等 蜡丰 酒捞袍 沥焊 昏力
		DELETE dbo.TblPcGoldItemEffect 		
		WHERE mEndDate <= @aItemExpireDate
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END					
		
		DELETE dbo.TblGuildGoldItemEffect		
		WHERE mEndDate <= @aItemExpireDate
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END				
		
		-- JJH
		-- 规凯艰 棺 惑磊凯艰 酒捞袍 昏力
		DELETE dbo.TblPcInventory
		WHERE mItemNo IN (
			SELECT mIID
			FROM dbo.TblLimitedResourceItem
			WHERE mResourceType IN (998,999)
		)
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END	

		-- 规凯艰 棺 惑磊凯艰 府家胶 汗盔	(窍靛内爹 -_-) (1俺究父 乐绢具茄促 - 扁裙狼档)
		UPDATE dbo.TblLimitedResource
		SET mRemainerCnt = 1
		WHERE mResourceType in (998,999)
			AND mRemainerCnt > 1

		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END				
		
		--辆丰等 捞亥飘 涅胶飘绰 PcQuest俊辑 昏力茄促.
		EXEC @aErr = dbo.UspDeletePcQuestExpireEvent @aEventQuestNoStr		-- 涅胶飘 昏力
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END				
					
												
	COMMIT TRAN		
	
TRN_END:

	IF @pIsItemRecovery = 1
	BEGIN
				
		IF @aStep = 1 
			AND @aErr <> 0	
			AND @aLastSupportMinute >= @pItemRecoveryTgTm		
		BEGIN	
				
			SET @aLastSupportMinute= -@aLastSupportMinute
			EXEC @aErr = dbo.UspUptItemDate @aLastSupportMinute		-- auto item recovery rollback
			
			IF @aErr <> 0			 
				SET @aStep = 2	-- auto item recovery rollback failed 	 
								
		END
		
		-- auto item recovery complete : support table = 2020-01-01 00:00:00 
		IF @aErr = 0 
			AND @aLastSupportMinute >= @pItemRecoveryTgTm
			AND @aStep IN ( 0, 1)
		BEGIN
			EXEC FNLParm.dbo.UspSetParmSvrLastSupportDate @pSvrNo
		END
		
	END 

	SELECT @aLastSupportMinute mItemSupportMinute, @aErr mError, @aStep mStep

GO

