CREATE PROCEDURE [dbo].[UspGetGuildByName]
	 @pGuildNm		CHAR(12)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT a.[mGuildSeqNo], a.[mGuildNo], a.[mGuildMark], RTRIM(a.[mGuildMsg]), a.[mRegDate], 
		   b.[mGuildAssNo], a.[mRewardExp]
	FROM dbo.TblGuild AS a 
		LEFT OUTER JOIN dbo.TblGuildAssMem AS b 
		ON(a.mGuildNo = b.mGuildNo)
	WHERE a.[mGuildNm] = @pGuildNm

GO

