/******************************************************************************
**		Name: UspSelectPcQuestCondition
**		Desc: 某腐磐 涅胶飘 炼扒 八荤
**
**		Auth: 沥柳龋
**		Date: 2010-02-18
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSelectPcQuestCondition]
     @pPcNo			INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT 
			T2.mQuestNo,
			T2.mMonsterNo,
			T2.mCnt
    FROM dbo.TblPcQuest as T1 
		INNER JOIN dbo.TblPcQuestCondition as T2
         ON T1.mPcNo = T2.mPcNo
			AND T1.mQuestNo = T2.mQuestNo
    WHERE T1.mPcNo = @pPcNo 
		AND T1.mValue < 99

GO

