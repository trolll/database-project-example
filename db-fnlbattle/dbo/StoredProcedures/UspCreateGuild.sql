 /******************************************************************************
**		Name: UspCreateGuild
**		Desc: 辨靛 积己
**
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      20100730	辫碍龋		辨靛付农 函版
*******************************************************************************/ 
CREATE  PROCEDURE [dbo].[UspCreateGuild]
	  @pGuildNm CHAR(12)  
	 ,@pPcNo  INT  
	 ,@pGuildNo INT   OUTPUT  
	 ,@pSeqNo INT  OUTPUT 
	 ,@pErrNoStr VARCHAR(50) OUTPUT  
AS  
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	DECLARE @aErrNo  INT  
	SET  @aErrNo  = 0  
	SET  @pErrNoStr = 'eErrNoSqlInternalError'  

	-- check唱 index啊 瘤沥灯栏骨肺 DBMS俊 狼秦辑 坷幅啊 八免登瘤父  
	-- 利寸茄 坷幅内靛甫 馆券窍瘤 臼栏骨肺 林肺 惯积且 坷幅甫 刚历 八荤茄促.  
	IF EXISTS( SELECT [mGuildNm]   
		FROM dbo.TblGuild   
		WHERE [mGuildNm] = @pGuildNm )  
	BEGIN  
		SET @aErrNo = 51  
		SET @pErrNoStr = 'eErrNoGuildAlreadyExistNm'  
		RETURN(@aErrNo)  
	END  

	IF EXISTS( SELECT [mPcNo]   
		FROM dbo.TblGuildMember WHERE [mPcNo] = @pPcNo )  
	BEGIN  
		SET @aErrNo = 52  
		SET @pErrNoStr = 'eErrNoCharAlreadyEnterGuild'  
		RETURN(@aErrNo)  
	END  
  
	BEGIN TRAN   

		SET @pSeqNo  = 0  
		INSERT INTO dbo.TblGuild([mGuildNm], [mGuildSeqNo])   
		VALUES(  
		@pGuildNm,   
		@pSeqNo)  
		
		IF(0 <> @@ERROR)  
		BEGIN  
			SET @aErrNo = 1  
			GOTO LABEL_END    
		END      
		SET @pGuildNo = @@IDENTITY  
		 
		INSERT INTO dbo.TblGuildMember([mGuildNo], [mPcNo], [mGuildGrade])  
		VALUES(  
		@pGuildNo,    
		@pPcNo,   
		0)  
		IF(0 <> @@ERROR)  
		BEGIN  
			SET @aErrNo = 2  
			GOTO LABEL_END    
		END   

	LABEL_END:    
	IF(0 = @aErrNo)  
		COMMIT TRAN  
	ELSE  
		ROLLBACK TRAN  

	SET NOCOUNT OFF   
	RETURN(@aErrNo)

GO

