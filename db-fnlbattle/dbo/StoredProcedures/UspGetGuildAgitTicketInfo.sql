CREATE PROCEDURE [dbo].[UspGetGuildAgitTicketInfo]
	 @pTicketSerialNo	BIGINT				-- 萍南矫府倔锅龋
	,@pTerritory		INT OUTPUT			-- 康瘤锅龋 [免仿]
	,@pGuildAgitNo		INT OUTPUT			-- 辨靛酒瘤飘锅龋 [免仿]
	,@pOwnerGID		INT OUTPUT			-- 家蜡辨靛 [免仿]
	,@pOwnerGuildName	NVARCHAR(12) OUTPUT	-- 家蜡辨靛捞抚 [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	SET		@pTerritory = -1
	SET		@pGuildAgitNo = -1
	SET		@pOwnerGuildName = ''
	SET 	@pOwnerGID = 0

	SELECT @pTerritory = mTerritory
		,@pGuildAgitNo = mGuildAgitNo
		,@pOwnerGID = mOwnerGID
		,@pOwnerGuildName = ISNULL((CASE mOwnerGID
						WHEN 0 THEN N'<NONE>'
						ELSE (SELECT mGuildNm FROM dbo.TblGuild WHERE mGuildNo = mOwnerGID)
						END), N'<FAIL>')
	FROM dbo.TblGuildAgitTicket 
	WHERE mTicketSerialNo = @pTicketSerialNo
	
	IF @@ROWCOUNT = 0 
	BEGIN
		RETURN(2)
	END

	RETURN(0)

GO

