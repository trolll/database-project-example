CREATE     PROCEDURE [dbo].[WspListStoreItemBySerial]
	@mSerialString VARCHAR(500)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		(	SELECT top 1 mUserId 
			FROM [FNLAccount].[dbo].[TblUser] 
			WHERE mUserNo = p.mUserNo) AS mUserId
		,p.mSerialNo
		,p.mRegDate
		, '' AS mEndDate -- 蜡瓤扁埃
		,ii.IName -- 酒捞袍 捞抚
		, p.mStatus -- 惑怕
		, p.mCnt -- 俺荐
		, p.mItemNo -- TID
		, p.mIsSeizure -- 拘幅咯何
		, p.mPracticalPeriod
		, p.mBindingType
	FROM 
		[dbo].[TblPcStore] AS p 
		INNER JOIN FNLParm.[dbo].[DT_Item] AS ii 
			ON p.mItemNo = ii.IID
		INNER JOIN dbo.fn_SplitTSQL (@mSerialstring,',') a
			ON p.mSerialNo = a.element
	ORDER BY p.mItemNo

GO

