CREATE PROCEDURE [dbo].[UspSetApplyAbnItem]
	 @pPcNo		INT					-- 家蜡磊
	,@pSerialNo		BIGINT			-- 格钎 酒捞袍狼 矫府倔锅龋
	,@pApplyAbnItemNo	INT			-- 眠啊加己 酒捞袍锅龋
	,@pApplyAbnValidMin	INT			-- 蜡瓤老.(窜困:盒)
AS
	SET NOCOUNT ON		
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF NOT EXISTS(	SELECT mSerialNo
					FROM dbo.TblPcInventory 
					WHERE mPcNo = @pPcNo 
							AND mSerialNo = @pSerialNo )
	BEGIN
		RETURN(2)	-- 秦寸 酒捞袍捞 粮犁窍瘤 臼澜
	END
	

	UPDATE dbo.TblPcInventory 
	SET 
		mApplyAbnItemNo = @pApplyAbnItemNo, 
		mApplyAbnItemEndDate = dbo.UfnGetEndDateByMin(GETDATE(), @pApplyAbnValidMin) 
	WHERE mPcNo = @pPcNo 
			AND mSerialNo = @pSerialNo
	IF @@ERROR <> 0
	BEGIN
		RETURN(1)		-- DB 郴何利牢 坷幅	
	END
		
	RETURN(0)

GO

