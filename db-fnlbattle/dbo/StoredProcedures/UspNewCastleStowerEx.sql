CREATE PROCEDURE [dbo].[UspNewCastleStowerEx]
	 @pIsTower		BIT
	,@pPlace		INT
	,@pGuildNo		INT
	,@pTaxBuy		INT
	,@pTaxBuyMax	INT
	,@pTaxHunt		INT
	,@pTaxHuntMax	INT
	,@pTaxGamble	INT
	,@pTaxGambleMax	INT		
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	INSERT TblCastleTowerStone([mIsTower], [mPlace], [mGuildNo], [mTaxBuy], [mTaxBuyMax], 
							   [mTaxHunt], [mTaxHuntMax], [mTaxGamble], [mTaxGambleMax], [mAsset])
						VALUES(@pIsTower, @pPlace, @pGuildNo, @pTaxBuy, @pTaxBuyMax, 
							   @pTaxHunt, @pTaxHuntMax, @pTaxGamble, @pTaxGambleMax, 0)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo	= 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

