/******************************************************************************  
**  Name: UspGetStatisticsPShopExchange  
**  Desc: 俺牢惑痢 抛捞喉俊 烹拌郴开阑 肺靛  
**  
**  Auth: 沥备柳  
**  Date: 09.05.07  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  辫碍龋      mItemStatus,  mTradeType 眠啊 
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetStatisticsPShopExchange]  
AS  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
 SELECT   
  mItemNo  
  , mBuyCount  
  , mSellCount  
  , mBuyTotalPrice  
  , mSellTotalPrice  
  , mBuyMinPrice  
  , mSellMinPrice  
  , mBuyMaxPrice  
  , mSellMaxPrice
  , mItemStatus
  , mTradeType
 FROM   
  dbo.TblStatisticsPShopExchange;

GO

