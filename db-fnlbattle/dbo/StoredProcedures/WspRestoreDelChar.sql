--------------------------------
-- 按眉疙 : [WspRestoreDelChar]
-- 汲疙 : 昏力等 某腐磐甫 汗备矫挪促
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.10.16
-- 荐沥磊 : 
-- 荐沥老 : 
-- 荐沥郴侩 : 
--------------------------------
-- ErrNo 1 : 秦寸 拌沥俊 后浇吩捞 绝促
-- ErrNo 2 : Update啊 窍唱狼 肺快俊 康氢阑 林瘤 臼疽促.(;;)
-- ErrNo 3 : 昏力等 某腐磐疙栏肺 积己等 某腐磐啊 粮犁茄促
-- ErrNo 4 : @mNo 俊 秦寸窍绰 昏力等 某腐磐啊 绝促
-- ErrNo 5 : 秦寸 某腐磐狼 浇吩捞 捞固 荤侩吝捞促.
-- ErrNo 0 : 沥惑利栏肺 诀单捞飘 登菌促
--------------------------------
CREATE         PROCEDURE [dbo].[WspRestoreDelChar]
	@mNo 		INT,		-- 昏力等 某腐磐 锅龋
	@mUserNo	INT		-- 蜡历锅龋
AS
	SET NOCOUNT ON
	DECLARE	@mCharNm	VARCHAR(20)
	DECLARE	@aErrNo		INT
	DECLARE	@aSlot		INT
	DECLARE	@aCnt		INT
		
	SET @aErrNo 	= 1
	
	IF (SELECT COUNT(*) FROM TblPc WITH (NOLOCK) WHERE mOwner = @mUserNo AND mDelDate IS NULL) < 3
	 BEGIN
		--昏力等 某腐磐牢瘤犬牢
		SET @aErrNo = 4
		IF EXISTS(SELECT * FROM TblPc WITH (NOLOCK) WHERE mNo = @mNo AND mDelDate IS NOT NULL)
 		 BEGIN
			--浇吩犬牢
			SELECT @aSlot = mSlot FROM TblPc WITH (NOLOCK) WHERE mNo = @mNo AND mDelDate IS NOT NULL
			SET @aErrNo = 5
			IF NOT EXISTS(SELECT * FROM TblPc WITH(NOLOCK) WHERE mOwner = @mUserNo AND mSlot = @aSlot AND mDelDate IS NULL)	
			 BEGIN
				--昏力登扁傈 某腐磐 疙 裙垫
				SELECT @mCharNm = mPcNm FROM TblPcDeleted WITH (NOLOCK) WHERE mPcNo = @mNo
				--昏力登扁傈 某腐磐疙捞 泅犁 霸烙辑滚俊 粮犁 窍瘤 臼绰瘤 犬牢
				IF NOT EXISTS(SELECT * FROM TblPc WITH (NOLOCK) WHERE mNm = @mCharNm)
			 	 BEGIN
				--昏力登扁傈 某腐磐 疙栏肺 Update
					BEGIN TRAN	
					UPDATE TblPc SET mNm = @mCharNm, mDeldate = NULL WHERE mNo = @mNo
					SET @aErrNo = 0
			
					IF @@ROWCOUNT <> 1 
					 BEGIN
						SET @aErrNo = 2
						GOTO LABEL_END
					 END

					DELETE TblPcDeleted WHERE mPcNo = @mNo
					IF @@ROWCOUNT <> 1 
					 BEGIN
						SET @aErrNo = 2
						GOTO LABEL_END
					 END
				 END
				ELSE
				 BEGIN
					SET @aErrNo = 3
					GOTO LABEL_END
				 END
			  END
			ELSE
			 BEGIN	--浇吩篮 咯蜡盒捞 乐瘤父 // 昏力 某腐磐狼 沥焊啊 荤侩窍带 浇吩篮 荤侩吝捞促. // 浇吩阑 函版秦具等促
				SET @aCnt = 0
				SET @aSlot = 0
				WHILE (@aCnt < 3)	-- 某腐磐 浇吩捞 弥措 3俺促
				 BEGIN
					IF NOT EXISTS(SELECT * FROM TblPc WITH (NOLOCK) WHERE mOwner = @mUserNo AND mSlot = @aCnt AND mDelDate IS NULL)
					 BEGIN
						SET @aSlot = @aCnt
					 END
					
					SET @aCnt = @aCnt + 1
				 END
				--昏力登扁傈 某腐磐 疙 裙垫
				SELECT @mCharNm = mPcNm FROM TblPcDeleted WITH (NOLOCK) WHERE mPcNo = @mNo
				--昏力登扁傈 某腐磐疙捞 泅犁 霸烙辑滚俊 粮犁 窍瘤 臼绰瘤 犬牢
				IF NOT EXISTS(SELECT * FROM TblPc WITH (NOLOCK) WHERE mNm = @mCharNm)
			 	 BEGIN
				--昏力登扁傈 某腐磐 疙栏肺 Update
					BEGIN TRAN	
					UPDATE TblPc SET mNm = @mCharNm, mDelDate = NULL, mSlot = @aSlot WHERE mNo = @mNo
					SET @aErrNo = 0
			
					IF @@ROWCOUNT <> 1 
					 BEGIN
						SET @aErrNo = 2
						GOTO LABEL_END
					 END

					DELETE TblPcDeleted WHERE mPcNo = @mNo
					IF @@ROWCOUNT <> 1 
					 BEGIN
						SET @aErrNo = 2
						GOTO LABEL_END
					 END
				 END
				ELSE
				 BEGIN
					SET @aErrNo = 3
					GOTO LABEL_END
				 END
					
			 END
		  END
	   END

LABEL_END:
	IF @aErrNo = 0 
 	 BEGIN
 		COMMIT TRAN
 	 END
	ELSE
 	 BEGIN
 		IF (@aErrNo <> 1) AND (@aErrNo <> 4) AND (@aErrNo <> 5)
		 BEGIN
		 	ROLLBACK TRAN
		 END
 	 END

	RETURN @aErrNo

GO

