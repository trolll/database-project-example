CREATE PROCEDURE [dbo].[UspExitDisciple]
	@pDisciple	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- 荤何啊 酒囱 版快父 力芭啊瓷
	IF NOT EXISTS(	SELECT mDisciple 
					FROM dbo.TblDiscipleMember 
					WHERE mDisciple = @pDisciple 
							AND mMaster <> @pDisciple )
	BEGIN
		RETURN(2) -- 粮犁窍瘤 臼绰 糕滚
	END
	
	DELETE dbo.TblDiscipleMember 
	WHERE mDisciple = @pDisciple
	IF @@ERROR > 0
	BEGIN
		RETURN(1)	-- dberror 
	END
	
	RETURN(0)

GO

