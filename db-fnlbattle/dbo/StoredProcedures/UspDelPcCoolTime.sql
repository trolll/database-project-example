/******************************************************************************
**		Name: UspDelPcCoolTime
**		Desc: 蜡历狼 巢篮 酿鸥烙捞 辆丰 登菌阑 版快 昏力茄促.
**
**		Auth: 沥备柳
**		Date: 09.09.30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	-----------------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE    PROCEDURE [dbo].[UspDelPcCoolTime]
	@pPcNo				INT,
	@pSID				INT,
	@pCoolTimeGroup		SMALLINT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DELETE	dbo.TblPcCoolTime
	WHERE	mPcNo = @pPcNo AND mSID = @pSID AND mCoolTimeGroup = @pCoolTimeGroup
	
	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN (0)

GO

