CREATE PROCEDURE [dbo].[UspCharTransfer]
	@pPcNo			INT,		-- 家蜡 某腐磐 锅龋 
	@pMoveUserNo	INT,		-- 捞悼 拌沥 锅龋
    @pSerialNo		BIGINT,		-- 酒捞袍 矫府倔 沥焊
    @pItemNo		INT			-- 酒捞袍 锅龋 	
AS
	SET NOCOUNT ON			

	DECLARE @aSlotNum TABLE(
		mSlot	TINYINT NOT NULL
	)
	DECLARE	@aErrNo		INT
			,@aRowCnt	INT
			,@aSlot		TINYINT
			
	SELECT  @aErrNo = 0, @aRowCnt = 0

	-----------------------------------------------
	-- 捞悼 拌沥狼 后 Slot 茫扁 
	-----------------------------------------------		
	INSERT INTO @aSlotNum VALUES(0)
	INSERT INTO @aSlotNum VALUES(1)
	INSERT INTO @aSlotNum VALUES(2)

	SELECT TOP 1 
		@aSlot = T1.mSlot
	FROM @aSlotNum T1 
		LEFT OUTER JOIN (
			SELECT mSlot 
			FROM dbo.TblPc
			WHERE mOwner = @pMoveUserNo
					AND mDelDate IS NULL
		) T2
		ON T1.mSlot = T2.mSlot		
	WHERE T2.mSlot IS NULL	
		
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR 	@aRowCnt = 0
	BEGIN
		RETURN (2)	-- 后 浇废捞 绝促. 
	END				
	
	IF @aSlot IS NULL
	BEGIN
		RETURN (2)	-- 后 浇废捞 绝促. 
	END 

	
	-----------------------------------------------
	-- 辨靛 付胶磐 咯何 
	-----------------------------------------------		
	IF EXISTS( SELECT *
					FROM dbo.TblGuildMember
					WHERE mPcNo = @pPcNo
							AND mGuildGrade = 0 )
	BEGIN
		RETURN(3) -- 辨靛 付胶磐 惑怕			
	END 
	
	-----------------------------------------------
	-- 拌沥 家蜡鼻 函版 
	-----------------------------------------------
	BEGIN TRAN
		
		DELETE dbo.TblPcInventory
		WHERE mPcNo = @pPcNo 
			AND	mSerialNo = @pSerialNo
			AND mItemNo = @pItemNo
				
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END		
					
		UPDATE dbo.TblPc
		SET
			mSlot = @aSlot,
			mOwner = @pMoveUserNo
		WHERE mNo = @pPcNo
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END		
		
		
	COMMIT TRAN	
	RETURN(0)

GO

