CREATE PROCEDURE [dbo].[UspAddRacingTicket]
	 @pSerialNo		BIGINT
	,@pPlace		INT
	,@pStage		INT
	,@pNID			INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	INSERT TblRacingTicket VALUES(@pSerialNo, @pPlace, @pStage, @pNID)
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo	= 1
	 END
	 
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

