/******************************************************************************  
**  File: 
**  Name: UspDelGuildSkillTreeNodeItem  
**  Desc: 辨靛 胶懦飘府畴靛酒捞袍 窍唱甫 昏力茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDelGuildSkillTreeNodeItem]
	@pGuildNo		INT,
	@pSTNIID	INT
AS  
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	IF 0 = @pSTNIID
		BEGIN
			DELETE FROM dbo.TblGuildSkillTreeInventory
			WHERE mGuildNo = @pGuildNo
			IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1		-- SQL Error
				END
		END
	ELSE
		BEGIN
			DELETE FROM dbo.TblGuildSkillTreeInventory
			WHERE mGuildNo = @pGuildNo AND mSTNIID = @pSTNIID
			IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1		-- SQL Error
				END
		END


	RETURN @aErrNo

GO

