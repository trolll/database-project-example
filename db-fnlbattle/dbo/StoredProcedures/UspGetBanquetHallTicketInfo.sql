CREATE PROCEDURE [dbo].[UspGetBanquetHallTicketInfo]
	 @pTicketSerialNo	BIGINT				-- 萍南矫府倔锅龋
	,@pTerritory		INT OUTPUT			-- 康瘤锅龋 [免仿]
	,@pBanquetHallType	INT OUTPUT			-- 楷雀厘鸥涝 [免仿]
	,@pBanquetHallNo	INT OUTPUT			-- 楷雀厘锅龋 [免仿]
	,@pOwnerPcNo		INT OUTPUT			-- 家蜡荤侩磊 [免仿]
	,@pOwnerPcName		NVARCHAR(12) OUTPUT	-- 家蜡荤侩磊捞抚 [涝仿/免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	SET		@pTerritory = -1
	SET		@pBanquetHallType = -1
	SET		@pBanquetHallNo = -1
	SET		@pOwnerPcName = ''

	SELECT @pTerritory = mTerritory
		,@pBanquetHallType = mBanquetHallType
		,@pBanquetHallNo = mBanquetHallNo
		,@pOwnerPcNo = mOwnerPcNo
		,@pOwnerPcName = ISNULL((CASE mOwnerPcNo
					WHEN 0 THEN N'<NONE>'
					ELSE (SELECT mNm FROM dbo.TblPc  WHERE mNo = mOwnerPcNo)
					END), N'<FAIL>')
	FROM dbo.TblBanquetHallTicket 
	WHERE mTicketSerialNo = @pTicketSerialNo

	IF @@ROWCOUNT = 0 
	BEGIN
		RETURN(2)
	END

	RETURN(0)

GO

