CREATE Procedure [dbo].[UspAddRacingResult]
	 @pPlace		INT
	,@pStage		INT
	,@pWinnerNID		SMALLINT
	,@pDividend		FLOAT
	,@pNo			INT OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	IF(0 <> @@TRANCOUNT) ROLLBACK
	BEGIN TRAN

	DECLARE	@aWinnerNID		INT

	-- P.S.> mWinnerNID 啊 0捞搁 搬苞绰 DRAW, 蝶扼辑 FNLParm.DT_Racing 俊辑 mNID绰 例措 0捞 乐绢辑绰 臼凳
	IF EXISTS(SELECT mWinnerNID FROM TblRacingResult WHERE mPlace = @pPlace AND mStage = @pStage)
	BEGIN
		-- 父距 铰磊啊 沥秦廉 乐促搁 诀单捞飘窍搁 臼凳
		SELECT @aWinnerNID = mWinnerNID FROM TblRacingResult WHERE mPlace = @pPlace AND mStage = @pStage
		IF (@aWinnerNID = 0)
		BEGIN
			UPDATE TblRacingResult
			SET mWinnerNID = @pWinnerNID, mDividend = @pDividend
			WHERE mPlace = @pPlace AND mStage = @pStage
		END
	END
	ELSE
	BEGIN
		INSERT TblRacingResult([mPlace], [mStage], [mWinnerNID], [mDividend])
		VALUES(@pPlace, @pStage, @pWinnerNID, @pDividend)
	END
	
	SET @pNo = @@IDENTITY
	IF(0 <> @@ERROR)
	 BEGIN
		SET	@aErrNo	= 1
		GOTO LABEL_END		 
	 END
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
			
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

