/******************************************************************************
**		Name: UspPushGuildRecruit
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩吝牢 辨靛 沥焊甫 火涝茄促.
**
**		Auth: 辫锐档, 沥柳龋
**		Date: 2009.07.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPushGuildRecruit]
	@pGuildNo			INT,
	@pValidDays			INT,
	@pIsPremium			BIT,
	@pIsMarkShow		BIT,
	@pGuildMsg			VARCHAR(250),
	@pRegDate			DATETIME	OUTPUT,
	@pEndDate			DATETIME	OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aErrNo		INT,
			@aRowCnt	INT
			
	SELECT	@aErrNo		= 0,			
			@pRegDate	= GETDATE(),
			@aRowCnt	= 0
	SELECT	@pEndDate	= DATEADD(day,@pValidDays,@pRegDate)
	

	----------------------------------------------
	-- 辨靛 府捻福泼 沥焊 诀单捞飘
	----------------------------------------------
	UPDATE dbo.TblGuildRecruit
	SET mIsMarkShow = @pIsMarkShow,
		mGuildMsg = @pGuildMsg
	WHERE [mGuildNo] = @pGuildNo

	SELECT  @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	IF(0 <> @aErrNo)
	BEGIN
		RETURN(1);
	END

	----------------------------------------------
	-- 辨靛 府捻福泼 沥焊 积己
	----------------------------------------------
	IF @aRowCnt = 0 
	BEGIN
		INSERT INTO dbo.TblGuildRecruit( mRegDate, mEndDate, mGuildNo, mIsPremium, mIsMarkShow, mGuildMsg)
		VALUES( @pRegDate, @pEndDate, @pGuildNo, @pIsPremium, @pIsMarkShow, @pGuildMsg)
		
		SELECT  @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
		IF (@aErrNo <> 0) OR (@aRowCnt <= 0)
		BEGIN
			RETURN(1);
		END	
	END 

	RETURN(0)

GO

