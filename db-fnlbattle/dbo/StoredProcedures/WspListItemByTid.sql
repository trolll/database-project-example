CREATE PROCEDURE [dbo].[WspListItemByTid]
	@mItemNo INT -- TID
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		mSerialNo
		, mNm
		, T1.mRegDate
		, T2.mUserId
		, NULL mGuildNm
	FROM (
		SELECT
			i.mSerialNo
			, p.mNm
			, i.mRegDate
			, p.mOwner
		FROM	
			dbo.TblPcInventory  i
			INNER JOIN dbo.TblPc p
				ON i.mPcNo = p.mNo
		WHERE 
			i.mItemNo = @mItemNo
			AND i.mPcNo > 1
			AND p.mDelDate IS NULL
		UNION ALL
		SELECT 
			mSerialNo,
			'Store' AS mNm,
			mRegDate,
			mUserNo	
		FROM 
			dbo.TblPcStore s
		WHERE 
			mItemNo = @mItemNo
	) T1
		INNER JOIN FNLAccount.dbo.TblUser T2
			ON T1.mOwner = T2.mUserNo	
	UNION ALL
	SELECT 
		mSerialNo
		, 'GuildStore' mNm
		, T1.mRegDate
		, NULL mUserId
		, T2.mGuildNm
	FROM dbo.TblGuildStore T1
		INNER JOIN dbo.TblGuild T2
			ON T1.mGuildNo = T2.mGuildNo
	WHERE mItemNo = @mItemNo

GO

