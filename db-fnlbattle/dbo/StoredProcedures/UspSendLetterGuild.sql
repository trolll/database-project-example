CREATE Procedure [dbo].[UspSendLetterGuild]
	 @pSerial		BIGINT		-- 祈瘤瘤 锅龋.
	,@pFromPcNm		CHAR(12)
	,@pToGuildNm	CHAR(12)
	,@pTitle		CHAR(30)
	,@pMsg			VARCHAR(2048)
	,@pErrNoStr		VARCHAR(50)	OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'

	DECLARE @aToGuildNo	INT
	SELECT @aToGuildNo=[mGuildNo] 
	FROM dbo.TblGuild WITH(NOLOCK)
	WHERE [mGuildNm]=@pToGuildNm
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoGuildNotExist'
		RETURN(@aErrNo)	 	 
	 END
	 
	INSERT dbo.TblLetter([mSerialNo], [mFromPcNm], [mTitle], [mMsg], [mToPcNm]) 
	VALUES(
		@pSerial, 
		@pFromPcNm, 
		@pTitle, 
		@pMsg, 
		@pToGuildNm)
	SET @aErrNo = @@ERROR
	IF(0 <> @aErrNo)
	BEGIN
		IF(2627 = @aErrNo)
		 BEGIN
			SET  @aErrNo	= 2
			SET	 @pErrNoStr	= 'eErrNoItemAlreadyUsed'		 
		 END
		ELSE
		 BEGIN
			SET  @aErrNo	= 3
			SET	 @pErrNoStr	= 'eErrNoSqlInternalError'		 
		 END
		GOTO LABEL_END
	END
	 
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

