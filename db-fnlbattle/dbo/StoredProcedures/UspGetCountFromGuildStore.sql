CREATE PROCEDURE [dbo].[UspGetCountFromGuildStore]
	  @pOwnerGuild	INT
	, @pGrade		TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		


	SELECT 
		ISNULL(COUNT(*),0)
	FROM dbo.TblGuildStore
	WHERE mGuildNo = @pOwnerGuild 
			AND mGrade = @pGrade
			AND mEndDate >  CONVERT(SMALLDATETIME, GETDATE())

GO

