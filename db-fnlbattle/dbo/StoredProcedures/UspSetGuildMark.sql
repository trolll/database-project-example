/******************************************************************************
**		Name: UspSetGuildMark
**		Desc: 辨靛 付农 盎脚
**
**              
**		Return values:
**			0 : 累诀 贸府 己傍
**			> 0 : SQL Error
**              
**		Auth: JUDY
**		Date: 2006-10-24
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2008.01.08	JUDY				辨靛 付农 诀单捞飘 朝楼 眠啊 		
**		2010.07.30	辫碍龋			辨靛付农 诀单捞飘
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetGuildMark]
	 @pGuildNo		INT
	,@pMarkSeq		INT 
	,@pGuildMark	BINARY(1784)	-- eSzGuildMark客 楷搬.
AS
	SET NOCOUNT ON	

	DECLARE @mGuildMarkUptDate SMALLDATETIME
	DECLARE @curDate	SMALLDATETIME
	DECLARE @dateDiff INT
	DECLARE @mGuildMark BINARY(1784)	-- eSzGuildMark客 楷搬.

	SELECT @mGuildMarkUptDate = mGuildMarkUptDate,
		@mGuildMark = mGuildMark
	FROM dbo.TblGuild  
	WHERE mGuildNo=@pGuildNo

	IF @mGuildMarkUptDate IS NULL
	BEGIN
	  RETURN(1) -- 辨靛 沥焊俊 巩力啊 乐促.
	END

	SET @curDate = GETDATE()
	SELECT @dateDiff = DATEDIFF(minute, @mGuildMarkUptDate, @curDate)	
		
	IF @dateDiff < 1 AND @mGuildMark IS NOT NULL
	BEGIN
		RETURN(2) -- 呈公 磊林 函版且 荐 绝嚼聪促.
	END

	UPDATE dbo.TblGuild 
	SET 
		mGuildSeqNo = @pMarkSeq,
		mGuildMark = @pGuildMark,
		mGuildMarkUptDate = @curDate
	WHERE mGuildNo = @pGuildNo

	IF @@ERROR <> 0 
	BEGIN
		RETURN(3) -- 诀单捞飘 角菩
	END
	
	RETURN(0)	-- NOT ERROR

GO

