/******************************************************************************
**		File: UspGetFlockingInfo.sql
**		Name: UspGetFlockingInfo
**		Desc: 笼窜青悼 沥焊 啊廉咳
**
**		Auth: 炔霖老
**		Date: 2006-10-30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE	[dbo].[UspGetFlockingInfo]
	@pMasterPcNo		INT,		-- 林牢捞 登绰 PC狼 锅龋
	@pFormationNo		INT OUTPUT,	-- 措屈锅龋 (0焊促 累栏搁 NPC笼窜捞 粮犁窍瘤 臼澜)
	@pOrderNo		INT OUTPUT,	-- 疙飞锅龋
	@pErrNoStr 		VARCHAR(50) OUTPUT		-- 俊矾巩磊凯 馆券
As
Begin
	SET NOCOUNT ON
	
	DECLARE @pRowCnt	INT,
			@pErr		INT,
			@pErr2		INT

	SELECT	@pRowCnt = 0, 
			@pErrNoStr	= 'eErrNoSqlInternalError',
			@pFormationNo	= -1,
			@pOrderNo = -1
	
	-- 林牢 PC狼 笼窜青悼 NPC 沥焊甫 啊廉咳
	SELECT @pFormationNo = mFormationNo, @pOrderNo = mOrderNo
	FROM dbo.TblFlocking WITH (NOLOCK) 
	WHERE mMasterPcNo = @pMasterPcNo	

	SELECT	@pRowCnt = @@ROWCOUNT, 
			@pErr  = @@ERROR

	IF @pRowCnt = 0
	BEGIN 
		SET	@pErrNoStr	= 'eErrNoUserNotExistId4'
		RETURN(1)		-- 荤侩磊 沥焊啊 绝促. 
	END 
	
	IF @pErr <> 0
	BEGIN 
		SET	@pErrNoStr	= 'eErrNoSqlInternalError'
		RETURN(@pErr)	-- SQL 郴何 俊矾 
	END 

	--  林牢 PC狼 笼窜青悼 NPC 府胶飘甫 啊廉咳
	SELECT @pRowCnt = COUNT(mSlaveNpcNo)
	FROM dbo.TblFlockingNpc WITH (NOLOCK)
	WHERE mMasterPcNo = @pMasterPcNo	

	SELECT	@pRowCnt = @@ROWCOUNT, 
			@pErr  = @@ERROR

	IF @pRowCnt = 0
	BEGIN 
		BEGIN TRANSACTION

		DELETE TblFlocking
		WHERE mMasterPcNo = @pMasterPcNo	

		SELECT	@pErr2  = @@ERROR

		IF(@pErr2 <> 0 )
		BEGIN
			ROLLBACK TRANSACTION
			SET	@pErrNoStr	= 'eErrNoSqlInternalError'
			RETURN(@pErr2)	-- SQL 郴何 俊矾 
		END
		ELSE	
		BEGIN
			COMMIT TRANSACTION
		END

		SELECT	@pFormationNo	= -1,
				@pOrderNo = -1

		SET	@pErrNoStr	= 'eErrNoUserNotExistId4'
		RETURN(1)		-- 荤侩磊 沥焊啊 绝促. 
	END 

	IF @pErr <> 0
	BEGIN 
		SET	@pErrNoStr	= 'eErrNoSqlInternalError'
		RETURN(@pErr)	-- SQL 郴何 俊矾 
	END 
		
	RETURN(0)			-- None Error
End

GO

