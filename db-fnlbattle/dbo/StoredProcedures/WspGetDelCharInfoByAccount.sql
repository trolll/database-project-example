CREATE PROCEDURE [dbo].[WspGetDelCharInfoByAccount]	
	@mUserId CHAR(20) -- 拌沥疙
AS
	SET NOCOUNT ON
	
	SELECT
		d.mPcNm, -- 某腐磐疙
		p.mNm, -- 某腐磐疙
		u.mUserId, -- 拌沥疙
		p.mRegDate, -- 某腐磐 积己老
		p.mDelDate -- 某腐磐 昏力老
		,p.mNo
	FROM
		[dbo].[TblPc] AS p WITH (NOLOCK)		
	INNER JOIN
		[FNLAccount].[dbo].[TblUser] AS u 		
	On 
		p.mOwner = u.mUserNo
	INNER JOIN
		[dbo].[TblPcDeleted] AS d WITH (NOLOCK)
	ON
		d.mPcNo = p.mNo
	WHERE
		u.mUserId = @mUserId
		AND p.mDelDate IS NOT NULL

GO

