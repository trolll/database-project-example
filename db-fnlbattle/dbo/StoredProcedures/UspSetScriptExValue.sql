CREATE PROCEDURE [dbo].[UspSetScriptExValue]
	@pNo		INT,
	@pVal		INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	IF EXISTS(SELECT * FROM TblScriptExValue WHERE mNo = @pNo)
	BEGIN
		UPDATE TblScriptExValue SET mVal = @pVal WHERE mNo = @pNo
	END
	ELSE
	BEGIN
		INSERT TblScriptExValue VALUES(@pNo, @pVal)
	END

	SET NOCOUNT OFF

GO

