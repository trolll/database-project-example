CREATE PROCEDURE [dbo].[UspResetGuildMaster] 
	@pGuildNo	INT,
	@pPcNo		INT,	-- 函版登绰 Master PcNo
	@pSerialNo	INT,
	@pItemNo	INT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			
	
	DECLARE	 @aRowCnt		INT
			,@aErrNo		INT
			,@aPrevGuildMasterNo	INT
			
	SELECT  @aPrevGuildMasterNo = 0, @aRowCnt = 0,	@aErrNo = 0	

	SELECT 
		@aPrevGuildMasterNo = mPcNo
	FROM dbo.TblGuildMember
	WHERE mGuildNo = @pGuildNo
		AND mGuildGrade = 0	
		
	SELECT  @aRowCnt = @@ROWCOUNT,	@aErrNo = @@ERROR
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(2)	-- 辨靛 付胶磐 沥焊啊 绝芭唱, 捞傈 付胶磐 沥焊甫 掘阑 荐啊 绝嚼聪促. 		
	END	
		
	IF NOT EXISTS(	SELECT mSerialNo
					FROM dbo.TblPcInventory
					WHERE mPcNo = @aPrevGuildMasterNo
						AND mSerialNo = @pSerialNo 
						AND mItemNo = @pItemNo )
	BEGIN
		RETURN(3)	-- 何啊辑厚胶 酒捞袍 粮犁咯何 眉农 
	END						
		
	BEGIN TRAN
	
		-- prev master
		UPDATE dbo.TblGuildMember
		SET	mGuildGrade = 4		-- 辨靛盔栏肺 窍氢
		WHERE mGuildNo = @pGuildNo
			AND mPcNo = @aPrevGuildMasterNo
			AND mGuildGrade = 0
			
		SELECT  @aRowCnt = @@ROWCOUNT,	@aErrNo = @@ERROR
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END						
		
	
		UPDATE dbo.TblGuildMember
		SET	mGuildGrade = 0	
		WHERE mGuildNo = @pGuildNo
			AND mPcNo = @pPcNo
						
		SELECT  @aRowCnt = @@ROWCOUNT,	@aErrNo = @@ERROR
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END	
		
		DELETE dbo.TblPcInventory
		WHERE mPcNo = @aPrevGuildMasterNo
			AND mSerialNo = @pSerialNo 
			AND mItemNo = @pItemNo		

		SELECT  @aRowCnt = @@ROWCOUNT,	@aErrNo = @@ERROR
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END	

						
	COMMIT TRAN
	RETURN(0)

GO

