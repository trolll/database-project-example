--------------------------------
-- 按眉疙 : WspGetGuildNo
-- 汲疙 : 辨靛狼 侥喊 锅龋甫 啊廉柯促
-- 累己磊 : 辫堡挤
-- 累己老 : 2006.07.11
-- 荐沥磊 : 
-- 荐沥老 : 
--------------------------------

CREATE    PROCEDURE [dbo].[WspGetGuildNo] 
	@mGuildNm	CHAR(12)	-- 辨靛捞抚
AS
	SET NOCOUNT ON

	SELECT
		mGuildNo
	FROM
		TblGuild
	WITH ( NOLOCK )
	WHERE
		mGuildNm = @mGuildNm

	SET NOCOUNT OFF

GO

