CREATE PROCEDURE [dbo].[UspCreateDisciple]
	@pMaster	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT,
			@aErrNo INT,
			@aRowCnt INT
	SELECT @aRv = 0, @aErrNo = 0, @aRowCnt = 0

	IF NOT EXISTS(	SELECT mNo 
					FROM dbo.TblPc 
					WHERE mNo = @pMaster)
	BEGIN
		RETURN(2) -- 粮犁窍瘤 臼绰 荤何 PC沥焊	
	END 
	
	IF EXISTS(	SELECT mMaster 
				FROM dbo.TblDisciple 
				WHERE mMaster = @pMaster)
	BEGIN
		RETURN(3)	-- 捞固 粮犁窍绰 葛烙
	END

	IF EXISTS(	SELECT mDisciple 
				FROM dbo.TblDiscipleMember 
				WHERE mDisciple = @pMaster )
	BEGIN
		RETURN(4)		-- 捞固 葛烙狼 糕滚肺 粮犁
	END


	BEGIN TRAN

		INSERT INTO dbo.TblDisciple (mMaster) 
		VALUES(@pMaster)	-- 葛烙沥焊 眠啊
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR

		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)			
		END

		INSERT INTO dbo.TblDiscipleMember (mMaster, mDisciple, mType) 
		VALUES(@pMaster, @pMaster, 1)		-- 付胶磐甫 糕滚肺 殿废
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR

		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)			
		END

	COMMIT TRAN
	RETURN(0)

GO

