CREATE PROCEDURE [dbo].[UspSetDialogScr]
	 @pMId			INT
	,@pScriptText	VARCHAR(8000)	-- eSzDialogScriptText 客 楷悼
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0	
	
	-- 胶农赋飘甫 持绰促
	IF EXISTS(SELECT * FROM dbo.TblDialogScript WHERE [mMId]=@pMId)
	 BEGIN
		UPDATE dbo.TblDialogScript SET [mScriptText] = @pScriptText
			WHERE [mMId] = @pMId
	 END
	ELSE
	 BEGIN
		INSERT dbo.TblDialogScript([mMId], [mScriptText])
			VALUES(@pMId, @pScriptText)	 
	 END
	 	 
	IF(@@ROWCOUNT <> 1)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END	 
	 END
		
LABEL_END:		
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

