/******************************************************************************
**		Name: WspUpdCharRankingPoint
**		Desc: 某腐磐 珐欧 器牢飘甫 荐沥茄促
**
**		Auth: 辫籍玫
**		Date: 20090429
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspUpdCharRankingPoint]
	@mFieldSvrNo		INT,	-- 鞘靛 辑滚 锅龋
	@mFieldSvrPcNo		INT,	-- 鞘靛 辑滚 PC锅龋
	@mContribution		INT = -1,	-- 扁咯档 器牢飘
	@mGuardian			INT = -1,	-- 荐龋脚 器牢飘
	@mTeleportTower		INT = -1,	-- 捞悼器呕 器牢飘
	@mPVP				INT = -1		-- PVP 器牢飘
AS
	SET NOCOUNT ON			-- 汲疙 : 搬苞 饭内靛 悸阑 馆券 救 矫挪促.
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	--- 函荐 急攫
	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aPcNo	INT

	--- 函荐 檬扁拳 
	SELECT @aErrNo = 0, @aRowCnt = 0, @aPcNo = 0   -- 茄临俊 檬扁拳 
	
	SELECT @aPcNo = mNo
	FROM dbo.TblChaosBattlePc
	WHERE mFieldSvrNo = @mFieldSvrNo
		AND mFieldSvrPcNo = @mFieldSvrPcNo

	IF @aPcNo = 0
	BEGIN
		RETURN (-1)				-- 某腐磐啊 粮犁窍瘤 臼绰促.
	END

	IF @mContribution <> -1
	BEGIN
		UPDATE dbo.TblPcRankingPoint
		SET mContribution = @mContribution
		WHERE mPcNo = @aPcNo
	END

	IF @mGuardian <> -1
	BEGIN
		UPDATE dbo.TblPcRankingPoint
		SET mGuardian = @mGuardian
		WHERE mPcNo = @aPcNo
	END

	IF @mTeleportTower <> -1
	BEGIN
		UPDATE dbo.TblPcRankingPoint
		SET mTeleportTower = @mTeleportTower
		WHERE mPcNo = @aPcNo
	END

	IF @mPvP <> -1
	BEGIN
		UPDATE dbo.TblPcRankingPoint
		SET mPvP = @mPvP
		WHERE mPcNo = @aPcNo
	END

	RETURN (0)

GO

