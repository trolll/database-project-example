/******************************************************************************
**		Name: UspPopPcQuestConditionAll
**		Desc: 涅胶飘 炼扒 昏力 (葛电 纳腐磐)
**				酒魔 6矫 概老 檬扁拳 捞酱
**
**		Auth: 沥柳龋
**		Date: 2010-02-04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPopPcQuestConditionAll]
	@pQuestNoStr			VARCHAR(3000)
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;

	DECLARE @aQuest			TABLE (
				mQuestNo		INT	 PRIMARY KEY      	)			


	INSERT INTO @aQuest(mQuestNo)
	SELECT CONVERT(INT, element) mObjID
	FROM  dbo.fn_SplitTSQL(  @pQuestNoStr, ',' )
	WHERE LEN(RTRIM(element)) > 0


	BEGIN TRAN

		DELETE T2
		FROM dbo.TblPcQuest T1
			INNER JOIN dbo.TblPcQuestCondition T2
				ON T1.mQuestNo = T2.mQuestNo
					AND T1.mPcNo = T2.mPcNo
		WHERE T1.mQuestNo IN ( 
				SELECT *
				FROM @aQuest
			) AND T1.mValue = 99


		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		DELETE 	dbo.TblPcQuest
		WHERE mQuestNo IN ( 
			SELECT *
			FROM @aQuest
		) AND mValue = 99


		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(2);			
		END 

	COMMIT TRAN

	RETURN(0)

GO

