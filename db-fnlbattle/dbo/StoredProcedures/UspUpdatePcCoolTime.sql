/******************************************************************************
**		Name: UspUpdatePcCoolTime
**		Desc: 巢篮 酿鸥烙 矫埃阑 盎脚茄促.
**
**		Auth: 沥备柳
**		Date: 09.09.30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	-----------------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE    PROCEDURE [dbo].[UspUpdatePcCoolTime]
	@pPcNo				INT,
	@pCoolTimeGroup		SMALLINT,
	@pRemainTime		INT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblPcCoolTime
	SET mRemainTime = @pRemainTime
	WHERE mPcNo = @pPcNo AND mCoolTimeGroup = @pCoolTimeGroup

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

