CREATE PROCEDURE [dbo].[WspPushItem]
	@mNo INT, -- 荤侩磊 锅龋
	@mItemNo INT, -- 酒捞袍 锅龋 (TID)
	@mStatus TINYINT, -- 酒捞袍 惑怕
	@mCnt INT, -- 酒捞袍 荐樊
	@mIsConfirm BIT, -- 犬牢咯何
	@pValidDay INT = NULL, -- 蜡瓤扁埃
	@pPraticalHour INT = NULL,
	@pBindingType	TINYINT = 0		-- BIDING TYPE OFF
AS
	SET NOCOUNT ON

	DECLARE @pCntUse TINYINT
	DECLARE @pIsStack BIT,
			@pIsPracticalPeriod BIT,
			@pIsCharge INT,
			@pErr INT

	IF @pValidDay IS NULL
		SELECT
			@pValidDay = ITermOfValidity
			, @pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
			, @pIsCharge = IIsCharge
		FROM FNLParm.[dbo].[DT_Item]
		WHERE IID = @mItemNo
	ELSE
		SELECT
			@pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
			, @pIsCharge = IIsCharge
		FROM FNLParm.[dbo].[DT_Item]
		WHERE IID = @mItemNo
	

	-- 绝绰 TID
	IF @pValidDay IS NULL OR @pCntUse IS NULL OR @pIsStack IS NULL
		RETURN 2
		
	IF @pIsPracticalPeriod = 1 AND ( @pPraticalHour IS NULL OR @pPraticalHour <= 0)
		RETURN 3
	
	IF @pPraticalHour > (30*24) 
		RETURN 4
		
	IF @pPraticalHour IS NULL
		SET @pPraticalHour = 0
		
	-- UspPushItem阑 荤侩窍咯 积己茄促
	EXECUTE dbo.UspPushItem @mNo, 0, @mItemNo, @pValidDay, @mCnt, @pCntUse, @mIsConfirm, @mStatus, @pIsStack, @pIsCharge, @pPraticalHour, @pBindingType

	RETURN 0

GO

