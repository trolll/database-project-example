CREATE PROCEDURE [dbo].[WspGetPcGoldItemEffect]  	
	@pPcNo	INT
AS 
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		T1.mRegDate
		, T1.mItemType
		, T3.mCodeDesc AS mItemTypeNm
		, T1.mItemNo
		, T2.IName AS mItemNm
		, T1.mParmA
		, T1.mEndDate
	FROM dbo.TblPcGoldItemEffect T1
		INNER JOIN [FNLParm].dbo.DT_ITEM T2
			ON T1.mItemNo = T2.IID	
		LEFT OUTER JOIN [FNLParm].dbo.TblCode T3
			On T1.mItemType = T3.mCode
				AND T3.mCodeType = 2	-- Pc GoldItem Effect Type
	WHERE mPcNo = @pPcNo

GO

