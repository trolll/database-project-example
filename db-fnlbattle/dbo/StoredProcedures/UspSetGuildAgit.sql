CREATE Procedure [dbo].[UspSetGuildAgit]			-- 漂沥 辨靛酒瘤飘狼 家蜡鼻 捞傈累诀
	 @pTerritory		INT			-- 康瘤锅龋
	,@pGuildAgitNo		INT			-- 辨靛酒瘤飘锅龋
	,@pLeftMin1		INT			-- 父丰盒 (己傍)
	,@pLeftMin2		INT			-- 父丰盒 (角菩)
	,@pInitSellingMoney	BIGINT			-- 檬扁陛 [府悸]
	,@pOwnerGID		INT OUTPUT		-- 家蜡辨靛 [免仿]
	,@pBuyingMoney	BIGINT	OUTPUT	-- 备涝陛咀 (泅犁) [免仿]
	,@pSellingMoney	BIGINT	OUTPUT	-- 概阿陛咀 (捞傈) [免仿]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
	BEGIN
		-- 牢胶畔胶啊 粮犁窃

		DECLARE @aOwnerGID 	INT
		DECLARE @aIsSelling		CHAR
		DECLARE @aSellingMoney 	BIGINT
		DECLARE @aBuyingMoney 	BIGINT
		DECLARE @aLeftMin		INT

		-- 泅犁 家蜡辨靛 沥焊 馆券
		SELECT @aOwnerGID = mOwnerGID, @aIsSelling = mIsSelling, @aSellingMoney = mSellingMoney, @aBuyingMoney = mBuyingMoney, @aLeftMin = mLeftMin FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

		IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction  WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
		BEGIN
			-- 涝蔓沥焊 乐澜

			DECLARE @aCurBidGID	INT
			DECLARE @aCurBidMoney	BIGINT

			-- 泅犁 秦寸 辨靛酒瘤飘 涝蔓沥焊 馆券
			SELECT @aCurBidGID = mCurBidGID, @aCurBidMoney = mCurBidMoney FROM TblGuildAgitAuction  WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

			-- 倡蔓瞪 辨靛啊 泅犁 辨靛酒瘤飘甫 焊蜡窍绊 乐绰 版快绰 涝蔓阂啊 -> 蜡蔓登档废 涝蔓辨靛 沥焊荐沥
			IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mOwnerGID = @aCurBidGID)
			BEGIN
				SET @aCurBidGID = 0
				SET @aCurBidMoney = 0
			END

			IF (@aCurBidGID <> 0 AND @aCurBidMoney > @aSellingMoney)
			BEGIN
				-- 涝蔓沥焊 乐澜

				BEGIN TRAN

				-- 辨靛酒瘤飘 沥焊 技泼
				UPDATE TblGuildAgit SET mOwnerGID = @aCurBidGID, mGuildAgitName = '', mRegDate = getdate(), mLeftMin = @pLeftMin1, mIsSelling = 0, mSellingMoney = @pInitSellingMoney, mBuyingMoney = @aCurBidMoney
				WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END
	
				-- 泅犁 家蜡辨靛 (粮犁且版快) 俊霸绰 概阿措陛 瘤阂
				IF (@aOwnerGID <> 0)
				BEGIN
					-- 概阿措陛俊辑 技陛 力寇 (泅犁 10%)				
					SET @aCurBidMoney = (@aCurBidMoney * (100 - 10))/100
	
					UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney + @aCurBidMoney WHERE mGID = @aOwnerGID
					IF(0 <> @@ERROR)
					BEGIN
						SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
						GOTO LABEL_END			 
					END
				END

				-- 涝蔓沥焊 府悸
				UPDATE TblGuildAgitAuction SET mCurBidGID = 0, mCurBidMoney = 0, mRegDate = getdate() WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END

				-- 免仿沥焊 技泼
				SELECT @pOwnerGID = mOwnerGID, @pBuyingMoney = mBuyingMoney, @pSellingMoney = ISNULL(@aSellingMoney, 0) FROM TblGuildAgit WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			END
			ELSE
			BEGIN
				-- 涝蔓沥焊 绝澜
	
				BEGIN TRAN
	
				-- 辨靛酒瘤飘 沥焊 府悸
				UPDATE TblGuildAgit SET mOwnerGID = 0, mGuildAgitName = '', mRegDate = getdate(), mLeftMin = @pLeftMin2, mIsSelling = 0, mSellingMoney = @pInitSellingMoney, mBuyingMoney = 0
				WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END
	
				-- 泅犁 家蜡辨靛俊霸绰 备概陛咀狼 老何肺 焊惑 (泅犁 50%)
				DECLARE @aRepayMoney BIGINT
				SET @aRepayMoney = (@aBuyingMoney * (100 - 50))/100
				IF (@aOwnerGID <> 0)
				BEGIN
					UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney + @aRepayMoney WHERE mGID = @aOwnerGID
					IF(0 <> @@ERROR)
					BEGIN
						SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
						GOTO LABEL_END			 
					END
				END

				-- 涝蔓沥焊 府悸
				UPDATE TblGuildAgitAuction SET mCurBidGID = 0, mCurBidMoney = 0, mRegDate = getdate() WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END

				-- 免仿沥焊 技泼
				SELECT @pOwnerGID = mOwnerGID, @pBuyingMoney = mBuyingMoney, @pSellingMoney = ISNULL(@aSellingMoney, 0) FROM TblGuildAgit WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			END
		END
		ELSE
		BEGIN
			-- 涝蔓沥焊 绝澜

			BEGIN TRAN

			-- 辨靛酒瘤飘 沥焊 府悸
			UPDATE TblGuildAgit SET mOwnerGID = 0, mGuildAgitName = '', mRegDate = getdate(), mLeftMin = @pLeftMin2, mIsSelling = 0, mSellingMoney = @pInitSellingMoney, mBuyingMoney = 0
			WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END

			-- 泅犁 家蜡辨靛俊霸绰 备概陛咀狼 老何肺 焊惑 (泅犁 50%)
--			DECLARE @aRepayMoney BIGINT
			SET @aRepayMoney = (@aBuyingMoney * (100 - 50))/100
			IF (@aOwnerGID <> 0)
			BEGIN
				UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney + @aRepayMoney WHERE mGID = @aOwnerGID
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END
			END

			-- 涝蔓沥焊 府悸
			INSERT TblGuildAgitAuction (mTerritory, mGuildAgitNo) VALUES(@pTerritory, @pGuildAgitNo)

			-- 免仿沥焊 技泼
			SELECT @pOwnerGID = mOwnerGID, @pBuyingMoney = mBuyingMoney, @pSellingMoney = ISNULL(@aSellingMoney, 0) FROM TblGuildAgit WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
		END
	END
	ELSE
	BEGIN
		-- 牢胶畔胶啊 粮犁窍瘤 臼澜

		IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction  WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
		BEGIN
			-- 涝蔓沥焊 乐澜

--			DECLARE @aCurBidGID	INT
--			DECLARE @aCurBidMoney	BIGINT
			SET @aSellingMoney = @pInitSellingMoney	-- 辨靛酒瘤飘 牢胶畔胶啊 绝歹扼档 公炼扒 檬扁涝蔓陛捞 魄概陛咀捞 登绢具 窃

			-- 泅犁 秦寸 辨靛酒瘤飘 涝蔓沥焊 馆券
			SELECT @aCurBidGID = mCurBidGID, @aCurBidMoney = mCurBidMoney FROM TblGuildAgitAuction  WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

			-- 倡蔓瞪 辨靛啊 泅犁 辨靛酒瘤飘甫 焊蜡窍绊 乐绰 版快绰 涝蔓阂啊 -> 蜡蔓登档废 涝蔓辨靛 沥焊荐沥
			IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mOwnerGID = @aCurBidGID)
			BEGIN
				SET @aCurBidGID = 0
				SET @aCurBidMoney = 0
			END

			IF (@aCurBidGID <> 0 AND @aCurBidMoney > @aSellingMoney)
			BEGIN
				-- 涝蔓沥焊 乐澜

				BEGIN TRAN

				-- 辨靛酒瘤飘 沥焊 涝仿
				INSERT TblGuildAgit (mTerritory, mGuildAgitNo, mOwnerGID, mLeftMin, mSellingMoney, mBuyingMoney)
				VALUES(@pTerritory, @pGuildAgitNo, @aCurBidGID, @pLeftMin1, @pInitSellingMoney, @aCurBidMoney)
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END
	
				-- 涝蔓沥焊 府悸
				UPDATE TblGuildAgitAuction SET mCurBidGID = 0, mCurBidMoney = 0, mRegDate = getdate() WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END

				-- 免仿沥焊 技泼
				SELECT @pOwnerGID = mOwnerGID, @pBuyingMoney = mBuyingMoney, @pSellingMoney = ISNULL(@aSellingMoney, 0) FROM TblGuildAgit WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			END
			ELSE
			BEGIN
				-- 涝蔓沥焊 绝澜
	
				BEGIN TRAN
	
				-- 辨靛酒瘤飘 沥焊 府悸
				INSERT TblGuildAgit (mTerritory, mGuildAgitNo, mSellingMoney, mLeftMin)
				VALUES(@pTerritory, @pGuildAgitNo, @pInitSellingMoney, @pLeftMin2)
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END

				-- 涝蔓沥焊 府悸
				UPDATE TblGuildAgitAuction SET mCurBidGID = 0, mCurBidMoney = 0, mRegDate = getdate() WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
				IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
					GOTO LABEL_END			 
				END

				-- 免仿沥焊 技泼
				SELECT @pOwnerGID = mOwnerGID, @pBuyingMoney = mBuyingMoney, @pSellingMoney = ISNULL(@aSellingMoney, 0) FROM TblGuildAgit WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			END
		END
		ELSE
		BEGIN
			-- 涝蔓沥焊 绝澜

			BEGIN TRAN

			-- 辨靛酒瘤飘 沥焊 府悸
			INSERT TblGuildAgit (mTerritory, mGuildAgitNo, mSellingMoney, mLeftMin)
			VALUES(@pTerritory, @pGuildAgitNo, @pInitSellingMoney, @pLeftMin2)
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB 郴何利牢 坷幅
				GOTO LABEL_END			 
			END

			-- 涝蔓沥焊 府悸
			INSERT TblGuildAgitAuction (mTerritory, mGuildAgitNo) VALUES(@pTerritory, @pGuildAgitNo)

			-- 免仿沥焊 技泼
			SELECT @pOwnerGID = mOwnerGID, @pBuyingMoney = mBuyingMoney, @pSellingMoney = ISNULL(@aSellingMoney, 0) FROM TblGuildAgit WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
		END
	END

	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

