CREATE  PROCEDURE [dbo].[UspUpdateItemNo]
	 @pPcNo			INT
	,@pSerial		BIGINT
	,@pItemNo		INT 
	,@pNewItemNo	INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	
	DECLARE	@aErrNo		INT,
				@aRowCnt	INT,
				@aItemNo	INT 
	
	SELECT	@aErrNo = 0, @aRowCnt = 0, @aItemNo = 0

	SELECT 
		@aItemNo = mItemNo 
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pSerial
			AND mPcNo =@pPcNo

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END	 
	END

	IF @aItemNo <> @pItemNo
	BEGIN
		UPDATE dbo.TblPcInventory 
		SET 
			mItemNo = @pItemNo
		WHERE  mPcNo = @pPcNo
					AND mSerialNo = @pSerial

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET  @aErrNo = 2
			GOTO T_END	 
		END	
		SET @pNewItemNo = @pItemNo
	END
	ELSE
	BEGIN
		SET @pNewItemNo = @aItemNo
	END

T_END:			
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

