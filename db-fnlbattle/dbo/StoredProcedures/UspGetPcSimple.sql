CREATE PROCEDURE [dbo].[UspGetPcSimple]
	 @pPcNo		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT a.[mSlot], RTRIM(a.[mNm]) AS mNm, a.[mClass], a.[mSex], a.[mHead], a.[mFace], a.[mBody], 
		   b.[mLevel], b.[mHpAdd], b.[mHp], b.[mMpAdd], b.[mMp], b.[mExp], b.[mStomach],
		   c.[mGuildNo], RTRIM(c.[mNickNm]), c.[mGuildGrade], b.[mChaotic], b.[mPosX], b.[mPosY], b.[mPosZ], 
		   ISNULL(d.[mMaster], 0) As mDiscipleNo, ISNULL(d.[mType], 0) As mDiscipleType
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
			ON(a.[mNo] = b.[mNo])
		LEFT JOIN dbo.TblGuildMember AS c 
			ON(a.[mNo] = c.[mPcNo])
		LEFT JOIN dbo.TblDiscipleMember AS d
			ON(a.[mNo] = d.[mDisciple])
	WHERE (a.[mNo]=@pPcNo) 
			AND ([mDelDate] IS NULL)
	ORDER BY a.[mSlot]

	SET NOCOUNT OFF

GO

