CREATE PROCEDURE [dbo].[UspSetItemCnt]
	 @pItemNo				INT
	,@pCntNewMerchant		BIGINT	-- 惑牢捞 魄 俺荐.
	,@pCntDelMerchant		BIGINT	-- 惑牢捞 魂 俺荐.
	,@pCntNewReinforce		BIGINT	-- 碍拳肺 积己等 俺荐.
	,@pCntDelReinforce		BIGINT	-- 碍拳肺 家葛等 俺荐.
	,@pCntNewCrafting		BIGINT	-- crafting栏肺 积己等 俺荐.
	,@pCntDelCrafting		BIGINT	-- crafting栏肺 家葛等 俺荐.	
	,@pCntNewNonPc			BIGINT	-- NON-PC啊 drop茄 俺荐.
	,@pCntDelNonPc			BIGINT	-- NON-PC啊 家拳矫挪 俺荐.
	,@pCntUsePc				BIGINT	-- PC啊 荤侩茄 俺荐.
	,@pCntUseNonPc			BIGINT	-- NON-PC啊 荤侩茄 俺荐.
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
		
	DECLARE	@aDate	SMALLDATETIME
	SET @aDate = CONVERT(VARCHAR(30), GETDATE(), 102)
	SET @aDate = DATEADD(dd, -1, @aDate)
	
	--酒捞袍喊肺 扁废阑 秦具 窍骨肺 龋免磊啊 rollback阑 措厚秦辑 寇何俊辑 tran阑 矫累且 荐 乐促.
	--IF(0 <> @@TRANCOUNT) ROLLBACK
	SET XACT_ABORT ON	
	BEGIN TRAN
	
	-- 贸澜 档涝等 酒捞袍狼 弥檬 家蜡磊啊 logout窍瘤 臼芭唱 芒绊俊 历厘窍瘤 臼阑 荐 乐促.
	IF NOT EXISTS(SELECT * FROM TblStatisticsItem WHERE (@aDate = [mRegDate]) AND (@pItemNo = [mItemNo]))
	 BEGIN
		INSERT INTO TblStatisticsItem([mRegDate], [mItemNo]) VALUES(@aDate, @pItemNo)
		IF(0 <> @@ERROR)
		 BEGIN
			SET @aErrNo = 1
			GOTO LABEL_END		 
		 END
	 END
	
	UPDATE TblStatisticsItem SET [mNewMerchantCnt]=@pCntNewMerchant,
								 [mDelMerchantCnt]=@pCntDelMerchant,
								 [mNewReinforceCnt]=@pCntNewReinforce,
								 [mDelReinforceCnt]=@pCntDelReinforce,
								 [mNewCraftingCnt]=@pCntNewCrafting,
								 [mDelCraftingCnt]=@pCntDelCrafting,
								 [mNewNonPcCnt]=@pCntNewNonPc,
								 [mDelNonPcCnt]=@pCntDelNonPc,
								 [mUsePcCnt]=@pCntUsePc,
								 [mUseNonPcCnt]=@pCntUseNonPc
		WHERE (@aDate = [mRegDate]) AND (@pItemNo = [mItemNo])
	IF(0 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo = 2
		GOTO LABEL_END
	 END		
													 
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
	SET NOCOUNT OFF

GO

