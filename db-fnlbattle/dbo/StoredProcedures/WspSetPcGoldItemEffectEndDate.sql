CREATE PROCEDURE [dbo].[WspSetPcGoldItemEffectEndDate]  	
	@pPcNo	INT
	, @pItemType INT
	, @pEndDate SMALLDATETIME
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	
	UPDATE dbo.TblPcGoldItemEffect
	SET
		mEndDate = @pEndDate
	WHERE mPcNo = @pPcNo
			AND  mItemType = @pItemType
			
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo <> 0 OR @aRowCnt <=0
		RETURN(1)	-- db error

    RETURN(0)

GO

