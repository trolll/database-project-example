CREATE PROCEDURE [dbo].[WspUptItemRecovery]  	
	@pMI	INT			-- 楷厘 矫埃(盒)
	, @pSvrNo	SMALLINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
		
	EXEC @aErrNo = dbo.UspUptItemDate @pMI
	
	IF @aErrNo <> 0
		RETURN(@aErrNo)
		
	-- 辑滚 汗备 矫埃阑 扁废 茄促.	
	EXEC [FNLParm].dbo.UspSetParmSvrLastSupportDate @pSvrNo

    RETURN(0)

GO

