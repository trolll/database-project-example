--------------------------------
-- 按眉疙 : WspUpdateCharChaotic
-- 汲疙 : 某腐磐 己氢阑 函版茄促
-- 累己磊 : 捞快籍
-- 累己老 : 2006.04.09
-- 荐沥磊 : 辫己犁
-- 荐沥老 : 2006.06.06
-- 荐沥郴侩 : 飘罚黎记 荤侩
--------------------------------
CREATE    PROCEDURE [dbo].[WspUpdateCharChaotic]	
	@mChaotic INT, -- 己氢
	@mNm CHAR(12) -- 某腐磐疙
AS
	SET NOCOUNT ON

	BEGIN TRANSACTION
	
	UPDATE
		[dbo].[TblPcState]
	SET
		mChaotic = @mChaotic
	WHERE		
		mNo = (SELECT mNo From [dbo].[TblPc] WHERE mNm = @mNm)			

	IF @@ERROR = 0
	BEGIN
		COMMIT TRANSACTION
		RETURN 0
	END
	ELSE
	BEGIN
		ROLLBACK TRANSACTION
		RETURN @@ERROR
	END

GO

