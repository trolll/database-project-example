/******************************************************************************  
**  File: 
**  Name: UspGetPcSkillPack  
**  Desc: PC狼 胶懦蒲甸阑 肺靛茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcSkillPack]
	 @pPcNo		INT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, 滚妨柳 酒捞袍
	
	SELECT 
		mSPID,
		DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
		FROM dbo.TblPcSkillPackInventory WITH(NOLOCK)
		WHERE 	mPcNo = @pPcNo
					AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY mSPID ASC

GO

