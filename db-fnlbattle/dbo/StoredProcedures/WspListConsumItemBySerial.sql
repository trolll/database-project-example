/******************************************************************************
**		Name: WspListConsumItemBySerial
**		Desc: 
**
**		Auth: JUDY
**		Date: 2010-12-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspListConsumItemBySerial]
	@mSerialString VARCHAR(500)		-- 霓付 备盒
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	SET ROWCOUNT 1000;		-- 弥措 啊廉棵 荐 乐绰 酒捞袍 荐甫 力茄 

	SELECT 
		 T1.mNm		-- 家瘤磊
		,T1.mNo		-- 某腐磐 锅龋
		,T4.IName		-- 酒捞袍疙
		,DATEDIFF(DAY, GETDATE(), T3.mEndDate) AS mEndDate	-- 蜡瓤扁埃
		,T3.mSerialNo
		,T2.mCurCnt
		,T2.mItemNo
		,0 mIsSeizure
		,T3.mRegDate	-- 嚼垫老磊
		,T3.mPracticalPeriod
		,T3.mBindingType
		,T3.mRestoreCnt
	FROM dbo.TblConsignmentItem  T3 WITH(INDEX=NC_TblConsignmentItem_mSerialNo)	
		INNER LOOP JOIN dbo.TblConsignment  T2 
			ON T3.mCnsmNo = T2.mCnsmNo				
		INNER JOIN dbo.TblPc T1 
			ON T1.mNo = T2.mPcNo			
		LEFT OUTER JOIN FNLParm.dbo.DT_Item T4
			ON T2.mItemNo = T4.IID	
	WHERE T3.mSerialNo IN (		
		SELECT 
			CONVERT(BIGINT, element)
		FROM dbo.fn_SplitTSQL ( @mSerialString, ',')
	)

GO

