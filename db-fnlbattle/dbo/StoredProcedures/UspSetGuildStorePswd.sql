CREATE PROCEDURE [dbo].[UspSetGuildStorePswd]
	  @pGuildNo	INT
	, @pPswd		CHAR(8)
	, @pStoreLv	TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE	 @aRowCnt		INT
	DECLARE  @aErr			INT

	SELECT  @aRowCnt = 0, @aErr = 0

	IF NOT EXISTS (	SELECT * 
					FROM dbo.TblGuild 
					WHERE mGuildNo = @pGuildNo)
	BEGIN
		RETURN(1)	-- 辨靛啊 绝澜
	END 
	
	UPDATE dbo.TblGuildStorePassword
	SET 
		mPassword = @pPswd 
	WHERE mGuildNo = @pGuildNo 
			AND mGrade = @pStoreLv
	
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT	

	IF(@aErr <> 0)
		RETURN(3)	-- db error
		
	IF(@aRowCnt >= 1 AND @aErr = 0)	-- effect row 1 over
		RETURN(0)
		
	INSERT INTO dbo.TblGuildStorePassword (mGuildNo, mGrade, mPassword) 
	VALUES(@pGuildNo, @pStoreLv, @pPswd)
	
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT	

	IF(@aErr <> 0) OR ( @aRowCnt <= 0 )		
		RETURN(2)	-- db error 

	RETURN(0)

GO

