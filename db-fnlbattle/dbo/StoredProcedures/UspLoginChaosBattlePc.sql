/******************************************************************************
**		Name: UspLoginChaosBattlePc
**		Desc: 墨坷胶硅撇 辑滚俊 肺弊牢茄促.
**			  鞘靛辑滚 汗备 -> 墨坷胶 硅撇 辑滚俊辑 官肺 汗备 贸府 啊瓷窍档废 函版 茄促. 
**
**		Auth: 辫 堡挤
**		Date: 2009-02-05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2009.08.10	kslive				拌沥捞傈俊 措茄 贸府 眠啊
**		2009.04.29	JUDY				昏力等 某腐磐 肺弊 坷幅登档废 函版 
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspLoginChaosBattlePc]
	  @pFieldSvrNo	INT
	, @pFieldPcNo	INT
	, @pFieldNm		CHAR(12)
	, @pOwner		INT
	, @pPcNo		INT OUTPUT
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	DECLARE		@aErrNo			INT;
	DECLARE		@aRowCnt		INT;
	DECLARE		@aNm			CHAR(12);
	DECLARE		@aDelDate		SMALLDATETIME;
	DECLARE		@aOwner			INT;
	
	SELECT	@aErrNo = 0, @aRowCnt = 0;
	
	SELECT
		  @pPcNo		= [mNo]
		, @aNm			= [mNm]
		, @aDelDate		= [mDelDate]
		, @aOwner		= [mOwner]
	FROM	dbo.TblChaosBattlePc
	WHERE	mFieldSvrNo = @pFieldSvrNo 
				AND mFieldSvrPcNo = @pFieldPcNo;

	SELECT	@aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR;	
	IF @aDelDate IS NOT NULL	
	BEGIN
		RETURN (5);		-- 昏力等 某腐磐啊 肺弊牢 矫档 茄促.
	END 
	
	-- 捞抚俊 措秦 眉农甫 茄促. (捞固 粮犁窍绰 捞抚捞扼搁 DB 狼 单捞磐甫 函版秦辑 货肺款 捞抚捞 眠啊瞪 荐 乐霸 茄促.)
	EXEC	@aErrNo = dbo.UspChaosBattleNameCheck @pFieldSvrNo, @pFieldPcNo, @pFieldNm, @pOwner
	IF(0 <> @aErrNo)
	 BEGIN
		RETURN(@aErrNo);
	 END
	 
	-- 某腐磐 沥焊啊 粮犁窍瘤 臼绰促搁 货肺 积己茄促. (家蜡磊 捞傈俊 措茄 贸府档 柳青茄促.)
	IF((0 = @aRowCnt) OR (@aOwner <> @pOwner))
	 BEGIN
	 
		EXEC	@aErrNo = dbo.UspCreateChaosBattlePc @pFieldSvrNo, @pFieldPcNo, @pFieldNm, @pOwner, @pPcNo OUTPUT;
		IF(0 <> @aErrNo)
		 BEGIN
			RETURN(@aErrNo);
		 END
		 
		IF (0 = @aRowCnt) 
			SET @aNm	= @pFieldNm;				
	 END
	 
	IF(RTRIM(@aNm) <> RTRIM(@pFieldNm))
	 BEGIN
		UPDATE	dbo.TblChaosBattlePc 
		SET 
			  [mNm] = @pFieldNm
			, [mChgNmDate] = GETDATE() 
			, [mLoginTm] = GETDATE()
		WHERE [mFieldSvrNo] = @pFieldSvrNo 
				AND [mFieldSvrPcNo] = @pFieldPcNo
	 END
	ELSE
	 BEGIN
		-- Login 矫埃阑 函版茄促.
		UPDATE	dbo.TblChaosBattlePc 
		SET 
			 [mLoginTm] = GETDATE()
		WHERE	[mFieldSvrNo] = @pFieldSvrNo 
					AND [mFieldSvrPcNo] = @pFieldPcNo
	 END
	
	SELECT	@aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;	
	IF((0 <> @aErrNo) OR (1 <> @aRowCnt))
	BEGIN
		SET		@aErrNo = 6;
	END
		
	RETURN(@aErrNo);

GO

