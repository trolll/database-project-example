/******************************************************************************
**		Name: WspListStoreItemByAccount
**		Desc: 拌沥 家蜡 芒绊 酒捞袍 炼雀
**
**		Auth: 辫籍玫
**		Date: 20090428
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspListStoreItemByAccount]
	@mUserId VARCHAR(20) -- 拌沥疙
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		p.mSerialNo
		,p.mRegDate
		-- 家瘤磊
		-- 困摹
		,ii.IName -- 酒捞袍 捞抚
		, p.mStatus -- 惑怕
		, DATEDIFF(DAY, GETDATE(), p.mEndDate) AS mEndDate  -- 蜡瓤扁埃
		-- , p.mSerialNo -- 矫府倔
		, p.mCnt -- 俺荐
		, p.mItemNo -- TID
		, p.mIsSeizure -- 拘幅咯何
		--, p.mNo -- Unique Key
		, p.mPracticalPeriod
		, p.mBindingType
		, p.mRestoreCnt
	FROM [dbo].[TblPcStore] AS p 
		INNER JOIN	[FNLAccount].[dbo].[TblUser] AS u 
			ON	p.mUserNo = u.mUserNo
		INNER JOIN	[FNLParm].[dbo].[DT_Item] AS ii 
			ON	p.mItemNo = ii.IID
	WHERE u.mUserId = @mUserId
	ORDER BY p.mItemNo

GO

