/******************************************************************************
**		Name: UspResetSpecificTerritoryGuildAgit
**		Desc: 漂沥 辨靛 酒瘤飘甫 檬扁拳 茄促.
**		Warn: 抛捞喉俊 Territory 甫 牢郸胶肺 棱篮 镑捞 绝促. 
**			  弊矾骨肺 角力 繁鸥烙俊 龋免窍妨搁 DBA 客 惑狼啊 鞘夸窍促. (泅犁绰 霸烙 檬扁拳 矫痢俊 龋免窃)
**
**		Auth: 辫 堡挤
**		Date: 2010-06-09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspResetSpecificTerritoryGuildAgit]
	@pTerritory		INT		-- 檬扁拳且 漂沥 康瘤
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT;
	SET		@aErrNo 	= 0;
	
	BEGIN TRAN
	
		-- 漂沥 康瘤狼 窍快胶甫 檬扁拳茄促. (券阂篮 鞘夸绝促.)
		UPDATE	dbo.TblGuildAgit	
		SET mOwnerGID = 0, mGuildAgitName = '', mLeftMin = 0, mIsSelling = 0, mSellingMoney = 0, mBuyingMoney = 0 
		WHERE	mTerritory = @pTerritory
		
		IF(0 <> @@ERROR)
		 BEGIN
			SET		@aErrNo	= 1;	-- DB Error
			GOTO	Label_End;
		 END
		 
		-- 漂沥 康瘤狼 版概 郴侩阑 檬扁拳 茄促. (券阂篮 鞘夸绝促.)
		UPDATE	dbo.TblGuildAgitAuction	SET mCurBidGID = 0, mCurBidMoney = 0 
		WHERE mTerritory = @pTerritory;
		
		IF(0 <> @@ERROR)
		 BEGIN
			SET		@aErrNo	= 2;	-- DB Error
			GOTO	Label_End;
		 END
		 
		-- 漂沥 康瘤狼 檬措厘 郴侩阑 檬扁拳 茄促.
		DELETE	dbo.TblGuildAgitTicket	
		WHERE mTerritory = @pTerritory;
		
		IF(0 <> @@ERROR)
		 BEGIN
			SET		@aErrNo	= 2;	-- DB Error
			GOTO	Label_End;
		 END

Label_End:
	IF(0 <> @aErrNo)
	 BEGIN
		ROLLBACK TRAN;
	 END
	ELSE
	 BEGIN
		COMMIT TRAN;
	 END
	 
	 
	RETURN(@aErrNo);

GO

