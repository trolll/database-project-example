CREATE Procedure [dbo].[UspGetGuildAgitAuctionList]	-- 版概 啊瓷茄 辨靛酒瘤飘 府胶飘 掘扁
	 @pTerritory		INT			-- 康瘤锅龋
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	-- TblGuildAgitAuction 狼 秦寸 康瘤锅龋狼 辨靛酒瘤飘锅龋 牢胶畔胶绰 UspReduceGuildAgitTime 俊辑 固府 积己凳捞 焊厘登绢具 窃

	-- 辨靛酒瘤飘 府胶飘 馆券 - 家蜡辨靛啊 绝芭唱 概阿可记捞 劝己拳等 版快俊 茄窃
	SELECT 
		 A.mTerritory
		,A.mGuildAgitNo
		,A.mOwnerGID
		,ISNULL((CASE A.mOwnerGID
			WHEN 0 THEN N'<NONE>'
			ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = A.mOwnerGID)
			END), N'<FAIL>') As mOwnerGuildName
		,A.mGuildAgitName
		,A.mSellingMoney
		,B.mCurBidGID
		,ISNULL((CASE B.mCurBidGID
			WHEN 0 THEN N'<NONE>'
			ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = B.mCurBidGID)
			END), N'<FAIL>') As mCurBidGuildName
		,B.mCurBidMoney
		,A.mLeftMin 
	FROM TblGuildAgit A JOIN TblGuildAgitAuction B ON A.mTerritory = B.mTerritory  AND A.mGuildAgitNo = B.mGuildAgitNo 	-- WITH(NOLOCK) 
	WHERE A.mTerritory = @pTerritory AND (A.mOwnerGID = 0 OR A.mIsSelling = 1)

	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo = 1	-- 1: DB 郴何利牢 坷幅
	END
	
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

