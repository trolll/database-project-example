CREATE PROCEDURE [dbo].[UspDelRacingTicket]
	 @pSerialNo		BIGINT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	DELETE dbo.TblRacingTicket 
	WHERE mSerialNo = @pSerialNo

	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)
	END	

	RETURN(0)

GO

