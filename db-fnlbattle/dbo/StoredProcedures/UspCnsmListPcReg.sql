/******************************************************************************  
**  Name: UspCnsmListPcReg  
**  Desc: 夯牢 殿废 拱前 府胶泼
**  
**                
**  Return values:  
**   饭内靛悸
**   
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmListPcReg]
 @pPcNo				INT
 ,@pBalance			BIGINT OUTPUT
AS  
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	SELECT 	@pBalance = mBalance
	FROM dbo.TblConsignmentAccount
	WHERE mPcNo=@pPcNo;
	IF @pBalance IS NULL
	BEGIN
		SET @pBalance = 0;
	END


	declare @aCurDate smalldatetime;
	SET @aCurDate = getdate();
	select a.mCnsmNo, a.mCategoryNo, a.mPrice, 
			CASE WHEN a.mTradeEndDate <= @aCurDate
			THEN 0 ELSE
				DATEDIFF(minute, getdate(), a.mTradeEndDate) END, 
			--a.mPcNo, -- @pPcNo客 鞍栏骨肺 鞘夸啊 绝促.
			a.mRegCnt, a.mCurCnt, a.mItemNo,
			b.mStatus, b.mCntUse, b.mOwner, b.mPracticalPeriod
	from dbo.TblConsignment a 
			inner join dbo.TblConsignmentItem b 
					on a.mCnsmNo = b.mCnsmNo
	where a.mPcNo=@pPcNo;

GO

