/******************************************************************************
**		Name: [UspSetPcItemBindType]
**		Desc: 酒捞袍狼 蓖加 鸥涝阑 函版茄促.
**		Auth: 辫碍龋
**		Date: 2010.01.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 	Author:	
**		Description: 
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcItemBindType]
   @pSerial  BIGINT  -- 措惑 酒捞袍狼 矫府倔锅龋  
 , @pBindType TINYINT  -- 官牢靛 鸥涝
AS
	SET NOCOUNT ON  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   

	DECLARE @aErrNo  INT  
	,@aRowCnt INT  

	SELECT  @aErrNo = 0, @aRowCnt = 0  

	UPDATE dbo.TblPcInventory  
	SET mBindingType = @pBindType  
	WHERE mSerialNo = @pSerial   

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  
	IF @aErrNo <> 0 OR  @aRowCnt = 0  
	BEGIN  
		RETURN (1) -- db error  
	END    
   
RETURN(0)

GO

