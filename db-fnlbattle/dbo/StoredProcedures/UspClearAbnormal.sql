CREATE Procedure [dbo].[UspClearAbnormal]
	 @pPcNo			INT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	DELETE TblPcAbnormal WHERE [mPcNo] = @pPcNo
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END
	 END
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

