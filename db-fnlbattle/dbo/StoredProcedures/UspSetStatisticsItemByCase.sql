CREATE PROCEDURE [dbo].[UspSetStatisticsItemByCase]
	  @pItemNo		INT
	, @pMerchantCreate		BIGINT
	, @pMerchantDelete		BIGINT
	, @pReinforceCreate	BIGINT
	, @pReinforceDelete		BIGINT
	, @pCraftingCreate		BIGINT
	, @pCraftingDelete		BIGINT
	, @pPcUseDelete		BIGINT
	, @pNpcUseDelete		BIGINT
	, @pNpcCreate		BIGINT
	, @pMonsterDrop		BIGINT
	, @pGSExchangeCreate	BIGINT = 0
	, @pGSExchangeDelete	BIGINT = 0 
AS
	SET NOCOUNT ON;
	
	DECLARE		@aErrNo		INT;
	SET @aErrNo = 0;
	
	INSERT INTO dbo.TblStatisticsItemByCase 
		([mItemNo], [mMerchantCreate], [mMerchantDelete], [mReinforceCreate], [mReinforceDelete], [mCraftingCreate], [mCraftingDelete], 
		[mPcUseDelete], [mNpcUseDelete], [mNpcCreate], [mMonsterDrop], [mGSExchangeCreate], [mGSExchangeDelete])
	VALUES
		(@pItemNo, @pMerchantCreate, @pMerchantDelete, @pReinforceCreate, @pReinforceDelete, @pCraftingCreate, 						@pCraftingDelete, @pPcUseDelete, @pNpcUseDelete, @pNpcCreate, @pMonsterDrop, @pGSExchangeCreate, @pGSExchangeDelete);

							
	IF @@ERROR <> 0
		SET @aErrNo = 2;
														
	RETURN(@aErrNo);

GO

