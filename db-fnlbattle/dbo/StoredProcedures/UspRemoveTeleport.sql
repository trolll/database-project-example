CREATE PROCEDURE [dbo].[UspRemoveTeleport]
	 @pNo			INT		-- 炮饭器飘 逞滚
	,@pPcNo			INT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	DELETE dbo.TblPcTeleport 
	WHERE ([mNo]=@pNo) 
		AND ([mPcNo]=@pPcNo)
	IF(0 <> @@ERROR)
	 BEGIN
		SET	@aErrNo	= 1
		GOTO LABEL_END		 
	 END	
			
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

