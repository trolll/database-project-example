CREATE PROCEDURE [dbo].[UspGetFriendRegistMe]
	 @pPcNo	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		b.mOwnerPcNo	
	FROM dbo.TblPc AS a
		INNER JOIN dbo.TblPcFriend AS b
			ON a.mNm = b.mFriendPcNm
					AND a.mNo = b.mFriendPcNo
	WHERE a.mNo = @pPcNo
			AND a.mDelDate IS NULL

	SET NOCOUNT OFF

GO

