CREATE PROCEDURE [dbo].[UspSetEvent]
	@pEventType	INT
AS
	SET NOCOUNT ON
	
	DECLARE @aRemainerCnt	INT,
			@aRandomVal		TINYINT,
			@aCurrRandomVal TINYINT	
	
	SELECT	@aRemainerCnt = 0,
			@aRandomVal = 0,
			@aCurrRandomVal = 0
		
	SELECT 
		@aRemainerCnt = mRemainerCnt,
		@aRandomVal = mRandomVal
	FROM dbo.TblEvent
	WHERE mEventType = @pEventType
	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0  
	BEGIN
		RETURN (1)	-- db error
	END 

	SELECT @aCurrRandomVal = CONVERT(TINYINT, RAND() * 100)		-- 100% 犬伏	
	IF @aCurrRandomVal > @aRandomVal
	BEGIN
		RETURN(2)	-- 犬伏俊 固 器窃.
	END  
		
	IF @aRemainerCnt = 0
	BEGIN
		RETURN(3)	-- 儡咯 肮荐啊 绝促.
	END

	UPDATE dbo.TblEvent
	SET 
		@aRemainerCnt = mRemainerCnt = mRemainerCnt - 1
	WHERE mEventType = @pEventType
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0  
	BEGIN
		RETURN (1)	-- db error
	END 		

	IF @aRemainerCnt <= 0 
	BEGIN
		RETURN(3)	-- 儡咯 肮荐啊 绝促.
	END
	
	RETURN(0)	-- 惯青

GO

