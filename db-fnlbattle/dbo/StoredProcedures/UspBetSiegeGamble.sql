CREATE  PROCEDURE [dbo].[UspBetSiegeGamble]
	 @pPcNo		INT
	,@pItemNo		INT
	,@pBettingMoney	INT
	,@pValidDay		INT		-- 蜡烹扁茄(窜困:老)
	,@pTerritory		INT
	,@pSiegeNo		INT
	,@pIsKeep		INT
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT
	DECLARE	@aSerial		BIGINT	-- 馆券瞪 SN.
	DECLARE	@aEndDay	SMALLDATETIME,
				@aRowCnt	INT

	SELECT	@aErrNo = 0, 
			@aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay),
			@aSerial = 0,
			@aRowCnt = 0
	
	-----------------------------------------------------
	-- 酒捞袍 惯鞭 矫府倔
	-----------------------------------------------------
	EXEC @aSerial =  dbo.UspGetItemSerial 
	IF @aSerial <= 0
	BEGIN
		SET @aErrNo = 8
		GOTO T_END	
	END 								
	

	BEGIN TRAN
			
		INSERT dbo.TblPcInventory ( mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
				VALUES(@aSerial, @pPcNo, @pItemNo, @aEndDay, 1, 1, 1, 0)	

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1
			GOTO T_ERR			 
		END 
				
		INSERT dbo.TblSiegeGambleTicket(mSerialNo, mTerritory, mNo, mIsKeep)
				VALUES(@aSerial, @pTerritory, @pSiegeNo, @pIsKeep)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 2
			GOTO T_ERR			 
		END
		 
		UPDATE dbo.TblSiegeGambleState 
		SET 
			mTotalMoney = mTotalMoney + @pBettingMoney 
		WHERE  mTerritory =@pTerritory
				AND  mNo = @pSiegeNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 3
			GOTO T_ERR			 
		END			

T_ERR:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
	
T_END:
	SELECT @aErrNo, @aSerial, DATEDIFF(dd,GETDATE(),@aEndDay)

GO

