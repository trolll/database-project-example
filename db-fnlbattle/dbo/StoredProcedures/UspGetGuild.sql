/******************************************************************************
**		File: UspGetGuild.sql
**		Name: UspGetGuild
**		Desc: Guild沥焊.
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20080722	JUDY				GuildMaster 沥焊 馆券
**		20090618	JUDY				辨靛 锅龋俊辑 辨靛疙 函版
**		20091029	辫碍龋				己 规绢 , 胶铺 规绢 驱琶 饭骇
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetGuild]
	 @pGuildNo		INT
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT a.[mGuildSeqNo], RTRIM(a.[mGuildNm]), a.[mGuildMark], RTRIM(a.[mGuildMsg]), a.[mRegDate], 
		   b.[mGuildAssNo], a.[mRewardExp], p.mNm, a.[mCastleDfnsLv], a.[mSpotDfnsLv]
	FROM dbo.TblGuild AS a 
		INNER JOIN dbo.TblGuildMember AS gm
			ON gm.mGuildNo = a.mGuildNo	
				AND gm.mGuildGrade = 0
		INNER JOIN dbo.TblPc AS p
			ON gm.mPcNo = p.mNo							
		LEFT OUTER JOIN dbo.TblGuildAssMem AS b 
			ON(a.mGuildNo = b.mGuildNo)							
	WHERE a.[mGuildNo] 	= @pGuildNo;

GO

