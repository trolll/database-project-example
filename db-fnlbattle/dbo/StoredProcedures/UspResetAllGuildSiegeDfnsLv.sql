/******************************************************************************
**		Name: UspResetAllGuildSiegeDfnsLv
**		Desc: 辨靛 痢飞 驱琶 饭骇 函版
**
**		Auth: 辫碍龋
**		Date: 2009.10.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 		Author:				Description: 
**		--------	--------			---------------------------------------
**  Date:   Author:    Description:   
**  -------- --------   ---------------------------------------  
**  2009.11.16 辫碍龋   pResetType 眠啊(0 - 傈何(default), 1 - mCastleDfnsLv 檬扁拳, 2 - mSpotDfnsLv 檬扁拳)
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspResetAllGuildSiegeDfnsLv]  
	@pResetType INT
AS  
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	DECLARE @aErrNo  INT  
	SET  @aErrNo  = 0   

	IF @pResetType = 0 
	BEGIN 
	 UPDATE dbo.TblGuild  
	 SET   
	  [mCastleDfnsLv]=0,  
	  [mSpotDfnsLv]=0  
	END
	ELSE IF @pResetType = 1
	BEGIN
	 UPDATE dbo.TblGuild  
	 SET   
	  [mCastleDfnsLv]=0
	END
	ELSE IF @pResetType = 2
	BEGIN
	 UPDATE dbo.TblGuild  
	 SET   
	  [mSpotDfnsLv]=0
	END

	SELECT @aErrNo = @@ERROR;  

	RETURN(@aErrNo);

GO

