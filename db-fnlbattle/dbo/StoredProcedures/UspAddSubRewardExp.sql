CREATE PROCEDURE [dbo].[UspAddSubRewardExp]
	 @pGuildNo		INT
	,@pVal			INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	UPDATE dbo.TblGuild 
	SET [mRewardExp]=[mRewardExp]+@pVal 
	WHERE ([mGuildNo]=@pGuildNo)
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo	= 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

