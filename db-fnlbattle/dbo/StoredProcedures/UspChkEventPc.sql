CREATE PROCEDURE [dbo].[UspChkEventPc]  	
	@pUserNo	INT
	, @pCharNm	VARCHAR(12)
	, @pResult	BIT = 0 OUTPUT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	

	IF EXISTS (	SELECT *
				FROM dbo.TblPc T1
					INNER JOIN dbo.TblPcState T2
						ON T1.mNo = T2.mNo
				WHERE T1.mOwner = @pUserNo
					AND T1.mNm = @pCharNm
					AND T1.mRegDate >= '2008-09-25 00:00:00'
					AND T1.mDelDate IS NULL
					AND T2.mLevel >= 3 )
	BEGIN
		SET @pResult = 1
	END					
	ELSE
	BEGIN 
		SET @pResult = 0
	END

GO

