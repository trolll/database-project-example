CREATE  PROCEDURE [dbo].[UspListGuildAgitTicket]
	 @pPcNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	SELECT	 a.mTicketSerialNo
			,a.mTerritory 
			,a.mGuildAgitNo
			,a.mFromPcNm
			,a.mToPcNm
	FROM dbo.TblGuildAgitTicket  AS a 
		INNER JOIN dbo.TblPcInventory    AS b 
		ON a.mTicketSerialNo = b.mSerialNo
	WHERE b.mPcNo=@pPcNo
			AND b.mEndDate > GETDATE() 
		
	SET NOCOUNT OFF

GO

