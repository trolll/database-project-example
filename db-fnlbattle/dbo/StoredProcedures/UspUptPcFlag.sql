CREATE Procedure [dbo].[UspUptPcFlag]
	@pPcNo		INT,
	@pFlag		TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


	UPDATE dbo.TblPcState 
	SET mFlag = @pFlag 
	WHERE mNo = @pPcNo	
	
	IF 0 = @@ROWCOUNT
		RETURN(1) -- PC 沥焊啊 粮犁窍瘤 臼澜

	IF @@ERROR <> 0
		RETURN(2) -- db error

	RETURN(0)

	SET NOCOUNT OFF

GO

