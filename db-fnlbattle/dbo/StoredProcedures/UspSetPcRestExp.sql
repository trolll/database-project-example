/******************************************************************************
**		Name: dbo.UspSetPcRestExp
**		Desc: TblPcState狼 绒侥 版氰摹 汲沥
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010.12.02	傍籍痹				TblPcState狼 mRestExp > mRestExpGuild 函版
**										mRestExpActivate, mRestExpDeactivate 眠啊肺
**										@pRestExp甫 @pRestExpGuild肺 荐沥
**										@pRestExpActivate, @pRestExpDeactivate 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcRestExp]
	@pPcNo				INT,
	@pRestExpGuild		BIGINT,
	@pRestExpActivate	BIGINT,
	@pRestExpDeactivate	BIGINT
AS  
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aRv INT
			, @aErrNo INT
			, @aRowCnt INT;

	SELECT @aRv = 0, @aErrNo = 0, @aRowCnt = 0;

	UPDATE dbo.TblPcState
	SET mRestExpGuild = @pRestExpGuild
		, mRestExpActivate = @pRestExpActivate
		, mRestExpDeactivate = @pRestExpDeactivate
	WHERE mNo = @pPcNo;

	SELECT @aErrNo = @@Error, @aRowCnt = @@RowCount;

	IF @aErrNo <> 0
	BEGIN
		SET @aRv = 1;
	END

	IF @aRowCnt <= 0
	BEGIN
		SET @aRv = 1;
	END

	RETURN (@aRv);

GO

