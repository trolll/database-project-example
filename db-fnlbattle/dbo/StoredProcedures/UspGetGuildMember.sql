CREATE PROCEDURE [dbo].[UspGetGuildMember]
	 @pGuildNo		INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	SELECT a.[mPcNo], RTRIM(a.[mNickNm]), a.[mGuildGrade], RTRIM(b.[mNm]), c.[mLogoutTm]
	FROM dbo.TblGuildMember AS a 
		INNER JOIN dbo.TblPc		AS b 
			ON(a.[mPcNo] = b.[mNo])
		INNER JOIN dbo.TblPcState	AS c 
			ON(a.[mPcNo] = c.[mNo])
		WHERE a.[mGuildNo] = @pGuildNo
		ORDER BY b.[mNm]

	SET NOCOUNT OFF

GO

