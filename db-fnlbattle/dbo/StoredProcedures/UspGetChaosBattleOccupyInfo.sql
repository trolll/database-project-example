/******************************************************************************
**		File: 
**		Name: UspGetChaosBattleOccupyInfo
**		Desc: 墨坷胶 硅撇 辑滚狼 痢飞 沥焊甫 肺靛茄促.
**
**		Auth: 辫 堡挤
**		Date: 2009-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetChaosBattleOccupyInfo]
	@pChaosBattleSvr	SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		  [mTerritory]
		, [mOccupySvrNo]
		, [mOccupyDate]
	FROM	dbo.TblChaosBattleOccupyInfo
	WHERE	[mChaosBattleSvrNo] = @pChaosBattleSvr

GO

