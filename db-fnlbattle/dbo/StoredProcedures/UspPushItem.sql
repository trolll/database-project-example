CREATE PROCEDURE [dbo].[UspPushItem]
	 @pPcNo			INT				-- 脚家蜡磊.(NON-PC搁 1烙)
	,@pSerial		BIGINT			-- 0捞搁 脚痹.
	,@pItemNo		INT
	,@pValidDay		INT				-- 蜡瓤老.(窜困:老)
	,@pCnt			INT				-- 秒垫且 酒捞袍狼 俺荐.
	,@pCntUse		TINYINT
	,@pIsConfirm	BIT
	,@pStatus		TINYINT 
	,@pIsStack		BIT
	,@pIsCharge		BIT = 0			-- 蜡丰 酒捞袍 咯何
	,@pPracticalPeriod	INT = 0			-- 瓤苞瘤加矫埃 
	,@pBindingType	TINYINT = 0
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	 @aEndDay		SMALLDATETIME
				,@aOrgPcNo		INT		-- 扁粮 家蜡 磊
				,@aIsSeizure		BIT	
				,@aCntRest		INT
				,@aRowCnt		INT
				,@aErr			INT
				,@aSerial	BIGINT
				,@aCntTarget		BIGINT
				,@aOwner		INT
				,@aPracticalPeriod	INT
				
	-----------------------------------------------
	-- 函荐 檬扁拳
	-----------------------------------------------
	SELECT		 @aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)
				,@aRowCnt = 0
				,@aErr = 0
				,@aOrgPcNo = 0
				,@aSerial = 0 
				,@aOwner = 0
				,@aPracticalPeriod = 0
	IF @@ERROR <> 0			-- 荤侩磊 沥狼 窃荐俊辑 俊矾 惯积矫 俊矾 馆券 
	BEGIN
		SET @aErr = 1				
		GOTO T_END
	END 
	IF @aEndDay < GETDATE()
	BEGIN
		SET @aErr = 1				
		GOTO T_END
	END 
	
	-----------------------------------------------
	-- 酒捞袍 蓖加 鸥涝 眉农 
	-----------------------------------------------	
	IF @pSerial <> 0 
		AND @pBindingType IN(1,2)	
	BEGIN
		SET @aErr = 11	-- 蓖加 加己篮 捞悼且 荐啊 绝促. 			
		GOTO T_END
	END 	
			
	-----------------------------------------------
	-- 409 : 捣 酒捞袍 
	-----------------------------------------------
	IF(@pItemNo = 409)
	BEGIN
		SET		@pIsConfirm = 1
		SET		@pStatus = 1
	END
		
	-----------------------------------------------
	-- 扁粮 酒捞袍
	-----------------------------------------------
	IF( @pSerial <> 0 )
	BEGIN		
		SELECT 	TOP 1		
			@aOrgPcNo = [mPcNo],	-- 扁粮 家蜡磊	
			@aCntRest=[mCnt], 
			@aEndDay=[mEndDate], 
			@aIsSeizure=[mIsSeizure],
			@aOwner = [mOwner],
			@aPracticalPeriod = [mPracticalPeriod],
			@pBindingType = mBindingType
		FROM dbo.TblPcInventory WITH(INDEX=PK_NC_TblPcInventory_1)
		WHERE  mSerialNo = @pSerial
		SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErr <> 0 OR 	@aRowCnt = 0
		BEGIN
			SET @aErr = 1				
			GOTO T_END
		END		
	
		IF @aIsSeizure <> 0
		BEGIN
			SET @aErr = 29	
			GOTO T_END					--	拘幅 惑怕 酒捞袍
		END
		SET @aCntRest = @aCntRest - @pCnt
		IF (@aCntRest <  0 )
		BEGIN
			SET @aErr = 2				-- 荤侩且 荐 乐绰 酒捞袍 肮荐啊 葛磊福促.( 公炼扒 0焊促 目具 茄促)
			GOTO T_END		
		END
		
		IF @pBindingType IN(1,2)	
		BEGIN
			SET @aErr = 11	-- 蓖加 加己篮 捞悼且 荐啊 绝促. 			
			GOTO T_END
		END 	
		
	END
	-----------------------------------------------
	-- 穿利屈 酒捞袍 粮犁咯何 眉农 
	-----------------------------------------------
	IF (@pIsStack <> 0) AND (1 <> @pPcNo)
	BEGIN
		SELECT 	TOP 1			
			@aCntTarget=ISNULL([mCnt],0), 
			@aSerial=[mSerialNo], 
			@aIsSeizure=ISNULL([mIsSeizure],0)
		FROM dbo.TblPcInventory WITH(INDEX=CL_TblPcInventory)		
		WHERE  mPcNo  = @pPcNo
				AND mItemNo = @pItemNo 
				AND mEndDate  = @aEndDay
				AND mIsConfirm  = @pIsConfirm  
				AND mStatus  = @pStatus
				AND mOwner = @aOwner  
				AND mPracticalPeriod = @pPracticalPeriod
				AND mBindingType = @pBindingType	-- 穿利屈 酒捞袍狼 版快 官牢靛 鸥涝捞 悼老秦具 茄促.
 
		SELECT @aErr = @@ERROR
		IF ( @aErr <> 0 )
		BEGIN
			SET @aErr = 3	
			GOTO T_END		
		END		
		IF(@aIsSeizure <> 0 )				
		 BEGIN
			SET @aErr = 28	
			GOTO T_END		
		 END	
	END	
	
	-----------------------------------------------
	-- 酒捞袍 捞悼
	-----------------------------------------------
	BEGIN TRAN
		IF @aCntTarget  <> 0 
		BEGIN
			UPDATE dbo.TblPcInventory				--	穿利侩 酒捞袍 粮犁 窍搁 扁粮 酒捞袍俊 穿利窍绊 
			SET
				mCnt = mCnt + @pCnt
			WHERE  mSerialNo = @aSerial
			
			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF  @aErr <> 0 OR 	@aRowCnt = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 4	
				GOTO T_END		
			END		
			IF ( @pSerial <> 0)
			BEGIN
				IF @aCntRest < 1		-- 儡咯酒捞袍 粮犁窍瘤 臼阑 锭 
				BEGIN
					DELETE dbo.TblPcInventory
					WHERE mSerialNo = @pSerial
	
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 5	
						GOTO T_END		
					END		
				END 
				ELSE
				BEGIN
					UPDATE dbo.TblPcInventory			
					SET
						mCnt = @aCntRest
					WHERE  mSerialNo =@pSerial				
	
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 6	
						GOTO T_END		
					END		
				END 
			END
		END		
		ELSE
		BEGIN
			IF ( @pSerial <> 0 )
			BEGIN
				IF @aCntRest < 1		-- 儡咯酒捞袍 粮犁窍瘤 臼阑 锭 
				BEGIN		
					SET @aSerial = @pSerial
				
					UPDATE dbo.TblPcInventory			
					SET
						mPcNo = @pPcNo,
						mCnt = @pCnt
					WHERE  mSerialNo =@pSerial				
		
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 7	
						GOTO T_END		
					END		
				END
				ELSE
				BEGIN
		
					UPDATE dbo.TblPcInventory			
					SET
						mCnt = @aCntRest
					WHERE  mSerialNo =@pSerial				
		
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 8	
						GOTO T_END		
					END			
				
					EXEC @aSerial =  dbo.UspGetItemSerial 	-- 矫府倔 掘扁 
					IF @aSerial <= 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 7		
						GOTO T_END		
					END		
				
					INSERT dbo.TblPcInventory(				-- 穿利侩 酒捞袍 粮犁窍瘤 臼栏搁, 货酚霸 积己 
						[mSerialNo],
						[mPcNo], 
						[mItemNo], 
						[mEndDate], 
						[mIsConfirm], 
						[mStatus], 
						[mCnt], 
						[mCntUse],
						[mOwner],
						[mPracticalPeriod],	mBindingType )
					VALUES(
						@aSerial,
						@pPcNo, 
						@pItemNo, 
						@aEndDay, 
						@pIsConfirm, 
						@pStatus, 
						@pCnt, 
						@pCntUse,
						@aOwner,
						@pPracticalPeriod,@pBindingType)
					
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN				
						ROLLBACK TRAN
						SET @aErr = 9
						GOTO T_END		
					END		
				END
			END 
			ELSE
			BEGIN
				EXEC @aSerial =  dbo.UspGetItemSerial 	-- 矫府倔 掘扁 
				IF @aSerial <= 0
				BEGIN
					ROLLBACK TRAN
					SET @aErr = 7		
					GOTO T_END		
				END		
				INSERT dbo.TblPcInventory(				
					[mSerialNo],
					[mPcNo], 
					[mItemNo], 
					[mEndDate], 
					[mIsConfirm], 
					[mStatus], 
					[mCnt], 
					[mCntUse],
					[mOwner],
					[mPracticalPeriod], mBindingType )
				VALUES(
					@aSerial,
					@pPcNo, 
					@pItemNo, 
					@aEndDay, 
					@pIsConfirm, 
					@pStatus, 
					@pCnt, 
					@pCntUse,
					@aOwner,
					@pPracticalPeriod, @pBindingType )
					
				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF  @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN				
					ROLLBACK TRAN
					SET @aErr = 10
					GOTO T_END		
				END		
			END			
		END
	COMMIT TRAN
T_END:
	SELECT @aErr, @aSerial AS Serial, DATEDIFF(mi,GETDATE(),@aEndDay)

GO

