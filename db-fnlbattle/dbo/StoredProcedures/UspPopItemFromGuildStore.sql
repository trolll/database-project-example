CREATE   PROCEDURE [dbo].[UspPopItemFromGuildStore]
	 @pNo			BIGINT		-- SN.
	,@pGuildNo		INT
	,@pCnt			INT		-- 波尘 俺荐.
	,@pItemNo		INT
	,@pPcNo			INT		-- 波郴绰 PC.
	,@pIsStack		BIT
	,@pGrade			TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	 @aRowCnt				INT
	DECLARE  @aErr					INT
	DECLARE	 @aIsSeizure			BIT
	DECLARE	 @aIsConfirm			BIT
	DECLARE	 @aStatus				INT
	DECLARE	 @aCnt					BIGINT
	DECLARE	 @aCntUse				INT
	DECLARE	 @aItemNo				INT
	DECLARE	 @aEndDate				SMALLDATETIME
	DECLARE	 @aSn					BIGINT
	DECLARE	 @aApplyAbnItemNo		INT
	DECLARE  @aApplyAbnItemLeftTick	INT
	DECLARE  @aApplyAbnItemEndDate	SMALLDATETIME
	DECLARE  @aRegDate				DATETIME
	DECLARE  @aOwner				INT
	DECLARE  @aPracticalPeriod		INT
	DECLARE  @aBindingType			TINYINT
	DECLARE	 @aRestoreCnt			TINYINT
					

	SELECT @aRowCnt = 0, @aErr = 0, @aSn = @pNo

	-----------------------------------------------------
	-- 波尘 酒捞袍 沥焊 掘扁
	-----------------------------------------------------
	SELECT 
		@aIsSeizure = mIsSeizure 
		, @aIsConfirm = mIsConfirm
		, @aStatus = mStatus
		, @aCnt= mCnt
		, @aCntUse= mCntUse 
		, @aItemNo= mItemNo
		, @aEndDate= mEndDate
		, @aApplyAbnItemNo=mApplyAbnItemNo
		, @aApplyAbnItemLeftTick=DATEDIFF(mi,GETDATE(),mApplyAbnItemEndDate)
		, @aApplyAbnItemEndDate = mApplyAbnItemEndDate
		, @aRegDate = mRegDate
		, @aOwner = mOwner
		, @aPracticalPeriod = mPracticalPeriod
		, @aBindingType = mBindingType
		, @aRestoreCnt = mRestoreCnt
	FROM dbo.TblGuildStore WITH(INDEX=NC_PK_TblGuildStore_1) 
	WHERE mSerialNo = @pNo		
			AND mGuildNo = @pGuildNo 
			AND mGrade = @pGrade
			
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT

	IF @aRowCnt <> 1			
	BEGIN
		SET @aErr = 1	-- 酒捞袍 沥焊啊 绝促
		GOTO T_END			 
	END
	 
	IF @aIsSeizure <> 0
	BEGIN
		SET @aErr = 11	-- 拘幅 惑怕
		GOTO T_END		 
	 END
	 
	IF @aCnt < @pCnt
	BEGIN
		SET @aErr = 2
		GOTO T_END
	END	 
	

	-----------------------------------------------------	 
	-- 胶琶屈捞 酒聪搁 官肺 秦寸 PC肺 持绰促.
	-----------------------------------------------------
	IF  @pIsStack = 0
	BEGIN
		IF @pCnt <> 1
		BEGIN
			SET @aErr = 2
			GOTO T_END		 
		END

		BEGIN TRAN

			-- 牢亥配府 殿废
			INSERT INTO dbo.TblPcInventory(
				[mRegDate]
				, [mSerialNo]
				, [mPcNo]
				, [mItemNo]
				, [mEndDate]
				, [mIsConfirm]
				, [mStatus]
				, [mCnt]
				, [mCntUse]
				, [mApplyAbnItemNo]
				, [mApplyAbnItemEndDate]
				, [mOwner]
				, [mPracticalPeriod]
				, [mBindingType]
				, [mRestoreCnt])
			VALUES(
				@aRegDate
				, @pNo
				, @pPcNo
				, @aItemNo
				, @aEndDate
				, @aIsConfirm
				, @aStatus
				, @aCnt
				, @aCntUse
				, @aApplyAbnItemNo
				, @aApplyAbnItemEndDate 
				, @aOwner
				, @aPracticalPeriod
				, @aBindingType
				, @aRestoreCnt)
			
			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF @aErr <> 0 OR 	@aRowCnt = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 4
				GOTO T_END
			END

			-- 芒绊俊 阶咯 乐绰 沥焊 昏力 
			DELETE dbo.TblGuildStore 
			WHERE mSerialNo = @pNo 

			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF @aErr <> 0 OR 	@aRowCnt = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 4
				GOTO T_END	 
			END

		COMMIT TRAN
		GOTO T_END
	 END

	-----------------------------------------------------	 
	-- 胶琶屈篮 扁粮俊 悼老茄 巴捞 乐栏搁 穿利窍绊, 绝栏搁 泅犁狼 巴阑 函版茄促.	
	-----------------------------------------------------
	DECLARE	@aTargetSn		BIGINT	
	DECLARE	@aCntTarget		INT
	SET		@aCntTarget = 0		

	SELECT 
		@aCntTarget=ISNULL(mCnt,0), 
		@aTargetSn=mSerialNo
	FROM dbo.TblPcInventory WITH(INDEX=CL_TblPcInventory) 
	WHERE mPcNo = @pPcNo
			AND mItemNo= @aItemNo
			AND mIsConfirm = @aIsConfirm
			AND mStatus = @aStatus
			AND mOwner = @aOwner
			AND mPracticalPeriod = @aPracticalPeriod
			AND mBindingType = @aBindingType
					
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErr <> 0
	BEGIN
		SET @aErr = 4
		GOTO T_END			 
	END	



	-----------------------------------------------------	 
	-- 酒捞袍 捞悼(芒绊->牢亥配府)
	-----------------------------------------
------------
	BEGIN TRAN
			 	
		-------------------------------------
		-- 穿利且 措惑捞 粮犁窍瘤 臼绰促.
		-------------------------------------
		IF(0 = @aCntTarget)
		BEGIN	
			IF(@aCnt <= @pCnt)	-- 葛电 酒捞袍阑 波陈促搁
			BEGIN 
				
				-- 牢亥配府 殿废
				INSERT INTO dbo.TblPcInventory(
					[mRegDate]
					, [mSerialNo]
					, [mPcNo]
					, [mItemNo]
					, [mEndDate]
					, [mIsConfirm]
					, [mStatus]
					, [mCnt]
					, [mCntUse]
					, [mApplyAbnItemNo]
					, [mApplyAbnItemEndDate]
					, [mOwner]
					, [mPracticalPeriod]
					, [mBindingType]
					, [mRestoreCnt])
				VALUES(
					@aRegDate
					, @pNo
					, @pPcNo
					, @aItemNo
					, @aEndDate
					, @aIsConfirm
					, @aStatus
					, @aCnt
					, @aCntUse
					, @aApplyAbnItemNo
					, @aApplyAbnItemEndDate 
					, @aOwner
					, @aPracticalPeriod
					, @aBindingType
					, @aRestoreCnt)									
				
				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErr = 4
					GOTO T_ERR
				END
	
				-- 芒绊俊 阶咯 乐绰 沥焊 昏力 
				DELETE dbo.TblGuildStore 
				WHERE mSerialNo = @pNo 
	
				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErr = 4
					GOTO T_ERR	 
				END
 
			END
			ELSE
			BEGIN					-- 老何父 波陈促搁.

				UPDATE dbo.TblGuildStore 
				SET 
					mCnt =mCnt - @pCnt 
				WHERE  mSerialNo  = @pNo

				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErr = 9
					GOTO T_ERR			 
				END

				EXEC @aSn =  dbo.UspGetItemSerial 
				IF @aSn <= 0
				BEGIN
					SET @aErr = 8
					GOTO T_ERR	
				END 								

				INSERT INTO dbo.TblPcInventory(				
					[mSerialNo],
					[mPcNo], 
					[mItemNo], 
					[mEndDate], 
					[mIsConfirm], 
					[mStatus], 
					[mCnt], 
					[mCntUse],
					[mOwner],
					[mPracticalPeriod], 
					[mBindingType],
					[mRestoreCnt])
				VALUES(
						@aSn,
						@pPcNo, 
						@aItemNo, 
						@aEndDate, 
						@aIsConfirm, 
						@aStatus, 
						@pCnt, 						
						@aCntUse,
						@aOwner,
						@aPracticalPeriod, 
						@aBindingType,
						@aRestoreCnt)	
				IF(0 <> @@ERROR)
				
				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErr = 8
					GOTO T_ERR			 
				END			 		 
			END	 
		END
		ELSE
		BEGIN
			
			 -- 穿利且 巴捞 乐栏搁
			SET @aSn = @aTargetSn
			UPDATE dbo.TblPcInventory 
			SET 
				mCnt = mCnt + @pCnt 
			WHERE  mSerialNo = @aTargetSn

			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF @aErr <> 0 OR 	@aRowCnt = 0
			BEGIN
				SET @aErr = 7
				GOTO T_ERR			 
			END	 
			
			IF(@aCnt <= @pCnt)
			BEGIN -- 葛电 酒捞袍阑 波陈促搁 芒绊俊辑 昏力窃.
				DELETE dbo.TblGuildStore 
				WHERE  mSerialNo = @pNo

				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErr = 8
					GOTO T_ERR				 
				END
			END
			ELSE
			BEGIN -- 老何 酒捞袍父 波陈栏搁 波辰 父怒 扁粮巴阑 皑家矫糯.
				UPDATE dbo.TblGuildStore 
				SET 
					mCnt = mCnt - @pCnt 
				WHERE mSerialNo = @pNo

				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN
					SET @aErr = 9
					GOTO T_ERR			 
				END	 
			END
		END

T_ERR:	
	IF(0 = @aErr)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
			
T_END:			
	SELECT @aErr, @aIsConfirm, @aStatus, @aCntUse, @aIsSeizure, @aItemNo, @aSn, @aApplyAbnItemNo, @aApplyAbnItemLeftTick, @aOwner, @aPracticalPeriod, @aRestoreCnt

GO

