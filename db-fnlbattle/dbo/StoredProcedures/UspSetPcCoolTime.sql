--USE FNLBattleTestQA1
--GO
/******************************************************************************
**		Name: UspSetPcCoolTime
**		Desc: 蜡历狼 酿鸥烙捞 老沥 矫埃 捞惑老 版快 DB俊 涝仿茄促.
**
**		Auth: 沥备柳
**		Date: 09.09.30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	-----------------------------------------------
**      09.05.16    沥备柳				鞍篮 郴侩捞 粮犁且 版快 公矫
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcCoolTime]
	@pPcNo				INT,
	@pSID				INT,
	@pCoolTimeGroup		SMALLINT,
	@pTotalTime			INT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT *
	FROM dbo.TblPcCoolTime 
	WHERE mPcNo = @pPcNo AND mSID = @pSID
		
	IF(0 < @@ROWCOUNT)
	BEGIN
		RETURN(0)
	END
	
	INSERT INTO dbo.TblPcCoolTime (mPcNo, mSID, mCoolTimeGroup, mRemainTime)
	VALUES (
			@pPcNo
			, @pSID
			, @pCoolTimeGroup
			, @pTotalTime
	)

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

