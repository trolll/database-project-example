CREATE PROCEDURE [dbo].[UspSetGuildName]
	 @pGuildNo		INT
	,@pGuildNm		CHAR(12)	
AS
	SET NOCOUNT ON	
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErr = 0
		
		
	--------------------------------------------------------------------------
	-- 吝汗等 辨靛 粮犁咯何 眉农 
	--------------------------------------------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblGuild WITH(NOLOCK)
				WHERE mGuildNm = @pGuildNm )
	BEGIN		
		RETURN(1)				-- 捞固 殿废等 辨靛疙捞促.
	END

	--------------------------------------------------------------------------
	-- 辨靛疙 函版 
	--------------------------------------------------------------------------			
	UPDATE dbo.TblGuild	
	SET 	
		mGuildNm = @pGuildNm
	WHERE  [mGuildNo] = @pGuildNo

	SELECT	@pRowCnt = @@ROWCOUNT, 
			@pErr  = @@ERROR	

	IF @pRowCnt = 0
	BEGIN				
		RETURN(2)
	END
	
	IF @pErr <> 0
	BEGIN				
		RETURN(@pErr)		-- System Error.
	END

	RETURN(0)				-- NOT ERROR

GO

