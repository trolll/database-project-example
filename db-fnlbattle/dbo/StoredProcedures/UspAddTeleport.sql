CREATE PROCEDURE [dbo].[UspAddTeleport]
	 @pPcNo			INT			-- 1捞搁 阁胶磐促. 八荤侩烙.
	,@pName			CHAR(20)
	,@pMapNo		INT
	,@pPosX			REAL
	,@pPosY			REAL
	,@pPosZ			REAL
	,@pNo			INT OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
		
	INSERT dbo.TblPcTeleport([mPcNo], [mName], [mMapNo], [mPosX], [mPosY], [mPosZ]) 
		VALUES(@pPcNo, @pName, @pMapNo, @pPosX, @pPosY, @pPosZ)
	
	SET @pNo = @@IDENTITY
	IF(0 <> @@ERROR)
	BEGIN 
		RETURN(1)
	END

	RETURN(0)

GO

