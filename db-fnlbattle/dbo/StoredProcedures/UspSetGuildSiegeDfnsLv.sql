/******************************************************************************
**		Name: UspSetGuildSiegeDfnsLv
**		Desc: 辨靛 痢飞 驱琶 饭骇 函版
**
**		Auth: 辫碍龋
**		Date: 2009.10.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 		Author:				Description: 
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetGuildSiegeDfnsLv]
	 @pGuildNo		INT
	,@pCastleDfnsLv		SMALLINT
	,@pSpotDfnsLv		SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	UPDATE dbo.TblGuild
	SET 
		[mCastleDfnsLv]=@pCastleDfnsLv,
		[mSpotDfnsLv]=@pSpotDfnsLv
	 WHERE ([mGuildNo]=@pGuildNo)
	 
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo	= 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

