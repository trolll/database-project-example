/******************************************************************************
**		Name: UspSelectPcQuestConditionCnt
**		Desc: 某腐磐 涅胶飘 炼扒 八荤
**
**		Auth: 沥柳龋
**		Date: 2010-02-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSelectPcQuestConditionCnt]
     @pPcNo			INT
	,@pQuestNo		INT	 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT 
			mCnt, mMaxCnt
    FROM dbo.TblPcQuestCondition 
    WHERE mPcNo = @pPcNo
		AND mQuestNo = @pQuestNo

GO

