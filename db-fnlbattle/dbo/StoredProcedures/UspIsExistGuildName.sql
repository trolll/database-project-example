CREATE PROCEDURE [dbo].[UspIsExistGuildName]
	@pGuildName	CHAR(12)	-- 粮犁窍绰瘤 犬牢窍妨绰 辨靛 疙
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	IF EXISTS( SELECT * 
				FROM dbo.TblGuild
				WHERE mGuildNm = @pGuildName)
	 BEGIN
		RETURN(0);	-- 粮犁窍绰版快 
	 END
	ELSE
	 BEGIN
		RETURN(1);	-- 粮犁窍瘤 臼绰 版快
	 END

GO

