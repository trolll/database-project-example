/******************************************************************************
**		File: UspSetPcInvenQSlotInfo
**		Name: UspSetPcInvenQSlotInfo
**		Desc: UspSetPcInvenQSlotInfo 函版
**
**		Auth: 沥柳龋
**		Date: 2008.04.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010.11.23	沥柳龋				@pInvenQslotInfo 荤捞令甫 8000 栏肺 函版
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspSetPcInvenQSlotInfo]
	 @pPcNo					INT
	,@pInvenQslotInfo		BINARY(8000)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   

	UPDATE dbo.TblPcInvenQSlotInfo 
	SET 
		mPcNo = @pPcNo, 
		mInfo = @pInvenQslotInfo,
		mUpdateDate = GETDATE()
	WHERE mPcNo = @pPcNo

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT 

	IF @aErrNo <> 0
		RETURN(1)		-- sql internal error

	IF @aRowCnt = 0 
	BEGIN

		INSERT INTO dbo.TblPcInvenQSlotInfo([mPcNo], [mInfo])
		VALUES(
			@pPcNo,  
			@pInvenQslotInfo) 
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(2)	-- DB Err
		END	

	END 

	RETURN(0)

GO

