CREATE PROCEDURE [dbo].[UspChkPc]
	@pPcNm	CHAR(12)
	, @pPcNo	INT		OUTPUT
	, @pUserNo	INT		OUTPUT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	
	SELECT 
		@pPcNo = mNo
		, @pUserNo = mOwner
	FROM dbo.TblPc
	WHERE mNm = @pPcNm
		AND mDelDate IS NULL
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo <> 0 
		RETURN(1)
		
	IF @aRowCnt <> 1
		RETURN(1)	

    RETURN(0)

GO

