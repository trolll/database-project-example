CREATE Procedure [dbo].[UspResetEquip]
	 @pPcNo			INT		-- 1捞搁 阁胶磐促. 八荤侩烙.
	,@pSlot			INT			-- CPcEquip::ePosMax.
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	DELETE dbo.TblPcEquip 
	WHERE ([mOwner]=@pPcNo) 
		AND ([mSlot]=@pSlot)
	IF(0 <> @@ERROR)
	BEGIN
		SET	@aErrNo	= 1
		GOTO LABEL_END		 
	END	
			
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

