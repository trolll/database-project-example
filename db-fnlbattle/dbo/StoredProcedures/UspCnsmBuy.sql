/******************************************************************************  
**  Name: UspCnsmBuy  
**  Desc: 困殴 酒捞袍 备概
**  
**                
**  Return values:  
**   0 : 累诀 贸府 己傍  
**   > 0 : SQL Error  
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmBuy]
 @pCnsmSn		BIGINT	-- 困殴 矫府倔 锅龋
 ,@pItemNo		INT		-- 酒捞袍 锅龋(老摹窍绰瘤 犬牢秦具 凳)
 ,@pIsStack		BIT		-- 胶琶屈牢瘤 咯何(1捞搁 胶琶屈)
 ,@pBuyCnt		INT	-- 备概 俺荐
 ,@pBuyerPcNo	INT	-- 备概磊
 ,@pFeePercent	TINYINT-- 荐荐丰啦 欺季飘蔼(1 捞惑 100 固父)
 ,@pMoneySn		BIGINT -- 备概磊 捣 Sn
 ,@pItemSerial	BIGINT OUTPUT	-- 备概等 酒捞袍Sn, 扁粮 酒捞袍俊 捍钦登菌促搁 措惑Sn
 ,@pResultCnt	INT OUTPUT		-- 备概等 俺荐 or 捍钦登菌促搁 弥辆 俺荐
 ,@pCategory	TINYINT OUTPUT -- 墨抛绊府
 ,@pRegPcNo	INT OUTPUT -- 困殴 酒捞袍 殿废茄 PC锅龋
 ,@pRegPcNm	CHAR(12) OUTPUT    -- 困殴 酒捞袍 殿废茄 PC捞抚
 ,@pResultMoneyCnt INT OUTPUT -- 瘤阂窍绊 巢篮 捣(@pMoneySn)
 ,@pSysChargedCnt INT OUTPUT -- 矫胶袍捞 啊廉埃 捣
 ,@pPrice		INT OUTPUT -- 俺寸 魄概 啊拜
 ,@pStatus	TINYINT OUTPUT -- 芭贰等 酒捞袍狼 惑怕
AS  
	SET NOCOUNT ON   ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   ;

	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;

	DECLARE @aPcNo					INT;
	DECLARE @aCurCnt				INT;
	DECLARE @aPrice					INT	;
	DECLARE @aRegDate			DATETIME;
	DECLARE @aSerialNo			BIGINT	;
	DECLARE @aItemNo				INT	;		
	DECLARE @aEndDate			DATETIME;
	DECLARE @aIsConfirm			BIT	;
	DECLARE @aStatus				TINYINT;
	DECLARE @aCntUse				TINYINT;
	DECLARE @aOwner				INT;
	DECLARE @aPracticalPeriod	INT;
	DECLARE @aBindingType		TINYINT;
	DECLARE @aRestoreCnt			TINYINT;

	SELECT @aRowCnt = 0, @aErrNo= 0  ;

	-- 困殴 酒捞袍阑 掘绢 柯促
	SELECT   
		@aPcNo	= [mPcNo],
		@aCurCnt	= [mCurCnt],
		@aPrice		= [mPrice],
		@pCategory	= [mCategoryNo],
		@aItemNo	= mItemNo
	FROM dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmSn AND GETDATE() < mTradeEndDate;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN 
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 1;  -- 困殴 酒捞袍捞 粮犁窍瘤 臼嚼聪促.(TblConsignment)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 郴何 坷幅
		END
		GOTO T_END;   
	END  


	SET @pRegPcNo = @aPcNo;
	SET @pPrice = @aPrice;

	-- 酒捞袍 锅龋甫 眉农窍绰 巴篮 @pIsStack狼 蜡瓤己阑 眉农窍绰 扁瓷阑 茄促.
	IF @aItemNo <> @pItemNo
	BEGIN
		SET @aErrNo = 2; -- 酒捞袍ID啊 老摹窍瘤 臼嚼聪促.
		GOTO T_END   ;
	END	

	-- 夯牢捞 棵赴 酒捞袍阑 备概且 荐绰 绝促
	IF @aPcNo = @pBuyerPcNo
	BEGIN
		SET @aErrNo = 3; -- 磊脚捞 棵赴 酒捞袍阑 备概且 荐 绝嚼聪促.
		GOTO T_END   ;
	END

	-- 夸没茄 俺荐焊促 何练窍促(厘厚狼 版快 @pBuyCnt啊 亲惑 1捞绢具 等促)
	IF (@aCurCnt - @pBuyCnt) < 0
	BEGIN
		SET @aErrNo = 4; -- 巢篮 酒捞袍 俺荐啊 何练钦聪促.
		GOTO T_END ;  
	END


	-- 备概磊 捣 眉农
	declare @aCurMoneyCnt INT;
	SELECT @aCurMoneyCnt=mCnt
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pMoneySn AND mIsSeizure = 0;
	IF @aCurMoneyCnt IS NULL OR CAST(@aCurMoneyCnt AS BIGINT) < (CAST(@aPrice AS BIGINT) * @pBuyCnt)
	BEGIN
		SET @aErrNo = 17 ;-- 捣捞 何练钦聪促.
		GOTO T_END  ; 
	END


	-- 魄概磊 捞抚 啊廉坷扁
	SELECT @pRegPcNm = mNm
	FROM dbo.TblPc
	WHERE mNo = @aPcNo

	IF @pRegPcNm IS NULL
	BEGIN
		SET @aErrNo = 100;  -- DB 郴何 坷幅
		GOTO T_END;
	END


	SELECT
		@aRegDate			= mRegDate,
		@aSerialNo			= mSerialNo,		
		@aEndDate			= mEndDate,
		@aIsConfirm			= mIsConfirm,
		@aStatus			= mStatus,
		@aCntUse			= mCntUse,
		@aOwner				= mOwner,
		@aPracticalPeriod	= mPracticalPeriod,
		@aBindingType		= mBindingType,
		@aRestoreCnt		= mRestoreCnt
	FROM dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmSn;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT;  
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 5  ;-- 困殴 酒捞袍捞 粮犁窍瘤 臼嚼聪促.(TblConsignmentItem)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 郴何 坷幅
		END
		GOTO T_END  ; 
	END 
	
	SET @pStatus = @aStatus;
		
	DECLARE @aCntTarget  INT  ;
	SET @aCntTarget = 0  ;

	-- 胶琶屈捞搁 备概磊俊霸 胶琶矫懦 措惑捞 乐绰瘤 眉农
	IF @pIsStack <> 0
	BEGIN
		DECLARE @aIsSeizure  BIT;  
		SET @aIsSeizure = 0;
		SELECT   
			@aCntTarget=ISNULL([mCnt],0),   
			@pItemSerial=[mSerialNo],   
			@aIsSeizure=ISNULL([mIsSeizure],0)  
		FROM dbo.TblPcInventory
		WHERE mPcNo =  @pBuyerPcNo
		AND mItemNo = @aItemNo   
		AND mIsConfirm = @aIsConfirm   
		AND mStatus =@aStatus   
		AND mOwner = @aOwner    
		AND mPracticalPeriod = @aPracticalPeriod  
		AND mBindingType = @aBindingType  
		AND (mCnt + @pBuyCnt) <= 1000000000;
		  
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT;  
		IF @aErrNo<> 0  -- @aRowCnt捞 0捞搁 胶琶 措惑捞 绝绰 巴捞促.
		BEGIN  
			SET @aErrNo = 100;  -- DB 郴何 坷幅
			GOTO T_END ;     
		END

		-- 胶琶矫虐妨绰 措惑捞 拘幅登绢 乐栏搁...
		-- 弊成 矫府倔 货肺 父甸绢辑 备概啊 啊瓷窍档废 且 荐档 乐变 茄单...
		IF @aCntTarget <> 0 AND @aIsSeizure <> 0  
		BEGIN  
			SET @aErrNo = 6 ;-- 酒捞袍捞 拘幅登绢 乐嚼聪促.
			GOTO T_END   ;   
		END
	END


	BEGIN TRAN;	

	-- 酒捞袍 昏力 OR 箭磊 皑家
	SET @aCurCnt = 0;
	IF @pIsStack <> 0 -- 胶琶屈牢 版快
	BEGIN
		UPDATE dbo.TblConsignment
		SET @aCurCnt = mCurCnt = mCurCnt - @pBuyCnt
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 7 ;-- 酒捞袍 皑家 角菩(TblConsignment)
			GOTO T_END  ; 
		END
	END

	-- 胶琶屈捞 酒聪菌芭唱 胶琶屈阑 促 瘤奎栏搁 酒捞袍 昏力
	IF @aCurCnt = 0
	BEGIN
		DELETE dbo.TblConsignmentItem
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 8; -- 酒捞袍 昏力 角菩(TblConsignmentItem)
			GOTO T_END  ; 
		END

		DELETE dbo.TblConsignment
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 9; -- 酒捞袍 昏力 角菩(TblConsignment)
			GOTO T_END  ; 
		END
	END
	
	-- 备概磊 捣 皑家, 捣阑 酒抗 昏力沁阑 版快 昏力 寸矫狼 俺荐啊 檬扁 俺荐客 促福搁 救等促.
	IF @aCurMoneyCnt = (@aPrice * @pBuyCnt)
	BEGIN
		SET @pResultMoneyCnt = 0; -- 巢篮 角滚 俺荐 OUTPUT
		DELETE dbo.TblPcInventory  
		WHERE mSerialNo = @pMoneySn and mCnt = @aCurMoneyCnt AND mIsSeizure = 0;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 10 ;-- 备概磊捣 皑家 角菩
			GOTO T_END   ;
		END
	END
	ELSE
	BEGIN
		UPDATE dbo.TblPcInventory
		SET @pResultMoneyCnt = mCnt = mCnt - (@aPrice * @pBuyCnt)
		WHERE mSerialNo = @pMoneySn AND mIsSeizure = 0;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 10 ;-- 备概磊捣 皑家 角菩
			GOTO T_END ;  
		END
	END

	
	IF @pIsStack = 0 -- 胶琶屈捞 酒囱 版快
	BEGIN		
		SET @pItemSerial = @aSerialNo;
		SET @pResultCnt = 1;
		-- 备概磊俊霸 酒捞袍 眠啊
		INSERT dbo.TblPcInventory(
			[mSerialNo],  
			[mPcNo],   
			[mItemNo],   
			[mEndDate],   
			[mIsConfirm],   
			[mStatus],   
			[mCnt],   
			[mCntUse],  
			[mOwner],  
			[mPracticalPeriod], 
			mBindingType )  
		VALUES(  
			@pItemSerial,  
			@pBuyerPcNo,   
			@aItemNo,   
			@aEndDate,   
			@aIsConfirm,   
			@aStatus,   
			@pBuyCnt,   
			@aCntUse,  
			@aOwner,  
			@aPracticalPeriod, 
			@aBindingType) ;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 11 ;-- 备概磊俊霸 酒捞袍 眠啊 角菩
			GOTO T_END   ;
		END
	END
	ELSE -- 胶琶屈牢 版快
	BEGIN
		IF @aCntTarget = 0 -- 穿利矫懦 措惑捞 绝绰 版快
		BEGIN
			EXEC @pItemSerial =  dbo.UspGetItemSerial;  -- 矫府倔 掘扁   
			IF @pItemSerial <= 0  
			BEGIN  
				ROLLBACK TRAN  ;
				SET @aErrNo = 12 ;-- 酒捞袍 矫府倔 积己 角菩
				GOTO T_END ;   
			END
			SET @pResultCnt = @pBuyCnt;
			-- 备概磊俊霸 酒捞袍 眠啊
			INSERT dbo.TblPcInventory(
				[mSerialNo],  
				[mPcNo],   
				[mItemNo],   
				[mEndDate],   
				[mIsConfirm],   
				[mStatus],   
				[mCnt],   
				[mCntUse],  
				[mOwner],  
				[mPracticalPeriod], 
				mBindingType )  
			VALUES(  
				@pItemSerial,  
				@pBuyerPcNo,   
				@aItemNo,   
				@aEndDate,   
				@aIsConfirm,   
				@aStatus,   
				@pBuyCnt,   
				@aCntUse,  
				@aOwner,  
				@aPracticalPeriod, 
				@aBindingType) ;
			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
			IF @aErrNo<> 0 OR @aRowCnt <= 0   
			BEGIN  
				ROLLBACK TRAN;
				SET @aErrNo = 13 ;-- 备概磊俊霸 酒捞袍 眠啊 角菩
				GOTO T_END;   
			END
		END
		ELSE -- 穿利矫懦 措惑捞 乐绰 版快
		BEGIN
			UPDATE dbo.TblPcInventory
			SET @pResultCnt = mCnt = mCnt + @pBuyCnt
			WHERE mSerialNo = @pItemSerial;
			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
			IF @aErrNo<> 0 OR @aRowCnt <= 0   
			BEGIN  
				ROLLBACK TRAN;
				SET @aErrNo = 14 ;-- 酒捞袍 穿利 角菩(俊矾)
				GOTO T_END ;  
			END
			IF 1000000000 < @pResultCnt
			BEGIN
				ROLLBACK TRAN;
				SET @aErrNo = 15; -- 酒捞袍 穿利 角菩(俺荐 檬苞)
				GOTO T_END;
			END
		END
	END

	-- 魄概磊 拌谅俊 荐荐丰甫 力寇茄 魄概 措陛 眠啊

	DECLARE @aBalance BIGINT;
	SELECT @aBalance=mBalance
	FROM dbo.TblConsignmentAccount
	WHERE mPcNo=@aPcNo;
	IF @aBalance IS NULL
	BEGIN
		INSERT dbo.TblConsignmentAccount
		VALUES(@aPcNo, 0);
	END

	SET @pSysChargedCnt = @pBuyCnt*( CAST( (@aPrice*(@pFeePercent/100.0)) AS INT) );

	UPDATE dbo.TblConsignmentAccount
	SET mBalance = mBalance + ((@pBuyCnt*@aPrice) - @pSysChargedCnt)
	WHERE mPcNo = @aPcNo;
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 16 ;-- 魄概 措陛 眠啊 角菩
		GOTO T_END  ; 
	END


T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN ; 

T_END:     
	SET NOCOUNT OFF   ;
	RETURN(@aErrNo) ;

GO

