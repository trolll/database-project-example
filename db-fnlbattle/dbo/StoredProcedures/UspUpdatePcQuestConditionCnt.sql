/******************************************************************************
**		Name: UspUpdatePcQuestConditionCnt
**		Desc: QA狼 祈狼己阑 困秦 阁胶磐 懦 墨款飘甫 函版茄促.
**
**		Auth: 沥柳龋
**		Date: 2011-01-05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePcQuestConditionCnt]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pMonNo   		INT
	,@pCnt   		INT
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    UPDATE dbo.TblPcQuestCondition 
	SET mCnt = 
		CASE
			WHEN 0 < (mCnt + @pCnt) THEN mCnt + @pCnt
			ELSE 0
		END
	WHERE mPcNo = @pPcNo 
			AND mQuestNo = @pQuestNo
            AND mMonsterNo = @pMonNo
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(2)
	END
	 		 
	RETURN(0)

GO

