/******************************************************************************
**		Name: WspListItemByPc
**		Desc: 墨坷胶 硅撇 某腐磐 家蜡 酒捞袍 炼雀
**
**		Auth: 辫籍玫
**		Date: 20090428
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspListItemByPc]
	@mCharNm VARCHAR(12),
	@mFieldSvrNo SMALLINT 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	SET ROWCOUNT 1000	-- 弥措 啊廉棵 荐 乐绰 酒捞袍 荐甫 力茄 

	-------------------------------------------------------
	-- 酒捞袍 沥焊甫 馆券
	-------------------------------------------------------
	SELECT 
		 T1.mNm
		,(
			SELECT 
				mSlot
			FROM dbo.TblPcEquip
			WHERE mSerialNo = T1.mSerialNo		
		 ) AS mSlot
		,T2.IName		
		,T1.mEndDate
		,T1.mSerialNo
		,T1.mCnt
		,T1.mItemNo
		,T1.mIsSeizure
		,T1.mRegDate
		,T1.mStatus
		,T1.mPracticalPeriod
		,T1.mBindingType
		,T1.mRestoreCnt
	FROM (
		SELECT 
			 T1.mNm
			,T1.mNo
			,T2.mStatus
			,DATEDIFF(DAY, GETDATE(), T2.mEndDate) AS mEndDate	-- 蜡瓤扁埃
			,T2.mSerialNo
			,T2.mCnt
			,T2.mItemNo
			,T2.mIsSeizure
			,T2.mRegDate	-- 嚼垫老磊
			,T2.mPracticalPeriod
			,T2.mBindingType
			,T2.mRestoreCnt
		FROM dbo.TblChaosBattlePc T1
			INNER JOIN [dbo].[TblPcInventory]  T2 
			ON T1.mNo = T2.mPcNo
				--AND T1.mDelDate IS NULL						
		WHERE T1.mNm = @mCharNm
			AND T1.mFieldSvrNo = @mFieldSvrNo
	) T1	
		INNER JOIN FNLParm.dbo.DT_Item AS T2
			ON T1.mItemNo = T2.IID
	ORDER BY T1.mNo ASC

GO

