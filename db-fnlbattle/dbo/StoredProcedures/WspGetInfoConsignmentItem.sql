CREATE PROCEDURE [dbo].[WspGetInfoConsignmentItem]
	@mCnsmNo	BIGINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		mPcNo,
		mItemNo,
		mIsConfirm,
		mStatus,
		mCntUse,
		mEndDate,
		mBindingType
	FROM dbo.TblConsignmentItem T1
		INNER JOIN dbo.TblConsignment T2 
			ON T1.mCnsmNo = T2.mCnsmNo
	WHERE T1.mCnsmNo = @mCnsmNo

GO

