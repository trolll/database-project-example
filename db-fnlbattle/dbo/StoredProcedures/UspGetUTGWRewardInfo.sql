/******************************************************************************  
**  Name: UspGetUTGWRewardInfo  
**  Desc: 烹钦辨靛傈 辑滚焊惑 沥焊甫 肺靛茄促.
**  
**  Auth: 沥柳宽  
**  Date: 2013-05-06  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  
**  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetUTGWRewardInfo]
	@pChaosBattleSvr	SMALLINT
	, @pRound			INT	OUTPUT
AS  
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SET @pRound = 0;
	
	SELECT
		TOP 1
		@pRound	= MAX([mRound])
	FROM
		dbo.TblUnitedGuildWarRewardInfo
	WHERE
		[mChaosBattleSvrNo] = @pChaosBattleSvr
	GROUP BY [mRound]
	ORDER BY [mRound] DESC
	
	IF(0 <> @@ROWCOUNT)
	BEGIN  
		SELECT
			[mRewardSvrNo]  
			, [mAdvantage]				
		FROM
			dbo.TblUnitedGuildWarRewardInfo
		WHERE
			[mRound] = @pRound
			AND [mChaosBattleSvrNo] = @pChaosBattleSvr
			AND mIsValid <> 0
	END

GO

