/******************************************************************************
**		Name: UspUpdatePcPopupGuide
**		Desc: TblPcPopupGuide 甫 诀单捞飘 窍绰 Procedure
**
**		Auth: 辫锐档
**		Date: 2009-09-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePcPopupGuide]
	@pPcNo				INT,
	@pGuideNo			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		
	DECLARE @aErrNo		INT,
			@aRowCount	INT,
			@aReadCnt	INT
			
	SELECT	@aErrNo		= 0, @aReadCnt = 0, @aRowCount = 0;
	
	UPDATE dbo.TblPcPopupGuide
	SET [mReadCnt] = [mReadCnt]+1
	WHERE ([mPcNo]=@pPcNo)
		AND ([mGuideNo]=@pGuideNo)	
	
	SELECT	@aErrNo	= @@ERROR, @aRowCount = @@ROWCOUNT;	

	IF @aErrNo <> 0
		RETURN(1)	-- DB ERROR

	IF ( @aRowCount = 0 )		
	BEGIN
	
		INSERT INTO dbo.TblPcPopupGuide([mPcNo], [mGuideNo], [mReadCnt])
		VALUES(@pPcNo, @pGuideNo, 1)		
		
		SELECT	@aErrNo	= @@ERROR, @aRowCount = @@ROWCOUNT;	
		IF @aErrNo <> 0
			RETURN(1)	-- DB ERROR
			
	END 
	
	RETURN(0)

GO

