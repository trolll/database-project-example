/******************************************************************************
**		Name: UspGetLimitedResourceItem
**		Desc: 何劝 包访 沥焊甫 啊廉柯促
**
**		Auth: 炼 技泅
**		Date: 2009-07-27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetLimitedResourceItem]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mResourceType, mIID, mIsComsume
	FROM dbo.TblLimitedResourceItem

GO

