/******************************************************************************  
**  File: 
**  Name: UspAddPcSkillPack  
**  Desc: PC狼 胶懦蒲 窍唱 眠啊
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspAddPcSkillPack]
	@pPcNo		INT,
	@pSPID		INT,
	@pValidDay	INT,
	@pLeftTick INT OUTPUT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, 滚妨柳 酒捞袍

	-- 捞固 乐促搁 父丰 朝楼父 技泼窍绊 眠啊 己傍栏肺 埃林茄促
	declare @aSPID INT
	select @aSPID=mSPID from TblPcSkillPackInventory
	where mPcNo=@pPcNo and mSPID = @pSPID

	DECLARE	 @aEndDay		SMALLDATETIME
	SELECT	 @aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)

	if @aSPID is not NULL
	begin
		update TblPcSkillPackInventory
		set mEndDate=@aEndDay
		where mPcNo=@pPcNo and mSPID = @pSPID
		if @@error <> 0
		begin
			return 1 -- DB 俊矾
		end
		return 0
	end
	
	insert into TblPcSkillPackInventory
	values(default, @pPcNo, @pSPID, @aEndDay)
	if @@error <> 0
	begin
		return 1 -- DB 俊矾
	end

	SET @pLeftTick = 	DATEDIFF(mi, GETDATE(), @aEndDay)

	return 0

GO

