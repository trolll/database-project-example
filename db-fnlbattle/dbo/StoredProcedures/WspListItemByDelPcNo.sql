CREATE  PROCEDURE [dbo].[WspListItemByDelPcNo]
    @pPcNo	INT
    ,@pPcNm	CHAR(12)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	SET ROWCOUNT 1000		-- 弥措 啊廉棵 荐 乐绰 酒捞袍 荐甫 力茄 

    SELECT 
        @pPcNm AS mNm
        ,(
            SELECT 
               mSlot
            FROM dbo.[TblPcEquip]
            WHERE mSerialNo = T1.mSerialNo                 
        ) AS mSlot
        ,T2.IName                         
        ,T1.mEndDate
        ,T1.mSerialNo
        ,T1.mCnt
        ,T1.mItemNo
        ,T1.mIsSeizure
        ,T1.mRegDate
        ,T1.mStatus
        ,T1.mPracticalPeriod
        ,T1.mBindingType
    FROM (
        SELECT 
            T1.mNm
            ,T1.mNo
            ,T2.mStatus
            ,DATEDIFF(DAY, GETDATE(), T2.mEndDate) AS mEndDate         -- 蜡瓤扁埃
            ,T2.mSerialNo
            ,T2.mCnt
            ,T2.mItemNo
            ,T2.mIsSeizure
            ,T2.mRegDate       -- 嚼垫老磊
            ,T2.mPracticalPeriod
            ,T2.mBindingType
        FROM [dbo].[TblPc] T1 
           INNER JOIN [dbo].[TblPcInventory]  T2 
              ON T1.mNo = T2.mPcNo
                AND T1.mDelDate IS NOT NULL           
        WHERE T1.mNo = @pPcNo
) T1        
    INNER JOIN FNLParm.[dbo].[DT_Item] AS T2
                ON T1.mItemNo = T2.IID
    ORDER BY T1.mNo ASC

GO

