/******************************************************************************
**		Name: WspUpdateItemRestoreCnt
**		Desc: 酒捞袍 蜡瓤扁埃 雀汗冉荐甫 荐沥茄促
**
**		Auth: 辫籍玫
**		Date: 2009-04-13
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                荐沥老           荐沥磊                             荐沥郴侩    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspUpdateItemRestoreCnt]
	@mSerialNo	BIGINT, -- 矫府倔
	@mRestoreCnt TINYINT,	-- 拘幅咯何
	@mPos		CHAR(1)		-- 酒捞袍 困摹(i:牢亥, s:芒绊, g:辨靛芒绊)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @mPos = 'i' 
	BEGIN
		UPDATE dbo.TblPcInventory
		SET
			mRestoreCnt = @mRestoreCnt
		WHERE mSerialNo = @mSerialNo
	END
	
	IF @mPos = 's' 
	BEGIN
		UPDATE dbo.TblPcStore
		SET
			mRestoreCnt = @mRestoreCnt
		WHERE mSerialNo = @mSerialNo	
	END 
	
	IF @mPos = 'g' 
	BEGIN
		UPDATE dbo.TblGuildStore
		SET
			mRestoreCnt = @mRestoreCnt
		WHERE mSerialNo = @mSerialNo	
	END 	

	IF @@ERROR <> 0
		RETURN 1
	
	RETURN 0

GO

