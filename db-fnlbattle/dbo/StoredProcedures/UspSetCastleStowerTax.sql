CREATE PROCEDURE [dbo].[UspSetCastleStowerTax]
	 @pPlace		INT
	,@pTaxBuy		INT
	,@pTaxHunt		INT
	,@pTaxGamble	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	UPDATE dbo.TblCastleTowerStone 
	SET 
		[mTaxBuy]=@pTaxBuy, 
		[mTaxHunt]=@pTaxHunt, 
		[mTaxGamble]=@pTaxGamble
	 WHERE ([mPlace]=@pPlace)
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo	= 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

