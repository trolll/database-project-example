/******************************************************************************  
**  Name: UspCnsmListAllValid  
**  Desc: 扁埃 父丰 救等 巴甸 傈何 府胶泼
**			辑滚 檬扁 load矫 角青登绰 SP
**                
**                
**  Author: 辫碍龋  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmListAllValid]
AS  
	 SET NOCOUNT ON;  
	 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	declare @aCurDate smalldatetime;
	SET @aCurDate = getdate();

	select a.mCnsmNo, a.mCategoryNo, a.mPrice, 
			CASE WHEN a.mTradeEndDate <= @aCurDate
			THEN 0 
			ELSE DATEDIFF(minute, getdate(), a.mTradeEndDate) END, 
			a.mPcNo, a.mRegCnt, a.mCurCnt, a.mItemNo,
			b.mStatus, b.mCntUse, b.mOwner, b.mPracticalPeriod
	from dbo.TblConsignment a 
		inner join dbo.TblConsignmentItem b 
			on a.mCnsmNo = b.mCnsmNo
	where @aCurDate < a.mTradeEndDate;

GO

