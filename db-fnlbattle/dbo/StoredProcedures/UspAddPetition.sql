CREATE PROCEDURE [dbo].[UspAddPetition]
	@pPcNo		INT,		-- PC锅龋
	@pCategory		INT,		-- 墨抛绊府 (盒幅)
	@pPosX		FLOAT,		-- X谅钎
	@pPosY		FLOAT,		-- Y谅钎
	@pPosZ		FLOAT,		-- Z谅钎
	@pText			CHAR(1000),	-- 柳沥郴侩
	@pIpAddress		CHAR(20)	-- IP林家
--WITH ENCRYPTION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	BEGIN TRAN

	-- 柳沥辑 火涝
	INSERT dbo.TblPetitionBoard (mCategory, mRegDate, mPcNo, mPosX, mPosY, mPosZ, mText, mIpAddress, mIsFin, mFinDate) 
	VALUES(@pCategory, GetDate(), @pPcNo, @pPosX, @pPosY, @pPosZ, @pText, @pIpAddress, DEFAULT, DEFAULT)

	-- 火涝茄 柳沥辑 锅龋甫 掘绢咳
	IF EXISTS(SELECT TOP 1 mPID FROM dbo.TblPetitionBoard WHERE mPcNo = @pPcNo AND mCategory = @pCategory ORDER BY mPID DESC)
	BEGIN
		DECLARE	@aAddPID	BIGINT
		SELECT TOP 1 @aAddPID = mPID FROM dbo.TblPetitionBoard WHERE mPcNo = @pPcNo AND mCategory = @pCategory ORDER BY mPID DESC

		-- 捞傈 柳沥辑 单捞磐 昏力客 悼矫俊 脚痹 柳沥辑 惑怕 火涝/盎脚
		IF EXISTS(SELECT mPID FROM dbo.TblPetitionCheckState WHERE mPcNo = @pPcNo AND mCategory = @pCategory)
		BEGIN
			-- 捞傈 柳沥辑 惑怕 单捞磐 粮犁 -> 柳沥辑 府胶飘 抛捞喉狼 秒家且 单捞磐 昏力 饶 惑怕 盎脚
			DECLARE	@aDelPID	BIGINT
			SELECT @aDelPID = mPID FROM dbo.TblPetitionCheckState WHERE mPcNo = @pPcNo AND mCategory = @pCategory
			-- 肯丰等 扒捞扼搁 昏力窍搁 臼登绊 穿利矫难具 窃
			DELETE dbo.TblPetitionBoard WHERE mPID = @aDelPID AND mIsFin = 0
			UPDATE dbo.TblPetitionCheckState SET mPID = @aAddPID WHERE mPcNo = @pPcNo AND mCategory = @pCategory
		END
		ELSE
		BEGIN
			-- 捞傈 柳沥辑 惑怕 单捞磐 绝澜 -> 脚痹 惑怕 单捞磐 火涝
			INSERT dbo.TblPetitionCheckState VALUES (@pPcNo, @pCategory, @aAddPID)
		END
	END
	ELSE
	BEGIN
		SET	@aErrNo = 2	-- 单捞磐 火涝 坷幅
	END

	IF @@ERROR = 0
	BEGIN
		COMMIT TRAN
	END
	ELSE
	BEGIN
		SET	@aErrNo = 1	-- 矫胶袍 坷幅
		ROLLBACK TRAN
	END

	SET NOCOUNT OFF
	RETURN(@aErrNo)
END

GO

