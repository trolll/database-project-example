CREATE PROCEDURE [dbo].[UspGetDiscipleHistoryCount]
	 @pMaster		INT
	,@pHistoryCount	INT OUTPUT
AS
	SET NOCOUNT ON	

	SET @pHistoryCount = 0

	SELECT @pHistoryCount = COUNT(mMaster)
	FROM dbo.TblDiscipleHistory WITH(NOLOCK)
	WHERE mMaster = @pMaster

	SET NOCOUNT OFF

GO

