CREATE PROCEDURE [dbo].[UspSetEquip]
	 @pPcNo			INT			-- 1捞搁 阁胶磐促. 八荤侩烙.
	,@pSerial		BIGINT
	,@pSlot			INT			-- CPcEquip::ePosMax.
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	IF NOT EXISTS(	SELECT * 
					FROM dbo.TblPcInventory WITH(INDEX=PK_NC_TblPcInventory_1)
					WHERE 	mSerialNo = @pSerial
							AND mPcNo = @pPcNo )
	BEGIN
		RETURN(1)
	END

	BEGIN TRAN

		--------------------------------------------------
		-- 捞固 粮犁窍绰 slot捞搁 昏力
		--------------------------------------------------
		DELETE dbo.TblPcEquip 
		WHERE mOwner = @pPcNo
				AND mSlot = @pSlot

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(2)
		END
		
		--------------------------------------------------
		-- 厘馒 酒捞袍 汲沥 
		--------------------------------------------------
		INSERT dbo.TblPcEquip(mOwner, mSlot, mSerialNo) 
		VALUES(
			@pPcNo, 
			@pSlot,
			@pSerial
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(2)
		END

	COMMIT TRAN
	RETURN(0)

GO

