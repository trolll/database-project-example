/******************************************************************************  
**  Name: UspDelUTGWRewardInfo  
**  Desc: 烹钦辨靛傈 辑滚焊惑 沥焊甫 府悸茄促.
**  
**  Auth: 沥柳宽  
**  Date: 2013-05-13  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  
**  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDelUTGWRewardInfo]
	@pRound					INT
	, @pRewardSvrNo			SMALLINT
	, @pChaosBattleSvrNo	SMALLINT
AS
	SET NOCOUNT ON  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
   
	IF NOT EXISTS 
	(
		SELECT
			[mRewardSvrNo] 
		FROM
			dbo.TblUnitedGuildWarRewardInfo
		WHERE
			[mRound] = @pRound
			AND [mRewardSvrNo] = @pRewardSvrNo
			AND [mChaosBattleSvrNo] = @pChaosBattleSvrNo
	)
	BEGIN
		RETURN 1
	END

	UPDATE 
		dbo.TblUnitedGuildWarRewardInfo
	SET	
		[mIsValid] = 0 
	WHERE 
		[mRound] = @pRound
		AND [mRewardSvrNo] = @pRewardSvrNo
		AND [mChaosBattleSvrNo] = @pChaosBattleSvrNo
	
	IF @@ERROR <> 0
	BEGIN
		RETURN 2
	END	
		
	RETURN(0)

GO

