CREATE PROCEDURE [dbo].[UspListBoardTop_T]
	@pBoardId		TINYINT
	,@pCnt			INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE @aSql	NVARCHAR(512)
	DECLARE @aWhere	NVARCHAR(100)

	SET @aWhere 	= N' @aBoardId		TINYINT'	
	SET @aSql 		= N' SELECT TOP ' + CONVERT(NVARCHAR, @pCnt) +  CHAR(10) +
					     ' 	[mRegDate], [mBoardNo], RTRIM([mFromPcNm]), RTRIM([mTitle]), RTRIM([mMsg]) '  + CHAR(10) +
					      'FROM dbo.TblBoard WITH(NOLOCK) '  + CHAR(10) +
					    ' WHERE [mBoardId] = @aBoardId ' + CHAR(10) +
					    ' ORDER BY [mBoardNo] DESC'				

	EXEC sp_executesql	@aSql, @aWhere, @aBoardId = @pBoardId

GO

