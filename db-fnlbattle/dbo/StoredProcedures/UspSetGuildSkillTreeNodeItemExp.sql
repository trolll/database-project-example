/******************************************************************************  
**  File: 
**  Name: UspSetGuildSkillTreeNodeItemExp
**  Desc: 辨靛 胶懦飘府畴靛酒捞袍狼 版氰摹甫 函版茄促.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetGuildSkillTreeNodeItemExp]
	@pGuildNo		INT,
	@pSTNIID		INT,
	@pExp			BIGINT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	update TblGuildSkillTreeInventory
	set mExp = @pExp
	where mGuildNo=@pGuildNo and mSTNIID = @pSTNIID
	
	if @@error <> 0
		begin
			set @aErrNo=1
		end

	RETURN @aErrNo

GO

