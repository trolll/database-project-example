CREATE PROCEDURE [dbo].[UspResetPcLostExp]
	  @pPcNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			

	-- PcNo 绰 2锅 何磐 矫累茄促.
	IF(@pPcNo < 2)
	BEGIN
		RETURN(1)
	END

	IF EXISTS(	SELECT * 
				FROM dbo.TblPc 
				WHERE mNo = @pPcNo 
					AND mDelDate IS NULL )
	BEGIN
	
		UPDATE dbo.TblPcState 
		SET mLostExp = 0 
		WHERE mNo = @pPcNo

		IF(@@ERROR <> 0)
		BEGIN
			RETURN(2)	-- DB Error
		END
		
		RETURN(0)		
 	END

	RETURN(3)

GO

