/******************************************************************************
**		Name: UspDeletePcQuestExpireEvent
**		Desc: 辆丰等 捞亥飘 涅胶飘甫 昏力
**
**		Auth: 沥柳龋
**		Date: 2010-06-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeletePcQuestExpireEvent]
	@pEventQuestNoStr			VARCHAR(3000)
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;

	DECLARE @aQuest			TABLE (
				mQuestNo		INT	 PRIMARY KEY      	)			


	-- 单捞磐啊 绝阑版快绰?
	INSERT INTO @aQuest(mQuestNo)
	SELECT CONVERT(INT, element) mObjID
	FROM  dbo.fn_SplitTSQL(  @pEventQuestNoStr, ',' )
	WHERE LEN(RTRIM(element)) > 0


	BEGIN TRAN

		DELETE T2
		FROM dbo.TblPcQuest T1
			INNER JOIN dbo.TblPcQuestCondition T2
				ON T1.mQuestNo = T2.mQuestNo
					AND T1.mPcNo = T2.mPcNo
		WHERE T1.mQuestNo IN ( 
				SELECT *
				FROM @aQuest
			)


		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		DELETE 	dbo.TblPcQuest
		WHERE mQuestNo IN ( 
			SELECT *
			FROM @aQuest
		) 


		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(2);			
		END 

	COMMIT TRAN

	RETURN(0)

GO

