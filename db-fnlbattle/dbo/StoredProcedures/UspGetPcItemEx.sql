CREATE PROCEDURE [dbo].[UspGetPcItemEx]
	 @pPcNo				INT,
	 @pItemNo1			INT,
	 @pItemNo2			INT,
	 @pItemNo3			INT, -- 眠啊等 何盒
	 @pItemSerialNo1	BIGINT OUTPUT,
	 @pItemSerialNo2	BIGINT OUTPUT,
	 @pItemSerialNo3	BIGINT OUTPUT -- 眠啊等 何盒
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 1
	
	SELECT @pItemSerialNo1 = 0,
			@pItemSerialNo2 = 0,
			@pItemSerialNo3 = 0 -- 眠啊等 何盒

	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, 滚妨柳 酒捞袍

	SELECT 
		@pItemSerialNo1 = mSerialNo
	FROM dbo.TblPcInventory
	WHERE mPcNo = @pPcNo
			AND mItemNo = @pItemNo1

	SELECT 
		@pItemSerialNo2 = mSerialNo
	FROM dbo.TblPcInventory
	WHERE mPcNo = @pPcNo
			AND mItemNo = @pItemNo2
			
	SELECT 
		@pItemSerialNo3 = mSerialNo
	FROM dbo.TblPcInventory
	WHERE mPcNo = @pPcNo
			AND mItemNo = @pItemNo3 -- 眠啊等 何盒

	RETURN(0)

GO

