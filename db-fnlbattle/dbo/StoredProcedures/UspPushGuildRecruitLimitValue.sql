/******************************************************************************
**		Name: UspPushGuildRecruitLimitValue
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩且 辨靛狼 啊涝弥家饭骇阑 力茄窍绰 沥焊甫 涝仿
**
**		Auth: 辫锐档, 沥柳龋
**		Date: 2009.07.09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPushGuildRecruitLimitValue]
	@pGuildNo			INT,
	@pClass				TINYINT,
	@pMinLevel			SMALLINT,
	@pMinChao			SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aErrNo		INT,
			@aRowCnt	INT
			
	SELECT	@aErrNo		= 0,
			@aRowCnt	= 0
	
	----------------------------------------------
	-- 辨靛 府捻福泼 沥焊 诀单捞飘
	----------------------------------------------
	UPDATE dbo.TblGuildRecruitLimitValue
	SET mMinLevel = @pMinLevel,
		mMinChao = @pMinChao
	WHERE [mGuildNo] = @pGuildNo
		AND [mClass] = @pClass

	SELECT  @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	IF(0 <> @aErrNo)
	BEGIN
		RETURN(1)
	END

	----------------------------------------------
	-- 辨靛 府捻福泼 沥焊 积己
	----------------------------------------------
	IF @aRowCnt = 0 
	BEGIN
		INSERT INTO dbo.TblGuildRecruitLimitValue( mGuildNo, mClass, mMinLevel, mMinChao)
		VALUES( @pGuildNo, @pClass, @pMinLevel, @pMinChao)
		
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		IF (@aErrNo <> 0) OR (@aRowCnt <= 0)
		BEGIN
			RETURN(1)
		END
	END
		
	RETURN(0)

GO

