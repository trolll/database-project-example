CREATE PROCEDURE [dbo].[UspDestroyGkill]
	 @pGuildNo	INT
	,@pPlace	INT		-- EPlace.
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	DELETE dbo.TblGkill 
	WHERE ([mGuildNo]=@pGuildNo) 
			AND ([mPlace]=@pPlace)

	-- 痢飞父 窍绊 窍唱档 build甫 救且 荐档 乐促.	 
	IF(@@ERROR <> 0) --OR (0 = @@ROWCOUNT))
	BEGIN
		SET @aErrNo = 1
	END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

