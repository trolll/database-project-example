CREATE PROCEDURE [dbo].[UspGetGuildGoldItemEffect]
	@pGuildNo	INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		  mItemType
		, mParmA
		, DATEDIFF(mi, GETDATE(), mEndDate)
		, mItemNo
	FROM dbo.TblGuildGoldItemEffect
	WHERE mGuildNo = @pGuildNo 
		AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())

GO

