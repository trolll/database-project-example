/******************************************************************************
**		Name: UspUpdateAbnormal
**		Desc: 
**
**		Auth:
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 2009-05-09		Author:	辫 辈挤
**		Description: INSERT 亲格 眠啊. (mRestoreCnt)
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateAbnormal]
	 @pPcNo			INT
	,@pAbnormalNo	INT	 
	,@pLeftTime		INT
	,@pAbParmNo		INT		-- 惑怕捞惑喊 啊函利牢 蔼.
	,@pRestoreCnt	TINYINT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	INSERT INTO dbo.TblPcAbnormal([mPcNo], [mParmNo], [mLeftTime], [mAbParmNo], [mRestoreCnt])
		VALUES(@pPcNo, @pAbnormalNo, @pLeftTime, @pAbParmNo, @pRestoreCnt)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END
	 END

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

