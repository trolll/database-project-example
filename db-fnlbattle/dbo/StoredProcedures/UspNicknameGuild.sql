CREATE PROCEDURE [dbo].[UspNicknameGuild]
	 @pPcNo			INT
	,@pNickNm		CHAR(16)
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
		
	UPDATE dbo.TblGuildMember 
	SET 
		[mNickNm]=@pNickNm
	WHERE ([mPcNo]=@pPcNo)
	IF(0 <> @@ERROR) OR (1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

