CREATE Procedure [dbo].[UspAddBanquetHallTicket]
	 @pTicketSerialNo	BIGINT			-- 萍南矫府倔锅龋
	,@pTerritory		INT			-- 康瘤锅龋
	,@pBanquetHallType	INT			-- 楷雀厘鸥涝
	,@pBanquetHallNo	INT			-- 楷雀厘锅龋
	,@pOwnerPcNo		INT			-- 家蜡荤侩磊
	,@pFromPcNm		VARCHAR(12)		-- 焊郴绰 荤恩 捞抚
	,@pToPcNm		VARCHAR(12)		-- 罐绰 惑措规 捞抚
    ,@pSendTicket   	INT			-- 焊郴绰 酒捞袍 捞抚
    ,@pRecvTicket   	INT			-- 罐绰 酒捞袍 捞抚

AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE 	@aRowCnt 			INT,
				@aErrNo				INT,
				@aToPcNo			INT,
				@aLetterLimit		BIT,
				@aToGuildNo			INT,
				@aFromGuildNo		INT,
				@aFromPcNo			INT,
				@aFromGuildAssNo	INT,
				@aToGuildAssNo		INT

	DECLARE	@pUnusedBanquetHallTicketItemID	INT
	DECLARE	@pReceivedBanquetHallTicketID	INT

			
	SELECT @aErrNo = 0, @aRowCnt = 0, @aToPcNo = 0, 
			@aRowCnt = 0, @aToPcNo = 0, @aLetterLimit = 0,
			@aToGuildNo = 0,@aFromGuildNo = 0,@aFromPcNo = 0,
			@aFromGuildAssNo = 0,@aToGuildAssNo = 0


	SET @pUnusedBanquetHallTicketItemID	= @pSendTicket		-- 楷雀厘 檬没厘
    SET @pReceivedBanquetHallTicketID	= @pRecvTicket		-- 罐篮 楷雀厘 檬没厘


	-- 罐绰 荤恩 某腐磐 犬牢
	SELECT 
		@aToPcNo = T1.mNo
		, @aLetterLimit = T2.mIsLetterLimit
		, @aToGuildNo = ISNULL(T3.mGuildNo, 0)
		, @aToGuildAssNo = ISNULL(T4.mGuildAssNo, 0)
	FROM dbo.TblPc T1
		INNER JOIN dbo.TblPcState T2 
				ON T1.mNo = T2.mNo	
		LEFT OUTER JOIN dbo.TblGuildMember T3
			ON T1.mNo = T3.mPcNo	
		LEFT OUTER JOIN dbo.TblGuildAssMem T4
			ON T3.mGuildNo = T4.mGuildNo
	WHERE T1.mNm = @pToPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 3			-- 3 : 罐绰 惑措规捞 粮犁窍瘤 臼澜
		GOTO T_END
	END

	-- 焊郴绰 荤恩 某腐磐沥焊 拳变
	SELECT
		@aFromPcNo = mNo
		, @aFromGuildNo = ISNULL(T2.mGuildNo, 0)
		, @aFromGuildAssNo = ISNULL(mGuildAssNo,0)
	FROM dbo.TblPc T1
		LEFT OUTER JOIN dbo.TblGuildMember T2
			ON T1.mNo = T2.mPcNo
		LEFT OUTER JOIN dbo.TblGuildAssMem T3
			ON T2.mGuildNo = T3.mGuildNo
	WHERE mNm = @pFromPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 3			-- 3 : 罐绰 惑措规捞 粮犁窍瘤 臼澜
		GOTO T_END
	END

	-- 夯牢捞 加茄 辨靛啊 酒聪扼搁 楷钦辨靛牢瘤 八荤
	-- 鞍篮 辨靛楷钦捞 酒聪扼搁 俊矾贸府
	IF ( @aLetterLimit = 1 )
		AND (
			( ( @aFromGuildNo <> 0 AND  @aToGuildNo <> 0 ) 
				AND (@aFromGuildNo <> @aToGuildNo )  )			
			OR ( ( @aFromGuildAssNo <> 0 AND  @aToGuildAssNo <> 0 ) 
				AND ( @aFromGuildAssNo <> @aToGuildAssNo )  )			
			OR ( @aToGuildNo = 0 )
			OR ( @aFromGuildNo = 0 )
		)
	BEGIN
		SET @aErrNo = 6			-- 7 : 罐绰 惑措规捞 荐脚芭何 惑怕
		GOTO T_END
	END 


	IF EXISTS(	SELECT mTicketSerialNo 
				FROM dbo.TblBanquetHallTicket 
				WHERE mTicketSerialNo = @pTicketSerialNo)
	BEGIN
		SET @aErrNo = 2			-- 2 : 萍南捞 捞固 粮犁窃
		GOTO T_END		
	END

	BEGIN TRAN

		-------------------------------------------------
		-- 楷雀厘 涝厘萍南 眠啊
		-------------------------------------------------
		INSERT dbo.TblBanquetHallTicket  (mTicketSerialNo, mTerritory, mBanquetHallType, mBanquetHallNo, mOwnerPcNo, mFromPcNm, mToPcNm) 
		VALUES( @pTicketSerialNo, @pTerritory, @pBanquetHallType, @pBanquetHallNo, @pOwnerPcNo, @pFromPcNm, @pToPcNm)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1		-- 1 : DB 郴何利牢 坷幅
			GOTO T_ERR			 
		END	 

		-------------------------------------------------
		-- 辨靛酒瘤飘 檬没厘 酒捞袍 荐沥, 家蜡鼻 捞傈		
		-------------------------------------------------
		UPDATE dbo.TblPcInventory 	
		SET 
			mItemNo = @pReceivedBanquetHallTicketID, 
			mIsConfirm = 0, 
			mPcNo = @aToPcNo, 
			mEndDate = dbo.UfnGetEndDate(GETDATE(), 7) 	-- 蜡瓤扁茄 7老肺 绊沥
		WHERE mSerialNo = @pTicketSerialNo
				AND mItemNo = @pUnusedBanquetHallTicketItemID

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 4		-- 4 : 萍南捞 粮犁窍瘤 臼澜
		END		 

T_ERR:	
	IF @aErrNo = 0
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
T_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

