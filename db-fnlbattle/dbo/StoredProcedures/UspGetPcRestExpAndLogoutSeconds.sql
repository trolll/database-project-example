/******************************************************************************
**		Name: dbo.UspGetPcRestExpAndLogoutSeconds
**		Desc: TblPcState狼 绒侥 版氰摹客 肺弊酒眶 矫埃 肺靛
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010.12.02	傍籍痹				TblPcState狼 mRestExp > mRestExpGuild 函版
**										mRestExpActivate, mRestExpDeactivate 眠啊肺
**										@pRestExp甫 @pRestExpGuild肺 荐沥
**										@pRestExpActivate, @pRestExpDeactivate 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcRestExpAndLogoutSeconds]
	@pPcNo				INT,
	@pRestExpGuild		BIGINT OUTPUT,
	@pRestExpActivate	BIGINT OUTPUT,
	@pRestExpDeactivate	BIGINT OUTPUT,
	@pLogoutSeconds		INT OUTPUT
AS  
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT @pRestExpGuild = 0, @pRestExpActivate = 0, @pRestExpDeactivate = 0, @pLogoutSeconds = 0;
	
	SELECT @pRestExpGuild = mRestExpGuild
			, @pRestExpActivate = mRestExpActivate
			, @pRestExpDeactivate = mRestExpDeactivate
			, @pLogoutSeconds = datediff(second, mLogoutTm, getdate())
	FROM dbo.TblPcState
	WHERE mNo = @pPcNo;

	IF(0 = @@ROWCOUNT)
	BEGIN
		RETURN(1);
	END

	RETURN(0);

GO

