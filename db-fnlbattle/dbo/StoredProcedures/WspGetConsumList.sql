/******************************************************************************
**		Name: WspGetConsumList
**		Desc: 困殴 惑痢 府胶飘甫 眠免 茄促.
**
**		Auth: JUDY
**		Date: 2011-01-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetConsumList]
	@pPcNm	VARCHAR(12)
	, @pItemNo	INT
	, @pEndDate	SMALLDATETIME
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	SET ROWCOUNT 1000;		-- 弥措 啊廉棵 荐 乐绰 酒捞袍 荐甫 力茄 

	SELECT 
		T2.mRegDate		-- 困殴老磊
		, T5.mUserID		-- 困殴 拌沥
		, T1.mNm			-- 魄概磊疙
		, T2.mItemNo
		, T4.IName mItemNm		
		, mCurCnt
		, mPrice		
	FROM dbo.TblPc T1 
		INNER JOIN dbo.TblConsignment T2
			ON T1.mNo = T2.mPcNo		
		LEFT OUTER JOIN FNLParm.dbo.DT_Item T4
			ON T2.mItemNo = T4.IID	
		LEFT OUTER JOIN FNLAccount.dbo.TblUser T5
			ON T1.mOwner = T5.mUserNo				
	WHERE T1.mNm = @pPcNm	
		AND T2.mItemNo = @pItemNo
		AND T2.mTradeEndDate > @pEndDate

GO

