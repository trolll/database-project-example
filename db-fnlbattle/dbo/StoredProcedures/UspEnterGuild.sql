CREATE PROCEDURE [dbo].[UspEnterGuild]
	 @pGuildNo	INT
	,@pPcNo		INT	
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	 
	IF NOT EXISTS ( SELECT  *
					FROM dbo.TblPc
					WHERE mNo = @pPcNo
						AND mDelDate IS NULL)
	BEGIN
		RETURN(2);		-- 某腐磐 沥焊啊 嘎瘤 臼绰促..** 坷幅蔼 馆券 且巴..
	END 

	IF EXISTS ( SELECT  *
					FROM dbo.TblGuildMember
					WHERE mPcNo = @pPcNo)
	BEGIN
		RETURN(3);		-- 捞固 辨靛俊 啊涝登绢 乐促..** 坷幅蔼 馆券 且巴..
	END 

	INSERT INTO dbo.TblGuildMember([mGuildNo], [mPcNo], [mGuildGrade])
	VALUES(@pGuildNo,  @pPcNo, 4)

		
	IF(0 <> @@ERROR)
		RETURN(1)

	RETURN(0)

GO

