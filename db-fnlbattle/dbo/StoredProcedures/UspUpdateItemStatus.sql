/******************************************************************************
**		Name: UspUpdateItemStatus
**		Desc: 酒捞袍 惑怕 蔼阑 函版 茄促. 
**			  
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2007.05.07	JUDY				酒捞袍 盒府 利侩
**		2007.08.30	JUDY				稠府 坷幅 荐沥 
**		2010.04.01	辫堡挤			阶咯龙 酒捞袍阑 茫绰 版快 蓖加 鸥涝, 家蜡磊, 瓤苞 瘤加矫埃捞 悼老茄瘤 厚背窍绰 何盒 眠啊. 矫府倔阑 货肺 积己窍绰 版快 酒捞袍 加己俊 蓖加 鸥涝, 家蜡磊 , 瓤苞 瘤加 矫埃 蔼阑 眠啊窍霸 函版
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateItemStatus]
	 @pPcNo			INT
	,@pSerial		BIGINT
	,@pStatus		TINYINT 
	,@pIsStack		BIT
	,@pNewSerial	BIGINT OUTPUT
	,@pNewStatus	TINYINT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE @aErrNo		INT	
	DECLARE @aCnt		INT 
	DECLARE @aCntUse	INT 
	DECLARE @aCntTarget	INT 
	DECLARE @aStatus	TINYINT 
	DECLARE @aEndDate	SMALLDATETIME
	DECLARE @aItemNo	INT
	DECLARE @aIsConfirm	BIT
	DECLARE	@aRowCnt	INT
	DECLARE	@aOwner		INT
	DECLARE	@aPracticalPeriod	INT
	DECLARE	@aBindingType		TINYINT
	
	SELECT 	@aErrNo = 0, @aRowCnt = 0

	------------------------------------------
	-- 秦寸 酒捞袍 沥焊甫 啊廉咳
	------------------------------------------
	SELECT 
		@aCnt 		= mCnt, 
		@aCntUse 	= mCntUse, 
		@aEndDate 	= mEndDate,
		@aItemNo 	= mItemNo,
		@aIsConfirm = mIsConfirm, 
		@aStatus 	= mStatus,
		@aOwner		= mOwner,
		@aPracticalPeriod	= mPracticalPeriod,
		@aBindingType		= mBindingType
	FROM dbo.TblPcInventory
	WHERE mPcNo = @pPcNo
			AND mSerialNo =  @pSerial

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END
	END

	SET		@pNewSerial = @pSerial
	SET		@pNewStatus = @aStatus



	BEGIN TRAN	
		------------------------------------------
		-- 惑怕函拳啊 乐栏搁 累诀矫累
		------------------------------------------
		IF @aStatus <> @pStatus
		BEGIN
			------------------------------------------
			-- 胶琶屈 酒捞袍捞搁 惑怕啊 函拳窍搁辑 胶琶捞 阶咯具且鞘夸啊 乐促.
			------------------------------------------
			IF @pIsStack <> 0
			BEGIN
				SELECT 
					TOP 1
					@aCntTarget=ISNULL(mCnt,0), 
					@pNewSerial=mSerialNo
				FROM dbo.TblPcInventory WITH(NOLOCK)
				WHERE mPcNo = @pPcNo
						AND mItemNo = @aItemNo
						AND mEndDate = @aEndDate
						AND mIsConfirm = @aIsConfirm
						AND mStatus = @pStatus
						AND mOwner = @aOwner  
						AND mPracticalPeriod = @aPracticalPeriod
						AND mBindingType = @aBindingType	-- 穿利屈 酒捞袍狼 版快 官牢靛 鸥涝捞 悼老秦具 茄促.
				
				SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo <> 0 
				BEGIN
					SET @aErrNo = 2
					GOTO T_ERR
				END
				 
				------------------------------------------
				--惑怕绰 stack屈捞扼绊 秦档 1俺父 函版等促. 唱赣瘤绰 捞傈 惑怕甫 蜡瘤茄促.
				------------------------------------------
				IF @aCnt <> 1
				BEGIN
					UPDATE dbo.TblPcInventory 
					SET 
						mCnt = mCnt-1
					WHERE mPcNo = @pPcNo
							AND mSerialNo = @pSerial

					SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF @aErrNo <> 0 OR @aRowCnt <= 0 
					BEGIN
						SET  @aErrNo = 4
						GOTO T_ERR	 
					END
				END				
				ELSE
				BEGIN	--昏力
					DELETE dbo.TblPcInventory 
					WHERE mSerialNo = @pSerial

					SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF @aErrNo <> 0 OR @aRowCnt <= 0 
					BEGIN
						SET @aErrNo = 3
						GOTO T_ERR
					END
				END
				
				------------------------------------------
				-- 扁粮巴俊 诀单捞飘
				------------------------------------------
				IF @aCntTarget <> 0
				BEGIN

					UPDATE dbo.TblPcInventory 
					SET 
						mCnt = mCnt+1
					WHERE  mPcNo = @pPcNo
							AND mSerialNo = @pNewSerial

					SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF @aErrNo <> 0 OR @aRowCnt <= 0 
					BEGIN
						SET  @aErrNo = 6
						GOTO T_ERR	 
					END
				END				
				ELSE	-- 货 惑怕甫 啊柳 酒捞袍阑 积己
				BEGIN
					EXEC @pNewSerial =  dbo.UspGetItemSerial 
					IF @pNewSerial <= 0
					BEGIN
						SET @aErrNo = 5
						GOTO T_ERR			 
					END		

					INSERT dbo.TblPcInventory 
						(mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse, mOwner, mPracticalPeriod,	mBindingType )
					VALUES
						(@pNewSerial, @pPcNo, @aItemNo, @aEndDate, @aIsConfirm, @pStatus, 1, @aCntUse, @aOwner, @aPracticalPeriod, @aBindingType)
						
					SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF @aErrNo <> 0 OR @aRowCnt <= 0 
					BEGIN
						SET @aErrNo = 5
						GOTO T_ERR			 
					END	
				END
			END			
			ELSE
			BEGIN	-- 胶琶屈捞 酒聪搁 泅犁巴俊辑 诀单捞飘父 秦林搁 等促.

				UPDATE dbo.TblPcInventory 
				SET 
					mStatus = @pStatus
				WHERE mPcNo = @pPcNo
						AND mSerialNo = @pSerial

				SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo <> 0 OR @aRowCnt <= 0 
				BEGIN
					SET  @aErrNo = 7
					GOTO T_ERR	 
				END
			 END
	 	 END

T_ERR:		
	IF @aErrNo = 0
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
	
T_END:	
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

