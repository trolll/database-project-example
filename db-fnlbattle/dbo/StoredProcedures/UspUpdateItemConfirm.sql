CREATE PROCEDURE [dbo].[UspUpdateItemConfirm]
	 @pPcNo			INT
	,@pSerial		BIGINT
AS
	SET NOCOUNT ON
		
	UPDATE dbo.TblPcInventory 
	SET mIsConfirm = 1
	WHERE mPcNo = @pPcNo
			AND mSerialNo = @pSerial

	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END	

	RETURN(0)

GO

