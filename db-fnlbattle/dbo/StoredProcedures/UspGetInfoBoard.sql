CREATE PROCEDURE [dbo].[UspGetInfoBoard]
	 @pBoardNo		INT
	,@pMsg			VARCHAR(512)	OUTPUT
	,@pErrNoStr		VARCHAR(50)		OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'	
	
	SELECT @pMsg=RTRIM([mMsg]) 
	FROM dbo.TblBoard 
	WHERE [mBoardNo] = @pBoardNo
	IF(@@ROWCOUNT <> 1)
	 BEGIN
		SET @aErrNo		= 1
		SET @pErrNoStr	= 'eErrNoBoardNotExist'
	 END
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

