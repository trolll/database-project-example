/******************************************************************************  
**  File: 
**  Name: UspAddPcSkillTreeNodeItem  
**  Desc: PC狼 胶懦飘府畴靛酒捞袍 窍唱 眠啊
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    积己
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspAddPcSkillTreeNodeItem]
	@pPcNo		INT,
	@pSTNIID	INT,
	@pValidSec	INT,	 -- 864000000檬(10000老)捞搁 公茄措, 2020-01-01 00:00:00 俊 父丰登绰 巴栏肺 距加茄促
	@pLeftMin	INT OUTPUT
AS 
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF @pPcNo = 1 OR @pPcNo = 0
	BEGIN 
		RETURN(0)	-- NPC, 滚妨柳 酒捞袍
	END

	DECLARE	 @aEndDay		SMALLDATETIME
			, @aErr INT
			, @aRowCnt INT

	SELECT 	@aErr = 0, 	@aRowCnt = 0;	
			
	IF 864000000 = @pValidSec
		BEGIN
			SELECT	 @aEndDay = dbo.UfnGetEndDate(GETDATE(), 864000000/(3600 * 24))
		END
	ELSE
		BEGIN
			SELECT	 @aEndDay = dateadd(second, @pValidSec, GETDATE())
		END
		
	UPDATE dbo.TblPcSkillTreeInventory
	SET mEndDate=@aEndDay
	WHERE mPcNo=@pPcNo 
		AND mSTNIID = @pSTNIID		

	SELECT @aErr = @@ERROR	, @aRowCnt = @@ROWCOUNT, @pLeftMin = DATEDIFF(mi, GETDATE(), @aEndDay);
	
	IF @aErr <> 0 
	begin
		RETURN(1) -- DB ERROR
	end	

	IF @aRowCnt > 0
		RETURN(0)
	
	insert into dbo.TblPcSkillTreeInventory
	values(default, @pPcNo, @pSTNIID, @aEndDay)
	if @@error <> 0
	begin
		return 2 -- DB 俊矾
	end

	return 0

GO

