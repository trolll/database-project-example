CREATE PROCEDURE [dbo].[UspEnterFriend]
	 @pOwnerPcNo	INT
	,@pPcNm			CHAR(15)
	,@pPcNo			INT			OUTPUT
	,@pErrNoStr		VARCHAR(50)	OUTPUT
	,@pLastLogoutTm	DATETIME	OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo		INT,
			@aFriendPcNm	CHAR(12)
			
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'
	SET		@aFriendPcNm = NULL
	
	SELECT 
		@pPcNo=a.[mNo], 
		@pLastLogoutTm=b.[mLogoutTm] 
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
			ON a.mNo = b.mNo
	WHERE a.mNm = @pPcNm
			AND a.mDelDate IS NULL
	IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
	BEGIN
		SET	 @pErrNoStr	= 'eErrNoCharNotExist'
		RETURN(1)
	END

	SELECT TOP 1 
		@aFriendPcNm = mFriendPcNm
	FROM dbo.TblPcFriend 
	WHERE mOwnerPcNo = @pOwnerPcNo
			 AND mFriendPcNo = @pPcNo						 
	IF (@@ROWCOUNT > 0) AND (@aFriendPcNm = @pPcNm)
	BEGIN
		SET	 @pErrNoStr	= 'eErrNoCharAlreadyRegister'
		RETURN(2) 	
	END 

	BEGIN TRAN
	
		IF @aFriendPcNm IS NOT NULL		-- duplicate pc number 
		BEGIN		
		
			UPDATE dbo.TblPcFriend	
			SET 
				mFriendPcNo =-mFriendPcNo			
			WHERE mOwnerPcNo = @pOwnerPcNo
				 AND mFriendPcNo = @pPcNo
				 
			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(3)
			END 	 
		END 
		
		INSERT INTO dbo.TblPcFriend([mOwnerPcNo], [mFriendPcNo], [mFriendPcNm])
		VALUES(@pOwnerPcNo,  @pPcNo, @pPcNm)		
		
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(3)
		END 	 

	COMMIT TRAN
	RETURN(0)

GO

