/****** Object:  View dbo.ViewUserActive    Script Date: 2011-4-19 15:24:33 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2011-3-4 11:36:40 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2010-3-22 15:58:16 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2009-12-14 11:35:24 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2009-7-14 13:13:26 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  View dbo.ViewUserActive    Script Date: 2008-11-10 10:37:15 ******/





CREATE VIEW [dbo].[ViewUserActive]
AS 
	SELECT a.[mOwner] FROM TblPc AS a INNER JOIN TblPcState AS b
										ON((a.[mNo] = b.[mNo]) AND (a.[mDelDate] IS NULL) AND
										   (DATEDIFF(dd,b.[mLoginTm],GETDATE()) <= 1))
					  GROUP BY a.[mOwner]

GO

