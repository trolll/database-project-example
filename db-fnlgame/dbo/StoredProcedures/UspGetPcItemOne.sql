/******************************************************************************    
**  Name: UspGetPcItemOne    
**  Desc: 아이템 한 개 정보 얻어오기  
**    
**                  
**  Return values:    
**   레코드셋  
**     
**                  
**  Author: 김강호    
**  Date: 2010-12-02    
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:       Author:    Description:    
**  --------    --------   ---------------------------------------    
**  2011 0615	dmbkh		mHoleCount 추가
*******************************************************************************/    
CREATE PROCEDURE [dbo].[UspGetPcItemOne]  
 @pSerialNo BIGINT  
AS  
  SET NOCOUNT ON  ;  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  ;  
  
  SELECT     
   mSerialNo    
   , mItemNo    
   , DATEDIFF(mi,GETDATE(),mEndDate) mEndDate       
   , mCnt    
   , mIsConfirm    
   , mStatus    
   , mCntUse    
   , mIsSeizure    
   , mApplyAbnItemNo    
   , DATEDIFF(mi,GETDATE(),mApplyAbnItemEndDate) mApplyAbnItemEndDate     
   , mOwner    
   , mPracticalPeriod    
   , mBindingType    
   , mRestoreCnt
   , mHoleCount    
  FROM dbo.TblPcInventory WITH(INDEX(CL_TblPcInventory), NOLOCK )    
   WHERE  mSerialNo = @pSerialNo    
   AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())    
   AND mCnt > 0 ;

GO

