/******************************************************************************
**		Name: UspExitChatFilter
**		Desc: Ã¤ÆÃÂ÷´ÜÁ¤º¸ »èÁ¦
**		Test:			
**		Auth: ³²¼º¸ð
**		Date: 2014/03/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspExitChatFilter]
	 @pOwnerPcNo		INT
	 ,@pPcNo			INT
	 ,@pChatFilterPcNm	CHAR(12)	-- Àü´ÞÀÎÀÚ Á¤º¸ º¯°æ 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DELETE dbo.TblPcChatFilter
	WHERE (mOwnerPcNo=@pOwnerPcNo) 
			AND ( mChatFilterPcNm = @pChatFilterPcNm ) 
	IF(1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END
		
	RETURN(0)

GO

