/******************************************************************************  
**  Name: UspDeleteBead
**  Desc: 구슬 제거(언링크 후 구슬 제거, 구슬 아이템 생성 안함)
**  
**                
**  Return values:  
**   0 : 성공
**   <>0 : 실패
**                
**  Author: 김강호
**  Date: 2011-06-14
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDeleteBead]
		@pEquipSerialNo	BIGINT
		,@pBeadIndex TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	delete from dbo.TblPcBead
	where mOwnerSerialNo=@pEquipSerialNo and mBeadIndex=@pBeadIndex;
	if @@ERROR <> 0 OR @@ROWCOUNT = 0
	BEGIN
		return 1;	-- 구슬을 제거할 수 없습니다.
	END
	
	RETURN 0;

GO

