/******************************************************************************
**		File: WspListServantByPc.sql
**		Name: WspListServantByPc
**		Desc: ¼­¹øÆ® Á¤º¸ Á¶È¸ (Ä³¸¯ÅÍ¸í)
**		Ex	: EXEC dbo.WspListServantByPc 'Å©Èý'
** 
**		Auth: 2015.06.15
**		Date: ÀÌ Áø ¼±
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		
*******************************************************************************/ 
CREATE PROC [dbo].[WspListServantByPc]
(
	@pCharNm VARCHAR(12)
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	SELECT
		P.mNm
		, I.mRegDate
		, I.mSerialNo
		, I.mItemNo
		, DI.IName
		, DATEDIFF(DD, GETDATE(), I.mEndDate) AS mEndDate
		, I.mStatus
		, I.mCnt
		, I.mIsSeizure
		, I.mPracticalPeriod
		, I.mBindingType
		, I.mRestoreCnt
		, EQ.mSlot
		, S.mName
		, S.mLevel
		, S.mExp
		, SkillPackName = STUFF((
								SELECT ',' + mName
								FROM   (
											SELECT DSP.mName
											FROM dbo.TblPcServantSkillPack AS SP
												INNER JOIN FNLParm.dbo.DT_SkillPack AS DSP
											ON SP.mSPID = DSP.mSPID
											WHERE SP.mSerialNo = S.mSerialNo
										) AS A
								FOR XML PATH('')),1,1,'')
	FROM dbo.TblPc AS P
		INNER JOIN dbo.TblPcInventory AS I 
	ON P.mNo = I.mPcNo
		LEFT OUTER JOIN dbo.TblPcEquip AS EQ
	ON I.mSerialNo = EQ.mSerialNo
		INNER JOIN dbo.TblPcServant AS S
	ON I.mSerialNo = S.mSerialNo
		INNER JOIN FNLParm.dbo.DT_Item AS DI
	ON I.mItemNo = DI.IID
	WHERE P.mNm = @pCharNm
	AND P.mDelDate IS NULL
END

GO

