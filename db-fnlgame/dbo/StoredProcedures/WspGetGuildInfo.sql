CREATE PROCEDURE [dbo].[WspGetGuildInfo]	
	@mGuildNm CHAR(12) 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		g.mGuildNm, 
		g.mGuildNo, 
		p.mNm, 
		(SELECT COUNT(*) FROM [dbo].[TblGuildMember]
		 WHERE mGuildNo = g.mGuildNo) mMemberCnt, 
		g.mRegDate, 
		c.mPlace, 
		g.mGuildSeqNo 		
	FROM
		[dbo].[TblGuildMember] AS gm 
		INNER JOIN [dbo].[TblGuild] AS g 
			ON	gm.mGuildNo = g.mGuildNo
		INNER JOIN [dbo].[TblPc] AS p 
			ON	gm.mPcNo = p.mNo
		LEFT OUTER JOIN [dbo].[TblCastleTowerStone] AS c 
			ON	gm.mGuildNo = c.mGuildNo
	WHERE
		g.mGuildNm = @mGuildNm
		AND gm.mGuildGrade = 0

GO

