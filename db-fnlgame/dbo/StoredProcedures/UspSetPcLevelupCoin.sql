/******************************************************************************
**		Name: UspSetPcLevelupCoin
**		Desc: ·¹º§ ¾÷ ÁÖÈ­ »ç¿ëÀÚ Á¤º¸ ¾÷µ¥ÀÌÆ®
**		Test:			
**		Auth: ³²¼º¸ð
**		Date: 2014/02/18
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcLevelupCoin]
	@pPcno		INT,
	@pExp		BIGINT,
	@pSection	INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF NOT EXISTS (SELECT mExp FROM dbo.TblPcLevelupCoin WHERE mPcno = @pPcno)
	BEGIN
		INSERT INTO dbo.TblPcLevelupCoin
		VALUES
		(
			@pPcno,
			@pExp,
			@pSection
		)
	END
	ELSE
	BEGIN
		UPDATE dbo.TblPcLevelupCoin
		SET mExp = @pExp, 
			mLastReceiptSection = @pSection
		WHERE mPcno = @pPcno
	END

GO

