/******************************************************************************
**		Name: UspUpdateServantSub
**		Desc: 辑锅飘 辑宏 牢郸胶 盎脚
**
**		Auth: 捞侩林
**		Date: 2016-04-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateServantSub]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pSubIndex		TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNo		INT
	DECLARE @aRowCnt	INT

	IF EXISTS (SELECT mSerialNo 
				FROM dbo.TblPcServantSub 
				WHERE mSerialNo = @pSerialNo)
	BEGIN
		UPDATE dbo.TblPcServantSub 
		SET mSubIndex = @pSubIndex
		WHERE mPcNo = @pPcNo
		  AND mSerialNo = @pSerialNo
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblPcServantSub (mPcNo, mSerialNo, mSubIndex)
		VALUES (@pPcNo, @pSerialNo, @pSubIndex)
	END

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR 	@aRowCnt = 0
	BEGIN
		RETURN(@aErrNo)
	END

	SET NOCOUNT OFF;

GO

