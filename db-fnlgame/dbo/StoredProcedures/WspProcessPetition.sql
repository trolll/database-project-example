--------------------------------
-- ░┤├╝╕φ : WspProcessPetition
-- ╝│╕φ : ┴°┴ñ└╗ ├│╕«╟╤┤┘
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.07.21
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
--------------------------------
CREATE PROCEDURE [dbo].[WspProcessPetition]
	@mPID BIGINT -- PID

AS
	SET NOCOUNT ON

	BEGIN TRANSACTION

	UPDATE
		[dbo].[TblPetitionBoard]
	SET
		mIsFin = 1,
		mFinDate    = GetDate()
		
	WHERE
		mPID  = @mPID AND mFinDate IS NULL

	IF @@ERROR = 0
		BEGIN
			COMMIT TRANSACTION
			RETURN 0
		END
	ELSE
		BEGIN
			ROLLBACK TRANSACTION
			RETURN 1
		END

	SET NOCOUNT OFF

GO

