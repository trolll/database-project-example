CREATE PROCEDURE [dbo].[WspUpdateCharExp]	
	@mExp BIGINT, -- °жЗиДЎ
	@mNm CHAR(12) -- ДіёЇЕНён
AS
	SET NOCOUNT ON

	UPDATE dbo.TblPcState
	SET
		mExp = @mExp 
	WHERE mNo = (SELECT mNo From [dbo].[TblPc] WHERE mNm = @mNm)			

	IF @@ERROR <> 0
		RETURN(1)
		
	RETURN(0)

GO

