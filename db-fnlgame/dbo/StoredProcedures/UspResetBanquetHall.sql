/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspResetBanquetHall    Script Date: 2008-11-10 10:37:17 ******/





CREATE Procedure [dbo].[UspResetBanquetHall]		-- ?? ???? ??? ??
	 @pTerritory		INT			-- ????
	,@pBanquetHallType	INT			-- ????? (0:?.???/1:?.???)
	,@pBanquetHallNo	INT			-- ?????
	,@pLeftMin		INT			-- ???
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	BEGIN TRAN

	-- ??? ?? ??
	UPDATE TblBanquetHall 
	SET mOwnerPcNo = 0, mLeftMin = @pLeftMin, mRegDate = getdate()
	WHERE mTerritory = @pTerritory AND mBanquetHallType = @pBanquetHallType AND mBanquetHallNo = @pBanquetHallNo
	
	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END			 
	END	 
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

