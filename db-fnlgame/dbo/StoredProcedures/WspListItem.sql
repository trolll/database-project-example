/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2009-6-1 15:32:38 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2009-5-12 9:18:17 ******/

/****** Object:  Stored Procedure dbo.WspListItem    Script Date: 2008-11-10 10:37:18 ******/

--FNLGame1
CREATE  PROCEDURE  [dbo].[WspListItem]
	@mSerialNo BIGINT, 
	@mPos	CHAR(1)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	IF @mPos = 'i' 
	BEGIN
		SELECT h.mPcNo pcno,
		       h.mItemNo itemno,
		       h.mStatus status,		       
		       h.mCnt cnt,
		       h.mIsConfirm isconfirm,
		       DATEDIFF(DAY, GETDATE(), h.mEndDate) intvalidday,
		       h.mSerialNo serialno,
		       --ADDED BY MENGDAN
		       mEndDate enddate	
		  FROM dbo.TblPcInventory h
		  WHERE h.mSerialNo=@mSerialNo
	END
	
	IF @mPos = 's' 
	BEGIN
		SELECT null pcno,
		       h.mItemNo itemno,
		       h.mStatus status,		       
		       h.mCnt cnt,
		       h.mIsConfirm isconfirm,
		       DATEDIFF(DAY, GETDATE(), h.mEndDate) intvalidday,
		       h.mSerialNo serialno,
		       --ADDED BY MENGDAN
		       mEndDate enddate	
		  FROM dbo.TblPcStore h
		  WHERE h.mSerialNo=@mSerialNo
	END

GO

