/******************************************************************************  
**  File: WspListPcByAccount.sql  
**  Name: WspListPcByAccount  
**  Desc:  
**   
**  Auth:   
**  Date:   
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  2007.08.06 JUDY    ѕЖАМµр Size Б¶Б¤  
**  2009.07.01 JUDY    ЗС±Ы єОєР јцБ¤  
*******************************************************************************/   
CREATE PROCEDURE [dbo].[WspListPcByAccount]  
 @mUserId VARCHAR(20) -- °иБ¤ён  
AS  
 SET NOCOUNT ON  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 SELECT  
  p.mRegdate  
  , p.mNm  
  , IsConnect =  
   CASE  
    WHEN s.mLoginTm >= s.mLogoutTm THEN 'Online'  
    WHEN s.mLoginTm < s.mLogoutTm THEN 'OffLine'  
    ELSE 'OffLine'  
   END  
  , p.mNo  
  , s.mLevel  
  , ISNULL(s.mLoginTm, CAST(STR('2000')+'-'+STR('01')+'-'+STR('01') AS DATETIME)) AS mLoginTm  
  , ISNULL(s.mLogoutTm, CAST(STR('2000')+'-'+STR('01')+'-'+STR('01') AS DATETIME)) AS mLogoutTm  
  , ISNULL(s.mIp, 0) AS mIp  
 FROM  
  [dbo].[TblPc] AS p  
 INNER JOIN  
  [FNLAccount].[dbo].[TblUser] AS u  
 ON  
  p.mOwner = u.mUserNo  
 INNER JOIN  
  [dbo].[TblPcState] AS s  
 ON  
  p.mNo = s.mNo  
 WHERE  
  u.mUserId = @mUserId  
  AND p.mDelDate IS NULL  
 ORDER BY  
  p.mNo

GO

