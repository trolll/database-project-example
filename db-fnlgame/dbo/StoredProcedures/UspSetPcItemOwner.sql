/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetPcItemOwner    Script Date: 2008-11-10 10:37:18 ******/






CREATE PROCEDURE [dbo].[UspSetPcItemOwner]
	  @pSerial		BIGINT		-- ?? ? ??? ?? ???? ?????
	, @pIsSeal	BIT		-- ?? ??(1 ?? ??, 0?? ??)
	, @pOwner	INT	OUTPUT	-- ??? ??? ??.
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	@aItemOwner INT	
	SELECT	@pOwner = 0, @aItemOwner = 0

	SELECT
	 	  @pOwner 	= T2.mOwner 	--(T2 ? TblPc??. TblUser ? mUserNo??.)
		, @aItemOwner 	= T1.mOwner	--(TblPcInventory ? mOwner??.) 
	FROM dbo.TblPcInventory T1
		INNER JOIN dbo.TblPc T2
			ON T2.mNo = T1.mPcNo 
				AND T2.mDelDate IS NULL		
	WHERE mSerialNo = @pSerial

	IF(@pIsSeal = 1)	
	BEGIN
		-- ??? ??? ??? ?? ???? ???? ?? ??? ?????.
		IF(@pOwner <> 0 AND @aItemOwner = 0) 
		BEGIN
			-- ?????. (Auto commit)
			UPDATE dbo.TblPcInventory
			SET mOwner = @pOwner
			WHERE mSerialNo = @pSerial
			
			IF(@@ERROR <> 0)
			BEGIN
				RETURN(2) -- DB Err
			END
		END
		ELSE
		BEGIN
			-- ??? ??? ??? ??? ??? ?????? ???.
			RETURN(3)
		END
	END
	ELSE
	BEGIN
		-- ???? ? ??? ??? ??? ??? ?? ??.
		IF(@aItemOwner <> 0)
		BEGIN
			-- ?? ?? ???. (Auto commit)
			UPDATE dbo.TblPcInventory
			SET mOwner = 0
			WHERE mSerialNo = @pSerial
			
			IF(@@ERROR <> 0)
			 BEGIN
				RETURN(4) -- DB Err
			 END
		END
		ELSE
		BEGIN			
			RETURN(5)	-- ?? ????? ???? ???
		END
	 END

	RETURN(0)

GO

