CREATE PROCEDURE [dbo].[WspGetItemCntByAllTID] 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		T2.IName,
		T1.*
	FROM (
		SELECT 
			 dt.mItemNo	 AS IID
			, COUNT(p.mItemNo) AS Cnt
		FROM 
			dbo.TblPcInventory p 
			RIGHT OUTER JOIN FNLParm.dbo.TblMonitorItem dt 
				ON	p.mItemNo = dt.mItemNo 			
		GROUP BY  dt.mName, dt.mItemNo	)T1
	INNER JOIN  FNLParm.dbo.DT_ITEM T2
			ON T1.IID = T2.IID
		ORDER BY T1.IID ASC

GO

