/******************************************************************************  
**  File: 
**  Name: UspGetGuildSkillTree  
**  Desc: 길드의 스킬트리노드아이템들을 로드한다.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetGuildSkillTree]
	 @pGuildNo		INT
AS  
	SET NOCOUNT ON; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
	SELECT 
		a.mSTNIID,
		DATEDIFF(mi,GETDATE(),a.mEndDate) mEndDate,
		a.mCreatorPcNo,
		RTRIM(b.mNm) mNm,
		a.mExp
	FROM dbo.TblGuildSkillTreeInventory a 
		INNER JOIN dbo.TblPc b 
			ON a.mCreatorPcNo=b.mNo
	WHERE 	a.mGuildNo = @pGuildNo
				AND a.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY a.mSTNIID ASC

GO

