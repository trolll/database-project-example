/****** Object:  Stored Procedure dbo.UspSendBanquetHallTicketToGuild    Script Date: 2011-4-19 15:24:41 ******/

/****** Object:  Stored Procedure dbo.UspSendBanquetHallTicketToGuild    Script Date: 2011-3-17 14:50:08 ******/

/****** Object:  Stored Procedure dbo.UspSendBanquetHallTicketToGuild    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.UspSendBanquetHallTicketToGuild    Script Date: 2010-12-23 17:46:05 ******/

/****** Object:  Stored Procedure dbo.UspSendBanquetHallTicketToGuild    Script Date: 2010-3-22 15:58:23 ******/

/****** Object:  Stored Procedure dbo.UspSendBanquetHallTicketToGuild    Script Date: 2009-12-14 11:35:31 ******/

/****** Object:  Stored Procedure dbo.UspSendBanquetHallTicketToGuild    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.UspSendBanquetHallTicketToGuild    Script Date: 2009-7-14 13:13:33 ******/
CREATE  PROCEDURE [dbo].[UspSendBanquetHallTicketToGuild]
	 @pSerial		BIGINT		-- ??? ??? ??.
	,@pToGuildNm	VARCHAR(12)
	,@pStatus		TINYINT
	,@pTerritory		INT		-- ????
	,@pBanquetHallType	INT		-- ?????
	,@pBanquetHallNo	INT		-- ?????
	,@pOwnerPcNo		INT		-- ???
	,@pFromPcNm		VARCHAR(12)	-- ??? ?? ??
    ,@pSendTicket   	INT		-- ??? ??? ??
    ,@pRecvTicket   	INT		-- ?? ??? ??
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE 	@aRowCnt 	INT,
				@aErrNo		INT
			
	SELECT @aErrNo = 0, @aRowCnt = 0

	DECLARE	@aFromPcNo		INT
	DECLARE	@aFromGuildNo		INT
	DECLARE	@aFromGuildAssNo	INT
	DECLARE	@aToGuildNo		INT
	DECLARE	@aToGuildAssNo		INT

	DECLARE @aSn	BIGINT
	DECLARE @aPcNo	INT
	DECLARE	@aEnd	DATETIME,
				@aBeginCount	INT,
				@aEndCount	INT
	DECLARE	@aBanquetHallTicket1	INT	-- ??? ???(??)
	DECLARE	@aBanquetHallTicket2	INT	-- ?? ??? ???
				
	DECLARE	@aGuildMember	TABLE (
					mSeq		INT	 IDENTITY(1,1)	 NOT NULL,
					mPcNo		INT	 NOT NULL
				)

	SELECT		 @aEnd = dbo.UfnGetEndDate(GETDATE(), 7)
				,@aBeginCount = 0
				,@aEndCount = 0
				,@aSn = 0
				,@aPcNo = 0
				,@aBanquetHallTicket1 = @pSendTicket	-- ??? ???(??) ?? (???? ????)
                ,@aBanquetHallTicket2 = @pRecvTicket	-- ??? ??? ?? (???? ????)

	--------------------------------------------------------------------------	
	--?? ?? ?? 
	--------------------------------------------------------------------------
	SELECT 
		@aToGuildNo = mGuildNo
	FROM dbo.TblGuild 
	WHERE mGuildNm = @pToGuildNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(2)		-- 2 : ?? ??? ???? ??
	END

	--------------------------------------------------------------------------	
	-- 2007.10.09 soundkey ??????? ??
	--------------------------------------------------------------------------
	-- From PcNo ????
	SELECT
		@aFromPcNo = mNo
	FROM dbo.TblPc
	WHERE mNm = @pFromPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(4)	-- From PcNo ???? ??
	END

	-- From GuildNo ????
	SELECT
		@aFromGuildNo = mGuildNo
	FROM dbo.TblGuildMember 
	WHERE mPcNo = @aFromPcNo

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(6)	-- ????? ??
	END
	
	-- ?????? ??
	IF @aFromGuildNo <> @aToGuildNo	-- ??? ?? ??? ???? ?????? ??
	BEGIN
		-- From GuildAssNo ????
		SELECT
			@aFromGuildAssNo = mGuildAssNo
		FROM dbo.TblGuildAssMem
		WHERE mGuildNo = @aFromGuildNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			RETURN(6)	--??????? ??
		END

		-- To GuildAssNo ????
		SELECT
			@aToGuildAssNo = mGuildAssNo
		FROM dbo.TblGuildAssMem
		WHERE mGuildNo = @aToGuildNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			RETURN(6)	-- ??????? ??
		END

		-- ?? ????? ???? ????
		IF @aFromGuildAssNo <> @aToGuildAssNo 
		BEGIN
			RETURN(8)	-- ?? ????? ??
		END
		
	END

	--------------------------------------------------------------------------	
	--??? ??? ??
	--------------------------------------------------------------------------
	DELETE dbo.TblPcInventory 
	WHERE mSerialNo = @pSerial
 			 AND mItemNo = @aBanquetHallTicket1

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(3)		-- 3 : ??? ???? ???? ??
	END
		 
	--------------------------------------------------------------------------	
	--?? ??? ??? ???
	--------------------------------------------------------------------------
	INSERT INTO @aGuildMember
	SELECT mPcNo
	FROM dbo.TblGuildMember 
	WHERE  mGuildNo = @aToGuildNo
	
	SET @aEndCount = @@ROWCOUNT	

	--------------------------------------------------------------------------	
	--??? ??? ??
	--------------------------------------------------------------------------	
	WHILE(@aEndCount <>0)
	BEGIN
		SELECT 
			TOP 1	@aPcNo = mPcNo
		FROM @aGuildMember
		WHERE mSeq = @aBeginCount+1

		EXEC @aSn =  dbo.UspGetItemSerial 
		IF @aSn <= 0
		BEGIN
			CONTINUE	 
		END			

		IF @aPcNo > 0 
		BEGIN
			INSERT dbo.TblPcInventory(mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
				VALUES(@aSn, @aPcNo, @aBanquetHallTicket2, @aEnd, 0, @pStatus, 1, 0)
			IF(0 = @@ERROR)
			BEGIN
			
				INSERT dbo.TblBanquetHallTicket (mTicketSerialNo, mTerritory, mBanquetHallType, mBanquetHallNo, mOwnerPcNo, mFromPcNm, mToPcNm)
					VALUES (@aSn,@pTerritory, @pBanquetHallType, @pBanquetHallNo, @pOwnerPcNo, @pFromPcNm, N'')
				IF(0 = @@ERROR)
				BEGIN
					SELECT @aPcNo, @aSn
				END
			END
		END

		SET @aEndCount = @aEndCount - 1
		SET @aBeginCount = @aBeginCount + 1
	END

	RETURN(0)

GO

