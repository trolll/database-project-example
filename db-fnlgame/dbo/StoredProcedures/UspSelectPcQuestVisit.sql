/******************************************************************************
**		Name: UspSelectPcQuestVisit
**		Desc: 俺么瘤 沤规 涅胶飘 惑怕甫 焊辰促.
**
**		Auth: 沥柳龋
**		Date: 2010-03-29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSelectPcQuestVisit]
     @pPcNo			INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT 
			T2.mQuestNo,
			T2.mPlaceNo,
			T2.mVisit
    FROM dbo.TblPcQuest as T1 
		INNER JOIN dbo.TblPcQuestVisit as T2
         ON T1.mPcNo = T2.mPcNo
			AND T1.mQuestNo = T2.mQuestNo
    WHERE T1.mPcNo = @pPcNo 
		AND T1.mValue < 99

GO

