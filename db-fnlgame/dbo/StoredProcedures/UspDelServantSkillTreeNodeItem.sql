/******************************************************************************
**		Name: UspDelServantSkillTreeNodeItem
**		Desc: ¼­¹øÆ® ½ºÅ³ Æ®¸® ³ëµå ¾ÆÀÌÅÛ Á¦°Å
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDelServantSkillTreeNodeItem]
	@pSerialNo		BIGINT
	,@pSTNIID		INT = 0
	,@pSTID1		INT = 0
	,@pSTID2		INT = 0
	,@pSTID3		INT = 0
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0

	IF 0 = @pSTNIID 
	BEGIN
		IF 0 < @pSTID1
		BEGIN
			DELETE FROM dbo.TblPcServantSkillTree
			WHERE mSerialNo = @pSerialNo
			  AND mSTID IN (@pSTID1, @pSTID2, @pSTID3)
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1		-- SQL Error
			END
		END
		ELSE
		BEGIN
			DELETE FROM dbo.TblPcServantSkillTree
			WHERE mSerialNo = @pSerialNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1		-- SQL Error
			END
		END
	END
	ELSE
	BEGIN
		DELETE FROM dbo.TblPcServantSkillTree
		WHERE mSerialNo = @pSerialNo AND mSTNIID = @pSTNIID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- SQL Error
		END
	END

	RETURN @aErrNo

	SET NOCOUNT OFF;

GO

