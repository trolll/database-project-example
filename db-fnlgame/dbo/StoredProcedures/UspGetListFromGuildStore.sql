/******************************************************************************  
**  Name: UspGetListFromGuildStore  
**  Desc: 길드 창고 리스트를 가져온다.  
**  
**  Auth:  김 광섭  
**  Date:  2008-01-06  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  2009.04.17 JUDY    인덱스 힌트 추가  
**  2011.06.30 dmbkh   mHoleCount 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetListFromGuildStore]  
   @pOwnerGuild INT  
 , @pGrade TINYINT  
AS  
 SET NOCOUNT ON   
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
 SET ROWCOUNT 300    
  
  
 SELECT   
  [mSerialNo],   
  [mItemNo],   
  [mIsConfirm],   
  [mStatus],   
  [mCnt],   
  [mCntUse],  
  [mOwner],  
  [mPracticalPeriod],
  [mHoleCount]
 FROM dbo.TblGuildStore WITH(INDEX(CL_TblGuildStore_1), NOLOCK )  
 WHERE mGuildNo = @pOwnerGuild   
   AND mGrade = @pGrade  
   AND mEndDate >  CONVERT(SMALLDATETIME, GETDATE())  
 ORDER BY mItemNo

GO

