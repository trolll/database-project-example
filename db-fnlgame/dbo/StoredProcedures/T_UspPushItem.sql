--------------------------------------------------------------
-- STEP 2_2
-- : ?? ??? ??
---------------------------------------------------------------



CREATE PROCEDURE [dbo].[T_UspPushItem]
	 @pPcNo			INT				-- ????.(NON-PC? 1?)
	,@pSerial		BIGINT			-- 0?? ??.
	,@pItemNo		INT
	,@pValidDay		INT				-- ???.(??:?)
	,@pCnt			INT				-- ??? ???? ??.
	,@pCntUse		TINYINT
	,@pIsConfirm	BIT
	,@pStatus		TINYINT 
	,@pIsStack		BIT
	,@pIsCharge		BIT = 0			-- ?? ??? ??
	,@pPracticalPeriod	INT = 0			-- ?????? 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	 @aEndDay		SMALLDATETIME
				,@aOrgPcNo		INT		-- ?? ?? ?
				,@aIsSeizure		BIT	
				,@aCntRest		INT
				,@aRowCnt		INT
				,@aErr			INT
				,@aSerial	BIGINT
				,@aCntTarget		BIGINT
				,@aOwner		INT
				,@aPracticalPeriod	INT
				
	-----------------------------------------------
	-- ?? ???
	-----------------------------------------------
	SELECT		 @aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)
				,@aRowCnt = 0
				,@aErr = 0
				,@aOrgPcNo = 0
				,@aSerial = 0 
				,@aOwner = 0
				,@aPracticalPeriod = 0
	IF @@ERROR <> 0			-- ??? ?? ???? ?? ??? ?? ?? 
	BEGIN
		SET @aErr = 1				
		GOTO T_END
	END 
	IF @aEndDay < GETDATE()
	BEGIN
		SET @aErr = 1				
		GOTO T_END
	END 
	-----------------------------------------------
	-- 409 : ? ??? 
	-----------------------------------------------
	IF(@pItemNo = 409)
	BEGIN
		SET		@pIsConfirm = 1
		SET		@pStatus = 1
	END
		
	-----------------------------------------------
	-- ?? ???
	-----------------------------------------------
	IF( @pSerial <> 0 )
	BEGIN		
		SELECT 	TOP 1		
			@aOrgPcNo = [mPcNo],	-- ?? ???	
			@aCntRest=[mCnt], 
			@aEndDay=[mEndDate], 
			@aIsSeizure=[mIsSeizure],
			@aOwner = [mOwner],
			@aPracticalPeriod = [mPracticalPeriod]
		FROM dbo.T_TblPcInventory 
		WHERE  mSerialNo = @pSerial
		SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErr <> 0 OR 	@aRowCnt = 0
		BEGIN
			SET @aErr = 1				
			GOTO T_END
		END		
	
		IF @aIsSeizure <> 0
		BEGIN
			SET @aErr = 29	
			GOTO T_END					--	?? ?? ???
		END
		SET @aCntRest = @aCntRest - @pCnt
		IF (@aCntRest <  0 )
		BEGIN
			SET @aErr = 2				-- ??? ? ?? ??? ??? ????.( ??? 0?? ?? ??)
			GOTO T_END		
		END
	END
	-----------------------------------------------
	-- ??? ??? ???? ?? 
	-----------------------------------------------
	IF (@pIsStack <> 0) AND (1 <> @pPcNo)
	BEGIN
		SELECT 	TOP 1			
			@aCntTarget=ISNULL([mCnt],0), 
			@aSerial=[mSerialNo], 
			@aIsSeizure=ISNULL([mIsSeizure],0)
		FROM dbo.T_TblPcInventory 
		WHERE  mPcNo  = @pPcNo
				AND mItemNo = @pItemNo 
				AND mEndDate  = @aEndDay
				AND mIsConfirm  = @pIsConfirm  
				AND mStatus  = @pStatus
				AND mOwner = @aOwner  
				AND mPracticalPeriod = @pPracticalPeriod
 
		SELECT @aErr = @@ERROR
		IF ( @aErr <> 0 )
		BEGIN
			SET @aErr = 3	
			GOTO T_END		
		END		
		IF(@aIsSeizure <> 0 )				
		 BEGIN
			SET @aErr = 28	
			GOTO T_END		
		 END	
	END	
	
	-----------------------------------------------
	-- ??? ??
	-----------------------------------------------
	BEGIN TRAN
		IF @aCntTarget  <> 0 
		BEGIN
			UPDATE dbo.T_TblPcInventory				--	??? ??? ?? ?? ?? ???? ???? 
			SET
				mCnt = mCnt + @pCnt
			WHERE  mSerialNo = @aSerial
			
			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF  @aErr <> 0 OR 	@aRowCnt = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 4	
				GOTO T_END		
			END		
			IF ( @pSerial <> 0)
			BEGIN
				IF @aCntRest < 1		-- ????? ???? ?? ? 
				BEGIN
					DELETE dbo.T_TblPcInventory
					WHERE mSerialNo = @pSerial
	
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 5	
						GOTO T_END		
					END		
				END 
				ELSE
				BEGIN
					UPDATE dbo.T_TblPcInventory			
					SET
						mCnt = @aCntRest
					WHERE  mSerialNo =@pSerial				
	
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 6	
						GOTO T_END		
					END		
				END 
			END
		END		
		ELSE
		BEGIN
			IF ( @pSerial <> 0 )
			BEGIN
				IF @aCntRest < 1		-- ????? ???? ?? ? 
				BEGIN		
					SET @aSerial = @pSerial
				
					UPDATE dbo.T_TblPcInventory			
					SET
						mPcNo = @pPcNo,
						mCnt = @pCnt
					WHERE  mSerialNo =@pSerial				
		
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 7	
						GOTO T_END		
					END		
				END
				ELSE
				BEGIN
		
					UPDATE dbo.T_TblPcInventory			
					SET
						mCnt = @aCntRest
					WHERE  mSerialNo =@pSerial				
		
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 8	
						GOTO T_END		
					END			
				
					EXEC @aSerial =  dbo.UspGetItemSerial 	-- ??? ?? 
					IF @aSerial <= 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 7		
						GOTO T_END		
					END		
				
					INSERT dbo.T_TblPcInventory(				-- ??? ??? ???? ???, ??? ?? 
						[mSerialNo],
						[mPcNo], 
						[mItemNo], 
						[mEndDate], 
						[mIsConfirm], 
						[mStatus], 
						[mCnt], 
						[mCntUse],
						[mOwner],
						[mPracticalPeriod]
						)
					VALUES(
						@aSerial,
						@pPcNo, 
						@pItemNo, 
						@aEndDay, 
						@pIsConfirm, 
						@pStatus, 
						@pCnt, 
						@pCntUse,
						@aOwner,
						@pPracticalPeriod
						)
					
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN				
						ROLLBACK TRAN
						SET @aErr = 9
						GOTO T_END		
					END		
				END
			END 
			ELSE
			BEGIN
				EXEC @aSerial =  dbo.UspGetItemSerial 	-- ??? ?? 
				IF @aSerial <= 0
				BEGIN
					ROLLBACK TRAN
					SET @aErr = 7		
					GOTO T_END		
				END		
				INSERT dbo.T_TblPcInventory(				
					[mSerialNo],
					[mPcNo], 
					[mItemNo], 
					[mEndDate], 
					[mIsConfirm], 
					[mStatus], 
					[mCnt], 
					[mCntUse],
					[mOwner],
					[mPracticalPeriod]
					)
				VALUES(
					@aSerial,
					@pPcNo, 
					@pItemNo, 
					@aEndDay, 
					@pIsConfirm, 
					@pStatus, 
					@pCnt, 
					@pCntUse,
					@aOwner,
					@pPracticalPeriod
					)
					
				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF  @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN				
					ROLLBACK TRAN
					SET @aErr = 10
					GOTO T_END		
				END		
			END			
		END
	COMMIT TRAN
T_END:
	SELECT @aErr, @aSerial AS Serial, DATEDIFF(mi,GETDATE(),@aEndDay)

GO

