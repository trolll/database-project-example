
/******************************************************************************  
**  Name: UspPopItem  
**  Desc:   
**  
**  Auth:   
**  Date:   
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  2015.03.26 捞侩林    辑锅飘 力芭 眠啊, 俊矾绰 30何磐 荤侩  
**  2017.01.06 傍籍痹    瘤开函荐(@aCnt) 悼扁拳 巩力肺 牢茄 绢轰隆 荐沥
**  2017.02.01 巢己葛	 绢轰隆 荐沥(@pIsStack 眠啊)
**  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspPopItem]  
  @pPcNo   INT     -- 酒捞袍 脚 家蜡磊.  
 ,@pSerial  BIGINT  
 ,@pCnt   INT     -- 滚副 俺荐.  
 ,@pCachingCnt INT     -- 滚副 俺荐甫 器窃茄 caching等 啊皑 俺荐.  
 ,@pIsSpend  TINYINT    -- 曼捞搁 荤扼咙.  
 ,@pIsStack		BIT
 ,@pSerialNew BIGINT OUTPUT  -- 老何父 滚赴版快 滚妨柳 酒捞袍狼 SN.  
AS  
 SET NOCOUNT ON  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   
    
 DECLARE @aErrNo  INT  
 DECLARE @aRowCnt INT  
 DECLARE @aCnt  INT  
 DECLARE @aIsSeizure BIT  
 DECLARE @aItemNo  INT  
 DECLARE @aEndDate SMALLDATETIME  
 DECLARE @aIsConfirm BIT  
 DECLARE @aStatus  TINYINT  
 DECLARE  @pRegDate  DATETIME  
 DECLARE @aOwner  INT  
 DECLARE @aPracticalPeriod INT,  
   @aBindingType TINYINT  
  
   
 -----------------------------------------------  
 -- 函荐 檬扁拳  
 -----------------------------------------------  
 SELECT   @aErrNo = 0, @aRowCnt = 0  
  
  
 -----------------------------------------------  
 -- 扁粮 酒捞袍 沥焊 犬牢(矫府倔 酒捞袍 沥焊 犬牢, 拘幅, 瞒皑 酒捞袍 犬牢)  
 -----------------------------------------------  
 SELECT   
  @aCnt= mCnt,   
  @aIsSeizure = mIsSeizure,   
  @aItemNo = mItemNo,   
  @aEndDate = mEndDate,   
  @aIsConfirm= mIsConfirm,   
  @aStatus= mStatus,  
  @aOwner = mOwner,  
  @aPracticalPeriod = mPracticalPeriod,  
  @aBindingType = mBindingType  
 FROM dbo.TblPcInventory  
 WHERE mSerialNo = @pSerial  
   
 SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  
 IF @aErrNo <> 0 OR @aRowCnt = 0  
 BEGIN  
  RETURN(1)   -- ERR : 1 , 贸府等 单捞鸥啊 绝芭唱, SQL INTERNAL ERROR    
 END    
 IF(0 <> @aIsSeizure)  
 BEGIN  
  RETURN(11)   -- ERR : 11 ,  拘幅等 酒捞袍篮 牢亥配府俊辑 波尘 荐 绝促  
 END  

 --------------------------------------------------------------  
 -- 2017/02/01 stack 酒捞袍 犬牢   
 -------------------------------------------------------------- 
 IF( (@pIsStack = 0) AND (@aCnt > 1) )
 BEGIN
	RETURN(13)
 END   

 SET @aCnt = @aCnt + @pCachingCnt  
 IF(@aCnt < 0)  
 BEGIN  
  RETURN(2)   -- ERR : 2 ,  波尘荐 乐绰 酒捞袍捞 粮犁窍瘤 臼绰促.  
 END  
  
 -- 官牢爹 坷幅 眉农   
 IF @pIsSpend = 0 -- true : 家葛己??  
  AND @aBindingType IN ( 1, 2)  
 BEGIN  
  RETURN(12)  
 END  
  
 --------------------------------------------------------------  
 -- 矫府倔 沥焊甫 掘绰促.  
 --------------------------------------------------------------  
 IF ( @aCnt > 0 AND @pIsSpend = 0 )  
 BEGIN  
  SET @pRegDate = GETDATE()  
  EXEC @pSerialNew =  dbo.UspGetItemSerial   
  IF @pSerialNew <= 0  
  BEGIN  
   RETURN(7)  
  END  
 END    
 ELSE  
 BEGIN    
  SET @pSerialNew = @pSerial  
 END  
  
 --------------------------------------------------------------  
 -- 酒捞袍阑 捞悼   
 --------------------------------------------------------------  
 BEGIN TRAN  
  ------------------------------------------------------  
  -- 儡咯肮荐 绝促.  
  ------------------------------------------------------  
  IF(@aCnt < 1)   
  BEGIN  
	   IF(0 <> @pIsSpend) -- 荤扼瘤绰 酒捞袍 捞搁   
	   BEGIN  
			DELETE dbo.TblPcInventory   
			WHERE mSerialNo = @pSerial  
			SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  
			IF @aErrNo <> 0 OR  @aRowCnt = 0  
			BEGIN  
			 ROLLBACK TRAN  
			 RETURN(3)   
			END  
			 ------------------------------------------------------  
			 -- 辑锅飘牢 版快 沥焊 力芭  
			 ------------------------------------------------------  
			 EXEC @aErrNo = UspDeleteServant @pSerial  
			 IF @aErrNo <> 0  
			 BEGIN  
			  ROLLBACK TRAN  
			  RETURN(30)  
			 END  
			 ------------------------------------------------------ 
	   END  
	   ELSE  
	   BEGIN  
			UPDATE	dbo.TblPcInventory  
			SET		mPcNo = @pPcNo
			WHERE	mSerialNo  = @pSerial   

			SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  
			IF @aErrNo <> 0 OR  @aRowCnt = 0  
			BEGIN  
			 ROLLBACK TRAN  
			 RETURN(4)  
			END  
	   END  
     
	   COMMIT TRAN  
	   RETURN(0)   
END  
  ------------------------------------------------------  
  -- 儡咯肮荐 粮犁.  
  ------------------------------------------------------  
  UPDATE dbo.TblPcInventory   
  SET   
   mCnt = mCnt - @pCnt   
  WHERE mSerialNo  = @pSerial   
  SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  
  IF @aErrNo <> 0 OR  @aRowCnt = 0  
  BEGIN  
   ROLLBACK TRAN  
   RETURN(5)  
  END  
  IF (@pIsSpend = 0)  
  BEGIN  
   INSERT INTO dbo.TblPcInventory([mSerialNo], [mPcNo], [mItemNo], [mEndDate], [mIsConfirm], [mStatus], [mCnt], [mOwner], [mPracticalPeriod], mBindingType )  
   VALUES(  
    @pSerialNew,  
    @pPcNo,   
    @aItemNo,   
    @aEndDate,   
    @aIsConfirm,   
    @aStatus,   
    @pCnt,  
    @aOwner,  
    @aPracticalPeriod,  
    @aBindingType  
    )  
     
   SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  
   IF @aErrNo <> 0 OR  @aRowCnt = 0  
   BEGIN  
    ROLLBACK TRAN  
    RETURN(6)  
   END  
  END   
   
 COMMIT TRAN  
 RETURN(0)  


/******************************************************************************
**		Name: UspPushItem
**		Desc: 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	2011.07.14	沥柳龋				穿利屈 酒捞袍 粮犁咯何 何盒俊 (阁胶磐版林 钎(1032)绰 力寇) 眠啊 
**		2013.10.14	沥柳宽				蜡烹扁埃 盒窜困 眠啊
**		2015.03.23	捞侩林				悼矫俊 贸府 瞪 锭 @aCntRest肺 牢秦 单捞磐啊 坷堪瞪 荐 乐绢辑 mCnt - @pCnt 肺 函版
**		2016.06.14	沥柳龋				mRestoreCnt甫 荤侩茄促.
**		2017.02.01	巢己葛				stack 酒捞袍 绢轰隆 荐沥
*******************************************************************************/

GO

