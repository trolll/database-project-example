/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspCreateDisciple    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspCreateDisciple]
	@pMaster	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT,
			@aErrNo INT,
			@aRowCnt INT
	SELECT @aRv = 0, @aErrNo = 0, @aRowCnt = 0

	IF NOT EXISTS(	SELECT mNo 
					FROM dbo.TblPc 
					WHERE mNo = @pMaster)
	BEGIN
		RETURN(2) -- ???? ?? ?? PC??	
	END 
	
	IF EXISTS(	SELECT mMaster 
				FROM dbo.TblDisciple 
				WHERE mMaster = @pMaster)
	BEGIN
		RETURN(3)	-- ?? ???? ??
	END

	IF EXISTS(	SELECT mDisciple 
				FROM dbo.TblDiscipleMember 
				WHERE mDisciple = @pMaster )
	BEGIN
		RETURN(4)		-- ?? ??? ??? ??
	END


	BEGIN TRAN

		INSERT INTO dbo.TblDisciple (mMaster) 
		VALUES(@pMaster)	-- ???? ??
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR

		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)			
		END

		INSERT INTO dbo.TblDiscipleMember (mMaster, mDisciple, mType) 
		VALUES(@pMaster, @pMaster, 1)		-- ???? ??? ??
		SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR

		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)			
		END

	COMMIT TRAN
	RETURN(0)

GO

