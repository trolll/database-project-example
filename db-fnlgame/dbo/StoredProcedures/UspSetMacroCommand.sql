/******************************************************************************
**		Name: UspSetMacroCommand
**		Desc: ¸ÅÅ©·Î Ä¿¸Çµå ÀúÀå
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetMacroCommand]
	@pPcNo			INT
	,@pMacroID		TINYINT
	,@pOrderNo		TINYINT
	,@pRefType		TINYINT
	,@pRefID		INT
	,@pRefParameter BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	INSERT INTO dbo.TblMacroCommand (mPcNo, mMacroID, mOrderNo, mRefType, mRefID, mRefParm)
	VALUES (@pPcNo, @pMacroID, @pOrderNo, @pRefType, @pRefID, @pRefParameter)

GO

