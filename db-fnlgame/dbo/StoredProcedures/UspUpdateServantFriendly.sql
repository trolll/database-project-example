/******************************************************************************
**		Name: UspUpdateServantFriendly
**		Desc: ¼­¹øÆ® Ä£¹Ðµµ °»½Å
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateServantFriendly]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pFriendly		SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE dbo.TblPcServant 
	SET mFriendly = @pFriendly
	WHERE mPcNo = @pPcNo
	  AND mSerialNo = @pSerialNo

	SET NOCOUNT OFF;

GO

