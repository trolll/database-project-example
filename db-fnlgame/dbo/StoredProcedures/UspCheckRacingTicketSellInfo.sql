/******************************************************************************
**		Name: UspCheckRacingTicketSellInfo
**		Desc: 몬스터 경주 티켓 판매 정보
**
**		Auth: 정진호
**		Date: 2011-07-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckRacingTicketSellInfo]
	 @pPcNo		INT
	,@pParmNo	INT  -- Ticket 의 아이템 ID
AS 
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT	 b.mSerialNo
			,a.mPlace
			,a.mStage
			,c.mDividend
			,c.mWinnerNID
			,a.mNID
	FROM dbo.TblRacingTicket  AS a 
		RIGHT OUTER JOIN dbo.TblPcInventory    AS b 
		ON a.mSerialNo = b.mSerialNo
		LEFT OUTER JOIN dbo.TblRacingResult AS c
		ON a.mStage = c.mStage
	WHERE b.mPcNo = @pPcNo and b.mitemno = @pParmNo		
			AND b.mEndDate > GETDATE()

GO

