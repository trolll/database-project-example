/******************************************************************************  
**  Name: UspGetBeadListFromStore
**  Desc: 창고에 있는 아이템에 결합된 구슬 리스팅
**  
**  Return values:  
**   레코드셋
**   
**  Author: 김강호
**  Date: 2011-06-30
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**  2011 0803   김강호     SET ROWCOUNT 1500로 변경
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetBeadListFromStore]
  @pOwner  INT  
AS

 SET NOCOUNT ON  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
   
 SET ROWCOUNT 1500
  
  	SELECT 
	 a.mOwnerSerialNo
	,a.mItemNo
	,a.mBeadIndex
	,DATEDIFF(mi,GETDATE(),a.mEndDate) as mEndDateA
	,a.mCntUse
	FROM	
	TblPcBead a
	INNER JOIN dbo.TblPcStore b ON a.mOwnerSerialNo=b.mSerialNo
	AND a.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	AND b.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	where b.mUserNo = @pOwner
	ORDER BY a.mItemNo;

GO

