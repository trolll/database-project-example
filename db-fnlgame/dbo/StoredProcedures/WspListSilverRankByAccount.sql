CREATE PROCEDURE [dbo].[WspListSilverRankByAccount]
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		TOP 20
		mUserID
		, mCnt mSilver
	FROM (
		SELECT 
			T1.mUserNo,
			SUM(mCnt) mCnt
		FROM (
			SELECT mUserNo, mCnt 
			FROM dbo.TblPcStore WITH(INDEX=NC_TblPcStore_2)
			WHERE mItemNo = 409
				AND mCnt > 1000000
			UNION ALL
			SELECT T2.mOwner mUserNo, SUM(mCnt) mCnt 
			FROM dbo.TblPcInventory T1 WITH(INDEX=NC_TblPcInventory_2) 
				INNER JOIN dbo.TblPc T2
					ON T1.mPcNo = T2.mNo
			WHERE mItemNo = 409
				AND mPcNo > 1
				AND mCnt > 1000000
			GROUP BY T2.mOwner) T1
		GROUP BY T1.mUserNo ) T1
		INNER JOIN FNLAccount.dbo.TblUser T2
			ON T1.mUserNO = T2.mUserNO
	WHERE T1.mCnt > 1000000
	ORDER BY T1.mCnt DESC

GO

