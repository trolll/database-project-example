/******************************************************************************  
**  Name: UspReturnBead
**  Desc: 구슬 환원(언링크 후 아이템 생성)
**  
**                
**  Return values:  
**   0 : 성공
**   <>0 : 실패
**                
**  Author: 김강호
**  Date: 2011-06-14
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**  2011 0803    김강호   SET XACT_ABORT ON; 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspReturnBead]
	@pEquipSerialNo		BIGINT
	,@pBeadIndex		TINYINT
	,@pPcNo				INT
	,@pBeadSerialNo		BIGINT OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;
		
	DECLARE @aItemNo	INT;
	DECLARE @aEndDay	SMALLDATETIME;
	DECLARE @aCntUse	SMALLINT;
						   	
	SELECT @aItemNo = mItemNo, @aEndDay = mEndDate, @aCntUse = mCntUse 
	FROM dbo.TblPcBead
	WHERE mOwnerSerialNo = @pEquipSerialNo 
		and mBeadIndex = @pBeadIndex
		and getdate() < mEndDate;
	IF @@ERROR <> 0 OR @aItemNo IS NULL
	BEGIN
		RETURN 1;	-- 구슬이 없습니다.
	END
	
	DECLARE @aSerial BIGINT;
	
	BEGIN TRAN;
	
	EXEC @aSerial =  dbo.UspGetItemSerial  -- 시리얼 얻기   
	IF @aSerial <= 0  
	BEGIN  
		ROLLBACK TRAN  ;
		RETURN 2;	-- 시리얼을 생성할 수 없습니다.
	END    
	
	DELETE FROM dbo.TblPcBead
	WHERE mOwnerSerialNo = @pEquipSerialNo 
		and mBeadIndex = @pBeadIndex;
	IF @@ERROR <> 0 OR @@ROWCOUNT <> 1
	BEGIN
		ROLLBACK TRAN  ;
		return 3;	-- 구슬을 삭제할 수 없습니다.
	END
	
	INSERT dbo.TblPcInventory(
		[mSerialNo],  
		[mPcNo],   
		[mItemNo],   
		[mEndDate],   
		[mIsConfirm],   
		[mStatus],   
		[mCnt],   
		[mCntUse],  
		[mOwner],  
		[mPracticalPeriod], 
		[mBindingType] )  
	VALUES(  
		@aSerial,  
		@pPcNo,   
		@aItemNo,   
		@aEndDay,   
		1,	-- IsConfirm
		1,	-- Status
		1,	-- Cnt   
		@aCntUse,  
		0,	-- owner
		0,	-- PracticalPeriod
		0);  -- BindingType
	if @@ERROR <> 0 OR @@ROWCOUNT = 0
	BEGIN
		ROLLBACK TRAN  ;
		RETURN 4;	-- 아이템을 추가할 수 없습니다.
	END
	
	SET @pBeadSerialNo = @aSerial;
	
	COMMIT TRAN;
		
	RETURN 0;

GO

