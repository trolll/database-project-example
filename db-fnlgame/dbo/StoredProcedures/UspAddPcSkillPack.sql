/******************************************************************************
**		Name: UspAddPcSkillPack
**		Desc: ½ºÅ³ ÆÑ Ãß°¡
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20151117	ÀÌ¿ëÁÖ				@pLeftTick ¹ÝÈ¯ ¼öÁ¤
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddPcSkillPack]
	@pPcNo		INT,
	@pSPID		INT,
	@pValidDay	INT,
	@pLeftTick INT OUTPUT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, ¹ö·ÁÁø ¾ÆÀÌÅÛ

	-- ÀÌ¹Ì ÀÖ´Ù¸é ¸¸·á ³¯Â¥¸¸ ¼¼ÆÃÇÏ°í Ãß°¡ ¼º°øÀ¸·Î °£ÁÖÇÑ´Ù
	DECLARE @aSPID INT
	SELECT @aSPID = mSPID FROM dbo.TblPcSkillPackInventory
	WHERE mPcNo = @pPcNo AND mSPID = @pSPID

	DECLARE	 @aEndDay		SMALLDATETIME
	SELECT	 @aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)

	IF @aSPID IS NOT NULL
	BEGIN
		UPDATE dbo.TblPcSkillPackInventory
		SET mEndDate = @aEndDay
		WHERE mPcNo = @pPcNo AND mSPID = @pSPID
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblPcSkillPackInventory
		VALUES (DEFAULT, @pPcNo, @pSPID, @aEndDay)
	END

	IF @@ERROR <> 0
	BEGIN
		RETURN 1 -- DB ¿¡·¯
	END

	SET @pLeftTick = DATEDIFF(MI, GETDATE(), @aEndDay)

	RETURN 0

GO

