/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.UspSetLetterLimit    Script Date: 2009-5-12 9:18:18 ******/
CREATE PROCEDURE [dbo].[UspSetLetterLimit]
	 @pIsOn			BIT
	 ,@pPcNo		INT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	


	UPDATE dbo.TblPcState 
	SET 
		[mIsLetterLimit]=@pIsOn
	WHERE [mNo] = @pPcNo
	
	IF(0 = @@ROWCOUNT)
	BEGIN
		SET @aErrNo	= 1 -- PC啊 粮犁窍瘤 臼澜
	END
	
	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo	= 2	-- DB Err
	END	

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

