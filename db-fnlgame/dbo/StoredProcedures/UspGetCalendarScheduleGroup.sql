/******************************************************************************
**		Name: UspGetCalendarScheduleGroup
**		Desc: ÀÏÁ¤¿¡ µî·ÏµÈ ±×·ì¸ñ·Ï
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-04-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetCalendarScheduleGroup]
	@pSerialNo			BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mGroupNo
	FROM dbo.TblCalendarSchedulegroup
	WHERE mSerialNo = @pSerialNo

GO

