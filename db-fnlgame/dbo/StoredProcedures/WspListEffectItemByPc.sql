/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2009-5-12 9:18:17 ******/  
  
/****** Object:  Stored Procedure dbo.WspListEffectItemByPc    Script Date: 2008-11-10 10:37:18 ******/  
  
--FNLGame1  
CREATE PROCEDURE [dbo].[WspListEffectItemByPc]  
 @mCharNm VARCHAR(12)   
AS  
 SET NOCOUNT ON  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   
 SET ROWCOUNT 1000   
  
 SELECT * FROM (  
  SELECT  T1.mPcNo  
    ,T3.IName  
    ,T1.mItemNo  
    ,null mSerialNo  
    ,T1.mEndDate  
    ,T1.mRegDate  
  FROM  dbo.TblPcGoldItemEffect T1   
   INNER JOIN [FNLParm].dbo.DT_Item AS T3  
    ON T1.mItemNo = T3.IID  
     AND T3.iischarge=1  
   INNER JOIN dbo.TblPc T4  
    ON T1.mPcNo = T4.mNo  
     AND T4.mNm = @mCharNm  
  WHERE  T1.mEndDate > CONVERT(SMALLDATETIME, GETDATE())  
  UNION ALL  
  SELECT  T2.mPcNo  
    ,T3.IName  
    ,T2.mItemNo  
    ,T2.mSerialNo  
    ,T2.mEndDate  
    ,T2.mRegDate  
  FROM  dbo.TblPcInventory T2   
   INNER JOIN [FNLParm].dbo.DT_Item AS T3  
    ON T2.mItemNo = T3.IID  
     AND T3.iischarge=1  
   INNER JOIN dbo.TblPc T4  
    ON T2.mPcNo = T4.mNo  
     AND T4.mNm = @mCharNm  
  WHERE  T2.mEndDate > CONVERT(SMALLDATETIME, GETDATE())  
 ) T1  
 ORDER BY T1.mRegDate ASC

GO

