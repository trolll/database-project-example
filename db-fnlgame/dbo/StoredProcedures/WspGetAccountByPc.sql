/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2009-7-14 13:13:29 ******/



/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2009-5-12 9:18:16 ******/  
  
/****** Object:  Stored Procedure dbo.WspGetAccountByPc    Script Date: 2008-11-10 10:37:18 ******/  
  
  
  
  
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  
CREATE   PROCEDURE [dbo].[WspGetAccountByPc]  
 @mNm VARCHAR(12)   
AS  
 SET NOCOUNT ON  
  
 SELECT  
  u.mRegdate  
  , u.mUserNo  
  , u.mUserId  
  , ISNULL(u.mLoginTm, CAST(STR('2000')+'-'+STR('01')+'-'+STR('01') AS DATETIME)) AS mLoginTm  
  , ISNULL(u.mLogoutTm, CAST(STR('2000')+'-'+STR('01')+'-'+STR('01') AS DATETIME)) AS mLogoutTm  
 FROM  
  [FNLAccount].[dbo].[TblUser] AS u   
 INNER JOIN  
  [TblPc] AS p  
 ON  
  u.mUserNo = p.mOwner   
   
 WHERE  
  p.mNm = @mNm  
  AND p.mDelDate IS NULL

GO

