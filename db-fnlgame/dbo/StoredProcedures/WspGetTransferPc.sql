CREATE PROCEDURE [dbo].[WspGetTransferPc]
	@pPcNm	CHAR(12)
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		T2.mRegDate AS mTransferDate,
		T2.mStatus AS mTransferStatus,		
		T1.mIsOverlap,
		T1.mOLDPcNm,
		T1.mTrasnsferLv, 
		T1.mNm,
		T1.mNo,
		T3.mLevel,
		T3.mExp,
		T1.mRegDate,
		T1.mTrasnsferLv,
		T1.mSvrNo,
		T1.mOwner,
		T4.mUserID
	FROM dbo.T_TblPc T1 
		LEFT OUTER JOIN dbo.T_TblTransferPc T2
			ON T1.mNo = T2.mNo
		LEFT OUTER JOIN dbo.TblPcState T3
			ON T1.mNo = T3.mNo
		LEFT OUTER JOIN FNLAccount.dbo.TblUser T4
			ON T1.mOwner = T4.mUserNo
	WHERE mOLDPcNm = @pPcNm

GO

