CREATE PROCEDURE [dbo].[WspSetCharName]
	@pNo			INT,			-- ─│╕»┼═ ╣°╚ú
	@pOrgPcNm		CHAR(12),	-- ║»░µ└ⁿ └╠╕º
	@pChgPcNm		CHAR(12)	-- ║»░µ╚─ └╠╕º
AS
BEGIN
	SET NOCOUNT ON
	SET LOCK_TIMEOUT 2000
		
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErr = 0
		
	------------------------------------------------------------------------------		
	-- ║»░µ╟╥ ─│╕»┼═ ┴╕└τ┐⌐║╬ ├╝┼⌐
	------------------------------------------------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblPc WITH(NOLOCK)
				WHERE mNm = @pChgPcNm)
	BEGIN			
		RETURN(1)					-- ║»░µ╟╥ ─│╕»┼═░í ┴╕└τ╟╤┤┘.
	END
		
	------------------------------------------------------------------------------		
	-- ─│╕»┼═╕φ ║»░µ
	------------------------------------------------------------------------------
	BEGIN TRAN
	
		UPDATE dbo.TblPc
		SET mNm = @pChgPcNm
		WHERE mNo =@pNo 
		
		SELECT	@pRowCnt = @@ROWCOUNT, 
				@pErr  = @@ERROR			

		IF @pRowCnt = 0
		BEGIN
			ROLLBACK TRAN			-- ─│╕»┼═░í ┴╕└τ╟╧┴÷ ╛╩░┼│¬, ░╗╜┼╡╚ ┴ñ║╕░í ╛°┤┘
			RETURN(2)			
		END 

		IF @pErr <> 0
		BEGIN
			ROLLBACK TRAN			
			RETURN(@pErr)			-- SQL System Error		
		END
	
	COMMIT TRAN
	RETURN(0)
END

GO

