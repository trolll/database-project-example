/******************************************************************************  
**  File: UspGetLimitedResource.sql  
**  Name: UspGetLimitedResource  
**  Desc: 리소스아이템을 가져온다  
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**	2012.06.11	정진호		mIsIncSys 컬럼 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetLimitedResource]
AS
	SET NOCOUNT ON
	
	SELECT 
		mRegDate,
		mResourceType,
		mMaxCnt,
		mRemainerCnt,
		mRandomVal,
		mUptDate,	
		mIsIncSys
	FROM dbo.TblLimitedResource

GO

