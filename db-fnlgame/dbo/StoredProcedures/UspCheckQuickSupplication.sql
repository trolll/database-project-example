/******************************************************************************
**		Name: UspCheckQuickSupplication
**		Desc: 신고함의 내용을 가져온다.
**
**		Auth: 김광섭
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2012-04-10	공석규				러시아 문자열 오류로 인해 mSupplication RTRIM
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckQuickSupplication]
	  @pPcNo		INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 1

	DECLARE		@aErrNo	INT
	DECLARE		@aQSID	BIGINT
	DECLARE		@aText	VARCHAR(1000)
	DECLARE		@aStatus	TINYINT
	DECLARE		@aCate	TINYINT
	DECLARE		@aDate	DATETIME

	SELECt	@aErrNo = 0, @aQSID = 0

	IF NOT EXISTS (	SELECT * 
					FROM TblQuickSupplicationState 
					WHERE mPcNo = @pPcNo 
						AND mStatus < 2 )
	BEGIN		
		SET @aErrNo = 1	-- PC 가 확인하지 않은 질문이 없다.
		GOTO T_END
	END

	SELECT
		  @aDate =   T1.mRegDate
		, @aQSID = T1.mQSID
		, @aText = T2.mSupplication
		, @aStatus = T1.mStatus
		, @aCate = T1.mCategory
	FROM dbo.TblQuickSupplicationState		T1
		INNER JOIN	dbo.TblQuickSupplication		T2
			ON T1.mQSID = T2.mQSID
	WHERE mPcNo = @pPcNo 
		AND mStatus < 2 
	ORDER BY T1.mQSID DESC

T_END:
	SELECT	@aErrNo, @aDate, @aQSID, RTRIM(@aText), @aStatus, @aCate

GO

