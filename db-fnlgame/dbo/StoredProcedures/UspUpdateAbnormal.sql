/****** Object:  Stored Procedure dbo.UspUpdateAbnormal    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspUpdateAbnormal    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspUpdateAbnormal    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspUpdateAbnormal    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.UspUpdateAbnormal    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspUpdateAbnormal    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspUpdateAbnormal    Script Date: 2009-11-16 10:23:29 ******/
/******************************************************************************
**		Name: UspUpdateAbnormal
**		Desc: 
**
**		Auth:
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 2009-05-09		Author:	? ??
**		Description: INSERT ?? ??. (mRestoreCnt)
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateAbnormal]
	 @pPcNo			INT
	,@pAbnormalNo	INT	 
	,@pLeftTime		INT
	,@pAbParmNo		INT		-- ????? ???? ?.
	,@pRestoreCnt	TINYINT
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	INSERT INTO dbo.TblPcAbnormal([mPcNo], [mParmNo], [mLeftTime], [mAbParmNo], [mRestoreCnt])
		VALUES(@pPcNo, @pAbnormalNo, @pLeftTime, @pAbParmNo, @pRestoreCnt)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END
	 END

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

