/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetRacingTicketInfo    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspGetRacingTicketInfo]
	 @pSerialNo		BIGINT
	,@pPlace		INT		OUTPUT
	,@pStage		INT		OUTPUT
	,@pNID			INT		OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	SET		@pPlace		= 0
	SET		@pStage		= 0
	SET		@pNID			= 0	-- 0 : NID? ??? (??)

	SELECT @pPlace = mPlace, @pStage = mStage, @pNID = mNID FROM dbo.TblRacingTicket WHERE mSerialNo = @pSerialNo

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

