/******************************************************************************  
**  Name: UspSetGuildMark  
**  Desc: 길드 마크 갱신  
**  
**                
**  Return values:  
**   0 : 작업 처리 성공  
**   > 0 : SQL Error  
**                
**  Auth: JUDY  
**  Date: 2006-10-24  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  2008.01.08 JUDY    길드 마크 업데이트 날짜 추가     
**  2010.07.30 김강호  길드마크 업데이트  
**  2010.10.11 김강호  @pMarkSeq를 OUTPUT 타입으로 변경
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetGuildMark]  
  @pGuildNo  INT  
 ,@pMarkSeq  INT OUTPUT  
 ,@pGuildMark BINARY(1784) -- eSzGuildMark와 연결.  
AS  
  SET NOCOUNT ON;   
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	DECLARE @mGuildMarkUptDate DATETIME  
	DECLARE @curDate DATETIME 
	DECLARE @dateDiff INT  
	DECLARE @mGuildMark BINARY(1784) -- eSzGuildMark와 연결.  

	SELECT 
		@mGuildMarkUptDate = mGuildMarkUptDate,
		@mGuildMark = mGuildMark
	FROM dbo.TblGuild    
	WHERE mGuildNo=@pGuildNo  

	IF @mGuildMarkUptDate IS NULL  
	BEGIN  
		RETURN(1) -- 길드 정보에 문제가 있다.  
	END  

	SET @curDate = GETDATE()  
	SELECT @dateDiff = DATEDIFF(minute, @mGuildMarkUptDate, @curDate)   

	IF @dateDiff < 1 AND @mGuildMark IS NOT NULL  
	BEGIN  
		RETURN(2) -- 너무 자주 변경할 수 없습니다.  
	END  

	DECLARE @aNewMarkSeq INT
	SET @aNewMarkSeq = DATEPART(year, @curDate)*1048576 + 
						DATEPART(month, @curDate)*65536 + 
						DATEPART(day, @curDate)*2048 + 
						DATEPART(hour, @curDate)*64 + 
						DATEPART(minute, @curDate)

	UPDATE dbo.TblGuild   
	SET   
		mGuildSeqNo = @aNewMarkSeq,  
		mGuildMark = @pGuildMark,
		mGuildMarkUptDate = @curDate
	WHERE mGuildNo = @pGuildNo  

	IF @@ERROR <> 0   
	BEGIN  
		RETURN(3) -- 업데이트 실패  
	END  

	SET @pMarkSeq =   @aNewMarkSeq 

	RETURN(0) -- NOT ERROR

GO

