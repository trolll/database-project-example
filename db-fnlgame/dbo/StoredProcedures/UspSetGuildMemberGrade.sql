/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildMemberGrade    Script Date: 2008-11-10 10:37:20 ******/



CREATE PROCEDURE [dbo].[UspSetGuildMemberGrade]
	  @pGuildNo	INT
	, @pPcNo		INT
	, @pLv		TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	 @aRowCnt		INT
	DECLARE  @aErr			INT

	SELECT  @aRowCnt = 0, @aErr = 0
	
	
	UPDATE dbo.TblGuildMember 
	SET mGuildGrade = @pLv 
	WHERE mGuildNo = @pGuildNo 
		AND mPcNo = @pPcNo
		
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT	

	IF (@aErr <> 0)
		RETURN(2)	-- db error 
		
	IF (@aRowCnt <= 0)	
		RETURN(1)	-- none row 
	
	RETURN(0)

GO

