/******************************************************************************
**		Name: UspGetClickPcCalendarSchedule
**		Desc: ÀÏÁ¤ Å¬¸¯½Ã Á¤º¸
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetClickPcCalendarSchedule]
	 @pSerialNo			BIGINT
	,@pOwnerPcNm		CHAR(12)		OUTPUT
	,@pTitle			CHAR(30)		OUTPUT
	,@pMsg				VARCHAR(512)	OUTPUT
	,@pScheduleDate		SMALLDATETIME	OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		 @pOwnerPcNm = T2.mNm
		,@pTitle = T1.mTitle
		,@pMsg = T1.mMsg
		,@pScheduleDate = mScheduleDate
	FROM dbo.TblCalendarSchedule T1
		INNER JOIN dbo.TblPc T2
	ON T1.mOwnerPcNo = T2.mNo
	WHERE T1.mSerialNo = @pSerialNo

	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0
	BEGIN
		RETURN(1);
	END

	RETURN(0);

GO

