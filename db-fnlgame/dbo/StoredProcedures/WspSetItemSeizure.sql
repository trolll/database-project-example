CREATE PROCEDURE [dbo].[WspSetItemSeizure]
	@mSerialNo	BIGINT, -- ЅГё®ѕу
	@mIsSeizure BIT,	-- ѕР·щї©єО
	@mPos		CHAR(1)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @mPos = 'i' 
	BEGIN
		UPDATE dbo.TblPcInventory
		SET
			mIsSeizure = @mIsSeizure
		WHERE mSerialNo = @mSerialNo
	END
	
	IF @mPos = 's' 
	BEGIN
		UPDATE dbo.TblPcStore
		SET
			mIsSeizure = @mIsSeizure
		WHERE mSerialNo = @mSerialNo	
	END 
	
	IF @mPos = 'g' 
	BEGIN
		UPDATE dbo.TblGuildStore
		SET
			mIsSeizure = @mIsSeizure
		WHERE mSerialNo = @mSerialNo	
	END 	

	IF @@ERROR <> 0
		RETURN 1
	
	RETURN 0

GO

