/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspGetAllPcByNo    Script Date: 2008-11-10 10:37:19 ******/





CREATE Procedure [dbo].[UspGetAllPcByNo]
	 @pmNo			INT
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	SELECT 
		TblPc.mNo, 
		TblPc.mNm, 
		TblPc.mClass, 
		TblPc.mHead, 
		TblPc.mFace, 
		TblPc.mBody, 
		TblPc.mHomeMapNo, 
		TblPc.mHomePosX, 
		TblPc.mHomePosY, 
		TblPc.mHomePosZ, 
		TblPcState.mLevel, 
		TblPcState.mExp, 
		TblPcState.mHp, 
		TblPcState.mMp, 
		TblPcState.mStomach
	FROM dbo.TblPc  WITH(NOLOCK)
		INNER JOIN dbo.TblPcState  WITH(NOLOCK)
		ON  TblPc.mNo = TblPcState.mNo
	WHERE TblPc.mNo = @pmNo
	
	IF(1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

