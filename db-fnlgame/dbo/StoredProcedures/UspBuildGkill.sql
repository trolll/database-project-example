/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2009-6-1 15:32:38 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2009-5-12 9:18:17 ******/

/****** Object:  Stored Procedure dbo.UspBuildGkill    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspBuildGkill]
	 @pGuildNo	INT
	,@pPlace	INT			-- EPlace.
	,@pNode		INT			-- EGkillNode
	,@pPcNo		INT
	,@pBuildStx	SMALLDATETIME OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	SET @pBuildStx = GETDATE()	
	-------------------------------------------------------------------------------------
	-- PC? ??? ?? ?? ???? ?? 
	-------------------------------------------------------------------------------------	
	IF EXISTS(	SELECT *
				FROM dbo.TblGkill 
				WHERE mPcNo = @pPcNo )
	BEGIN
		RETURN(2)	--	?? PC? ??? ?? ????? ????.	
	END 				
		
	-------------------------------------------------------------------------------------
	-- ?? ?? ?? ??
	-------------------------------------------------------------------------------------	
	INSERT TblGkill([mRegDate], [mGuildNo], [mPlace], [mNode], [mPcNo])
			VALUES(@pBuildStx, @pGuildNo, @pPlace, @pNode, @pPcNo)
	IF(@@ERROR <> 0)
	 BEGIN
		RETURN(1)	--  ?? ?? ?? ?? ?? 
	 END

	SET NOCOUNT OFF
	RETURN(0)

GO

