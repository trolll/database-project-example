/******************************************************************************  
**  Name: UspSetStatisticsPShopExchange  
**  Desc: 개인상점 테이블에 통계내역을 입력  
**  
**  Auth: 정구진  
**  Date: 09.05.07  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:         Author:    Description:  
**  --------      --------   ---------------------------------------  
**   2010.12.07.  김강호      mItemStatus,  mTradeType 추가 
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetStatisticsPShopExchange]  
   @pItemNo    INT  
 , @pExchangeCntBuy  BIGINT  
 , @pExchangeCntSell  BIGINT  
 , @pTotalPriceBuy  BIGINT  
 , @pTotalPriceSell  BIGINT  
 , @pMinPriceBuy   BIGINT  
 , @pMinPriceSell  BIGINT  
 , @pMaxPriceBuy   BIGINT  
 , @pMaxPriceSell  BIGINT  
 , @pItemStatus SMALLINT
 , @pTradeType SMALLINT
AS  
 SET NOCOUNT ON  ;
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
   
 DECLARE  @aErrNo  INT  ;
 SET @aErrNo = 0  ;
  
 INSERT INTO dbo.TblStatisticsPShopExchange  
  ([mItemNo], [mBuyCount], [mSellCount], [mBuyTotalPrice], [mSellTotalPrice],   
  [mBuyMinPrice], [mSellMinPrice], [mBuyMaxPrice], [mSellMaxPrice]
  , mItemStatus, mTradeType)  
 VALUES  
  (@pItemNo, @pExchangeCntBuy, @pExchangeCntSell, @pTotalPriceBuy, @pTotalPriceSell,   
  @pMinPriceBuy, @pMinPriceSell, @pMaxPriceBuy, @pMaxPriceSell
  , @pItemStatus, @pTradeType);
         
 IF @@ERROR <> 0  
 BEGIN
  SET @aErrNo = 2  ;
 END
                
 RETURN(@aErrNo)  ;

GO

