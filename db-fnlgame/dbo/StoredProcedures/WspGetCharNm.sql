/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2009-7-14 13:13:32 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2009-5-12 9:18:19 ******/

/****** Object:  Stored Procedure dbo.WspGetCharNm    Script Date: 2008-11-10 10:37:20 ******/


CREATE PROCEDURE [dbo].[WspGetCharNm]
	@mOwner int, @mNm char(12) OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT @mNm = mNm
	FROM TBLPC a
	inner join tblpcstate b
	on a.mNo = b.mNo
	WHERE a.mOwner = @mOwner
	and b.mLoginTm > b.mLogoutTm

GO

