CREATE PROCEDURE [dbo].[WspSetGuildMark]  	
	 @pGuildNo		INT
	,@pGuildMark	BINARY(1784)	-- eSzGuildMark와 연결.
AS
	SET NOCOUNT ON			
		
	UPDATE dbo.TblGuild 
	SET 
		mGuildSeqNo = mGuildSeqNo+1, 
		mGuildMark = @pGuildMark,
		mGuildMarkUptDate = GETDATE()
	WHERE mGuildNo = @pGuildNo

	IF @@ERROR <> 0 
		RETURN(1)
	
	RETURN(0)

GO

