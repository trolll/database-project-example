/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2009-7-14 13:13:32 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2009-5-12 9:18:19 ******/

/****** Object:  Stored Procedure dbo.WspInitPassword    Script Date: 2008-11-10 10:37:20 ******/

CREATE PROCEDURE  [dbo].[WspInitPassword]
	 @pGuildNo	INT
	,@pGuildNm	CHAR(12)	
	,@pOperator	CHAR(20)	
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000	
	
	DECLARE @pRowCnt	INT
	SELECT	@pRowCnt = 0
	BEGIN TRAN
			
		DELETE dbo.TblGuildStorePassword	
		--modified by mengdan 2008-6-5
		--SET  mPassword = '1234'
		WHERE  [mGuildNo] = @pGuildNo

		SELECT	@pRowCnt = @@ROWCOUNT
		IF @pRowCnt = 0
		BEGIN				
			ROLLBACK TRAN		
			RETURN(1)
		END
		
		IF @@ERROR <> 0
		BEGIN				
			ROLLBACK TRAN			
			RETURN(@@ERROR)		
		END
		
		INSERT INTO 
			dbo.TblGuildStorePasswordHistory
			(mRegDate,
			 mGuildNo,
			mOperator)
		VALUES
			(GETDATE(),
			 @pGuildNo,
			@pOperator)
		IF @@ERROR <> 0
		BEGIN				
			ROLLBACK TRAN			
			RETURN(@@ERROR)		
		END
	
	COMMIT TRAN
	RETURN(0)

GO

