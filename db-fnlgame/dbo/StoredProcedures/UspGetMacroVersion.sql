/******************************************************************************
**		Name: UspGetMacroVersion
**		Desc: ¸ÅÅ©·Î ¹öÀü
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMacroVersion]
	@pPcNo INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mVersion
	FROM dbo.TblMacroContentVersion
	WHERE mPcNo = @pPcNo

GO

