/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2009-7-14 13:13:32 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2009-5-12 9:18:19 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePos    Script Date: 2008-11-10 10:37:20 ******/





CREATE Procedure [dbo].[UspUpdatePos]
	 @pPcNo			INT
	,@pHp			INT
	,@pMp			INT
	,@pMapNo		INT
	,@pPosX			REAL
	,@pPosY			REAL
	,@pPosZ			REAL
	,@pStomach		SMALLINT	 
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	-- ID? ???? ??(2601)? ???.
	UPDATE dbo.TblPcState 
	SET [mHp]=@pHp, [mMp]=@pMp, [mMapNo]=@pMapNo, [mPosX]=@pPosX, [mPosY]=@pPosY, 
	  [mPosZ]=@pPosZ, [mStomach]=@pStomach
	WHERE [mNo] = @pPcNo
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END	 
	 END	

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

