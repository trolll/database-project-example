/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2011-4-19 15:24:33 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2010-3-22 15:58:16 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspSetItemCnt    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspSetItemCnt]
	 @pItemNo				INT
	,@pCntNewMerchant		BIGINT	-- ??? ? ??.
	,@pCntDelMerchant		BIGINT	-- ??? ? ??.
	,@pCntNewReinforce		BIGINT	-- ??? ??? ??.
	,@pCntDelReinforce		BIGINT	-- ??? ??? ??.
	,@pCntNewCrafting		BIGINT	-- crafting?? ??? ??.
	,@pCntDelCrafting		BIGINT	-- crafting?? ??? ??.	
	,@pCntNewNonPc			BIGINT	-- NON-PC? drop? ??.
	,@pCntDelNonPc			BIGINT	-- NON-PC? ???? ??.
	,@pCntUsePc				BIGINT	-- PC? ??? ??.
	,@pCntUseNonPc			BIGINT	-- NON-PC? ??? ??.
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
		
	DECLARE	@aDate	SMALLDATETIME
	SET @aDate = CONVERT(VARCHAR(30), GETDATE(), 102)
	SET @aDate = DATEADD(dd, -1, @aDate)
	
	--????? ??? ?? ??? ???? rollback? ???? ???? tran? ??? ? ??.
	--IF(0 <> @@TRANCOUNT) ROLLBACK
	SET XACT_ABORT ON	
	BEGIN TRAN
	
	-- ?? ??? ???? ?? ???? logout?? ??? ??? ???? ?? ? ??.
	IF NOT EXISTS(SELECT * FROM TblStatisticsItem WHERE (@aDate = [mRegDate]) AND (@pItemNo = [mItemNo]))
	 BEGIN
		INSERT INTO TblStatisticsItem([mRegDate], [mItemNo]) VALUES(@aDate, @pItemNo)
		IF(0 <> @@ERROR)
		 BEGIN
			SET @aErrNo = 1
			GOTO LABEL_END		 
		 END
	 END
	
	UPDATE TblStatisticsItem SET [mNewMerchantCnt]=@pCntNewMerchant,
								 [mDelMerchantCnt]=@pCntDelMerchant,
								 [mNewReinforceCnt]=@pCntNewReinforce,
								 [mDelReinforceCnt]=@pCntDelReinforce,
								 [mNewCraftingCnt]=@pCntNewCrafting,
								 [mDelCraftingCnt]=@pCntDelCrafting,
								 [mNewNonPcCnt]=@pCntNewNonPc,
								 [mDelNonPcCnt]=@pCntDelNonPc,
								 [mUsePcCnt]=@pCntUsePc,
								 [mUseNonPcCnt]=@pCntUseNonPc
		WHERE (@aDate = [mRegDate]) AND (@pItemNo = [mItemNo])
	IF(0 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo = 2
		GOTO LABEL_END
	 END		
													 
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
	SET NOCOUNT OFF

GO

