/******************************************************************************  
**  Name: UspGetStatisticsPShopExchange  
**  Desc: 개인상점 테이블에 통계내역을 로드  
**  
**  Auth: 정구진  
**  Date: 09.05.07  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  김강호      mItemStatus,  mTradeType 추가 
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetStatisticsPShopExchange]  
AS  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
 SELECT   
  mItemNo  
  , mBuyCount  
  , mSellCount  
  , mBuyTotalPrice  
  , mSellTotalPrice  
  , mBuyMinPrice  
  , mSellMinPrice  
  , mBuyMaxPrice  
  , mSellMaxPrice
  , mItemStatus
  , mTradeType
 FROM   
  dbo.TblStatisticsPShopExchange;

GO

