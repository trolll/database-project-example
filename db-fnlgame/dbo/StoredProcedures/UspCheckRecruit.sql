/****** Object:  Stored Procedure dbo.UspCheckRecruit    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspCheckRecruit    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspCheckRecruit    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspCheckRecruit    Script Date: 2010-12-23 17:45:58 ******/
/******************************************************************************
**		Name: UspCheckRecruit
**		Desc: 辨靛葛笼 矫胶袍 眉农.(林扁利栏肺? 12矫埃 付促 辑滚俊辑 犁 肺靛 茄促)
**
**		Auth: 沥柳龋
**		Date: 2009.09.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckRecruit]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	----------------------------------------------
	-- 扁茄捞 瘤抄 辨靛葛笼 沥焊 眉农
	----------------------------------------------
	SELECT
		mGuildNo
	FROM
		dbo.TblGuildRecruit
	WHERE
		mEndDate < GETDATE();

GO

