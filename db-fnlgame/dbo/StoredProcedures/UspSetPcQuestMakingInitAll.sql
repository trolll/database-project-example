/******************************************************************************
**		Name: UspSetPcQuestMakingInitAll
**		Desc: 檬扁拳 沥焊 悸泼
**
**		Auth: 沥柳龋
**		Date: 2010-04-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcQuestMakingInitAll]
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblPcState
	SET mQMCnt = 0, mGuildQMCnt = 0
	WHERE mNo in (
			SELECT T1.mNo
			FROM dbo.TblPc AS T1 
				INNER JOIN dbo.TblPcState AS T2
					ON T1.mNo = T2.mNo
			WHERE T1.mDelDate IS NULL
			)
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1);
	END
	 		 
	RETURN(0);

GO

