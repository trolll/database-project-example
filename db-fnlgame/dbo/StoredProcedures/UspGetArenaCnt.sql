/******************************************************************************
**		Name: UspGetArenaCnt
**		Desc: 酒饭唱 曼咯冉荐甫 掘绢柯促
**
**		Auth: 沥柳龋
**		Date: 2016-09-02
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetArenaCnt]
	 @pPcNo			INT 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
			 T1.mFierceCnt
			,T1.mBossCnt
    FROM dbo.TblPcState T1
    		INNER JOIN dbo.TblPc  T2 
			ON T1.mNo = T2.mNo 
	WHERE T1.mNo = @pPcNo

GO

