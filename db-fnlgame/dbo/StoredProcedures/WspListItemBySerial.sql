CREATE PROCEDURE [dbo].[WspListItemBySerial]  
 @mSerialString VARCHAR(500)  
AS  
 SET NOCOUNT ON   
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 DECLARE @sql NVARCHAR(2000)  
 SET @sql =  
 'SELECT  
  ( SELECT mUserId   
   FROM [FNLAccount].[dbo].[TblUser]   
   WHERE mUserNo = p.mOwner) AS mUserId  
  , p.mNm  
  , e.mSlot  
  , ii.IName  
  , i.mStatus  
  , DATEDIFF(DAY, GETDATE(), i.mEndDate) AS mEndDate  
  , i.mSerialNo  
  , i.mCnt  
  , i.mItemNo  
  , i.mIsSeizure  
  , p.mNo  
  , i.mPracticalPeriod  
  , i.mBindingType
  , i.mSerialNo
	, i.mIsConfirm --И®АО ї©єО (1Ає И®АО, 0Ає №МИ®АО)  
	  ,(  
	   SELECT COUNT(*)  
	   FROM FNLParm.dbo.DT_Item  
	   WHERE IID = i.mItemNo  
	   AND IType IN (1 ,2 ,3 ,6 ,7 ,8 ,17 ,18 ,20)  
	   AND i.mIsConfirm = 1  
	   AND i.mIsSeizure = 0  
	  ) AS mIsEquipBead --·й АеВш °ЎґЙї©єО  
	  , i.mHoleCount --ЅЅ·Ф°№јц  
 FROM   
  dbo.TblPc  p  
  INNER JOIN dbo.TblPcInventory i  
   ON i.mPcNo = p.mNo  
  INNER JOIN FNLParm.dbo.DT_Item  ii  
   ON i.mItemNo = ii.IID  
  LEFT OUTER JOIN dbo.TblPcEquip e  
   ON e.mOwner = p.mNo   
    AND e.mSerialNo = i.mSerialNo  
 WHERE i.mSerialNo IN (' + @mSerialString + ')  
 ORDER BY p.mNo'  
  
 EXECUTE sp_executesql @sql

GO

