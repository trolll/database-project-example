/******************************************************************************
**		Name: UspGetPcTeleport
**		Desc: 扁撅府胶飘 炼雀
**		Test:			
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      2013/04/24  巢己葛				mName -> 扁粮 char(20)俊辑 varchar(50)栏肺 荐沥			
**										int mType, int mLevel, varchar(50) mOrgName 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcTeleport]
	 @pPcNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		mNo, 
		RTRIM(mName), 
		mMapNo, 
		mPosX, 
		mPosY, 
		mPosZ,
		mType,
		mLevel,
		RTRIM(mOrgName)
	FROM dbo.TblPcTeleport
	WHERE mPcNo = @pPcNo
	ORDER BY mName ASC

GO

