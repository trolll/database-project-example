/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgit    Script Date: 2008-11-10 10:37:16 ******/





CREATE Procedure [dbo].[UspCheckOwnGuildAgit]
	 @pTerritory		INT OUTPUT			-- ???? [??/??]
	,@pOwnerGID		INT						-- ????
	,@pIsOwnGuildAgit	TINYINT OUTPUT		-- ????? ???? [??]
	,@pGuildAgitNo		INT OUTPUT			-- ??????? [??]
	,@pLeftMin		INT OUTPUT				-- ??? [??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	SET		@pIsOwnGuildAgit = 0
	SET		@pGuildAgitNo = 0
	SET		@pLeftMin = 0

	IF (@pOwnerGID = 0)
	BEGIN
		RETURN(0)
	END

	IF (@pTerritory <= 0)	-- ?? ????? 1???
	BEGIN
		
		SELECT 
			@pTerritory = mTerritory, 
			@pIsOwnGuildAgit = 1, 
			@pGuildAgitNo = mGuildAgitNo, 
			@pLeftMin = mLeftMin 
		FROM dbo.TblGuildAgit WITH (NOLOCK) 
		WHERE mOwnerGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(1)		-- 1: DB ???? ??
		END	 

		RETURN(0)
	END

	SELECT 
		@pTerritory = mTerritory, 
		@pIsOwnGuildAgit = 1, 
		@pGuildAgitNo = mGuildAgitNo, 
		@pLeftMin = mLeftMin 
	FROM dbo.TblGuildAgit WITH (NOLOCK) 
	WHERE mTerritory = @pTerritory 
		AND mOwnerGID = @pOwnerGID
		
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)		-- 1: DB ???? ??
	END	 

	RETURN(0)

GO

