/******************************************************************************
**		Name: UspGetPcQuestValue
**		Desc: 韤橃姢韸?歆勴枆臧掛潉 臧€鞝胳槰雼?
**
**		Auth: 鞝曥順?
**		Date: 2010-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspGetPcQuestValue]
	  @pPcNo		INT
	 ,@pQuestNo		INT
	 ,@pValue		INT	OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @pValue = 0;
			
    SELECT 
			@pValue = 
					CASE WHEN mValue IS NULL THEN 0 
						ELSE mValue	
					END 
    FROM dbo.TblPcQuest
    WHERE mPcNo = @pPcNo 
		AND mQuestNo = @pQuestNo

	RETURN(0)

GO

