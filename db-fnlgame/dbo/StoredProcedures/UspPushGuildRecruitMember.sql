/****** Object:  Stored Procedure dbo.UspPushGuildRecruitMember    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspPushGuildRecruitMember    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspPushGuildRecruitMember    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspPushGuildRecruitMember    Script Date: 2010-12-23 17:46:00 ******/
/******************************************************************************
**		Name: UspPushGuildRecruitMember
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩且 某腐磐甫 持绰促. 粮犁窍搁 诀单捞飘茄促.
**
**		Auth: 辫锐档, 沥柳龋
**		Date: 2009.07.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPushGuildRecruitMember]
	@pPcNo		INT,
	@pGuildNo	INT,
	@pState		TINYINT,
	@pPcMsg		VARCHAR(250),
	@pRegDate	DATETIME	OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aErrNo		INT,
			@aRowCnt	INT
			
	SELECT	@aErrNo = 0,
			@pRegDate = GETDATE(),
			@aRowCnt = 0

	IF NOT EXISTS ( SELECT  *
					FROM dbo.TblPc
					WHERE mNo = @pPcNo
						AND mDelDate IS NULL)
	BEGIN
		RETURN(2);		-- 某腐磐 沥焊啊 嘎瘤 臼绰促..** 坷幅蔼 馆券 且巴..
	END 


	----------------------------------------------
	-- 辨靛 府捻福泼 脚没牢 沥焊 诀单捞飘
	----------------------------------------------
	UPDATE dbo.TblGuildRecruitMemberList
	SET	mRegDate = @pRegDate,
		mGuildNo = @pGuildNo,
		mState = @pState,
		mPcMsg = @pPcMsg,
		mIsHide = 0
	WHERE [mPcNo] = @pPcNo
	
	SELECT  @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT;
	IF(0 <> @aErrNo)
	BEGIN
		RETURN(1)
	END
		
	----------------------------------------------
	-- 辨靛 府捻福泼 脚没牢 沥焊 积己
	----------------------------------------------
	IF @aRowCnt = 0 
	BEGIN
		INSERT INTO dbo.TblGuildRecruitMemberList( mRegDate, mPcNo, mGuildNo, mState, mPcMsg, mIsHide )
		VALUES( @pRegDate, @pPcNo, @pGuildNo, @pState, @pPcMsg, 0 )

		
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		IF (@aErrNo <> 0) OR (@aRowCnt <= 0)
		BEGIN
			RETURN(1)
		END
	END
		
	RETURN(0)

GO

