/******************************************************************************
**		Name: UspSetCastleStowerRuined
**		Desc: 스팟의 폐허 여부 상태를 변경한다.
**
**		Auth: 김강호
**		Date: 2009.10.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2011.11.15	정구진				폐허화 지역 몬스터 리스폰 여부
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspSetCastleStowerRuined]
	 @pPlace		INT
	,@pIsRuined		BIT		-- 폐허화 상태 여부?
	,@pIsRespawn	BIT		-- 몬스터 리스폰 여부
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
		
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	IF(0 = @pIsRuined)
	BEGIN
		DELETE FROM dbo.TblCastleTowerStoneRuined 
		WHERE mPlace=@pPlace
	END 
	ELSE
	BEGIN		
		IF NOT EXISTS ( SELECT * FROM dbo.TblCastleTowerStoneRuined WHERE mPlace = @pPlace )
		BEGIN
			INSERT INTO dbo.TblCastleTowerStoneRuined(mPlace, mRuinMonsterRespawn)
			VALUES(@pPlace, @pIsRespawn)
		END
	END

	SELECT @aErrNo = @@ERROR;

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

