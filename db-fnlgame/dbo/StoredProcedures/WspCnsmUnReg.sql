/******************************************************************************
**		Name: WspCnsmUnReg
**		Desc:  위탁상점 취소(운영툴에서는 압류시 위탁 상점의 아이템을 삭제하여 인벤토리에 재 등록 처리 한다)
**
**		Auth: JUDY
**		Date: 2011-01-05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspCnsmUnReg]
	@pCnsmSn  BIGINT	-- 위탁 시리얼 번호	
AS
	SET NOCOUNT ON   ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;

	DECLARE @aPcNo		INT
				, @aItemNo	INT
				, @aIsStack		SMALLINT
				, @pCatetory TINYINT 
				,@pItemSerial BIGINT 	-- 회수된 아이템Sn, 기존것에 병합되었다면 대상Sn
				,@pResultCnt  INT 		-- 회수된 개수 or 병합 결과 최종개수
				,@pCurCnt INT  -- 회수된 개수
				,@pPrice INT  -- 개당 판매 가격				

	SELECT @aRowCnt = 0, @aErrNo= 0  ;
	
	SELECT   
		@aPcNo		= [mPcNo],
		@aItemNo	= mItemNo,
		@aIsStack = IMaxStack	
	FROM dbo.TblConsignment T1
		INNER JOIN FNLParm.dbo.DT_ITEM T2
			ON T1.mItemNo = T2.IID
	WHERE  mCnsmNo  =@pCnsmSn;   	
	
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 1;  -- 위탁 아이템이 존재하지 않습니다.(TblConsignment)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 내부 오류
		END
		RETURN(@aErrNo);
	END  
	
	EXEC @aErrNo = dbo.WspCnsmUnregister @pCnsmSn, @aItemNo, @aIsStack, @aPcNo, @pCatetory OUTPUT, @pItemSerial OUTPUT, @pResultCnt OUTPUT,  @pCurCnt OUTPUT, @pPrice OUTPUT

GO

