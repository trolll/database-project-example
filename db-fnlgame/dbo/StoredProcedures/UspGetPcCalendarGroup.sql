/******************************************************************************
**		Name: UspGetPcCalendarGroup
**		Desc: ´Þ·Â ±×·ì¸ñ·Ï
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-21
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcCalendarGroup]
	 @pPcNo			INT 
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		 mGroupNo
		, RTRIM(mGroupNm)
	FROM dbo.TblCalendarGroup
	WHERE mOwnerPcNo = @pPcNo

GO

