/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetPetitonCheckState    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspGetPetitonCheckState]
	@mPcNo		INT		-- PC??
--WITH ENCRYPTION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	BEGIN TRAN

		-- ??? ??? ???? ?? ????? ?? ? 2?? ?? ??? ?? ?
		DELETE dbo.TblPetitionCheckState
		FROM dbo.TblPetitionCheckState A JOIN dbo.TblPetitionBoard B ON A.mPID = B.mPID
		WHERE A.mPcNo = @mPcNo AND B.mIsFin = CAST(1 AS BIT) AND B.mFinDate < DATEADD(dd, -2, GetDate())
		
		-- ??? ??? ??? ???
		SELECT A.mCategory, B.mIsFin
		FROM dbo.TblPetitionCheckState A JOIN dbo.TblPetitionBoard  B ON A.mPID = B.mPID
		WHERE A.mPcNo = @mPcNo

	IF @@ERROR = 0
	BEGIN
		COMMIT TRAN
	END
	ELSE
	BEGIN
		SET	@aErrNo = 1	-- ??? ??
		ROLLBACK TRAN
	END

	SET NOCOUNT OFF
	RETURN(@aErrNo)
END

GO

