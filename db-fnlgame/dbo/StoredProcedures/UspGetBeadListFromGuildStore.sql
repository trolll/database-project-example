/******************************************************************************    
**  Name: UspGetBeadListFromGuildStore    
**  Desc: 길드 창고의 장비에 결합된 구슬 리스트를 가져온다.    
**    
**  Auth:  김강호
**  Date:  2011-06-30
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:  Author:    Description:    
**  -------- --------   ---------------------------------------    
*******************************************************************************/    
CREATE PROCEDURE [dbo].[UspGetBeadListFromGuildStore]    
   @pOwnerGuild INT    
 , @pGrade TINYINT    
AS    
	SET NOCOUNT ON     
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
	SET ROWCOUNT 300      

	SELECT 
		 a.mOwnerSerialNo
		,a.mItemNo
		,a.mBeadIndex
		,DATEDIFF(mi,GETDATE(),a.mEndDate) as mEndDateA
		,a.mCntUse
	FROM	
		TblPcBead a
		INNER JOIN dbo.TblGuildStore b ON a.mOwnerSerialNo=b.mSerialNo
		AND a.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
		AND b.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
		AND b.mGuildNo = @pOwnerGuild
		AND b.mGrade = @pGrade
	ORDER BY a.mItemNo;

GO

