/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetDiscipleHistoryCount    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspGetDiscipleHistoryCount]
	 @pMaster		INT
	,@pHistoryCount	INT OUTPUT
AS
	SET NOCOUNT ON	

	SET @pHistoryCount = 0

	SELECT @pHistoryCount = COUNT(mMaster)
	FROM dbo.TblDiscipleHistory WITH(NOLOCK)
	WHERE mMaster = @pMaster

	SET NOCOUNT OFF

GO

