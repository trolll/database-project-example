/******************************************************************************
**		Name: UspEnchantAchieveItem
**		Desc: Àü¸®Ç° ÀÎÃ¦Æ®. ÃÖ´ë 8°³ÀÇ ÁÖÈ­ °ªÀÌ ¿Ã¼öÀÖ´Ù.
**
**		Auth: ³²±âºÀ
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	2014/04/28	³²¼º¸ð				Àü¸®Ç° Àç·áÈ­·Î ÀÎÇÏ¿© »èÁ¦µÇ´Â ÁÖÈ­°¡ ¾ø´Â °æ¿ì¿¡µµ ¾÷µ¥ÀÌÆ® µÇµµ·Ï ¼öÁ¤
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspEnchantAchieveItem]
	 @pPcNo  INT 
	,@pTrophyNo BIGINT
	,@pExp INT
	,@pCoinNo1 BIGINT
	,@pCoinNo2 BIGINT
	,@pCoinNo3 BIGINT
	,@pCoinNo4 BIGINT
	,@pCoinNo5 BIGINT
	,@pCoinNo6 BIGINT
	,@pCoinNo7 BIGINT
	,@pCoinNo8 BIGINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE	 @aErr INT

	SET @aErr = 0

BEGIN TRAN

	DELETE dbo.TblPcAchieveInventory 
	WHERE mSerialNo IN (@pCoinNo1,@pCoinNo2,@pCoinNo3,@pCoinNo4,@pCoinNo5,@pCoinNo6,@pCoinNo7,@pCoinNo8)
	-- 2014/04/28
	-- IF @@ERROR <> 0 OR @@ROWCOUNT = 0
	IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SET @aErr = 1;
			GOTO T_END
		END

	UPDATE dbo.TblPcAchieveInventory 
	SET mExp = @pExp
	WHERE mSerialNo = @pTrophyNo AND mPcNo = @pPcNo
	IF @@ERROR <> 0 OR @@ROWCOUNT = 0
		BEGIN
			ROLLBACK TRAN
			SET @aErr = 2;
			GOTO T_END
		END

COMMIT TRAN

T_END:
	SELECT @aErr

GO

