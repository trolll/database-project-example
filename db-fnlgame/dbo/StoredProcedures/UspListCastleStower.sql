/******************************************************************************
**		Name: UspListCastleStower
**		Desc: 
**
**		Auth:
**		Date:
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	2013-06-20	傍籍痹				荐沥
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspListCastleStower] 
--WITH ENCRYPTION 
AS 
	SET NOCOUNT ON -- Count-set搬苞甫积己窍瘤富酒扼
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT a .[mPlace], a.[mGuildNo] , a. [mTaxBuy], a .[mTaxBuyMax], a.[mTaxHunt], a.[mTaxHuntMax] ,  
	 a .[mTaxGamble], a.[mTaxGambleMax] , a. [mAsset], a .[mChgDate], a.[mAssetBuy], a.[mAssetHunt] , 
	 a .[mAssetGamble], a. [mSiegeDate], a.[mSiegeDayOfWeek] , a.[mSiegeDayOfWeekLimit]
	FROM dbo.TblCastleTowerStone AS a

GO

