--------------------------------
-- ░┤├╝╕φ : WspGetGuildNo
-- ╝│╕φ : ▒µ╡σ└╟ ╜─║░ ╣°╚ú╕ª ░í┴«┐┬┤┘
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.07.11
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
--------------------------------

CREATE PROCEDURE [dbo].[WspGetGuildNo] 
	@mGuildNm	CHAR(12)	-- ▒µ╡σ└╠╕º
AS
	SET NOCOUNT ON

	SELECT
		mGuildNo
	FROM
		TblGuild
	WITH ( NOLOCK )
	WHERE
		mGuildNm = @mGuildNm

	SET NOCOUNT OFF

GO

