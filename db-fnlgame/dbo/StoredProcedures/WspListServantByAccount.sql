/******************************************************************************
**		File: WspListServantByAccount.sql
**		Name: WspListServantByAccount
**		Desc: ¼­¹øÆ® Á¤º¸ Á¶È¸ (°èÁ¤¸í)
**		Ex	: EXEC dbo.WspListServantByAccount 'hun002'
** 
**		Auth: 2015.06.15
**		Date: ÀÌ Áø ¼±
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		
*******************************************************************************/ 
CREATE PROC [dbo].[WspListServantByAccount]
(
	@pUserId VARCHAR(20)
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	DECLARE @mUserNo INT
	
	SELECT @mUserNo = mUserNo
	FROM FNLAccount.dbo.TblUser
	WHERE mUserId = @pUserId
	AND mDelDate = '1900-01-01'
	
	IF (@mUserNo IS NULL)
	BEGIN
		RETURN (1)
	END
	
	SELECT
		P.mNm
		, I.mRegDate
		, I.mSerialNo
		, I.mItemNo
		, DI.IName
		, DATEDIFF(DD, GETDATE(), I.mEndDate) AS mEndDate
		, I.mStatus
		, I.mCnt
		, I.mIsSeizure
		, I.mPracticalPeriod
		, I.mBindingType
		, I.mRestoreCnt
		, EQ.mSlot
		, S.mName
		, S.mLevel
		, S.mExp
		, SkillPackName = STUFF((
								SELECT ',' + mName
								FROM   (
											SELECT DSP.mName
											FROM dbo.TblPcServantSkillPack AS SP
												INNER JOIN FNLParm.dbo.DT_SkillPack AS DSP
											ON SP.mSPID = DSP.mSPID
											WHERE SP.mSerialNo = S.mSerialNo
										) AS A
								FOR XML PATH('')),1,1,'')
	FROM dbo.TblPc AS P
		INNER JOIN dbo.TblPcInventory AS I 
	ON P.mNo = I.mPcNo
		LEFT OUTER JOIN dbo.TblPcEquip AS EQ
	ON I.mSerialNo = EQ.mSerialNo
		INNER JOIN dbo.TblPcServant AS S
	ON I.mSerialNo = S.mSerialNo
		INNER JOIN FNLParm.dbo.DT_Item AS DI
	ON I.mItemNo = DI.IID
	WHERE P.mOwner = @mUserNo
	AND P.mDelDate IS NULL
END

GO

