/******************************************************************************  
**  File: UspListCastleStowerRuined.sql
**  Name: UspListCastleStowerRuined 
**  Desc: 폐허화 지역 얻기
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2011.11.15	kirba		수정
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspListCastleStowerRuined]	 
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	SELECT 
		mPlace,
		mRuinMonsterRespawn
	FROM dbo.TblCastleTowerStoneRuined
	
	SET NOCOUNT OFF

GO

