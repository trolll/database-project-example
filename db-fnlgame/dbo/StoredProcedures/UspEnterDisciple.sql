/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2009-5-12 9:18:17 ******/

/****** Object:  Stored Procedure dbo.UspEnterDisciple    Script Date: 2008-11-10 10:37:19 ******/


CREATE PROCEDURE [dbo].[UspEnterDisciple]
	 @pMaster	INT
	,@pDisciple	INT
	,@pType	TINYINT	-- ?? ???? ?? (0:?? / 1:?? / 2:?? / 3:??)
	,@pJoinCount	INT OUTPUT	-- ?? ???? ??
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT
	DECLARE @aErrNo INT
	DECLARE @aMaster INT
	DECLARE @aMasterLevel INT
	DECLARE @aLevel INT
	DECLARE @aJoinCount INT
	SET @aRv = 0
	SET @aMasterLevel = 0
	SET @aLevel = 0
	SET @aJoinCount = 0
	SET @pJoinCount = 0

	-- ????? ??? ???? PC??? ????? ??
	SELECT 
		@aLevel = mLevel,  
		@aJoinCount = mDiscipleJoinCount 
	FROM dbo.TblPcState T1
		INNER JOIN dbo.TblPc T2
			ON T1.mNo = T2.mNO
	WHERE T1.mNo = @pDisciple
			AND T2.mDelDate IS NULL
	
	IF @@ROWCOUNT <= 0
	BEGIN
		RETURN(2)  -- ???? ?? ??? PC??
	END

	-- ??? ???? PC??? ????? ??
	SELECT 
		@aMasterLevel = mLevel 
	FROM dbo.TblPcState 
	WHERE mNo = @pMaster
	IF @@ROWCOUNT <= 0
	BEGIN
		RETURN(6)  -- ???? ?? ??? PC??
	END
	
	IF (@pMaster = @pDisciple)
	BEGIN
		RETURN(3)		-- ?? ?? ??? ???? ??			
	END	
	
	-- ?? ???? ???? ???? ??
	SELECT @aMaster = mMaster 
	FROM dbo.TblDiscipleMember 
	WHERE mDisciple = @pDisciple	
	IF @@ROWCOUNT > 0
	BEGIN
		IF (@aMaster <> @pMaster)
		BEGIN
			RETURN(3)		-- ?? ?? ??? ???? ??			
		END

		-- ???? ???? ???? ?? ??? ??
		IF(@aMasterLevel <= @aLevel)
		BEGIN			
			DELETE dbo.TblDiscipleMember 
			WHERE mDisciple = @pDisciple	-- ??? ???? ??? ??
			
			RETURN(4)		-- ?? ??? ?? ??
		END

		-- ?? ???? ?? ??
		IF (@aJoinCount <= 0)
		BEGIN			
			DELETE dbo.TblDiscipleMember 
			WHERE mDisciple = @pDisciple	-- ??? ???? ??? ??
			
			RETURN(5)		-- ???? ?? ??
		END

		BEGIN TRAN

			UPDATE dbo.TblDiscipleMember 
			SET mType = @pType 
			WHERE mDisciple = @pDisciple
			
			SET @aErrNo = @@ERROR
			IF @aErrNo <> 0
			BEGIN
				ROLLBACK TRAN
				RETURN(1)
			END

			UPDATE dbo.TblPcState 
			SET @pJoinCount =
					mDiscipleJoinCount = mDiscipleJoinCount - 1 
			WHERE mNo = @pDisciple
			
			SET @aErrNo = @@ERROR
			IF @aErrNo <> 0
			BEGIN
				ROLLBACK TRAN
				RETURN(1)
			END

		COMMIT TRAN
		RETURN(0)
	END


	-- ?? ???? ???? ???? ??
	-- ???? ???? ???? ?? ??? ??
	IF(@aMasterLevel <= @aLevel)
	BEGIN
		RETURN(4)		-- ?? ??? ?? ??
	END

	-- ?? ???? ?? ??
	IF (@aJoinCount <= 0)
	BEGIN
		RETURN(5)		-- ???? ?? ??		
	END

	INSERT dbo.TblDiscipleMember (mMaster, mDisciple, mType) 
	VALUES(@pMaster, @pDisciple, @pType)
	SET @aErrNo = @@ERROR
	IF @aErrNo <> 0
	BEGIN
		RETURN(1)	-- db error 
	END

	RETURN(0)

GO

