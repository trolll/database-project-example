/****** Object:  Stored Procedure dbo.WspListStoreItemByAccount    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.WspListStoreItemByAccount    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.WspListStoreItemByAccount    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.WspListStoreItemByAccount    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.WspListStoreItemByAccount    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.WspListStoreItemByAccount    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.WspListStoreItemByAccount    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.WspListStoreItemByAccount    Script Date: 2009-7-14 13:13:30 ******/
CREATE PROCEDURE [dbo].[WspListStoreItemByAccount]  
 @mUserId VARCHAR(20) -- ???  
AS  
 SET NOCOUNT ON   
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 SELECT  
  p.mSerialNo  
  ,p.mRegDate  
  -- ???  
  -- ??  
  ,ii.IName -- ??? ??  
  , p.mStatus -- ??  
  , DATEDIFF(DAY, GETDATE(), p.mEndDate) AS mEndDate  -- ????  
  -- , p.mSerialNo -- ???  
  , p.mCnt -- ??  
  , p.mItemNo -- TID  
  , p.mIsSeizure -- ????  
  --, p.mNo -- Unique Key  
  , p.mPracticalPeriod  
  , p.mBindingType  
  , p.mOwner
 FROM [dbo].[TblPcStore] AS p   
  INNER JOIN [FNLAccount].[dbo].[TblUser] AS u   
   ON p.mUserNo = u.mUserNo  
  INNER JOIN FNLParm.[dbo].[DT_Item] AS ii   
   ON p.mItemNo = ii.IID  
 WHERE u.mUserId = @mUserId  
 ORDER BY p.mItemNo

GO

