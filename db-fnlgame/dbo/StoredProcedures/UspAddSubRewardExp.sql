/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspAddSubRewardExp    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspAddSubRewardExp]
	 @pGuildNo		INT
	,@pVal			INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	UPDATE dbo.TblGuild 
	SET [mRewardExp]=[mRewardExp]+@pVal 
	WHERE ([mGuildNo]=@pGuildNo)
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo	= 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

