/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetDialog    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspSetDialog]
	 @pMId			INT
	,@pClick		VARCHAR(6000)    -- eSzDialogClick ? ??
	,@pDie			VARCHAR(100)
	,@pAttacked		VARCHAR(100)
	,@pTarget		VARCHAR(100)
	,@pBear			VARCHAR(100)
	,@pGossip1		VARCHAR(100)
	,@pGossip2		VARCHAR(100)
	,@pGossip3		VARCHAR(100)
	,@pGossip4		VARCHAR(100)
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0	
	
	IF EXISTS(SELECT * FROM TblDialog WHERE [mMId]=@pMId)
	 BEGIN
		UPDATE TblDialog SET [mClick]=@pClick, [mDie]=@pDie, [mAttacked]=@pAttacked, [mTarget]=@pTarget, [mBear]=@pBear, 
							 [mGossip1]=@pGossip1, [mGossip2]=@pGossip2, [mGossip3]=@pGossip3, [mGossip4]=@pGossip4
			WHERE [mMId] = @pMId
	 END
	ELSE
	 BEGIN
		INSERT TblDialog([mMId], [mClick], [mDie], [mAttacked], [mTarget], [mBear], 
			   [mGossip1], [mGossip2], [mGossip3], [mGossip4])
			VALUES(@pMId, @pClick, @pDie, @pAttacked, @pTarget, @pBear, 
				   @pGossip1, @pGossip2, @pGossip3, @pGossip4)	 
	 END
	 
	IF(@@ROWCOUNT <> 1)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END	 
	 END
		
LABEL_END:		
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

