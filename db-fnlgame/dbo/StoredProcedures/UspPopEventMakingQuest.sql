/******************************************************************************
**		Name: UspPopEventMakingQuest
**		Desc: 矫胶袍 捞亥飘 涅胶飘 昏力
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-04-02
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPopEventMakingQuest]
	 @pPcNo			INT
	,@pQuestNo		INT	 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	BEGIN TRAN

		DELETE dbo.TblPcQuestCondition
		WHERE mQuestNo = @pQuestNo AND mPcNo = @pPcNo
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		DELETE dbo.TblPcQuestVisit
		WHERE mQuestNo = @pQuestNo AND mPcNo = @pPcNo
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(2);			
		END 

		DELETE dbo.TblPcQuest
		WHERE mQuestNo = @pQuestNo AND mPcNo = @pPcNo
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(3);			
		END 

	COMMIT TRAN
	 		 
	RETURN(0);

GO

