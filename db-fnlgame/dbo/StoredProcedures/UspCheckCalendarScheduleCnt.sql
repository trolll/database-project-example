/******************************************************************************
**		Name: UspCheckCalendarScheduleCnt
**		Desc: ÇØ´ç ³¯Â¥¿¡ ¸¸µé ¼ö ÀÖ´Â °¹¼ö Ã¼Å©
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckCalendarScheduleCnt]
	 @pOwnerPcNo		INT 
	,@pScheduleDate		SMALLDATETIME
	,@pMaxCnt			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	 @aDay		INT
			,@aMonth	INT
			,@aCnt		INT

	SELECT	 @aDay = DATEPART(DD, @pScheduleDate)
			,@aMonth = DATEPART(MM, @pScheduleDate)
			,@aCnt = 0

	SELECT  @aCnt = COUNT(*)
	FROM dbo.TblCalendarPcSchedule	AS T1
		INNER JOIN dbo.TblCalendarSchedule AS T2
	ON T1.mSerialNo = T2.mSerialNo AND T1.mPcNo = T2.mOwnerPcNo
	WHERE DATEPART(DD, T2.mScheduleDate) = @aDay
	AND DATEPART(MM, T2.mScheduleDate) = @aMonth
	AND T1.mPcNo = @pOwnerPcNo

	IF @pMaxCnt <= @aCnt
	BEGIN
		RETURN(1);
	END 

	RETURN(0);

GO

