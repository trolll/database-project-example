/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2009-7-14 13:13:32 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2009-5-12 9:18:19 ******/

/****** Object:  Stored Procedure dbo.UspUpdateMpHp    Script Date: 2008-11-10 10:37:20 ******/





CREATE PROCEDURE [dbo].[UspUpdateMpHp]
	 @pPcNo			INT
	,@pHp			SMALLINT
	,@pMp			SMALLINT
AS
	SET NOCOUNT ON			
	
	DECLARE	@aErrNo		INT,
			@aRowCnt	INT
			
	SELECT 	@aErrNo = 0	, 	@aRowCnt = 0	

	UPDATE dbo.TblPcState
	SET
		mHpAdd = mHpAdd + @pHp,
		mMpAdd = mMpAdd + @pMp
	WHERE mNo = @pPcNo
	SELECT 	@aErrNo = @@ERROR, 	@aRowCnt = @@ROWCOUNT

	IF @aErrNo <> 0
	BEGIN
		RETURN(1)	
	END
	
	IF @aRowCnt <= 0
	BEGIN
		RETURN(2)
	END 

	RETURN(0)

GO

