/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromGuildStore    Script Date: 2009-5-12 9:18:18 ******/
CREATE PROCEDURE [dbo].[UspGetCountFromGuildStore]
	  @pOwnerGuild	INT
	, @pGrade		TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		


	SELECT 
		ISNULL(COUNT(*),0)
	FROM dbo.TblGuildStore
	WHERE mGuildNo = @pOwnerGuild 
			AND mGrade = @pGrade
			AND mEndDate >  CONVERT(SMALLDATETIME, GETDATE())

GO

