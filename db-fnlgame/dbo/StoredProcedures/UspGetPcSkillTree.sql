/******************************************************************************  
**  File: 
**  Name: UspGetPcSkillTree  
**  Desc: PC의 스킬트리노드아이템들을 로드한다.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcSkillTree]
	 @pPcNo		INT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	IF @pPcNo = 1 OR @pPcNo = 0 
	BEGIN
		RETURN(0)	-- NPC, 버려진 아이템
	END
	
	SELECT 
		mSTNIID,
		DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
	FROM dbo.TblPcSkillTreeInventory WITH(NOLOCK)
	WHERE 	mPcNo = @pPcNo
				AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY mSTNIID ASC

GO

