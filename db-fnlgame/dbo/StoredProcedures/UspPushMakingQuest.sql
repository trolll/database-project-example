/******************************************************************************
**		Name: UspPushMakingQuest
**		Desc: 捞亥飘 涅胶飘 殿废
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPushMakingQuest]
	 @pPcNo				INT
	,@pType				TINYINT		-- 1: 老馆, 2: 辨靛
	,@pMonID			INT
	,@pMaxCnt			INT
	,@pTargetPcNo		INT
	,@pStartNQuestNo	INT			-- 老馆 涅胶飘 皋捞欧 矫累锅龋
	,@pStartGQuestNo	INT			-- 辨靛 涅胶飘 皋捞欧 矫累锅龋
	,@pEndNQuestNo		INT			-- 老馆 涅胶飘 皋捞欧 场锅龋
	,@pEndGQuestNo		INT			-- 辨靛 涅胶飘 皋捞欧 场锅龋
	,@pQuestNo			INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE  @aNormalQuestLastNo INT
			,@aGuildQuestLastNo INT
			,@aQuestNo INT;

	SELECT	 @aQuestNo = 0
			,@aNormalQuestLastNo = 0
			,@aGuildQuestLastNo = 0;

	EXEC dbo.UspCheckPcMakingQuestLastNo @pPcNo, @pStartNQuestNo, @pStartGQuestNo, @pEndNQuestNo, @pEndGQuestNo, @aNormalQuestLastNo OUTPUT, @aGuildQuestLastNo OUTPUT
	IF @@ERROR <> 0 
	BEGIN
		RETURN(11);	
	END 	

	SELECT
		@aQuestNo = 
					CASE WHEN (@pType = 1) 
							THEN 
								CASE WHEN (@aNormalQuestLastNo = 0)
										THEN @aNormalQuestLastNo + @pStartNQuestNo 
									 ELSE @aNormalQuestLastNo + 1
								END
						 WHEN (@pType = 2)
							THEN
								CASE WHEN (@aGuildQuestLastNo = 0)
										THEN @aGuildQuestLastNo + @pStartGQuestNo
									 ELSE @aGuildQuestLastNo + 1
								END
					END 

	SET @pQuestNo = @aQuestNo

	BEGIN TRAN
			
		INSERT INTO dbo.TblPcQuest([mPcNo], [mQuestNo], [mValue])
			VALUES(@pPcNo, @aQuestNo, 1)	
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		INSERT INTO dbo.TblPcQuestCondition([mPcNo], [mQuestNo], [mMonsterNo], [mCnt], [mMaxCnt])
			VALUES(@pPcNo, @aQuestNo, @pMonID, 0, @pMaxCnt)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(2);			
		END 

		IF @pType = 2 AND @pTargetPcNo <> 0
		BEGIN
			INSERT INTO dbo.TblGuildQuest([mGuildMasterNo], [mMQuestNo], [mPcNo])
				VALUES(@pPcNo, @aQuestNo, @pTargetPcNo)
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN;
				RETURN(3);			
			END 

			IF @pPcNo <> @pTargetPcNo -- 辨靛 付胶磐尔 鸥百捞 促福搁 鸥百俊档 持绢霖促.
			BEGIN
				INSERT INTO dbo.TblPcQuest([mPcNo], [mQuestNo], [mValue])
					VALUES(@pTargetPcNo, @aQuestNo, 1)	
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN;
					RETURN(4);			
				END 

				INSERT INTO dbo.TblPcQuestCondition([mPcNo], [mQuestNo], [mMonsterNo], [mCnt], [mMaxCnt])
					VALUES(@pTargetPcNo, @aQuestNo, @pMonID, 0, @pMaxCnt)
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN;
					RETURN(5);			
				END 
			END
		END

	COMMIT TRAN

	RETURN(0);

GO

