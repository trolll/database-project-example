/******************************************************************************
**		Name: UspPopMakingQuest
**		Desc: 捞亥飘 涅胶飘 昏力
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPopMakingQuest]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pType			TINYINT		-- 1: 老馆, 2: 辨靛
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aGuildMasterNo INT;
	DECLARE @aTargetPcNo INT;

	-- 辨靛付胶磐啊 器扁窍搁 @aGuildMasterNo 蔼捞 绝促
	SELECT @aGuildMasterNo = mGuildMasterNo
	FROM dbo.TblGuildQuest
	WHERE mMQuestNo = @pQuestNo AND mPcNo = @pPcNo
	IF @@ERROR <> 0
	BEGIN
		RETURN(1);			
	END 

	SELECT @aTargetPcNo = mPcNo
	FROM dbo.TblGuildQuest
	WHERE mMQuestNo = @pQuestNo AND mGuildMasterNo = @pPcNo
	IF @@ERROR <> 0
	BEGIN
		RETURN(2);			
	END

	BEGIN TRAN

		DELETE dbo.TblPcQuestCondition
		WHERE mQuestNo = @pQuestNo AND mPcNo = @pPcNo
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(3);			
		END 

		DELETE dbo.TblPcQuest
		WHERE mQuestNo = @pQuestNo AND mPcNo = @pPcNo
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(4);			
		END 

		IF (@pType = 2)
		BEGIN
			IF (@aGuildMasterNo IS NOT NULL)
			BEGIN
				DELETE dbo.TblGuildQuest
				WHERE mMQuestNo = @pQuestNo AND mPcNo = @pPcNo
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRAN;
					RETURN(5);			
				END 

				IF (@aGuildMasterNo <> @pPcNo)
				BEGIN
					DELETE dbo.TblPcQuestCondition
					WHERE mQuestNo = @pQuestNo AND mPcNo = @aGuildMasterNo
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN;
						RETURN(6);			
					END 

					DELETE dbo.TblPcQuest
					WHERE mQuestNo = @pQuestNo AND mPcNo = @aGuildMasterNo
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN;
						RETURN(7);			
					END 
				END
			END
			ELSE
			BEGIN		
				IF (@aTargetPcNo IS NOT NULL)
				BEGIN
					DELETE dbo.TblGuildQuest
					WHERE mMQuestNo = @pQuestNo AND mPcNo = @aTargetPcNo
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN;
						RETURN(8);			
					END 

					DELETE dbo.TblPcQuestCondition
					WHERE mQuestNo = @pQuestNo AND mPcNo = @aTargetPcNo
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN;
						RETURN(9);			
					END 

					DELETE dbo.TblPcQuest
					WHERE mQuestNo = @pQuestNo AND mPcNo = @aTargetPcNo
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRAN;
						RETURN(10);			
					END 
				END
			END		
		END	

	COMMIT TRAN
	 		 
	RETURN(0)

GO

