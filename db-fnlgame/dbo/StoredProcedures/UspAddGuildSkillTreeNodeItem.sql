/******************************************************************************
**		Name: UspAddGuildSkillTreeNodeItem
**		Desc: 辨靛 胶懦 眠啊
**			  
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013/09/03	巢己葛				诀单捞飘 矫俊档 LeftMin阑 汲沥窍档废 荐沥	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddGuildSkillTreeNodeItem]
	@pGuildNo		INT,
	@pSTNIID		INT,
	@pCreatorPcNo	INT,
	@pValidSec		INT,	 -- 864000000檬(10000老)捞搁 公茄措, 2020-01-01 00:00:00 俊 父丰登绰 巴栏肺 距加茄促
	@pLeftMin		INT OUTPUT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	-- 捞固 乐促搁 父丰 朝楼父 技泼窍绊 眠啊 己傍栏肺 埃林茄促
	DECLARE @aSTNIID INT

	SELECT @aSTNIID = mSTNIID
	FROM dbo.TblGuildSkillTreeInventory
	WHERE mGuildNo=@pGuildNo
	AND mSTNIID = @pSTNIID

	DECLARE @aEndDay SMALLDATETIME
	IF (864000000 = @pValidSec)
	BEGIN
		SELECT @aEndDay = dbo.UfnGetEndDate(GETDATE(), 864000000/(3600 * 24))
	END
	ELSE
	BEGIN
		SELECT @aEndDay = DATEADD(SECOND, @pValidSec, GETDATE())
	END

	IF (@aSTNIID IS NOT NULL)
	BEGIN
		UPDATE dbo.TblGuildSkillTreeInventory	
		SET mEndDate = @aEndDay,
			mCreatorPcNo = @pCreatorPcNo,
			mExp = 0
		WHERE mGuildNo=@pGuildNo AND mSTNIID = @pSTNIID

		IF (@@error <> 0)
		BEGIN
			RETURN 1 -- DB 俊矾
		END
		
		-- 2013/09/03
		SET @pLeftMin = DATEDIFF(MI,GETDATE(), @aEndDay)
		
		RETURN 0
	END
	
	INSERT INTO dbo.TblGuildSkillTreeInventory
	VALUES(DEFAULT, @pGuildNo, @pSTNIID, @aEndDay, @pCreatorPcNo, 0)

	IF (@@error <> 0)
	BEGIN
		RETURN 1 -- DB 俊矾
	END

	SET @pLeftMin = DATEDIFF(MI,GETDATE(), @aEndDay)

	RETURN 0

GO

