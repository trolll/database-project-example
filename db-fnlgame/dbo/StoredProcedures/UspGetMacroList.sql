/******************************************************************************
**		Name: UspGetMacroList
**		Desc: ¸ÅÅ©·Î ¸®½ºÆ®
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMacroList]
	@pPcNo INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mMacroID, mMacroSlot, mMacroName, mIconNo
	FROM dbo.TblMacroList
	WHERE mPcNo = @pPcNo

GO

