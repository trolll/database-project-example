CREATE PROCEDURE [dbo].[WspSetGuildName]
	 @pGuildNo		INT
	,@pGuildNm		CHAR(12)	
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000	-- 2├╩ └╠│╗┐í └█╛≈└╠ │í│¬┴÷ ╛╩└╕╕Θ 1222 ┐└╖∙╕ª ╣▌╚»
	
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErr = 0
		
		
	--------------------------------------------------------------------------
	-- ┴▀║╣╡╚ ▒µ╡σ ┴╕└τ┐⌐║╬ ├╝┼⌐ 
	--------------------------------------------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblGuild WITH(NOLOCK)
				WHERE mGuildNm = @pGuildNm )
	BEGIN		
		RETURN(1)				-- └╠╣╠ ╡ε╖╧╡╚ ▒µ╡σ╕φ└╠┤┘.
	END

	--------------------------------------------------------------------------
	-- ▒µ╡σ╕φ ║»░µ 
	--------------------------------------------------------------------------			
	BEGIN TRAN
			
		UPDATE dbo.TblGuild	
		SET 	
			mGuildNm = @pGuildNm
		WHERE  [mGuildNo] = @pGuildNo

		SELECT	@pRowCnt = @@ROWCOUNT, 
				@pErr  = @@ERROR	

		IF @pRowCnt = 0
		BEGIN				
			ROLLBACK TRAN		-- ▒µ╡σ░í ┴╕└τ╟╧┴÷ ╛╩┤┬┤┘.		
			RETURN(2)
		END
		
		IF @pErr <> 0
		BEGIN				
			ROLLBACK TRAN			
			RETURN(@pErr)		-- System Error.
		END

	COMMIT TRAN
	RETURN(0)	-- NOT ERROR

GO

