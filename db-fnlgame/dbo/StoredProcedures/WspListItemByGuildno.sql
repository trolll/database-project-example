/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2009-7-14 13:13:32 ******/

/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2009-5-12 9:18:19 ******/  
  
/****** Object:  Stored Procedure dbo.WspListItemByGuildno    Script Date: 2008-11-10 10:37:20 ******/  
  
  
CREATE PROCEDURE  [dbo].[WspListItemByGuildno]  
 @pGuildNo INT  
 ,@pGrade TINYINT  
AS  
 SET NOCOUNT ON  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   
 SET ROWCOUNT 1000   
  
 SELECT  T2.IName  
  ,T1.mStatus  
  ,DATEDIFF(DAY, GETDATE(), T1.mEndDate) AS mEndDate   
  ,T1.mSerialNo  
  ,T1.mCnt  
  ,T1.mItemNo  
  ,T1.mIsSeizure  
  ,T1.mRegDate   
  ,T1.mGrade  
  ,T1.mOwner  
 FROM   [dbo].[TblGuildStore]  T1  
 INNER JOIN [FNLParm].dbo.DT_Item AS T2  
  ON T1.mItemNo = T2.IID  
 WHERE T1.mGuildNo = @pGuildNo  
  AND T1.mGrade = @pGrade  
 ORDER BY T1.mSerialNo ASC

GO

