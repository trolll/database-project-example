/******************************************************************************
**		Name: UspDeleteMacroAll
**		Desc: ¸ÅÅ©·Î ÀüÃ¼ »èÁ¦
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteMacroAll]
	@pPcNo			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRAN
		DELETE dbo.TblMacroCommand
		WHERE mPcNo = @pPcNo

		IF(@@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
		END

		DELETE dbo.TblMacroList
		WHERE mPcNo = @pPcNo

		IF(@@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN
		END
	COMMIT TRAN

GO

