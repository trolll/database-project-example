
-- 2018/04/03
-- 吝惫 1502 菩摹何磐 利侩 矫累. 青款狼 急拱(4220) 瘤鞭 吝瘤.(吝惫 夸没荤亲)
-- 1601/1602 菩摹 锭 UspCreatePc 客 郴侩 厚背秦辑 累诀秦具窃.
CREATE  PROCEDURE [dbo].[UspCreatePc]		
	 @pOwner	INT						
	,@pSlot		TINYINT
	,@pNm		CHAR(12)				--Char Name
	,@pClass	TINYINT					--0,1,2吝俊 酒公芭唱
	,@pSex		TINYINT					--0,1 吝俊 酒公芭唱
	,@pHead		TINYINT					--叼弃飘 1
	,@pFace		TINYINT					--叼弃飘 1
	,@pBody		TINYINT					--叼弃飘 1
	,@pHomeMap	INT						--EMap.
	,@pHomeX	REAL						
	,@pHomeY	REAL						
	,@pHomeZ	REAL						
	,@pPcNo		INT			OUTPUT
	,@pLevel	SMALLINT	OUTPUT
	,@pErrNoStr	VARCHAR(50)	OUTPUT	
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT,
				@aSerial		BIGINT,
				@aSerialSub		BIGINT,
				@aRowCnt 	INT

	SELECT		@aErrNo		= 0,
				@pErrNoStr	= 'eErrNoSqlInternalError',
				@aRowCnt = 0,
				@aSerialSub = 0;


	---------------------------------------------------
	-- 吝汗 某腐磐疙 粮犁咯何 眉农 
	---------------------------------------------------		
	IF EXISTS (	SELECT * 
				FROM dbo.TblPc
				WHERE mNm = @pNm)
	BEGIN
		SELECT		@aErrNo		= 2,
					@pErrNoStr	= 'eErrNoCharAlreadyExistNm'

		GOTO T_END
	END

	---------------------------------------------------
	-- 某腐磐 浇吩 粮犁咯何 眉农 
	---------------------------------------------------
	IF EXISTS (	SELECT * 
				FROM dbo.TblPc
				WHERE mOwner = @pOwner
						AND mSlot = @pSlot
						AND mDelDate IS NULL )
	BEGIN
		SELECT		@aErrNo		= 1,
					@pErrNoStr	= 'eErrNoUserCharSlotBusy'

		GOTO T_END
	 END	

	-----------------------------------------------------
	-- 酒捞袍 惯鞭 矫府倔
	-----------------------------------------------------
	EXEC @aSerial =  dbo.UspGetItemSerial 
	IF @aSerial <= 0
	BEGIN
		SET @aErrNo = 8
		GOTO T_END	
	END 	

	EXEC @aSerialSub =  dbo.UspGetItemSerial 
	IF @aSerialSub <= 0
	BEGIN
		SET @aErrNo = 8
		GOTO T_END	
	END 	

		 
	BEGIN TRAN	 
	 
		---------------------------------------------------
		-- 某腐磐 积己 
		---------------------------------------------------
		INSERT INTO dbo.TblPc( mOwner, mSlot, mNm, mClass, mSex, mHead, mFace, mBody, 
			mHomeMapNo, mHomePosX, mHomePosY, mHomePosZ)
		VALUES(	@pOwner, @pSlot,@pNm, @pClass, @pSex, @pHead, @pFace, @pBody, @pHomeMap, 
			@pHomeX, @pHomeY, @pHomeZ)
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET	 @aErrNo	= 3
			GOTO T_ERR		
		END

		SET @pPcNo	= @@IDENTITY
		SET @pLevel	= 1
	 
		---------------------------------------------------
		-- 某腐磐 惑怕
		---------------------------------------------------
		INSERT INTO dbo.TblPcState(mNo, mLevel, mExp, mHpAdd, mHp, mMpAdd, mMp, mMapNo, mPosX, mPosY, mPosZ)
		VALUES(@pPcNo, @pLevel, 0, 0, 0, 0, 0, @pHomeMap, @pHomeX, @pHomeY, @pHomeZ)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET	 @aErrNo = 4
			GOTO T_ERR		 
		END 

		---------------------------------------------------
		-- 扁夯 酒捞袍 唱捞橇(153)甫 霖促.
		---------------------------------------------------
		INSERT dbo.TblPcInventory ( mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
		VALUES(@aSerial, @pPcNo, 153, '2079-01-01', 1, 1, 1, 0)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET	 @aErrNo = 5
			GOTO T_ERR		 
		END

		---------------------------------------------------
		-- 吝惫 夸没栏肺 瘤鞭俊辑 力寇
		---------------------------------------------------
		--INSERT dbo.TblPcInventory ( mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
		--VALUES(@aSerialSub, @pPcNo, 4220, '2079-01-01', 1, 1, 1, 0)

		--SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		--IF @aErrNo <> 0  OR @aRowCnt <= 0 
		--BEGIN
		--	SET	 @aErrNo = 5
		--	GOTO T_ERR		 
		--END

T_ERR:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN	
		
T_END:			
	RETURN(@aErrNo)

GO

