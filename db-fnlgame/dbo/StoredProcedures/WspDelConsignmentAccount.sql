CREATE PROCEDURE [dbo].[WspDelConsignmentAccount]
(
	@pPcNo INT
	, @pDelBalance BIGINT
)
AS
/******************************************************************************
**		Name: WspDelConsignmentAccount
**		Desc: 위탁상점 정산금 삭제
**		Test:
			DECLARE @Return INT
			SET @Return = 0
			EXEC dbo.WspDelConsignmentAccount
				2165
				, 5
			SELECT @Return
			
**		Return:
			0 : 성공
			1 : 잔액이 없거나, 삭제하려는 금액이 잔액보다 큼
			2 : 잔액 업데이트 실패
			3 : 잔액 삭제 실패
			
**		Auth: 이진선
**		Date: 2012-12-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-------------------------------------------------
	-- 변수 선언
	-------------------------------------------------
	DECLARE @aErrNo INT
	DECLARE @aRowCnt INT
	DECLARE @aCurBalance BIGINT
	DECLARE @aChgBalance BIGINT

	-------------------------------------------------
	-- 변수 초기화
	-------------------------------------------------
	SELECT @aErrNo = 0
			, @aRowCnt = 0
			, @aCurBalance = 0
			, @aChgBalance = 0

	-------------------------------------------------
	-- 정상금 잔액 체크
	-------------------------------------------------
	SELECT @aCurBalance = mBalance
	FROM dbo.TblConsignmentAccount
	WHERE mPcNo = @pPcNo

	IF (@aCurBalance IS NULL OR @pDelBalance IS NULL OR @aCurBalance < @pDelBalance)
	BEGIN
		RETURN (1);	--잔액이 없거나, 삭제하려는 금액이 잔액보다 큼
	END

	BEGIN TRAN;
		UPDATE dbo.TblConsignmentAccount SET @aChgBalance = mBalance = mBalance - @pDelBalance
		WHERE mPcNo = @pPcNo

		SELECT @aErrNo= @@ERROR ,@aRowCnt = @@ROWCOUNT
		IF (@aErrNo<> 0 OR @aRowCnt <= 0)
		BEGIN
			ROLLBACK TRAN;
			RETURN (2);	--잔액 업데이트 실패
		END

		IF (@aChgBalance = 0)
		BEGIN
			DELETE dbo.TblConsignmentAccount
			WHERE mPcNo = @pPcNo

			SELECT @aErrNo= @@ERROR ,@aRowCnt = @@ROWCOUNT
			IF (@aErrNo<> 0 OR @aRowCnt <= 0)
			BEGIN
				ROLLBACK TRAN;
				RETURN (3); --잔액 삭제 실패
			END
		END
	COMMIT TRAN;

	RETURN (0);
END

GO

