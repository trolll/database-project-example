/******************************************************************************
**		Name: UspChangeTeleport
**		Desc: 扁撅府胶飘 荐沥
**		Test:			
**		Auth: 巢己葛
**		Date: 2013/04/24
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspChangeTeleport]
	 @pNo			INT
	,@pPcNo			INT
	,@pName			VARCHAR(50)
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	UPDATE dbo.TblPcTeleport
	SET [mName] = @pName
	WHERE ([mNo]=@pNo) 
		AND ([mPcNo]=@pPcNo)

	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)	 
	END	

	RETURN(0)

GO

