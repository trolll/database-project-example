/******************************************************************************  
**  File: UspInsLimitedResource.sql  
**  Name: UspInsLimitedResource  
**  Desc: 리소스아이템 추가  
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description:  
**  --------	--------	---------------------------------------  
**	2012.06.11	정진호		mIsIncSys 컬럼 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspInsLimitedResource]  
 @pResourceType INT,  
 @pMaxCnt  INT,  
 @pRemainerCnt INT,  
 @pRandomVal  FLOAT,
 @pIsIncSys BIT  
AS   
	SET NOCOUNT ON  
   
	IF EXISTS( SELECT *   
		FROM dbo.TblLimitedResource   
		WHERE mResourceType = @pResourceType )  
	BEGIN  
		RETURN(1)  
	END      
  
	INSERT INTO dbo.TblLimitedResource  
	VALUES(  
		GETDATE(),  
		@pResourceType,  
		@pMaxCnt,  
		@pRemainerCnt,  
		@pRandomVal,  
		GETDATE(),
		@pIsIncSys
	)  
    
	IF @@ERROR <> 0  OR @@ROWCOUNT <= 0  
	BEGIN  
		RETURN(1) -- db error  
	END  
     
	RETURN(0)

GO

