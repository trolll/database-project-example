/******************************************************************************  
**  Name: UspResetAllCooltime  
**  Desc: 쿨타임을 전부 제거한다.
**  
**  Auth: 김강호  
**  Date: 2011.07.18
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- -----------------------------------------------  
**      수정일      수정자              수정내용      
*******************************************************************************/  
CREATE    PROCEDURE [dbo].[UspResetAllCooltime]  
	@pPcNo    INT
AS  
	SET NOCOUNT ON -- Count-set결과를 생성하지 말아라.  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DELETE dbo.TblPcCoolTime 
	WHERE mPcNo = @pPcNo ;
	
	IF(@@ERROR <> 0)  
	BEGIN  
		RETURN(1)  ;
	END  

	RETURN(0)  ;

GO

