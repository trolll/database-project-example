/******************************************************************************
**		Name: WspInsertBead
**		Desc: ·й »рАФ
**
**		Auth: АМБшј±
**		Date: 20120302
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspInsertBead]
(
	@pEquipSerialNo	BIGINT --·й АеВшЗТ ѕЖАМЕЫ ЅГё®ѕу №шИЈ
	, @pBeadIndex TINYINT --ЅЅ·Ф А§ДЎ
	, @pBeadItemNo INT --·й ѕЖАМЕЫ №шИЈ
	, @pStatus TINYINT --ѕЖАМЕЫ »уЕВ (0 : АъБЦ, 1: єёЕл, 2 : Гає№)  
	, @pLeftDate INT --АЇИї±в°Ј
	, @pCntUse SMALLINT --»зїлИЅјц
	, @pIsEquipBead BIT --·й АеВш °ЎґЙї©єО (1 = АеВш °ЎґЙ, 0 = АеВш єТ°ЎґЙ)
	, @pHoleCount TINYINT --¶Хё° ЅЅ·Ф °іјц
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;

	DECLARE @mCurHoleCount TINYINT
	DECLARE @mEndDate SMALLDATETIME

	SET @mCurHoleCount = 0
	SET @mEndDate = '2020-01-01 00:00:00' --АЇИї±в°Ј №МАФ·ВЅГ, №«Б¦ЗС ±в°Ј јіБ¤

	IF (@pLeftDate IS NOT NULL) --АЇИї±в°Ј АФ·ВЅГ, ±в°Ј јіБ¤
	BEGIN
		SET @mEndDate = DATEADD(DD,@pLeftDate,GETDATE())
	END

	IF (@pCntUse IS NULL) --»зїлИЅјц №МАФ·ВЅГ, №«Б¦ЗС ИЅјц јіБ¤
	BEGIN
		SET @pCntUse = 0
	END

	-------------------------------------------------
	-- ·й АеВш °ЎґЙЗС ѕЖАМЕЫАОБц И®АО
	-------------------------------------------------
	IF (@pIsEquipBead = 0)
	BEGIN
		RETURN 1; --·й АеВш єТ°ЎґЙЗС ѕЖАМЕЫ АФґПґЩ.
	END
	-------------------------------------------------
	-- ·й ѕЖАМЕЫАОБц И®АО
	-------------------------------------------------
	IF NOT EXISTS (SELECT * FROM FNLParm.dbo.DT_Bead WHERE IID = @pBeadItemNo)
	BEGIN
		RETURN 2; --·й ѕЖАМЕЫАМ ѕЖґХґПґЩ.
	END
	-------------------------------------------------
	-- »уЕВ°Є И®АО - ѕЖАМЕЫ »уЕВ (0 : АъБЦ, 1: єёЕл, 2 : Гає№)  
	-------------------------------------------------
	IF (@pStatus < 0 OR @pStatus >= 3 OR @pStatus IS NULL)
	BEGIN
		RETURN 3; --БёАзЗПБц ѕКґВ »уЕВАФґПґЩ.
	END
	-------------------------------------------------
	-- ЅЅ·Ф ¶Хё° °№јц ГК°ъЅГ
	-------------------------------------------------
	SELECT @mCurHoleCount = COUNT(*)
	FROM dbo.TblPcBead
	WHERE mOwnerSerialNo = @pEquipSerialNo
	
	IF (@pHoleCount <= @mCurHoleCount)
	BEGIN
		RETURN 4; --ЅЅ·ФАМ ІЛ ГЎЅАґПґЩ.
	END	
	-------------------------------------------------
	-- АФ·ВЗС ЅЅ·Ф А§ДЎ°Ў АЦґВБц И®АО
	-------------------------------------------------
	IF EXISTS (SELECT *
	FROM dbo.TblPcBead
	WHERE mOwnerSerialNo = @pEquipSerialNo AND mBeadIndex = @pBeadIndex)
	BEGIN
		RETURN 5; --АМ№М БёАзЗПґВ ЅЅ·Ф А§ДЎ АФґПґЩ.
	END

	-------------------------------------------------
	-- ·й ѕЖАМЕЫ »рАФ
	-------------------------------------------------
	INSERT dbo.TblPcBead
	(
		mRegDate
		, mOwnerSerialNo
		, mBeadIndex
		, mItemNo
		, mEndDate
		, mCntUse
	)
	VALUES
	(
		GETDATE()
		, @pEquipSerialNo
		, @pBeadIndex
		, @pBeadItemNo
		, @mEndDate
		, @pCntUse
	)

	RETURN 0;
END

GO

