/******************************************************************************
**		Name: UspRegisterEventQuest
**		Desc: 捞亥飘 涅胶飘 殿废
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspRegisterEventQuest]
	@pEQuestNo				INT
	,@pType					TINYINT		-- 1: 阁胶磐 荤成, 2: 俺么瘤 沤规
	,@pParmID				INT
	,@pParmA				INT
	,@pIsOverLap			BIT
AS
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;

	--辑滚 犁矫累矫 促矫 殿废登搁 救等促.

	BEGIN TRAN

		IF @pIsOverLap <> 1
		BEGIN
			INSERT INTO dbo.TblPcQuest([mPcNo], [mQuestNo], [mValue])
			SELECT T1.mNo, @pEQuestNo, 1 -- 矫累蔼篮 1捞促.
			FROM dbo.TblPc AS T1 
				INNER JOIN dbo.TblPcState AS T2
					ON T1.mNo = T2.mNo
			WHERE T1.mDelDate IS NULL
			IF(0 <> @@ERROR)
			BEGIN
				ROLLBACK TRAN;
			END
		END

		IF @pType = 1
		BEGIN
			INSERT INTO dbo.TblPcQuestCondition([mPcNo], [mQuestNo], [mMonsterNo], [mCnt], [mMaxCnt])
			SELECT T1.mNo, @pEQuestNo, @pParmID, 0, @pParmA
			FROM dbo.TblPc AS T1 
				INNER JOIN dbo.TblPcState AS T2
					ON T1.mNo = T2.mNo
			WHERE T1.mDelDate IS NULL
			IF(0 <> @@ERROR)
			BEGIN
				ROLLBACK TRAN;
			END
		END

		IF @pType = 2
		BEGIN
			INSERT INTO dbo.TblPcQuestVisit([mPcNo], [mQuestNo], [mPlaceNo], [mVisit])
			SELECT T1.mNo, @pEQuestNo, @pParmID, 0
			FROM dbo.TblPc AS T1 
				INNER JOIN dbo.TblPcState AS T2
					ON T1.mNo = T2.mNo
			WHERE T1.mDelDate IS NULL
			IF(0 <> @@ERROR)
			BEGIN
				ROLLBACK TRAN;
			END
		END

	COMMIT TRAN

GO

