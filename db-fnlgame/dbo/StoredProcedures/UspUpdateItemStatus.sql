/******************************************************************************
**		Name: UspUpdateItemStatus
**		Desc: 鞎勳澊韰?靸來儨 臧掛潉 氤€瓴?頃滊嫟. 
**			  
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2007.05.07	JUDY				鞎勳澊韰?攵勲Μ 鞝侅毄
**		2007.08.30	JUDY				雲茧Μ 鞓る 靾橃爼 
**		2010.04.01	旯€甏戩劖			鞂撿棳歆?鞎勳澊韰滌潉 彀倦姅 瓴届毎 攴€靻?韮€鞛? 靻岇湢鞛? 須臣 歆€靻嶌嫓臧勳澊 霃欖澕頃滌 牍勱祼頃橂姅 攵€攵?於旉皜. 鞁滊Μ鞏检潉 靸堧 靸濎劚頃橂姅 瓴届毎 鞎勳澊韰?靻嶌劚鞐?攴€靻?韮€鞛? 靻岇湢鞛?, 須臣 歆€靻?鞁滉皠 臧掛潉 於旉皜頃橁矊 氤€瓴?
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateItemStatus]
	 @pPcNo			INT
	,@pSerial		BIGINT
	,@pStatus		TINYINT 
	,@pIsStack		BIT
	,@pNewSerial	BIGINT OUTPUT
	,@pNewStatus	TINYINT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE @aErrNo		INT	
	DECLARE @aCnt		INT 
	DECLARE @aCntUse	INT 
	DECLARE @aCntTarget	INT 
	DECLARE @aStatus	TINYINT 
	DECLARE @aEndDate	SMALLDATETIME
	DECLARE @aItemNo	INT
	DECLARE @aIsConfirm	BIT
	DECLARE	@aRowCnt	INT
	DECLARE	@aOwner		INT
	DECLARE	@aPracticalPeriod	INT
	DECLARE	@aBindingType		TINYINT
	
	SELECT 	@aErrNo = 0, @aRowCnt = 0

	------------------------------------------
	-- 頃措嫻 鞎勳澊韰?鞝曤炒毳?臧€鞝胳槾
	------------------------------------------
	SELECT 
		@aCnt 		= mCnt, 
		@aCntUse 	= mCntUse, 
		@aEndDate 	= mEndDate,
		@aItemNo 	= mItemNo,
		@aIsConfirm = mIsConfirm, 
		@aStatus 	= mStatus,
		@aOwner		= mOwner,
		@aPracticalPeriod	= mPracticalPeriod,
		@aBindingType		= mBindingType
	FROM dbo.TblPcInventory
	WHERE mPcNo = @pPcNo
			AND mSerialNo =  @pSerial

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END
	END

	SET		@pNewSerial = @pSerial
	SET		@pNewStatus = @aStatus



	BEGIN TRAN	
		------------------------------------------
		-- 靸來儨氤€頇旉皜 鞛堨溂氅?鞛戩梾鞁滌瀾
		------------------------------------------
		IF @aStatus <> @pStatus
		BEGIN
			------------------------------------------
			-- 鞀ろ儩順?鞎勳澊韰滌澊氅?靸來儨臧€ 氤€頇旐晿氅挫劀 鞀ろ儩鞚?鞂撿棳鞎柬暊頃勳殧臧€ 鞛堧嫟.
			------------------------------------------
			IF @pIsStack <> 0
			BEGIN
				SELECT 
					TOP 1
					@aCntTarget=ISNULL(mCnt,0), 
					@pNewSerial=mSerialNo
				FROM dbo.TblPcInventory WITH(NOLOCK)
				WHERE mPcNo = @pPcNo
						AND mItemNo = @aItemNo
						AND mEndDate = @aEndDate
						AND mIsConfirm = @aIsConfirm
						AND mStatus = @pStatus
						AND mOwner = @aOwner  
						AND mPracticalPeriod = @aPracticalPeriod
						AND mBindingType = @aBindingType	-- 雸勳爜順?鞎勳澊韰滌潣 瓴届毎 氚旍澑霌?韮€鞛呾澊 霃欖澕頃挫暭 頃滊嫟.
				
				SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo <> 0 
				BEGIN
					SET @aErrNo = 2
					GOTO T_ERR
				END
				 
				------------------------------------------
				--靸來儨電?stack順曥澊霛缄碃 頃措弰 1臧滊 氤€瓴诫悳雼? 雮橂ǜ歆€電?鞚挫爠 靸來儨毳?鞙犾頃滊嫟.
				------------------------------------------
				IF @aCnt <> 1
				BEGIN
					UPDATE dbo.TblPcInventory 
					SET 
						mCnt = mCnt-1
					WHERE mPcNo = @pPcNo
							AND mSerialNo = @pSerial

					SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF @aErrNo <> 0 OR @aRowCnt <= 0 
					BEGIN
						SET  @aErrNo = 4
						GOTO T_ERR	 
					END
				END				
				ELSE
				BEGIN	--靷牅
					DELETE dbo.TblPcInventory 
					WHERE mSerialNo = @pSerial

					SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF @aErrNo <> 0 OR @aRowCnt <= 0 
					BEGIN
						SET @aErrNo = 3
						GOTO T_ERR
					END
				END
				
				------------------------------------------
				-- 旮办〈瓴冹棎 鞐呺嵃鞚错姼
				------------------------------------------
				IF @aCntTarget <> 0
				BEGIN

					UPDATE dbo.TblPcInventory 
					SET 
						mCnt = mCnt+1
					WHERE  mPcNo = @pPcNo
							AND mSerialNo = @pNewSerial

					SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF @aErrNo <> 0 OR @aRowCnt <= 0 
					BEGIN
						SET  @aErrNo = 6
						GOTO T_ERR	 
					END
				END				
				ELSE	-- 靸?靸來儨毳?臧€歆?鞎勳澊韰滌潉 靸濎劚
				BEGIN
					EXEC @pNewSerial =  dbo.UspGetItemSerial 
					IF @pNewSerial <= 0
					BEGIN
						SET @aErrNo = 5
						GOTO T_ERR			 
					END		

					INSERT dbo.TblPcInventory 
						(mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse, mOwner, mPracticalPeriod,	mBindingType )
					VALUES
						(@pNewSerial, @pPcNo, @aItemNo, @aEndDate, @aIsConfirm, @pStatus, 1, @aCntUse, @aOwner, @aPracticalPeriod, @aBindingType)
						
					SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF @aErrNo <> 0 OR @aRowCnt <= 0 
					BEGIN
						SET @aErrNo = 5
						GOTO T_ERR			 
					END	
				END
			END			
			ELSE
			BEGIN	-- 鞀ろ儩順曥澊 鞎勲媹氅?順勳灛瓴冹棎靹?鞐呺嵃鞚错姼毵?頃挫＜氅?霅滊嫟.

				UPDATE dbo.TblPcInventory 
				SET 
					mStatus = @pStatus
				WHERE mPcNo = @pPcNo
						AND mSerialNo = @pSerial

				SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF @aErrNo <> 0 OR @aRowCnt <= 0 
				BEGIN
					SET  @aErrNo = 7
					GOTO T_ERR	 
				END
			 END
	 	 END

T_ERR:		
	IF @aErrNo = 0
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
	
T_END:	
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

