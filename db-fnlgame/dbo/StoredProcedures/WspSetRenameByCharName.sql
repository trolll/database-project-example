/******************************************************************************
**		File: WspSetRenameByCharName.sql
**		Name: WspSetRenameByCharName
**		Desc: ─│╕»┼═╕φ ║»░µ
**
**              
**		Return values:
**			0 : └█╛≈ ├│╕« ╝║░°
**			Error > 0 : SQL Error
**              
**		Parameters:
**		Input							Output
**     ----------						-----------
**
**		Auth: JUDY
**		Date: 2006-10-30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspSetRenameByCharName]
	@pNo			INT,			-- ─│╕»┼═ ╣°╚ú
	@pOrgPcNm		CHAR(12),	-- ║»░µ└ⁿ └╠╕º
	@pChgPcNm		CHAR(12)	-- ║»░µ╚─ └╠╕º
AS
BEGIN
	SET NOCOUNT ON
	SET LOCK_TIMEOUT 2000
		
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErr = 0
		
	------------------------------------------------------------------------------		
	-- ║»░µ╟╥ ─│╕»┼═ ┴╕└τ┐⌐║╬ ├╝┼⌐
	------------------------------------------------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblPc WITH(NOLOCK)
				WHERE mNm = @pChgPcNm)
	BEGIN			
		RETURN(1)					-- ║»░µ╟╥ ─│╕»┼═░í ┴╕└τ╟╤┤┘.
	END
		
	------------------------------------------------------------------------------		
	-- ─│╕»┼═╕φ ║»░µ
	------------------------------------------------------------------------------
	BEGIN TRAN
	
		UPDATE dbo.TblPc
		SET mNm = @pChgPcNm
		WHERE mNo =@pNo 
		
		SELECT	@pRowCnt = @@ROWCOUNT, 
				@pErr  = @@ERROR			

		IF @pErr <> 0 OR @pRowCnt = 0
		BEGIN
			ROLLBACK TRAN			
			RETURN(@pErr)			-- SQL System Error		
		END
	
	COMMIT TRAN
	RETURN(0)
END

GO

