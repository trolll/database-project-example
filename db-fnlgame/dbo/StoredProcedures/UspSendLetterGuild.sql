/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSendLetterGuild    Script Date: 2008-11-10 10:37:17 ******/





CREATE Procedure [dbo].[UspSendLetterGuild]
	 @pSerial		BIGINT		-- ??? ??.
	,@pFromPcNm		CHAR(12)
	,@pToGuildNm	CHAR(12)
	,@pTitle		CHAR(30)
	,@pMsg			VARCHAR(2048)
	,@pErrNoStr		VARCHAR(50)	OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'

	DECLARE @aToGuildNo	INT
	SELECT @aToGuildNo=[mGuildNo] 
	FROM dbo.TblGuild WITH(NOLOCK)
	WHERE [mGuildNm]=@pToGuildNm
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo	= 1
		SET	 @pErrNoStr	= 'eErrNoGuildNotExist'
		RETURN(@aErrNo)	 	 
	 END
	 
	INSERT dbo.TblLetter([mSerialNo], [mFromPcNm], [mTitle], [mMsg], [mToPcNm]) 
	VALUES(
		@pSerial, 
		@pFromPcNm, 
		@pTitle, 
		@pMsg, 
		@pToGuildNm)
	SET @aErrNo = @@ERROR
	IF(0 <> @aErrNo)
	BEGIN
		IF(2627 = @aErrNo)
		 BEGIN
			SET  @aErrNo	= 2
			SET	 @pErrNoStr	= 'eErrNoItemAlreadyUsed'		 
		 END
		ELSE
		 BEGIN
			SET  @aErrNo	= 3
			SET	 @pErrNoStr	= 'eErrNoSqlInternalError'		 
		 END
		GOTO LABEL_END
	END
	 
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

