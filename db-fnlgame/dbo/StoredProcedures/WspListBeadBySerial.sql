/******************************************************************************  
**  Name: WspListBeadBySerial  
**  Desc: ѕЖАМЕЫїЎ №ЪИщ ·й ё®ЅєЖ® °Л»ц  
**  
**  Auth: АМБшј±  
**  Date: 20120228  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**        
*******************************************************************************/  
CREATE PROCEDURE [dbo].[WspListBeadBySerial]  
(  
 @pSerialNo BIGINT  
 , @pType TINYINT  
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
   
 IF (@pType = 1) --TblPcInventory  
 BEGIN  
  SELECT  
   T3.mNm --ДіёЇЕНён  
   , T1.mBeadIndex --ЅЅ·ФА§ДЎ  
   , T4.IName --·й ѕЖАМЕЫ ён  
   , T4.IStatus --ѕЖАМЕЫ »уЕВ (0 : АъБЦ, 1: єёЕл, 2 : Гає№)  
   , DATEDIFF(DD,GETDATE(),T1.mEndDate) AS mEndDate --АЇИї±в°Ј  
   , T1.mCntUse --»зїл ИЅјц  
   , T1.mItemNo --·й ѕЖАМЕЫ №шИЈ  
   , T1.mRegDate --ЅАµжАПАЪ  
  FROM dbo.TblPcBead AS T1  
   INNER JOIN dbo.TblPcInventory AS T2  
  ON T1.mOwnerSerialNo = T2.mSerialNo  
   INNER JOIN dbo.TblPc AS T3  
  ON T2.mPcNo = T3.mNo  
   INNER JOIN FNLParm.dbo.DT_Item AS T4  
  ON T1.mItemNo = T4.IID  
  WHERE T1.mOwnerSerialNo = @pSerialNo  
 END  
 ELSE IF (@pType = 2) --TblPcStore  
 BEGIN  
  SELECT  
   T3.mUserId AS mNm--°иБ¤ён  
   , T1.mBeadIndex --ЅЅ·ФА§ДЎ  
   , T4.IName --·й ѕЖАМЕЫ ён  
   , T4.IStatus --ѕЖАМЕЫ »уЕВ (0 : АъБЦ, 1: єёЕл, 2 : Гає№)  
   , DATEDIFF(DD,GETDATE(),T1.mEndDate) AS mEndDate --АЇИї±в°Ј  
   , T1.mCntUse --»зїл ИЅјц  
   , T1.mItemNo --·й ѕЖАМЕЫ №шИЈ  
   , T1.mRegDate --ЅАµжАПАЪ  
  FROM dbo.TblPcBead AS T1  
   INNER JOIN dbo.TblPcStore AS T2  
  ON T1.mOwnerSerialNo = T2.mSerialNo  
   INNER JOIN FNLAccount.dbo.TblUser AS T3  
  ON T2.mUserNo = T3.mUserNo  
   INNER JOIN FNLParm.dbo.DT_Item AS T4  
  ON T1.mItemNo = T4.IID  
  WHERE T1.mOwnerSerialNo = @pSerialNo  
 END  
 ELSE IF (@pType = 3) --TblGuildStore  
 BEGIN  
  SELECT  
   T3.mGuildNm AS mNm  --±жµеён  
   , T1.mBeadIndex --ЅЅ·ФА§ДЎ  
   , T4.IName --·й ѕЖАМЕЫ ён  
   , T4.IStatus --ѕЖАМЕЫ »уЕВ (0 : АъБЦ, 1: єёЕл, 2 : Гає№)  
   , DATEDIFF(DD,GETDATE(),T1.mEndDate) AS mEndDate --АЇИї±в°Ј  
   , T1.mCntUse --»зїл ИЅјц  
   , T1.mItemNo --·й ѕЖАМЕЫ №шИЈ  
   , T1.mRegDate --ЅАµжАПАЪ  
  FROM dbo.TblPcBead AS T1  
   INNER JOIN dbo.TblGuildStore AS T2  
  ON T1.mOwnerSerialNo = T2.mSerialNo  
   INNER JOIN dbo.TblGuild AS T3  
  ON T2.mGuildNo = T3.mGuildNo  
   INNER JOIN FNLParm.dbo.DT_Item AS T4  
  ON T1.mItemNo = T4.IID  
  WHERE T1.mOwnerSerialNo = @pSerialNo  
 END  
END

GO

