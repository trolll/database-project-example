CREATE PROCEDURE [dbo].[WspGetDelCharInfoByAccount]	
	@mUserId CHAR(20) -- ░Φ┴ñ╕φ
AS
	SET NOCOUNT ON
	
	SELECT
		d.mPcNm, -- ─│╕»┼═╕φ
		p.mNm, -- ─│╕»┼═╕φ
		u.mUserId, -- ░Φ┴ñ╕φ
		p.mRegDate, -- ─│╕»┼═ ╗²╝║└╧
		p.mDelDate -- ─│╕»┼═ ╗Φ┴ª└╧
		,p.mNo
	FROM
		[dbo].[TblPc] AS p WITH (NOLOCK)		
	INNER JOIN
		[FNLAccount].[dbo].[TblUser] AS u 		
	On 
		p.mOwner = u.mUserNo
	INNER JOIN
		[dbo].[TblPcDeleted] AS d WITH (NOLOCK)
	ON
		d.mPcNo = p.mNo
	WHERE
		u.mUserId = @mUserId
		AND p.mDelDate IS NOT NULL

GO

