/******************************************************************************  
**  Name: UspCnsmBuy  
**  Desc: 위탁 아이템 구매
**  
**                
**  Return values:  
**   0 : 작업 처리 성공  
**   > 0 : SQL Error  
**                
**  Author: 김강호  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**  2011 0712   김강호     HoleCount 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmBuy]
 @pCnsmSn		BIGINT	-- 위탁 시리얼 번호
 ,@pItemNo		INT		-- 아이템 번호(일치하는지 확인해야 됨)
 ,@pIsStack		BIT		-- 스택형인지 여부(1이면 스택형)
 ,@pBuyCnt		INT	-- 구매 개수
 ,@pBuyerPcNo	INT	-- 구매자
 ,@pFeePercent	TINYINT-- 수수료율 퍼센트값(1 이상 100 미만)
 ,@pMoneySn		BIGINT -- 구매자 돈 Sn
 ,@pItemSerial	BIGINT OUTPUT	-- 구매된 아이템Sn, 기존 아이템에 병합되었다면 대상Sn
 ,@pResultCnt	INT OUTPUT		-- 구매된 개수 or 병합되었다면 최종 개수
 ,@pCategory	TINYINT OUTPUT -- 카테고리
 ,@pRegPcNo	INT OUTPUT -- 위탁 아이템 등록한 PC번호
 ,@pRegPcNm	CHAR(12) OUTPUT    -- 위탁 아이템 등록한 PC이름
 ,@pResultMoneyCnt INT OUTPUT -- 지불하고 남은 돈(@pMoneySn)
 ,@pSysChargedCnt INT OUTPUT -- 시스템이 가져간 돈
 ,@pPrice		INT OUTPUT -- 개당 판매 가격
 ,@pStatus	TINYINT OUTPUT -- 거래된 아이템의 상태
AS
BEGIN
	SET NOCOUNT ON   ;
	SET XACT_ABORT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   ;

	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;

	DECLARE @aPcNo					INT;
	DECLARE @aCurCnt				INT;
	DECLARE @aPrice					INT	;
	DECLARE @aRegDate			DATETIME;
	DECLARE @aSerialNo			BIGINT	;
	DECLARE @aItemNo				INT	;		
	DECLARE @aEndDate			DATETIME;
	DECLARE @aIsConfirm			BIT	;
	DECLARE @aStatus				TINYINT;
	DECLARE @aCntUse				SMALLINT;
	DECLARE @aOwner				INT;
	DECLARE @aPracticalPeriod	INT;
	DECLARE @aBindingType		TINYINT;
	DECLARE @aRestoreCnt			TINYINT;
	DECLARE @aHoleCount			TINYINT;

	SELECT @aRowCnt = 0, @aErrNo= 0  ;

	-- 위탁 아이템을 얻어 온다
	SELECT   
		@aPcNo	= [mPcNo],
		@aCurCnt	= [mCurCnt],
		@aPrice		= [mPrice],
		@pCategory	= [mCategoryNo],
		@aItemNo	= mItemNo
	FROM dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmSn AND GETDATE() < mTradeEndDate;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN 
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 1;  -- 위탁 아이템이 존재하지 않습니다.(TblConsignment)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 내부 오류
		END
		GOTO T_END;   
	END  


	SET @pRegPcNo = @aPcNo;
	SET @pPrice = @aPrice;

	-- 아이템 번호를 체크하는 것은 @pIsStack의 유효성을 체크하는 기능을 한다.
	IF @aItemNo <> @pItemNo
	BEGIN
		SET @aErrNo = 2; -- 아이템ID가 일치하지 않습니다.
		GOTO T_END;
	END	

	-- 본인이 올린 아이템을 구매할 수는 없다
	IF @aPcNo = @pBuyerPcNo
	BEGIN
		SET @aErrNo = 3; -- 자신이 올린 아이템을 구매할 수 없습니다.
		GOTO T_END;
	END

	-- 요청한 개수보다 부족하다(장비의 경우 @pBuyCnt가 항상 1이어야 된다)
	IF (@aCurCnt - @pBuyCnt) < 0
	BEGIN
		SET @aErrNo = 4; -- 남은 아이템 개수가 부족합니다.
		GOTO T_END;  
	END


	-- 구매자 돈 체크
	declare @aCurMoneyCnt INT;
	SELECT @aCurMoneyCnt=mCnt
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pMoneySn AND mIsSeizure = 0;
	IF @aCurMoneyCnt IS NULL OR CAST(@aCurMoneyCnt AS BIGINT) < (CAST(@aPrice AS BIGINT) * @pBuyCnt)
	BEGIN
		SET @aErrNo = 17 ;-- 돈이 부족합니다.
		GOTO T_END; 
	END


	-- 판매자 이름 가져오기
	SELECT @pRegPcNm = mNm
	FROM dbo.TblPc
	WHERE mNo = @aPcNo

	IF @pRegPcNm IS NULL
	BEGIN
		SET @aErrNo = 100;  -- DB 내부 오류
		GOTO T_END;
	END


	SELECT
		@aRegDate			= mRegDate,
		@aSerialNo			= mSerialNo,		
		@aEndDate			= mEndDate,
		@aIsConfirm			= mIsConfirm,
		@aStatus			= mStatus,
		@aCntUse			= mCntUse,
		@aOwner				= mOwner,
		@aPracticalPeriod	= mPracticalPeriod,
		@aBindingType		= mBindingType,
		@aRestoreCnt		= mRestoreCnt,
		@aHoleCount			= mHoleCount
	FROM dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmSn;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT;  
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 5  ;-- 위탁 아이템이 존재하지 않습니다.(TblConsignmentItem)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 내부 오류
		END
		GOTO T_END; 
	END 
	
	SET @pStatus = @aStatus;
		
	DECLARE @aCntTarget  INT  ;
	SET @aCntTarget = 0  ;

	-- 스택형이면 구매자에게 스택시킬 대상이 있는지 체크
	IF @pIsStack <> 0
	BEGIN
		DECLARE @aIsSeizure  BIT;  
		SET @aIsSeizure = 0;
		SELECT   
			@aCntTarget=ISNULL([mCnt],0),   
			@pItemSerial=[mSerialNo],   
			@aIsSeizure=ISNULL([mIsSeizure],0)  
		FROM dbo.TblPcInventory
		WHERE mPcNo =  @pBuyerPcNo
		AND mItemNo = @aItemNo   
		AND mIsConfirm = @aIsConfirm   
		AND mStatus =@aStatus   
		AND mOwner = @aOwner    
		AND mPracticalPeriod = @aPracticalPeriod  
		AND mBindingType = @aBindingType  ;
		-- AND (mCnt + @pBuyCnt) <= 1000000000; -- 개수 초과로 스택이 분리되는 상황을 막는다.
		  
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT;  
		IF @aErrNo<> 0  -- @aRowCnt이 0이면 스택 대상이 없는 것이다.
		BEGIN  
			SET @aErrNo = 100;  -- DB 내부 오류
			GOTO T_END;     
		END

		-- 스택시키려는 대상이 압류되어 있으면...
		-- 그냥 시리얼 새로 만들어서 구매가 가능하도록 할 수도 있긴 한데...
		IF @aCntTarget <> 0 AND @aIsSeizure <> 0  
		BEGIN  
			SET @aErrNo = 6 ;-- 아이템이 압류되어 있습니다.
			GOTO T_END;   
		END
	END


	BEGIN TRAN;	

	-- 아이템 삭제 OR 숫자 감소
	SET @aCurCnt = 0;
	IF @pIsStack <> 0 -- 스택형인 경우
	BEGIN
		UPDATE dbo.TblConsignment
		SET @aCurCnt = mCurCnt = mCurCnt - @pBuyCnt
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 7 ;-- 아이템 감소 실패(TblConsignment)
			GOTO T_END; 
		END
	END

	-- 스택형이 아니었거나 스택형을 다 지웠으면 아이템 삭제
	IF @aCurCnt = 0
	BEGIN
		DELETE dbo.TblConsignmentItem
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 8; -- 아이템 삭제 실패(TblConsignmentItem)
			GOTO T_END; 
		END

		DELETE dbo.TblConsignment
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 9; -- 아이템 삭제 실패(TblConsignment)
			GOTO T_END; 
		END
	END
	
	-- 구매자 돈 감소, 돈을 아예 삭제했을 경우 삭제 당시의 개수가 초기 개수와 다르면 안된다.
	IF @aCurMoneyCnt = (@aPrice * @pBuyCnt)
	BEGIN
		SET @pResultMoneyCnt = 0; -- 남은 실버 개수 OUTPUT
		DELETE dbo.TblPcInventory  
		WHERE mSerialNo = @pMoneySn and mCnt = @aCurMoneyCnt AND mIsSeizure = 0;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 10 ;-- 구매자돈 감소 실패
			GOTO T_END;
		END
	END
	ELSE
	BEGIN
		UPDATE dbo.TblPcInventory
		SET @pResultMoneyCnt = mCnt = mCnt - (@aPrice * @pBuyCnt)
		WHERE mSerialNo = @pMoneySn AND mIsSeizure = 0;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 10 ;-- 구매자돈 감소 실패
			GOTO T_END;  
		END
	END

	
	IF @pIsStack = 0 -- 스택형이 아닌 경우
	BEGIN		
		SET @pItemSerial = @aSerialNo;
		SET @pResultCnt = 1;
		-- 구매자에게 아이템 추가
		INSERT dbo.TblPcInventory(
			[mSerialNo],  
			[mPcNo],   
			[mItemNo],   
			[mEndDate],   
			[mIsConfirm],   
			[mStatus],   
			[mCnt],   
			[mCntUse],  
			[mOwner],  
			[mPracticalPeriod], 
			mBindingType,
			mHoleCount )  
		VALUES(  
			@pItemSerial,  
			@pBuyerPcNo,   
			@aItemNo,   
			@aEndDate,   
			@aIsConfirm,   
			@aStatus,   
			@pBuyCnt,   
			@aCntUse,  
			@aOwner,  
			@aPracticalPeriod, 
			@aBindingType,
			@aHoleCount) ;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 11 ;-- 구매자에게 아이템 추가 실패
			GOTO T_END;
		END
	END
	ELSE -- 스택형인 경우
	BEGIN
		IF @aCntTarget = 0 -- 누적시킬 대상이 없는 경우
		BEGIN
			EXEC @pItemSerial =  dbo.UspGetItemSerial;  -- 시리얼 얻기   
			IF @pItemSerial <= 0  
			BEGIN  
				ROLLBACK TRAN  ;
				SET @aErrNo = 12 ;-- 아이템 시리얼 생성 실패
				GOTO T_END;   
			END
			SET @pResultCnt = @pBuyCnt;
			-- 구매자에게 아이템 추가
			INSERT dbo.TblPcInventory(
				[mSerialNo],  
				[mPcNo],   
				[mItemNo],   
				[mEndDate],   
				[mIsConfirm],   
				[mStatus],   
				[mCnt],   
				[mCntUse],  
				[mOwner],  
				[mPracticalPeriod], 
				mBindingType,
				mHoleCount )  
			VALUES(  
				@pItemSerial,  
				@pBuyerPcNo,   
				@aItemNo,   
				@aEndDate,   
				@aIsConfirm,   
				@aStatus,   
				@pBuyCnt,   
				@aCntUse,  
				@aOwner,  
				@aPracticalPeriod, 
				@aBindingType,
				@aHoleCount) ;
			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
			IF @aErrNo<> 0 OR @aRowCnt <= 0   
			BEGIN  
				ROLLBACK TRAN;
				SET @aErrNo = 13 ;-- 구매자에게 아이템 추가 실패
				GOTO T_END;   
			END
		END
		ELSE -- 누적시킬 대상이 있는 경우
		BEGIN
			UPDATE dbo.TblPcInventory
			SET @pResultCnt = mCnt = mCnt + @pBuyCnt
			WHERE mSerialNo = @pItemSerial;
			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
			IF @aErrNo<> 0 OR @aRowCnt <= 0   
			BEGIN  
				ROLLBACK TRAN;
				SET @aErrNo = 14 ;-- 아이템 누적 실패(에러)
				GOTO T_END;  
			END
			IF 1000000000 < @pResultCnt
			BEGIN
				ROLLBACK TRAN;
				SET @aErrNo = 15; -- 아이템 누적 실패(개수 초과)
				GOTO T_END;
			END
		END
	END

	-- 판매자 계좌에 수수료를 제외한 판매 대금 추가

	DECLARE @aBalance BIGINT;
	SELECT @aBalance=mBalance
	FROM dbo.TblConsignmentAccount
	WHERE mPcNo=@aPcNo;
	IF @aBalance IS NULL
	BEGIN
		INSERT dbo.TblConsignmentAccount
		VALUES(@aPcNo, 0);
	END

	SET @pSysChargedCnt = @pBuyCnt*( CAST( (@aPrice*(@pFeePercent/100.0)) AS INT) );

	UPDATE dbo.TblConsignmentAccount
	SET mBalance = mBalance + ((@pBuyCnt*@aPrice) - @pSysChargedCnt)
	WHERE mPcNo = @aPcNo;
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 16 ;-- 판매 대금 추가 실패
		GOTO T_END; 
	END


T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN ; 

T_END:     
	SET NOCOUNT OFF   ;
	RETURN(@aErrNo) ;
END

GO

