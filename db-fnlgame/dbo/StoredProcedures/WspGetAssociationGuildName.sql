--------------------------------
-- ░┤├╝╕φ : WspGetAssociationGuildName
-- ╝│╕φ : ┐¼╟╒ ▒µ╡σ└╟ ╕±╖╧└╗ ░í┴«┐┬┤┘
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.07.11
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
--------------------------------


CREATE PROCEDURE [dbo].[WspGetAssociationGuildName] 
	@mAssNo		INT	-- ┐¼╟╒▒µ╡σ └╬╜─╣°╚ú
AS
	SET NOCOUNT ON

	SELECT
		mGuildNo
	FROM
		TblGuildAssMem 
	WITH (NOLOCK)
	WHERE
		mGuildAssNo = @mAssNo
		
	SET NOCOUNT OFF

GO

