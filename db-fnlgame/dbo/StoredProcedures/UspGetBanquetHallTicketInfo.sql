/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallTicketInfo    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspGetBanquetHallTicketInfo]
	 @pTicketSerialNo	BIGINT				-- ???????
	,@pTerritory		INT OUTPUT			-- ???? [??]
	,@pBanquetHallType	INT OUTPUT			-- ????? [??]
	,@pBanquetHallNo	INT OUTPUT			-- ????? [??]
	,@pOwnerPcNo		INT OUTPUT			-- ????? [??]
	,@pOwnerPcName		NVARCHAR(12) OUTPUT	-- ??????? [??/??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	SET		@pTerritory = -1
	SET		@pBanquetHallType = -1
	SET		@pBanquetHallNo = -1
	SET		@pOwnerPcName = ''

	SELECT @pTerritory = mTerritory
		,@pBanquetHallType = mBanquetHallType
		,@pBanquetHallNo = mBanquetHallNo
		,@pOwnerPcNo = mOwnerPcNo
		,@pOwnerPcName = ISNULL((CASE mOwnerPcNo
					WHEN 0 THEN N'<NONE>'
					ELSE (SELECT mNm FROM dbo.TblPc  WHERE mNo = mOwnerPcNo)
					END), N'<FAIL>')
	FROM dbo.TblBanquetHallTicket 
	WHERE mTicketSerialNo = @pTicketSerialNo

	IF @@ROWCOUNT = 0 
	BEGIN
		RETURN(2)
	END

	RETURN(0)

GO

