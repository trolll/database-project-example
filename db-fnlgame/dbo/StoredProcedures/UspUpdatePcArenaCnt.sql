/******************************************************************************
**		Name: UspUpdatePcArenaCnt
**		Desc: 酒饭唱 曼咯冉荐 诀单捞飘
**
**		Auth: 沥柳龋
**		Date: 2016-09-02
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePcArenaCnt]
	@pPcNo			INT 
	,@pType			TINYINT		-- 0: 拜傈, 1: 焊胶傈
	,@pCnt			INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @pType = 0
	BEGIN
		UPDATE dbo.TblPcState
		SET mFierceCnt = 
			CASE
				WHEN 0 < (mFierceCnt + @pCnt)
					THEN mFierceCnt + @pCnt
				ELSE 0
			END 			
		WHERE mNo = @pPcNo
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(1);
		END
	END

	IF @pType = 1
	BEGIN
		UPDATE dbo.TblPcState
		SET mBossCnt = 
			CASE
				WHEN 0 < (mBossCnt + @pCnt)
					THEN mBossCnt + @pCnt
				ELSE 0
			END 	
		WHERE mNo = @pPcNo
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(2);
		END
	END
	 		 
	RETURN(0);

GO

