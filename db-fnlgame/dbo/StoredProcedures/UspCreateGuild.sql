/******************************************************************************
**		Name: UspCreateGuild
**		Desc: 길드 생성
**
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      20100730	김강호		길드마크 변경
*******************************************************************************/ 
CREATE  PROCEDURE [dbo].[UspCreateGuild]
	  @pGuildNm CHAR(12)  
	 ,@pPcNo  INT  
	 ,@pGuildNo INT   OUTPUT  
	 ,@pSeqNo INT  OUTPUT 
	 ,@pErrNoStr VARCHAR(50) OUTPUT  
AS  
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
	DECLARE @aErrNo  INT  
	SET  @aErrNo  = 0  
	SET  @pErrNoStr = 'eErrNoSqlInternalError'  

	-- check나 index가 지정됐으므로 DBMS에 의해서 오류가 검출되지만  
	-- 적당한 오류코드를 반환하지 않으므로 주로 발생할 오류를 먼저 검사한다.  
	IF EXISTS( SELECT [mGuildNm]   
		FROM dbo.TblGuild   
		WHERE [mGuildNm] = @pGuildNm )  
	BEGIN  
		SET @aErrNo = 51  
		SET @pErrNoStr = 'eErrNoGuildAlreadyExistNm'  
		RETURN(@aErrNo)  
	END  

	IF EXISTS( SELECT [mPcNo]   
		FROM dbo.TblGuildMember WHERE [mPcNo] = @pPcNo )  
	BEGIN  
		SET @aErrNo = 52  
		SET @pErrNoStr = 'eErrNoCharAlreadyEnterGuild'  
		RETURN(@aErrNo)  
	END  
  
	BEGIN TRAN   

		SET @pSeqNo  = 0  
		INSERT INTO dbo.TblGuild([mGuildNm], [mGuildSeqNo])   
		VALUES(  
		@pGuildNm,   
		@pSeqNo)  
		
		IF(0 <> @@ERROR)  
		BEGIN  
			SET @aErrNo = 1  
			GOTO LABEL_END    
		END      
		SET @pGuildNo = @@IDENTITY  
		 
		INSERT INTO dbo.TblGuildMember([mGuildNo], [mPcNo], [mGuildGrade])  
		VALUES(  
		@pGuildNo,    
		@pPcNo,   
		0)  
		IF(0 <> @@ERROR)  
		BEGIN  
			SET @aErrNo = 2  
			GOTO LABEL_END    
		END   

	LABEL_END:    
	IF(0 = @aErrNo)  
		COMMIT TRAN  
	ELSE  
		ROLLBACK TRAN  

	SET NOCOUNT OFF   
	RETURN(@aErrNo)

GO

