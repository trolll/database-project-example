/******************************************************************************
**		Name: UspSetPcCoolTime
**		Desc: 유저의 쿨타임이 일정 시간 이상일 경우 DB에 입력한다.
**
**		Auth: 정구진
**		Date: 09.09.30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	-----------------------------------------------
**      09.05.16    정구진				같은 내용이 존재할 경우 무시
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcCoolTime]
	@pPcNo				INT,
	@pSID				INT,
	@pCoolTimeGroup		SMALLINT,
	@pTotalTime			INT
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT *
	FROM dbo.TblPcCoolTime 
	WHERE mPcNo = @pPcNo AND mSID = @pSID
		
	IF(0 < @@ROWCOUNT)
	BEGIN
		RETURN(0)
	END
	
	INSERT INTO dbo.TblPcCoolTime (mPcNo, mSID, mCoolTimeGroup, mRemainTime)
	VALUES (
			@pPcNo
			, @pSID
			, @pCoolTimeGroup
			, @pTotalTime
	)

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

