-----------------------------------------------------------------------------------
-- PROCEDURE
-----------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[WspGetGuildMemberInfo]	
	@mGuildNm CHAR(12) -- ±жµеён
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		p.mNm, 
		s.mLevel, 
		p.mClass, 
		gm.mNickNm,
		gm.mGuildGrade,
		gm.mRegDate,
		u.mUserID,
		u.mUserNo
	FROM [dbo].[TblGuildMember] AS gm 
		INNER JOIN [dbo].[TblGuild] AS g 
			ON gm.mGuildNo = g.mGuildNo
		INNER JOIN [dbo].[TblPc] AS p 
			ON gm.mPcNo = p.mNo
		INNER JOIN [dbo].[TblPcState] AS s 
			ON	gm.mPcNo = s.mNo
		INNER JOIN FNLAccount.dbo.TblUser AS u
			ON p.mOwner = u.mUserNo	
	WHERE g.mGuildNm = @mGuildNm		
	ORDER BY  gm.mGuildGrade, s.mLevel DESC

GO

