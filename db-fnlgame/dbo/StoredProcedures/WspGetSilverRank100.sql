CREATE PROCEDURE [dbo].[WspGetSilverRank100]
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		TOP 30
		  u.mUserId
		, sum(cast(p.mCnt as bigint)) AS MoneyCnt
	FROM
		dbo.TblPcStore AS p 
		INNER JOIN dbo.TblPc AS pc 
			ON	pc.mOwner = p.mUserNo 
				AND p.mItemNo = 409
	INNER JOIN
		[FNLAccount].[dbo].[TblUser] AS u		
			ON	u.mUserNo = pc.mOwner 
				AND pc.mDelDate IS NULL 
				AND u.mUserId <> 'NON-PC'
	GROUP BY u.mUserId
	ORDER BY MoneyCnt DESC

GO

