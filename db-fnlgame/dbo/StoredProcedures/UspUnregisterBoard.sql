/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspUnregisterBoard    Script Date: 2008-11-10 10:37:18 ******/




---------------------------------------------
-- 11.08
---------------------------------------------
CREATE  Procedure [dbo].[UspUnregisterBoard]
	 @pPcNm			CHAR(12)
	,@pBoardId		TINYINT
	,@pBoardNo		INT
	,@pIsGM			BIT	-- ??? ?? ??
	,@pErrNoStr		VARCHAR(50)	OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'

	IF (@pIsGM = 0)		-- ?????
	BEGIN	
		DELETE dbo.TblBoard 
		WHERE ([mBoardNo]=@pBoardNo) 
				AND ([mFromPcNm]=@pPcNm) 
				AND ([mBoardId]=@pBoardId)
		IF(1 <> @@ROWCOUNT)
		 BEGIN
			SET	@aErrNo	= 2
			SET	 @pErrNoStr	= 'eErrNoBoardNotExist'
			GOTO LABEL_END		 
		 END	
	END
	ELSE IF (@pIsGM = 1)	-- ????? ?????? ??? ??
	BEGIN
		DELETE dbo.TblBoard 
		WHERE ([mBoardNo]=@pBoardNo) 
				AND ([mBoardId]=@pBoardId)
		IF(1 <> @@ROWCOUNT)
		 BEGIN
			SET	@aErrNo	= 2
			SET	 @pErrNoStr	= 'eErrNoBoardNotExist'
			GOTO LABEL_END		 
		 END	
		

	END
			
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

