CREATE PROCEDURE [dbo].[WspGetTransferPcInventory]
	@pPcNo	INT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	
	SELECT 
		T2.IName,
		mSvrNo,
		mOLDSerialNo,
		mRegDate,
		mSerialNo,
		mPcNo,
		mItemNo,
		mEndDate,
		mIsConfirm,
		mStatus,
		mCnt,
		mCntUse,
		mIsSeizure,
		mApplyAbnItemNo,
		mApplyAbnItemEndDate,
		mOwner,
		mPracticalPeriod		
	FROM dbo.T_TblPcInventory T1
		INNER JOIN FNLParm.dbo.DT_ITEM T2
			ON T1.mItemNo = T2.IID
	WHERE mPcNo = @pPcNo

GO

