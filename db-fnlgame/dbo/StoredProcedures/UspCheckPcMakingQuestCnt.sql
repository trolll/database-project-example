/******************************************************************************
**		Name: UspCheckPcMakingQuestCnt
**		Desc: 父甸绢柳 涅胶飘啊 割俺牢啊.
**
**		Auth: 沥柳龋
**		Date: 2010-02-23
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckPcMakingQuestCnt]
	 @pPcNo			INT 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
			 T1.mQMCnt
			,T1.mGuildQMCnt
    FROM dbo.TblPcState T1
    		INNER JOIN dbo.TblPc  T2 
			ON T1.mNo = T2.mNo 
	WHERE T1.mNo = @pPcNo

GO

