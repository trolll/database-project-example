/******************************************************************************
**		Name: UspUpdateMacroPosition
**		Desc: ¸ÅÅ©·Î À§Ä¡ ¾÷µ¥ÀÌÆ®
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateMacroPosition]
	@pPcNo			INT
	,@pMacroID		TINYINT
	,@pMacroSlot	TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE dbo.TblMacroList
	SET mMacroSlot = @pMacroSlot
	WHERE mPcNo = @pPcNo AND mMacroID = @pMacroID

GO

