/******************************************************************************
**		Name: UspGetChatFilter
**		Desc: Ã¤ÆÃÂ÷´ÜÁ¤º¸ Á¶È¸
**		Test:			
**		Auth: ³²¼º¸ð
**		Date: 2014/03/04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetChatFilter]
	 @pPcNo	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		[mChatFilterPcNo] =
			CASE 
				WHEN a.[mChatFilterPcNm] <> c.[mNm] THEN 0
				WHEN a.[mChatFilterPcNo] <> c.mNo THEN 0
				WHEN c.[mNm] IS NULL THEN 0
				ELSE c.mNo	-- new pc number
			END,
		RTRIM(a.[mChatFilterPcNm]), 
		ISNULL(b.[mLogoutTm], GETDATE())
	FROM dbo.TblPcChatFilter AS a 
		LEFT OUTER JOIN dbo.TblPc c
			ON ( a.mChatFilterPcNm = c.mNm)		
		LEFT JOIN dbo.TblPcState AS b 
			ON ( c.mNo = b.mNo )
	WHERE mOwnerPcNo = @pPcNo

GO

