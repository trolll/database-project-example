/******************************************************************************
**		Name: UspResetSpecificTerritoryGuildAgit
**		Desc: 특정 길드 아지트를 초기화 한다.
**		Warn: 테이블에 Territory 를 인덱스로 잡은 곳이 없다. 
**			  그러므로 실제 런타임에 호출하려면 DBA 와 상의가 필요하다. (현재는 게임 초기화 시점에 호출함)
**
**		Auth: 김 광섭
**		Date: 2010-06-09
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspResetSpecificTerritoryGuildAgit]
	@pTerritory		INT		-- 초기화할 특정 영지
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT;
	SET		@aErrNo 	= 0;
	
	BEGIN TRAN
	
		-- 특정 영지의 하우스를 초기화한다. (환불은 필요없다.)
		UPDATE	dbo.TblGuildAgit	
		SET mOwnerGID = 0, mGuildAgitName = '', mLeftMin = 0, mIsSelling = 0, mSellingMoney = 0, mBuyingMoney = 0 
		WHERE	mTerritory = @pTerritory
		
		IF(0 <> @@ERROR)
		 BEGIN
			SET		@aErrNo	= 1;	-- DB Error
			GOTO	Label_End;
		 END
		 
		-- 특정 영지의 경매 내용을 초기화 한다. (환불은 필요없다.)
		UPDATE	dbo.TblGuildAgitAuction	SET mCurBidGID = 0, mCurBidMoney = 0 
		WHERE mTerritory = @pTerritory;
		
		IF(0 <> @@ERROR)
		 BEGIN
			SET		@aErrNo	= 2;	-- DB Error
			GOTO	Label_End;
		 END
		 
		-- 특정 영지의 초대장 내용을 초기화 한다.
		DELETE	dbo.TblGuildAgitTicket	
		WHERE mTerritory = @pTerritory;
		
		IF(0 <> @@ERROR)
		 BEGIN
			SET		@aErrNo	= 2;	-- DB Error
			GOTO	Label_End;
		 END

Label_End:
	IF(0 <> @aErrNo)
	 BEGIN
		ROLLBACK TRAN;
	 END
	ELSE
	 BEGIN
		COMMIT TRAN;
	 END
	 
	 
	RETURN(@aErrNo);

GO

