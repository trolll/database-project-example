/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspListLetter    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspListLetter]
	 @pPcNo		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			
	
	SELECT 
		a.mSerialNo, 
		a.mTitle, 
		a.mFromPcNm, 
		a.mToPcNm
	FROM dbo.TblLetter AS a 
		INNER JOIN TblPcInventory AS b 
		ON a.mSerialNo  = b.mSerialNo
	WHERE b.mPcNo =@pPcNo
			AND b.mItemNo IN (722, 724)
	
	SET NOCOUNT OFF

GO

