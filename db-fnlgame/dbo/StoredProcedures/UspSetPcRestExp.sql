/******************************************************************************
**		Name: dbo.UspSetPcRestExp
**		Desc: TblPcState의 휴식 경험치 설정
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010.12.02	공석규				TblPcState의 mRestExp > mRestExpGuild 변경
**										mRestExpActivate, mRestExpDeactivate 추가로
**										@pRestExp를 @pRestExpGuild로 수정
**										@pRestExpActivate, @pRestExpDeactivate 추가
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcRestExp]
	@pPcNo				INT,
	@pRestExpGuild		BIGINT,
	@pRestExpActivate	BIGINT,
	@pRestExpDeactivate	BIGINT
AS  
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aRv INT
			, @aErrNo INT
			, @aRowCnt INT;

	SELECT @aRv = 0, @aErrNo = 0, @aRowCnt = 0;

	UPDATE dbo.TblPcState
	SET mRestExpGuild = @pRestExpGuild
		, mRestExpActivate = @pRestExpActivate
		, mRestExpDeactivate = @pRestExpDeactivate
	WHERE mNo = @pPcNo;

	SELECT @aErrNo = @@Error, @aRowCnt = @@RowCount;

	IF @aErrNo <> 0
	BEGIN
		SET @aRv = 1;
	END

	IF @aRowCnt <= 0
	BEGIN
		SET @aRv = 1;
	END

	RETURN (@aRv);

GO

