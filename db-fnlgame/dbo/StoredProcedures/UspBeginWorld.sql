/******************************************************************************
**		File: UspBeginWorld.sql
**		Name: UspBeginWorld
**		Desc: ÇÊµå¼­¹ö°¡ ½ÃÀÛµÆ´Ù. ÃÊ±âÈ­µîÀ» ÇÑ´Ù.
**
**		Warn: 1.¸¸·áµÈ itemÀ» »èÁ¦ÇÑ´Ù. 
**			:   (ÀåºñµîÀ» CASCADE¸¦ ÀÌ¿ëÇØ¼­ »èÁ¦°¡ µÇµµ·Ï ÇÑ´Ù.)
**			: 1.¸ó½ºÅÍ°¡ ¼ÒÀ¯ÇÑ itemµµ »èÁ¦¸¦ ÇÑ´Ù.
**			: 1.loginÁßÀÎ PC¸¦ logout½ÃÅ²´Ù.
**
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2008.05.07	JUDY				±â°£Á¦ ¾ÆÀÌÅÛ º¹±¸ ±â´É Ãß°¡
**		2008.05.16	JUDY				Á¾·á ¾ÆÀÌÅÛ Á¤¸®	
**		2008.08.01	JUDY				ÀÌº¥Æ® ±â°£ Á¾·á ÇÔ¼ö Ã³¸® ±â´É ¿À·ù.
**		2009.01.21	JUDY				Æ©´×1Â÷(ÀÌº¥Æ® ¾ÆÀÌÅÛ °¡Á®¿À´Â Áß... ÀÎµ¦½º°¡ ÀÌ»óÇÏ°Ô Å¸°Å³ª,, ´Ù¸¥ ¹®Á¦°¡ ¹ß»ýÇÑ´Ù.) 
**		2009.04.23	JUDY				ÀÌº¥Æ® ¾ÆÀÌÅÛ ±â°£ ¿¬Àå Ãß°¡
**		2010.04.06	JUDY				ÀÏÀÏ Äù½ºÆ® ÃÊ±âÈ­ ÇÑ´Ù.
**		2010.07.29	Á¤ÁøÈ£				ºÀÀÎµÈ ´øÁ¯ º¸°­
**		2011.05.12  Á¤ÁøÈ£				ºÀÀÎµÈ ´øÀü Á¶°Ç ¼öÁ¤
**		2012.10.01	³²±âºÀ				±¸½½ÀÌ Àû¿ëµÈ ¾ÆÀÌÅÛ ±â°£ Á¾·á½Ã Ã³¸®
**		2013.03.27	Á¤ÁøÈ£				¸¸·áµÈ ÀÌº¥Æ® Äù½ºÆ®¸¦ »èÁ¦ÇÑ´Ù. (Äù½ºÆ® ¸ÞÀÌÅ· ½Ã½ºÅÛ)
**		2013.10.15	Á¤Áø¿í				ÀèÆÌÈ®ÀÎÁõÀ» »èÁ¦ÇÑ´Ù. (ÀèÆÌ ½Ã½ºÅÛ)
**		2014.04.24	Á¤Áø¿í				ÀèÆÌÈ®ÀÎÁõÀ» »èÁ¦ÇÑ´Ù.  2Ãþ´øÀü Ãß°¡ (ÀèÆÌ ½Ã½ºÅÛ)
**		2014.04.25	Á¤ÁøÈ£				¸¸·áµÈ ÀÏÁ¤À» »èÁ¦ÇÑ´Ù. (´Þ·Â ½Ã½ºÅÛ)
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspBeginWorld]
	@pSvrNo	SMALLINT
	, @pIsItemRecovery BIT
	, @pItemRecoveryTgTm	INT		-- Recovery Item Target interval time 
	, @pItemRecoveryTm	INT			-- Recovery item default time
	, @pItemExpireDD	SMALLINT = 0	-- ¾ÆÀÌÅÛ »èÁ¦¸¦ À§ÇÑ »èÁ¦ÀÏ( ÇöÀçÀÏ -5ÀÏÀü Á¾·á¾ÆÀÌÅÛ »èÁ¦)
AS
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;
	
	DECLARE @aLastSupportMinute INT,
					@aErr				INT,
					@aStep				SMALLINT,
					@aItemExpireDate	SMALLDATETIME,
					@aCurrDate			SMALLDATETIME,
					@aArrExpireItem		VARCHAR(8000),
					@aSupportTime		INT,
					@aLastTime			INT,
					@aQuestNoStr		VARCHAR(3000),
					@aEventQuestNoStr	VARCHAR(3000),
					@aMakingQuestNoStr	VARCHAR(500)
		
	DECLARE @aItemNo			TABLE (
				mItemNo		INT
			)				
			
	SELECT  @aLastSupportMinute = 0
			, @aErr =0
			, @aStep = 0 -- ÀÚµ¿ ¿¬Àå ÀÛ¾÷
			, @aItemExpireDate = DATEADD(DD, -@pItemExpireDD, GETDATE() )
			, @aCurrDate = GETDATE()
			, @aArrExpireItem = ''
			, @aSupportTime = 0
			, @aLastTime = 0
			, @aQuestNoStr = ''		
			, @aEventQuestNoStr = ''
			, @aMakingQuestNoStr = ''	
	
	-- Event Expire item 
	-- @aCurrDate -> @aItemExpireDate º¯°æ
	EXEC FNLParm.dbo.UspGetExpireEventItem @pSvrNo, @aItemExpireDate, @aArrExpireItem OUTPUT		
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 2	-- ¸¶Áö¸· ¼­¹ö Áö¿ø ¼­¹ö Á¤º¸ ¾ò±â ½ÇÆÐ
		GOTO TRN_END	
	END 
	
	-- ÀÏÀÏ Äù½ºÆ® ÃÊ±âÈ­ Á¤º¸¸¦ ¾ò¾î¿Â´Ù.
	-- ÃÊ±âÈ­ ÇØ¾ßµÉ Äù½ºÆ® ¹øÈ£¸¦ ±¸ÇÑ´Ù.
	EXEC FNLParm.dbo.UspGetExpireQuest @pSvrNo, @aCurrDate, @aQuestNoStr OUTPUT, @aSupportTime OUTPUT, @aLastTime OUTPUT
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 1	-- sql error
		GOTO TRN_END	
	END 	
	
	-- ¿Ï·áµÈ ÀÌº¥Æ® Äù½ºÆ® Á¤º¸¸¦ ¾ò¾î¿Â´Ù.
	-- »èÁ¦ ÇØ¾ßµÉ Äù½ºÆ® ¹øÈ£¸¦ ±¸ÇÑ´Ù.
	EXEC FNLParm.dbo.UspGetExpireEventQuest @pSvrNo, @aEventQuestNoStr OUTPUT
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 1	-- sql error
		GOTO TRN_END	
	END 	

	-- ¸¸·áµÈ ¸ÞÀÌÅ· ÀÌº¥Æ®Äù½ºÆ® ¹øÈ£¸¦ °¡Á®¿Â´Ù.
	EXEC FNLParm.dbo.UspGetExpireMakingEventQuest @pSvrNo, @aMakingQuestNoStr OUTPUT
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 1	-- sql error
		GOTO TRN_END	
	END 


	INSERT INTO @aItemNo(mItemNo)
	SELECT CONVERT(INT, element) mObjID
	FROM  dbo.fn_SplitTSQL(  @aArrExpireItem, ',' )
	WHERE LEN(RTRIM(element)) > 0
	
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 2	-- ¸¶Áö¸· ¼­¹ö Áö¿ø ¼­¹ö Á¤º¸ ¾ò±â ½ÇÆÐ
		GOTO TRN_END	
	END 		
	
	IF @pItemExpireDD > 0
		SET @pItemExpireDD = -@pItemExpireDD		-- ±â°£ Á¾·á Ã³¸®¸¦ À§ÇØ¼­.
		
	IF @pIsItemRecovery = 1
	BEGIN
	
		EXEC @aLastSupportMinute = FNLParm.dbo.UspGetParmSvrLastSupportDate @pSvrNo
		SELECT @aErr = @@ERROR
		
		IF @aErr <> 0 
		BEGIN
			SET @aErr = 2	-- ¸¶Áö¸· ¼­¹ö Áö¿ø ¼­¹ö Á¤º¸ ¾ò±â ½ÇÆÐ
			GOTO TRN_END
		END
							
		IF @aLastSupportMinute >= @pItemRecoveryTgTm
		BEGIN	
			SET @aLastSupportMinute = @aLastSupportMinute + @pItemRecoveryTm	-- ±âº» ¿¬Àå ¿ÀÂ÷ ½Ã°£
			
			EXEC @aErr = dbo.UspUptItemDate @aLastSupportMinute		-- ±â°£Á¦ ¾ÆÀÌÅÛ ÀÚµ¿ ¿¬Àå
			
			IF @aErr <> 0 
			BEGIN
				SET @aErr = 3	-- ±â°£Á¦ ¾ÆÀÌÅÛ ÀÚµ¿ ¿¬Àå ½ÇÆÐ
				GOTO TRN_END
			END

		END 
		ELSE
			SET @aLastSupportMinute = 0	-- ÀÚµ¿ º¸»óÀÌ ÀÌ·ç¾îÁöÁö ¾Ê´Â´Ù¸é º¸»ó ½Ã°£ Å¬¸®¾î ½ÃÄÑÁØ´Ù.

	END 

	-- Â÷ÀÌ°¡ 24½Ã°£ ÀÌ»óÀÌ¸é ¹«Á¶°Ç ÃÊ±âÈ­ÇÑ´Ù
	-- Äù½ºÆ® ÃÊ±âÈ­ ½Ã°£ 6½Ã(¼­¹ö¿¡ ÇÏµåÄÚµù µÇ¾îÀÖÀ½)
	IF 24 <= @aSupportTime OR @aLastTime < 6
	BEGIN
		-- ÃÊ±âÈ­
		EXEC @aErr = dbo.UspPopPcQuestConditionAll @aQuestNoStr		-- Äù½ºÆ® »èÁ¦
		IF @aErr <> 0 
		BEGIN
			SET @aErr = 1	-- sql error
			GOTO TRN_END
		END	
	END

	BEGIN
		--Á¾·áµÈ ÀÌº¥Æ® Äù½ºÆ®´Â PcQuest¿¡¼­ »èÁ¦ÇÑ´Ù.
		EXEC @aErr = dbo.UspDeletePcQuestExpireEvent @aEventQuestNoStr		-- Äù½ºÆ® »èÁ¦			
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			GOTO TRN_END
		END				
	END

	-- ¸¸·áµÈ ÀÌº¥Æ® Äù½ºÆ® »èÁ¦ (Äù½ºÆ® ¸ÞÀÌÅ· ½Ã½ºÅÛ)
	BEGIN
		EXEC @aErr = dbo.UspDelExpireMakingEventQuest @aMakingQuestNoStr	
		IF @aErr <> 0 
		BEGIN
			SET @aErr = 1	-- sql error
			GOTO TRN_END
		END	
	END

	-- ¸¸·áµÈ ÀÏÁ¤ »èÁ¦ (´Þ·Â ½Ã½ºÅÛ)
	BEGIN
		EXEC @aErr = dbo.UspDelExpireCalendarSchedule	
		IF @aErr <> 0 
		BEGIN
			SET @aErr = 1	-- sql error
			GOTO TRN_END
		END	
	END
	
	BEGIN TRAN
		
		SET @aStep = 1	-- ¾ÆÀÌÅÛ °»½Å ÀÛ¾÷


		-- 1. Inventory, Store, GuildStroe ±â°£ Á¾·á ¾ÆÀÌÅÛ »èÁ¦ ¹× ½ºÅÃ Á¾·áµÈ ¾ÆÀÌÅÛ »èÁ¦
		-- 1. Event Á¾·á ¾ÆÀÌÅÛ »èÁ¦
		-- ¾ÆÀÌÅÛ »èÁ¦ ÈÄ ¿¬°üµÈ '·é' ¿ø»ó º¹±Í.

		-- ·é Á¾·á ¾ÆÀÌÅÛ »èÁ¦
		DELETE dbo.TblPcBead
		WHERE mEndDate <= @aItemExpireDate	
			OR mItemNo IN (				
				SELECT mItemNO
				FROM @aItemNo				
			)	
			
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		


		DECLARE @TempTable table (
			mID int IDENTITY PRIMARY KEY,
			mSerialNo bigint NOT NULL,
			mBeadIndex tinyint NOT NULL,
			mPcNo int NOT NULL);


		-- ¾ÆÀÌÅÛ »èÁ¦Àü ¿¬°üµÈ ·é Á¤º¸ ÀúÀå.
		INSERT INTO @TempTable
			SELECT dbo.TblPcInventory.mSerialNo, dbo.TblPcBead.mBeadIndex, dbo.TblPcInventory.mPcNo
				FROM dbo.TblPcBead INNER JOIN dbo.TblPcInventory ON dbo.TblPcInventory.mSerialNo = dbo.TblPcBead.mOwnerSerialNo
			WHERE dbo.TblPcInventory.mEndDate <= @aItemExpireDate	
				OR dbo.TblPcInventory.mCnt < 1		
				OR dbo.TblPcInventory.mPcNo IN(0,1)	
				OR dbo.TblPcInventory.mItemNo IN (				
					SELECT mItemNO
					FROM @aItemNo)

		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		


		-- ±â°£ ¿Ï·áµÈ ¾ÆÀÌÅÛ »èÁ¦
		DELETE dbo.TblPcInventory
		WHERE mEndDate <= @aItemExpireDate	
			OR mCnt < 1		
			OR mPcNo IN(0,1)	
			OR mItemNo IN (				
				SELECT mItemNO
				FROM @aItemNo				
			)	
			
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		


		-- '·é' ¿ø»ó º¹±Í.
		DECLARE @mNo int, 
				@maxNo int, 
				@mSerialNo bigint,
				@mBeadIndex tinyint,
				@mPcNo int,
				@mBeadSerialNo bigint;

		SELECT @mNo = MIN(mID), @maxNo = MAX(mID) FROM @TempTable
		IF @@ROWCOUNT > 0
			BEGIN
				WHILE @mNo <= @maxNo
					BEGIN
						SELECT @mSerialNo = mSerialNo, @mBeadIndex = mBeadIndex, @mPcNo = mPcNo 
							FROM @TempTable	WHERE mID = @mNo
						EXEC dbo.UspReturnBead @mSerialNo, @mBeadIndex, @mPcNo, @mBeadSerialNo OUTPUT
						SET @mNo = @mNo + 1
					END
			END
		-- '·é' ¿ø»ó º¹±Í ¿Ï·á.








		
		DELETE dbo.TblPcStore
		WHERE mEndDate <= @aItemExpireDate
			OR mCnt < 1
			OR mItemNo In (				
				SELECT mItemNO
				FROM @aItemNo							
			)					
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		
		
		DELETE dbo.TblGuildStore
		WHERE mEndDate <= @aItemExpireDate
			OR mCnt < 1
			OR mItemNo In (				
				SELECT mItemNO
				FROM @aItemNo					
			)		
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		
						
		-- ·Î±×¾Æ¿ô ½Ã°£ Àç ¼³Á¤ 		
		UPDATE dbo.TblPcState 
		SET 
			mLogoutTm = GETDATE() 
		WHERE mLogoutTm < mLoginTm
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		

		-- Á¸ÀçÇÏÁö ¾Ê´Â ±æµåÇÏ¿ì½º  ÃÊ´ëÀåÁ¤º¸ Á¦°Å
		DELETE dbo.TblGuildAgitTicket 
		WHERE NOT EXISTS(	SELECT mSerialNo 
							FROM dbo.TblPcInventory 
							WHERE mSerialNo = mTicketSerialNo)

		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		

		-- Á¸ÀçÇÏÁö ¾Ê´Â È¦  ÃÊ´ëÀåÁ¤º¸ Á¦°Å
		DELETE dbo.TblBanquetHallTicket 
		WHERE NOT EXISTS(	SELECT mSerialNo 
							FROM dbo.TblPcInventory 
							WHERE mSerialNo = mTicketSerialNo	)
							
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END		
						
		-- Á¸ÀçÇÏÁö ¾Ê´Â ±æµå°¡ ÀÔÂûÇÑ ±æµåÇÏ¿ì½º °æ¸ÅÁ¤º¸ ÃÊ±âÈ­
		UPDATE dbo.TblGuildAgitAuction
		SET		
			mCurBidGID = 0, mCurBidMoney = 0, mRegDate = getdate()
		WHERE mCurBidGID <> 0 
				AND (NOT EXISTS(
								SELECT mGuildNo 
								FROM dbo.TblGuild 
								WHERE mGuildNo = mCurBidGID))
								
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END										

		-- Á¸ÀçÇÏÁö ¾Ê´Â ±æµå°¡ °¡Áö°í ÀÖ´Â ±æµåÇÏ¿ì½º Á¤º¸ ÃÊ±âÈ­
		UPDATE dbo.TblGuildAgit
		SET 
			mOwnerGID = 0, 
			mGuildAgitName = '', 
			mRegDate = getdate(), 
			mLeftMin = 4320, 
			mIsSelling = 0, 
			mSellingMoney = 100000, 
			mBuyingMoney = 0
		WHERE mOwnerGID <> 0 
				AND (NOT EXISTS(
								SELECT mGuildNo 
								FROM dbo.TblGuild 
								WHERE mGuildNo = mOwnerGID))

		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END			

		-- ±âÇÑÀÌ Áö³­ Ãß°¡¼Ó¼º ¾ÆÀÌÅÛ ¹øÈ£ ÃÊ±âÈ­
		UPDATE dbo.TblPcInventory
		SET mApplyAbnItemNo = 0
		WHERE mApplyAbnItemEndDate < GETDATE()
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END					
		
		-- »èÁ¦µÈ ÆíÁöÁö Á¤º¸ »èÁ¦ 
		DELETE dbo.TblLetter 
		WHERE mSerialNo NOT IN (
			SELECT mSerialNo
			FROM dbo.TblPcInventory
			WHERE mItemNo IN (721,722,724,725)
			UNION ALL
			SELECT mSerialNo
			FROM dbo.TblPcStore
			WHERE mItemNo IN (721,722,724,725)
		)
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END					
		
		-- ¸¸·áµÈ À¯·á ¾ÆÀÌÅÛ Á¤º¸ »èÁ¦
		DELETE dbo.TblPcGoldItemEffect 		
		WHERE mEndDate <= @aItemExpireDate
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END					
		
		DELETE dbo.TblGuildGoldItemEffect		
		WHERE mEndDate <= @aItemExpireDate
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END				
		
		-- JJH
		-- ¹æ¿­¼è ¹× »óÀÚ¿­¼è ¾ÆÀÌÅÛ »èÁ¦
		DELETE dbo.TblPcInventory
		WHERE mItemNo IN (
			SELECT mIID
			FROM dbo.TblLimitedResourceItem
			WHERE mResourceType IN (998,999)
		)
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END	

		-- ¹æ¿­¼è ¹× »óÀÚ¿­¼è ¸®¼Ò½º º¹¿ø	(ÇÏµåÄÚµù -_-) (1°³¾¿¸¸ ÀÖ¾î¾ßÇÑ´Ù - ±âÈ¹ÀÇµµ)
		UPDATE dbo.TblLimitedResource
		SET mRemainerCnt = 1
		WHERE mResourceType in (998,999)

		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END
		
		-- ÀèÆÌ È®ÀÎÁõ ¾ÆÀÌÅÛ »èÁ¦
		DELETE dbo.TblPcInventory
		WHERE mItemNo IN (7590, 7591, 7592, 7593, 7594, 7595, 7650, 7651, 7652, 7653, 7654, 7655)
		
		SELECT @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 1	-- sql error
			ROLLBACK TRAN
			GOTO TRN_END
		END					
												
	COMMIT TRAN		
	
TRN_END:

	IF @pIsItemRecovery = 1
	BEGIN
				
		IF @aStep = 1 
			AND @aErr <> 0	
			AND @aLastSupportMinute >= @pItemRecoveryTgTm		
		BEGIN	
				
			SET @aLastSupportMinute= -@aLastSupportMinute
			EXEC @aErr = dbo.UspUptItemDate @aLastSupportMinute		-- auto item recovery rollback
			
			IF @aErr <> 0			 
				SET @aStep = 2	-- auto item recovery rollback failed 	 
								
		END
		
		-- auto item recovery complete : support table = 2020-01-01 00:00:00 
		IF @aErr = 0 
			AND @aLastSupportMinute >= @pItemRecoveryTgTm
			AND @aStep IN ( 0, 1)
		BEGIN
			EXEC FNLParm.dbo.UspSetParmSvrLastSupportDate @pSvrNo
		END
		
	END 

	SELECT @aLastSupportMinute mItemSupportMinute, @aErr mError, @aStep mStep

GO

