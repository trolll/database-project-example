/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAccountInfo    Script Date: 2008-11-10 10:37:16 ******/





CREATE Procedure [dbo].[UspGetGuildAccountInfo]		-- ????? ? ??
	 @pGID			INT			-- ????
	,@pGuildMoney		BIGINT OUTPUT	-- ???? [??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF EXISTS(SELECT mGID FROM TblGuildAccount WITH(NOLOCK) WHERE mGID = @pGID)
	BEGIN
		-- ????? ???

		SELECT @pGuildMoney = mGuildMoney FROM TblGuildAccount WITH(NOLOCK) WHERE mGID = @pGID
		GOTO LABEL_END_LAST
	END
	ELSE
	BEGIN
		-- ????? ???? ??

		BEGIN TRAN

		INSERT TblGuildAccount (mGID, mGuildMoney) VALUES (@pGID, 0)
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB ???? ??
			GOTO LABEL_END			 
		END

		SET @pGuildMoney = 0
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

