/******************************************************************************
**		Name: dbo.UspGetPcRestExpAndLogoutSeconds
**		Desc: TblPcState의 휴식 경험치와 로그아웃 시간 로드
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2010.12.02	공석규				TblPcState의 mRestExp > mRestExpGuild 변경
**										mRestExpActivate, mRestExpDeactivate 추가로
**										@pRestExp를 @pRestExpGuild로 수정
**										@pRestExpActivate, @pRestExpDeactivate 추가
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcRestExpAndLogoutSeconds]
	@pPcNo				INT,
	@pRestExpGuild		BIGINT OUTPUT,
	@pRestExpActivate	BIGINT OUTPUT,
	@pRestExpDeactivate	BIGINT OUTPUT,
	@pLogoutSeconds		INT OUTPUT
AS  
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT @pRestExpGuild = 0, @pRestExpActivate = 0, @pRestExpDeactivate = 0, @pLogoutSeconds = 0;
	
	SELECT @pRestExpGuild = mRestExpGuild
			, @pRestExpActivate = mRestExpActivate
			, @pRestExpDeactivate = mRestExpDeactivate
			, @pLogoutSeconds = datediff(second, mLogoutTm, getdate())
	FROM dbo.TblPcState
	WHERE mNo = @pPcNo;

	IF(0 = @@ROWCOUNT)
	BEGIN
		RETURN(1);
	END

	RETURN(0);

GO

