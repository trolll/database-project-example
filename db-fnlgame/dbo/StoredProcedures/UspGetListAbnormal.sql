/****** Object:  Stored Procedure dbo.UspGetListAbnormal    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspGetListAbnormal    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspGetListAbnormal    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspGetListAbnormal    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspGetListAbnormal    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspGetListAbnormal    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspGetListAbnormal    Script Date: 2009-11-16 10:23:28 ******/
/******************************************************************************
**		Name: UspGetListAbnormal
**		Desc: 
**
**		Auth:
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 2009-05-09		Author:	? ??
**		Description: SELECT ?? ??. (mRestoreCnt)
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspGetListAbnormal]
	 @pPcNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- time()? ????? ????? '1970-1-1 0:0:0'??.
	SELECT 
		  [mParmNo]
		, [mLeftTime]
		, [mAbParmNo]
		, [mRestoreCnt]
	FROM dbo.TblPcAbnormal
	WHERE mPcNo=@pPcNo;

GO

