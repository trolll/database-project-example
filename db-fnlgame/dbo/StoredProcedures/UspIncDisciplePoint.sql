/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspIncDisciplePoint    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspIncDisciplePoint]
	 @pMaster		INT
	,@pIncPoint		INT
	,@pMaxPoint		INT
	,@pCurPoint		INT OUTPUT	-- ?? ?????
	,@pMaxCurPoint	INT OUTPUT	-- ????? ?? ?????
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT
	SET @aRv = 0
	SET @pCurPoint = 0
	SET @pMaxCurPoint = 0

	SELECT 
		TOP 1 
		@pCurPoint = mCurPoint, 
		@pMaxCurPoint = mMaxCurPoint 
	FROM dbo.TblDisciple 
	WHERE mMaster = @pMaster
	
	IF @@ROWCOUNT <= 0
	BEGIN
		RETURN(2)	-- ???? ?? ??
	END

	-- ?? ????? + ?? ???? ??? ??????? ?? ??
	IF (@pCurPoint + @pIncPoint > @pMaxPoint)
	BEGIN
		SET @pCurPoint = @pMaxPoint
	END
	ELSE
	BEGIN
		SET @pCurPoint = @pCurPoint + @pIncPoint
	END

	-- ????? ?? ?????? ??? ?? ??????? ??? ??
	IF (@pCurPoint > @pMaxCurPoint)
	BEGIN
		SET @pMaxCurPoint = @pCurPoint
	END

	UPDATE dbo.TblDisciple 
	SET 
		mCurPoint = @pCurPoint, 
		mMaxCurPoint = @pMaxCurPoint 
	WHERE mMaster = @pMaster
	IF @@ERROR > 0
	BEGIN
		RETURN(1)		
	END
	
	RETURN(0)

GO

