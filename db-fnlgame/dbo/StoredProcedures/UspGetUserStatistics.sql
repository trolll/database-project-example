/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2011-4-19 15:24:33 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2011-3-4 11:36:40 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2010-3-22 15:58:16 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2009-12-14 11:35:24 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2009-7-14 13:13:26 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspGetUserStatistics    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspGetUserStatistics]
	 @pActiveUserCnt	INT		OUTPUT
	,@pActiveMoney		BIGINT	OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	SELECT TOP 1 @pActiveUserCnt=[mActiveUserCnt], @pActiveMoney=[mActiveMoney]
		FROM TblStatisticsUser ORDER BY [mRegDate] DESC
	IF(0 = @@ROWCOUNT)
	 BEGIN
		SET	@pActiveUserCnt	= 0
		SET	@pActiveMoney	= 0
	 END
	SET NOCOUNT OFF

GO

