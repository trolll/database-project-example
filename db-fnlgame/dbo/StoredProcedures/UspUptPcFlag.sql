/******************************************************************************  
**  File: UspUptPcFlag.sql
**  Name: UspUptPcFlag
**  Desc: 캐릭터의 mFlag 정보를 업데이트
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:		Description: 
**  ----------	----------	---------------------------------------  
**	2012-09-14  kongkong7	mFlag Type변경(TINYINT > SMALLINT)
*******************************************************************************/
CREATE PROC [dbo].[UspUptPcFlag]
(
	@pPcNo INT
	, @pFlag SMALLINT
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	UPDATE dbo.TblPcState SET mFlag = @pFlag WHERE mNo = @pPcNo;
	
	IF (@@ROWCOUNT = 0)
	BEGIN
		RETURN(1) -- PC 정보가 존재하지 않음
	END

    IF (@@ERROR <> 0)
	BEGIN
		RETURN(2) -- db error	
	END
            
	RETURN (0)
END

GO

