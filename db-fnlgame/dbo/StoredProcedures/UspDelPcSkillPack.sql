/******************************************************************************  
**  File: 
**  Name: UspDelPcSkillPack  
**  Desc: PC의 스킬팩 하나를 삭제한다.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDelPcSkillPack]
	@pPcNo		INT,
	@pSPID	INT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, 버려진 아이템

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	DELETE FROM dbo.TblPcSkillPackInventory
	WHERE mPcNo = @pPcNo AND mSPID = @pSPID
	IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- SQL Error
		END

	RETURN @aErrNo

GO

