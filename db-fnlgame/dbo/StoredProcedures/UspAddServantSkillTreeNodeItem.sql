/******************************************************************************
**		Name: UspAddServantSkillTreeNodeItem
**		Desc: ¼­¹øÆ® ½ºÅ³ Æ®¸® ³ëµå ¾ÆÀÌÅÛ Ãß°¡
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddServantSkillTreeNodeItem]
	@pSerialNo		BIGINT
	,@pSTID			INT
	,@pSTNIID		INT
	,@pValidSec		INT  -- 864000000ÃÊ(10000ÀÏ)ÀÌ¸é ¹«ÇÑ´ë, 2020-01-01 00:00:00 ¿¡ ¸¸·áµÇ´Â °ÍÀ¸·Î ¾à¼ÓÇÑ´Ù  
	,@pLeftMin		INT OUTPUT 
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	
	DECLARE	 @aEndDay		SMALLDATETIME
			, @aErr			INT
			, @aRowCnt		INT

	SELECT 	@aErr = 0, 	@aRowCnt = 0;	
			
	IF 864000000 = @pValidSec
		BEGIN
			SELECT	 @aEndDay = dbo.UfnGetEndDate(GETDATE(), 864000000/(3600 * 24))
		END
	ELSE
		BEGIN
			SELECT	 @aEndDay = dateadd(second, @pValidSec, GETDATE())
		END
		
	UPDATE dbo.TblPcServantSkillTree
	SET mEndDate=@aEndDay
	WHERE mSerialNo=@pSerialNo
		AND mSTNIID = @pSTNIID		

	SELECT @aErr = @@ERROR	, @aRowCnt = @@ROWCOUNT, @pLeftMin = DATEDIFF(mi, GETDATE(), @aEndDay);
	
	IF @aErr <> 0 
	BEGIN
		RETURN(1) -- DB ERROR
	END

	IF @aRowCnt > 0
		RETURN(0)
	
	insert into dbo.TblPcServantSkillTree
	values(default, @pSerialNo, @pSTID, @pSTNIID, @aEndDay)
	IF @@error <> 0
	BEGIN
		RETURN(2) -- DB ¿¡·¯
	END

	RETURN(0)

	SET NOCOUNT OFF;

GO

