/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.UspSetEquip    Script Date: 2009-5-12 9:18:18 ******/
CREATE PROCEDURE [dbo].[UspSetEquip]
	 @pPcNo			INT			-- 1捞搁 阁胶磐促. 八荤侩烙.
	,@pSerial		BIGINT
	,@pSlot			INT			-- CPcEquip::ePosMax.
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	IF NOT EXISTS(	SELECT * 
					FROM dbo.TblPcInventory WITH(INDEX=PK_NC_TblPcInventory_1)
					WHERE 	mSerialNo = @pSerial
							AND mPcNo = @pPcNo )
	BEGIN
		RETURN(1)
	END

	BEGIN TRAN

		--------------------------------------------------
		-- 捞固 粮犁窍绰 slot捞搁 昏力
		--------------------------------------------------
		DELETE dbo.TblPcEquip 
		WHERE mOwner = @pPcNo
				AND mSlot = @pSlot

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(2)
		END
		
		--------------------------------------------------
		-- 厘馒 酒捞袍 汲沥 
		--------------------------------------------------
		INSERT dbo.TblPcEquip(mOwner, mSlot, mSerialNo) 
		VALUES(
			@pPcNo, 
			@pSlot,
			@pSerial
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RETURN(2)
		END

	COMMIT TRAN
	RETURN(0)

GO

