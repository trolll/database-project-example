/******************************************************************************  
**  Name: UspCnsmListAllValid  
**  Desc: 기간 만료 안된 것들 전부 리스팅
**			서버 초기 load시 실행되는 SP
**                
**                
**  Author: 김강호  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
** 2011 0712    김강호     레코드셋에 ItemSn, HoleCount 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmListAllValid]
AS
BEGIN
	SET NOCOUNT ON;  
	SET XACT_ABORT ON;	 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	declare @aCurDate smalldatetime;
	SET @aCurDate = getdate();

	select a.mCnsmNo, a.mCategoryNo, a.mPrice, 
			CASE WHEN a.mTradeEndDate <= @aCurDate
			THEN 0 
			ELSE DATEDIFF(minute, getdate(), a.mTradeEndDate) END, 
			a.mPcNo, a.mRegCnt, a.mCurCnt, a.mItemNo,
			b.mStatus, b.mCntUse, b.mOwner, b.mPracticalPeriod,
			b.mSerialNo, b.mHoleCount
	from dbo.TblConsignment a 
		inner join dbo.TblConsignmentItem b 
			on a.mCnsmNo = b.mCnsmNo
	where @aCurDate < a.mTradeEndDate;
END

GO

