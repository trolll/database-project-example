/******************************************************************************  
**  Name: WspDeleteItem  
**  Desc: ¾ÆÀÌÅÛ »èÁ¦
**  
**  Auth:   
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:		Author:    Description:  
**  --------	--------   ---------------------------------------  
**  04/15/2013	³²±âºÀ		¾÷Àû½Ã½ºÅÛ Ãß°¡
**  2015.06.22	ÀÌ¿ëÁÖ		¼­¹øÆ® Á¦°Å Ãß°¡ (¿¡·¯ 2¹ø)
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspDeleteItem]
(
	@mSerialNo	BIGINT
	, @mCnt		BIGINT
	, @mPos		CHAR(1)
)
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @ErrNo		INT
			, @RowCnt	INT

	SELECT @ErrNo		= 0
			, @RowCnt	= 0

	IF (@mPos = 'i')
	BEGIN
		IF(	SELECT mCnt 
			FROM dbo.TblPcInventory 
			WHERE mSerialNo = @mSerialNo) - @mCnt < 1
		BEGIN
			BEGIN TRAN

			DELETE FROM dbo.TblPcInventory
			WHERE mSerialNo = @mSerialNo

			SELECT @ErrNo = @@ERROR, @RowCnt = @@ROWCOUNT

			IF (@ErrNo <> 0 OR @RowCnt <= 0)
			BEGIN
				ROLLBACK TRAN
				RETURN 1
			END

			------------------------------------------------------
			-- ¼­¹øÆ®ÀÎ °æ¿ì Á¤º¸ Á¦°Å
			------------------------------------------------------
			EXEC @ErrNo = dbo.UspDeleteServant @mSerialNo
			
			IF @ErrNo <> 0
			BEGIN
				ROLLBACK TRAN
				RETURN 2
			END
			
			COMMIT TRAN
		END
		ELSE
		BEGIN			
			UPDATE dbo.TblPcInventory 
			SET	mCnt = mCnt - @mCnt
			WHERE mSerialNo = @mSerialNo

			SELECT @ErrNo = @@ERROR, @RowCnt = @@ROWCOUNT

			IF (@ErrNo <> 0 OR @RowCnt <= 0)
			BEGIN
				RETURN 1
			END
		END
	END
	
	IF (@mPos = 's')
	BEGIN
		IF(	SELECT mCnt 
			FROM dbo.TblPcStore 
			WHERE mSerialNo = @mSerialNo) - @mCnt < 1
		BEGIN
			DELETE FROM dbo.TblPcStore
			WHERE mSerialNo = @mSerialNo

			SELECT @ErrNo = @@ERROR, @RowCnt = @@ROWCOUNT

			IF (@ErrNo <> 0 OR @RowCnt <= 0)
			BEGIN
				RETURN 1
			END
		END
		ELSE
		BEGIN			
			UPDATE dbo.TblPcStore 
			SET	mCnt = mCnt - @mCnt
			WHERE mSerialNo = @mSerialNo

			SELECT @ErrNo = @@ERROR, @RowCnt = @@ROWCOUNT

			IF (@ErrNo <> 0 OR @RowCnt <= 0)
			BEGIN
				RETURN 1
			END
		END	
	END 
	
	IF (@mPos = 'g')
	BEGIN
		IF(	SELECT mCnt 
			FROM dbo.TblGuildStore 
			WHERE mSerialNo = @mSerialNo) - @mCnt < 1
		BEGIN
			DELETE FROM dbo.TblGuildStore
			WHERE mSerialNo = @mSerialNo

			SELECT @ErrNo = @@ERROR, @RowCnt = @@ROWCOUNT

			IF (@ErrNo <> 0 OR @RowCnt <= 0)
			BEGIN
				RETURN 1
			END
		END
		ELSE
		BEGIN			
			UPDATE dbo.TblGuildStore 
			SET	mCnt = mCnt - @mCnt
			WHERE mSerialNo = @mSerialNo

			SELECT @ErrNo = @@ERROR, @RowCnt = @@ROWCOUNT

			IF (@ErrNo <> 0 OR @RowCnt <= 0)
			BEGIN
				RETURN 1
			END
		END	
	END 	
	
	IF (@mPos = 'a')
	BEGIN
		DELETE FROM dbo.TblPcAchieveInventory
		WHERE mSerialNo = @mSerialNo

		SELECT @ErrNo = @@ERROR, @RowCnt = @@ROWCOUNT

		IF (@ErrNo <> 0 OR @RowCnt <= 0)
		BEGIN
			RETURN 1
		END
	END 	

	RETURN(0)
END

GO

