/****** Object:  Stored Procedure dbo.UspSelectGuildRecruit    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSelectGuildRecruit    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSelectGuildRecruit    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSelectGuildRecruit    Script Date: 2010-12-23 17:46:00 ******/
CREATE PROCEDURE [dbo].[UspSelectGuildRecruit]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		TOP 100 
			T1.mRegDate,
			T1.mEndDate,	
			T1.mGuildNo,
			RTRIM(T2.mGuildNm),
			T1.mIsPremium,
			T1.mIsMarkShow,
			T1.mGuildMsg
	FROM
		dbo.TblGuildRecruit T1
			INNER JOIN dbo.TblGuild T2
				ON T1.mGuildNo = T2.mGuildNo			
	WHERE T1.mEndDate >= GETDATE()		
	ORDER BY T1.mIsPremium DESC, T1.mRegDate ASC;

GO

