/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitTicketInfo    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspGetGuildAgitTicketInfo]
	 @pTicketSerialNo	BIGINT				-- ???????
	,@pTerritory		INT OUTPUT			-- ???? [??]
	,@pGuildAgitNo		INT OUTPUT			-- ??????? [??]
	,@pOwnerGID		INT OUTPUT			-- ???? [??]
	,@pOwnerGuildName	NVARCHAR(12) OUTPUT	-- ?????? [??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	SET		@pTerritory = -1
	SET		@pGuildAgitNo = -1
	SET		@pOwnerGuildName = ''
	SET 	@pOwnerGID = 0

	SELECT @pTerritory = mTerritory
		,@pGuildAgitNo = mGuildAgitNo
		,@pOwnerGID = mOwnerGID
		,@pOwnerGuildName = ISNULL((CASE mOwnerGID
						WHEN 0 THEN N'<NONE>'
						ELSE (SELECT mGuildNm FROM dbo.TblGuild WHERE mGuildNo = mOwnerGID)
						END), N'<FAIL>')
	FROM dbo.TblGuildAgitTicket 
	WHERE mTicketSerialNo = @pTicketSerialNo
	
	IF @@ROWCOUNT = 0 
	BEGIN
		RETURN(2)
	END

	RETURN(0)

GO

