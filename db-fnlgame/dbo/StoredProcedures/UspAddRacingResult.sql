/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspAddRacingResult    Script Date: 2008-11-10 10:37:16 ******/




--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

CREATE Procedure [dbo].[UspAddRacingResult]
	 @pPlace		INT
	,@pStage		INT
	,@pWinnerNID		SMALLINT
	,@pDividend		FLOAT
	,@pNo			INT OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	IF(0 <> @@TRANCOUNT) ROLLBACK
	BEGIN TRAN

	DECLARE	@aWinnerNID		INT

	-- P.S.> mWinnerNID ? 0?? ??? DRAW, ??? FNLParm.DT_Racing ?? mNID? ?? 0? ???? ??
	IF EXISTS(SELECT mWinnerNID FROM TblRacingResult WHERE mPlace = @pPlace AND mStage = @pStage)
	BEGIN
		-- ?? ??? ??? ??? ?????? ??
		SELECT @aWinnerNID = mWinnerNID FROM TblRacingResult WHERE mPlace = @pPlace AND mStage = @pStage
		IF (@aWinnerNID = 0)
		BEGIN
			UPDATE TblRacingResult
			SET mWinnerNID = @pWinnerNID, mDividend = @pDividend
			WHERE mPlace = @pPlace AND mStage = @pStage
		END
	END
	ELSE
	BEGIN
		INSERT TblRacingResult([mPlace], [mStage], [mWinnerNID], [mDividend])
		VALUES(@pPlace, @pStage, @pWinnerNID, @pDividend)
	END
	
	SET @pNo = @@IDENTITY
	IF(0 <> @@ERROR)
	 BEGIN
		SET	@aErrNo	= 1
		GOTO LABEL_END		 
	 END
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
			
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

