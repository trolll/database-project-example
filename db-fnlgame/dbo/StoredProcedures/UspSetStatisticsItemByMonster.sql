/******************************************************************************  
**  Name: UspSetStatisticsItemByMonster  
**  Desc: 몬스터, NPC 통계 데이터 저장
**  
**  Auth:   
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  김강호      mItemStatus,  mModType 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetStatisticsItemByMonster]  
   @pMID   INT  
 , @pItemNo  INT  
 , @pCreate  BIGINT  
 , @pDelete  BIGINT  
 , @pItemStatus SMALLINT
 , @pModType SMALLINT
AS  
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
   
	DECLARE @aErrNo INT  ;
	SET @aErrNo = 0  ;

	INSERT INTO dbo.TblStatisticsItemByMonster
		([MID], [mItemNo], [mCreate], [mDelete]
		 , mItemStatus, mModType)   
	VALUES 
		(@pMID, @pItemNo, @pCreate, @pDelete
		 , @pItemStatus, @pModType)  ;
    
	IF(@@ERROR <> 0) 
	BEGIN 
		SET @aErrNo = 2; -- DB Error  
	END
   
	RETURN(@aErrNo)  ;

GO

