/******************************************************************************
**		Name: WspGetPetitionEx
**		Desc: 진정서 내용을 가져온다.
**
**		Auth: JUDY
**		Date: 2010.04.14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      20100414	JUDY			계정 정보를 반환 한다.         
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetPetitionEx]
	@mFlag		INT,	-- 플래그값 (0이면 미처리 목록, 1 이면 모든목록)
	@in_begin_date	CHAR(8),
	@in_end_date	CHAR(8)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF @mFlag = 0
	BEGIN
	
		SELECT 
			TOP 1000
			  pe.mRegDate
			, pe.mText
			, pe.mIsFin
			, p.mNm
			, ca.mContent
			, pe.mPID
			, pe.mIpAddress
			, de.mPcNm
			, tu.mUserID
		FROM [dbo].[TblPetitionBoard]AS pe
			INNER JOIN [dbo].[TblPc] AS p
				ON pe.mPcNo = p.mNo
			LEFT OUTER JOIN [dbo].[TblPcDeleted] AS de
				ON p.mNo = de.mPcNo 			
			INNER JOIN FNLParm.[dbo].[TblPetitionCategory] AS ca
				ON pe.mCategory = ca.mCategory
			INNER JOIN FNLAccount.dbo.TblUser AS tu
				ON p.mOwner = tu.mUserNo
		WHERE pe.mRegDate >= CONVERT(DATETIME, @in_begin_date )
				AND pe.mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_End_Date ) )
				AND pe.mIsFin = 0
		ORDER BY pe.mRegDate DESC
		
		RETURN;
	END

	SELECT 
		TOP 1000
		  pe.mRegDate
		, pe.mText
		, pe.mIsFin
		, p.mNm
		, ca.mContent
		, pe.mPID
		, pe.mIpAddress
		, de.mPcNm
		, tu.mUserID
	FROM [dbo].[TblPetitionBoard]AS pe
		INNER JOIN [dbo].[TblPc] AS p
			ON pe.mPcNo = p.mNo
		LEFT OUTER JOIN [dbo].[TblPcDeleted] AS de
			ON p.mNo = de.mPcNo 			
		INNER JOIN FNLParm.[dbo].[TblPetitionCategory] AS ca
			ON pe.mCategory = ca.mCategory
		INNER JOIN FNLAccount.dbo.TblUser AS tu
			ON p.mOwner = tu.mUserNo
	WHERE pe.mRegDate >= CONVERT(DATETIME, @in_begin_date )
			AND pe.mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_End_Date ) )
	ORDER BY pe.mRegDate DESC

GO

