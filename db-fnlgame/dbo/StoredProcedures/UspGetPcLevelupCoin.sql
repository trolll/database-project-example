/******************************************************************************
**		Name: UspGetPcLevelupCoin
**		Desc: ·¹º§ ¾÷ ÁÖÈ­ »ç¿ëÀÚ Á¤º¸ È¹µæ
**		Test:			
**		Auth: ³²¼º¸ð
**		Date: 2014/02/18
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcLevelupCoin]
	@pPcno  INT 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		mExp
		, mLastReceiptSection
	FROM dbo.TblPcLevelupCoin
	WHERE mPcno = @pPcno

GO

