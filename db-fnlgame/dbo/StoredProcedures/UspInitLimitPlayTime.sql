/******************************************************************************
**		Name: UspInitLimitPlayTime
**		Desc: Æ¯È­¼­¹ö°ü·Ã ÀÏ¹Ý Á¦ÇÑ ½Ã°£, PC¹æ Á¦ÇÑ ½Ã°£ ÃÊ±âÈ­
			  ÀÏ¹Ý ÇÃ·¹ÀÌ Á¦ÇÑ½Ã°£ 45½Ã, PC¹æ ÇÃ·¹ÀÌ Á¦ÇÑ½Ã°£ 15½Ã(ÃßÈÄ¿¡ º¯°æµÉ¼ö ÀÖÀ½)
**
**		Auth: °ø ¼® ±Ô
**		Date: 2014.11.03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2015.01.26	Á¤ÁøÈ£				AccountDB¿¡¼­ GameDB·Î º¯°æ, ÄÁÅÙÃ÷ ¿É¼Ç¿¡¼­ Á¦ÇÑ½Ã°£À» ¹Þ¾Æ¿È
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[UspInitLimitPlayTime]
     @pNormalLimitPlayTimeMax   	INT
    ,@pPcBangLimitPlayTimeMax   	INT
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	-- TblParmSvrOpÅ×ÀÌºí mOpNo = 113
	-- @pNormalLimitPlayTimeMax °ªÀº  mOpValue4 * 60 * 60 ÀÌ´Ù. ½Ã°£À» ÃÊ·Î º¯°æÇØ¼­ ³Ö¾îÁØ´Ù. (45 * 60 * 60)
	-- @@pPcBangLimitPlayTimeMax °ªÀº mOpValue5 * 60 * 60 ÀÌ´Ù. ½Ã°£À» ÃÊ·Î º¯°æÇØ¼­ ³Ö¾îÁØ´Ù. (15 * 60 * 60)
	
	UPDATE dbo.TblLimitPlayTime 
	SET mNormalLimitTime = @pNormalLimitPlayTimeMax, mPcBangLimitTime = @pPcBangLimitPlayTimeMax
END

GO

