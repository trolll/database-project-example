/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspListGuildAgitTicket    Script Date: 2008-11-10 10:37:17 ******/





CREATE  PROCEDURE [dbo].[UspListGuildAgitTicket]
	 @pPcNo		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	SELECT	 a.mTicketSerialNo
			,a.mTerritory 
			,a.mGuildAgitNo
			,a.mFromPcNm
			,a.mToPcNm
	FROM dbo.TblGuildAgitTicket  AS a 
		INNER JOIN dbo.TblPcInventory    AS b 
		ON a.mTicketSerialNo = b.mSerialNo
	WHERE b.mPcNo=@pPcNo
			AND b.mEndDate > GETDATE() 
		
	SET NOCOUNT OFF

GO

