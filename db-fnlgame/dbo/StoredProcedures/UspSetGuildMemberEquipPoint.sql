/******************************************************************************
**		Name: UspSetGuildMemberEquipPoint
**		Desc: 
**
**		Auth:
**		Date:
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	2013-06-20	巢扁豪				荐沥
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetGuildMemberEquipPoint]
	  @pGuildNo	INT
	, @pPcNo	INT
	, @pEquipPoint	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE	 @aRowCnt		INT
	DECLARE  @aErr			INT

	SELECT  @aRowCnt = 0, @aErr = 0
	
	UPDATE dbo.TblGuildMember 
	SET mEquipPoint = @pEquipPoint 
	WHERE mGuildNo = @pGuildNo 
	AND mPcNo = @pPcNo
		
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT	

	IF (@aErr <> 0)
		RETURN(2)	-- db error 
		
	IF (@aRowCnt <= 0)	
		RETURN(1)	-- none row 
	
	RETURN(0)

GO

