/******************************************************************************
**		Name: WspGetPcRegionQuest
**		Desc: PCÀÇ Áö¿ª Äù½ºÆ® Á¤º¸¸¦ °¡Á®¿Â´Ù.
**
**		Auth: Á¤Áø¿í
**		Date: 2014-09-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:		Description:
**		--------	--------	---------------------------------------
**		2014-09-19	Á¤Áø¿í		»ý¼º
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetPcRegionQuest]
	@pPcNo	INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		T1.mQuestNo
		, T3.mQuestNm
		, T4.MName
		, T2.mCnt
		, T2.mMaxCnt
		, T1.mBeginDate
	FROM dbo.TblPcQuest T1
		INNER JOIN dbo.TblPcQuestCondition as T2
			ON T1.mQuestNo = T2.mQuestNo
		INNER JOIN FNLParm.dbo.TblRegionQuest as T3
			ON T1.mQuestNo = T3.mQuestNo
		INNER JOIN FNLParm.dbo.DT_Monster as T4
			ON T2.mMonsterNo = T4.MID
	WHERE T1.mPcNo = @pPcNo

GO

