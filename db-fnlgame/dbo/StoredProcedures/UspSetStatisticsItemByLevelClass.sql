/******************************************************************************  
**  Name: UspGetStatisticsItemByLevelClass  
**  Desc: 아이템 통계 데이터 저장
**  
**  Auth:   
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  김강호      mItemStatus,  mCnsmRegFee, mCnsmBuyFee 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetStatisticsItemByLevelClass]       
   @pItemNo  INT    
 , @pMerchantCreate  BIGINT    
 , @pMerchantDelete  BIGINT    
 , @pReinforceCreate BIGINT    
 , @pReinforceDelete  BIGINT    
 , @pCraftingCreate  BIGINT    
 , @pCraftingDelete  BIGINT    
 , @pPcUseDelete  BIGINT    
 , @pNpcUseDelete  BIGINT    
 , @pNpcCreate  BIGINT    
 , @pMonsterDrop  BIGINT    
 , @pGSExchangeCreate BIGINT    
 , @pGSExchangeDelete BIGINT    
 , @pLevel   SMALLINT    
 , @pClass   SMALLINT  
 , @pItemStatus  SMALLINT  
 , @pCnsmRegFee  BIGINT    
 , @pCnsmBuyFee  BIGINT    
AS    
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
    
	DECLARE  @aErrNo  INT;    
	SET @aErrNo = 0;    
     
	INSERT INTO dbo.TblStatisticsItemByLevelClass     
		([mItemNo], [mMerchantCreate], [mMerchantDelete], [mReinforceCreate], [mReinforceDelete], [mCraftingCreate], [mCraftingDelete],     
		[mPcUseDelete], [mNpcUseDelete], [mNpcCreate], [mMonsterDrop], [mGSExchangeCreate], [mGSExchangeDelete],     
		[mLevel], [mClass]  
		, mItemStatus, mCnsmRegFee, mCnsmBuyFee)    
	VALUES    
		(@pItemNo, @pMerchantCreate, @pMerchantDelete, @pReinforceCreate, @pReinforceDelete, @pCraftingCreate,      
		@pCraftingDelete, @pPcUseDelete, @pNpcUseDelete, @pNpcCreate, @pMonsterDrop, @pGSExchangeCreate, @pGSExchangeDelete,    
		@pLevel, @pClass    
		,@pItemStatus, @pCnsmRegFee, @pCnsmBuyFee);    
		
	IF @@ERROR <> 0    
	BEGIN
		SET @aErrNo = 2;    
	END
		              
	RETURN(@aErrNo);

GO

