/******************************************************************************
**		Name: UspSendEtelrGuildAgitTicket
**		Desc: ¿¤Å×¸£ ¾ÆÁöÆ® ÃÊ´ëÀå(°³ÀÎ)
**
**		Auth: °ø¼®±Ô
**		Date: 2014.09.22
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 		Author:				Description: 
**		--------	--------			---------------------------------------
**		
*******************************************************************************/ 	
CREATE PROCEDURE [dbo].[UspSendEtelrGuildAgitTicket]
	 @pTicketSerialNo	BIGINT		-- Æ¼ÄÏ½Ã¸®¾ó¹øÈ£
	,@pTerritory		INT			-- ¿µÁö¹øÈ£
	,@pGuildAgitNo		INT			-- ±æµå¾ÆÁöÆ®¹øÈ£
	,@pOwnerGID			INT			-- ¼ÒÀ¯±æµå
	,@pFromPcNm			VARCHAR(12)	-- º¸³»´Â »ç¶÷ ÀÌ¸§
	,@pToPcNm			VARCHAR(12)	-- ¹Þ´Â »ó´ë¹æ ÀÌ¸§
    ,@pSendTicket   	INT			-- º¸³»´Â ¾ÆÀÌÅÛ ÀÌ¸§
    ,@pRecvTicket   	INT			-- ¹Þ´Â ¾ÆÀÌÅÛ ÀÌ¸§
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aRowCnt	 		INT,
			@aErrNo				INT,
			@aToPcNo			INT,
			@aLetterLimit		BIT,
			@aToGuildNo			INT,
			@aFromGuildNo		INT,
			@aFromPcNo			INT,
			@aFromGuildAssNo	INT,
			@aToGuildAssNo		INT
			
	SELECT @aErrNo = 0,
			@aRowCnt = 0, 
			@aRowCnt = 0,
			@aToPcNo = 0,
			@aLetterLimit = 0,
			@aToGuildNo = 0,
			@aFromGuildNo = 0,
			@aFromPcNo = 0,
			@aFromGuildAssNo = 0,
			@aToGuildAssNo = 0

	DECLARE	@pReceivedGuildAgitTicketID	INT
	DECLARE	@pUnusedGuildAgitTicketItemID	INT

	SET 		@pUnusedGuildAgitTicketItemID	= @pSendTicket		-- ±æµå¾ÆÁöÆ® ÃÊÃ»Àå
    SET 		@pReceivedGuildAgitTicketID 	= @pRecvTicket		-- ¹ÞÀº ±æµå¾ÆÁöÆ® ÃÊÃ»Àå
	-- ¹Þ´Â »ç¶÷ Ä³¸¯ÅÍ È®ÀÎ
	SELECT 
		@aToPcNo = T1.mNo
		, @aLetterLimit = T2.mIsLetterLimit
		, @aToGuildNo = ISNULL(T3.mGuildNo, 0)
		, @aToGuildAssNo = ISNULL(T4.mGuildAssNo, 0)
	FROM dbo.TblPc T1
		INNER JOIN dbo.TblPcState T2 
				ON T1.mNo = T2.mNo	
		LEFT OUTER JOIN dbo.TblGuildMember T3
			ON T1.mNo = T3.mPcNo	
		LEFT OUTER JOIN dbo.TblGuildAssMem T4
			ON T3.mGuildNo = T4.mGuildNo
	WHERE T1.mNm = @pToPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF (@aErrNo <> 0 OR @aRowCnt <= 0)
	BEGIN
		SET @aErrNo = 3			-- 3 : ¹Þ´Â »ó´ë¹æÀÌ Á¸ÀçÇÏÁö ¾ÊÀ½
		GOTO T_END
	END

	-- º¸³»´Â »ç¶÷ Ä³¸¯ÅÍÁ¤º¸ È­±ä
	SELECT
		@aFromPcNo = mNo
		, @aFromGuildNo = ISNULL(T2.mGuildNo, 0)
		, @aFromGuildAssNo = ISNULL(mGuildAssNo,0)
	FROM dbo.TblPc T1
		LEFT OUTER JOIN dbo.TblGuildMember T2
			ON T1.mNo = T2.mPcNo
		LEFT OUTER JOIN dbo.TblGuildAssMem T3
			ON T2.mGuildNo = T3.mGuildNo
	WHERE mNm = @pFromPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 3			-- 3 : ¹Þ´Â »ó´ë¹æÀÌ Á¸ÀçÇÏÁö ¾ÊÀ½
		GOTO T_END
	END
	
	IF ( (@aFromGuildNo <> @aToGuildNo) AND ( (@aFromGuildAssNo <> @aToGuildAssNo) OR (@aToGuildAssNo = 0) OR (@aFromGuildAssNo = 0) ) )
		OR (@aToGuildNo = 0) 
		OR (@aFromGuildNo = 0)   
	BEGIN    
		SET @aErrNo = 8   -- 8 : °°Àº ±æµå ¹× ¿¬ÇÕ¿¡°Ô¸¸ º¸³¾¼ö ÀÖ½À´Ï´Ù.    
		GOTO T_END    
	END

	-------------------------------------------------
	-- Æ¼ÄÏ Á¤º¸ Á¸Àç¿©ºÎ Ã¼Å© 
	-------------------------------------------------
	IF EXISTS(	SELECT mTicketSerialNo 
				FROM dbo.TblGuildAgitTicket 
				WHERE mTicketSerialNo = @pTicketSerialNo)
	BEGIN
		SET @aErrNo = 2			-- 2 : Æ¼ÄÏÀÌ ÀÌ¹Ì Á¸ÀçÇÔ
		GOTO T_END			 
	END

	BEGIN TRAN

		-------------------------------------------------
		-- ±æµå¾ÆÁöÆ® ÀÔÀåÆ¼ÄÏ Ãß°¡
		-------------------------------------------------
		INSERT dbo.TblGuildAgitTicket (mTicketSerialNo, mTerritory, mGuildAgitNo, mOwnerGID, mFromPcNm, mToPcNm) 
		VALUES(@pTicketSerialNo, @pTerritory, @pGuildAgitNo, @pOwnerGID, @pFromPcNm, @pToPcNm)
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1		-- 1 : DB ³»ºÎÀûÀÎ ¿À·ù
			GOTO T_ERR			 
		END	 
	
		-------------------------------------------------
		-- ±æµå¾ÆÁöÆ® ÃÊÃ»Àå ¾ÆÀÌÅÛ ¼öÁ¤, ¼ÒÀ¯±Ç ÀÌÀü		
		-------------------------------------------------
		UPDATE dbo.TblPcInventory 		
		SET 
			mItemNo=@pReceivedGuildAgitTicketID, 
			mIsConfirm=0, 
			mPcNo=@aToPcNo, 
			mEndDate=dbo.UfnGetEndDate(GETDATE(), 7)	-- À¯È¿±âÇÑ 7ÀÏ·Î °íÁ¤
		WHERE mSerialNo = @pTicketSerialNo
				AND mItemNo = @pUnusedGuildAgitTicketItemID

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 4		-- 4 : Æ¼ÄÏÀÌ Á¸ÀçÇÏÁö ¾ÊÀ½
		END		 

T_ERR:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
T_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

