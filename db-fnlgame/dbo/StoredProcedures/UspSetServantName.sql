/******************************************************************************
**		Name: UspSetServantName
**		Desc: ¼­¹øÆ® ÀÌ¸§ ¼³Á¤
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetServantName]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pName			VARCHAR(6)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE dbo.TblPcServant 
	SET mName = @pName
	WHERE mPcNo = @pPcNo
	  AND mSerialNo = @pSerialNo

	SET NOCOUNT OFF;

GO

