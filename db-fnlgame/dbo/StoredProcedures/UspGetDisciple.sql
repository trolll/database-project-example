/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspGetDisciple    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspGetDisciple]
	 @pMaster		INT
	,@pMasterNm		VARCHAR(12) OUTPUT
	,@pMasterLevel		INT OUTPUT
	,@pCurPoint		INT OUTPUT
	,@pMaxCurPoint	INT OUTPUT
	,@pRegDate		DATETIME OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SET @pMasterNm = ''
	SET @pMasterLevel = 0
	SET @pCurPoint = 0
	SET @pMaxCurPoint = 0
	SET @pRegDate='1900-01-01 00:00:00'

	SELECT @pMasterNm = ISNULL(b.[mNm], '<NONE>'), 
		@pMasterLevel = ISNULL(c.[mLevel], 0), 
		@pCurPoint = a.[mCurPoint], 
		@pMaxCurPoint = a.[mMaxCurPoint], 
		@pRegDate = a.[mRegDate]
	FROM TblDisciple As a 
		JOIN TblPc As b 
			ON a.[mMaster] = b.[mNo]
		JOIN TblPcState As c 
			ON a.[mMaster] = c.[mNo]
	WHERE a.[mMaster] = @pMaster

GO

