/******************************************************************************
**		Name: [UspSetPcItemBindType]
**		Desc: 鞎勳澊韰滌潣 攴€靻?韮€鞛呾潉 氤€瓴巾暅雼?
**		Auth: 旯€臧曧樃
**		Date: 2010.01.28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 	Author:	
**		Description: 
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcItemBindType]
   @pSerial  BIGINT  -- 雽€靸?鞎勳澊韰滌潣 鞁滊Μ鞏茧矆順? 
 , @pBindType TINYINT  -- 氚旍澑霌?韮€鞛?
AS
	SET NOCOUNT ON  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   

	DECLARE @aErrNo  INT  
	,@aRowCnt INT  

	SELECT  @aErrNo = 0, @aRowCnt = 0  

	UPDATE dbo.TblPcInventory  
	SET mBindingType = @pBindType  
	WHERE mSerialNo = @pSerial   

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  
	IF @aErrNo <> 0 OR  @aRowCnt = 0  
	BEGIN  
		RETURN (1) -- db error  
	END    
   
RETURN(0)

GO

