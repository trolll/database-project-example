/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspResetCharOutside    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspResetCharOutside]
	@aPcNo	INT,        -- ?? ??? ??
	@pUserNo INT,		-- ?? ??  
    @pSex   TINYINT,	-- ??? ??     
	@pHead	TINYINT,	-- ??
    @pFace  TINYINT,    -- ??   
    @pSerialNo	BIGINT,	-- ??? ??? ??
    @pItemNo INT		-- ??? ?? 
AS
	SET NOCOUNT ON			

	DECLARE	@aErrNo		INT
			,@aRowCnt	INT
			
	SELECT  @aErrNo = 0, @aRowCnt = 0
	
	-----------------------------------------------
	-- ?? ?? ?? 
	-----------------------------------------------		
	BEGIN TRAN
	
		UPDATE dbo.TblPc
		SET
			mSex = @pSex,
			mHead = @pHead,
			mFace = @pFace
		WHERE mOwner = @pUserNo		
				AND mNo = @aPcNo
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END		
	
		DELETE dbo.TblPcInventory
		WHERE mPcNo = @aPcNo 
			AND	mSerialNo = @pSerialNo
			AND mItemNo = @pItemNo
				
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END					

	COMMIT TRAN	
	RETURN(0)

GO

