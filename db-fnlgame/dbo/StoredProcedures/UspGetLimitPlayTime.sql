/******************************************************************************
**		Name: UspGetLimitPlayTime
**		Desc: ÇÃ·¹ÀÌ Á¦ÇÑ½Ã°£À» °¡Á®¿Â´Ù. ½Å±ÔÀ¯Àú¸é »õ·Î µî·ÏÇÑ´Ù.
**			  
**		Auth: Á¤ÁøÈ£
**		Date: 2015.01.27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetLimitPlayTime]
	  @pUserNo						INT
	 ,@pNormalLimitPlayTimeMax   	INT
     ,@pPcBangLimitPlayTimeMax   	INT
	 ,@pNormalLimitTime				INT OUTPUT
	 ,@pPcBangLimitTime				INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF NOT EXISTS(SELECT mUserNo FROM dbo.TblLimitPlayTime WHERE mUserNo = @pUserNo)
	BEGIN
		INSERT INTO dbo.TblLimitPlayTime (mUserNo, mNormalLimitTime, mPcBangLimitTime)
		VALUES (@pUserNo, @pNormalLimitPlayTimeMax, @pPcBangLimitPlayTimeMax)
		IF(0 <> @@ERROR)
		BEGIN
			SELECT @pNormalLimitTime = 0, @pPcBangLimitTime = 0
			RETURN(1);
		END
	END

	SELECT @pNormalLimitTime = mNormalLimitTime, @pPcBangLimitTime = mPcBangLimitTime FROM dbo.TblLimitPlayTime WHERE mUserNo = @pUserNo
	
	RETURN(0);

GO

