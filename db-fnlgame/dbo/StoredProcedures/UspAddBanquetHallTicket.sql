/****** Object:  Stored Procedure dbo.UspAddBanquetHallTicket    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.UspAddBanquetHallTicket    Script Date: 2011-3-17 14:50:08 ******/

/****** Object:  Stored Procedure dbo.UspAddBanquetHallTicket    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.UspAddBanquetHallTicket    Script Date: 2010-12-23 17:46:05 ******/

/****** Object:  Stored Procedure dbo.UspAddBanquetHallTicket    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.UspAddBanquetHallTicket    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.UspAddBanquetHallTicket    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.UspAddBanquetHallTicket    Script Date: 2009-7-14 13:13:33 ******/
CREATE Procedure [dbo].[UspAddBanquetHallTicket]
	 @pTicketSerialNo	BIGINT			-- ???????
	,@pTerritory		INT			-- ????
	,@pBanquetHallType	INT			-- ?????
	,@pBanquetHallNo	INT			-- ?????
	,@pOwnerPcNo		INT			-- ?????
	,@pFromPcNm		VARCHAR(12)		-- ??? ?? ??
	,@pToPcNm		VARCHAR(12)		-- ?? ??? ??
    ,@pSendTicket   	INT			-- ??? ??? ??
    ,@pRecvTicket   	INT			-- ?? ??? ??

AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE 	@aRowCnt 			INT,
				@aErrNo				INT,
				@aToPcNo			INT,
				@aLetterLimit		BIT,
				@aToGuildNo			INT,
				@aFromGuildNo		INT,
				@aFromPcNo			INT,
				@aFromGuildAssNo	INT,
				@aToGuildAssNo		INT

	DECLARE	@pUnusedBanquetHallTicketItemID	INT
	DECLARE	@pReceivedBanquetHallTicketID	INT

			
	SELECT @aErrNo = 0, @aRowCnt = 0, @aToPcNo = 0, 
			@aRowCnt = 0, @aToPcNo = 0, @aLetterLimit = 0,
			@aToGuildNo = 0,@aFromGuildNo = 0,@aFromPcNo = 0,
			@aFromGuildAssNo = 0,@aToGuildAssNo = 0


	SET @pUnusedBanquetHallTicketItemID	= @pSendTicket		-- ??? ???
    SET @pReceivedBanquetHallTicketID	= @pRecvTicket		-- ?? ??? ???


	-- ?? ?? ??? ??
	SELECT 
		@aToPcNo = T1.mNo
		, @aLetterLimit = T2.mIsLetterLimit
		, @aToGuildNo = ISNULL(T3.mGuildNo, 0)
		, @aToGuildAssNo = ISNULL(T4.mGuildAssNo, 0)
	FROM dbo.TblPc T1
		INNER JOIN dbo.TblPcState T2 
				ON T1.mNo = T2.mNo	
		LEFT OUTER JOIN dbo.TblGuildMember T3
			ON T1.mNo = T3.mPcNo	
		LEFT OUTER JOIN dbo.TblGuildAssMem T4
			ON T3.mGuildNo = T4.mGuildNo
	WHERE T1.mNm = @pToPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 3			-- 3 : ?? ???? ???? ??
		GOTO T_END
	END

	-- ??? ?? ????? ??
	SELECT
		@aFromPcNo = mNo
		, @aFromGuildNo = ISNULL(T2.mGuildNo, 0)
		, @aFromGuildAssNo = ISNULL(mGuildAssNo,0)
	FROM dbo.TblPc T1
		LEFT OUTER JOIN dbo.TblGuildMember T2
			ON T1.mNo = T2.mPcNo
		LEFT OUTER JOIN dbo.TblGuildAssMem T3
			ON T2.mGuildNo = T3.mGuildNo
	WHERE mNm = @pFromPcNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 3			-- 3 : ?? ???? ???? ??
		GOTO T_END
	END

	-- ??? ?? ??? ???? ?????? ??
	-- ?? ????? ???? ????
	IF ( @aLetterLimit = 1 )
		AND (
			( ( @aFromGuildNo <> 0 AND  @aToGuildNo <> 0 ) 
				AND (@aFromGuildNo <> @aToGuildNo )  )			
			OR ( ( @aFromGuildAssNo <> 0 AND  @aToGuildAssNo <> 0 ) 
				AND ( @aFromGuildAssNo <> @aToGuildAssNo )  )			
			OR ( @aToGuildNo = 0 )
			OR ( @aFromGuildNo = 0 )
		)
	BEGIN
		SET @aErrNo = 6			-- 7 : ?? ???? ???? ??
		GOTO T_END
	END 


	IF EXISTS(	SELECT mTicketSerialNo 
				FROM dbo.TblBanquetHallTicket 
				WHERE mTicketSerialNo = @pTicketSerialNo)
	BEGIN
		SET @aErrNo = 2			-- 2 : ??? ?? ???
		GOTO T_END		
	END

	BEGIN TRAN

		-------------------------------------------------
		-- ??? ???? ??
		-------------------------------------------------
		INSERT dbo.TblBanquetHallTicket  (mTicketSerialNo, mTerritory, mBanquetHallType, mBanquetHallNo, mOwnerPcNo, mFromPcNm, mToPcNm) 
		VALUES( @pTicketSerialNo, @pTerritory, @pBanquetHallType, @pBanquetHallNo, @pOwnerPcNo, @pFromPcNm, @pToPcNm)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1		-- 1 : DB ???? ??
			GOTO T_ERR			 
		END	 

		-------------------------------------------------
		-- ????? ??? ??? ??, ??? ??		
		-------------------------------------------------
		UPDATE dbo.TblPcInventory 	
		SET 
			mItemNo = @pReceivedBanquetHallTicketID, 
			mIsConfirm = 0, 
			mPcNo = @aToPcNo, 
			mEndDate = dbo.UfnGetEndDate(GETDATE(), 7) 	-- ???? 7?? ??
		WHERE mSerialNo = @pTicketSerialNo
				AND mItemNo = @pUnusedBanquetHallTicketItemID

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 4		-- 4 : ??? ???? ??
		END		 

T_ERR:	
	IF @aErrNo = 0
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
T_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

