/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2009-5-12 9:18:19 ******/

/****** Object:  Stored Procedure dbo.UspSettleSiegeGamble    Script Date: 2008-11-10 10:37:20 ******/





CREATE  PROCEDURE  [dbo].[UspSettleSiegeGamble]	
	 @pPcNo		INT
	,@pSn			BIGINT
AS	
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE @aErrNo	INT,
			 @aRowCnt	INT
	DECLARE @aTerritory	INT
	DECLARE @aNo		INT
	DECLARE @aIsKeep	INT
	DECLARE @aIsFinish	BIT
	DECLARE @aDividend	INT
	DECLARE @aIsKeep1	INT
	DECLARE @aEndDay	SMALLDATETIME
	DECLARE @aMoneySn	BIGINT

	SET		@aEndDay = dbo.UfnGetEndDate(GETDATE(), 1000000)		
	SET		@aErrNo = 0
	SET		@aRowCnt = 0

	---------------------------------------------
	-- ?? ???? ?? 
	---------------------------------------------
	SELECT 
			@aTerritory = mTerritory,
			@aNo = mNo, 
			@aIsKeep = mIsKeep
	FROM dbo.TblSiegeGambleTicket
	WHERE mSerialNo = @pSn

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END
	END
	 
	---------------------------------------------
	-- ?? ?? ?? 
	---------------------------------------------	 
	SELECT 
		@aTerritory = mTerritory, 
		@aIsKeep1 = mIsKeep, 
		@aDividend = mDividend, 
		@aIsFinish = mIsFinish
	FROM dbo.TblSiegeGambleState
	WHERE mTerritory = @aTerritory 
			AND mNo = @aNo
	
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 2
		GOTO T_END
	END		
	 
	IF @aIsFinish = 0
	BEGIN
		SET @aErrNo = 3
		GOTO T_END
	END
	 
	---------------------------------------------
	-- ???? ??? ???? ??^^.
	---------------------------------------------	
	IF @aIsKeep <> @aIsKeep1
	BEGIN
		SET @aDividend = 10
	END

	BEGIN TRAN

		---------------------------------------------
		-- ?? ??
		---------------------------------------------
		DELETE dbo.TblSiegeGambleTicket 
		WHERE mSerialNo = @pSn

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 11
			GOTO T_ERR
		END	

		---------------------------------------------
		-- ??? ??
		---------------------------------------------
		DELETE dbo.TblPcInventory 
		WHERE mSerialNo = @pSn
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 4
			GOTO T_ERR		 
		 END	
	
		SELECT 
			TOP 1 @aMoneySn = mSerialNo
		FROM dbo.TblPcInventory
		WHERE mPcNo = @pPcNo
					AND mItemNo = 409

		IF @aMoneySn IS NOT NULL
		BEGIN

			UPDATE dbo.TblPcInventory 
			SET 
				mCnt = mCnt + @aDividend 
			WHERE mSerialNo = @aMoneySn 

			SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF @aErrNo <> 0 OR @aRowCnt <= 0 
			BEGIN
				SET @aErrNo = 5
				GOTO T_ERR			 
			END		
	
		END
		ELSE	
		BEGIN

			EXEC @aMoneySn =  dbo.UspGetItemSerial 
			IF @aMoneySn <= 0
			BEGIN
				SET @aErrNo = 5
				GOTO T_ERR			 
			END					INSERT dbo.TblPcInventory (mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
				VALUES(@aMoneySn, @pPcNo, 409, @aEndDay, 1, 1, @aDividend, 0)
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 6
				GOTO T_ERR			 
			END
			
		END

T_ERR:
	IF @aErrNo = 0
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN

T_END:	
	SELECT @aErrNo, @aMoneySn, DATEDIFF(dd,GETDATE(),@aEndDay), @aDividend, @aTerritory, @aNo
	SET NOCOUNT OFF

GO

