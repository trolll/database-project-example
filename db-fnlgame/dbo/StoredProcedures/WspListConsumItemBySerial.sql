/******************************************************************************
**		Name: WspListConsumItemBySerial
**		Desc: 
**
**		Auth: JUDY
**		Date: 2010-12-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspListConsumItemBySerial]
	@mSerialString VARCHAR(500)		-- 콤마 구분
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	SET ROWCOUNT 1000;		-- 최대 가져올 수 있는 아이템 수를 제한 

	SELECT 
		 T1.mNm		-- 소지자
		,T1.mNo		-- 캐릭터 번호
		,T4.IName		-- 아이템명
		,DATEDIFF(DAY, GETDATE(), T3.mEndDate) AS mEndDate	-- 유효기간
		,T3.mSerialNo
		,T2.mCurCnt
		,T2.mItemNo
		,0 mIsSeizure
		,T3.mRegDate	-- 습득일자
		,T3.mPracticalPeriod
		,T3.mBindingType
		,T3.mRestoreCnt
	FROM dbo.TblConsignmentItem  T3 WITH(INDEX=NC_TblConsignmentItem_mSerialNo)	
		INNER LOOP JOIN dbo.TblConsignment  T2 
			ON T3.mCnsmNo = T2.mCnsmNo				
		INNER JOIN dbo.TblPc T1 
			ON T1.mNo = T2.mPcNo			
		LEFT OUTER JOIN FNLParm.dbo.DT_Item T4
			ON T2.mItemNo = T4.IID	
	WHERE T3.mSerialNo IN (		
		SELECT 
			CONVERT(BIGINT, element)
		FROM dbo.fn_SplitTSQL ( @mSerialString, ',')
	)

GO

