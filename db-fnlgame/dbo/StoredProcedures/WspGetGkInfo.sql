/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2009-7-14 13:13:32 ******/

/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2009-5-12 9:18:19 ******/  
  
/****** Object:  Stored Procedure dbo.WspGetGkInfo    Script Date: 2008-11-10 10:37:20 ******/  
  
  
  
-- GAME DB 鬧雷linkserver  
  
-- GAME DB  
  
CREATE PROCEDURE [dbo].[WspGetGkInfo]   
 @mNo int  
 AS  
 SET NOCOUNT ON  
   
 SELECT   
  k.mRegDate ,  
  p.mPlaceNm,  
  n.mDesc,  
  c.mNm   
 FROM   
  TblGkill AS k  
 INNER JOIN   
  [FNLParm].dbo.TblPlace AS p  
 ON   
  k.mPlace=p.mPlaceNo  
 INNER JOIN   
  [FNLParm].dbo.TblGkillNode AS n  
 ON   
  k.mNode=n.mNode  
 INNER JOIN   
  Tblpc c  
 ON   
  k.mPcNo=c.mNo  
 WHERE  
  k.mGuildNo=@mNo  
 ORDER BY  
  k.mRegDate DESC  
  
  
  /****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2009-5-12 9:18:19 ******/  
  
/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2008-11-10 10:37:20 ******/

GO

