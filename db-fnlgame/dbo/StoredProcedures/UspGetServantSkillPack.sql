/******************************************************************************
**		Name: UspGetServantSkillPack
**		Desc: ¼­¹øÆ® ½ºÅ³ ÆÑ Á¤º¸ ¾ò±â
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetServantSkillPack]
	@pSerialNo		BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		mSPID,
		DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
	FROM dbo.TblPcServantSkillPack WITH(NOLOCK)
	WHERE   mSerialNo = @pSerialNo
		AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY mSPID ASC

	SET NOCOUNT OFF;

GO

