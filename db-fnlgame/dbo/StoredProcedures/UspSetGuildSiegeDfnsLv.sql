/******************************************************************************
**		Name: UspSetGuildSiegeDfnsLv
**		Desc: ±æµå Á¡·É ÇýÅÃ ·¹º§ º¯°æ
**
**		Auth: ±è°­È£
**		Date: 2009.10.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 		Author:				Description: 
**		--------	--------			---------------------------------------
**      2014.9.24	°ø¼®±Ô				¿¤Å×¸£ °ø¼º Á¡·É ÇýÅÃ Ãß°¡
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetGuildSiegeDfnsLv]
	 @pGuildNo				INT
	,@pCastleDfnsLv			SMALLINT
	,@pSpotDfnsLv			SMALLINT
	,@pEtelrCastleDfnsLv	SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo INT
	SET @aErrNo = 0	

	UPDATE dbo.TblGuild SET mCastleDfnsLv = @pCastleDfnsLv
							, mSpotDfnsLv = @pSpotDfnsLv
							, mEtelrCastleDfnsLv = @pEtelrCastleDfnsLv
	WHERE mGuildNo = @pGuildNo
	 
	IF(@@ROWCOUNT = 0)
	BEGIN
		SET @aErrNo	= 1
	END

	RETURN @aErrNo

GO

