/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2011-4-19 15:24:33 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspAddGuildAgitTime    Script Date: 2008-11-10 10:37:16 ******/





CREATE Procedure [dbo].[UspAddGuildAgitTime]
	 @pTerritory		INT			-- ????
	,@pOwnerGID		INT			-- ????
	,@pBuyingMoney	BIGINT			-- ????
	,@pGuildAgitNo		INT OUTPUT		-- ???????
	,@pLeftMin		INT OUTPUT		-- ??? (??) [??/??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	SET 		@pGuildAgitNo = 0

	-- ??? ???? ??
	IF NOT EXISTS(SELECT mGID FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pOwnerGID)
	BEGIN
		SET @aErrNo = 2			-- 2 : ????? ???? ??
		GOTO LABEL_END_LAST			 
	END

	-- ??? ???? ??
	DECLARE @aCurGuildMoney	BIGINT
	SELECT @aCurGuildMoney = mGuildMoney FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pOwnerGID
	IF (@aCurGuildMoney < @pBuyingMoney)
	BEGIN
		SET @aErrNo = 3			-- 3 : ????? ??
		GOTO LABEL_END_LAST			 
	END

	-- ????? ???? ??
	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID)
	BEGIN
		-- ?? ????? ??

		BEGIN TRAN

		-- ?????? ? ??
		UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney - @pBuyingMoney WHERE mGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB ???? ??
			GOTO LABEL_END			 
		END	 

		-- ??? ??
		UPDATE TblGuildAgit SET mLeftMin = mLeftMin + @pLeftMin WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1		-- 1 : DB ???? ??
			GOTO LABEL_END			 
		END	 

		SELECT @pGuildAgitNo = mGuildAgitNo, @pLeftMin = mLeftMin FROM TblGuildAgit WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID
	END
	ELSE
	BEGIN
		-- ????? ???? ??

		SET @aErrNo = 4			-- 4 : ?? ?????? ???? ??
		GOTO LABEL_END_LAST			 
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

