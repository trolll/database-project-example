/******************************************************************************  
**  File: 
**  Name: UspSetGuildSkillTreeNodeItemExp
**  Desc: 길드 스킬트리노드아이템의 경험치를 변경한다.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetGuildSkillTreeNodeItemExp]
	@pGuildNo		INT,
	@pSTNIID		INT,
	@pExp			BIGINT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	update TblGuildSkillTreeInventory
	set mExp = @pExp
	where mGuildNo=@pGuildNo and mSTNIID = @pSTNIID
	
	if @@error <> 0
		begin
			set @aErrNo=1
		end

	RETURN @aErrNo

GO

