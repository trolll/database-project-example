/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspBattleGuild    Script Date: 2008-11-10 10:37:16 ******/


CREATE PROCEDURE [dbo].[UspBattleGuild]
	 @pGuildNo1	INT
	,@pGuildNo2	INT	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	DECLARE	@aGuildNo1	INT
	DECLARE	@aGuildNo2	INT
	
	IF(@pGuildNo1 < @pGuildNo2)
	 BEGIN
		SET	@aGuildNo1	= @pGuildNo1
		SET	@aGuildNo2	= @pGuildNo2
	 END
	ELSE
	 BEGIN
		SET	@aGuildNo1	= @pGuildNo2
		SET	@aGuildNo2	= @pGuildNo1	 
	 END

	-- ?? ??? ?? 
	IF NOT EXISTS(  SELECT * 
					FROM dbo.TblGuild
					WHERE mGuildNo = @aGuildNo1 )
	BEGIN
		RETURN(1)	-- db error 	
	END 

	IF NOT EXISTS(  SELECT * 
					FROM dbo.TblGuild
					WHERE mGuildNo = @aGuildNo2 )
	BEGIN
		RETURN(1)	-- db error 	
	END 
	
	-- ?? ?? ?? ?? 
	IF EXISTS(	SELECT *
				FROM dbo.TblGuildBattle
				WHERE mGuildNo1 = @aGuildNo1 AND  mGuildNo2 = @aGuildNo2)
	BEGIN
		SET @aErrNo = 2		-- ?? ?? ?? ??? ???? ??
		RETURN(@aErrNo)
	END 

	INSERT INTO TblGuildBattle(	[mGuildNo1], [mGuildNo2]) 
	VALUES(	
			@aGuildNo1, 
			@aGuildNo2
	)
	
	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo = 1		-- SQL Error
		RETURN(@aErrNo)
	END	
	
	RETURN(0)

GO

