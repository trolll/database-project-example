/******************************************************************************
**		Name: UspResetPcQuestConditionCnt
**		Desc: Ä³¸¯ÅÍ Äù½ºÆ® Á¶°Ç ¸®¼Â
**
**		Auth: Á¤Áø¿í
**		Date: 2014-09-16
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:		Description:
**		--------	--------	---------------------------------------
**		2014-09-16	Á¤Áø¿í		»ý¼º
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspResetPcQuestConditionCnt]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pMonNo   		INT
	,@aMaxCnt		INT OUTPUT	-- Àâ¾Æ¾ßÇÏ´Â ¸ó½ºÅÍ ¼ö
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    UPDATE dbo.TblPcQuestCondition 
	SET mCnt = 0, @aMaxCnt = mMaxCnt
	WHERE mPcNo = @pPcNo
			AND mQuestNo = @pQuestNo
            AND mMonsterNo = @pMonNo

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(2);
	END
	 		 
	RETURN(0);

GO

