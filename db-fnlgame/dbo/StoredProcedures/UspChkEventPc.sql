/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2009-6-1 15:32:38 ******/

/****** Object:  Stored Procedure dbo.UspChkEventPc    Script Date: 2009-5-12 9:18:17 ******/
CREATE PROCEDURE [dbo].[UspChkEventPc]  	
	@pUserNo	INT
	, @pCharNm	VARCHAR(12)
	, @pResult	BIT = 0 OUTPUT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	

	IF EXISTS (	SELECT *
				FROM dbo.TblPc T1
					INNER JOIN dbo.TblPcState T2
						ON T1.mNo = T2.mNo
				WHERE T1.mOwner = @pUserNo
					AND T1.mNm = @pCharNm
					AND T1.mRegDate >= '2008-09-25 00:00:00'
					AND T1.mDelDate IS NULL
					AND T2.mLevel >= 3 )
	BEGIN
		SET @pResult = 1
	END					
	ELSE
	BEGIN 
		SET @pResult = 0
	END

GO

