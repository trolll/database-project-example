/******************************************************************************
**		Name: UspGetPcAchieveList
**		Desc: 诀利 府胶飘 炼雀
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcAchieveList]
  @pPcNo  INT  
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT   
		 mAchieveID
		,mActionCount
		,mIsComplete
		,mIsNew
		,mSerialNo
	FROM dbo.TblPcAchieveList WHERE mPcNo = @pPcNo

GO

