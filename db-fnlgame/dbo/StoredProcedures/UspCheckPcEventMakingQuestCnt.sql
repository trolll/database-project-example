/******************************************************************************
**		Name: UspCheckPcEventMakingQuestCnt
**		Desc: 柳青吝牢 捞亥飘 涅胶飘啊 割俺牢啊
**
**		Auth: 沥柳龋
**		Date: 2010-04-04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspCheckPcEventMakingQuestCnt]
	  @pPcNo			INT
	 ,@pStartSQuestNo	INT			-- 矫胶袍 涅胶飘 皋捞欧 矫累锅龋
	 ,@pEndSQuestNo		INT			-- 矫胶袍 涅胶飘 皋捞欧 场锅龋
	 ,@pEventQCnt		INT OUTPUT

AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @pEventQCnt = count(*)
	FROM dbo.TblPcQuest as T1
	WHERE T1.mPcNo = @pPcNo 
		AND T1.mQuestNo BETWEEN @pStartSQuestNo AND @pEndSQuestNo
		AND T1.mValue < 99;

GO

