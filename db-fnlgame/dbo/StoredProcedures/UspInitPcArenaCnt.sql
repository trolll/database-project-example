/******************************************************************************
**		Name: UspInitPcArenaCnt
**		Desc: 酒饭唱 曼咯冉荐 檬扁拳
**
**		Auth: 沥柳龋
**		Date: 2016-09-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspInitPcArenaCnt]
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblPcState
	SET mFierceCnt = 0, mBossCnt = 0

GO

