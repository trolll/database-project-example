/******************************************************************************  
**  Name: UspGetCnsmBead
**  Desc: 지정된 위탁판매 아이템에 결합된 구슬 리스팅
**  
**                
**  Return values:  
**   레코드셋
**   
**                
**  Author: 김강호
**  Date: 2011-07-11
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetCnsmBead]
	@pCnsmSn BIGINT -- 위탁 시리얼 번호
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT 
	a.mOwnerSerialNo
	,a.mItemNo
	,a.mBeadIndex
	,DATEDIFF(mi,GETDATE(),a.mEndDate) as mEndDateA
	,a.mCntUse
	FROM	
	TblPcBead a
	where a.mOwnerSerialNo IN(SELECT mSerialNo FROM dbo.TblConsignmentItem WHERE mCnsmNo=@pCnsmSn)
		and	a.mEndDate > CONVERT(SMALLDATETIME, GETDATE());

GO

