/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetLimitedResource    Script Date: 2009-5-12 9:18:16 ******/

CREATE PROCEDURE [dbo].[UspSetLimitedResource]
	@pResourceType INT
AS
	SET NOCOUNT ON
	
	DECLARE @aRemainerCnt INT,
			@aRandomVal FLOAT,
			@aCurrRandomVal FLOAT 
			
			
	SELECT @aRemainerCnt = 0,
			@aRandomVal = 0,
			@aCurrRandomVal = 0
			
			
	SELECT 
		@aRemainerCnt = mRemainerCnt,
		@aRandomVal = mRandomVal
	FROM dbo.TblLimitedResource
	WHERE mResourceType = @pResourceType

	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0 
	BEGIN
		RETURN (1) -- db error
	END 
	
	SELECT @aCurrRandomVal = RAND() * 100 -- 100% ??
	
	IF CONVERT(BIGINT,(@aCurrRandomVal*10000)) > CONVERT(BIGINT, (@aRandomVal*10000))
	BEGIN
		RETURN(2) -- ??? ? ??.
	END 
	
	IF @aRemainerCnt <= 0
	BEGIN
		RETURN(3) -- ?? ??? ??.
	END
	
	UPDATE dbo.TblLimitedResource
	SET 
		@aRemainerCnt = mRemainerCnt = mRemainerCnt - 1,
		mUptDate = GETDATE()
	WHERE mResourceType = @pResourceType
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0 
	BEGIN
		RETURN (1) -- db error
	END 
	
	IF @aRemainerCnt < 0 
	BEGIN
		RETURN(3) -- ?? ??? ??.
	END
	
	RETURN(0) -- ??

GO

