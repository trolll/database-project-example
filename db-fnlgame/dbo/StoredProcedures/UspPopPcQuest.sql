/******************************************************************************
**		Name: UspPopPcQuest
**		Desc: 韤橃姢韸?靷牅
**
**		Auth: 鞝曥順?
**		Date: 2010-02-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPopPcQuest]
	 @pPcNo			INT
	,@pQuestNo		INT	 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	BEGIN TRAN

		DELETE dbo.TblPcQuestCondition
		WHERE mQuestNo = @pQuestNo AND mPcNo = @pPcNo

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		DELETE dbo.TblPcQuest
		WHERE mQuestNo = @pQuestNo AND mPcNo = @pPcNo

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(2);			
		END 

	COMMIT TRAN
	 		 
	RETURN(0)

GO

