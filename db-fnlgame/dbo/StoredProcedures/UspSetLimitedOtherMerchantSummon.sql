/******************************************************************************
**		Name: UspSetLimitedOtherMerchantSummon
**		Desc: ÀÌ°èÀÇ »óÀÎ ¼ÒÈ¯ È½¼ö Á¦ÇÑ
**
**		Auth: Á¤ÁøÈ£
**		Date: 2015-10-16
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetLimitedOtherMerchantSummon]
	@pMerchantID	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aAbleSummonCnt	TINYINT
	SELECT	@aAbleSummonCnt = 0
			
	SELECT 
		@aAbleSummonCnt = mAbleSummonCnt
	FROM dbo.TblLimitedOtherMerchantSummon
	WHERE mMerchantID = @pMerchantID
	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0  
	BEGIN
		RETURN (1)	-- db error
	END 
		
	IF @aAbleSummonCnt <= 0
	BEGIN
		RETURN(2)	-- ÀÜ¿© °¹¼ö°¡ ¾ø´Ù.
	END

	UPDATE dbo.TblLimitedOtherMerchantSummon
	SET 
		@aAbleSummonCnt = mAbleSummonCnt = mAbleSummonCnt - 1
	WHERE mMerchantID = @pMerchantID
	
	IF @@ERROR <> 0 OR @@ROWCOUNT <= 0  
	BEGIN
		RETURN (1)	-- db error
	END 		

	IF @aAbleSummonCnt < 0 
	BEGIN
		RETURN(2)	-- ÀÜ¿© °¹¼ö°¡ ¾ø´Ù.
	END
	
	RETURN(0)	-- ¹ßÇà

GO

