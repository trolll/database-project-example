/******************************************************************************
**		Name: UspCheckPcRegionQuestCnt
**		Desc: ÁøÇàÁßÀÎ Áö¿ªÄù½ºÆ® °¹¼ö
**
**		Auth: Á¤Áø¿í
**		Date: 2014-09-26
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:		Description:
**		--------	--------	---------------------------------------
**		2014-09-26	Á¤Áø¿í		»ý¼º
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspCheckPcRegionQuestCnt]
	  @pPcNo			INT
	 ,@pStartRegionQNo	INT			-- Áö¿ª Äù½ºÆ® ½ÃÀÛ¹øÈ£
	 ,@pEndRegionQNo	INT			-- Áö¿ª Äù½ºÆ® ³¡¹øÈ£
	 ,@pCnt				INT OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @pCnt = COUNT(*)
	FROM dbo.TblPcQuest
	WHERE mPcNo = @pPcNo
	AND mQuestNo BETWEEN @pStartRegionQNo AND @pEndRegionQNo
	AND mValue < 99

GO

