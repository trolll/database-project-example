CREATE PROCEDURE [dbo].[WspListGuildItemBySerial]  	
	@pSerialstring	VARCHAR(500)
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		T3.mGuildNm
		, T3.mGuildNo
		, T4.iName
		, T1.mStatus
		, DATEDIFF(DAY, GETDATE(), T1.mEndDate) AS mEndDate
		, T1.mSerialNo
		, T1.mCnt
		, T1.mItemNo
		, T1.mIsSeizure		
		, T1.mPracticalPeriod 
		, T1.mBindingType
	FROM dbo.TblGuildStore AS T1
		INNER JOIN dbo.fn_SplitTSQL(@pSerialstring,',') T2
			ON T1.mSerialNo = T2.element	
		INNER JOIN dbo.TblGuild T3
			ON T1.mGuildNo = T3.mGuildNo
		LEFT OUTER JOIN [FNLParm].dbo.DT_ITEM T4
			ON T1.mItemNo = T4.IID

GO

