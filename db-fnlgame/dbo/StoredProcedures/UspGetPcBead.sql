/******************************************************************************  
**  Name: UspGetPcBead
**  Desc: 구슬 리스팅(장비에 결합된 구슬)
**  
**                
**  Return values:  
**   레코드셋
**   
**                
**  Author: 김강호
**  Date: 2011-06-14
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcBead]
			 @pPcNo		INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	IF @pPcNo = 1 OR @pPcNo = 0 
	BEGIN
		RETURN(0)	-- NPC, 버려진 아이템
	END
	
	SELECT 
	a.mOwnerSerialNo
	,a.mItemNo
	,a.mBeadIndex
	,DATEDIFF(mi,GETDATE(),a.mEndDate) as mEndDateA
	,a.mCntUse
	FROM	
	TblPcBead a
	INNER JOIN dbo.TblPcInventory b ON a.mOwnerSerialNo=b.mSerialNo
	AND b.mPcNo = @pPcNo
	AND a.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	AND b.mEndDate > CONVERT(SMALLDATETIME, GETDATE());
	
	RETURN 0;

GO

