CREATE PROCEDURE [dbo].[WspPushItemStore]
	@pUserNo		INT				-- ─│╕»┼═╕ª ╝╥└»╟╤ ╗τ┐δ└┌╣°╚ú.(├ó░φ┤┬ ─│╕»┼═░í ░°└»╟╘)
	,@pItemNo		INT				-- ╛╞└╠┼█ ╣°╚ú 	
	,@pCnt			INT				-- ╗²╝║╟╥ ░╣╝÷
	,@pStatus		TINYINT			-- ╗≤┼┬
	,@pIsSeizure	BIT				-- ╛╨╖∙┐⌐║╬ 
	,@pIsConfirm	BIT				-- ╚«└╬ ┐⌐║╬ 
	,@pIsStack		BIT
	,@pValidDay		INT				-- └»╚┐└╧.(┤▄└º:└╧)
	,@pTargetSn		BIGINT OUTPUT	-- ┼δ╟╒╡╚ SN╣°╚ú.
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
		 	
	DECLARE	@aTargetSn		BIGINT	
	DECLARE	@aCntTarget		INT
	SET		@aCntTarget = 0		
	DECLARE	@aIsSeizure		BIT	
	
	-- ╡╖.
	IF(@pItemNo = 409)
	 BEGIN
		SET		@pIsConfirm = 1
		SET		@pStatus = 1
	 END
	
	-- ╕╕╖ß└╧ 
	DECLARE	@aEndDay	SMALLDATETIME
	SET		@aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)
		
	
	IF @pIsStack = 1	
	BEGIN
		-- ╜║┼├╟ⁿ└╟ ░µ┐∞ ┤⌐└√╟╥ ╛╞└╠┼█└╗ ├ú┤┬┤┘.
		SELECT 
			@aCntTarget=ISNULL(mCnt,0), 
			@aTargetSn=mSerialNo, 
			@aIsSeizure=ISNULL(mIsSeizure,0)
		FROM dbo.TblPcStore
		WHERE mUserNo = @pUserNo				AND mItemNo = @pItemNo				AND mIsConfirm = @pIsConfirm				AND mStatus = @pStatus

		IF(0 <> @@ERROR)
		BEGIN
			RETURN(4)
		END
			
		IF(0 <> @aIsSeizure)
		BEGIN
			RETURN(5)	 
		END	
	END
	
	--------------------------------------------------------------
	-- ╛╞└╠┼█└╗ ╗²╝║╟╧░┼│¬ ┤⌐└√╟╤┤┘. 
	--------------------------------------------------------------
	IF @aTargetSn > 0
	BEGIN
		-- ┤⌐└√╟╥ ┤δ╗≤ ┴╕└τ 
		SET @pTargetSn = @aTargetSn
		
		UPDATE dbo.TblPcStore 
		SET 
			mCnt = mCnt + @pCnt
		WHERE mSerialNo = @aTargetSn
				AND mItemNo = @pItemNo
		IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
		BEGIN
			RETURN(6)
		END		
				
		RETURN(0)
	END 
	
	EXEC @pTargetSn = dbo.UspGetItemSerial 
	IF @pTargetSn <= 0 
	BEGIN
		RETURN(7)
	END 	
			
	-- ├ó░φ└╟ ╜┼▒╘ ╛╞└╠┼█ ╡ε╖╧ 
	INSERT INTO dbo.TblPcStore (mSerialNo, mUserNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
	VALUES(
		@pTargetSn,
		@pUserNo, 
		@pItemNo, 
		@aEndDay, 
		@pIsConfirm, 
		@pStatus, 
		@pCnt, 
		0	-- UsedCount (╗²╝║└║ 0└╕╖╬ ╝╝╞├ ╟╤┤┘)
	)	
	
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(8)
	END
	
	RETURN(0)

GO

