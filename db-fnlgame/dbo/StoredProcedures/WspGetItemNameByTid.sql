--------------------------------
-- ░┤├╝╕φ : WspGetItemNameByTid
-- ╝│╕φ : TID ░¬└╕╖╬ ╛╞└╠┼█ └╠╕º└╗ ╛≥┤┬┤┘.
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.06.27
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
--------------------------------

CREATE PROCEDURE [dbo].[WspGetItemNameByTid]
	@IID	INT	-- TID
AS
	SET NOCOUNT ON

	SELECT
		IName
	FROM	
		[FNLParm].[dbo].[DT_Item] --WITH (NOLOCK)
		--OPENQUERY(A55077, 'SELECT * FROM [FNLParm1].[dbo].[DT_Item] WITH (NOLOCK)')
	WHERE
		IID = @IID

	SET NOCOUNT OFF

GO

