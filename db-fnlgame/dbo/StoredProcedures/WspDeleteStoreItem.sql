/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.WspDeleteStoreItem    Script Date: 2008-11-10 10:37:18 ******/





CREATE    PROCEDURE [dbo].[WspDeleteStoreItem]
	@mNo INT, 
	@mCnt BIGINT -- ??
AS
	SET NOCOUNT ON

	SET XACT_ABORT ON
	SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

	BEGIN TRANSACTION

	IF (SELECT mCnt FROM [dbo].[TblPcStore] WHERE mSerialNo = @mNo) - @mCnt < 1

		BEGIN
			-- ??
			DELETE FROM
				[dbo].[TblPcStore]
			WHERE
				mSerialNo = @mNo
		END

	ELSE

		BEGIN
			-- ?? ??
			UPDATE
				[dbo].[TblPcStore]
			SET
				mCnt = mCnt - @mCnt
			WHERE
				mSerialNo = @mNo
		END

	IF @@ERROR = 0
		BEGIN
			COMMIT TRANSACTION
			RETURN 0
		END
	ELSE
		BEGIN
			ROLLBACK TRANSACTION
			RETURN 1
		END

GO

