/******************************************************************************
**		Name: UspSelectPcQuestTimeLimit
**		Desc: 氇摖 鞁滉皠鞝滍暅 韤橃姢韸?瓴€靷?
**
**		Auth: 鞝曥順?
**		Date: 2010-02-17
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSelectPcQuestTimeLimit]
     @pPcNo			INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT 
			mQuestNo,
			mLimitTime = DATEDIFF(ss,GETDATE(),mLimitTime)
    FROM dbo.TblPcQuest 
    WHERE mPcNo = @pPcNo 
		AND mValue < 99
		AND mLimitTime IS NOT NULL

GO

