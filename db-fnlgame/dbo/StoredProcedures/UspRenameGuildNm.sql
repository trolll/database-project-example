/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2011-4-19 15:24:41 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2011-3-17 14:50:08 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2010-12-23 17:46:05 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2010-3-22 15:58:23 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2009-12-14 11:35:31 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2009-7-14 13:13:33 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2009-6-1 15:32:41 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2009-5-12 9:18:20 ******/

/****** Object:  Stored Procedure dbo.UspRenameGuildNm    Script Date: 2008-11-10 10:37:21 ******/





CREATE PROCEDURE [dbo].[UspRenameGuildNm]
	@pGuildNo		INT,		-- ????
	@pPcNo			INT,		-- ????? ?? ??? ??
	@pOrgGuildNm	CHAR(12),	-- ?? ?? ??
	@pNewGuildNm	CHAR(12),	-- ??? ?? ??
    @pSerialNo		BIGINT,		-- ??? ??? ??
    @pItemNo		INT			-- ??? ?? 	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE	@aErrNo		INT
			,@aRowCnt	INT
			,@aNewPcNo	INT
			
	SELECT  @aErrNo = 0, @aRowCnt = 0, @aNewPcNo = 0
	

	-----------------------------------------------
	-- ?? ??? ???? 
	-----------------------------------------------
	IF EXISTS( SELECT * 
				FROM dbo.TblGuild
				WHERE mGuildNm = @pNewGuildNm )
	BEGIN
		RETURN(2)			
	END 
	
	-----------------------------------------------
	-- ??? ????? ?? ?? 
	-----------------------------------------------	
	IF NOT EXISTS( SELECT *
					FROM dbo.TblGuildMember
					WHERE mGuildNo = @pGuildNo
							AND mPcNo = @pPcNo
							AND mGuildGrade = 0 )
	BEGIN
		RETURN(3)			
	END 	
	
	-----------------------------------------------
	-- ???? ?? 
	-----------------------------------------------		
	IF EXISTS(  SELECT *
				FROM dbo.TblGuildBattle
				WHERE mGuildNo1 = @pGuildNo ) 
	BEGIN
		RETURN(4)			
	END 

	IF EXISTS(  SELECT *
				FROM dbo.TblGuildBattle
				WHERE mGuildNo2 = @pGuildNo ) 
	BEGIN
		RETURN(4)			
	END 
	
	-----------------------------------------------
	-- ?? ?? ??
	-----------------------------------------------		
	IF EXISTS(	SELECT *
				FROM dbo.TblGuildAssMem
				WHERE mGuildNo = @pGuildNo )
	BEGIN
		RETURN(5)			
	END 	
	
	-----------------------------------------------
	-- ?? ?? 
	-----------------------------------------------		
	IF EXISTS(	SELECT *
				FROM dbo.TblCastleTowerStone
				WHERE mGuildNo = @pGuildNo )
	BEGIN
		RETURN(6)			
	END 	

	-----------------------------------------------
	-- ??? ??
	-----------------------------------------------	
	BEGIN TRAN		

		DELETE dbo.TblPcInventory
		WHERE mPcNo = @pPcNo 
			AND	mSerialNo = @pSerialNo
			AND mItemNo = @pItemNo
				
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END		
	
		UPDATE dbo.TblGuild
		SET
			mGuildNm = @pNewGuildNm
		WHERE mGuildNo = @pGuildNo
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END		
	
	COMMIT TRAN
	RETURN(0)

GO

