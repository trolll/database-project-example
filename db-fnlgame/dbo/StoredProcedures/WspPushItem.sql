
/******************************************************************************
**		Name: WspPushItem
**		Desc: 
**		Ex	:
			DECLARE @Return INT
			EXEC @Return = dbo.WspPushItem
				2206
				, 409
				, 1
				, 1
				, 1
				, 1000
				, 0
				, 1
				, 20
			SELECT @Return
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	2012.03.28	捞柳急				霸烙 SP (UspPushItem) 龋免 何盒 盒府. 昆绢靛刮 UspPushItem 鼻茄 芭何. 5玫父 角滚 捞悼 矫 肺弊.
**		2015.06.16	捞柳急				辑锅飘 瘤鞭 眠啊
										- 辑锅飘狼 漂己惑 柳拳 惑怕狼 辑锅飘甫 瘤鞭且 荐 绝促. (公炼扒 扁夯屈)
										- 扁夯屈 辑锅飘狼 版快 汲沥蔼 函版 (款康矫俊 角荐规瘤)
**		2015.06.19	捞柳急				DT_Item IType = 33 (IT_Servant)
												IType = 34 (IT_ServantScroll)
										33篮, TblServantType SEvolutionStep = 0 犬牢鞘夸
**		2019.03.08	沥柳宽				酒捞袍 蜡瓤扁埃 2020斥俊辑 2079斥栏肺 函版
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspPushItem]
(
	@pNo INT,
	@pItemNo INT,
	@pStatus TINYINT,
	@pCnt INT,
	@pIsConfirm BIT,
	@pValidDay INT = NULL,
	@pPraticalHour INT = NULL,
	@pBindingType TINYINT = 0,
	@pLevel SMALLINT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;	

	DECLARE @pCntUse TINYINT
			, @pIsStack BIT
			, @pIsPracticalPeriod BIT
			, @pIsCharge INT
			, @pErr INT
			, @aEndDay SMALLDATETIME
			, @aOrgPcNo INT
			, @aIsSeizure BIT	
			, @aCntRest INT
			, @aRowCnt INT
			, @aErr INT
			, @aSerial BIGINT
			, @aCntTarget BIGINT
			, @aOwner INT
			, @aPracticalPeriod INT
			, @pSerial BIGINT
			, @aIType INT
	
	SELECT @aRowCnt = 0
			, @aErr = 0
			, @aOrgPcNo = 0
			, @aSerial = 0 
			, @aOwner = 0
			, @aPracticalPeriod = 0
			, @pSerial = 0
			, @aIType = 0

	IF (@pValidDay IS NULL)
	BEGIN
		SELECT
			@pValidDay = ITermOfValidity
			, @pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
			, @pIsCharge = IIsCharge
			, @aIType = IType
		FROM FNLParm.[dbo].[DT_Item]
		WHERE IID = @pItemNo
	END
	ELSE
	BEGIN
		SELECT
			@pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
			, @pIsCharge = IIsCharge
			, @aIType = IType
		FROM FNLParm.[dbo].[DT_Item]
		WHERE IID = @pItemNo
	END
		
	SET @aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)

	IF @aEndDay < GETDATE()
	BEGIN
		SET @aErr = 1				
		GOTO T_END
	END 

	IF (@pValidDay IS NULL OR @pCntUse IS NULL OR @pIsStack IS NULL)
	BEGIN
		RETURN 2
	END
		
	IF (@pIsPracticalPeriod = 1 AND ( @pPraticalHour IS NULL OR @pPraticalHour <= 0))
	BEGIN
		RETURN 3
	END
	
	IF (@pPraticalHour > (30*24))
	BEGIN
		RETURN 4
	END

	IF (@pLevel IS NOT NULL)  
	BEGIN  
		IF EXISTS (SELECT * FROM FNLParm.dbo.TblServantType WHERE IID = @pItemNo AND SEvolutionStep = 0)
		BEGIN  
			SELECT @pCnt = 1  
					, @pStatus = 1  
					, @pIsConfirm = 1  
					, @pBindingType = 1  
					, @aEndDay = '2079-01-01'	-- 扁粮俊绰 '2020-01-01'
					, @pPraticalHour = 0
		END
		ELSE  
		BEGIN  
			RETURN 5  
		END  
	END
	
	IF (@aIType = 34)
	BEGIN
		SELECT @pLevel = 1 
				, @pStatus = 1
				, @pIsConfirm = 1
				, @pBindingType = 1
	END

	IF (@pPraticalHour IS NULL)
	BEGIN
		SET @pPraticalHour = 0
	END
	
	/*	
	-- UspPushItem阑 荤侩窍咯 积己茄促
	EXECUTE dbo.UspPushItem @pNo, 0, @pItemNo, @pValidDay, @pCnt, @pCntUse, @pIsConfirm, @pStatus, @pIsStack, @pIsCharge, @pPraticalHour, @pBindingType
	*/
	
	IF (@pSerial <> 0 AND @pBindingType IN(1,2))
	BEGIN
		SET @aErr = 11	-- 蓖加 加己篮 捞悼且 荐啊 绝促. 			
		GOTO T_END
	END 	
			
	-----------------------------------------------
	-- 409 : 捣 酒捞袍 
	-----------------------------------------------
	IF(@pItemNo = 409)
	BEGIN
		SET @pIsConfirm = 1
		SET @pStatus = 1
	END
		
	-----------------------------------------------
	-- 扁粮 酒捞袍
	-----------------------------------------------
	IF( @pSerial <> 0 )
	BEGIN		
		SELECT 	TOP 1		
			@aOrgPcNo = [mPcNo],	-- 扁粮 家蜡磊	
			@aCntRest=[mCnt], 
			@aEndDay=[mEndDate], 
			@aIsSeizure=[mIsSeizure],
			@aOwner = [mOwner],
			@aPracticalPeriod = [mPracticalPeriod],
			@pBindingType = mBindingType
		FROM dbo.TblPcInventory WITH(INDEX=PK_NC_TblPcInventory_1)
		WHERE  mSerialNo = @pSerial
		SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErr <> 0 OR 	@aRowCnt = 0
		BEGIN
			SET @aErr = 1				
			GOTO T_END
		END		
	
		IF @aIsSeizure <> 0
		BEGIN
			SET @aErr = 29	
			GOTO T_END					--	拘幅 惑怕 酒捞袍				
		END
		SET @aCntRest = @aCntRest - @pCnt
		IF (@aCntRest <  0 )
		BEGIN
			SET @aErr = 2				-- 荤侩且 荐 乐绰 酒捞袍 肮荐啊 葛磊福促.( 公炼扒 0焊促 目具 茄促)
			GOTO T_END	
		END
		
		IF @pBindingType IN(1,2)	
		BEGIN
			SET @aErr = 11	-- 蓖加 加己篮 捞悼且 荐啊 绝促. 			
			GOTO T_END		
		END 	
		
	END
	-----------------------------------------------
	-- 穿利屈 酒捞袍 粮犁咯何 眉农
	-----------------------------------------------
	IF (@pIsStack <> 0) AND (1 <> @pNo)
	BEGIN
		SELECT 	TOP 1			
			@aCntTarget=ISNULL([mCnt],0), 
			@aSerial=[mSerialNo], 
			@aIsSeizure=ISNULL([mIsSeizure],0)
		FROM dbo.TblPcInventory WITH(INDEX=CL_TblPcInventory)		
		WHERE  mPcNo  = @pNo
				AND mItemNo = @pItemNo 
				AND mEndDate  = @aEndDay
				AND mIsConfirm  = @pIsConfirm  
				AND mStatus  = @pStatus
				AND mOwner = @aOwner  
				AND mPracticalPeriod = @pPraticalHour
				AND mBindingType = @pBindingType	-- 穿利屈 酒捞袍狼 版快 官牢靛 鸥涝捞 悼老秦具 茄促.
 
		SELECT @aErr = @@ERROR
		IF ( @aErr <> 0 )
		BEGIN
			SET @aErr = 3	
			GOTO T_END
		END		
		IF(@aIsSeizure <> 0 )				
		 BEGIN
			SET @aErr = 28	
			GOTO T_END
		 END	
	END	
	
	-----------------------------------------------
	-- 酒捞袍 捞悼
	-----------------------------------------------
	BEGIN TRAN;
		IF @aCntTarget  <> 0 
		BEGIN
			UPDATE dbo.TblPcInventory				--	穿利侩 酒捞袍 粮犁 窍搁 扁粮 酒捞袍俊 穿利窍绊 
			SET
				mCnt = mCnt + @pCnt
			WHERE  mSerialNo = @aSerial
			
			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF  @aErr <> 0 OR 	@aRowCnt = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 4	
				GOTO T_END	
			END		
			IF ( @pSerial <> 0)
			BEGIN
				IF @aCntRest < 1		-- 儡咯酒捞袍 粮犁窍瘤 臼阑 锭 
				BEGIN
					DELETE dbo.TblPcInventory
					WHERE mSerialNo = @pSerial
	
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 5	
						GOTO T_END	
					END		
				END 
				ELSE
				BEGIN
					UPDATE dbo.TblPcInventory			
					SET
						mCnt = @aCntRest
					WHERE  mSerialNo =@pSerial				
	
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 6	
						GOTO T_END	
					END		
				END 
			END
		END		
		ELSE
		BEGIN
			IF ( @pSerial <> 0 )
			BEGIN
				IF @aCntRest < 1		-- 儡咯酒捞袍 粮犁窍瘤 臼阑 锭 
				BEGIN		
					SET @aSerial = @pSerial
				
					UPDATE dbo.TblPcInventory			
					SET
						mPcNo = @pNo,
						mCnt = @pCnt
					WHERE  mSerialNo =@pSerial				
		
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 7	
						GOTO T_END
					END		
				END
				ELSE
				BEGIN
		
					UPDATE dbo.TblPcInventory			
					SET
						mCnt = @aCntRest
					WHERE  mSerialNo =@pSerial				
		
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 8	
						GOTO T_END	
					END			
				
					EXEC @aSerial =  dbo.UspGetItemSerial 	-- 矫府倔 掘扁 
					IF @aSerial <= 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 7		
						GOTO T_END	
					END		
				
					INSERT dbo.TblPcInventory(				-- 穿利侩 酒捞袍 粮犁窍瘤 臼栏搁, 货酚霸 积己 
						[mSerialNo],
						[mPcNo], 
						[mItemNo], 
						[mEndDate], 
						[mIsConfirm], 
						[mStatus], 
						[mCnt], 
						[mCntUse],
						[mOwner],
						[mPracticalPeriod],	mBindingType )
					VALUES(
						@aSerial,
						@pNo, 
						@pItemNo, 
						@aEndDay, 
						@pIsConfirm, 
						@pStatus, 
						@pCnt, 
						@pCntUse,
						@aOwner,
						@pPraticalHour,@pBindingType)
					
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN				
						ROLLBACK TRAN
						SET @aErr = 9
						GOTO T_END	
					END	
					
					IF (@pLevel IS NOT NULL)
					BEGIN
						INSERT INTO dbo.TblPcServant (mPcNo, mSerialNo, mLevel)
						VALUES (@pNo, @aSerial, @pLevel)
						
						SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
						
						IF  @aErr <> 0 OR 	@aRowCnt = 0
						BEGIN				
							ROLLBACK TRAN
							SET @aErr = 9
							GOTO T_END	
						END
					END
				END
			END 
			ELSE
			BEGIN
				EXEC @aSerial =  dbo.UspGetItemSerial 	-- 矫府倔 掘扁 
				IF @aSerial <= 0
				BEGIN
					ROLLBACK TRAN
					SET @aErr = 7		
					GOTO T_END	
				END		
				INSERT dbo.TblPcInventory(				
					[mSerialNo],
					[mPcNo], 
					[mItemNo], 
					[mEndDate], 
					[mIsConfirm], 
					[mStatus], 
					[mCnt], 
					[mCntUse],
					[mOwner],
					[mPracticalPeriod], mBindingType )
				VALUES(
					@aSerial,
					@pNo, 
					@pItemNo, 
					@aEndDay, 
					@pIsConfirm, 
					@pStatus, 
					@pCnt, 
					@pCntUse,
					@aOwner,
					@pPraticalHour, @pBindingType )
					
				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF  @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN				
					ROLLBACK TRAN
					SET @aErr = 10
					GOTO T_END	
				END
				
				IF (@pLevel IS NOT NULL)
				BEGIN
					INSERT INTO dbo.TblPcServant (mPcNo, mSerialNo, mLevel)
					VALUES (@pNo, @aSerial, @pLevel)
					
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN				
						ROLLBACK TRAN
						SET @aErr = 9
						GOTO T_END	
					END
				END
			END			
		END
	COMMIT TRAN;

	--##. 5玫父 角滚 捞惑捞搁 角青茄 荤恩 肺弊
	/*
	IF (@pItemNo = 409 AND @pCnt >= 50000000)
	BEGIN
		INSERT INTO dbo.TblLog (mRegDate, mSUSER, mUSER, mPcNo)
		VALUES (GETDATE(), SUSER_SNAME(), USER_NAME(), @pNo)
	END
	*/
END
T_END:
	SELECT @aErr, @aSerial AS Serial, DATEDIFF(mi,GETDATE(),@aEndDay)
	
	RETURN 0

GO

