/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspBetSiegeGamble    Script Date: 2008-11-10 10:37:16 ******/





CREATE  PROCEDURE [dbo].[UspBetSiegeGamble]
	 @pPcNo		INT
	,@pItemNo		INT
	,@pBettingMoney	INT
	,@pValidDay		INT		-- ????(??:?)
	,@pTerritory		INT
	,@pSiegeNo		INT
	,@pIsKeep		INT
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT
	DECLARE	@aSerial		BIGINT	-- ??? SN.
	DECLARE	@aEndDay	SMALLDATETIME,
				@aRowCnt	INT

	SELECT	@aErrNo = 0, 
			@aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay),
			@aSerial = 0,
			@aRowCnt = 0
	
	-----------------------------------------------------
	-- ??? ?? ???
	-----------------------------------------------------
	EXEC @aSerial =  dbo.UspGetItemSerial 
	IF @aSerial <= 0
	BEGIN
		SET @aErrNo = 8
		GOTO T_END	
	END 								
	

	BEGIN TRAN
			
		INSERT dbo.TblPcInventory ( mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
				VALUES(@aSerial, @pPcNo, @pItemNo, @aEndDay, 1, 1, 1, 0)	

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1
			GOTO T_ERR			 
		END 
				
		INSERT dbo.TblSiegeGambleTicket(mSerialNo, mTerritory, mNo, mIsKeep)
				VALUES(@aSerial, @pTerritory, @pSiegeNo, @pIsKeep)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 2
			GOTO T_ERR			 
		END
		 
		UPDATE dbo.TblSiegeGambleState 
		SET 
			mTotalMoney = mTotalMoney + @pBettingMoney 
		WHERE  mTerritory =@pTerritory
				AND  mNo = @pSiegeNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 3
			GOTO T_ERR			 
		END			

T_ERR:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
	
T_END:
	SELECT @aErrNo, @aSerial, DATEDIFF(dd,GETDATE(),@aEndDay)

GO

