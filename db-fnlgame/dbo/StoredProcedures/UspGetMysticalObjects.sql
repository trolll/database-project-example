/******************************************************************************
**		Name: UspGetMysticalObjects
**		Desc: ½Å±â¾ÆÀÌÅÛ Á¤º¸¸¦ °¡Á®¿Â´Ù.
**
**		Auth: Á¤Áø¿í
**		Date: 2015-07-06
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMysticalObjects]
	 @pItemAll   VARCHAR(300)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT
		T1.mRegDate
		, T1.mSerialNo
		, T1.mPcNo
		, T1.mItemNo
		, DATEDIFF(mi,GETDATE(),mEndDate) AS mEndDate
		, RTRIM(T2.mNm)
		, ISNULL(RTRIM(T3.mGuildNm), '')
	FROM TblPcInventory	 AS T1
		INNER JOIN TblPc AS T2
			ON T1.mPcNo = T2.mNo
		LEFT OUTER JOIN TblGuild AS T3
			ON T3.mGuildNo = (SELECT mGuildNo FROM TblGuildMember WHERE mPcNo = T1.mPcNo)
	WHERE
		mItemNo IN (SELECT CONVERT(INT, element) FROM dbo.fn_SplitTSQL (@pItemAll, ',')) 
		AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
		AND mCnt > 0
	ORDER BY
		T1.mRegDate

GO

