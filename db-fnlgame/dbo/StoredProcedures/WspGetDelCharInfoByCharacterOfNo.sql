CREATE PROCEDURE [dbo].[WspGetDelCharInfoByCharacterOfNo]  	
	@mNo INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	SELECT
		d.mPcNm,
		p.mNm, 
		u.mUserId, 
		u.mUserNo, 
		p.mClass, 
		p.mDelDate, 
		p.mRegDate, 
		s.mLevel, 
		s.mExp, 
		s.mPosX, s.mPosY, s.mPosZ, 
		s.mHp, 
		s.mMp, 
		s.mChaotic, 
		g.mGuildNm, 
		s.mIp, 
		p.mNo
	FROM
		[dbo].[TblPcDeleted] AS d 
		INNER JOIN [dbo].[TblPc] AS p 
			ON d.mPcNo = p.mNo
		INNER JOIN [FNLAccount].[dbo].[TblUser] AS u 
			ON p.mOwner = u.mUserNo
		INNER JOIN [dbo].[TblPcState] AS s 
			ON p.mNo = s.mNo
		LEFT OUTER JOIN [dbo].[TblGuildMember] gm
			ON d.mPcNo = gm.mPcNo
		LEFT OUTER JOIN [dbo].[TblGuild] g
			ON gm.mGuildNo = g.mGuildNo
	WHERE	
		d.mPcNo = @mNo

GO

