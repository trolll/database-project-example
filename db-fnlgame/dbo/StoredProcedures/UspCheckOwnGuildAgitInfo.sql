/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnGuildAgitInfo    Script Date: 2008-11-10 10:37:16 ******/
CREATE PROCEDURE [dbo].[UspCheckOwnGuildAgitInfo]
	@pTerritory	INT,
	@pGuildAgitNo	INT,
	@pGuildNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	IF EXISTS ( SELECT * 
				FROM dbo.TblGuildAgit 
				WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo AND mOwnerGID = @pGuildNo)
	 BEGIN
		RETURN(0);	-- ? ?? ??? ?????.
	 END
	ELSE
	 BEGIN
		RETURN(1);	-- ? ?? ??? ???? ???.
	 END

GO

