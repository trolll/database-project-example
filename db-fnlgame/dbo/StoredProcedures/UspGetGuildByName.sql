/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2011-4-19 15:24:41 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2011-3-17 14:50:08 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2010-12-23 17:46:05 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2009-12-14 11:35:31 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2009-7-14 13:13:33 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2009-6-1 15:32:41 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2009-5-12 9:18:20 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildByName    Script Date: 2008-11-10 10:37:21 ******/





CREATE PROCEDURE [dbo].[UspGetGuildByName]
	 @pGuildNm		CHAR(12)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT a.[mGuildSeqNo], a.[mGuildNo], a.[mGuildMark], RTRIM(a.[mGuildMsg]), a.[mRegDate], 
		   b.[mGuildAssNo], a.[mRewardExp]
	FROM dbo.TblGuild AS a 
		LEFT OUTER JOIN dbo.TblGuildAssMem AS b 
		ON(a.mGuildNo = b.mGuildNo)
	WHERE a.[mGuildNm] = @pGuildNm

GO

