CREATE PROCEDURE [dbo].[WspRestoreDeletedPc]
(
	@pUserNo INT
	, @pPcNo	INT
	, @pPcNm	CHAR(12)
)
AS
/******************************************************************************
**		Name: WspRestoreDeletedChar
**		Desc: 삭제된 캐릭터 복구하기
**
**		Auth: 이진선
**		Date: 2012-09-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2014-03-25	이 진 선			삭제일로부터 7일이 경과할 경우 복구 불가 (정책) 제외 - 운영팀요청
*******************************************************************************/
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	-------------------------------------------------
	-- 변수 선언
	-------------------------------------------------
	--DECLARE @mDelDate DATETIME
	DECLARE @aSlot	TINYINT
	DECLARE @mSlot	TINYINT
	DECLARE @mRowCnt	INT
	DECLARE @mError	INT

	DECLARE @mTSlot TABLE
	(
		mSlot INT
	)

	INSERT INTO @mTSlot VALUES (0)
	INSERT INTO @mTSlot VALUES (1)
	INSERT INTO @mTSlot VALUES (2)

	-------------------------------------------------
	-- 변수 초기화
	-------------------------------------------------
	--SET @mDelDate = NULL
	SET @aSlot = NULL
	SET @mSlot = NULL
	SET @mRowCnt = 0
	SET @mError = 0

	-------------------------------------------------
	-- 1) 삭제된 캐릭터인지 체크
	-------------------------------------------------
	IF NOT EXISTS (SELECT * FROM dbo.TblPcDeleted WHERE mPcNo = @pPcNo AND mPcNm = @pPcNm)
	BEGIN
		RETURN (1);	--삭제된 캐릭터가 아닙니다.
	END

	/*
	-------------------------------------------------
	-- 삭제일로부터 7일이 경과할 경우 복구 불가 (정책)
	-------------------------------------------------
	SELECT @mDelDate = mDelDate
	FROM dbo.TblPc 
	WHERE mNo = @pPcNo AND mDelDate IS NOT NULL
	
	IF (DATEDIFF(DD,@mDelDate,GETDATE()) > 7)
	BEGIN
		RETURN (2);	--삭제일로부터 7일이 경과할 경우 복구 불가합니다.
	END
	*/

	-------------------------------------------------
	-- 캐릭터 개수 체크
	-------------------------------------------------
	IF (SELECT COUNT(*) FROM dbo.TblPc WHERE mOwner = @pUserNo AND mDelDate IS NULL) >= 3
	BEGIN
		RETURN (3);	--캐릭터 개수가 3개이상입니다.
	END

	-------------------------------------------------
	-- 캐릭터명 중복 여부 확인
	-------------------------------------------------
	IF EXISTS (SELECT * FROM dbo.TblPc WHERE mNm = @pPcNm)
	BEGIN
		RETURN (4);	--캐릭터명이 중복됩니다.
	END

	-------------------------------------------------
	-- 예전 슬롯이 비어 있으면, 예전 슬롯에 복구
	-------------------------------------------------
	SELECT @aSlot = mSlot 
	FROM dbo.TblPc
	WHERE mNo = @pPcNo AND mDelDate IS NOT NULL
	
	IF NOT EXISTS (SELECT * FROM TblPc WHERE mOwner = @pUserNo AND mSlot = @aSlot AND mDelDate IS NULL)
	BEGIN
		UPDATE dbo.TblPc SET mNm = @pPcNm, mDeldate = NULL WHERE mNo = @pPcNo

		SELECT @mRowCnt = @@ROWCOUNT, @mError = @@ERROR
		IF (@mRowCnt <= 0 OR @mError <> 0)
		BEGIN
			RETURN (5);	--기존 슬롯으로 캐릭터 정보 복구 실패
		END
		
		DELETE dbo.TblPcDeleted WHERE mPcNo = @pPcNo
		
		SELECT @mRowCnt = @@ROWCOUNT, @mError = @@ERROR
		IF (@mRowCnt <= 0 OR @mError <> 0)
		BEGIN
			RETURN (6);	--기존 슬롯으로 삭제정보 삭제 실패
		END
	END
	ELSE
	BEGIN
		-------------------------------------------------
		-- 빈 슬롯 체크
		-------------------------------------------------
		SET @mSlot = 
		(
			SELECT TOP 1 mSlot  
			FROM @mTSlot  
			WHERE mSlot NOT IN (  
				SELECT mSlot  
				FROM dbo.TblPc    
				WHERE mOwner = @pUserNo  
				AND mDelDate IS NULL )
		)
		IF @mSlot IS NULL OR @mSlot > 3
		BEGIN
			RETURN (7);	--빈 슬롯 체크 실패
		END

		UPDATE dbo.TblPc SET mNm = @pPcNm, mDeldate = NULL, mSlot = @mSlot  WHERE mNo = @pPcNo;

		SELECT @mRowCnt = @@ROWCOUNT, @mError = @@ERROR
		IF (@mRowCnt <= 0 OR @mError <> 0)
		BEGIN
			RETURN (8);	--새 슬롯으로 캐릭터 정보 복구 실패
		END

		DELETE dbo.TblPcDeleted WHERE mPcNo = @pPcNo
		
		SELECT @mRowCnt = @@ROWCOUNT, @mError = @@ERROR
		IF (@mRowCnt <= 0 OR @mError <> 0)
		BEGIN
			RETURN (9);	--새 슬롯으로 삭제정보 삭제 실패
		END
	END

	RETURN (0);
END

GO

