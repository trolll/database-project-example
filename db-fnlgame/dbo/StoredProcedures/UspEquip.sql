/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspEquip    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspEquip]
	 @pPcNo			INT		-- 1?? ????. ????.
	,@pSerial		BIGINT
	,@pWhere		INT			-- CPcEquip::ePosMax.
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
			
	DECLARE	@aCol		NVARCHAR(50)
	IF(0 = @pWhere)
		SET @aCol	= '[mWeapon]'
	ELSE IF(1 = @pWhere)
		SET @aCol	= '[mShield]'
	ELSE IF(2 = @pWhere)
		SET @aCol	= '[mArmor]'
	ELSE IF(3 = @pWhere)
		SET @aCol	= '[mRing1]'
	ELSE IF(4 = @pWhere)
		SET @aCol	= '[mRing2]'
	ELSE IF(5 = @pWhere)
		SET @aCol	= '[mAmulet]'
	ELSE IF(6 = @pWhere)
		SET @aCol	= '[mBoot]'
	ELSE IF(7 = @pWhere)
		SET @aCol	= '[mGlove]'
	ELSE IF(8 = @pWhere)
		SET @aCol	= '[mCap]'
	ELSE IF(9 = @pWhere)
		SET @aCol	= '[mBelt]'
	ELSE IF(10 = @pWhere)
		SET @aCol	= '[mCloak]'
	ELSE
	 BEGIN
		SET	@aErrNo	= 1
		GOTO LABEL_END
	 END
	
	DECLARE	@aSql		NVARCHAR(500)
	IF EXISTS(SELECT * FROM TblPcEquip WHERE (@pPcNo = [mOwner]))
	 BEGIN
		SET @aSql	= N'UPDATE dbo.TblPcEquip SET ' + @aCol + '=' + CAST(@pSerial AS NVARCHAR(20)) 
					+ ' WHERE ' + CAST(@pPcNo AS NVARCHAR(20)) + '=[mOwner]'
	 END
	ELSE
	 BEGIN
		SET @aSql	= N'INSERT dbo.TblPcEquip([mOwner],' + @aCol + ') VALUES(' 
					+ CAST(@pPcNo AS NVARCHAR(20)) + ',' + CAST(@pSerial AS NVARCHAR(20)) + ')'
	 END
	 
	EXEC @aErrNo=sp_executesql @aSql
LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

