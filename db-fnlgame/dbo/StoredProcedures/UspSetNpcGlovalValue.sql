/****** Object:  Stored Procedure dbo.UspSetNpcGlovalValue    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetNpcGlovalValue    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetNpcGlovalValue    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetNpcGlovalValue    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetNpcGlovalValue    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetNpcGlovalValue    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetNpcGlovalValue    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetNpcGlovalValue    Script Date: 2009-7-14 13:13:29 ******/
CREATE PROCEDURE [dbo].[UspSetNpcGlovalValue]
	@pId		INT,
	@pValue	INT
AS

SET NOCOUNT ON

	IF EXISTS(SELECT * FROM TblNpcGlobalValue WHERE @pId = [mNId])
	BEGIN
		UPDATE TblNpcGlobalValue 
		SET mValue = @pValue 
		WHERE @pId = [mNId]

		RETURN(0)
	END

	RETURN(1)

GO

