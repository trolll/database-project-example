--------------------------------
-- ░┤├╝╕φ : [WspGetGuildAsset]
-- ╝│╕φ : ▒µ╡σ╕φ└╕╖╬ ╟÷└τ ▒µ╡σ░í ╝╥└»╟╤ ┐╡┴÷└╟ └▄░φ╕ª ╛≥╛ε┐┬┤┘.
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.07.12
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
-- ╝÷┴ñ│╗┐δ : 
--------------------------------
CREATE PROCEDURE [dbo].[WspGetGuildAsset]
	@mGuildNm VARCHAR(12) -- ▒µ╡σ └╠╕º
AS
	SET NOCOUNT ON

	SELECT
		p.mAsset
	FROM
		[dbo].[TblCastleTowerStone] AS p		WITH ( NOLOCK )
	INNER	JOIN
		[dbo].[TblGuild]	AS g				WITH ( NOLOCK )
	ON	
		g.mGuildNo = p.mGuildNo
	WHERE
		g.mGuildNm = @mGuildNm

	SET NOCOUNT OFF

GO

