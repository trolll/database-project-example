/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspGetGkill    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspGetGkill]
	 @pGuildNo	INT
	,@pPlace	INT		-- EPlace.
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	-- ?????? guild??? ?? ??.
	IF(0 = @pPlace)
	BEGIN
		SELECT a.[mGuildNo], a.[mNode], a.[mPcNo], a.[mRegDate], b.[mNm] 
		FROM dbo.TblGkill AS a 
				INNER JOIN dbo.TblPc AS b 
		ON(a.[mPcNo] = b.[mNo]) 
		WHERE (a.[mGuildNo]=@pGuildNo) 
			AND (a.[mPlace]=@pPlace)
	 
	END
	ELSE
	 BEGIN
		SELECT a.[mGuildNo], a.[mNode], a.[mPcNo], a.[mRegDate], b.[mNm] 
		FROM dbo.TblGkill AS a 			INNER JOIN dbo.TblPc AS b 
		ON(a.[mPcNo] = b.[mNo]) 
		WHERE a.[mPlace] = @pPlace
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

