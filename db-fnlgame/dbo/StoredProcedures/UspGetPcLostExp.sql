/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspGetPcLostExp    Script Date: 2008-11-10 10:37:19 ******/






-- CREATE TABLE TblPcGoldItemEffect
-- (
-- 	  mRegDate	DATETIME		NOT NULL		DEFAULT(GETDATE())	-- ????
-- 	, mPcNo		INT		NOT NULL					-- ?? PC??
-- 	, mItemType	INT		NOT NULL					-- PC? ????? ??
-- 	, mParmA		FLOAT		NOT NULL		DEFAULT(0)		-- ?????? ???? ???
-- 	, mEndDate	SMALLDATETIME	NOT NULL 				-- ?? ??
-- )
-- GO

-- mPcNo ? ??? ????
-- CREATE CLUSTERED INDEX CL_TblPcGoldItemEffect_1	
-- 	ON	dbo.TblPcGoldItemEffect(mPcNo) WITH FILLFACTOR = 80
-- GO
-- 
-- CREATE TABLE dbo.TblGuildGoldItemEffect
-- (
-- 	  mRegDate	DATETIME		NOT NULL		DEFAULT(GETDATE())	-- ?? ??
-- 	, mGuildNo	INT		NOT NULL							-- ?? ?? ??
-- 	, mItemType	INT		NOT NULL							-- ?? ????? ??
-- 	, mParmA	FLOAT		NOT NULL		DEFAULT(0)		-- ???? ???? ???
-- 	, mEndDate	SMALLDATETIME	NOT NULL 					-- ?? ??
-- )
-- GO
-- 
-- -- mGuildNo ? ??? ????
-- CREATE CLUSTERED INDEX CL_TblGuildGoldItemEffect_1	
-- 	ON	dbo.TblGuildGoldItemEffect(mGuildNo) WITH FILLFACTOR = 80
-- GO	
CREATE PROCEDURE [dbo].[UspGetPcLostExp]
	  @pPcNo		INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT T1.mLostExp 
	FROM dbo.TblPcState T1
		INNER JOIN dbo.TblPc  T2 
			ON T1.mNo = T2.mNo 
				AND T2.mDelDate IS NULL
	WHERE T1.mNo = @pPcNo

GO

