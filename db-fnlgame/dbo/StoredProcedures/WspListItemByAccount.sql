/******************************************************************************  
**  Name: WspListItemByAccount  
**  Desc: 계정 소유 아이템 조회  
**  
**  Auth: 김석천  
**  Date: 20090428  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  20120228 이진선    확인 여부 | 룬 장착 가능여부 | 슬롯갯수 조회 추가
**	20130402 이진선	   무기 및 방어구 아이템 외에, 악세서리 아이템도 룬 장착가능하도록 변경
						(1 ,2 ,3 ,6 ,7 ,8 ,17 ,18 ,20)
						-> (1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 20)
*******************************************************************************/  
CREATE PROCEDURE [dbo].[WspListItemByAccount]
(  
	@mUserId VARCHAR(20) -- 계정명
)
AS
BEGIN
	SET NOCOUNT ON  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   
	SET ROWCOUNT 1000  -- 최대 가져올 수 있는 아이템 수를 제한

	DECLARE  @aUserNo INT
			, @aErrNo  INT
			, @aRowCnt INT  

	SELECT @aUserNo = 0 , @aErrNo = 0, @aRowCnt = 0  

	-------------------------------------------------------  
	-- 계정 존재여부 체크  
	-------------------------------------------------------  
	SELECT   
		@aUserNo = mUserNo  
	FROM FNLAccount.dbo.TblUser  
	WHERE mUserId = @mUserId  
	AND mDelDate = '1900-01-01'  

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  
	IF @aErrNo <> 0 OR @aRowCnt <= 0   
	BEGIN  
		RETURN(1) -- 계정이 존재하지 않는다.   
	END   

	-------------------------------------------------------  
	-- 아이템 정보를 반환  
	-------------------------------------------------------  
	SELECT   
		T1.mNm  
		,(  
			SELECT   
			mSlot  
			FROM dbo.TblPcEquip  
			WHERE mSerialNo = T1.mSerialNo    
		) AS mSlot  
		,T2.IName    
		,T1.mEndDate  
		,T1.mSerialNo  
		,T1.mCnt  
		,T1.mItemNo  
		,T1.mIsSeizure  
		,T1.mRegDate  
		,T1.mStatus  
		,T1.mPracticalPeriod  
		,T1.mBindingType  
		,T1.mRestoreCnt  
		,T1.mIsConfirm --확인 여부 (1은 확인, 0은 미확인)  
		,(  
			SELECT COUNT(*)  
			FROM FNLParm.dbo.DT_Item  
			WHERE IID = T1.mItemNo  
			AND IType IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 20)
			AND T1.mIsConfirm = 1  
			AND T1.mIsSeizure = 0
		) AS mIsEquipBead --룬 장착 가능여부  
	FROM (  
	SELECT   
		T1.mNm  
		,T1.mNo  
		,T2.mStatus  
		,DATEDIFF(DAY, GETDATE(), T2.mEndDate) AS mEndDate -- 유효기간  
		,T2.mSerialNo  
		,T2.mCnt  
		,T2.mItemNo  
		,T2.mIsSeizure  
		,T2.mRegDate -- 습득일자  
		,T2.mPracticalPeriod  
		,T2.mBindingType  
		,T2.mRestoreCnt  
		,T2.mIsConfirm --확인 여부 (1은 확인, 0은 미확인)   
	FROM dbo.TblPc T1 WITH(INDEX=IxTblPcOwner)  
		INNER JOIN dbo.TblPcInventory  T2 WITH(INDEX=CL_TblPcInventory)  
	ON T1.mNo = T2.mPcNo  
	AND T2.mItemNo NOT IN (261)  
	AND T1.mDelDate IS NULL          
	WHERE T1.mOwner = @aUserNo  
	) T1   
	INNER JOIN FNLParm.dbo.DT_Item AS T2  
	ON T1.mItemNo = T2.IID  
	ORDER BY T1.mNo ASC
END

GO

