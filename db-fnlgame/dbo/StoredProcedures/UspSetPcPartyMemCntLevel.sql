/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2009-5-12 9:18:19 ******/

/****** Object:  Stored Procedure dbo.UspSetPcPartyMemCntLevel    Script Date: 2008-11-10 10:37:20 ******/





CREATE PROCEDURE [dbo].[UspSetPcPartyMemCntLevel]
	 @pPcNo		INT
	,@pPartyMemCntLevel	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	UPDATE dbo.TblPcState
	SET mPartyMemCntLevel = @pPartyMemCntLevel
	WHERE [mNo]=@pPcNo
	
	SET NOCOUNT OFF

GO

