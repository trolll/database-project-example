/******************************************************************************
**		Name: UspGetStorePassword
**		Desc: 俺牢芒绊 菩胶况靛 啊廉坷扁
**
**		Auth: 傍籍痹
**		Date: 2016-11-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetStorePassword]
	 @pUserNo	INT
		
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT RTRIM(mPassword) FROM dbo.TblStorePassword WHERE mUserNo = @pUserNo

GO

