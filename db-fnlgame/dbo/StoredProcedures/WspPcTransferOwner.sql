CREATE PROCEDURE [dbo].[WspPcTransferOwner]
	@pPcNo	INT
	, @pPcNm	CHAR(12)
	, @pUserID	VARCHAR(20)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
	
	DECLARE	@aErrNo		INT,
			@aRowCnt	INT,
			@aOwnerNo	INT,
			@aSlot		TINYINT	
			
	SELECT @aOwnerNo = 0, @aErrNo = 0, @aRowCnt = 0, @aSlot = NULL
		
	SELECT
		TOP 1
		@aOwnerNo = mUserNo
	FROM FNLAccount.dbo.TblUser
	WHERE mUserID = @pUserID
		AND mDelDate = '1900-01-01' 						
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(1)	-- not exists account
	END 
	
	IF (	SELECT COUNT(*)
			FROM dbo.TblPc
			WHERE mOwner = @aOwnerNo
				AND mDelDate IS NULL ) >= 3
	BEGIN
		RETURN(2)	-- јТАЇ °иБ¤ Б¤єё ГК°ъ 
	END 
	
	-- Slot Number
	SELECT 
		TOP 1
		@aSlot = T2.mSlot	
	FROM (
		SELECT mNo, mSlot
		FROM dbo.TblPc
		WHERE mOwner = @aOwnerNo
			AND mDelDate IS NULL) T1
		RIGHT OUTER JOIN 
		(
			SELECT 0 mSlot
			UNION ALL
			SELECT 1 mSlot
			UNION ALL
			SELECT 2 mSlot
		) T2	
		ON T1.mSlot = T2.mSlot
	WHERE T1.mSlot IS NULL
	ORDER BY T2.mSlot ASC
	
	IF @aSlot IS NULL
		RETURN(2)	-- єу ЅЅ·ФАМ ѕшґЩ.
		
	UPDATE dbo.TblPc
	SET 
		mOwner = @aOwnerNo
		, mSlot = @aSlot
	WHERE mNo = @pPcNo
		AND mDelDate IS NULL
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(3)	-- db update error
	END 	
	
	RETURN(0)

GO

