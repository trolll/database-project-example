/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspUpdateQuest    Script Date: 2008-11-10 10:37:18 ******/


CREATE PROCEDURE [dbo].[UspUpdateQuest]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pValue		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	DECLARE @aValue		INT
	SET		@aErrNo = 0
	
	SELECT @aValue=[mValue]
	FROM dbo.TblPcQuest 
	WHERE ([mPcNo]=@pPcNo) 
		AND ([mQuestNo]=@pQuestNo)	-- 1?? ??? ???.
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)
	END
			
	IF(@aValue IS NULL)
	BEGIN
	
		IF (@pValue=99)	-- Quest Start, End
		BEGIN
			INSERT INTO dbo.TblPcQuest([mPcNo], [mQuestNo], [mValue],[mBeginDate],[mEndDate])
				VALUES(@pPcNo, @pQuestNo, @pValue, GETDATE(), GETDATE())
				
			IF(0 <> @@ERROR)
			BEGIN
				RETURN(2)
			END
			
			RETURN(0)
		END
		
		
		IF (@pValue <> 0)
		BEGIN		
			INSERT INTO dbo.TblPcQuest([mPcNo], [mQuestNo], [mValue])
				VALUES(@pPcNo, @pQuestNo, @pValue)
				
			IF(0 <> @@ERROR)
			BEGIN
				RETURN(2)
			END	
			
			RETURN(0)	
		END
	
		RETURN(0)
	END


	IF(@pValue = 0)
	BEGIN
		DELETE dbo.TblPcQuest 
		WHERE [mPcNo]=@pPcNo 
				AND [mQuestNo]=@pQuestNo
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(2)
		END
		
		RETURN(0)
	END

	-- Value change check 
	IF(@aValue <> @pValue)
	BEGIN
		
		IF ( @pValue = 99 )	-- Quest ?? ??? ??? ???? ?? ??. 
		BEGIN
		
			UPDATE dbo.TblPcQuest 
			SET [mValue]=@pValue
				, mEndDate = GETDATE()	  
			WHERE [mPcNo]=@pPcNo 
				AND [mQuestNo]=@pQuestNo
			
			IF(0 <> @@ERROR)
			BEGIN
				RETURN(3)
			END		
						
			RETURN(0)
		END 
	
		UPDATE dbo.TblPcQuest 
		SET [mValue]=@pValue 
		WHERE [mPcNo]=@pPcNo 
			AND [mQuestNo]=@pQuestNo
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(3)
		END
		 		 
		RETURN(0)
	END

	RETURN(0)

GO

