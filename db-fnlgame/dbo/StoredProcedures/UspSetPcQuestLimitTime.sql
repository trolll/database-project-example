/******************************************************************************
**		Name: UspSetPcQuestLimitTime
**		Desc: 韤橃姢韸?鞁滉皠鞝滍暅 霌彪
**
**		Auth: 鞝曥順?
**		Date: 2010-02-12
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetPcQuestLimitTime]
     @pPcNo			INT
	,@pQuestNo		INT	 
    ,@pLimitTime    INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    DECLARE	@aLimitTime		SMALLDATETIME
    SET @aLimitTime = DATEADD(ss, @pLimitTime, GETDATE())

    UPDATE dbo.TblPcQuest 
	SET mLimitTime = @aLimitTime
	WHERE [mPcNo]=@pPcNo 
		AND [mQuestNo]=@pQuestNo
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)
	END
	 		 
	RETURN(0)

GO

