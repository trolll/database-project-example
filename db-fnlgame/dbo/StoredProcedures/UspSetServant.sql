/******************************************************************************
**		Name: UspSetServant
**		Desc: ¼­¹øÆ® Á¤º¸ ÀúÀå
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetServant]
	@pPcNo			INT
	,@pSerialNo		BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNo		INT
	DECLARE @aRowCnt	INT

	IF EXISTS (SELECT mSerialNo 
				FROM dbo.TblPcServant 
				WHERE mSerialNo = @pSerialNo)
	BEGIN
		UPDATE dbo.TblPcServant 
		SET mPcNo = @pPcNo 
		WHERE mSerialNo = @pSerialNo
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblPcServant (mPcNo, mSerialNo)
		VALUES (@pPcNo, @pSerialNo)
	END

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR 	@aRowCnt = 0
	BEGIN
		RETURN(@aErrNo)
	END

	SET NOCOUNT OFF;

GO

