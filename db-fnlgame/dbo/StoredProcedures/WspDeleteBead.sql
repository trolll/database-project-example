/******************************************************************************
**		Name: WspDeleteBead
**		Desc: ·й Б¦°Е
**
**		Auth: АМБшј±
**		Date: 20120305
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspDeleteBead]
(
	@pEquipSerialNo BIGINT
	, @pBeadIndex TINYINT
	, @pItemNo INT OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;

	SELECT @pItemNo = mItemNo
	FROM dbo.TblPcBead
	WHERE mOwnerSerialNo=@pEquipSerialNo and mBeadIndex=@pBeadIndex;

	DELETE FROM dbo.TblPcBead
	WHERE mOwnerSerialNo=@pEquipSerialNo and mBeadIndex=@pBeadIndex;

	IF ( @@ERROR <> 0 OR @@ROWCOUNT = 0 )
	BEGIN
		RETURN 1; -- ·йА» Б¦°ЕЗТ јц ѕшЅАґПґЩ.
	END
	
	RETURN 0;
END

GO

