/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspGetPcSimple    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspGetPcSimple]
	 @pPcNo		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT a.[mSlot], RTRIM(a.[mNm]) AS mNm, a.[mClass], a.[mSex], a.[mHead], a.[mFace], a.[mBody], 
		   b.[mLevel], b.[mHpAdd], b.[mHp], b.[mMpAdd], b.[mMp], b.[mExp], b.[mStomach],
		   c.[mGuildNo], RTRIM(c.[mNickNm]), c.[mGuildGrade], b.[mChaotic], b.[mPosX], b.[mPosY], b.[mPosZ], 
		   ISNULL(d.[mMaster], 0) As mDiscipleNo, ISNULL(d.[mType], 0) As mDiscipleType
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
			ON(a.[mNo] = b.[mNo])
		LEFT JOIN dbo.TblGuildMember AS c 
			ON(a.[mNo] = c.[mPcNo])
		LEFT JOIN dbo.TblDiscipleMember AS d
			ON(a.[mNo] = d.[mDisciple])
	WHERE (a.[mNo]=@pPcNo) 
			AND ([mDelDate] IS NULL)
	ORDER BY a.[mSlot]

	SET NOCOUNT OFF

GO

