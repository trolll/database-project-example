/******************************************************************************
**		Name: UspPushPcCalendarGroup
**		Desc: ±×·ì ¸¸µé±â ¹× ±×·ì¸í º¯°æ
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-17
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPushPcCalendarGroup]
	 @pOwnerPcNo		INT 
	,@pGroupNo			TINYINT
	,@pGroupNm			VARCHAR(12)
	,@pMaxCnt			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE  @aErr		INT
			,@aRowCnt	INT
			,@aCnt		INT
			
	SELECT	 @aErr		= 0
			,@aRowCnt	= 0
			,@aCnt		= 0
	
	UPDATE dbo.TblCalendarGroup
	SET	
		 mRegDate	= GETDATE()
		,mGroupNm	= @pGroupNm
	WHERE mOwnerPcNo = @pOwnerPcNo AND mGroupNo = @pGroupNo

	SELECT  @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT;
	IF (@aErr <> 0)
	BEGIN
		RETURN(1);
	END

	IF @aRowCnt = 0 
	BEGIN
		SELECT @aCnt = count(*)
		FROM dbo.TblCalendarGroup
		WHERE mOwnerPcNo = @pOwnerPcNo

		IF @pMaxCnt <= @aCnt
		BEGIN
			RETURN(2);
		END

		INSERT INTO dbo.TblCalendarGroup(mRegDate, mOwnerPcNo, mGroupNo, mGroupNm)
		VALUES(GETDATE(), @pOwnerPcNo, @pGroupNo, @pGroupNm)

		SELECT  @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT;
		IF (@aErr <> 0) OR (@aRowCnt <= 0)
		BEGIN
			RETURN(1);
		END
	END

	RETURN(0);

GO

