/****** Object:  Stored Procedure dbo.UspListPcInfo    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspListPcInfo    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspListPcInfo    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspListPcInfo    Script Date: 2010-12-23 17:46:03 ******/
CREATE PROCEDURE [dbo].[UspListPcInfo]
	 @pUserNo	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT 
		a.mNo,			-- 某腐磐 锅龋
		a.mNm,			-- 某腐磐 捞抚
		b.mLevel,		-- 某腐磐 饭骇
		a.mRegDate,		-- 积己老 		
		c.mGuildGrade,
		d.mGuildNm,
		b.mTotUseTm,
		a.mClass
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
			ON a.mNo = b.mNo
		LEFT OUTER JOIN dbo.TblGuildMember AS c
			ON a.mNo = c.mPcNo	
		LEFT OUTER JOIN dbo.TblGuild AS d
			ON c.mGuildNo = d.mGuildNo
	WHERE a.mOwner = @pUserNo
			AND mDelDate IS NULL

GO

