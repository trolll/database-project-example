/******************************************************************************  
**  Name: UspDeleteBeadOfItem
**  Desc: 아이템의 구슬 전부 제거
**  
**                
**  Return values:  
**   0 : 성공
**   <>0 : 실패
**                
**  Author: 김강호
**  Date: 2011-06-14
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDeleteBeadOfItem]
		@pEquipSerialNo	BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	delete from dbo.TblPcBead
	where mOwnerSerialNo=@pEquipSerialNo
	if @@ERROR <> 0
	BEGIN
		return 1
	END
	
	RETURN 0;

GO

