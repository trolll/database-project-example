--------------------------------
-- ░┤├╝╕φ : WspUpdateCharLevel
-- ╝│╕φ : ─│╕»┼═ ╖╣║º└╗ ║»░µ╟╤┤┘
-- └█╝║└┌ : └╠┐∞╝«
-- └█╝║└╧ : 2006.04.09
-- ╝÷┴ñ└┌ : ▒Φ╝║└τ
-- ╝÷┴ñ└╧ : 2006.06.06
-- ╝÷┴ñ│╗┐δ : ╞«╖ú└Φ╝╟ ╗τ┐δ
--------------------------------
CREATE PROCEDURE [dbo].[WspUpdateCharLevel]	
	@mLevel SMALLINT, -- ╖╣║º
	@mNm CHAR(12) -- ─│╕»┼═╕φ
AS
	SET NOCOUNT ON
	
	BEGIN TRANSACTION

	UPDATE
		[dbo].[TblPcState]
	SET
		mLevel = @mLevel 
		, mExp = 0
	WHERE		
		mNo = (SELECT mNo From [dbo].[TblPc] WHERE mNm = @mNm)			

	IF @@ERROR = 0
	BEGIN
		COMMIT TRANSACTION
		RETURN 0
	END
	ELSE
	BEGIN
		ROLLBACK TRANSACTION
		RETURN @@ERROR
	END

GO

