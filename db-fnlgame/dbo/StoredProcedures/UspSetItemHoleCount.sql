/******************************************************************************  
**  Name: UspSetItemHoleCount
**  Desc: 아이템에 소켓 개수 설정
**  
**                
**  Return values:  
**   0 : 성공
**   <>0 : 실패
**                
**  Author: 김강호
**  Date: 2011-06-14
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetItemHoleCount]
		@pSerialNo	BIGINT
		,@pNewCount	TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aHoleCount	TINYINT;
	
	SELECT @aHoleCount=mHoleCount FROM TblPcInventory
	WHERE mSerialNo = @pSerialNo;
	IF @@ERROR <> 0 OR @aHoleCount IS NULL
	BEGIN
		RETURN 1;	-- 아이템이 존재하지 않습니다.
	END
	
	-- O: 0 -> 0, 0 -> 양수, 양수 -> 0
	-- X: 양수 -> 양수
	IF @aHoleCount<>0 AND @pNewCount<>0
	BEGIN
		RETURN 2;	-- 슬롯개수가 초기화되지 않았습니다.
	END
	
	UPDATE 	TblPcInventory
	SET mHoleCount=@pNewCount
	WHERE mSerialNo = @pSerialNo AND mHoleCount=@aHoleCount
	IF @@ERROR <> 0 OR @@ROWCOUNT= 0
	BEGIN
		RETURN 3;	-- 소켓 개수를 설정할 수 없습니다.
	END
	
	RETURN 0;

GO

