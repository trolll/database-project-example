/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsItemByCase    Script Date: 2008-11-10 10:37:18 ******/



CREATE PROCEDURE [dbo].[UspSetStatisticsItemByCase]
	  @pItemNo		INT
	, @pMerchantCreate		BIGINT
	, @pMerchantDelete		BIGINT
	, @pReinforceCreate	BIGINT
	, @pReinforceDelete		BIGINT
	, @pCraftingCreate		BIGINT
	, @pCraftingDelete		BIGINT
	, @pPcUseDelete		BIGINT
	, @pNpcUseDelete		BIGINT
	, @pNpcCreate		BIGINT
	, @pMonsterDrop		BIGINT
	, @pGSExchangeCreate	BIGINT
	, @pGSExchangeDelete	BIGINT
AS
	SET NOCOUNT ON;
	
	DECLARE		@aErrNo		INT;
	SET @aErrNo = 0;
	
	INSERT INTO dbo.TblStatisticsItemByCase 
		([mItemNo], [mMerchantCreate], [mMerchantDelete], [mReinforceCreate], [mReinforceDelete], [mCraftingCreate], [mCraftingDelete], 
		[mPcUseDelete], [mNpcUseDelete], [mNpcCreate], [mMonsterDrop], [mGSExchangeCreate], [mGSExchangeDelete])
	VALUES
		(@pItemNo, @pMerchantCreate, @pMerchantDelete, @pReinforceCreate, @pReinforceDelete, @pCraftingCreate, 						@pCraftingDelete, @pPcUseDelete, @pNpcUseDelete, @pNpcCreate, @pMonsterDrop, @pGSExchangeCreate, @pGSExchangeDelete);

							
	IF @@ERROR <> 0
		SET @aErrNo = 2;
														
	RETURN(@aErrNo);

GO

