/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildAccountDraw    Script Date: 2008-11-10 10:37:18 ******/





CREATE Procedure [dbo].[UspSetGuildAccountDraw]	-- ????? ? ??
	 @pGID			INT			-- ????
	,@pGuildMoney		BIGINT OUTPUT	-- ???? [??/??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF EXISTS(SELECT mGID FROM TblGuildAccount WITH(NOLOCK) WHERE mGID = @pGID)
	BEGIN
		-- ????? ???

		DECLARE @aGuildMoney	BIGINT
		SELECT @aGuildMoney = mGuildMoney FROM TblGuildAccount WITH(NOLOCK) WHERE mGID = @pGID
		IF (@aGuildMoney >= @pGuildMoney)
		BEGIN
			-- ????? ??? ?? ??

			BEGIN TRAN
	
			UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney - @pGuildMoney WHERE mGID = @pGID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB ???? ??
				GOTO LABEL_END			 
			END	 

			SELECT @pGuildMoney = mGuildMoney FROM TblGuildAccount WHERE mGID = @pGID
		END
		ELSE
		BEGIN
			-- ????? ????

			SET @aErrNo = 2		-- 2 : ???? ????
			GOTO LABEL_END_LAST			 
		END
	END
	ELSE
	BEGIN
		-- ????? ???? ??

		BEGIN TRAN

		INSERT TblGuildAccount (mGID, mGuildMoney) VALUES (@pGID, 0)
		IF(0 <> @@ERROR)
		BEGIN
			ROLLBACK TRAN
			SET @aErrNo = 1		-- 1 : DB ???? ??
			GOTO LABEL_END_LAST
		END

		COMMIT TRAN
		SET @aErrNo = 3			-- 3 : ????? ???? ??
		GOTO LABEL_END_LAST			 
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

