CREATE PROCEDURE [dbo].[WspGetTransferGuild]
	@pGuildNm CHAR(12)
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		mSvrNo
		,mIsOverlap
		,mOLDGuildNm
		,mTrasnsferLv
		,T1.mRegDate
		,mGuildNo
		,mGuildNm
		,ISNULL(mGuildMoney, 0) AS mGuildMoney
	FROM dbo.T_TblGuild T1
		LEFT JOIN dbo.T_TblGuildAccount T2
			ON T1.mGuildNo = T2.mGID
	WHERE mOLDGuildNm = @pGuildNm

GO

