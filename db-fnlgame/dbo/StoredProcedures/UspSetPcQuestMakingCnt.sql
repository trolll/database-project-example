/******************************************************************************
**		Name: UspSetPcQuestMakingCnt
**		Desc: 柳青吝牢 皋捞欧 涅胶飘 荐甫 悸泼茄促.
**
**		Auth: 沥柳龋
**		Date: 2010-04-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspSetPcQuestMakingCnt]
	  @pPcNo			INT
	 ,@pNormalQCnt		INT
	 ,@pGuildQCnt		INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblPcState
	SET mQMCnt = @pNormalQCnt,
		mGuildQMCnt = @pGuildQCnt				
	WHERE mNo = @pPcNo
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1);
	END

	RETURN(0);

GO

