/******************************************************************************
**		Name: UspDeletePcQuestExpireEvent
**		Desc: 종료된 이벤트 퀘스트를 삭제
**
**		Auth: 정진호
**		Date: 2010-06-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeletePcQuestExpireEvent]
	@pEventQuestNoStr			VARCHAR(3000)
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;

	DECLARE @aQuest			TABLE (
				mQuestNo		INT	 PRIMARY KEY      	)			


	-- 데이터가 없을경우는?
	INSERT INTO @aQuest(mQuestNo)
	SELECT CONVERT(INT, element) mObjID
	FROM  dbo.fn_SplitTSQL(  @pEventQuestNoStr, ',' )
	WHERE LEN(RTRIM(element)) > 0


	BEGIN TRAN

		DELETE T2
		FROM dbo.TblPcQuest T1
			INNER JOIN dbo.TblPcQuestCondition T2
				ON T1.mQuestNo = T2.mQuestNo
					AND T1.mPcNo = T2.mPcNo
		WHERE T1.mQuestNo IN ( 
				SELECT *
				FROM @aQuest
			)


		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		DELETE 	dbo.TblPcQuest
		WHERE mQuestNo IN ( 
			SELECT *
			FROM @aQuest
		) 


		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(2);			
		END 

	COMMIT TRAN

	RETURN(0)

GO

