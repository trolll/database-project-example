/******************************************************************************
**		Name: UspUpdatePcQuestCondition
**		Desc: Ä³¸¯ÅÍ Äù½ºÆ® Á¶°Ç ¾÷µ¥ÀÌÆ®
**
**		Auth: Á¤ÁøÈ£
**		Date: 2010-02-04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2011-10-10	Á¤ÁøÈ£				»ç³ÉÇÑ ¸ó½ºÅÍ ¼ö¸¦ ¹ÝÈ¯ÇÏµµ·Ï º¯°æ
**		2013-03-21	Á¤ÁøÈ£				Àâ¾Æ¾ßÇÏ´Â ¸ó½ºÅÍ ¼ö¸¦ ¹ÝÈ¯ÇÏµµ·Ï º¯°æ
**		2014-02-26	Á¤Áø¿í				Äù½ºÆ® ¸ó½ºÅÍ Ä«¿îÆÃ Áõ°¡ ÄÁÅÙÃ÷ ¿É¼Ç Ãß°¡
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePcQuestCondition]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pMonNo   		INT
	,@aResultCnt	INT OUTPUT	-- »ç³ÉÇÑ ¸ó½ºÅÍ ¼ö
	,@aMaxCnt		INT OUTPUT	-- Àâ¾Æ¾ßÇÏ´Â ¸ó½ºÅÍ ¼ö
	,@aQuestMonCountingInc	INT	-- Äù½ºÆ® ¸ó½ºÅÍ Ä«¿îÆÃ Áõ°¡ ÄÁÅÙÃ÷ ¿É¼Ç
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    UPDATE dbo.TblPcQuestCondition
	SET  @aResultCnt = mCnt = mCnt + 1 + @aQuestMonCountingInc	-- 1¾¿ Áõ°¡ + Áõ°¡¿É¼Ç°ª
		,@aMaxCnt = mMaxCnt
	WHERE mPcNo = @pPcNo 
			AND mQuestNo = @pQuestNo
            AND mMonsterNo = @pMonNo
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(2);
	END
	 		 
	RETURN(0);

GO

