/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspUpdateItemNo    Script Date: 2008-11-10 10:37:18 ******/





CREATE  PROCEDURE [dbo].[UspUpdateItemNo]
	 @pPcNo			INT
	,@pSerial		BIGINT
	,@pItemNo		INT 
	,@pNewItemNo	INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	
	DECLARE	@aErrNo		INT,
				@aRowCnt	INT,
				@aItemNo	INT 
	
	SELECT	@aErrNo = 0, @aRowCnt = 0, @aItemNo = 0

	SELECT 
		@aItemNo = mItemNo 
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pSerial
			AND mPcNo =@pPcNo

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END	 
	END

	IF @aItemNo <> @pItemNo
	BEGIN
		UPDATE dbo.TblPcInventory 
		SET 
			mItemNo = @pItemNo
		WHERE  mPcNo = @pPcNo
					AND mSerialNo = @pSerial

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET  @aErrNo = 2
			GOTO T_END	 
		END	
		SET @pNewItemNo = @pItemNo
	END
	ELSE
	BEGIN
		SET @pNewItemNo = @aItemNo
	END

T_END:			
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

