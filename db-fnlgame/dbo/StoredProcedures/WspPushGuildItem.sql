CREATE PROCEDURE [dbo].[WspPushGuildItem]  
	@pGuildNo INT, -- ±жµе №шИЈ
	@pItemNo INT, -- ѕЖАМЕЫ №шИЈ (TID)
	@pStatus TINYINT, -- ѕЖАМЕЫ »уЕВ
	@pCnt INT, -- ѕЖАМЕЫ јц·®
	@pIsConfirm BIT, -- И®АОї©єО
	@pGuildGrade	TINYINT = 0,	-- Гў°н ·№є§ 1·О µо·П
	@pTargetSn BIGINT OUTPUT,
	@pValidDay INT = NULL, -- АЇИї±в°Ј	
	@pPraticalHour INT = NULL,
	@pBindingType	TINYINT = 0		-- BIDING TYPE OFF
AS
	SET NOCOUNT ON
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
		 	
	DECLARE @pCntUse TINYINT
	DECLARE @pIsStack BIT,
			@pIsPracticalPeriod BIT

	SELECT @pTargetSn = 0, @pIsPracticalPeriod = 0
	

	IF @pValidDay IS NULL
		SELECT
			@pValidDay = ITermOfValidity
			, @pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
		FROM
			[FNLParm].[dbo].[DT_Item]
		WHERE
			IID = @pItemNo
	ELSE
		SELECT
			@pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
		FROM
			[FNLParm].[dbo].[DT_Item]			
		WHERE
			IID = @pItemNo
	

	-- ѕшґВ TID
	IF @pValidDay IS NULL OR @pIsStack IS NULL
		RETURN (2)
		 	
	DECLARE	@aTargetSn		BIGINT	
	DECLARE	@aCntTarget		INT
	SET		@aCntTarget = 0		
	DECLARE	@aIsSeizure		BIT	
	
	-- µ·.
	IF(@pItemNo = 409)
	 BEGIN
		SET		@pIsConfirm = 1
		SET		@pStatus = 1
	 END
	
	-- ёё·бАП 
	DECLARE	@aEndDay	SMALLDATETIME
	SET		@aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)
		
	-- Иї°ъ БцјУ ЅГ°Ј
	IF @pIsPracticalPeriod = 1 AND ( @pPraticalHour IS NULL OR @pPraticalHour <= 0)
		RETURN 3
	
	IF @pPraticalHour > (30*24) 
		RETURN 4
		
	IF @pPraticalHour IS NULL
		SET @pPraticalHour = 0
			
	
	IF @pIsStack = 1	
	BEGIN
		-- ЅєЕГЗьАЗ °жїм ґ©АыЗТ ѕЖАМЕЫА» ГЈґВґЩ.
		SELECT 
			@aCntTarget=ISNULL(mCnt,0), 
			@aTargetSn=mSerialNo, 
			@aIsSeizure=ISNULL(mIsSeizure,0)
		FROM dbo.TblGuildStore
		WHERE mGuildNo = @pGuildNo
				AND mItemNo = @pItemNo
				AND mIsConfirm = @pIsConfirm
				AND mStatus = @pStatus

		IF(0 <> @@ERROR)
		BEGIN
			RETURN(4)
		END
			
		IF(0 <> @aIsSeizure)
		BEGIN
			RETURN(5)	 
		END	
	END
	
	--------------------------------------------------------------
	-- ѕЖАМЕЫА» »эјєЗП°ЕіЄ ґ©АыЗСґЩ. 
	--------------------------------------------------------------
	IF @aTargetSn > 0
	BEGIN
		-- ґ©АыЗТ ґл»у БёАз 
		SET @pTargetSn = @aTargetSn
		
		UPDATE dbo.TblGuildStore 
		SET 
			mCnt = mCnt + @pCnt
		WHERE mSerialNo = @aTargetSn
				AND mItemNo = @pItemNo
		IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
		BEGIN
			RETURN(6)
		END		
				
		RETURN(0)
	END 
	
	EXEC @pTargetSn = dbo.UspGetItemSerial 
	IF @pTargetSn <= 0 
	BEGIN
		RETURN(7)
	END 	
			
	-- Гў°нАЗ ЅЕ±Ф ѕЖАМЕЫ µо·П 
	INSERT INTO dbo.TblGuildStore (mSerialNo, mGuildNo, mItemNo, mEndDate, mIsConfirm, 
		mStatus, mCnt, mCntUse, mGrade, mOwner, mPracticalPeriod, mBindingType )
	VALUES(
		@pTargetSn,
		@pGuildNo, 
		@pItemNo, 
		@aEndDay, 
		@pIsConfirm, 
		@pStatus, 
		@pCnt, 
		0	-- UsedCount (»эјєАє 0Аё·О јјЖГ ЗСґЩ)
		, @pGuildGrade
		, 0
		, @pPraticalHour
		, @pBindingType
	)	
	
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(8)
	END
	
	RETURN(0)

GO

