/******************************************************************************  
**  Name: UspRenameCharNm 
**  Desc: Ä³¸¯ÅÍ ÀÌ¸§º¯°æ
**  
**                
**  Return values:  
**   0 : ÀÛ¾÷ Ã³¸® ¼º°ø  
**   > 0 : SQL Error  
**   
**                
**  Author: 
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**  2014.04.08  Á¤ÁøÈ£	   ´Þ·Â °ü·Ã Á¤º¸ »èÁ¦ Ãß°¡
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspRenameCharNm]
	@pPcNo		INT,		-- ¼ÒÀ¯ Ä³¸¯ÅÍ ¹øÈ£
	@pUserNo	INT,		-- °èÁ¤ ¹øÈ£  
	@pOrgPcNm	CHAR(12),	-- ÀÌÀü Ä³¸¯ÅÍ¸í 
	@pNewPcNm	CHAR(12),	-- º¯°æÇÒ Ä³¸¯ÅÍ ÀÌ¸§
    @pSerialNo	BIGINT,		-- ¾ÆÀÌÅÛ ½Ã¸®¾ó Á¤º¸
	@pItemNo	INT			-- ¾ÆÀÌÅÛ ¹øÈ£ 	
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE	@aErrNo		INT
			,@aRowCnt	INT 
			,@aNewPcNo	INT
			,@aOldPcNm	CHAR(12)
			,@aPlace		INT
			,@aGkillNode	INT
			,@aGuildNo	INT
			,@aDiscipleNo	INT
			,@aDiscipleType	TINYINT
	
	-- @aPlace°¡ -1 ÀÌ¸é ½ºÅ³À» ºôµåÇÑÀû¾ø´Â Ä³¸¯ÅÍ´Ù
	SELECT  @aErrNo = 0, @aRowCnt = 0, @aNewPcNo = 0, @aPlace = -1, @aGuildNo = 0, @aDiscipleNo =0
	
	-----------------------------------------------
	-- Áßº¹ Ä³¸¯ÅÍ¸í Á¸Àç¿©ºÎ Ã¼Å© 
	-----------------------------------------------
	IF EXISTS( SELECT * 
				FROM dbo.TblPc
				WHERE mNm = @pNewPcNm )
	BEGIN
		SET @aErrNo = 2
		GOTO T_END
	END 
	
	-----------------------------------------------
	-- ÀÌÀü Ä³¸¯¸í°ú µ¿ÀÏ¿©ºÎ Ã¼Å© 
	-----------------------------------------------	
	SELECT 
		@aOldPcNm = T1.mNm
		, @aDiscipleNo = ISNULL(T2.mMaster, 0)
		, @aDiscipleType = ISNULL(T2.mType,0)
		, @aGuildNo = ISNULL(T3.mGuildNo, 0)
		, @aPlace = ISNULL(T4.mPlace, -1)
		, @aGkillNode = ISNULL(T4.mNode, -1)
	FROM dbo.TblPc T1
		LEFT OUTER JOIN dbo.TblDiscipleMember T2
			ON T1.mNo = T2.mDisciple 
		LEFT OUTER JOIN dbo.TblGuildMember T3
			ON T1.mNo = T3.mPcNo
		LEFT OUTER JOIN dbo.TblGkill T4
			ON T1.mNo = T4.mPcNo
	WHERE mNo = @pPcNo
	
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
	IF @aErrNo <> 0  OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END
	END 	
	
	IF @aOldPcNm <> @pOrgPcNm
	BEGIN
		SET @aErrNo = 3		-- ÀÌÀü Ä³¸¯¸í°ú µ¿ÀÏÇÏÁö ¾Ê´Ù. 
		GOTO T_END	
	END 
			
	BEGIN TRAN
					
		---------------------------------------------------
		-- »ç¿ëÇÑ ¾ÆÀÌÅÛ »èÁ¦
		---------------------------------------------------				
		DELETE dbo.TblPcInventory
		WHERE mPcNo = @pPcNo 
			AND	mSerialNo = @pSerialNo
			AND mItemNo = @pItemNo	
			
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1
			GOTO T_TRAN	 
		END 		
				
		---------------------------------------------------
		-- Ä³¸¯ÅÍ¸í º¯°æ 
		---------------------------------------------------
		UPDATE dbo.TblPc
		SET
			mNm = @pNewPcNm
		WHERE mOwner = @pUserNo
				AND mNo = @pPcNo
				
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			SET @aErrNo = 1
			GOTO T_TRAN
		END		
		
		---------------------------------------------------
		-- ÀÌÀü Ä³¸¯ÅÍ¸í Á¸ÀçÇÏÁö ¾Êµµ·Ï º¯°æ(ÀÌÀü Ä³¸¯ÅÍ¸íÀº »èÁ¦ ³¯Â¥¸¦ µî·Ï) 
		---------------------------------------------------
		INSERT INTO dbo.TblPc( mOwner, mSlot, mNm, mClass,mHomeMapNo, mHomePosX, mHomePosY, mHomePosZ, mDelDate)
		VALUES(	0, 0,@pOrgPcNm, 0, 0, 0, 0, 0, GETDATE() )
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1
			GOTO T_TRAN
		END

		SET @aNewPcNo = @@IDENTITY
	 
		---------------------------------------------------
		-- Ä³¸¯ÅÍ »óÅÂ
		---------------------------------------------------
		INSERT INTO dbo.TblPcState(mNo, mLevel, mExp, mHpAdd, mHp, mMpAdd, mMp, mMapNo, mPosX, mPosY, mPosZ)
		VALUES(@aNewPcNo, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 1
			GOTO T_TRAN
		END 

		---------------------------------------------------------------   
		-- ´Þ·Â °ü·Ã Á¤º¸ »èÁ¦ (2014-4-8 : ppoi1019) 
		---------------------------------------------------------------     
		--½ÂÀÎ»èÁ¦
		DELETE dbo.TblCalendarAgreement  
		WHERE mOwnerPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END   

		--±×·ì»èÁ¦
		DELETE dbo.TblCalendarGroup  
		WHERE mOwnerPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END 

		--±×·ì¸â¹ö»èÁ¦
		DELETE dbo.TblCalendarGroupMember  
		WHERE mOwnerPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END  

		--µî·ÏÇÑ ÀÏÁ¤»èÁ¦
		DELETE dbo.TblCalendarSchedule  
		WHERE mOwnerPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END  

		--µî·ÏÇÑ ÀÏÁ¤±×·ì»èÁ¦
		DELETE dbo.TblCalendarScheduleGroup  
		WHERE mOwnerPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END   

		--µî·ÏÇÑ À¯ÀúÀÏÁ¤»èÁ¦
		DELETE dbo.TblCalendarPcSchedule  
		WHERE mOwnerPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END  

		--µî·ÏµÈ À¯ÀúÀÏÁ¤»èÁ¦
		DELETE dbo.TblCalendarPcSchedule  
		WHERE mPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END  

		--´Ù¸¥À¯Àú ½ÂÀÎ¸®½ºÆ®¿¡ µî·ÏµÇ¾îÀÖ´Â Á¤º¸»èÁ¦
		DELETE dbo.TblCalendarAgreement  
		WHERE mMemberPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END   

		--´Ù¸¥À¯Àú ±×·ì¿¡ µî·ÏµÇ¾îÀÖ´Â Á¤º¸»èÁ¦
		DELETE dbo.TblCalendarGroupMember  
		WHERE mMemberPcNo = @pPcNo
		IF( @@ERROR <> 0)  
		BEGIN  
		SET  @aErrNo = 1  
		GOTO T_TRAN  
		END 
		
T_TRAN:
	IF(@aErrNo <> 0)
	 BEGIN
		ROLLBACK TRAN
	 END
	ELSE
	 BEGIN		
		COMMIT TRAN
	 END

T_END:
	SELECT	@aErrNo, @aGuildNo, @aPlace, @aGkillNode, @aDiscipleNo, @aDiscipleType

GO

