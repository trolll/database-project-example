/******************************************************************************
**		Name: UspPopGuildQuestMakingItem
**		Desc: 辨靛 涅胶飘 皋捞欧 焊惑 昏力
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-05-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspPopGuildQuestMakingItem]
	  @pPcNo			INT
	 ,@pSerial			BIGINT	OUTPUT
	 ,@pCnt				INT		OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT	@pSerial = 0, @pCnt = 0

	-- 昏力秦具且 酒捞袍篮 (4825) 怕檬狼 阂揪 蓖加 酒捞袍捞促.  胶琶屈
    SELECT 	TOP 1
			@pSerial = mSerialNo,
			@pCnt = mCnt
    FROM dbo.TblPcInventory
    WHERE mPcNo = @pPcNo
		AND mBindingType = 1	-- 蓖加 O, 钎矫 O
		AND mItemNo = 4825		-- 怕檬狼 阂揪

	IF @pSerial <> 0
	BEGIN
		-- 公炼扒 昏力
		-- 怕檬狼 阂揪 蓖加 酒捞袍篮 辨靛 涅胶飘 皋捞欧 焊惑栏肺父 荤侩等促.
		DELETE dbo.TblPcInventory 
		WHERE mSerialNo = @pSerial
		IF @@ERROR <> 0 
		BEGIN
			RETURN(2);	
		END 
	END

	RETURN(0);

GO

