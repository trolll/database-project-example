/****** Object:  Stored Procedure dbo.UspGetGuildRecruitMemberList    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitMemberList    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitMemberList    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitMemberList    Script Date: 2010-12-23 17:45:59 ******/
CREATE PROCEDURE [dbo].[UspGetGuildRecruitMemberList]

       @pPcNo       INT

AS

       SET NOCOUNT ON;

 

       SELECT

             mRegDate,

             mGuildNo,

             mState,

             mPcMsg

       FROM

             dbo.TblGuildRecruitMemberList AS a

       WHERE

             a.[mPcNo] = @pPcNo

GO

