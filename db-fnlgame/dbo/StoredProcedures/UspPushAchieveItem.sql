/******************************************************************************
**		Name: UspPushAchieveItem
**		Desc: 诀利 酒捞袍(林拳)甫 牢亥配府俊 眠啊.
**			  固犬牢 林拳 1俺 皑家
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPushAchieveItem]
	 @pPcNo  INT 
	,@pItemNo INT
	,@pGuildID INT
	,@pCoinPoint INT
	,@pAchieveID TINYINT
	,@pExp INT
	,@pLimitLevel SMALLINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aSerial BIGINT
			,@aErr INT

	SET @aErr = 0
	SET @aSerial = 0

	-- 矫府倔 掘扁
	EXEC @aSerial = dbo.UspGetItemSerial

	IF @aSerial <= 0
	BEGIN
		SET @aErr = 2
		GOTO T_END
	END


	INSERT dbo.TblPcAchieveInventory
	(				
		mSerialNo
		, mPcNo
		, mItemNo
		, mAchieveGuildID
		, mCoinPoint
		, mSlotNo
		, mAchieveID
		, mExp
		, mLimitLevel
	)
	VALUES
	(
		@aSerial
		, @pPcNo
		, @pItemNo
		, @pGuildID
		, @pCoinPoint
		, 0
		, @pAchieveID
		, @pExp
		, @pLimitLevel
	)

	IF @@ERROR <> 0 OR @@ROWCOUNT = 0
	BEGIN
		SET @aErr = 5
		GOTO T_END
	END	

T_END:
	SELECT @aErr, @aSerial

GO

