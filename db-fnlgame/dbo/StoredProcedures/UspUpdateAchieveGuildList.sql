/******************************************************************************
**		Name: UspUpdateAchieveGuildList
**		Desc: 辨靛 傍己傈狼 诀利 辨靛 府胶飘 盎脚.
**			  GAME DB俊辑父 荤侩.
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateAchieveGuildList]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aValue INT
	SET @aValue = 0;

	-- 抛捞喉捞 厚绢乐绰 版快 檬扁拳 登瘤 臼疽栏哥, 叼弃飘 蔼捞 粮犁窍瘤 臼澜.
	IF NOT EXISTS(SELECT mAchieveGuildID FROM dbo.TblAchieveGuildList)
	BEGIN
		RETURN 2;
	END

	-- 啊厘 付瘤阜 诀单捞飘 盎脚冉荐甫 啊廉柯促.
	SELECT TOP 1 @aValue = mUpdateIndex 
	FROM dbo.TblAchieveGuildList ORDER BY mUpdateIndex DESC

	IF (@@ROWCOUNT = 0)
	BEGIN
		RETURN 3;
	END
	
	SET @aValue = @aValue + 1;

	DECLARE @aTable TABLE ( mRank int IDENTITY PRIMARY KEY,
						    mGuildNo int,
							mGuildNm nvarchar(20));


	INSERT INTO @aTable(mGuildNo, mGuildNm) 
	SELECT TOP 5 T2.mGuildNo, T2.mGuildNm
	FROM dbo.TblGuildBattleResult AS T1
	INNER JOIN dbo.TblGuild AS T2 ON T1.mGuildNo = T2.mGuildNo
	ORDER BY (T1.mWinCnt - T1.mLoseCnt) DESC, T1.mWinCnt DESC


	-- 辨靛 傍己傈狼 辨靛 珐欧 沥焊 诀单捞飘.
	INSERT INTO dbo.TblAchieveGuildList(mGuildRank, mGuildName, mMemberName, mEquipPoint, mUpdateIndex) 
	SELECT T3.mRank, T3.mGuildNm, T2.mNm, T1.mEquipPoint, @aValue
	FROM dbo.TblGuildMember AS T1 
	INNER JOIN dbo.TblPc AS T2 ON T1.mPcNo = T2.mNo
	INNER JOIN @aTable AS T3 ON T1.mGuildNo = T3.mGuildNo

	IF @@ERROR = 0
	BEGIN
		TRUNCATE TABLE dbo.TblGuildMember
	END

	RETURN 0;

GO

