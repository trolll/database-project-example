/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspResetCachingItem    Script Date: 2008-11-10 10:37:17 ******/





CREATE  PROCEDURE [dbo].[UspResetCachingItem]
	 @pPcNo			INT
	,@pSerial		BIGINT
	,@pCachingCnt	INT			-- ???.(?, ??? ??)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			
	
	DECLARE	 @aRowCnt		INT
				,@aErrNo		INT
	DECLARE	 @aCnt	INT

	SELECT		@aErrNo = 0,
				@aCnt = 0,
				@aRowCnt = 0
		
	---------------------------------------
	-- ???? ?? ?? 
	---------------------------------------
	SELECT 
		@aCnt = mCnt
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pSerial
			AND mPcNo = @pPcNo
		
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1
		GOTO T_END			
	END
	
	---------------------------------------
	--  reset? ??? ??? 1? ????? ??. 
	---------------------------------------
	SET @aCnt = @aCnt + @pCachingCnt
	IF @aCnt < 1
	BEGIN
		SET @aErrNo = 2
		GOTO T_END			
	END

	---------------------------------------
	--  ?? ??
	---------------------------------------
	UPDATE dbo.TblPcInventory 
	SET 
		mCnt = @aCnt 
	WHERE mSerialNo = @pSerial

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 3
		GOTO T_END			
	END		

T_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

