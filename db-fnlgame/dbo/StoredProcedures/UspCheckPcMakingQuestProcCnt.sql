/******************************************************************************
**		Name: UspCheckPcMakingQuestProcCnt
**		Desc: 柳青吝牢 皋捞欧 涅胶飘啊 割俺牢啊
**
**		Auth: 沥柳龋
**		Date: 2010-02-23
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspCheckPcMakingQuestProcCnt]
	  @pPcNo			INT
	 ,@pStartNQuestNo	INT			-- 老馆 涅胶飘 皋捞欧 矫累锅龋
	 ,@pStartGQuestNo	INT			-- 辨靛 涅胶飘 皋捞欧 矫累锅龋
	 ,@pEndNQuestNo		INT			-- 老馆 涅胶飘 皋捞欧 场锅龋
	 ,@pEndGQuestNo		INT			-- 辨靛 涅胶飘 皋捞欧 场锅龋
	 ,@pNormalQCnt		INT OUTPUT
	 ,@pGuildQCnt		INT OUTPUT

AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT @pNormalQCnt = count(*)
    FROM dbo.TblPcQuest as T1
    WHERE T1.mPcNo = @pPcNo 
		AND T1.mQuestNo BETWEEN @pStartNQuestNo AND @pEndNQuestNo		-- 老馆 涅胶飘 皋捞欧
		AND T1.mValue < 99;

	SELECT @pGuildQCnt = count(*)
    FROM dbo.TblPcQuest as T1
    WHERE T1.mPcNo = @pPcNo 
		AND T1.mQuestNo BETWEEN @pStartGQuestNo AND @pEndGQuestNo		-- 辨靛 涅胶飘 皋捞欧
		AND T1.mValue < 99;

GO

