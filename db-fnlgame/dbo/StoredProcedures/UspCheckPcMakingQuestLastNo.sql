/******************************************************************************
**		Name: UspCheckPcMakingQuestLastNo
**		Desc: 付瘤阜 涅胶飘 锅龋甫 啊廉柯促
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-21
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckPcMakingQuestLastNo]
	 @pPcNo					INT 
	,@pStartNQuestNo		INT
	,@pStartGQuestNo		INT
	,@pEndNQuestNo			INT
	,@pEndGQuestNo			INT
	,@pNormalQuestLastNo	INT OUTPUT
	,@pGuildQuestLastNo		INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
			TOP 1 @pNormalQuestLastNo = mQuestNo
    FROM dbo.TblPcQuest 
    WHERE mPcNo = @pPcNo 
		AND mValue < 99
		AND mQuestNo BETWEEN @pStartNQuestNo AND @pEndNQuestNo		-- 老馆 涅胶飘 皋捞欧
	ORDER BY mQuestNo DESC

	SELECT 
			TOP 1 @pGuildQuestLastNo = mQuestNo
    FROM dbo.TblPcQuest 
    WHERE mPcNo = @pPcNo 
		AND mValue < 99
		AND mQuestNo BETWEEN @pStartGQuestNo AND @pEndGQuestNo		-- 辨靛 涅胶飘 皋捞欧
	ORDER BY mQuestNo DESC

GO

