/******************************************************************************
**		Name: UspGetAchieveGuildList
**		Desc: 诀利辨靛 府胶飘.
**            mUpdateIndex 啊 啊厘 臭篮 弊缝父 啊廉柯促.
**			  FNLParmDev OR FNLParmReal 狼 惑炔喊 炼例 鞘夸
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013/08/02	巢己葛				诀利辨靛鸥涝阑 涝仿栏肺 罐档废 荐沥	 	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveGuildList]
(
	@pGuildType			INT			-- 诀利辨靛鸥涝
)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aValue INT
	SET @aValue = 0;

	-- 烹钦 辨靛傈 辨靛 沥焊.
	IF @pGuildType = 2
	BEGIN
		SELECT TOP 1 @aValue = mUpdateIndex 
		FROM FNLParm.dbo.TblAchieveGuildList ORDER BY mUpdateIndex DESC

		SELECT 
			 mAchieveGuildID
			,mGuildRank
			,mGuildName
			,mMemberName
			,mEquipPoint
			,mUpdateIndex
		FROM FNLParm.dbo.TblAchieveGuildList
		WHERE @aValue <= mUpdateIndex
	END
	
	-- 傍己傈 辨靛 沥焊.
	IF @pGuildType = 1
	BEGIN
		SELECT TOP 1 @aValue = mUpdateIndex 
		FROM dbo.TblAchieveGuildList ORDER BY mUpdateIndex DESC

		SELECT 
			 mAchieveGuildID
			,mGuildRank
			,mGuildName
			,mMemberName
			,mEquipPoint
			,mUpdateIndex
		FROM dbo.TblAchieveGuildList
		WHERE @aValue <= mUpdateIndex
	END


	-- 叼弃飘 辨靛 沥焊.
	IF @pGuildType <> 1 AND @pGuildType <> 2
	BEGIN
		SELECT 
			 mAchieveGuildID
			,mGuildRank
			,mGuildName
			,mMemberName
			,mEquipPoint
			,mUpdateIndex
		FROM FNLParm.dbo.DT_AchieveGuildList
	END

GO

