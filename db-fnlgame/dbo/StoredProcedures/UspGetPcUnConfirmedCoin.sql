/******************************************************************************
**		Name: UspGetPcUnConfirmedCoin
**		Desc: 固犬牢 林拳 啊廉坷扁
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcUnConfirmedCoin]
	@pPcNo  INT  
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aCoin INT
			,@aErr INT
			,@aRowCnt INT

	SET @aCoin = 0
	SELECT @aCoin = mCoin FROM dbo.TblPcUnConfirmedCoin WHERE mPcNo = @pPcNo
	SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT

	IF @aErr <> 0
	BEGIN
		SET @aErr = 1
		GOTO T_END
	END	

	IF @aRowCnt = 0
	BEGIN
		SET @aErr = 0
		SET @aCoin = 0
		GOTO T_END
	END

T_END:
	SELECT @aErr, @aCoin

GO

