/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2009-5-12 9:18:17 ******/

/****** Object:  Stored Procedure dbo.UspEnterFriend    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspEnterFriend]
	 @pOwnerPcNo	INT
	,@pPcNm			CHAR(15)
	,@pPcNo			INT			OUTPUT
	,@pErrNoStr		VARCHAR(50)	OUTPUT
	,@pLastLogoutTm	DATETIME	OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo		INT,
			@aFriendPcNm	CHAR(12)
			
	SET		@aErrNo		= 0
	SET		@pErrNoStr	= 'eErrNoSqlInternalError'
	SET		@aFriendPcNm = NULL
	
	SELECT 
		@pPcNo=a.[mNo], 
		@pLastLogoutTm=b.[mLogoutTm] 
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
			ON a.mNo = b.mNo
	WHERE a.mNm = @pPcNm
			AND a.mDelDate IS NULL
	IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))
	BEGIN
		SET	 @pErrNoStr	= 'eErrNoCharNotExist'
		RETURN(1)
	END

	SELECT TOP 1 
		@aFriendPcNm = mFriendPcNm
	FROM dbo.TblPcFriend 
	WHERE mOwnerPcNo = @pOwnerPcNo
			 AND mFriendPcNo = @pPcNo						 
	IF (@@ROWCOUNT > 0) AND (@aFriendPcNm = @pPcNm)
	BEGIN
		SET	 @pErrNoStr	= 'eErrNoCharAlreadyRegister'
		RETURN(2) 	
	END 

	BEGIN TRAN
	
		IF @aFriendPcNm IS NOT NULL		-- duplicate pc number 
		BEGIN	
			UPDATE dbo.TblPcFriend	
			SET 
				mFriendPcNo =-mFriendPcNo			
			WHERE mOwnerPcNo = @pOwnerPcNo
				 AND mFriendPcNo = @pPcNo
				 
			IF @@ERROR <> 0 
			BEGIN
				ROLLBACK TRAN
				RETURN(3)
			END 	 
		END 
		
		INSERT INTO dbo.TblPcFriend([mOwnerPcNo], [mFriendPcNo], [mFriendPcNm])
		VALUES(@pOwnerPcNo,  @pPcNo, @pPcNm)		
		
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(3)
		END 	 

	COMMIT TRAN
	RETURN(0)

GO

