/******************************************************************************
**		Name: UspUpdateServantExp
**		Desc: ¼­¹øÆ® °æÇèÄ¡ °»½Å
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateServantExp]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pLevel		SMALLINT
	,@pExp			BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE dbo.TblPcServant 
	SET mLevel = @pLevel
	    ,mExp = @pExp
	WHERE mPcNo = @pPcNo
	  AND mSerialNo = @pSerialNo

	SET NOCOUNT OFF;

GO

