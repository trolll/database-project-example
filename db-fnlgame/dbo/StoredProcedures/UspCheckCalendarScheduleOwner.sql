/******************************************************************************
**		Name: UspCheckCalendarScheduleOwner
**		Desc: ¸¸µç»ç¶÷ È®ÀÎ
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckCalendarScheduleOwner]
	 @pOwnerPcNo		INT 
	,@pSerialNo			BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF NOT EXISTS ( SELECT  *
					FROM dbo.TblCalendarSchedule
					WHERE mSerialNo = @pSerialNo AND mOwnerPcNo = @pOwnerPcNo)
	BEGIN
		RETURN(1);
	END 

	RETURN(0);

GO

