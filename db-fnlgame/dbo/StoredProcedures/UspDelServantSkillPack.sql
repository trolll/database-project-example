/******************************************************************************
**		Name: UspDelServantSkillPack
**		Desc: 辑锅飘 胶懦 蒲 力芭
**
**		Auth: 捞侩林
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20160331	捞侩林				SkillPackType 眠啊 (函脚 胶懦蒲牢瘤 酒囱瘤 备喊侩)
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDelServantSkillPack]
	@pSerialNo			BIGINT
	,@pSPID				INT = 0
	,@pSkillPackType	SMALLINT = 0
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0

	IF 0 = @pSPID 
	BEGIN
		IF 0 = @pSkillPackType
		BEGIN
			DELETE FROM dbo.TblPcServantSkillPack
			WHERE mSerialNo = @pSerialNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1		-- SQL Error
			END
		END
		ELSE
		BEGIN
			DELETE FROM dbo.TblPcServantSkillPack
			WHERE mSerialNo = @pSerialNo AND mSkillPackType = @pSkillPackType
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 2		-- SQL Error
			END
		END
	END
	ELSE
	BEGIN
		DELETE FROM dbo.TblPcServantSkillPack
		WHERE mSerialNo = @pSerialNo AND mSPID = @pSPID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 3		-- SQL Error
		END
	END

	RETURN @aErrNo

	SET NOCOUNT OFF;

GO

