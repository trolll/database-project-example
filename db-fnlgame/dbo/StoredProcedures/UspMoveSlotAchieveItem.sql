/******************************************************************************
**		Name: UspMoveSlotAchieveItem
**		Desc: 酒捞袍 浇吩 困摹 函版.
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspMoveSlotAchieveItem]
	@pPcNo  INT
	,@pSerialNo1 BIGINT
	,@pSlot1	TINYINT
	,@pSerialNo2 BIGINT
	,@pSlot2	TINYINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aErr INT
	SET @aErr = 0

	BEGIN TRAN

		UPDATE TblPcAchieveInventory SET mSlotNo = @pSlot1 
		WHERE mPcNo = @pPcNo AND mSerialNo = @pSerialNo1

		IF @@ERROR <> 0 OR @@ROWCOUNT = 0
		BEGIN
			ROLLBACK TRAN
			SET @aErr = 1
			GOTO T_END
		END

		IF @pSerialNo2 <> 0
		BEGIN
			UPDATE TblPcAchieveInventory SET mSlotNo = @pSlot2 
					WHERE mPcNo = @pPcNo AND mSerialNo = @pSerialNo2

			IF @@ERROR <> 0 OR @@ROWCOUNT = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 2
				GOTO T_END
			END
		END

	COMMIT TRAN

T_END:
	SELECT @aErr

GO

