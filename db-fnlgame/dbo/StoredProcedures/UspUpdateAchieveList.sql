/******************************************************************************
**		Name: UspUpdateAchieveList
**		Desc: 诀利 府胶飘 诀单捞飘.
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateAchieveList]
	@pPcNo  INT
	,@pAchieveID TINYINT
	,@pActionCount	SMALLINT
	,@pIsComplete BIT
	,@pIsNew BIT
	,@pSerialNo BIGINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aErr INT
	SET @aErr = 0
	
	IF NOT EXISTS(SELECT mAchieveID FROM dbo.TblPcAchieveList 
					WHERE mPcNo = @pPcNo AND mAchieveID = @pAchieveID)
	BEGIN
		INSERT dbo.TblPcAchieveList
		(
			mPcNo
			, mAchieveID
			, mActionCount
			, mIsComplete
			, mIsNew
			, mSerialNo
		)
		VALUES
		(
			@pPcNo
			, @pAchieveID
			, @pActionCount
			, @pIsComplete
			, @pIsNew
			, @pSerialNo
		)

		IF @@ERROR <> 0 OR @@ROWCOUNT = 0
		BEGIN
			SET @aErr = 1;
		END
	END
	ELSE
	BEGIN
		UPDATE dbo.TblPcAchieveList 
		SET mActionCount = @pActionCount, 
			mIsComplete = @pIsComplete, 
			mIsNew = @pIsNew
		WHERE mPcNo = @pPcNo AND mAchieveID = @pAchieveID

		IF @@ERROR <> 0 OR @@ROWCOUNT = 0
		BEGIN
			SET @aErr = 2;
		END
	END

	SELECT @aErr

GO

