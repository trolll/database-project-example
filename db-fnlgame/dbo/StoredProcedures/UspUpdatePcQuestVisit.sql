/******************************************************************************
**		Name: UspUpdatePcQuestVisit
**		Desc: 涅胶飘 惑怕 诀单捞飘
**
**		Auth: 沥柳龋
**		Date: 2010-03-29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePcQuestVisit]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pPlaceNo 		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblPcQuestVisit
	SET mVisit = 1
	WHERE mPcNo = @pPcNo
			AND mQuestNo = @pQuestNo
			AND mPlaceNo = @pPlaceNo
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1);
	END
	 		 
	RETURN(0);

GO

