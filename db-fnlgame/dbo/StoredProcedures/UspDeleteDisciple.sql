/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspDeleteDisciple    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspDeleteDisciple]
	@pMaster	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT
	DECLARE @aCount INT
	SET @aRv = 0

	IF NOT EXISTS(SELECT mMaster FROM TblDisciple WHERE mMaster  = @pMaster)
	BEGIN
		RETURN(3)		-- ???? ?? ??
	END 

	SELECT @aCount = COUNT(mMaster) 
	FROM dbo.TblDiscipleMember 
	WHERE mMaster = @pMaster	
	IF @aCount > 1
	BEGIN
		RETURN(2)		-- ???? ??? ?? ??? ?? ???? ?
	END

	BEGIN TRAN
	
		DELETE dbo.TblDiscipleMember 
		WHERE mMaster = @pMaster	-- ??? ?? ???
		
		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1) -- DB ERROR
		END
		
		DELETE dbo.TblDiscipleHistory 
		WHERE mMaster = @pMaster		-- ??? ?? ???

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1) -- DB ERROR
		END
		
		DELETE dbo.TblDisciple 
		WHERE mMaster = @pMaster

		IF @@ERROR <> 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1) -- DB ERROR
		END
		
	COMMIT TRAN

	RETURN(0) -- non error

GO

