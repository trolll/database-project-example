/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspListDialogScr    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspListDialogScr]
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	SELECT a.[mMID], RTRIM(a.[mClick]), RTRIM(a.[mDie]), RTRIM(a.[mAttacked]), RTRIM(a.[mTarget]), RTRIM(a.[mBear]),
		   RTRIM(a.[mGossip1]), RTRIM(a.[mGossip2]), RTRIM(a.[mGossip3]), RTRIM(a.[mGossip4]), RTRIM(b.[mScriptText])
		FROM TblDialog AS a LEFT JOIN TblDialogScript AS b ON(a.[mMID] = b.[mMID])
		
	SET NOCOUNT OFF

GO

