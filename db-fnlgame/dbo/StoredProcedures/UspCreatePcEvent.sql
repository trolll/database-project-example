/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2009-6-1 15:32:38 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2009-5-12 9:18:17 ******/

/****** Object:  Stored Procedure dbo.UspCreatePcEvent    Script Date: 2008-11-10 10:37:19 ******/
CREATE PROCEDURE [dbo].[UspCreatePcEvent]  	
	 @pOwner	INT						
	,@pSlot		TINYINT
	,@pNm		CHAR(12)				--Char Name
	,@pClass	TINYINT					--0,1,2?? ????
	,@pSex		TINYINT					--0,1 ?? ????
	,@pHead		TINYINT					--??? 1
	,@pFace		TINYINT					--??? 1
	,@pBody		TINYINT					--??? 1
	,@pHomeMap	INT						--EMap.
	,@pHomeX	REAL						
	,@pHomeY	REAL						
	,@pHomeZ	REAL						
	,@pPcNo		INT			OUTPUT
	,@pLevel	SMALLINT	OUTPUT
	,@pErrNoStr	VARCHAR(50)	OUTPUT	
AS
	SET NOCOUNT ON;			
	SET XACT_ABORT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aSeq INT, 
			@aItemNo	INT, 
			@aCnt	INT, 
			@aCntUse	INT,
			@aStatus	TINYINT,
			@aSerial BIGINT	
			
	DECLARE @aInventory	TABLE (
		mSeq INT IDENTITY(1,1) NOT NULL
		, mItemNo	INT	NOT NULL
		, mCnt	INT NOT NULL
		, mCntUse	INT	NOT NULL
		, mStatus	TINYINT NOT NULL
		, mTermOfValidty INT NOT NULL
		, mSerialNo	BIGINT NOT NULL
	)
			
	SELECT @aErrNo = 0, @aRowCnt = 0, @aSerial = 0  
	
	-- Class check
	IF @pClass NOT IN (0, 1, 2, 3)
	BEGIN
		RETURN(5)	-- invalid character 			
	END 

	--SELECT @pHomeMap = 0, @pHomeX = 214024, @pHomeY = 25731 , @pHomeZ = 13000; 
	SELECT @pLevel	= 57;
	
	
	-- New Serial ID
	INSERT INTO @aInventory( mItemNo, mCnt, mCntUse, mStatus, mTermOfValidty,  mSerialNo )
	SELECT mItemNo, mCnt, mCntUse, mStatus, mTermOfValidty,  0 
	FROM dbo.T_TblEventItem
	WHERE mClass = @pClass
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT 	
	
	IF @aErrNo <> 0
	BEGIN
		RETURN(1)	-- db sql error 
	END 	
	
	WHILE (@aRowCnt > 0 )	
	BEGIN
		EXEC @aSerial =  dbo.UspGetItemSerial 

		IF @aSerial <= 0
			SET @aErrNo = @aErrNo + 1
	
		UPDATE @aInventory	
		SET
			mSerialNo = @aSerial
		WHERE mSeq = @aRowCnt
		
		SELECT @aErrNo = @@ERROR
		
		IF @aErrNo <> 0
			SET @aErrNo = @aErrNo + 1	
		
		SET @aRowCnt = @aRowCnt - 1
	END 
	
	IF @aErrNo > 0 
	BEGIN
		RETURN(1)	-- db sql error 
	END 

	-- Character Create
	BEGIN TRAN
	
		INSERT INTO dbo.TblPc( mOwner, mSlot, mNm, mClass, mSex, mHead, mFace, mBody, 
			mHomeMapNo, mHomePosX, mHomePosY, mHomePosZ)
		VALUES(	@pOwner, @pSlot ,@pNm, @pClass, @pSex, @pHead, @pFace, @pBody, @pHomeMap, 
			@pHomeX, @pHomeY, @pHomeZ)
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET	 @aErrNo	= 3
			GOTO T_ERR		
		END

		SET @pPcNo	= @@IDENTITY
	 
		INSERT INTO dbo.TblPcState(mNo, mLevel, mExp, mHpAdd, mHp, mMpAdd, mMp, mMapNo, mPosX, mPosY, mPosZ, mStomach)
		VALUES(@pPcNo, @pLevel, 45000000, 0, 10, 0, 10, @pHomeMap, @pHomeX, @pHomeY, @pHomeZ, 100)

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET	 @aErrNo = 4
			GOTO T_ERR		 
		END 	

		-- item create 
		INSERT INTO dbo.TblPcInventory ( mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse, mPracticalPeriod)
		SELECT mSerialNo, @pPcNo, mItemNo, dbo.UfnGetEndDate(getdate(), mTermOfValidty) mEndDate, 1, mStatus, mCnt, mCntUse, 72
		FROM @aInventory

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT			
		IF @aErrNo <> 0  OR @aRowCnt <= 0 
		BEGIN
			SET	 @aErrNo = 5
			GOTO T_ERR		 
		END

T_ERR:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN	
		
T_END:			
	RETURN(@aErrNo)

GO

