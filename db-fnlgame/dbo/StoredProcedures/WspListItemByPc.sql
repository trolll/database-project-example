/****** Object:  Stored Procedure dbo.WspListItemByPc    Script Date: 2011-4-19 15:24:40 ******/    
    
/****** Object:  Stored Procedure dbo.WspListItemByPc    Script Date: 2011-3-17 14:50:07 ******/    
    
/****** Object:  Stored Procedure dbo.WspListItemByPc    Script Date: 2011-3-4 11:36:47 ******/    
    
/****** Object:  Stored Procedure dbo.WspListItemByPc    Script Date: 2010-12-23 17:46:04 ******/    
    
/****** Object:  Stored Procedure dbo.WspListItemByPc    Script Date: 2010-3-22 15:58:22 ******/    
    
/****** Object:  Stored Procedure dbo.WspListItemByPc    Script Date: 2009-12-14 11:35:30 ******/    
    
/****** Object:  Stored Procedure dbo.WspListItemByPc    Script Date: 2009-11-16 10:23:30 ******/    
    
/****** Object:  Stored Procedure dbo.WspListItemByPc    Script Date: 2009-7-14 13:13:32 ******/    
    
CREATE  PROCEDURE [dbo].[WspListItemByPc]      
 @mCharNm VARCHAR(12)       
AS      
 SET NOCOUNT ON      
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       
 SET ROWCOUNT 1000 -- ?? ??? ? ?? ??? ?? ??       
      
 -------------------------------------------------------      
 -- ??? ??? ??      
 -------------------------------------------------------      
  SELECT   
   T1.mNm  
  ,(  
   SELECT   
    mSlot  
   FROM dbo.TblPcEquip  
   WHERE mSerialNo = T1.mSerialNo    
   ) AS mSlot  
  ,T2.IName    
  ,T1.mEndDate  
  ,T1.mSerialNo  
  ,T1.mCnt  
  ,T1.mItemNo  
  ,T1.mIsSeizure  
  ,T1.mRegDate  
  ,T1.mStatus  
  ,T1.mPracticalPeriod  
  ,T1.mBindingType  
  ,T1.mRestoreCnt  
  ,T1.mIsConfirm --확인 여부 (1은 확인, 0은 미확인)  
  ,(  
   SELECT COUNT(*)  
   FROM FNLParm.dbo.DT_Item  
   WHERE IID = T1.mItemNo  
   AND IType IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 17, 18, 20)
   AND T1.mIsConfirm = 1  
   AND T1.mIsSeizure = 0  
  ) AS mIsEquipBead --룬 장착 가능여부  
 FROM (  
  SELECT   
    T1.mNm  
   ,T1.mNo  
   ,T2.mStatus  
   ,DATEDIFF(DAY, GETDATE(), T2.mEndDate) AS mEndDate -- 유효기간  
   ,T2.mSerialNo  
   ,T2.mCnt  
   ,T2.mItemNo  
   ,T2.mIsSeizure  
   ,T2.mRegDate -- 습득일자  
   ,T2.mPracticalPeriod  
   ,T2.mBindingType  
   ,T2.mRestoreCnt  
   ,T2.mIsConfirm --확인 여부 (1은 확인, 0은 미확인)  
  FROM [dbo].[TblPc] T1  
   INNER JOIN [dbo].[TblPcInventory]  T2   
   ON T1.mNo = T2.mPcNo  
    AND T1.mDelDate IS NULL        
  WHERE T1.mNm = @mCharNm  
 ) T1   
  INNER JOIN FNLParm.dbo.DT_Item AS T2  
   ON T1.mItemNo = T2.IID  
 ORDER BY T1.mNo ASC

GO

