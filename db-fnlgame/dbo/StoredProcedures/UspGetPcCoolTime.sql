/****** Object:  Stored Procedure dbo.UspGetPcCoolTime    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetPcCoolTime    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspGetPcCoolTime    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetPcCoolTime    Script Date: 2010-12-23 17:45:59 ******/
/******************************************************************************
**		Name: UspGetPcCoolTime
**		Desc: 蜡历狼 巢篮 酿鸥烙 矫埃阑 啊瘤绊 柯促.
**
**		Auth: 沥备柳
**		Date: 09.09.30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	-----------------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE    PROCEDURE [dbo].[UspGetPcCoolTime]
	@pPcNo		INT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		mPcNo
		, mSID
		, mCoolTimeGroup
		, mRemainTime
	FROM 
		dbo.TblPcCoolTime
	WHERE mPcNo = @pPcNo
	
	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

