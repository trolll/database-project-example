/******************************************************************************
**		Name: UspUptLimitedResourceItem
**		Desc: 부활 관련 정보를 가져온다
**
**		Auth: 조 세현
**		Date: 2009-07-27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      2010-07-26  정진호              mRemainerCnt 체크 추가
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUptLimitedResourceItem]
	@pIsInc	BIT,
	@pType	INT,
	@pCnt	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF 0 = @pIsInc	-- 아이템을 얻을때
	BEGIN
		DECLARE @aCnt INT
		SELECT @aCnt = 0

		SELECT @aCnt = mRemainerCnt
		FROM dbo.TblLimitedResource
		WHERE mResourceType = @pType

		IF (0 <> @@ERROR) OR (0 = @@ROWCOUNT)
		BEGIN
			RETURN(2)
		END

		IF 0 >= @aCnt
		BEGIN
			RETURN(1)	-- 현재 남은 리소스 제한 아이템 갯수가 없다
		END
	END
	
	IF NOT EXISTS(	SELECT *
					FROM dbo.TblLimitedResourceItem
					WHERE mResourceType = @pType)
	BEGIN
		RETURN(2)
	END 				

	IF 0 < @pIsInc
	BEGIN
		UPDATE dbo.TblLimitedResource
		SET mRemainerCnt = 
			CASE 
				WHEN mMaxCnt < (mRemainerCnt + @pCnt) THEN mMaxCnt
				ELSE mRemainerCnt + @pCnt
			END
		WHERE mResourceType = @pType
	END
	ELSE
	BEGIN
		UPDATE dbo.TblLimitedResource
		SET mRemainerCnt = 
			CASE
				WHEN (mRemainerCnt - @pCnt) < 0 THEN 0
				ELSE mRemainerCnt - @pCnt
			END
		WHERE mResourceType = @pType
	END
	
	IF (0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	BEGIN
		RETURN(2)
	END

	RETURN(0)

GO

