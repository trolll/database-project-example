/******************************************************************************
**		Name: UspGetAchieveCoinPoint
**		Desc: 林拳 器牢飘 拌魂
**			  FNLParmDev OR FNLParmReal 狼 惑炔喊 炼例 鞘夸
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013/08/02	巢己葛				诀利辨靛鸥涝阑 涝仿栏肺 罐档废 荐沥	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveCoinPoint]
(
	@pGuildType			INT			-- 诀利辨靛鸥涝
	, @pRarity			INT			-- 锐蓖档
	, @pAchieveGuildID	INT			-- 诀利 辨靛 酒捞叼
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE   @aCoinPoint INT
			, @aRarityPoint INT
			, @aEquipPoint INT
			, @aGuildRankPoint INT
			, @aUpdatePoint INT

	SELECT 	  @aCoinPoint = 0
			, @aRarityPoint = 0
			, @aEquipPoint = 0
			, @aGuildRankPoint = 0
			, @aUpdatePoint = 0

	-- 林拳 酒捞袍狼 锐蓖档 痢荐甫 舅酒辰促.
	SELECT @aRarityPoint = mRarityPoint 
	FROM FNLParm.dbo.TP_AchieveRarityPoint WHERE mRarityID = @pRarity
	IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
	BEGIN
		SET @aCoinPoint = 0
		GOTO T_END
	END

	-- 烹钦 辨靛傈 辨靛 沥焊.
	IF @pGuildType = 2
	BEGIN
		SELECT @aEquipPoint = T1.mEquipPoint, @aGuildRankPoint = T2.mPoint, @aUpdatePoint = T1.mUpdateIndex
		FROM FNLParm.dbo.TblAchieveGuildList AS T1 
			INNER JOIN FNLParm.dbo.TP_AchieveGuildPoint AS T2 
		ON T1.mGuildRank = T2.mRank
		WHERE T1.mAchieveGuildID = @pAchieveGuildID

		IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
		BEGIN
			SET @aCoinPoint = 0
			GOTO T_END
		END
	END

	-- 傍己傈 辨靛 沥焊.
	IF @pGuildType = 1
	BEGIN
		SELECT @aEquipPoint = T1.mEquipPoint, @aGuildRankPoint = T2.mPoint, @aUpdatePoint = T1.mUpdateIndex
		FROM dbo.TblAchieveGuildList AS T1 
			INNER JOIN FNLParm.dbo.TP_AchieveGuildPoint AS T2 
		ON T1.mGuildRank = T2.mRank
		WHERE T1.mAchieveGuildID = @pAchieveGuildID

		IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
		BEGIN
			SET @aCoinPoint = 0
			GOTO T_END
		END
	END


	-- 扁夯 辨靛 沥焊
	IF @pGuildType <> 1 AND @pGuildType <> 2
	BEGIN
		SELECT @aEquipPoint = T1.mEquipPoint, @aGuildRankPoint = T2.mPoint, @aUpdatePoint = T1.mUpdateIndex
		FROM FNLParm.dbo.DT_AchieveGuildList AS T1 
			INNER JOIN FNLParm.dbo.TP_AchieveGuildPoint AS T2 
		ON T1.mGuildRank = T2.mRank
		WHERE T1.mAchieveGuildID = @pAchieveGuildID

		IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
		BEGIN
			SET @aCoinPoint = 0
			GOTO T_END
		END
	END

	-- A * 辨靛 珐农 器牢飘 + B * 锐蓖档 器牢飘 + C * 厘厚 器牢飘 + D
	-- D = 辨靛盎脚冉荐(痢荐)
	SET @aCoinPoint = (1 * @aGuildRankPoint) + (1 * @aRarityPoint) + (1 * @aEquipPoint) + @aUpdatePoint

T_END:
	RETURN @aCoinPoint;
END

GO

