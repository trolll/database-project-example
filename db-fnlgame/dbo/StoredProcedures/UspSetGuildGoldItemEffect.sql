/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildGoldItemEffect    Script Date: 2008-11-10 10:37:18 ******/



CREATE PROCEDURE [dbo].[UspSetGuildGoldItemEffect]
	  @pGuildNo		INT
	, @pItemType	INT
	, @pParmA		FLOAT
	, @pValidHour	INT	-- Hour
	, @pItemId		INT	
	, @pLeftTime	INT	OUTPUT
	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aEndDay	SMALLDATETIME
	SELECT @aEndDay = dbo.UfnGetEndDateByHour( GETDATE(), @pValidHour)
		, @pLeftTime= DATEDIFF(mi, GETDATE(), @aEndDay)
	
	IF EXISTS(	SELECT *
				FROM dbo.TblGuildGoldItemEffect  
				WHERE mGuildNo= @pGuildNo	
					AND mItemType = @pItemType)
	BEGIN
		UPDATE dbo.TblGuildGoldItemEffect 
		SET 
			  mRegDate = GETDATE()
			, mParmA = @pParmA
			, mEndDate = @aEndDay
			, mItemNo = @pItemId
		WHERE mGuildNo = @pGuildNo 
			AND mItemType = @pItemType
			
		IF(@@ERROR <>0)
		BEGIN
			RETURN(3)
		END
		
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblGuildGoldItemEffect (mGuildNo, mItemType, mParmA, mEndDate, mItemNo) 
		VALUES(@pGuildNo, @pItemType, @pParmA, @aEndDay, @pItemId)

		IF(@@ERROR <>0 )
		BEGIN
			RETURN(2)	-- DB Err
		END
	END
	
	RETURN(0)

GO

