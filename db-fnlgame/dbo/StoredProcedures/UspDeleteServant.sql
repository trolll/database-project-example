/******************************************************************************
**		Name: UspDeleteServant
**		Desc: ¼­¹øÆ® Á¤º¸ »èÁ¦
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteServant]
	@pSerialNo		BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNo		INT
	DECLARE @aRowCnt	INT

	DELETE dbo.TblPcServant
	WHERE mSerialNo = @pSerialNo
	
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR 	@aRowCnt = 0
	BEGIN
		RETURN(@aErrNo)
	END

	-- ½ºÅ³ Æ®¸® ÀüÃ¼ »èÁ¦
	EXEC @aErrNo = UspDelServantSkillTreeNodeItem @pSerialNo
	IF @aErrNo <> 0
	BEGIN
		RETURN(@aErrNo)
	END

	-- ½ºÅ³ ÆÑ ÀüÃ¼ »èÁ¦
	EXEC @aErrNo = UspDelServantSkillPack @pSerialNo
	IF @aErrNo <> 0
	BEGIN
		RETURN(@aErrNo)
	END

	SET NOCOUNT OFF;

GO

