/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspListBoard    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspListBoard]
	 @pBoardId		TINYINT
	,@pIsNext		INT
	,@pBoardNo		INT		-- ???.
	,@pCnt			INT	
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE @aSql		NVARCHAR(512)
	DECLARE @aWhere	NVARCHAR(100)

	SET @aWhere 	= N' @aBoardId		TINYINT, @aBoardNo		INT '	
	SET @aSql = N'SELECT TOP ' + CAST(@pCnt AS VARCHAR(10)) + ' [mRegDate], [mBoardNo], RTRIM([mFromPcNm]), RTRIM([mTitle]) ' +  CHAR(10) +
				N'FROM TblBoard ' +  CHAR(10) +
				N'WHERE ([mBoardId] = @aBoardId ) '+  CHAR(10) 
				

	IF(0 <> @pIsNext)
 	BEGIN
		SET @aSql = @aSql + 	N'AND ([mBoardNo] <= @aBoardNo ) ' +  CHAR(10) +
							N'ORDER BY [mBoardNo] DESC'	+  CHAR(10)
	END
	ELSE
	BEGIN
		DECLARE	@aCnt	INT

		SELECT @aCnt=COUNT(*) 
		FROM dbo.TblBoard 
		WHERE ([mBoardId]=@pBoardId) 
				AND ([mBoardNo]=@pBoardNo)

		IF(@pCnt < @aCnt)
		BEGIN
			SET @aSql = @aSql + N'AND ([mBoardNo] >= @aBoardNo ) ' +  CHAR(10) +
								N'ORDER BY [mBoardNo] ASC' +  CHAR(10)
		END
		ELSE
		BEGIN
			SET @aSql = @aSql + N'ORDER BY [mBoardNo] DESC'		 	 
		END

	 END				

	EXEC sp_executesql	@aSql, @aWhere, @aBoardId = @pBoardId, 	@aBoardNo = @pBoardNo

GO

