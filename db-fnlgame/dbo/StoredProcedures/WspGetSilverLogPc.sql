/******************************************************************************  
**  File: 
**  Name: WspGetSilverLogPc  
**  Desc: 캐릭터별 실버 증감 조회
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2012.03.23 이진선    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[WspGetSilverLogPc]
(
	@pDiffTime SMALLINT = 20  -- 분단위  
	 , @pIsCurr BIT = 0    -- 현재?
	 , @pCnt INT
	 , @pStartDate SMALLDATETIME  
	 , @pEndDate SMALLDATETIME  
)
AS
BEGIN
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
  
  
	 IF @pIsCurr = 0  
	 BEGIN  
		SELECT
			T2.mOwner
			, T2.mNo
			, T2.mRegDate AS mPcCreateDate
			, T2.mNm
			, T1.mRegDate AS mRegDate
			, T1.mCnt
			, T1.mDiffCnt
		FROM dbo.TBL_SILVER_LOG_PC AS T1
			INNER JOIN dbo.TblPc AS T2
		ON T1.mPcNo = T2.mNo
		WHERE T1.mRegDate >= DATEADD(mi, -@pDiffTime, GETDATE())
		AND T1.mDiffCnt >= ( @pCnt * 10000 )
		ORDER BY T1.mRegDate ASC;  
	 END   
	 ELSE  
	 BEGIN  
		  SELECT
				T2.mOwner
				, T2.mNo
				, T2.mRegDate AS mPcCreateDate
				, T2.mNm
				, T1.mRegDate AS mRegDate
				, T1.mCnt
				, T1.mDiffCnt
			FROM dbo.TBL_SILVER_LOG_PC AS T1
				INNER JOIN dbo.TblPc AS T2
			ON T1.mPcNo = T2.mNo
		  WHERE T1.mRegDate BETWEEN @pStartDate AND @pEndDate + 1
		  AND T1.mDiffCnt >= ( @pCnt * 10000 )
		  ORDER BY T1.mRegDate ASC;  
	 END  

END

GO

