/******************************************************************************
**		Name: UspGetPcQuestVisit
**		Desc: 俺么瘤 沤规 涅胶飘 惑怕甫 啊廉柯促.
**
**		Auth: 沥柳龋
**		Date: 2010-04-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcQuestVisit]
     @pPcNo			INT
	,@pQuestNo		INT	 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
			mPlaceNo,
			mVisit
    FROM dbo.TblPcQuestVisit
    WHERE mPcNo = @pPcNo 
		AND mQuestNo = @pQuestNo

GO

