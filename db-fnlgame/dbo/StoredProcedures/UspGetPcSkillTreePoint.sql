/******************************************************************************  
**  File: 
**  Name: UspGetPcSkillTreePoint  
**  Desc: PC의 스킬트리 포인트 얻기
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.28 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcSkillTreePoint]
	 @pPcNo		INT,
	@pPoint	SMALLINT OUTPUT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SET @pPoint = 0

	declare @aPoint smallint
	select @aPoint = mSkillTreePoint from TblPcState
	where mNo = @pPcNo
	
	if @aPoint is not null
	begin
		SET @pPoint = @aPoint
	end

	return 0

GO

