/******************************************************************************
**		Name: UspAddRacingTicket
**		Desc: 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2011-07-05	정진호				티켓 갯수를 저장한다.
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddRacingTicket]
	 @pSerialNo		BIGINT
	,@pPlace		INT
	,@pStage		INT
	,@pNID			INT
	,@pTicketCnt	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	INSERT TblRacingTicket(mSerialNo, mPlace, mStage, mNID, mTicketCnt)
	VALUES(@pSerialNo, @pPlace, @pStage, @pNID, @pTicketCnt)
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @aErrNo	= 1
	 END
	 
	RETURN(@aErrNo)

GO

