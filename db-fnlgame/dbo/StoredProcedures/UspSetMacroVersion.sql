/******************************************************************************
**		Name: UspSetMacroVersion
**		Desc: ¸ÅÅ©·Î ÄÁÅÙÃ÷ ¹öÀü ÀúÀå
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetMacroVersion]
	@pPcNo		INT
	,@pVersion	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF EXISTS (SELECT mVersion
				FROM dbo.TblMacroContentVersion
				WHERE mPcNo = @pPcNo)
	BEGIN
		UPDATE dbo.TblMacroContentVersion
		SET mVersion = @pVersion
		WHERE mPcNo = @pPcNo
	END
	ELSE
	BEGIN
		INSERT INTO  dbo.TblMacroContentVersion (mPcNo, mVersion)
		VALUES (@pPcNo, @pVersion)
	END

GO

