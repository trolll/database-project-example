/******************************************************************************
**		Name: UspGetServantSubList
**		Desc: 辑宏 辑锅飘 沥焊 掘扁
**
**		Auth: 捞侩林
**		Date: 2016-04-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetServantSubList]
	@pPcNo			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	mSerialNo, mSubIndex
	FROM dbo.TblPcServantSub
	WHERE  mPcNo = @pPcNo

	SET NOCOUNT OFF;

GO

