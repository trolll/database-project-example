-----------------------------------------------------------------------------------------------------------------
-- SP		: WspSetCastleStower
-- DESC		: ╜║╞╠└╟ ┴í╖╔▒µ╡σ╕ª ║»░µ╟╤┤┘.(▒Γ┴╕┐í ┴í╖╔└┌░í ╛╞┤╤ ╗⌡╖╬┐ε ┴í╖╔└┌╖╬) // UspSetCastleStower└╗ ╗τ┐δ
-- ╗²╝║└╧└┌	: 2006-10-18
-- ╗²╝║└┌	: ▒Φ▒ñ╝╖
-- ErrNo	: UspGetCastleStower └╟ ErrNo ┬ⁿ┴╢
-- ErrNo 5	: ▒µ╡σ╕φ┐í ╟╪┤τ╟╧┤┬ ▒µ╡σ░í ┴╕└τ╟╧┴÷ ╛╩┤┬┤┘
-----------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[WspSetCastleStower]
	 @pPlaceNo	INT				-- ┴÷┐¬ ╣°╚ú
	,@pGuildNm	VARCHAR(20)		-- ▒µ╡σ └╠╕º
AS
	SET NOCOUNT ON
	
	DECLARE	@aErrNo		INT
	DECLARE @pGuildNo	INT
	
	SELECT @aErrNo = 2, @pGuildNo = 0
	SELECT @pGuildNo = mGuildNo FROM TblGuild WITH(NOLOCK) WHERE mGuildNm = @pGuildNm	
	
	IF(@pGuildNo <> 0)
	 BEGIN
		EXEC @aErrNo = UspSetCastleStower @pPlaceNo, @pGuildNo, 1
	 END
	
	
	RETURN(@aErrNo)

GO

