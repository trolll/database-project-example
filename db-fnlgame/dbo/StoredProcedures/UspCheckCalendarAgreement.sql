/******************************************************************************
**		Name: UspCheckCalendarAgreement
**		Desc: ½ÂÀÎµÇ¾î ÀÖ´ÂÁö, ½ÂÀÎ ÃÖ´ëÀÎÁö Ã¼Å©
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckCalendarAgreement]
	 @pOwnerPcNo		INT 
	,@pOppPcNo			INT
	,@pMaxCnt			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aCnt		INT
	SELECT	@aCnt		= 0

	IF EXISTS (
					SELECT  *
					FROM dbo.TblCalendarAgreement
					WHERE mOwnerPcNo = @pOwnerPcNo AND mMemberPcNo = @pOppPcNo
				)
	BEGIN
		RETURN(1);
	END 

	SELECT @aCnt = COUNT(*)
	FROM dbo.TblCalendarAgreement
	WHERE mOwnerPcNo = @pOwnerPcNo

	IF @pMaxCnt <= @aCnt
	BEGIN
		RETURN(2);
	END

	RETURN(0);

GO

