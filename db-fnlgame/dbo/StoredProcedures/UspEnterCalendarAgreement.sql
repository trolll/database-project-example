/******************************************************************************
**		Name: UspEnterCalendarAgreement
**		Desc: ½ÂÀÎ µî·Ï
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspEnterCalendarAgreement]
	 @pOwnerPcNo		INT 
	,@pOppPcNo			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- ³ª¿Í »ó´ëÆí ¸ðµÎ µî·ÏµÇ¾î¾ß ÇÑ´Ù.
	IF EXISTS ( SELECT  *
					FROM dbo.TblCalendarAgreement
					WHERE mOwnerPcNo = @pOwnerPcNo AND mMemberPcNo = @pOppPcNo)
	BEGIN
		RETURN(1);
	END 

	IF EXISTS ( SELECT  *
					FROM dbo.TblCalendarAgreement
					WHERE mOwnerPcNo = @pOppPcNo AND mMemberPcNo = @pOwnerPcNo)
	BEGIN
		RETURN(1);
	END 

	BEGIN TRAN
		INSERT INTO dbo.TblCalendarAgreement(mRegDate, mOwnerPcNo, mMemberPcNo)
		VALUES(GETDATE(), @pOwnerPcNo, @pOppPcNo)

		IF (@@ERROR <> 0) OR (@@ROWCOUNT <= 0)
		BEGIN
			ROLLBACK TRAN;		
			RETURN(1);
		END

		INSERT INTO dbo.TblCalendarAgreement(mRegDate, mOwnerPcNo, mMemberPcNo)
		VALUES(GETDATE(), @pOppPcNo, @pOwnerPcNo)

		IF (@@ERROR <> 0) OR (@@ROWCOUNT <= 0)
		BEGIN
			ROLLBACK TRAN;		
			RETURN(1);
		END
	
	COMMIT TRAN

	RETURN(0);

GO

