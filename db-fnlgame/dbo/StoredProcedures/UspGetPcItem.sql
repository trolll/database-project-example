/******************************************************************************
** Name: UspGetPcItem
** Desc: PC°¡ ¼ÒÀ¯ÇÑ ¾ÆÀÌÅÛ ¸®½ºÆÃ
**
**
** Return values:
** ·¹ÄÚµå¼Â
**
**
** Author:
** Date:
*******************************************************************************
** Change History
*******************************************************************************
** Date: Author: Description:
** -------- -------- ---------------------------------------
** 2011 0615 dmbkh mHoleCount Ãß°¡
** 2012 1116 ³²±âºÀ ±â°£ÀÌ Áö³­ ÀåºñÀÇ ±¸½½ È¯¿ø Ãß°¡.
** 2015 0512 ÀÌ¿ëÁÖ ROWCOUNT 160 --> TOP 160 / TOP 20 (¼­¹øÆ®¿ë) ¹× À¯´Ï¿ÂÀ¸·Î ¹ÝÈ¯
** 2015 0710 ÀÌ¿ëÁÖ ÀÓ½ÃÅ×ÀÌºí ÀÚ·áÇü ¹®Á¦°¡ ÀÖ¾î¼­ ¹Ù·Î UNION ALL
** 2015 0710 ÀÌ¿ëÁÖ WITH(INDEX(CL_TblPcInventory), NOLOCK ), WITH(INDEX(UCL_PK_TblPcServant), NOLOCK )¸¦ Á¦°ÅÇØµµ ÀÎµ¦½º·Î Å½»öÇÏ¹Ç·Î Á¤¸®
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcItem]
	@pPcNo INT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @pPcNo = 1 OR @pPcNo = 0
	RETURN(0) -- NPC, ¹ö·ÁÁø ¾ÆÀÌÅÛ

	DECLARE @TempTable table (
		mID int IDENTITY PRIMARY KEY,
		mSerialNo bigint NOT NULL,
		mBeadIndex tinyint NOT NULL,
		mPcNo int NOT NULL);


	-- ¾ÆÀÌÅÛ »èÁ¦Àü ¿¬°üµÈ ·é Á¤º¸ ÀúÀå.
	INSERT INTO @TempTable
	SELECT TOP (160) dbo.TblPcInventory.mSerialNo, dbo.TblPcBead.mBeadIndex, dbo.TblPcInventory.mPcNo
	FROM dbo.TblPcBead INNER JOIN dbo.TblPcInventory ON dbo.TblPcInventory.mSerialNo = dbo.TblPcBead.mOwnerSerialNo
	WHERE dbo.TblPcInventory.mEndDate <= CONVERT(SMALLDATETIME, GETDATE())
		AND dbo.TblPcBead.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
		AND dbo.TblPcInventory.mPcNo = @pPcNo


	-- '·é' ¿ø»ó º¹±Í.
	DECLARE @mNo int,
		@maxNo int,
		@mSerialNo bigint,
		@mBeadIndex tinyint,
		@mPcNo int,
		@mBeadSerialNo bigint;

	SELECT @mNo = MIN(mID), @maxNo = MAX(mID) FROM @TempTable
	IF @@ROWCOUNT > 0
	BEGIN
		WHILE @mNo <= @maxNo
		BEGIN
			SELECT TOP (160) @mSerialNo = mSerialNo, @mBeadIndex = mBeadIndex, @mPcNo = mPcNo
			FROM @TempTable WHERE mID = @mNo
			EXEC dbo.UspReturnBead @mSerialNo, @mBeadIndex, @mPcNo, @mBeadSerialNo OUTPUT
		SET @mNo = @mNo + 1
		END
	END
	-- '·é' ¿ø»ó º¹±Í ¿Ï·á.

	-- Pc Item Á¶È¸
	SELECT * FROM (SELECT TOP (160)
		mSerialNo
		, mItemNo
		, mEndDate
		, mCnt
		, mIsConfirm
		, mStatus
		, mCntUse
		, mIsSeizure
		, mApplyAbnItemNo
		, mApplyAbnItemEndDate -- mi
		, mOwner
		, mPracticalPeriod
		, mBindingType
		, mRestoreCnt
		, mHoleCount
	FROM (
		SELECT
			dbo.TblPcInventory.mSerialNo
			, mItemNo
			, DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
			, mCnt
			, mIsConfirm
			, mStatus
			, mCntUse
			, mIsSeizure
			, mApplyAbnItemNo
			, DATEDIFF(mi,GETDATE(),mApplyAbnItemEndDate) mApplyAbnItemEndDate
			, mOwner
			, mPracticalPeriod
			, mBindingType
			, mRestoreCnt
			, mOrderNo =
				CASE
				WHEN mItemNo = 1136 THEN 1
				WHEN mItemNo = 1135 THEN 2
				WHEN mItemNo = 722 THEN 3
				WHEN mItemNo = 724 THEN 4
				ELSE
				0
				END
			, mHoleCount
		FROM dbo.TblPcInventory
			LEFT OUTER JOIN dbo.TblPcServant
			ON dbo.TblPcInventory.mSerialNo = dbo.TblPcServant.mSerialNo
		WHERE dbo.TblPcInventory.mPcNo = @pPcNo
			AND dbo.TblPcServant.mSerialNo IS NULL
			AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
			AND mCnt > 0
		) T1
	ORDER BY mOrderNo, mSerialNo ASC
	) T2
	-- Servant Item Á¶È¸
	UNION ALL
	SELECT TOP (20)
		dbo.TblPcInventory.mSerialNo
		, mItemNo
		, DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
		, mCnt
		, mIsConfirm
		, mStatus
		, mCntUse
		, mIsSeizure
		, mApplyAbnItemNo
		, DATEDIFF(mi,GETDATE(),mApplyAbnItemEndDate) mApplyAbnItemEndDate -- mi
		, mOwner
		, mPracticalPeriod
		, mBindingType
		, mRestoreCnt
		, mHoleCount
	FROM dbo.TblPcInventory
		INNER JOIN dbo.TblPcServant
		ON dbo.TblPcInventory.mSerialNo = dbo.TblPcServant.mSerialNo
	WHERE dbo.TblPcInventory.mPcNo = @pPcNo
		AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
		AND mCnt > 0

GO

