CREATE PROCEDURE [dbo].[WspGetTransferPcRepration]
	@pPcNo	INT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		mPcNo,
		mError,
		mSerialNo,
		mEndDateTm,
		mItemNo,
		T2.IName
	FROM dbo.T_TblItemRepration T1
		LEFT OUTER JOIN FNLParm.dbo.DT_ITEM T2
			ON T1.mItemNo = T2.IID
	WHERE mPcNo = @pPcNo

GO

