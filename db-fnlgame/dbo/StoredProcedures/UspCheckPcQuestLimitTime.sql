/******************************************************************************
**		Name: UspCheckPcQuestLimitTime
**		Desc: 韤橃姢韸?鞁滉皠鞝滍暅 觳错伂
**
**		Auth: 鞝曥順?
**		Date: 2010-02-12
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspCheckPcQuestLimitTime]
	  @pPcNo		INT
	 ,@pQuestNo		INT
	 ,@pLimitTime	INT	OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @pLimitTime = 0;
			
    SELECT 
			@pLimitTime = 
					CASE WHEN mLimitTime IS NULL THEN 0 
						ELSE DATEDIFF(ss,GETDATE(),  mLimitTime )	
					END 
    FROM dbo.TblPcQuest
    WHERE mPcNo = @pPcNo 
		AND mQuestNo = @pQuestNo

	RETURN(0)

GO

