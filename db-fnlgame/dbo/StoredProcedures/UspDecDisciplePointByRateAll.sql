/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspDecDisciplePointByRateAll    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspDecDisciplePointByRateAll]
	 @pDecRate		INT	-- ??? ??? (%)
	,@pMinPoint		INT	-- ?? ??? ?? ???
	,@pExpLev1		INT	-- ??? ??? (??1)
	,@pExpLev2		INT	-- ??? ??? (??2)
	,@pExpLev3		INT	-- ??? ??? (??3)
	,@pExpLev4		INT	-- ??? ??? (??4)
	,@pExpLev5		INT	-- ??? ??? (??5)
	,@pExpSumLev1	INT	-- ??? ??? ?? (??1)
	,@pExpSumLev2	INT	-- ??? ??? ?? (??2)
	,@pExpSumLev3	INT	-- ??? ??? ?? (??3)
	,@pExpSumLev4	INT	-- ??? ??? ?? (??4)
	,@pExpSumLev5	INT	-- ??? ??? ?? (??5)
--------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aCurDate DATETIME
	DECLARE @aErrNo INT,
			@aRowCnt INT,
			@aDecPoint INT,
			@aMaxPoint INT	
						
	SELECT	@aErrNo = 0, @aCurDate = GETDATE(), @aRowCnt = 0,
			@aMaxPoint = 0


	UPDATE dbo.TblDisciple 
	SET 
		mCurPoint = 
			CASE
				WHEN (mCurPoint - T2.mDescPoint) < @pMinPoint THEN @pMinPoint
				ELSE
					 (mCurPoint - T2.mDescPoint)
			END									
		, mDecDate = @aCurDate 		
	FROM dbo.TblDisciple T1
		INNER JOIN (			
			SELECT 
				mMaster,
				mDescPoint = 
					CASE 					
						WHEN mCurPoint < @pExpSumLev1 
							THEN @pExpLev1 * @pDecRate / 100
						WHEN mCurPoint >= @pExpSumLev1 AND mCurPoint <= @pExpSumLev2 
							THEN @pExpLev2 * @pDecRate / 100
						WHEN mCurPoint >= @pExpSumLev2 AND mCurPoint <= @pExpSumLev3 
							THEN @pExpLev3 * @pDecRate / 100
						WHEN mCurPoint >= @pExpSumLev3 AND mCurPoint <= @pExpSumLev4 
							THEN @pExpLev4 * @pDecRate / 100
						WHEN mCurPoint > @pExpSumLev4 
							THEN @pExpLev5 * @pDecRate / 100
					END						
			FROM dbo.TblDisciple
			WHERE mCurPoint > @pMinPoint 
				AND DATEDIFF(HH, mDecDate, @aCurDate) >= 24				
		) T2
			ON	T1.mMaster = T2.mMaster
	WHERE T1.mCurPoint > @pMinPoint 
		AND DATEDIFF(HH, T1.mDecDate, @aCurDate) >= 24

	SELECT @aErrNo = @@ERROR
	
	IF @aErrNo <> 0
		RETURN (1)
	
	RETURN(0)

GO

