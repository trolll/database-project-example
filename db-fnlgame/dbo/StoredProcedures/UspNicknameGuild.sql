/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspNicknameGuild    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspNicknameGuild]
	 @pPcNo			INT
	,@pNickNm		CHAR(16)
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
		
	UPDATE dbo.TblGuildMember 
	SET 
		[mNickNm]=@pNickNm
	WHERE ([mPcNo]=@pPcNo)
	IF(0 <> @@ERROR) OR (1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

