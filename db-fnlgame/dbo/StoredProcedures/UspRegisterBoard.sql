/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspRegisterBoard    Script Date: 2008-11-10 10:37:17 ******/





CREATE Procedure [dbo].[UspRegisterBoard]
	 @pBoardId		TINYINT
	,@pPcNm			CHAR(12)
	,@pTitle		CHAR(30)
	,@pMsg			VARCHAR(512)
	,@pRegDate		DATETIME	OUTPUT
	,@pBoardNo		INT			OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	SET @pRegDate = GETDATE()
	INSERT TblBoard([mRegDate], [mBoardId], [mFromPcNm], [mTitle], [mMsg])
		VALUES(@pRegDate, @pBoardId, @pPcNm, @pTitle, @pMsg)
	SET @pBoardNo = @@IDENTITY
	IF(0 <> @@ERROR)
	 BEGIN
		SET	@aErrNo	= 1	 
	 END
			
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

