/******************************************************************************
**		Name: UspResetAllGuildSiegeDfnsLv
**		Desc: ±æµå Á¡·É ÇýÅÃ ·¹º§ º¯°æ
**
**		Auth: ±è°­È£
**		Date: 2009.10.29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 		Author:				Description: 
**		--------	--------			---------------------------------------
**		2009.11.16	±è°­È£				pResetType Ãß°¡(0 - ÀüºÎ(default), 1 - mCastleDfnsLv ÃÊ±âÈ­, 2 - mSpotDfnsLv ÃÊ±âÈ­)
**		2014.09.24	°ø¼®±Ô				pResetType Ãß°¡(0 - ÀüºÎ¿¡ mEtelrCastleDfnsLv Æ÷ÇÔ, 3 - mEtelrCastleDfnsLv ÃÊ±âÈ­, 4 - (1, 2) ÃÊ±âÈ­)
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspResetAllGuildSiegeDfnsLv]
	@pResetType INT
AS  
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	DECLARE @aErrNo	INT  
	SET @aErrNo	= 0   

	IF (@pResetType = 0)
	BEGIN 
		UPDATE dbo.TblGuild SET mCastleDfnsLv = 0
								, mSpotDfnsLv = 0
								, mEtelrCastleDfnsLv = 0
	END
	ELSE IF (@pResetType = 1)
	BEGIN
		UPDATE dbo.TblGuild SET mCastleDfnsLv = 0
	END
	ELSE IF (@pResetType = 2)
	BEGIN
		UPDATE dbo.TblGuild SET mSpotDfnsLv = 0
	END
	ELSE IF (@pResetType = 3)
	BEGIN
		UPDATE dbo.TblGuild SET mEtelrCastleDfnsLv = 0
	END
	ELSE IF (@pResetType = 4)
	BEGIN
		UPDATE dbo.TblGuild SET mCastleDfnsLv = 0
								, mSpotDfnsLv = 0
	END

	SELECT @aErrNo = @@ERROR

	RETURN @aErrNo

GO

