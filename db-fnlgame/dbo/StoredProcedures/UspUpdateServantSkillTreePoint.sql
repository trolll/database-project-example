/******************************************************************************
**		Name: UspUpdateServantSkillTreePoint
**		Desc: ¼­¹øÆ® ½ºÅ³ Æ®¸® Æ÷ÀÎÆ® ÇöÈ² °»½Å
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateServantSkillTreePoint]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pSkillTreePoint	INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE dbo.TblPcServant 
	SET mSkillTreePoint = @pSkillTreePoint
	WHERE mPcNo = @pPcNo
	  AND mSerialNo = @pSerialNo

	SET NOCOUNT OFF;

GO

