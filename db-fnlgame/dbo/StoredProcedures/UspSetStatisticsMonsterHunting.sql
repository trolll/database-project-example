/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetStatisticsMonsterHunting    Script Date: 2008-11-10 10:37:18 ******/





CREATE PROCEDURE [dbo].[UspSetStatisticsMonsterHunting]
	  @pMID			INT
	, @pHuntingCnt	BIGINT
AS
	SET NOCOUNT ON
	
	DECLARE @aErrNo INT
	SET @aErrNo = 0
	
	INSERT INTO dbo.TblStatisticsMonsterHunting ([MID], [mHuntingCnt]) 
	VALUES (@pMID, @pHuntingCnt)
		
	IF(@@ERROR <> 0)
		SET @aErrNo = 2 -- DB Error

	
	RETURN(@aErrNo)

GO

