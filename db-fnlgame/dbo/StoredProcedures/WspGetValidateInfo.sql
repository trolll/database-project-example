/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2009-7-14 13:13:32 ******/

/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2009-5-12 9:18:19 ******/  
  
/****** Object:  Stored Procedure dbo.WspGetValidateInfo    Script Date: 2008-11-10 10:37:20 ******/  
  
CREATE PROCEDURE [dbo].[WspGetValidateInfo]  
 @charNm NVARCHAR(20),  
 @str NVARCHAR(20),  
 @type int,  
 @gameUserId nvarchar(30)  
AS  
  
     SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
  
     declare @pUserNo int  
     declare @sql nvarchar(1000)  
      
     set @pUserNo = 0  
       
     if @type = 0                     --判?角色是否存在  
 begin  
  exec  [FNLAccount].dbo.UspGetUserNo @pUserId=@gameUserId,@pUserNo=@pUserNo output            
  if @pUserNo = 0  
   begin                   
    return 0                        
   end  
  else  
   begin  
    if exists (select mNm from tblpc where mNm = @charNm and mOwner = @pUserNo and mDelDate IS NULL)  
     begin                        
      return 1  
     end   
    else  
     begin  
      return 0  
     end                                 
   end                   
 end  
     
   if @type = 2                     --?件是玩家等?是否?到?定??  
 begin  
 if exists (select b.mlevel from tblpc a,tblpcstate b where a.mNo = b.mNo and a.mNm = @charNm and b.mlevel >= convert(smallint, @str))   
  begin  
   return 1  
  end  
 else  
  begin  
   return 0  
  end  
 end  
   
   if @type = 3                    
 begin  
 if @str = 'Y'                                                 ---?件是是公???或建立者  
  begin  
   if exists (select a.mNm from tblPc a,tblGuildMember b where a.mNo = b.mPcNo and a.mNm = @charNm )   
    begin  
     return 1  
    end  
   else  
    begin  
     return 0  
    end  
  end     
   
 if @str = 'N'                                                     ---?件是非公???和建立者  
  begin  
   if exists (select a.mNm from tblPc a,tblGuildMember b where a.mNo = b.mPcNo and a.mNm = @charNm )   
    begin  
     return 0  
    end  
   else  
    begin  
     return 1  
    end  
   end            
  end  
  
      if @type = 5                    ---?件是登?日期小于?定日期  
 begin  
  if exists (select b.mloginTm from tblpc a,tblpcstate b where a.mNo = b.mNo and a.mNm = @charNm and b.mLoginTm < convert(datetime,@str,120) )   
   begin  
    return 1  
   end  
  else  
   begin  
    return 0  
   end  
 end

GO

