/******************************************************************************  
**  File: UspGetListFromStore.sql  
**  Name: UspGetListFromStore  
**  Desc: 창고 아이템을 가져온다.  
**  
**  
**  Auth:   
**  Date:   
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  2006.11.29 JUDY    WHERE 조건형 부분 수정  
**  2007.05.07 JUDY    아이템 분리 적용  
**  2007.10.01 soundkey   소유자 정보 추가  
**  2008.01.07 JUDY    mPracticalPeriod 효과 지속 시간 추가  
**  2009.04.17 JUDY    인덱스 힌트 추가  
**  2011.07.12 dmbkh	mHoleCount 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetListFromStore]  
  @pOwner  INT  
AS  
 SET NOCOUNT ON  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
   
 SET ROWCOUNT 300   
  
 SELECT   
  [mSerialNo],   
  [mItemNo],   
  [mIsConfirm],   
  [mStatus],   
  [mCnt],   
  [mCntUse],  
  [mOwner], -- 소유자 정보 추가.   
  [mPracticalPeriod], -- 효과지속시간 추가  
  [mHoleCount]
 FROM dbo.TblPcStore WITH(INDEX(CL_TblPcStore), NOLOCK )  
 WHERE mUserNo = @pOwner  
   AND mEndDate >  CONVERT(SMALLDATETIME, GETDATE())  
 ORDER BY mItemNo

GO

