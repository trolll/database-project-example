/******************************************************************************
**		Name: UspGetMacroCommand
**		Desc: ¸ÅÅ©·Î Ä¿¸Çµå
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetMacroCommand]
	@pPcNo		INT
	,@pMacroID	TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mOrderNo, mRefType, mRefID, mRefParm
	FROM dbo.TblMacroCommand
	WHERE mPcNo = @pPcNo
	AND mMacroID = @pMacroID

GO

