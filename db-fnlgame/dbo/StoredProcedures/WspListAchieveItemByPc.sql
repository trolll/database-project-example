/******************************************************************************  
**  Name: WspListAchieveItemByPc  
**  Desc: 某腐磐 家蜡 林拳/傈府前 酒捞袍 炼雀  
**        FNLParmDev OR FNLParmTest 狼 惑炔喊 炼例 鞘夸
**  
**  Auth: 巢扁豪  
**  Date: 04/15/2013
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:  Author:    Description:  
**  -------- --------   ---------------------------------------  
**  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[WspListAchieveItemByPc]
(
	@pCharNm VARCHAR(12)
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET ROWCOUNT 1000; -- 弥措 啊廉棵 荐 乐绰 酒捞袍 荐甫 力茄   
  
	IF NOT EXISTS (SELECT mNo FROM dbo.TblPc WHERE mNm = @pCharNm)
	BEGIN
		RETURN 1;
	END


	DECLARE @TempTable table (
		mPcNo int,
		mSerialNo bigint,
		mItemNo int,
		mRegDate datetime,
		mAchieveGuildID int,
		mCoinPoint int,
		mSlotNo tinyint,
		mAchieveID tinyint,
		mExp int,
		mLimitLevel int,
		mIsSeizure bit,
		mIsTrophy bit,
		mIsEquip bit);

	INSERT INTO @TempTable
		SELECT T2.mPcNo, T2.mSerialNo, T2.mItemNo, T2.mRegDate, T2.mAchieveGuildID,
			   T2.mCoinPoint, T2.mSlotNo, T2.mAchieveID, T2.mExp, T2.mLimitLevel, T2.mIsSeizure, 0, 0
		FROM dbo.TblPc AS T1 
		INNER JOIN dbo.TblPcAchieveInventory AS T2 
		ON T1.mNo = T2.mPcNo 
		WHERE T1.mNm = @pCharNm
	
	IF @@ERROR <> 0
		RETURN 2;

	UPDATE T1 SET T1.mIsEquip = 1 
	FROM @TempTable AS T1 INNER JOIN dbo.TblPcAchieveEquip AS T2
	ON T1.mSerialNo = T2.mSerialNo 
	WHERE T1.mPcNo = T2.mPcNo
	IF @@ERROR <> 0
		RETURN 3;

	UPDATE @TempTable SET mIsTrophy = 1 
	WHERE mAchieveID <> 0
	IF @@ERROR <> 0
		RETURN 3;

	-------------------------------------------------------  
	-- 酒捞袍 沥焊甫 馆券  
	-------------------------------------------------------
	SELECT
		@pCharNm AS mNm			-- 某腐磐 捞抚
		,T2.IName				-- 林拳/傈府前 捞抚
		,T1.mSerialNo			-- 林拳/傈府前 GUID
		,T1.mItemNo				-- 酒捞袍 ID
		,T1.mRegDate			-- 殿废老
		,T1.mAchieveGuildID		-- 诀利 辨靛 ID
		,T1.mCoinPoint			-- 林拳 器牢飘
		,T1.mSlotNo				-- 浇吩 困摹
		,T1.mIsTrophy			-- 傈府前 咯何
		,T1.mAchieveID			-- 积己等 傈府前苞 楷拌等 诀利 ID
		,T1.mExp				-- 傈府前 版氰摹
		,T1.mLimitLevel			-- 傈府前 弥措 饭骇
		,T1.mIsEquip			-- 傈府前 馒侩 咯何
		,T1.mIsSeizure			-- 拘幅 咯何
	FROM @TempTable AS T1
		INNER JOIN FNLParm.dbo.DT_Item AS T2
	ON T1.mItemNo = T2.IID

	RETURN 0;
END

GO

