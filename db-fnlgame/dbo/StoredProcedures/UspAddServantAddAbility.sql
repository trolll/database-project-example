/******************************************************************************
**		Name: UspAddServantAddAbility
**		Desc: 辑锅飘 钦己 眠啊 瓷仿摹 盎脚
**
**		Auth: 捞侩林
**		Date: 2016-10-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddServantAddAbility]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pAddStr		SMALLINT
	,@pAddDex		SMALLINT
	,@pAddInt		SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNo		INT
	DECLARE @aRowCnt	INT

	UPDATE dbo.TblPcServant
	SET mAddStr = mAddStr + @pAddStr
	  , mAddDex = mAddDex + @pAddDex
	  , mAddInt = mAddInt + @pAddInt
	  , mCombineCount = mCombineCount + 1
	WHERE mPcNo = @pPcNo
	  AND mSerialNo = @pSerialNo

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt = 0
	BEGIN
		RETURN(@aErrNo)
	END

	SET NOCOUNT OFF;

GO

