/******************************************************************************  
**  Name: UspCnsmListPcReg  
**  Desc: 본인 등록 물품 리스팅
**  
**                
**  Return values:  
**   레코드셋
**   
**                
**  Author: 김강호  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**   2011 0712  김강호     레코드셋에 ItemSn, HoleCount 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmListPcReg]
 @pPcNo				INT
 ,@pBalance			BIGINT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;  
	SET XACT_ABORT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	SELECT 	@pBalance = mBalance
	FROM dbo.TblConsignmentAccount
	WHERE mPcNo=@pPcNo;
	IF @pBalance IS NULL
	BEGIN
		SET @pBalance = 0;
	END


	declare @aCurDate smalldatetime;
	SET @aCurDate = getdate();
	select a.mCnsmNo, a.mCategoryNo, a.mPrice, 
			CASE WHEN a.mTradeEndDate <= @aCurDate
			THEN 0 ELSE
				DATEDIFF(minute, getdate(), a.mTradeEndDate) END, 
			--a.mPcNo, -- @pPcNo와 같으므로 필요가 없다.
			a.mRegCnt, a.mCurCnt, a.mItemNo,
			b.mStatus, b.mCntUse, b.mOwner, b.mPracticalPeriod
			, b.mSerialNo, b.mHoleCount
	from dbo.TblConsignment a 
			inner join dbo.TblConsignmentItem b 
					on a.mCnsmNo = b.mCnsmNo
	where a.mPcNo=@pPcNo;
END

GO

