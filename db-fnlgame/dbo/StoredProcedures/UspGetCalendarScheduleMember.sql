/******************************************************************************
**		Name: UspGetCalendarScheduleMember
**		Desc: ÀÏÁ¤¿¡ µî·ÏµÈ ¸â¹ö¸ñ·Ï (ÀÏÁ¤ÀÌ »èÁ¦µÇ¾ú´Ù´Â°É ¾Ë·ÁÁÙ¶§ »ç¿ë)
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-04-07
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetCalendarScheduleMember]
	 @pOwnerPcNo		INT 
	,@pSerialNo			BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT DISTINCT T1.mMemberPcNo
	FROM dbo.TblCalendarGroupMember AS T1
		 INNER JOIN dbo.TblPc AS T2 
	ON T1.mMemberPcNo = T2.mNo
	WHERE mOwnerPcNo = @pOwnerPcNo 
	AND mGroupNo IN (
					SELECT mGroupNo
					FROM dbo.TblCalendarSchedulegroup
					WHERE mSerialNo = @pSerialNo
						AND mOwnerPcNo = @pOwnerPcNo
					)

GO

