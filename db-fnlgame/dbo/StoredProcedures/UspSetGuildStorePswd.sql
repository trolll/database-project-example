/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2009-6-1 15:32:40 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildStorePswd    Script Date: 2008-11-10 10:37:20 ******/



CREATE PROCEDURE [dbo].[UspSetGuildStorePswd]
	  @pGuildNo	INT
	, @pPswd		CHAR(8)
	, @pStoreLv	TINYINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE	 @aRowCnt		INT
	DECLARE  @aErr			INT

	SELECT  @aRowCnt = 0, @aErr = 0

	IF NOT EXISTS (	SELECT * 
					FROM dbo.TblGuild 
					WHERE mGuildNo = @pGuildNo)
	BEGIN
		RETURN(1)	-- ??? ??
	END 
	
	UPDATE dbo.TblGuildStorePassword
	SET 
		mPassword = @pPswd 
	WHERE mGuildNo = @pGuildNo 
			AND mGrade = @pStoreLv
	
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT	

	IF(@aErr <> 0)
		RETURN(3)	-- db error
		
	IF(@aRowCnt >= 1 AND @aErr = 0)	-- effect row 1 over
		RETURN(0)
		
	INSERT INTO dbo.TblGuildStorePassword (mGuildNo, mGrade, mPassword) 
	VALUES(@pGuildNo, @pStoreLv, @pPswd)
	
	SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT	

	IF(@aErr <> 0) OR ( @aRowCnt <= 0 )		
		RETURN(2)	-- db error 

	RETURN(0)

GO

