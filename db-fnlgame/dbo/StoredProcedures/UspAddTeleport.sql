/******************************************************************************
**		Name: UspAddPcTeleport
**		Desc: 扁撅府胶飘 眠啊
**		Test:			
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      2013/04/24  巢己葛				mName -> 扁粮 char(20)俊辑 varchar(50)栏肺 荐沥			
**										int mType, int mLevel, varchar(50) mOrgName 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddTeleport]
	 @pPcNo			INT			-- 1捞搁 阁胶磐促. 八荤侩烙.
	,@pName			VARCHAR(50)  
	,@pMapNo		INT  
	,@pPosX			REAL  
	,@pPosY			REAL  
	,@pPosZ			REAL
	,@pType			INT
	,@pLevel		INT
	,@pOrgName		VARCHAR(50)  
	,@pNo			INT OUTPUT
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	INSERT dbo.TblPcTeleport([mPcNo], [mName], [mMapNo], [mPosX], [mPosY], [mPosZ], [mType], [mLevel], [mOrgName])   
	VALUES(@pPcNo, @pName, @pMapNo, @pPosX, @pPosY, @pPosZ, @pType, @pLevel, @pOrgName)  
	
	SET @pNo = @@IDENTITY

	IF(0 <> @@ERROR)
	BEGIN 
		RETURN(1)
	END

	RETURN(0)

GO

