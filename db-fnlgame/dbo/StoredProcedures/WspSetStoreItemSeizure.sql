CREATE PROCEDURE [dbo].[WspSetStoreItemSeizure]
	@mNo INT, -- ├ó░φ╣°╚ú
	@mIsSeizure BIT -- ╛╨╖∙┐⌐║╬
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


	UPDATE dbo.TblPcStore
	SET
		mIsSeizure = @mIsSeizure
	WHERE mUserNo = @mNo

	IF @@ERROR <> 0
		RETURN 1
	
	RETURN 0

GO

