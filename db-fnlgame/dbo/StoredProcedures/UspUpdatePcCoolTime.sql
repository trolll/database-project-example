/****** Object:  Stored Procedure dbo.UspUpdatePcCoolTime    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePcCoolTime    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePcCoolTime    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.UspUpdatePcCoolTime    Script Date: 2010-12-23 17:46:01 ******/
/******************************************************************************
**		Name: UspUpdatePcCoolTime
**		Desc: 巢篮 酿鸥烙 矫埃阑 盎脚茄促.
**
**		Auth: 沥备柳
**		Date: 09.09.30
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	-----------------------------------------------
**      荐沥老      荐沥磊              荐沥郴侩    
*******************************************************************************/
CREATE    PROCEDURE [dbo].[UspUpdatePcCoolTime]
	@pPcNo				INT,
	@pCoolTimeGroup		SMALLINT,
	@pRemainTime		INT
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	UPDATE dbo.TblPcCoolTime
	SET mRemainTime = @pRemainTime
	WHERE mPcNo = @pPcNo AND mCoolTimeGroup = @pCoolTimeGroup

	IF(@@ERROR <> 0)
	BEGIN
		RETURN(1)
	END

	RETURN(0)

GO

