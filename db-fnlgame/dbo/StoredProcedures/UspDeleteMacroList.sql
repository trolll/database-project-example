/******************************************************************************
**		Name: UspDeleteMacroList
**		Desc: ¸ÅÅ©·Î ¸®½ºÆ® »èÁ¦
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteMacroList]
	@pPcNo			INT
	,@pMacroID		TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DELETE dbo.TblMacroList
	WHERE mPcNo = @pPcNo
	AND mMacroID = @pMacroID

GO

