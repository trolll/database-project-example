/******************************************************************************
**		Name: UspSetGuildMandateToSubMaster
**		Desc: Guild鞚?攵€旮鸽鞐愱矊 甓岉暅 鞙勳瀯 靹れ爼.
**
**		Auth: 臁办劯順?
**		Date: 2010.03.04
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetGuildMandateToSubMaster]
	  @pGuildNo			INT
	, @pIsSet			BIT
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	UPDATE [dbo].[TblGuild] 
	SET 
		[mMandateSubMaster] = @pIsSet 
	WHERE [mGuildNo] = @pGuildNo

	IF @@ERROR <> 0 
	BEGIN
		RETURN(1)
	END
	
	RETURN(0)

GO

