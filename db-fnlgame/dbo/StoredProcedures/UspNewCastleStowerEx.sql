/******************************************************************************
**		Name: UspNewCastleStowerEx
**		Desc: 
**
**		Auth:
**		Date:
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	2013-06-20	傍籍痹				荐沥
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspNewCastleStowerEx]
	 @pIsTower		BIT
	,@pPlace		INT
	,@pGuildNo		INT
	,@pTaxBuy		INT
	,@pTaxBuyMax	INT
	,@pTaxHunt		INT
	,@pTaxHuntMax	INT
	,@pTaxGamble	INT
	,@pTaxGambleMax	INT
	,@pLimitWeek	SMALLINT		
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set搬苞甫 积己窍瘤 富酒扼.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	INSERT TblCastleTowerStone([mIsTower], [mPlace], [mGuildNo], [mTaxBuy], [mTaxBuyMax], 
							   [mTaxHunt], [mTaxHuntMax], [mTaxGamble], [mTaxGambleMax], [mAsset], [mSiegeDayOfWeekLimit])
						VALUES(@pIsTower, @pPlace, @pGuildNo, @pTaxBuy, @pTaxBuyMax, 
							   @pTaxHunt, @pTaxHuntMax, @pTaxGamble, @pTaxGambleMax, 0, @pLimitWeek)
	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo	= 1
	END

	RETURN(@aErrNo)

GO

