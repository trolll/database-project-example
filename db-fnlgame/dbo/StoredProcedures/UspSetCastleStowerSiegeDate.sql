/******************************************************************************   
**  File: UspSetCastleStowerSiegeDate.sql 
**  Name: UspSetCastleStowerSiegeDate 
**  Desc: 玫傍狼级傍己矫埃汲沥
*******************************************************************************   
**  Change History   
*******************************************************************************   
**  Date:  Author:  Description:  
**  ---------- ---------- ---------------------------------------   
** 2013.04.20     kongkong7 积己
*******************************************************************************/   
CREATE PROCEDURE [dbo].[UspSetCastleStowerSiegeDate] 
	@pPlace          INT,
	@pDate           DATETIME,
	@pDayOfWeek      SMALLINT
AS 
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  
	DECLARE @aErrNo  INT 
	SET  @aErrNo  = 0
 
	UPDATE dbo .TblCastleTowerStone  
	SET  
	[mSiegeDate] = @pDate, [mSiegeDayOfWeek] = @pDayOfWeek
	WHERE ([mPlace] = @pPlace)
 
	IF(1 <> @@ROWCOUNT) 
	BEGIN 
		SET @aErrNo = 1 
	END 
 
	RETURN( @aErrNo)

GO

