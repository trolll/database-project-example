/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspAddPetition    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspAddPetition]
	@pPcNo		INT,		-- PC??
	@pCategory		INT,		-- ???? (??)
	@pPosX		FLOAT,		-- X??
	@pPosY		FLOAT,		-- Y??
	@pPosZ		FLOAT,		-- Z??
	@pText			CHAR(1000),	-- ????
	@pIpAddress		CHAR(20)	-- IP??
--WITH ENCRYPTION
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	BEGIN TRAN

	-- ??? ??
	INSERT dbo.TblPetitionBoard (mCategory, mRegDate, mPcNo, mPosX, mPosY, mPosZ, mText, mIpAddress, mIsFin, mFinDate) 
	VALUES(@pCategory, GetDate(), @pPcNo, @pPosX, @pPosY, @pPosZ, @pText, @pIpAddress, DEFAULT, DEFAULT)

	-- ??? ??? ??? ???
	IF EXISTS(SELECT TOP 1 mPID FROM dbo.TblPetitionBoard WHERE mPcNo = @pPcNo AND mCategory = @pCategory ORDER BY mPID DESC)
	BEGIN
		DECLARE	@aAddPID	BIGINT
		SELECT TOP 1 @aAddPID = mPID FROM dbo.TblPetitionBoard WHERE mPcNo = @pPcNo AND mCategory = @pCategory ORDER BY mPID DESC

		-- ?? ??? ??? ??? ??? ?? ??? ?? ??/??
		IF EXISTS(SELECT mPID FROM dbo.TblPetitionCheckState WHERE mPcNo = @pPcNo AND mCategory = @pCategory)
		BEGIN
			-- ?? ??? ?? ??? ?? -> ??? ??? ???? ??? ??? ?? ? ?? ??
			DECLARE	@aDelPID	BIGINT
			SELECT @aDelPID = mPID FROM dbo.TblPetitionCheckState WHERE mPcNo = @pPcNo AND mCategory = @pCategory
			-- ??? ???? ???? ??? ????? ?
			DELETE dbo.TblPetitionBoard WHERE mPID = @aDelPID AND mIsFin = 0
			UPDATE dbo.TblPetitionCheckState SET mPID = @aAddPID WHERE mPcNo = @pPcNo AND mCategory = @pCategory
		END
		ELSE
		BEGIN
			-- ?? ??? ?? ??? ?? -> ?? ?? ??? ??
			INSERT dbo.TblPetitionCheckState VALUES (@pPcNo, @pCategory, @aAddPID)
		END
	END
	ELSE
	BEGIN
		SET	@aErrNo = 2	-- ??? ?? ??
	END

	IF @@ERROR = 0
	BEGIN
		COMMIT TRAN
	END
	ELSE
	BEGIN
		SET	@aErrNo = 1	-- ??? ??
		ROLLBACK TRAN
	END

	SET NOCOUNT OFF
	RETURN(@aErrNo)
END

GO

