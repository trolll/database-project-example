CREATE PROCEDURE [dbo].[WspListItemByGuild]  	
	@pGuildNo	INT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		T2.IName		
		,T1.mEndDate
		,T1.mSerialNo
		,T1.mCnt
		,T1.mItemNo
		,T1.mIsSeizure
		,T1.mRegDate
		,T1.mStatus
		,T1.mGrade
		,T1.mPracticalPeriod
		,T1.mBindingType		
	FROM dbo.TblGuildStore T1		
		INNER JOIN [FNLParm].dbo.DT_Item AS T2
			ON T1.mItemNo = T2.IID
	WHERE mGuildNo = @pGuildNo

GO

