/******************************************************************************
**		Name: UspAddServantSkillPack
**		Desc: 辑锅飘 胶懦 蒲 眠啊
**
**		Auth: 捞侩林
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20151117	捞侩林				@pLeftTick 馆券 荐沥
**		20160331	捞侩林				SkillPackType 眠啊 (函脚 胶懦蒲牢瘤 酒囱瘤 备喊侩)
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddServantSkillPack]
	@pSerialNo			BIGINT
	,@pSPID				INT
	,@pValidDay			INT
	,@pLeftTick			INT OUTPUT 
	,@pSkillPackType	SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- 捞固 乐促搁 父丰 朝楼父 技泼窍绊 眠啊 己傍栏肺 埃林茄促
	DECLARE @aSPID INT
	SELECT @aSPID = mSPID FROM dbo.TblPcServantSkillPack
	WHERE mSerialNo = @pSerialNo AND mSPID = @pSPID

	DECLARE  @aEndDay  SMALLDATETIME
	SELECT  @aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)

	IF @aSPID IS NOT NULL
	BEGIN
		UPDATE dbo.TblPcServantSkillPack
		SET mEndDate = @aEndDay
		WHERE mSerialNo = @pSerialNo AND mSPID = @pSPID
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblPcServantSkillPack
		VALUES(DEFAULT, @pSerialNo, @pSPID, @aEndDay, @pSkillPackType)
	END
	
	IF @@ERROR <> 0
	BEGIN
		RETURN 1 -- DB 俊矾
	END

	SET @pLeftTick =  DATEDIFF(mi, GETDATE(), @aEndDay)

	RETURN 0

	SET NOCOUNT OFF;

GO

