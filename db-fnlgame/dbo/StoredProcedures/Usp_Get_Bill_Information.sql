CREATE  PROCEDURE [dbo].[Usp_Get_Bill_Information]
	 @UserLogin			VARCHAR(20) 
	,@ClientIP   		VARCHAR(20) 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo	INT = 0
	DECLARE	@Balance INT = 0 
	DECLARE	@BillingResult INT=0

--BillingResult
--    SUCCESS = 1,
--    FAILED = 0,
--    SYSTEM_ERROR = -10,
--    NOT_ENOUGH_MONEY = -3,
--    ACCOUNT_BLOCKED = -1,
--    IP_DENIED = -2,
--    INVALID_PRICE = -4,
--    ITEM_NOT_SELL = -5,
--    INCORRECT_LOGIN = -6	



IF EXISTS (SELECT Cash FROM FNLAccount.dbo.Member WHERE mUserId=@UserLogin)
BEGIN
	SET @Balance=(SELECT Cash FROM FNLAccount.dbo.Member where mUserId=@UserLogin)
	IF @Balance>=0 SET @BillingResult=1
	IF @Balance<0 SET @BillingResult=-3
	SELECT @BillingResult, @Balance 
	RETURN(1)
END
ELSE
BEGIN
   RETURN(0)
END

GO

