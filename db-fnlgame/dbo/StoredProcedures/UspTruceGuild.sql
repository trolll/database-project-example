/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspTruceGuild    Script Date: 2008-11-10 10:37:18 ******/





CREATE PROCEDURE [dbo].[UspTruceGuild]
	 @pGuildNo1	INT
	,@pGuildNo2	INT	
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	DECLARE	@aGuildNo1	INT
	DECLARE	@aGuildNo2	INT
	IF(@pGuildNo1 < @pGuildNo2)
	 BEGIN
		SET	@aGuildNo1	= @pGuildNo1
		SET	@aGuildNo2	= @pGuildNo2
	 END
	ELSE
	 BEGIN
		SET	@aGuildNo1	= @pGuildNo2
		SET	@aGuildNo2	= @pGuildNo1	 
	 END	

	DECLARE	@aRegDate	DATETIME
	
	SELECT @aRegDate=[mRegDate] 
	FROM dbo.TblGuildBattle 
	WHERE ([mGuildNo1]=@aGuildNo1) 
		AND ([mGuildNo2]=@aGuildNo2)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		RETURN(1)
	END

	BEGIN TRAN	

		DELETE dbo.TblGuildBattle 
		WHERE ([mGuildNo1]=@aGuildNo1) AND ([mGuildNo2]=@aGuildNo2)
		IF(1 <> @@ROWCOUNT)
		BEGIN
			SET @aErrNo = 2
			GOTO LABEL_END		 
		END
		 
		INSERT INTO dbo.TblGuildBattleHistory([mGuildNo1], [mGuildNo2], [mStxDate], [mEtxDate], [mWinnerGuildNo]) 
									VALUES(@aGuildNo1,  @aGuildNo2,  @aRegDate,	 GETDATE(),	 0)
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 2
			GOTO LABEL_END		 
		END	
									
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN	
		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

