/******************************************************************************
**		Name: UspDelPcCalendarGroupMember
**		Desc: ±×·ì¿¡ ¸â¹öÃß°¡
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-17
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDelPcCalendarGroupMember]
	 @pOwnerPcNo		INT 
	,@pGroupNo			TINYINT
	,@pMemberPcNm		CHAR(12)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aMemberPcNo	INT
	SELECT	@aMemberPcNo	= 0

	IF NOT EXISTS ( SELECT T1.mNo
					FROM dbo.TblPc AS T1
						INNER JOIN dbo.TblCalendarAgreement AS T2
							ON T1.mNo = T2.mMemberPcNo
					WHERE T1.mNm = @pMemberPcNm
						AND T1.mDelDate IS NULL	)
	BEGIN
		RETURN(2);
	END 

	SELECT @aMemberPcNo = mNo
	FROM dbo.TblPc
	WHERE mNm = @pMemberPcNm

	IF NOT EXISTS ( SELECT  *
					FROM dbo.TblCalendarGroupMember
					WHERE mOwnerPcNo = @pOwnerPcNo 
						AND mGroupNo = @pGroupNo
						AND mMemberPcNo = @aMemberPcNo)
	BEGIN
		RETURN(1);
	END 

	DECLARE @aSerial		TABLE (
				mSerial		BIGINT	 PRIMARY KEY      	)			

	-- ÇØ´ç ±×·ì°ú ¿¬°áµÈ SerialNo¸¦ ±¸ÇÑ´Ù.
	INSERT INTO @aSerial(mSerial)
	SELECT mSerialNo
	FROM dbo.TblCalendarScheduleGroup
	WHERE mOwnerPcNo = @pOwnerPcNo AND mGroupNo = @pGroupNo

	
	BEGIN TRAN

		DELETE dbo.TblCalendarGroupMember
		WHERE mOwnerPcNo = @pOwnerPcNo AND mGroupNo = @pGroupNo AND mMemberPcNo = @aMemberPcNo
		
		IF (@@ERROR <> 0) OR (@@ROWCOUNT <= 0)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

		DELETE dbo.TblCalendarPcSchedule
		WHERE mPcNo = @aMemberPcNo		
			AND mSerialNo IN (
							 SELECT *
							 FROM @aSerial
							 )
		IF (@@ERROR <> 0) 
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

	COMMIT TRAN

	RETURN(0);

GO

