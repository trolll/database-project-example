/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetPcItemEx    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspGetPcItemEx]
	 @pPcNo				INT,
	 @pItemNo1			INT,
	 @pItemNo2			INT,
	 @pItemNo3			INT, -- ??? ??
	 @pItemSerialNo1	BIGINT OUTPUT,
	 @pItemSerialNo2	BIGINT OUTPUT,
	 @pItemSerialNo3	BIGINT OUTPUT -- ??? ??
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET ROWCOUNT 1
	
	SELECT @pItemSerialNo1 = 0,
			@pItemSerialNo2 = 0,
			@pItemSerialNo3 = 0 -- ??? ??

	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, ??? ???

	SELECT 
		@pItemSerialNo1 = mSerialNo
	FROM dbo.TblPcInventory
	WHERE mPcNo = @pPcNo
			AND mItemNo = @pItemNo1

	SELECT 
		@pItemSerialNo2 = mSerialNo
	FROM dbo.TblPcInventory
	WHERE mPcNo = @pPcNo
			AND mItemNo = @pItemNo2
			
	SELECT 
		@pItemSerialNo3 = mSerialNo
	FROM dbo.TblPcInventory
	WHERE mPcNo = @pPcNo
			AND mItemNo = @pItemNo3 -- ??? ??

	RETURN(0)

GO

