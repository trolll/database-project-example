/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspNewCastleStower    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspNewCastleStower]
	 @pPlace		INT
	,@pGuildNo		INT
	,@pTaxBuy		INT
	,@pTaxBuyMax	INT
	,@pTaxHunt		INT
	,@pTaxHuntMax	INT
	,@pTaxGamble	INT
	,@pTaxGambleMax	INT		
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	INSERT TblCastleTowerStone([mPlace], [mGuildNo], [mTaxBuy], [mTaxBuyMax], 
							   [mTaxHunt], [mTaxHuntMax], [mTaxGamble], [mTaxGambleMax], [mAsset])
						VALUES(@pPlace, @pGuildNo, @pTaxBuy, @pTaxBuyMax, 
							   @pTaxHunt, @pTaxHuntMax, @pTaxGamble, @pTaxGambleMax, 0)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo	= 1
	 END

	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

