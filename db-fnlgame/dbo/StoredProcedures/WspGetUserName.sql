--------------------------------
-- ░┤├╝╕φ : WspGetUserName
-- ╝│╕φ : ─│╕»┼═ └╠╕º└╗ ╛≥┤┬┤┘.
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.06.27
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
--------------------------------
CREATE PROCEDURE [dbo].[WspGetUserName]
	@mNo INT-- ─│╕»┼═ ╣°╚ú

AS
	SET NOCOUNT ON
	
	SELECT
		mNm

	FROM
		[dbo].[TblPc]
	WITH (NOLOCK)
	WHERE
		mNo = @mNo AND mDelDate IS NULL

	SET NOCOUNT OFF

GO

