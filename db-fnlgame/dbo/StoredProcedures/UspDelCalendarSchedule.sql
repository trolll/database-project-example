/******************************************************************************
**		Name: UspDelCalendarSchedule
**		Desc: ÀÏÁ¤ÀÌ µî·ÏµÈ ±×·ì¸ñ·Ï°ú °³ÀÎÀÏÁ¤ »èÁ¦
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDelCalendarSchedule]
	 @pSerialNo			BIGINT
	,@pOwnerPcNo		INT 
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRAN

		DELETE dbo.TblCalendarSchedule
		WHERE mSerialNo = @pSerialNo AND mOwnerPcNo = @pOwnerPcNo

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		-- µî·ÏµÈ ±×·ì »èÁ¦
		DELETE dbo.TblCalendarScheduleGroup
		WHERE mSerialNo = @pSerialNo AND mOwnerPcNo = @pOwnerPcNo

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		-- µî·ÏµÈ À¯Àú »èÁ¦
		DELETE dbo.TblCalendarPcSchedule
		WHERE mSerialNo = @pSerialNo AND mOwnerPcNo = @pOwnerPcNo

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

	COMMIT TRAN
	RETURN(0);

GO

