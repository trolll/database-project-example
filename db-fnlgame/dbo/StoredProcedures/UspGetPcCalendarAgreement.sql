/******************************************************************************
**		Name: UspGetPcCalendarAgreement
**		Desc: ´Þ·Â ½ÂÀÎ¸ñ·Ï
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-21
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcCalendarAgreement]
	 @pPcNo			INT 
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		 T1.mMemberPcNo
		, RTRIM(T2.mNm)
	FROM dbo.TblCalendarAgreement AS T1
		 INNER JOIN dbo.TblPc AS T2 
	ON T1.mMemberPcNo = T2.mNo
	WHERE mOwnerPcNo = @pPcNo

GO

