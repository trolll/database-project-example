/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2009-6-1 15:32:38 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2009-5-12 9:18:17 ******/

/****** Object:  Stored Procedure dbo.UspCharTransfer    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspCharTransfer]
	@pPcNo			INT,		-- ?? ??? ?? 
	@pMoveUserNo	INT,		-- ?? ?? ??
    @pSerialNo		BIGINT,		-- ??? ??? ??
    @pItemNo		INT			-- ??? ?? 	
AS
	SET NOCOUNT ON			

	DECLARE @aSlotNum TABLE(
		mSlot	TINYINT NOT NULL
	)
	DECLARE	@aErrNo		INT
			,@aRowCnt	INT
			,@aSlot		TINYINT
			
	SELECT  @aErrNo = 0, @aRowCnt = 0

	-----------------------------------------------
	-- ?? ??? ? Slot ?? 
	-----------------------------------------------		
	INSERT INTO @aSlotNum VALUES(0)
	INSERT INTO @aSlotNum VALUES(1)
	INSERT INTO @aSlotNum VALUES(2)

	SELECT TOP 1 
		@aSlot = T1.mSlot
	FROM @aSlotNum T1 
		LEFT OUTER JOIN (
			SELECT mSlot 
			FROM dbo.TblPc
			WHERE mOwner = @pMoveUserNo
					AND mDelDate IS NULL
		) T2
		ON T1.mSlot = T2.mSlot		
	WHERE T2.mSlot IS NULL	
		
	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR 	@aRowCnt = 0
	BEGIN
		RETURN (2)	-- ? ??? ??. 
	END				
	
	IF @aSlot IS NULL
	BEGIN
		RETURN (2)	-- ? ??? ??. 
	END 

	
	-----------------------------------------------
	-- ?? ??? ?? 
	-----------------------------------------------		
	IF EXISTS( SELECT *
					FROM dbo.TblGuildMember
					WHERE mPcNo = @pPcNo
							AND mGuildGrade = 0 )
	BEGIN
		RETURN(3) -- ?? ??? ??			
	END 
	
	-----------------------------------------------
	-- ?? ??? ?? 
	-----------------------------------------------
	BEGIN TRAN
		
		DELETE dbo.TblPcInventory
		WHERE mPcNo = @pPcNo 
			AND	mSerialNo = @pSerialNo
			AND mItemNo = @pItemNo
				
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END		
					
		UPDATE dbo.TblPc
		SET
			mSlot = @aSlot,
			mOwner = @pMoveUserNo
		WHERE mNo = @pPcNo
		
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR 	@aRowCnt = 0
		BEGIN
			ROLLBACK TRAN
			RETURN (1)	-- db error
		END		
		
		
	COMMIT TRAN	
	RETURN(0)

GO

