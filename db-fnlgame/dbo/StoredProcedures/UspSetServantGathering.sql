/******************************************************************************
**		Name: UspSetServantGathering
**		Desc: 辑锅飘 盲笼 殿废
**
**		Auth: 捞侩林
**		Date: 2016-10-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetServantGathering]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pEndDate		DATETIME
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNo		INT
		, @aRowCnt	INT

	INSERT dbo.TblPcServantGathering (mPcNo, mSerialNo, mEndDate)
	VALUES (@pPcNo, @pSerialNo, @pEndDate)

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt = 0
	BEGIN
		RETURN(@aErrNo)
	END

	SET NOCOUNT OFF;

GO

