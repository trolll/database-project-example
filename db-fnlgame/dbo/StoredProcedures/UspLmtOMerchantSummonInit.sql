/******************************************************************************
**		Name: UspLmtOMerchantSummonInit
**		Desc: ÀÌ°è »óÀÎ ¼ÒÈ¯ Á¦ÇÑ È½¼ö ÃÊ±âÈ­
**
**		Auth: Á¤ÁøÈ£
**		Date: 2015-10-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspLmtOMerchantSummonInit]
AS
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DELETE dbo.TblLimitedOtherMerchantSummon

	INSERT INTO dbo.TblLimitedOtherMerchantSummon(mMerchantID, mAbleSummonCnt)
	SELECT mMerchantID, mMaxSummonCnt
	FROM FNLParm.dbo.TblOtherMerchantInfo

GO

