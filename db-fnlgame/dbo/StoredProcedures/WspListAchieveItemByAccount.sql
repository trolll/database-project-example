/******************************************************************************    
**  Name: WspListAchieveItemByAccount
**  Desc: 拌沥 家蜡 林拳/傈府前 酒捞袍 炼雀
**  Auth: 巢扁豪    
**  Date: 04/15/2013  
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:  Author:    Description:    
**  -------- --------   ---------------------------------------    
**    
*******************************************************************************/    
CREATE PROCEDURE [dbo].[WspListAchieveItemByAccount]  
(    
	@pUserId	VARCHAR(20) -- 拌沥疙  
)  
AS  
BEGIN  
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET ROWCOUNT 1000;
  
	DECLARE @aUserNo	INT  
			, @aErrNo	INT  
			, @aRowCnt	INT    
  
	SELECT @aUserNo = 0 , @aErrNo = 0, @aRowCnt = 0    
  
	-------------------------------------------------------    
	-- 拌沥 粮犁咯何 眉农    
	-------------------------------------------------------
	SELECT     
		@aUserNo = mUserNo    
	FROM FNLAccount.dbo.TblUser    
	WHERE mUserId = @pUserId    
	AND mDelDate = '1900-01-01'    

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
 
	IF (@aErrNo <> 0 OR @aRowCnt <= 0)   
	BEGIN    
		RETURN(1)   
	END     
  

	DECLARE @TempTable table (
		mSerialNo bigint,
		mPcNo int,
		mIsTrophy bit,
		mIsEquip bit);

	INSERT INTO @TempTable
		SELECT T2.mSerialNo, T1.mNo, 1, 0
		FROM dbo.TblPc AS T1 INNER JOIN dbo.TblPcAchieveInventory AS T2
		ON T1.mNo = T2.mPcNo WHERE T1.mOwner = @aUserNo AND T2.mAchieveID <> 0
	IF @@ERROR <> 0
		RETURN 2;

	UPDATE T1 SET T1.mIsEquip = 1 
	FROM @TempTable AS T1 INNER JOIN dbo.TblPcAchieveEquip AS T2
	ON T1.mSerialNo = T2.mSerialNo 
	WHERE T1.mPcNo = T2.mPcNo
	IF @@ERROR <> 0
		RETURN 3;

	 -------------------------------------------------------    
	 -- 酒捞袍 沥焊甫 馆券    
	 -------------------------------------------------------  
	SELECT
		T1.mNm					-- 某腐磐 捞抚
		,T3.IName				-- 林拳/傈府前 捞抚
		,T2.mSerialNo			-- 林拳/傈府前 GUID
		,T2.mItemNo				-- 酒捞袍 ID
		,T2.mRegDate			-- 殿废老
		,T2.mAchieveGuildID		-- 诀利 辨靛 ID
		,T2.mCoinPoint			-- 林拳 器牢飘
		,T2.mSlotNo				-- 浇吩 困摹
		,T4.mIsTrophy			-- 傈府前 咯何
		,T2.mAchieveID			-- 积己等 傈府前苞 楷拌等 诀利 ID
		,T2.mExp				-- 傈府前 版氰摹
		,T2.mLimitLevel			-- 傈府前 弥措 饭骇
		,T4.mIsEquip			-- 傈府前 馒侩 咯何
		,T2.mIsSeizure			-- 拘幅 咯何
	FROM dbo.TblPc AS T1
		INNER JOIN dbo.TblPcAchieveInventory AS T2
	ON T1.mNo = T2.mPcNo
		INNER JOIN FNLParm.dbo.DT_Item AS T3
	ON T2.mItemNo = T3.IID
		INNER JOIN @TempTable AS T4
	ON T1.mNo = T4.mPcNo
	WHERE T1.mOwner = @aUserNo
	ORDER BY T1.mNo ASC
	  
	RETURN 0;
END

GO

