/******************************************************************************
**		Name: UspUpdatePcMakingQuestCnt
**		Desc: 涅胶飘甫 父甸菌阑版快 诀单捞飘
**
**		Auth: 沥柳龋
**		Date: 2010-03-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePcMakingQuestCnt]
	@pPcNo			INT 
	,@pType			TINYINT		-- 1: 老馆, 2: 辨靛
	,@pCnt			INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @pType = 1
	BEGIN
		UPDATE dbo.TblPcState
		SET mQMCnt = 
			CASE
				WHEN 0 < (mQMCnt + @pCnt)
					THEN mQMCnt + @pCnt
				ELSE 0
			END 			
		WHERE mNo = @pPcNo
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(1);
		END
	END

	IF @pType = 2
	BEGIN
		UPDATE dbo.TblPcState
		SET mGuildQMCnt = 
			CASE
				WHEN 0 < (mGuildQMCnt + @pCnt)
					THEN mGuildQMCnt + @pCnt
				ELSE 0
			END 	
		WHERE mNo = @pPcNo
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(2);
		END
	END
	 		 
	RETURN(0);

GO

