/******************************************************************************
**		Name: UspRegLmtOMerchantSummon
**		Desc: ÀÌ°è »óÀÎ ¼ÒÈ¯ Á¦ÇÑ È½¼ö
**
**		Auth: Á¤ÁøÈ£
**		Date: 2015-10-16
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspRegLmtOMerchantSummon]
	@pMerchantID			INT
	,@pLimitCnt				TINYINT
AS
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF NOT EXISTS(
	SELECT *
	FROM dbo.TblLimitedOtherMerchantSummon
	WHERE mMerchantID = @pMerchantID
	)
	BEGIN
		INSERT INTO dbo.TblLimitedOtherMerchantSummon(mMerchantID, mAbleSummonCnt)
		VALUES (@pMerchantID, @pLimitCnt)
	END

GO

