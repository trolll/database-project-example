/******************************************************************************
**		Name: UspAddPcUnConfirmedCoin
**		Desc: 固犬牢 林拳 刘啊/皑家
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddPcUnConfirmedCoin]
	 @pPcNo  INT 
	,@pCoin INT 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aCoin INT
			,@aErr INT
			,@aRowCnt INT

	SET @aCoin = 0

	SELECT @aCoin = mCoin FROM dbo.TblPcUnconfirmedCoin WHERE mPcNo = @pPcNo
	SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT;

	IF @aErr <> 0
	BEGIN
		SET @aErr = 1
		GOTO T_END
	END	

	IF @aRowCnt = 0
	BEGIN
		IF @pCoin < 0
		BEGIN
			SET @aErr = 2
			GOTO T_END
		END	

		INSERT INTO dbo.TblPcUnConfirmedCoin (mPcNo, mCoin) VALUES (@pPcNo, @pCoin)
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT;
	END
	ELSE
	BEGIN
		SET @aCoin = @aCoin + @pCoin
		IF @aCoin < 0
		BEGIN
			SET @aErr = 3
			GOTO T_END
		END
		
		UPDATE dbo.TblPcUnConfirmedCoin SET mCoin = @aCoin WHERE mPcNo = @pPcNo
		SELECT @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT;
	END

	IF @aErr <> 0 OR @aRowCnt = 0
	BEGIN
		SET @aErr = 4
		GOTO T_END
	END	

T_END:
	SELECT @aErr

GO

