/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetApplyAbnItem    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspSetApplyAbnItem]
	 @pPcNo		INT					-- ???
	,@pSerialNo		BIGINT			-- ?? ???? ?????
	,@pApplyAbnItemNo	INT			-- ???? ?????
	,@pApplyAbnValidMin	INT			-- ???.(??:?)
AS
	SET NOCOUNT ON		
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF NOT EXISTS(	SELECT mSerialNo
					FROM dbo.TblPcInventory 
					WHERE mPcNo = @pPcNo 
							AND mSerialNo = @pSerialNo )
	BEGIN
		RETURN(2)	-- ?? ???? ???? ??
	END
	

	UPDATE dbo.TblPcInventory 
	SET 
		mApplyAbnItemNo = @pApplyAbnItemNo, 
		mApplyAbnItemEndDate = dbo.UfnGetEndDateByMin(GETDATE(), @pApplyAbnValidMin) 
	WHERE mPcNo = @pPcNo 
			AND mSerialNo = @pSerialNo
	IF @@ERROR <> 0
	BEGIN
		RETURN(1)		-- DB ???? ??	
	END
		
	RETURN(0)

GO

