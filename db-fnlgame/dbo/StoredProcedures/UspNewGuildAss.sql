/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2011-4-19 15:24:41 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2011-3-17 14:50:08 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2010-12-23 17:46:05 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2010-3-22 15:58:23 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2009-12-14 11:35:31 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2009-7-14 13:13:33 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2009-6-1 15:32:41 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2009-5-12 9:18:20 ******/

/****** Object:  Stored Procedure dbo.UspNewGuildAss    Script Date: 2008-11-10 10:37:21 ******/





CREATE PROCEDURE [dbo].[UspNewGuildAss]
	 @pGuildNo	INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0
	
	IF(0 <> @@TRANCOUNT) ROLLBACK
	SET XACT_ABORT ON
	BEGIN TRAN	
		
	INSERT INTO TblGuildAss([mGuildNo]) VALUES(@pGuildNo)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END		
	 END
	 
	INSERT INTO TblGuildAssMem([mGuildAssNo], [mGuildNo]) VALUES(@pGuildNo, @pGuildNo)
	IF(0 <> @@ERROR)
	 BEGIN
		SET @aErrNo = 2
		GOTO LABEL_END		
	 END	 
	 
LABEL_END:		
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

