CREATE PROCEDURE [dbo].[WspListConsignmentAccountByPc]
(
	@pPcNm VARCHAR(15)
)
AS
/******************************************************************************
**		Name: WspListConsignmentAccountByPc
**		Desc: 캐릭터번호로 위탁판매 정상금 잔액 확인
**		Test:
			EXEC dbo.WspListConsignmentAccountByPc
				'JSChar'
**		Auth: 이진선
**		Date: 2012-12-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT
		T1.mNo	--캐릭터 번호
		, T1.mNm	--캐릭터명
		, T2.mBalance	--정산금 잔액
	FROM dbo.TblPc AS T1
		INNER JOIN dbo.TblConsignmentAccount AS T2
	ON T1.mNo = T2.mPcNo
	WHERE T1.mNm = @pPcNm
	AND T1.mDelDate IS NULL
END

GO

