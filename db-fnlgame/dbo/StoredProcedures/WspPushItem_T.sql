/******************************************************************************
**		Name: WspPushItem_T
**		Desc: 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	2012.03.28	이진선				게임 SP (UspPushItem) 호출 부분 분리. 웹어드민 UspPushItem 권한 거부. 5천만 실버 이동 시 로그.
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspPushItem_T]
(
	@mNo INT, -- 사용자 번호
	@mItemNo INT, -- 아이템 번호 (TID)
	@mStatus TINYINT, -- 아이템 상태
	@mCnt INT, -- 아이템 수량
	@mIsConfirm BIT, -- 확인여부
	@pValidDay INT = NULL, -- 유효기간
	@pPraticalHour INT = NULL,
	@pBindingType	TINYINT = 0		-- BIDING TYPE OFF
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;	

	DECLARE @pCntUse TINYINT
	DECLARE @pIsStack BIT,
			@pIsPracticalPeriod BIT,
			@pIsCharge INT,
			@pErr INT

	IF @pValidDay IS NULL
		SELECT
			@pValidDay = ITermOfValidity
			, @pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
			, @pIsCharge = IIsCharge
		FROM [FNLParm].[dbo].[DT_Item]
		WHERE IID = @mItemNo
	ELSE
		SELECT
			@pCntUse = IUseNum
			, @pIsStack = IMaxStack
			, @pIsPracticalPeriod = mIsPracticalPeriod
			, @pIsCharge = IIsCharge
		FROM [FNLParm].[dbo].[DT_Item]
		WHERE IID = @mItemNo
	

	-- 없는 TID
	IF @pValidDay IS NULL OR @pCntUse IS NULL OR @pIsStack IS NULL
		RETURN 2
		
	IF @pIsPracticalPeriod = 1 AND ( @pPraticalHour IS NULL OR @pPraticalHour <= 0)
		RETURN 3
	
	IF @pPraticalHour > (30*24) 
		RETURN 4
		
	IF @pPraticalHour IS NULL
		SET @pPraticalHour = 0
	
	/*	
	-- UspPushItem을 사용하여 생성한다
	EXECUTE dbo.UspPushItem @mNo, 0, @mItemNo, @pValidDay, @mCnt, @pCntUse, @mIsConfirm, @mStatus, @pIsStack, @pIsCharge, @pPraticalHour, @pBindingType
	*/

	DECLARE	 @aEndDay		SMALLDATETIME
			,@aOrgPcNo		INT		-- 기존 소유 자
			,@aIsSeizure		BIT	
			,@aCntRest		INT
			,@aRowCnt		INT
			,@aErr			INT
			,@aSerial	BIGINT
			,@aCntTarget		BIGINT
			,@aOwner		INT
			,@aPracticalPeriod	INT
			,@pSerial BIGINT
				
	-----------------------------------------------
	-- 변수 초기화
	-----------------------------------------------
	SELECT		 @aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)
				,@aRowCnt = 0
				,@aErr = 0
				,@aOrgPcNo = 0
				,@aSerial = 0 
				,@aOwner = 0
				,@aPracticalPeriod = 0
				,@pSerial = 0			

	IF @@ERROR <> 0			-- 사용자 정의 함수에서 에러 발생시 에러 반환 
	BEGIN
		SET @aErr = 1				
		GOTO T_END	
	END 
	IF @aEndDay < GETDATE()
	BEGIN
		SET @aErr = 1				
		GOTO T_END
	END 
	
	-----------------------------------------------
	-- 아이템 귀속 타입 체크 
	-----------------------------------------------	
	IF @pSerial <> 0 
		AND @pBindingType IN(1,2)	
	BEGIN
		SET @aErr = 11	-- 귀속 속성은 이동할 수가 없다. 			
		GOTO T_END
	END 	
			
	-----------------------------------------------
	-- 409 : 돈 아이템 
	-----------------------------------------------
	IF(@mItemNo = 409)
	BEGIN
		SET		@mIsConfirm = 1
		SET		@mStatus = 1
	END
		
	-----------------------------------------------
	-- 기존 아이템
	-----------------------------------------------
	IF( @pSerial <> 0 )
	BEGIN		
		SELECT 	TOP 1		
			@aOrgPcNo = [mPcNo],	-- 기존 소유자	
			@aCntRest=[mCnt], 
			@aEndDay=[mEndDate], 
			@aIsSeizure=[mIsSeizure],
			@aOwner = [mOwner],
			@aPracticalPeriod = [mPracticalPeriod],
			@pBindingType = mBindingType
		FROM dbo.TblPcInventory WITH(INDEX=PK_NC_TblPcInventory_1)
		WHERE  mSerialNo = @pSerial
		SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErr <> 0 OR 	@aRowCnt = 0
		BEGIN
			SET @aErr = 1				
			GOTO T_END
		END		
	
		IF @aIsSeizure <> 0
		BEGIN
			SET @aErr = 29	
			GOTO T_END					--	압류 상태 아이템				
		END
		SET @aCntRest = @aCntRest - @mCnt
		IF (@aCntRest <  0 )
		BEGIN
			SET @aErr = 2				-- 사용할 수 있는 아이템 갯수가 모자르다.( 무조건 0보다 커야 한다)
			GOTO T_END	
		END
		
		IF @pBindingType IN(1,2)	
		BEGIN
			SET @aErr = 11	-- 귀속 속성은 이동할 수가 없다. 			
			GOTO T_END		
		END 	
		
	END
	-----------------------------------------------
	-- 누적형 아이템 존재여부 체크
	-----------------------------------------------
	IF (@pIsStack <> 0) AND (1 <> @mNo)
	BEGIN
		SELECT 	TOP 1			
			@aCntTarget=ISNULL([mCnt],0), 
			@aSerial=[mSerialNo], 
			@aIsSeizure=ISNULL([mIsSeizure],0)
		FROM dbo.TblPcInventory WITH(INDEX=CL_TblPcInventory)		
		WHERE  mPcNo  = @mNo
				AND mItemNo = @mItemNo 
				AND mEndDate  = @aEndDay
				AND mIsConfirm  = @mIsConfirm  
				AND mStatus  = @mStatus
				AND mOwner = @aOwner  
				AND mPracticalPeriod = @pPraticalHour
				AND mBindingType = @pBindingType	-- 누적형 아이템의 경우 바인드 타입이 동일해야 한다.
 
		SELECT @aErr = @@ERROR
		IF ( @aErr <> 0 )
		BEGIN
			SET @aErr = 3	
			GOTO T_END
		END		
		IF(@aIsSeizure <> 0 )				
		 BEGIN
			SET @aErr = 28	
			GOTO T_END
		 END	
	END	
	
	-----------------------------------------------
	-- 아이템 이동
	-----------------------------------------------
	BEGIN TRAN;
		IF @aCntTarget  <> 0 
		BEGIN
			UPDATE dbo.TblPcInventory				--	누적용 아이템 존재 하면 기존 아이템에 누적하고 
			SET
				mCnt = mCnt + @mCnt
			WHERE  mSerialNo = @aSerial
			
			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF  @aErr <> 0 OR 	@aRowCnt = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 4	
				GOTO T_END	
			END		
			IF ( @pSerial <> 0)
			BEGIN
				IF @aCntRest < 1		-- 잔여아이템 존재하지 않을 때 
				BEGIN
					DELETE dbo.TblPcInventory
					WHERE mSerialNo = @pSerial
	
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 5	
						GOTO T_END	
					END		
				END 
				ELSE
				BEGIN
					UPDATE dbo.TblPcInventory			
					SET
						mCnt = @aCntRest
					WHERE  mSerialNo =@pSerial				
	
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 6	
						GOTO T_END	
					END		
				END 
			END
		END		
		ELSE
		BEGIN
			IF ( @pSerial <> 0 )
			BEGIN
				IF @aCntRest < 1		-- 잔여아이템 존재하지 않을 때 
				BEGIN		
					SET @aSerial = @pSerial
				
					UPDATE dbo.TblPcInventory			
					SET
						mPcNo = @mNo,
						mCnt = @mCnt
					WHERE  mSerialNo =@pSerial				
		
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 7	
						GOTO T_END
					END		
				END
				ELSE
				BEGIN
		
					UPDATE dbo.TblPcInventory			
					SET
						mCnt = @aCntRest
					WHERE  mSerialNo =@pSerial				
		
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 8	
						GOTO T_END	
					END			
				
					EXEC @aSerial =  dbo.UspGetItemSerial 	-- 시리얼 얻기 
					IF @aSerial <= 0
					BEGIN
						ROLLBACK TRAN
						SET @aErr = 7		
						GOTO T_END	
					END		
				
					INSERT dbo.TblPcInventory(				-- 누적용 아이템 존재하지 않으면, 새롭게 생성 
						[mSerialNo],
						[mPcNo], 
						[mItemNo], 
						[mEndDate], 
						[mIsConfirm], 
						[mStatus], 
						[mCnt], 
						[mCntUse],
						[mOwner],
						[mPracticalPeriod],	mBindingType )
					VALUES(
						@aSerial,
						@mNo, 
						@mItemNo, 
						@aEndDay, 
						@mIsConfirm, 
						@mStatus, 
						@mCnt, 
						@pCntUse,
						@aOwner,
						@pPraticalHour,@pBindingType)
					
					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF  @aErr <> 0 OR 	@aRowCnt = 0
					BEGIN				
						ROLLBACK TRAN
						SET @aErr = 9
						GOTO T_END	
					END		
				END
			END 
			ELSE
			BEGIN
				EXEC @aSerial =  dbo.UspGetItemSerial 	-- 시리얼 얻기 
				IF @aSerial <= 0
				BEGIN
					ROLLBACK TRAN
					SET @aErr = 7		
					GOTO T_END	
				END		
				INSERT dbo.TblPcInventory(				
					[mSerialNo],
					[mPcNo], 
					[mItemNo], 
					[mEndDate], 
					[mIsConfirm], 
					[mStatus], 
					[mCnt], 
					[mCntUse],
					[mOwner],
					[mPracticalPeriod], mBindingType )
				VALUES(
					@aSerial,
					@mNo, 
					@mItemNo, 
					@aEndDay, 
					@mIsConfirm, 
					@mStatus, 
					@mCnt, 
					@pCntUse,
					@aOwner,
					@pPraticalHour, @pBindingType )
					
				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF  @aErr <> 0 OR 	@aRowCnt = 0
				BEGIN				
					ROLLBACK TRAN
					SET @aErr = 10
					GOTO T_END	
				END		
			END			
		END
	COMMIT TRAN;
end
T_END:
	--SELECT @aErr, @aSerial AS Serial, DATEDIFF(mi,GETDATE(),@aEndDay)
	
	RETURN 0

GO

