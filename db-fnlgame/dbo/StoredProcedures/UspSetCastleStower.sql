/******************************************************************************
**		Name: UspSetCastleStower
**		Desc: 
**
**		Auth:
**		Date:
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	2013-06-20	傍籍痹				荐沥
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetCastleStower]
       @pPlaceNo   INT
       ,@pGuildNo  INT
       ,@pIsNew    BIT    -- 曼捞搁林牢捞函版灯促
       ,@pLimitWeek SMALLINT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON     -- Count-set搬苞甫积己窍瘤富酒扼
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE     @aErrNo            INT
	SET         @aErrNo            = 0   

	UPDATE dbo. TblCastleTowerStone
	SET
		[mChgDate] =CASE @pIsNew WHEN 0 THEN [mChgDate] ELSE GETDATE() END,
		[mGuildNo] =@pGuildNo,
		[mAssetBuy] =0,
		[mAssetHunt] =0,
		[mAssetGamble] =0,
		[mSiegeDate] =0,
		[mSiegeDayOfWeek] =0,
		[mSiegeDayOfWeekLimit] =@pLimitWeek
	WHERE ( [mPlace]=@pPlaceNo )

	IF(1 <> @@ROWCOUNT )
	BEGIN
		 SET @aErrNo = 1
	END

	RETURN(@aErrNo )

GO

