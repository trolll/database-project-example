/******************************************************************************
**		Name: UspUpdatePcQuestConditionCnt
**		Desc: QA의 편의성을 위해 몬스터 킬 카운트를 변경한다.
**
**		Auth: 정진호
**		Date: 2011-01-05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2011-10-10	정진호				사냥한 몬스터 수를 반환하도록 변경
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdatePcQuestConditionCnt]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pMonNo   		INT
	,@pCnt   		INT
	,@aResultCnt	INT OUTPUT	-- 사냥한 몬스터 수
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    UPDATE dbo.TblPcQuestCondition 
	SET @aResultCnt = mCnt = 
		CASE
			WHEN 0 < (mCnt + @pCnt) THEN mCnt + @pCnt
			ELSE 0
		END
	WHERE mPcNo = @pPcNo 
			AND mQuestNo = @pQuestNo
            AND mMonsterNo = @pMonNo
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(2);
	END
	 		 
	RETURN(0)

GO

