/******************************************************************************  
**  File: 
**  Name: UspGetPcSkillPack  
**  Desc: PC의 스킬팩들을 로드한다.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcSkillPack]
	 @pPcNo		INT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
	IF @pPcNo = 1 OR @pPcNo = 0 
		RETURN(0)	-- NPC, 버려진 아이템
	
	SELECT 
		mSPID,
		DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
		FROM dbo.TblPcSkillPackInventory WITH(NOLOCK)
		WHERE 	mPcNo = @pPcNo
					AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY mSPID ASC

GO

