/******************************************************************************
**		Name: UspGetGuildQuestPc
**		Desc: 辨靛 涅胶飘 措惑磊
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-19
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE  PROCEDURE [dbo].[UspGetGuildQuestPc]
	  @pGuildMasterNo	INT
	 ,@pQuestNo			INT 
	 ,@pPcNo			INT OUTPUT
	 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT @pPcNo = 0;
			
    SELECT 
			@pPcNo = 
					CASE WHEN mPcNo IS NULL THEN 0 
						ELSE mPcNo	
					END 
    FROM dbo.TblGuildQuest
    WHERE mGuildMasterNo = @pGuildMasterNo 
		AND mMQuestNo = @pQuestNo

	RETURN(0)

GO

