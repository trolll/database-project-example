/******************************************************************************  
**  Name: UspGetStatisticsItemByLevelClass  
**  Desc: 아이템 통계 테이블 로드
**  
**  Auth:   
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  김강호      mItemStatus,  mCnsmRegFee, mCnsmBuyFee 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetStatisticsItemByLevelClass]  
AS  
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
  
	SELECT   
		mItemNo  
		, mMerchantCreate  
		, mMerchantDelete  
		, mReinforceCreate  
		, mReinforceDelete  
		, mCraftingCreate  
		, mCraftingDelete  
		, mPcUseDelete  
		, mNpcUseDelete  
		, mNpcCreate  
		, mMonsterDrop  
		, mGSExchangeCreate   
		, mGSExchangeDelete  
		, mLevel  
		, mClass    
		, mItemStatus
		, mCnsmRegFee
		, mCnsmBuyFee
	FROM   
		dbo.TblStatisticsItemByLevelClass;

GO

