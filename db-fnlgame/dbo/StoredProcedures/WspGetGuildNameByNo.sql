--------------------------------
-- ░┤├╝╕φ : WspGetGuildNameByNo
-- ╝│╕φ : ▒µ╡σ└╟ └╠╕º└╗ ░í┴«┐┬┤┘
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.07.11
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
--------------------------------

CREATE PROCEDURE [dbo].[WspGetGuildNameByNo] 
	@mGuildNo		INT	-- ▒µ╡σ╣°╚ú
AS
	SET NOCOUNT ON

	SELECT
		mGuildNm
	FROM
		TblGuild
	WITH (NOLOCK)
	WHERE
		mGuildNo = @mGuildNo

	SET NOCOUNT OFF

GO

