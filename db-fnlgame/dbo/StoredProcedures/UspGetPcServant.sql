/******************************************************************************
**		Name: UspGetPcServant
**		Desc: 辑锅飘 沥焊 掘扁
**
**		Auth: 捞侩林
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2015.09.14	捞侩林				mIsRestore 眠啊
**		2016.10.07	捞侩林				mAddStr, mAddDex, mAddInt, mCombineCount 眠啊
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcServant]
	@pPcNo			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	mSerialNo, mName, mLevel
		, mExp, mFriendly, mSkillPoint, mSkillTreePoint, mIsRestore
		, mAddStr, mAddDex, mAddInt, mCombineCount
	FROM dbo.TblPcServant WITH(INDEX(UCL_PK_TblPcServant), NOLOCK)
	WHERE  mPcNo = @pPcNo
	ORDER BY mSerialNo ASC

	SET NOCOUNT OFF;

GO

