/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspBidGuildAgitAuction    Script Date: 2008-11-10 10:37:16 ******/





CREATE Procedure [dbo].[UspBidGuildAgitAuction]			-- ????? ?? ?? (?? ??? ??? ??)
	 @pTerritory		INT				-- ????
	,@pGuildAgitNo		INT				-- ???????
	,@pBidGID		INT OUTPUT			-- ???? [??/??]
	,@pBidMoney		BIGINT OUTPUT		-- ???? [??/??]
	,@pCurBidGuildName	NVARCHAR(50) OUTPUT	-- ?????? [??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	SET		@pCurBidGuildName = ''

	-- ??? ???? ??
	IF NOT EXISTS(SELECT mGID FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pBidGID)
	BEGIN
		SET @aErrNo = 2			-- 2 : ????? ???? ??
		GOTO LABEL_END_LAST			 
	END

	-- ??? ???? ??
	DECLARE @aCurGuildMoney	BIGINT
	SELECT @aCurGuildMoney = mGuildMoney FROM TblGuildAccount WITH (NOLOCK) WHERE mGID = @pBidGID
	IF (@aCurGuildMoney < @pBidMoney)
	BEGIN
		SET @aErrNo = 3			-- 3 : ????? ??
		GOTO LABEL_END_LAST			 
	END

	-- ?? ?????? ???? ?? ??? ????
	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mOwnerGID = @pBidGID)
	BEGIN
		SET @aErrNo = 4			-- 4 : ?? ?? ?????? ???? ??
		GOTO LABEL_END_LAST			 
	END

	-- ?? ?????? ?? ?? ??? ????
	IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH(NOLOCK) WHERE mCurBidGID = @pBidGID)
	BEGIN
		SET @aErrNo = 8			-- 8 : ?? ???
		GOTO LABEL_END_LAST			 
	END

	-- ????? ?? ??
	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
	BEGIN
		-- ????? ???

		DECLARE @aCurBidGID	INT
		DECLARE @aCurBidMoney	BIGINT
		DECLARE @aIsSelling		TINYINT

		SELECT @aCurBidGID = mOwnerGID, @aCurBidMoney = mSellingMoney, @aIsSelling = mIsSelling FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

		-- ?? ?????? ???? ?? ???? ?? ???? ?? ??
		IF (@aCurBidGID <> 0 AND @aIsSelling = 0)
		BEGIN
			SET @aErrNo = 9			-- 9 : ?? ?????? ???? ??
			GOTO LABEL_END_LAST			 
		END

		-- ?? ????? ??? ????? ??
		IF (@aCurBidMoney >= @pBidMoney)
		BEGIN
			-- ????? ?????? ??
			SET @pBidMoney = @aCurBidMoney
			SET @pBidGID = @aCurBidGID
			SET @pCurBidGuildName = ISNULL((CASE @aCurBidGID
				WHEN 0 THEN N'<NONE>'
				ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = @aCurBidGID)
				END), N'<FAIL>')
			SET @aErrNo = 5			-- 5 : ?????? ????? ??
			GOTO LABEL_END_LAST			 
		END

		-- ?? ?? ?????? ??? ???? ????? ??
		IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
		BEGIN
			SELECT @aCurBidGID = mCurBidGID, @aCurBidMoney = mCurBidMoney FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			IF (@aCurBidMoney >= @pBidMoney AND @aCurBidGID <> 0)
			BEGIN				-- ????? ?????? ??
				SET @pBidMoney = @aCurBidMoney
				SET @pBidGID = @aCurBidGID
				SET @pCurBidGuildName = ISNULL((CASE @aCurBidGID
					WHEN 0 THEN N'<NONE>'
					ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = @aCurBidGID)
					END), N'<FAIL>')
				SET @aErrNo = 6		-- 6 : ???? ?? ?? ???? ????? ??
				GOTO LABEL_END_LAST			 
			END
		END

		-- ???? (??)

		BEGIN TRAN

		-- ?????? ? ??
		UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney - @pBidMoney WHERE mGID = @pBidGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1	-- 1 : DB ???? ??
			GOTO LABEL_END			 
		END	 

		-- ????? ???? ??
		IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo)
		BEGIN
			SELECT @aCurBidGID = mCurBidGID, @aCurBidMoney = mCurBidMoney FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

			-- ?? ????? ???? (?? 3%)
			SET @aCurBidMoney = (@aCurBidMoney * (100 - 3)) / 100

			-- ?? ????? ???? ??
			UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney + @aCurBidMoney WHERE mGID = @aCurBidGID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB ???? ??
				GOTO LABEL_END			 
			END

			-- ??? ???? ??
			UPDATE TblGuildAgitAuction SET mCurBidGID = @pBidGID, mCurBidMoney = @pBidMoney, mRegDate = GETDATE()  WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB ???? ??
				GOTO LABEL_END			 
			END	 

			-- ???? ?? ??
			SET @pCurBidGuildName = ISNULL((CASE @pBidGID
				WHEN 0 THEN N'<NONE>'
				ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = @pBidGID)
				END), N'<FAIL>')
		END
		ELSE
		BEGIN
			-- ??? ???? ??
			INSERT TblGuildAgitAuction (mTerritory, mGuildAgitNo, mCurBidGID, mCurBidMoney) VALUES (@pTerritory, @pGuildAgitNo, @pBidGID, @pBidMoney)
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB ???? ??
				GOTO LABEL_END			 
			END

			-- ???? ?? ??
			SET @pCurBidGuildName = ISNULL((CASE @pBidGID
				WHEN 0 THEN N'<NONE>'
				ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = @pBidGID)
				END), N'<FAIL>')
		END
	END
	ELSE
	BEGIN
		-- ????? ???? ??

		SET @aErrNo = 7		-- 7 : ?? ??? ??? ?????? ???? ??
		GOTO LABEL_END_LAST			 
	END
	
LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

