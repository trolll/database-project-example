CREATE PROCEDURE [dbo].[WspListStoreItemBySerial]
	@mSerialString VARCHAR(500)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		(	SELECT top 1 mUserId 
			FROM [FNLAccount].[dbo].[TblUser] 
			WHERE mUserNo = p.mUserNo) AS mUserId
		,p.mSerialNo
		,p.mRegDate
		, '' AS mEndDate -- АЇИї±в°Ј
		,ii.IName -- ѕЖАМЕЫ АМё§
		, p.mStatus -- »уЕВ
		, p.mCnt -- °іјц
		, p.mItemNo -- TID
		, p.mIsSeizure -- ѕР·щї©єО
		, p.mPracticalPeriod
		, p.mBindingType
	FROM 
		[dbo].[TblPcStore] AS p 
		INNER JOIN [FNLParm].[dbo].[DT_Item] AS ii 
			ON p.mItemNo = ii.IID
		INNER JOIN dbo.fn_SplitTSQL (@mSerialstring,',') a
			ON p.mSerialNo = a.element
	ORDER BY p.mItemNo

GO

