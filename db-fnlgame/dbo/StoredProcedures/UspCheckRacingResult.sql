/******************************************************************************
**		Name: UspCheckRacingResult
**		Desc: 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2011-07-05	정진호				티켓 갯수를 반환하도록 수정
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckRacingResult]
	 @pSerialNo		BIGINT
	,@pPlace		INT		OUTPUT
	,@pStage		INT		OUTPUT
	,@pResult		TINYINT	OUTPUT
	,@pDividend		FLOAT	OUTPUT
	,@pTicketCnt	INT		OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set결과를 생성하지 말아라.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	DECLARE	@aNID			INT
	DECLARE	@aWinnerNID		INT
	SET		@pPlace		= 0
	SET		@pStage		= 0
	SET		@pResult		= 0	-- 0:실패 / 1:성공 / 2:DRAW
	SET		@pDividend		= 0.0
	SET		@pTicketCnt     = 0

	SELECT @pPlace = mPlace, @pStage = mStage, @aNID = mNID, @pTicketCnt = mTicketCnt 	FROM TblRacingTicket WHERE mSerialNo = @pSerialNo
	IF(1 <> @@ROWCOUNT)
	 BEGIN
		SET @pResult	= 2	-- 오류인 경우에도 일단 DRAW 로 간주
	 END
	ELSE
	 BEGIN
		SELECT @aWinnerNID = mWinnerNID, @pDividend = mDividend FROM TblRacingResult WHERE mPlace = @pPlace AND mStage = @pStage
		IF(1 <> @@ROWCOUNT)
		 BEGIN
			SET @pResult	= 2		-- 오류인 경우에도 경우에도 일단 DRAW로 간주
		 END
		ELSE
		 BEGIN
			IF (@aWinnerNID = @aNID)
			 BEGIN
				SET @pResult	= 1 	-- 승자있음
			 END
			ELSE IF (@aWinnerNID = 0)
			 BEGIN
				SET @pResult	= 2	-- DRAW 임
			 END
		 END
	 END
	 
	RETURN(@aErrNo)

GO

