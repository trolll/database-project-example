/******************************************************************************  
**  Name: UspGetPcBeadOne
**  Desc: 구슬 한 개 리스팅(장비에 결합된 구슬)
**  
**                
**  Return values:  
**   레코드셋
**   
**                
**  Author: 김강호
**  Date: 2011-06-28
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcBeadOne]
	@pSn BIGINT
	,@pBeadIndex TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	

	SELECT 
	a.mOwnerSerialNo
	,a.mItemNo
	,a.mBeadIndex
	,DATEDIFF(mi,GETDATE(),a.mEndDate) as mEndDateA
	,a.mCntUse
	FROM	
	TblPcBead a
	INNER JOIN dbo.TblPcInventory b ON a.mOwnerSerialNo=b.mSerialNo
	AND a.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	AND b.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	WHERE a.mOwnerSerialNo=@pSn and a.mBeadIndex=@pBeadIndex

GO

