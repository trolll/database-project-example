CREATE PROCEDURE [dbo].[WspGetQuickSupplication]
	@pQSID BIGINT
AS
	SET NOCOUNT ON 
	
	SELECT
		T1.mRegDate
		,T1.mQSID
		,T2.mSupplication
		,T2.mIp
		,T1.mStatus
		,T1.mCategory
		,T1.mPcNo
		,p.mNm
	FROM dbo.TblQuickSupplicationState T1
		INNER JOIN dbo.TblQuickSupplication T2
			ON T1.mQSID = T2.mQSID
		INNER JOIN dbo.TblPc AS p 
			ON T1.mPcNo = p.mNo
	WHERE T1.mQSID=@pQSID

GO

