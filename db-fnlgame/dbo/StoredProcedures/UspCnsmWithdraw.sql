/******************************************************************************  
**  Name: UspCnsmWithdraw  
**  Desc: 위탁판매 정산금 찾기
**  
**                
**  Return values:  
**   0 : 작업 처리 성공  
**   > 0 : SQL Error  
**                
**  Author: 김강호  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmWithdraw]
 @pPcNo				INT
 ,@pMoneySn			BIGINT	-- 돈 Sn
 ,@pCnt				INT		-- 인출액수
 ,@pMoneyItemNo		INT		-- 돈 아이템 번호
 ,@pStatus			TINYINT		-- 돈 아이템 번호
 ,@pMoneySnNew		BIGINT OUTPUT	-- 돈 Sn(돈이 새로 생겼을 경우)
 ,@pResultMoneyCnt	INT OUTPUT		-- 결과 돈 액수
 ,@pResultBalance	BIGINT OUTPUT	-- 결과 정산금
AS  

	 SET NOCOUNT ON  ;
	 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  ;


	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;


	DECLARE @aCurMoneyCnt INT;
	SELECT 	@aCurMoneyCnt = ISNULL(mCnt, 0)
	FROM dbo.TblPcInventory
	WHERE mPcNo=@pPcNo AND mSerialNo=@pMoneySn AND mIsSeizure = 0;

	-- 너무 많이 찾으려고 하는지 체크
	IF 1000000000 < (@pCnt+@aCurMoneyCnt) OR @pCnt < 1
	BEGIN
		SET @aErrNo = 1 ;-- 찾으려는 액수가 부적절합니다.
		GOTO T_END   ;		
	END
	
	DECLARE @aBalance BIGINT
	SELECT @aBalance = mBalance
	FROM dbo.TblConsignmentAccount
	WHERE  mPcNo = @pPcNo;
	IF @aBalance IS NULL OR @aBalance < @pCnt
	BEGIN
		SET @aErrNo = 1; -- 찾으려는 액수가 부적절합니다.
		GOTO T_END;
	END
	
	

	BEGIN TRAN

	-- 정산금 감소
	UPDATE dbo.TblConsignmentAccount
	SET @pResultBalance = mBalance = mBalance - @pCnt
	WHERE mPcNo = @pPcNo;
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0
	BEGIN
		ROLLBACK TRAN;
		SET @aErrNo = 2 ;-- 정산금 감소 실패
		GOTO T_END   ;		
	END

	IF 0 = @pMoneySn
	BEGIN
		EXEC @pMoneySnNew =  dbo.UspGetItemSerial;  -- 신규 시리얼 얻기   
		IF @pMoneySnNew <= 0  
		BEGIN  
			ROLLBACK TRAN  ;
			SET @aErrNo = 3 ;-- 시리얼 생성 실패
			GOTO T_END;    
		END	

		declare @aEndDay datetime;
		SELECT   @aEndDay = dbo.UfnGetEndDate(GETDATE(), 10000)  ;

		-- 인벤토리에 아이템 추가
		INSERT dbo.TblPcInventory(
			[mSerialNo],  
			[mPcNo],   
			[mItemNo],   
			[mEndDate],   
			[mIsConfirm],   
			[mStatus],   
			[mCnt],   
			[mCntUse],  
			[mOwner],  
			[mPracticalPeriod], 
			mBindingType )  
		VALUES(  
			@pMoneySnNew,  
			@pPcNo,   
			@pMoneyItemNo,--@aItemNo,   
			@aEndDay,--@aEndDate,
			1, --@aIsConfirm,   
			@pStatus, --@aStatus,   
			@pCnt,   
			0, --@aCntUse,  
			0, --@aOwner,  
			0, --@aPracticalPeriod, 
			0) --@aBindingType);
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT;  
		IF  @aErrNo <> 0 OR  @aRowCnt <= 0  
		BEGIN
			ROLLBACK TRAN  ;
			SET @aErrNo = 4 ;-- 아이템 추가 실패
			GOTO T_END;
		END
		SET @pResultMoneyCnt = @pCnt;
	END
	ELSE
	BEGIN
		SET @pMoneySnNew = @pMoneySn;

		-- 소지금에 추가
		UPDATE dbo.TblPcInventory
		SET @pResultMoneyCnt = mCnt = mCnt + @pCnt
		WHERE mPcNo=@pPcNo AND mSerialNo=@pMoneySn AND mIsSeizure = 0;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0
		BEGIN
			ROLLBACK TRAN;
			SET @aErrNo = 5; -- 인벤토리 업데이트 실패
			GOTO T_END  ; 		
		END
		IF 1000000000 < @pResultMoneyCnt
		BEGIN
			ROLLBACK TRAN;
			SET @aErrNo = 6; -- 인벤토리 잔액이 너무 크다
			GOTO T_END   ;		
		END
	END


T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN  ;

T_END:     
	SET NOCOUNT OFF  ; 
	RETURN(@aErrNo) ;

GO

