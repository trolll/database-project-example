/******************************************************************************
**		Name: UspGetPcCalendarSchedule
**		Desc: ´Þ·Â ÀÏÁ¤¸ñ·Ï
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-21
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcCalendarSchedule]
	 @pPcNo			INT 
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- ¾ÕµÚ·Î 2°³¿ù¾¿ ÃÑ 5°³¿ù ³»¿ë¸¸ º¸³½´Ù.
	-- ÇöÀç 4¿ùÀÌ¸é 2,3,4,5,6 ¿ù ³»¿ë¸¸ º¸³½´Ù. ³¯Â¥´Â ½Å°æ¾²Áö ¾Ê´Â´Ù.
	SELECT 
		 T1.mSerialNo
		,T1.mOwnerPcNo
		,RTRIM(T3.mNm)
		,T2.mTitle
		,T2.mScheduleDate
	FROM dbo.TblCalendarPcSchedule AS T1
		 INNER JOIN dbo.TblCalendarSchedule AS T2
				ON T1.mSerialNo = T2.mSerialNo
		 INNER JOIN dbo.TblPc AS T3
				ON T1.mOwnerPcNo = T3.mNo
	WHERE T1.mPcNo = @pPcNo
		 AND -3 < DATEDIFF( MM, GETDATE(), T2.mScheduleDate )
		 AND 3 > DATEDIFF( MM, GETDATE(), T2.mScheduleDate )
		 ORDER BY T2.mScheduleDate DESC	--140611 ¼öÁ¤

GO

