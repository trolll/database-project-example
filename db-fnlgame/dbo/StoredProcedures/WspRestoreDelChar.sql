--------------------------------
-- ░┤├╝╕φ : [WspRestoreDelChar]
-- ╝│╕φ : ╗Φ┴ª╡╚ ─│╕»┼═╕ª ║╣▒╕╜├┼▓┤┘
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.10.16
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
-- ╝÷┴ñ│╗┐δ : 
--------------------------------
-- ErrNo 1 : ╟╪┤τ ░Φ┴ñ┐í ║≤╜╜╖╘└╠ ╛°┤┘
-- ErrNo 2 : Update░í ╟╧│¬└╟ ╖╬┐∞┐í ┐╡╟Γ└╗ ┴╓┴÷ ╛╩╛╥┤┘.(;;)
-- ErrNo 3 : ╗Φ┴ª╡╚ ─│╕»┼═╕φ└╕╖╬ ╗²╝║╡╚ ─│╕»┼═░í ┴╕└τ╟╤┤┘
-- ErrNo 4 : @mNo ┐í ╟╪┤τ╟╧┤┬ ╗Φ┴ª╡╚ ─│╕»┼═░í ╛°┤┘
-- ErrNo 5 : ╟╪┤τ ─│╕»┼═└╟ ╜╜╖╘└╠ └╠╣╠ ╗τ┐δ┴▀└╠┤┘.
-- ErrNo 0 : ┴ñ╗≤└√└╕╖╬ ╛≈╡Ñ└╠╞« ╡╟╛·┤┘
--------------------------------
CREATE PROCEDURE [dbo].[WspRestoreDelChar]
	@mNo 		INT,		-- ╗Φ┴ª╡╚ ─│╕»┼═ ╣°╚ú
	@mUserNo	INT		-- └»└·╣°╚ú
AS
	SET NOCOUNT ON
	DECLARE	@mCharNm	VARCHAR(20)
	DECLARE	@aErrNo		INT
	DECLARE	@aSlot		INT
	DECLARE	@aCnt		INT
		
	SET @aErrNo 	= 1
	
	IF (SELECT COUNT(*) FROM TblPc WITH (NOLOCK) WHERE mOwner = @mUserNo AND mDelDate IS NULL) < 3
	 BEGIN
		--╗Φ┴ª╡╚ ─│╕»┼═└╬┴÷╚«└╬
		SET @aErrNo = 4
		IF EXISTS(SELECT * FROM TblPc WITH (NOLOCK) WHERE mNo = @mNo AND mDelDate IS NOT NULL)
 		 BEGIN
			--╜╜╖╘╚«└╬
			SELECT @aSlot = mSlot FROM TblPc WITH (NOLOCK) WHERE mNo = @mNo AND mDelDate IS NOT NULL
			SET @aErrNo = 5
			IF NOT EXISTS(SELECT * FROM TblPc WITH(NOLOCK) WHERE mOwner = @mUserNo AND mSlot = @aSlot AND mDelDate IS NULL)	
			 BEGIN
				--╗Φ┴ª╡╟▒Γ└ⁿ ─│╕»┼═ ╕φ ╚╣╡µ
				SELECT @mCharNm = mPcNm FROM TblPcDeleted WITH (NOLOCK) WHERE mPcNo = @mNo
				--╗Φ┴ª╡╟▒Γ└ⁿ ─│╕»┼═╕φ└╠ ╟÷└τ ░╘└╙╝¡╣÷┐í ┴╕└τ ╟╧┴÷ ╛╩┤┬┴÷ ╚«└╬
				IF NOT EXISTS(SELECT * FROM TblPc WITH (NOLOCK) WHERE mNm = @mCharNm)
			 	 BEGIN
				--╗Φ┴ª╡╟▒Γ└ⁿ ─│╕»┼═ ╕φ└╕╖╬ Update
					BEGIN TRAN	
					UPDATE TblPc SET mNm = @mCharNm, mDeldate = NULL WHERE mNo = @mNo
					SET @aErrNo = 0
			
					IF @@ROWCOUNT <> 1 
					 BEGIN
						SET @aErrNo = 2
						GOTO LABEL_END
					 END

					DELETE TblPcDeleted WHERE mPcNo = @mNo
					IF @@ROWCOUNT <> 1 
					 BEGIN
						SET @aErrNo = 2
						GOTO LABEL_END
					 END
				 END
				ELSE
				 BEGIN
					SET @aErrNo = 3
					GOTO LABEL_END
				 END
			  END
			ELSE
			 BEGIN	--╜╜╖╘└║ ┐⌐└»║╨└╠ └╓┴÷╕╕ // ╗Φ┴ª ─│╕»┼═└╟ ┴ñ║╕░í ╗τ┐δ╟╧┤° ╜╜╖╘└║ ╗τ┐δ┴▀└╠┤┘. // ╜╜╖╘└╗ ║»░µ╟╪╛▀╡╚┤┘
				SET @aCnt = 0
				SET @aSlot = 0
				WHILE (@aCnt < 3)	-- ─│╕»┼═ ╜╜╖╘└╠ ├╓┤δ 3░│┤┘
				 BEGIN
					IF NOT EXISTS(SELECT * FROM TblPc WITH (NOLOCK) WHERE mOwner = @mUserNo AND mSlot = @aCnt AND mDelDate IS NULL)
					 BEGIN
						SET @aSlot = @aCnt
					 END
					
					SET @aCnt = @aCnt + 1
				 END
				--╗Φ┴ª╡╟▒Γ└ⁿ ─│╕»┼═ ╕φ ╚╣╡µ
				SELECT @mCharNm = mPcNm FROM TblPcDeleted WITH (NOLOCK) WHERE mPcNo = @mNo
				--╗Φ┴ª╡╟▒Γ└ⁿ ─│╕»┼═╕φ└╠ ╟÷└τ ░╘└╙╝¡╣÷┐í ┴╕└τ ╟╧┴÷ ╛╩┤┬┴÷ ╚«└╬
				IF NOT EXISTS(SELECT * FROM TblPc WITH (NOLOCK) WHERE mNm = @mCharNm)
			 	 BEGIN
				--╗Φ┴ª╡╟▒Γ└ⁿ ─│╕»┼═ ╕φ└╕╖╬ Update
					BEGIN TRAN	
					UPDATE TblPc SET mNm = @mCharNm, mDelDate = NULL, mSlot = @aSlot WHERE mNo = @mNo
					SET @aErrNo = 0
			
					IF @@ROWCOUNT <> 1 
					 BEGIN
						SET @aErrNo = 2
						GOTO LABEL_END
					 END

					DELETE TblPcDeleted WHERE mPcNo = @mNo
					IF @@ROWCOUNT <> 1 
					 BEGIN
						SET @aErrNo = 2
						GOTO LABEL_END
					 END
				 END
				ELSE
				 BEGIN
					SET @aErrNo = 3
					GOTO LABEL_END
				 END
					
			 END
		  END
	   END

LABEL_END:
	IF @aErrNo = 0 
 	 BEGIN
 		COMMIT TRAN
 	 END
	ELSE
 	 BEGIN
 		IF (@aErrNo <> 1) AND (@aErrNo <> 4) AND (@aErrNo <> 5)
		 BEGIN
		 	ROLLBACK TRAN
		 END
 	 END

	RETURN @aErrNo

GO

