/******************************************************************************
**		Name: UspSelectPcQuestConditionCnt
**		Desc: 
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013-04-07	沥柳龋				阁胶磐 ID档 罐酒坷档废 荐沥
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSelectPcQuestConditionCnt]
     @pPcNo			INT
	,@pQuestNo		INT	 
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    SELECT 
		mCnt, mMaxCnt, mMonsterNo
    FROM dbo.TblPcQuestCondition 
    WHERE mPcNo = @pPcNo
	AND mQuestNo = @pQuestNo

GO

