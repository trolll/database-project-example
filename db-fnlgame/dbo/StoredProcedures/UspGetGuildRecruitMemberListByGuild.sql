/****** Object:  Stored Procedure dbo.UspGetGuildRecruitMemberListByGuild    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitMemberListByGuild    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitMemberListByGuild    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitMemberListByGuild    Script Date: 2010-12-23 17:46:03 ******/
/******************************************************************************
**		Name: UspGetGuildRecruitMemberListByGuild
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩吝牢 某腐磐 沥焊甫 掘绢柯促.
**
**		Auth: 辫锐档, 沥柳龋
**		Date: 2009.07.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetGuildRecruitMemberListByGuild]
	@pGuildNo		INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		T1.mRegDate,
		mPcNo,
		mState,
		mPcMsg,
		RTRIM(T2.mNm) mPcNm,
		T3.mLevel mPcLv,
		T2.mClass mPcClass

	FROM
		dbo.TblGuildRecruitMemberList T1
			INNER JOIN dbo.TblPc T2
				ON T1.mPcNo = T2.mNo 
			INNER JOIN dbo.TblPcState T3
				ON T2.mNo = T3.mNo
	WHERE
		T1.[mGuildNo] = @pGuildNo 
		and T1.mIsHide = 0
		AND T2.mDelDate IS NULL

GO

