/****** Object:  Stored Procedure dbo.UspPopGuildRecruit    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspPopGuildRecruit    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspPopGuildRecruit    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspPopGuildRecruit    Script Date: 2010-12-23 17:46:00 ******/
/******************************************************************************
**		Name: UspPopGuildRecruit
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩吝牢 辨靛 沥焊甫 力芭茄促.
**
**		Auth: 辫锐档, 沥柳龋
**		Date: 2009.07.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPopGuildRecruit]
	@pGuildNo			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	BEGIN TRAN

		DELETE dbo.TblGuildRecruitLimitValue
		WHERE [mGuildNo] = @pGuildNo;

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		DELETE dbo.TblGuildRecruit
		WHERE [mGuildNo] = @pGuildNo;

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

	COMMIT TRAN

	RETURN(0)

GO

