/******************************************************************************
**		Name: UspPushEventMakingQuestRank
**		Desc: 珐欧阑 历厘秦敌促.
**
**		Auth: 沥柳龋
**		Date: 2010-04-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPushEventMakingQuestRank]
	 @pQuestNo			INT
	 ,@pPcNo			INT
	 ,@pLastRank		TINYINT
	 ,@pEventQRank		INT OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRank		INT
	SELECT  @aRank = 0, @pEventQRank = 0

	SELECT TOP 1 @aRank = mRank
	FROM dbo.TblEventQuestRank
	WHERE mQuestNo = @pQuestNo
	ORDER BY mRank DESC

	IF @aRank <= @pLastRank
	BEGIN
		SET @pEventQRank = @aRank + 1

		INSERT INTO dbo.TblEventQuestRank(mQuestNo, mRank, mPcNo)
		VALUES (@pQuestNo, @pEventQRank, @pPcNo)
		IF(0 <> @@ERROR)
		BEGIN
			RETURN(1);
		END
	END
	ELSE
	BEGIN
		SET @pEventQRank = @aRank
	END
	 		 
	RETURN(0);

GO

