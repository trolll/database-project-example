CREATE PROCEDURE [dbo].[WspGetCharInfo]	
	@mNm CHAR(12) -- ДіёЇЕНён
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		p.mNm, -- ДіёЇЕНён
		u.mUserId, -- °иБ¤ён
		p.mClass, -- Е¬·ЎЅє		
		(
			SELECT 
				COUNT(*) 
			FROM [FNLAccount].[dbo].[TblUserBlock] b
			WHERE mUserNo = p.mOwner AND ( mCertify > GETDATE()  OR mChat > 0 OR mBoard > GETDATE())  ) AS mBlock, 		
		(
			SELECT 
				COUNT(*) 
			FROM [FNLAccount].[dbo].[TblUserBlockHistory] bh			
			WHERE mUserNo = p.mOwner) AS mBlockHistory, 
		p.mRegDate, -- ДіёЇЕН »эјєАП
		s.mLevel, -- ·№є§
		s.mExp, -- °жЗиДЎ
		s.mPosX, s.mPosY, s.mPosZ, -- А§ДЎ
		s.mHp, -- HP
		s.mMp, -- MP
		s.mChaotic, -- јєЗв
		g.mGuildNm, -- ±жµеён
		s.mLoginTm, -- ГЦ±Щ ј­№ц БўјУ АПЅГ
		s.mLogoutTm, -- ГЦ±Щ ј­№ц ·О±ЧѕЖїф АПЅГ
		s.mIp, -- ГЦ±Щ ј­№ц БўјУ IP
		p.mDelDate, -- ДіёЇЕН »иБ¦АП
		p.mOwner, -- ДіёЇЕН °иБ¤ №шИЈ
		p.mNo, -- ДіёЇЕН №шИЈ
		s.mIsPreventItemDrop	-- ѕЖАМЕЫ µе¶ш №жБц ї©єО 
	FROM
		[dbo].[TblPc] AS p 
		INNER JOIN	[dbo].[TblPcState] AS s 
			ON	p.mNo = s.mNo
		INNER JOIN	[FNLAccount].[dbo].[TblUser] AS u
			ON	p.mOwner = u.mUserNo			
		LEFT OUTER JOIN [dbo].[TblGuildMember] gm
			ON	p.mNo = gm.mPcNo
		Left OUTER JOIN	[dbo].[TblGuild] As g 
			ON	g.mGuildNo = gm.mGuildNo
	WHERE		
		p.mNm = @mNm
		AND p.mDelDate IS NULL

GO

