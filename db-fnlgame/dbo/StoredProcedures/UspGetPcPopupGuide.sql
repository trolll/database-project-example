/****** Object:  Stored Procedure dbo.UspGetPcPopupGuide    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetPcPopupGuide    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspGetPcPopupGuide    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetPcPopupGuide    Script Date: 2010-12-23 17:45:59 ******/
/******************************************************************************
**		Name: UspGetPcPopupGuide
**		Desc: TblPcPopupGuide 甫 掘绢 坷绰 Procedure
**
**		Auth: 辫锐档
**		Date: 2009-09-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcPopupGuide]
	@pPcNo				INT,
	@pGuideNo			INT,
	@pReadCnt			INT OUTPUT
AS
	SET NOCOUNT ON
	SET QUOTED_IDENTIFIER ON

	SELECT @pReadCnt = [mReadCnt]
	FROM dbo.TblPcPopupGuide
	WHERE ([mPcNo]=@pPcNo)
		AND ([mGuideNo]=@pGuideNo)
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)
	END
	
	----------------------------------------------
	-- 粮犁窍瘤 臼促搁 ReadCnt 绰 0 捞促.
	----------------------------------------------
	IF(@pReadCnt IS NULL)
	BEGIN
		SELECT @pReadCnt = 0
	END
	
	RETURN(0)

GO

