/******************************************************************************
**		Name: UspGetPcScheduleSerial
**		Desc: ÀÏÁ¤±×·ì¿¡ µî·ÏµÈ ½Ã¸®¾ó 
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-04-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcScheduleSerial]
	 @pOwnerPcNm		CHAR(12)
	,@pMemberPcNm		CHAR(12)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT mSerialNo
	FROM dbo.TblCalendarPcSchedule
	WHERE mPcNo IN (
						SELECT  mNo
						FROM dbo.TblPc
						WHERE mNm = @pOwnerPcNm
					)
	AND mOwnerPcNo IN (
						SELECT  mNo
						FROM dbo.TblPc
						WHERE mNm = @pMemberPcNm
					  )

GO

