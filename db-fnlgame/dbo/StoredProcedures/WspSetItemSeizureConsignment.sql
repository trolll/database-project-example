/******************************************************************************  
**  Name: WspSetItemSeizureConsignment  
**  Desc: 아이템 압류
**  
**                
**  Return values:  
**   0 : 작업 처리 성공  
**   > 0 : SQL Error  
**   
**                
**  Author: 김강호  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[WspSetItemSeizureConsignment]
	@pCnsmNo BIGINT -- 위탁 등록 번호
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;

	DECLARE @aPcNo					INT;
	DECLARE @aCurCnt				INT	;

	DECLARE @aRegDate			DATETIME;
	DECLARE @aSerialNo			BIGINT	;
	DECLARE @aItemNo			INT		;	
	DECLARE @aEndDate			DATETIME;
	DECLARE @aIsConfirm			BIT	;
	DECLARE @aStatus			TINYINT;
	DECLARE @aCntUse			TINYINT;
	DECLARE @aOwner				INT;
	DECLARE @aPracticalPeriod	INT;
	DECLARE @aBindingType		TINYINT;
	DECLARE @aRestoreCnt		TINYINT;

	SELECT @aRowCnt = 0, @aErrNo= 0  ;
	SELECT   
		@aPcNo		= [mPcNo],
		@aCurCnt	= [mCurCnt],
		@aItemNo	= [mItemNo]
	FROM dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmNo;   

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 1;  -- 위탁 아이템이 존재하지 않습니다.(TblConsignment)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 내부 오류
		END
		GOTO T_END ;  
	END  

	SELECT
		@aRegDate			= mRegDate,
		@aSerialNo			= mSerialNo,		
		@aEndDate			= mEndDate,
		@aIsConfirm			= mIsConfirm,
		@aStatus			= mStatus,
		@aCntUse			= mCntUse,
		@aOwner				= mOwner,
		@aPracticalPeriod	= mPracticalPeriod,
		@aBindingType		= mBindingType,
		@aRestoreCnt		= mRestoreCnt
	FROM dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmNo;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 4 ; -- 위탁 아이템이 존재하지 않습니다.(TblConsignmentItem)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100 ; -- DB 내부 오류
		END
		GOTO T_END ;  
	END  


	BEGIN TRAN;

	-- 아이템 삭제
	DELETE dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmNo;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 5; -- 아이템 삭제 실패(TblConsignmentItem)
		GOTO T_END   ;
	END

	DELETE dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmNo and mCurCnt = @aCurCnt;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 6 ;-- 아이템 삭제 실패(TblConsignment)
		GOTO T_END   ;
	END

	-- 인벤토리에 아이템 추가(등록한 사람에게 압류된 상태로 추가)
	INSERT dbo.TblPcInventory(
		[mSerialNo],  
		[mPcNo],   
		[mItemNo],   
		[mEndDate],   
		[mIsConfirm],   
		[mStatus],   
		[mCnt],   
		[mCntUse],  
		[mOwner],  
		[mPracticalPeriod], 
		[mBindingType],
		[mIsSeizure])  
	VALUES(  
		@aSerialNo,  
		@aPcNo,   
		@aItemNo,   
		@aEndDate,   
		@aIsConfirm,   
		@aStatus,   
		@aCurCnt,   
		@aCntUse,  
		@aOwner,  
		@aPracticalPeriod, 
		@aBindingType,
		1) ;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 7; -- 인벤토리 아이템 추가 실패
		GOTO T_END  ; 
	END
	
	-- TODO: 게임 로그 남기기	
	
	COMMIT TRAN
	GOTO T_END


T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN ; 

T_END:     
	SET NOCOUNT OFF  ; 
	RETURN(@aErrNo) ;

GO

