CREATE PROCEDURE [dbo].[WspGetPetitionAudit]
	  @mYear		SMALLINT
	, @mMonth		SMALLINT
	, @mDay			SMALLINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @mStart	DATETIME
	DECLARE @mEnd		DATETIME

	SET	@mStart 	= CAST(STR(@mYear) + '-' + STR(@mMonth) + '-' + STR(@mDay) + ' 00:00:00' AS DATETIME)
	SET	@mEnd 	= DATEADD(MI, 1, CAST(STR(@mYear) + '-' + STR(@mMonth) + '-' + STR(@mDay) + ' 23:58:59' AS DATETIME))

	SELECT 
		T2.mCategory,
		T2.mContent,
		ISNULL(T1.mTot, 0) mTot,
		ISNULL(T1.mFinCnt, 0) mFinCnt,
		ISNULL(T1.mNotFinCnt, 0) mNotFinCnt,
		ISNULL(T1.mOverHourCnt, 0) mOverHourCnt
	FROM (
		SELECT 
			mCategory,
			COUNT(*) mTot,
			ISNULL( SUM(mFinCnt), 0 ) mFinCnt,
			ISNULL( SUM(mNotFinCnt), 0 ) mNotFinCnt,
			ISNULL( SUM(mOverHour), 0 ) mOverHourCnt
		FROM (
			SELECT 
				mCategory,
				( CASE WHEN mIsFin = 1 THEN 1 END) mFinCnt,
				( CASE WHEN mIsFin = 0 THEN 1 END) mNotFinCnt,
				( CASE WHEN DATEDIFF(DD, mRegDate, GETDATE() ) > 0 AND mIsFin = 0 THEN 1 ELSE 0 END) mOverHour
			FROM dbo.TblPetitionBoard 
			WHERE mRegDate BETWEEN @mStart AND @mEnd ) T1
		GROUP BY T1.mCategory) T1
			RIGHT OUTER JOIN FNLParm.dbo.TblPetitionCategory T2
				ON T1.mCategory = T2.mCategory

GO

