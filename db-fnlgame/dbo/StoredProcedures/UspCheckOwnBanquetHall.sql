/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspCheckOwnBanquetHall    Script Date: 2008-11-10 10:37:16 ******/





CREATE Procedure [dbo].[UspCheckOwnBanquetHall]	
	 @pTerritory		INT OUTPUT		-- ???? [??/??]
	,@pOwnerPcNo		INT				-- ?????
	,@pIsOwnBanquetHall	TINYINT OUTPUT	-- ????? ???? [??]
	,@pBanquetHallType	INT OUTPUT		-- ????? [??]
	,@pBanquetHallNo	INT OUTPUT		-- ????? [??]
	,@pLeftMin		INT OUTPUT			-- ??? [??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	
	DECLARE	@aErrNo		INT,
			@aRowCnt	INT
			
	SELECT	@aErrNo = 0,
			@aRowCnt = 0

	SET		@pIsOwnBanquetHall = 0
	SET		@pBanquetHallType = 0
	SET		@pBanquetHallNo = 0
	SET		@pLeftMin = 0

	IF (@pOwnerPcNo = 0)
	BEGIN
		RETURN(0)
	END
	
	IF (@pTerritory <= 0)	-- ?? ????? 1???	
	BEGIN
		-- ?? ?? ??
		SELECT 
			@pTerritory = mTerritory, 
			@pIsOwnBanquetHall = 1, 
			@pBanquetHallType = mBanquetHallType, 
			@pBanquetHallNo = mBanquetHallNo, 
			@pLeftMin = mLeftMin 
		FROM dbo.TblBanquetHall WITH (NOLOCK) 
		WHERE mOwnerPcNo = @pOwnerPcNo
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		
		IF @aErrNo<> 0
		BEGIN
			RETURN(1)		-- 1: DB ???? ??
		END

		RETURN(0)		
	END
	
	
	SELECT 
		@pTerritory = mTerritory, 
		@pIsOwnBanquetHall = 1, 
		@pBanquetHallType = mBanquetHallType, 
		@pBanquetHallNo = mBanquetHallNo, 
		@pLeftMin = mLeftMin 
	FROM dbo.TblBanquetHall WITH (NOLOCK) 
	WHERE mTerritory = @pTerritory 
		AND mOwnerPcNo = @pOwnerPcNo
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo<> 0
	BEGIN
		RETURN(1)		-- 1: DB ???? ??
	END

	RETURN(0)

GO

