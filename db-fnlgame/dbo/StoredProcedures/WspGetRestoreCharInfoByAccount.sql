/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2009-5-12 9:18:17 ******/  
  
/****** Object:  Stored Procedure dbo.WspGetRestoreCharInfoByAccount    Script Date: 2008-11-10 10:37:18 ******/  
  
  
  
CREATE PROCEDURE [dbo].[WspGetRestoreCharInfoByAccount]   
 @mUserId CHAR(20) -- ???  
AS  
 SET NOCOUNT ON  
   
 SELECT  
    
  p.mNm, -- ????  
  u.mUserId, -- ???  
  u.mUserNo,  
    
  r.mRegDate,  
  p.mNo,  
  r.mPcRestoredOperator  
 FROM  
  [dbo].[TblPc] AS p WITH (NOLOCK)    
 INNER JOIN  
  [FNLAccount].[dbo].[TblUser] AS u     
 On   
  p.mOwner = u.mUserNo  
 INNER JOIN  
  [dbo].[TblPcRestored] AS r WITH (NOLOCK)  
 ON  
  r.mPcNo = p.mNo  
 WHERE  
  u.mUserId = @mUserId

GO

