/******************************************************************************
**		Name: UspSetMacroList
**		Desc: ¸ÅÅ©·Î ¸®½ºÆ® ÀúÀå
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspSetMacroList]
	@pPcNo			INT
	,@pMacroID		TINYINT
	,@pMacroSlot	TINYINT
	,@pMacroName	VARCHAR(6)
	,@pMacroIconNo	TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF EXISTS (SELECT *
				FROM dbo.TblMacroList
				WHERE mPcNo = @pPcNo
				  AND mMacroID = @pMacroID)
	BEGIN
		UPDATE dbo.TblMacroList
		SET mMacroName = @pMacroName, mIconNo = @pMacroIconNo
		WHERE mPcNo = @pPcNo
		AND mMacroID = @pMacroID
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblMacroList (mPcNo, mMacroID, mMacroSlot, mMacroName, mIconNo)
		VALUES (@pPcNo, @pMacroID, @pMacroSlot, @pMacroName, @pMacroIconNo)
	END

GO

