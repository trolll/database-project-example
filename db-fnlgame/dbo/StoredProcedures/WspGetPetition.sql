CREATE PROCEDURE [dbo].[WspGetPetition]
	@mFlag		INT	-- ╟├╖í▒╫░¬ (0└╠╕Θ ╣╠├│╕« ╕±╖╧, 1 └╠╕Θ ╕≡╡τ╕±╖╧)
AS
	SET NOCOUNT ON

	IF @mFlag = 0
	BEGIN
		SELECT 
			TOP 1000
			  pe.mRegDate
			, pe.mText
			, pe.mIsFin
			, p.mNm
			, ca.mContent
			, pe.mPID
			, pe.mIpAddress
			, de.mPcNm
		FROM
			[dbo].[TblPetitionBoard]AS pe WITH (NOLOCK)
		INNER JOIN
			[dbo].[TblPc] AS p WITH (NOLOCK)
		ON
			pe.mPcNo = p.mNo
		FULL OUTER JOIN
			[dbo].[TblPcDeleted] AS de WITH (NOLOCK)
		ON
			p.mDelDate IS NOT NULL AND p.mNo = de.mPcNo 
		INNER JOIN
			FNLParm.[dbo].[TblPetitionCategory] AS ca --WITH (NOLOCK)
			--OPENQUERY(A55077, 'SELECT * FROM [FNLParm1].[dbo].[TblPetitionCategory] WITH (NOLOCK)') AS ca
		ON
			pe.mCategory = ca.mCategory
		WHERE
			pe.mIsFin = 0
		ORDER BY
 			pe.mRegDate
		DESC
	END
	
	ELSE
	BEGIN
		SELECT 
			TOP 1000
			  pe.mRegDate
			, pe.mText
			, pe.mIsFin
			, p.mNm
			, ca.mContent
			, pe.mPID
			, pe.mIpAddress
			, de.mPcNm

		FROM
			[dbo].[TblPetitionBoard]AS pe WITH (NOLOCK)
		INNER JOIN
			[dbo].[TblPc] AS p WITH (NOLOCK)
		ON
			pe.mPcNo = p.mNo
		FULL OUTER JOIN
			[dbo].[TblPcDeleted] AS de WITH (NOLOCK)
		ON
			p.mDelDate IS NOT NULL AND p.mNo = de.mPcNo 
		INNER JOIN
			FNLParm.[dbo].[TblPetitionCategory] AS ca --WITH (NOLOCK)
			--OPENQUERY(A55077, 'SELECT * FROM [FNLParm1].[dbo].[TblPetitionCategory] WITH (NOLOCK)') AS ca
		ON
			pe.mCategory = ca.mCategory
		ORDER BY
 			pe.mRegDate
		DESC
	END

	SET NOCOUNT OFF

GO

