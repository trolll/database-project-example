--------------------------------
-- ░┤├╝╕φ : WspGetGuildMark
-- ╝│╕φ : ▒µ╡σ ╕╢┼⌐(Binary) ╡Ñ└╠┼═╕ª ╛≥╛ε┐┬┤┘
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.07.11
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : --------------------------------

CREATE PROCEDURE [dbo].[WspGetGuildMark] 
	@mGuildNm	CHAR(12)		-- ▒µ╡σ └╠╕º

AS
	SET NOCOUNT ON

	SELECT
		mGuildMark
	FROM	
		TblGuild
	WITH ( NOLOCK ) 
	WHERE
		mGuildNm = @mGuildNm
		
	SET NOCOUNT OFF

GO

