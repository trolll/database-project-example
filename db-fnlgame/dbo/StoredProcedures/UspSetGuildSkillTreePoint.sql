/******************************************************************************  
**  File: 
**  Name: UspSetGuildSkillTreePoint  
**  Desc: Guild의 스킬트리 포인트 세팅
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.28 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspSetGuildSkillTreePoint]
	 @pGuildNo		INT,
	@pPoint	SMALLINT 
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	update TblGuild
	set mSkillTreePoint = @pPoint
	where mGuildNo = @pGuildNo

	if @@error <> 0
	begin
		return 1
	end

	return 0

GO

