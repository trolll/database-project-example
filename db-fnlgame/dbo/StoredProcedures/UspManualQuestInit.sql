/******************************************************************************
**		Name: UspManualQuestInit
**		Desc: 반복 퀘스트 초기화 (수동보상일경우만 사용)
**			  아침 6시에 퀘스트 초기화가 진행되는데
**			  그 시간에 점검중이라면 실행을 해줘야한다.
**
**		Auth: 정진호
**		Date: 2011.04.08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**  
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspManualQuestInit]
	@pSvrNo				SMALLINT
AS
BEGIN
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;
	
	DECLARE @aErr INT
	DECLARE @aCurrDate SMALLDATETIME
	DECLARE @aQuestNoStr VARCHAR(3000)
	DECLARE @aEventQuestNoStr VARCHAR(3000)			
			
	SET @aErr =0
	SET @aCurrDate = GETDATE()
	SET @aQuestNoStr = ''		
	SET @aEventQuestNoStr = ''
	
	-- 일일 퀘스트 초기화 정보를 얻어온다.
	-- 초기화 해야될 퀘스트 번호를 구한다.
	EXEC FNLParm.dbo.UspGetExpireQuestManual @aCurrDate, @aQuestNoStr OUTPUT
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 1 -- sql error (UspGetExpireQuestManual)
		GOTO TRN_END	
	END 	
	
	-- 완료된 이벤트 퀘스트 정보를 얻어온다.
	-- 삭제 해야될 퀘스트 번호를 구한다.
	EXEC FNLParm.dbo.UspGetExpireEventQuest @pSvrNo, @aEventQuestNoStr OUTPUT
	IF @@ERROR <> 0 
	BEGIN
		SET @aErr = 2 -- sql error (UspGetExpireEventQuest)
		GOTO TRN_END	
	END 	

	BEGIN TRAN
		-- 초기화
		EXEC @aErr = dbo.UspPopPcQuestConditionAll @aQuestNoStr	-- 퀘스트 삭제
		SET @aErr = @@ERROR
		IF @aErr <> 0 
		BEGIN
			SET @aErr = 3	-- sql error (UspPopPcQuestConditionAll)
			ROLLBACK TRAN
			GOTO TRN_END
		END	

		--종료된 이벤트 퀘스트는 PcQuest에서 삭제한다.
		EXEC @aErr = dbo.UspDeletePcQuestExpireEvent @aEventQuestNoStr -- 퀘스트 삭제
		SET @aErr = @@ERROR				
		IF @aErr <> 0
		BEGIN
			SET @aErr = 4	-- sql error (UspDeletePcQuestExpireEvent)
			ROLLBACK TRAN
			GOTO TRN_END
		END				
	COMMIT TRAN

TRN_END:
	SELECT @aErr mError
END

GO

