/******************************************************************************  
**  Name: UspDeletePcEx 
**  Desc: Ä³¸¯ÅÍ »èÁ¦
**  
**                
**  Return values:  
**   0 : ÀÛ¾÷ Ã³¸® ¼º°ø  
**   > 0 : SQL Error  
**   
**                
**  Author: 
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**  2010.01.21  dmbkh      À§Å¹ÆÇ¸Å Ã¼Å© ±â´É Ãß°¡
**  2014.04.03  Á¤ÁøÈ£	   ´Þ·Â °ü·Ã Á¤º¸ »èÁ¦ Ãß°¡
**  2014.10.01  ÀÌ¿ëÁÖ	   ¸ÅÅ©·Î °ü·Ã Á¤º¸ »èÁ¦ Ãß°¡
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDeletePcEx]
	@pOwner			INT  
	,@pPcNo			INT  
	,@pNm			CHAR(12) OUTPUT  
	,@pGuildNo		INT OUTPUT -- °¡ÀÔÇÑ ±æµå¹øÈ£. ¾øÀ¸¸é TGuildNoDef(0)  
	,@pGuildGrade	TINYINT OUTPUT -- EGuildGrade. °¡ÀÔÇÑ ±æµå°¡ ÀÖÀ» ¶§¸¸ ÀÇ¹Ì ÀÖÀ½.  
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
   
	DECLARE @aErrNo		INT  
			, @aDelFlag	INT  
			, @aRowCnt	INT  
  
	SELECT @aErrNo = 0, @aRowCnt = 0, @aDelFlag = 1
  
	---------------------------------------------------------------   
	-- Ä³¸¯ÅÍ Ã¼Å©   
	---------------------------------------------------------------      
	SELECT   
		@pNm=[mNm]   
	FROM dbo.TblPc  
	WHERE ([mOwner]=@pOwner)   
	AND ([mNo]=@pPcNo)   
	AND ([mDelDate] IS NULL)

	IF(1 <> @@ROWCOUNT)  
	BEGIN   
		RETURN(1)  
	END  
    
	---------------------------------------------------------------   
	-- Ä³¸¯ÅÍ »èÁ¦ Ã¼Å©   
	-- »èÁ¦½Ã°£ 24½Ã°£ ÀÌ³», »èÁ¦ È½¼ö Á¦ÇÑ 5È¸  
	---------------------------------------------------------------      
	SELECT   
		@aDelFlag =  
					CASE   
						WHEN DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE()) <= 24 AND mDelCntOfPc > 4
							THEN 5 -- max ¼öÄ¡  
						WHEN DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE()) <= 24   
							THEN mDelCntOfPc   
						WHEN DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE()) > 24    
							THEN 1 -- Delete Clear  
					END    
	FROM dbo.TblPcOfAccount  
	WHERE mOwner = @pOwner  
   
	IF ( @aDelFlag >= 5 )  
	BEGIN  
		RETURN(7) -- »èÁ¦ È½¼ö ÃÊ°ú  
	END   
    
	---------------------------------------------------------------   
	-- ±æµå °¡ÀÔ Á¤º¸ Ã¼Å©   
	---------------------------------------------------------------      
	SELECT   
		@pGuildNo=[mGuildNo],   
		@pGuildGrade=[mGuildGrade]   
	FROM dbo.TblGuildMember  
	WHERE [mPcNo] = @pPcNo

	IF(@@ROWCOUNT <> 0)  
	BEGIN  
		RETURN(2)
	END    
   
	---------------------------------------------------------------   
	-- »çÁ¦¸ðÀÓ °¡ÀÔ Á¤º¸ Ã¼Å© (2007-07-05:b4nfter)   
	---------------------------------------------------------------      
	-- 1:»çºÎ,2:Á¦ÀÚ  
	IF EXISTS( SELECT mMaster   
				FROM dbo.TblDiscipleMember  
				WHERE mDisciple = @pPcNo  
				AND mType BETWEEN 1 AND 2 )  
	BEGIN  
		RETURN(6)  
	END   
    
	---------------------------------------------------------------   
	-- À§Å¹ÆÇ¸Å µî·Ï Ã¼Å© (2011-01-21:dmbkh)
	---------------------------------------------------------------          
	IF EXISTS(SELECT mCnsmNo
				FROM dbo.TblConsignment
				WHERE mPcNo = @pPcNo)
	BEGIN
		RETURN(8)
	END

	---------------------------------------------------------------   
	-- À§Å¹ÆÇ¸Å Á¤»ê±Ý Ã¼Å© (2011-01-21:dmbkh)
	---------------------------------------------------------------          	
	DECLARE @aCnsmBalance BIGINT

	SELECT @aCnsmBalance = mBalance
	FROM dbo.TblConsignmentAccount
	WHERE mPcNo=@pPcNo
	
	IF (@aCnsmBalance IS NOT NULL AND @aCnsmBalance > 0)
	BEGIN
		RETURN(8)
	END

	BEGIN TRAN
		--------------------------------------------------------------  
		-- »çÁ¦ ¸ðÀÓ ´ë»ó »èÁ¦(´ë±â, ¾øÀ½)  
		--------------------------------------------------------------  
		DELETE dbo.TblDiscipleMember  
		WHERE mDisciple = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 3  
			GOTO LABEL_END  
		END
   
		---------------------------------------------------------------   
		-- Ä£±¸ ¸ñ·Ï »èÁ¦  
		---------------------------------------------------------------     
		DELETE dbo.TblPcFriend  
		WHERE mOwnerPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 3  
			GOTO LABEL_END  
		END   
     
		---------------------------------------------------------------   
		-- Ä³¸¯ÅÍ »èÁ¦ Á¤º¸ µî·Ï  
		---------------------------------------------------------------     
		INSERT dbo.TblPcDeleted([mPcNo], [mPcNm])
		SELECT @pPcNo, @pNm

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 4  
			GOTO LABEL_END  
		END  
       
		---------------------------------------------------------------   
		-- ÇÑ¹ø »èÁ¦µÈ ÀÌ¸§À» Àç»ç¿ëÇÒ ¼ö ÀÖ´Ù.(2006-4-13:milkji)  
		---------------------------------------------------------------  
		UPDATE dbo.TblPc   
		SET   
			[mDelDate]=GETDATE(),   
			[mNm]=','+CONVERT(CHAR(11),[mNo])  
		WHERE ([mNo]=@pPcNo)   
		AND ([mDelDate] IS NULL)
		 
		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 5  
			GOTO LABEL_END  
		END  
  
		---------------------------------------------------------------   
		-- »èÁ¦ È½¼ö Á¦ÇÑ ±â´É Ãß°¡  
		---------------------------------------------------------------     
		UPDATE dbo.TblPcOfAccount  
		SET  
			mDelDateOfPcDeleted =CASE WHEN DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE() ) <= 24 THEN mDelDateOfPcDeleted  
								ELSE GETDATE() END   
			, mDelCntOfPc = CASE WHEN  DATEDIFF(HH, mDelDateOfPcDeleted, GETDATE() ) <= 24 THEN mDelCntOfPc+1  
							ELSE 1 END          
		WHERE mOwner = @pOwner  
    
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		  
		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 5  
			GOTO LABEL_END  
		END    
  
		IF ( @aRowCnt = 0 )  
		BEGIN -- »ý¼º Á¤º¸°¡ ¾ø´Ù¸é..    
			INSERT INTO dbo.TblPcOfAccount(mOwner) VALUES(@pOwner)  
      
			IF( @@ERROR <> 0)  
			BEGIN  
				SET  @aErrNo = 6  
				GOTO LABEL_END  
			END
		END   

		---------------------------------------------------------------   
		-- ´Þ·Â °ü·Ã Á¤º¸ »èÁ¦ (2014-4-3 : ppoi1019) 
		---------------------------------------------------------------     
		--½ÂÀÎ»èÁ¦
		DELETE dbo.TblCalendarAgreement  
		WHERE mOwnerPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END   

		--±×·ì»èÁ¦
		DELETE dbo.TblCalendarGroup  
		WHERE mOwnerPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END 

		--±×·ì¸â¹ö»èÁ¦
		DELETE dbo.TblCalendarGroupMember  
		WHERE mOwnerPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END  

		--µî·ÏÇÑ ÀÏÁ¤»èÁ¦
		DELETE dbo.TblCalendarSchedule  
		WHERE mOwnerPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END  

		--µî·ÏÇÑ ÀÏÁ¤±×·ì»èÁ¦
		DELETE dbo.TblCalendarScheduleGroup  
		WHERE mOwnerPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END   

		--µî·ÏÇÑ À¯ÀúÀÏÁ¤»èÁ¦
		DELETE dbo.TblCalendarPcSchedule  
		WHERE mOwnerPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END  

		--µî·ÏµÈ À¯ÀúÀÏÁ¤»èÁ¦
		DELETE dbo.TblCalendarPcSchedule  
		WHERE mPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END  

		--´Ù¸¥À¯Àú ½ÂÀÎ¸®½ºÆ®¿¡ µî·ÏµÇ¾îÀÖ´Â Á¤º¸»èÁ¦
		DELETE dbo.TblCalendarAgreement  
		WHERE mMemberPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END   

		--´Ù¸¥À¯Àú ±×·ì¿¡ µî·ÏµÇ¾îÀÖ´Â Á¤º¸»èÁ¦
		DELETE dbo.TblCalendarGroupMember  
		WHERE mMemberPcNo = @pPcNo

		IF( @@ERROR <> 0)  
		BEGIN  
			SET  @aErrNo = 1  
			GOTO LABEL_END  
		END 

		---------------------------------------------------------------   
		-- ¸ÅÅ©·Î °ü·Ã Á¤º¸ »èÁ¦ (2014-10-01 : choco)
		---------------------------------------------------------------    
		-- ¸ÅÅ©·Î Ä¿¸Çµå »èÁ¦
		DELETE dbo.TblMacroCommand
		WHERE mPcNo = @pPcNo

		IF( @@ERROR <> 0)
		BEGIN
			SET  @aErrNo = 1
			GOTO LABEL_END
		END
					
		-- ¸ÅÅ©·Î ¸®½ºÆ® »èÁ¦
		DELETE dbo.TblMacroList
		WHERE mPcNo = @pPcNo

		IF( @@ERROR <> 0)
		BEGIN
			SET  @aErrNo = 1
			GOTO LABEL_END
		END
  					
		-- ¸ÅÅ©·Î ¹öÀü »èÁ¦
		DELETE dbo.TblMacroContentVersion
		WHERE mPcNo = @pPcNo

		IF( @@ERROR <> 0)
		BEGIN
			SET  @aErrNo = 1
			GOTO LABEL_END
		END
  
	LABEL_END:    
	IF(@aErrNo = 0)  
		COMMIT TRAN  
	ELSE  
	ROLLBACK TRAN

	RETURN(@aErrNo)

GO

