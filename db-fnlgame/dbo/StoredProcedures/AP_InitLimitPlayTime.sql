/******************************************************************************
**		Name: AP_InitLimitPlayTime
**		Desc: Æ¯È­¼­¹ö°ü·Ã ÀÏ¹Ý Á¦ÇÑ ½Ã°£, PC¹æ Á¦ÇÑ ½Ã°£ ÃÊ±âÈ­
			  ÀÏ¹Ý ÇÃ·¹ÀÌ Á¦ÇÑ½Ã°£ 45½Ã, PC¹æ ÇÃ·¹ÀÌ Á¦ÇÑ½Ã°£ 15½Ã(ÃßÈÄ¿¡ º¯°æµÉ¼ö ÀÖÀ½)
**
**		Auth: ÀÌ Áø ¼±
**		Date: 2015.03.05
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/ 
CREATE PROCEDURE [dbo].[AP_InitLimitPlayTime]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON
	
	UPDATE dbo.TblLimitPlayTime 
	SET mNormalLimitTime = (45 * 60 * 60), mPcBangLimitTime = (15 * 60 * 60)
END

GO

