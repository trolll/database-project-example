/******************************************************************************  
**  Name: UspGetStatisticsItemByMonster  
**  Desc: 몬스터, NPC 통계 테이블 로드
**  
**  Auth:   
**  Date: 
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:        Author:    Description:  
**  --------     --------   ---------------------------------------  
**  2010.12.07.  김강호      mItemStatus,  mModType 추가
*******************************************************************************/  
CREATE Procedure [dbo].[UspGetStatisticsItemByMonster]  
AS  
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   

	SELECT   
		MID  
		,mItemNo  
		,mCreate  
		,mDelete
		,mItemStatus
		,mModType
	FROM dbo.TblStatisticsItemByMonster

GO

