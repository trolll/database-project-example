CREATE PROCEDURE [dbo].[WspUptItemRecovery]  	
	@pMI	INT			-- ї¬Ае ЅГ°Ј(єР)
	, @pSvrNo	SMALLINT
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
		
	EXEC @aErrNo = dbo.UspUptItemDate @pMI
	
	IF @aErrNo <> 0
		RETURN(@aErrNo)
		
	-- ј­№ц є№±ё ЅГ°ЈА» ±в·П ЗСґЩ.	
	EXEC FNLParm.dbo.UspSetParmSvrLastSupportDate @pSvrNo

    RETURN(0)

GO

