/******************************************************************************
**		Name: UspGetAchieveGameGuild
**		Desc: 诀利辨靛 扁夯 蔼.
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetAchieveGameGuild]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SELECT   
		 mAchieveGuildID
		,mGuildRank
		,mGuildName
		,mMemberName
		,mEquipPoint
		,mUpdateIndex
	FROM dbo.TblAchieveGuildList

GO

