/******************************************************************************  
**  Name: WspCnsmUnregister  
**  Desc: 위탁 취소 or 기간 만료된 것 회수
**  
**                
**  Return values:  
**   0 : 작업 처리 성공  
**   > 0 : SQL Error  
**                
**  Author: 김강호  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[WspCnsmUnregister]
 @pCnsmSn  BIGINT	-- 위탁 시리얼 번호
 ,@pItemNo INT		-- 아이템 번호(위탁 데이터와 일치 여부 확인)
 ,@pIsStack BIT		-- 스택형인지 여부
 ,@pPcNo	INT
 ,@pCatetory TINYINT OUTPUT
 ,@pItemSerial BIGINT OUTPUT	-- 회수된 아이템Sn, 기존것에 병합되었다면 대상Sn
 ,@pResultCnt  INT OUTPUT		-- 회수된 개수 or 병합 결과 최종개수
 ,@pCurCnt INT OUTPUT -- 회수된 개수
 ,@pPrice INT OUTPUT -- 개당 판매 가격
AS  
	SET NOCOUNT ON   ;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aErrNo		INT;
	DECLARE @aRowCnt	INT;

	DECLARE @aPcNo					INT;
	DECLARE @aCurCnt				INT	;

	DECLARE @aRegDate			DATETIME;
	DECLARE @aSerialNo			BIGINT	;
	DECLARE @aItemNo			INT		;	
	DECLARE @aEndDate			DATETIME;
	DECLARE @aIsConfirm			BIT	;
	DECLARE @aStatus			TINYINT;
	DECLARE @aCntUse			TINYINT;
	DECLARE @aOwner				INT;
	DECLARE @aPracticalPeriod	INT;
	DECLARE @aBindingType		TINYINT;
	DECLARE @aRestoreCnt		TINYINT;

	SELECT @aRowCnt = 0, @aErrNo= 0  ;
	SELECT   
		@aPcNo		= [mPcNo],
		@aCurCnt	= [mCurCnt],
		@pCatetory	= [mCategoryNo],
		@aItemNo	= mItemNo,
		@pPrice = mPrice
	FROM dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmSn;   

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 1;  -- 위탁 아이템이 존재하지 않습니다.(TblConsignment)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100;  -- DB 내부 오류
		END
		GOTO T_END ;  
	END  

	SET @pCurCnt = @aCurCnt;

	-- PC 번호 확인(본인이 등록한 것인지 확인)
	IF @aPcNo <> @pPcNo
	BEGIN  
		SET @aErrNo = 2 ;-- 본인이 등록한 아이템이 아닙니다.
		GOTO T_END   ;
	END

	-- 아이템 번호가 일치하는지 확인
	IF @aItemNo <> @pItemNo
	BEGIN
		SET @aErrNo = 3; -- 아이템ID가 일치하지 않습니다.
		GOTO T_END;
	END

	SELECT
		@aRegDate			= mRegDate,
		@aSerialNo			= mSerialNo,		
		@aEndDate			= mEndDate,
		@aIsConfirm			= mIsConfirm,
		@aStatus			= mStatus,
		@aCntUse			= mCntUse,
		@aOwner				= mOwner,
		@aPracticalPeriod	= mPracticalPeriod,
		@aBindingType		= mBindingType,
		@aRestoreCnt		= mRestoreCnt
	FROM dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmSn;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		IF @aErrNo = 0 AND @aRowCnt = 0
		BEGIN
			SET @aErrNo = 4 ; -- 위탁 아이템이 존재하지 않습니다.(TblConsignmentItem)
		END
		ELSE
		BEGIN
			SET @aErrNo = 100 ; -- DB 내부 오류
		END
		GOTO T_END ;  
	END  


	-- 스택형이 아닌 경우
	IF @pIsStack = 0
	BEGIN
		SET @pItemSerial = @aSerialNo;
		SET @pResultCnt = @aCurCnt;

		BEGIN TRAN;

		-- 아이템 삭제
		DELETE dbo.TblConsignmentItem
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 5; -- 아이템 삭제 실패(TblConsignmentItem)
			GOTO T_END   ;
		END

		DELETE dbo.TblConsignment
		WHERE  mCnsmNo  =@pCnsmSn;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 6 ;-- 아이템 삭제 실패(TblConsignment)
			GOTO T_END   ;
		END

		-- 인벤토리에 아이템 추가
		INSERT dbo.TblPcInventory
		(
			[mSerialNo],  
			[mPcNo],   
			[mItemNo],   
			[mEndDate],   
			[mIsConfirm],   
			[mStatus],   
			[mCnt],   
			[mCntUse],  
			[mOwner],  
			[mPracticalPeriod], 
			mBindingType,
			mIsSeizure 
		)  
		VALUES
		(  
			@pItemSerial,  
			@pPcNo,   
			@aItemNo,   
			@aEndDate,   
			@aIsConfirm,   
			@aStatus,   
			@aCurCnt,   
			@aCntUse,  
			@aOwner,  
			@aPracticalPeriod, 
			@aBindingType,
			1
		) ;

		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 7; -- 인벤토리 아이템 추가 실패
			GOTO T_END  ; 
		END
		COMMIT TRAN
		GOTO T_END
	END


	-- 스택형일 경우

	DECLARE @aCntTarget  INT  ;
	DECLARE @aIsSeizure  BIT ; 
	SET  @aCntTarget = 0  ;   

	SELECT   
		@aCntTarget=ISNULL([mCnt],0),   
		@pItemSerial=[mSerialNo],   
		@aIsSeizure=ISNULL([mIsSeizure],0)  
	FROM dbo.TblPcInventory
	WHERE mPcNo		=  @pPcNo
	AND mItemNo		= @aItemNo   
	AND mIsConfirm	= @aIsConfirm   
	AND mStatus		= @aStatus   
	AND mOwner		= @aOwner    
	AND mPracticalPeriod	= @aPracticalPeriod  
	AND mBindingType		= @aBindingType
	AND mIsSeizure = 1
	AND (mCnt + @aCurCnt) <= 1000000000  ;
	  
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0  
	BEGIN  
		SET @aErrNo = 100 ; -- DB 내부 오류  
		GOTO T_END  ;    
	END

	-- 스택시키려는 대상이 압류되어 있으면 판매 취소를 못한다.
	-- 그냥 시리얼 새로 만들어서 판매 취소가 가능하도록 할 수도 있긴 한데...
	IF @aCntTarget<> 0 AND @aIsSeizure <> 0  
	BEGIN  
		SET @aErrNo = 8; -- 아이템이 압류되어 있습니다.
		GOTO T_END   ;   
	END


	BEGIN TRAN;
	-- @aCurCnt에 얻어온 개수만큼을 빼서 업데이트한 결과가 0이면 DELETE를 한다.
	-- DELETE에 실패할 경우 에러
	DECLARE @aResultCnt INT;
	SET @aResultCnt = 0;

		UPDATE TblConsignment
		SET @aResultCnt = mCurCnt = mCurCnt - @aCurCnt
		WHERE  mCnsmNo  =@pCnsmSn;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN;
			SET @aErrNo = 9 ; -- 삭제할 수 없습니다(TblConsignment)
			GOTO T_END   ;
		END
		IF @aResultCnt <> 0
		BEGIN
			ROLLBACK TRAN;
			SET @aErrNo = 10 ; -- 삭제할 수 없습니다(TblConsignment)
			GOTO T_END   ;
		END

	-- 아이템 삭제
	DELETE dbo.TblConsignmentItem
	WHERE  mCnsmNo  =@pCnsmSn;
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT;  
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 11 ; -- 삭제할 수 없습니다(TblConsignmentItem)
		GOTO T_END ;  
	END

	DELETE dbo.TblConsignment
	WHERE  mCnsmNo  =@pCnsmSn;
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN;
		SET @aErrNo = 12;  -- 삭제할 수 없습니다(TblConsignment)
		GOTO T_END   ;
	END

	IF @aCntTarget = 0 -- 누적할 아이템이 없을 경우
	BEGIN
		SET @pResultCnt = @aCurCnt;
		EXEC @pItemSerial =  dbo.UspGetItemSerial  ;-- 신규 시리얼 얻기   
		IF @pItemSerial <= 0  
		BEGIN  
			ROLLBACK TRAN  ;
			SET @aErrNo = 13;  -- 아이템 시리얼 생성 실패
			GOTO T_END    ;
		END	

		-- 인벤토리에 아이템 추가
		INSERT dbo.TblPcInventory
		(
			[mSerialNo],  
			[mPcNo],   
			[mItemNo],   
			[mEndDate],   
			[mIsConfirm],   
			[mStatus],   
			[mCnt],   
			[mCntUse],  
			[mOwner],  
			[mPracticalPeriod], 
			mBindingType,
			mIsSeizure 
		)  
		VALUES(  
			@pItemSerial,  
			@pPcNo,   
			@aItemNo,   
			@aEndDate,   
			@aIsConfirm,   
			@aStatus,   
			@aCurCnt,   
			@aCntUse,  
			@aOwner,  
			@aPracticalPeriod, 
			@aBindingType,
			1
		)  ;
		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF  @aErrNo <> 0 OR  @aRowCnt = 0  
		BEGIN
			ROLLBACK TRAN  ;
			SET @aErrNo = 14 ; -- 인벤토리에 아이템 추가 실패
			GOTO T_END;
		END
	END
	ELSE -- 누적할 아이템이 있을 경우
	BEGIN
		UPDATE dbo.TblPcInventory
		SET @pResultCnt = mCnt = mCnt + @aCurCnt
		WHERE mSerialNo = @pItemSerial;
		IF  @aErrNo <> 0 OR  @aRowCnt = 0  
		BEGIN
			ROLLBACK TRAN  ;
			SET @aErrNo = 15;  -- 인벤토리에 아이템 추가 실패
			GOTO T_END;
		END
		IF 1000000000 < @pResultCnt
		BEGIN
			ROLLBACK TRAN ; 
			SET @aErrNo = 16;  -- 인벤토리에 아이템 추가 실패
			GOTO T_END;
		END
	END

T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN ; 

T_END:     
	SET NOCOUNT OFF  ; 
	RETURN(@aErrNo) ;

GO

