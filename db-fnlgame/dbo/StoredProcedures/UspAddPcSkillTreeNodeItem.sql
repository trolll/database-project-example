/******************************************************************************  
**  File: 
**  Name: UspAddPcSkillTreeNodeItem  
**  Desc: PC의 스킬트리노드아이템 하나 추가
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspAddPcSkillTreeNodeItem]
	@pPcNo		INT,
	@pSTNIID	INT,
	@pValidSec	INT,	 -- 864000000초(10000일)이면 무한대, 2020-01-01 00:00:00 에 만료되는 것으로 약속한다
	@pLeftMin	INT OUTPUT
AS 
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF @pPcNo = 1 OR @pPcNo = 0
	BEGIN 
		RETURN(0)	-- NPC, 버려진 아이템
	END

	DECLARE	 @aEndDay		SMALLDATETIME
			, @aErr INT
			, @aRowCnt INT

	SELECT 	@aErr = 0, 	@aRowCnt = 0;	
			
	IF 864000000 = @pValidSec
		BEGIN
			SELECT	 @aEndDay = dbo.UfnGetEndDate(GETDATE(), 864000000/(3600 * 24))
		END
	ELSE
		BEGIN
			SELECT	 @aEndDay = dateadd(second, @pValidSec, GETDATE())
		END
		
	UPDATE dbo.TblPcSkillTreeInventory
	SET mEndDate=@aEndDay
	WHERE mPcNo=@pPcNo 
		AND mSTNIID = @pSTNIID		

	SELECT @aErr = @@ERROR	, @aRowCnt = @@ROWCOUNT, @pLeftMin = DATEDIFF(mi, GETDATE(), @aEndDay);
	
	IF @aErr <> 0 
	begin
		RETURN(1) -- DB ERROR
	end	

	IF @aRowCnt > 0
		RETURN(0)
	
	insert into dbo.TblPcSkillTreeInventory
	values(default, @pPcNo, @pSTNIID, @aEndDay)
	if @@error <> 0
	begin
		return 2 -- DB 에러
	end

	return 0

GO

