/******************************************************************************
**		Name: UspGetPcAchieveEquip
**		Desc: 馒侩吝牢 傈府前 府胶飘
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcAchieveEquip]
	 @pPcNo  INT 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	SET ROWCOUNT 10; -- 弥措 啊廉棵 荐 乐绰 酒捞袍 荐甫 力茄   

	SELECT T1.mSerialNo, T1.mItemNo 
	FROM dbo.TblPcAchieveEquip AS T1 
		INNER JOIN dbo.TblPcAchieveInventory AS T2
		ON T1.mSerialNo = T2.mSerialNo
	WHERE T1.mPcNo = @pPcNo

GO

