/******************************************************************************
**		Name: WspUpdateItemRestoreCnt
**		Desc: ѕЖАМЕЫ АЇИї±в°Ј Иёє№ИЅјцё¦ јцБ¤ЗСґЩ
**
**		Auth: ±иј®Гµ
**		Date: 2009-04-13
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**                јцБ¤АП           јцБ¤АЪ                             јцБ¤і»їл    
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspUpdateItemRestoreCnt]
	@mSerialNo	BIGINT, -- ЅГё®ѕу
	@mRestoreCnt TINYINT,	-- ѕР·щї©єО
	@mPos		CHAR(1)		-- ѕЖАМЕЫ А§ДЎ(i:АОєҐ, s:Гў°н, g:±жµеГў°н)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @mPos = 'i' 
	BEGIN
		UPDATE dbo.TblPcInventory
		SET
			mRestoreCnt = @mRestoreCnt
		WHERE mSerialNo = @mSerialNo
	END
	
	IF @mPos = 's' 
	BEGIN
		UPDATE dbo.TblPcStore
		SET
			mRestoreCnt = @mRestoreCnt
		WHERE mSerialNo = @mSerialNo	
	END 
	
	IF @mPos = 'g' 
	BEGIN
		UPDATE dbo.TblGuildStore
		SET
			mRestoreCnt = @mRestoreCnt
		WHERE mSerialNo = @mSerialNo	
	END 	

	IF @@ERROR <> 0
		RETURN 1
	
	RETURN 0

GO

