/******************************************************************************
**		File: UspGetPcEquip.sql
**		Name: UspGetPcEquip
**		Desc: 旌愲Ν韯?鞛レ癌 鞎勳澊韰滌潉 鞏混柎霃堧嫟.
**
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2006.11.29	JUDY				WHERE 臁瓣贝順?攵€攵?靾橃爼
**		2007.05.07	JUDY				鞎勳澊韰?攵勲Μ 鞝侅毄
**		2009.12.07	旯€甏戩劖				攴€靻?鞝曤炒毳?於旉皜搿?鞏浑姅雼?
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcEquip]
	 @pPcNo		INT
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;			
	
	SELECT 
		a.mSlot, 
		a.mSerialNo,
		b.mItemNo,
		b.mBindingType
	FROM dbo.TblPcEquip AS a 
		INNER JOIN dbo.TblPcInventory AS b 
		ON a.mSerialNo = b.mSerialNo
	WHERE a.mOwner = @pPcNo
			AND b.mEndDate >= GETDATE() 
			AND b.mIsSeizure = 0

GO

