CREATE PROCEDURE [dbo].[WspGetInfoGuildItem] 
	@mSerial	INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		T1.mGuildNo,
		T2.mGuildNm,
		mItemNo,
		mIsConfirm,
		mStatus,
		mCnt,
		mCntUse,
		mIsSeizure,
		mEndDate,
		mGrade,
		mPracticalPeriod,
		mBindingType
	FROM dbo.TblGuildStore T1
		INNER JOIN dbo.TblGuild T2
			ON T1.mGuildNo = T2.mGuildNo
	WHERE mSerialNo = @mSerial

GO

