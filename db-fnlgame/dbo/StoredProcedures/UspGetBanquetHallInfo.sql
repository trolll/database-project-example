/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetBanquetHallInfo    Script Date: 2008-11-10 10:37:16 ******/





CREATE Procedure [dbo].[UspGetBanquetHallInfo]		-- ?? ???? ?? ??? ???? ???? ??? ?? ?? ??
	 @pOwnerPcNo		INT			-- ?????
	,@pTerritory		INT OUTPUT		-- ???? [??/??]
	,@pBanquetHallType	INT OUTPUT		-- ????? (0:?.??? / 1:?.???) [??]
	,@pBanquetHallNo	INT OUTPUT		-- ????? [??]
	,@pLeftMin		INT OUTPUT		-- ??? [??]
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	IF (@pTerritory <= 0)	-- ?? ????? 1???
	BEGIN
		-- ?? ?? ??
		IF EXISTS(SELECT mOwnerPcNo FROM TblBanquetHall WITH (NOLOCK) WHERE mOwnerPcNo = @pOwnerPcNo)
		BEGIN
			SELECT @pTerritory = mTerritory, @pBanquetHallType = mBanquetHallType, @pBanquetHallNo = mBanquetHallNo, @pLeftMin = mLeftMin FROM TblBanquetHall WITH (NOLOCK) WHERE mOwnerPcNo = @pOwnerPcNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1: DB ???? ??
			END	 
		END
		ELSE
		BEGIN
			SET	@aErrNo = 2		-- 2 : ???? ??? ???? ??
		END
	END
	ELSE
	BEGIN
		-- ?? ?? ??
		IF EXISTS(SELECT mOwnerPcNo FROM TblBanquetHall WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerPcNo = @pOwnerPcNo)
		BEGIN
			SELECT @pTerritory = mTerritory, @pBanquetHallType = mBanquetHallType, @pBanquetHallNo = mBanquetHallNo, @pLeftMin = mLeftMin FROM TblBanquetHall WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerPcNo = @pOwnerPcNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB ???? ??
			END	 
		END
		ELSE
		BEGIN
			SET	@aErrNo = 2		-- 2 : ???? ??? ???? ??
		END
	END
	
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

