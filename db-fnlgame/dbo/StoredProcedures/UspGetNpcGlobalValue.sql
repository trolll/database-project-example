/****** Object:  Stored Procedure dbo.UspGetNpcGlobalValue    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetNpcGlobalValue    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspGetNpcGlobalValue    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetNpcGlobalValue    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetNpcGlobalValue    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspGetNpcGlobalValue    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspGetNpcGlobalValue    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetNpcGlobalValue    Script Date: 2009-7-14 13:13:28 ******/
CREATE PROCEDURE [dbo].[UspGetNpcGlobalValue]
 AS

SET NOCOUNT ON

	SELECT mNid, mName, mValue
	FROM TblNpcGlobalValue

GO

