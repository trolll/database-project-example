/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspDelGuildAgitAuction    Script Date: 2008-11-10 10:37:16 ******/





CREATE Procedure [dbo].[UspDelGuildAgitAuction]
	 @pTerritory		INT			-- ????
	,@pGuildAgitNo		INT OUTPUT		-- ??????? [??]
	,@pOwnerGID		INT 			-- ????
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET XACT_ABORT ON
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	SET		@pGuildAgitNo = 0

	IF EXISTS(SELECT mOwnerGID FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID)
	BEGIN
		-- ????? ???

		DECLARE @aIsSelling		CHAR
		SELECT @pGuildAgitNo = mGuildAgitNo, @aIsSelling = mIsSelling FROM TblGuildAgit WITH(NOLOCK) WHERE mTerritory = @pTerritory AND mOwnerGID = @pOwnerGID
		IF (@aIsSelling = 0)
		BEGIN
			-- ???? ?? (????)
			GOTO LABEL_END_LAST			 
		END

		BEGIN TRAN

		-- ???? ??
		UPDATE TblGuildAgit SET mIsSelling = 0 WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo AND mOwnerGID = @pOwnerGID
		IF(0 <> @@ERROR)
		BEGIN
			SET @aErrNo = 1	-- 1 : DB ???? ??
			GOTO LABEL_END			 
		END	 

		-- ?? ????? ??? ?? ??
		IF EXISTS(SELECT mCurBidGID FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo AND mCurBidGID <> 0 AND mCurBidMoney > 0)
		BEGIN
			DECLARE @aCurBidGID INT
			DECLARE @aCurBidMoney BIGINT
			SELECT @aCurBidGID = mCurBidGID, @aCurBidMoney = mCurBidMoney FROM TblGuildAgitAuction WITH (NOLOCK) WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo

			-- ????? ???? ??
			UPDATE TblGuildAccount SET mGuildMoney = mGuildMoney + @aCurBidMoney WHERE mGID = @aCurBidGID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB ???? ??
				GOTO LABEL_END			 
			END	 

			-- ????? ???? ???			
			UPDATE TblGuildAgitAuction SET mCurBidGID = 0, mCurBidMoney = 0 WHERE mTerritory = @pTerritory AND mGuildAgitNo = @pGuildAgitNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1	-- 1 : DB ???? ??
				GOTO LABEL_END			 
			END	 
		END
	END
	ELSE
	BEGIN
		-- ????? ???? ??

		SET @aErrNo = 2			-- 2 : ?? ?????? ???? ??
		GOTO LABEL_END_LAST			 
	END
	

LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

