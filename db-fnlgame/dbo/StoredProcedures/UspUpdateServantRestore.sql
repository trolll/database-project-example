/******************************************************************************
**		Name: UspUpdateServantRestore
**		Desc: ¼­¹øÆ® º¹¿ø °¡´É¿©ºÎ °»½Å
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-09-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateServantRestore]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pIsRestore		BIT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE dbo.TblPcServant 
	SET mIsRestore = @pIsRestore
	WHERE mPcNo = @pPcNo
	  AND mSerialNo = @pSerialNo

	SET NOCOUNT OFF;

GO

