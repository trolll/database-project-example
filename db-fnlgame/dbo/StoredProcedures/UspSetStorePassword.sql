/******************************************************************************
**		Name: UspSetStorePassword
**		Desc: 俺牢芒绊 菩胶况靛 汲沥
**
**		Auth: 傍籍痹
**		Date: 2016-11-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
-- 俺牢芒绊 菩胶况靛 汲沥 橇肺矫廉 积己
CREATE PROCEDURE [dbo].[UspSetStorePassword]
	  @pIsSet		BIT
	, @pUserNo		INT
	, @pPassword	CHAR(8)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DECLARE  @aErr			INT

	SELECT @aErr = 0

	IF NOT EXISTS (	SELECT * FROM dbo.TblPc WHERE mOwner = @pUserNo)
		BEGIN
			RETURN(1)	-- 蜡历啊 绝澜
		END 
	
	IF (1 = @pIsSet) -- 菩胶况靛 汲沥
		BEGIN
			IF NOT EXISTS (	SELECT * FROM dbo.TblStorePassword WHERE mUserNo = @pUserNo)
				BEGIN
					INSERT INTO dbo.TblStorePassword (mUserNo, mPassword) values (@pUserNo, @pPassword)
				END
			ELSE
				BEGIN
					UPDATE dbo.TblStorePassword SET mPassword = @pPassword  WHERE mUserNo = @pUserNo
				END
		END 
	ELSE	-- 菩胶况靛 檬扁拳
		BEGIN
			DELETE FROM TblStorePassword WHERE mUserNo = @pUserNo
		END

	SELECT @aErr = @@ERROR

	IF(@aErr <> 0)
		RETURN(2)	-- db error

	RETURN(0)

GO

