CREATE PROCEDURE [dbo].[WspGetInfoDeletedPc]
(
	@pPcNm CHAR(12)
)
AS
/******************************************************************************
**		Name: WspGetInfoDeletedPc
**		Desc: 삭제된 캐릭터 정보 보기
**
**		Auth: 이진선
**		Date: 2012-09-20

**		TestCode: 
		EXEC dbo.WspGetInfoDeletedPc
			'ddmao'
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT
		T2.mRegDate	--캐릭터생성일
		, T2.mOwner	--계정번호
		, T4.mUserId	--계정명
		, T1.mPcNo	--캐릭터번호
		, T1.mPcNm	--캐릭터명
		, T3.mLevel	--레벨
		, T3.mExp	--경험치
		, T2.mDelDate	--삭제일
	FROM dbo.TblPcDeleted AS T1
		INNER JOIN dbo.TblPc AS T2
	ON T1.mPcNo = T2.mNo
		INNER JOIN dbo.TblPcState AS T3
	ON T2.mNo = T3.mNo
		INNER JOIN FNLAccount.dbo.TblUser AS T4
	ON T2.mOwner = T4.mUserNo
	WHERE T1.mPcNm = @pPcNm
END

GO

