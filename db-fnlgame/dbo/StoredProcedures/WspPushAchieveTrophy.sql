/******************************************************************************
**		Name: WspPushAchieveTrophy
**		Desc: 傈府前 酒捞袍 牢亥配府俊 火涝
**            FNLParmDev OR FNLParmReal 狼 惑炔喊 炼例 鞘夸
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspPushAchieveTrophy]
(
	 @pPcNo				INT		-- 荤侩磊 锅龋
	,@pItemNo			INT		-- 酒捞袍 锅龋 (IID)
	,@pAchieveGuildID	INT		-- 诀利 辨靛 酒捞叼
	,@pAchieveID		TINYINT	-- 诀利 ID
	,@pExp				INT		-- 版氰摹
	,@pLimitLevel		TINYINT	-- 弥措饭骇
)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aRarity INT
			, @aCoinPoint INT
			, @aSerial BIGINT
	
	SELECT @aRarity = 0
			, @aCoinPoint = 0
			, @aSerial = 0

	IF NOT EXISTS (SELECT mNo FROM dbo.TblPc WHERE mNo = @pPcNo)
	BEGIN
		RETURN 1;
	END

	IF (@pLimitLevel <= 0)
	BEGIN
		RETURN 2;
	END
	
	IF NOT EXISTS (SELECT IID FROM FNLParm.dbo.DT_Item WHERE IID = @pItemNo)
	BEGIN
		RETURN 3;
	END

	IF NOT EXISTS (SELECT mID FROM FNLParm.dbo.DT_AchieveList WHERE mID = @pAchieveID)
	BEGIN
		RETURN 4;
	END

	-- 矫府倔 掘扁
	EXEC @aSerial = dbo.UspGetItemSerial

	IF (@aSerial <= 0)
	BEGIN
		RETURN 5;
	END

	-- 林拳 酒捞袍狼 锐蓖档甫 舅酒辰促.
	SELECT @aRarity = mRarity
	FROM FNLParm.dbo.DT_AchieveItemTrophy
	WHERE IID = @pItemNo

	IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
	BEGIN
		RETURN 6;
	END

	-- 林拳 器牢飘 掘扁
	EXEC @aCoinPoint = UspGetAchieveCoinPoint @aRarity, @pAchieveGuildID

	IF (@aCoinPoint = 0)
	BEGIN
		RETURN 7;
	END

	INSERT dbo.TblPcAchieveInventory
	(				
		mSerialNo
		, mPcNo
		, mItemNo
		, mAchieveGuildID
		, mCoinPoint
		, mSlotNo
		, mAchieveID
		, mExp
		, mLimitLevel
	)
	VALUES
	(
		@aSerial
		, @pPcNo
		, @pItemNo
		, @pAchieveGuildID
		, @aCoinPoint
		, 0
		, @pAchieveID
		, @pExp
		, @pLimitLevel
	)
	
	IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
	BEGIN
		RETURN 8;
	END
	
	RETURN 0;
END

GO

