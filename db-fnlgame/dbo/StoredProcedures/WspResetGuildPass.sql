CREATE PROCEDURE [dbo].[WspResetGuildPass]  	
	@pGuildNo	INT
AS
	SET NOCOUNT ON			
		
	DECLARE @aErrNo INT,
			@aRowCnt INT

	SELECT @aErrNo = 0, @aRowCnt = 0   
	
	
	DELETE dbo.TblGuildStorePassword
	WHERE mGuildNo = @pGuildNo
	
	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	
	IF @aErrNo <> 0 
		RETURN(1)	-- db error 	

    RETURN(0)

GO

