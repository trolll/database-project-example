CREATE PROCEDURE [dbo].[WspGetTransferGuildRepration]
	@pGuildNo INT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT *
	FROM dbo.T_TblGuildMoneyReparation
	WHERE mGuildNo = @pGuildNo

GO

