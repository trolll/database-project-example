/******************************************************************************  
**  File: 
**  Name: UspDelPcSkillTreeNodeItem  
**  Desc: PC의 스킬트리노드아이템 하나를 삭제한다.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDelPcSkillTreeNodeItem]
	@pPcNo		INT,
	@pSTNIID	INT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF @pPcNo = 1 OR @pPcNo = 0 
	BEGIN
		RETURN(0)	-- NPC, 버려진 아이템
	END

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	IF 0 = @pSTNIID 
		BEGIN
			DELETE FROM dbo.TblPcSkillTreeInventory
			WHERE mPcNo = @pPcNo
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1		-- SQL Error
			END
		END
	ELSE
		BEGIN
			DELETE FROM dbo.TblPcSkillTreeInventory
			WHERE mPcNo = @pPcNo AND mSTNIID = @pSTNIID
			IF(0 <> @@ERROR)
			BEGIN
				SET @aErrNo = 1		-- SQL Error
			END
		END

	RETURN @aErrNo

GO

