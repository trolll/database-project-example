/******************************************************************************
**		Name: UspInitAchieveGuildList
**		Desc: 诀利辨靛 府胶飘 檬扁拳.
**			  FNLParmDev OR FNLParmReal 狼 惑炔喊 炼例 鞘夸
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013/08/02	巢己葛				诀利辨靛鸥涝阑 涝仿栏肺 罐档废 荐沥    	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspInitAchieveGuildList]
(
	@pGuildType			INT			-- 诀利辨靛鸥涝
)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aError INT
	SET @aError = 0;
	
	-- 傍己傈 辨靛 沥焊.
	IF @pGuildType = 1
	BEGIN
		IF NOT EXISTS(SELECT mAchieveGuildID FROM dbo.TblAchieveGuildList)
		BEGIN
			INSERT 
			INTO dbo.TblAchieveGuildList(mGuildRank, mGuildName, mMemberName, mEquipPoint, mUpdateIndex)
			SELECT mGuildRank, mGuildName, mMemberName, mEquipPoint, mUpdateIndex 
			FROM FNLParm.dbo.DT_AchieveGuildList 
			
			IF @@ERROR <> 0 OR @@ROWCOUNT = 0
				RETURN 1;
		END
	END

	SET NOCOUNT OFF

	RETURN 0;

GO

