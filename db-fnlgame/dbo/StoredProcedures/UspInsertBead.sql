/******************************************************************************    
**  Name: UspInsertBead  
**  Desc: 구슬 삽입(링크 후 아이템 제거)  
**    
**                  
**  Return values:    
**   0 : 성공  
**   <>0 : 실패  
**                  
**  Author: 김강호  
**  Date: 2011-06-14  
*******************************************************************************    
**  Change History    
*******************************************************************************    
**  Date:       Author:    Description:    
**  --------    --------   ---------------------------------------    
*******************************************************************************/    
CREATE PROCEDURE [dbo].[UspInsertBead]  
  @pEquipSerialNo BIGINT  
  ,@pBeadSerialNo BIGINT  
  ,@pBeadIndex TINYINT  
  ,@pLeftMin INT OUTPUT  
AS  
 SET NOCOUNT ON;  
 SET XACT_ABORT ON;
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
 DECLARE @aBeadItemNo INT;  
 DECLARE  @aEndDay  SMALLDATETIME;
 DECLARE @aCntUse  SMALLINT;
   
 SELECT @aBeadItemNo = mItemNo, @aEndDay =  mEndDate , @aCntUse =   mCntUse
 FROM dbo.TblPcInventory  
 WHERE mSerialNo = @pBeadSerialNo;  
 IF @aBeadItemNo IS NULL OR @@ERROR <> 0  
 BEGIN  
  return 1;  
 END  
  
 SELECT @pLeftMin = DATEDIFF(mi,GETDATE(),@aEndDay);  
   
 -- 기간 만료된 것을 지운다.
 DELETE FROM dbo.TblPcBead  
 WHERE [mOwnerSerialNo]=@pEquipSerialNo and ([mEndDate] <= getdate());
   
 BEGIN TRAN;   
   
 INSERT dbo.TblPcBead(  
  [mRegDate],  
  [mOwnerSerialNo],  
  [mBeadIndex],  
  [mItemNo],  
  [mEndDate],  
  [mCntUse])  
 VALUES(  
  getdate(),   
  @pEquipSerialNo,   
  @pBeadIndex,   
  @aBeadItemNo,   
  @aEndDay,   
  @aCntUse);  
 IF @@ERROR <> 0 OR @@ROWCOUNT <> 1  
 BEGIN  
  ROLLBACK TRAN;  
  RETURN 2; -- 구슬을 추가할 수 없습니다.  
 END  
   
 DELETE FROM dbo.TblPcInventory  
 WHERE mSerialNo = @pBeadSerialNo;  
 IF @@ERROR <> 0 OR @@ROWCOUNT <> 1  
 BEGIN  
  ROLLBACK TRAN;  
  RETURN 3; -- 구슬아이템을 삭제할 수 없습니다.  
 END  
   
 COMMIT TRAN;  
   
   
 RETURN 0;

GO

