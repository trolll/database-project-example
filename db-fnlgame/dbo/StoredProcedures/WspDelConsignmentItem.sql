CREATE PROCEDURE [dbo].[WspDelConsignmentItem]
(
	@pCnsmNo BIGINT
	, @pDelCnt BIGINT
)
AS
/******************************************************************************
**		Name: WspDelConsignmentItem
**		Desc: 위탁상점 아이템 삭제
**		Test:
			DECLARE @Return INT
			SET @Return = 0
			EXEC @Return = dbo.WspDelConsignmentItem
				13
				, 1
			SELECT @Return

**		Return:
			0 : 성공
			1 : 아이템이 없거나, 삭제하려는 개수가 존재하는 개수 보다 큼
			2 : 아이템 개수 업데이트 실패
			3 : 위탁판매 아이템 정보 삭제 실패
			4 : 위탁판매 정보 삭제 실패
**		Auth: 이진선
**		Date: 2012-12-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	-------------------------------------------------
	-- 변수 선언
	-------------------------------------------------
	DECLARE @aChkCnt INT
	DECLARE @aChgCnt INT
	DECLARE @aErrNo INT
	DECLARE @aRowCnt INT

	-------------------------------------------------
	-- 변수 초기화
	-------------------------------------------------
	SELECT @aChkCnt = 0
			,@aChgCnt = 0
			, @aErrNo = 0
			, @aRowCnt = 0

	-------------------------------------------------
	-- 아이템 개수 체크
	-------------------------------------------------
	SELECT @aChkCnt = mCurCnt
	FROM dbo.TblConsignment
	WHERE mCnsmNo = @pCnsmNo
	
	IF (@aChkCnt IS NULL OR @pDelCnt IS NULL OR @aChkCnt < @pDelCnt)
	BEGIN
		RETURN (1);	--아이템이 없거나, 삭제하려는 개수가 존재하는 개수 보다 큼
	END

	BEGIN TRAN;
		UPDATE dbo.TblConsignment SET @aChgCnt = mCurCnt = mCurCnt - @pDelCnt
		WHERE mCnsmNo = @pCnsmNo
		
		SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
		IF (@aErrNo <> 0 OR @aRowCnt <= 0)
		BEGIN
			ROLLBACK TRAN;
			RETURN(2);	--아이템 개수 업데이트 실패
		END

		IF (@aChgCnt = 0)
		BEGIN
			DELETE FROM dbo.TblConsignmentItem
			WHERE mCnsmNo = @pCnsmNo

			SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
			IF (@aErrNo <> 0 OR @aRowCnt <= 0)
			BEGIN
				ROLLBACK TRAN;
				RETURN(3);	--위탁판매 아이템 정보 삭제 실패
			END

			DELETE FROM dbo.TblConsignment
			WHERE mCnsmNo = @pCnsmNo		
			
			SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
			IF (@aErrNo <> 0 OR @aRowCnt <= 0)
			BEGIN
				ROLLBACK TRAN;
				RETURN(4);	--위탁판매 정보 삭제 실패
			END
		END
	COMMIT TRAN;

	RETURN (0);	--성공
END

GO

