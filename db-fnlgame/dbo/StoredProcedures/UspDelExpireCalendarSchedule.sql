/******************************************************************************
**		Name: UspDelExpireCalendarSchedule
**		Desc: Áö³­ ÀÏÁ¤ »èÁ¦
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDelExpireCalendarSchedule]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	
	DECLARE @aSerial		TABLE (
				mSerial		BIGINT	 PRIMARY KEY      	)			

	-- ÇØ´ç ±×·ì°ú ¿¬°áµÈ SerialNo¸¦ ±¸ÇÑ´Ù.
	-- 3°³¿ù ÀüÀÇ ÀÏÁ¤Àº »èÁ¦ (³¯Â¥´Â »ó°ü¾øÀ½ ¿ù´ÜÀ§)
	INSERT INTO @aSerial(mSerial)
	SELECT mSerialNo
	FROM dbo.TblCalendarSchedule
	WHERE 3 <= DATEDIFF( MM, mScheduleDate, GETDATE() )

	BEGIN TRAN

		DELETE dbo.TblCalendarSchedule
		WHERE mSerialNo IN (
							SELECT *
							FROM @aSerial
							)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		-- µî·ÏµÈ ±×·ì »èÁ¦
		DELETE dbo.TblCalendarScheduleGroup
		WHERE mSerialNo IN (
							SELECT *
							FROM @aSerial
							)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

		-- µî·ÏµÈ À¯Àú »èÁ¦
		DELETE dbo.TblCalendarPcSchedule
		WHERE mSerialNo IN (
							SELECT *
							FROM @aSerial
							)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);			
		END 

	COMMIT TRAN
	RETURN(0);

GO

