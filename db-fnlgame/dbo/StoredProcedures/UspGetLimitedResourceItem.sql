/****** Object:  Stored Procedure dbo.UspGetLimitedResourceItem    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetLimitedResourceItem    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspGetLimitedResourceItem    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetLimitedResourceItem    Script Date: 2010-12-23 17:45:59 ******/
/******************************************************************************
**		Name: UspGetLimitedResourceItem
**		Desc: 何劝 包访 沥焊甫 啊廉柯促
**
**		Auth: 炼 技泅
**		Date: 2009-07-27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetLimitedResourceItem]
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mResourceType, mIID, mIsComsume
	FROM dbo.TblLimitedResourceItem

GO

