CREATE PROCEDURE [dbo].[WspListCastle]
AS
	SET NOCOUNT ON			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT
		
		c.mPlace
		, g.mGuildNm
		, (
			SELECT p.mNm
			FROM [dbo].[TblPc] AS p
				INNER JOIN [dbo].[TblGuildMember] AS gm 
					ON p.mNo = gm.mPcNo
			WHERE gm.mGuildNo = c.mGuildNo AND gm.mGuildGrade = 0
		  ) AS mGuildMasterNm
		, (
			SELECT COUNT(*)
			FROM [dbo].[TblGuildMember] AS gm
			WHERE gm.mGuildNo = c.mGuildNo
		  ) AS mGuildTotCnt
		, c.mTaxBuy
		, c.mTaxHunt
		, c.mTaxGamble
		, c.mAsset
		, c.mGuildNo
		, c.mAssetBuy
		, c.mAssetHunt
		, c.mAssetGamble
	FROM [dbo].[TblCastleTowerStone] AS c
		INNER JOIN [dbo].[TblGuild] AS g 
			ON	c.mGuildNo = g.mGuildNo
	ORDER BY c.mPlace ASC

GO

