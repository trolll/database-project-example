/******************************************************************************  
**  Name: UspCnsmListCategoryItem  
**  Desc: 카테고리 한 개 최근 등록 리스트
**  
**                
**   
**                
**  Author: 김강호  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
** 2011 0712    김강호     레코드셋에 ItemSn, HoleCount 추가
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmListCategoryItem]
 @pCategory		TINYINT			-- 카테고리 번호
 ,@pPageNo		INT				-- 요청 페이지
 ,@pCntPerPage	INT			-- 페이지당 아이템수
 ,@pRealPageNo	INT OUTPUT	-- 결과셋의 실제 페이지 번호가 반환된다.
AS 
BEGIN 
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	-- 최근에 등록된 것이 먼저 보이도록 한다.
	declare @aCurDate smalldatetime;
	SET @aCurDate = getdate();

	-- 전체 로우 카운트
	declare @aTotalRow INT;
	select @aTotalRow = count(*)
	from dbo.TblConsignment a
	where a.mCategoryNo = @pCategory and @aCurDate < a.mTradeEndDate;
	

	-- 전체 페이지수 계산
	declare @aTotalPageNo INT;
	SET @aTotalPageNo = (@aTotalRow / @pCntPerPage);
	IF 0 < (@aTotalRow % @pCntPerPage)
	BEGIN
		SET @aTotalPageNo = @aTotalPageNo + 1;
	END

	IF 0 = @aTotalPageNo
	BEGIN
		SET @aTotalPageNo = 1;
	END
	
	SET @pRealPageNo = @pPageNo;
	IF @pRealPageNo <= 0 
	BEGIN
		SET @pRealPageNo = 1;
	END
	IF @aTotalPageNo < @pRealPageNo
	BEGIN
		SET @pRealPageNo = @aTotalPageNo;
	END	
	
	
	/*
	-- 필요한 레코드의 시작과 끝 번호 계산
	DECLARE @aStartRow INT;
	DECLARE @aEndRow INT;
	SET @aStartRow = (@pRealPageNo-1)*@pCntPerPage;
	IF @aStartRow < 0
	BEGIN
		SET @aStartRow = 0;
	END
	SET @aEndRow = @aStartRow + @pCntPerPage + 1;	

	WITH CategoryList AS
	(
		select 
			a.mCnsmNo AS mCnsmNo, a.mCategoryNo AS mCategoryNo, a.mPrice AS mPrice, 
				CASE WHEN a.mTradeEndDate <= @aCurDate
				THEN 0 ELSE
					DATEDIFF(minute, getdate(), a.mTradeEndDate) END AS mLeftMinutes, a.mPcNo AS mPcNo, a.mRegCnt AS mRegCnt, 
				a.mCurCnt AS mCurCnt, a.mItemNo AS mItemNo,
				b.mStatus AS mStatus, b.mCntUse AS mCntUse, b.mOwner AS mOwner, b.mPracticalPeriod AS mPracticalPeriod,
				count(*) as RowNumber
				--ROW_NUMBER() OVER(order by a.mCnsmNo desc) AS RowNumber
		from dbo.TblConsignment a, dbo.TblConsignment a2 inner join TblConsignmentItem b on a.mCnsmNo = b.mCnsmNo
		where a.mCategoryNo = @pCategory 
			and @aCurDate < a.mTradeEndDate
			and a.mCnsmNo < b.mCnsmNo
		
	)
	select 
		mCnsmNo, mCategoryNo, mPrice, 
		mLeftMinutes, mPcNo, mRegCnt, mCurCnt, mItemNo,
			mStatus, mCntUse, mOwner, mPracticalPeriod
	FROM CategoryList
	WHERE @aStartRow < RowNumber AND RowNumber < @aEndRow;	
	*/
	
	
   DECLARE @lastKeyValue BIGINT;

	DECLARE @numberToIgnore int;
    SET @numberToIgnore = (@pRealPageNo-1) * @pCntPerPage;
    
    
    IF @numberToIgnore > 0
	BEGIN
	
		SET ROWCOUNT @numberToIgnore;
		
		SELECT
            @lastKeyValue = [UniqueValue]
        FROM
        (
			SELECT
               cnsm.[mCnsmNo] AS [UniqueValue]
            FROM
                dbo.TblConsignment cnsm
            WHERE
				cnsm.[mCategoryNo] = @pCategory
				and
                cnsm.[mTradeEndDate] > @aCurDate
         ) AS Derived
          ORDER BY           
           [UniqueValue] DESC
	END	
		
	SET ROWCOUNT @pCntPerPage;	
	
	SELECT
        Derived.[UniqueValue],
		Derived.[mCategoryNo], 
		Derived.[mPrice], 
		Derived.[mLeftMinutes], 
		Derived.[mPcNo], 
		Derived.[mRegCnt], 
		Derived.[mCurCnt], 
		Derived.[mItemNo],
		Derived.[mStatus], 
		Derived.[mCntUse], 
		Derived.[mOwner], 
		Derived.[mPracticalPeriod],
		Derived.[mSerialNo],
		Derived.[mHoleCount]
	FROM
	(	
		select 
			a.mCnsmNo AS [UniqueValue], 
			a.mCategoryNo AS mCategoryNo, 
			a.mPrice AS mPrice, 
			CASE WHEN a.mTradeEndDate <= @aCurDate
			THEN 0 ELSE
				DATEDIFF(minute, getdate(), a.mTradeEndDate) END AS mLeftMinutes, 
			a.mPcNo AS mPcNo, 
			a.mRegCnt AS mRegCnt, 
			a.mCurCnt AS mCurCnt, 
			a.mItemNo AS mItemNo,
			b.mStatus AS mStatus, 
			b.mCntUse AS mCntUse, 
			b.mOwner AS mOwner, 
			b.mPracticalPeriod AS mPracticalPeriod,
			b.mSerialNo AS mSerialNo,
			b.mHoleCount as mHoleCount
		from dbo.TblConsignment a 
			inner join TblConsignmentItem b on a.mCnsmNo = b.mCnsmNo
		where a.mCategoryNo = @pCategory 
			and @aCurDate < a.mTradeEndDate						
	 ) AS Derived
	WHERE
    (
        @lastKeyValue IS NULL
    )
    OR
    (
		[UniqueValue] < @lastKeyValue	        
    )    
    ORDER BY
        [UniqueValue] DESC

    SET ROWCOUNT 0	
END

GO

