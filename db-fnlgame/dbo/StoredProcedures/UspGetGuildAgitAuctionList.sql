/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildAgitAuctionList    Script Date: 2008-11-10 10:37:16 ******/





CREATE Procedure [dbo].[UspGetGuildAgitAuctionList]	-- ?? ??? ????? ??? ??
	 @pTerritory		INT			-- ????
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET LOCK_TIMEOUT 2000
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0

	-- TblGuildAgitAuction ? ?? ????? ??????? ????? UspReduceGuildAgitTime ?? ?? ???? ????? ?

	-- ????? ??? ?? - ????? ??? ????? ???? ??? ??
	SELECT 
		 A.mTerritory
		,A.mGuildAgitNo
		,A.mOwnerGID
		,ISNULL((CASE A.mOwnerGID
			WHEN 0 THEN N'<NONE>'
			ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = A.mOwnerGID)
			END), N'<FAIL>') As mOwnerGuildName
		,A.mGuildAgitName
		,A.mSellingMoney
		,B.mCurBidGID
		,ISNULL((CASE B.mCurBidGID
			WHEN 0 THEN N'<NONE>'
			ELSE (SELECT mGuildNm FROM TblGuild WITH (NOLOCK) WHERE mGuildNo = B.mCurBidGID)
			END), N'<FAIL>') As mCurBidGuildName
		,B.mCurBidMoney
		,A.mLeftMin 
	FROM TblGuildAgit A JOIN TblGuildAgitAuction B ON A.mTerritory = B.mTerritory  AND A.mGuildAgitNo = B.mGuildAgitNo 	-- WITH(NOLOCK) 
	WHERE A.mTerritory = @pTerritory AND (A.mOwnerGID = 0 OR A.mIsSelling = 1)

	IF(0 <> @@ERROR)
	BEGIN
		SET @aErrNo = 1	-- 1: DB ???? ??
	END
	
LABEL_END_LAST:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

