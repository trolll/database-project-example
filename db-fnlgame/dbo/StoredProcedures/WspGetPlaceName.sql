--------------------------------
-- ░┤├╝╕φ : WspGetPlaceName
-- ╝│╕φ : ┴÷┐¬ ╕φ░· Index ╣°╚ú╕ª ░í┴«┐┬┤┘
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.07.11
-- ╝÷┴ñ└┌ :  
-- ╝÷┴ñ└╧ : 
--------------------------------

CREATE PROCEDURE [dbo].[WspGetPlaceName] 
AS
	SET NOCOUNT ON

	SELECT
		*
	FROM
		TblPlace
	WITH ( NOLOCK )

	SET NOCOUNT OFF

GO

