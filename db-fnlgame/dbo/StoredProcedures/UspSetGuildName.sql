/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetGuildName    Script Date: 2008-11-10 10:37:18 ******/





CREATE PROCEDURE [dbo].[UspSetGuildName]
	 @pGuildNo		INT
	,@pGuildNm		CHAR(12)	
AS
	SET NOCOUNT ON	
	DECLARE @pRowCnt	INT,
			@pErr		INT

	SELECT	@pRowCnt = 0, 
			@pErr = 0
		
		
	--------------------------------------------------------------------------
	-- ??? ?? ???? ?? 
	--------------------------------------------------------------------------
	IF EXISTS(	SELECT *
				FROM dbo.TblGuild WITH(NOLOCK)
				WHERE mGuildNm = @pGuildNm )
	BEGIN		
		RETURN(1)				-- ?? ??? ?????.
	END

	--------------------------------------------------------------------------
	-- ??? ?? 
	--------------------------------------------------------------------------			
	UPDATE dbo.TblGuild	
	SET 	
		mGuildNm = @pGuildNm
	WHERE  [mGuildNo] = @pGuildNo

	SELECT	@pRowCnt = @@ROWCOUNT, 
			@pErr  = @@ERROR	

	IF @pRowCnt = 0
	BEGIN				
		RETURN(2)
	END
	
	IF @pErr <> 0
	BEGIN				
		RETURN(@pErr)		-- System Error.
	END

	RETURN(0)				-- NOT ERROR

GO

