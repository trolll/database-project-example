/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspAddQuickSupplication    Script Date: 2008-11-10 10:37:16 ******/



create PROCEDURE [dbo].[UspAddQuickSupplication]
	  @pPcNo		INT
	, @pCate		TINYINT
	, @pSupplication	VARCHAR(1000)
	, @pPosX		FLOAT
	, @pPosY		FLOAT
	, @pPosZ		FLOAT
	, @pIp			VARCHAR(20)
AS
	SET NOCOUNT ON		
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE	@aErrNo		INT
	DECLARE @aQSID		BIGINT
	DECLARE	@aTime		DATETIME
	SELECT	@aErrNo = 0, @aQSID = 0, @aTime = '1900-01-01 00:00:00'

	-- ????? ???? ?? ??? ???? ? ??? ?????.
	IF EXISTS ( SELECT mQSID 
				FROM dbo.TblQuickSupplicationState 
				WHERE mPcNo = @pPcNo 
						AND mStatus < 2 )
	BEGIN		
		SET @aErrNo = 1	-- ??? ??? ??.
		GOTO T_RETURN
	END

	BEGIN TRAN
		
		SET @aTime = GETDATE()
			
		-- ???? ????.
		INSERT INTO dbo.TblQuickSupplicationState (mRegDate, mPcNo, mCategory) 
		VALUES (
			@aTime
			, @pPcNo
			, @pCate
		)
		
		IF(@@ERROR <> 0)
		BEGIN			
			SET @aErrNo = 2	-- DB Insert Error
			GOTO T_END
		END
		
		SELECT  @aQSID = @@IDENTITY

	
		-- ?? ??? ?? ??. 			
		INSERT  INTO dbo.TblQuickSupplication (mQSID, mSupplication, mPosX, mPosY, mPosZ, mIp)
		VALUES (@aQSID, @pSupplication, @pPosX, @pPosY, @pPosZ, @pIp)
		
		IF(@@ERROR <> 0)
		BEGIN
			SET @aErrNo = 3
			GOTO T_END
		END
			
T_END:
	IF ( @aErrNo = 0)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN

	 
T_RETURN:
	SELECT @aErrNo, @aQSID, @aTime

GO

