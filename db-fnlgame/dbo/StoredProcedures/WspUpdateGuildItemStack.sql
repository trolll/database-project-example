/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2010-12-23 17:46:05 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2009-7-14 13:13:32 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2009-6-1 15:32:41 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2009-5-12 9:18:19 ******/

/****** Object:  Stored Procedure dbo.WspUpdateGuildItemStack    Script Date: 2008-11-10 10:37:21 ******/



CREATE PROCEDURE [dbo].[WspUpdateGuildItemStack]
	@pSerial		BIGINT
	,@pLeftCnt		INT
AS
	SET NOCOUNT ON	
		
	UPDATE dbo.TblGuildStore 
	SET 
		mCnt = @pLeftCnt
	WHERE mSerialNo = @pSerial
	IF @@ERROR<>0 OR @@ROWCOUNT <> 1
	BEGIN
		RETURN(1)	 
	END	
	
	RETURN(0)

GO

