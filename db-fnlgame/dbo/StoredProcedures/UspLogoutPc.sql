/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspLogoutPc    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspLogoutPc]
	 @pUserNo	INT
	,@pPcNo		INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo = 0
	
	DECLARE @aLogin	DATETIME
	SELECT 
		@aLogin=b.[mLoginTm] 
	FROM dbo.TblPc AS a 
		INNER JOIN dbo.TblPcState AS b 
		ON(a.[mNo] = b.[mNo])
	WHERE (a.[mOwner]=@pUserNo) 
		AND (a.[mNo]=@pPcNo) 
		AND (a.[mDelDate] IS NULL)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		SET  @aErrNo = 1
		GOTO LABEL_END	 	 
	END
	
	DECLARE @aDate	DATETIME
	SET		@aDate = GETDATE()	
	DECLARE @aHour	INT
	SET		@aHour = DATEDIFF(mi, @aLogin, @aDate)
	
	UPDATE dbo.TblPcState 
	SET 
		[mLogoutTm]=@aDate, 
		[mTotUseTm]=[mTotUseTm]+@aHour
	WHERE ([mNo]=@pPcNo)
	IF(0 <> @@ERROR) OR (0 = @@ROWCOUNT)
	 BEGIN
		SET  @aErrNo = 2
		GOTO LABEL_END	 
	 END

LABEL_END:		
	SET NOCOUNT OFF	
	RETURN(@aErrNo)

GO

