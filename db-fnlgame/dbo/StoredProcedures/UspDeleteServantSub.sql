/******************************************************************************
**		Name: UspDeleteServantSub
**		Desc: 辑锅飘 辑宏 牢郸胶 檬扁拳
**
**		Auth: 捞侩林
**		Date: 2016-04-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteServantSub]
	@pPcNo			INT
	,@pSerialNo		BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF 0 = @pSerialNo
	BEGIN
		DELETE dbo.TblPcServantSub
		WHERE mPcNo = @pPcNo
	END
	ELSE
	BEGIN
		DELETE dbo.TblPcServantSub
		WHERE mPcNo = @pPcNo
		  AND mSerialNo = @pSerialNo
	END

	SET NOCOUNT OFF;

GO

