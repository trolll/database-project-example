CREATE PROCEDURE [dbo].[WspGetTransferSerial]
	@pSerialNo BIGINT,
	@pOp TINYINT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF @pOp = 0		-- 신규 시리얼
	BEGIN
		SELECT *
		FROM (
			SELECT 
				'Inventory' AS mPos,
				mSerialNo,
				mOLDSerialNo,
				T2.mSvrNo,
				T2.mNm,
				T3.mRegDate -- 이동일
			FROM dbo.T_TblPcInventory T1		
				INNER JOIN dbo.T_TblPc T2
					ON T1.mPcNo = T2.mNo
				LEFT OUTER JOIN dbo.T_TblTransferPc T3
					ON T1.mPcNo = T3.mNo			
			WHERE mSerialNo = @pSerialNo
			UNION ALL
			SELECT 
				'Store' AS mPos,
				mSerialNo,
				mOLDSerialNo,
				T1.mSvrNo,			
				T2.mUserID AS mNm,
				NULL AS mRegDate
			FROM dbo.T_TblPcStore T1
				INNER JOIN FNLAccount.dbo.TblUser T2
					ON T1.mUserNo = T2.mUserNo
			WHERE mSerialNo = @pSerialNo
			UNION ALL
			SELECT 
				'GuildStore' AS mPos,
				mSerialNo,
				mOLDSerialNo,
				T2.mSvrNo,
				T2.mGuildNm mNm,
				NULL mRegDate -- 이동일
			FROM dbo.T_TblGuildStore T1		
				INNER JOIN dbo.T_TblGuild T2
					ON T1.mGuildNo = T2.mGuildNo
			WHERE mSerialNo = @pSerialNo			
		) T1
		
		RETURN 0
	END

	SELECT *
	FROM (
		SELECT 
			'Inventory' AS mPos,
			mSerialNo,
			mOLDSerialNo,
			T2.mSvrNo,
			T2.mNm,
			T3.mRegDate -- 이동일
		FROM dbo.T_TblPcInventory T1		
			INNER JOIN dbo.T_TblPc T2
				ON T1.mPcNo = T2.mNo
			LEFT OUTER JOIN dbo.T_TblTransferPc T3
				ON T1.mPcNo = T3.mNo			
		WHERE mOLDSerialNo = @pSerialNo
		UNION ALL
		SELECT 
			'Store' AS mPos,
			mSerialNo,
			mOLDSerialNo,
			T1.mSvrNo,			
			T2.mUserID AS mNm,
			NULL AS mRegDate
		FROM dbo.T_TblPcStore T1
			INNER JOIN FNLAccount.dbo.TblUser T2
				ON T1.mUserNo = T2.mUserNo
		WHERE mOLDSerialNo = @pSerialNo
		UNION ALL
		SELECT 
			'GuildStore' AS mPos,
			mSerialNo,
			mOLDSerialNo,
			T2.mSvrNo,
			T2.mGuildNm mNm,
			NULL mRegDate -- 이동일
		FROM dbo.T_TblGuildStore T1		
			INNER JOIN dbo.T_TblGuild T2
				ON T1.mGuildNo = T2.mGuildNo
		WHERE mOLDSerialNo = @pSerialNo					
	) T1
	
	RETURN 0

GO

