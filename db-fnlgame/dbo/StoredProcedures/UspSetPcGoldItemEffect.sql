/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspSetPcGoldItemEffect    Script Date: 2008-11-10 10:37:18 ******/



CREATE PROCEDURE [dbo].[UspSetPcGoldItemEffect]
	  @pPcNo		INT
	, @pItemType	INT
	, @pParmA		FLOAT
	, @pValidHour	INT	-- Hour
	, @pItemId		INT	
	, @pLeftTime	INT	OUTPUT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	IF(@pPcNo < 2)
	BEGIN		
		RETURN(1)	-- NPC? ?????? ??? ? ??.
	END

	DECLARE	@aEndDay	SMALLDATETIME
	SELECT @aEndDay = dbo.UfnGetEndDateByHour( GETDATE(), @pValidHour)
		, @pLeftTime= DATEDIFF(mi, GETDATE(), @aEndDay)

	IF EXISTS(	SELECT *
				FROM dbo.TblPcGoldItemEffect 
					WHERE mPcNo = @pPcNo 
						AND mItemType = @pItemType)
	BEGIN
		UPDATE dbo.TblPcGoldItemEffect
		SET 
			  mRegDate = GETDATE()
			, mParmA = @pParmA
			, mEndDate = @aEndDay
			, mItemNo = @pItemId
		WHERE mPcNo = @pPcNo 
				AND mItemType = @pItemType
	END
	ELSE
	BEGIN
		INSERT INTO dbo.TblPcGoldItemEffect (mPcNo, mItemType, mParmA, mEndDate, mItemNo) 
		VALUES(
			@pPcNo
			, @pItemType
			, @pParmA
			, @aEndDay
			, @pItemId)
	END
	
	IF(@@ERROR <>0 )
	BEGIN
		RETURN(2)	-- DB Err
	END
	
	RETURN(0)

GO

