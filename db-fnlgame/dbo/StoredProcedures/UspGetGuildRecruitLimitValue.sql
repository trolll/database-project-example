/****** Object:  Stored Procedure dbo.UspGetGuildRecruitLimitValue    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitLimitValue    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitLimitValue    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetGuildRecruitLimitValue    Script Date: 2010-12-23 17:45:59 ******/
/******************************************************************************
**		Name: UspGetGuildRecruitLimitValue
**		Desc: 辨靛葛笼 矫胶袍阑 捞侩吝牢 辨靛 力茄沥焊甫 掘绢柯促.
**
**		Auth: 辫锐档, 沥柳龋
**		Date: 2009.07.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetGuildRecruitLimitValue]
	@pGuildNo		INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT
		mClass,
		mMinLevel,
		mMinChao
	FROM
		dbo.TblGuildRecruitLimitValue 
	WHERE
		[mGuildNo] = @pGuildNo
	ORDER BY [mClass];

GO

