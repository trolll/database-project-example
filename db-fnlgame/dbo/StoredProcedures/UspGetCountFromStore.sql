/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetCountFromStore    Script Date: 2009-5-12 9:18:15 ******/
CREATE PROCEDURE [dbo].[UspGetCountFromStore]
	@pUserNo	INT

AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	SELECT
		ISNULL(COUNT(*),0)
	FROM dbo.TblPcStore
	WHERE mUserNo = @pUserNo
		AND mEndDate >  CONVERT(SMALLDATETIME, GETDATE())

GO

