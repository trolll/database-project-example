/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2009-11-16 10:23:26 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2009-5-12 9:18:16 ******/

/****** Object:  Stored Procedure dbo.UspListGuildBattleNm    Script Date: 2008-11-10 10:37:17 ******/





CREATE PROCEDURE [dbo].[UspListGuildBattleNm]
	 @pGuildNo		INT
AS
	SET NOCOUNT ON	
	
	SELECT 
		@pGuildNo AS mGuildNo1,
		T2.mGuildNo AS mGuildNo2,
		RTRIM(T2.mGuildNm) AS mGuildNm
	FROM (	
		SELECT 
			CASE [mGuildNo1]
				WHEN @pGuildNo THEN [mGuildNo2]
				ELSE [mGuildNo1]
			END AS mTargetGuildNo
		FROM dbo.TblGuildBattle WITH(NOLOCK)
		WHERE ([mGuildNo1] = @pGuildNo) 
			OR ([mGuildNo2] = @pGuildNo)
	) T1 INNER JOIN dbo.TblGuild T2  WITH(NOLOCK) 
		On T1.mTargetGuildNo = T2.mGuildNo
		
	SET NOCOUNT OFF

GO

