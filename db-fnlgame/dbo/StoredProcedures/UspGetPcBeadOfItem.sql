/******************************************************************************  
**  Name: UspGetPcBeadOfItem
**  Desc: 아이템 1개에 결합된 구슬 리스팅
**  
**  Return values:  
**   레코드셋
**   
**  Author: 김강호
**  Date: 2011-06-30
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetPcBeadOfItem]
	 @pOwnerItemSn		BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
	-- a.mOwnerSerialNo
	a.mItemNo
	,a.mBeadIndex
	,DATEDIFF(mi,GETDATE(),a.mEndDate) as mEndDateA
	,a.mCntUse
	FROM	
	TblPcBead a
	INNER JOIN dbo.TblPcInventory b ON a.mOwnerSerialNo=b.mSerialNo
	AND a.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	AND b.mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	AND a.mOwnerSerialNo = @pOwnerItemSn;

GO

