/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.WspGetGuildPlace    Script Date: 2009-7-14 13:13:32 ******/



CREATE         PROCEDURE [dbo].[WspGetGuildPlace]   
 @mGuildNm CHAR(12) -- ???  
AS  
 SET NOCOUNT ON  
   
 SELECT  
  mPlaceNm as mplace -- ????  
    
 FROM  
  [FNLParm].dbo.TblPlace  AS   pl  
 INNER JOIN  
  [dbo].[TblCastleTowerStone] AS c WITH (NOLOCK)  
 ON   
   c.mPlace=pl.mplaceno  
 INNER JOIN    
  [dbo].[TblGuildMember] AS gm WITH (NOLOCK)  
 ON   
  gm.mGuildNo = c.mGuildNo  
   
 INNER JOIN  
  [dbo].[TblGuild] AS g WITH (NOLOCK)  
 ON  
  gm.mGuildNo = g.mGuildNo  
   
 WHERE  
  g.mGuildNm = @mGuildNm  
  AND gm.mGuildGrade = 0

GO

