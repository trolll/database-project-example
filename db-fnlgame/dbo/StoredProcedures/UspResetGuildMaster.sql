/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspResetGuildMaster    Script Date: 2008-11-10 10:37:20 ******/



CREATE PROCEDURE [dbo].[UspResetGuildMaster] 
	@pGuildNo	INT,
	@pPcNo		INT,	-- ???? Master PcNo
	@pSerialNo	INT,
	@pItemNo	INT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED			
	
	DECLARE	 @aRowCnt		INT
			,@aErrNo		INT
			,@aPrevGuildMasterNo	INT
			
	SELECT  @aPrevGuildMasterNo = 0, @aRowCnt = 0,	@aErrNo = 0	

	SELECT 
		@aPrevGuildMasterNo = mPcNo
	FROM dbo.TblGuildMember
	WHERE mGuildNo = @pGuildNo
		AND mGuildGrade = 0	
		
	SELECT  @aRowCnt = @@ROWCOUNT,	@aErrNo = @@ERROR
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(2)	-- ?? ??? ??? ???, ?? ??? ??? ?? ?? ????. 		
	END	
		
	IF NOT EXISTS(	SELECT mSerialNo
					FROM dbo.TblPcInventory
					WHERE mPcNo = @aPrevGuildMasterNo
						AND mSerialNo = @pSerialNo 
						AND mItemNo = @pItemNo )
	BEGIN
		RETURN(3)	-- ????? ??? ???? ?? 
	END						
		
	BEGIN TRAN
	
		-- prev master
		UPDATE dbo.TblGuildMember
		SET	mGuildGrade = 4		-- ????? ??
		WHERE mGuildNo = @pGuildNo
			AND mPcNo = @aPrevGuildMasterNo
			AND mGuildGrade = 0
			
		SELECT  @aRowCnt = @@ROWCOUNT,	@aErrNo = @@ERROR
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END						
		
	
		UPDATE dbo.TblGuildMember
		SET	mGuildGrade = 0	
		WHERE mGuildNo = @pGuildNo
			AND mPcNo = @pPcNo
						
		SELECT  @aRowCnt = @@ROWCOUNT,	@aErrNo = @@ERROR
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END	
		
		DELETE dbo.TblPcInventory
		WHERE mPcNo = @aPrevGuildMasterNo
			AND mSerialNo = @pSerialNo 
			AND mItemNo = @pItemNo		

		SELECT  @aRowCnt = @@ROWCOUNT,	@aErrNo = @@ERROR
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			ROLLBACK TRAN
			RETURN(1)
		END	

						
	COMMIT TRAN
	RETURN(0)

GO

