/******************************************************************************  
**  Name: UspCnsmReg  
**  Desc: 위탁 아이템 등록
**  
**                
**  Return values:  
**   0 : 작업 처리 성공  
**   > 0 : SQL Error  
**                
**  Author: 김강호  
**  Date: 2010-12-02  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
**  2011 0712    김강호    HoleCount 추가
**  2011 0803    김강호    @aCntUse 데이터형 변경 : tinyint -> smallint
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspCnsmReg]
  @pSn		BIGINT	-- 등록할 아이템 시리얼 번호
 ,@pCnt		INT		-- 등록할 개수
 ,@pIsStack	BIT		-- 스택형인지 여부(1이면 스택)
 ,@pFeeItemSn	BIGINT	-- 수수료 아이템
 ,@pFeeItemCnt	INT		-- 수수료 아이템 개수
 ,@pSlotCnt		INT		-- 등록 가능한 슬롯 수
 ,@pCatetory	TINYINT -- 카테고리
 ,@pPrice		INT		-- 개당 판매 가격
 ,@pPeriodMinute	INT	-- 판매 기간(분단위)
 ,@pCnsmSn			BIGINT OUTPUT -- 등록 성공시 위탁 번호
 ,@pTradeLeftMin	INT OUTPUT -- 남은 판매 기간(분단위)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
 
	DECLARE @aRowCnt   INT  ;
	DECLARE @aErrNo    INT  ;

	DECLARE @aCnt    INT  ;
	DECLARE @aItemNo   INT  ;
	DECLARE @aIsConfirm   BIT ; 
	DECLARE @aStatus   TINYINT;  
	DECLARE @aCntUse   SMALLINT ; 
	DECLARE @aPcNo    INT  ;
	DECLARE @aEndDate   SMALLDATETIME  ;
	DECLARE @aLeftCnt   INT  ;
	DECLARE @aRegDate   DATETIME  ;
	DECLARE @aOwner    INT  ;
	DECLARE @aPracticalPeriod INT  ;
	DECLARE @aBindingType  TINYINT ; 
	DECLARE @aRestoreCnt  TINYINT  ;
	DECLARE @aHoleCount	TINYINT  ;

	IF(@pCnt < 1 OR 100 < @pCnt)
	BEGIN  
		SET @aErrNo = 1; -- 부적절한 개수입니다.
		GOTO T_END; 
	END  

	IF(@pSlotCnt < 1)
	BEGIN  
		SET @aErrNo = 2; -- 등록 가능한 슬롯이 부적절합니다.
		GOTO T_END;  
	END  

	-- 개당 가격이 10억을 넘으면 안된다
	IF(@pPrice < 100 OR 1000000000 < @pPrice)
	BEGIN  
		SET @aErrNo = 3; -- 판매 가격이 부적절합니다.
		GOTO T_END;
	END  

	IF(@pPeriodMinute < 3 OR 60*24*5 < @pPeriodMinute)
	BEGIN  
		SET @aErrNo = 4; -- 판매 기간이 부적절합니다.
		GOTO T_END;
	END 

	SET @pTradeLeftMin = @pPeriodMinute;

	SELECT @aRowCnt = 0, @aErrNo= 0  ;
	------------------------------------------  
	-- 인벤토리 아이템 체크   
	------------------------------------------  
	SELECT   
		@aLeftCnt=[mCnt],     
		@aCnt=[mCnt],   
		@aItemNo=[mItemNo],   
		@aIsConfirm=[mIsConfirm],   
		@aStatus=[mStatus],   
		@aCntUse=[mCntUse],  
		@aPcNo=[mPcNo],   
		@aEndDate=[mEndDate],  
		@aRegDate = [mRegDate],  
		@aOwner = [mOwner],  
		@aPracticalPeriod = [mPracticalPeriod],  
		@aBindingType = mBindingType,  
		@aRestoreCnt = mRestoreCnt,
		@aHoleCount  = mHoleCount
	FROM dbo.TblPcInventory
	WHERE  mSerialNo  =@pSn   
	AND mIsSeizure = 0  ;

	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		SET @aErrNo = 5; -- 인벤토리에 아이템이 존재하지 않습니다.
		GOTO T_END;
	END  

	SET @aCnt = @aCnt - @pCnt ; 
	IF @aCnt < 0  
	BEGIN  
		SET @aErrNo = 6; -- 인벤토리에 아이템 개수가 부족합니다.
		GOTO T_END;
	END  

	-- 바인딩 체크 (귀속 상태일 경우 이동이 절대 불가)   
	IF @aBindingType IN (1,2)   
	BEGIN  
		SET @aErrNo = 7 ;-- 귀속아이템입니다.
		GOTO T_END;
	END  

	-- 수수료 아이템 개수 체크
	DECLARE @aFeeItemCnt INT;
	SELECT @aFeeItemCnt=ISNULL([mCnt],0)
	FROM dbo.TblPcInventory
	WHERE  mSerialNo  =@pFeeItemSn  
	AND mIsSeizure = 0 	;
	
	IF(@aFeeItemCnt < @pFeeItemCnt)
	BEGIN
		SET @aErrNo = 8 ;-- 수수료 아이템이 부족합니다.  
		GOTO T_END;
	END
	-- @aFeeItemCnt가 0 이면 아이템을 삭제하고 그렇지 않으면 @pFeeItemCnt만큼 개수 감소시켜야 된다
	SET @aFeeItemCnt = @aFeeItemCnt - @pFeeItemCnt;



	-- 등록된 위탁 목록 개수 체크(선행 체크)
	DECLARE @aRegCnt INT;
	DECLARE @aExpectedIncome BIGINT;
	SELECT @aRegCnt = COUNT(*), @aExpectedIncome = ISNULL(SUM(CAST(mPrice AS BIGINT)*mCurCnt),0) 
	FROM dbo.TblConsignment
	WHERE mPcNo = @aPcNo;
	
	-- 이미 등록한 개수가 최대 슬롯 개수와 같거나 더 크면 안됨
	IF(@pSlotCnt <= @aRegCnt)
	BEGIN
		SET @aErrNo = 9; -- 등록 가능한 슬롯이 부족합니다.
		GOTO T_END;
	END

	-- 예상 판매 대금과 정산금의 합이 8000억을 넘으면 안 된다.
	BEGIN
		DECLARE @aBalance BIGINT;
		SELECT @aBalance = ISNULL(mBalance, 0) 
		FROM dbo.TblConsignmentAccount
		WHERE mPcNo=@aPcNo;
		IF 800000000000 < (@aExpectedIncome + @aBalance + CAST(@pPrice AS BIGINT) * @pCnt)
		BEGIN
			SET @aErrNo = 10; -- 판매 대금을 정산금 계좌가 수용할 수 없습니다.
			GOTO T_END;
		END
	END


	IF(@pIsStack = 0 and 1 <> @pCnt)  
	BEGIN  
		SET @aErrNo = 11; -- 스택형이 아니면 개수는 1이어야 됩니다.
		GOTO T_END;
	END

	BEGIN TRAN;

	-- 위탁 등록
	INSERT dbo.TblConsignment(
		mRegDate			,			
		mCategoryNo			,
		mPrice				,
		mTradeEndDate		,
		mPcNo				,
		mRegCnt				,
		mCurCnt				,
		mItemNo)
	VALUES(
		getdate(),			
		@pCatetory,
		@pPrice,
		DATEADD(minute, @pPeriodMinute, getdate()),
		@aPcNo,
		@pCnt,
		@pCnt,
		@aItemNo);
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT ; 
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN  
		SET @aErrNo = 12; -- 등록할 수 없습니다(TblConsignment)
		GOTO T_END;
	END

	SET @pCnsmSn = SCOPE_IDENTITY();

	INSERT dbo.TblConsignmentItem(
		mCnsmNo,
		mRegDate,
		mSerialNo,		-- 기존 시리얼 유지
		mEndDate,
		mIsConfirm,
		mStatus,
		mCntUse,
		mOwner,
		mPracticalPeriod,
		mBindingType,
		mRestoreCnt,
		mHoleCount)
	VALUES(
		@pCnsmSn,
		@aRegDate,
		@pSn,
		@aEndDate,
		@aIsConfirm,
		@aStatus,
		@aCntUse,
		@aOwner,
		@aPracticalPeriod,
		@aBindingType,
		@aRestoreCnt,
		@aHoleCount);
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN  
		SET @aErrNo = 13; -- 등록할 수 없습니다(TblConsignmentItem)
		GOTO T_END;
	END
	
	-- 수수료 차감, 남은게 있으면 개수 감소, 남은게 없으면 DELETE
	IF @aFeeItemCnt < 1
	BEGIN
		DELETE dbo.TblPcInventory  
		WHERE mSerialNo = @pFeeItemSn and mCnt = @pFeeItemCnt;
		-- 지워진 로우의 mCnt가 @pFeeItemCnt와 같지 않다면 롤백이다
	END
	ELSE
	BEGIN
		UPDATE dbo.TblPcInventory  
		SET mCnt = mCnt - @pFeeItemCnt
		WHERE mSerialNo = @pFeeItemSn;
	END
	SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
	IF @aErrNo<> 0 OR @aRowCnt <= 0   
	BEGIN  
		ROLLBACK TRAN  
		SET @aErrNo = 14 ;-- 수수료 차감에 실패했습니다. 
		GOTO T_END;
	END

	-- 등록된 위탁 목록 개수 체크
	SELECT @aRegCnt = COUNT(*) FROM dbo.TblConsignment
	WHERE mPcNo = @aPcNo;
	IF(@pSlotCnt < @aRegCnt)
	BEGIN
		ROLLBACK TRAN;
		SET @aErrNo = 15; -- 슬롯 개수가 초과됐습니다.
		GOTO T_END;
	END


	IF @pIsStack = 0
	BEGIN
		-- 스택형이 아니면 인벤토리 아이템 정보 삭제   
		DELETE dbo.TblPcInventory  
		WHERE mSerialNo = @pSn  ;
		SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
		IF @aErrNo<> 0 OR @aRowCnt <= 0   
		BEGIN  
			ROLLBACK TRAN  ;
			SET @aErrNo = 16 ;-- 인벤토리 아이템 삭제 실패
			GOTO T_END;
		END
	END
	ELSE -- 스택형이면
	BEGIN
		IF @aCnt < 1	-- 전부 판매할 경우
		BEGIN		
			-- 인벤토리에 쌓여 있는 정보 삭제
			DELETE dbo.TblPcInventory   
			WHERE mSerialNo = @pSn and mCnt = @pCnt;
			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
			IF @aErrNo<> 0 OR  @aRowCnt = 0  
			BEGIN  
				ROLLBACK TRAN
				SET @aErrNo = 16; -- 인벤토리 아이템 삭제 실패
				GOTO T_END;
			END			
			-- 지워진 로우의 mCnt가 pCnt와 같지 않으면 롤백해야 된다.
		END
		ELSE -- 일부만 판매할 경우
		BEGIN
			UPDATE dbo.TblPcInventory   
			SET   
			mCnt =mCnt - @pCnt   
			WHERE  mSerialNo  = @pSn;
			SELECT @aErrNo= @@ERROR,  @aRowCnt = @@ROWCOUNT  ;
			IF @aErrNo<> 0 OR  @aRowCnt = 0  
			BEGIN
				ROLLBACK TRAN  ;
				SET @aErrNo = 16; -- 인벤토리 아이템 삭제 실패
				GOTO T_END      ;
			END
		END
	END


T_ERR:   
	IF(0 = @aErrNo)  
		COMMIT TRAN  ;
	ELSE  
		ROLLBACK TRAN ; 

T_END:     
	SET NOCOUNT OFF   ;
	RETURN(@aErrNo) ;
END

GO

