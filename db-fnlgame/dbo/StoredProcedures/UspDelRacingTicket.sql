/******************************************************************************
**		Name: UspDelRacingTicket
**		Desc: 티켓을 감소시키며, 동시에 현재 몬스터 NID를 반환한다
**
**		Auth: 
**		Date: 2010-01-20
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      20100120	조세현				티켓을 감소시키며, 동시에 현재 몬스터 NID를 반환한다
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDelRacingTicket]
	 @pSerialNo		BIGINT
	,@pNID			INT OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	-- 해당 NID를 검색한다.
	SET @pNID			= 0

	SELECT @pNID = mNID 
	FROM dbo.TblRacingTicket
	WHERE mSerialNo = @pSerialNo

	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)
	END	

	DELETE dbo.TblRacingTicket 
	WHERE mSerialNo = @pSerialNo

	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)
	END	

	RETURN(0)

GO

