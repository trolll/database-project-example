--------------------------------
-- ░┤├╝╕φ : WspUpdateCharPosition
-- ╝│╕φ : ─│╕»┼═ └º─í╕ª ║»░µ╟╤┤┘
-- └█╝║└┌ : └╠┐∞╝«
-- └█╝║└╧ : 2006.04.09
-- ╝÷┴ñ└┌ : ▒Φ╝║└τ
-- ╝÷┴ñ└╧ : 2006.06.06
-- ╝÷┴ñ│╗┐δ : ╞«╖ú└Φ╝╟ ╗τ┐δ
--------------------------------
CREATE PROCEDURE [dbo].[WspUpdateCharPosition]	
	@mPosX REAL, -- └º─íX
	@mPosY REAL, -- └º─íY
	@mPosZ REAL, -- └º─íZ
	@mNm CHAR(12) -- ─│╕»┼═╕φ
AS
	SET NOCOUNT ON

	BEGIN TRANSACTION
	
	UPDATE
		[dbo].[TblPcState]
	SET
		mPosX = @mPosX,
		mPosY = @mPosY,
		mPosZ = @mPosZ
	WHERE		
		mNo = (SELECT mNo From [dbo].[TblPc] WHERE mNm = @mNm)			


	IF @@ERROR = 0
	BEGIN
		COMMIT TRANSACTION
		RETURN 0
	END
	ELSE
	BEGIN
		ROLLBACK TRANSACTION
		RETURN @@ERROR
	END

GO

