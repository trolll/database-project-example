/******************************************************************************
**		Name: UspDeleteServantGathering
**		Desc: 辑锅飘 盲笼 辆丰
**
**		Auth: 捞侩林
**		Date: 2016-10-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2016-12-16	捞侩林				盲笼 秒家 眠啊 CTrServantGatheringEndReq eFlagCancel = 0, eFlagFinish = 1
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteServantGathering]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pFlag			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE	@aErrNo		INT
		, @aRowCnt	INT = 0

	IF @pFlag = 0
	BEGIN
		DELETE
		FROM dbo.TblPcServantGathering
		WHERE mPcNo = @pPcNo
		  AND mSerialNo = @pSerialNo
		  AND GETDATE() < mEndDate
	END
	ELSE IF @pFlag = 1
	BEGIN
		DELETE
		FROM dbo.TblPcServantGathering
		WHERE mPcNo = @pPcNo
		  AND mSerialNo = @pSerialNo
		  AND mEndDate <= GETDATE()
	END

	SELECT @aErrNo = @@ERROR, @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt = 0
	BEGIN
		SET NOCOUNT OFF;
		RETURN 1;
	END

	SET NOCOUNT OFF;
	RETURN 0;

GO

