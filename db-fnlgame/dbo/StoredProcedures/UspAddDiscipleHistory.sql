/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2011-4-19 15:24:33 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspAddDiscipleHistory    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspAddDiscipleHistory]
	 @pMaster		INT
	,@pDisciple		INT
	,@pDiscipleNm		VARCHAR(32)
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE 	@aRv INT,
				@aErrNo	INT,
				@aHistoryNo	INT
			
	SELECT @aRv = 0, @aErrNo = 0, @aHistoryNo = 0

	SELECT
		@aHistoryNo = ISNULL(MAX(mNo),0)
	FROM dbo.TblDiscipleHistory 
	WHERE mMaster = @pMaster
	

	INSERT dbo.TblDiscipleHistory (mMaster, mNo, mDisciple, mDiscipleNm) 
	VALUES (@pMaster, @aHistoryNo+1, @pDisciple, @pDiscipleNm)
	
	SELECT @aRv = @@ROWCOUNT,  @aErrNo = @@ERROR
	
	IF @aErrNo <> 0 OR @aRv <= 0 
	BEGIN
		RETURN(1) -- db error 
	END

	RETURN (0)	-- non error

GO

