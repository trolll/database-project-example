/******************************************************************************
**		Name: UspGetPcAchieveItem
**		Desc: 诀利 酒捞袍 府胶飘 炼雀
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013/08/02	巢己葛				诀利辨靛鸥涝阑 涝仿栏肺 罐档废 荐沥     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcAchieveItem]
(
	@pGuildType			INT			-- 诀利辨靛鸥涝
	, @pPcNo			INT  
)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	SET ROWCOUNT 100; -- 弥措 啊廉棵 荐 乐绰 酒捞袍 荐甫 力茄   

	-- 烹钦 辨靛傈 辨靛 沥焊.
	IF @pGuildType = 2
	BEGIN
		SELECT   
			 T1.mSerialNo  
			,T1.mItemNo  
			,T1.mAchieveGuildID  
			,T1.mCoinPoint  
			,T1.mSlotNo  
			,T1.mAchieveID  
			,T1.mExp
			,T1.mLimitLevel
			,T1.mIsSeizure
			,DATEDIFF(dd, T1.mRegDate, GETDATE())
			,ISNULL(T2.mGuildRank, 0)
			,ISNULL(T2.mGuildName, '')
			,ISNULL(T2.mMemberName,'')
		FROM dbo.TblPcAchieveInventory AS T1
			INNER JOIN FNLParm.dbo.TblAchieveGuildlist AS T2
			ON T1.mAchieveGuildID = T2.mAchieveGuildID
		WHERE T1.mPcNo = @pPcNo
		ORDER BY T1.mSlotNo
	END


	-- 傍己傈 辨靛 沥焊.
	IF @pGuildType = 1
	BEGIN
		SELECT   
			 T1.mSerialNo  
			,T1.mItemNo  
			,T1.mAchieveGuildID  
			,T1.mCoinPoint  
			,T1.mSlotNo  
			,T1.mAchieveID  
			,T1.mExp
			,T1.mLimitLevel
			,T1.mIsSeizure
			,DATEDIFF(dd, T1.mRegDate, GETDATE())
			,ISNULL(T2.mGuildRank, 0)
			,ISNULL(T2.mGuildName, '')
			,ISNULL(T2.mMemberName,'')
		FROM dbo.TblPcAchieveInventory AS T1
			INNER JOIN dbo.TblAchieveGuildlist AS T2
			ON T1.mAchieveGuildID = T2.mAchieveGuildID
		WHERE T1.mPcNo = @pPcNo
		ORDER BY T1.mSlotNo
	END


	-- 叼弃飘 辨靛 沥焊.
	IF @pGuildType <> 1 AND @pGuildType <> 2
	BEGIN
		SELECT   
			 T1.mSerialNo  
			,T1.mItemNo  
			,T1.mAchieveGuildID  
			,T1.mCoinPoint  
			,T1.mSlotNo  
			,T1.mAchieveID  
			,T1.mExp
			,T1.mLimitLevel
			,T1.mIsSeizure
			,DATEDIFF(dd, T1.mRegDate, GETDATE())
			,ISNULL(T2.mGuildRank, 0)
			,ISNULL(T2.mGuildName, '')
			,ISNULL(T2.mMemberName,'')
		FROM dbo.TblPcAchieveInventory AS T1
			INNER JOIN FNLParm.dbo.DT_AchieveGuildList AS T2
			ON T1.mAchieveGuildID = T2.mAchieveGuildID
		WHERE T1.mPcNo = @pPcNo
		ORDER BY T1.mSlotNo
	END

GO

