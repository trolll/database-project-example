/******************************************************************************
**		File: UspGetGuild.sql
**		Name: UspGetGuild
**		Desc: GuildÁ¤º¸.
**
**		Auth: 
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20080722	JUDY				GuildMaster Á¤º¸ ¹ÝÈ¯
**		20090618	JUDY				±æµå ¹øÈ£¿¡¼­ ±æµå¸í º¯°æ
**		20091029	±è°­È£				¼º ¹æ¾î , ½ºÆÌ ¹æ¾î ÇýÅÃ ·¹º§
**		20091124	Á¶¼¼Çö				ºÎ±æ¸¶ À§ÀÓ ¿©ºÎ Ãß°¡
**		20141209	°ø¼®±Ô				¿¤Å×¸£ ¼º ¹æ¾î ÇýÅÃ ·¹º§
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetGuild]
	 @pGuildNo		INT
AS
	SET NOCOUNT ON;	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	SELECT a.[mGuildSeqNo], RTRIM(a.[mGuildNm]), a.[mGuildMark], RTRIM(a.[mGuildMsg]), a.[mRegDate], 
		   b.[mGuildAssNo], a.[mRewardExp], p.mNm, a.[mCastleDfnsLv]
		   , a.[mSpotDfnsLv]
		   , a.[mMandateSubMaster]
		   , a.[mEtelrCastleDfnsLv]
	FROM dbo.TblGuild AS a 
		INNER JOIN dbo.TblGuildMember AS gm
			ON gm.mGuildNo = a.mGuildNo	
				AND gm.mGuildGrade = 0
		INNER JOIN dbo.TblPc AS p
			ON gm.mPcNo = p.mNo							
		LEFT OUTER JOIN dbo.TblGuildAssMem AS b 
			ON(a.mGuildNo = b.mGuildNo)							
	WHERE a.[mGuildNo] 	= @pGuildNo

GO

