/******************************************************************************
**		Name: UspGetPcCalendarGroupMember
**		Desc: ´Þ·Â ±×·ì ¸â¹ö¸ñ·Ï
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-02-21
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetPcCalendarGroupMember]
	 @pPcNo			INT 
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		 T1.mGroupNo
		,T1.mMemberPcNo
		,RTRIM(T2.mNm)
	FROM dbo.TblCalendarGroupMember AS T1
		 INNER JOIN dbo.TblPc AS T2 
	ON T1.mMemberPcNo = T2.mNo
	WHERE mOwnerPcNo = @pPcNo

GO

