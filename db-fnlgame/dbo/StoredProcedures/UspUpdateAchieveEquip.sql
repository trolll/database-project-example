/******************************************************************************
**		Name: UspUpdateAchieveEquip
**		Desc: 傈府前 馒侩 咯何
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateAchieveEquip]
	 @pPcNo  INT 
	,@pEquipSerialNo BIGINT
	,@pUnEquipSerialNo BIGINT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE	 @aErr INT
		     ,@aItemNo INT

	SET @aErr = 0
	SET @aItemNo = 0


	BEGIN TRAN
		IF @pUnEquipSerialNo <> 0
		BEGIN
			DELETE dbo.TblPcAchieveEquip WHERE mSerialNo = @pUnEquipSerialNo AND mPcNo = @pPcNo

			IF @@ERROR <> 0 OR @@ROWCOUNT = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 1
				GOTO T_END
			END
		END
			
		IF @pEquipSerialNo <> 0
		BEGIN
			SELECT @aItemNo = mItemNo FROM dbo.TblPcAchieveInventory WHERE (mSerialNo = @pEquipSerialNo AND mPcNo = @pPcNo)

			IF @@ERROR <> 0 OR @@ROWCOUNT = 0 OR @aItemNo = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 2
				GOTO T_END
			END

			INSERT dbo.TblPcAchieveEquip (mSerialNo, mPcNo, mItemNo) VALUES (@pEquipSerialNo, @pPcNo, @aItemNo)

			IF @@ERROR <> 0 OR @@ROWCOUNT = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 3
				GOTO T_END
			END
		END		
	COMMIT TRAN

T_END:
	SELECT @aErr

GO

