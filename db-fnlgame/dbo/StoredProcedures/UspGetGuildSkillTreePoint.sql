/******************************************************************************  
**  File: 
**  Name: UspGetGuildSkillTreePoint  
**  Desc: Guild의 스킬트리 포인트 얻기
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.28 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspGetGuildSkillTreePoint]
	 @pGuildNo		INT,
	@pPoint	SMALLINT OUTPUT
AS  
	SET NOCOUNT ON   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	SET @pPoint = 0

	declare @aPoint smallint
	select @aPoint = mSkillTreePoint from TblGuild
	where mGuildNo = @pGuildNo
	
	if @aPoint is not null
	begin
		SET @pPoint = @aPoint
	end

	return 0

GO

