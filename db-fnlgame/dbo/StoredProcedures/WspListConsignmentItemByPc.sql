/******************************************************************************
**		Name: WspListConsignmentItemByPc
**		Desc: 
**
**		Auth: JUDY
**		Date: 2010-12-28
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspListConsignmentItemByPc]
	@mCharNm VARCHAR(12)	-- 캐릭터명
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	SET ROWCOUNT 1000;		-- 최대 가져올 수 있는 아이템 수를 제한 

	SELECT 
		 T1.mNm		-- 소지자
		,T1.mNo		-- 캐릭터 번호
		,T4.IName		-- 아이템명
		,DATEDIFF(DAY, GETDATE(), T3.mEndDate) AS mEndDate	-- 유효기간
		,T3.mSerialNo
		,T2.mCurCnt
		,T2.mItemNo
		,0 mIsSeizure
		,T3.mRegDate	-- 습득일자
		,T3.mPracticalPeriod
		,T3.mBindingType
		,T3.mRestoreCnt
		, T2.mCnsmNo
		, mRegCnt
		, mCurCnt
		, mPrice
		, mTradeEndDate		
	FROM dbo.TblPc T1 
		INNER JOIN dbo.TblConsignment  T2 
			ON T1.mNo = T2.mPcNo
		INNER JOIN dbo.TblConsignmentItem  T3
			ON T2.mCnsmNo = T3.mCnsmNo					
		LEFT OUTER JOIN FNLParm.dbo.DT_Item T4
			ON T2.mItemNo = T4.IID	
	WHERE T1.mNm = @mCharNm
		AND T1.mDelDate IS NULL

GO

