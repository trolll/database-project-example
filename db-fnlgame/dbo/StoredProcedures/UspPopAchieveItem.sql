/******************************************************************************
**		Name: UspPushAchieveItem
**		Desc: 诀利 酒捞袍 牢亥配府俊辑 力芭. (林拳/傈府前)
**            诀利 府胶飘狼 傈府前 酒捞袍 殿废 力芭.
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPopAchieveItem]
	@pPcNo INT  
	,@pSerialNo BIGINT 
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @aErr INT
			,@aIsSeizure BIT

	SET @aErr = 0

	SELECT @aIsSeizure = mIsSeizure FROM dbo.TblPcAchieveInventory 
	WHERE mSerialNo = @pSerialNo AND mPcNo = @pPcNo

	IF @@ERROR <> 0 OR @@ROWCOUNT = 0
	BEGIN
		SET @aErr = 1
		GOTO T_END
	END
	
	-- 拘幅 酒捞袍
	IF @aIsSeizure <> 0
	BEGIN
		SET @aErr = 2
		GOTO T_END
	END

	BEGIN TRAN
		-- 诀利 府胶飘狼 傈府前 酒捞袍 殿废 力芭.
		UPDATE dbo.TblPcAchieveList SET mSerialNo = 0 WHERE mSerialNo = @pSerialNo AND mPcNo = @pPcNo

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			SET @aErr = 3
			GOTO T_END
		END

		DELETE dbo.TblPcAchieveInventory WHERE mSerialNo = @pSerialNo AND mPcNo = @pPcNo

		IF @@ERROR <> 0 OR @@ROWCOUNT = 0
		BEGIN
			ROLLBACK TRAN
			SET @aErr = 4
			GOTO T_END
		END

	COMMIT TRAN

T_END:
	SELECT @aErr

GO

