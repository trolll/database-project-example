/******************************************************************************
**		Name: UspDeleteCalendarAgreement
**		Desc: ½ÂÀÎ »èÁ¦
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteCalendarAgreement]
	 @pOwnerPcNo		INT 
	,@pMemberPcNm		CHAR(12)
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aMemberPcNo	INT
	SET @aMemberPcNo		= 0

	IF NOT EXISTS ( SELECT T1.mNo
					FROM dbo.TblPc AS T1
						INNER JOIN TblCalendarAgreement AS T2
							ON T1.mNo = T2.mMemberPcNo
					WHERE T1.mNm = @pMemberPcNm
						AND T2.mOwnerPcNo = @pOwnerPcNo
						AND T1.mDelDate IS NULL	)
	BEGIN
		RETURN(1);
	END 

	SELECT  @aMemberPcNo = mNo
	FROM dbo.TblPc
	WHERE mNm = @pMemberPcNm

	BEGIN TRAN

		-- ³»²¨ 
		DELETE dbo.TblCalendarAgreement 
		WHERE mOwnerPcNo = @pOwnerPcNo 
				AND mMemberPcNo = @aMemberPcNo
		IF(0 <> @@ERROR)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END
		
		DELETE dbo.TblCalendarPcSchedule
		WHERE mPcNo = @pOwnerPcNo AND mOwnerPcNo = @aMemberPcNo
		IF(0 <> @@ERROR)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

		DELETE dbo.TblCalendarGroupMember
		WHERE mOwnerPcNo = @pOwnerPcNo AND mMemberPcNo = @aMemberPcNo
		IF(0 <> @@ERROR)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

		-- »ó´ë¹æ²¨
		DELETE dbo.TblCalendarAgreement 
		WHERE mOwnerPcNo = @aMemberPcNo
				AND mMemberPcNo = @pOwnerPcNo
		IF(0 <> @@ERROR)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END
		
		DELETE dbo.TblCalendarPcSchedule
		WHERE mPcNo = @aMemberPcNo AND mOwnerPcNo = @pOwnerPcNo
		IF(0 <> @@ERROR)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

		DELETE dbo.TblCalendarGroupMember
		WHERE mOwnerPcNo = @aMemberPcNo AND mMemberPcNo = @pOwnerPcNo
		IF(0 <> @@ERROR)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

	COMMIT TRAN

	RETURN(0);

GO

