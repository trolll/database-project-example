/******************************************************************************
**		Name: UspDecTicketUseCnt
**		Desc: 阁胶磐版林厘 萍南皑家 
**			  (阁胶磐版林厘萍南篮 厚胶琶屈捞绊 UseCnt啊 Cnt开且阑 茄促.)
**
**		Auth: 沥柳龋
**		Date: 2011-07-29
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2017-01-09	沥柳龋				mCntUse = @aCntUse -> mCntUse = mCntUse - @pCachingCnt 函版
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDecTicketUseCnt]
	 @pSerial		BIGINT
	,@pCachingCnt	INT			-- 啊皑蔼.(溜, 澜荐档 啊瓷)
	,@pNID			INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		
	
	DECLARE	 @aRowCnt		INT
				,@aErrNo		INT
				,@aCntUse		INT

	SELECT		@aErrNo = 0,	@aRowCnt = 0, @aCntUse = 0;
	SET			@pNID = 0;

	SELECT 
		@aCntUse = mCntUse
	FROM dbo.TblPcInventory
	WHERE mSerialNo = @pSerial;

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT;
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		SET @aErrNo = 1;
		GOTO T_END;	
	END

	--@@ROWCOUNT啊 0老荐档 乐促.
	SELECT @pNID = mNID
	FROM dbo.TblRacingTicket
	WHERE mSerialNo = @pSerial;

	SELECT @aErrNo = @@ERROR;
	IF @aErrNo <> 0
	BEGIN
		SET @aErrNo = 1;
		GOTO T_END;	
	END
	
	SET @aCntUse = @aCntUse - @pCachingCnt;
	IF @aCntUse < 0
	BEGIN
		SET @aErrNo = 2;
		GOTO T_END;	
	END

	BEGIN TRAN
		UPDATE dbo.TblPcInventory 
		SET 
			mCntUse = mCntUse - @pCachingCnt
		WHERE mSerialNo = @pSerial;

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT;
		IF @aErrNo <> 0 OR @aRowCnt <= 0 
		BEGIN
			SET @aErrNo = 3;
			GOTO T_ERR;
		END

		SELECT @aCntUse = mCntUse FROM dbo.TblPcInventory WHERE mSerialNo = @pSerial;	
		
		--@@ROWCOUNT啊 0老荐档 乐促.
		UPDATE dbo.TblRacingTicket 
		SET mTicketCnt = @aCntUse
		WHERE mSerialNo = @pSerial;
		SELECT @aErrNo = @@ERROR;
		IF @aErrNo <> 0
		BEGIN
			SET @aErrNo = 3;
			GOTO T_ERR;
		END				

T_ERR:
	IF(0 = @aErrNo)
		COMMIT TRAN;
	ELSE
		ROLLBACK TRAN;

T_END:
	RETURN(@aErrNo);

GO

