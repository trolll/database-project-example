/******************************************************************************
**		Name: UspGetGroupScheduleSerial
**		Desc: ÀÏÁ¤±×·ì¿¡ µî·ÏµÈ ½Ã¸®¾ó 
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-04-08
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetGroupScheduleSerial]
	 @pOwnerPcNo		INT 
	,@pGroup			VARCHAR(300)	-- ±×·ì¹øÈ£ ¸ñ·Ï
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @aGroup TABLE
	(
		mGroup TINYINT PRIMARY KEY      	
	)			

	INSERT INTO @aGroup(mGroup)
	SELECT CONVERT(TINYINT, element) mObjID
	FROM  dbo.fn_SplitTSQL(  @pGroup, ',' )
	WHERE LEN(RTRIM(element)) > 0

	SELECT mSerialNo
	FROM TblCalendarScheduleGroup
	WHERE mOwnerPcNo = @pOwnerPcNo
		AND mGroupNo IN (
						SELECT *
						FROM @aGroup
						)

GO

