/****** Object:  Stored Procedure dbo.UspSendGuildAgitTicketToGuild    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspSendGuildAgitTicketToGuild    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspSendGuildAgitTicketToGuild    Script Date: 2011-3-4 11:36:46 ******/

/****** Object:  Stored Procedure dbo.UspSendGuildAgitTicketToGuild    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspSendGuildAgitTicketToGuild    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspSendGuildAgitTicketToGuild    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspSendGuildAgitTicketToGuild    Script Date: 2009-11-16 10:23:29 ******/

/****** Object:  Stored Procedure dbo.UspSendGuildAgitTicketToGuild    Script Date: 2009-7-14 13:13:31 ******/
CREATE PROCEDURE [dbo].[UspSendGuildAgitTicketToGuild]
	 @pSerial		BIGINT		-- ????? ??? ??.
	,@pToGuildNm	VARCHAR(12)
	,@pStatus		TINYINT
	,@pTerritory	INT		-- ????
	,@pGuildAgitNo	INT		-- ???????
	,@pOwnerGID		INT		-- ????
	,@pFromPcNm		VARCHAR(12)	-- ??? ?? ??
    ,@pSendTicket  	INT		-- ??? ??? ??
    ,@pRecvTicket  	INT		-- ?? ??? ??
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE 	@aRowCnt 	INT,
				@aErrNo		INT
		
	SELECT @aErrNo = 0, @aRowCnt = 0

	DECLARE	@aToGuildNo	INT
	DECLARE 	@aSn	BIGINT
	DECLARE 	@aPcNo	INT
	DECLARE	@aEnd	DATETIME,
				@aBeginCount	INT,
				@aEndCount	INT
	DECLARE	@aGuildAgitTicket1	INT	-- ????? ???(??)
	DECLARE	@aGuildAgitTicket2	INT	-- ?? ????? ???
				
	DECLARE	@aGuildMember	TABLE (
					mSeq		INT	 IDENTITY(1,1)	 NOT NULL,
					mPcNo		INT	 NOT NULL
				)

	SELECT		 @aEnd = dbo.UfnGetEndDate(GETDATE(), 7)
				,@aBeginCount = 0
				,@aEndCount = 0
				,@aSn = 0
				,@aPcNo = 0
				,@aGuildAgitTicket1 = @pSendTicket	-- ????? ???(??) ?? (???? ????)
                ,@aGuildAgitTicket2 = @pRecvTicket	-- ?? ????? ??? ?? (???? ????)

	--------------------------------------------------------------------------	
	--?? ?? ?? 
	--------------------------------------------------------------------------
	SELECT 
		@aToGuildNo=mGuildNo
	FROM dbo.TblGuild 
	WHERE mGuildNm = @pToGuildNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(2)		-- 2 : ?? ??? ???? ??
	 END

	--------------------------------------------------------------------------	
	--????? ??? ??
	--------------------------------------------------------------------------
	DELETE dbo.TblPcInventory 
	WHERE mSerialNo = @pSerial
			 AND mItemNo = @aGuildAgitTicket1

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF @aErrNo <> 0 OR @aRowCnt <= 0 
	BEGIN
		RETURN(3)		-- 3 : ????? ???? ???? ??
	END
		 
	--------------------------------------------------------------------------	
	--?? ??? ??? ???
	--------------------------------------------------------------------------
	INSERT INTO @aGuildMember
	SELECT [mPcNo] 
	FROM dbo.TblGuildMember 
	WHERE  [mGuildNo] = @aToGuildNo
	
	SET @aEndCount = @@ROWCOUNT	

	--------------------------------------------------------------------------	
	--????? ??? ??
	--------------------------------------------------------------------------	
	WHILE(@aEndCount <>0)
	BEGIN

		SELECT 
			TOP 1	@aPcNo = mPcNo
		FROM @aGuildMember
		WHERE mSeq = @aBeginCount+1

		EXEC @aSn =  dbo.UspGetItemSerial 
		IF @aSn <= 0
		BEGIN
			CONTINUE	 
		END			

		IF @aPcNo > 0 
		BEGIN
			INSERT dbo.TblPcInventory(mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
				VALUES(@aSn, @aPcNo, @aGuildAgitTicket2, @aEnd, 0, @pStatus, 1, 0)

			IF(0 = @@ERROR)
			BEGIN
				INSERT dbo.TblGuildAgitTicket (mTicketSerialNo, mTerritory, mGuildAgitNo, mOwnerGID, mFromPcNm, mToPcNm)
				VALUES (@aSn,@pTerritory, @pGuildAgitNo, @pOwnerGID, @pFromPcNm, N'')
				IF(0 = @@ERROR)
				BEGIN
					SELECT @aPcNo, @aSn
				END
			END
		END

		SET @aEndCount = @aEndCount - 1
		SET @aBeginCount = @aBeginCount + 1
	END

	RETURN(0)

GO

