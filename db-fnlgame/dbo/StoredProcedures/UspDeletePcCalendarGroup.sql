/******************************************************************************
**		Name: UspDeletePcCalendarGroup
**		Desc: ±×·ì »èÁ¦
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-17
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeletePcCalendarGroup]
	 @pOwnerPcNo		INT 
	,@pGroupNo			TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE		 @aErr			INT
				,@aRowCnt		INT
				,@aSerialNo		BIGINT
				,@aCnt			INT

	SELECT 		 @aErr = 0
				,@aRowCnt = 0
				,@aSerialNo = 0 
				,@aCnt = 0


	DECLARE @aSerial		TABLE (
				mSerial		BIGINT	 PRIMARY KEY      	)		

	DECLARE @aMember		TABLE (
				mMember		INT	 PRIMARY KEY      	)			
	

	-- ÇØ´ç ±×·ì°ú ¿¬°áµÈ SerialNo¸¦ ±¸ÇÑ´Ù.
	INSERT INTO @aSerial(mSerial)
	SELECT mSerialNo
	FROM TblCalendarScheduleGroup
	WHERE mOwnerPcNo = @pOwnerPcNo AND mGroupNo = @pGroupNo

	SELECT @aCnt = COUNT(*)
	FROM @aSerial

	INSERT INTO @aMember(mMember)
	SELECT DISTINCT T1.mMemberPcNo
	FROM dbo.TblCalendarGroupMember AS T1
		 INNER JOIN dbo.TblPc AS T2 
				ON T1.mMemberPcNo = T2.mNo
	WHERE mOwnerPcNo = @pOwnerPcNo 
		AND mGroupNo = @pGroupNo


	BEGIN TRAN

		DELETE dbo.TblCalendarGroup
		WHERE mOwnerPcNo = @pOwnerPcNo AND mGroupNo = @pGroupNo
		
		SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF (@aErr <> 0) OR (@aRowCnt <= 0)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

		DELETE dbo.TblCalendarGroupMember
		WHERE mOwnerPcNo = @pOwnerPcNo AND mGroupNo = @pGroupNo
		
		SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF (@aErr <> 0) 
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

		IF @aRowCnt <> 0
		BEGIN
			DELETE dbo.TblCalendarScheduleGroup
			WHERE mOwnerPcNo = @pOwnerPcNo AND mGroupNo = @pGroupNo

			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF (@aErr <> 0)
			BEGIN
				ROLLBACK TRAN;
				RETURN(1);
			END

			IF @aCnt > 0
			BEGIN
				DELETE dbo.TblCalendarPcSchedule
				WHERE mOwnerPcNo = @pOwnerPcNo 
					AND mSerialNo IN (
									 SELECT *
									 FROM @aSerial
									 )
					AND mPcNo IN (
									 SELECT *
									 FROM @aMember
									 )

				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF (@aErr <> 0)
				BEGIN
					ROLLBACK TRAN;
					RETURN(1);
				END
			END
		END

	COMMIT TRAN

	RETURN(0);

GO

