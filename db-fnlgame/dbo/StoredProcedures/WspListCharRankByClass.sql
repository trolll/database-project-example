--------------------------------
-- ░┤├╝╕φ : WspListCharRankByClass
-- ╝│╕φ : ┼¼╖í╜║ ║░ ╖╣║º ╝°└º╕ª ╛≥┤┬┤┘
-- └█╝║└┌ : ▒Φ╝║└τ
-- └█╝║└╧ : 2006.04.16
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
--------------------------------
CREATE PROCEDURE [dbo].[WspListCharRankByClass]
	@mClass TINYINT -- ┼¼╖í╜║ (0:▒Γ╗τ, 1:╖╣└╬└·, 2:┐ñ╟┴)
AS
	SET NOCOUNT ON

	SELECT
		TOP 50 mNm, mLevel
	FROM
		[dbo].[TblPc] AS p WITH (NOLOCK)
	INNER JOIN
		[dbo].[TblPcState] AS s WITH (NOLOCK)
	ON
		p.mNo = s.mNo
	WHERE
		mClass = @mClass
		AND p.mDelDate IS NULL
	ORDER BY
		s.mLevel DESC, mExp DESC

GO

