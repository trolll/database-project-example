/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2011-4-19 15:24:40 ******/

/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2011-3-17 14:50:07 ******/

/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2011-3-4 11:36:47 ******/

/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2010-12-23 17:46:04 ******/

/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2010-3-22 15:58:22 ******/

/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2009-12-14 11:35:30 ******/

/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2009-11-16 10:23:30 ******/

/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2009-7-14 13:13:32 ******/


/****** Object:  Stored Procedure dbo.WspPushItemStore4MCntuse    Script Date: 2009-5-12 9:18:19 ******/  
  
CREATE  PROCEDURE [dbo].[WspPushItemStore4MCntuse]  
  @pUserNo  INT    -- ???? ??? ?????.(??? ???? ???)  
 ,@pItemNo  INT    -- ??? ??    
 ,@pCnt   INT    -- ??? ??  
 ,@pStatus  TINYINT   -- ??  
 ,@pIsSeizure BIT    -- ????   
 ,@pIsConfirm BIT    -- ?? ??   
 ,@pIsStack  BIT  
 ,@pValidDay  INT    -- ???.(??:?)  
 ,@pTargetSn  BIGINT OUTPUT -- ??? SN??.  
 ,@pPraticalHour INT = NULL  -- ?? ?? ??  
AS  
 SET NOCOUNT ON   
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
    
 DECLARE @aErrNo  INT  
 SET  @aErrNo = 0  
      
 DECLARE @aTargetSn  BIGINT   
 DECLARE @aCntTarget  INT  
 DECLARE @pCntUse TINYINT  
 SET  @aCntTarget = 0    
 DECLARE @aIsSeizure  BIT   
   , @pIsPracticalPeriod BIT  
  
   
 -- ?.  
 IF(@pItemNo = 409)  
  BEGIN  
  SET  @pIsConfirm = 1  
  SET  @pStatus = 1  
  END  
   
 -- ???   
 DECLARE @aEndDay SMALLDATETIME  
 SET  @aEndDay = dbo.UfnGetEndDate(GETDATE(), @pValidDay)  
   
 -- ?? ?? ?? ?? ??   
 SELECT  
  @pIsPracticalPeriod = mIsPracticalPeriod  
 FROM  
  [FNLParm].[dbo].[DT_Item]      
 WHERE  
  IID = @pItemNo   
    
    
 IF @pIsPracticalPeriod = 1 AND ( @pPraticalHour IS NULL OR @pPraticalHour <= 0)  
  RETURN (3)  
   
 IF @pPraticalHour > (30*24)   
  RETURN (4)  
    
 IF @pPraticalHour IS NULL  
  SET @pPraticalHour = 0  
     
 IF @pIsStack = 1   
 BEGIN  
  -- ???? ?? ??? ???? ???.  
  SELECT   
   @aCntTarget=ISNULL(mCnt,0),   
   @aTargetSn=mSerialNo,   
   @aIsSeizure=ISNULL(mIsSeizure,0)  
  FROM dbo.TblPcStore  
  WHERE mUserNo = @pUserNo  
    AND mItemNo = @pItemNo  
    AND mIsConfirm = @pIsConfirm  
    AND mStatus = @pStatus  
  
  IF(0 <> @@ERROR)  
  BEGIN  
   RETURN(4)  
  END  
     
  IF(0 <> @aIsSeizure)  
  BEGIN  
   RETURN(5)    
  END   
 END  
   
 --------------------------------------------------------------  
 -- ???? ????? ????.   
 --------------------------------------------------------------  
 IF @aTargetSn > 0  
 BEGIN  
  -- ??? ?? ??   
  SET @pTargetSn = @aTargetSn  
    
  UPDATE dbo.TblPcStore   
  SET   
   mCnt = mCnt + @pCnt  
  WHERE mSerialNo = @aTargetSn  
    AND mItemNo = @pItemNo  
  IF((0 <> @@ERROR) OR (1 <> @@ROWCOUNT))  
  BEGIN  
   RETURN(6)  
  END    
      
  RETURN(0)  
 END   
   
 EXEC @pTargetSn = dbo.UspGetItemSerial   
 IF @pTargetSn <= 0   
 BEGIN  
  RETURN(7)  
 END    
     
   
 SELECT @pCntUse = IUseNum  
  FROM [FNLParm].[dbo].[DT_Item]   
  WHERE IID = @pItemNo  
  
 -- ??? ?? ??? ??  
 INSERT INTO dbo.TblPcStore (mSerialNo, mUserNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse, mPracticalPeriod)  
 VALUES(  
  @pTargetSn,  
  @pUserNo,   
  @pItemNo,   
  @aEndDay,   
  @pIsConfirm,   
  @pStatus,   
  @pCnt,   
  @pCntUse, -- UsedCount (??? 0?? ?? ??)  
  @pPraticalHour  
 )   
   
 IF(0 <> @@ERROR)  
 BEGIN  
  RETURN(8)  
 END  
   
 RETURN(0)

GO

