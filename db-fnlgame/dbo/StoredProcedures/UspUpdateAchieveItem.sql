/******************************************************************************
**		Name: UspUpdateAchieveItem
**		Desc: 林拳甫 傈府前栏肺 函券 (林拳 -> 傈府前)
**			  诀利 府胶飘俊 傈府前 殿废.
**
**		Auth: 巢扁豪
**		Date: 2013.04.01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**     	
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateAchieveItem]
	 @pPcNo  INT 
	,@pSerialNo BIGINT
	,@pItemNo INT
	,@pGuildID INT
	,@pCoinPoint INT
	,@pSlot TINYINT
	,@pAchieveID TINYINT
	,@pExp INT
	,@pLimitLevel SMALLINT
	,@pIsSeizeure BIT
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE	 @aErr INT
			,@aSerialNo BIGINT
			,@aChangeTrophy INT

	SET @aErr = 0
	SET @aSerialNo = 0
	SET @aChangeTrophy = 0

	BEGIN TRAN

		-- 诀利苞 楷包等 酒捞袍 殿废
		IF @pAchieveID <> 0
		BEGIN
			SELECT @aSerialNo = mSerialNo FROM dbo.TblPcAchieveList WHERE mPcNo = @pPcNo AND mAchieveID = @pAchieveID

			IF @aSerialNo = 0
			BEGIN
				UPDATE dbo.TblPcAchieveList SET mSerialNo = @pSerialNo 
				WHERE mPcNo = @pPcNo AND mAchieveID = @pAchieveID

				IF @@ERROR <> 0 OR @@ROWCOUNT = 0
				BEGIN
					ROLLBACK TRAN
					SET @aErr = 1
					GOTO T_END
				END

				SET @aChangeTrophy = 1;
			END
		END

		IF @aChangeTrophy = 1
		BEGIN
			UPDATE dbo.TblPcAchieveInventory 
			SET mRegDate = getdate(),
				mItemNo = @pItemNo, 
				mAchieveGuildID = @pGuildID, 
				mCoinPoint = @pCoinPoint,
				mSlotNo = @pSlot, 
				mAchieveID = @pAchieveID,
				mExp = @pExp, 
				mLimitLevel = @pLimitLevel, 
				mIsSeizure = @pIsSeizeure
			WHERE mSerialNo = @pSerialNo AND mPcNo = @pPcNo
			
			IF @@ERROR <> 0 OR @@ROWCOUNT = 0
			BEGIN
				ROLLBACK TRAN
				SET @aErr = 2
				GOTO T_END
			END
		END
		ELSE
			BEGIN
				UPDATE dbo.TblPcAchieveInventory 
				SET mItemNo = @pItemNo, 
					mAchieveGuildID = @pGuildID, 
					mCoinPoint = @pCoinPoint,
					mSlotNo = @pSlot, 
					mAchieveID = @pAchieveID,
					mExp = @pExp, 
					mLimitLevel = @pLimitLevel, 
					mIsSeizure = @pIsSeizeure
				WHERE mSerialNo = @pSerialNo AND mPcNo = @pPcNo
				
				IF @@ERROR <> 0 OR @@ROWCOUNT = 0
				BEGIN
					ROLLBACK TRAN
					SET @aErr = 2
					GOTO T_END
				END
			END

	COMMIT TRAN

T_END:
	SELECT @aErr

GO

