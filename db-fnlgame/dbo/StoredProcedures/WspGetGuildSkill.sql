--------------------------------
-- °ґГјён : WspGetGuildSkill
-- јіён : ±жµеёнАё·О ЗцАз ±жµе±в єфµе ЗС ±жµе ЅєЕіёс·ПА» ѕтѕоїВґЩ
-- АЫјєАЪ : ±и±¤ј·
-- АЫјєАП : 2006.07.12
-- јцБ¤АЪ : 
-- јцБ¤АП : 
-- јцБ¤і»їл : 
--------------------------------
CREATE PROCEDURE [dbo].[WspGetGuildSkill]
	@mGuildNm VARCHAR(12) -- ±жµе АМё§
AS
	SET NOCOUNT ON

	SELECT
		g.mRegDate
		, g.mSTNIID
		, p.mNm
	FROM [dbo].[TblGuild] as gu	WITH ( NOLOCK )
	INNER	JOIN
		[dbo].[TblGuildSkillTreeInventory]	as g	WITH ( NOLOCK )	
	ON
		g.mGuildNo = gu.mGuildNo
	INNER	JOIN
		[dbo].[TblPc] as p	WITH ( NOLOCK )
	ON
		p.mNo = g.mCreatorPcNo
	WHERE
		gu.mGuildNm = @mGuildNm
	ORDER 	BY
		g.mCreatorPcNo

	SET NOCOUNT OFF

GO

