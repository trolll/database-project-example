/******************************************************************************
**		Name: UspDelExpireMakingEventQuest
**		Desc: 父丰等 捞亥飘 涅胶飘 昏力
**		Test:
			
**		Auth: 沥柳龋
**		Date: 2013-03-27
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDelExpireMakingEventQuest]
	@pQuestNoStr			VARCHAR(500)
AS
	SET NOCOUNT ON;			
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET XACT_ABORT ON;

	DECLARE @aQuest			TABLE (mQuestNo		INT)	

	INSERT INTO @aQuest(mQuestNo)
	SELECT CONVERT(INT, element) mObjID
	FROM  dbo.fn_SplitTSQL(  @pQuestNoStr, ',' )
	WHERE LEN(RTRIM(element)) > 0


	BEGIN TRAN

		DELETE 	dbo.TblEventQuestRank
		WHERE mQuestNo IN ( 
				SELECT *
				FROM @aQuest
			)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(5);			
		END 

		DELETE T2
		FROM dbo.TblPcQuest T1
			INNER JOIN dbo.TblPcQuestVisit T2
				ON T1.mQuestNo = T2.mQuestNo
					AND T1.mPcNo = T2.mPcNo
		WHERE T1.mQuestNo IN ( 
				SELECT *
				FROM @aQuest
			)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(2);			
		END 

		DELETE T2
		FROM dbo.TblPcQuest T1
			INNER JOIN dbo.TblPcQuestCondition T2
				ON T1.mQuestNo = T2.mQuestNo
					AND T1.mPcNo = T2.mPcNo
		WHERE T1.mQuestNo IN ( 
				SELECT *
				FROM @aQuest
			)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(3);			
		END 

		DELETE 	dbo.TblPcQuest
		WHERE mQuestNo IN ( 
				SELECT *
				FROM @aQuest
			)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN;
			RETURN(4);			
		END 

	COMMIT TRAN

	RETURN(0)

GO

