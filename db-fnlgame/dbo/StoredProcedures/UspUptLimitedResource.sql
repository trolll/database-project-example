/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2011-4-19 15:24:37 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2011-3-17 14:50:04 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2011-3-4 11:36:44 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2010-12-23 17:46:01 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2010-3-22 15:58:19 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2009-11-16 10:23:27 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2009-7-14 13:13:29 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2009-6-1 15:32:37 ******/

/****** Object:  Stored Procedure dbo.UspUptLimitedResource    Script Date: 2009-5-12 9:18:16 ******/

CREATE PROCEDURE [dbo].[UspUptLimitedResource]
	@pResourceType	INT,
	@pRandomVal		FLOAT
AS
	SET NOCOUNT ON
	
	UPDATE dbo.TblLimitedResource
	SET mRandomVal = @pRandomVal
	WHERE mResourceType = @pResourceType
	
	IF @@ERROR <> 0  OR @@ROWCOUNT <= 0
	BEGIN
		RETURN(1)	-- db error
	END

	RETURN(0)

GO

