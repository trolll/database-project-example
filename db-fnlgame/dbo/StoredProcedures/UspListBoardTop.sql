/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2011-4-19 15:24:33 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2011-3-17 14:50:00 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2010-12-23 17:45:58 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2010-3-22 15:58:16 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2009-11-16 10:23:24 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2009-7-14 13:13:26 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2009-6-1 15:32:35 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2009-5-12 9:18:14 ******/

/****** Object:  Stored Procedure dbo.UspListBoardTop    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspListBoardTop]
	@pBoardId		TINYINT
	,@pCnt			INT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE @aSql	NVARCHAR(512)
	DECLARE @aWhere	NVARCHAR(100)

	SET @aWhere 	= N' @aBoardId		TINYINT'	
	SET @aSql 		= N' SELECT TOP ' + CONVERT(NVARCHAR, @pCnt) +  CHAR(10) +
					     ' 	[mRegDate], [mBoardNo], RTRIM([mFromPcNm]), RTRIM([mTitle]), RTRIM([mMsg]) '  + CHAR(10) +
					      'FROM dbo.TblBoard WITH(NOLOCK) '  + CHAR(10) +
					    ' WHERE [mBoardId] = @aBoardId ' + CHAR(10) +
					    ' ORDER BY [mBoardNo] DESC'				

	EXEC sp_executesql	@aSql, @aWhere, @aBoardId = @pBoardId

GO

