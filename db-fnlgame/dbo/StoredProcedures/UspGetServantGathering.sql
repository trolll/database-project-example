/******************************************************************************
**		Name: UspGetServantGathering
**		Desc: 辑锅飘 盲笼 沥焊 掘扁
**
**		Auth: 捞侩林
**		Date: 2016-10-11
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetServantGathering]
	@pPcNo			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT mSerialNo, mEndDate
	FROM dbo.TblPcServantGathering
	WHERE mPcNo = @pPcNo

	SET NOCOUNT OFF;

GO

