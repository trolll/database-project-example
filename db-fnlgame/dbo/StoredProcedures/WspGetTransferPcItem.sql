CREATE PROCEDURE [dbo].[WspGetTransferPcItem]
	@pPcNo	INT
	,@pUserNO	INT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		T1.*,
		T2.IName
	FROM (
		SELECT 
			'Inventory' AS mPos,
			mSvrNo,
			mOLDSerialNo,
			mRegDate,
			mSerialNo,
			mItemNo,
			mEndDate,
			mIsConfirm,
			mStatus,
			mCnt,
			mCntUse,
			mIsSeizure,
			mApplyAbnItemNo,
			mApplyAbnItemEndDate,
			mOwner,
			mPracticalPeriod					
		FROM dbo.T_TblPcInventory
		WHERE mPcNo = @pPcNo
		UNION ALL
		SELECT 
			'Store' AS mPos,
			mSvrNo,
			mOLDSerialNo,
			mRegDate,
			mSerialNo,
			mItemNo,
			mEndDate,
			mIsConfirm,
			mStatus,
			mCnt,
			mCntUse,
			mIsSeizure,
			mApplyAbnItemNo,
			mApplyAbnItemEndDate,
			mOwner,
			mPracticalPeriod	
		FROM dbo.T_TblPcStore
		WHERE mUserNo = @pUserNO
	) T1 LEFT OUTER JOIN FNLParm.dbo.DT_ITEM T2
		ON T1.mItemNo = T2.IID

GO

