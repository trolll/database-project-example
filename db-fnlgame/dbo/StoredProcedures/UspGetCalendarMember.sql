/******************************************************************************
**		Name: UspGetCalendarMember
**		Desc: ÀÏÁ¤¿¡ µî·ÏµÈ ¸â¹ö¸ñ·Ï (ÀÏÁ¤ÀÌ µî·ÏµÇ¾ú´Ù´Â°É ¾Ë·ÁÁÙ¶§ »ç¿ë)
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetCalendarMember]
	 @pOwnerPcNo		INT 
	,@pGroup			VARCHAR(300)	-- ±×·ì¹øÈ£ ¸ñ·Ï
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @aGroup TABLE
	(
		mGroup TINYINT PRIMARY KEY      	
	)			

	INSERT INTO @aGroup(mGroup)
	SELECT CONVERT(TINYINT, element) mObjID
	FROM dbo.fn_SplitTSQL(  @pGroup, ',' )
	WHERE LEN(RTRIM(element)) > 0

	SELECT DISTINCT T1.mMemberPcNo
	FROM dbo.TblCalendarGroupMember AS T1
		 INNER JOIN dbo.TblPc AS T2 
	ON T1.mMemberPcNo = T2.mNo
	WHERE mOwnerPcNo = @pOwnerPcNo 
	AND mGroupNo IN (
						SELECT *
						FROM @aGroup
					)

GO

