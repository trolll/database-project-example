/******************************************************************************
**		Name: UspGetServantSkillTree
**		Desc: ¼­¹øÆ® ½ºÅ³Æ®¸® Á¤º¸ ¾ò±â
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspGetServantSkillTree]
	@pSerialNo		BIGINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT 
		mSTNIID,
		DATEDIFF(mi,GETDATE(),mEndDate) mEndDate
	FROM dbo.TblPcServantSkillTree WITH(NOLOCK)
	WHERE   mSerialNo = @pSerialNo
		AND mEndDate > CONVERT(SMALLDATETIME, GETDATE())
	ORDER BY mSTNIID ASC

	SET NOCOUNT OFF;

GO

