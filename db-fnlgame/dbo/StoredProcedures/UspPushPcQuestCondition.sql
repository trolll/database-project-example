/******************************************************************************
**		Name: UspPushPcQuestCondition
**		Desc: 旌愲Ν韯?韤橃姢韸?臁瓣贝 鞛呺牓
**
**		Auth: 鞝曥順?
**		Date: 2010-02-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspPushPcQuestCondition]
	 @pPcNo			INT
	,@pQuestNo		INT	 
	,@pMonNo		INT
    ,@pMaxCnt  		INT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF EXISTS ( SELECT * 
				FROM dbo.TblPcQuestCondition
				WHERE mQuestNo = @pQuestNo 
					AND mPcNo = @pPcNo 
					AND mMonsterNo = @pMonNo )
	BEGIN
		RETURN(0);
	END 
    
	INSERT INTO dbo.TblPcQuestCondition([mPcNo], [mQuestNo], [mMonsterNo], [mCnt], [mMaxCnt])
		VALUES(@pPcNo, @pQuestNo, @pMonNo, 0, @pMaxCnt)
	IF(0 <> @@ERROR)
	BEGIN
		RETURN(1)
	END	

	RETURN(0)

GO

