/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2011-4-19 15:24:38 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2011-3-17 14:50:05 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2010-12-23 17:46:02 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2010-3-22 15:58:20 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2009-12-14 11:35:28 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2009-7-14 13:13:30 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2009-6-1 15:32:38 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2009-5-12 9:18:17 ******/

/****** Object:  Stored Procedure dbo.UspDecDiscipleJoinCount    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspDecDiscipleJoinCount]
	 @pPcNo	INT
	,@pDecNum	INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aRv INT
	DECLARE @aErrNo INT,
			@aRowCnt INT
	DECLARE @aDiscipleJoin INT

	SELECT @aErrNo = 0, @aRv = 0, @aRowCnt = 0


	IF NOT EXISTS(	SELECT mNo 
					FROM dbo.TblPcState 
					WHERE mNo = @pPcNo 
						AND mDiscipleJoinCount - @pDecNum >= 0)
	BEGIN
		RETURN(0)
	END	
	
	UPDATE dbo.TblPcState 
	SET @pDecNum =
			mDiscipleJoinCount = mDiscipleJoinCount - @pDecNum 
	WHERE mNo = @pPcNo
	
	SELECT @aRowCnt = @@ROWCOUNT, @aErrNo = @@ERROR
	IF @aErrNo <> 0 OR @aRowCnt <= 0
	BEGIN
		RETURN(1)
	END 

	RETURN(0)

GO

