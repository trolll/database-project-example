/******************************************************************************  
**  File: 
**  Name: UspDelGuildSkillTreeNodeItem  
**  Desc: 길드 스킬트리노드아이템 하나를 삭제한다.
**  
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:    Author:    Description: 
**  -------- --------   ---------------------------------------  
**  2010.05.25 dmbkh    생성
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspDelGuildSkillTreeNodeItem]
	@pGuildNo		INT,
	@pSTNIID	INT
AS  
	SET NOCOUNT ON;   
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	

	IF 0 = @pSTNIID
		BEGIN
			DELETE FROM dbo.TblGuildSkillTreeInventory
			WHERE mGuildNo = @pGuildNo
			IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1		-- SQL Error
				END
		END
	ELSE
		BEGIN
			DELETE FROM dbo.TblGuildSkillTreeInventory
			WHERE mGuildNo = @pGuildNo AND mSTNIID = @pSTNIID
			IF(0 <> @@ERROR)
				BEGIN
					SET @aErrNo = 1		-- SQL Error
				END
		END


	RETURN @aErrNo

GO

