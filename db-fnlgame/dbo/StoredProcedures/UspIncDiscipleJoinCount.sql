/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2011-4-19 15:24:39 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2011-3-17 14:50:06 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2011-3-4 11:36:45 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2010-12-23 17:46:03 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2010-3-22 15:58:21 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2009-12-14 11:35:29 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2009-11-16 10:23:28 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2009-7-14 13:13:31 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2009-6-1 15:32:39 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2009-5-12 9:18:18 ******/

/****** Object:  Stored Procedure dbo.UspIncDiscipleJoinCount    Script Date: 2008-11-10 10:37:19 ******/





CREATE PROCEDURE [dbo].[UspIncDiscipleJoinCount]
	 @pPcNo	INT
	,@pIncNum	INT OUTPUT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aErrNo INT

	UPDATE dbo.TblPcState 
	SET 
		@pIncNum = 
			mDiscipleJoinCount = mDiscipleJoinCount + @pIncNum 
	WHERE mNo = @pPcNo
	
	SET @aErrNo = @@ERROR
	IF @aErrNo <> 0
	BEGIN
		RETURN(1) -- db error		
	END
	
	RETURN(0)

GO

