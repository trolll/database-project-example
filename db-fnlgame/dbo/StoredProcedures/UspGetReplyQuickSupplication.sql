/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2011-4-19 15:24:35 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2011-3-17 14:50:02 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2011-3-4 11:36:42 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2009-12-14 11:35:26 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2009-7-14 13:13:28 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspGetReplyQuickSupplication    Script Date: 2008-11-10 10:37:17 ******/



create  PROCEDURE [dbo].[UspGetReplyQuickSupplication]
	@pQSID		BIGINT
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE	@aErrNo	INT
	DECLARE	@aReply	VARCHAR(1000)

	SET @aErrNo = 0

	IF NOT EXISTS (	SELECT * 
					FROM dbo.TblQuickSupplicationReply 
					WHERE mQSID = @pQSID)
	BEGIN
		SET @aErrNo = 1
		GOTO T_END
	END	
	
	SELECT @aReply = mReply
	FROM dbo.TblQuickSupplicationReply
	WHERE mQSID = @pQSID

T_END:
	SELECT	@aErrNo, @aReply

GO

