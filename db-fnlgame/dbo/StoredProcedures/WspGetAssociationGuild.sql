--------------------------------
-- ░┤├╝╕φ : WspGetAssociationGuild
-- ╝│╕φ : ┐¼╟╒ ▒µ╡σ└╟ ╕±╖╧└╗ ░í┴«┐┬┤┘
-- └█╝║└┌ : ▒Φ▒ñ╝╖
-- └█╝║└╧ : 2006.07.11
-- ╝÷┴ñ└┌ : 
-- ╝÷┴ñ└╧ : 
--------------------------------

CREATE PROCEDURE [dbo].[WspGetAssociationGuild] 
	@mGuildNo	INT	-- ▒µ╡σ ╜─║░ ╣°╚ú
AS
	SET NOCOUNT ON

	SELECT 
		mGuildAssNo 
	FROM 
		TblGuildAssMem 
	WITH ( NOLOCK )
	WHERE 
		mGuildNo = @mGuildNo

	SET NOCOUNT OFF

GO

