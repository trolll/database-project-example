/******************************************************************************
**		Name: UspDeleteMacroVersion
**		Desc: ¸ÅÅ©·Î ÄÁÅÙÃ÷ ¹öÀü »èÁ¦
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteMacroVersion]
	@pPcNo			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DELETE dbo.TblMacroContentVersion
	WHERE mPcNo = @pPcNo

GO

