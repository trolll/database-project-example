/******************************************************************************
**		Name: UspDeleteMacroCommand
**		Desc: ¸ÅÅ©·Î Ä¿¸Çµå »èÁ¦
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2014-10-01
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspDeleteMacroCommand]
	@pPcNo			INT
	,@pMacroID		TINYINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DELETE dbo.TblMacroCommand
	WHERE mPcNo = @pPcNo
	AND mMacroID = @pMacroID

GO

