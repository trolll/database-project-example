/******************************************************************************  
**  Name: UspIncBeadUseCount
**  Desc: 구슬 사용 카운트 증가
**  
**                
**  Return values:  
**   0 : 성공
**   <>0 : 실패
**                
**  Author: 김강호
**  Date: 2011-06-21
*******************************************************************************  
**  Change History  
*******************************************************************************  
**  Date:       Author:    Description:  
**  --------    --------   ---------------------------------------  
*******************************************************************************/  
CREATE PROCEDURE [dbo].[UspIncBeadUseCount]
	@pEquipSerialNo		BIGINT
	,@pBeadIndex		TINYINT
	,@pCnt				SMALLINT			
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	update dbo.TblPcBead
	set mCntUse = mCntUse+@pCnt
	where mOwnerSerialNo = @pEquipSerialNo and mBeadIndex = @pBeadIndex;
	if @@error <> 0 or @@rowcount = 0
	begin
		return 1;
	end
	
	
	RETURN 0;

GO

