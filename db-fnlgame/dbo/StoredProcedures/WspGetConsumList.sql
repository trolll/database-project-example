/******************************************************************************
**		Name: WspGetConsumList
**		Desc: 위탁 상점 리스트를 추출 한다.
**
**		Auth: JUDY
**		Date: 2011-01-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
CREATE PROCEDURE [dbo].[WspGetConsumList]
	@pPcNm	VARCHAR(12)
	, @pItemNo	INT
	, @pEndDate	SMALLDATETIME
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
	SET ROWCOUNT 1000;		-- 최대 가져올 수 있는 아이템 수를 제한 

	SELECT 
		T2.mRegDate		-- 위탁일자
		, T5.mUserID		-- 위탁 계정
		, T1.mNm			-- 판매자명
		, T2.mItemNo
		, T4.IName mItemNm		
		, mCurCnt
		, mPrice		
	FROM dbo.TblPc T1 
		INNER JOIN dbo.TblConsignment T2
			ON T1.mNo = T2.mPcNo		
		LEFT OUTER JOIN FNLParm.dbo.DT_Item T4
			ON T2.mItemNo = T4.IID	
		LEFT OUTER JOIN FNLAccount.dbo.TblUser T5
			ON T1.mOwner = T5.mUserNo				
	WHERE T1.mNm = @pPcNm	
		AND T2.mItemNo = @pItemNo
		AND T2.mTradeEndDate > @pEndDate

GO

