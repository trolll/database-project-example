/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2011-4-19 15:24:34 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2011-3-17 14:50:01 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2011-3-4 11:36:41 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2010-12-23 17:45:59 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2010-3-22 15:58:17 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2009-12-14 11:35:25 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2009-11-16 10:23:25 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2009-7-14 13:13:27 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2009-6-1 15:32:36 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2009-5-12 9:18:15 ******/

/****** Object:  Stored Procedure dbo.UspFinishSiegeGamble    Script Date: 2008-11-10 10:37:16 ******/





CREATE PROCEDURE [dbo].[UspFinishSiegeGamble]
	 @pTerritory	INT
	,@pNo			INT	--??.
	,@pTotalMoney	INT	--?????.	 
	,@pOccupyGuild0	INT
	,@pOccupyGuild1	INT
	,@pOccupyGuild2	INT
	,@pOccupyGuild3	INT
	,@pOccupyGuild4	INT
	,@pOccupyGuild5	INT
	,@pOccupyGuild6	INT
	,@pIsKeep		INT
	,@pDividend		INT	OUTPUT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON	-- Count-set??? ???? ???.
	
	DECLARE	@aErrNo		INT
	SET		@aErrNo		= 0	
	SET		@pDividend	= 0
	
	IF(0 <> @@TRANCOUNT) ROLLBACK
	SET XACT_ABORT ON
	BEGIN TRAN	
	
	--??? = (? ???? * 90%) / ????
	--10%? ????.
	DECLARE	@aTotCnt	INT
	DECLARE @aOkCnt		INT	
	SELECT @aTotCnt=COUNT(*) FROM TblSiegeGambleTicket WHERE ([mTerritory]=@pTerritory) AND ([mNo]=@pNo)
	SELECT @aOkCnt=COUNT(*) FROM TblSiegeGambleTicket WHERE ([mTerritory]=@pTerritory) AND ([mNo]=@pNo) AND ([mIsKeep]=@pIsKeep)
	IF(0 < @aOkCnt)
	 BEGIN
		SET @pDividend = (@pTotalMoney*0.9) / @aOkCnt
	 END
	
	UPDATE TblSiegeGambleState 
			SET [mIsFinish]=1, [mDividend]=@pDividend, [mIsKeep]=@pIsKeep,
				[mOccupyGuild0]=@pOccupyGuild0, [mOccupyGuild1]=@pOccupyGuild1, 
				[mOccupyGuild2]=@pOccupyGuild2, [mOccupyGuild3]=@pOccupyGuild3,
				[mOccupyGuild4]=@pOccupyGuild4, [mOccupyGuild5]=@pOccupyGuild5,
				[mOccupyGuild6]=@pOccupyGuild6
			WHERE ([mTerritory] = @pTerritory) AND ([mNo] = @pNo)
	IF(1 <> @@ROWCOUNT)
	BEGIN
		SET @aErrNo = 1
		GOTO LABEL_END			 
	END			

	SET @pNo = @pNo + 1
	INSERT TblSiegeGambleState([mTerritory],[mNo],[mIsFinish],[mTotalMoney],[mDividend]) 
		VALUES(@pTerritory, @pNo, 0, 0, 0)
	IF(0 <> @@ERROR)
	 BEGIN
			SET @aErrNo = 2
			GOTO LABEL_END			 
	 END			

LABEL_END:	
	IF(0 = @aErrNo)
		COMMIT TRAN
	ELSE
		ROLLBACK TRAN
		
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

