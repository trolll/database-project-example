CREATE PROCEDURE [dbo].[WspListConsignmentAccountByAccountId]
(
	@pUserId VARCHAR(20)
)
AS
/******************************************************************************
**		Name: WspListConsignmentAccountByAccountId
**		Desc: 계정명으로 위탁판매 정상금 잔액 확인
**		Test:
			DECLARE @Return INT
			SET @Return = 0
			EXEC @Return = dbo.WspListConsignmentAccountByAccountId
				'shmssky'
			SELECT @Return
**		Return:
			0 : 성공
			1 : 계정 존재 안함
**		Auth: 이진선
**		Date: 2012-12-03
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**       
*******************************************************************************/
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-------------------------------------------------
	-- 변수 선언
	-------------------------------------------------
	DECLARE @aUserNo INT
	DECLARE @aErrNo INT
	DECLARE @aRowCnt INT

	-------------------------------------------------
	-- 변수 초기화
	-------------------------------------------------
	SELECT @aUserNo = 0
		, @aErrNo = 0
		, @aRowCnt = 0

	-------------------------------------------------
	-- 계정 존재여부 체크
	-------------------------------------------------
	SELECT 
		@aUserNo = mUserNo
	FROM FNLAccount.dbo.TblUser
	WHERE mUserId = @pUserId
	AND mDelDate = '1900-01-01'

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF (@aErrNo <> 0 OR @aRowCnt <= 0)
	BEGIN
		RETURN(1)	-- 계정 존재 안함
	END

	SELECT
		T1.mNo	--캐릭터 번호
		, T1.mNm	--캐릭터명
		, T2.mBalance	--정산금 잔액
	FROM dbo.TblPc AS T1
		INNER JOIN dbo.TblConsignmentAccount AS T2
	ON T1.mNo = T2.mPcNo
	WHERE T1.mOwner = @aUserNo
	AND T1.mDelDate IS NULL
		
	RETURN (0);
END

GO

