/******************************************************************************
**		Name: UspSendEtelrGuildAgitTicketToGuild
**		Desc: ¿¤Å×¸£ ¾ÆÁöÆ® ÃÊ´ëÀå(±æµå)
**
**		Auth: °ø¼®±Ô
**		Date: 2014.09.22
*******************************************************************************
**		Change History
*******************************************************************************
**		Date: 		Author:				Description: 
**		--------	--------			---------------------------------------
**		
*******************************************************************************/  	
CREATE PROCEDURE [dbo].[UspSendEtelrGuildAgitTicketToGuild]
	 @pSerial		BIGINT		-- ±æµå¾ÆÁöÆ® ÃÊÃ»Àå ¹øÈ£.
	,@pToGuildNm	VARCHAR(12)
	,@pStatus		TINYINT
	,@pTerritory	INT			-- ¿µÁö¹øÈ£
	,@pGuildAgitNo	INT			-- ±æµå¾ÆÁöÆ®¹øÈ£
	,@pOwnerGID		INT			-- ¼ÒÀ¯±æµå
	,@pFromPcNm		VARCHAR(12)	-- º¸³»´Â »ç¶÷ ÀÌ¸§
    ,@pSendTicket  	INT			-- º¸³»´Â ¾ÆÀÌÅÛ ÀÌ¸§
    ,@pRecvTicket  	INT			-- ¹Þ´Â ¾ÆÀÌÅÛ ÀÌ¸§
------------------WITH ENCRYPTION
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aRowCnt INT
			,@aErrNo INT
		
	SELECT @aErrNo = 0, @aRowCnt = 0

	DECLARE	@aToGuildNo			INT,
			@aFromGuildAssNo	INT,
			@aToGuildAssNo		INT,
			@aSn				BIGINT,
			@aFromPcNo			INT,
			@aEnd				DATETIME,
			@aBeginCount		INT,
			@aEndCount			INT,
			@aGuildAgitTicket1	INT,	-- ±æµåÇÏ¿ì½º ÃÊÃ»Àå(±æµå)
			@aGuildAgitTicket2	INT		-- ¹ÞÀº ±æµåÇÏ¿ì½º ÃÊÃ»Àå
				
	DECLARE	@aGuildMember		TABLE (
					mSeq		INT	 IDENTITY(1,1)	 NOT NULL,
					mPcNo		INT	 NOT NULL
				)

	SELECT		 @aEnd = dbo.UfnGetEndDate(GETDATE(), 7)
				,@aBeginCount = 0
				,@aEndCount = 0
				,@aSn = 0
				,@aFromPcNo = 0
				,@aGuildAgitTicket1 = @pSendTicket	-- ±æµå¾ÆÁöÆ® ÃÊÃ»Àå(±æµå) ¹øÈ£ (ÇÏµåÄÚµù µÇ¾îÀÖÀ½)
                ,@aGuildAgitTicket2 = @pRecvTicket	-- ¹ÞÀº ±æµå¾ÆÁöÆ® ÃÊÃ»Àå ¹øÈ£ (ÇÏµåÄÚµù µÇ¾îÀÖÀ½)

	--------------------------------------------------------------------------	
	--±æµå Á¤º¸ Ã¼Å© 
	--------------------------------------------------------------------------
	SELECT 
		@aToGuildNo=mGuildNo
	FROM dbo.TblGuild 
	WHERE mGuildNm = @pToGuildNm

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF (@aErrNo <> 0 OR @aRowCnt <= 0 )
	BEGIN
		RETURN(2)		-- 2 : ¹Þ´Â ±æµå°¡ Á¸ÀçÇÏÁö ¾ÊÀ½
	END
	 
	-- ¿¬ÇÕ±æµåÀÎÁö °Ë»ç
	IF (@pOwnerGID <> @aToGuildNo)	-- º»ÀÎÀÌ ¼ÓÇÑ ±æµå°¡ ¾Æ´Ï¶ó¸é ¿¬ÇÕ±æµåÀÎÁö °Ë»ç
	BEGIN
		-- From GuildAssNo ¾ò¾î¿À±â
		SELECT
			@aFromGuildAssNo = mGuildAssNo
		FROM dbo.TblGuildAssMem
		WHERE mGuildNo = @pOwnerGID

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF (@aErrNo <> 0 OR @aRowCnt <= 0)
		BEGIN
			RETURN(6)	--±æµå¿¬ÇÕ¸â¹ö°¡ ¾Æ´Ô
		END

		-- To GuildAssNo ¾ò¾î¿À±â
		SELECT
			@aToGuildAssNo = mGuildAssNo
		FROM dbo.TblGuildAssMem
		WHERE mGuildNo = @aToGuildNo

		SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
		IF @aErrNo <> 0 OR @aRowCnt <= 0
		BEGIN
			RETURN(6)	-- ±æµå¿¬ÇÕ¸â¹ö°¡ ¾Æ´Ô
		END

		-- °°Àº ±æµå¿¬ÇÕÀÌ ¾Æ´Ï¶ó¸é ¿¡·¯Ã³¸®
		IF @aFromGuildAssNo <> @aToGuildAssNo
		BEGIN
			RETURN(6)	-- °°Àº ±æµå¿¬ÇÕÀÌ ¾Æ´Ô
		END
	END

	--------------------------------------------------------------------------	
	--±æµå¾ÆÁöÆ® ÃÊÃ»Àå Á¦°Å
	--------------------------------------------------------------------------
	DELETE dbo.TblPcInventory 
	WHERE mSerialNo = @pSerial
	AND mItemNo = @aGuildAgitTicket1

	SELECT @aErrNo = @@ERROR,  @aRowCnt = @@ROWCOUNT
	IF (@aErrNo <> 0 OR @aRowCnt <= 0)
	BEGIN
		RETURN(3)		-- 3 : ±æµå¾ÆÁöÆ® ÃÊÃ»ÀåÀÌ Á¸ÀçÇÏÁö ¾ÊÀ½
	END
		 
	--------------------------------------------------------------------------	
	--ÇöÀç ±æµå¿ø Á¤º¸¸¦ ¾ò´Â´Ù
	--------------------------------------------------------------------------
	INSERT INTO @aGuildMember
	SELECT mPcNo
	FROM dbo.TblGuildMember 
	WHERE mGuildNo = @aToGuildNo
	
	SET @aEndCount = @@ROWCOUNT	

	--------------------------------------------------------------------------	
	--±æµå¾ÆÁöÆ® ÃÊÃ»Àå Àü¼Û
	--------------------------------------------------------------------------	
	WHILE(@aEndCount <> 0)
	BEGIN
		SELECT 
			TOP 1	@aFromPcNo = mPcNo
		FROM @aGuildMember
		WHERE mSeq = @aBeginCount+1

		EXEC @aSn =  dbo.UspGetItemSerial

		IF (@aSn <= 0)
		BEGIN
			CONTINUE	 
		END			

		IF (@aFromPcNo > 0)
		BEGIN
			INSERT dbo.TblPcInventory(mSerialNo, mPcNo, mItemNo, mEndDate, mIsConfirm, mStatus, mCnt, mCntUse)
			VALUES(@aSn, @aFromPcNo, @aGuildAgitTicket2, @aEnd, 0, @pStatus, 1, 0)

			IF(@@ERROR = 0)
			BEGIN
				INSERT dbo.TblGuildAgitTicket (mTicketSerialNo, mTerritory, mGuildAgitNo, mOwnerGID, mFromPcNm, mToPcNm)
				VALUES (@aSn,@pTerritory, @pGuildAgitNo, @pOwnerGID, @pFromPcNm, N'')

				IF(@@ERROR = 0)
				BEGIN
					SELECT @aFromPcNo, @aSn
				END
			END
		END

		SET @aEndCount = @aEndCount - 1
		SET @aBeginCount = @aBeginCount + 1
	END

	RETURN(0)

GO

