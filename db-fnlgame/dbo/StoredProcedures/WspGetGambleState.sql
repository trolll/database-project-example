-----------------------------------------------------------------------------------------------------------------
-- SP		: WspGetGambleState
-- DESC		: ┴╛╖ß╡╟┴÷ ╛╩└║ ░°╝║ ╜┬║╬╗τ└╟ ┴ñ║╕╕ª ░í┴«┐┬┤┘.
-- ╗²╝║└╧└┌	: 2006-10-19
-- ╗²╝║└┌	: ▒Φ▒ñ╝╖
-----------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[WspGetGambleState]

AS
	SET NOCOUNT ON
	
	SELECT 
		mRegDate, mTerritory, mIsFinish, mNo, mTotalMoney, mDividend, mOccupyGuild0, mOccupyGuild1, mOccupyGuild2, mOccupyGuild3, mOccupyGuild4,
		mOccupyGuild5, mOccupyGuild6, mIsKeep
	FROM 
		TblSiegeGambleState WITH (NOLOCK)
	GROUP BY
		mRegDate, mTerritory, mIsFinish, mNo, mTotalMoney, mDividend, mOccupyGuild0, mOccupyGuild1, mOccupyGuild2, mOccupyGuild3, mOccupyGuild4,
		mOccupyGuild5, mOccupyGuild6, mIsKeep	
	HAVING 
		mIsFinish = 0
	ORDER BY
		mTerritory

GO

