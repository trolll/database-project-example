/******************************************************************************
**		Name: UspUpdateServantSkillPoint
**		Desc: ¼­¹øÆ® ½ºÅ³ Æ÷ÀÎÆ® °»½Å
**
**		Auth: ÀÌ¿ëÁÖ
**		Date: 2015-03-10
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspUpdateServantSkillPoint]
	@pPcNo			INT
	,@pSerialNo		BIGINT
	,@pSkillPoint		SMALLINT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	UPDATE dbo.TblPcServant 
	SET mSkillPoint = @pSkillPoint
	WHERE mPcNo = @pPcNo
	  AND mSerialNo = @pSerialNo

	SET NOCOUNT OFF;

GO

