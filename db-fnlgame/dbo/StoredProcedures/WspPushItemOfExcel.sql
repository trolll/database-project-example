CREATE PROCEDURE [dbo].[WspPushItemOfExcel]
	@mUserId char(12),			-- 계정명	
	@mNm char(12),				-- 캐릭터명	

	@mItemNo INT,				-- 아이템 번호 (TID)
	@mItemName nvarchar(40),	-- 아이템 이름

	@mStatus TINYINT, 			-- 아이템 상태
	@mCnt INT, 					-- 아이템 수량
	@mIsConfirm BIT, 			-- 확인여부
	@mNo INT OUTPUT,			-- 캐릭터 번호
	@pValidDay INT = NULL 		-- 유효기간
AS
	DECLARE	@mUserNo INT,		-- 계정 번호
			@mConfirmItemNo INT,	-- 체크된
			@pCntUse TINYINT,	-- 사용 갯수	
			@pIsStack BIT		-- 스택 여부 


	SET NOCOUNT ON	

	------------------------------
	--임시 추가
	RETURN(0)
	------------------------------

	
	SELECT @mNo = 0, @mUserNo = 0, @pCntUse = 0, @pIsStack = 0

	---------------------------------------------------------	
	-- 아이템 존재여부 체크 
	---------------------------------------------------------	
	SELECT TOP 1 
		@pValidDay = 
		CASE 
			WHEN @pValidDay IS NULL THEN  ITermOfValidity
			ELSE @pValidDay
		END
		, @pCntUse = IUseNum
		, @pIsStack = IMaxStack			
	FROM FNLParm.dbo.DT_Item 
	-- FROM FNLParm.dbo.DT_Item WITH(NOLOCK) test
	WHERE IID = @mItemNo AND IName = RTRIM(@mItemName)
	IF @@ROWCOUNT <> 1
	BEGIN
		RETURN(30) 	-- 생성할 아이템이 존재하지 않거나, 일치 하지 않다.
	END	

	IF @pValidDay IS NULL OR @pCntUse IS NULL OR @pIsStack IS NULL
		RETURN(30) 	-- 생성할 아이템이 존재하지 않거나, 일치 하지 않다.

	---------------------------------------------------------	
	-- 계정 존재여부 체크 
	---------------------------------------------------------
	SELECT TOP 1 @mUserNo = mUserNo
	FROM FNLAccount.dbo.TblUser 
	-- FROM FNLAccount.dbo.TblUser WITH(NOLOCK) test 
	WHERE mUserId = @mUserId

	IF @@ROWCOUNT <> 1
	BEGIN
		RETURN(31) -- 계정이 존재하지 않다.
	END

	---------------------------------------------------------	
	-- 캐릭터 존재여부 체크 
	---------------------------------------------------------
	SELECT TOP 1 @mNo = mNo
	FROM dbo.TblPc  WITH(NOLOCK)
	WHERE mNm = @mNm AND mOwner = @mUserNo AND  mDelDate IS NULL
	IF @@ROWCOUNT <> 1
	BEGIN
		RETURN(32) -- 소유 형태가 틀리거나 캐릭터가 존재하지 않다.
	END	 	
	
	---------------------------------------------------------	
	--UspPushItem 프로시져를 이용해 아이템을 생성 
	---------------------------------------------------------
	EXECUTE dbo.UspPushItem @mNo, 0, @mItemNo, @pValidDay, @mCnt, @pCntUse, @mIsConfirm, @mStatus, @pIsStack
	RETURN(0)

GO

