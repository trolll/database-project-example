CREATE PROCEDURE [dbo].[WspUptItemBindingType]  	
	@mSerialNo	BIGINT, -- ЅГё®ѕу
	@mBindType TINYINT,	-- №ЩАОµщ ЕёАФ
	@mPos		CHAR(1)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @mPos = 'i' 
	BEGIN
		UPDATE dbo.TblPcInventory
		SET
			mBindingType = @mBindType
		WHERE mSerialNo = @mSerialNo
	END
	
	IF @mPos = 's' 
	BEGIN
		UPDATE dbo.TblPcStore
		SET
			mBindingType = @mBindType
		WHERE mSerialNo = @mSerialNo	
	END 
	
	IF @mPos = 'g' 
	BEGIN
		UPDATE dbo.TblGuildStore
		SET
			mBindingType = @mBindType
		WHERE mSerialNo = @mSerialNo	
	END 	

	IF @@ERROR <> 0
		RETURN 1
	
	RETURN 0

GO

