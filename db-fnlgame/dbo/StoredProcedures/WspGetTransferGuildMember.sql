CREATE PROCEDURE [dbo].[WspGetTransferGuildMember]
	@pGuildNo INT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT
		T2.mOLDPcNm
		, T2.mNm
		, T3.mStatus	-- 이동여부 체크 
		, T1.mGuildGrade
	FROM dbo.T_TblGuildMember T1
		INNER JOIN dbo.T_TblPc T2
			ON T1.mPcNo = T2.mNo
		LEFT JOIN dbo.T_TblTransferPc T3
			ON T1.mPcNo = T3.mNo		
	WHERE mGuildNo = @pGuildNo

GO

