/******************************************************************************
**		Name: UspAddPcCalendarGroupMember
**		Desc: ±×·ì¿¡ ¸â¹öÃß°¡
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-17
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspAddPcCalendarGroupMember]
	 @pOwnerPcNo		INT 
	,@pGroupNo			TINYINT
	,@pMemberPcNm		CHAR(12)
	,@pMaxCnt			INT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE  @aCnt			INT
			,@aMemberPcNo	INT

	SELECT	 @aCnt			= 0
			,@aMemberPcNo	= 0

	IF NOT EXISTS ( SELECT T1.mNo
					FROM dbo.TblPc AS T1
						INNER JOIN dbo.TblCalendarAgreement AS T2
							ON T1.mNo = T2.mMemberPcNo
					WHERE T1.mNm = @pMemberPcNm
						AND T1.mDelDate IS NULL	)
	BEGIN
		RETURN(2);
	END 

	SELECT @aMemberPcNo = mNo
	FROM dbo.TblPc
	WHERE mNm = @pMemberPcNm

	IF EXISTS ( SELECT  *
					FROM dbo.TblCalendarGroupMember
					WHERE mOwnerPcNo = @pOwnerPcNo 
						AND mGroupNo = @pGroupNo
						AND mMemberPcNo = @aMemberPcNo)
	BEGIN
		RETURN(3);
	END 

	SELECT @aCnt = count(*)
	FROM dbo.TblCalendarGroupMember
	WHERE mOwnerPcNo = @pOwnerPcNo
		AND mGroupNo = @pGroupNo

	IF @pMaxCnt <= @aCnt
	BEGIN
		RETURN(4);
	END

	DECLARE @aSerial		TABLE (
				mSerial		BIGINT	 PRIMARY KEY      	)			

	-- ÇØ´ç ±×·ì°ú ¿¬°áµÈ SerialNo¸¦ ±¸ÇÑ´Ù.
	INSERT INTO @aSerial(mSerial)
	SELECT mSerialNo
	FROM TblCalendarScheduleGroup
	WHERE mOwnerPcNo = @pOwnerPcNo AND mGroupNo = @pGroupNo
	
	--ÇÑ¸íÀÌ ¿©·¯ ±×·ì¿¡ ¼ÓÇØÀÖÀ» ¼ö ÀÖ´Ù. (20140611 ¼öÁ¤)
	--ÀÌ¹Ì µî·ÏµÈ ÀÏÁ¤ÀÌ¸é ¸®ÅÏÇÑ´Ù.
	IF EXISTS ( SELECT  *
					FROM TblCalendarPcSchedule
					WHERE mPcNo = @aMemberPcNo
						AND mSerialNo IN
							(SELECT mSerial	FROM @aSerial)
				)
	BEGIN
		RETURN(0);
	END 
	
	BEGIN TRAN
	
		INSERT INTO dbo.TblCalendarGroupMember(mRegDate, mOwnerPcNo, mGroupNo, mMemberPcNo)
		VALUES(GETDATE(), @pOwnerPcNo, @pGroupNo, @aMemberPcNo)

		IF (@@ERROR <> 0) OR (@@ROWCOUNT <= 0)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

		INSERT INTO dbo.TblCalendarPcSchedule(mRegDate, mPcNo, mSerialNo, mOwnerPcNo)
		SELECT GETDATE(), @aMemberPcNo, mSerial, @pOwnerPcNo
		FROM @aSerial

		IF (@@ERROR <> 0)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END

	COMMIT TRAN

	RETURN(0);

GO

