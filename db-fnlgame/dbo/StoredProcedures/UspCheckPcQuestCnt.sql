/******************************************************************************
**		Name: UspCheckPcQuestCnt
**		Desc: ÃÖ´ë ÁøÇà Äù½ºÆ® ¼ö¸¦ °¡Á®¿Â´Ù.
**
**		Auth: Á¤ÁøÈ£
**		Date: 2010-02-23
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		2013-03-21	Á¤ÁøÈ£				¸ÞÀÌÅ· Äù½ºÆ®´Â ÇØ´çµÇÁö ¾Êµµ·Ï ¼öÁ¤
**		2014-09-16	Á¤Áø¿í				Áö¿ª Äù½ºÆ®´Â ÇØ´çµÇÁö ¾Êµµ·Ï ¼öÁ¤
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspCheckPcQuestCnt]
	 @pPcNo					INT 
	,@pStartQMakingNo		INT			-- Äù½ºÆ® ¸ÞÀÌÅ· ½ÃÀÛ¹øÈ£
	,@pEndQMakingNo			INT			-- Äù½ºÆ® ¸ÞÀÌÅ· ³¡¹øÈ£
	,@pStartRegionQNo		INT			-- Áö¿ª Äù½ºÆ® ½ÃÀÛ¹øÈ£
	,@pEndRegionQNo			INT			-- Áö¿ª Äù½ºÆ® ³¡¹øÈ£
AS
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		COUNT(*) AS mCnt
    FROM dbo.TblPcQuest 
    WHERE mPcNo = @pPcNo 
		AND mValue < 99
		AND mQuestNo NOT BETWEEN @pStartQMakingNo AND @pEndQMakingNo
		AND mQuestNo NOT BETWEEN @pStartRegionQNo AND @pEndRegionQNo

GO

