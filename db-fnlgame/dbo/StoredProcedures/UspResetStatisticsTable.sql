/****** Object:  Stored Procedure dbo.UspResetStatisticsTable    Script Date: 2011-4-19 15:24:36 ******/

/****** Object:  Stored Procedure dbo.UspResetStatisticsTable    Script Date: 2011-3-17 14:50:03 ******/

/****** Object:  Stored Procedure dbo.UspResetStatisticsTable    Script Date: 2011-3-4 11:36:43 ******/

/****** Object:  Stored Procedure dbo.UspResetStatisticsTable    Script Date: 2010-12-23 17:46:00 ******/

/****** Object:  Stored Procedure dbo.UspResetStatisticsTable    Script Date: 2010-3-22 15:58:18 ******/

/****** Object:  Stored Procedure dbo.UspResetStatisticsTable    Script Date: 2009-12-14 11:35:27 ******/

/****** Object:  Stored Procedure dbo.UspResetStatisticsTable    Script Date: 2009-11-16 10:23:26 ******/
/******************************************************************************
**		Name: dbo.UspResetStatisticsTable
**		Desc: ?? ??? ???
**
**		Auth: ? ??
**		Date: 2007-07-02
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**		20080827	JUDY				??? ? ?? ?? ???? ???  
*******************************************************************************/
CREATE Procedure [dbo].[UspResetStatisticsTable]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	DELETE DBO.TblStatisticsItemByCase WITH(TABLOCK)
	DELETE DBO.TblStatisticsMonsterHunting WITH(TABLOCK)
	DELETE DBO.TblStatisticsItemByMonster WITH(TABLOCK)
	DELETE DBO.TblStatisticsItemByLevelClass WITH(TABLOCK)
	DELETE DBO.TblStatisticsPShopExchange WITH(TABLOCK)

GO

