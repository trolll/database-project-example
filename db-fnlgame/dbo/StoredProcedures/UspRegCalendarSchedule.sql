/******************************************************************************
**		Name: UspRegCalendarSchedule
**		Desc: ÀÏÁ¤ µî·Ï
**
**		Auth: Á¤ÁøÈ£
**		Date: 2014-03-14
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspRegCalendarSchedule]
	 @pSerialNo			BIGINT
	,@pOwnerPcNo		INT 
	,@pTitle			CHAR(30)
	,@pMsg				VARCHAR(512)
	,@pScheduleDate		SMALLDATETIME
	,@pGroup			VARCHAR(300)	-- ±×·ì¹øÈ£ ¸ñ·Ï
	,@pRvSerialNo		BIGINT OUTPUT
AS
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE		 @aErr			INT
				,@aRowCnt		INT
				,@aSerialNo		BIGINT
				,@aScheduleDate SMALLDATETIME
				,@aCnt			INT
				,@aMemCnt		INT

	SELECT 		 @aErr = 0
				,@aRowCnt = 0
				,@aSerialNo = 0 
				,@aScheduleDate = GETDATE()
				,@aCnt = 0
				,@aMemCnt = 0


	DECLARE @aGroup			TABLE (
				mGroup		TINYINT	 PRIMARY KEY    )			

	DECLARE @aMember		TABLE (
				mMember		INT	 PRIMARY KEY      	)			


	INSERT INTO @aGroup(mGroup)
	SELECT CONVERT(TINYINT, element) mObjID
	FROM  dbo.fn_SplitTSQL(  @pGroup, ',' )
	WHERE LEN(RTRIM(element)) > 0

	SELECT @aCnt = COUNT(*)
	FROM @aGroup
	WHERE mGroup <> 0


	-- Áö³­ ÀÏÁ¤°ú 3°³¿ù µÚÀÇ ÀÏÁ¤Àº ¼öÁ¤ ¹× µî·Ï ºÒ°¡ (³¯Â¥´Â »ó°ü¾øÀ½ ¿ù´ÜÀ§)
	IF @pScheduleDate < GETDATE()
	BEGIN
		RETURN(2);
	END

	IF 3 <= DATEDIFF(MM, GETDATE(), @pScheduleDate)
	BEGIN
		RETURN(3);
	END

	BEGIN TRAN

		UPDATE dbo.TblCalendarSchedule
		SET	
			 mRegDate		= GETDATE()
			,mTitle			= @pTitle
			,mMsg			= @pMsg
			,mScheduleDate	= @pScheduleDate
		WHERE mSerialNo = @pSerialNo AND mOwnerPcNo = @pOwnerPcNo

		SELECT  @aErr = @@ERROR, @aRowCnt = @@ROWCOUNT;
		IF (@aErr <> 0)
		BEGIN
			ROLLBACK TRAN;
			RETURN(1);
		END
		
		IF @aRowCnt = 0
		BEGIN
			EXEC @aSerialNo =  dbo.UspGetItemSerial 	-- ½Ã¸®¾ó ¾ò±â 
			IF @aSerialNo <= 0
			BEGIN
				ROLLBACK TRAN;
				RETURN(1);
			END	

			INSERT INTO dbo.TblCalendarSchedule(mRegDate, mOwnerPcNo, mSerialNo, mTitle, mMsg, mScheduleDate)
			VALUES(GETDATE(), @pOwnerPcNo, @aSerialNo, @pTitle, @pMsg, @pScheduleDate)

			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF (@aErr <> 0) OR (@aRowCnt <= 0)
			BEGIN
				ROLLBACK TRAN;		
				RETURN(1);
			END
			ELSE
			BEGIN
				SELECT @pRvSerialNo = @aSerialNo

				INSERT INTO dbo.TblCalendarPcSchedule(mRegDate, mPcNo, mSerialNo, mOwnerPcNo)
				VALUES(GETDATE(), @pOwnerPcNo, @aSerialNo, @pOwnerPcNo)

				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF (@aErr <> 0) OR (@aRowCnt <= 0)
				BEGIN
					ROLLBACK TRAN;
					RETURN(1);
				END	

				IF @aCnt > 0
				BEGIN
					INSERT INTO dbo.TblCalendarScheduleGroup(mRegDate, mOwnerPcNo, mSerialNo, mGroupNo)
					SELECT GETDATE(), @pOwnerPcNo, @aSerialNo, mGroup
					FROM @aGroup

					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF (@aErr <> 0) OR (@aRowCnt <= 0)
					BEGIN
						ROLLBACK TRAN;
						RETURN(1);
					END

					INSERT INTO @aMember(mMember)
					SELECT DISTINCT T1.mMemberPcNo
					FROM dbo.TblCalendarGroupMember AS T1
						 INNER JOIN dbo.TblPc AS T2 
								ON T1.mMemberPcNo = T2.mNo
					WHERE mOwnerPcNo = @pOwnerPcNo 
						AND mGroupNo IN (
										SELECT *
										FROM @aGroup
										)

					SELECT @aMemCnt = COUNT(*)
					FROM @aMember	

					IF @aMemCnt > 0
					BEGIN
						INSERT INTO dbo.TblCalendarPcSchedule(mRegDate, mPcNo, mSerialNo, mOwnerPcNo)
						SELECT GETDATE(), mMember, @aSerialNo, @pOwnerPcNo
						FROM @aMember

						SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
						IF (@aErr <> 0) OR (@aRowCnt <= 0)
						BEGIN
							ROLLBACK TRAN;
							RETURN(1);
						END
					END										
				END				
			END
		END
		ELSE
		BEGIN
			-- µî·ÏµÈ ±×·ì »èÁ¦
			DELETE dbo.TblCalendarScheduleGroup
			WHERE mSerialNo = @pSerialNo AND mOwnerPcNo = @pOwnerPcNo

			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN;
				RETURN(1);			
			END 

			-- µî·ÏµÈ À¯Àú »èÁ¦
			DELETE dbo.TblCalendarPcSchedule
			WHERE mSerialNo = @pSerialNo AND mOwnerPcNo = @pOwnerPcNo

			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRAN;
				RETURN(1);			
			END 

			SELECT @pRvSerialNo = @pSerialNo

			INSERT INTO dbo.TblCalendarPcSchedule(mRegDate, mPcNo, mSerialNo, mOwnerPcNo)
			VALUES(GETDATE(), @pOwnerPcNo, @pSerialNo, @pOwnerPcNo)

			SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
			IF (@aErr <> 0) OR (@aRowCnt <= 0)
			BEGIN
				ROLLBACK TRAN;
				RETURN(1);
			END	

			IF @aCnt > 0
			BEGIN
				INSERT INTO dbo.TblCalendarScheduleGroup(mRegDate, mOwnerPcNo, mSerialNo, mGroupNo)
				SELECT GETDATE(), @pOwnerPcNo, @pSerialNo, mGroup
				FROM @aGroup

				SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
				IF (@aErr <> 0) OR (@aRowCnt <= 0)
				BEGIN
					ROLLBACK TRAN;
					RETURN(1);
				END

				INSERT INTO @aMember(mMember)
				SELECT DISTINCT T1.mMemberPcNo
				FROM dbo.TblCalendarGroupMember AS T1
					 INNER JOIN dbo.TblPc AS T2 
							ON T1.mMemberPcNo = T2.mNo
				WHERE mOwnerPcNo = @pOwnerPcNo 
					AND mGroupNo IN (
									SELECT *
									FROM @aGroup
									)

				SELECT @aMemCnt = COUNT(*)
				FROM @aMember	

				IF @aMemCnt > 0
				BEGIN
					INSERT INTO dbo.TblCalendarPcSchedule(mRegDate, mPcNo, mSerialNo, mOwnerPcNo)
					SELECT GETDATE(), mMember, @pSerialNo, @pOwnerPcNo
					FROM @aMember

					SELECT @aErr = @@ERROR,  @aRowCnt = @@ROWCOUNT
					IF (@aErr <> 0) OR (@aRowCnt <= 0)
					BEGIN
						ROLLBACK TRAN;
						RETURN(1);
					END
				END
			END			
		END

	COMMIT TRAN	

	RETURN(0);

GO

