/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2011-4-19 15:24:41 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2011-3-17 14:50:08 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2011-3-4 11:36:48 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2010-12-23 17:46:05 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2010-3-22 15:58:23 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2009-12-14 11:35:31 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2009-11-16 10:23:31 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2009-7-14 13:13:33 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2009-6-1 15:32:41 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2009-5-12 9:18:20 ******/

/****** Object:  User Defined Function dbo.UfnGetEndDateByMin    Script Date: 2008-11-10 10:37:21 ******/





CREATE FUNCTION [dbo].[UfnGetEndDateByMin]
(
	 @pGetDate	DATETIME	-- ?????? ????? GETDATE()? ??? ? ??.
	,@pValidMin	INT
) RETURNS DATETIME
--------------WITH ENCRYPTION
AS
BEGIN
	DECLARE @aDate	DATETIME
	SET @aDate = DATEADD(mi, @pValidMin, @pGetDate)
	RETURN @aDate
END

GO

