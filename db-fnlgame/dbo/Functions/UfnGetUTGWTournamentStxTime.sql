/******************************************************************************
**		File: 
**		Name: UfnGetUTGWTournamentStxTime
**		Desc: 토너먼트 시작 시간을 계산한다.
**
**		Auth: 김 광섭
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**    	2010-12-06	김광섭				월 주기의 의미가 변경됨. (기존에는 1이 이번달이지만 변경 후 0 이 이번달임)
*******************************************************************************/
CREATE FUNCTION [dbo].[UfnGetUTGWTournamentStxTime]
(
	  @pDate			DATETIME
	, @pPeriodMonth		INT
	, @pPeriodWeek		INT
	, @pDayOfTheWeek	INT
) RETURNS DATETIME
AS
BEGIN
	/**
		Warn.	해당 함수에서는 @pDayOfTheWeek 와 SQL 내부의 요일 순서가 동일해야한다.
				현재 게임서버에서는 일요일부터 시작한다. 하지만 SQL 은 내부적으로 설정에 따라 변경이 된다.
				반드시 해당 함수를 호출하는 SP 에서는 "SET DATEFIRST 7" 을 호출해서 내부적으로 일요일부터 요일이 시작되게 변경해야한다.
				
				또한, 게임서버는 요일이 0부터 시작이지만 SQL 서버는 1부터 시작한다. 이를 맞춰서 해당 함수를 호출해야한다.
				해당 함수에서는 @pDayOfTheWeek 인자가 1 부터 7까지의 범위를 갖고있음을 가정한다.
	*/

	DECLARE	@aRvDate			DATETIME;
	DECLARE	@aRvDayOfTheWeek	INT;
	
	/**
		1. 현재 날짜에서 현재 일수를 뺀다. (1월 10일 경우 10일을 뺀다.)
			=> 지난달의 마지막 날로 설정된다.
		2. 해당 값에 하루를 더한다.
			=> 현재 달의 1일로 설정된다.
	*/
	SET	@aRvDate = DATEADD(dd, (DAY(@pDate) * -1) + 1, @pDate);
	
	-- 20101206(kslive) : 반복주기의 의미가 1 : 현재달에서, 0 : 현재달로 변경한다. 그러므로 DATAADD 에서 @pPeriodMonth 에 가감없이 그대로 사용한다.
	-- 반복주기의 달을 더한다. (1은 현재 달이기 때문에 @pPeriodMonth 에서 1을 뺀만큼 더한다.
	SET	@aRvDate = DATEADD(mm, @pPeriodMonth, @aRvDate);
	
	-- 반복주기의 주를 더한다. (1은 첫주이기 때문에 @pPeriodWeek 에서 1을 뺀만큼 더한다.
	SET	@aRvDate = DATEADD(wk, @pPeriodWeek - 1, @aRvDate);
	
	-- 반복 요일과 현재 구한 요일이 다르면 해당 요일의 날짜로 이동한다.
	SET	@aRvDayOfTheWeek = DATEPART(dw, @aRvDate);
	IF(@aRvDayOfTheWeek <> @pDayOfTheWeek)
	 BEGIN
		IF(@aRvDayOfTheWeek < @pDayOfTheWeek)
		 BEGIN
			-- 반복 요일이 현재 구한 날짜의 요일보다 큰 경우
			SET @aRvDate = DATEADD(dd, @pDayOfTheWeek - @aRvDayOfTheWeek, @aRvDate);
		 END
		ELSE
		 BEGIN
			-- 반복 요일이 현재 구한 날짜의 요일보다 작은 경우 
			SET @aRvDate = DATEADD(dd, -(@aRvDayOfTheWeek - @pDayOfTheWeek), @aRvDate);
		 END
	 END
	
	RETURN(@aRvDate);
END

GO

