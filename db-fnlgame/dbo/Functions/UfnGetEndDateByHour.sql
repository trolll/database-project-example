
CREATE FUNCTION [dbo].[UfnGetEndDateByHour]
(
	 @pGetDate		DATETIME	
	,@pValidHour	INT	-- MAX 30 * 24
) RETURNS DATETIME
AS
BEGIN
	DECLARE @aDate	DATETIME

	IF(10000 <= @pValidHour)
	BEGIN
		SET	@aDate = '2079-01-01'	-- 扁粮俊绰 '2020-01-01'
	END
	ELSE
	BEGIN
		SET @aDate = DATEADD(HH, @pValidHour, @pGetDate)
		SET @aDate = DATEADD(MI, -1, @aDate)	
	END

	RETURN @aDate
END

GO

