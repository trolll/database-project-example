/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2011-4-19 15:24:41 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2011-3-17 14:50:08 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2011-3-4 11:36:48 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2010-12-23 17:46:05 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2010-3-22 15:58:23 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2009-12-14 11:35:31 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2009-11-16 10:23:31 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2009-7-14 13:13:33 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2009-6-1 15:32:41 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2009-5-12 9:18:20 ******/

/****** Object:  User Defined Function dbo.UfnGetEquipCol    Script Date: 2008-11-10 10:37:21 ******/





CREATE FUNCTION [dbo].[UfnGetEquipCol]
(
	 @pSlot		INT		-- CPcEquip::ePosMax.
) RETURNS NVARCHAR(20)
--------------WITH ENCRYPTION
AS
BEGIN
	DECLARE	@aCol		NVARCHAR(20)
	IF(0 = @pSlot)
		SET @aCol	= N'[mWeapon]'
	ELSE IF(1 = @pSlot)
		SET @aCol	= N'[mShield]'
	ELSE IF(2 = @pSlot)
		SET @aCol	= N'[mArmor]'
	ELSE IF(3 = @pSlot)
		SET @aCol	= N'[mRing1]'
	ELSE IF(4 = @pSlot)
		SET @aCol	= N'[mRing2]'
	ELSE IF(5 = @pSlot)
		SET @aCol	= N'[mAmulet]'
	ELSE IF(6 = @pSlot)
		SET @aCol	= N'[mBoot]'
	ELSE IF(7 = @pSlot)
		SET @aCol	= N'[mGlove]'
	ELSE IF(8 = @pSlot)
		SET @aCol	= N'[mCap]'
	ELSE IF(9 = @pSlot)
		SET @aCol	= N'[mBelt]'
	ELSE IF(10 = @pSlot)
		SET @aCol	= N'[mCloak]'
	ELSE
		SET @aCol	= NULL
	 
	RETURN @aCol
END

GO

