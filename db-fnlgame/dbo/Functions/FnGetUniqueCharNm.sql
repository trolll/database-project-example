

--이름 변경했는데, 또 중복 되는게 있으면,.
CREATE FUNCTION dbo.FnGetUniqueCharNm
(
	@aNm	VARCHAR(12),	--'jinsunjinsun@3' > jinsunjinsun
	@aNo	INT,
	@aSvrNo	SMALLINT
)
RETURNS CHAR(12)
AS
BEGIN

	DECLARE @aString VARCHAR(36) ,@aLen INT ,@aPos	INT	,@aNewNm VARCHAR(12)
			,@aAppendStrLen SMALLINT,@aStrPos INT, @aPosS INT
			
	SELECT @aString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
			,@aLen = LEN(@aNm)
			,@aPos = 0
			,@aNewNm = NULL
			,@aAppendStrLen = 1 
			,@aStrPos = 0
			,@aPosS = 0 

	IF DATALENGTH(LTRIM(RTRIM(@aNm))) > (12-@aAppendStrLen) 
	BEGIN
		SET @aPos = 10	
		WHILE(1=1)
		BEGIN			
			IF DATALENGTH(SUBSTRING(@aNm, LEN(@aNm)-@aStrPos, @aStrPos+1)) >= @aAppendStrLen 
			BEGIN
				SET @aPos = LEN(@aNm) - (@aStrPos+1)
				BREAK
			END 
			SET @aStrPos =  @aStrPos + 1	

			IF @aStrPos > 5 
				BREAK
		END	
	END 
	ELSE
		SET @aPos = LEN(@aNm)	
	
	SET @aPosS = CHARINDEX(@aString, SUBSTRING(@aNm,  LEN(@aNm) - 1, 1 ))	
	SET @aPosS = @aPosS + 1
	SET @aNm = SUBSTRING(@aNm, 1, @aPos )	
	
	WHILE( @aPosS <= LEN(@aString) ) 
	BEGIN		
		-- 제한 문자열에 도달 했다.
		IF LEN(@aString) <= @aPosS	--더 이상 교체할게 없다.
		BEGIN
			SET @aNm = SUBSTRING(@aNm, 1, @aPos-2 ) + '@'	
			SET @aPosS = 1
			-- 2017.10.30 신규 추가 부분
			SET @aPos = @aPos - 1

			IF @aPos <= 1
				break;
			-----------------------------
		END 

		SET @aNewNm =  @aNm + SUBSTRING(@aString, @aPosS, 1) 
		
		IF NOT EXISTS( 
			SELECT T1.mNm
			FROM dbo.T_TblPc T1 
				INNER JOIN dbo.T_TblTransferPc T2 
					ON T1.mNo = T2.mNO		
			WHERE T1.mNm = @aNewNm
		)
		BEGIN
			BREAK
		END 
	
		-- 바꿨는데도 존재한다면
		SET @aPosS = @aPosS + 1 
	END
	
	RETURN @aNewNm
END

GO

