CREATE TABLE [dbo].[TblGuildStorePasswordHistory] (
    [mId]       INT       IDENTITY (1, 1) NOT NULL,
    [mRegDate]  DATETIME  CONSTRAINT [DF__TblGuildS__mRegD__73DA2C14] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]  INT       NOT NULL,
    [mOperator] CHAR (20) NOT NULL,
    CONSTRAINT [PK_TblGuildStorePasswordHistory] PRIMARY KEY NONCLUSTERED ([mId] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [IxTblGuildStoremGuildNo]
    ON [dbo].[TblGuildStorePasswordHistory]([mGuildNo] ASC) WITH (FILLFACTOR = 80);


GO

