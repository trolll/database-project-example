CREATE TABLE [dbo].[TblRacingResult] (
    [mPlace]     INT           NOT NULL,
    [mStage]     INT           NOT NULL,
    [mWinnerNID] INT           NOT NULL,
    [mDividend]  FLOAT (53)    NOT NULL,
    [mRegDate]   SMALLDATETIME CONSTRAINT [DF_TblRacingResult_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblRacingResult] PRIMARY KEY CLUSTERED ([mPlace] ASC, [mStage] ASC) WITH (FILLFACTOR = 80)
);


GO

