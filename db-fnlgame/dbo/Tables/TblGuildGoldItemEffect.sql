CREATE TABLE [dbo].[TblGuildGoldItemEffect] (
    [mRegDate]  DATETIME      CONSTRAINT [DF__TblGuildG__mRegD__7834CCDD] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]  INT           NOT NULL,
    [mItemType] INT           NOT NULL,
    [mParmA]    FLOAT (53)    CONSTRAINT [DF__TblGuildG__mParm__7928F116] DEFAULT ((0)) NOT NULL,
    [mEndDate]  SMALLDATETIME NOT NULL,
    [mItemNo]   INT           CONSTRAINT [DF_TblGuildGoldItemEffect_mItemNo] DEFAULT ((409)) NOT NULL,
    CONSTRAINT [CL_PK_TblGuildGoldItemEffect] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mItemType] ASC) WITH (FILLFACTOR = 80)
);


GO

