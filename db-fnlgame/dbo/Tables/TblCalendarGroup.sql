CREATE TABLE [dbo].[TblCalendarGroup] (
    [mRegDate]   SMALLDATETIME NOT NULL,
    [mOwnerPcNo] INT           NOT NULL,
    [mGroupNo]   TINYINT       NOT NULL,
    [mGroupNm]   VARCHAR (12)  NOT NULL,
    CONSTRAINT [UCL_PK_TblCalendarGroup] PRIMARY KEY CLUSTERED ([mOwnerPcNo] ASC, [mGroupNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'±×·ìÀÌ¸§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroup', @level2type = N'COLUMN', @level2name = N'mGroupNm';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'´Þ·Â±×·ì', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroup';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'±×·ì¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroup', @level2type = N'COLUMN', @level2name = N'mGroupNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä³¸¯ÅÍ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroup', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'µî·ÏÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroup', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

