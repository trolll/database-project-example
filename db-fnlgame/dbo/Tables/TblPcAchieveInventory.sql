CREATE TABLE [dbo].[TblPcAchieveInventory] (
    [mRegDate]        DATETIME CONSTRAINT [DF_TblPcAchieveInventory_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]       BIGINT   CONSTRAINT [DF_TblPcAchieveInventory_mSerialNo] DEFAULT ((0)) NOT NULL,
    [mPcNo]           INT      CONSTRAINT [DF_TblPcAchieveInventory_mPcNo] DEFAULT ((0)) NOT NULL,
    [mItemNo]         INT      CONSTRAINT [DF_TblPcAchieveInventory_mItemNo] DEFAULT ((0)) NOT NULL,
    [mAchieveGuildID] INT      CONSTRAINT [DF_TblPcAchieveInventory_mAchieveGuildID] DEFAULT ((0)) NOT NULL,
    [mCoinPoint]      INT      CONSTRAINT [DF_TblPcAchieveInventory_mCoinPoint] DEFAULT ((0)) NOT NULL,
    [mSlotNo]         TINYINT  CONSTRAINT [DF_TblPcAchieveInventory_mSlotNo] DEFAULT ((0)) NOT NULL,
    [mAchieveID]      TINYINT  CONSTRAINT [DF_TblPcAchieveInventory_mAchieveID] DEFAULT ((0)) NOT NULL,
    [mExp]            INT      CONSTRAINT [DF_TblPcAchieveInventory_mExp] DEFAULT ((0)) NOT NULL,
    [mLimitLevel]     TINYINT  CONSTRAINT [DF_TblPcAchieveInventory_mLimitLevel] DEFAULT ((0)) NOT NULL,
    [mIsSeizure]      BIT      CONSTRAINT [DF_TblPcAchieveInventory_mIsSeizure] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblPcAchieveInventory] PRIMARY KEY CLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 80)
);


GO

