CREATE TABLE [dbo].[TblRacingTicket] (
    [mSerialNo]  BIGINT NOT NULL,
    [mPlace]     INT    NOT NULL,
    [mStage]     INT    NOT NULL,
    [mNID]       INT    NOT NULL,
    [mTicketCnt] INT    CONSTRAINT [DF_TblRacingTicket_mTicketCnt] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_TblRacingTicket] PRIMARY KEY CLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 80)
);


GO

