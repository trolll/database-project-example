CREATE TABLE [dbo].[TblMacroContentVersion] (
    [mPcNo]    INT NOT NULL,
    [mVersion] INT NOT NULL,
    CONSTRAINT [UCL_PK_TblMacroContentVersion] PRIMARY KEY CLUSTERED ([mPcNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î ¹öÀü', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroContentVersion', @level2type = N'COLUMN', @level2name = N'mVersion';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î ¼ÒÀ¯ Ä³¸¯ÅÍ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroContentVersion', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

