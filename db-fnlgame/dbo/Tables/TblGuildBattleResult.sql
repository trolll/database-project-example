CREATE TABLE [dbo].[TblGuildBattleResult] (
    [mGuildNo] INT CONSTRAINT [DF_TblGuildBattleResult_mGuildNo] DEFAULT ((0)) NOT NULL,
    [mWinCnt]  INT CONSTRAINT [DF_TblGuildBattleResult_mWinCnt] DEFAULT ((0)) NOT NULL,
    [mLoseCnt] INT CONSTRAINT [DF_TblGuildBattleResult_mLoseCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblGuildBattleResult] PRIMARY KEY CLUSTERED ([mGuildNo] ASC) WITH (FILLFACTOR = 80)
);


GO

