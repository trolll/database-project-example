CREATE TABLE [dbo].[TblGuildBattleHistory] (
    [mGuildNo1]      INT      NOT NULL,
    [mGuildNo2]      INT      NOT NULL,
    [mStxDate]       DATETIME NOT NULL,
    [mEtxDate]       DATETIME NOT NULL,
    [mWinnerGuildNo] INT      NOT NULL
);


GO

CREATE NONCLUSTERED INDEX [IxTblGuildBattleHistory]
    ON [dbo].[TblGuildBattleHistory]([mGuildNo1] ASC, [mGuildNo2] ASC) WITH (FILLFACTOR = 80);


GO

