CREATE TABLE [dbo].[TblPcQuest] (
    [mPcNo]      INT           NOT NULL,
    [mQuestNo]   INT           NOT NULL,
    [mValue]     INT           NOT NULL,
    [mBeginDate] SMALLDATETIME CONSTRAINT [DF_TblPcQuest_mBeginDate] DEFAULT (getdate()) NULL,
    [mEndDate]   SMALLDATETIME NULL,
    [mLimitTime] SMALLDATETIME NULL,
    CONSTRAINT [PK_CL_TblPcQuest] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mQuestNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblPcQuest_mQuestNo]
    ON [dbo].[TblPcQuest]([mQuestNo] ASC, [mValue] ASC, [mPcNo] ASC) WITH (FILLFACTOR = 80);


GO

