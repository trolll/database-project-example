CREATE TABLE [dbo].[TblQuickSupplication] (
    [mQSID]         BIGINT         NOT NULL,
    [mSupplication] VARCHAR (1000) NOT NULL,
    [mPosX]         FLOAT (53)     NOT NULL,
    [mPosY]         FLOAT (53)     NOT NULL,
    [mPosZ]         FLOAT (53)     NOT NULL,
    [mIp]           CHAR (15)      NOT NULL,
    CONSTRAINT [PK_CL_TblQuickSupplication] PRIMARY KEY CLUSTERED ([mQSID] ASC) WITH (FILLFACTOR = 80)
);


GO

