CREATE TABLE [dbo].[TblGuildAccount] (
    [mGID]        INT           NOT NULL,
    [mGuildMoney] BIGINT        CONSTRAINT [DF_TblGuildAccount_mMoney] DEFAULT ((0)) NOT NULL,
    [mRegDate]    SMALLDATETIME CONSTRAINT [DF_TblGuildAccount_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblGuildAccount] PRIMARY KEY CLUSTERED ([mGID] ASC) WITH (FILLFACTOR = 80)
);


GO

