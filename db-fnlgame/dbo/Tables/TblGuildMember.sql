CREATE TABLE [dbo].[TblGuildMember] (
    [mRegDate]    DATETIME  CONSTRAINT [DF__TblGuildM__mRegD__07420643] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]    INT       NOT NULL,
    [mPcNo]       INT       NOT NULL,
    [mGuildGrade] TINYINT   NOT NULL,
    [mNickNm]     CHAR (16) NULL,
    [mEquipPoint] INT       CONSTRAINT [DF_TblGuildMember_mEquipPoint] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblGuildMember] PRIMARY KEY NONCLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblGuildMemberTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo]),
    CONSTRAINT [FkTblGuildMemberTblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO

CREATE CLUSTERED INDEX [IX_TblGuildMember]
    ON [dbo].[TblGuildMember]([mGuildNo] ASC) WITH (FILLFACTOR = 80);


GO

