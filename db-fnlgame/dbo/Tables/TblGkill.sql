CREATE TABLE [dbo].[TblGkill] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF__TblGkill__mRegDa__438CC5CB] DEFAULT (getdate()) NOT NULL,
    [mGuildNo] INT           NOT NULL,
    [mPlace]   INT           NOT NULL,
    [mNode]    INT           NOT NULL,
    [mPcNo]    INT           NOT NULL,
    CONSTRAINT [PK_TblGkill] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mPlace] ASC, [mNode] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblGkillPcNo]
    ON [dbo].[TblGkill]([mPcNo] ASC) WITH (FILLFACTOR = 80);


GO

