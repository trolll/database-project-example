CREATE TABLE [dbo].[TblSiegeGambleState] (
    [mRegDate]      DATETIME CONSTRAINT [DF__TblSiegeG__mRegD__61722E6D] DEFAULT (getdate()) NOT NULL,
    [mTerritory]    INT      NOT NULL,
    [mNo]           INT      NOT NULL,
    [mIsFinish]     BIT      NOT NULL,
    [mTotalMoney]   INT      NOT NULL,
    [mDividend]     INT      NOT NULL,
    [mOccupyGuild0] INT      CONSTRAINT [DF__TblSiegeG__mOccu__6542BF51] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild1] INT      CONSTRAINT [DF__TblSiegeG__mOccu__6636E38A] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild2] INT      CONSTRAINT [DF__TblSiegeG__mOccu__672B07C3] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild3] INT      CONSTRAINT [DF__TblSiegeG__mOccu__681F2BFC] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild4] INT      CONSTRAINT [DF__TblSiegeG__mOccu__69135035] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild5] INT      CONSTRAINT [DF__TblSiegeG__mOccu__6A07746E] DEFAULT ((0)) NOT NULL,
    [mOccupyGuild6] INT      CONSTRAINT [DF__TblSiegeG__mOccu__6AFB98A7] DEFAULT ((0)) NOT NULL,
    [mIsKeep]       INT      CONSTRAINT [DF__TblSiegeG__mIsKe__6BEFBCE0] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblSiegeGambleState] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mNo] ASC) WITH (FILLFACTOR = 80),
    CHECK ((0)<[mTerritory] AND [mTerritory]<(5)),
    CHECK ((0)<[mTerritory] AND [mTerritory]<(5)),
    CHECK ((0)<=[mDividend]),
    CHECK ((0)<=[mDividend]),
    CHECK ((0)<=[mTotalMoney]),
    CHECK ((0)<=[mTotalMoney])
);


GO

