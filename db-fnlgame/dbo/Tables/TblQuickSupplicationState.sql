CREATE TABLE [dbo].[TblQuickSupplicationState] (
    [mRegDate]  DATETIME CONSTRAINT [DF__TblQuickS__mRegD__4341E1B1] DEFAULT (getdate()) NOT NULL,
    [mQSID]     BIGINT   IDENTITY (1, 1) NOT NULL,
    [mPcNo]     INT      NOT NULL,
    [mCategory] TINYINT  NOT NULL,
    [mStatus]   TINYINT  CONSTRAINT [DF__TblQuickS__mStat__443605EA] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_NC_TblQuickSupplicationState] PRIMARY KEY NONCLUSTERED ([mQSID] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE CLUSTERED INDEX [CL_TblQuickSupplicationState_mPcNo]
    ON [dbo].[TblQuickSupplicationState]([mPcNo] ASC, [mStatus] ASC) WITH (FILLFACTOR = 80);


GO

