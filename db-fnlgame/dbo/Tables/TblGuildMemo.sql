CREATE TABLE [dbo].[TblGuildMemo] (
    [mMemoNo]       INT           IDENTITY (1, 1) NOT NULL,
    [mGuildNo]      INT           NOT NULL,
    [mManageUserNo] VARCHAR (20)  NULL,
    [mMemoText]     VARCHAR (100) NOT NULL,
    [mRegDate]      DATETIME      CONSTRAINT [DF__TblGuildM__mRegD__76B698BF] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblGuildMemo] PRIMARY KEY NONCLUSTERED ([mMemoNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [IxtblpcmGuildNo]
    ON [dbo].[TblGuildMemo]([mGuildNo] ASC) WITH (FILLFACTOR = 80);


GO

