CREATE TABLE [dbo].[TblGuildRecruitLimitValue] (
    [mGuildNo]  INT      NOT NULL,
    [mClass]    TINYINT  NOT NULL,
    [mMinLevel] SMALLINT NOT NULL,
    [mMinChao]  SMALLINT NOT NULL,
    CONSTRAINT [UCL_TblGuildRecruitLimitValue_UnitedKey] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mClass] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_TblGuildRecruitLimitValue_mClass] CHECK ((0)<=[mClass] AND [mClass]<=(4)),
    CONSTRAINT [CK_TblGuildRecruitLimitValue_mMinChao] CHECK ((-30000)<=[mMinChao] AND [mMinChao]<=(30000)),
    CONSTRAINT [CK_TblGuildRecruitLimitValue_mMinLevel] CHECK ((1)<=[mMinLevel] AND [mMinLevel]<=(200))
);


GO

