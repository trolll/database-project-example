CREATE TABLE [dbo].[TblGuild] (
    [mRegDate]           DATETIME      CONSTRAINT [DF__TblGuild__mRegDa__04659998] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]           INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mGuildSeqNo]        INT           CONSTRAINT [DF_TblGuild_mGuildSeqNo] DEFAULT ((0)) NOT NULL,
    [mGuildNm]           CHAR (12)     NOT NULL,
    [mGuildMsg]          CHAR (60)     NULL,
    [mGuildMark]         BINARY (1784) NULL,
    [mRewardExp]         INT           CONSTRAINT [DF_TblGuild_mRewardExp] DEFAULT ((0)) NOT NULL,
    [mGuildMarkUptDate]  SMALLDATETIME CONSTRAINT [DF_TblGuild_mUptDate] DEFAULT (getdate()) NOT NULL,
    [mCastleDfnsLv]      SMALLINT      CONSTRAINT [DF_TblGuild_mCastleDfnsLv] DEFAULT ((0)) NOT NULL,
    [mSpotDfnsLv]        SMALLINT      CONSTRAINT [DF_TblGuild_mSpotDfnsLv] DEFAULT ((0)) NOT NULL,
    [mMandateSubMaster]  BIT           CONSTRAINT [DF_TblGuild_mMandateSubMaster] DEFAULT ((0)) NOT NULL,
    [mSkillTreePoint]    SMALLINT      CONSTRAINT [DF_TblGuild_mSkillTreePoint] DEFAULT ((0)) NOT NULL,
    [mEtelrCastleDfnsLv] SMALLINT      CONSTRAINT [DF_TblGuild_mEtelrCastleDfnsLv] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblGuild] PRIMARY KEY CLUSTERED ([mGuildNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblGuildNm]
    ON [dbo].[TblGuild]([mGuildNm] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [NC_TblGuild_mGuildMarkUptDate]
    ON [dbo].[TblGuild]([mGuildMarkUptDate] DESC) WITH (FILLFACTOR = 80);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¿¤Å×¸£ °ø¼º ¹æ¾î·¹º§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblGuild', @level2type = N'COLUMN', @level2name = N'mEtelrCastleDfnsLv';


GO

