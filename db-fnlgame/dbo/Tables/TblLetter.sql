CREATE TABLE [dbo].[TblLetter] (
    [mRegDate]  DATETIME      CONSTRAINT [DF__TblLetter__mRegD__3414ACBA] DEFAULT (getdate()) NOT NULL,
    [mSerialNo] BIGINT        NOT NULL,
    [mFromPcNm] CHAR (12)     NOT NULL,
    [mTitle]    CHAR (30)     NOT NULL,
    [mMsg]      VARCHAR (512) NOT NULL,
    [mToPcNm]   CHAR (12)     NOT NULL,
    CONSTRAINT [PkTblLetter] PRIMARY KEY CLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 80)
);


GO

