CREATE TABLE [dbo].[TblPcEquip] (
    [mRegDate]  DATETIME CONSTRAINT [DF__TblPcEqui__mRegD__2F2FFC0C] DEFAULT (getdate()) NOT NULL,
    [mOwner]    INT      NOT NULL,
    [mSlot]     INT      NOT NULL,
    [mSerialNo] BIGINT   NOT NULL,
    CONSTRAINT [PkTblPcEquip] PRIMARY KEY CLUSTERED ([mOwner] ASC, [mSlot] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblPcEquipTblPcInventory] FOREIGN KEY ([mSerialNo]) REFERENCES [dbo].[TblPcInventory] ([mSerialNo]) ON DELETE CASCADE
);


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblPcEquip]
    ON [dbo].[TblPcEquip]([mSerialNo] ASC) WITH (FILLFACTOR = 80);


GO

