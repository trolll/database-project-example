CREATE TABLE [dbo].[TblGuildAss] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF__TblGuildA__mRegD__550C5788] DEFAULT (getdate()) NOT NULL,
    [mGuildNo] INT           NOT NULL,
    CONSTRAINT [PkTblGuildAss] PRIMARY KEY CLUSTERED ([mGuildNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblGuildAssTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo])
);


GO

