CREATE TABLE [dbo].[TblCalendarPcSchedule] (
    [mRegDate]   SMALLDATETIME NOT NULL,
    [mPcNo]      INT           NOT NULL,
    [mSerialNo]  BIGINT        NOT NULL,
    [mOwnerPcNo] INT           NOT NULL,
    CONSTRAINT [UCL_PK_TblCalendarPcSchedule] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mSerialNo] ASC)
);


GO

CREATE NONCLUSTERED INDEX [NCL_TblCalendarPcSchedule_mSerialNo_mOwnerPcNo]
    ON [dbo].[TblCalendarPcSchedule]([mSerialNo] ASC, [mOwnerPcNo] ASC);


GO

CREATE NONCLUSTERED INDEX [NCL_TblCalendarPcSchedule_mOwnerPcNo]
    ON [dbo].[TblCalendarPcSchedule]([mOwnerPcNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÄÉ¸¯ÅÍ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarPcSchedule', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÏÁ¤À» ¸¸µç ÄÉ¸¯ÅÍ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarPcSchedule', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'µî·ÏÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarPcSchedule', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÏÁ¤¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarPcSchedule', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'PCÀÏÁ¤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarPcSchedule';


GO

