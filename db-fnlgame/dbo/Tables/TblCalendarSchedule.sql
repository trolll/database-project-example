CREATE TABLE [dbo].[TblCalendarSchedule] (
    [mRegDate]      SMALLDATETIME NOT NULL,
    [mOwnerPcNo]    INT           NOT NULL,
    [mSerialNo]     BIGINT        NOT NULL,
    [mTitle]        CHAR (30)     NOT NULL,
    [mMsg]          VARCHAR (512) NOT NULL,
    [mScheduleDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [UCL_PK_TblCalendarSchedule_mSn] PRIMARY KEY CLUSTERED ([mSerialNo] ASC)
);


GO

CREATE NONCLUSTERED INDEX [NCL_TblCalendarSchedule_mOwnerPcNo]
    ON [dbo].[TblCalendarSchedule]([mOwnerPcNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ÇÇà³¯Â¥', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarSchedule', @level2type = N'COLUMN', @level2name = N'mScheduleDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÏÁ¤¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarSchedule', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'ÀÏÁ¤', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarSchedule';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'³»¿ë', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarSchedule', @level2type = N'COLUMN', @level2name = N'mMsg';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä³¸¯ÅÍ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarSchedule', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Á¦¸ñ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarSchedule', @level2type = N'COLUMN', @level2name = N'mTitle';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'µî·ÏÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarSchedule', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

