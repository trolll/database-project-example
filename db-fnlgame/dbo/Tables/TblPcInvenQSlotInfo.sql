CREATE TABLE [dbo].[TblPcInvenQSlotInfo] (
    [mUpdateDate] DATETIME      CONSTRAINT [DF_TblPcInvenQSlotInfo_mUpdateDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]       INT           NOT NULL,
    [mInfo]       BINARY (8000) NULL,
    CONSTRAINT [UCL_PK_TblPcInvenQSlotInfo] PRIMARY KEY CLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblPcInvenQSlotInfoTblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO

