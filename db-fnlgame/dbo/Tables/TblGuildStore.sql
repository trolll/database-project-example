CREATE TABLE [dbo].[TblGuildStore] (
    [mRegDate]             DATETIME      CONSTRAINT [DF__TblGuildS__mRegD__68736660] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]            BIGINT        NOT NULL,
    [mGuildNo]             INT           NOT NULL,
    [mItemNo]              INT           NOT NULL,
    [mEndDate]             SMALLDATETIME NOT NULL,
    [mIsConfirm]           BIT           NOT NULL,
    [mStatus]              TINYINT       NOT NULL,
    [mCnt]                 INT           NOT NULL,
    [mCntUse]              SMALLINT      CONSTRAINT [DF_TblGuildStore_mCntUse] DEFAULT ((0)) NULL,
    [mIsSeizure]           BIT           CONSTRAINT [DF__TblGuildS__mIsSe__6A5BAED2] DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemNo]      INT           CONSTRAINT [DF__TblGuildS__mAppl__6B4FD30B] DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemEndDate] SMALLDATETIME CONSTRAINT [DF__TblGuildS__mAppl__6C43F744] DEFAULT (getdate()) NOT NULL,
    [mOwner]               INT           NOT NULL,
    [mGrade]               TINYINT       NOT NULL,
    [mPracticalPeriod]     INT           CONSTRAINT [DF__TblGuildS__mPrac__6D381B7D] DEFAULT ((0)) NOT NULL,
    [mBindingType]         TINYINT       CONSTRAINT [DF_TblGuildStore_mBindingType] DEFAULT ((0)) NOT NULL,
    [mRestoreCnt]          TINYINT       CONSTRAINT [DF_TblGuildStore_mRestoreCnt] DEFAULT ((0)) NOT NULL,
    [mHoleCount]           TINYINT       CONSTRAINT [DF_TblGuildStore_mHoleCount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [NC_PK_TblGuildStore_1] PRIMARY KEY NONCLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_TblGuildStore_1] CHECK ((0)<[mCnt]),
    CONSTRAINT [CK_TblGuildStore_2] CHECK ((0)<=[mGrade] AND [mGrade]<(2)),
    CONSTRAINT [CK_TblGuildStore_mBindingType] CHECK ([mBindingType]=(2) OR ([mBindingType]=(1) OR [mBindingType]=(0))),
    CONSTRAINT [FkTblGuildStoremGuildNo] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo]) ON DELETE CASCADE
);


GO

CREATE CLUSTERED INDEX [CL_TblGuildStore_1]
    ON [dbo].[TblGuildStore]([mGuildNo] ASC, [mGrade] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [NC_TblGuildStore_2]
    ON [dbo].[TblGuildStore]([mItemNo] ASC, [mCnt] ASC) WITH (FILLFACTOR = 80);


GO

CREATE trigger [dbo].[insert_role_item_log_on_TblGuildStore_trigger]
on [dbo].[TblGuildStore]
for update
as
if update(mitemno)
BEGIN  
declare @today datetime;  
set @today = getdate() 
declare @is_leg int; 
set @is_leg = -1;
select @is_leg = isnull(a.id,0) from [FNLParm].[dbo].[X_item_change_role] as a,DELETED,INSERTED where lost_item_id = DELETED.mItemNo and got_item_id = INSERTED.mItemNo
insert into FNLAccount.dbo.X_TblRoleItemLog(tablename,srvNo,mPcNo,before_mItemNo,after_mItemNo,before_mSerialNo,after_mSerialNo,created_at,is_leg) 
select 'TblGuildStore',1164,INSERTED.mGuildNo,DELETED.mItemNo,INSERTED.mItemNo,DELETED.mSerialNo,INSERTED.mSerialNo,@today,@is_leg from INSERTED,DELETED
END

GO

