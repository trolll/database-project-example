CREATE TABLE [dbo].[TblCastleGate] (
    [mRegDate] SMALLDATETIME CONSTRAINT [DF__TblCastle__mRegD__338B682C] DEFAULT (getdate()) NOT NULL,
    [mNo]      BIGINT        NOT NULL,
    [mHp]      INT           NOT NULL,
    [mIsOpen]  BIT           CONSTRAINT [DF__TblCastle__mIsOp__3573B09E] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblCastleGate] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 80),
    CHECK ((0)<=[mHp]),
    CHECK ((0)<=[mHp])
);


GO

