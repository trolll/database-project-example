CREATE TABLE [dbo].[TblPcServant] (
    [mRegDate]        DATETIME    CONSTRAINT [DF_TblPcServant_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]           INT         CONSTRAINT [DF_TblPcServant_mPcNo] DEFAULT ((0)) NOT NULL,
    [mSerialNo]       BIGINT      CONSTRAINT [DF_TblPcServant_mSerialNo] DEFAULT ((0)) NOT NULL,
    [mName]           VARCHAR (6) CONSTRAINT [DF_TblPcServant_mName] DEFAULT ('') NOT NULL,
    [mLevel]          SMALLINT    CONSTRAINT [DF_TblPcServant_mLevel] DEFAULT ((1)) NOT NULL,
    [mExp]            BIGINT      CONSTRAINT [DF_TblPcServant_mExp] DEFAULT ((0)) NOT NULL,
    [mFriendly]       SMALLINT    CONSTRAINT [DF_TblPcServant_mFriendly] DEFAULT ((0)) NOT NULL,
    [mSkillPoint]     SMALLINT    CONSTRAINT [DF_TblPcServant_mSkillPoint] DEFAULT ((0)) NOT NULL,
    [mSkillTreePoint] INT         CONSTRAINT [DF_TblPcServant_mSkillTreePoint] DEFAULT ((0)) NOT NULL,
    [mIsRestore]      BIT         CONSTRAINT [DF_TblPcServant_mIsRestore] DEFAULT ((0)) NOT NULL,
    [mAddStr]         SMALLINT    CONSTRAINT [DF_mAddStr] DEFAULT ((0)) NOT NULL,
    [mAddDex]         SMALLINT    CONSTRAINT [DF_mAddDex] DEFAULT ((0)) NOT NULL,
    [mAddInt]         SMALLINT    CONSTRAINT [DF_mAddInt] DEFAULT ((0)) NOT NULL,
    [mCombineCount]   SMALLINT    CONSTRAINT [DF_mCombineCount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblPcServant] PRIMARY KEY CLUSTERED ([mSerialNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ÀÌ¸§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mName';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä³¸¯ÅÍ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® ÀÎº¥Åä¸®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 眠啊 瘤瓷', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mAddInt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 眠啊 塞', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mAddStr';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ½ºÅ³ Æ®¸® ÇöÈ²', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mSkillTreePoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® Ä£¹Ðµµ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mFriendly';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ·¹º§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mLevel';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¾ÆÀÌÅÛ »ý¼ºÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¾ÆÀÌÅÛ ½Ã¸®¾ó', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 钦己 冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mCombineCount';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 眠啊 刮酶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mAddDex';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'º¹¿ø °¡´ÉÇÑ ¼­¹øÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mIsRestore';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® ÀÜ¿© ½ºÅ³ Æ÷ÀÎÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mSkillPoint';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼­¹øÆ® °æÇèÄ¡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServant', @level2type = N'COLUMN', @level2name = N'mExp';


GO

