CREATE TABLE [dbo].[TblPcOfAccount] (
    [mRegDate]            SMALLDATETIME CONSTRAINT [DF_TblPcOfAccount_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mOwner]              INT           NOT NULL,
    [mDelDateOfPcDeleted] SMALLDATETIME CONSTRAINT [DF_TblPcOfAccount_mDelDateOfPcDeleted] DEFAULT (getdate()) NOT NULL,
    [mDelCntOfPc]         INT           CONSTRAINT [DF_TblPcOfAccount_mDelCntOfPc] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [CL_PKTblPcOfAccount] PRIMARY KEY CLUSTERED ([mOwner] ASC) WITH (FILLFACTOR = 80)
);


GO

