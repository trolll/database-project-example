CREATE TABLE [dbo].[TblSiegeGambleTicket] (
    [mRegDate]   DATETIME CONSTRAINT [DF__TblSiegeG__mRegD__6ECC298B] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]  BIGINT   NOT NULL,
    [mTerritory] INT      NOT NULL,
    [mNo]        INT      NOT NULL,
    [mIsKeep]    INT      CONSTRAINT [DF__TblSiegeG__mIsKe__6FC04DC4] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblSiegeGambleTicket] PRIMARY KEY NONCLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE CLUSTERED INDEX [IX_TblSiegeGambleTicket]
    ON [dbo].[TblSiegeGambleTicket]([mTerritory] ASC, [mNo] ASC) WITH (FILLFACTOR = 80);


GO

