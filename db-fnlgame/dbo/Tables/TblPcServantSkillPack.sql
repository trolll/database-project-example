CREATE TABLE [dbo].[TblPcServantSkillPack] (
    [mRegDate]       DATETIME      CONSTRAINT [DF_TblPcServantSkillPack_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]      BIGINT        NOT NULL,
    [mSPID]          INT           NOT NULL,
    [mEndDate]       SMALLDATETIME NOT NULL,
    [mSkillPackType] SMALLINT      CONSTRAINT [DF_mSkillPackType] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [UCL_PK_TblPcServantSkillPack] PRIMARY KEY CLUSTERED ([mSerialNo] ASC, [mSPID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³ ÆÑ ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillPack', @level2type = N'COLUMN', @level2name = N'mSPID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»ý¼ºÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillPack', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Á¾·áÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillPack', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼ÒÀ¯ ¼­¹øÆ® ½Ã¸®¾ó', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillPack', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® ½ºÅ³Æ®¸®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillPack';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 胶懦蒲 鸥涝(1:扁夯, 2:函脚)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillPack', @level2type = N'COLUMN', @level2name = N'mSkillPackType';


GO

