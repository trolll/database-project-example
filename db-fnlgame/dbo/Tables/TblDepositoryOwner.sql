CREATE TABLE [dbo].[TblDepositoryOwner] (
    [mRegDate] SMALLDATETIME NOT NULL,
    [mRegNo]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [mOwner]   INT           NOT NULL,
    [mPos]     TINYINT       NOT NULL,
    [mPcNo]    INT           NULL,
    [mPcNm]    VARCHAR (12)  NULL,
    [mItemCnt] INT           NOT NULL,
    CONSTRAINT [NC_PK_TblDepositoryOwner_1] PRIMARY KEY NONCLUSTERED ([mRegNo] ASC) WITH (FILLFACTOR = 80)
);


GO

