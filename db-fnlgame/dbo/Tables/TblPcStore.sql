CREATE TABLE [dbo].[TblPcStore] (
    [mRegDate]             DATETIME      CONSTRAINT [DF__TblPcStor__mRegD__3D73F9C2] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]            BIGINT        NOT NULL,
    [mUserNo]              INT           NOT NULL,
    [mItemNo]              INT           NOT NULL,
    [mEndDate]             SMALLDATETIME NOT NULL,
    [mIsConfirm]           BIT           NOT NULL,
    [mStatus]              TINYINT       NOT NULL,
    [mCnt]                 INT           NOT NULL,
    [mCntUse]              SMALLINT      CONSTRAINT [DF_TblPcStore_mCntUse] DEFAULT ((0)) NULL,
    [mIsSeizure]           BIT           CONSTRAINT [DF__TblPcStor__mIsSe__4050666D] DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemNo]      INT           CONSTRAINT [DF__TblPcStor__mAppl__41448AA6] DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemEndDate] SMALLDATETIME CONSTRAINT [DF__TblPcStor__mAppl__4238AEDF] DEFAULT (getdate()) NOT NULL,
    [mOwner]               INT           CONSTRAINT [DF__TblPcStor__mOwne__737017C0] DEFAULT ((0)) NOT NULL,
    [mPracticalPeriod]     INT           CONSTRAINT [DF_TblPcStore_mPracticalPeriod] DEFAULT ((0)) NOT NULL,
    [mBindingType]         TINYINT       CONSTRAINT [DF_TblPcStore_mBindingType] DEFAULT ((0)) NOT NULL,
    [mRestoreCnt]          TINYINT       CONSTRAINT [DF_TblPcStore_mRestoreCnt] DEFAULT ((0)) NOT NULL,
    [mHoleCount]           TINYINT       CONSTRAINT [DF_TblPcStore_mHoleCount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_NC_TblPcStore_1] PRIMARY KEY NONCLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK__TblPcStore__mCnt__2AD55B43] CHECK ((0)<[mCnt]),
    CONSTRAINT [CK_TblPcStore_mBindingType] CHECK ([mBindingType]=(2) OR ([mBindingType]=(1) OR [mBindingType]=(0))),
    CONSTRAINT [CK_TblPcStore_mUserNo] CHECK ([mUserNo]>(1))
);


GO

CREATE CLUSTERED INDEX [CL_TblPcStore]
    ON [dbo].[TblPcStore]([mUserNo] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [NC_TblPcStore_2]
    ON [dbo].[TblPcStore]([mItemNo] ASC, [mCnt] ASC) WITH (FILLFACTOR = 80);


GO

CREATE trigger [dbo].[insert_role_item_log_on_TblPcStore_trigger]
on [dbo].[TblPcStore]
for update
as
if update(mitemno)
BEGIN  
declare @today datetime;  
set @today = getdate() 
declare @is_leg int; 
set @is_leg = -1;
select @is_leg = isnull(a.id,0) from [FNLParm].[dbo].[X_item_change_role] as a,DELETED,INSERTED where lost_item_id = DELETED.mItemNo and got_item_id = INSERTED.mItemNo
insert into FNLAccount.dbo.X_TblRoleItemLog(tablename,srvNo,mPcNo,before_mItemNo,after_mItemNo,before_mSerialNo,after_mSerialNo,created_at,is_leg) 
select 'TblPcStore',1164,INSERTED.muserno,DELETED.mItemNo,INSERTED.mItemNo,DELETED.mSerialNo,INSERTED.mSerialNo,@today,@is_leg from INSERTED,DELETED
END

GO

