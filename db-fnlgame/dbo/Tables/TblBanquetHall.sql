CREATE TABLE [dbo].[TblBanquetHall] (
    [mTerritory]       INT           NOT NULL,
    [mBanquetHallType] INT           NOT NULL,
    [mBanquetHallNo]   INT           NOT NULL,
    [mOwnerPcNo]       INT           CONSTRAINT [DF_TblBanquetHall_mOwnerPcNo] DEFAULT ((0)) NOT NULL,
    [mRegDate]         SMALLDATETIME CONSTRAINT [DF_TblBanquetHall_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mLeftMin]         INT           CONSTRAINT [DF_TblBanquetHall_mLeftMin] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblBanquetHall] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mBanquetHallType] ASC, [mBanquetHallNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [IX_TblBanquetHall]
    ON [dbo].[TblBanquetHall]([mOwnerPcNo] ASC) WITH (FILLFACTOR = 80);


GO

