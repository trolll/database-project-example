CREATE TABLE [dbo].[TblNpcGlobalValue] (
    [mNId]   INT          NOT NULL,
    [mName]  VARCHAR (20) NOT NULL,
    [mValue] INT          NOT NULL,
    CONSTRAINT [UCL_PK_TblNpcGlobalValue] PRIMARY KEY NONCLUSTERED ([mName] ASC) WITH (FILLFACTOR = 80)
);


GO

