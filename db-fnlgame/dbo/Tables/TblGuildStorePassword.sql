CREATE TABLE [dbo].[TblGuildStorePassword] (
    [mRegDate]  DATETIME CONSTRAINT [DF_TblGuildStorePassword_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]  INT      NOT NULL,
    [mPassword] CHAR (8) NOT NULL,
    [mGrade]    TINYINT  NOT NULL,
    CONSTRAINT [CL_PK_TblGuildStorePassword] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mGrade] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_TblGuildStorePassword_mGrade] CHECK ((0)<=[mGrade] AND [mGrade]<(2)),
    CONSTRAINT [FkTblGuildStorePasswordmGuildNo] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo]) ON DELETE CASCADE
);


GO

