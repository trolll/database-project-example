CREATE TABLE [dbo].[TblDiscipleHistory] (
    [mMaster]     INT          NOT NULL,
    [mNo]         INT          NOT NULL,
    [mDisciple]   INT          NOT NULL,
    [mDiscipleNm] VARCHAR (50) NOT NULL,
    [mRegDate]    DATETIME     CONSTRAINT [DF_TblDiscipleHistory_mRegDate] DEFAULT (getdate()) NOT NULL
);


GO

CREATE CLUSTERED INDEX [CL_TblDiscipleHistory]
    ON [dbo].[TblDiscipleHistory]([mMaster] ASC) WITH (FILLFACTOR = 80);


GO

