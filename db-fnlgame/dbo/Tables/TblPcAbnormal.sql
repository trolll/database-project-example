CREATE TABLE [dbo].[TblPcAbnormal] (
    [mPcNo]       INT     NOT NULL,
    [mParmNo]     INT     NOT NULL,
    [mLeftTime]   INT     NOT NULL,
    [mAbParmNo]   INT     CONSTRAINT [DF_TblPcAbnormal_mAbParmNo] DEFAULT ((0)) NOT NULL,
    [mRestoreCnt] TINYINT CONSTRAINT [DF_TblPcAbnormal_mRestoreCnt] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_TblPcAbnormal] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mParmNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblPcAbnormalTblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO

