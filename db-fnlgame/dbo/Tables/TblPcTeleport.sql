CREATE TABLE [dbo].[TblPcTeleport] (
    [mNo]      INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mPcNo]    INT          NOT NULL,
    [mName]    VARCHAR (50) NOT NULL,
    [mMapNo]   INT          NOT NULL,
    [mPosX]    REAL         NOT NULL,
    [mPosY]    REAL         NOT NULL,
    [mPosZ]    REAL         NOT NULL,
    [mType]    INT          CONSTRAINT [DF_TblPcTeleport_mType] DEFAULT ((0)) NOT NULL,
    [mLevel]   INT          CONSTRAINT [DF_TblPcTeleport_mLevel] DEFAULT ((1)) NOT NULL,
    [mOrgName] VARCHAR (50) CONSTRAINT [DF_TblPcTeleport_mOrgName] DEFAULT ('') NOT NULL,
    CONSTRAINT [PkTblPcTeleport] PRIMARY KEY NONCLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblPcTeleportTblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO

CREATE CLUSTERED INDEX [IxTblTblPcTeleportPcNo]
    ON [dbo].[TblPcTeleport]([mPcNo] ASC) WITH (FILLFACTOR = 80);


GO

