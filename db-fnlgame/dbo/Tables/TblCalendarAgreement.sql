CREATE TABLE [dbo].[TblCalendarAgreement] (
    [mRegDate]    SMALLDATETIME NOT NULL,
    [mOwnerPcNo]  INT           NOT NULL,
    [mMemberPcNo] INT           NOT NULL,
    CONSTRAINT [UCL_PK_TblCalendarAgreement] PRIMARY KEY CLUSTERED ([mOwnerPcNo] ASC, [mMemberPcNo] ASC)
);


GO

CREATE NONCLUSTERED INDEX [NCL_TblCalendarAgreement_mMemberPcNo]
    ON [dbo].[TblCalendarAgreement]([mMemberPcNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'µî·ÏÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarAgreement', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'½ÂÀÎ¸â¹ö¸®½ºÆ®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarAgreement';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸â¹ö ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarAgreement', @level2type = N'COLUMN', @level2name = N'mMemberPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä³¸¯ÅÍ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarAgreement', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

