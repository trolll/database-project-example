CREATE TABLE [dbo].[TblGuildRecruitMemberList] (
    [mRegDate] DATETIME      CONSTRAINT [DF_TblGuildRecruitMemberList_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT           NOT NULL,
    [mGuildNo] INT           NOT NULL,
    [mState]   TINYINT       NOT NULL,
    [mPcMsg]   VARCHAR (500) CONSTRAINT [DF_TblGuildRecruitMemberList_mPcMsgz] DEFAULT (' ') NULL,
    [mIsHide]  BIT           CONSTRAINT [DF_TblGuildRecruitMemberList_mIsRefusal] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UNC_TblGuildRecruitMemberList_mPcNo] PRIMARY KEY NONCLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_TblGuildRecruitMemberList_mState] CHECK ((0)<=[mState] AND [mState]<=(5))
);


GO

CREATE CLUSTERED INDEX [CL_TblGuildRecruitMemberList_mGuildNo]
    ON [dbo].[TblGuildRecruitMemberList]([mGuildNo] ASC) WITH (FILLFACTOR = 80);


GO

