CREATE TABLE [dbo].[TblPetitionCheckState] (
    [mPcNo]     INT    NOT NULL,
    [mCategory] INT    NOT NULL,
    [mPID]      BIGINT NOT NULL,
    CONSTRAINT [PK_TblPetitionPcState] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mCategory] ASC) WITH (FILLFACTOR = 80)
);


GO

