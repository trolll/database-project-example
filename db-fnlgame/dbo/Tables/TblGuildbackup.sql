CREATE TABLE [dbo].[TblGuildbackup] (
    [mRegDate]    DATETIME      NOT NULL,
    [mGuildNo]    INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mGuildSeqNo] TINYINT       NOT NULL,
    [mGuildNm]    CHAR (12)     NOT NULL,
    [mGuildMsg]   CHAR (60)     NULL,
    [mGuildMark]  BINARY (1784) NULL,
    [mRewardExp]  INT           NOT NULL
);


GO

