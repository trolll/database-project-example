CREATE TABLE [dbo].[TblQuickSupplicationComment] (
    [mRegDate] DATETIME      CONSTRAINT [DF__TblQuickS__mRegD__49EEDF40] DEFAULT (getdate()) NOT NULL,
    [mQSID]    BIGINT        NOT NULL,
    [mComment] VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_CL_TblQuickSupplicationComment] PRIMARY KEY CLUSTERED ([mQSID] ASC) WITH (FILLFACTOR = 80)
);


GO

