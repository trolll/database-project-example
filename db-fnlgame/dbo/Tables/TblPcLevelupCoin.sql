CREATE TABLE [dbo].[TblPcLevelupCoin] (
    [mPcno]               INT    CONSTRAINT [DF_TblPcLevelupCoin_mPcno] DEFAULT ((0)) NOT NULL,
    [mExp]                BIGINT CONSTRAINT [DF_TblPcLevelupCoin_mExp] DEFAULT ((0)) NOT NULL,
    [mLastReceiptSection] INT    CONSTRAINT [DF_TblPcLevelupCoin_mLastReceiptSection] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblPcLevelupCoin_mPcno] PRIMARY KEY CLUSTERED ([mPcno] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'·¹º§ ¾÷ ÁÖÈ­ »ç¿ëÀÚ Á¤º¸', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcLevelupCoin';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°æÇèÄ¡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcLevelupCoin', @level2type = N'COLUMN', @level2name = N'mExp';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»ç¿ëÀÚ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcLevelupCoin', @level2type = N'COLUMN', @level2name = N'mPcno';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸¶Áö¸· ÄÚÀÎ ¼ö·É ±¸°£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcLevelupCoin', @level2type = N'COLUMN', @level2name = N'mLastReceiptSection';


GO

