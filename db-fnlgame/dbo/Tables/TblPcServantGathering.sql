CREATE TABLE [dbo].[TblPcServantGathering] (
    [mRegDate]  DATETIME CONSTRAINT [DF_TblPcServantGathering_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]     INT      NOT NULL,
    [mSerialNo] BIGINT   NOT NULL,
    [mEndDate]  DATETIME NOT NULL,
    CONSTRAINT [PK_TblPcServantGathering] PRIMARY KEY CLUSTERED ([mPcNo] ASC),
    CONSTRAINT [FK_TblPcServantGathering_TblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo]),
    CONSTRAINT [FK_TblPcServantGathering_TblPcInventory] FOREIGN KEY ([mSerialNo]) REFERENCES [dbo].[TblPcInventory] ([mSerialNo])
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 盲笼 惑怕', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantGathering';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'盲笼吝牢 辑锅飘 矫府倔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantGathering', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑锅飘 盲笼 殿废 矫埃', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantGathering', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantGathering', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

