CREATE TABLE [dbo].[TblPcDeleted] (
    [mRegDate] DATETIME  CONSTRAINT [DF__TblPcDele__mRegD__269AB60B] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT       NOT NULL,
    [mPcNm]    CHAR (12) NULL,
    CONSTRAINT [PkTblPcDeleted] PRIMARY KEY CLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblPcDeletedTblPc] FOREIGN KEY ([mPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO

CREATE NONCLUSTERED INDEX [NC_TblPcDeleted_mPcNm]
    ON [dbo].[TblPcDeleted]([mPcNm] DESC) WITH (FILLFACTOR = 80);


GO

