CREATE TABLE [dbo].[TblCalendarGroupMember] (
    [mRegDate]    SMALLDATETIME NOT NULL,
    [mOwnerPcNo]  INT           NOT NULL,
    [mGroupNo]    TINYINT       NOT NULL,
    [mMemberPcNo] INT           NOT NULL,
    CONSTRAINT [UCL_PK_TblCalendarGroupMember] PRIMARY KEY CLUSTERED ([mOwnerPcNo] ASC, [mGroupNo] ASC, [mMemberPcNo] ASC)
);


GO

CREATE NONCLUSTERED INDEX [NCL_TblCalendarGroupMember_mMemberPcNo]
    ON [dbo].[TblCalendarGroupMember]([mMemberPcNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä³¸¯ÅÍ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroupMember', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸â¹ö ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroupMember', @level2type = N'COLUMN', @level2name = N'mMemberPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'µî·ÏÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroupMember', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'±×·ì¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroupMember', @level2type = N'COLUMN', @level2name = N'mGroupNo';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'´Þ·Â±×·ì¸â¹ö', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarGroupMember';


GO

