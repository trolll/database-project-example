CREATE TABLE [dbo].[TblGuildRecruit] (
    [mRegDate]    DATETIME      CONSTRAINT [DF_TblGuildRecruit_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mEndDate]    DATETIME      NOT NULL,
    [mGuildNo]    INT           NOT NULL,
    [mIsPremium]  BIT           NOT NULL,
    [mIsMarkShow] BIT           NOT NULL,
    [mGuildMsg]   VARCHAR (500) CONSTRAINT [DF_TblGuildRecruit_mGuildMsg] DEFAULT (' ') NULL,
    CONSTRAINT [UNC_TblGuildRecruit_mGuildNo] PRIMARY KEY NONCLUSTERED ([mGuildNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [NL_TblGuildRecruit_mGuildNm]
    ON [dbo].[TblGuildRecruit]([mEndDate] DESC) WITH (FILLFACTOR = 80);


GO

