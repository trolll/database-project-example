CREATE TABLE [dbo].[TblDisciple] (
    [mMaster]      INT      NOT NULL,
    [mCurPoint]    INT      CONSTRAINT [DF_TblDisciple_mCurPoint] DEFAULT ((0)) NOT NULL,
    [mMaxCurPoint] INT      CONSTRAINT [DF_TblDisciple_mMaxPoint] DEFAULT ((0)) NOT NULL,
    [mRegDate]     DATETIME CONSTRAINT [DF_TblDisciple_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mDecDate]     DATETIME CONSTRAINT [DF_TblDisciple_mDecDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [CL_PK_TblDisciple] PRIMARY KEY CLUSTERED ([mMaster] ASC) WITH (FILLFACTOR = 80)
);


GO

