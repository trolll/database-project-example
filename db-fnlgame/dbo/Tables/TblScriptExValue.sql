CREATE TABLE [dbo].[TblScriptExValue] (
    [mNo]  INT NOT NULL,
    [mVal] INT NOT NULL,
    CONSTRAINT [PK_TblScriptExternValue] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 80)
);


GO

