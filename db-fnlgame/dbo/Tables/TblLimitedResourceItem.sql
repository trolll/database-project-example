CREATE TABLE [dbo].[TblLimitedResourceItem] (
    [mResourceType] INT NOT NULL,
    [mIID]          INT NOT NULL,
    [mIsComsume]    BIT CONSTRAINT [DF_TblLimitedResourceItem_mIsComsume] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblLimitedResourceItem] PRIMARY KEY CLUSTERED ([mResourceType] ASC, [mIID] ASC) WITH (FILLFACTOR = 80)
);


GO

