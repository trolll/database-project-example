CREATE TABLE [dbo].[TblDialogScript] (
    [mRegDate]    SMALLDATETIME  CONSTRAINT [DF_TblDialogScript_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mMId]        INT            NOT NULL,
    [mScriptText] VARCHAR (8000) CONSTRAINT [DF_TblDialogScript_mScript] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_TblDialogScript] PRIMARY KEY CLUSTERED ([mMId] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [IX_TblDialogScript]
    ON [dbo].[TblDialogScript]([mMId] ASC) WITH (FILLFACTOR = 80);


GO

