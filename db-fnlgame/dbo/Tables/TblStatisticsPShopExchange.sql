CREATE TABLE [dbo].[TblStatisticsPShopExchange] (
    [mRegDate]        DATETIME CONSTRAINT [DF__TblStatis__mRegD__2665ABE1] DEFAULT (getdate()) NOT NULL,
    [mItemNo]         INT      NOT NULL,
    [mBuyCount]       BIGINT   NULL,
    [mSellCount]      BIGINT   NULL,
    [mBuyTotalPrice]  BIGINT   NULL,
    [mSellTotalPrice] BIGINT   NULL,
    [mBuyMinPrice]    BIGINT   NULL,
    [mSellMinPrice]   BIGINT   NULL,
    [mBuyMaxPrice]    BIGINT   NULL,
    [mSellMaxPrice]   BIGINT   NULL,
    [mItemStatus]     SMALLINT CONSTRAINT [DF_TblStatisticsPShopExchange_mItemStatus] DEFAULT ((0)) NOT NULL,
    [mTradeType]      SMALLINT CONSTRAINT [DF_TblStatisticsPShopExchange_mTradeType] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblStatisticsPShopExchange_mItemNo_mItemStatus_mTradeType] PRIMARY KEY CLUSTERED ([mItemNo] ASC, [mItemStatus] ASC, [mTradeType] ASC) WITH (FILLFACTOR = 80)
);


GO

