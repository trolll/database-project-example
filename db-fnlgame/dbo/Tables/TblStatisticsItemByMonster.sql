CREATE TABLE [dbo].[TblStatisticsItemByMonster] (
    [mRegDate]    DATETIME CONSTRAINT [DF__TblStatis__mRegD__642DD430] DEFAULT (getdate()) NOT NULL,
    [MID]         INT      NOT NULL,
    [mItemNo]     INT      NOT NULL,
    [mCreate]     BIGINT   NULL,
    [mDelete]     BIGINT   NULL,
    [mItemStatus] SMALLINT CONSTRAINT [DF_TblStatisticsItemByMonster_mItemStatus] DEFAULT ((0)) NOT NULL,
    [mModType]    SMALLINT CONSTRAINT [DF_TblStatisticsItemByMonster_mModType] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PKTblStatisticsItemByMonster_MID_mItemNo_mItemStatus_mModType] PRIMARY KEY CLUSTERED ([MID] ASC, [mItemNo] ASC, [mItemStatus] ASC, [mModType] ASC) WITH (FILLFACTOR = 80)
);


GO

