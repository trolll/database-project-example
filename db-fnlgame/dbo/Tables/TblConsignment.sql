CREATE TABLE [dbo].[TblConsignment] (
    [mRegDate]      SMALLDATETIME CONSTRAINT [DF_TblConsignment_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mCnsmNo]       BIGINT        IDENTITY (1, 1) NOT NULL,
    [mCategoryNo]   TINYINT       NOT NULL,
    [mPrice]        INT           NOT NULL,
    [mTradeEndDate] SMALLDATETIME NOT NULL,
    [mPcNo]         INT           NOT NULL,
    [mRegCnt]       INT           NOT NULL,
    [mCurCnt]       INT           NOT NULL,
    [mItemNo]       INT           NOT NULL,
    CONSTRAINT [UNC_PK_TblConsignment_mCnsmNo] PRIMARY KEY CLUSTERED ([mCnsmNo] ASC) WITH (FILLFACTOR = 80),
    CHECK ((0)<[mRegCnt]),
    CHECK ((0)<[mRegCnt]),
    CHECK ((0)<=[mCategoryNo] AND [mCategoryNo]<(14)),
    CHECK ((0)<=[mCategoryNo] AND [mCategoryNo]<(14)),
    CHECK ((0)<=[mCurCnt]),
    CHECK ((0)<=[mCurCnt])
);


GO

CREATE NONCLUSTERED INDEX [NC_TblConsignment_mCategoryNo_mTradeEndDate]
    ON [dbo].[TblConsignment]([mCategoryNo] ASC, [mTradeEndDate] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [NC_TblConsignment_mPcNo]
    ON [dbo].[TblConsignment]([mPcNo] ASC) WITH (FILLFACTOR = 80);


GO

