CREATE TABLE [dbo].[TblPcInventory] (
    [mRegDate]             DATETIME      CONSTRAINT [DF__TblPcInve__mRegD__36C6FC33] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]            BIGINT        NOT NULL,
    [mPcNo]                INT           NOT NULL,
    [mItemNo]              INT           NOT NULL,
    [mEndDate]             SMALLDATETIME NOT NULL,
    [mIsConfirm]           BIT           NOT NULL,
    [mStatus]              TINYINT       NOT NULL,
    [mCnt]                 INT           NOT NULL,
    [mCntUse]              SMALLINT      CONSTRAINT [DF_TblPcInventory_mCntUse] DEFAULT ((0)) NULL,
    [mIsSeizure]           BIT           CONSTRAINT [DF__TblPcInve__mIsSe__39A368DE] DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemNo]      INT           CONSTRAINT [DF__TblPcInve__mAppl__3A978D17] DEFAULT ((0)) NOT NULL,
    [mApplyAbnItemEndDate] SMALLDATETIME CONSTRAINT [DF__TblPcInve__mAppl__3B8BB150] DEFAULT (getdate()) NOT NULL,
    [mOwner]               INT           CONSTRAINT [DF__TblPcInve__mOwne__727BF387] DEFAULT ((0)) NOT NULL,
    [mPracticalPeriod]     INT           CONSTRAINT [DF_TblPcInventory_mPracticalPeriod] DEFAULT ((0)) NOT NULL,
    [mBindingType]         TINYINT       CONSTRAINT [DF_TblPcInventory_mBindingType] DEFAULT ((0)) NOT NULL,
    [mRestoreCnt]          TINYINT       CONSTRAINT [DF_TblPcInventory_mRestoreCnt] DEFAULT ((0)) NOT NULL,
    [mHoleCount]           TINYINT       CONSTRAINT [DF_TblPcInventory_mHoleCount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_NC_TblPcInventory_1] PRIMARY KEY NONCLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK__TblPcInven__mCnt__0880433F] CHECK ((0)<[mCnt]),
    CONSTRAINT [CK_TblPcInventory_mBindingType] CHECK ([mBindingType]=(2) OR ([mBindingType]=(1) OR [mBindingType]=(0)))
);


GO

CREATE NONCLUSTERED INDEX [NC_TblPcInventory_2]
    ON [dbo].[TblPcInventory]([mItemNo] ASC, [mCnt] ASC) WITH (FILLFACTOR = 80);


GO

CREATE CLUSTERED INDEX [CL_TblPcInventory]
    ON [dbo].[TblPcInventory]([mPcNo] ASC) WITH (FILLFACTOR = 80);


GO

CREATE trigger [dbo].[insert_role_item_log_on_TblPcInventory_trigger]
on [dbo].[TblPcInventory]
for update
as
if update(mitemno)
BEGIN  
declare @today datetime;  
set @today = getdate() 
declare @is_leg int; 
set @is_leg = -1;
select @is_leg = isnull(a.id,0) from [FNLParm].[dbo].[X_item_change_role] as a,DELETED,INSERTED where lost_item_id = DELETED.mItemNo and got_item_id = INSERTED.mItemNo
insert into FNLAccount.dbo.X_TblRoleItemLog(tablename,srvNo,mPcNo,before_mItemNo,after_mItemNo,before_mSerialNo,after_mSerialNo,created_at,is_leg) 
select 'TblPcInventory',1164,INSERTED.mPcNo,DELETED.mItemNo,INSERTED.mItemNo,DELETED.mSerialNo,INSERTED.mSerialNo,@today,@is_leg from INSERTED,DELETED
END

GO

