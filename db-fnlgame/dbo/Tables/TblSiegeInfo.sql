CREATE TABLE [dbo].[TblSiegeInfo] (
    [mRegDate]   DATETIME CONSTRAINT [DF_TblSiegeInfo_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mChgDate]   DATETIME CONSTRAINT [DF_TblSiegeInfo_mChgDate] DEFAULT ('2000-01-01 00:00') NOT NULL,
    [mTerritory] INT      NOT NULL,
    [mIsSiege]   INT      CONSTRAINT [DF_TblSiegeInfo_mIsSiege] DEFAULT ((2)) NOT NULL,
    CONSTRAINT [IX_TblSiegeInfo] UNIQUE NONCLUSTERED ([mTerritory] ASC) WITH (FILLFACTOR = 80)
);


GO

