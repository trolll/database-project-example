CREATE TABLE [dbo].[TblPcGoldItemEffect] (
    [mRegDate]  DATETIME      CONSTRAINT [DF__TblPcGold__mRegD__75586032] DEFAULT (getdate()) NOT NULL,
    [mPcNo]     INT           NOT NULL,
    [mItemType] INT           NOT NULL,
    [mParmA]    FLOAT (53)    CONSTRAINT [DF__TblPcGold__mParm__764C846B] DEFAULT ((0)) NOT NULL,
    [mEndDate]  SMALLDATETIME NOT NULL,
    [mItemNo]   INT           CONSTRAINT [DF_TblPcGoldItemEffect_mItemNo] DEFAULT ((409)) NOT NULL,
    CONSTRAINT [CL_PK_TblPcGoldItemEffect] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mItemType] ASC) WITH (FILLFACTOR = 80)
);


GO

