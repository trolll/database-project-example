CREATE TABLE [dbo].[TblPcQuestCondition] (
    [mPcNo]      INT NOT NULL,
    [mQuestNo]   INT NOT NULL,
    [mMonsterNo] INT NOT NULL,
    [mCnt]       INT NOT NULL,
    [mMaxCnt]    INT NOT NULL,
    CONSTRAINT [UCL_PK_TblPcQuestCondition_mPcNo] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mQuestNo] ASC, [mMonsterNo] ASC) WITH (FILLFACTOR = 80)
);


GO

