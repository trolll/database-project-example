CREATE TABLE [dbo].[TblCastleTowerStoneRuined] (
    [mPlace]              INT NOT NULL,
    [mRuinMonsterRespawn] BIT CONSTRAINT [DF_TblCastleTowerStoneRuined_mRuinMonsterRespawn] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PK_TblCastleTowerStoneRuined] PRIMARY KEY CLUSTERED ([mPlace] ASC) WITH (FILLFACTOR = 80)
);


GO

