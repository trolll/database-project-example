CREATE TABLE [dbo].[TblGuildQuest] (
    [mGuildMasterNo] INT NOT NULL,
    [mMQuestNo]      INT NOT NULL,
    [mPcNo]          INT NOT NULL,
    CONSTRAINT [UCL_PK_TblGuildQuest_mGuildMasterNo] PRIMARY KEY CLUSTERED ([mGuildMasterNo] ASC, [mMQuestNo] ASC) WITH (FILLFACTOR = 80)
);


GO

