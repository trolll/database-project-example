CREATE TABLE [dbo].[TblGuildAgitTicket] (
    [mTicketSerialNo] BIGINT        NOT NULL,
    [mTerritory]      INT           NOT NULL,
    [mGuildAgitNo]    INT           NOT NULL,
    [mOwnerGID]       INT           NOT NULL,
    [mRegDate]        SMALLDATETIME CONSTRAINT [DF_TblGuildAgitTicket_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mFromPcNm]       NVARCHAR (12) CONSTRAINT [DF_TblGuildAgitTicket_mFromPcNm] DEFAULT ('') NOT NULL,
    [mToPcNm]         NVARCHAR (12) CONSTRAINT [DF_TblGuildAgitTicket_mToPcNm] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_TblGuildAgitTicket] PRIMARY KEY CLUSTERED ([mTicketSerialNo] ASC) WITH (FILLFACTOR = 80)
);


GO

