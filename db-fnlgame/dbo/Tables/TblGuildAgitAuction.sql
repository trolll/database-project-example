CREATE TABLE [dbo].[TblGuildAgitAuction] (
    [mTerritory]   INT           NOT NULL,
    [mGuildAgitNo] INT           NOT NULL,
    [mCurBidGID]   INT           CONSTRAINT [DF_TblGuildAgitAuction_mCurBidGID] DEFAULT ((0)) NOT NULL,
    [mCurBidMoney] BIGINT        CONSTRAINT [DF_TblGuildAgitAuction_mCurBidMoney] DEFAULT ((0)) NOT NULL,
    [mRegDate]     SMALLDATETIME CONSTRAINT [DF_TblGuildAgitAuction_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TblGuildAgitAuction] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mGuildAgitNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [IX_TblGuildAgitAuction]
    ON [dbo].[TblGuildAgitAuction]([mCurBidGID] ASC) WITH (FILLFACTOR = 80);


GO

