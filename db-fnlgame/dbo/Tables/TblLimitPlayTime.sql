CREATE TABLE [dbo].[TblLimitPlayTime] (
    [mUserNo]          INT NOT NULL,
    [mNormalLimitTime] INT NOT NULL,
    [mPcBangLimitTime] INT NOT NULL,
    CONSTRAINT [PK_TblLimitPlayTime_mUserNo] PRIMARY KEY CLUSTERED ([mUserNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'Æ¯È­¼­¹ö °ü·Ã Á¦ÇÑ½Ã°£ Å×ÀÌºí', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Æ¯È­¼­¹ö°ü·Ã ÀÏ¹Ý Á¦ÇÑ ½Ã°£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayTime', @level2type = N'COLUMN', @level2name = N'mNormalLimitTime';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'°èÁ¤ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayTime', @level2type = N'COLUMN', @level2name = N'mUserNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Æ¯È­¼­¹ö°ü·Ã PC¹æ Á¦ÇÑ ½Ã°£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitPlayTime', @level2type = N'COLUMN', @level2name = N'mPcBangLimitTime';


GO

