CREATE TABLE [dbo].[TblPc] (
    [mRegDate]   DATETIME  CONSTRAINT [DF__TblPc__mRegDate__1E05700A] DEFAULT (getdate()) NOT NULL,
    [mOwner]     INT       NOT NULL,
    [mSlot]      TINYINT   NOT NULL,
    [mNo]        INT       IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mNm]        CHAR (12) NOT NULL,
    [mClass]     TINYINT   NOT NULL,
    [mSex]       TINYINT   CONSTRAINT [DF_TblPc_mSex] DEFAULT ((0)) NOT NULL,
    [mHead]      TINYINT   CONSTRAINT [DF_TblPc_mHead] DEFAULT ((0)) NOT NULL,
    [mFace]      TINYINT   CONSTRAINT [DF_TblPc_mFace] DEFAULT ((0)) NOT NULL,
    [mBody]      TINYINT   CONSTRAINT [DF_TblPc_mBody] DEFAULT ((0)) NOT NULL,
    [mHomeMapNo] INT       NOT NULL,
    [mHomePosX]  REAL      NOT NULL,
    [mHomePosY]  REAL      NOT NULL,
    [mHomePosZ]  REAL      NOT NULL,
    [mDelDate]   DATETIME  NULL,
    CONSTRAINT [PkTblPc] PRIMARY KEY NONCLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK__TblPc__mClass] CHECK ((0)<=[mClass] AND [mClass]<=(4)),
    CONSTRAINT [CK__TblPc__mNm__1FEDB87C] CHECK ((1)<=datalength(rtrim([mNm]))),
    CONSTRAINT [CK__TblPc__mNo__1EF99443] CHECK ((0)<=[mNo])
);


GO

CREATE CLUSTERED INDEX [IxTblPcOwner]
    ON [dbo].[TblPc]([mOwner] ASC) WITH (FILLFACTOR = 80);


GO

CREATE UNIQUE NONCLUSTERED INDEX [IxTblPcNm]
    ON [dbo].[TblPc]([mNm] ASC) WITH (FILLFACTOR = 80);


GO

