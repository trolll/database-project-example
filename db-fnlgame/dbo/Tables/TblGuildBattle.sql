CREATE TABLE [dbo].[TblGuildBattle] (
    [mRegDate]  DATETIME CONSTRAINT [DF__TblGuildB__mRegD__0C06BB60] DEFAULT (getdate()) NOT NULL,
    [mGuildNo1] INT      NOT NULL,
    [mGuildNo2] INT      NOT NULL,
    CONSTRAINT [PkTblGuildBattle] PRIMARY KEY CLUSTERED ([mGuildNo1] ASC, [mGuildNo2] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblGuildBattle_mGuildNo2]
    ON [dbo].[TblGuildBattle]([mGuildNo2] ASC) WITH (FILLFACTOR = 80);


GO

