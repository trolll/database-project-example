CREATE TABLE [dbo].[TblPcUnConfirmedCoin] (
    [mRegDate] DATETIME CONSTRAINT [DF_TblPcUnConfirmedCoin_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT      CONSTRAINT [DF_TblPcUnConfirmedCoin_mPcNo] DEFAULT ((0)) NOT NULL,
    [mCoin]    INT      CONSTRAINT [DF_TblPcUnConfirmedCoin_mCoin] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblPcUnConfirmedCoin] PRIMARY KEY CLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 80)
);


GO

