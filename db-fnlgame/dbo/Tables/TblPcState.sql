CREATE TABLE [dbo].[TblPcState] (
    [mNo]                INT       NOT NULL,
    [mLevel]             SMALLINT  NOT NULL,
    [mExp]               BIGINT    NOT NULL,
    [mHpAdd]             INT       NOT NULL,
    [mHp]                INT       NOT NULL,
    [mMpAdd]             INT       NOT NULL,
    [mMp]                INT       NOT NULL,
    [mMapNo]             INT       NOT NULL,
    [mPosX]              REAL      NOT NULL,
    [mPosY]              REAL      NOT NULL,
    [mPosZ]              REAL      NOT NULL,
    [mStomach]           SMALLINT  CONSTRAINT [DF_TblPcState_mStomach] DEFAULT ((100)) NOT NULL,
    [mIp]                CHAR (15) NULL,
    [mLoginTm]           DATETIME  NULL,
    [mLogoutTm]          DATETIME  NULL,
    [mTotUseTm]          INT       CONSTRAINT [DF_TblPcState_mTotUseTm] DEFAULT ((0)) NOT NULL,
    [mPkCnt]             INT       CONSTRAINT [DF_TblPcState_mPkCnt] DEFAULT ((0)) NOT NULL,
    [mChaotic]           INT       CONSTRAINT [DF_TblPcState_mChaotic] DEFAULT ((0)) NOT NULL,
    [mDiscipleJoinCount] INT       CONSTRAINT [DF__TblPcStat__mDiscipleJoinCount] DEFAULT ((3)) NOT NULL,
    [mPartyMemCntLevel]  INT       CONSTRAINT [DF__TblPcStat__mPart__1E8F7FEF] DEFAULT ((0)) NOT NULL,
    [mLostExp]           BIGINT    CONSTRAINT [DF_TblPcState_mLostExp] DEFAULT ((0)) NOT NULL,
    [mIsLetterLimit]     BIT       CONSTRAINT [DF_TblPcState_mIsLetterLimit] DEFAULT ((0)) NOT NULL,
    [mFlag]              SMALLINT  CONSTRAINT [DF_TblPcState_mFlag] DEFAULT ((0)) NOT NULL,
    [mIsPreventItemDrop] BIT       CONSTRAINT [DF_TblPcState_mIsPreventItemDrop] DEFAULT ((0)) NULL,
    [mSkillTreePoint]    SMALLINT  CONSTRAINT [DF_TblPcState_mSkillTreePoint] DEFAULT ((0)) NOT NULL,
    [mRestExpGuild]      BIGINT    CONSTRAINT [DF_TblPcState_mRestExp] DEFAULT ((0)) NOT NULL,
    [mRestExpActivate]   BIGINT    CONSTRAINT [DF_TblPcState_mRestExpActivate] DEFAULT ((0)) NOT NULL,
    [mRestExpDeactivate] BIGINT    CONSTRAINT [DF_TblPcState_mRestExpDeactivate] DEFAULT ((0)) NOT NULL,
    [mQMCnt]             SMALLINT  CONSTRAINT [DF_TblPcState_mQMCnt] DEFAULT ((0)) NOT NULL,
    [mGuildQMCnt]        SMALLINT  CONSTRAINT [DF_TblPcState_mGuildQMCnt] DEFAULT ((0)) NOT NULL,
    [mFierceCnt]         SMALLINT  CONSTRAINT [DF_TblPcState_mFierceCnt] DEFAULT ((0)) NOT NULL,
    [mBossCnt]           SMALLINT  CONSTRAINT [DF_TblPcState_mBossCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PkTblPcState] PRIMARY KEY CLUSTERED ([mNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK_TblPcState_mLevel] CHECK ((0)<=[mLevel] AND [mLevel]<=(200)),
    CONSTRAINT [FkTblPcStateTblPc] FOREIGN KEY ([mNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒饭唱 焊胶傈 曼咯冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mBossCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'酒饭唱 拜傈 曼咯冉荐', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcState', @level2type = N'COLUMN', @level2name = N'mFierceCnt';


GO

