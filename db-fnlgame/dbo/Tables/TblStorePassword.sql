CREATE TABLE [dbo].[TblStorePassword] (
    [mRegDate]  DATETIME CONSTRAINT [DF_TblStorePassword_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mUserNo]   INT      NOT NULL,
    [mPassword] CHAR (8) NOT NULL,
    CONSTRAINT [CL_PK_TblStorePassword] PRIMARY KEY CLUSTERED ([mUserNo] ASC) WITH (FILLFACTOR = 90)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'蜡历锅龋', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStorePassword', @level2type = N'COLUMN', @level2name = N'mUserNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'芒绊 菩胶况靛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStorePassword';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'菩胶况靛', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStorePassword', @level2type = N'COLUMN', @level2name = N'mPassword';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'殿废老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblStorePassword', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

