CREATE TABLE [dbo].[TblGuildAssMem] (
    [mRegDate]    SMALLDATETIME CONSTRAINT [DF__TblGuildA__mRegD__58DCE86C] DEFAULT (getdate()) NOT NULL,
    [mGuildAssNo] INT           NOT NULL,
    [mGuildNo]    INT           NOT NULL,
    CONSTRAINT [PkTblGuildAssMem] PRIMARY KEY NONCLUSTERED ([mGuildNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblGuildAssMemTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo]),
    CONSTRAINT [FkTblGuildAssMemTblGuildAss] FOREIGN KEY ([mGuildAssNo]) REFERENCES [dbo].[TblGuildAss] ([mGuildNo])
);


GO

CREATE CLUSTERED INDEX [IxTblGuildAssMemGuildAssNo]
    ON [dbo].[TblGuildAssMem]([mGuildAssNo] ASC) WITH (FILLFACTOR = 80);


GO

