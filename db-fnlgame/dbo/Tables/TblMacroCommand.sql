CREATE TABLE [dbo].[TblMacroCommand] (
    [mPcNo]    INT     NOT NULL,
    [mMacroID] TINYINT NOT NULL,
    [mOrderNo] TINYINT NOT NULL,
    [mRefType] TINYINT NOT NULL,
    [mRefID]   INT     NOT NULL,
    [mRefParm] BIGINT  NOT NULL,
    CONSTRAINT [UCL_PK_TblMacroCommand] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mMacroID] ASC, [mOrderNo] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä¿¸Çµå ÂüÁ¶ Å¸ÀÔ(¾ÆÀÌÅÛ,½ºÅ³)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroCommand', @level2type = N'COLUMN', @level2name = N'mRefType';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä¿¸Çµå ÂüÁ¶ Ãß°¡ Á¤º¸', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroCommand', @level2type = N'COLUMN', @level2name = N'mRefParm';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î ¼ÒÀ¯ Ä³¸¯ÅÍ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroCommand', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä¿¸Çµå ½ÇÇà ¼ø¼­', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroCommand', @level2type = N'COLUMN', @level2name = N'mOrderNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ä¿¸Çµå ÂüÁ¶ ¾ÆÀÌµð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroCommand', @level2type = N'COLUMN', @level2name = N'mRefID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î ¾ÆÀÌµð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroCommand', @level2type = N'COLUMN', @level2name = N'mMacroID';


GO

