CREATE TABLE [dbo].[TblCalendarScheduleGroup] (
    [mRegDate]   SMALLDATETIME NOT NULL,
    [mOwnerPcNo] INT           NOT NULL,
    [mSerialNo]  BIGINT        NOT NULL,
    [mGroupNo]   TINYINT       NOT NULL,
    CONSTRAINT [UCL_PK_TblCalendarScheduleGroup] PRIMARY KEY CLUSTERED ([mOwnerPcNo] ASC, [mSerialNo] ASC, [mGroupNo] ASC)
);


GO

CREATE NONCLUSTERED INDEX [NCL_TblCalendarSchedulegroup_mSerialNo]
    ON [dbo].[TblCalendarScheduleGroup]([mSerialNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'±×·ì¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarScheduleGroup', @level2type = N'COLUMN', @level2name = N'mGroupNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'µî·ÏÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarScheduleGroup', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÀÏÁ¤¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarScheduleGroup', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'ÀÏÁ¤±×·ì', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarScheduleGroup';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ÄÉ¸¯ÅÍ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblCalendarScheduleGroup', @level2type = N'COLUMN', @level2name = N'mOwnerPcNo';


GO

