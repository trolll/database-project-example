CREATE TABLE [dbo].[TblBoard] (
    [mRegDate]  DATETIME      NOT NULL,
    [mBoardId]  TINYINT       NOT NULL,
    [mBoardNo]  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mFromPcNm] CHAR (12)     NOT NULL,
    [mTitle]    CHAR (30)     NOT NULL,
    [mMsg]      VARCHAR (512) NOT NULL,
    CONSTRAINT [PkTblBoard] PRIMARY KEY CLUSTERED ([mBoardId] ASC, [mBoardNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [CK__TblBoard__mBoard__403A8C7D] CHECK ((0)<=[mBoardId]),
    CONSTRAINT [CK__TblBoard__mBoard__412EB0B6] CHECK ((0)<[mBoardNo])
);


GO

