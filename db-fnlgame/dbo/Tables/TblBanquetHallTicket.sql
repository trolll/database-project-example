CREATE TABLE [dbo].[TblBanquetHallTicket] (
    [mTicketSerialNo]  BIGINT        NOT NULL,
    [mTerritory]       INT           NOT NULL,
    [mBanquetHallType] INT           NOT NULL,
    [mBanquetHallNo]   INT           NOT NULL,
    [mOwnerPcNo]       INT           NOT NULL,
    [mRegDate]         SMALLDATETIME CONSTRAINT [DF_TblBanquetHallTicket_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mFromPcNm]        NVARCHAR (12) CONSTRAINT [DF_TblBanquetHallTicket_mFromPcNm] DEFAULT ('') NOT NULL,
    [mToPcNm]          NVARCHAR (12) CONSTRAINT [DF_TblBanquetHallTicket_mToPcNm] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_TblBanquetHallTicket] PRIMARY KEY CLUSTERED ([mTicketSerialNo] ASC) WITH (FILLFACTOR = 80)
);


GO

