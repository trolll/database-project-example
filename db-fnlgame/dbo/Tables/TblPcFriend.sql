CREATE TABLE [dbo].[TblPcFriend] (
    [mRegDate]    SMALLDATETIME CONSTRAINT [DF__TblPcFrie__mRegD__6DD739FB] DEFAULT (getdate()) NOT NULL,
    [mOwnerPcNo]  INT           NOT NULL,
    [mFriendPcNo] INT           NOT NULL,
    [mFriendPcNm] CHAR (12)     NOT NULL,
    CONSTRAINT [PK_CL_TblPcFriend] PRIMARY KEY CLUSTERED ([mOwnerPcNo] ASC, [mFriendPcNo] ASC, [mFriendPcNm] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FkTblPcFriendTblPc] FOREIGN KEY ([mOwnerPcNo]) REFERENCES [dbo].[TblPc] ([mNo])
);


GO

CREATE NONCLUSTERED INDEX [NC_TblPcFriend_mFriendPcNm]
    ON [dbo].[TblPcFriend]([mFriendPcNm] ASC) WITH (FILLFACTOR = 80);


GO

