CREATE TABLE [dbo].[TblQuickSupplicationReply] (
    [mRegDate] DATETIME       CONSTRAINT [DF__TblQuickS__mRegD__47127295] DEFAULT (getdate()) NOT NULL,
    [mQSID]    BIGINT         NOT NULL,
    [mReply]   VARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_CL_TblQuickSupplicationReply] PRIMARY KEY CLUSTERED ([mQSID] ASC) WITH (FILLFACTOR = 80)
);


GO

