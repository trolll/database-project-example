CREATE TABLE [dbo].[TblPetitionBoard] (
    [mPID]       BIGINT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [mCategory]  INT         NOT NULL,
    [mRegDate]   DATETIME    NOT NULL,
    [mPcNo]      INT         NOT NULL,
    [mPosX]      FLOAT (53)  NOT NULL,
    [mPosY]      FLOAT (53)  NOT NULL,
    [mPosZ]      FLOAT (53)  NOT NULL,
    [mText]      CHAR (1000) NOT NULL,
    [mIpAddress] CHAR (20)   NOT NULL,
    [mIsFin]     BIT         CONSTRAINT [DF_TblPetitionBoard_mIsFin] DEFAULT ((0)) NOT NULL,
    [mFinDate]   DATETIME    CONSTRAINT [DF_TblPetitionBoard_mFinDate] DEFAULT (NULL) NULL,
    CONSTRAINT [PK_TblPetitionBoard] PRIMARY KEY CLUSTERED ([mPID] ASC, [mCategory] ASC, [mRegDate] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblPetitionBoard_1]
    ON [dbo].[TblPetitionBoard]([mRegDate] ASC, [mCategory] ASC, [mIsFin] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [NC_TblPetitionBoard_2]
    ON [dbo].[TblPetitionBoard]([mCategory] ASC, [mPcNo] ASC) WITH (FILLFACTOR = 80);


GO

