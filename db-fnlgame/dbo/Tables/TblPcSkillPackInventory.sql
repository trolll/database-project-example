CREATE TABLE [dbo].[TblPcSkillPackInventory] (
    [mRegDate] DATETIME      CONSTRAINT [DF_TblPcSkillPackInventory_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT           NOT NULL,
    [mSPID]    INT           NOT NULL,
    [mEndDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [UCL_PK_TblPcSkillPackInventory_mPcNo_mSPID] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mSPID] ASC) WITH (FILLFACTOR = 80)
);


GO

