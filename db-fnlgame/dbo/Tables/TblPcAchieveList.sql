CREATE TABLE [dbo].[TblPcAchieveList] (
    [mID]          INT      IDENTITY (1, 1) NOT NULL,
    [mRegDate]     DATETIME CONSTRAINT [DF_TblPcAchieveList_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]        INT      CONSTRAINT [DF_TblPcAchieveList_mPcNo] DEFAULT ((0)) NOT NULL,
    [mAchieveID]   TINYINT  CONSTRAINT [DF_TblPcAchieveList_mAchieveID] DEFAULT ((0)) NOT NULL,
    [mActionCount] SMALLINT CONSTRAINT [DF_TblPcAchieveList_mActionCount] DEFAULT ((0)) NOT NULL,
    [mIsComplete]  BIT      CONSTRAINT [DF_TblPcAchieveList_mIsComplete] DEFAULT ((0)) NOT NULL,
    [mIsNew]       BIT      CONSTRAINT [DF_TblPcAchieveList_mIsNew] DEFAULT ((0)) NOT NULL,
    [mSerialNo]    BIGINT   CONSTRAINT [DF_TblPcAchieveList_mSerialNo] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblPcAchieveList] PRIMARY KEY CLUSTERED ([mID] ASC) WITH (FILLFACTOR = 80)
);


GO

