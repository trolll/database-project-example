CREATE TABLE [dbo].[TblMacroList] (
    [mPcNo]      INT         NOT NULL,
    [mMacroID]   TINYINT     NOT NULL,
    [mMacroSlot] TINYINT     NOT NULL,
    [mMacroName] VARCHAR (6) NOT NULL,
    [mIconNo]    TINYINT     NOT NULL,
    CONSTRAINT [UCL_PK_TblMacroList] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mMacroID] ASC, [mMacroSlot] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î ¾ÆÀÌµð', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroList', @level2type = N'COLUMN', @level2name = N'mMacroID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î ÀÌ¸§', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroList', @level2type = N'COLUMN', @level2name = N'mMacroName';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î ¼ÒÀ¯ Ä³¸¯ÅÍ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroList', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î À§Ä¡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroList', @level2type = N'COLUMN', @level2name = N'mMacroSlot';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¸ÅÅ©·Î ¾ÆÀÌÄÜ ¹øÈ£', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblMacroList', @level2type = N'COLUMN', @level2name = N'mIconNo';


GO

