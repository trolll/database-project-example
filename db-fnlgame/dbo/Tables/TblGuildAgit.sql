CREATE TABLE [dbo].[TblGuildAgit] (
    [mTerritory]     INT           NOT NULL,
    [mGuildAgitNo]   INT           NOT NULL,
    [mOwnerGID]      INT           CONSTRAINT [DF_TblGuildAgit_mOwnerGID] DEFAULT ((0)) NOT NULL,
    [mGuildAgitName] NVARCHAR (50) CONSTRAINT [DF_TblGuildAgit_mGuildAgitName] DEFAULT ('') NOT NULL,
    [mRegDate]       SMALLDATETIME CONSTRAINT [DF_TblGuildAgit_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mLeftMin]       INT           CONSTRAINT [DF_TblGuildAgit_mLeftMin] DEFAULT ((0)) NOT NULL,
    [mIsSelling]     TINYINT       CONSTRAINT [DF_TblGuildAgit_mIsSelling] DEFAULT ((0)) NOT NULL,
    [mSellingMoney]  BIGINT        CONSTRAINT [DF_TblGuildAgit_mSellingMoney] DEFAULT ((0)) NOT NULL,
    [mBuyingMoney]   BIGINT        CONSTRAINT [DF_TblGuildAgit_mBuyingMoney] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblGuildAgit] PRIMARY KEY CLUSTERED ([mTerritory] ASC, [mGuildAgitNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [IX_TblGuildAgit]
    ON [dbo].[TblGuildAgit]([mOwnerGID] ASC) WITH (FILLFACTOR = 80);


GO

