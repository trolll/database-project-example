CREATE TABLE [dbo].[TblStatisticsMonsterHunting] (
    [mRegDate]    DATETIME CONSTRAINT [DF__TblStatis__mRegD__670A40DB] DEFAULT (getdate()) NOT NULL,
    [MID]         INT      NOT NULL,
    [mHuntingCnt] BIGINT   NULL,
    CONSTRAINT [CL_PKTblStatisticsMonsterHunting] PRIMARY KEY CLUSTERED ([MID] ASC) WITH (FILLFACTOR = 80)
);


GO

