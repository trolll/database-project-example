CREATE TABLE [dbo].[TblPcServantSub] (
    [mRegDate]  DATETIME CONSTRAINT [DF_TblPcServantSub_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]     INT      NOT NULL,
    [mSerialNo] BIGINT   NOT NULL,
    [mSubIndex] TINYINT  NOT NULL,
    CONSTRAINT [CL_PK_TblPcServantSub] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mSubIndex] ASC),
    CONSTRAINT [CL_FK_TblPcServantSub] FOREIGN KEY ([mSerialNo]) REFERENCES [dbo].[TblPcInventory] ([mSerialNo]) ON DELETE CASCADE
);


GO

CREATE NONCLUSTERED INDEX [IX_TblPCServantSub_mSerialNo]
    ON [dbo].[TblPcServantSub]([mSerialNo] ASC);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'积己老', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSub', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑宏 辑锅飘 困摹', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSub', @level2type = N'COLUMN', @level2name = N'mSubIndex';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'家蜡磊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSub', @level2type = N'COLUMN', @level2name = N'mPcNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'辑宏 辑锅飘 厘馒', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSub';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'辑宏 辑锅飘 矫府倔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSub', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

