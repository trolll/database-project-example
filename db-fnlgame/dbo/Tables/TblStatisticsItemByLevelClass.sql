CREATE TABLE [dbo].[TblStatisticsItemByLevelClass] (
    [mRegDate]          DATETIME CONSTRAINT [DF__TblStatis__mRegD__03275C9C] DEFAULT (getdate()) NOT NULL,
    [mItemNo]           INT      NOT NULL,
    [mLevel]            SMALLINT NOT NULL,
    [mClass]            SMALLINT NOT NULL,
    [mMerchantCreate]   BIGINT   NULL,
    [mMerchantDelete]   BIGINT   NULL,
    [mReinforceCreate]  BIGINT   NULL,
    [mReinforceDelete]  BIGINT   NULL,
    [mCraftingCreate]   BIGINT   NULL,
    [mCraftingDelete]   BIGINT   NULL,
    [mPcUseDelete]      BIGINT   NULL,
    [mNpcUseDelete]     BIGINT   NULL,
    [mNpcCreate]        BIGINT   NULL,
    [mMonsterDrop]      BIGINT   NULL,
    [mGSExchangeCreate] BIGINT   NULL,
    [mGSExchangeDelete] BIGINT   NULL,
    [mItemStatus]       SMALLINT CONSTRAINT [DF_TblStatisticsItemByLevelClass_mItemStatus] DEFAULT ((0)) NOT NULL,
    [mCnsmRegFee]       BIGINT   CONSTRAINT [DF_TblStatisticsItemByLevelClass_mCnsmRegFee] DEFAULT ((0)) NOT NULL,
    [mCnsmBuyFee]       BIGINT   CONSTRAINT [DF_TblStatisticsItemByLevelClass_mCnsmBuyFee] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_PKTblStatisticsItemByLevelClass_mItemNo_mLevel_mClass_mItemStatus] PRIMARY KEY CLUSTERED ([mItemNo] ASC, [mLevel] ASC, [mClass] ASC, [mItemStatus] ASC) WITH (FILLFACTOR = 80)
);


GO

