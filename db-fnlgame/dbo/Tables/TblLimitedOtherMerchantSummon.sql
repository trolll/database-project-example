CREATE TABLE [dbo].[TblLimitedOtherMerchantSummon] (
    [mMerchantID]    INT     NOT NULL,
    [mAbleSummonCnt] TINYINT NOT NULL,
    CONSTRAINT [UCL_PK_TblLimitedOtherMerchantSummon] PRIMARY KEY CLUSTERED ([mMerchantID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'Caption', @value = N'ÀÌ°è »óÀÎ ¼ÒÈ¯ È½¼ö', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedOtherMerchantSummon';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼ÒÈ¯ °¡´ÉÇÑ È½¼ö', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedOtherMerchantSummon', @level2type = N'COLUMN', @level2name = N'mAbleSummonCnt';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»óÀÎID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblLimitedOtherMerchantSummon', @level2type = N'COLUMN', @level2name = N'mMerchantID';


GO

