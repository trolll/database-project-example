CREATE TABLE [dbo].[TblPcSkillTreeInventory] (
    [mRegDate] DATETIME      CONSTRAINT [DF_TblPcSkillTreeInventory_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]    INT           NOT NULL,
    [mSTNIID]  INT           NOT NULL,
    [mEndDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [UCL_PK_TblPcSkillTreeInventory_mPcNo_mSTNIID] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mSTNIID] ASC) WITH (FILLFACTOR = 80)
);


GO

