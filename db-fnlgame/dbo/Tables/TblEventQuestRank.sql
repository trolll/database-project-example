CREATE TABLE [dbo].[TblEventQuestRank] (
    [mQuestNo] INT     NOT NULL,
    [mRank]    TINYINT NOT NULL,
    [mPcNo]    INT     NOT NULL,
    CONSTRAINT [UCL_PK_TblEventQuestRank_mQuestNo] PRIMARY KEY CLUSTERED ([mQuestNo] ASC, [mRank] ASC) WITH (FILLFACTOR = 80)
);


GO

