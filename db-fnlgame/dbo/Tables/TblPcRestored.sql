CREATE TABLE [dbo].[TblPcRestored] (
    [mRegDate]            DATETIME     CONSTRAINT [DF_TblPcRestored_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mPcNo]               INT          NOT NULL,
    [mPcNm]               CHAR (12)    NULL,
    [mPcRestoredOperator] VARCHAR (20) NULL,
    CONSTRAINT [PK_TblPcRestored] PRIMARY KEY CLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 80)
);


GO

