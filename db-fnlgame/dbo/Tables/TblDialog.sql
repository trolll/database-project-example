CREATE TABLE [dbo].[TblDialog] (
    [mRegDate]  SMALLDATETIME  CONSTRAINT [DF_TblDialog_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mMId]      INT            NOT NULL,
    [mClick]    VARCHAR (6000) NOT NULL,
    [mDie]      VARCHAR (100)  NOT NULL,
    [mAttacked] VARCHAR (100)  NOT NULL,
    [mTarget]   VARCHAR (100)  NOT NULL,
    [mBear]     VARCHAR (100)  NOT NULL,
    [mGossip1]  VARCHAR (100)  NOT NULL,
    [mGossip2]  VARCHAR (100)  NOT NULL,
    [mGossip3]  VARCHAR (100)  NOT NULL,
    [mGossip4]  VARCHAR (100)  NOT NULL,
    CONSTRAINT [PK_TblDialog] PRIMARY KEY CLUSTERED ([mMId] ASC) WITH (FILLFACTOR = 80)
);


GO

