CREATE TABLE [dbo].[TblConsignmentAccount] (
    [mPcNo]    INT    NOT NULL,
    [mBalance] BIGINT NOT NULL,
    CONSTRAINT [UCL_PK_TblConsignmentAccount_mPcNo] PRIMARY KEY CLUSTERED ([mPcNo] ASC) WITH (FILLFACTOR = 80),
    CHECK ((0)<=[mBalance]),
    CHECK ((0)<=[mBalance])
);


GO

