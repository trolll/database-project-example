CREATE TABLE [dbo].[TblPcBead] (
    [mRegDate]       DATETIME      NOT NULL,
    [mOwnerSerialNo] BIGINT        NOT NULL,
    [mBeadIndex]     TINYINT       NOT NULL,
    [mItemNo]        INT           NOT NULL,
    [mEndDate]       SMALLDATETIME NOT NULL,
    [mCntUse]        SMALLINT      NOT NULL,
    CONSTRAINT [UNC_TblPcBead_mOwnerSerialNo_mBeadIndex] UNIQUE NONCLUSTERED ([mOwnerSerialNo] ASC, [mBeadIndex] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UNC_TblPcBead_mOwnerSerialNo_mItemNo] UNIQUE NONCLUSTERED ([mOwnerSerialNo] ASC, [mItemNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE CLUSTERED INDEX [CL_TblPcBead_mOwnerSerialNo]
    ON [dbo].[TblPcBead]([mOwnerSerialNo] ASC) WITH (FILLFACTOR = 80);


GO

