CREATE TABLE [dbo].[TblPcPopupGuide] (
    [mPcNo]    INT NOT NULL,
    [mGuideNo] INT NOT NULL,
    [mReadCnt] INT CONSTRAINT [DF_TblPcPopupGuide_mReadCnt] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_TblPcPopupGuide_UnitedKey] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mGuideNo] ASC) WITH (FILLFACTOR = 80)
);


GO

