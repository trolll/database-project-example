CREATE TABLE [dbo].[TblPcCoolTime] (
    [mPcNo]          INT      NOT NULL,
    [mSID]           INT      NOT NULL,
    [mCoolTimeGroup] SMALLINT NOT NULL,
    [mRemainTime]    INT      NOT NULL,
    CONSTRAINT [UCL_TblPcCoolTime] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mCoolTimeGroup] ASC) WITH (FILLFACTOR = 80)
);


GO

