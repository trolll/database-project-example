CREATE TABLE [dbo].[TblPcAchieveEquip] (
    [mSerialNo] BIGINT CONSTRAINT [DF_TblPcAchieveEquip_mSerialNo] DEFAULT ((0)) NOT NULL,
    [mPcNo]     INT    CONSTRAINT [DF_TblPcAchieveEquip_mPcNo] DEFAULT ((0)) NOT NULL,
    [mItemNo]   INT    CONSTRAINT [DF_TblPcAchieveEquip_mItemNo] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TblPcAchieveEquip] PRIMARY KEY CLUSTERED ([mSerialNo] ASC) WITH (FILLFACTOR = 80)
);


GO

