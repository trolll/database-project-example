CREATE TABLE [dbo].[TblStatisticsItemByCase] (
    [mRegDate]          DATETIME CONSTRAINT [DF__TblStatis__mRegD__61516785] DEFAULT (getdate()) NOT NULL,
    [mItemNo]           INT      NOT NULL,
    [mMerchantCreate]   BIGINT   NULL,
    [mMerchantDelete]   BIGINT   NULL,
    [mReinforceCreate]  BIGINT   NULL,
    [mReinforceDelete]  BIGINT   NULL,
    [mCraftingCreate]   BIGINT   NULL,
    [mCraftingDelete]   BIGINT   NULL,
    [mPcUseDelete]      BIGINT   NULL,
    [mNpcUseDelete]     BIGINT   NULL,
    [mNpcCreate]        BIGINT   NULL,
    [mMonsterDrop]      BIGINT   NULL,
    [mGSExchangeCreate] BIGINT   NULL,
    [mGSExchangeDelete] BIGINT   NULL,
    CONSTRAINT [CL_PKTblStatisticsItemByCase] PRIMARY KEY CLUSTERED ([mItemNo] ASC) WITH (FILLFACTOR = 80)
);


GO

