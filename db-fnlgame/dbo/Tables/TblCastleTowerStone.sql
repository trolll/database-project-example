CREATE TABLE [dbo].[TblCastleTowerStone] (
    [mRegDate]             SMALLDATETIME CONSTRAINT [DF__TblCastle__mRegD__7BD11EEE] DEFAULT (getdate()) NOT NULL,
    [mChgDate]             SMALLDATETIME CONSTRAINT [DF__TblCastle__mChgD__7CC54327] DEFAULT (getdate()) NOT NULL,
    [mIsTower]             BIT           CONSTRAINT [DF_TblCastleTowerStone_mIsTower] DEFAULT ((0)) NOT NULL,
    [mPlace]               INT           NOT NULL,
    [mGuildNo]             INT           NOT NULL,
    [mTaxBuy]              INT           NOT NULL,
    [mTaxBuyMax]           INT           NOT NULL,
    [mTaxHunt]             INT           NOT NULL,
    [mTaxHuntMax]          INT           NOT NULL,
    [mTaxGamble]           INT           NOT NULL,
    [mTaxGambleMax]        INT           NOT NULL,
    [mAsset]               BIGINT        NOT NULL,
    [mAssetBuy]            BIGINT        CONSTRAINT [DF_TblCastleTowerStone_mAssetBuy] DEFAULT ((0)) NOT NULL,
    [mAssetHunt]           BIGINT        CONSTRAINT [DF_TblCastleTowerStone_mAssetHunt] DEFAULT ((0)) NOT NULL,
    [mAssetGamble]         BIGINT        CONSTRAINT [DF_TblCastleTowerStone_mAssetGamble] DEFAULT ((0)) NOT NULL,
    [mSiegeDate]           DATETIME      CONSTRAINT [DF_TblCastleTowerStone_mSiegeDate] DEFAULT ((0)) NULL,
    [mSiegeDayOfWeek]      SMALLINT      CONSTRAINT [DF_TblCastleTowerStone_mSiegeDateLimit] DEFAULT ((0)) NOT NULL,
    [mSiegeDayOfWeekLimit] SMALLINT      CONSTRAINT [DF_TblCastleTowerStone_mSiegeDayOfWeekLimit] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [CK__TblCastle__mAsse__037240B6] CHECK ((0)<=[mAsset]),
    CONSTRAINT [CK__TblCastle__mTaxB__7DB96760] CHECK ((0)<=[mTaxBuy]),
    CONSTRAINT [CK__TblCastle__mTaxB__7EAD8B99] CHECK ((0)<=[mTaxBuyMax]),
    CONSTRAINT [CK__TblCastle__mTaxG__0189F844] CHECK ((0)<=[mTaxGamble]),
    CONSTRAINT [CK__TblCastle__mTaxG__027E1C7D] CHECK ((0)<=[mTaxGambleMax]),
    CONSTRAINT [CK__TblCastle__mTaxH__0095D40B] CHECK ((0)<=[mTaxHuntMax]),
    CONSTRAINT [CK__TblCastle__mTaxH__7FA1AFD2] CHECK ((0)<=[mTaxHunt]),
    CONSTRAINT [FkTblCastleTowerStoneTblGuild] FOREIGN KEY ([mGuildNo]) REFERENCES [dbo].[TblGuild] ([mGuildNo])
);


GO

CREATE NONCLUSTERED INDEX [IxTblCastleTowerStoneGuildNo]
    ON [dbo].[TblCastleTowerStone]([mGuildNo] ASC) WITH (FILLFACTOR = 80);


GO

CREATE CLUSTERED INDEX [IxTblCastleTowerStonePlace]
    ON [dbo].[TblCastleTowerStone]([mPlace] ASC) WITH (FILLFACTOR = 80);


GO

