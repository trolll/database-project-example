CREATE TABLE [dbo].[TblDepository] (
    [mDepositoryNo]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [mRegNo]               BIGINT        NOT NULL,
    [mRegDate]             DATETIME      CONSTRAINT [DF__TblDeposi__mRegD__34FEAF52] DEFAULT (getdate()) NOT NULL,
    [mSerialNo]            BIGINT        NOT NULL,
    [mNo]                  INT           NOT NULL,
    [mItemNo]              INT           NOT NULL,
    [mEndDate]             SMALLDATETIME NOT NULL,
    [mIsConfirm]           BIT           NOT NULL,
    [mStatus]              TINYINT       NOT NULL,
    [mCnt]                 INT           NOT NULL,
    [mCntUse]              TINYINT       NOT NULL,
    [mIsSeizure]           BIT           NOT NULL,
    [mApplyAbnItemNo]      INT           NOT NULL,
    [mApplyAbnItemEndDate] SMALLDATETIME NOT NULL,
    [mOwner]               INT           NOT NULL,
    [mPracticalPeriod]     INT           NOT NULL,
    [mBindingType]         TINYINT       NULL,
    CONSTRAINT [NC_PK_TblDepository_1] PRIMARY KEY NONCLUSTERED ([mDepositoryNo] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [FK_TblDepository_TblDepositoryOwner_mRegNo] FOREIGN KEY ([mRegNo]) REFERENCES [dbo].[TblDepositoryOwner] ([mRegNo])
);


GO

