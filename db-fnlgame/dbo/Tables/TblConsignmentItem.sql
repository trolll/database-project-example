CREATE TABLE [dbo].[TblConsignmentItem] (
    [mCnsmNo]          BIGINT   NOT NULL,
    [mRegDate]         DATETIME NOT NULL,
    [mSerialNo]        BIGINT   NOT NULL,
    [mEndDate]         DATETIME NOT NULL,
    [mIsConfirm]       BIT      NOT NULL,
    [mStatus]          TINYINT  NOT NULL,
    [mCntUse]          SMALLINT NULL,
    [mOwner]           INT      NOT NULL,
    [mPracticalPeriod] INT      NOT NULL,
    [mBindingType]     TINYINT  NOT NULL,
    [mRestoreCnt]      TINYINT  NOT NULL,
    [mHoleCount]       TINYINT  CONSTRAINT [DF_TblConsignmentItem_mHoleCount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [UCL_FK_TblConsignmentItem_mCnsmNo] PRIMARY KEY CLUSTERED ([mCnsmNo] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblConsignmentItem_mSerialNo]
    ON [dbo].[TblConsignmentItem]([mSerialNo] ASC) WITH (FILLFACTOR = 80);


GO

