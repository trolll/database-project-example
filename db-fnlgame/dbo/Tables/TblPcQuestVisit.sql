CREATE TABLE [dbo].[TblPcQuestVisit] (
    [mPcNo]    INT NOT NULL,
    [mQuestNo] INT NOT NULL,
    [mPlaceNo] INT NOT NULL,
    [mVisit]   BIT NOT NULL,
    CONSTRAINT [UCL_PK_TblPcQuestVisit_mPcNo] PRIMARY KEY CLUSTERED ([mPcNo] ASC, [mQuestNo] ASC, [mPlaceNo] ASC) WITH (FILLFACTOR = 80)
);


GO

