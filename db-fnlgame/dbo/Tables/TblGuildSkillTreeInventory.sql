CREATE TABLE [dbo].[TblGuildSkillTreeInventory] (
    [mRegDate]     DATETIME      CONSTRAINT [DF_TblGuildSkillTreeInventory_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mGuildNo]     INT           NOT NULL,
    [mSTNIID]      INT           NOT NULL,
    [mEndDate]     SMALLDATETIME NOT NULL,
    [mCreatorPcNo] INT           NOT NULL,
    [mExp]         BIGINT        NOT NULL,
    CONSTRAINT [UCL_PK_TblGuildSkillTreeInventory_mGuildNo_mSTNIID] PRIMARY KEY CLUSTERED ([mGuildNo] ASC, [mSTNIID] ASC) WITH (FILLFACTOR = 80)
);


GO

