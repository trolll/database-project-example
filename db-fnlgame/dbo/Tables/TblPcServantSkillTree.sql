CREATE TABLE [dbo].[TblPcServantSkillTree] (
    [mRegDate]  DATETIME      CONSTRAINT [DF_TblPcServantSkillTree_mRegDate] DEFAULT (getdate()) NOT NULL,
    [mSerialNo] BIGINT        NOT NULL,
    [mSTID]     INT           NOT NULL,
    [mSTNIID]   INT           NOT NULL,
    [mEndDate]  SMALLDATETIME NOT NULL,
    CONSTRAINT [UCL_PK_TblPcServantSkillTree] PRIMARY KEY CLUSTERED ([mSerialNo] ASC, [mSTNIID] ASC)
);


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³ Æ®¸® ³ëµå ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillTree', @level2type = N'COLUMN', @level2name = N'mSTNIID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'¼ÒÀ¯ ¼­¹øÆ® ½Ã¸®¾ó', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillTree', @level2type = N'COLUMN', @level2name = N'mSerialNo';


GO

EXECUTE sp_addextendedproperty @name = N'Capton', @value = N'¼­¹øÆ® ½ºÅ³Æ®¸®', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillTree';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Á¾·áÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillTree', @level2type = N'COLUMN', @level2name = N'mEndDate';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'½ºÅ³ Æ®¸® ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillTree', @level2type = N'COLUMN', @level2name = N'mSTID';


GO

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'»ý¼ºÀÏ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TblPcServantSkillTree', @level2type = N'COLUMN', @level2name = N'mRegDate';


GO

