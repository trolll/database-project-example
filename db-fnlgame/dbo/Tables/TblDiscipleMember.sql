CREATE TABLE [dbo].[TblDiscipleMember] (
    [mMaster]   INT      NOT NULL,
    [mDisciple] INT      NOT NULL,
    [mMemPoint] INT      CONSTRAINT [DF_TblDiscipleMember_mMemPoint] DEFAULT ((0)) NOT NULL,
    [mType]     TINYINT  CONSTRAINT [DF_TblDiscipleMember_mIsWait] DEFAULT ((3)) NOT NULL,
    [mRegDate]  DATETIME CONSTRAINT [DF_TblDiscipleMember_mRegDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [NC_PK_TblDiscipleMember_mDisciple] PRIMARY KEY NONCLUSTERED ([mDisciple] ASC) WITH (FILLFACTOR = 80)
);


GO

CREATE CLUSTERED INDEX [CL_TblDiscipleMember]
    ON [dbo].[TblDiscipleMember]([mMaster] ASC) WITH (FILLFACTOR = 80);


GO

