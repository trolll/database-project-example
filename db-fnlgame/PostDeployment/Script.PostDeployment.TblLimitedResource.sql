/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLGame1164
Source Table          : TblLimitedResource
Date                  : 2023-10-07 09:01:49
*/


INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2018-04-27 21:49:00.000', 998, 1, 1, 100.0, '2018-04-27 21:49:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2018-07-31 20:05:00.000', 999, 1, 1, 100.0, '2018-07-31 20:05:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-23 11:29:00.000', 1170, 40, 35, 100.0, '2020-02-01 20:11:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-23 11:30:00.000', 1171, 50, 44, 100.0, '2020-02-01 19:03:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:26:00.000', 1172, 20, 18, 100.0, '2020-01-31 23:28:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:26:00.000', 1173, 25, 20, 100.0, '2020-01-31 23:36:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:26:00.000', 1174, 40, 37, 100.0, '2020-02-01 19:12:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:26:00.000', 1175, 50, 43, 100.0, '2020-02-01 13:50:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:26:00.000', 1176, 20, 18, 100.0, '2020-01-24 11:36:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:26:00.000', 1177, 25, 20, 100.0, '2020-02-04 08:05:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:26:00.000', 1178, 5, 4, 100.0, '2020-01-24 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:26:00.000', 1179, 7, 7, 100.0, '2020-01-22 19:26:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:27:00.000', 1180, 10, 9, 100.0, '2020-02-01 19:12:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-23 11:30:00.000', 1181, 15, 14, 100.0, '2020-01-28 15:23:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:27:00.000', 1182, 5, 5, 100.0, '2020-01-22 19:27:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:27:00.000', 1183, 7, 6, 100.0, '2020-01-28 14:07:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:27:00.000', 1184, 15, 14, 100.0, '2020-01-30 11:18:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:28:00.000', 1185, 20, 20, 100.0, '2020-01-22 19:28:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:28:00.000', 1186, 7, 7, 100.0, '2020-01-22 19:28:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:28:00.000', 1187, 10, 10, 100.0, '2020-01-22 19:28:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:28:00.000', 1188, 20, 19, 100.0, '2020-01-29 14:21:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-01-22 19:28:00.000', 1189, 25, 25, 100.0, '2020-01-22 19:28:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-03 21:28:00.000', 1258, 1, 1, 100.0, '2020-02-03 21:28:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1259, 3, 3, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1260, 3, 3, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1261, 10, 10, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:17:00.000', 1262, 20, 20, 100.0, '2020-02-04 11:17:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:17:00.000', 1263, 20, 20, 100.0, '2020-02-04 11:17:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1264, 25, 25, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1265, 20, 20, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1266, 20, 20, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1267, 10, 10, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1268, 10, 10, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:17:00.000', 1269, 15, 15, 100.0, '2020-02-04 11:17:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:17:00.000', 1270, 20, 20, 100.0, '2020-02-04 11:17:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1271, 30, 30, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1272, 30, 30, 100.0, '2020-02-04 11:14:00.000', 0);
GO

INSERT INTO [dbo].[TblLimitedResource] ([mRegDate], [mResourceType], [mMaxCnt], [mRemainerCnt], [mRandomVal], [mUptDate], [mIsIncSys]) VALUES ('2020-02-04 11:14:00.000', 1273, 30, 30, 100.0, '2020-02-04 11:14:00.000', 0);
GO

