/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLGame1164
Source Table          : TblSiegeGambleState
Date                  : 2023-10-07 09:01:49
*/


INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-03 13:17:29.573', 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-07 21:00:06.330', 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-21 19:03:49.750', 1, 3, 1, 0, 0, 0, 0, 161, 0, 0, 0, 40, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-28 21:00:06.857', 1, 4, 1, 0, 0, 0, 209, 40, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-05 21:00:04.533', 1, 5, 1, 0, 0, 205, 209, 0, 0, 0, 6, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-12 21:00:05.937', 1, 6, 1, 0, 0, 205, 0, 0, 0, 0, 6, 0, 33);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-19 21:00:06.857', 1, 7, 1, 0, 0, 205, 0, 0, 0, 16, 6, 0, 33);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-26 21:00:07.320', 1, 8, 1, 0, 0, 205, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-02 21:00:07.700', 1, 9, 1, 0, 0, 205, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-09 21:00:07.763', 1, 10, 1, 0, 0, 205, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-16 21:00:07.263', 1, 11, 1, 0, 0, 205, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-23 21:00:06.727', 1, 12, 1, 0, 0, 205, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-30 21:00:07.103', 1, 13, 1, 0, 0, 205, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-07 21:00:06.607', 1, 14, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-14 21:01:08.017', 1, 15, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-21 21:00:06.090', 1, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-28 20:59:57.633', 1, 17, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-04 21:00:03.030', 1, 18, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-11 21:00:02.797', 1, 19, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-18 21:00:05.533', 1, 20, 1, 0, 0, 209, 0, 401, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-25 21:00:02.767', 1, 21, 1, 0, 0, 0, 0, 401, 209, 0, 401, 209, 4);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-01 21:00:02.903', 1, 22, 1, 0, 0, 209, 401, 401, 209, 401, 401, 209, 108);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-08 21:00:01.940', 1, 23, 1, 0, 0, 209, 0, 401, 209, 0, 0, 0, 13);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-15 21:00:02.070', 1, 24, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-22 21:00:03.423', 1, 25, 1, 0, 0, 0, 0, 401, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-29 21:00:05.460', 1, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-06 20:59:55.950', 1, 27, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-13 21:00:01.617', 1, 28, 1, 0, 0, 548, 0, 0, 401, 401, 401, 548, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-20 21:00:02.070', 1, 29, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-27 21:00:01.973', 1, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-03 21:01:05.057', 1, 31, 1, 0, 0, 0, 0, 548, 0, 401, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-10 21:00:04.517', 1, 32, 1, 0, 0, 0, 0, 548, 0, 0, 0, 0, 4);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-17 21:00:04.673', 1, 33, 1, 0, 0, 548, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-24 21:00:03.093', 1, 34, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-01 21:01:02.450', 1, 35, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-08 21:00:01.847', 1, 36, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-15 21:00:02.017', 1, 37, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-22 21:00:07.023', 1, 38, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-29 21:01:02.197', 1, 39, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-05 21:00:04.840', 1, 40, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-12 21:00:02.080', 1, 41, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-19 21:00:04.760', 1, 42, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-26 21:00:05.060', 1, 43, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-02 21:00:04.387', 1, 44, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-09 20:59:54.853', 1, 45, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-16 21:01:02.147', 1, 46, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-23 21:00:03.010', 1, 47, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-02 21:00:02.440', 1, 48, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-09 21:00:02.250', 1, 49, 1, 0, 0, 401, 209, 401, 401, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-16 21:00:02.693', 1, 50, 1, 0, 0, 401, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-23 21:00:02.140', 1, 51, 1, 0, 0, 401, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-30 21:00:02.353', 1, 52, 1, 0, 0, 401, 747, 0, 0, 0, 747, 401, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-06 21:00:02.387', 1, 53, 1, 0, 0, 401, 401, 0, 804, 0, 747, 401, 97);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-13 21:00:03.120', 1, 54, 1, 0, 0, 401, 401, 0, 0, 0, 747, 0, 35);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-20 21:00:02.097', 1, 55, 1, 0, 0, 401, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-27 21:00:04.857', 1, 56, 1, 0, 0, 401, 0, 0, 0, 401, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-04 21:00:02.177', 1, 57, 1, 0, 0, 0, 0, 0, 401, 959, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-11 21:01:01.897', 1, 58, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-18 21:00:05.340', 1, 59, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-25 21:00:05.687', 1, 60, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-01 21:00:05.547', 1, 61, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-08 21:01:01.597', 1, 62, 1, 0, 0, 401, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-15 21:00:02.050', 1, 63, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-22 21:00:01.947', 1, 64, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-29 21:00:02.210', 1, 65, 1, 0, 0, 401, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-06 21:01:02.637', 1, 66, 1, 0, 0, 401, 0, 0, 0, 747, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-13 21:00:02.323', 1, 67, 1, 0, 0, 401, 401, 0, 0, 747, 401, 401, 17);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-20 21:00:01.450', 1, 68, 1, 0, 0, 401, 401, 747, 0, 747, 401, 401, 115);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-27 21:00:02.147', 1, 69, 1, 0, 0, 401, 401, 747, 0, 747, 401, 401, 119);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-03 21:01:02.600', 1, 70, 1, 0, 0, 401, 0, 0, 747, 0, 401, 0, 33);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-10 21:00:02.200', 1, 71, 1, 0, 0, 401, 0, 0, 747, 0, 401, 0, 41);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-17 21:00:02.380', 1, 72, 1, 0, 0, 401, 401, 0, 747, 0, 401, 0, 41);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-24 21:00:05.673', 1, 73, 1, 0, 0, 401, 401, 0, 747, 0, 401, 0, 43);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-31 21:01:02.377', 1, 74, 1, 0, 0, 401, 401, 0, 747, 0, 401, 0, 43);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-07 21:00:04.543', 1, 75, 1, 0, 0, 401, 401, 0, 747, 0, 401, 0, 43);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-14 21:00:05.293', 1, 76, 1, 0, 0, 401, 401, 747, 747, 0, 401, 747, 43);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-21 21:00:04.373', 1, 77, 1, 0, 0, 401, 401, 747, 747, 0, 401, 747, 111);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-28 21:00:05.803', 1, 78, 1, 0, 0, 401, 401, 747, 747, 0, 401, 747, 111);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-05 21:00:56.893', 1, 79, 1, 0, 0, 401, 401, 747, 747, 0, 401, 747, 111);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-12 21:00:02.310', 1, 80, 1, 0, 0, 401, 401, 747, 747, 0, 401, 747, 111);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-19 21:00:02.893', 1, 81, 1, 0, 0, 401, 401, 0, 0, 0, 401, 0, 35);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-26 21:00:02.847', 1, 82, 1, 0, 0, 401, 401, 0, 0, 0, 401, 0, 35);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-02 21:01:02.610', 1, 83, 1, 0, 0, 401, 401, 0, 0, 0, 401, 0, 35);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-09 21:00:05.017', 1, 84, 1, 0, 0, 401, 401, 0, 0, 0, 401, 0, 35);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-16 21:00:01.800', 1, 85, 1, 0, 0, 401, 401, 0, 0, 0, 401, 0, 35);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-23 21:00:01.887', 1, 86, 1, 0, 0, 401, 401, 401, 401, 401, 401, 0, 35);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-30 21:01:03.230', 1, 87, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 63);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-07 21:00:04.970', 1, 88, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-14 21:00:04.530', 1, 89, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-21 21:00:02.067', 1, 90, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-28 21:01:02.087', 1, 91, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-04 21:00:04.910', 1, 92, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-11 21:00:04.377', 1, 93, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-18 21:00:05.203', 1, 94, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-25 21:00:05.133', 1, 95, 1, 0, 0, 401, 401, 401, 401, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-02-01 21:00:56.820', 1, 96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-03 13:17:29.573', 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-07 21:00:06.330', 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-21 19:03:49.753', 2, 3, 1, 0, 0, 0, 0, 0, 55, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-28 21:00:06.857', 2, 4, 1, 0, 0, 0, 205, 0, 0, 16, 6, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-05 21:00:04.537', 2, 5, 1, 0, 0, 34, 13, 26, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-12 21:00:05.940', 2, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-19 21:00:06.867', 2, 7, 1, 0, 0, 34, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-26 21:00:07.323', 2, 8, 1, 0, 0, 0, 0, 34, 401, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-02 21:00:07.700', 2, 9, 1, 0, 0, 401, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-09 21:00:07.767', 2, 10, 1, 0, 0, 442, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-16 21:00:07.263', 2, 11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-23 21:00:06.727', 2, 12, 1, 0, 0, 0, 0, 34, 0, 34, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-30 21:00:07.110', 2, 13, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-07 21:00:06.607', 2, 14, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-14 21:01:08.017', 2, 15, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-21 21:00:06.093', 2, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-28 20:59:57.633', 2, 17, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-04 21:00:03.030', 2, 18, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-11 21:00:02.800', 2, 19, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-18 21:00:05.533', 2, 20, 1, 0, 0, 0, 0, 0, 705, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-25 21:00:02.767', 2, 21, 1, 0, 0, 0, 548, 0, 401, 401, 401, 401, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-01 21:00:02.903', 2, 22, 1, 0, 0, 401, 548, 0, 0, 401, 401, 401, 114);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-08 21:00:01.943', 2, 23, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-15 21:00:02.070', 2, 24, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-22 21:00:03.427', 2, 25, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-29 21:00:05.463', 2, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-06 20:59:55.950', 2, 27, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-13 21:00:01.617', 2, 28, 1, 0, 0, 678, 401, 678, 0, 0, 0, 678, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-20 21:00:02.073', 2, 29, 1, 0, 0, 0, 548, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-27 21:00:01.973', 2, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-03 21:01:05.067', 2, 31, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-10 21:00:04.517', 2, 32, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-17 21:00:04.677', 2, 33, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-24 21:00:03.093', 2, 34, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-01 21:01:02.453', 2, 35, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-08 21:00:01.847', 2, 36, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-15 21:00:02.017', 2, 37, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-22 21:00:07.023', 2, 38, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-29 21:01:02.197', 2, 39, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-05 21:00:04.840', 2, 40, 1, 0, 0, 0, 0, 0, 401, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-12 21:00:02.080', 2, 41, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-19 21:00:04.760', 2, 42, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-26 21:00:05.063', 2, 43, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-02 21:00:04.390', 2, 44, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-09 20:59:54.853', 2, 45, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-16 21:01:02.147', 2, 46, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-23 21:00:03.010', 2, 47, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-02 21:00:02.440', 2, 48, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-09 21:00:02.250', 2, 49, 1, 0, 0, 0, 804, 747, 0, 0, 0, 804, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-16 21:00:02.693', 2, 50, 1, 0, 0, 804, 401, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-23 21:00:02.140', 2, 51, 1, 0, 0, 804, 401, 0, 209, 888, 0, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-30 21:00:02.353', 2, 52, 1, 0, 0, 804, 401, 401, 209, 888, 0, 0, 27);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-06 21:00:02.390', 2, 53, 1, 0, 0, 804, 401, 401, 209, 888, 804, 401, 31);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-13 21:00:03.120', 2, 54, 1, 0, 0, 0, 0, 0, 209, 888, 0, 0, 24);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-20 21:00:02.097', 2, 55, 1, 0, 0, 0, 401, 0, 209, 888, 0, 401, 24);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-27 21:00:04.857', 2, 56, 1, 0, 0, 0, 0, 0, 209, 888, 0, 0, 24);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-04 21:00:02.180', 2, 57, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-11 21:01:01.900', 2, 58, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-18 21:00:05.343', 2, 59, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-25 21:00:05.690', 2, 60, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-01 21:00:05.557', 2, 61, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-08 21:01:01.600', 2, 62, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-15 21:00:02.053', 2, 63, 1, 0, 0, 0, 0, 0, 0, 401, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-22 21:00:01.947', 2, 64, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-29 21:00:02.210', 2, 65, 1, 0, 0, 747, 0, 747, 0, 0, 747, 747, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-06 21:01:02.640', 2, 66, 1, 0, 0, 747, 0, 747, 747, 0, 747, 747, 101);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-13 21:00:02.323', 2, 67, 1, 0, 0, 0, 0, 0, 401, 0, 401, 747, 64);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-20 21:00:01.450', 2, 68, 1, 0, 0, 747, 0, 987, 401, 0, 401, 747, 104);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-27 21:00:02.163', 2, 69, 1, 0, 0, 747, 0, 987, 401, 0, 401, 747, 109);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-03 21:01:02.603', 2, 70, 1, 0, 0, 747, 0, 987, 0, 0, 0, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-10 21:00:02.203', 2, 71, 1, 0, 0, 747, 0, 987, 0, 0, 0, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-17 21:00:02.380', 2, 72, 1, 0, 0, 747, 0, 987, 0, 0, 0, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-24 21:00:05.680', 2, 73, 1, 0, 0, 747, 0, 987, 0, 0, 747, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-31 21:01:02.380', 2, 74, 1, 0, 0, 747, 0, 987, 0, 0, 747, 747, 101);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-07 21:00:04.547', 2, 75, 1, 0, 0, 747, 0, 987, 0, 0, 747, 747, 101);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-14 21:00:05.293', 2, 76, 1, 0, 0, 747, 0, 987, 747, 0, 747, 747, 101);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-21 21:00:04.373', 2, 77, 1, 0, 0, 747, 0, 987, 747, 0, 747, 747, 109);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-28 21:00:05.807', 2, 78, 1, 0, 0, 747, 0, 987, 747, 0, 747, 747, 109);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-05 21:00:56.893', 2, 79, 1, 0, 0, 747, 0, 987, 747, 0, 747, 747, 109);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-12 21:00:02.310', 2, 80, 1, 0, 0, 747, 0, 987, 747, 0, 747, 747, 109);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-19 21:00:02.897', 2, 81, 1, 0, 0, 747, 0, 987, 0, 0, 0, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-26 21:00:02.847', 2, 82, 1, 0, 0, 747, 0, 987, 0, 0, 0, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-02 21:01:02.610', 2, 83, 1, 0, 0, 747, 0, 987, 0, 0, 0, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-09 21:00:05.017', 2, 84, 1, 0, 0, 747, 0, 987, 0, 0, 0, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-16 21:00:01.800', 2, 85, 1, 0, 0, 747, 0, 987, 0, 0, 0, 747, 69);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-23 21:00:01.887', 2, 86, 1, 0, 0, 747, 0, 401, 401, 401, 0, 747, 65);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-30 21:01:03.230', 2, 87, 1, 0, 0, 747, 401, 401, 401, 401, 401, 747, 93);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-07 21:00:04.970', 2, 88, 1, 0, 0, 747, 401, 401, 401, 401, 401, 747, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-14 21:00:04.533', 2, 89, 1, 0, 0, 747, 401, 401, 401, 401, 401, 747, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-21 21:00:02.070', 2, 90, 1, 0, 0, 747, 401, 401, 401, 401, 401, 747, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-28 21:01:02.087', 2, 91, 1, 0, 0, 1073, 401, 401, 401, 401, 401, 1073, 62);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-04 21:00:04.927', 2, 92, 1, 0, 0, 1073, 401, 401, 401, 401, 401, 1073, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-11 21:00:04.400', 2, 93, 1, 0, 0, 1073, 401, 401, 401, 401, 401, 1073, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-18 21:00:05.203', 2, 94, 1, 0, 0, 1073, 401, 401, 401, 401, 401, 1073, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-25 21:00:05.133', 2, 95, 1, 0, 0, 1073, 401, 401, 401, 401, 401, 1073, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-02-01 21:00:56.820', 2, 96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-03 13:17:29.573', 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-07 21:00:06.333', 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-21 19:03:49.753', 3, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-28 21:00:06.860', 3, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-05 21:00:04.537', 3, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-12 21:00:05.940', 3, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-19 21:00:06.873', 3, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-26 21:00:07.323', 3, 8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-02 21:00:07.703', 3, 9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-09 21:00:07.767', 3, 10, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-16 21:00:07.270', 3, 11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-23 21:00:06.727', 3, 12, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-30 21:00:07.117', 3, 13, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-07 21:00:06.607', 3, 14, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-14 21:01:08.020', 3, 15, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-21 21:00:06.093', 3, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-28 20:59:57.637', 3, 17, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-04 21:00:03.030', 3, 18, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-11 21:00:02.800', 3, 19, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-18 21:00:05.537', 3, 20, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-25 21:00:02.767', 3, 21, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-01 21:00:02.907', 3, 22, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-08 21:00:01.943', 3, 23, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-15 21:00:02.073', 3, 24, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-22 21:00:03.427', 3, 25, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-29 21:00:05.463', 3, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-06 20:59:55.950', 3, 27, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-13 21:00:01.620', 3, 28, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-20 21:00:02.073', 3, 29, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-27 21:00:01.977', 3, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-03 21:01:05.067', 3, 31, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-10 21:00:04.520', 3, 32, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-17 21:00:04.677', 3, 33, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-24 21:00:03.097', 3, 34, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-01 21:01:02.453', 3, 35, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-08 21:00:01.850', 3, 36, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-15 21:00:02.020', 3, 37, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-22 21:00:07.023', 3, 38, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-29 21:01:02.200', 3, 39, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-05 21:00:04.843', 3, 40, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-12 21:00:02.080', 3, 41, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-19 21:00:04.760', 3, 42, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-26 21:00:05.063', 3, 43, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-02 21:00:04.390', 3, 44, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-09 20:59:54.857', 3, 45, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-16 21:01:02.150', 3, 46, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-23 21:00:03.023', 3, 47, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-02 21:00:02.440', 3, 48, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-09 21:00:02.253', 3, 49, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-16 21:00:02.697', 3, 50, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-23 21:00:02.143', 3, 51, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-30 21:00:02.357', 3, 52, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-06 21:00:02.390', 3, 53, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-13 21:00:03.123', 3, 54, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-20 21:00:02.097', 3, 55, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-27 21:00:04.857', 3, 56, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-04 21:00:02.180', 3, 57, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-11 21:01:01.900', 3, 58, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-18 21:00:05.343', 3, 59, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-25 21:00:05.690', 3, 60, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-01 21:00:05.583', 3, 61, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-08 21:01:01.600', 3, 62, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-15 21:00:02.053', 3, 63, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-22 21:00:01.950', 3, 64, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-29 21:00:02.220', 3, 65, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-06 21:01:02.643', 3, 66, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-13 21:00:02.327', 3, 67, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-20 21:00:01.460', 3, 68, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-27 21:00:02.173', 3, 69, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-03 21:01:02.610', 3, 70, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-10 21:00:02.203', 3, 71, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-17 21:00:02.380', 3, 72, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-24 21:00:05.690', 3, 73, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-31 21:01:02.380', 3, 74, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-07 21:00:04.550', 3, 75, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-14 21:00:05.297', 3, 76, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-21 21:00:04.377', 3, 77, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-28 21:00:05.807', 3, 78, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-05 21:00:56.897', 3, 79, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-12 21:00:02.310', 3, 80, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-19 21:00:02.897', 3, 81, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-26 21:00:02.847', 3, 82, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-02 21:01:02.610', 3, 83, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-09 21:00:05.020', 3, 84, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-16 21:00:01.800', 3, 85, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-23 21:00:01.893', 3, 86, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-30 21:01:03.233', 3, 87, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-07 21:00:04.973', 3, 88, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-14 21:00:04.533', 3, 89, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-21 21:00:02.070', 3, 90, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-28 21:01:02.090', 3, 91, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-04 21:00:04.930', 3, 92, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-11 21:00:04.400', 3, 93, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-18 21:00:05.203', 3, 94, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-25 21:00:05.137', 3, 95, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-02-01 21:00:56.827', 3, 96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-03 13:17:29.573', 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-07 21:00:06.333', 4, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-21 19:03:49.753', 4, 3, 1, 0, 0, 117, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-04-28 21:00:06.860', 4, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-05 21:00:04.537', 4, 5, 1, 0, 0, 0, 0, 55, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-12 21:00:05.940', 4, 6, 1, 0, 0, 0, 298, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-19 21:00:06.877', 4, 7, 1, 0, 0, 0, 298, 6, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-05-26 21:00:07.327', 4, 8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-02 21:00:07.703', 4, 9, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-09 21:00:07.767', 4, 10, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-16 21:00:07.277', 4, 11, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-23 21:00:06.730', 4, 12, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-06-30 21:00:07.120', 4, 13, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-07 21:00:06.610', 4, 14, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-14 21:01:08.020', 4, 15, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-21 21:00:06.097', 4, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-07-28 20:59:57.637', 4, 17, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-04 21:00:03.033', 4, 18, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-11 21:00:02.800', 4, 19, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-18 21:00:05.537', 4, 20, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-08-25 21:00:02.770', 4, 21, 1, 0, 0, 548, 209, 209, 401, 0, 0, 209, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-01 21:00:02.907', 4, 22, 1, 0, 0, 548, 209, 401, 401, 209, 548, 209, 75);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-08 21:00:01.943', 4, 23, 1, 0, 0, 0, 209, 0, 0, 209, 0, 0, 18);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-15 21:00:02.073', 4, 24, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-22 21:00:03.430', 4, 25, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-09-29 21:00:05.463', 4, 26, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-06 20:59:55.953', 4, 27, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-13 21:00:01.620', 4, 28, 1, 0, 0, 401, 401, 548, 548, 0, 401, 401, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-20 21:00:02.073', 4, 29, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-10-27 21:00:01.977', 4, 30, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-03 21:01:05.070', 4, 31, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-10 21:00:04.520', 4, 32, 1, 0, 0, 0, 0, 0, 0, 0, 0, 401, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-17 21:00:04.677', 4, 33, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-11-24 21:00:03.097', 4, 34, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-01 21:01:02.457', 4, 35, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-08 21:00:01.850', 4, 36, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-15 21:00:02.020', 4, 37, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-22 21:00:07.027', 4, 38, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2018-12-29 21:01:02.200', 4, 39, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-05 21:00:04.843', 4, 40, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-12 21:00:02.083', 4, 41, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-19 21:00:04.763', 4, 42, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-01-26 21:00:05.063', 4, 43, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-02 21:00:04.390', 4, 44, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-09 20:59:54.857', 4, 45, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-16 21:01:02.150', 4, 46, 1, 0, 0, 747, 401, 0, 0, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-02-23 21:00:03.043', 4, 47, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-02 21:00:02.443', 4, 48, 1, 0, 0, 0, 747, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-09 21:00:02.253', 4, 49, 1, 0, 0, 0, 747, 0, 401, 0, 401, 401, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-16 21:00:02.697', 4, 50, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-23 21:00:02.143', 4, 51, 1, 0, 0, 747, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-03-30 21:00:02.377', 4, 52, 1, 0, 0, 747, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-06 21:00:02.390', 4, 53, 1, 0, 0, 747, 0, 0, 0, 747, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-13 21:00:03.123', 4, 54, 1, 0, 0, 747, 0, 0, 0, 747, 0, 0, 17);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-20 21:00:02.100', 4, 55, 1, 0, 0, 747, 0, 0, 0, 0, 0, 747, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-04-27 21:00:04.860', 4, 56, 1, 0, 0, 747, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-04 21:00:02.180', 4, 57, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-11 21:01:01.900', 4, 58, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-18 21:00:05.347', 4, 59, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-05-25 21:00:05.690', 4, 60, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-01 21:00:05.590', 4, 61, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-08 21:01:01.603', 4, 62, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-15 21:00:02.053', 4, 63, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-22 21:00:01.950', 4, 64, 1, 0, 0, 0, 401, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-06-29 21:00:02.220', 4, 65, 1, 0, 0, 987, 401, 0, 987, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-06 21:01:02.647', 4, 66, 1, 0, 0, 987, 401, 0, 987, 987, 0, 0, 11);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-13 21:00:02.327', 4, 67, 1, 0, 0, 987, 401, 401, 0, 401, 0, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-20 21:00:01.460', 4, 68, 1, 0, 0, 987, 401, 401, 0, 401, 0, 0, 23);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-07-27 21:00:02.187', 4, 69, 1, 0, 0, 987, 401, 401, 0, 401, 0, 0, 23);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-03 21:01:02.610', 4, 70, 1, 0, 0, 987, 401, 0, 0, 0, 0, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-10 21:00:02.203', 4, 71, 1, 0, 0, 987, 401, 0, 0, 0, 0, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-17 21:00:02.380', 4, 72, 1, 0, 0, 987, 401, 0, 0, 0, 0, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-24 21:00:05.690', 4, 73, 1, 0, 0, 987, 401, 0, 0, 0, 0, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-08-31 21:01:02.380', 4, 74, 1, 0, 0, 987, 401, 0, 0, 0, 0, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-07 21:00:04.550', 4, 75, 1, 0, 0, 987, 401, 0, 0, 0, 0, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-14 21:00:05.297', 4, 76, 1, 0, 0, 987, 401, 747, 747, 747, 747, 0, 3);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-21 21:00:04.380', 4, 77, 1, 0, 0, 987, 401, 747, 747, 747, 747, 0, 63);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-09-28 21:00:05.823', 4, 78, 1, 0, 0, 987, 401, 747, 747, 747, 747, 0, 63);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-05 21:00:56.897', 4, 79, 1, 0, 0, 987, 401, 747, 747, 747, 747, 0, 63);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-12 21:00:02.313', 4, 80, 1, 0, 0, 987, 401, 747, 747, 747, 747, 0, 63);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-19 21:00:02.897', 4, 81, 1, 0, 0, 987, 401, 0, 747, 0, 0, 0, 11);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-10-26 21:00:02.850', 4, 82, 1, 0, 0, 987, 401, 0, 747, 0, 0, 0, 11);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-02 21:01:02.613', 4, 83, 1, 0, 0, 987, 401, 0, 747, 0, 0, 0, 11);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-09 21:00:05.020', 4, 84, 1, 0, 0, 987, 401, 0, 747, 0, 0, 0, 11);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-16 21:00:01.803', 4, 85, 1, 0, 0, 987, 401, 0, 747, 0, 0, 0, 11);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-23 21:00:01.897', 4, 86, 1, 0, 0, 401, 401, 401, 747, 401, 0, 401, 10);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-11-30 21:01:03.237', 4, 87, 1, 0, 0, 401, 401, 401, 747, 401, 401, 401, 95);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-07 21:00:04.973', 4, 88, 1, 0, 0, 401, 401, 401, 747, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-14 21:00:04.537', 4, 89, 1, 0, 0, 401, 401, 401, 747, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-21 21:00:02.077', 4, 90, 1, 0, 0, 401, 401, 401, 747, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2019-12-28 21:01:02.090', 4, 91, 1, 0, 0, 401, 401, 401, 1073, 401, 401, 401, 119);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-04 21:00:04.930', 4, 92, 1, 0, 0, 401, 401, 401, 1073, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-11 21:00:04.403', 4, 93, 1, 0, 0, 401, 401, 401, 1073, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-18 21:00:05.207', 4, 94, 1, 0, 0, 401, 401, 401, 1073, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-01-25 21:00:05.137', 4, 95, 1, 0, 0, 401, 401, 401, 1073, 401, 401, 401, 127);
GO

INSERT INTO [dbo].[TblSiegeGambleState] ([mRegDate], [mTerritory], [mNo], [mIsFinish], [mTotalMoney], [mDividend], [mOccupyGuild0], [mOccupyGuild1], [mOccupyGuild2], [mOccupyGuild3], [mOccupyGuild4], [mOccupyGuild5], [mOccupyGuild6], [mIsKeep]) VALUES ('2020-02-01 21:00:56.830', 4, 96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
GO

