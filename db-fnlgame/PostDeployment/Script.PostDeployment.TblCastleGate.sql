/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLGame1164
Source Table          : TblCastleGate
Date                  : 2023-10-07 09:01:49
*/


INSERT INTO [dbo].[TblCastleGate] ([mRegDate], [mNo], [mHp], [mIsOpen]) VALUES ('2018-04-07 19:00:00.000', 2, 150, 1);
GO

INSERT INTO [dbo].[TblCastleGate] ([mRegDate], [mNo], [mHp], [mIsOpen]) VALUES ('2018-04-07 19:00:00.000', 52, 150, 1);
GO

INSERT INTO [dbo].[TblCastleGate] ([mRegDate], [mNo], [mHp], [mIsOpen]) VALUES ('2018-04-07 19:00:00.000', 102, 150, 1);
GO

INSERT INTO [dbo].[TblCastleGate] ([mRegDate], [mNo], [mHp], [mIsOpen]) VALUES ('2018-04-07 19:00:00.000', 153, 150, 1);
GO

INSERT INTO [dbo].[TblCastleGate] ([mRegDate], [mNo], [mHp], [mIsOpen]) VALUES ('2018-04-25 20:20:00.000', 801, 1000, 1);
GO

INSERT INTO [dbo].[TblCastleGate] ([mRegDate], [mNo], [mHp], [mIsOpen]) VALUES ('2018-04-25 20:29:00.000', 802, 1000, 1);
GO

