/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLGame1164
Source Table          : TblBanquetHall
Date                  : 2023-10-07 09:01:49
*/


INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 0, 0, '2020-02-04 21:09:00.000', 1013);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 1, 0, '2020-02-04 22:42:00.000', 1106);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 2, 0, '2020-02-04 04:49:00.000', 69);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 3, 0, '2020-02-04 13:51:00.000', 575);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 4, 0, '2020-02-04 18:28:00.000', 852);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 5, 0, '2020-02-04 23:31:00.000', 1155);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 6, 0, '2020-02-04 23:32:00.000', 1156);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 7, 0, '2020-02-05 00:04:00.000', 1188);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 8, 0, '2020-02-04 09:08:00.000', 328);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 9, 0, '2020-02-04 03:55:00.000', 15);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 10, 0, '2020-02-04 09:52:00.000', 372);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 0, 11, 0, '2023-10-03 05:06:00.000', 1339);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 1, 0, 0, '2020-02-04 20:37:00.000', 981);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 1, 1, 0, '2020-02-04 23:08:00.000', 1132);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (1, 1, 2, 0, '2020-02-04 21:20:00.000', 1024);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 0, 0, '2020-02-04 18:15:00.000', 839);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 1, 0, '2020-02-04 14:17:00.000', 601);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 2, 0, '2020-02-04 04:03:00.000', 23);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 3, 0, '2020-02-04 16:51:00.000', 755);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 4, 0, '2020-02-04 11:55:00.000', 459);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 5, 0, '2020-02-05 01:03:00.000', 1247);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 6, 0, '2020-02-04 04:44:00.000', 64);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 7, 0, '2020-02-04 05:58:00.000', 138);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 8, 0, '2020-02-04 07:27:00.000', 227);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 9, 0, '2020-02-04 20:18:00.000', 962);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 10, 0, '2020-02-04 04:17:00.000', 37);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 0, 11, 0, '2020-02-04 17:25:00.000', 789);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 1, 0, 0, '2020-02-04 23:17:00.000', 1141);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 1, 1, 0, '2020-02-04 07:49:00.000', 249);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (2, 1, 2, 0, '2020-02-04 03:51:00.000', 11);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 0, 0, '2020-02-04 20:38:00.000', 982);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 1, 0, '2020-02-05 00:22:00.000', 1206);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 2, 0, '2020-02-04 18:26:00.000', 850);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 3, 0, '2020-02-04 19:21:00.000', 905);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 4, 0, '2020-02-04 17:27:00.000', 791);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 5, 0, '2020-02-04 23:04:00.000', 1128);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 6, 0, '2020-02-05 00:11:00.000', 1195);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 7, 0, '2020-02-04 11:27:00.000', 431);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 8, 0, '2020-02-04 10:48:00.000', 392);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 9, 0, '2020-02-04 17:06:00.000', 770);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 10, 0, '2020-02-04 07:34:00.000', 234);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 0, 11, 0, '2020-02-04 13:57:00.000', 581);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 1, 0, 0, '2020-02-05 00:10:00.000', 1194);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 1, 1, 0, '2020-02-04 13:57:00.000', 581);
GO

INSERT INTO [dbo].[TblBanquetHall] ([mTerritory], [mBanquetHallType], [mBanquetHallNo], [mOwnerPcNo], [mRegDate], [mLeftMin]) VALUES (4, 1, 2, 0, '2020-02-04 13:15:00.000', 539);
GO

