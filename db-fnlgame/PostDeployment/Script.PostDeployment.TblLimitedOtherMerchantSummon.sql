/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLGame1164
Source Table          : TblLimitedOtherMerchantSummon
Date                  : 2023-10-07 09:01:49
*/


INSERT INTO [dbo].[TblLimitedOtherMerchantSummon] ([mMerchantID], [mAbleSummonCnt]) VALUES (2626, 3);
GO

INSERT INTO [dbo].[TblLimitedOtherMerchantSummon] ([mMerchantID], [mAbleSummonCnt]) VALUES (2627, 3);
GO

INSERT INTO [dbo].[TblLimitedOtherMerchantSummon] ([mMerchantID], [mAbleSummonCnt]) VALUES (2628, 3);
GO

INSERT INTO [dbo].[TblLimitedOtherMerchantSummon] ([mMerchantID], [mAbleSummonCnt]) VALUES (2629, 2);
GO

INSERT INTO [dbo].[TblLimitedOtherMerchantSummon] ([mMerchantID], [mAbleSummonCnt]) VALUES (2630, 2);
GO

INSERT INTO [dbo].[TblLimitedOtherMerchantSummon] ([mMerchantID], [mAbleSummonCnt]) VALUES (2631, 6);
GO

