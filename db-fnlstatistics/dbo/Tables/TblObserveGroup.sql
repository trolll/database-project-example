CREATE TABLE [dbo].[TblObserveGroup] (
    [mObServeGpID] INT          NOT NULL,
    [mObServeNm]   VARCHAR (30) NOT NULL,
    CONSTRAINT [UCL_PK_TblObserveGroup] PRIMARY KEY CLUSTERED ([mObServeGpID] ASC) WITH (FILLFACTOR = 60)
);


GO

