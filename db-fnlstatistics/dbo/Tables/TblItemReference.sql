CREATE TABLE [dbo].[TblItemReference] (
    [mItemNo]      INT NOT NULL,
    [mCreateValue] INT CONSTRAINT [DF_TblItemReference_mCreateValue] DEFAULT (0) NULL,
    [mDeleteValue] INT CONSTRAINT [DF_TblItemReference_mDeleteValue] DEFAULT (0) NULL,
    CONSTRAINT [PK_TblItemReference] PRIMARY KEY CLUSTERED ([mItemNo] ASC) WITH (FILLFACTOR = 60)
);


GO

