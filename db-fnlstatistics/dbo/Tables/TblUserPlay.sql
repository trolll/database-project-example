CREATE TABLE [dbo].[TblUserPlay] (
    [mRegDate]      CHAR (8) CONSTRAINT [DF__TblUserPl__mRegD__49C3F6B7] DEFAULT (convert(char,getdate(),112)) NOT NULL,
    [mUVCnt]        INT      NOT NULL,
    [mActiveCnt]    INT      NOT NULL,
    [mNewActiveCnt] INT      NOT NULL,
    [mSecederCnt]   BIGINT   NOT NULL,
    CONSTRAINT [PK_TblUserPlay] PRIMARY KEY CLUSTERED ([mRegDate] DESC) WITH (FILLFACTOR = 60)
);


GO

