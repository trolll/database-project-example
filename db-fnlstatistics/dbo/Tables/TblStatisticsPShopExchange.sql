CREATE TABLE [dbo].[TblStatisticsPShopExchange] (
    [mSvrNo]          SMALLINT NOT NULL,
    [mRegDate]        CHAR (8) NOT NULL,
    [mItemNo]         INT      NOT NULL,
    [mBuyCount]       BIGINT   NULL,
    [mSellCount]      BIGINT   NULL,
    [mBuyTotalPrice]  BIGINT   NULL,
    [mSellTotalPrice] BIGINT   NULL,
    [mBuyMinPrice]    BIGINT   NULL,
    [mSellMinPrice]   BIGINT   NULL,
    [mBuyMaxPrice]    BIGINT   NULL,
    [mSellMaxPrice]   BIGINT   NULL,
    CONSTRAINT [UCL_TblStatisticsPShopExchange] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mRegDate] ASC, [mItemNo] ASC) WITH (FILLFACTOR = 60)
);


GO

