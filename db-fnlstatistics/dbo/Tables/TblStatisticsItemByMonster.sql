CREATE TABLE [dbo].[TblStatisticsItemByMonster] (
    [mSvrNo]   SMALLINT NOT NULL,
    [mRegDate] CHAR (8) NOT NULL,
    [MID]      INT      NOT NULL,
    [mItemNo]  INT      NOT NULL,
    [mCreate]  BIGINT   NULL,
    [mDelete]  BIGINT   NULL,
    CONSTRAINT [CL_PK_TblStatisticsItemByMonster] PRIMARY KEY CLUSTERED ([mRegDate] ASC, [MID] ASC, [mSvrNo] ASC, [mItemNo] ASC) WITH (FILLFACTOR = 60)
);


GO

