CREATE TABLE [dbo].[TblStatisticsItemByCase_] (
    [mSvrNo]            SMALLINT NOT NULL,
    [mRegdate]          INT      NOT NULL,
    [mItemNo]           INT      NOT NULL,
    [mMerchantCreate]   BIGINT   NULL,
    [mMerchantDelete]   BIGINT   NULL,
    [mReinforceCreate]  BIGINT   NULL,
    [mReinforceDelete]  BIGINT   NULL,
    [mCraftingCreate]   BIGINT   NULL,
    [mCraftingDelete]   BIGINT   NULL,
    [mPcUseDelete]      BIGINT   NULL,
    [mNpcUseDelete]     BIGINT   NULL,
    [mNpcCreate]        BIGINT   NULL,
    [mMonsterDrop]      BIGINT   NULL,
    [mGSExchangeCreate] BIGINT   NULL,
    [mGSExchangeDelete] BIGINT   NULL
);


GO

