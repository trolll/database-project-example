CREATE TABLE [dbo].[TblObserveItemDetail] (
    [mRegDate]  CHAR (8) CONSTRAINT [DFTblObserveItemDetailRegDate] DEFAULT (convert(char,getdate(),112)) NOT NULL,
    [mSvrNo]    INT      NOT NULL,
    [mItemNo]   INT      NOT NULL,
    [mStatus]   TINYINT  NOT NULL,
    [mEquipPos] TINYINT  NOT NULL,
    [mVal]      BIGINT   CONSTRAINT [DFTblObserveItemDetailVal] DEFAULT (0) NOT NULL,
    CONSTRAINT [PKTblObserveItemDetailItemNo] PRIMARY KEY CLUSTERED ([mRegDate] ASC, [mSvrNo] ASC, [mItemNo] ASC, [mStatus] ASC, [mEquipPos] ASC) WITH (FILLFACTOR = 60)
);


GO

