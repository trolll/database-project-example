CREATE TABLE [dbo].[TblStatisticsItemByCase] (
    [mSvrNo]            SMALLINT NOT NULL,
    [mRegDate]          CHAR (8) NOT NULL,
    [mItemNo]           INT      NOT NULL,
    [mMerchantCreate]   BIGINT   NULL,
    [mMerchantDelete]   BIGINT   NULL,
    [mReinforceCreate]  BIGINT   NULL,
    [mReinforceDelete]  BIGINT   NULL,
    [mCraftingCreate]   BIGINT   NULL,
    [mCraftingDelete]   BIGINT   NULL,
    [mPcUseDelete]      BIGINT   NULL,
    [mNpcUseDelete]     BIGINT   NULL,
    [mNpcCreate]        BIGINT   NULL,
    [mMonsterDrop]      BIGINT   NULL,
    [mGSExchangeCreate] BIGINT   NULL,
    [mGSExchangeDelete] BIGINT   NULL,
    CONSTRAINT [CL_PK_TblStatisticsItemByCase] PRIMARY KEY CLUSTERED ([mRegDate] ASC, [mItemNo] ASC, [mSvrNo] ASC) WITH (FILLFACTOR = 60)
);


GO

