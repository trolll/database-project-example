CREATE TABLE [dbo].[TblObserveGroupItem] (
    [mObServeGpID] INT           NOT NULL,
    [mItemNo]      INT           NOT NULL,
    [mItemNm]      NVARCHAR (40) NOT NULL,
    CONSTRAINT [UCL_PK_TblObserveGroupItem] PRIMARY KEY CLUSTERED ([mObServeGpID] ASC, [mItemNo] ASC) WITH (FILLFACTOR = 60),
    CONSTRAINT [FK_TblObserveGroupItemTblObserveGroup] FOREIGN KEY ([mObServeGpID]) REFERENCES [dbo].[TblObserveGroup] ([mObServeGpID])
);


GO

CREATE NONCLUSTERED INDEX [NC_TblObserveGroupItem_1]
    ON [dbo].[TblObserveGroupItem]([mItemNo] ASC) WITH (FILLFACTOR = 60);


GO

