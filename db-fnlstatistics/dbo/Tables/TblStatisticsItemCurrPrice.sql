CREATE TABLE [dbo].[TblStatisticsItemCurrPrice] (
    [mRegDate] CHAR (8) NOT NULL,
    [mSvrNo]   SMALLINT NOT NULL,
    [mPrice]   INT      NOT NULL,
    CONSTRAINT [UCL_TblStatisticsItemCurrPrice_mRegDate] PRIMARY KEY CLUSTERED ([mSvrNo] ASC, [mRegDate] ASC) WITH (FILLFACTOR = 60)
);


GO

