CREATE TABLE [dbo].[TblStatisticsGoldItem] (
    [mRegDate]   INT           NOT NULL,
    [mUserNo]    INT           NOT NULL,
    [mUserID]    VARCHAR (20)  NOT NULL,
    [GoldItemID] BIGINT        NOT NULL,
    [ItemName]   NVARCHAR (40) NOT NULL,
    [mOrderType] TINYINT       NOT NULL,
    [Count]      INT           NOT NULL,
    [UseGold]    INT           NOT NULL,
    [mSrvNo]     SMALLINT      NOT NULL,
    [mNo]        INT           NOT NULL,
    [mNm]        NVARCHAR (12) NOT NULL,
    [mClass]     TINYINT       NOT NULL,
    [mLevel]     SMALLINT      NOT NULL,
    [mIp]        CHAR (15)     NOT NULL,
    [mPcbangLv]  SMALLINT      NOT NULL
);


GO

CREATE CLUSTERED INDEX [CL_TblStatisticsGoldItem]
    ON [dbo].[TblStatisticsGoldItem]([mRegDate] ASC) WITH (FILLFACTOR = 60);


GO

