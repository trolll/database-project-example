CREATE TABLE [dbo].[TblObserveItem] (
    [mRegDate] CHAR (8) CONSTRAINT [DFTblObserveItemRegDate] DEFAULT (convert(char,getdate(),112)) NOT NULL,
    [mSvrNo]   INT      NOT NULL,
    [mItemNo]  INT      NOT NULL,
    [mVal]     BIGINT   CONSTRAINT [DFTblObserveItemVal] DEFAULT (0) NOT NULL,
    CONSTRAINT [PKTblObserveItemItemNo] PRIMARY KEY CLUSTERED ([mRegDate] ASC, [mSvrNo] ASC, [mItemNo] ASC) WITH (FILLFACTOR = 60)
);


GO

