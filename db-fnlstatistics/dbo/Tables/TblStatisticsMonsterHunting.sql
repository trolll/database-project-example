CREATE TABLE [dbo].[TblStatisticsMonsterHunting] (
    [mSvrNo]      SMALLINT NOT NULL,
    [mRegDate]    CHAR (8) NOT NULL,
    [MID]         INT      NOT NULL,
    [mHuntingCnt] BIGINT   NULL,
    CONSTRAINT [CL_PK_TblStatisticsMonsterHunting] PRIMARY KEY CLUSTERED ([mRegDate] ASC, [MID] ASC, [mSvrNo] ASC) WITH (FILLFACTOR = 60)
);


GO

