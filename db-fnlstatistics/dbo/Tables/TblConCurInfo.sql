CREATE TABLE [dbo].[TblConCurInfo] (
    [mRegDate] DATETIME NOT NULL,
    [mSvrNo]   SMALLINT NOT NULL,
    [mUserCnt] INT      NOT NULL,
    [mPcCnt]   INT      NULL,
    CONSTRAINT [PkTblConCurInfo] PRIMARY KEY CLUSTERED ([mRegDate] ASC, [mSvrNo] ASC) WITH (FILLFACTOR = 60)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblConCurInfo_1]
    ON [dbo].[TblConCurInfo]([mRegDate] DESC) WITH (FILLFACTOR = 60);


GO

