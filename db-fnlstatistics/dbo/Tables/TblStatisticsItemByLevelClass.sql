CREATE TABLE [dbo].[TblStatisticsItemByLevelClass] (
    [mSvrNo]            SMALLINT NOT NULL,
    [mRegDate]          INT      NOT NULL,
    [mItemNo]           INT      NOT NULL,
    [mLevel]            SMALLINT NOT NULL,
    [mClass]            SMALLINT NOT NULL,
    [mMerchantCreate]   BIGINT   NULL,
    [mMerchantDelete]   BIGINT   NULL,
    [mReinforceCreate]  BIGINT   NULL,
    [mReinforceDelete]  BIGINT   NULL,
    [mCraftingCreate]   BIGINT   NULL,
    [mCraftingDelete]   BIGINT   NULL,
    [mPcUseDelete]      BIGINT   NULL,
    [mNpcUseDelete]     BIGINT   NULL,
    [mNpcCreate]        BIGINT   NULL,
    [mMonsterDrop]      BIGINT   NULL,
    [mGSExchangeCreate] BIGINT   NULL,
    [mGSExchangeDelete] BIGINT   NULL,
    CONSTRAINT [CL_PK_TblStatisticsItemByLevelClass] PRIMARY KEY CLUSTERED ([mRegDate] ASC, [mSvrNo] ASC, [mItemNo] ASC, [mLevel] ASC, [mClass] ASC) WITH (FILLFACTOR = 60)
);


GO

