CREATE TABLE [dbo].[TblSvrInfo] (
    [mSvrNo]       SMALLINT      NOT NULL,
    [mDesc]        VARCHAR (100) NOT NULL,
    [mSupportType] TINYINT       NOT NULL,
    [mSvrInfo]     TINYINT       NOT NULL,
    [mIsTest]      BIT           NOT NULL,
    CONSTRAINT [UCL_TblSvrInfo_mSvrNo] PRIMARY KEY CLUSTERED ([mSvrNo] ASC) WITH (FILLFACTOR = 60)
);


GO

