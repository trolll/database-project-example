
CREATE PROCEDURE dbo.UspGetMaxConCurInfo
	@in_Date  CHAR(10)	-- 2006-10-10
AS	
	SELECT 
		-- 按时间段抽取
		mHour,
		-- 追加的服务器在此处登记
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 101 THEN mMaxUserCnt END ),0) AS '101',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 102 THEN mMaxUserCnt END ), 0) AS '102',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 103 THEN mMaxUserCnt END ), 0) AS '103',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 104 THEN mMaxUserCnt END ), 0) AS '104',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 105 THEN mMaxUserCnt END ), 0) AS '105',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 106 THEN mMaxUserCnt END ), 0) AS '106',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 107 THEN mMaxUserCnt END ), 0) AS '107',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 108 THEN mMaxUserCnt END ), 0) AS '108',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 109 THEN mMaxUserCnt END ), 0) AS '109',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 110 THEN mMaxUserCnt END ), 0) AS '110',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 111 THEN mMaxUserCnt END ), 0) AS '111',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 112 THEN mMaxUserCnt END ), 0) AS '112',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 113 THEN mMaxUserCnt END ), 0) AS '113',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 114 THEN mMaxUserCnt END ), 0) AS '114',		
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 115 THEN mMaxUserCnt END ), 0) AS '115',	-- (World)名服务器		
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 117 THEN mMaxUserCnt END ), 0) AS '117',
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 116 THEN mMaxUserCnt END ), 0) AS '116',	-- World 名
		
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 118 THEN mMaxUserCnt END ), 0) AS '118',	
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 119 THEN mMaxUserCnt END ), 0) AS '119',	
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 120 THEN mMaxUserCnt END ), 0) AS '120',	
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 121 THEN mMaxUserCnt END ), 0) AS '121',	
		ISNULL(SUM( CASE WHEN T1.mSvrNo = 122 THEN mMaxUserCnt END ), 0) AS '122',	
			
		ISNULL(SUM( mMaxUserCnt), 0) AS 'TOT'
	FROM (
		SELECT 
			T1.mSvrNo AS mSvrNo,
			T1.mCode AS mHour,
			ISNULL(T2.mMaxUserCnt, 0) AS mMaxUserCnt
		FROM (
			SELECT 
				T1.mCode, 
				mSvrNo
			FROM dbo.TblCode T1
				CROSS JOIN  (
					SELECT mSvrNo
					FROM dbo.TblConCurInfo WITH(NOLOCK)
						WHERE mRegDate >= CONVERT(DATETIME, @in_Date )
							AND mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_Date ) )
					GROUP BY mSvrNo	
				) T2
			WHERE mCodeType = 3 
		) T1 LEFT OUTER JOIN 
			(
				SELECT 
					mSvrNo, 
					DATEPART(hh, mRegDate) AS mHour,
					MAX(mUserCnt) AS mMaxUserCnt
				FROM dbo.TblConCurInfo WITH(NOLOCK)
					WHERE mRegDate >= CONVERT(DATETIME, @in_Date )
						AND mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_Date ) )
				GROUP BY mSvrNo, DATEPART(hh, mRegDate)
			) T2
		ON T1.mSvrNo = T2.mSvrNo AND T1.mCode = T2.mHour
	) T1	
	GROUP BY mHour  WITH ROLLUP

GO

