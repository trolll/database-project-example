CREATE PROCEDURE dbo.UspGetMaxPeriodConInfoOfServer
	@in_begin_date	CHAR(8),
	@in_end_date	CHAR(8)
AS 
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT 
		T2.*
	FROM
		 (
			SELECT 
				mDate
				, MAX(mMaxUserCnt) AS mMaxUserCnt
			FROM
			 (
				SELECT 
					mDate, 
					mHour,  
					Sum(mMaxUserCnt)  AS mMaxUserCnt
				FROM (
					SELECT  
						CONVERT(CHAR(8) , mRegDate, 112) AS mDate,
						DATEPART(hh, mRegDate) AS mHour,
						mSvrNo,
						MAX(mUserCnt) As mMaxUserCnt
					FROM dbo.TblConCurInfo WITH(NOLOCK)	
					WHERE mRegDate >= CONVERT(DATETIME, @in_begin_date )
							AND mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_End_Date ) )
					GROUP BY CONVERT(CHAR(8) , mRegDate, 112), DATEPART(hh, mRegDate), mSvrNo		
					) T1
				GROUP BY mDate, mHour
			) T1
			GROUP BY mDate
		) T1
		INNER JOIN 
		(
			SELECT 
				-- 矫埃措喊 眠免
				mDate,
				mHour,
				-- 眠啊登绰 辑滚绰 咯扁俊辑 殿废 
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 101 THEN mMaxUserCnt END ),0) AS '101',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 102 THEN mMaxUserCnt END ), 0) AS '102',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 103 THEN mMaxUserCnt END ), 0) AS '103',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 104 THEN mMaxUserCnt END ), 0) AS '104',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 105 THEN mMaxUserCnt END ), 0) AS '105',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 106 THEN mMaxUserCnt END ), 0) AS '106',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 107 THEN mMaxUserCnt END ), 0) AS '107',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 108 THEN mMaxUserCnt END ), 0) AS '108',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 109 THEN mMaxUserCnt END ), 0) AS '109',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 110 THEN mMaxUserCnt END ), 0) AS '110',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 111 THEN mMaxUserCnt END ), 0) AS '111',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 112 THEN mMaxUserCnt END ), 0) AS '112',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 113 THEN mMaxUserCnt END ), 0) AS '113',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 114 THEN mMaxUserCnt END ), 0) AS '114',		
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 115 THEN mMaxUserCnt END ), 0) AS '115',	-- 骇乃 辑滚 		
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 117 THEN mMaxUserCnt END ), 0) AS '117',
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 116 THEN mMaxUserCnt END ), 0) AS '116',	-- 捞橇府飘
				
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 118 THEN mMaxUserCnt END ), 0) AS '118',	
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 119 THEN mMaxUserCnt END ), 0) AS '119',	
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 120 THEN mMaxUserCnt END ), 0) AS '120',	
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 121 THEN mMaxUserCnt END ), 0) AS '121',	
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 122 THEN mMaxUserCnt END ), 0) AS '122',	
				ISNULL(SUM( CASE WHEN T1.mSvrNo = 124 THEN mMaxUserCnt END ), 0) AS '124',
					
				ISNULL(SUM(mMaxUserCnt), 0) AS Tot
			FROM (
						SELECT 
							mSvrNo, 
							DATEPART(hh, mRegDate) as mHour,
							CONVERT(CHAR(8) , mRegDate, 112) AS mDate,
							MAX(mUserCnt) AS mMaxUserCnt
						FROM dbo.TblConCurInfo WITH(NOLOCK)
							WHERE mRegDate >= CONVERT(DATETIME, @in_begin_date )
									AND mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_end_date ) )
						GROUP BY CONVERT(CHAR(8) , mRegDate, 112),  mSvrNo, DATEPART(hh, mRegDate)
			) T1	
			GROUP BY mDate, mHour  ) T2
		ON T1.mDate = T2.mDate
			AND T1.mMaxUserCnt = T2.Tot
		ORDER BY T1.mDate

GO

