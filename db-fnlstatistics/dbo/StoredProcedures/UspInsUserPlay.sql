


------------------------------------------------------------------------------------
-- PROCEDURE
------------------------------------------------------------------------------------
CREATE PROCEDURE dbo.UspInsUserPlay
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aBeginDate DATETIME
	DECLARE @aEndDate DATETIME
	
	SELECT @aBeginDate =
			CONVERT(DATETIME, CONVERT(CHAR(10), getdate()-1, 110) + ' 3:00:00'),
		   @aEndDate = 
		   	CONVERT(DATETIME, CONVERT(CHAR(10), getdate(), 110) + ' 2:29:29')
		   	  		   		
	INSERT INTO dbo.TblUserPlay(mUVCnt, mActiveCnt, mNewActiveCnt, mSecederCnt)
	SELECT 
		(	SELECT COUNT(*)
			FROM FNLAccount.dbo.TblUser 
			WHERE mLoginTm BETWEEN @aBeginDate AND @aEndDate 
		) AS mUVCnt,		-- UV Player
		(
			SELECT COUNT(*)
			FROM FNLAccount.dbo.TblUser 
			WHERE mLoginTm >= DATEADD(dd, -7, @aBeginDate)
		) AS mActiveCnt,	-- Active Player
		(
			SELECT COUNT(*)
			FROM FNLAccount.dbo.TblUser
			WHERE mLoginTm >= @aBeginDate	
					AND mRegDate >= @aBeginDate		
		) AS mNewActiveCnt,	-- New Account
		(
			SELECT COUNT(*)
			FROM FNLAccount.dbo.TblUser
			WHERE mLoginTm <=  DATEADD(dd, -14, @aBeginDate)						
		) AS mSecederCnt	-- 捞呕 蜡历(2林 扁霖)

GO

