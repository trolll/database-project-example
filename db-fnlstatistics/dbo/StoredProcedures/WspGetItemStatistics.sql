--FNLStatistics
CREATE  PROCEDURE dbo.WspGetItemStatistics
	 @svrno   VARCHAR(10)
	,@data0		CHAR(8)			
	,@data1		CHAR(8)			
	,@data2		CHAR(8)
	,@data3		CHAR(8)
	,@data4		CHAR(8)
	,@data5		CHAR(8)
	,@data6		CHAR(8)		   	
AS
	SET NOCOUNT ON

select  a0.IID,a0.IName,(a5.mVal-a6.mVal) as d5mVal,(a4.mVal-a5.mVal) as d4mVal,(a3.mVal-a4.mVal) as d3mVal,(a2.mVal-a3.mVal) as d2mVal,(a1.mVal-a2.mVal) as d1mVal,(a0.mVal-a1.mVal) as d0mVal,a1.mVal as currentmVal,
(a1.mVal-a6.mVal)/5 as averagemVal
		from (
		select a.mRegDate,b.IID,b.IName ,ISNULL (a.mVal,0) as mVal 
		From 
			FNLStatistics.dbo.TblObserveItem a 
			right outer join FNLParm.dbo.DT_Item b  
    		on a.mItemNo=b.IID 
			and a.mSvrNo=@svrno
			and a.mRegDate = @data0	
		) a0,  (		
		select a.mRegDate,b.IID,b.IName ,ISNULL (a.mVal,0) as mVal 
		From 
			FNLStatistics.dbo.TblObserveItem a 
			right outer join FNLParm.dbo.DT_Item b  
    		on a.mItemNo=b.IID 
			and a.mSvrNo=@svrno 
			and a.mRegDate = @data1 	
		) a1, (		
		select a.mRegDate,b.IID,b.IName ,ISNULL (a.mVal,0) as mVal 
		From 
			FNLStatistics.dbo.TblObserveItem a 
			right outer join FNLParm.dbo.DT_Item b  
    		on a.mItemNo=b.IID 
			and a.mSvrNo=@svrno 
			and a.mRegDate = @data2	
		) a2, (		
		select a.mRegDate,b.IID,b.IName ,ISNULL (a.mVal,0) as mVal 
		From 
			FNLStatistics.dbo.TblObserveItem a 
			right outer join FNLParm.dbo.DT_Item b  
    		on a.mItemNo=b.IID 
			and a.mSvrNo=@svrno 
			and a.mRegDate = @data3	
		) a3, (		
		select a.mRegDate,b.IID,b.IName ,ISNULL (a.mVal,0) as mVal 
		From 
			FNLStatistics.dbo.TblObserveItem a 
			right outer join FNLParm.dbo.DT_Item b  
    		on a.mItemNo=b.IID 
			and a.mSvrNo=@svrno 
			and a.mRegDate = @data4	
		) a4, (		
		select a.mRegDate,b.IID,b.IName ,ISNULL (a.mVal,0) as mVal 
		From 
			FNLStatistics.dbo.TblObserveItem a 
			right outer join FNLParm.dbo.DT_Item b  
    		on a.mItemNo=b.IID 
			and a.mSvrNo=@svrno 
			and a.mRegDate = @data5 	
		) a5, (		
		select a.mRegDate,b.IID,b.IName ,ISNULL (a.mVal,0) as mVal 
		From 
			FNLStatistics.dbo.TblObserveItem a 
			right outer join FNLParm.dbo.DT_Item b  
    		on a.mItemNo=b.IID 
			and a.mSvrNo=@svrno 
			and a.mRegDate = @data6	
		) a6
		where a0.IID = a1.IID and a1.IID = a2.IID and a2.IID = a3.IID and a3.IID = a4.IID and a4.IID = a5.IID and a5.IID = a6.IID
 		order by a0.IID

GO

