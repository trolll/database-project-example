


CREATE PROCEDURE dbo.WspGetItemChangeDetail_Total
	 @startDate		CHAR(8)			-- 搜索开始日期
	,@endDate		CHAR(8)			-- 搜索结束日期
	,@mItemNo		VARCHAR(10)		-- 道具编号
AS
	SET NOCOUNT ON
		
	SELECT mItemNo,mRegDate, 
	mMerchantCreate, mReinforceCreate, mCraftingCreate, mNpcCreate, mMonsterDrop,
	mMerchantDelete, mReinforceDelete, mCraftingDelete, mPcUseDelete, mNpcUseDelete,
	(mMerchantCreate + mReinforceCreate + mCraftingCreate + mNpcCreate + mMonsterDrop) addSum,
	(mMerchantDelete + mReinforceDelete + mCraftingDelete + mPcUseDelete + mNpcUseDelete) delSum  
	FROM FNLStatistics.dbo.TblStatisticsItemByCase  
	WHERE  mRegDate BETWEEN @startDate  AND @endDate AND mItemNo= @mItemNo
	ORDER BY mRegDate

GO

