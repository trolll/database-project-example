
CREATE PROCEDURE dbo.WspGetItemChangeByAllMonster_Total
	 @startDate		CHAR(8)			-- 搜索开始日期
	,@endDate		CHAR(8)			-- 搜索结束日期
	,@mItemNo		VARCHAR(10)		-- 道具编号
AS
	SET NOCOUNT ON
		
	SELECT a.MID,b.MName,a.mRegDate,a.mCreate,a.mDelete
	FROM FNLStatistics.dbo.TblStatisticsItemByMonster a 
	LEFT OUTER JOIN FNLParm.dbo.DT_Monster b 
	ON a.MID = b.MID 
	WHERE  a.mItemNo = @mItemNo AND a.mRegDate BETWEEN @startDate  AND @endDate 
	ORDER BY a.MID,a.mRegDate

GO

