CREATE PROCEDURE dbo.UspGetUserPlay
	@pBeginDate	CHAR(8),
	@pEndDate	CHAR(8),
	@pType	TINYINT = NULL
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		
	IF @pType IS NULL	-- date between 
	BEGIN
	
		SELECT 
			mRegDate
			,mUVCnt
			,mActiveCnt
			,mNewActiveCnt
			,mSecederCnt
		FROM dbo.TblUserPlay
		WHERE mRegDate BETWEEN @pBeginDate AND @pEndDate
		ORDER BY mRegDate ASC
	
		RETURN(0)
	END
	
	
	-- day (between - 30 and now )  
	IF @pType = 3	-- DAY(30)
	BEGIN
	
		SELECT 
			mRegDate
			,mUVCnt
			,mActiveCnt
			,mNewActiveCnt
			,mSecederCnt
		FROM dbo.TblUserPlay
		WHERE mRegDate BETWEEN CONVERT(CHAR, GETDATE()-30, 112) AND CONVERT(CHAR, GETDATE(), 112)
		ORDER BY mRegDate ASC
	
		RETURN(0)
	END	
	
	IF @pType = 2	-- MONTH(TOT)
	BEGIN
	
		SELECT 
			SUBSTRING(mRegDate, 1, 6) mRegDate
			,AVG(mUVCnt) mUVCnt
			,AVG(mActiveCnt) mActiveCnt
			,AVG(mNewActiveCnt) mNewActiveCnt
			,AVG(mSecederCnt) mSecederCnt
		FROM dbo.TblUserPlay
		WHERE mRegDate BETWEEN CONVERT(CHAR, GETDATE()-30, 112) AND CONVERT(CHAR, GETDATE(), 112)	
		GROUP BY SUBSTRING(mRegDate, 1, 6)
		ORDER BY SUBSTRING(mRegDate, 1, 6)	
		
		RETURN(0)			
	END 	
	
	IF @pType = 1	-- YEAR
	BEGIN
	
		SELECT 
			SUBSTRING(mRegDate, 1, 4) mRegDate
			,AVG(mUVCnt) mUVCnt
			,AVG(mActiveCnt) mActiveCnt
			,AVG(mNewActiveCnt) mNewActiveCnt
			,AVG(mSecederCnt) mSecederCnt
		FROM dbo.TblUserPlay
		WHERE mRegDate BETWEEN CONVERT(CHAR, GETDATE()-30, 112) AND CONVERT(CHAR, GETDATE(), 112)	
		GROUP BY SUBSTRING(mRegDate, 1, 4)
		ORDER BY SUBSTRING(mRegDate, 1, 4)	
		
		RETURN(0)			
	END 	
	
	RETURN(0)

GO

