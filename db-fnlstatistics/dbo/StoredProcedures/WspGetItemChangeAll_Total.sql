CREATE  PROCEDURE dbo.WspGetItemChangeAll_Total
	 @endDate		CHAR(8)			-- 搜索结束日期
AS
	SET NOCOUNT ON
		
	SELECT a.mItemNo,b.IName,a.addValue,a.delValue,c.mCreateValue,c.mDeleteValue
	FROM (
	SELECT mItemNo,
	mMerchantCreate + mReinforceCreate + mCraftingCreate + mNpcCreate + mMonsterDrop addValue,
	mMerchantDelete + mReinforceDelete + mCraftingDelete + mPcUseDelete + mNpcUseDelete delValue 
	FROM FNLStatistics.dbo.TblStatisticsItemByCase  
	WHERE  mRegDate = @endDate) a 
	LEFT OUTER JOIN FNLParm.dbo.DT_Item b  ON a.mItemNo=b.IID
	LEFT OUTER JOIN FNLStatistics.dbo.TblItemReference c ON a.mItemNo = c.mItemNo  		
	ORDER BY b.IName

GO

