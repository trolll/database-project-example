
CREATE PROCEDURE dbo.WspGetAllItemChangeByMonster_Total
	 @startDate		CHAR(8)			-- 搜索开始日期
	,@endDate		CHAR(8)			-- 搜索结束日期
	,@MID		VARCHAR(10)		   	-- 怪物编号
AS
	SET NOCOUNT ON
		
	SELECT a.mItemNo,b.IName,a.mRegDate,a.mCreate,a.mDelete
	FROM FNLStatistics.dbo.TblStatisticsItemByMonster a 
	LEFT OUTER JOIN FNLParm.dbo.DT_Item b 
	ON a.mItemNo = b.IID 
	WHERE  a.MID = @MID AND a.mRegDate BETWEEN @startDate  AND @endDate 
	ORDER BY a.mItemNo,a.mRegDate

GO

