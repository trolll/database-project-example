
CREATE PROCEDURE dbo.UspGetObserveItemOfServer
	 @pItemNo		INT
	,@pDate			CHAR(8)			-- 搜索日期
AS
	SET NOCOUNT ON 

	SELECT 
		T2.IName AS mItemNm		
		,T1.mSvrNO
		,T1.mVal
	FROM dbo.TblObserveItem T1
		INNER JOIN FNLParm.dbo.DT_ITEM T2
			ON T1.mItemNo = T2.IID
	WHERE mRegDate = @pDate
		AND mItemNo = @pItemNo
	ORDER BY mSvrNo ASC

GO

