

CREATE PROCEDURE dbo.WspGetItemChangeStandard
	@IType		VARCHAR(10)		-- 道具类型编号
AS
	SET NOCOUNT ON
		
	SELECT a.mItemNo,b.IName,a.mCreateValue,a.mDeleteValue 
	FROM FNLStatistics.dbo.TblItemReference a 
	LEFT OUTER JOIN FNLParm.dbo.DT_Item b ON a.mItemNo=b.IID 
	WHERE b.IType = @IType

GO

