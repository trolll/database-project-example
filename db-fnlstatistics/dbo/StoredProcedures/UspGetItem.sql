
----------------------------------------------------------------
-- PROCEDURES
---------------------------------------------------------------- 
CREATE PROCEDURE dbo.UspGetItem
	@pItemNm	NVARCHAR(40)
AS
	SET NOCOUNT ON 

	IF @pItemNm IS NOT NULL
	BEGIN
		SELECT 
			IID
			, IName
		FROM FNLParm.dbo.DT_ITEM
		WHERE IName LIKE @pItemNm + '%'
		
		RETURN 0
	END 
	
	
	SELECT 
		IID
		, IName
	FROM FNLParm.dbo.DT_ITEM

GO

