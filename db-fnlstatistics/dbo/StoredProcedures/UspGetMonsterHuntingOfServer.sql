
CREATE PROCEDURE dbo.UspGetMonsterHuntingOfServer
	 @pMonsterID	INT
	,@pDate			CHAR(8)			-- 搜索日期
AS
	SET NOCOUNT ON 

	SELECT 
		T1.MID
		,T2.MName
		,T1.mSvrNo
		,mHuntingCnt 		
	FROM dbo.TblStatisticsMonsterHunting T1
		INNER JOIN FNLParm.dbo.DT_Monster T2
			ON T1.MID = T2.MID
	WHERE mRegDate = @pDate 
		AND T1.MID = @pMonsterID

GO

