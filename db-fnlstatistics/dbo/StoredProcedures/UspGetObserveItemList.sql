
CREATE PROCEDURE dbo.UspGetObserveItemList
AS
	SELECT 
		IID mItemNo,
		IName mItemName
	FROM FNLParm.dbo.DT_ITEM
	
	IF @@ERROR <> 0
	BEGIN
		RETURN(1)	-- db sql error
	END
	
	RETURN(0)

GO

