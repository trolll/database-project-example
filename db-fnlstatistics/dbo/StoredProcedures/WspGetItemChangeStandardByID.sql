

CREATE PROCEDURE dbo.WspGetItemChangeStandardByID
	@mItemNo		VARCHAR(10)		-- 道具编号
AS
	SET NOCOUNT ON
		
	SELECT a.mItemNo,a.mCreateValue,a.mDeleteValue 
	FROM FNLStatistics.dbo.TblItemReference a 
	WHERE a.mItemNo = @mItemNo

GO

