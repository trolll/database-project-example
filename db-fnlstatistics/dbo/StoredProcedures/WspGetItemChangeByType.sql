
CREATE  PROCEDURE dbo.WspGetItemChangeByType
	 @svrNo		        VARCHAR(10)		-- 服务器
	,@endDate		CHAR(8)			-- 搜索结束日期
	,@iType		        VARCHAR(10)		-- 搜索道具类型
AS
	SET NOCOUNT ON
		
	SELECT a.mItemNo,b.IName,a.addValue,a.delValue,c.mCreateValue,c.mDeleteValue
	FROM (
	SELECT mItemNo,
	mMerchantCreate + mReinforceCreate + mCraftingCreate + mNpcCreate + mMonsterDrop addValue,
	mMerchantDelete + mReinforceDelete + mCraftingDelete + mPcUseDelete + mNpcUseDelete delValue 
	FROM FNLStatistics.dbo.TblStatisticsItemByCase  
	WHERE mSvrNo= @svrNo AND mRegDate = @endDate) a 
	LEFT OUTER JOIN FNLParm.dbo.DT_Item b  ON a.mItemNo = b.IID
	LEFT OUTER JOIN FNLStatistics.dbo.TblItemReference c ON a.mItemNo = c.mItemNo  		
	WHERE b.IType = @iType
	ORDER BY b.IName

GO

