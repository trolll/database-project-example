
/******************************************************************************
**		Name: WspGetMaxPeriodConInfoOfServer
**		Desc: 扁埃措喊 辑滚悼立阑 荐摹甫 佬绢柯促
**
**		Auth: 辫籍玫
**		Date: 2009-04-22
*******************************************************************************
**		Change History
*******************************************************************************
**		Date:		Author:				Description:
**		--------	--------			---------------------------------------
**      20090623	JUDY				悼立 炼雀 滚弊 荐沥
**		20090811	JUDY				QA 棺 扁鸥 烙矫 辑滚 笼拌俊辑 力芭
**		20090914	JUDY				悼立 炼雀 譬醋
*******************************************************************************/
CREATE PROCEDURE dbo.WspGetMaxPeriodConInfoOfServer
	@in_begin_date	CHAR(8),
	@in_end_date	CHAR(8)
AS
	SET NOCOUNT ON			-- 汲疙 : 搬苞 饭内靛 悸阑 馆券 救 矫挪促.
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	--- 函荐 急攫
	DECLARE @aSvrNoList NVARCHAR(4000),
			@aSql		NVARCHAR(4000),
			@aNewLine	VARCHAR(2)

	SELECT @aSvrNoList = '',
		@aSql = '',
		@aNewline = NCHAR(13) + NCHAR(10)

	-- 炼雀且 辑滚 格废 啊廉坷扁
	SELECT @aSvrNoList = @aSvrNoList + ', ISNULL(SUM( CASE WHEN T1.mSvrNo = ' + CAST(mSvrNo AS VARCHAR) + ' THEN mMaxUserCnt END ),0) AS ''' + CAST(mSvrNo AS VARCHAR) + ''''
	FROM
		(
			SELECT DISTINCT mSvrNo
			FROM dbo.TblConCurInfo
			WHERE mRegDate >= CAST(@in_begin_date AS DATETIME)
				AND mRegDate <= CAST(@in_end_date AS DATETIME)
		) T1
	WHERE mSvrNo <= 800	-- QA 棺 扁鸥 烙矫 辑滚 力芭	
	ORDER BY mSvrNo ASC		

	-- 辑滚 锅龋 格废俊辑 菊狼 镜葛 绝绰 巩磊客, 第狼 醚钦 凯 眠啊	
	SET @aSvrNoList = STUFF(@aSvrNoList, 1, 2, '') + ', ISNULL(SUM( mMaxUserCnt), 0) AS ''TOT'''

	SET @aSql = 
	N'	WITH CTE_CURR AS ' + @aNewline + 
	N'	( ' + @aNewline + 
	N'		SELECT    ' + @aNewline + 
	N'			mSvrNo,  ' + @aNewline + 
	N'			CONVERT(CHAR(8) , mRegDate, 112) AS mDate,  ' + @aNewline + 
	N'			DATEPART(hh, mRegDate) AS mHour, 			 ' + @aNewline + 
	N'			MAX(mUserCnt) As mMaxUserCnt  ' + @aNewline + 
	N'		FROM dbo.TblConCurInfo WITH(NOLOCK)  ' + @aNewline + 
	N'		WHERE mRegDate >= CONVERT(DATETIME, @in_begin_date )  ' + @aNewline + 
	N'				AND mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_End_Date ) )  ' + @aNewline + 
	N'		GROUP BY CONVERT(CHAR(8) , mRegDate, 112), DATEPART(hh, mRegDate), mSvrNo  ' + @aNewline + 
	N'	), CTE_CURR_DD_HH AS ' + @aNewline + 
	N'	( ' + @aNewline + 
	N'		SELECT  ' + @aNewline + 
	N'			mDate ' + @aNewline + 
	N'			, mHour ' + @aNewline + 
	N'			, ' + @aSvrNoList + @aNewline +
	N'		FROM CTE_CURR T1 ' + @aNewline + 
	N'		GROUP BY mDate, mHour ' + @aNewline + 
	N'	), CTE_CURR_DD_MAX AS ' + @aNewline + 
	N'	( ' + @aNewline + 
	N'		 ' + @aNewline + 
	N'		SELECT ' + @aNewline + 
	N'			mDate, MAX(TOT) AS mMaxUserCnt ' + @aNewline + 
	N'		FROM CTE_CURR_DD_HH ' + @aNewline + 
	N'		GROUP BY mDate ' + @aNewline + 
	N'	) ' + @aNewline + 
	N'	SELECT  ' + @aNewline + 
	N'		T1.* ' + @aNewline + 
	N'	FROM CTE_CURR_DD_HH T1 ' + @aNewline + 
	N'		INNER JOIN CTE_CURR_DD_MAX T2 ' + @aNewline + 
	N'			ON T1.mDate = T2.mDate ' + @aNewline + 
	N'				AND T1.TOT = T2.mMaxUserCnt ' + @aNewline + 
	N'	ORDER BY T1.mDate ASC; '

	-- 悼利 SQL角青
	exec sp_executesql @aSql,
		N'@in_begin_date CHAR(8), @in_end_date	CHAR(8)',
		@in_begin_date = @in_begin_date, @in_end_date = @in_end_date

GO

