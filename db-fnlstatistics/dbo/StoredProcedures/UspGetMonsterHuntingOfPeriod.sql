

CREATE PROCEDURE dbo.UspGetMonsterHuntingOfPeriod
	 @pSvrNo		TINYINT			-- NULL
	,@pDate			CHAR(8)			-- 搜索日期
	,@pMonsterID	INT				-- 怪物 ID
AS
	SET NOCOUNT ON
	
	DECLARE @aDate	CHAR(8)
			,@aPrevDate	CHAR(8)

	SELECT @aDate = CONVERT(CHAR, DATEADD(D, 1, @pDate ), 112)
			,@aPrevDate = CONVERT(CHAR, DATEADD(D, -8, @pDate ), 112)
	

	SELECT 
		CONVERT(CHAR(8), mRegDate, 112) AS mRegDate
		, mHuntingCnt
	FROM dbo.TblStatisticsMonsterHunting
	WHERE MID = @pMonsterID
		AND (mRegDate >= @aPrevDate AND mRegDate < @aDate)
			

	RETURN(0)

GO

