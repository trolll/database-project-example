CREATE PROCEDURE dbo.WspGetMonsterHuntedCount
	 @svrNo		        VARCHAR(10)		-- 服务器
	,@startDate		CHAR(8)			-- 搜索开始日期
	,@endDate		CHAR(8)			-- 搜索结束日期
AS
	SET NOCOUNT ON
		
	SELECT a.MID,b.MName,a.mRegDate,a.mHuntingCnt
	FROM FNLStatistics.dbo.TblStatisticsMonsterHunting a 
	LEFT OUTER JOIN FNLParm.dbo.DT_Monster b 
	ON a.MID = b.MID 
	WHERE a.mSvrNo = @svrNo AND a.mRegDate BETWEEN @startDate AND @endDate 
	ORDER BY a.MID,a.mRegDate

GO

