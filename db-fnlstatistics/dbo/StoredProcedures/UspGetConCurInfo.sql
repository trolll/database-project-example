CREATE PROCEDURE [dbo].[UspGetConCurInfo]	
AS		
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	
	DECLARE @Max_DateTime	DATETIME
	
	SELECT TOP 1
		@Max_DateTime = mRegDate
	FROM dbo.TblConCurInfo
	ORDER BY mRegDate DESC
		
	IF @Max_DateTime = NULL
		SET @Max_DateTime = GETDATE()
		
	--------------------------------------------------------------------------	
	-- 抛挤侩 悼立 单捞鸥甫 佬绢柯促
	--------------------------------------------------------------------------
	INSERT INTO dbo.TblConCurInfo (mRegDate, mSvrNo, mUserCnt, mPcCnt)
	SELECT mLastChg, mSvrNo, mUserCnt, mPcCnt
	FROM FNLParm.dbo.TblConCurInfoNow
	WHERE mLastChg > @Max_DateTime

GO

