
CREATE PROCEDURE dbo.UspGetMaxPeriodConInfo
	@in_begin_date	CHAR(8),
	@in_end_date	CHAR(8)
AS 
	SET NOCOUNT ON
	DECLARE @tm_periodconinfo TABLE (
		mDate		CHAR(8),
		mHour		TINYINT,
		mUserCnt	INT
	)
	
	
	INSERT INTO @tm_periodconinfo
	SELECT 
		mDate, 
		mHour,  
		Sum(mMaxUserCnt)  AS mMaxUserCnt
	FROM (
		SELECT  
			CONVERT(CHAR(8) , mRegDate, 112) AS mDate,
			DATEPART(hh, mRegDate) AS mHour,
			mSvrNo,
			MAX(mUserCnt) As mMaxUserCnt
		FROM dbo.TblConCurInfo WITH(NOLOCK)	
		WHERE mRegDate >= CONVERT(DATETIME, @in_begin_date )
				AND mRegDate <=  DATEADD(DD, 1,  CONVERT(DATETIME, @in_End_Date ) )
		GROUP BY CONVERT(CHAR(8) , mRegDate, 112), DATEPART(hh, mRegDate), mSvrNo		
		) T1
	GROUP BY mDate, mHour
	
	
	
	SELECT T1.*, 
		(
		SELECT TOP 1 mHour
		FROM @tm_periodconinfo
		WHERE mDate = T1.mDate AND 
			mUserCnt = T1.mUserCnt		
		) As mHour,
		mWeek =
		CASE DATEPART(WeekDay, mDate)
			WHEN 1 THEN 'Sunday'
			WHEN 2 THEN 'Monday'
			WHEN 3 THEN 'Tuesday'
			WHEN 4 THEN 'Wednesday'
			WHEN 5 THEN 'Thursday'
			WHEN 6 THEN 'Friday'
			WHEN 7 THEN 'Saturday'
			ELSE 'Non-Day'
		END		
	FROM  (
		SELECT
			mDate, 
			Max(mUserCnt)AS mUserCnt
		FROM @tm_periodconinfo T1
		GROUP BY mDate
		
	) T1
	ORDER BY mDate ASC

GO

