
CREATE PROCEDURE dbo.UspGetObserveItem
	 @pSvrNo		TINYINT			-- NULL
	,@pDate			CHAR(8)			-- 搜索日期
AS
	DECLARE @aDate	CHAR(8)
			,@aPrevDate	CHAR(8)

	SELECT @aDate = @pDate
			,@aPrevDate = CONVERT(CHAR, DATEADD(D, -1, @pDate ), 112)
	------------------------------------------------------------------
	-- 特定服务器道具抽取
	------------------------------------------------------------------
	IF @pSvrNo IS NOT NULL
	BEGIN
	
		SELECT 
			T1.IID AS mItemNo
			,T1.IName AS mItemName
			,ISNULL(T2.mVal, 0) AS mVal
			,ISNULL(T3.mVal, 0) AS mPrevVal
			,ISNULL(T2.mVal,0) - ISNULL(T3.mVal, 0) As mDiff
		FROM FNLParm.dbo.DT_ITEM T1
			LEFT OUTER JOIN (							
				SELECT 
					mItemNo
					, SUM(mVal) AS mVal			
				FROM TblObserveItem
				WHERE mRegDate = @aDate
					AND mSvrNo = @pSvrNo
				GROUP BY mItemNo 
			) T2 ON T1.IID = T2.mItemNo
			LEFT OUTER JOIN (
				SELECT 
					mItemNo
					, SUM(mVal) AS mVal			
				FROM TblObserveItem
				WHERE mRegDate = @aPrevDate
					AND mSvrNo = @pSvrNo
				GROUP BY mItemNo 
			) T3 ON T1.IID = T3.mItemNo

		RETURN(0)
	END
	
	------------------------------------------------------------------
	-- 全部道具现况
	------------------------------------------------------------------
	SELECT 
		T1.IID AS mItemNo
		,T1.IName AS mItemName
		,ISNULL(T2.mVal, 0) AS mVal
		,ISNULL(T3.mVal, 0) AS mPrevVal
		,ISNULL(T2.mVal,0) - ISNULL(T3.mVal, 0) As mDiff
	FROM FNLParm.dbo.DT_ITEM T1
		LEFT OUTER JOIN (							
			SELECT 
				mItemNo
				, SUM(mVal) AS mVal			
			FROM TblObserveItem
			WHERE mRegDate = @aDate
				AND mSvrNo NOT IN( 117 )
			GROUP BY mItemNo 
		) T2 ON T1.IID = T2.mItemNo
		LEFT OUTER JOIN (
			SELECT 
				mItemNo
				, SUM(mVal) AS mVal			
			FROM TblObserveItem
			WHERE mRegDate = @aPrevDate
				AND mSvrNo NOT IN( 117 )
			GROUP BY mItemNo 
		) T3 ON T1.IID = T3.mItemNo
   
	RETURN(0)

GO

