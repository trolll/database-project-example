CREATE PROCEDURE dbo.UspGetItemByCaseClass
	@pStartDate	INT
	,@pEndDate	INT
	,@pTID		INT
	,@pClass	SMALLINT
	,@pStartLevel SMALLINT
	,@pEndLevel SMALLINT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	

	IF ( @pEndLevel - @pStartLevel) > 10	
		RETURN 2	-- level overflow (inner 10 level diff)
		
	IF ( DATEDIFF(D,  CONVERT(SMALLDATETIME, CONVERT(CHAR(8), @pStartDate) ) , CONVERT(SMALLDATETIME,  CONVERT(CHAR(8),@pEndDate) ) ) ) > 7
		RETURN 3
	

	SELECT 
		mSvrNo, mRegDate, mItemNo, mLevel, mClass, mMerchantCreate, mMerchantDelete, mReinforceCreate, 
		mReinforceDelete, mCraftingCreate, mCraftingDelete, mPcUseDelete, mNpcUseDelete, mNpcCreate, 
		mMonsterDrop, mGSExchangeCreate, mGSExchangeDelete
	FROM dbo.TblStatisticsItemByLevelClass
	WHERE mRegDate BETWEEN @pStartDate AND @pEndDate
		AND mItemNo = @pTID		
		AND mLevel BETWEEN @pStartLevel AND @pEndLevel
		AND mClass = @pClass
	ORDER BY mRegDate

GO

