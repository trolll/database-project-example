
CREATE PROCEDURE dbo.UspGetMonsterHunting
	 @pSvrNo		TINYINT			-- NULL
	,@pDate			CHAR(8)			-- 搜索日期
AS
	SET NOCOUNT ON
		
	IF @pSvrNo IS NULL
	BEGIN
	
		SELECT 
			T1.*
			, T2.MName
		FROM (
			SELECT 
				T1.MID
				,ISNULL(T1.mHuntingCnt, 0) AS mVal
				,ISNULL(T2.mHuntingCnt, 0) AS mPrevVal
				,ISNULL(T1.mHuntingCnt,0) - ISNULL(T2.mHuntingCnt, 0) As mDiff		
			FROM (	
				SELECT 
					MID
					,SUM(mHuntingCnt) AS mHuntingCnt
				FROM dbo.TblStatisticsMonsterHunting
				WHERE mRegDate = @pDate
				GROUP BY MID					
			) T1 LEFT OUTER JOIN 
			(	
				SELECT 
					MID
					,SUM(mHuntingCnt) AS mHuntingCnt
				FROM dbo.TblStatisticsMonsterHunting
				WHERE mRegDate = @pDate - 1
				GROUP BY MID	
			) T2 
				ON T1.MID = T2.MID 
		) T1 
			INNER JOIN FNLParm.dbo.DT_Monster T2
				ON T1.MID = T2.MID	
	
		RETURN(0)
	END 
	
	
	SELECT 
		T1.*
		, T2.MName
	FROM (
		SELECT 
			T1.MID
			,ISNULL(T1.mHuntingCnt, 0) AS mVal
			,ISNULL(T2.mHuntingCnt, 0) AS mPrevVal
			,ISNULL(T1.mHuntingCnt,0) - ISNULL(T2.mHuntingCnt, 0) As mDiff		
		FROM (	
			SELECT 
				MID
				,mHuntingCnt 		
			FROM dbo.TblStatisticsMonsterHunting
			WHERE mRegDate = @pDate 
				AND mSvrNo = @pSvrNo
		) T1 LEFT OUTER JOIN 
		(	
			SELECT 
				MID
				,mHuntingCnt 		
			FROM dbo.TblStatisticsMonsterHunting
			WHERE mRegDate = @pDate -1
				AND mSvrNo = @pSvrNo
		) T2 
			ON T1.MID = T2.MID 
	) T1 
		INNER JOIN FNLParm.dbo.DT_Monster T2
			ON T1.MID = T2.MID

	RETURN(0)

GO

