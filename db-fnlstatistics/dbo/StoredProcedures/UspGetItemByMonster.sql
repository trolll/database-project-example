
CREATE PROCEDURE dbo.UspGetItemByMonster
	@pStartDate	CHAR(8)
	,@pEndDate	CHAR(8)
	,@pSvrType	TINYINT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	-- warring : test server除外 (mSvrNo 117)	

	IF @pSvrType = 1 
	BEGIN
		
		SELECT 
			mSvrNo
			,mRegDate
			,T2.MID
			,T2.MName
			,mCreate
			,mDelete			
		FROM (
			SELECT 
				mSvrNo
				,mRegDate
				,MID
				,SUM(mCreate) AS mCreate
				,SUM(mDelete) AS mDelete			
			FROM dbo.TblStatisticsItemByMonster
			WHERE mRegDate BETWEEN @pStartDate AND @pEndDate
				AND mSvrNo NOT IN(117)
				AND mItemNo = 409
			GROUP BY mRegDate, MID , mSvrNo 
		) T1 INNER JOIN FNLParm.dbo.DT_Monster T2
			ON T1.MID = T2.MID
		ORDER BY mRegDate ASC, T1.MID ASC, mSvrNo ASC
	
		RETURN(0)
	END 

	SELECT 
		NULL AS mSvrNo
		,mRegDate
		,T2.MID
		,T2.MName
		,mCreate
		,mDelete			
	FROM (
		SELECT 
			mRegDate
			,MID
			,SUM(mCreate) AS mCreate
			,SUM(mDelete) AS mDelete			
		FROM dbo.TblStatisticsItemByMonster
		WHERE mRegDate BETWEEN @pStartDate AND @pEndDate
			AND mSvrNo NOT IN(117)
			AND mItemNo = 409
		GROUP BY mRegDate, MID 
	) T1 INNER JOIN FNLParm.dbo.DT_Monster T2
		ON T1.MID = T2.MID		
	ORDER BY mRegDate ASC, T1.MID ASC
	
	RETURN 0

GO

