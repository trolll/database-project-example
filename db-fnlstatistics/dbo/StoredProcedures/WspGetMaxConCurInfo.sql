-- modify by huangl
-- ALTER PROCEDURE [dbo].[WspGetMaxConCurInfo]
Create PROCEDURE [dbo].[WspGetMaxConCurInfo]
	@in_date	CHAR(10)	-- 2006-10-10
AS
	SET NOCOUNT ON			-- 汲疙 : 搬苞 饭内靛 悸阑 馆券 救 矫挪促.
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @aSvrNoList nVARCHAR(4000),
			@aSql		nVARCHAR(4000),
			@aNewLine	VARCHAR(2),
			@aDate		DATETIME

	-- 函荐甸 檬扁拳
	SELECT @aSvrNoList = '',
		@aSql = '',
		@aNewline = NCHAR(13) + NCHAR(10),
		@aDate = CAST(@in_date AS DATETIME)

	-- 八祸且 辑滚 锅龋甸 啊廉坷扁
	SELECT @aSvrNoList = @aSvrNoList + ', ISNULL(SUM( CASE WHEN T1.mSvrNo = ' + CAST(mSvrNo AS VARCHAR) + ' THEN mMaxUserCnt END ),0) AS ''' + CAST(mSvrNo AS VARCHAR) + ''''
	FROM
		(
			SELECT DISTINCT mSvrNo
			FROM dbo.TblConCurInfo
			WHERE mRegDate >= CAST(@in_date AS DATETIME)
				AND mRegDate <= DATEADD(DD, 1, CAST(@in_date AS DATETIME))
				
		) T1
	ORDER BY mSvrNo ASC

	-- 辑滚 锅龋 格废俊辑 菊狼 镜葛 绝绰 巩磊客, 第狼 醚钦 凯 眠啊
	SET @aSvrNoList = STUFF(@aSvrNoList, 1, 2, '') + ', ISNULL(SUM( mMaxUserCnt), 0) AS ''TOT'''

	-- 悼利SQL累己
	SET @aSql = 
		N'SELECT mHour, ' + @aSvrNoList + @aNewline +
		N'FROM (' + @aNewline +
		N'		SELECT mCode AS mHour, mSvrNo, MAX(mUserCnt) AS mMaxUserCnt' + @aNewline +
		N'		FROM (' + @aNewline +
		N'			SELECT DATEPART(hh, mRegDate) AS mHour, mSvrNo, mUserCnt' + @aNewline +
		N'			FROM dbo.TblConCurInfo' + @aNewline +
		N'			WHERE mRegDate >= @in_Date' + @aNewline +
		N'				AND mRegDate <= DATEADD(DD, 1, @in_Date)' + @aNewline +
		N'			) T1' + @aNewline +
		N'			RIGHT OUTER JOIN dbo.TblCode T2' + @aNewline +
		N'				ON T1.mHour = T2.mCode' + @aNewline +
		N'		WHERE T2.mCodeType = 3' + @aNewline +
		N'		GROUP BY mCode, mSvrNo' + @aNewline +
		N'		) T1' + @aNewline +
		N'GROUP BY mHour WITH ROLLUP' + @aNewline +
		N'' + @aNewline +
		N''

	-- PRINT @aSql

	-- 悼利 SQL角青
	exec sp_executesql @aSql, N'@in_Date DATETIME', @in_Date = @aDate

GO

