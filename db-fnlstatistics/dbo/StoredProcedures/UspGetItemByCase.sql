CREATE PROCEDURE dbo.UspGetItemByCase
	@pStartDate	CHAR(8)
	,@pEndDate	CHAR(8)
	,@pSvrType	TINYINT
	,@pTID		INT
AS
	SET NOCOUNT ON 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	-- warring : test server绰 力寇(mSvrNo 117)

	IF @pSvrType = 1 
	BEGIN
		SELECT 
			T1.mRegDate
			, T1.mSvrNo AS mSvrNo
			, ISNULL(T2.mVal, 0) AS mItemTotal
			, ISNULL(T2.mVal,0) - ISNULL(T2.mPrevVal, 0) As mItemDiffTotal
			, T1.mMerchantCreate
			, T1.mMerchantDelete
			, T1.mReinforceCreate
			, T1.mReinforceDelete
			, T1.mCraftingCreate
			, T1.mCraftingDelete
			, T1.mPcUseDelete
			, T1.mNpcUseDelete
			, T1.mNpcCreate
			, T1.mMonsterDrop
			, T1.mMonsterDrop
			, T1.mGSExchangeCreate
			, T1.mGSExchangeDelete
		FROM (	
			-- total
			SELECT 
				mRegDate
				, mSvrNo
				,SUM(mMerchantCreate) AS mMerchantCreate
				,SUM(mMerchantDelete) AS mMerchantDelete
				,SUM(mReinforceCreate) AS mReinforceCreate
				,SUM(mReinforceDelete) AS mReinforceDelete
				,SUM(mCraftingCreate) AS mCraftingCreate
				,SUM(mCraftingDelete) AS mCraftingDelete
				,SUM(mPcUseDelete) AS mPcUseDelete
				,SUM(mNpcUseDelete) AS mNpcUseDelete
				,SUM(mNpcCreate) AS mNpcCreate
				,SUM(mMonsterDrop) AS mMonsterDrop
				,SUM(mGSExchangeCreate) AS mGSExchangeCreate
				,SUM(mGSExchangeDelete) AS mGSExchangeDelete				
			FROM dbo.TblStatisticsItemByCase
			WHERE mRegDate BETWEEN @pStartDate AND @pEndDate
				AND mItemNo = @pTID				
			GROUP BY mRegDate, mSvrNo
		) T1 LEFT OUTER JOIN  (		
				SELECT 
					mRegDate
					, mSvrNo
					, SUM(mVal) AS mVal	-- today
					, (
					
						SELECT 
							SUM(mVal)					
						FROM dbo.TblObserveItem
						WHERE mRegDate = CONVERT(CHAR, DATEADD(DD,  -1, T1.mRegDate), 112)
							AND mItemNo = @pTID
							AND mSvrNo  = T1.mSvrNo				
					) As mPrevVal
				FROM dbo.TblObserveItem T1
				WHERE mRegDate BETWEEN @pStartDate AND @pEndDate						
						AND mItemNo = @pTID
				GROUP BY mRegDate, mSvrNo
		) T2 ON T1.mRegDate = T2.mRegDate 
			AND T1.mSvrNo = T2.mSvrNo
	
		RETURN(0)
	END 


	-- Sum total 
	SELECT 
		T1.mRegDate
		, NULL AS mSvrNo
		, ISNULL(T2.mVal, 0) AS mItemTotal
		, ISNULL(T2.mVal,0) - ISNULL(T2.mPrevVal, 0) As mItemDiffTotal
		, T1.mMerchantCreate
		, T1.mMerchantDelete
		, T1.mReinforceCreate
		, T1.mReinforceDelete
		, T1.mCraftingCreate
		, T1.mCraftingDelete
		, T1.mPcUseDelete
		, T1.mNpcUseDelete
		, T1.mNpcCreate
		, T1.mMonsterDrop
		, T1.mMonsterDrop
		, T1.mGSExchangeCreate
		, T1.mGSExchangeDelete		
	FROM (	
		-- total
		SELECT  
			mRegDate
			,SUM(mMerchantCreate) AS mMerchantCreate
			,SUM(mMerchantDelete) AS mMerchantDelete
			,SUM(mReinforceCreate) AS mReinforceCreate
			,SUM(mReinforceDelete) AS mReinforceDelete
			,SUM(mCraftingCreate) AS mCraftingCreate
			,SUM(mCraftingDelete) AS mCraftingDelete
			,SUM(mPcUseDelete) AS mPcUseDelete
			,SUM(mNpcUseDelete) AS mNpcUseDelete
			,SUM(mNpcCreate) AS mNpcCreate
			,SUM(mMonsterDrop) AS mMonsterDrop
			,SUM(mGSExchangeCreate) AS mGSExchangeCreate
			,SUM(mGSExchangeDelete) AS mGSExchangeDelete							
		FROM dbo.TblStatisticsItemByCase
		WHERE mRegDate BETWEEN @pStartDate AND @pEndDate
			AND mItemNo = @pTID
			AND mSvrNo NOT IN(117)
		GROUP BY mRegDate 
	) T1 LEFT OUTER JOIN  (		
			SELECT 
				mRegDate
				, SUM(mVal) AS mVal	-- today
				, (
				
					SELECT 
						SUM(mVal)					
					FROM dbo.TblObserveItem
					WHERE mRegDate = CONVERT(CHAR, DATEADD(DD,  -1, T1.mRegDate), 112)
						AND mItemNo = @pTID
						AND mSvrNo  NOT IN (117)				
				) As mPrevVal
			FROM dbo.TblObserveItem T1
			WHERE mRegDate BETWEEN @pStartDate AND @pEndDate
					AND mSvrNo  NOT IN (117)
					AND mItemNo = @pTID
			GROUP BY mRegDate
	) T2 ON  T1.mRegDate = T2.mRegDate 

	RETURN 0

GO

