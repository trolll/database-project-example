CREATE PROCEDURE dbo.UspInsAuditToStatDB
	@pSvrNo		SMALLINT
	, @pSvrPath		NVARCHAR(4000)
	, @WORKDATE		CHAR(8) = ''
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	--select @pSvrNo=101, @pSvrPath= 'H65627.FNLAudit1'
	IF @WORKDATE = '' 
	BEGIN
		SET @WORKDATE = CONVERT(CHAR(8), GETDATE(),112)
	END 

	DELETE FROM dbo.TblObserveItem
	WHERE mRegDate = @WORKDATE 
		AND mSvrNo = @pSvrNo


	DECLARE @aSQL		NVARCHAR(4000)
	DECLARE @aParm	NVARCHAR(4000)
	
	SET @aParm = '@aSvrNo SMALLINT, @aSvrPath NVARCHAR(4000) '

	SET @aSQL =		N' INSERT INTO dbo.TblObserveItem (mSvrno, mItemNo, mVal) '
	SET @aSQL = @aSQL +	N' SELECT ' +  CONVERT(NVARCHAR, @pSvrNo) 
	SET @aSQL = @aSQL +	N' , mItemNo '
	SET @aSQL = @aSQL +	N' , mCnt '
	SET @aSQL = @aSQL +	N' FROM ' + CONVERT(NVARCHAR(4000), @pSvrPath)  + '.dbo.TblObserveItem '

	EXEC sp_executesql @aSQL, @aParm, @aSvrNo = @pSvrNo, @aSvrPath = @pSvrPath	
	
	SET @aParm = '@aSvrNo SMALLINT, @aSvrPath NVARCHAR(4000)'

	SET @aSQL =		N' INSERT INTO dbo.TblObserveItemDetail (mSvrno, mItemNo, mStatus, mEquipPos, mVal) '
	SET @aSQL = @aSQL +	N' SELECT ' +  CONVERT(NVARCHAR, @pSvrNo) 
	SET @aSQL = @aSQL +	N' , mItemNo '
	SET @aSQL = @aSQL +	N' , mStatus, mEquipPos, mCnt '
	SET @aSQL = @aSQL +	N' FROM ' + CONVERT(NVARCHAR(4000), @pSvrPath)  + '.dbo.TblObserveItemDetail '

	EXEC sp_executesql @aSQL, @aParm, @aSvrNo = @pSvrNo, @aSvrPath = @pSvrPath

GO

