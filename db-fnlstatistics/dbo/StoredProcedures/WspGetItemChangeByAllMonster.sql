

CREATE PROCEDURE dbo.WspGetItemChangeByAllMonster
	 @svrNo		        VARCHAR(10)		-- 服务器
	,@startDate		CHAR(8)			-- 搜索开始日期
	,@endDate		CHAR(8)			-- 搜索结束日期
	,@mItemNo		VARCHAR(10)		-- 道具编号
AS
	SET NOCOUNT ON
		
	SELECT a.MID,b.MName,a.mRegDate,a.mCreate,a.mDelete
	FROM FNLStatistics.dbo.TblStatisticsItemByMonster a 
	LEFT OUTER JOIN FNLParm.dbo.DT_Monster b 
	ON a.MID = b.MID 
	WHERE a.mSvrNo = @svrNo AND a.mItemNo = @mItemNo AND a.mRegDate BETWEEN @startDate  AND @endDate 
	ORDER BY a.MID,a.mRegDate

GO

