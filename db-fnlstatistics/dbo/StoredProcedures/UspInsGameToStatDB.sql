/*************************************************************************************************************
** 2011-03-31 菩摹
*************************************************************************************************************/
CREATE PROCEDURE [dbo].[UspInsGameToStatDB]
	@pSvrNo			smallint,
	@pSvrPath		NVARCHAR(4000)
AS
	SET NOCOUNT ON 
	
	DECLARE @aSQL	NVARCHAR(4000)
	DECLARE @aParm	NVARCHAR(4000)
				, @aObjNm NVARCHAR(300)	
				, @aIsObjCnt INT
				
	SELECT @aIsObjCnt = 0;			

	/*
	RAISERROR
		(N'The SvrNo is:%d, the database name is: %s.',
		10, -- Severity.
		1, -- State.
		@pSvrNo, -- First substitution argument.
		@pSvrPath) WITH LOG; -- Second substitution argument.
	*/
	
	-- dbo.TblStatisticsItemByCase
	SET @aParm = '@aSvrNo SMALLINT, @aSvrPath NVARCHAR(4000)'
		
	SET @aSQL =			N' INSERT INTO dbo.TblStatisticsItemByLevelClass(mSvrNo,mRegDate,mItemNo, mLevel, mClass,'
	SET @aSQL = @aSQL +	N'	mMerchantCreate,mMerchantDelete,mReinforceCreate,mReinforceDelete,'
	SET @aSQL = @aSQL +	N'  mCraftingCreate,mCraftingDelete,mPcUseDelete,mNpcUseDelete,mNpcCreate,'
	SET @aSQL = @aSQL +	N'  mMonsterDrop, mGSExchangeCreate, mGSExchangeDelete)	'
	SET @aSQL = @aSQL +	N' SELECT ' +  CONVERT(NVARCHAR(4000), @pSvrNo) 
	SET @aSQL = @aSQL +	N',CONVERT(INT,CONVERT(VARCHAR, GETDATE(), 112)) '
	SET @aSQL = @aSQL +	N',mItemNo '
	SET @aSQL = @aSQL +	N',mLevel '
	SET @aSQL = @aSQL +	N',mClass '
	SET @aSQL = @aSQL +	N',SUM(mMerchantCreate) '
	SET @aSQL = @aSQL +	N',SUM(mMerchantDelete) '
	SET @aSQL = @aSQL +	N',SUM(mReinforceCreate) '
	SET @aSQL = @aSQL +	N',SUM(mReinforceDelete) '
	SET @aSQL = @aSQL +	N',SUM(mCraftingCreate) '
	SET @aSQL = @aSQL +	N',SUM(mCraftingDelete) '
	SET @aSQL = @aSQL +	N',SUM(mPcUseDelete) '
	SET @aSQL = @aSQL +	N',SUM(mNpcUseDelete) '
	SET @aSQL = @aSQL +	N',SUM(mNpcCreate) '
	SET @aSQL = @aSQL +	N',SUM(mMonsterDrop) '
	SET @aSQL = @aSQL +	N',SUM(mGSExchangeCreate) '
	SET @aSQL = @aSQL +	N',SUM(mGSExchangeDelete) '
	SET @aSQL = @aSQL +	N' FROM ' + CONVERT(NVARCHAR(4000), @pSvrPath)  + '.dbo.TblStatisticsItemByLevelClass'
	SET @aSQL = @aSQL + N' GROUP BY mItemNo, mLevel, mClass'

	EXEC sp_executesql @aSQL, @aParm, @aSvrNo = @pSvrNo, @aSvrPath = @pSvrPath	

	-- dbo.TblStatisticsItemByMonster
	SET @aParm = '@aSvrNo SMALLINT, @aSvrPath NVARCHAR(4000)'
		
	SET @aSQL =			N' INSERT INTO dbo.TblStatisticsItemByMonster(mSvrNo,mRegDate, '
	SET @aSQL = @aSQL +	N'	MID,mItemNo,mCreate,mDelete) '
	SET @aSQL = @aSQL +	N' SELECT ' +  CONVERT(NVARCHAR(4000), @pSvrNo) 
	SET @aSQL = @aSQL +	N',CONVERT(CHAR, GETDATE(), 112) '
	SET @aSQL = @aSQL +	N',MID'
	SET @aSQL = @aSQL +	N',mItemNo'
	SET @aSQL = @aSQL +	N',SUM(mCreate)'
	SET @aSQL = @aSQL +	N',SUM(mDelete)'
	SET @aSQL = @aSQL +	N' FROM ' + CONVERT(NVARCHAR(4000), @pSvrPath)  + '.dbo.TblStatisticsItemByMonster'
	SET @aSQL = @aSQL +	N' GROUP BY MID, mItemNo'
	
	EXEC sp_executesql @aSQL, @aParm, @aSvrNo = @pSvrNo, @aSvrPath = @pSvrPath		
	
	
	-- dbo.TblStatisticsMonsterHunting
	SET @aParm = '@aSvrNo SMALLINT, @aSvrPath NVARCHAR(4000)'
		
	SET @aSQL =			N' INSERT INTO dbo.TblStatisticsMonsterHunting(mSvrNo,mRegDate,MID,mHuntingCnt) '
	SET @aSQL = @aSQL +	N' SELECT ' +  CONVERT(NVARCHAR(4000), @pSvrNo) 
	SET @aSQL = @aSQL +	N',CONVERT(CHAR, GETDATE(), 112) '
	SET @aSQL = @aSQL +	N',MID'
	SET @aSQL = @aSQL +	N',mHuntingCnt'
	SET @aSQL = @aSQL +	N' FROM ' + CONVERT(NVARCHAR(4000), @pSvrPath)  + '.dbo.TblStatisticsMonsterHunting'
	
	EXEC sp_executesql @aSQL, @aParm, @aSvrNo = @pSvrNo, @aSvrPath = @pSvrPath										 
	
	
	-- dbo.TblStatisticsPShopExchange
	SET @aParm = '@aIsObjCnt INT OUTPUT, @aObjNm NVARCHAR(300)';
	SET @aObjNm = 'TblStatisticsPShopExchange'

	SET @aSQL =			N'SELECT @aIsObjCnt = COUNT(*) ' 
	SET @aSQL = @aSQL +	N'FROM ' + CONVERT(NVARCHAR(4000), @pSvrPath)  + '.dbo.sysobjects '
	SET @aSQL = @aSQL +	N'WHERE name = @aObjNm'
	EXEC sp_executesql @aSQL, @aParm, @aObjNm = @aObjNm, @aIsObjCnt=@aIsObjCnt OUTPUT;	
	
	IF @aIsObjCnt > 0
	BEGIN
		SET @aParm = '@aSvrNo SMALLINT, @aSvrPath NVARCHAR(4000)'
			
		SET @aSQL =			N' INSERT INTO dbo.TblStatisticsPShopExchange( mSvrNo, mRegDate, mItemNo, mBuyCount, mSellCount, mBuyTotalPrice, mSellTotalPrice, mBuyMinPrice, mSellMinPrice, mBuyMaxPrice, mSellMaxPrice ) '
		SET @aSQL = @aSQL +	N' SELECT ' +  CONVERT(NVARCHAR(4000), @pSvrNo) 
		SET @aSQL = @aSQL +	N' ,CONVERT(CHAR, GETDATE(), 112) '
		SET @aSQL = @aSQL +	N' ,mItemNo, SUM(mBuyCount), SUM(mSellCount), SUM(mBuyTotalPrice), SUM(mSellTotalPrice), SUM(mBuyMinPrice), SUM(mSellMinPrice), SUM(mBuyMaxPrice), SUM(mSellMaxPrice)'
		SET @aSQL = @aSQL +	N' FROM ' + CONVERT(NVARCHAR(4000), @pSvrPath)  + '.dbo.TblStatisticsPShopExchange'
		SET @aSQL = @aSQL + N' GROUP BY mItemNo'
		
		EXEC sp_executesql @aSQL, @aParm, @aSvrNo = @pSvrNo, @aSvrPath = @pSvrPath			
	END 
	
	-- all Fnlgame db statistics table clear
	SET @aSQL = N' EXEC ' + CONVERT(NVARCHAR(4000), @pSvrPath)  + '.dbo.UspResetStatisticsTable'	
	EXEC sp_executesql @aSQL
	

	-- 2009.10.27 眠啊
	-- item tot statistics
	IF NOT EXISTS ( SELECT * 
					FROM dbo.TblStatisticsItemByCase
					WHERE mRegDate = CONVERT(INT,CONVERT(VARCHAR, GETDATE(), 112))
						AND mSvrNo = @pSvrNo )
	BEGIN

		INSERT INTO dbo.TblStatisticsItemByCase(mSvrNo,mRegDate,mItemNo,mMerchantCreate,mMerchantDelete,
			mReinforceCreate,mReinforceDelete,mCraftingCreate,mCraftingDelete,mPcUseDelete,mNpcUseDelete,
			mNpcCreate,mMonsterDrop,mGSExchangeCreate,mGSExchangeDelete)
		SELECT  
			mSvrNo, mRegDate, mItemNo, SUM(mMerchantCreate), SUM(mMerchantDelete),
			SUM(mReinforceCreate), SUM(mReinforceDelete), SUM(mCraftingCreate), 
			SUM(mCraftingDelete), SUM(mPcUseDelete), SUM(mNpcUseDelete), 
			SUM(mNpcCreate), SUM(mMonsterDrop), SUM(mGSExchangeCreate), SUM(mGSExchangeDelete)
		FROM dbo.TblStatisticsItemByLevelClass
		WHERE mRegDate = CONVERT(INT,CONVERT(VARCHAR, GETDATE(), 112))
				AND mSvrNo = @pSvrNo
		GROUP BY mSvrNo, mRegDate, mItemNo	

	END

GO

