






/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2011-9-26 13:33:17 ******/


/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2011-3-4 17:01:02 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2010-12-16 17:11:38 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2010-3-22 10:25:03 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2009-12-11 9:19:50 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2009-11-10 16:47:53 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2009-9-11 9:27:45 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogDeath    Script Date: 2009-7-13 13:37:40 ******/
CREATE PROCEDURE [dbo].[UspInsertLogDeath]
     @pSvrNo			SMALLINT
    ,@pUserId			VARCHAR(20)
    ,@pCharNo			INT	
    ,@pCharNm			CHAR(20)
    ,@pToCharNo			INT	
    ,@pToCharNm			CHAR(20)
    ,@pGuildNo			BIGINT
    ,@pType             TINYINT
    ,@pPlace			INT
    ,@pPosX				REAL
	,@pPosY				REAL
	,@pPosZ				REAL
    ,@pIsMacro          BIT
    ,@pDrop1			INT
    ,@pDropNm1			CHAR(40)
    ,@pDrop2			INT
    ,@pDropNm2			CHAR(40)
    ,@pDrop3			INT
    ,@pDropNm3			CHAR(40)
    ,@pDrop4			INT
    ,@pDropNm4			CHAR(40)
    ,@pDrop5			INT
    ,@pDropNm5			CHAR(40)
    ,@pDrop6			INT
    ,@pDropNm6			CHAR(40)
    ,@pDrop7			INT
    ,@pDropNm7			CHAR(40)

AS
	SET NOCOUNT ON		
	
	DECLARE	@aErrNo	INT
	
	INSERT INTO TblLogDeath(mSvrNo, mUserId, mCharNo, mCharNm, mToCharNo, mToCharNm, mGuildNo, mType, mPlace, mPosX, mPosY, mPosZ, mIsMacro, mDrop1, mDropNm1, mDrop2, mDropNm2, mDrop3, mDropNm3, mDrop4, mDropNm4, mDrop5, mDropNm5, mDrop6, mDropNm6, mDrop7, mDropNm7)
	       VALUES(@pSvrNo, @pUserId, @pCharNo, @pCharNm, @pToCharNo, @pToCharNm, @pGuildNo, @pType, @pPlace, @pPosX, @pPosY, @pPosZ, @pIsMacro, @pDrop1, @pDropNm1, @pDrop2, @pDropNm2, @pDrop3, @pDropNm3, @pDrop4, @pDropNm4, @pDrop5, @pDropNm5, @pDrop6, @pDropNm6, @pDrop7, @pDropNm7)

	SET @aErrNo = @@ERROR						
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

