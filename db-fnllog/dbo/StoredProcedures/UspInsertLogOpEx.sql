






/****** Object:  Stored Procedure dbo.UspInsertLogOpEx    Script Date: 2011-9-26 13:33:16 ******/


/****** Object:  Stored Procedure dbo.UspInsertLogOpEx    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOpEx    Script Date: 2011-3-4 17:01:02 ******/
/******************************************************************************
**		Name: UspInsertLogOpEx
**		Desc: 霸烙肺弊 & 墨坷胶 海撇 肺弊 扁废
**
**		Auth:
**		Date: 
*******************************************************************************
**		Change History
*******************************************************************************
**		Date		Author				Description
**		--------	--------			---------------------------------------
**		2007.08.06	JUDY				酒捞叼 Size 炼沥
**		2008.04.25	JUDY				版氰摹 BIGINT 函版
**		2009-03-30	辫 辈挤				INSERT 亲格 眠啊
**		20090608	JUDY				肺弊 亲格 眠啊
**		2009.10.16	JUDY				角滚樊 刘啊 眠捞 扁瓷 力芭
*******************************************************************************/
CREATE PROCEDURE [dbo].[UspInsertLogOpEx]
	 @pSvrNo			SMALLINT
	,@pCategory			INT
	,@pLogType			INT	
	,@pUserNo			INT
	,@pUserId			VARCHAR(20)
	,@pIp				CHAR(15)
	,@pCharNo			INT		
	,@pCharNm			CHAR(20)
	,@pMapNo			INT	
	,@pPosX				REAL
	,@pPosY				REAL
	,@pPosZ				REAL
	,@pLevel			SMALLINT
	,@pExp				BIGINT	-- 版氰摹 2008.04.25
	,@pChaotic			INT
	,@pCharNoOpp		INT		
	,@pCharNmOpp		CHAR(20)
	,@pItemId			INT
	,@pItemNm			CHAR(40)
	,@pItemIsConfirm	BIT
	,@pItemStatus		TINYINT
	,@pItemCnt			BIGINT
	,@pItemCntTot		BIGINT
	,@pItemSerial		BIGINT
	,@pItemSerialOpp	BIGINT
	,@pInt1				INT
	,@pInt2				INT
	,@pInt3				INT
	,@pChar1			CHAR(40)
	,@pChar2			CHAR(40)
	,@pBigInt1			BIGINT
	,@pBigInt2			BIGINT
	,@pVolitionOfHonor	SMALLINT
	,@pHonorPoint		INT
	,@pChaosPoint		BIGINT
AS
	SET NOCOUNT ON		
	
	DECLARE	@aErrNo	INT
	
	INSERT INTO dbo.TblLogOp([mSvrNo], [mCategory], [mLogType], [mUserNo], [mUserId], [mIp], 
						 [mCharNo], [mCharNm], [mMapNo], [mPosX], [mPosY], [mPosZ], [mLevel], [mExp], [mChaotic],
						 [mCharNoOpp], [mCharNmOpp], [mItemId], [mItemNm], [mItemIsConfirm], [mItemStatus], 
						 [mItemCnt], [mItemCntTot], [mItemSerial], [mItemSerialOpp], [mInt1], [mInt2], [mInt3], 
						 [mChar1], [mChar2], [mBigInt1], [mBigInt2], [mVolitionOfHonor], [mHonorPoint], [mChaosPoint])
				  VALUES(@pSvrNo, @pCategory, @pLogType, @pUserNo, @pUserId, @pIp,
						 @pCharNo, @pCharNm, @pMapNo, @pPosX, @pPosY, @pPosZ, @pLevel, @pExp, @pChaotic,
						 @pCharNoOpp, @pCharNmOpp, @pItemId, @pItemNm, @pItemIsConfirm, @pItemStatus, 
						 @pItemCnt, @pItemCntTot, @pItemSerial, @pItemSerialOpp, @pInt1, @pInt2, @pInt3, 
						 @pChar1, @pChar2, @pBigInt1, @pBigInt2, @pVolitionOfHonor, @pHonorPoint, @pChaosPoint)

	SET @aErrNo = @@ERROR						
	
	/*
	IF @pLogType IN ( 5012, 5013)
		AND @pItemId = 409
	BEGIN		
		EXEC dbo.UspInsertLogPc @pLogType, @pCharNo, @pCharNm, @pItemCnt, @pItemCntTot
	END
	*/
	
	RETURN(@aErrNo)

GO

