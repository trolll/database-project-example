






/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2011-9-26 13:33:16 ******/


/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2011-3-4 17:01:02 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2010-12-16 17:11:38 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2010-3-22 10:25:03 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2009-12-11 9:19:50 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2009-11-10 16:47:53 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2009-9-11 9:27:45 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_Log_DROP_TABLE    Script Date: 2009-7-13 13:37:40 ******/
CREATE PROCEDURE dbo.AP_CYCLE_Log_DROP_TABLE    
AS    
 SET NOCOUNT ON     
     
 -- ???     
 -- 20070103    
 -- ???? ??     
    
 DECLARE @DROP_SQL VARCHAR(8000)    
 SET @DROP_SQL =''    
    
 IF OBJECT_ID('TEMPDB..#DEL_TARGET') IS NOT NULL    
 DROP TABLE #DEL_TARGET    
    
 CREATE TABLE #DEL_TARGET    
 (IDX INT IDENTITY(1,1)    
 , TABLENAME VARCHAR(200)    
 , TABLEDATE SMALLDATETIME    
 , TABLEOBJECT VARCHAR(200)    
 )    
    
 INSERT INTO #DEL_TARGET(TABLENAME, TABLEDATE)    
 SELECT     
  NAME, RIGHT(NAME,8)    
 FROM     
  SYSOBJECTS     
 WHERE     
  TYPE ='U'      
  AND ISDATE(RIGHT(NAME,8))=1    
  AND (PATINDEX('%SYS%',NAME) > 0 OR PATINDEX('%OP%',NAME) > 0 OR PATINDEX('%DEATH%',NAME) > 0 OR PATINDEX('%CHAT%',NAME) > 0)    
    
 SELECT @DROP_SQL = @DROP_SQL+'DROP TABLE '+TABLENAME+' '+CHAR(10) FROM #DEL_TARGET WHERE TABLEDATE < GETDATE() - 30    
 -- SELECT LEN (@DROP_SQL)    
 -- PRINT @DROP_SQL    
 EXEC (@DROP_SQL)    
 SET @DROP_SQL = ''    
     
 -- ?? ?? ?? ???    
 TRUNCATE TABLE #DEL_TARGET

GO

