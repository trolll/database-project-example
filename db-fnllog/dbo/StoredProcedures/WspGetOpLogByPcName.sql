



CREATE  PROCEDURE [dbo].[WspGetOpLogByPcName]
	@mCharNm VARCHAR(20), -- 旌愲Ν韯半獏
	@mLogType	VARCHAR(100),			-- 搿滉犯 韮€鞛?
	@mInt3	INT,			-- 靷湢 1,
	@mInt4	INT,			-- 靷湢 2,
	@mInt5	INT,			-- 靷湢 3,

	@mStartYear SMALLINT,				-- 瓴€靸?鞁滌瀾雲?
	@mStartMonth SMALLINT,				-- 瓴€靸?鞁滌瀾鞗?
	@mStartDay SMALLINT,				-- 瓴€靸?鞁滌瀾鞚?
	@mStartHour  SMALLINT,				-- 瓴€靸?鞁滉皠
	@mStartMin SMALLINT,				-- 瓴€靸?攵?

	@mEndYear SMALLINT,					-- 瓴€靸?鞁滌瀾雲?
	@mEndMonth SMALLINT,				-- 瓴€靸?鞁滌瀾鞗?
	@mEndDay SMALLINT,					-- 瓴€靸?鞁滌瀾鞚?
	@mEndHour SMALLINT,					-- 毵堨毵?鞁?
	@mEndMin SMALLINT, 					-- 毵堨毵?攵?
	@mItemID	VARCHAR(50)	= ''				-- 臁绊殞 鞎勳澊韰?ID
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @mStartDate	DATETIME
	DECLARE @mEndDate	DATETIME
			, @today	DATETIME
			, @LogTableCnt	INT
			, @LogSeq	INT
	DECLARE @mLogTableNm		TABLE (
		mSeq	INT	 IDENTITY(1,1),
		mTableNm	NVARCHAR(50) )
	
	DECLARE @mTableNm	NVARCHAR(50)
	DECLARE @SQLField	NVARCHAR(1000)
	DECLARE @SQLWhere	NVARCHAR(1000)
	DECLARE @SQLOrder	NVARCHAR(1000)
	DECLARE @SQL		NVARCHAR(4000)
	DECLARE @SQLExec	NVARCHAR(4000)
			
		
	
	SET @mStartDate = CAST(
							STR(@mStartYear) + '-' + 
							STR(@mStartMonth) + '-' + 
							STR(@mStartDay) + ' ' + 
							STR(@mStartHour) + ':' + 
							STR(@mStartMin) + ':00'  AS DATETIME)
	SET @mEndDate  = CAST(
							STR(@mEndYear) + '-' + 
							STR(@mEndMonth) + '-' + 
							STR(@mEndDay) + ' ' + 
							STR(@mEndHour) + ':' + 
							STR(@mEndMin) + ':00'  AS DATETIME)
							
	SELECT @today = GETDATE(), @LogTableCnt = 0, @LogSeq = 1
	
	--------------------------------------------------------------------------------------
	-- 瓴€靸夑赴臧?鞝滍暅 靹れ爼
	--------------------------------------------------------------------------------------
	IF @mEndDate > GETDATE()
	BEGIN
		SET @mEndDate  = GETDATE()	-- 順勳灛 鞁滉皠鞙茧 鞛“鞝?
	END 

	IF DATEDIFF(DD, @mStartDate, @mEndDate ) > 3 OR DATEDIFF(DD, @mStartDate, @mEndDate ) < 0
	BEGIN		
		RETURN(1)	
	END  
		
	--------------------------------------------------------------------------------------
	-- 瓴€靸?頃勲摐 靹れ爼
	--------------------------------------------------------------------------------------
	SET @SQLField = ' TOP 1500 mRegDate,mIp,mCharNm,mLevel,mExp'
	SET @SQLField = @SQLField + ',mLogType,mCategory' 
	SET @SQLField = @SQLField + ',mItemNm,mItemStatus '
	SET @SQLField = @SQLField + ',mItemSerial,mItemSerialOpp,mItemCnt,mItemCntTot,mCharNmOpp,mInt1 '
	SET @SQLField = @SQLField + ',mInt2,mInt3,mChar1,mChar2,mChaotic,mUserNo,mCharNoOpp '
	SET @SQLField = @SQLField + ',mUserId,mPosX,mPosY,mPosZ,mBigInt1,mBigInt2,mVolitionOfHonor,mHonorPoint,mChaosPoint'

		
	--------------------------------------------------------------------------------------
	-- 搿滉犯韰岇澊敫?靹犾爼
	--------------------------------------------------------------------------------------	
	INSERT INTO @mLogTableNm(mTableNm)
	SELECT mLogTableNm
	FROM (
		SELECT 		
			mLogTableNm =
				CASE 
					WHEN @today BETWEEN mSDYmd AND mEDYmd  THEN 'TblLogOp'				
					ELSE
						'TblLogOp_' + CONVERT(CHAR(10), CONVERT(INT,CONVERT(CHAR(8), mSDYmd, 112)) )
				END 
		FROM DBO.TblCopyYmd
		WHERE mSDYmd >= 
					CASE 
						WHEN DATEPART(HH, @mStartDate) < 5 THEN DATEADD(DD,-1,@mStartDate)
							ELSE CONVERT(DATETIME,CONVERT(CHAR(8), @mStartDate, 112) + ' 05:00:00')
					END
					AND mSDYmd < @mEndDate			
					AND mSDYmd < @today	) T1
	WHERE mLogTableNm IN (
			SELECT name
			FROM sysobjects
			WHERE xtype='U'
	)
	
	SELECT @LogTableCnt = @@ROWCOUNT

	IF @LogTableCnt <= 0 
	BEGIN
		RETURN(1)	-- 臁挫灛頃橃 鞎婋姅雼? 瓴€靸夗暊 搿滉犯 韰岇澊敫旍澊 臁挫灛頃橃 鞎婋姅雼?		
	END 

	--------------------------------------------------------------------------------------
	-- 瓴€靸?臁瓣贝 靹れ爼
	--------------------------------------------------------------------------------------		
	SET @SQLWhere = ''		
	BEGIN
		SET @SQLWhere = 'WHERE mCharNm = @mCharNm '
		SET @SQLWhere =  @SQLWhere +  ' AND mRegDate BETWEEN @mStartDate AND  @mEndDate '
		
		IF LEN(RTRIM(@mLogType)) > 0 
			SET @SQLWhere =  @SQLWhere + ' AND mLogType IN ( ' +  CONVERT(NVARCHAR, @mLogType)  + ') '
		ELSE
			SET @SQLWhere =  @SQLWhere + ' AND ( mLogType <= 8000 OR mLogType > 9000 ) ' 
					
		IF @mInt3	IS NOT NULL AND LEN(@mInt3) > 0
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND mInt3 IN ( ' +   CONVERT(NVARCHAR, @mInt3)
			
			IF @mInt4 IS NOT NULL AND @mInt4 > 0
			BEGIN
				SET @SQLWhere = @SQLWhere + ' , ' +   CONVERT(NVARCHAR, @mInt4)
			END
			
			IF @mInt5 IS NOT NULL AND @mInt5 > 0
			BEGIN
				SET @SQLWhere = @SQLWhere + ', ' +   CONVERT(NVARCHAR, @mInt5)
			END

			SET @SQLWhere = @SQLWhere + ' ) '
		END 	

		-- 2010.01.12	JUDY, TID 鞎勳澊韰?鞝曤炒 臁瓣贝 頃 於旉皜
		IF LEN(@mItemID) > 0 AND @mItemID IS NOT NULL 
		BEGIN
			SET @SQLWhere =  @SQLWhere +  ' AND mItemId IN ( ' + CONVERT(VARCHAR(1000), @mItemID ) + ')'
		END 
	END

	--------------------------------------------------------------------------------------
	-- 炜茧Μ 靸濎劚 ( MAX : 1500 Row Count )
	--------------------------------------------------------------------------------------					
	SET @SQL = ''
	WHILE ( @LogSeq <= @LogTableCnt )
	BEGIN
	
		SELECT
			@mTableNm = mTableNm	
		FROM @mLogTableNm
		WHERE mSeq = @LogSeq
		
		IF @@ROWCOUNT <= 0 
		BEGIN
			BREAK
		END
		
		IF @LogSeq > 1
		BEGIN
			SET @SQL = @SQL + ' UNION ALL '	 + CHAR(10)		
		END
	
		-- 2009.05.08 By Judy..
		SET @SQL = @SQL + ' SELECT ' +  CHAR(10)	
						
		IF NOT EXISTS ( select *
					from sysobjects t1
						inner join syscolumns t2
							on t1.id = t2.id
					where t1.name = @mTableNm
						and t2.name = 'mVolitionOfHonor' )
		BEGIN
			SET @SQL = @SQL + SUBSTRING(@SQLField, 0, LEN(@SQLField) - 40 ) + ' , 0 mVolitionOfHonor, 0 mHonorPoint, 0 mChaosPoint '	+ CHAR(10)	
		END
		ELSE
		BEGIN
			SET @SQL = @SQL + @SQLField	+ CHAR(10)	
		END 		
		
		SET @SQL = @SQL + ' FROM dbo.' + @mTableNm  + ' '  + CHAR(10)	
		SET @SQL = @SQL + ' ' + @SQLWhere  + CHAR(10)
		
		SET @LogSeq = @LogSeq + 1
	END

	SET ROWCOUNT 1500	-- 斓滊寑 臧€鞝胳槵 靾?鞛堧姅 搿滉犯 鞝滍暅 
	
	SET @SQLExec = ' '
	SET @SQLExec = ' SELECT ' + RTRIM( @SQLField  ) + CHAR(10)
	SET @SQLExec = @SQLExec + ' FROM ( '
	SET @SQLExec = @SQLExec + RTRIM( @SQL )
	SET @SQLExec = @SQLExec + ' ) T1 ORDER BY mRegDate ASC'  + CHAR(10)	
	SET @SQLExec = @SQLExec + ' OPTION(MAXDOP  1) ' + CHAR(10)	
	PRINT @SQLExec

	-- 炜茧Μ 瓴€靸?鞁ろ枆
	EXEC sp_executesql  @SQLExec, N' @mCharNm VARCHAR(20), @mStartDate DATETIME, @mEndDate DATETIME ', @mCharNm = @mCharNm, @mStartDate = @mStartDate, 
@mEndDate = @mEndDate

GO

