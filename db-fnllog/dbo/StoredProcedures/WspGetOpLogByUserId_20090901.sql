






/****** Object:  Stored Procedure dbo.WspGetOpLogByUserId_20090901    Script Date: 2011-9-26 13:33:17 ******/


/****** Object:  Stored Procedure dbo.WspGetOpLogByUserId_20090901    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.WspGetOpLogByUserId_20090901    Script Date: 2011-3-4 17:01:02 ******/

/****** Object:  Stored Procedure dbo.WspGetOpLogByUserId_20090901    Script Date: 2010-12-16 17:11:38 ******/

/****** Object:  Stored Procedure dbo.WspGetOpLogByUserId_20090901    Script Date: 2010-3-22 10:25:03 ******/

/****** Object:  Stored Procedure dbo.WspGetOpLogByUserId_20090901    Script Date: 2009-12-11 9:19:50 ******/

/****** Object:  Stored Procedure dbo.WspGetOpLogByUserId_20090901    Script Date: 2009-11-10 16:47:53 ******/

/****** Object:  Stored Procedure dbo.WspGetOpLogByUserId_20090901    Script Date: 2009-9-11 9:27:45 ******/
CREATE PROCEDURE [dbo].[WspGetOpLogByUserId_20090901]  
 @mUserId VARCHAR(20), -- ???  
 @mLogType VARCHAR(100),   -- ?? ??  
 @mInt3 INT,   -- ?? 1,  
 @mInt4 INT,   -- ?? 2,  
 @mInt5 INT,   -- ?? 3,  
  
 @mStartYear SMALLINT,    -- ?? ???  
 @mStartMonth SMALLINT,    -- ?? ???  
 @mStartDay SMALLINT,    -- ?? ???  
 @mStartHour  SMALLINT,    -- ?? ??  
 @mStartMin SMALLINT,    -- ?? ?  
  
 @mEndYear SMALLINT,     -- ?? ???  
 @mEndMonth SMALLINT,    -- ?? ???  
 @mEndDay SMALLINT,     -- ?? ???  
 @mEndHour SMALLINT,     -- ??? ?  
 @mEndMin SMALLINT     -- ??? ?  
AS  
 SET NOCOUNT ON  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 DECLARE @mStartDate DATETIME  
 DECLARE @mEndDate DATETIME  
   , @today DATETIME  
   , @LogTableCnt INT  
   , @LogSeq INT  
 DECLARE @mLogTableNm  TABLE (  
  mSeq INT  IDENTITY(1,1),  
  mTableNm NVARCHAR(50) )  
   
 DECLARE @mTableNm NVARCHAR(50)  
 DECLARE @SQLField NVARCHAR(1000)  
 DECLARE @SQLWhere NVARCHAR(1000)  
 DECLARE @SQLOrder NVARCHAR(1000)  
 DECLARE @SQL  NVARCHAR(4000)  
 DECLARE @SQLExec NVARCHAR(4000)  
     
    
   
 SET @mStartDate = CAST(  
       STR(@mStartYear) + '-' +   
       STR(@mStartMonth) + '-' +   
       STR(@mStartDay) + ' ' +   
       STR(@mStartHour) + ':' +   
       STR(@mStartMin) + ':00'  AS DATETIME)  
 SET @mEndDate  = CAST(  
       STR(@mEndYear) + '-' +   
       STR(@mEndMonth) + '-' +   
       STR(@mEndDay) + ' ' +   
       STR(@mEndHour) + ':' +   
       STR(@mEndMin) + ':00'  AS DATETIME)  
         
 SELECT @today = GETDATE(), @LogTableCnt = 0, @LogSeq = 1  
   
 --------------------------------------------------------------------------------------  
 -- ???? ?? ??  
 --------------------------------------------------------------------------------------  
 IF @mEndDate > GETDATE()  
 BEGIN  
  SET @mEndDate  = GETDATE() -- ?? ???? ???   
 END   
  
 IF DATEDIFF(DD, @mStartDate, @mEndDate ) > 3 OR DATEDIFF(DD, @mStartDate, @mEndDate ) < 0  
 BEGIN    
  RETURN(1)   
 END    
    
 --------------------------------------------------------------------------------------  
 -- ?? ?? ??  
 --------------------------------------------------------------------------------------  
 SET @SQLField = ' TOP 1500 mRegDate,mIp,mCharNm,mLevel,mExp'  
 SET @SQLField = @SQLField + ',mLogType,mCategory'   
 SET @SQLField = @SQLField + ',mItemNm,mItemStatus '  
 SET @SQLField = @SQLField + ',mItemSerial,mItemSerialOpp,mItemCnt,mItemCntTot,mCharNmOpp,mInt1 '  
 SET @SQLField = @SQLField + ',mInt2,mInt3,mChar1,mChar2,mChaotic,mUserNo,mCharNoOpp '  
 SET @SQLField = @SQLField + ',mUserId,mPosX,mPosY,mPosZ,mBigInt1,mBigInt2,mVolitionOfHonor,mHonorPoint,mChaosPoint'  
  
    
 --------------------------------------------------------------------------------------  
 -- ????? ??  
 --------------------------------------------------------------------------------------   
 INSERT INTO @mLogTableNm(mTableNm)  
 SELECT mLogTableNm  
 FROM (  
  SELECT     
   mLogTableNm =  
    CASE   
     WHEN @today BETWEEN mSDYmd AND mEDYmd  THEN 'TblLogOp'      
     ELSE  
      'TblLogOp_' + CONVERT(CHAR(10), CONVERT(INT,CONVERT(CHAR(8), mSDYmd, 112)) )  
    END   
  FROM DBO.TblCopyYmd  
  WHERE mSDYmd >=   
     CASE   
      WHEN DATEPART(HH, @mStartDate) < 5 THEN DATEADD(DD,-1,@mStartDate)  
       ELSE CONVERT(DATETIME,CONVERT(CHAR(8), @mStartDate, 112) + ' 05:00:00')  
     END  
     AND mSDYmd < @mEndDate     
     AND mSDYmd < @today ) T1  
 WHERE mLogTableNm IN (  
   SELECT name  
   FROM sysobjects  
   WHERE xtype='U'  
 )  
   
 SELECT @LogTableCnt = @@ROWCOUNT  
  
 IF @LogTableCnt <= 0   
 BEGIN  
  RETURN(1) -- ???? ???. ??? ?? ???? ???? ???.    
 END   
  
 --------------------------------------------------------------------------------------  
 -- ?? ?? ??  
 --------------------------------------------------------------------------------------    
 SET @SQLWhere = ''    
 BEGIN  
  SET @SQLWhere = 'WHERE mUserId = @mUserId '  
  SET @SQLWhere =  @SQLWhere +  ' AND mRegDate BETWEEN @mStartDate AND  @mEndDate '  
      
  IF LEN(RTRIM(@mLogType)) > 0   
   SET @SQLWhere =  @SQLWhere + ' AND mLogType IN ( ' +  CONVERT(NVARCHAR, @mLogType)  + ') '  
  ELSE  
   SET @SQLWhere =  @SQLWhere + ' AND ( mLogType <= 8000 OR mLogType > 9000 ) '   
     
  IF @mInt3 IS NOT NULL AND LEN(@mInt3) > 0  
  BEGIN  
   SET @SQLWhere = @SQLWhere + ' AND mInt3 IN ( ' +   CONVERT(NVARCHAR, @mInt3)  
     
   IF @mInt4 IS NOT NULL AND @mInt4 > 0  
   BEGIN  
    SET @SQLWhere = @SQLWhere + ' , ' +   CONVERT(NVARCHAR, @mInt4)  
   END  
     
   IF @mInt5 IS NOT NULL AND @mInt5 > 0  
   BEGIN  
    SET @SQLWhere = @SQLWhere + ', ' +   CONVERT(NVARCHAR, @mInt5)  
   END  
  
   SET @SQLWhere = @SQLWhere + ' ) '  
  END       
 END  
  
 --------------------------------------------------------------------------------------  
 -- ?? ?? ( MAX : 1500 Row Count )  
 --------------------------------------------------------------------------------------       
 SET @SQL = ''  
 WHILE ( @LogSeq <= @LogTableCnt )  
 BEGIN  
   
  SELECT  
   @mTableNm = mTableNm   
  FROM @mLogTableNm  
  WHERE mSeq = @LogSeq  
    
  IF @@ROWCOUNT <= 0   
  BEGIN  
   BREAK  
  END  
    
  IF @LogSeq > 1  
  BEGIN  
   SET @SQL = @SQL + ' UNION ALL '  + CHAR(10)    
  END  
    
  -- 2009.05.08 By Judy..  
  SET @SQL = @SQL + ' SELECT ' +  CHAR(10)   
        
  IF NOT EXISTS ( select *  
     from sysobjects t1  
      inner join syscolumns t2  
       on t1.id = t2.id  
     where t1.name = @mTableNm  
      and t2.name = 'mVolitionOfHonor' )  
  BEGIN  
   SET @SQL = @SQL + SUBSTRING(@SQLField, 0, LEN(@SQLField) - 40 ) + ' , 0 mVolitionOfHonor, 0 mHonorPoint, 0 mChaosPoint ' + CHAR(10)   
  END  
  ELSE  
  BEGIN  
   SET @SQL = @SQL + @SQLField + CHAR(10)   
  END     
    
  SET @SQL = @SQL + ' FROM dbo.' + @mTableNm  + ' '  + CHAR(10)   
  SET @SQL = @SQL + ' ' + @SQLWhere  + CHAR(10)  
    
    
  SET @LogSeq = @LogSeq + 1  
 END  
  
 SET ROWCOUNT 1500 -- ?? ??? ? ?? ?? ??   
   
 SET @SQLExec = ' '  
 SET @SQLExec = ' SELECT ' + RTRIM( @SQLField  ) + CHAR(10)  
 SET @SQLExec = @SQLExec + ' FROM ( '  
 SET @SQLExec = @SQLExec + RTRIM( @SQL )  
 SET @SQLExec = @SQLExec + ' ) T1 ORDER BY mRegDate ASC'  + CHAR(10)   
 SET @SQLExec = @SQLExec + ' OPTION(MAXDOP  1) ' + CHAR(10)   
 --PRINT @SQLExec  
  
 -- ?? ?? ??  
 EXEC sp_executesql  @SQLExec, N' @mUserId VARCHAR(12), @mStartDate DATETIME, @mEndDate DATETIME ', @mUserId = @mUserId, @mStartDate = @mStartDate, @mEndDate = @mEndDate

GO

