






/****** Object:  Stored Procedure dbo.UspInsertLogToUseMacro    Script Date: 2011-9-26 13:33:16 ******/


/****** Object:  Stored Procedure dbo.UspInsertLogToUseMacro    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogToUseMacro    Script Date: 2011-3-4 17:01:02 ******/
CREATE PROCEDURE [dbo].[UspInsertLogToUseMacro]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @aSQL NVARCHAR(2000)
			, @aTableNm	NVARCHAR(20)	
	
	SET @aTableNm = N'TblLogOp_' + CONVERT(NVARCHAR(10), DATEADD(DD, -1, GETDATE()), 112)
	
	SET @aSQL = N''	
	SET @aSQL = @aSQL + N'INSERT INTO dbo.TblLogUseMacro ' + CHAR(10)
	SET @aSQL = @aSQL + N'SELECT  ' + CHAR(10)
	SET @aSQL = @aSQL + N'	T2.mRegDate ' + CHAR(10)
	SET @aSQL = @aSQL + N'	,T2.mUserNo ' + CHAR(10)
	SET @aSQL = @aSQL + N'	,T2.mUserId ' + CHAR(10)
	SET @aSQL = @aSQL + N'	,T2.mCharNo ' + CHAR(10)
	SET @aSQL = @aSQL + N'	,T2.mCharNm ' + CHAR(10)
	SET @aSQL = @aSQL + N'	,T2.mInt1 AS mCount ' + CHAR(10)
	SET @aSQL = @aSQL + N'	,T2.mCharNoOpp AS mGMCharNo ' + CHAR(10)
	SET @aSQL = @aSQL + N'	,T2.mCharNmOpp AS mGmCharNm ' + CHAR(10)
	SET @aSQL = @aSQL + N'FROM ( ' + CHAR(10)
	SET @aSQL = @aSQL + N'	SELECT mCharNo, MAX(mRegDate) AS mRegDate ' + CHAR(10)
	SET @aSQL = @aSQL + N'	FROM  '+ @aTableNm  + CHAR(10)
	SET @aSQL = @aSQL + N'	WHERE mLogType IN(5112,5143) ' + CHAR(10)
	SET @aSQL = @aSQL + N'	GROUP BY mCharNo  ' + CHAR(10)
	SET @aSQL = @aSQL + N') T1 INNER JOIN '+ @aTableNm + ' T2 ' + CHAR(10)
	SET @aSQL = @aSQL + N'	ON T1.mRegDate = T2.mRegDate ' + CHAR(10)
	SET @aSQL = @aSQL + N'		AND T1.mCharNo = T2.mCharNo ' + CHAR(10)
	SET @aSQL = @aSQL + N'		AND T2.mLogType IN(5112,5143) ' + CHAR(10)
	SET @aSQL = @aSQL + N' OPTION(MAXDOP  1)' + CHAR(10)

	EXEC sp_executesql  @aSQL, N' @aTableNm NVARCHAR(20) ', @aTableNm = @aTableNm

GO

