






/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2011-9-26 13:33:16 ******/


/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2011-3-4 17:01:02 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2010-12-16 17:11:38 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2010-3-22 10:25:03 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2009-12-11 9:19:50 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2009-11-10 16:47:53 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2009-9-11 9:27:45 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2009-7-13 13:37:40 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2009-6-1 15:20:07 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2009-5-12 8:41:50 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogOp    Script Date: 2009-2-25 13:56:05 ******/
CREATE PROCEDURE dbo.UspInsertLogOp
	 @pSvrNo			SMALLINT
	,@pCategory			INT
	,@pLogType			INT	
	,@pUserNo			INT
	,@pUserId			VARCHAR(20)
	,@pIp				CHAR(15)
	,@pCharNo			INT		
	,@pCharNm			CHAR(20)
	,@pMapNo			INT	
	,@pPosX				REAL
	,@pPosY				REAL
	,@pPosZ				REAL
	,@pLevel			SMALLINT
	,@pExp				BIGINT	-- °??è?? 2008.04.25
	,@pCharNoOpp		INT		
	,@pCharNmOpp		CHAR(20)
	,@pItemId			INT
	,@pItemNm			CHAR(40)
	,@pItemIsConfirm	BIT
	,@pItemStatus		TINYINT
	,@pItemCnt			BIGINT
	,@pItemCntTot		BIGINT
	,@pItemSerial		BIGINT
	,@pItemSerialOpp	BIGINT
	,@pInt1				INT
	,@pInt2				INT
	,@pInt3				INT
	,@pChar1			CHAR(40)
	,@pChar2			CHAR(40)
	,@pBigInt1			BIGINT
	,@pBigInt2			BIGINT
--WITH ENCRYPTION
AS
	SET NOCOUNT ON		-- Count-set°á°ú?| ?y?o??á? ?????ó.
	
	DECLARE	@aErrNo	INT
	
	INSERT INTO TblLogOp([mSvrNo], [mCategory], [mLogType], [mUserNo], [mUserId], [mIp], 
						 [mCharNo], [mCharNm], [mMapNo], [mPosX], [mPosY], [mPosZ], [mLevel], [mExp],
						 [mCharNoOpp], [mCharNmOpp], [mItemId], [mItemNm], [mItemIsConfirm], [mItemStatus], 
						 [mItemCnt], [mItemCntTot], [mItemSerial], [mItemSerialOpp], [mInt1], [mInt2], [mInt3], 
						 [mChar1], [mChar2], [mBigInt1], [mBigInt2])
				  VALUES(@pSvrNo, @pCategory, @pLogType, @pUserNo, @pUserId, @pIp,
						 @pCharNo, @pCharNm, @pMapNo, @pPosX, @pPosY, @pPosZ, @pLevel, @pExp,
						 @pCharNoOpp, @pCharNmOpp, @pItemId, @pItemNm, @pItemIsConfirm, @pItemStatus, 
						 @pItemCnt, @pItemCntTot, @pItemSerial, @pItemSerialOpp, @pInt1, @pInt2, @pInt3, 
						 @pChar1, @pChar2, @pBigInt1, @pBigInt2)

	SET @aErrNo = @@ERROR						
	SET NOCOUNT OFF
	RETURN(@aErrNo)

GO

