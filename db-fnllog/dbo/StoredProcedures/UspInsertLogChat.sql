






/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2011-9-26 13:33:17 ******/


/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2011-3-4 17:01:02 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2010-12-16 17:11:38 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2010-3-22 10:25:03 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2009-12-11 9:19:50 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2009-11-10 16:47:53 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2009-9-11 9:27:45 ******/

/****** Object:  Stored Procedure dbo.UspInsertLogChat    Script Date: 2009-7-13 13:37:40 ******/
CREATE PROCEDURE [dbo].[UspInsertLogChat]
	@pSvrNo			SMALLINT	
	,@pChatType		INT			
	,@pUserId		VARCHAR(20)	
	,@pIp			VARCHAR(15)	
	,@pCharNm		VARCHAR(12)	
	,@pChat			VARCHAR(100)
    ,@pToNm		VARCHAR(12)	-- ????? ??? ???
AS
	SET NOCOUNT ON		
	DECLARE	@aErrNo	INT
	
	INSERT INTO dbo.TblLogChat(mSvrNo, mChatType, 
		mCharNm, mChat, mUserId, mIp
        ,mToNm
        )
	VALUES(
		@pSvrNo
		, @pChatType
		, @pCharNm
		, @pChat
		, @pUserId
		, @pIp
        , @pToNm
        )

	SET @aErrNo = @@ERROR						
	
	RETURN(@aErrNo)

GO

