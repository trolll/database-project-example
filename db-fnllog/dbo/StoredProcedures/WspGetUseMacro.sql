






/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2011-9-26 13:33:17 ******/


/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2011-3-4 17:01:02 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2010-12-16 17:11:38 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2010-3-22 10:25:03 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2009-12-11 9:19:50 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2009-11-10 16:47:53 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2009-9-11 9:27:45 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2009-7-13 13:37:40 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2009-6-1 15:20:07 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2009-5-12 8:41:50 ******/

/****** Object:  Stored Procedure dbo.WspGetUseMacro    Script Date: 2009-2-25 13:56:06 ******/





CREATE PROCEDURE [WspGetUseMacro]
	@pStartDate	varchar(10)
	,@pEndDate varchar(10)
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	SELECT mRegDate,mUserNo,mUserId,mCharNo,mCharNm,mCount,mGMCharNo,mGmCharNm
	FROM dbo.TblLogUseMacro
	WHERE mRegDate >= @pStartDate AND  mRegDate <= dateadd(day,1,convert(datetime,@pEndDate)) order by mregdate

GO

