






/****** Object:  Stored Procedure dbo.AP_CYCLE_LOG    Script Date: 2011-9-26 13:33:17 ******/


/****** Object:  Stored Procedure dbo.AP_CYCLE_LOG    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_LOG    Script Date: 2011-3-4 17:01:02 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_LOG    Script Date: 2010-12-16 17:11:38 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_LOG    Script Date: 2010-3-22 10:25:03 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_LOG    Script Date: 2009-12-11 9:19:50 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_LOG    Script Date: 2009-11-10 16:47:53 ******/

/****** Object:  Stored Procedure dbo.AP_CYCLE_LOG    Script Date: 2009-9-11 9:27:45 ******/
CREATE PROCEDURE dbo.AP_CYCLE_LOG
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @sql VARCHAR(2000)

	IF OBJECT_ID('TblLogOp_new') IS NOT NULL
	BEGIN
		DROP TABLE dbo.TblLogOp_new
	END 

	

	
	--1. ??? ??? ??
	
	-- tbllogop
		CREATE TABLE [dbo].[TblLogOp_new] (
			[mRegDate] [datetime] NOT NULL ,
			[mSvrNo] [smallint] NOT NULL ,
			[mCategory] [int] NOT NULL ,
			[mLogType] [int] NOT NULL ,
			[mUserNo] [int] NULL ,
			[mUserId] [varchar] (20)  NULL ,
			[mIp] [varchar] (15)  NULL ,
			[mCharNo] [int] NULL ,
			[mCharNm] [varchar] (20)  NULL ,
			[mMapNo] [int] NULL ,
			[mPosX] [real] NULL ,
			[mPosY] [real] NULL ,
			[mPosZ] [real] NULL ,
			[mLevel] [smallint] NULL ,
			[mExp] [bigint] NULL ,
			[mChaotic] [int] NULL ,
			[mCharNoOpp] [int] NULL ,
			[mCharNmOpp] [varchar] (20)  NULL ,
			[mItemId] [int] NULL ,
			[mItemNm] [varchar] (40)  NULL ,
			[mItemIsConfirm] [bit] NULL ,
			[mItemStatus] [tinyint] NULL ,
			[mItemCnt] [bigint] NULL ,
			[mItemCntTot] [bigint] NULL ,
			[mItemSerial] [bigint] NULL ,
			[mItemSerialOpp] [bigint] NULL ,
			[mInt1] [int] NULL ,
			[mInt2] [int] NULL ,
			[mInt3] [int] NULL ,
			[mChar1] [varchar] (40)  NULL ,
			[mChar2] [varchar] (40)  NULL ,
			[mBigInt1] [bigint] NULL ,
			[mBigInt2] [bigint] NULL ,
			[mVolitionOfHonor] SMALLINT,
			[mHonorPoint]	INT,
			[mChaosPoint]	BIGINT 			
		) 

		 CREATE  INDEX [IxTblLogOpChar] ON [dbo].[TblLogOp_new]
		 (
			mCharNm, 		
			mLogType
		) WITH FILLFACTOR = 80

		 CREATE  INDEX [IxTblLogOpUser] ON [dbo].[TblLogOp_new]
		 (	
			mUserId, 
			mLogType
		) WITH FILLFACTOR = 80

		 CREATE  INDEX [IxTblLogOpIp] ON [dbo].[TblLogOp_new]
		 (
			mIp, 
			mLogType
		) WITH FILLFACTOR = 80

		CREATE  INDEX [IxTblLogOpItemSerial] ON [dbo].[TblLogOp_new]
		(
			mItemSerial, 
			mItemSerialOpp
		) WITH FILLFACTOR = 80
					
		CREATE  INDEX [IxTblLogOpLogType] ON [dbo].[TblLogOp_new]
		(
			mLogType
		) WITH FILLFACTOR = 80


		set @sql = 'ALTER TABLE [dbo].[TblLogOp_new] ADD 
						CONSTRAINT [DF_TblLogOp_'+ convert(char(8),getdate(),112)+'] DEFAULT (getdate()) FOR [mRegDate],
						 CHECK (0 <= [mCategory]),
						 CHECK (0 < [mLogType])'
		exec (@sql)
		
		
		-- tbllogsys
	



	--2. ??? ?? ??--?? ????	
		declare @old_table_name1 varchar(100)
	--	declare @old_table_name2 varchar(100)
	--	declare @old_table_name3 varchar(100)
	--	declare @old_table_name4 varchar(100)

		set @old_table_name1 = 'TblLogOp_'  + convert(char(8),dateadd(dd,-1,getdate()),112) 
	--	set @old_table_name2 = 'TblLogSys_' + convert(char(8),dateadd(dd,-1,getdate()),112) 
	--	set @old_table_name3 = 'TblLogDeath_' + convert(char(8),dateadd(dd,-1,getdate()),112) 
	--	set @old_table_name4 = 'TblLogChat_' + convert(char(8),dateadd(dd,-1,getdate()),112) 


	--3. ??? ?? ??
		exec sp_rename 'TblLogOp',@old_table_name1
		exec sp_rename 'TblLogOp_new','TblLogOp'

		--exec sp_rename 'TblLogSys',@old_table_name2
		--exec sp_rename 'TblLogSys_new','TblLogSys'
		
		--exec sp_rename 'TblLogDeath',@old_table_name3
		--exec sp_rename 'TblLogDeath_new','TblLogDeath'
		
		--exec sp_rename 'TblLogChat',@old_table_name4
		--exec sp_rename 'TblLogChat_new','TblLogChat'				


	-- *. ???? ??? ?? ???? ??(???)
		exec AP_CYCLE_Log_DROP_TABLE
		
	-- *. ??? ??? ?? ?? ??(By Judy) 
		EXEC dbo.UspInsertLogToUseMacro

GO

