






/****** Object:  Stored Procedure dbo.WspGetAdminLogByUserId    Script Date: 2011-9-26 13:33:16 ******/


/****** Object:  Stored Procedure dbo.WspGetAdminLogByUserId    Script Date: 2011-3-17 15:43:49 ******/

/****** Object:  Stored Procedure dbo.WspGetAdminLogByUserId    Script Date: 2011-3-4 17:01:02 ******/

/****** Object:  Stored Procedure dbo.WspGetAdminLogByUserId    Script Date: 2010-12-16 17:11:38 ******/

/****** Object:  Stored Procedure dbo.WspGetAdminLogByUserId    Script Date: 2010-3-22 10:25:03 ******/

/****** Object:  Stored Procedure dbo.WspGetAdminLogByUserId    Script Date: 2009-12-11 9:19:50 ******/

/****** Object:  Stored Procedure dbo.WspGetAdminLogByUserId    Script Date: 2009-11-10 16:47:53 ******/

/****** Object:  Stored Procedure dbo.WspGetAdminLogByUserId    Script Date: 2009-9-11 9:27:45 ******/
CREATE PROCEDURE [dbo].WspGetAdminLogByUserId
	@mSvrNo SMALLINT,		-- ??
	@mUserId VARCHAR(20),	-- ???

	@mLogType VARCHAR(100), -- ?? ??
	@mInt3	INT,			-- ??
	@mItemNm VARCHAR(40),	-- ????

	@mStartYear SMALLINT,		
	@mStartMonth SMALLINT,		
	@mStartDay SMALLINT,		
	
	@mOrder VARCHAR(4) = 'ASC' 
	
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @sql NVARCHAR(3000)
	DECLARE @mTable     NVARCHAR(50)
			

	DECLARE @mStartDate DATETIME
	DECLARE @mEndDate DATETIME

	SET @mStartDate = CAST(STR(@mStartYear) + '-' + STR(@mStartMonth) + '-' + STR(@mStartDay) + ' 00:00:00' AS DATETIME)

	-- char??? ????? space? ???? ? RTRIM
	SET @mUserId = RTRIM(@mUserId)
	SET @mItemNm = RTRIM(@mItemNm)

	IF DATEPART(Day, @mStartDate) = DATEPART(Day, GetDate())
	BEGIN
		SET @mTable = N'TblLogOp '
	END
	ELSE
	BEGIN
		SET @mTable = N'TblLogOp_' + CONVERT(NVARCHAR(20), @mStartDate, 112)
	END

	DECLARE @aAddColumn	NVARCHAR(100)
	IF EXISTS ( select *
				from sysobjects t1
					inner join syscolumns t2
						on t1.id = t2.id
				where t1.name = @mTable
					and t2.name = 'mVolitionOfHonor' )
	BEGIN
		SET @aAddColumn = ' ,  mVolitionOfHonor,mHonorPoint,mChaosPoint '		
	END 
	ELSE
	BEGIN
		SET @aAddColumn = ' , 0 mVolitionOfHonor, 0 mHonorPoint, 0 mChaosPoint '		
	END 					

	SET @sql =
		'SELECT
			mRegDate,mIp,mCharNm,mLevel,mExp
			,mLogType,mCategory
			,mItemNm,mItemStatus
			,mItemSerial,mItemSerialOpp,mItemCnt,mItemCntTot,mCharNmOpp,mInt1,mInt2,mInt3,mChar1,mChar2,mChaotic,mUserNo,mCharNoOpp
			,mUserId, mPosX, mPosY, mPosZ,mBigInt1,mBigInt2 '
	SET @sql = @sql + @aAddColumn		
	SET @sql = @sql + ' FROM '
	SET @sql = @sql + @mTable
	SET @sql = @sql + ' WITH (NOLOCK) '
	SET @sql = @sql + ' WHERE '
	SET @sql = @sql + '		mSvrNo = @mSvrNo '
		

	-- ??? ????
	IF @mUserId IS NOT NULL AND LEN(@mUserId) > 0
	BEGIN
		-- like ?? ??
		SET @sql = @sql + ' AND mUserId = @mUserId '
	END

	-- ???? ????
	IF @mLogType IS NOT NULL AND LEN(@mLogType) > 0
	BEGIN
		SET @sql = @sql + ' AND ' + @mLogType  
	END
	ELSE
	BEGIN	
		IF DATEPART(YY, @mStartDate) <= 2007 AND DATEPART(MM, @mStartDate) <= 5 AND DATEPART(DD, @mStartDate) < 17 
			SET @sql = @sql + ' AND ( mLogType BETWEEN 5027 AND 5047 ) ' 
		ELSE
			SET @sql = @sql + ' AND mLogType > 8000 '		
	END 

	-- ?? ?? ??
	IF @mInt3 IS NOT NULL AND @mInt3 > -1
	BEGIN
		SET @sql = @sql + ' AND mInt3 = @mInt3 '
	END

	-- ???? ????
	IF @mItemNm IS NOT NULL AND LEN(@mItemNm) > 0
	BEGIN
		SET @mItemNm = '%' + @mItemNm + '%'
		SET @sql = @sql + ' AND mItemNm LIKE @mItemNm '
	END

	-- ?? ??
	SET @sql = @sql + N' ORDER BY mRegDate ' + CONVERT(NVARCHAR, @mOrder)

	EXECUTE sp_executesql @sql, N'@mSvrNo SMALLINT, @mUserId VARCHAR(12), @mInt3 INT, @mItemNm VARCHAR(40)', @mSvrNo = @mSvrNo, @mUserId = @mUserId, @mInt3 = @mInt3, @mItemNm = @mItemNm

GO

