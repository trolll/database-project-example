CREATE TABLE [dbo].[TblLogUseMacro] (
    [mRegDate]  DATETIME     NOT NULL,
    [mUserNo]   INT          NOT NULL,
    [mUserId]   VARCHAR (20) NOT NULL,
    [mCharNo]   INT          NOT NULL,
    [mCharNm]   VARCHAR (12) NOT NULL,
    [mCount]    INT          NOT NULL,
    [mGMCharNo] INT          NULL,
    [mGmCharNm] VARCHAR (12) NULL,
    CONSTRAINT [CL_PK_TblLogUseMacro] PRIMARY KEY CLUSTERED ([mRegDate] ASC, [mUserNo] ASC, [mCharNo] ASC)
);


GO

CREATE NONCLUSTERED INDEX [NC_TblLogUseMacro_mUserId]
    ON [dbo].[TblLogUseMacro]([mUserId] ASC);


GO

