CREATE TABLE [dbo].[TblLogNmType] (
    [mTypeNo] INT        NOT NULL,
    [mNm]     CHAR (50)  NOT NULL,
    [mMemo]   CHAR (255) NOT NULL,
    CONSTRAINT [PKTblLogNmTypemTypeNo] PRIMARY KEY CLUSTERED ([mTypeNo] ASC) WITH (FILLFACTOR = 90)
);


GO

