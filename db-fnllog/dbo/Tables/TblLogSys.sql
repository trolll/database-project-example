CREATE TABLE [dbo].[TblLogSys] (
    [mRegDate] DATETIME      CONSTRAINT [DF_TblLogSys_20090705] DEFAULT (getdate()) NOT NULL,
    [mLogLv]   TINYINT       NOT NULL,
    [mSvrNo]   SMALLINT      NOT NULL,
    [mMsg]     VARCHAR (512) NOT NULL
);


GO

