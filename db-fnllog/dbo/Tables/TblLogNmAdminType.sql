CREATE TABLE [dbo].[TblLogNmAdminType] (
    [mTypeNo] INT        NOT NULL,
    [mNm]     CHAR (50)  NOT NULL,
    [mMemo]   CHAR (255) NOT NULL,
    CONSTRAINT [PKTblLogNmAdminTypemTypeNo] PRIMARY KEY CLUSTERED ([mTypeNo] ASC) WITH (FILLFACTOR = 90)
);


GO

