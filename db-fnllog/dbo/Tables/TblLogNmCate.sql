CREATE TABLE [dbo].[TblLogNmCate] (
    [mCateNo] INT       NOT NULL,
    [mNm]     CHAR (50) NOT NULL,
    CONSTRAINT [PKTblLogNmCatemCateNo] PRIMARY KEY CLUSTERED ([mCateNo] ASC) WITH (FILLFACTOR = 90)
);


GO

