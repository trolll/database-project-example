CREATE TABLE [dbo].[TblLogChat] (
    [mRegDate]  DATETIME      CONSTRAINT [DF_TblLogChat_20090703] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]    SMALLINT      NOT NULL,
    [mChatType] INT           NOT NULL,
    [mUserId]   VARCHAR (20)  NULL,
    [mIp]       VARCHAR (15)  NULL,
    [mCharNm]   VARCHAR (12)  NULL,
    [mChat]     VARCHAR (100) NULL,
    [mToNm]     VARCHAR (12)  NULL,
    CONSTRAINT [CK_TblLogChat_20090703] CHECK ((0)<=[mChatType])
);


GO

CREATE NONCLUSTERED INDEX [NCL_TblLogChat_mIp]
    ON [dbo].[TblLogChat]([mIp] ASC);


GO

CREATE NONCLUSTERED INDEX [NCL_TblLogChat_mCharNm]
    ON [dbo].[TblLogChat]([mCharNm] ASC);


GO

CREATE NONCLUSTERED INDEX [NCL_TblLogChat_mUserId]
    ON [dbo].[TblLogChat]([mUserId] ASC);


GO

