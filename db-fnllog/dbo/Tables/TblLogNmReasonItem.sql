CREATE TABLE [dbo].[TblLogNmReasonItem] (
    [mReasonNo] INT       NOT NULL,
    [mNm]       CHAR (50) NOT NULL,
    CONSTRAINT [PKTblLogNmReasonItemmReasonNo] PRIMARY KEY CLUSTERED ([mReasonNo] ASC) WITH (FILLFACTOR = 90)
);


GO

