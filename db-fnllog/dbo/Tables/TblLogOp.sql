CREATE TABLE [dbo].[TblLogOp] (
    [mRegDate]         DATETIME     DEFAULT (getdate()) NOT NULL,
    [mSvrNo]           SMALLINT     NOT NULL,
    [mCategory]        INT          NOT NULL,
    [mLogType]         INT          NOT NULL,
    [mUserNo]          INT          NULL,
    [mUserId]          VARCHAR (20) NULL,
    [mIp]              VARCHAR (15) NULL,
    [mCharNo]          INT          NULL,
    [mCharNm]          VARCHAR (20) NULL,
    [mMapNo]           INT          NULL,
    [mPosX]            REAL         NULL,
    [mPosY]            REAL         NULL,
    [mPosZ]            REAL         NULL,
    [mLevel]           SMALLINT     NULL,
    [mExp]             BIGINT       NULL,
    [mChaotic]         INT          NULL,
    [mCharNoOpp]       INT          NULL,
    [mCharNmOpp]       VARCHAR (20) NULL,
    [mItemId]          INT          NULL,
    [mItemNm]          VARCHAR (40) NULL,
    [mItemIsConfirm]   BIT          NULL,
    [mItemStatus]      TINYINT      NULL,
    [mItemCnt]         BIGINT       NULL,
    [mItemCntTot]      BIGINT       NULL,
    [mItemSerial]      BIGINT       NULL,
    [mItemSerialOpp]   BIGINT       NULL,
    [mInt1]            INT          NULL,
    [mInt2]            INT          NULL,
    [mInt3]            INT          NULL,
    [mChar1]           VARCHAR (40) NULL,
    [mChar2]           VARCHAR (40) NULL,
    [mBigInt1]         BIGINT       NULL,
    [mBigInt2]         BIGINT       NULL,
    [mVolitionOfHonor] SMALLINT     NULL,
    [mHonorPoint]      INT          NULL,
    [mChaosPoint]      BIGINT       NULL,
    CONSTRAINT [CK__TblLogOp__mCateg__540C7B00] CHECK ((0)<=[mCategory]),
    CONSTRAINT [CK__TblLogOp__mLogTy__55009F39] CHECK ((0)<[mLogType])
);


GO

CREATE NONCLUSTERED INDEX [IxTblLogOpIp]
    ON [dbo].[TblLogOp]([mIp] ASC, [mLogType] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [IxTblLogOpItemSerial]
    ON [dbo].[TblLogOp]([mItemSerial] ASC, [mItemSerialOpp] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [IxTblLogOpUser]
    ON [dbo].[TblLogOp]([mUserId] ASC, [mLogType] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [IxTblLogOpLogType]
    ON [dbo].[TblLogOp]([mLogType] ASC) WITH (FILLFACTOR = 80);


GO

CREATE NONCLUSTERED INDEX [IxTblLogOpChar]
    ON [dbo].[TblLogOp]([mCharNm] ASC, [mLogType] ASC) WITH (FILLFACTOR = 80);


GO

