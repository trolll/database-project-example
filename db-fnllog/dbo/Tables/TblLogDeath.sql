CREATE TABLE [dbo].[TblLogDeath] (
    [mRegDate]  DATETIME     CONSTRAINT [DF_TblLogDeath_20090705] DEFAULT (getdate()) NOT NULL,
    [mSvrNo]    SMALLINT     NULL,
    [mUserId]   VARCHAR (20) NOT NULL,
    [mCharNo]   INT          NOT NULL,
    [mCharNm]   VARCHAR (12) NOT NULL,
    [mToCharNo] INT          NULL,
    [mToCharNm] VARCHAR (12) NULL,
    [mGuildNo]  BIGINT       NULL,
    [mType]     TINYINT      NOT NULL,
    [mPlace]    INT          NOT NULL,
    [mPosX]     REAL         NOT NULL,
    [mPosY]     REAL         NOT NULL,
    [mPosZ]     REAL         NOT NULL,
    [mIsMacro]  BIT          NOT NULL,
    [mDrop1]    INT          NULL,
    [mDropNm1]  VARCHAR (40) NULL,
    [mDrop2]    INT          NULL,
    [mDropNm2]  VARCHAR (40) NULL,
    [mDrop3]    INT          NULL,
    [mDropNm3]  VARCHAR (40) NULL,
    [mDrop4]    INT          NULL,
    [mDropNm4]  VARCHAR (40) NULL,
    [mDrop5]    INT          NULL,
    [mDropNm5]  VARCHAR (40) NULL,
    [mDrop6]    INT          NULL,
    [mDropNm6]  VARCHAR (40) NULL,
    [mDrop7]    INT          NULL,
    [mDropNm7]  VARCHAR (40) NULL
);


GO

