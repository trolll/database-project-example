/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblPlaceInfo
Date                  : 2023-10-07 09:09:29
*/


INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (0, '???? ?? ??', 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (1, '????', 1, 0, 2, 1, 1, 0, 10, 255, 255, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (2, '?????', 0, 0, 2, 1, 0, 0, 0, 0, 0, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (3, '???', 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (4, '????? ??', 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (5, '?????', 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (6, '??? ????', 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (7, '??? ??', 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (8, '???? 1?', 1, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (9, '?????', 0, 1, 2, 1, 0, 0, 255, 10, 10, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (10, '?????', 0, 1, 2, 1, 0, 0, 255, 20, 20, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (11, '????', 1, 1, 0, 1, 1, 0, 255, 30, 30, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (12, '????(?)', 0, 0, 0, 1, 1, 1, 255, 40, 40, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (13, '????', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (14, '?? ?????(spot)', 1, 0, 0, 1, 1, 1, 255, 60, 60, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (15, '??? ?', 1, 1, 0, 1, 1, 0, 255, 70, 70, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (16, '??? ??', 1, 1, 0, 1, 1, 0, 255, 80, 80, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (17, '??? ???', 1, 1, 0, 1, 1, 0, 255, 90, 90, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (18, '???(spot)', 1, 0, 0, 1, 1, 1, 255, 100, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (19, '??? ????', 1, 1, 0, 1, 1, 0, 255, 110, 110, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (20, '?????(spot)', 1, 0, 0, 1, 1, 1, 255, 120, 120, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (21, '????(spot)', 1, 0, 0, 1, 1, 1, 255, 130, 130, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (22, '??? ?', 1, 1, 0, 1, 1, 0, 255, 140, 140, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (23, '??? ?? 1?', 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (24, '??? ?? 2?(spot)', 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (25, '??? ?? 3?', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (26, '??? ?? 4?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (27, '??? ??', 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (28, '??? ?? 5?', 0, 0, 0, 1, 1, 0, 60, 60, 60, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (29, '??? ?? 6?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (30, '????? ??? ??', 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (31, '????? ?? ??', 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (32, '???? 1?', 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (33, '???? 2?(spot)', 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (34, '???? 3?', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (35, '???? 4?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (36, '???? 5?', 0, 0, 0, 1, 1, 0, 130, 130, 130, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (37, '???? 6?', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (38, '???? 7?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (39, '??????', 0, 1, 2, 1, 0, 0, 10, 10, 255, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (40, '?????(spot)', 0, 0, 0, 1, 1, 1, 20, 20, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (41, '????', 1, 1, 0, 1, 1, 0, 30, 30, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (42, '?????(?????)(spot)', 1, 0, 0, 1, 1, 1, 40, 40, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (43, '?????(???)(spot)', 1, 0, 0, 1, 1, 1, 50, 50, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (44, '??? ???', 1, 1, 0, 1, 1, 0, 60, 60, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (45, '???? ??', 0, 1, 2, 1, 0, 0, 70, 70, 255, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (46, '????(spot)', 1, 0, 0, 1, 1, 1, 80, 80, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (47, '??? ???', 1, 1, 0, 1, 1, 0, 90, 90, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (48, '????? ???', 1, 1, 2, 1, 0, 0, 255, 160, 160, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (49, '??? ??(spot)', 1, 0, 0, 1, 1, 1, 110, 110, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (50, '??? ??(spot)', 1, 0, 0, 1, 1, 1, 120, 120, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (51, '???? 1?', 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (52, '???? 2?(spot)', 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (53, '???? 3?', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (54, '???? 4?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (55, '???? 5?', 0, 0, 0, 1, 1, 0, 200, 200, 200, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (56, '???? 6?', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (57, '???? 7?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (58, '???????', 1, 1, 0, 1, 1, 0, 130, 130, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (59, '????? 1?', 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (60, '????? 2?', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (61, '????? 3?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (62, '????? 4?', 0, 0, 0, 1, 1, 0, 60, 250, 60, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (63, '????? 5?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (64, '????? 6?', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (65, '????? 7?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (66, '???? 1?', 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (67, '???? 2?', 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (68, '???? 3?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (69, '???? 4?', 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (70, '???? 5?', 0, 0, 0, 1, 1, 0, 10, 250, 10, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (71, '??????', 0, 1, 1, 1, 1, 0, 255, 150, 150, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (72, '???????', 0, 1, 1, 1, 1, 0, 140, 140, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (73, '??? ??', 0, 1, 2, 1, 0, 0, 10, 255, 10, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (74, '??? ?(spot)', 0, 0, 0, 1, 1, 1, 20, 255, 20, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (75, '???????', 1, 1, 0, 1, 1, 0, 30, 255, 30, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (76, '???? ????', 1, 1, 0, 1, 1, 0, 40, 255, 40, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (77, '???? ???(spot)', 1, 0, 0, 1, 1, 1, 50, 255, 50, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (78, '?????(spot)', 1, 0, 0, 1, 1, 1, 60, 255, 60, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (79, '????(spot)', 1, 0, 0, 1, 1, 1, 70, 255, 70, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (80, '???', 1, 1, 0, 1, 1, 0, 80, 255, 80, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (81, '???? ??(spot)', 1, 0, 0, 1, 1, 1, 90, 255, 90, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (82, '?????', 1, 1, 0, 1, 1, 0, 100, 255, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (83, '??????', 0, 1, 1, 1, 1, 0, 110, 255, 110, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (84, '???????1?', 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (85, '???????2?', 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (86, '???????3?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (87, '???????4?', 1, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (88, '???????5?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (89, '???? ????1?', 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (90, '???? ????2?(spot)', 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (91, '???? ????3?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (92, '???? ????4?', 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (93, '???? ????5?', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (94, '????', 0, 1, 2, 1, 0, 0, 255, 255, 5, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (95, '???(????)', 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (96, '????? (???)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (97, '????? (????)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (98, '????? (???)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (99, '??? (?) (???)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (100, '??? (?) (???)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (101, '??? (?) (???)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (102, '??? (?) (???)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (103, '??? (?) (???)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (104, '??? (?) (???)', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (105, '? ?? ??? 0~30', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (106, '? ?? ??? 31~44', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (107, '? ?? ??? 45~Over', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (108, '??? ??', 0, 1, 0, 1, 1, 0, 255, 150, 160, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (109, '????', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (110, '??? ?? ???', 0, 0, 1, 0, 1, 0, 125, 240, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (111, '???? ??', 1, 1, 0, 1, 1, 0, 255, 255, 10, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (112, '?? ???? ??', 1, 1, 0, 1, 1, 0, 255, 255, 15, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (113, '??????', 1, 1, 0, 1, 1, 0, 255, 255, 20, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (114, '????', 1, 1, 0, 1, 1, 0, 255, 255, 25, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (115, '???? ??', 1, 1, 0, 1, 1, 0, 255, 255, 30, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (116, '??? ??', 1, 1, 0, 1, 1, 0, 255, 255, 35, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (117, '??? ??', 1, 1, 0, 1, 1, 0, 255, 255, 40, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (118, '???? ???', 1, 1, 0, 1, 1, 0, 255, 255, 45, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (119, '??? ??', 1, 1, 0, 1, 1, 0, 255, 255, 50, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (120, '???? ???', 1, 1, 2, 1, 0, 0, 255, 255, 85, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (121, '???? ?? ??', 1, 1, 0, 1, 1, 0, 255, 255, 60, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (122, '???? ?? ??', 0, 0, 0, 1, 1, 0, 255, 255, 65, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (123, '????? ?? 1?', 1, 0, 0, 1, 1, 0, 255, 255, 70, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (124, '????? ?? 2?', 1, 0, 0, 1, 1, 0, 255, 255, 75, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (125, '????? ?? 3?', 0, 0, 0, 1, 1, 0, 255, 255, 80, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (126, '??? ??', 1, 1, 0, 1, 1, 0, 255, 255, 90, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (127, '???', 0, 0, 2, 0, 0, 0, 255, 155, 170, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (128, '???', 0, 0, 2, 0, 0, 0, 255, 155, 170, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (129, '?? ????? ??', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (130, '?? ????? ??', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (131, '?? ????? ??', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (132, '??? ???? ??? 1', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (133, '??? ???? ??? 2', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (134, '??? ???? ??? 3', 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (135, '???? ?? - ??', 0, 0, 0, 1, 1, 0, 200, 240, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (136, '???? ?? - ?? ??', 0, 0, 2, 1, 0, 0, 210, 240, 220, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (137, '???? ?? - ??', 0, 0, 0, 1, 1, 0, 220, 240, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (138, '???? ??', 0, 0, 0, 1, 1, 0, 230, 240, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (139, '??? ??? 54 Over', 0, 0, 1, 1, 1, 0, 135, 240, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (140, '??? ??? ????', 0, 0, 1, 1, 1, 0, 240, 240, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (141, '??? ?? ????', 0, 0, 1, 1, 1, 0, 250, 240, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (142, '??? ?', 0, 0, 2, 1, 1, 0, 255, 0, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (143, '??? ??', 0, 1, 2, 1, 0, 0, 255, 10, 255, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (144, '??? ??', 0, 0, 2, 1, 1, 0, 255, 20, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (145, '??? ?', 0, 0, 2, 1, 1, 0, 255, 30, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (146, '??? ??', 0, 0, 2, 1, 1, 0, 255, 40, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (147, '??? ?', 0, 0, 2, 1, 1, 0, 255, 50, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (148, '??? ?', 0, 0, 2, 1, 1, 0, 255, 60, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (149, '?? ??', 0, 0, 1, 1, 1, 0, 255, 70, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (150, '????? ?', 0, 0, 1, 1, 1, 0, 255, 80, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (151, '???? ?? ??', 0, 0, 1, 1, 1, 0, 255, 90, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (152, '??? ??', 0, 0, 1, 1, 1, 0, 255, 100, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (153, '??? ??', 0, 0, 0, 1, 1, 0, 255, 10, 230, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (154, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 10, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (155, '??? ?? ?? ??', 0, 0, 0, 1, 1, 0, 255, 10, 210, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (156, '??? ??', 0, 0, 0, 1, 1, 0, 255, 20, 230, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (157, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 20, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (158, '??? ?? ?? ??', 0, 0, 0, 1, 1, 0, 255, 20, 210, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (159, '??? ??', 0, 0, 0, 1, 1, 0, 255, 30, 230, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (160, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 30, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (161, '??? ?? ?? ??', 0, 0, 0, 1, 1, 0, 255, 30, 210, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (162, '??? ??', 0, 0, 0, 1, 1, 0, 255, 40, 230, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (163, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 40, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (164, '??? ?? ?? ??', 0, 0, 0, 1, 1, 0, 255, 40, 210, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (165, '???? ?? ??(??? ?? ??)', 1, 0, 0, 1, 1, 0, 255, 15, 15, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (166, '???? ??', 1, 0, 0, 1, 1, 0, 255, 25, 25, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (167, '??? ??', 0, 0, 0, 1, 1, 0, 15, 255, 15, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (168, '????? ??1?', 0, 0, 0, 1, 1, 0, 25, 255, 25, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (169, '????? ??2?', 0, 0, 0, 1, 1, 0, 35, 255, 35, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (170, '????? ?', 0, 0, 0, 1, 1, 0, 190, 10, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (171, '??? ?', 0, 0, 0, 1, 1, 0, 200, 10, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (172, '??? ?', 0, 0, 0, 1, 1, 0, 210, 10, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (173, '??? ?', 0, 0, 0, 1, 1, 0, 220, 10, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (174, '??? ?', 0, 0, 0, 1, 1, 0, 230, 10, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (175, '????? ? ???', 0, 0, 0, 1, 1, 0, 240, 10, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (176, '?? ?? ???', 0, 0, 1, 1, 0, 0, 255, 5, 15, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (177, '?? ?? ???', 0, 0, 1, 1, 0, 0, 255, 5, 16, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (178, '?? ?? ???', 0, 0, 1, 1, 0, 0, 255, 5, 17, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (179, '??? ?? ???', 0, 0, 1, 1, 0, 0, 255, 5, 18, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (180, '? 1 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 1, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (181, '? 1 ????', 0, 0, 2, 1, 0, 0, 255, 5, 20, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (182, '? 1 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 34, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (183, '? 2 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 2, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (184, '? 2 ????', 0, 0, 2, 1, 0, 0, 255, 5, 21, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (185, '? 2 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 35, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (186, '? 3 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 3, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (187, '? 3 ????', 0, 0, 2, 1, 0, 0, 255, 5, 22, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (188, '? 3 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 36, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (189, '? 4 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 4, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (190, '? 4 ????', 0, 0, 2, 1, 0, 0, 255, 5, 23, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (191, '? 4 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 37, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (192, '? 5 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 5, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (193, '? 5 ????', 0, 0, 2, 1, 0, 0, 255, 5, 24, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (194, '? 5 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 38, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (195, '? 6 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 6, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (196, '? 6 ????', 0, 0, 2, 1, 0, 0, 255, 5, 25, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (197, '? 6 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 39, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (198, '? 7 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 7, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (199, '? 7 ????', 0, 0, 2, 1, 0, 0, 255, 5, 26, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (200, '? 7 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 40, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (201, '? 8 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 8, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (202, '? 8 ????', 0, 0, 2, 1, 0, 0, 255, 5, 27, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (203, '? 8 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 41, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (204, '? 9 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 9, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (205, '? 9 ????', 0, 0, 2, 1, 0, 0, 255, 5, 28, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (206, '? 9 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 42, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (207, '? 10 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 10, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (208, '? 10 ????', 0, 0, 2, 1, 0, 0, 255, 5, 29, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (209, '? 10 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 43, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (210, '? 11 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 11, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (211, '? 11 ????', 0, 0, 2, 1, 0, 0, 255, 5, 30, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (212, '? 11 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 44, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (213, '? 12 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 12, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (214, '? 12 ????', 0, 0, 2, 1, 0, 0, 255, 5, 31, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (215, '? 12 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 45, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (216, '? 13 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 13, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (217, '? 13 ????', 0, 0, 2, 1, 0, 0, 255, 5, 32, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (218, '? 13 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 46, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (219, '? 14 ?? ??', 0, 0, 1, 1, 1, 0, 255, 5, 14, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (220, '? 14 ????', 0, 0, 2, 1, 0, 0, 255, 5, 33, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (221, '? 14 ???? ???', 0, 0, 1, 1, 1, 0, 255, 5, 47, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (222, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (223, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 1, 2, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (224, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 1, 3, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (225, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 1, 4, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (226, '????? ??', 0, 0, 2, 1, 1, 0, 255, 255, 11, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (227, '???? ?? ??', 0, 0, 2, 1, 0, 0, 255, 255, 12, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (228, '??? ?', 0, 0, 0, 1, 1, 0, 255, 255, 13, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (229, '????? ??', 0, 0, 0, 1, 1, 0, 255, 255, 14, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (230, '?? ?? ?? ??', 0, 0, 0, 1, 1, 0, 255, 255, 16, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (231, '??? ??', 0, 0, 0, 1, 1, 0, 255, 255, 17, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (232, '???? ????', 0, 0, 0, 1, 1, 0, 255, 255, 18, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (233, '????? ??', 0, 0, 0, 1, 1, 0, 255, 255, 21, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (234, '?? ?? ??', 0, 0, 2, 1, 0, 0, 20, 255, 255, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (235, '??? ?? ??', 0, 0, 2, 1, 0, 0, 30, 255, 255, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (236, '??? ??', 0, 0, 2, 1, 0, 0, 40, 255, 255, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (237, '??? ??', 0, 0, 0, 1, 1, 0, 50, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (238, '??? ?? ??', 0, 0, 0, 1, 1, 0, 60, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (239, '??? ?? ??', 0, 0, 0, 1, 1, 0, 70, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (240, '??? ?? ??', 0, 0, 0, 1, 1, 0, 80, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (241, '?? ??? ??', 0, 0, 0, 1, 1, 0, 90, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (242, '????? ?? ???', 0, 0, 0, 1, 1, 0, 100, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (243, '????? ?? ???', 0, 0, 0, 1, 1, 0, 110, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (244, '????? ?? ???', 0, 0, 0, 1, 1, 0, 120, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (245, '?? ??? ??', 0, 0, 0, 1, 1, 0, 130, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (246, '?? ??', 0, 0, 0, 1, 1, 0, 140, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (247, '?? ?? ???', 0, 0, 0, 1, 1, 0, 150, 255, 255, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (248, '??? ???', 0, 0, 0, 1, 1, 0, 255, 255, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (249, '??? ??? ??', 0, 0, 2, 1, 0, 0, 255, 255, 101, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (250, '??? ?', 0, 0, 0, 1, 1, 0, 255, 255, 102, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (251, '??? ??', 0, 0, 0, 1, 1, 0, 255, 255, 103, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (252, '??? ??', 0, 0, 0, 1, 1, 0, 255, 255, 104, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (253, '???? ???', 0, 0, 0, 1, 1, 0, 255, 255, 105, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (254, '??? ??', 0, 0, 0, 1, 1, 0, 255, 255, 106, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (255, '???? ????', 0, 0, 2, 1, 0, 0, 255, 255, 107, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (256, '??? ?? ????', 0, 0, 0, 1, 1, 0, 255, 255, 108, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (257, '??? ?? ???', 0, 0, 0, 1, 1, 0, 255, 255, 109, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (258, '???? ??', 0, 0, 0, 1, 1, 0, 255, 255, 110, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (259, '??? ??', 0, 0, 0, 1, 1, 0, 255, 255, 111, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (260, '??? ??? ????', 0, 0, 0, 1, 1, 0, 255, 255, 112, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (261, '??? ??', 0, 0, 0, 1, 1, 0, 255, 50, 230, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (262, '??? ?? ?? ??', 0, 0, 2, 1, 0, 0, 255, 50, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (263, '??? ?? ?? ??', 0, 0, 0, 1, 1, 0, 255, 50, 210, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (264, '??? ?? ?? ????', 0, 0, 2, 1, 0, 0, 255, 1, 5, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (265, '?? ????', 0, 0, 1, 1, 1, 0, 255, 255, 115, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (266, '?1 ??? ??', 0, 0, 1, 1, 1, 0, 255, 255, 116, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (267, '?2 ??? ??', 0, 0, 1, 1, 1, 0, 255, 255, 117, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (268, '?3 ??? ??', 0, 0, 1, 1, 1, 0, 255, 255, 118, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (269, '?4 ??? ??', 0, 0, 1, 1, 1, 0, 255, 255, 119, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (270, '?5 ??? ??', 0, 0, 1, 1, 1, 0, 255, 255, 120, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (271, '?6 ??? ??', 0, 0, 1, 1, 1, 0, 255, 255, 121, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (272, '?7 ??? ??', 0, 0, 1, 1, 1, 0, 255, 255, 122, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (273, '?8 ??? ??', 0, 0, 1, 1, 1, 0, 255, 255, 123, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (274, '?????????', 0, 0, 2, 1, 0, 0, 1, 255, 201, 1, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (275, '???????', 0, 0, 1, 1, 1, 0, 1, 255, 218, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (276, '????????', 0, 0, 0, 1, 1, 1, 1, 255, 202, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (277, '??????', 0, 0, 0, 1, 1, 1, 1, 255, 203, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (278, '???????', 0, 0, 1, 1, 0, 0, 1, 255, 204, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (279, '??????', 0, 1, 0, 1, 1, 0, 1, 255, 205, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (280, '??????', 0, 1, 0, 1, 1, 0, 1, 255, 206, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (281, '????', 0, 1, 0, 1, 1, 0, 1, 255, 207, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (282, '????', 0, 1, 0, 1, 1, 0, 1, 255, 208, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (283, '??????', 0, 0, 0, 1, 1, 0, 1, 255, 209, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (284, '??????', 0, 1, 0, 1, 1, 0, 1, 255, 210, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (285, '??????', 0, 1, 0, 1, 1, 0, 1, 255, 211, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (286, '??????', 0, 1, 0, 1, 1, 0, 1, 255, 212, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (287, '??????', 0, 0, 0, 1, 1, 0, 1, 255, 213, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (288, '?????', 0, 1, 0, 1, 1, 0, 1, 255, 214, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (289, '??????', 0, 1, 0, 1, 1, 0, 1, 255, 215, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (290, '??????', 0, 1, 0, 1, 1, 0, 1, 255, 216, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (291, '?????', 0, 1, 0, 1, 1, 0, 1, 255, 217, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (292, '??? ? ???', 0, 1, 0, 1, 1, 0, 1, 255, 219, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (293, '??? ? ??? ??', 0, 0, 0, 1, 1, 0, 1, 255, 220, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (294, '?????? ??', 1, 0, 0, 1, 1, 0, 1, 255, 235, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (295, '?????? ??', 1, 0, 0, 1, 1, 0, 1, 255, 234, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (296, '?????? 1?', 1, 0, 0, 1, 1, 0, 1, 255, 230, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (297, '?????? 1?', 1, 0, 0, 1, 1, 0, 1, 255, 231, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (298, '?????? 2?', 1, 0, 0, 1, 1, 0, 1, 255, 232, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (299, '?????? 2?', 1, 0, 0, 1, 1, 0, 1, 255, 233, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (300, '????? ??', 0, 0, 0, 1, 1, 0, 1, 255, 240, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (301, '??? ???', 1, 0, 0, 1, 1, 0, 255, 1, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (302, '??? ??', 0, 0, 0, 1, 1, 0, 255, 2, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (303, '???? ?? 70', 1, 0, 0, 1, 1, 0, 255, 10, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (304, '???? ?? 80', 1, 0, 0, 1, 1, 0, 255, 20, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (305, '????? ??', 1, 0, 0, 1, 1, 0, 255, 30, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (306, '??? ???', 1, 0, 0, 1, 1, 0, 255, 40, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (307, '????? ?? ??', 1, 0, 0, 1, 1, 0, 255, 50, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (308, '?1 ???', 0, 0, 1, 0, 1, 0, 255, 110, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (309, '?2 ???', 0, 0, 1, 0, 1, 0, 255, 60, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (310, '?3 ???', 0, 0, 1, 0, 1, 0, 255, 70, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (311, '?4 ???', 0, 0, 1, 0, 1, 0, 255, 80, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (312, '?5 ???', 0, 0, 1, 0, 1, 0, 255, 90, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (313, '?6 ???', 0, 0, 1, 0, 1, 0, 255, 95, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (314, '?1 ??', 0, 0, 1, 1, 1, 0, 255, 35, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (315, '?2 ??', 0, 0, 1, 1, 1, 0, 255, 45, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (316, '?3 ??', 0, 0, 1, 1, 1, 0, 255, 55, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (317, '?4 ??', 0, 0, 1, 1, 1, 0, 255, 65, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (318, '?5 ??', 0, 0, 1, 1, 1, 0, 255, 75, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (319, '?6 ??', 0, 0, 1, 1, 1, 0, 255, 85, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (320, '???? ?? ??', 0, 0, 0, 1, 1, 0, 255, 102, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (321, '???? ?? ??', 0, 0, 0, 1, 1, 0, 255, 103, 100, 0, 1);
GO

INSERT INTO [dbo].[TblPlaceInfo] ([mPlaceNo], [mPlaceNm], [mTeleport], [mTeleportSave], [mCombat], [mReturn], [mIsCollision], [mIsSiege], [mRgbQuadRed], [mRgbQuadGreen], [mRgbQuadBlue], [mIsTown], [mIsSupport]) VALUES (322, '???? ?? ??', 1, 0, 0, 1, 1, 0, 255, 101, 100, 0, 1);
GO

