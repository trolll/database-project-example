/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblEventDungeonInfo
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[TblEventDungeonInfo] ([mDungeonPlace], [mBossMID], [mIngressBossIID], [mIngressQuestIID], [mIngressAID], [mStartX], [mStartZ], [mEndX], [mEndZ], [mStartPosX], [mStartPosY], [mStartPosZ], [mDesc], [mDungeonType], [mMinArea], [mPartyCnt], [mPlayTime], [mAcceptTime], [mStartPosX2], [mStartPosY2], [mStartPosZ2], [mTeleportPosRatio]) VALUES (28, 540, 6730, 6678, 2339, 945474.0, 189131.0, 976612.0, 220498.0, 945998.0, 16363.0, 219649.0, '0.000000', 0, 90000000, 10, 30, 30, 0.0, 0.0, 0.0, 50);
GO

INSERT INTO [dbo].[TblEventDungeonInfo] ([mDungeonPlace], [mBossMID], [mIngressBossIID], [mIngressQuestIID], [mIngressAID], [mStartX], [mStartZ], [mEndX], [mEndZ], [mStartPosX], [mStartPosY], [mStartPosZ], [mDesc], [mDungeonType], [mMinArea], [mPartyCnt], [mPlayTime], [mAcceptTime], [mStartPosX2], [mStartPosY2], [mStartPosZ2], [mTeleportPosRatio]) VALUES (36, 577, 7576, 7578, 2936, 946178.0, 757212.0, 975808.0, 786833.0, 950874.0, 16970.0, 784714.0, '0.000000', 0, 90000000, 10, 30, 30, 0.0, 0.0, 0.0, 50);
GO

INSERT INTO [dbo].[TblEventDungeonInfo] ([mDungeonPlace], [mBossMID], [mIngressBossIID], [mIngressQuestIID], [mIngressAID], [mStartX], [mStartZ], [mEndX], [mEndZ], [mStartPosX], [mStartPosY], [mStartPosZ], [mDesc], [mDungeonType], [mMinArea], [mPartyCnt], [mPlayTime], [mAcceptTime], [mStartPosX2], [mStartPosY2], [mStartPosZ2], [mTeleportPosRatio]) VALUES (55, 538, 6980, 6976, 2355, 1010060.0, 1782.0, 1036040.0, 28451.0, 1009826.0, 13041.0, 29615.0, '0.000000', 0, 90000000, 10, 30, 30, 0.0, 0.0, 0.0, 50);
GO

INSERT INTO [dbo].[TblEventDungeonInfo] ([mDungeonPlace], [mBossMID], [mIngressBossIID], [mIngressQuestIID], [mIngressAID], [mStartX], [mStartZ], [mEndX], [mEndZ], [mStartPosX], [mStartPosY], [mStartPosZ], [mDesc], [mDungeonType], [mMinArea], [mPartyCnt], [mPlayTime], [mAcceptTime], [mStartPosX2], [mStartPosY2], [mStartPosZ2], [mTeleportPosRatio]) VALUES (62, 818, 6731, 6679, 2340, 1071262.0, 252062.0, 1102760.0, 283560.0, 1072964.0, 16109.0, 280085.0, '0.000000', 0, 90000000, 10, 30, 30, 0.0, 0.0, 0.0, 50);
GO

INSERT INTO [dbo].[TblEventDungeonInfo] ([mDungeonPlace], [mBossMID], [mIngressBossIID], [mIngressQuestIID], [mIngressAID], [mStartX], [mStartZ], [mEndX], [mEndZ], [mStartPosX], [mStartPosY], [mStartPosZ], [mDesc], [mDungeonType], [mMinArea], [mPartyCnt], [mPlayTime], [mAcceptTime], [mStartPosX2], [mStartPosY2], [mStartPosZ2], [mTeleportPosRatio]) VALUES (70, 536, 6981, 6977, 2356, 1010424.0, 632148.0, 1036786.0, 659063.0, 1010792.0, 13018.0, 657954.0, '0.000000', 0, 90000000, 10, 30, 30, 0.0, 0.0, 0.0, 50);
GO

INSERT INTO [dbo].[TblEventDungeonInfo] ([mDungeonPlace], [mBossMID], [mIngressBossIID], [mIngressQuestIID], [mIngressAID], [mStartX], [mStartZ], [mEndX], [mEndZ], [mStartPosX], [mStartPosY], [mStartPosZ], [mDesc], [mDungeonType], [mMinArea], [mPartyCnt], [mPlayTime], [mAcceptTime], [mStartPosX2], [mStartPosY2], [mStartPosZ2], [mTeleportPosRatio]) VALUES (306, 0, 0, 8457, 3526, 1134745.0, 1071792.0, 1165293.0, 1102466.0, 1156643.0, 14137.0, 1076131.0, '0.000000', 1, 90000000, 5, 30, 30, 1143935.0, 14137.0, 1088997.0, 30);
GO

