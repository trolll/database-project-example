/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblEvent
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (7, 'Event', 0, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (8, 'Server combine guild rename', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (9, 'Hades event', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (10, 'golden treasure box', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (11, 'ThanksGiving Day', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (12, 'Material', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (13, 'Annexed service NPC', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (14, 'Disciple', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (15, 'CN_Real_X-Mas_Event_08', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (16, 'CN New_Year_Event(bokjumeoni) 09', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (17, 'china Random_Box_Event', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (18, 'Valentine Day', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (19, 'CN_R2Year_Event', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (20, 'PCZone_Support', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (21, 'Double_Sevens_Day', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (22, 'Premium NPC', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (23, 'CN_NPC1068', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (24, 'China_National_Day', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (25, 'Hallowin', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (26, 'meet_winter', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (27, '110527_DANOH_EVENT', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (28, '110527_DANOH_EVENT_1', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (29, '110527_DANOH_EVENT', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (30, '110527_DANOH_EVENT_1', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (31, 'CN_Thanksgiving_2011', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (32, 'CN_Thanksgiving_2011', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (33, 'CN_Thanksgiving_2011', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (34, 'CN_EVENT_NPC_2011080', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (35, 'CN_NewYears_2012', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (36, '0116_CN_NewYears_2012', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (37, 'tower', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (38, 'tower', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (39, 'tower', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (40, 'CN_Thanksgiving_2011', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (41, 'Lunar_New_Year_20130123', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (42, 'cheeky_rabbit_20130314', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (43, 'cheeky_rabbit_20130314', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (44, 'CN_Thanksgiving_2011', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (45, 'cheeky_rabbit_20130724', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (46, 'cheeky_rabbit_20130724', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (47, 'CN_Thanksgiving_2011', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (48, 'CN_Thanksgiving_2013', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (49, 'Lunar_New_Year_20130123', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (50, 'cheeky_rabbit_20130314', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (51, 'cheeky_rabbit_20130314', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (52, 'cheeky_rabbit_20130314', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (53, 'cheeky_rabbit_20130314', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (54, 'cheeky_rabbit_20130314', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (55, 'CN_Thanksgiving_2013', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (56, 'CN_Thanksgiving_2013', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (57, '?? ?? ???', 1, '???? ?? ???, ??(5?)');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (58, '????', 1, '????');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (59, 'CN_SkillTreeInitialize', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (60, 'thanksgiving_day_event_20141104', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (61, 'lunar_new_year_event_20150115', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (62, 'lunar_new_year_event_20150115', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (63, '困殴 魄概 酒捞袍', 1, '困殴魄概惑牢 NPC 眠啊');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (64, 'CN_SkillTreeInitialize', 1, '胶懦飘府 檬扁拳');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (65, '墨坷胶 器牢飘 NPC 眠啊', 1, '墨坷胶 器牢飘 NPC 眠啊');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (66, '困殴 魄概 酒捞袍', 1, '困殴魄概惑牢 NPC 眠啊');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (67, 'CN_SkillTreeInitialize', 1, '胶懦飘府 檬扁拳');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (68, '墨坷胶 器牢飘 NPC 眠啊', 1, '墨坷胶 器牢飘 NPC 眠啊');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (69, '拱距力炼荤_event_20151015', 1, NULL);
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (70, '?￥oê??????', 1, '???à?o?￥oê ???? NPC');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (71, '?￥oê??????', 1, '???à?o?￥oê ???? NPC');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (72, '惯饭器福', 1, '惯饭器福 带傈 NPC');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (73, '2018 New Year Event', 1, '');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (74, '辑锅飘 炼访荤', 1, '辑锅飘 炼访荤');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (75, '惯饭器福炮饭器磐', 1, '惯饭器福炮饭器磐');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (76, 'emerald', 1, 'emerald');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (77, 'rabbit', 1, 'rabbit');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (78, 'rabbit180917', 1, 'rabbit180917');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (79, 'emerald', 1, 'emerald');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (80, 'rabbit', 1, 'rabbit');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (81, 'rabbit181207', 1, 'rabbit181207');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (82, 'rabbit190121', 1, 'rabbit190121');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (84, '宠物系统', 1, '宠物系统');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (85, '团队战终止', 1, '团队战终止');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (86, '华利弗地下城个体数活动', 1, '华利弗地下城个体数活动');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (87, 'rabbit190121', 1, 'rabbit190121');
GO

INSERT INTO [dbo].[TblEvent] ([mEventID], [mTitle], [mIsEverEvent], [mDesc]) VALUES (88, '竞技场内容', 1, '竞技场内容');
GO

