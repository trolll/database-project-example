/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_SkillAbnormal
Date                  : 2023-10-07 09:08:45
*/


INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (5, 5);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (6, 167);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (7, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (13, 36);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (8, 8);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (4, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (20, 57);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (16, 45);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (16, 62);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (17, 45);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (17, 62);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (18, 50);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (14, 11);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (14, 25);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (141, 21);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (19, 54);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (22, 9);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (23, 16);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (25, 22);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (27, 65);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (24, 17);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (30, 26);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (54, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (34, 70);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (35, 68);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (37, 73);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (38, 75);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (39, 76);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (41, 52);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (40, 130);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (36, 12);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (102, 175);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (21, 65);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (52, 80);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (44, 78);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (45, 79);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (46, 81);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (47, 84);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (49, 71);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (28, 66);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (50, 86);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (51, 87);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1953, 2254);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (53, 69);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (48, 85);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (42, 88);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (55, 25);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (56, 63);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2433, 2839);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (359, 160);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (66, 80);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (62, 8);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (73, 71);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (61, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (63, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (68, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (69, 106);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (64, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (65, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (60, 102);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (59, 103);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1841, 184);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (76, 107);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (77, 115);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1842, 1035);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1843, 1036);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1844, 2108);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (960, 2114);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (960, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (80, 121);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (81, 122);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (119, 55);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (89, 123);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (94, 141);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (95, 143);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (96, 142);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (97, 143);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (105, 159);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (106, 160);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (109, 161);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (108, 166);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (110, 144);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (107, 165);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (111, 169);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (113, 171);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (112, 170);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (114, 173);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (115, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (116, 103);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (122, 177);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (121, 179);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (124, 180);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (125, 180);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (126, 181);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (127, 182);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (128, 183);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (131, 184);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (132, 187);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (90, 124);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (91, 130);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (93, 131);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (98, 152);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (99, 154);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (100, 153);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (103, 155);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (104, 156);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (88, 17);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (134, 188);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (133, 186);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (135, 189);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (136, 2353);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (150, 202);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (140, 195);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (142, 11);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (143, 50);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (145, 25);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (146, 36);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (147, 62);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (150, 201);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (151, 205);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (152, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (152, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (153, 207);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (154, 210);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (155, 212);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (156, 213);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (157, 211);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (158, 214);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (159, 215);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (160, 218);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (161, 217);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (162, 219);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (163, 216);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (164, 221);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (165, 220);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (166, 227);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (169, 230);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (170, 231);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (175, 236);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (176, 238);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (182, 214);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (183, 212);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2434, 2834);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (185, 245);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (188, 259);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (315, 488);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (148, 199);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (189, 260);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (192, 331);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (191, 333);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (190, 332);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (257, 330);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (196, 246);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (195, 52);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (204, 237);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (212, 148);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (258, 52);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (260, 310);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (205, 268);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (205, 269);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (205, 270);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (215, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (214, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (213, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (260, 311);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (210, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (209, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (208, 229);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (206, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (216, 272);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (217, 273);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (218, 271);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (167, 229);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (167, 275);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (314, 487);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (222, 281);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (363, 498);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (225, 286);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (224, 285);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (221, 279);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (797, 978);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (798, 979);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (235, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (235, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (235, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (235, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (235, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (235, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (241, 324);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (242, 308);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (243, 309);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (246, 310);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (246, 311);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (246, 312);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1043, 1875);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (246, 315);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (248, 316);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (179, 244);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (179, 241);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (251, 319);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (260, 312);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (261, 335);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (252, 320);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (261, 334);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (262, 336);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (262, 337);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (263, 338);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (263, 339);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (194, 358);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (280, 361);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (281, 362);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (282, 363);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (283, 365);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (284, 455);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (285, 456);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (291, 464);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (288, 460);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (292, 461);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (293, 462);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (290, 463);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (297, 459);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (294, 465);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (295, 466);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (296, 467);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (289, 468);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (290, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (290, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (289, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (289, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (288, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (288, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (291, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (291, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (292, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (292, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (293, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (293, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1401, 1418);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1409, 1440);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (869, 1452);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (870, 1453);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1408, 1419);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (871, 1454);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (309, 482);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (311, 483);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (312, 485);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (316, 489);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (317, 490);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (318, 491);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (961, 2114);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (364, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (364, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (364, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (364, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (364, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (364, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (373, 508);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (374, 509);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (375, 511);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (376, 782);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (377, 518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (378, 519);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (379, 520);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (385, 530);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (383, 529);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (387, 532);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (389, 533);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (528, 691);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (396, 550);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (395, 550);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (138, 196);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (137, 197);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (139, 198);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (149, 200);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (310, 484);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (961, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (363, 499);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (380, 523);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (193, 261);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (197, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (198, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (199, 280);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (200, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (201, 4);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (254, 131);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (255, 322);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (265, 343);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (265, 344);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (265, 345);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (286, 457);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (287, 458);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (313, 486);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (219, 277);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (220, 278);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (322, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (323, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (324, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (325, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (326, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (397, 237);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (152, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (152, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (152, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (152, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (168, 228);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (223, 287);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (171, 233);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (172, 232);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (249, 317);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (250, 318);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (360, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (360, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (361, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (361, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (362, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (362, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (360, 495);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (362, 497);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (361, 496);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (29, 206);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2247, 2591);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (279, 360);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (178, 240);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (320, 492);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2435, 2835);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (184, 248);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2436, 2836);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2437, 2854);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2438, 2856);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2439, 2858);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (444, 693);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2440, 2860);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2441, 2862);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (343, 22);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (344, 22);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (345, 22);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (346, 22);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (15, 22);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2442, 2864);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (382, 528);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (390, 534);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (406, 562);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (393, 537);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (409, 572);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (398, 551);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (399, 553);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (400, 552);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (227, 288);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (268, 348);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (269, 349);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (270, 350);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (271, 351);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (272, 352);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (273, 353);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (274, 354);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (275, 355);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (276, 356);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (277, 357);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (239, 300);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (238, 299);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (240, 301);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (253, 325);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (207, 326);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (207, 327);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (278, 359);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (211, 328);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (264, 342);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (267, 346);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (266, 347);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (299, 298);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (300, 297);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (301, 475);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (302, 476);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (303, 477);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (304, 478);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (305, 479);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (306, 480);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (307, 481);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (308, 120);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (365, 500);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (366, 217);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (367, 216);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (368, 218);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (369, 267);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (369, 169);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (369, 170);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (369, 171);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (370, 7);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (370, 501);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (370, 502);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (370, 503);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (370, 504);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (370, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (371, 505);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (372, 506);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (386, 531);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (401, 554);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (402, 555);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (403, 556);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (464, 623);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (447, 594);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (448, 595);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (449, 596);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (450, 597);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (451, 598);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (452, 599);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (453, 601);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (454, 602);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (455, 603);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (456, 604);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (457, 605);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (458, 606);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (459, 600);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (460, 607);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (461, 609);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (462, 633);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (463, 635);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (465, 507);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (466, 614);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (467, 615);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (468, 616);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (469, 617);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (470, 618);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (471, 619);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (472, 620);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (473, 621);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (474, 630);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (475, 631);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (476, 634);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (477, 636);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (479, 634);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (478, 636);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (480, 632);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (481, 637);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (482, 638);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (486, 694);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (484, 640);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (485, 641);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (458, 704);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (487, 644);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (474, 705);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (475, 705);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (522, 677);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 343);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 344);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 345);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (542, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2443, 2866);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2444, 2868);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (524, 679);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (527, 690);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (408, 567);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (394, 538);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (412, 573);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (532, 54);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (533, 103);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (530, 57);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (531, 50);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (529, 36);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (483, 692);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (488, 695);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (489, 689);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (534, 703);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (535, 702);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (536, 701);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (537, 700);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (538, 699);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (539, 698);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (540, 697);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (541, 696);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (543, 707);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (490, 646);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (491, 647);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (518, 674);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (519, 675);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (520, 610);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (521, 676);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (523, 678);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (544, 708);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (545, 709);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (546, 710);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (549, 713);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (550, 714);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (551, 715);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (552, 716);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (553, 717);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (554, 718);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (555, 719);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (556, 720);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (557, 721);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (558, 722);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (559, 724);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (565, 725);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (566, 726);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (568, 237);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (569, 728);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (572, 731);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (573, 732);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (574, 733);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (575, 734);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (576, 735);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (577, 736);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (578, 737);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (579, 738);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (580, 739);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (581, 740);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (582, 741);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (583, 742);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (584, 743);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (587, 746);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (586, 745);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (585, 744);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (585, 747);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (587, 748);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (586, 748);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (590, 751);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (588, 749);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (589, 750);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (594, 754);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (595, 755);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (596, 756);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (611, 776);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (597, 758);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (598, 759);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (599, 760);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (601, 757);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (612, 777);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (613, 778);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (614, 779);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (615, 780);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (616, 781);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (617, 784);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (621, 786);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1725, 1898);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1724, 1897);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (620, 787);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (622, 790);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (648, 816);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (570, 729);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (651, 829);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (652, 830);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (667, 834);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (649, 833);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (668, 836);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (670, 11);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (670, 25);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (671, 837);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (672, 838);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (673, 839);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (674, 840);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (675, 841);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (676, 842);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (677, 843);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (678, 844);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (679, 845);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (680, 846);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (681, 847);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (682, 848);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (683, 849);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (684, 850);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (685, 851);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (686, 852);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (687, 853);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (688, 854);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (689, 855);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (690, 856);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (691, 857);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (692, 858);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (693, 859);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (694, 860);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (696, 864);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (697, 866);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (698, 862);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (699, 865);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (700, 868);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (701, 870);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (706, 867);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (707, 869);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (708, 872);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (709, 873);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (710, 874);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2588, 3044);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (712, 877);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (713, 878);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (714, 879);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (715, 880);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (718, 889);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (719, 886);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (720, 888);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (721, 885);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (722, 887);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (718, 892);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1127, 1885);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1127, 2105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (724, 895);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (725, 897);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (726, 160);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (727, 894);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (728, 896);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (758, 926);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (759, 931);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (780, 9);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (799, 980);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (800, 981);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (763, 948);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (765, 950);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (764, 949);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (766, 951);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (767, 952);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (768, 953);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (769, 954);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (770, 955);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (773, 958);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (771, 956);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (772, 957);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (774, 959);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (777, 962);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (776, 961);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (775, 960);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (801, 982);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (802, 983);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (803, 984);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (804, 985);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (805, 986);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (806, 987);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (807, 988);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (808, 989);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (809, 990);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (810, 991);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (811, 22);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (813, 633);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (814, 995);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (815, 999);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (816, 1000);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (817, 2871);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2445, 2872);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2454, 2883);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2455, 2884);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2457, 2883);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2458, 2884);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (819, 1001);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (820, 1004);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (821, 1003);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (822, 758);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (840, 1026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (840, 1025);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (841, 1027);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (841, 1028);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (841, 1029);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (842, 1032);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (843, 1033);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (844, 1030);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (845, 1031);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (752, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (752, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (752, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (752, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (752, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (752, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (846, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (859, 1050);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (855, 1046);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (856, 1047);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (857, 1048);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (858, 1049);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (887, 1067);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (888, 1073);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (889, 1074);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (890, 1075);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (891, 1076);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (892, 1082);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1714, 1834);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1715, 1833);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1726, 1899);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1727, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1727, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1727, 868);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1727, 872);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1727, 879);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1727, 885);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1728, 1900);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1388, 1319);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1389, 1320);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1390, 1321);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1729, 1901);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1730, 1902);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1731, 1903);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1732, 1904);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1733, 1905);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1380, 1311);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1381, 1312);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1382, 1313);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1379, 1310);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1376, 1307);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1377, 1308);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1378, 1309);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1375, 1301);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1372, 1298);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1373, 1299);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1374, 1300);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1367, 1293);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1368, 1294);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1369, 1295);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1370, 1296);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1371, 1297);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1362, 1288);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1363, 1289);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1364, 1290);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1365, 1291);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1366, 1292);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (965, 2116);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (965, 1523);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (966, 2116);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1351, 1280);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1352, 1281);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1353, 1282);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1354, 1283);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1355, 1284);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (328, 1918);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1344, 1276);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1345, 1277);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1346, 1278);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1339, 1271);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1340, 1272);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1341, 1273);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1342, 1274);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1343, 1275);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1336, 1257);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1337, 1258);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1338, 1259);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1327, 1267);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1328, 1268);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1329, 1269);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1330, 1270);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1322, 1252);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1323, 1253);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1324, 1254);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1325, 1255);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1326, 1256);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1331, 1262);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1332, 1263);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1333, 1264);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1334, 1265);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1335, 1266);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1317, 1247);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1318, 1248);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1319, 1249);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1320, 1250);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1321, 1251);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1312, 1242);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1313, 1243);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1314, 1244);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1315, 1245);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1316, 1246);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1302, 1237);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1303, 1238);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1304, 1239);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1305, 1240);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1306, 1241);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1299, 1234);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1300, 1235);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1301, 1236);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1289, 1229);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1290, 1230);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1291, 1231);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1218, 1596);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1293, 1233);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1284, 1224);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1285, 1225);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1286, 1226);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1287, 1227);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1288, 1228);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1276, 1219);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1277, 1220);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1278, 1221);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1279, 1222);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1280, 1223);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1268, 1216);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1269, 1217);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1270, 1218);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1265, 1213);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1266, 1214);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1267, 1215);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1262, 1210);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1263, 1211);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1264, 1212);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1259, 1207);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1260, 1208);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1261, 1209);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (329, 1918);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (330, 1918);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (334, 1919);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1251, 1199);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1252, 1200);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1253, 1201);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1254, 1202);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1255, 1203);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (333, 1919);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (332, 1919);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (331, 1919);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1245, 1193);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1246, 1194);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1247, 1195);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1237, 1185);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1238, 1186);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1239, 1187);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1240, 1188);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1241, 1189);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1230, 1600);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1231, 1601);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1225, 1147);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1226, 1149);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1227, 1151);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1228, 1153);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1229, 1154);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1219, 1597);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1678, 1783);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1213, 1123);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1214, 1125);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1215, 1127);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1216, 1128);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (70, 1919);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1404, 1426);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1405, 1427);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1406, 1428);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (872, 1455);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (873, 1456);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (874, 1457);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (875, 1458);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (876, 1459);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (877, 1460);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (878, 1461);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (879, 1462);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (880, 1463);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (881, 1464);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (882, 1465);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (883, 1466);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (894, 1467);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (895, 1468);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (896, 1469);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (897, 1470);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (898, 1471);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (71, 1920);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (966, 1523);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (967, 2117);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (967, 1523);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (974, 1533);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (904, 1477);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (905, 1478);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (906, 1479);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (907, 1480);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (908, 1481);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (909, 1482);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (974, 2118);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (337, 1920);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (338, 1920);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (913, 1550);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (914, 1551);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (915, 1552);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (916, 1486);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (917, 1487);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (918, 1488);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (919, 1489);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (920, 1490);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (921, 1491);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (922, 1492);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (923, 1493);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1679, 1784);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1680, 1785);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1681, 1786);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1682, 1787);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1683, 1788);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1684, 1789);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1376, 1786);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1377, 1787);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1378, 1788);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1379, 1789);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (72, 1921);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (339, 1921);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (340, 1921);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (341, 1921);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (342, 1921);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (975, 1533);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (975, 2119);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (977, 1536);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (977, 2120);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (978, 1536);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (9, 1918);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2462, 1040);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (327, 1918);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1753, 1951);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1754, 1952);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1755, 1953);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (950, 1509);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (978, 2121);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (953, 1512);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (954, 1512);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (956, 1515);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (957, 1515);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (959, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (951, 1509);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (950, 2122);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (392, 536);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1756, 1954);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (404, 558);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (405, 557);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (233, 298);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1988, 2308);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (825, 1012);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (851, 1037);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (847, 1035);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (847, 1038);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (848, 184);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (848, 326);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (849, 1036);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (849, 1039);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (850, 1038);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (850, 1036);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1415, 1451);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1138, 1560);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1139, 1561);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1140, 1562);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1141, 1563);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1142, 1564);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1164, 1565);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1165, 1566);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1166, 1567);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1177, 1568);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1178, 1569);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1179, 1570);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1180, 1571);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1181, 1572);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (958, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1757, 1955);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1761, 1959);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1762, 1960);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1763, 1961);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (963, 1523);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1758, 1956);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1759, 1957);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1760, 1958);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1773, 1964);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (968, 1528);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1774, 1965);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1824, 2079);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1825, 2080);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1826, 2081);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (973, 1533);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1827, 2082);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1828, 2083);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (976, 1536);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1829, 2084);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1775, 1966);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (979, 1539);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1776, 1967);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1777, 1968);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (982, 1875);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (983, 1876);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (984, 1550);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (985, 1551);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (986, 1486);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (987, 1487);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (988, 1480);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (989, 1481);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1190, 1565);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1191, 1566);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1192, 1567);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1220, 1457);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1221, 1458);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1222, 1459);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1223, 1460);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1224, 1461);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1232, 1026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1778, 1969);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1779, 1970);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1749, 1984);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (996, 1533);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (997, 1534);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (998, 1536);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (999, 1537);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1000, 1539);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1001, 1540);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1002, 1452);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1003, 1453);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1004, 1454);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1005, 1455);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1006, 1456);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1007, 1457);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1008, 1458);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1009, 1459);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1010, 1460);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1011, 1461);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1012, 1542);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1013, 1543);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1014, 1544);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1015, 1545);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1016, 1546);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1017, 1467);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1018, 1468);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1019, 1469);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1020, 1470);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1021, 1471);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1720, 1848);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1721, 1849);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1750, 1985);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1751, 1986);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1823, 17);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1032, 1477);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1033, 1478);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1034, 1479);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1035, 1480);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1036, 1481);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1037, 1482);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1722, 1853);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1044, 1876);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1045, 1877);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1723, 1704);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1716, 1843);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1151, 1412);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1152, 1413);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1153, 1414);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1046, 1550);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1047, 1551);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1048, 1552);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1049, 1486);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1050, 1487);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1051, 1488);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1830, 2085);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1416, 1573);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1417, 1482);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1418, 1574);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1271, 1528);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1272, 1529);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1273, 1530);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1274, 1531);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1275, 1532);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1281, 1539);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1282, 1540);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1283, 1541);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1294, 1489);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1831, 2086);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (335, 1920);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1292, 1232);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (336, 1920);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1801, 2057);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1348, 1581);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1349, 1580);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1350, 1579);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1356, 1582);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1357, 1583);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1358, 1584);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1106, 1585);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1107, 1586);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1108, 1587);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1421, 1611);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1391, 1322);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1391, 1795);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1392, 1323);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1392, 1796);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1393, 1324);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1393, 1797);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1394, 1325);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1394, 1798);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1395, 1326);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1395, 1799);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1396, 1327);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1396, 1800);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1397, 1328);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1397, 1801);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1398, 1329);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1398, 1802);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1399, 1330);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1399, 1803);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1400, 1331);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1400, 1804);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1690, 1327);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1691, 1328);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1692, 1329);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1693, 1330);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1694, 1331);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1685, 1322);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1686, 1323);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1687, 1324);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1688, 1325);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1689, 1326);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1703, 1819);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1704, 1820);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1402, 1836);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1717, 1844);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1157, 1056);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1695, 1836);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (899, 1026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (951, 2123);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1705, 1821);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1706, 1822);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1217, 1132);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1707, 1823);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1383, 1790);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1708, 1824);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1709, 1825);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1710, 1826);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1711, 1827);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1712, 1828);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1384, 1791);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1385, 1792);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1386, 1793);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1387, 1794);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1154, 1415);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1155, 1777);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1111, 2105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (910, 1875);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (911, 1876);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (912, 1877);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (618, 1883);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1128, 1886);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1128, 2105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1742, 1946);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1743, 1947);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1744, 1948);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1745, 1949);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (962, 2115);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (940, 1499);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (941, 1500);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (942, 1500);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (943, 1501);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (944, 1504);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (945, 1504);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (946, 1505);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (947, 1505);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (948, 1506);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (78, 104);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (78, 118);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (78, 2109);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1836, 2095);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1837, 2096);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1838, 2094);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (962, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (953, 2124);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (954, 2125);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (956, 2126);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (957, 2127);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (67, 116);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (67, 119);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (67, 2110);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (79, 117);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (79, 120);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (79, 2111);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (321, 493);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (321, 494);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (321, 2112);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (723, 890);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (723, 891);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (723, 2113);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1846, 2128);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1847, 2129);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1848, 2130);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1849, 2131);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1925, 2246);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1872, 2152);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1873, 2154);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1897, 2197);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1898, 2198);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1899, 2199);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1900, 2200);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1901, 2201);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1902, 2202);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1903, 2203);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1914, 2214);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1915, 2215);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1916, 2216);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1883, 2173);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1917, 2217);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1918, 2218);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1919, 2219);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1927, 2248);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1928, 2249);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1929, 2250);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1933, 2228);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1934, 2229);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1935, 2230);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1940, 2241);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1938, 2239);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1939, 2240);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1941, 2242);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1943, 176);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1945, 491);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1946, 518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1944, 181);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1947, 2243);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1948, 2244);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1926, 2247);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1989, 2309);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2020, 2327);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2021, 2328);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2022, 2329);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2023, 2330);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2024, 2341);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2025, 2342);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2042, 2350);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 144);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 159);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 161);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2047, 2357);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2049, 2359);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2404, 2794);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2091, 2396);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2053, 2364);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2054, 2365);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2055, 2366);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2056, 2367);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2057, 2368);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2058, 2369);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2059, 2370);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2060, 2371);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2061, 2372);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2062, 2373);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2092, 2397);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2093, 2398);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2093, 2573);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2094, 2400);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2095, 2401);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2096, 2402);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2097, 2403);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2098, 2404);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2099, 2405);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2100, 2406);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2101, 2407);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2102, 2408);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2103, 2409);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2104, 2410);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2105, 2412);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2106, 2413);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2107, 2414);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2405, 2795);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2109, 2416);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2110, 2417);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2111, 2418);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2112, 2419);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2113, 2420);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2114, 2421);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2115, 2422);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2185, 2505);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2186, 2506);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2187, 2507);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2446, 2873);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2189, 2512);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2190, 2513);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2191, 2515);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2192, 2516);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2193, 2517);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2194, 2518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2195, 2519);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2196, 2520);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2197, 2521);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2198, 2524);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2447, 2874);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2448, 2876);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2449, 2877);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2463, 1042);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2464, 1041);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2465, 1044);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2466, 1043);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2467, 2885);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2468, 2886);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2469, 2887);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2470, 2888);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2471, 2889);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2481, 2904);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2516, 2940);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2201, 2539);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2208, 2540);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2209, 2541);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2210, 2542);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2211, 2543);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2517, 2941);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2518, 2942);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2080, 2389);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2081, 2390);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2227, 2563);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2228, 2564);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2229, 2565);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2230, 2566);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2231, 2567);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2232, 2568);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2233, 2569);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2234, 2570);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2235, 2571);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2236, 2572);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2222, 2556);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2223, 2557);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2224, 2558);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2225, 2559);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2226, 2561);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2248, 2592);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2249, 2593);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2250, 2594);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2251, 2595);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2252, 2596);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2253, 2597);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2254, 2598);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2255, 2599);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2265, 2618);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2266, 2619);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2267, 2620);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2268, 2621);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2269, 2622);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2270, 2623);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2271, 2624);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2272, 2625);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2273, 2626);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2274, 2627);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2275, 2628);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2276, 2629);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2277, 2630);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2278, 2631);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2279, 2632);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2280, 2633);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2281, 2634);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2282, 2635);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2283, 2636);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2284, 2637);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2285, 2638);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2286, 2639);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2287, 2642);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2288, 2643);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2289, 2645);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2290, 2646);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2291, 2647);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2292, 2651);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2293, 2652);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2294, 2653);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2295, 2654);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2296, 2655);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2297, 2656);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2298, 2657);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2299, 2658);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2300, 2659);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2301, 2660);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2302, 2661);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2303, 2662);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2304, 2663);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2305, 2664);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2306, 2665);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2307, 2666);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2308, 2667);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2309, 2668);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2310, 2669);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2311, 2670);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2312, 2671);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2313, 2672);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (413, 574);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (414, 575);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (415, 576);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (416, 577);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (417, 578);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (418, 579);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (419, 580);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (420, 581);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (421, 582);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (422, 583);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (423, 584);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (424, 585);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (425, 586);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (426, 587);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (427, 588);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (428, 589);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (429, 590);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (430, 591);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (431, 592);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (432, 593);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (433, 611);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (439, 612);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (440, 613);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (441, 624);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (442, 625);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (443, 626);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (445, 628);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (446, 629);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (407, 563);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (407, 564);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (407, 565);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (492, 566);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (493, 567);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (494, 648);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (495, 649);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (496, 650);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (497, 651);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (498, 652);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (499, 653);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (500, 654);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (501, 655);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (502, 656);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (503, 657);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (504, 658);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (505, 659);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (506, 660);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (507, 661);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (508, 662);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (509, 663);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (510, 664);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (511, 665);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (512, 666);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (513, 667);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (514, 668);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (515, 669);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (516, 670);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (517, 671);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (526, 687);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (525, 688);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (391, 535);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (560, 724);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (561, 719);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (562, 720);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (563, 721);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (564, 722);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (571, 730);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (624, 791);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (625, 792);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (626, 793);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (627, 794);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (628, 795);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (629, 796);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (630, 814);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (631, 797);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (632, 798);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (633, 799);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (634, 800);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (635, 801);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (636, 802);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (637, 803);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (638, 804);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (639, 805);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (640, 806);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (641, 807);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (642, 808);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (643, 809);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (644, 810);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (645, 811);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (646, 812);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (647, 813);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (732, 899);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (731, 900);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (731, 901);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (731, 902);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (730, 903);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (234, 297);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (782, 963);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (761, 343);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (761, 345);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (761, 344);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (762, 93);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (762, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (762, 99);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (762, 105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (762, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (762, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (783, 964);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (784, 965);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (785, 966);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (786, 967);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (787, 968);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (788, 969);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (789, 970);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (790, 971);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (791, 972);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (792, 973);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (793, 974);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (794, 975);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (795, 976);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1937, 2232);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1410, 867);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1410, 874);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1410, 885);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1410, 879);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1410, 172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1410, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1411, 1441);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1411, 1442);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1411, 1443);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (619, 1892);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1702, 1450);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1952, 1007);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (174, 242);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1971, 2291);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2031, 475);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2032, 476);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2033, 477);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2038, 2347);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2040, 358);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2382, 2763);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2383, 2764);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1420, 1576);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1422, 1612);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1423, 1613);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1424, 1614);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1425, 1615);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1426, 1616);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1427, 1617);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1428, 1618);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1429, 1619);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1430, 1620);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1431, 1621);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1432, 1622);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1433, 1623);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1434, 1624);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1435, 1625);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1436, 1626);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1437, 1627);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1438, 1628);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1439, 1629);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1440, 1630);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1441, 1631);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1442, 1632);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1443, 1633);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1444, 1634);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1445, 1635);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1446, 1636);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1447, 1637);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1448, 1638);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1449, 1639);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1450, 1640);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1451, 1641);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1452, 1642);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1453, 1643);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1454, 1644);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1455, 1645);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1456, 1646);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1457, 1647);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1458, 1648);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1459, 1649);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1460, 1650);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1461, 1651);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1462, 1652);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1463, 1653);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1464, 1654);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1465, 1655);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1466, 1656);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1467, 1657);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1468, 1658);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1469, 1659);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1470, 1660);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1471, 1661);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1472, 1662);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1473, 1663);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1474, 1664);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1475, 1665);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1476, 1666);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1477, 1667);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1478, 1668);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1479, 1669);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1480, 1670);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1481, 1671);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1482, 1672);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1484, 1674);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1483, 1673);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1485, 1675);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1486, 1676);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1487, 1677);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1488, 1678);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1489, 1679);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1490, 1680);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1491, 1681);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1492, 1682);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1493, 1683);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1494, 1684);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1495, 1685);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1496, 1686);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1497, 1687);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1498, 1688);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1499, 1689);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1500, 1690);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1501, 1691);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1502, 1692);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1503, 1693);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1504, 1694);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1505, 1695);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1506, 1696);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1507, 1697);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1508, 1698);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1509, 1699);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1510, 1700);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1511, 1701);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1512, 1702);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1513, 1703);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1514, 1705);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1515, 1706);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1516, 1707);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1517, 1708);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1518, 1709);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1519, 1710);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1520, 1711);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1521, 1712);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1522, 1713);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1523, 1714);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1524, 1715);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1525, 1716);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1526, 1717);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1527, 1718);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1528, 1719);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1529, 1720);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1530, 1721);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1531, 1722);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1532, 1723);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1533, 1724);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1534, 1725);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1535, 1726);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1536, 1727);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1537, 1728);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1538, 1729);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1539, 1730);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1631, 1731);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1632, 1732);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1633, 1733);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1634, 1734);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1635, 1735);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1636, 1736);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1637, 1737);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1638, 1738);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1639, 1739);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1640, 1740);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1641, 1741);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1642, 1742);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1643, 1743);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1644, 1744);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1645, 1745);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1646, 1746);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1647, 1747);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1648, 1748);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1649, 1749);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1650, 1750);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1651, 1751);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1652, 1752);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1653, 1753);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1654, 1754);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1655, 1755);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1656, 1756);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1657, 1757);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1658, 1758);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1659, 1759);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1660, 1760);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1661, 1761);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1662, 1762);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1663, 1763);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1664, 1764);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1665, 1765);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1666, 1766);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1667, 1767);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1668, 1768);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1669, 1769);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1670, 1770);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1671, 1771);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1672, 1772);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1673, 1773);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1674, 1774);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1675, 1775);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1676, 1776);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1248, 1602);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1249, 1603);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1250, 1604);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1256, 1605);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1257, 1606);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1258, 1607);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2384, 2765);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2385, 2766);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1027, 1837);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1028, 1838);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1029, 1839);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1030, 1840);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1031, 1841);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2386, 2767);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2387, 2768);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2388, 2769);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2389, 2770);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2390, 2771);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2391, 2772);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2392, 2773);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2393, 2774);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2394, 2775);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2395, 2776);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2396, 2777);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2406, 2796);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2407, 2797);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2408, 2798);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1103, 1371);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2079, 2388);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1104, 1372);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1105, 1373);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2409, 2809);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2410, 2810);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2411, 2811);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2412, 2812);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2415, 2817);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2416, 2818);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2417, 2819);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2418, 2820);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2431, 2837);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2452, 2881);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2480, 2903);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2521, 2946);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2523, 2948);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2524, 2949);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2519, 2944);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2520, 2945);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2522, 2947);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2525, 2950);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2526, 2951);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2527, 2952);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2528, 2953);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2529, 2954);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2530, 2955);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2531, 2956);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2532, 2957);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2533, 2958);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2534, 2959);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2535, 2962);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2536, 2963);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2537, 2964);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2538, 2965);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2539, 2967);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2547, 2977);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2548, 2978);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2549, 2979);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2550, 2980);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2545, 2975);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2551, 2986);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2552, 2987);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2599, 3048);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2600, 3049);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2601, 3050);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2557, 2994);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2557, 2995);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2559, 2998);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2559, 2999);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2559, 2997);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2560, 3001);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2561, 3002);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2562, 3003);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2563, 3004);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2563, 3005);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2563, 2997);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2565, 3007);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2565, 3008);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2565, 2997);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2566, 3010);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2568, 3011);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2568, 3012);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2568, 2997);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2569, 3014);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2571, 3015);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2571, 3016);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2571, 3017);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2571, 3018);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2572, 3019);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2572, 3020);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2573, 3021);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2573, 3022);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2574, 3023);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2574, 3024);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2575, 3025);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2575, 3026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2577, 3027);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2578, 3029);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2578, 3030);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2578, 3031);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2579, 3033);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2581, 474);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2581, 1040);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2581, 1041);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2581, 1042);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2581, 1043);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2581, 1044);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2582, 3035);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2582, 3036);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2582, 2997);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2584, 3038);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2584, 3039);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2584, 2997);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2602, 3051);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2592, 103);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2593, 131);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2594, 12);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2595, 130);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2586, 3041);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2603, 3052);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2604, 3053);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2605, 3054);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2606, 3055);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2607, 3056);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2608, 3057);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2609, 3058);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2647, 3061);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2648, 3062);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2646, 3066);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2617, 3067);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2618, 3068);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2619, 3069);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2636, 241);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2636, 244);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2620, 3099);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2624, 3100);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2626, 3090);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2627, 3091);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2788, 3235);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (547, 711);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (548, 712);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (650, 817);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (861, 1052);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1022, 1026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1055, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1782, 2011);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1790, 2041);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1790, 2025);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1791, 2042);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1068, 1533);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1069, 1533);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1070, 1534);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1071, 1534);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1072, 1535);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1079, 1875);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1080, 1875);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1081, 1876);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1082, 1876);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1083, 1877);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1098, 1884);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1099, 1885);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1100, 1886);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1101, 1887);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1102, 1888);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1109, 1889);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1110, 1889);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1870, 2161);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1911, 2211);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1113, 1891);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1124, 1884);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1125, 1884);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1126, 1885);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1875, 2165);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1912, 2212);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1125, 1889);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1126, 1889);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1913, 2213);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1936, 2231);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1791, 2026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1792, 2027);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1793, 2028);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1806, 2062);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1807, 2063);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1966, 2285);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1972, 2292);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1787, 488);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1973, 2293);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2043, 96);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2063, 2361);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2238, 2574);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1789, 488);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1808, 2064);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1809, 2065);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1795, 2015);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1795, 2053);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1796, 2016);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1796, 2054);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1797, 2017);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1810, 2066);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1798, 2012);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1811, 2067);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1802, 2018);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2239, 2575);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1812, 2068);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1813, 2069);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1814, 2070);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1815, 2071);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1816, 2072);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1817, 2073);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1818, 2074);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1819, 2075);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1820, 2076);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1821, 2077);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1822, 2078);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2240, 2576);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2241, 2577);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2242, 2578);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2243, 2579);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2244, 2580);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2245, 2581);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2246, 2582);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2086, 1598);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2087, 1598);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2088, 1599);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2413, 2815);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2414, 2816);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (180, 2814);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2432, 2838);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2453, 2882);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2540, 2968);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2597, 3046);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2598, 3047);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2789, 3236);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (120, 176);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2790, 3237);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2616, 3065);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2739, 3074);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2740, 3075);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2741, 3076);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2742, 3077);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2744, 3079);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2743, 3078);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2746, 3081);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2745, 3080);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2631, 3104);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2632, 3095);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2633, 3096);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2634, 3106);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2635, 3137);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2637, 3108);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2638, 3110);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2639, 3111);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2640, 3112);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2641, 3113);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2635, 3133);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2747, 3105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2642, 3132);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2642, 3138);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2749, 180);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2750, 487);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2751, 1301);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2752, 679);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2754, 242);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2755, 238);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2756, 489);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2757, 197);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2760, 195);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2761, 183);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2762, 176);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2763, 491);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2764, 182);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2765, 488);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2644, 3135);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2644, 3139);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2791, 3238);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2624, 3124);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2796, 3248);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2797, 3249);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2798, 3250);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2799, 3251);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2800, 3252);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2801, 3253);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2802, 3254);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2803, 3255);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2804, 3256);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2805, 3257);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2806, 3258);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2807, 3259);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2808, 3260);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2809, 3261);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2810, 3262);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2811, 3263);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2812, 3264);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2813, 3265);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2814, 3266);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2815, 3267);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2816, 3268);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2817, 3269);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2818, 3270);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2819, 3271);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2820, 3272);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2821, 3273);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2822, 3274);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2823, 3275);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2824, 3276);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2825, 3277);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2826, 3278);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2827, 3279);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2828, 3280);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2829, 3281);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2830, 3282);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2831, 3283);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2832, 3284);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2833, 3285);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2834, 3286);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2835, 3287);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2836, 3288);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2837, 3289);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2838, 3290);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2839, 3291);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2840, 3292);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2841, 3293);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2842, 3294);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2843, 3295);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2844, 3296);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2845, 3297);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2846, 3298);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2847, 3299);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2861, 3300);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2862, 3301);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2863, 3302);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2864, 3303);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2865, 3304);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2866, 3305);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2867, 3306);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2868, 3307);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2869, 3308);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2870, 3309);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2871, 3310);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2872, 3311);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2873, 3312);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2848, 3313);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2849, 3314);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2850, 3315);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2851, 3316);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2852, 3317);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2853, 3318);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2854, 3319);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2855, 3320);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2856, 3321);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2857, 3322);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2858, 3323);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2859, 3324);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2860, 3325);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2874, 3326);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2875, 3327);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2876, 3328);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2877, 3329);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2878, 3330);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2879, 3331);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2880, 3332);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2881, 3333);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2882, 3334);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2883, 3335);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2884, 3336);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2885, 3337);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2886, 3338);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2888, 3339);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2889, 3340);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2887, 3341);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2890, 3342);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2891, 3343);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2892, 3344);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2893, 3345);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2894, 3346);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2895, 3347);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2896, 3348);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2897, 3349);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2898, 3350);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2899, 3351);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2900, 3352);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2901, 3353);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2902, 3354);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2903, 3355);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2904, 3356);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2905, 3357);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2906, 3358);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2907, 3359);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2908, 3360);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2909, 3361);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2910, 3362);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2911, 3363);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2912, 3364);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2913, 3365);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2914, 3366);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2916, 3368);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2922, 3370);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2923, 3372);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2926, 3374);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2927, 3376);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2918, 3378);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2920, 3380);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2924, 3382);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2925, 3384);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2928, 3386);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2929, 3388);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2915, 3390);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2917, 3391);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2903, 3392);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2904, 3393);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2905, 3394);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2906, 3395);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2907, 3396);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2908, 3397);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2909, 3398);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2912, 3399);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2913, 3400);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2936, 3427);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2937, 3428);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2947, 3438);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2947, 3439);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2938, 3429);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2939, 3430);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2940, 3431);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2941, 3432);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2942, 3433);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2943, 3434);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2944, 3435);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2945, 3436);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2946, 3437);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2930, 3446);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2931, 3446);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2932, 3446);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2933, 3446);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2954, 3447);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (602, 770);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (603, 771);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (604, 772);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (605, 767);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (607, 768);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (609, 769);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (606, 773);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (608, 774);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (610, 775);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (760, 475);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (760, 476);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (760, 477);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (839, 1024);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1119, 1555);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1120, 1556);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1121, 1557);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1122, 1558);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1123, 1559);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (247, 1037);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1412, 216);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1412, 1850);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1413, 217);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1413, 1852);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1414, 218);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1414, 1842);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1785, 2013);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1785, 2051);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1794, 2014);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1794, 2052);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1949, 2251);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1950, 2252);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1951, 2253);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1976, 2296);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1977, 2297);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1978, 2298);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1979, 2299);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1980, 2300);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1981, 2301);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1982, 2302);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1983, 2303);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1984, 2304);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1985, 2305);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1986, 2306);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1987, 2307);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1990, 2310);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1991, 2311);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1992, 2312);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1993, 2313);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1994, 2314);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1995, 2315);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1996, 2316);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1997, 2317);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1998, 2318);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1999, 2319);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2000, 2320);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2001, 2321);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2002, 2322);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2003, 2323);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2005, 864);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2610, 3059);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2611, 3060);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2083, 2392);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2083, 2554);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2084, 2393);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2084, 2554);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2085, 2394);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2085, 2554);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2134, 2436);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2134, 2555);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2135, 2437);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2135, 2555);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2136, 2438);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2136, 2555);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2120, 2648);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2121, 2649);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2122, 2650);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2950, 3440);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2951, 3441);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2948, 3442);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2949, 3443);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2952, 3444);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2953, 3445);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2955, 3448);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (653, 831);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (654, 832);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (655, 818);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (656, 819);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (657, 820);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (658, 821);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (659, 822);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (660, 823);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (661, 824);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (662, 825);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (663, 826);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (664, 827);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (665, 828);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (729, 331);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (729, 333);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (729, 332);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (741, 912);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (742, 913);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (743, 914);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (744, 915);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (746, 917);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (745, 916);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (747, 918);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (748, 919);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (750, 920);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (751, 921);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (852, 184);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (298, 474);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (298, 1043);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (298, 1044);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (812, 992);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1832, 103);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1833, 131);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1834, 12);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1835, 130);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1295, 1490);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1296, 1491);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1297, 1492);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1298, 1493);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1308, 1814);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1309, 1815);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1310, 1816);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1311, 1817);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2090, 2719);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2108, 2720);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2375, 2440);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2372, 2440);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2373, 2440);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2374, 2440);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2376, 2440);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2630, 3094);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2629, 3131);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2629, 3140);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (733, 905);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (734, 906);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (735, 907);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (736, 908);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (737, 50);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (738, 50);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (739, 909);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (740, 910);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (860, 1051);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (886, 1066);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (893, 1087);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (929, 1112);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (930, 1113);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (931, 1114);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (753, 76);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (754, 922);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (755, 923);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (756, 924);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (757, 925);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (796, 977);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (829, 1014);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (830, 1015);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (831, 1016);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (832, 1017);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (833, 1018);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (834, 1019);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (835, 1020);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (836, 1021);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (837, 1022);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (838, 1023);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (932, 1115);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (933, 1116);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1718, 1846);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1719, 1845);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1112, 2105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1788, 2093);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1786, 2092);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1850, 2132);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1851, 2133);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1852, 2134);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1853, 2135);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1854, 2136);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1855, 2137);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1856, 2138);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1857, 2139);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1858, 2140);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1859, 2141);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1860, 2142);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1861, 2143);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1862, 2144);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1863, 2145);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1864, 2146);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1865, 2147);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1866, 2148);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1867, 2149);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1868, 2150);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1869, 2151);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1874, 2162);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1876, 2166);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1877, 2167);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1878, 2168);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1038, 1165);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1039, 1166);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1040, 1167);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1041, 1168);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1042, 1169);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1052, 1170);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1053, 1171);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1054, 1172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1879, 2169);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1880, 2170);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1881, 2171);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1882, 2172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1930, 2225);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1060, 1302);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1061, 1303);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1062, 1304);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1063, 1305);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1064, 1306);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1065, 1332);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1066, 1333);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1067, 1334);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1884, 2174);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1885, 2175);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1886, 2176);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1887, 2177);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1888, 2178);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1073, 1340);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1074, 1341);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1075, 1342);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1076, 1343);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1077, 1344);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1078, 1345);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1889, 2179);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1890, 2180);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1891, 2181);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1892, 2182);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1893, 2183);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1084, 1351);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1085, 1352);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1086, 1353);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1087, 1354);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1088, 1355);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1089, 1356);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1090, 1357);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1091, 1358);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1092, 1359);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1093, 1361);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1094, 1362);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1095, 1363);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1096, 1364);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1097, 1365);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1894, 2184);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1895, 2185);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1896, 2186);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1931, 2226);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1932, 2227);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2460, 2884);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2461, 2884);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (711, 875);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1968, 2288);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1969, 2289);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1970, 2290);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2119, 2423);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2176, 2495);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1114, 1379);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1115, 1380);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1116, 1381);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1117, 1382);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1118, 1383);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2650, 3143);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2651, 3144);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2123, 2430);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2124, 2431);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2125, 2432);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2126, 2433);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2127, 2434);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2128, 2435);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2652, 3145);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2653, 3146);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1129, 1394);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1130, 1395);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1131, 1396);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1132, 1397);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1133, 1398);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1134, 1399);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1135, 1400);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1136, 1401);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1137, 1402);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1143, 1403);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1144, 1404);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1145, 1405);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1146, 1406);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1147, 1407);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1148, 1408);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1149, 1409);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1150, 1410);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2654, 3147);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2137, 2441);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2139, 2442);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2140, 2443);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2141, 2444);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1156, 1055);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2142, 2445);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1158, 1057);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1159, 1058);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1160, 1059);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1161, 1063);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1162, 1064);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1163, 1065);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1167, 1068);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1168, 1069);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1169, 1070);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1170, 1071);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1171, 1072);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1172, 1077);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1173, 1078);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1174, 1079);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1175, 1080);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1176, 1081);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1182, 1083);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1183, 1084);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1184, 1085);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1185, 1086);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1186, 1088);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1187, 1089);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1188, 1090);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1189, 1091);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1193, 1092);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1194, 1093);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1195, 1094);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1196, 1095);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1197, 1096);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1198, 1097);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1199, 1098);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1200, 1099);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1201, 1100);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1202, 1101);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1203, 1102);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1204, 1103);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1205, 1104);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1206, 1105);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1207, 1106);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1208, 1107);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1209, 1108);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1210, 1109);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1211, 1110);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1212, 1111);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2143, 2446);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2144, 2449);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2145, 2449);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2146, 2450);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2148, 2451);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2149, 2452);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2150, 2453);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2151, 2454);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2152, 2455);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2153, 2456);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2154, 2456);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2155, 2457);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2156, 2457);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2157, 2458);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (177, 2462);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2160, 2464);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2162, 2465);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2163, 2466);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2164, 2467);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2165, 2469);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2675, 3148);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2169, 2488);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2170, 2489);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2171, 2490);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2172, 2491);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2173, 2492);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2174, 2493);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2175, 2494);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2711, 3149);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2177, 2496);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2178, 2497);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2179, 2498);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2180, 2499);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2182, 2501);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2181, 2500);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2183, 2502);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2184, 2504);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2712, 3150);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2713, 3151);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2714, 3152);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2723, 3153);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2724, 3154);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2725, 3155);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2726, 3156);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2676, 3157);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2677, 3158);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2678, 3159);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2679, 3160);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2680, 3161);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2681, 3162);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2682, 3163);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2715, 3164);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2716, 3165);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2717, 3166);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2718, 3167);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2683, 3168);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2684, 3169);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2685, 3170);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2686, 3171);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2687, 3172);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2688, 3173);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2689, 3174);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2719, 3175);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2720, 3176);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2721, 3177);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2722, 3178);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2690, 3179);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2691, 3180);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2692, 3181);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2693, 3182);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2694, 3183);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2695, 3184);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2696, 3185);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2727, 3186);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2728, 3187);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2729, 3188);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2730, 3189);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2697, 3190);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2698, 3191);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2699, 3192);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2700, 3193);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2701, 3194);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2702, 3195);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2703, 3196);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2731, 3197);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2732, 3198);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2733, 3199);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2734, 3200);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2704, 3201);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2705, 3202);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2706, 3203);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2707, 3204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2708, 3205);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2709, 3206);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2710, 3207);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2735, 3208);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2736, 3209);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2737, 3210);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2738, 3211);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2784, 3212);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2785, 3213);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2766, 3214);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2767, 3215);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2768, 3216);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2769, 3217);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2770, 3219);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2771, 3220);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2772, 3221);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2773, 3218);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2774, 3222);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2775, 3223);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2776, 3224);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2777, 3225);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2778, 3226);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2779, 3227);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2780, 3228);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2781, 3229);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2782, 3230);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2783, 3231);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (826, 992);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1419, 1575);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (924, 1813);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (925, 1814);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (926, 1815);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (927, 1816);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (928, 1817);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1307, 1813);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1803, 2059);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1799, 2058);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1805, 2061);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1359, 1285);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1347, 1279);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1736, 1941);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1737, 1942);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1734, 1923);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1735, 1922);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1360, 1286);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1361, 1287);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1920, 2220);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1921, 2221);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1922, 2222);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1923, 2223);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1924, 2224);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1955, 905);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1956, 906);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1957, 907);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1958, 908);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1959, 909);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1960, 910);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1967, 2287);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1974, 2294);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1975, 2295);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2064, 2375);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2065, 2376);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2066, 2377);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2067, 2378);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2068, 2379);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2069, 2380);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2070, 2381);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2071, 2382);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2072, 2383);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2073, 2384);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2074, 2385);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2075, 2386);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2076, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2077, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2078, 1519);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2459, 2883);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2203, 2544);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2203, 2545);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2203, 2788);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2921, 3402);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2082, 2391);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2921, 3403);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2474, 2897);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2475, 2898);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2476, 2899);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2477, 2900);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2478, 2901);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2479, 2902);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2919, 3404);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2919, 3405);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (884, 1553);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (885, 1554);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (934, 1494);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (935, 1494);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (936, 1495);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (937, 1495);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (938, 1496);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (939, 1499);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (990, 1509);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (991, 1510);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2615, 3086);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2051, 2362);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2052, 2363);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2219, 2549);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2220, 2550);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2221, 2551);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2214, 2721);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (949, 1509);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1804, 2060);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2419, 2821);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (952, 1512);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2420, 2822);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2625, 3088);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (955, 1515);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2621, 3071);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2199, 2732);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2204, 2733);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2205, 2734);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (992, 1512);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (993, 1513);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (994, 1515);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (995, 1516);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1713, 1830);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2348, 2737);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2349, 2738);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2350, 2739);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2351, 2740);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2352, 2741);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2353, 2742);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2354, 2743);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2355, 2744);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2356, 2745);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2357, 2746);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2200, 2757);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2206, 2758);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2207, 2759);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2362, 2747);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2363, 2748);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2364, 2749);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2365, 2750);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2366, 2751);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2367, 2752);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2368, 2753);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2369, 2754);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2370, 2755);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2371, 2756);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2202, 2760);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2212, 2761);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2213, 2762);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2622, 3072);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2378, 2722);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2379, 2723);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2623, 3073);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2930, 3409);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2931, 3422);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2932, 3423);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2643, 3134);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2628, 3136);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2933, 3424);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1904, 2204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (964, 1523);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1905, 2205);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1906, 2206);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1907, 2207);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (969, 1528);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (970, 1529);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (971, 1529);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (972, 1530);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1908, 2208);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1909, 2209);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1910, 2210);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1871, 2245);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (980, 1539);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (981, 1540);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (900, 1026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (901, 1878);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (902, 1878);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (903, 1474);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1023, 1026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1024, 1878);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1025, 1878);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1026, 1474);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1233, 1026);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1234, 1878);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1235, 1878);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1236, 1474);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1056, 1518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1057, 1519);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1058, 1519);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1059, 1520);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1839, 2097);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1839, 2098);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1839, 2012);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2004, 2325);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2041, 2349);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2377, 2722);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2380, 2723);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2381, 2724);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1772, 1963);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1781, 2010);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (1797, 2055);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2397, 2723);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2398, 2723);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2399, 2724);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2400, 2724);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2401, 2725);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2450, 2878);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2451, 2880);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2482, 2905);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2483, 2906);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2484, 2907);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2485, 2908);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2486, 2909);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2487, 2910);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2488, 2911);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2489, 2912);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2490, 2913);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2491, 2914);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2492, 2915);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2493, 2916);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2494, 2917);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2495, 2918);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2496, 2919);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2497, 2920);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2498, 2921);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2499, 2922);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2500, 2923);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2501, 2924);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2502, 2925);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2503, 2926);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2504, 2927);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2505, 2928);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2506, 2929);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2507, 2930);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2508, 2931);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2510, 2933);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2509, 2932);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2511, 2934);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2512, 2935);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2596, 3045);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2314, 2673);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2315, 2674);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2316, 2675);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2317, 2676);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2318, 2677);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2328, 2687);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2329, 2688);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2330, 2689);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2331, 2690);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2332, 2691);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2333, 2692);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2334, 2693);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2335, 2694);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2336, 2695);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2321, 2679);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2319, 2678);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2326, 2684);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2324, 2682);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2323, 2681);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2325, 2683);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2320, 2678);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2327, 2685);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2322, 2680);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2337, 2696);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2338, 2697);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2339, 2698);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2340, 2699);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2341, 2700);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2342, 2701);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2343, 2702);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2344, 2703);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2345, 2704);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2158, 2705);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2159, 2706);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2166, 2470);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2167, 2471);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2168, 2472);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2346, 2707);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2347, 2708);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2129, 2709);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2130, 2709);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2131, 2710);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2132, 2710);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2133, 2711);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2256, 2600);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2257, 2601);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2258, 2602);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2259, 2603);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2260, 2604);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2261, 2605);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2262, 2606);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2263, 2607);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2264, 2608);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2215, 2721);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2216, 2722);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2217, 2722);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2218, 2723);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (58, 2778);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (347, 2779);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (348, 2780);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (349, 2781);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (350, 2782);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (57, 2783);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (355, 2784);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (356, 2785);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (357, 2786);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (358, 2787);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2188, 2511);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2541, 2971);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2542, 2972);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2543, 2973);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2544, 2974);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2553, 2990);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2546, 2976);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2555, 2993);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2590, 3043);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2649, 3142);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2792, 3239);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2793, 3240);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2794, 3241);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2795, 3242);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2956, 3449);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2957, 3450);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2958, 3451);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2972, 3465);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2971, 3464);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2987, 3494);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2989, 3497);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2990, 3500);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2991, 3503);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2992, 3506);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2993, 3509);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2994, 3512);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2995, 3515);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2996, 3518);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2997, 3521);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2998, 3524);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2988, 3525);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2999, 180);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3000, 176);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3011, 3086);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3012, 3528);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3013, 3529);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3016, 3530);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3017, 3531);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3024, 3532);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3025, 3533);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3014, 3534);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3015, 3535);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3018, 3536);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3019, 3537);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3020, 3538);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3021, 3539);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3022, 3540);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3023, 3541);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3026, 3542);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3027, 3543);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3028, 3544);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3032, 3545);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3029, 3546);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3033, 3547);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3030, 3548);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3034, 3549);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3031, 3550);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3035, 3551);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3036, 3552);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3036, 3553);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3037, 3554);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3037, 3555);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3038, 3556);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3038, 3557);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3039, 3558);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3039, 3559);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3040, 3560);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3040, 3561);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3041, 3562);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3041, 3563);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3042, 3564);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3042, 3565);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3043, 3566);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3043, 3567);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3044, 3568);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3044, 3569);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3046, 3570);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3046, 3571);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3048, 3572);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3048, 3573);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3045, 3574);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3047, 3576);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3049, 3580);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3050, 3582);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3051, 3586);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3052, 3588);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3077, 3592);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3086, 3654);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3087, 3655);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3088, 3656);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3089, 3657);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3090, 3658);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3091, 3659);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3092, 92);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3092, 204);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3092, 3660);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3093, 3672);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3093, 3673);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3096, 3674);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3097, 3675);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3098, 3676);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3099, 3677);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3109, 3689);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3110, 3690);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3111, 3691);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3112, 3692);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3113, 3693);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3114, 3694);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3115, 3695);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3116, 3696);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3117, 3697);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3118, 3698);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3119, 3699);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3120, 3700);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3121, 3701);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3122, 3702);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3123, 3703);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3124, 3704);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3125, 3705);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3126, 3706);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3127, 3707);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3128, 3708);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3129, 3709);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3130, 3710);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3131, 3711);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3132, 3712);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3133, 3773);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3133, 3774);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3133, 3775);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3134, 3776);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3134, 3777);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3134, 3778);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3135, 3779);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3135, 3780);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3135, 3781);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3136, 3782);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3136, 3783);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3136, 3784);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3137, 3785);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3137, 3786);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3137, 3787);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3138, 3788);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3138, 3789);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3138, 3790);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3139, 3791);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3139, 3792);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3139, 3793);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3140, 3794);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3140, 3795);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3140, 3796);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3141, 3797);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3141, 3798);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3141, 3799);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3142, 3800);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3142, 3801);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3142, 3802);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3143, 3803);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3143, 3804);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3143, 3805);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3144, 3806);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3144, 3807);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3144, 3808);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2970, 3463);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2969, 3462);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2968, 3461);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2967, 3460);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2966, 3459);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2965, 3458);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2964, 3457);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2963, 3456);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2962, 3455);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2961, 3454);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2960, 3453);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2959, 3452);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2986, 3479);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2985, 3478);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2984, 3477);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2983, 3476);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2982, 3475);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2981, 3474);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2980, 3473);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2979, 3472);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2978, 3471);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2977, 3470);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2976, 3469);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2975, 3468);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2974, 3467);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (2973, 3466);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3100, 3680);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3101, 3681);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3102, 3682);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3103, 3683);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3104, 3684);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3105, 3685);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3106, 3688);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3107, 3686);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3108, 3687);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3001, 487);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3002, 491);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3003, 242);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3004, 238);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3005, 1301);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3006, 679);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3007, 195);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3008, 183);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3009, 489);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3010, 197);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3053, 3598);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3054, 3600);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3055, 3602);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3056, 3604);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3057, 3606);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3058, 3608);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3059, 3610);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3060, 3612);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3061, 3614);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3062, 3616);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3063, 3618);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3064, 3620);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3065, 3622);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3066, 3624);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3067, 3626);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3068, 3628);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3069, 3630);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3070, 3632);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3071, 3634);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3072, 3636);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3073, 3638);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3074, 3640);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3075, 3642);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3076, 3644);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3082, 3650);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3083, 3651);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3078, 3652);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3079, 3653);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3084, 3646);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3085, 3647);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3093, 3679);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (3145, 3809);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (298, 3810);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (298, 3811);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (298, 3812);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (107232, 107234);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (107233, 107235);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (107257, 107259);
GO

INSERT INTO [dbo].[DT_SkillAbnormal] ([SID], [AbnormalID]) VALUES (107258, 107260);
GO

