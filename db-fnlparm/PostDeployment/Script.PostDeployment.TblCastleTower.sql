/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblCastleTower
Date                  : 2023-10-07 09:08:50
*/


INSERT INTO [dbo].[TblCastleTower] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (1, 396022.90625, 15499.65625, 330316.78125, 430.0, 4.712389, 'r1_ob_battletower_t1_tower.r3m                    ', '2013-05-20 14:58:47.397');
GO

INSERT INTO [dbo].[TblCastleTower] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (51, 132950.984375, 22061.707031, 270658.34375, 430.0, 4.694936, 'r1_ob_battletower_t1_tower.r3m                    ', '2013-05-20 14:58:47.243');
GO

INSERT INTO [dbo].[TblCastleTower] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (101, 403784.65625, 19122.783203, 101423.71875, 430.0, 4.712389, 'r1_ob_battletower_t1_tower.r3m                    ', '2013-05-20 14:58:47.380');
GO

INSERT INTO [dbo].[TblCastleTower] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (151, 1024008.6875, 28927.615234, 521745.3125, 430.0, 1.553343, 'r1_ob_battletower_t1_tower.r3m                    ', '2013-05-20 14:58:47.600');
GO

INSERT INTO [dbo].[TblCastleTower] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (800, 838433.25, 34143.550781, 1151491.25, 159.05957, 5.51524, 'r8_Battle_castle_a_01_02.r3m                      ', '2013-05-20 14:58:47.567');
GO

