/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblQuestCondition
Date                  : 2023-10-07 09:09:29
*/


INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (2, 1, 827, 4, NULL, 155);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (42, 1, 851, 4, NULL, 156);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (43, 1, 809, 4, NULL, 157);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (44, 1, 810, 3, NULL, 158);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (45, 1, 2803, 2, NULL, 159);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (46, 1, 812, 2, NULL, 160);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (56, 1, 326, 1, NULL, 161);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (56, 1, 807, 1, NULL, 162);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (85, 1, 1479, 50, NULL, 51);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (85, 1, 1480, 20, NULL, 52);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (90, 1, 1476, 1, NULL, 58);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (93, 1, 1488, 1, NULL, 182);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (97, 1, 1486, 1, NULL, 164);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (98, 1, 1487, 1, NULL, 165);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (99, 1, 1489, 1, NULL, 166);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (116, 1, 2541, 1, NULL, 44);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (117, 1, 2542, 1, NULL, 45);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (118, 1, 2543, 1, NULL, 46);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (119, 1, 2545, 1, NULL, 47);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (120, 1, 2544, 1, NULL, 48);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (125, 1, 2722, 5, NULL, 23);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (126, 1, 2723, 5, NULL, 24);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (127, 1, 2724, 1, NULL, 25);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (128, 1, 2725, 5, NULL, 28);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (129, 1, 2726, 1, NULL, 29);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (131, 1, 2728, 5, NULL, 26);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (132, 1, 2729, 2, NULL, 27);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (134, 1, 2731, 5, NULL, 18);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (135, 1, 2732, 12, NULL, 19);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (136, 1, 2733, 5, NULL, 20);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (157, 1, 3369, 1, NULL, 49);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (158, 1, 3370, 1, NULL, 50);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (160, 1, 3458, 30, NULL, 30);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (161, 1, 3459, 40, NULL, 31);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (162, 1, 3460, 50, NULL, 32);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (163, 1, 3461, 75, NULL, 33);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (164, 1, 3462, 100, NULL, 34);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (165, 1, 3463, 125, NULL, 35);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (166, 1, 3464, 150, NULL, 36);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (167, 1, 3465, 175, NULL, 37);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (168, 1, 3466, 200, NULL, 38);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (169, 1, 3467, 250, NULL, 39);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (170, 1, 3494, 1, NULL, 40);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (171, 1, 3495, 1, NULL, 41);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (172, 1, 3496, 1, NULL, 42);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (173, 1, 3497, 1, NULL, 43);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (188, 1, 4019, 1, NULL, 72);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (189, 1, 4021, 5, NULL, 73);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (190, 1, 919, 2, NULL, 74);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (191, 1, 4022, 2, NULL, 75);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (192, 1, 4067, 3, NULL, 76);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (193, 1, 4068, 5, NULL, 77);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (194, 1, 4023, 3, NULL, 78);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (195, 1, 4024, 1, NULL, 79);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (196, 1, 920, 2, NULL, 80);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (197, 1, 4069, 5, NULL, 81);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (199, 1, 4026, 25, NULL, 83);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (201, 1, 4028, 5, NULL, 85);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (202, 1, 4029, 5, NULL, 86);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (203, 1, 4030, 1, NULL, 87);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (204, 1, 4031, 3, NULL, 88);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (205, 1, 4032, 2, NULL, 89);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (206, 1, 4033, 50, NULL, 90);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (207, 1, 4130, 1, NULL, 91);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (208, 1, 429, 1, NULL, 92);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (209, 1, 358, 1, NULL, 93);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (211, 1, 4035, 5, NULL, 95);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (213, 1, 4037, 50, NULL, 97);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (214, 1, 921, 5, NULL, 98);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (215, 1, 4038, 10, NULL, 99);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (216, 1, 4131, 5, NULL, 100);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (217, 1, 4132, 10, NULL, 101);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (218, 1, 4133, 50, NULL, 102);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (219, 1, 921, 5, NULL, 103);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (220, 1, 4129, 50, NULL, 104);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (221, 1, 4046, 5, NULL, 110);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (222, 1, 4047, 5, NULL, 111);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (224, 1, 4040, 1, NULL, 105);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (225, 1, 4041, 5, NULL, 106);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (226, 1, 4042, 1, NULL, 107);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (227, 1, 4044, 1, NULL, 108);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (229, 1, 4045, 5, NULL, 109);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (233, 1, 4049, 10, NULL, 186);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (234, 1, 4050, 15, NULL, 113);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (235, 1, 4051, 5, NULL, 114);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (236, 1, 4052, 5, NULL, 115);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (237, 1, 4053, 10, NULL, 116);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (238, 1, 4054, 5, NULL, 117);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (239, 1, 4055, 50, NULL, 118);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (241, 1, 4142, 2, NULL, 119);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (242, 1, 4143, 1, NULL, 120);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (243, 1, 4144, 2, NULL, 121);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (245, 1, 4146, 10, NULL, 122);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (246, 1, 2162, 5, NULL, 123);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (247, 1, 2160, 5, NULL, 124);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (249, 1, 4027, 50, NULL, 84);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (250, 1, 4034, 5, NULL, 94);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (251, 1, 4036, 1, NULL, 96);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (301, 0, 1633, 1, NULL, 60);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (301, 0, 1634, 1, NULL, 61);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (301, 0, 1635, 1, NULL, 62);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (302, 0, 1637, 1, NULL, 63);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (302, 0, 1638, 1, NULL, 64);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (302, 0, 1639, 1, NULL, 65);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (303, 0, 1641, 1, NULL, 66);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (303, 0, 1642, 1, NULL, 67);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (303, 0, 1643, 1, NULL, 68);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (304, 0, 1645, 1, NULL, 69);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (304, 0, 1646, 1, NULL, 70);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (304, 0, 1647, 1, NULL, 71);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (311, 0, 81, 20, NULL, 125);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (312, 0, 100, 20, NULL, 126);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (314, 0, 652, 10, NULL, 129);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (314, 0, 696, 10, NULL, 130);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (315, 0, 75, 10, NULL, 132);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (315, 0, 391, 10, NULL, 131);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (316, 0, 648, 10, NULL, 133);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (316, 0, 649, 10, NULL, 134);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (318, 0, 409, 20, NULL, 137);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (319, 0, 33, 20, NULL, 138);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (320, 0, 454, 10, NULL, 139);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (320, 0, 505, 10, NULL, 140);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (321, 0, 43, 10, NULL, 141);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (321, 0, 459, 10, NULL, 142);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (322, 0, 665, 10, NULL, 144);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (322, 0, 694, 10, NULL, 143);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (323, 0, 97, 10, NULL, 150);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (323, 0, 740, 10, NULL, 145);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (324, 0, 34, 10, NULL, 146);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (324, 0, 36, 10, NULL, 147);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (325, 0, 147, 10, NULL, 185);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (325, 0, 418, 10, NULL, 148);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (331, 0, 145, 1, NULL, 167);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (331, 0, 146, 1, NULL, 168);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (332, 0, 76, 1, NULL, 169);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (332, 0, 149, 1, NULL, 170);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (333, 0, 840, 3, NULL, 171);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (334, 0, 841, 20, NULL, 172);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (335, 0, 402, 5, NULL, 173);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (336, 0, 507, 20, NULL, 174);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (337, 0, 843, 10, NULL, 175);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (338, 1, 851, 3, NULL, 181);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (386, 1, 5685, 1, NULL, 273);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (387, 0, 477, 20, NULL, 355);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (388, 0, 473, 20, NULL, 275);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (389, 0, 103, 20, NULL, 365);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (390, 0, 112, 20, NULL, 367);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (391, 0, 735, 20, NULL, 278);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (392, 0, 736, 3, NULL, 279);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (393, 0, 111, 20, NULL, 280);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (394, 0, 397, 20, NULL, 281);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (395, 0, 110, 20, NULL, 282);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (396, 0, 737, 20, NULL, 283);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (397, 0, 45, 20, NULL, 284);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (398, 0, 742, 20, NULL, 285);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (399, 0, 659, 20, NULL, 286);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (400, 0, 1308, 20, NULL, 287);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (401, 0, 1309, 20, NULL, 288);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (402, 0, 1310, 20, NULL, 289);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (403, 1, 5686, 10, NULL, 290);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (404, 0, 1311, 20, NULL, 291);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (405, 0, 1312, 10, NULL, 292);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (406, 0, 1314, 10, NULL, 293);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (407, 0, 1318, 10, NULL, 294);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (408, 0, 1316, 10, NULL, 295);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (409, 0, 1313, 10, NULL, 296);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (410, 0, 1317, 10, NULL, 297);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (411, 0, 1320, 10, NULL, 298);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (412, 0, 1321, 10, NULL, 299);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (413, 0, 1319, 10, NULL, 300);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (414, 0, 1336, 10, NULL, 301);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (415, 0, 1334, 10, NULL, 302);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (416, 0, 1338, 10, NULL, 303);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (417, 0, 1339, 10, NULL, 304);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (418, 0, 1335, 10, NULL, 305);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (419, 0, 1337, 10, NULL, 306);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (420, 0, 1341, 10, NULL, 331);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (421, 0, 873, 20, NULL, 336);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (421, 0, 874, 20, NULL, 337);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (422, 0, 879, 20, NULL, 338);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (423, 0, 881, 20, NULL, 373);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (424, 0, 877, 10, NULL, 340);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (425, 0, 875, 20, NULL, 341);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (426, 0, 876, 20, NULL, 342);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (427, 0, 882, 20, NULL, 343);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (428, 0, 878, 10, NULL, 344);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (430, 0, 883, 20, NULL, 369);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (431, 0, 872, 20, NULL, 370);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (432, 0, 870, 20, NULL, 371);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (433, 0, 869, 20, NULL, 372);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (436, 1, 5579, 1, NULL, 234);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (437, 1, 5581, 10, NULL, 235);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (438, 1, 5582, 50, NULL, 236);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (438, 1, 5583, 1, NULL, 237);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (439, 1, 5584, 50, NULL, 238);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (440, 1, 5585, 25, NULL, 239);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (440, 1, 5586, 25, NULL, 240);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (441, 1, 5587, 25, NULL, 241);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (441, 1, 5588, 1, NULL, 242);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (442, 1, 5589, 25, NULL, 243);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (442, 1, 5590, 25, NULL, 244);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (443, 1, 5591, 1, NULL, 245);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (443, 1, 5592, 1, NULL, 246);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (444, 1, 5593, 1, NULL, 247);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (445, 1, 5595, 25, NULL, 248);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (446, 1, 5597, 10, NULL, 249);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (446, 1, 5598, 1, NULL, 250);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (447, 1, 5599, 10, NULL, 251);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (448, 1, 5600, 10, '', 252);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (449, 1, 5601, 5, NULL, 253);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (450, 1, 5605, 10, NULL, 362);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (450, 1, 5606, 10, NULL, 363);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (450, 1, 5607, 10, NULL, 364);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (451, 1, 5602, 10, NULL, 359);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (451, 1, 5603, 10, NULL, 360);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (451, 1, 5604, 10, NULL, 361);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (467, 0, 1799, 1, NULL, 345);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (468, 0, 1803, 1, NULL, 348);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (468, 0, 1804, 1, NULL, 349);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (469, 0, 1807, 1, NULL, 350);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (469, 0, 1808, 1, NULL, 351);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (470, 0, 1810, 1, NULL, 352);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (470, 0, 1811, 1, NULL, 353);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (470, 0, 1812, 1, NULL, 354);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (471, 1, 5632, 1, NULL, 262);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (472, 1, 5633, 1, NULL, 263);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (473, 1, 5634, 1, NULL, 264);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (474, 1, 5635, 1, NULL, 265);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (475, 1, 5636, 1, NULL, 266);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (476, 1, 5637, 1, NULL, 267);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (477, 1, 5638, 1, NULL, 268);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (478, 1, 5639, 1, NULL, 269);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (479, 1, 5640, 1, NULL, 270);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (480, 1, 5641, 1, NULL, 271);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (480, 1, 5642, 1, NULL, 272);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (481, 0, 145, 1, NULL, 412);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (481, 0, 146, 1, NULL, 411);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (482, 1, 5914, 100, NULL, 413);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (484, 1, 5915, 100, NULL, 415);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (485, 0, 81, 250, NULL, 453);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (485, 0, 83, 250, NULL, 454);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (486, 0, 365, 1, NULL, 457);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (486, 0, 369, 1, NULL, 456);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (486, 0, 371, 1, NULL, 458);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (486, 0, 389, 1, NULL, 455);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (487, 0, 115, 600, NULL, 459);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (488, 1, 5916, 200, NULL, 460);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (490, 1, 5917, 200, NULL, 461);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (491, 1, 5918, 200, NULL, 462);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (493, 0, 359, 1, NULL, 465);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (493, 0, 360, 1, NULL, 464);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (493, 0, 361, 1, NULL, 466);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (493, 0, 362, 1, NULL, 463);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (494, 0, 100, 1, NULL, 469);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (495, 0, 460, 1, NULL, 470);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (495, 0, 461, 1, NULL, 471);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (495, 0, 462, 1, NULL, 472);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (495, 0, 463, 1, NULL, 473);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (497, 0, 665, 700, NULL, 474);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (497, 0, 694, 500, NULL, 475);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (498, 0, 652, 1, NULL, 476);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (498, 0, 696, 1, NULL, 495);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (499, 0, 648, 1, NULL, 477);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (499, 0, 649, 1, NULL, 478);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (499, 0, 650, 1, NULL, 479);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (500, 0, 702, 1, NULL, 481);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (500, 1, 5919, 1, NULL, 480);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (501, 0, 48, 1, NULL, 378);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (501, 0, 97, 1, NULL, 379);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (501, 0, 740, 1, NULL, 380);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (502, 0, 646, 400, NULL, 381);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (502, 0, 699, 400, NULL, 382);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (503, 1, 5920, 50, NULL, 383);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (503, 1, 5921, 50, NULL, 384);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (505, 1, 5922, 50, NULL, 385);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (506, 1, 5923, 50, NULL, 386);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (507, 1, 5924, 50, NULL, 387);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (507, 1, 5925, 50, NULL, 388);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (508, 1, 5926, 50, NULL, 389);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (509, 1, 5927, 100, NULL, 467);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (509, 1, 5928, 100, NULL, 468);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (510, 0, 37, 1, NULL, 418);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (510, 0, 443, 1, NULL, 417);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (510, 0, 444, 1, NULL, 416);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (510, 1, 5932, 1, NULL, 419);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (512, 0, 104, 1, NULL, 420);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (512, 1, 5929, 50, NULL, 421);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (513, 0, 449, 1, NULL, 422);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (513, 0, 451, 1, NULL, 423);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (513, 0, 827, 1, NULL, 424);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (513, 1, 5933, 1, NULL, 425);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (514, 0, 131, 1, NULL, 428);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (514, 0, 437, 1, NULL, 426);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (514, 0, 515, 1, NULL, 427);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (515, 0, 101, 1, NULL, 430);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (515, 0, 452, 1, NULL, 432);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (515, 0, 522, 1, NULL, 431);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (515, 1, 5934, 1, NULL, 433);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (517, 0, 106, 1, NULL, 438);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (517, 0, 400, 1, NULL, 437);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (517, 0, 456, 1, NULL, 436);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (517, 0, 457, 1, NULL, 435);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (517, 0, 469, 1, NULL, 434);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (518, 0, 73, 1, NULL, 441);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (518, 0, 105, 1, NULL, 443);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (518, 0, 401, 1, NULL, 444);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (518, 0, 417, 1, NULL, 442);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (518, 0, 466, 1, NULL, 439);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (518, 0, 468, 1, NULL, 440);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (518, 1, 5935, 1, NULL, 445);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (520, 0, 516, 1, NULL, 446);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (520, 0, 519, 1, NULL, 447);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (520, 0, 825, 1, NULL, 448);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (521, 0, 534, 1, NULL, 450);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (521, 0, 535, 1, NULL, 449);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (521, 0, 820, 1, NULL, 451);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (521, 1, 5936, 1, NULL, 452);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (522, 0, 655, 500, NULL, 483);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (522, 0, 692, 400, NULL, 482);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (523, 0, 693, 300, NULL, 484);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (523, 0, 824, 500, NULL, 485);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (525, 0, 129, 1, NULL, 486);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (525, 0, 656, 1, NULL, 488);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (525, 0, 657, 1, NULL, 487);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (525, 1, 5930, 200, NULL, 489);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (526, 0, 662, 1, NULL, 490);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (527, 0, 90, 1, NULL, 390);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (527, 0, 501, 1, NULL, 391);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (527, 0, 520, 1, NULL, 392);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (527, 0, 523, 1, NULL, 393);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (527, 0, 738, 1, NULL, 394);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (527, 0, 739, 1, NULL, 395);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (528, 1, 5931, 100, NULL, 396);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (530, 0, 103, 1, NULL, 397);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (530, 0, 112, 1, NULL, 398);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (530, 0, 473, 1, NULL, 399);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (530, 0, 477, 1, NULL, 400);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (530, 0, 735, 1, NULL, 401);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (531, 0, 110, 1, NULL, 405);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (531, 0, 111, 1, NULL, 403);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (531, 0, 397, 1, NULL, 404);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (531, 0, 736, 1, NULL, 402);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (532, 0, 45, 1, NULL, 406);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (532, 0, 96, 1, NULL, 407);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (532, 0, 659, 1, NULL, 408);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (532, 0, 737, 1, NULL, 409);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (532, 0, 742, 1, NULL, 410);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (533, 1, 6168, 1, NULL, 499);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (534, 1, 6169, 1, NULL, 497);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (535, 1, 6170, 1, NULL, 498);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (540, 0, 873, 1, NULL, 563);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (540, 0, 874, 1, NULL, 564);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (540, 0, 877, 1, NULL, 565);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (541, 0, 879, 1, NULL, 566);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (541, 0, 881, 1, NULL, 567);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (542, 0, 875, 1, NULL, 568);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (542, 0, 878, 1, NULL, 569);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (542, 0, 882, 1, NULL, 570);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (543, 0, 876, 1, NULL, 571);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (544, 0, 869, 1, NULL, 574);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (544, 0, 872, 1, NULL, 573);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (544, 0, 883, 1, NULL, 572);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (545, 0, 870, 1, NULL, 575);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (546, 1, 6445, 255, NULL, 576);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (547, 0, 1633, 1, NULL, 581);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (547, 0, 1634, 1, NULL, 582);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (547, 0, 1635, 1, NULL, 583);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (548, 0, 1637, 1, NULL, 584);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (548, 0, 1638, 1, NULL, 585);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (548, 0, 1639, 1, NULL, 586);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (549, 0, 1641, 1, NULL, 587);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (549, 0, 1642, 1, NULL, 588);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (549, 0, 1643, 1, NULL, 589);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (550, 0, 1645, 1, NULL, 590);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (550, 0, 1646, 1, NULL, 591);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (550, 0, 1647, 1, NULL, 592);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (551, 1, 6446, 1, NULL, 593);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (552, 0, 1800, 1, NULL, 594);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (552, 0, 1801, 1, NULL, 595);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (553, 1, 6447, 255, NULL, 596);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (554, 0, 1805, 1, NULL, 603);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (554, 0, 1806, 1, NULL, 602);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (554, 0, 1809, 1, NULL, 604);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (555, 1, 6448, 250, NULL, 600);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (556, 1, 6449, 1, NULL, 601);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (557, 0, 1313, 1, NULL, 605);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (557, 0, 1314, 1, NULL, 606);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (557, 0, 1316, 1, NULL, 607);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (558, 1, 6466, 250, NULL, 608);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (560, 0, 1334, 1, NULL, 610);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (560, 0, 1335, 1, NULL, 609);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (561, 1, 6450, 200, NULL, 611);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (562, 0, 1996, 1, NULL, 612);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (562, 0, 1997, 1, NULL, 613);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (563, 1, 6452, 200, NULL, 614);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (564, 0, 2000, 1, NULL, 620);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (565, 0, 1993, 1, NULL, 616);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (565, 0, 1994, 1, NULL, 617);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (566, 1, 6451, 200, NULL, 618);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (567, 0, 1999, 1, NULL, 619);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (568, 0, 1996, 1, NULL, 627);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (568, 0, 1997, 1, NULL, 628);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (568, 0, 1998, 1, NULL, 629);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (569, 0, 1988, 1, NULL, 630);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (569, 0, 1989, 1, NULL, 631);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (570, 0, 1990, 1, NULL, 632);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (570, 0, 1991, 1, NULL, 633);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (571, 0, 1992, 1, NULL, 634);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (572, 0, 1993, 1, NULL, 635);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (572, 0, 1994, 1, NULL, 643);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (572, 0, 1995, 1, NULL, 637);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (573, 0, 1983, 1, NULL, 638);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (573, 0, 1986, 1, NULL, 639);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (574, 0, 1984, 1, NULL, 641);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (574, 0, 1985, 1, NULL, 640);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (575, 0, 1987, 1, NULL, 642);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (578, 1, 430, 1, NULL, 621);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (579, 1, 903, 1, NULL, 622);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (580, 1, 907, 3, NULL, 623);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (581, 0, 2011, 1, NULL, 1231);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (581, 0, 2553, 1, NULL, 1232);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (582, 0, 130, 1, NULL, 1233);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (582, 0, 2537, 1, NULL, 1234);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (583, 0, 2009, 1, NULL, 1236);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (583, 0, 2551, 1, NULL, 1237);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (584, 1, 6291, 3, NULL, 539);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (585, 1, 6292, 3, NULL, 540);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (586, 1, 6293, 4, NULL, 541);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (587, 1, 6294, 3, NULL, 542);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (588, 0, 2011, 1, NULL, 1238);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (588, 0, 2553, 1, NULL, 1239);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (589, 0, 130, 1, NULL, 1240);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (589, 0, 2537, 1, NULL, 1241);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (590, 0, 2009, 1, NULL, 1244);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (590, 0, 2551, 1, NULL, 1245);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (591, 1, 6291, 3, NULL, 545);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (592, 1, 6292, 3, NULL, 546);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (593, 1, 6293, 4, NULL, 547);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (594, 1, 6294, 3, NULL, 548);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (595, 0, 2011, 1, NULL, 1242);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (595, 0, 2553, 1, NULL, 1243);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (596, 0, 130, 1, NULL, 1246);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (596, 0, 2537, 1, NULL, 1247);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (597, 0, 2009, 1, NULL, 1248);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (597, 0, 2551, 1, NULL, 1249);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (598, 1, 6291, 3, NULL, 520);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (599, 1, 6292, 3, NULL, 521);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (600, 1, 6293, 4, NULL, 522);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (601, 1, 6294, 3, NULL, 523);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (603, 1, 430, 1, NULL, 624);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (604, 1, 2719, 1, NULL, 625);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (605, 1, 2721, 3, NULL, 626);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (606, 1, 2718, 3, NULL, 554);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (607, 1, 2736, 5, NULL, 555);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (608, 1, 1034, 3, NULL, 556);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (609, 0, 1001, 1, NULL, 1251);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (609, 0, 2542, 1, NULL, 1252);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (610, 0, 1005, 1, NULL, 1253);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (610, 0, 2545, 1, NULL, 1254);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (611, 0, 1007, 5, NULL, 1264);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (611, 0, 1008, 5, NULL, 1265);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (612, 0, 1006, 1, NULL, 1261);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (612, 0, 2546, 1, NULL, 1263);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (614, 1, 430, 10, NULL, 562);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (615, 1, 6644, 1, NULL, 649);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (616, 1, 6644, 1, NULL, 650);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (617, 1, 6644, 1, NULL, 651);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (618, 1, 6644, 1, NULL, 652);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (619, 1, 6644, 1, NULL, 653);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (620, 1, 6644, 1, NULL, 654);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (621, 1, 6644, 1, NULL, 655);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (622, 1, 6644, 1, NULL, 656);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (623, 1, 430, 1, NULL, 687);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (624, 1, 903, 1, NULL, 688);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (625, 1, 907, 3, NULL, 689);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (626, 1, 6644, 1, NULL, 657);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (627, 1, 6644, 1, NULL, 658);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (628, 1, 6644, 1, NULL, 660);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (629, 1, 6644, 1, NULL, 661);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (630, 1, 6644, 1, NULL, 662);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (631, 1, 6644, 1, NULL, 663);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (632, 1, 6644, 1, NULL, 664);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (633, 1, 6644, 1, NULL, 666);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (634, 1, 430, 1, NULL, 690);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (635, 1, 903, 1, NULL, 691);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (636, 1, 907, 3, NULL, 692);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (637, 1, 6644, 1, NULL, 667);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (638, 1, 6644, 1, NULL, 668);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (639, 1, 6644, 1, NULL, 669);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (640, 1, 6644, 1, NULL, 671);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (641, 1, 6644, 1, NULL, 672);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (642, 1, 6644, 1, NULL, 673);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (643, 1, 6644, 1, NULL, 674);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (644, 1, 6644, 1, NULL, 675);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (645, 1, 6645, 1, NULL, 676);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (646, 1, 6645, 1, NULL, 677);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (647, 1, 6645, 1, NULL, 678);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (648, 1, 6645, 1, NULL, 679);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (649, 1, 6645, 1, NULL, 680);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (650, 1, 6645, 1, NULL, 681);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (651, 1, 6645, 1, NULL, 682);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (652, 1, 6645, 1, NULL, 683);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (653, 1, 6645, 1, NULL, 684);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (654, 1, 6645, 1, NULL, 685);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (655, 0, 2059, 100, NULL, 646);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (656, 0, 2060, 100, NULL, 647);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (657, 0, 2058, 100, NULL, 648);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (658, 1, 6700, 50, NULL, 732);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (659, 1, 6702, 100, NULL, 733);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (660, 0, 2059, 300, NULL, 1165);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (661, 1, 6701, 50, NULL, 736);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (662, 1, 6688, 1, NULL, 699);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (663, 1, 6688, 1, NULL, 700);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (664, 0, 79, 2, NULL, 702);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (664, 0, 667, 2, NULL, 701);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (664, 0, 832, 2, NULL, 703);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (665, 1, 6688, 1, NULL, 704);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (666, 1, 803, 5, NULL, 942);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (667, 1, 6689, 1, NULL, 706);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (668, 1, 6690, 10, NULL, 707);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (669, 0, 74, 15, NULL, 708);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (670, 0, 86, 5, NULL, 1137);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (671, 1, 5523, 5, NULL, 710);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (672, 1, 6691, 1, NULL, 711);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (673, 0, 395, 10, NULL, 712);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (673, 0, 396, 5, NULL, 713);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (674, 0, 148, 15, NULL, 714);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (675, 1, 5526, 5, NULL, 715);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (676, 1, 6693, 1, NULL, 716);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (677, 0, 81, 20, NULL, 718);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (677, 1, 5524, 1, NULL, 719);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (678, 1, 5524, 1, NULL, 720);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (679, 1, 6692, 1, NULL, 721);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (680, 0, 2071, 150, '', 722);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (681, 1, 6696, 150, NULL, 723);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (682, 0, 2072, 150, NULL, 724);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (682, 0, 2074, 150, NULL, 725);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (683, 1, 6697, 250, NULL, 745);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (684, 0, 2076, 150, NULL, 744);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (685, 1, 6698, 150, NULL, 728);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (686, 0, 2078, 150, NULL, 729);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (686, 0, 2079, 150, NULL, 730);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (687, 1, 6699, 250, NULL, 740);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (688, 1, 6703, 100, NULL, 737);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (689, 0, 2060, 300, NULL, 1164);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (690, 1, 6704, 1, NULL, 746);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (691, 1, 6705, 1, NULL, 747);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (692, 0, 667, 2, NULL, 756);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (693, 1, 6951, 1, NULL, 757);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (694, 1, 5530, 5, NULL, 758);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (695, 0, 843, 10, NULL, 759);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (696, 0, 83, 1, NULL, 784);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (696, 0, 153, 1, NULL, 785);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (696, 0, 405, 1, NULL, 786);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (697, 1, 6952, 1, NULL, 764);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (698, 0, 86, 1, NULL, 1135);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (698, 0, 402, 1, NULL, 1136);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (699, 0, 507, 15, NULL, 767);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (699, 1, 6953, 1, NULL, 768);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (700, 1, 6953, 1, NULL, 770);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (701, 1, 5534, 5, NULL, 771);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (702, 0, 75, 15, NULL, 772);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (703, 0, 409, 20, NULL, 773);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (704, 1, 6954, 1, NULL, 774);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (705, 0, 147, 10, NULL, 776);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (705, 0, 418, 20, NULL, 775);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (706, 1, 6955, 1, NULL, 777);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (707, 1, 7015, 1, NULL, 920);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (708, 0, 81, 15, NULL, 779);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (709, 0, 82, 10, NULL, 780);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (710, 1, 6956, 1, NULL, 781);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (711, 1, 6957, 1, NULL, 918);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (713, 0, 398, 1, NULL, 788);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (713, 0, 481, 1, NULL, 787);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (714, 1, 6958, 1, NULL, 789);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (715, 1, 5537, 10, NULL, 790);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (716, 1, 6958, 1, NULL, 934);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (717, 1, 5538, 5, NULL, 792);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (718, 0, 463, 10, NULL, 793);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (719, 0, 460, 5, NULL, 794);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (719, 0, 461, 1, NULL, 795);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (720, 1, 6959, 1, NULL, 796);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (721, 0, 92, 5, NULL, 797);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (721, 0, 697, 10, NULL, 798);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (722, 0, 652, 10, NULL, 799);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (723, 1, 6960, 1, NULL, 800);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (724, 1, 5540, 3, NULL, 801);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (725, 1, 6961, 1, NULL, 802);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (726, 1, 6961, 1, NULL, 803);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (727, 0, 665, 10, NULL, 804);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (727, 0, 666, 5, NULL, 805);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (728, 0, 648, 1, NULL, 806);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (728, 0, 649, 1, NULL, 807);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (728, 0, 650, 1, NULL, 808);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (729, 0, 483, 10, NULL, 810);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (729, 0, 486, 10, NULL, 809);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (730, 1, 6962, 1, NULL, 811);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (732, 0, 34, 1, NULL, 812);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (732, 0, 36, 1, NULL, 813);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (732, 0, 89, 1, NULL, 814);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (733, 1, 6963, 1, NULL, 815);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (734, 0, 97, 5, NULL, 927);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (734, 0, 646, 10, NULL, 816);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (735, 1, 6964, 1, '', 818);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (736, 0, 41, 15, NULL, 819);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (737, 1, 6965, 3, NULL, 820);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (738, 1, 6966, 1, NULL, 821);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (739, 0, 474, 15, NULL, 823);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (740, 1, 6967, 1, NULL, 825);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (741, 1, 6968, 10, NULL, 826);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (742, 0, 454, 5, NULL, 928);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (742, 0, 505, 5, NULL, 930);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (743, 0, 122, 3, NULL, 931);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (743, 0, 416, 1, NULL, 829);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (744, 0, 358, 3, NULL, 932);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (745, 1, 6969, 1, NULL, 832);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (746, 0, 404, 15, NULL, 933);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (747, 0, 2140, 150, NULL, 834);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (748, 0, 2142, 200, NULL, 835);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (749, 0, 2143, 150, NULL, 836);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (749, 0, 2144, 150, NULL, 837);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (750, 1, 6982, 250, NULL, 921);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (751, 0, 2145, 150, NULL, 839);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (752, 0, 2147, 200, NULL, 840);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (753, 0, 2148, 150, NULL, 841);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (753, 0, 2149, 150, NULL, 842);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (754, 1, 6983, 250, NULL, 922);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (755, 1, 6970, 50, NULL, 844);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (756, 1, 6972, 100, NULL, 845);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (757, 0, 2058, 300, NULL, 1161);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (758, 1, 6971, 50, NULL, 847);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (759, 1, 6973, 100, NULL, 848);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (760, 0, 2059, 300, NULL, 1163);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (761, 1, 6974, 1, NULL, 850);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (762, 1, 6975, 1, NULL, 851);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (763, 0, 83, 600, NULL, 852);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (764, 0, 86, 700, NULL, 853);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (765, 1, 6991, 1, NULL, 854);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (766, 0, 33, 800, NULL, 855);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (767, 1, 6992, 1, NULL, 856);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (768, 0, 418, 1000, NULL, 857);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (769, 0, 361, 1, NULL, 861);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (769, 0, 363, 1, NULL, 858);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (769, 0, 578, 1, NULL, 860);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (769, 0, 579, 1, NULL, 859);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (770, 0, 34, 600, NULL, 862);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (771, 0, 43, 400, NULL, 863);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (771, 0, 459, 400, NULL, 864);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (772, 1, 6993, 1, NULL, 865);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (773, 1, 6986, 50, NULL, 866);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (774, 0, 122, 400, NULL, 867);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (774, 0, 416, 200, NULL, 1014);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (775, 1, 6987, 60, NULL, 869);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (776, 0, 358, 1, NULL, 1021);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (777, 0, 131, 1, NULL, 873);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (777, 0, 437, 1, NULL, 871);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (777, 0, 515, 1, NULL, 872);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (778, 0, 101, 1, NULL, 874);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (778, 0, 452, 1, NULL, 876);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (778, 0, 522, 1, NULL, 875);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (778, 1, 6988, 1, NULL, 877);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (779, 1, 6994, 1, NULL, 878);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (780, 0, 106, 1, NULL, 883);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (780, 0, 400, 1, NULL, 882);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (780, 0, 456, 1, NULL, 881);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (780, 0, 457, 1, NULL, 880);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (780, 0, 469, 1, NULL, 879);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (781, 0, 73, 1, NULL, 886);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (781, 0, 105, 1, NULL, 887);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (781, 0, 417, 1, NULL, 888);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (781, 0, 466, 1, NULL, 884);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (781, 0, 468, 1, NULL, 885);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (781, 1, 6989, 1, NULL, 889);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (782, 1, 6995, 1, NULL, 944);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (783, 0, 516, 1, NULL, 891);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (783, 0, 519, 1, NULL, 892);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (783, 0, 825, 1, NULL, 893);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (784, 0, 534, 1, NULL, 895);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (784, 0, 535, 1, NULL, 894);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (784, 0, 820, 1, NULL, 896);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (784, 1, 6990, 1, NULL, 897);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (785, 0, 655, 30, NULL, 898);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (786, 1, 6999, 10, NULL, 915);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (787, 1, 7193, 1, NULL, 935);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (788, 0, 693, 50, NULL, 939);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (789, 1, 7000, 15, NULL, 902);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (790, 1, 7001, 50, NULL, 903);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (791, 0, 738, 40, NULL, 904);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (792, 1, 7002, 20, NULL, 905);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (793, 1, 7003, 20, NULL, 917);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (794, 1, 7194, 1, NULL, 936);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (795, 0, 108, 30, NULL, 909);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (795, 0, 819, 30, NULL, 908);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (796, 1, 7004, 50, NULL, 910);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (797, 0, 2307, 1, NULL, 945);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (797, 0, 2308, 1, NULL, 946);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (797, 0, 2309, 1, NULL, 947);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (797, 0, 2315, 1, NULL, 1017);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (798, 1, 7209, 1, NULL, 948);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (799, 0, 2255, 20, NULL, 949);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (799, 0, 2256, 20, NULL, 950);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (800, 0, 2259, 40, NULL, 951);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (801, 0, 2257, 25, NULL, 952);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (801, 0, 2258, 25, NULL, 953);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (802, 0, 2260, 30, NULL, 954);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (803, 0, 2261, 40, NULL, 955);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (803, 0, 2262, 30, NULL, 956);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (804, 1, 7210, 1, NULL, 957);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (805, 0, 2252, 1, NULL, 958);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (805, 0, 2253, 1, NULL, 959);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (805, 0, 2254, 1, NULL, 960);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (806, 1, 7211, 1, NULL, 961);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (807, 1, 7212, 1, NULL, 962);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (808, 0, 2288, 30, NULL, 963);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (808, 0, 2289, 30, NULL, 964);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (809, 0, 2285, 50, NULL, 965);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (810, 1, 7214, 20, NULL, 967);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (811, 0, 2290, 30, NULL, 970);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (811, 0, 2291, 30, NULL, 971);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (812, 1, 7215, 20, NULL, 972);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (813, 1, 7216, 1, NULL, 973);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (814, 0, 2293, 1, NULL, 974);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (814, 0, 2294, 1, NULL, 975);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (814, 0, 2295, 1, NULL, 976);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (815, 1, 7213, 1, NULL, 977);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (816, 0, 2296, 30, NULL, 980);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (816, 0, 2298, 30, NULL, 981);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (817, 0, 2297, 50, NULL, 982);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (818, 1, 7217, 20, NULL, 984);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (819, 0, 2302, 30, NULL, 985);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (819, 0, 2303, 30, NULL, 986);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (820, 1, 7218, 20, NULL, 987);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (821, 1, 7219, 1, NULL, 988);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (822, 0, 2304, 1, NULL, 989);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (822, 0, 2305, 1, NULL, 990);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (822, 0, 2306, 1, NULL, 991);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (823, 0, 2266, 1, NULL, 1139);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (823, 0, 2267, 1, NULL, 1138);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (823, 0, 2288, 1, NULL, 994);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (823, 0, 2289, 1, NULL, 993);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (824, 0, 2263, 1, NULL, 1140);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (824, 0, 2264, 1, NULL, 1141);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (824, 0, 2265, 1, NULL, 1142);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (824, 0, 2285, 1, NULL, 995);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (824, 0, 2286, 1, NULL, 996);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (824, 0, 2287, 1, NULL, 997);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (825, 0, 2268, 1, NULL, 1143);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (825, 0, 2269, 1, NULL, 1145);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (825, 0, 2270, 1, NULL, 1146);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (825, 0, 2290, 1, NULL, 998);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (825, 0, 2291, 1, NULL, 999);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (825, 0, 2292, 1, NULL, 1020);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (826, 0, 2271, 1, NULL, 1149);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (826, 0, 2272, 1, NULL, 1148);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (826, 0, 2273, 1, NULL, 1147);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (826, 0, 2293, 1, NULL, 1000);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (826, 0, 2294, 1, NULL, 1001);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (826, 0, 2295, 1, NULL, 1002);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (827, 0, 2274, 1, NULL, 1150);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (827, 0, 2275, 1, NULL, 1151);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (827, 0, 2276, 1, NULL, 1152);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (827, 0, 2296, 1, NULL, 1003);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (827, 0, 2297, 1, NULL, 1004);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (827, 0, 2298, 1, NULL, 1005);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (828, 0, 2277, 1, NULL, 1153);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (828, 0, 2278, 1, NULL, 1154);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (828, 0, 2299, 1, NULL, 1006);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (828, 0, 2300, 1, NULL, 1007);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (829, 0, 2279, 1, NULL, 1155);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (829, 0, 2280, 1, NULL, 1156);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (829, 0, 2281, 1, NULL, 1157);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (829, 0, 2301, 1, NULL, 1008);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (829, 0, 2302, 1, NULL, 1009);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (829, 0, 2303, 1, NULL, 1010);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (830, 0, 2282, 1, NULL, 1158);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (830, 0, 2283, 1, NULL, 1159);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (830, 0, 2284, 1, NULL, 1160);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (830, 0, 2304, 1, NULL, 1011);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (830, 0, 2305, 1, NULL, 1012);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (830, 0, 2306, 1, NULL, 1013);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (831, 0, 145, 1, NULL, 1134);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (831, 0, 146, 1, NULL, 1133);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (832, 1, 7573, 50, NULL, 1166);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (833, 1, 7574, 100, NULL, 1167);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (834, 0, 2060, 300, NULL, 1168);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (835, 1, 7575, 1, NULL, 1169);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (836, 0, 2448, 150, NULL, 1170);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (837, 1, 7579, 100, NULL, 1171);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (838, 0, 2450, 200, NULL, 1175);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (839, 1, 7580, 250, NULL, 1176);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (840, 0, 2304, 700, NULL, 1230);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (841, 0, 2454, 800, NULL, 1178);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (842, 0, 2455, 800, NULL, 1179);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (843, 1, 7586, 200, NULL, 1180);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (844, 0, 2293, 700, NULL, 1181);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (845, 0, 2458, 800, NULL, 1184);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (846, 0, 2460, 800, NULL, 1185);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (847, 1, 7587, 200, NULL, 1186);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (848, 1, 7582, 10, NULL, 1187);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (849, 0, 2458, 30, NULL, 1188);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (849, 0, 2460, 30, NULL, 1189);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (850, 1, 7585, 20, NULL, 1190);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (851, 0, 2458, 1, NULL, 1191);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (851, 0, 2459, 1, NULL, 1192);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (851, 0, 2460, 1, NULL, 1193);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (851, 0, 2461, 1, NULL, 1194);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (852, 1, 7583, 10, NULL, 1195);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (853, 0, 2454, 30, NULL, 1196);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (853, 0, 2455, 30, NULL, 1197);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (854, 1, 7584, 20, NULL, 1198);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (855, 0, 2454, 1, NULL, 1199);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (855, 0, 2455, 1, NULL, 1200);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (855, 0, 2456, 1, NULL, 1201);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (855, 0, 2457, 1, NULL, 1202);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (856, 0, 74, 20, NULL, 1268);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (857, 0, 145, 40, '', 1269);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (858, 0, 1798, 1, NULL, 1270);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (858, 0, 1799, 1, NULL, 1271);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (858, 0, 1800, 1, NULL, 1272);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (858, 0, 1801, 1, NULL, 1273);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (859, 1, 8082, 20, NULL, 1274);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (860, 1, 8083, 20, NULL, 1275);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (861, 1, 8084, 1, NULL, 1276);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (862, 0, 1810, 100, NULL, 1277);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (862, 0, 1811, 100, NULL, 1278);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (862, 0, 1812, 100, NULL, 1279);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (863, 1, 8085, 1, NULL, 1280);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (864, 1, 8086, 20, NULL, 1281);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (865, 0, 395, 50, NULL, 1282);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (865, 0, 396, 50, NULL, 1283);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (866, 0, 2610, 800, NULL, 1284);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (867, 1, 8376, 100, NULL, 1285);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (868, 0, 2611, 1000, NULL, 1286);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (869, 1, 8377, 200, NULL, 1287);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (870, 1, 8378, 1, NULL, 1295);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (871, 0, 2614, 800, NULL, 1289);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (872, 1, 8379, 100, NULL, 1290);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (873, 0, 2615, 1000, NULL, 1291);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (874, 1, 8380, 200, NULL, 1292);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (875, 1, 8381, 1, NULL, 1293);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (876, 1, 8398, 10, NULL, 1318);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (877, 1, 8399, 15, NULL, 1319);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (878, 1, 8400, 20, NULL, 1320);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (879, 1, 8401, 30, NULL, 1321);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (880, 1, 8402, 50, NULL, 1322);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (881, 0, 2672, 30, NULL, 1323);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (882, 0, 2673, 50, NULL, 1324);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (883, 0, 2674, 30, NULL, 1325);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (884, 0, 2675, 50, NULL, 1326);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (885, 0, 2676, 100, NULL, 1327);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (886, 0, 2672, 1, NULL, 1328);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (886, 0, 2673, 1, NULL, 1329);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (886, 0, 2674, 1, NULL, 1330);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (886, 0, 2675, 1, NULL, 1331);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (886, 0, 2676, 1, NULL, 1332);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (887, 1, 8403, 10, NULL, 1333);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (888, 1, 8404, 15, NULL, 1334);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (889, 1, 8405, 20, NULL, 1335);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (890, 1, 8406, 30, NULL, 1336);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (891, 1, 8407, 50, NULL, 1337);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (892, 0, 2677, 30, NULL, 1338);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (893, 0, 2678, 50, NULL, 1339);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (894, 0, 2679, 30, NULL, 1340);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (895, 0, 2680, 50, NULL, 1341);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (896, 0, 2681, 100, NULL, 1342);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (897, 0, 2677, 1, NULL, 1343);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (897, 0, 2678, 1, NULL, 1344);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (897, 0, 2679, 1, NULL, 1345);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (897, 0, 2680, 1, NULL, 1346);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (897, 0, 2681, 1, NULL, 1347);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (900, 0, 85, 50, NULL, 309);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (900, 0, 127, 50, NULL, 307);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (900, 0, 410, 50, NULL, 308);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (901, 0, 840, 50, NULL, 310);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (901, 0, 841, 50, NULL, 311);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (902, 0, 83, 75, NULL, 312);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (902, 0, 153, 75, NULL, 313);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (902, 0, 405, 75, NULL, 314);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (903, 0, 402, 75, NULL, 317);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (903, 0, 507, 75, NULL, 315);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (903, 0, 525, 75, NULL, 316);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (904, 0, 91, 75, NULL, 318);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (904, 0, 533, 75, NULL, 319);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (904, 0, 825, 75, NULL, 320);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (905, 0, 656, 100, NULL, 321);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (906, 0, 358, 100, NULL, 322);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (906, 0, 646, 100, NULL, 323);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (907, 0, 103, 100, NULL, 325);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (907, 0, 473, 100, NULL, 324);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (907, 0, 735, 100, NULL, 326);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (908, 0, 873, 150, NULL, 327);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (908, 0, 874, 150, NULL, 328);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (909, 0, 869, 200, NULL, 329);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (909, 0, 872, 200, NULL, 330);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (910, 0, 882, 200, NULL, 750);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (911, 0, 878, 200, NULL, 751);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (912, 0, 883, 200, NULL, 752);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (913, 0, 870, 200, NULL, 753);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (914, 0, 870, 100, NULL, 923);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (914, 0, 872, 100, NULL, 925);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (914, 0, 883, 100, NULL, 924);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1000, 0, 2501, 800, NULL, 1348);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1000, 0, 2503, 800, NULL, 1349);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1001, 0, 93, 1, NULL, 1350);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1001, 0, 95, 1, NULL, 1351);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1001, 0, 99, 1, NULL, 1352);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1001, 0, 102, 1, NULL, 1353);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1001, 0, 408, 1, NULL, 1354);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1002, 1, 8450, 50, NULL, 1355);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1002, 1, 8451, 50, NULL, 1356);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1002, 1, 8452, 50, NULL, 1357);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1002, 1, 8453, 15, NULL, 1358);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1002, 1, 8454, 10, NULL, 1359);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2379, 1, NULL, 1360);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2380, 1, NULL, 1361);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2381, 1, NULL, 1362);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2382, 1, NULL, 1363);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2390, 1, NULL, 1364);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2391, 1, NULL, 1365);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2392, 1, NULL, 1366);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2393, 1, NULL, 1367);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2394, 1, NULL, 1368);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2395, 1, NULL, 1369);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2396, 1, NULL, 1370);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2397, 1, NULL, 1371);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2398, 1, NULL, 1372);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2399, 1, NULL, 1373);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2400, 1, NULL, 1374);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2401, 1, NULL, 1375);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2402, 1, NULL, 1376);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2403, 1, NULL, 1377);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1003, 0, 2404, 1, NULL, 1378);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1004, 1, 8455, 100, NULL, 1379);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1005, 0, 92, 100, NULL, 1381);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1005, 0, 100, 100, NULL, 1380);
GO

INSERT INTO [dbo].[TblQuestCondition] ([mQuestNo], [mType], [mID], [mCnt], [mDesc], [mSeqNo]) VALUES (1006, 1, 8885, 20, NULL, 1383);
GO

