/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblSpecificProcItem
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1135, 1, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1136, 0, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1288, 2, 30, 293, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1289, 2, 30, 294, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1290, 2, 30, 295, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1291, 2, 30, 296, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1386, 2, 10, 282, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1387, 2, 10, 280, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1588, 6, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1590, 4, 0, 0, 0, 20.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1591, 4, 0, 0, 0, 30.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1592, 4, 0, 0, 0, 40.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1593, 5, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1594, 4, 2, 2, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1595, 3, 50, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1596, 3, 100, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1597, 4, 3, 3, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1598, 4, 4, 4, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1599, 4, 5, 5, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1600, 4, 6, 6, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1601, 4, 7, 7, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1602, 4, 3, 7, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1625, 5, 1, 1, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1626, 5, 2, 2, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (1825, 4, 1, 1, 0, 1.3);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2069, 4, 8, 8, 0, 1.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2070, 4, 9, 9, 0, 1.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2071, 4, 1, 1, 0, 1.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2072, 4, 8, 8, 0, 2.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2073, 4, 9, 9, 0, 2.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2074, 4, 1, 1, 0, 2.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2587, 4, 1, 1, 0, 1.3);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2588, 4, 1, 1, 0, 1.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2589, 4, 1, 1, 0, 2.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2592, 4, 2, 2, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2735, 4, 10, 10, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2796, 7, 3, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2797, 4, 10, 10, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2802, 6, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2824, 9, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2825, 8, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2836, 4, 1, 1, 0, 1.3);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (2837, 4, 1, 1, 0, 2.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3346, 11, 5, 18, 1, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3347, 12, 1, 7, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3348, 12, 1, 15, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3349, 13, 100, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3350, 14, 677, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3351, 14, 678, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3353, 10, 700, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3363, 10, 4000, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3364, 10, 9000, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3365, 10, 33000, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3366, 10, 7000, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3367, 10, 14000, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3368, 10, 21000, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3371, 4, 11, 11, 0, 99.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3372, 4, 11, 11, 0, 1.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3373, 4, 11, 11, 0, 2.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3422, 4, 12, 12, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (3960, 15, 500, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4206, 4, 13, 13, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4321, 4, 13, 13, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4322, 4, 13, 13, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4323, 4, 13, 13, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4390, 4, 1, 1, 0, 1.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4391, 4, 1, 1, 0, 1.3);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4891, 12, 3, 1, 1, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4892, 12, 10, 3, 1, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4893, 12, 10, 5, 1, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (4894, 12, 10, 7, 1, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5243, 2, 30, 1008, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5244, 2, 30, 1009, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5245, 2, 30, 1010, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5246, 2, 30, 1011, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5515, 2, 30, 1432, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5516, 2, 30, 1433, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5517, 2, 30, 1434, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5518, 2, 30, 1435, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5519, 2, 30, 1430, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5520, 2, 30, 1431, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5688, 16, 1, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5689, 16, 2, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5896, 4, 14, 14, 0, 1.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5901, 4, 1, 1, 0, 1.3);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5905, 4, 1, 1, 0, 2.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5906, 4, 1, 1, 0, 1.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (5907, 4, 1, 1, 0, 1.3);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6282, 4, 14, 14, 0, 10.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6551, 12, 3, 7, 1, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6651, 4, 1, 1, 0, 3.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6652, 4, 1, 1, 0, 3.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6653, 4, 1, 1, 0, 4.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6654, 4, 1, 1, 0, 4.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6655, 4, 1, 1, 0, 5.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6656, 4, 1, 1, 0, 5.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6657, 4, 1, 1, 0, 6.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6658, 4, 1, 1, 0, 6.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6659, 4, 1, 1, 0, 7.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (6682, 4, 15, 15, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7031, 2, 30, 2712, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7032, 2, 30, 2714, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7229, 17, 34751, 20358, 521266, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7230, 17, 73514, 22104, 531998, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7231, 17, 227693, 14590, 505416, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7232, 17, 242504, 11322, 536220, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7233, 17, 47726, 16734, 265625, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7234, 17, 434627, 19983, 255745, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7235, 17, 447605, 18549, 282636, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7236, 17, 275895, 18004, 89787, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7237, 17, 952581, 21459, 1159685, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7238, 17, 659298, 11386, 220366, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7239, 17, 1074504, 20377, 532954, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7240, 17, 824542, 41405, 386098, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7797, 18, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7798, 4, 1, 1, 0, 1.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7799, 4, 9, 9, 0, 1.5);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (7800, 4, 8, 8, 0, 2.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8004, 19, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8005, 16, 3, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8080, 20, 0, 0, 0, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8368, 17, 963250, 15363, 1225592, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8369, 17, 321916, 11355, 66857, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8370, 17, 44903, 16594, 164467, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8371, 17, 196065, 11626, 303820, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8372, 17, 621304, 18527, 380403, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8373, 17, 575289, 18734, 376099, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8374, 17, 1141111, 15427, 319400, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8375, 17, 1158096, 13840, 382268, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8487, 21, 1, 99, 1200, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8914, 21, 1, 99, 100000, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8915, 21, 1, 99, 1000000, 0.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8916, 21, 1, 99, 0, 1.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8917, 21, 1, 99, 0, 3.0);
GO

INSERT INTO [dbo].[TblSpecificProcItem] ([mIID], [mProcNo], [mAParam], [mBParam], [mCParam], [mDParam]) VALUES (8918, 21, 1, 99, 0, 5.0);
GO

