/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblCode
Date                  : 2023-10-07 09:08:49
*/


INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (106, 7, 0, '辟芭府 公扁');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (107, 7, 1, '盔芭府 公扁');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (108, 7, 2, '癌渴');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (109, 7, 3, '规菩');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (110, 7, 4, '厘癌');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (111, 7, 5, '何明');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (112, 7, 6, '捧备');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (113, 7, 7, '噶配');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (114, 7, 8, '厩技荤府');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (115, 7, 9, '氓');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (116, 7, 10, '肯靛');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (117, 7, 11, '家葛前');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (118, 7, 12, '扁鸥');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (119, 7, 13, '概磐府倔');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (58, 6, 0, '等待收领');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (59, 6, 1, '收领申请');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (60, 6, 2, '收领完毕');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (61, 6, 3, '退款/取消');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (62, 6, 4, '取消支付(GM)');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (63, 6, 9, '错误');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (54, 5, 0, 'System Gift Or Give');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (55, 5, 1, ' Web Buy ');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (56, 5, 2, 'Gift Buy');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (57, 5, 3, 'InGame Buy');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (46, 4, 1, 'PcBangLv1');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (47, 4, 2, 'PcBangLv2');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (48, 4, 3, 'PcBangLv3');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (49, 4, 4, 'ePcBangLvNo');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (1, 3, 0, '激活基本公会技能');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (2, 3, 1, '激活公会仓库 Lv.1');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (3, 3, 2, '激活公会仓库 Lv.2');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (4, 2, 0, '记忆列表增加');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (5, 2, 1, '经验值增加');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (6, 2, 2, '地下城入场券');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (7, 2, 3, '开放戒指栏1');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (8, 2, 4, '开放戒指栏2');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (9, 2, 5, '开放项链栏');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (10, 2, 6, '开放腰带栏');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (11, 2, 7, '开放披风栏');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (12, 1, 0, '0 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (13, 1, 1, '1 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (14, 1, 2, '2 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (15, 1, 3, '3 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (16, 1, 4, '4 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (17, 1, 5, '5 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (18, 1, 6, '6 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (19, 1, 7, '7 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (20, 1, 8, '8 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (21, 1, 9, '9 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (22, 1, 10, '10 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (23, 1, 11, '11 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (24, 1, 12, '12 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (25, 1, 13, '13 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (26, 1, 14, '14 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (27, 1, 15, '15 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (28, 1, 16, '16 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (29, 1, 17, '17 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (30, 1, 18, '18 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (31, 1, 19, '19 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (32, 1, 20, '20 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (33, 1, 21, '21 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (34, 1, 22, '22 H');
GO

INSERT INTO [dbo].[TblCode] ([mSeq], [mCodeType], [mCode], [mCodeDesc]) VALUES (35, 1, 23, '23 H');
GO

