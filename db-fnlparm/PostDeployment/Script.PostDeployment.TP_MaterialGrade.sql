/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_MaterialGrade
Date                  : 2023-10-07 09:08:49
*/


INSERT INTO [dbo].[TP_MaterialGrade] ([MGrade], [MGradeDesc]) VALUES (1, 'Normal');
GO

INSERT INTO [dbo].[TP_MaterialGrade] ([MGrade], [MGradeDesc]) VALUES (2, 'Rare');
GO

INSERT INTO [dbo].[TP_MaterialGrade] ([MGrade], [MGradeDesc]) VALUES (3, 'Epic');
GO

INSERT INTO [dbo].[TP_MaterialGrade] ([MGrade], [MGradeDesc]) VALUES (4, 'Legend');
GO

