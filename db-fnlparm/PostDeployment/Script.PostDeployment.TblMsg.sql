/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMsg
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 5983105, 'eErrNoARSAuthExpired', '电话认证信息已过期');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 25488234, 'eErrNoChn_INITFAIL', 'Chn_初始化失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 65635579, 'eErrNoUOtpRv10', 'U-OTP 验证码输入错误的次数已经超过最大限制.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 75776226, 'eErrNoUOtpRv14', '您已经20次以上错误输入了身份证号和密码.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 89398300, 'eErrNoWebzenCertifyTimeOverAuthKey', '验证码有效期限已经到期.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 150374958, 'eErrNoChuMgr_DBGW_SEL_COLUMN_INVALID', 'DBGW Select 数据无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 207324885, 'eErrNoERR_SMALLBUFFER', '(PORTE)缓存不足');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 208026095, 'eErrNoChuMgr_INITED_FAIL', '模块初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 214852943, 'eErrNoCantConnectChina', '认证失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 241024234, 'eErrNoARSInvalidTelecom', '运营商名义核实错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 270146090, 'eErrNoOtpInvalidPswd', 'OTP(一次性密码)密码错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 285585174, 'eErrNoPORTE_AUTH_TICKET_ENCRYPT_FAIL', '(PORTE)验证码加密失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 337304604, 'eErrNoChuMgr_DBGW_FUNC_INVALID', 'DBGW Query 函数无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 436550671, 'eErrNoSecEmailSubmitErr', '安全验证码发送失败- 发送时遇到错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 474736442, 'eErrNoARSInvalidUser', '不是电话认证对象');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 489708825, 'eErrNoRscKeyInvalidFmt', '资源(resource key)文件异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 524050990, 'eErrNoOtpCantRcv', '无法获取OTP(一次性密码)数据,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 543585051, 'eErrNoUOtpRv4', '您输入的帐号不存在或输入有误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 547224100, 'eErrNoChn_AUTH_TICKET_WRONG_FORMAT', 'Chn_验证码格式错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 624077907, 'eErrNoWebzenCertifyInvalidAuthKey', '验证码错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 637423802, 'eLogNoSCSvcDeactivation', 'SC卡未激活情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 638771216, 'eErrNoCantFindARSInfoFile', '未找到电话认证信息文件');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 645162616, 'eErrNoChuMgr_DBGW_EXEC_FAILED', 'DBGW 查询失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 655079170, 'eErrNoUOtpRv0', '无意义');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 701513520, 'eErrNoUOtpRv8', '请确认U-OTP注册状态.(O005, O007, O008, O009)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 726758859, 'eErrNoARSAuthOverDelay', '电话认证输入超时');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 743916560, 'eLogNoARSServerSend', '已传送到ARS认证服务器。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 752793422, 'eErrNoChuMgr_INITED_NOT', '模块没有被初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 774543117, 'eErrNoChn_AUTH_TICKET_TIMEOVER', 'Chn_验证码已过有效期');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 779563836, 'eErrNoChn_CREATECONNETIONPOOL', 'Chn_无法连接认证服务器');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 780566244, 'eErrNoARSAuthCancel', '取消电话认证');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 794852852, 'eErrNoHangameInvalidParm', '服务器认证参数异常,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 799609633, 'eErrNoUOtpRv24', '无法进行UOTP认证');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 803911485, 'eErrNoChn_AUTH_TICKET_DIFF_IP', 'Chn_登陆的Ip有误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 810355099, 'eErrNoChn_AUTH_TICKET_DECRYPT_FAIL', 'Chn_验证码解密失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 828880881, 'eErrNoPORTE_AUTH_FAIL', '(PORTE)验证失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 829515234, 'eErrNoERR_UNEXPECTED', '(PORTE)预想之外的错误.请稍后再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 830957530, 'eErrNoChuMgr_DLL_UNEXPECTED', '未定义错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 879314443, 'eErrNoHangameInvalidPswd', '用户密码出错.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 892363584, 'eErrNoChn_AUTH_TICKET_DIFF_ID', 'Chn_登陆的ID有误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 957706152, 'eErrNoERR_GETCONNETIONPOOL', '(PORTE)无法连接');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 977268296, 'eErrNoHangameNotRegularMem', '您的帐号不是正式会员帐号.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1011493210, 'eErrNoChannelServerInfo', '无法找到频道服务器配置文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1069506105, 'eErrNoHangameNotEqualIp', '没有被认证的服务器,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1085437596, 'eErrNoHangameRcvedInvalidFmt', '从服务器获取信息异常,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1117606566, 'eErrNoUOtpUndefinedError', 'U-OTP中没用定义的错误,请稍后再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1151069489, 'eErrNoHangameTooManyFail', '密码输入错误三次,请重新登陆游戏.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1175935279, 'eErrNoWebzenCertifyNotExistUserInfo', '不存在的玩家情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1243992141, 'eErrNoChuMgr_Unknown', '未知错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1269186291, 'eErrNoCannotInitChUserMgr', '模块初始化未成功.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1297126485, 'eErrNoWebzenCertifyUnknownErrValue', '验证码认证中收到了错误的信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1342998420, 'eErrNoUOtpRv1', '登录成功.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1376029973, 'eErrNoSecKeyNotSkip', '请先通过安全验证码认证.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1379027733, 'eErrNoAbnormalSecKeyReq', '非正常的安全验证卡申请.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1400348890, 'eErrNoPORTE_UNKNOWN_ERROR', '(PORTE)未知错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1418830864, 'eErrNoNormalUserUsesAdminClient', '(PORTE)普通玩家无法使用管理员客户端登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1426297652, 'eErrNoCantFindARSAuthInfoFileKey', '未找到电话认证服务器秘钥');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1454587812, 'eErrNoERR_INITFAIL', '(PORTE)已设定的环境初始化失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1465992077, 'eErrNoUOtpRv5', '非联众的正常帐号.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1491666716, 'eErrNoOtpCantSnd', '无法发送OTP(一次性密码)认证请求,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1492617870, 'eErrNoLoginGwNotExist', '无法找到认证GW');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1502904013, 'eErrNoIsNotExistChannelCertifyInfo', '验证情报不存在.请重新登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1517155501, 'eErrNoChuMgr_DBGW_CONNECTION_FAILED', '无法连接 DBGW 服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1591152550, 'eErrNoUOtpRv9', 'U-OTP认证服务器认证出错.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1606674350, 'eErrNoUOtpRv21', 'U-OTP认证过程中收到未定义的结果.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1680659822, 'eErrNoARSServerInvalidPacket', '电话认证数据包错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1689763453, 'eErrNoHangameFailSystem', '系统出错,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1716320127, 'eErrNoChuMgr_DBGW_SEL_INVALID', '无效的 DBGW Select 结果.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1753077313, 'eLogNoSCSvcActivation', 'SC卡未激活情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1765180357, 'eErrNoSecEmailCtsErr', '安全验证码发送失败- 内容出现错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1801008807, 'eErrNoChn_AUTH_FAIL', 'Chn_验证失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1907210533, 'eErrNoChn_AUTH_TICKET_MAKEKEY_FAIL', 'Chn_验证码加密失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1907582488, 'eErrNoChuMgr_ARGUMENT_INVALID', '数据传输错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1925503193, 'eErrNoWebzenCertifyMissMatchCertifyIP', '验证码认证中与IP不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1937993844, 'eErrNoUOtpRv15', '此用户输入密码有误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1951935258, 'eErrNoChn_ISNOTINITED', 'Chn_模块未被初始化');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 1961800813, 'eErrNoUOtpRv11', 'U-OTP 再次发送申请中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2008880089, 'eErrNoChn_AHTH_TICKET_INVALID', 'Chn_值过长');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2064860483, 'eErrNoIPMissMatch', 'IP不一致.请重新登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2093224465, 'eErrNoHangameNotCertify', '没有通过服务器认证.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2108958059, 'eErrNoChuMgr_DBGW_SEL_OVERCOUNT', 'DBGW Select 结果为 1个以上.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2121777790, 'eErrNoSecEmailConnFail', '无法连接游戏服务器,安全验证码发送失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2145998534, 'eErrNoRscKeyTryPatch', '游戏需安装补丁文件,请关闭程序后重新启动游戏.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2168942375, 'eErrNoOptTerminateSvc', 'R2 OTP服务结束，请参照主页.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2208232205, 'eErrNoIpBlocked', '此IP已经被禁止.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2209798178, 'eLogNoSCSvcRequest', 'SC卡申请情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2246024486, 'eErrNoChn_INVALIDUSERINFO', 'Chn_String格式错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2294923952, 'eErrNoChn_AUTH_TICKET_ENCRYPT_FAIL', 'Chn_验证码加密失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2297537567, 'eErrNoARSAuthCantConnect', '无法连接电话认证');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2334373019, 'eErrNoHangameNotMember', '没有找到此用户帐号.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2381193506, 'eErrNoRscKeyCantOpen', '无法初始化资源(resource key)文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2503810964, 'eErrNoChn_ALREADYINITED', 'Chn_已初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2515736091, 'eErrNoPORTE_AUTH_TICKET_TIMEOVER', '(PORTE)验证码已过有效期.请重新登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2539632769, 'eErrNoERR_NORESPONSE', '(PORTE)认证服务器没有应答.请稍后再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2566321562, 'eErrNoOtpNotActive', '您不是OTP(一次性密码)用户.请确认您是否注册了OTP服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2574382034, 'eErrNoChuMgr_DBGW_SEL_FAILED', 'DBGW Select 作业失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2598425921, 'eErrNoUOtpRv13', '您输入的身份证号或姓名与注册信息不符.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2599006591, 'eErrNoOtpInvalidFmt', 'OTP(一次性密码)数据接收出错,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2600024373, 'eErrNoChn_GETCONNETIONPOOL', '无法获得Chn_ConnectionPool.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2665565373, 'eErrNoERR_CREATECONNETIONPOOL', '(PORTE)无法连接认证服务器.请稍后再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2677746060, 'eErrNoAgeLimit', '年龄不符合要求.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2689292763, 'eErrNoMissmatchChannelCertifyInfo', '与服务器验证码情报不一致.请重新登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2752826906, 'eErrNoChuMgr_DBGW_RPC_CONNECTOR_FAILED', 'RPCConnector 生成失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2776303212, 'eErrNoPORTE_AHTH_TICKET_INVALID', '(PORTE)值过长.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2783163107, 'eErrNoHangameCantSnd', '无法连接认证服务器,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2834732497, 'eErrNoOtpRcved0', 'OTP(一次性密码)数据异常,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2843829309, 'eErrNoPORTE_AUTH_TICKET_DECRYPT_FAIL', '(PORTE)验证码解密失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2853329052, 'eErrNoUserBlockedEx', '您的帐号已经被封停.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2886371096, 'eErrNoSecKeyIndexOutBound', '安全验证码请求信息值超出范围.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2982798305, 'eErrNoRscKeyCantRead', '无法读取资源(resource key)文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2983036546, 'eErrNoUOtpRv23', '无法收到UOTP结果.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 2994746643, 'eErrNoERR_ISNOTINITED', '(PORTE)模块初始化未成功.(PubAuthInit()方式没有呼出或失败)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3043755624, 'eErrNoERR_INVALIDARGUMENT', '(PORTE)为NULL.(ex. Game Info. String或Out buffer为NULL时返回错误)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3096903453, 'eErrNoInvalidARSAuthServerInfo', '未找到ARS认证服务器信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3109132162, 'eErrNoPORTE_AUTH_TICKET_WRONG_FORMAT', '(PORTE)验证码格式错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3111068321, 'eErrNoUOtpRv7', '验证码不一致.(0001)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3120997932, 'eErrNoHangameUndefinedError', '系统出现未知错误,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3187861176, 'eErrNoUOtpRv3', '参数数值有误.(P002)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3203424102, 'eErrNoERR_ALREADYINITED', '(PORTE)PubAuthInit, PubSetEventCallBack API第二次呼出时通知已初始化');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3233419467, 'eErrNoHangameRcved0', '从服务器获取认证信息出错,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3263665853, 'eErrNoSecKeyAuthLoopOver', '安全验证码生成过程中发生循环错误(Loop Over).');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3277628547, 'eErrNoARSInvalidNumber', '无效电话号码');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3328639997, 'eErrNoUserBlocked', '您的帐号已经被封停.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3335173652, 'eErrNoUOtpRv22', '非正常的UOTP结果.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3372602926, 'eErrNoUOtpRv2', '必须参数未被传达.(P001)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3462926903, 'eErrNoUOtpRv6', '您尚未申请过 U-OTP.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3489353589, 'eErrNoARSInvalidName', '电话认证实名认证失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3572146307, 'eErrNoARSInvalidValue', '电话认证输入值错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3582555204, 'eErrNoERR_INVALIDUSERINFO', '(PORTE)User Info. String格式错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3658672605, 'eErrNoSecEmailToErr', '安全验证码发送失败- 收信人地址错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3685780912, 'eErrNoWebzenCertifyFailed', '验证码确认失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3795245009, 'eErrNoHangameCantRcv', '无法从服务器获取认证信息,请稍候再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3795707989, 'eErrNoCertificationInitFailed', '验证初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3804052956, 'eErrNoSecEmailFromErr', '安全验证码发送失败- 无法发出邮件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3818376230, 'eErrNoSecKeyTableNotExist', '无法找到安全验证码信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3833927220, 'eErrNoRscKeyNotEqual', '程序文件受损,请检查是否感染病毒或重新下载游戏相关文件!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3845844746, 'eErrNoChn_INVALIDARGUMENT', 'Chn_为NULL');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3853967064, 'eErrNoChn_UNEXPECTED', 'Chn_预想之外的错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3875402202, 'eErrNoHangameIdentificationFailed', '请在R2官方网站上经过本人认证后重新登录');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3890841466, 'eErrNoChn_Unknow', '未知的错误信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3891689205, 'eErrNoPORTE_AUTH_TICKET_DIFF_ID', '(PORTE)验证码登录的User ID和服务器传达的User ID不同');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3898725507, 'eErrNoPORTE_AUTH_TICKET_MAKEKEY_FAIL', '(PORTE)验证码加密失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3898744205, 'eErrNoWebzenCertifyInvalidParameter', '验证码非正常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 3991397847, 'eErrNoUOtpRv12', '您已经3次输入密码错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 4038980960, 'eErrNoSecKeyAuthFail', '安全验证码验证失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 4073406575, 'eErrNoChuMgr_DBGW_RPC_CLIENT_FAILED', 'RPCClient 生成失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 4096499770, 'eErrNoChn_AUTH_UNKONWN_CLIENT_IP', 'Chn_未知的客户端 Ip .');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 4136537690, 'eErrNoChuMgr_DBGW_LOAD_LOCAL_FAILED', '本地路径读取失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 4140913966, 'eErrNoHangameNotRealName', '您的帐号还没有通过实名认证.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10001, 4247560840, 'eErrNoPORTE_AUTH_TICKET_DIFF_IP', '(PORTE)验证码登录的Client IP和服务器传达的Client IP不同');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1398152, 'eErrNoInvalidLimitPlayTime', '特定服务器游戏时间终止');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 6636658, 'eErrNoUTGWDoNotUnregTime', '不是可取消跨服工会对战登录的时间.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 16382934, 'eErrNoInvalidRegionOptions', '错误的领地选项值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 16629501, 'eScrNo000004', '道具消失了.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 16825820, 'eErrNoAreadyItemIncSysCheck', '道具异常，道具增加系统维护中');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 29844547, 'eErrNoOverflowChaosBattlePoint', '不能超过混沌战场点数最大值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 32293317, 'eErrNoCastleNotExistStone', '守护石不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 35843248, 'eLogNoGoldItemBuyOk', '交易成功.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 43781514, 'eErrNoFsmCannotOpenStt', '无法初始化FSM的 STT.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 44042365, 'eErrNoWebzenShopGiftResult3', '超过了送礼物的限度.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 51863005, 'eErrNoShopServerInfo', '商店服务器信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 53961444, 'eErrNoDisconnectToArenaSessionOver', '竞技场已终止。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 54212101, 'eScrNo000059', '如果你太忙的话那就算啦.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 55200166, 'eErrNoCnsmRemoveItemFailed', '无法删除道具');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 61420906, 'eErrNoCharLakeMp', '法力值不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 63873873, 'eLogNoPcKick', '被踢出(Kicked).');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 64480278, 'eErrNoNotEnoughGroupUsedPoint', '技能列表点数累积量不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 79647031, 'eErrNoTeamRankHBTUserBelow', '组队一骑讨参赛者不足，已失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 85648594, 'eErrNoOtherGuildTooManyBattle', '对方公会正与多个公会宣战中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 92474104, 'eErrNoRegionInvalidCombat', 'region文件中被设置了无效的战斗信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 95744787, 'eErrNoItemNotBead', '不是符文道具');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 97373284, 'eErrNoWebzenShopGiftResult7', '不能送礼物的商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 103425388, 'eErrNoPShopExpireBeadOwn', '存在有效期限未满的符文。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 106227623, 'eErrNoCharIsTransform', '角色正在变身');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 119665369, 'eErrNoGuildAssNotMem', '您不是公会联盟成员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 121174638, 'eErrNoFierceBattleRegFailed', '竞技场激战登录失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 125100398, 'eErrNoGkillOnlyBuildOnePlace', '创建守护石的数量不能超过限制.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 127862594, 'eErrNoThreadSenderTimeout', '发送信件thread超时.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 128893561, 'eErrNoEventDungeonIngressAbnormal', '没有入场BUFF或未使用入场卷');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 129941472, 'eErrNoCharBlockChat', '禁止聊天状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 139358223, 'eErrNoCantGiftPcBangItem', '网吧专用商品是无法送礼物的.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 139753137, 'eErrNoGameGuardInfo', '缺少GameGuardInfo.ini文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 150133026, 'eErrNoCnsmSlotCntOver', '没有出售栏。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 155972310, 'eScrNo000008', '公会会长专用.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 159089396, 'eErrNoGoldItemFreeMustPackageMember', '免费金币道具只对Package的构成品有效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 160937622, 'eLogNoR2DayExpStx', 'R2特别活动开始.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 160972143, 'eErrNoCnsmMyListNotInited', '登陆列表没有初始化。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 161740347, 'eErrNoLetterCannotFetch', '无法获取信件信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 166496913, 'eErrNoNotAlliedGuild', '只能邀请所在公会或联盟成员。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 167475490, 'eErrNoHeightCantOpenFile', '无法打开高度文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 169541614, 'eErrNoSiegeGambleCantFinish', '无法结束攻城竞猜.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 170490702, 'eErrNoAlreadyOwner', '归他人拥有.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 170756592, 'eErrNoRegionInvalidSzFile', 'region文件大小无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 173635969, 'eErrNoInvalidUTGWMapInfo', '非正常的跨服公会对战地图情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 176337926, 'eErrNoHeightCantReadFile', '无法读取高度文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 177044014, 'eScrNo000055', '祝愿好运随时陪伴在你身边.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 178907388, 'eErrNoCantExitGuildWhenUTGWProgress', '跨服公会战进行期间无法退出及解散公会.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 184826220, 'eErrNoUTGWAlreadyRegsitered', '已登录了跨服公会对战列表里.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 192909014, 'eErrNoNoTicketInfo', '没有找到邀请函相关信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 193031995, 'eErrNoSnapShotFileLoadFailed', '从文件加载SnapShot数据时，发生错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 211112342, 'eErrNoGkillInsertFailed', '创建公会技能情报失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 211223272, 'eErrNoUTGWHBTCantRegDup', '一个角色无法重复参与一骑讨战斗.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 212940454, 'eErrNoItemMallTooManyNotice', '商店通知事项已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 214767838, 'eErrNoFailedInitWebzenShopMgr', '商店管理初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 221086512, 'eErrNoTransformTooManyList', '变身列表内容过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 226366149, 'eErrNoFierceBattleIsNotBattleSession', '竞技场激战比赛不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 232832567, 'eScrNo000051', '好难受……呃…呃……');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 241441326, 'eErrNoCastleCantUpdateAsset', '数据库无法更新银币信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 241888521, 'eErrNoCharOnlyFighter', '只有骑士职业可以创建公会.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 253780855, 'eErrNoCnsmItemExpireType', '是期限道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 256491606, 'eErrNoIsNotMonster', '对象不是怪物.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 268310972, 'eErrNoInvalidSnapShotTypeHeadSz', '图片尺寸不符及无法识别的图片');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 269700412, 'eErrNoCharLowLv', '等级不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 273728407, 'eScrNo000018', '五');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 277860540, 'eErrNoCantCreateSummon', '当前状态或场地无法召唤召唤兽.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 281061747, 'eErrNoRegionCantReadFile', '无法读取region文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 287449557, 'eErrNoItemExchangeDbButCantInv', '交易成功,但内部发生错误.请重新登陆游戏');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 288591643, 'eErrNoWebzenShopThrowResult1', '礼物箱内不存在该商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 289171796, 'eLogNoR2DayExpEtx', 'R2特别活动结束.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 293933671, 'eErrNoGuildLackRewardExp', '公会荣誉勋章不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 294293414, 'eErrNoRegionInvalidFileNm', '地区文件名错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 298640993, 'eErrNoAlreadyRegsterQuest', '该地区已登记。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 301048627, 'eErrNoAlreadyRegisterTeleport', '这是已经记录过的传送地点.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 301174293, 'eErrNoMissMatchTermOfEffectivity', '金币物品效果持续时间的设置与道具的设置不同.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 308963253, 'eErrNoNotEnoughAbnSiegeDfnsBenefit', '适用Base等级的守护者异常状态个数不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 311137571, 'eLogNoWebzenBillingInfo', '充值模块LOG.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 316227768, 'eLogNoQuestMakingStxed', '正在进行任务制作活动。\r点击“L”确认活动任务窗口。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 316359468, 'eErrNoUTGWCantRegOnlineGuildMemCntBelow', '在线的公会成员人数少于跨服公会对战申请最少在线人数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 322672492, 'eErrNoMMTimerFailed', '多媒体定时器设定失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 324367619, 'eErrNoGkillNotExistDefault', '没有找到基本公会技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 327834237, 'eErrNoInvalidSiegeDfnsBaseLv', '不符合的守护者异常状态等级基本值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 328616815, 'eScrNo000045', '前往艾斯本镇。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 330916951, 'eErrNoRegionCantOpenFile', '无法打开region文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 331127841, 'eErrNoSilverIsSeizure', '银币道具被扣留');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 344609303, 'eErrNoCnsmAddAccountFailed', '无法添加预付金');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 344916222, 'eErrNoGkillNotExistRoom', '房间中没有该技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 349527899, 'eErrNoGSExchangeNotExist', '不存在的交易目录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 350182234, 'eErrNoInvalidTrCycle', '此操作发生问题');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 352030582, 'eScrNo000041', '尚未完成任务');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 352076737, 'eErrNoTeamBattleRegisterLowLevel', '等级不足，无法申请。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 352495799, 'eErrNoUTGWEnterMemberBelow', '没有达到跨服公会战可开始的人数因此被进行了败北处理.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 353524581, 'eErrNoWebzenShopUnknownError', '异常错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 366165683, 'eErrNoBeadHoleNot', '无空余符文槽');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 367154755, 'eLogNoQuestNotComlpete', '未完成任务。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 378715637, 'eErrNoSpotCastleNotOwner', '没有占领城池/营地.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 378870510, 'eLogNoR2DayExpStxed', 'R2特别活动正在进行中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 382841529, 'eErrNoGkillTooFar', '距离过远.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 391417318, 'eErrNoPShopChangeItemCnt1', '个人商店个数变更失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 392463347, 'eErrNoRefineInfo', '组合信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 397617364, 'eErrNoFailedWebzenScriptLoad', '截图及DOWNLOAD服务器读取失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 401361320, 'eErrNoChinaToxError1', 'ID不存在');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 409260154, 'eErrNoRacingResultSerchInvalid', '结果不一致');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 419128836, 'eErrNoNotGuildSkill', '不是公会技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 422503690, 'eErrNoItemInvalidId', '非法的道具ID.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 432126197, 'eLogNoSecurityUpdate', '安全维护系统已经升级.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 433835219, 'eErrNoTransformLackList', '变身列表内容必须有两个以上.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 434540476, 'eScrNo000014', '一');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 434729357, 'eErrNoIsObserverMode', '旁观模式');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 438306628, 'eScrNo000049', '请帮我买个面包吧！');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 439960929, 'eErrNoCastleRepairGateFree', '城门已经免费修理完毕.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 446829316, 'eErrNoPhQoolCannotPush', '无法装入物理Qool.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 448625795, 'eErrNoConsignmentNotExistSeller', '无买家信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 448872125, 'eErrNoEventDungeonAccpetCancel', '你的队友还没准备好，请等待队友。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 452580228, 'eErrNoCharIsDeleting', '正在删除角色.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 457456167, 'eErrNoCantLogoutAllPc', '无法另所有PC登出.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 457768968, 'eErrNoGSExchangeNoMoreEntry', '没有可交易的物品');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 475939802, 'eErrNoDisconnectToUTGWSessionOver', '跨服公会对战比赛已结束.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 480499410, 'eErrNoGuildAssRejectEnter', '申请加入公会联盟被拒绝.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 487500238, 'eLogNoSnapShotItemList', 'SnapShot确认道具情报。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 492717523, 'eErrNoGuildRecruitMemTooMay', '加入申请者过多');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 496603334, 'eErrNoGoldItemCantBuyPub', '金币道具购买中发生错误.(PUB)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 498869445, 'eErrNoGSExchangeInvalidSilver', '低于银币最少交易量.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 501092436, 'eErrNoLoginingInternalError1', '认证过程中内部发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 503061942, 'eErrNoInvalidArenaMapInfo', '非正常竞技场地图信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 506309877, 'eErrNoWebzenShopBuyResult10', '无法使用金币\r请在账号中心管理中设置。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 508066237, 'eErrNoUserIdNotCertifySelf', '请确认身份证号码后再次尝试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 510238403, 'eErrNoItemMallTooManyCateId', '金币道具的ID种类过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 511965800, 'eErrNoCharParalyzed', '你处于麻痹状态,无法行动.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 512445349, 'eScrNo000010', '公会已经存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 513480490, 'eErrNoMustHaveSettingPswd', '请设置相应级别密码后使用.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 519975559, 'eErrNoConsignmentInvalidItem', '请输入要登录物品的附加信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 522418250, 'eErrNoCantInitChaosBattleMgr', '无法初始化混沌战场管理器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 525468044, 'eErrNoSnapShotFileInvalidFormat', 'SnapShot文件格式有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 529622583, 'eErrNoNotConnectedShopServer', '无法连接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 532526611, 'eErrNoDiscipleNotMaster', '这不是师父.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 538267409, 'eErrNoCantChangeSubMasterInSiege', '攻城战期间无法更改公会副会长.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 543360792, 'eErrNoSkillPackNoHave', '您没有获得该技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 544265885, 'eErrNoBossBattleRegFailed', '竞技场BOSS战登录失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 548837740, 'eErrNoCantEventMsgSetItem', '无法活得活动物品信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 555747170, 'eErrNoGkillNotActiveNode', '非激活节点.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 558086601, 'eErrNoChaosBattleEntranceFailed', '进入混沌战场失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 558338496, 'eScrNo000082', '您可参加的HappyBean活动次数如下:(最多三次)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 558879519, 'eErrNoNotAcquireItem', '未获得道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 563546055, 'eErrNoItemNotHolePunched', '无空余符文槽');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 564144621, 'eScrNo000065', '刚刚开始就要放弃吗?');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 568803731, 'eErrNoTrapExistNear', '周围有陷阱.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 569845215, 'eErrNoCantUseMaterial', '无法使用材料。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 570549632, 'eErrNoCastleFullAsset', '银币已经到达上限.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 575009022, 'eErrNoRegionQuestCannotLoad', '无法获取地区任务信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 577151253, 'eScrNo000038', '您虽然已经申请了安全验证卡, 但是尚未进行激活.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 582206930, 'eLogNoBeginWorldGame', '游戏数据(Game DB)库已经初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 585246104, 'eErrNoInvalidGuildMember', '你不是公会成员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 586198554, 'eErrNoGkillNotBuildPreNode', '之前的公会技能节点尚未完成研究.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 590671236, 'eErrNoWebzenShopBuyResult1', '持有金币不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 608324583, 'eErrNoRegionOptionValue', '领地选项信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 609823133, 'eErrNoWebzenShopBuyResult5', '停止出售.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 613540607, 'eErrNoInvalidPosition', '位置无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 617987013, 'eLogNoSnapShotDataFileHeadInfo', 'SnapShot数据文件情报。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 621314775, 'eErrNoTargetTooFar', '与对方距离过远.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 622510989, 'eErrNoScriptSettingFail', '设置脚本失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 626116494, 'eErrNoInvalidNpcRole', '不符合的NPC号码.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 626507233, 'eErrNoDisconnectedWebzenShopServer', '与商城服务器连接结束.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 627649040, 'eErrNoGuildNotSaveChangedInfo', '游戏数据库无法记录公会变更信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 631915630, 'eErrNoEventDungeonEnter', '入场条件不符，无法进入。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 632808274, 'eErrNoGuildRecruitPremiumTooMay', '高级公会招募申请者过多');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 640226281, 'eErrNoGuildPointNotEnough', '缺少公会点数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 640308697, 'eScrNo000086', '进行中');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 641173483, 'eErrNoItemInvalidUseType', '使用(运行)Type无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 644160023, 'eErrNoPhCannotInitSimulator', '无法初始化物理模拟器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 644940167, 'eErrNoNeedMoneyNotSpend', '支付过程中发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 649227001, 'eErrNoCastleNotSick', '没有要修理的地方.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 650497215, 'eErrNoWaveCannotGather', '无法找到WAVE文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 651030004, 'eScrNo000061', '真的就这么走了吗?');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 652683600, 'eErrNoPathEngCantOpenTok', '无法打开tok文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 658161411, 'eErrNoCnsmInvalidCount', '数量不符。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 658602817, 'eErrNoWebzenShopUseResult2', '此道具为仅供指定网吧用户收领.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 663553432, 'eErrNoGoldItemPackageCantMember', '金币Package的构成品不能成为金币Package.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 672556486, 'eScrNo000069', '就快完成了,真的要放弃吗?');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 679107151, 'eErrNoNotAgitOwner', '您所在公会未拥有公会议事厅.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 683313260, 'eErrNoAlreayItemMallListChecking', '正在检测道具商店物品列表');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 683316108, 'eErrNoDiscipleNotWaitState', '没有待处理的拜师申请.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 686674116, 'eErrNoPortalInvalidPos', '入口位置不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 688088019, 'eErrNoInvalidCouponItemInfo', '礼券信息有误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 688689889, 'eErrNoNoMoreCreateSummon', '无法再召唤召唤兽.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 690889445, 'eErrNoTeamRankMapIsEmpty', '组队战场不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 703747976, 'eErrNoAchieveCannotUseThisServer', '该区无法使用。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 707337282, 'eErrNoCharIsCastingSkill', '正在施放技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 712532460, 'eErrNoItemNotEquipRideRing', '只可佩带1个德拉克戒指。
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 718252675, 'eErrNoDiscipleMasterNotExist', '目前没有拜师.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 720995809, 'eErrNoArenaTowerInsertFailed', '圣物情报追加失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 724411706, 'eErrNoLetterLimit', '发信过于频繁');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 725005351, 'eErrNoCantUptChaosBattleCastleGateInfo', '无法更新混沌战场城门信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 730193622, 'eErrNoItemNotTarget', '没有指定道具使用对象.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 735112510, 'eScrNo000034', '开始重新设定任务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 737469075, 'eErrNoNotExistLoginPos', '登录坐标不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 740825654, 'eErrNoIsNotPcBangUser', '不是网吧用户.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 741955832, 'eErrNoMacroDetectRespFailAbnormal', '发生错误.已确定您是非法程序使用者,一段时间内将限制您使用游戏');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 743547945, 'eErrNoPosInvalid', '位置错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 750594343, 'eScrNo000030', '等级与本竞技场不符.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 757231729, 'eErrNoUnitedGuildWarMgrInitFailed', '跨服公会对战管理初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 757393881, 'eErrNoPreviousRecall', '转移申请，未得到响应.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 759267367, 'eErrNoChaosSilverLack', '混沌银币不足。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 762834647, 'eErrNoCharNotExistMerchant', '此商人不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 764770642, 'eErrNoEventDungeonMaster', '只有队长可以申请。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 770663862, 'eErrNoWebzenShopBuyResult9', '购买商品限制时间错误，请稍后再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 774229982, 'eErrNoNeedMoneyInvalidType', '商品种类无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 777763406, 'eErrNoCharNotExist', '没有游戏角色.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 788463303, 'eErrNoInvalidNpcScript', '脚本有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 790735649, 'eErrNoGuildUTGWRegister', '跨服公会战待机中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 793782302, 'eErrNoIsNotOwner', '你不能使用这件物品');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 793923304, 'eErrNoDiscipleMgrCantOpen', '不能初始化师徒管理员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 800135607, 'eErrNoArenaIsNotRegisterPc', '无法登录。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 806355564, 'eErrNoNotEtelrGuildAgit', '艾泰尔公会议事厅不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 806808131, 'eErrNoInvalidTeamBattleNpc', '不是团队战场管理员。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 807247013, 'eErrNoNotExistBossBattlePcInfo', '竞技场BOSS战PC信息不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 809083942, 'eErrNoCharIsRiding', '角色正在骑乘坐骑.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 817386814, 'eErrNoInvalidUTGWSessionState', '非正常的跨服公会对战状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 821021155, 'eErrNoPcSkillTreeCannotFetch', '你还没有激活个人技能列表.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 821782812, 'eErrNoWebzenShopScriptInvalidListPtr', '无法读取商品信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 822890863, 'eErrNoSecurityCantUpdate', '无法升级安全维护工具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 822957191, 'eScrNo000079', '后悔的话下次再来也可以.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 823486703, 'eErrNoGkillInvalidBuildPath', '公会技能研究路径不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 827855356, 'eErrNoCantOpenDataMember', '无法初始化data member.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 829069810, 'eErrNoPShopIsNotConfirmItem', '个人商店内销售的商品只能是已鉴定过的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 835996382, 'eErrNoBidOverlap', '您已经参加了公会议事厅的竞拍.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 837517077, 'eErrNoInvalidChaosBattleMapCnt', '不存在的混沌战场地图.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 837864736, 'eErrNoInvalidWebzenStorageInGameItemInfo', '异常单位商品信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 838705852, 'eErrNoLowBidMoney2', '您的竞拍金额低于目前竞拍金额.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 838813704, 'eErrNoHeightInvalidFileNm', '高度文件名错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 843388087, 'eErrNoCnsmItemNotExist', '道具不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 844030390, 'eErrNoInsertGuardianInfoFailed', '混沌战场守护神信息生成失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 845494403, 'eErrNoDisconnectToChaosBattleJoin', '因接触混沌战场与服务器连接中断');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 845985808, 'eLogNoPathEngLoadedTok', 'path engine的 tok已经加载(load).');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 852221055, 'eScrNo000024', '发生错误:请联系游戏客服人员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 857026910, 'eErrNoGkillNodeInvalidSkill', '公会技能节点的技能无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 859634378, 'eErrNoGuildAssCantCreate', '无法生成公会联盟.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 861436381, 'eErrNoPcInvenQslotCannotLoad', '背包，快捷栏无法呼出');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 879653753, 'eErrNoQuestInvalidNo', '任务编号错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 881553639, 'eErrNoInvalidStoreGrade', '没有相应等级的公会仓库使用权限.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 881918330, 'eErrNoCnsmUnknownCategory', '无法识别。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 882755918, 'eErrNoUTTournamentGuildCntBelow', '擂台赛参加公会数不足因此取消擂台赛.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 894162318, 'eErrNoCnatApplyAbnormal', '不能在应用适当效果了');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 896554720, 'eErrNoAlreadyPShopOpen', '个人商店已经开设.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 900077670, 'eScrNo000020', '同意');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 904215350, 'eErrNoInvalidString', '字符串无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 910552202, 'eErrNoGSExchagneInvalidCond', '金币交易条件不充足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 912205159, 'eErrNoAlreadyRegPShopUser', '您开启个人商店.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 912465833, 'eErrNoMissMatchWebzenBillingGUID', 'BILLINGGUID情报不同。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 914033468, 'eErrNoTeamBattleState', '打开团队战现况时出错。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 915160326, 'eLogNoGkillBuilded', '正在生成公会技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 928762966, 'eErrNoItemCantDropByMonster', '该怪物无法掉落该道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 929882946, 'eErrNoCnsmInvalidSubstrOnCategory', '列表里不存在获得邀请文字栏的道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 933449013, 'eErrNoPartyAlreadyEnter', '已经加入了队伍.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 940493692, 'eErrNoTeamRankDoNotRegTime', '组队战申请期限已过。无法申请。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 942681323, 'eErrNoNotExistsGuildName', '该公会不存在');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 944802230, 'eErrNoItemCantFindBow', '没有找到弓.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 948068490, 'eErrNoQuestMakingLimit', '任务制作栏已满，无法生成。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 951785644, 'eScrNo000075', '以后有时间再来吧!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 953569020, 'eErrNoPShopItemBuyCntToMany', '购买道具个数超过个人商店限定.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 953768954, 'eErrNoPShopClosed', '目前尚未开设个人商店.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 954960112, 'eErrNoShopSvrCantConnect', '无法连接商城服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 956328708, 'eErrNoNotExistsTerritory', '不存在的领地.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 957727013, 'eErrNoPShopChangeItemInfoFail', '删除个人商店列表失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 958856733, 'eErrNoTeamRankCantSendPcInfoReq', '组队战服务器中加载该角色的信息失败，请重新尝试。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 959602488, 'eErrNoGuildAssFullMem', '公会联盟成员过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 964911439, 'eErrNoInvalidTeamRankSessionState', '组队战状态有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 972580480, 'eErrNoUTGWCantRegMaxRegCntOver', '超过了跨服公会对战每日可登陆次数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 973427852, 'eErrNoGuildRecruitCantOpen', '公会招募系统开启失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 977422774, 'eErrNoCantActionWhenTartgetPcLogin', '目标PC正在登录中，无法使用该功能。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 982954576, 'eErrNoNotUseItemState', '无法使用.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 983762140, 'eErrNoUTGWDoNotRegHBTInfoState', '一骑讨参赛者情报登录只有在登录待机时才能进行.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 985091167, 'eErrNoMoneyCantChangeButIng', '转帐过程中发生错误,系统正在处理.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 987072071, 'eErrNoGkillAlreadyBuildNode', '已经完成研究或正在研究中的公会技能节点.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 998084692, 'eScrNo000028', '登陆过程中发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1000343314, 'eErrNoItemNotUseStateOrPos', '当前状态或场所内无法使用此道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1019115048, 'eErrNoCalendarGroupInvalidNm', '无效名称。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1020143743, 'eErrNoTeamRankRegPartyFailed', '跨服公会战服务器登录中，发生错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1023907422, 'eLogNoCharBearCnt', '生成的NON-PC个数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1026010028, 'eErrNoUTGWHalidomInsertFailed', '圣物情报追加失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1030143530, 'eErrNoRegionUnknownFile', '未知region文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1035693409, 'eErrNoConsignmentFull', '商品栏已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1037497040, 'eErrNoGSCantRollback', '金币交易不能恢复.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1052827113, 'eLogNoCharReformSuccessAll', '受理成功, 请重新登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1056296854, 'eErrNoConsignmentNotExistItem3', '没有可上架出售的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1064506730, 'eErrNoAlreadyRegsterPc', '角色已经登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1066001552, 'eErrNoChatFilterSendFail', '无法给屏蔽玩家发送信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1068995210, 'eErrNoFailedShopScriptVersionCheck', '商城脚本版本确认失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1070904844, 'eErrNoItemCantEquipSpear', '装备盾牌后无法装备长矛及其他双手武器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1071045813, 'eScrNo000071', '后悔的话下次再来也可以.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1071553370, 'eErrNoUTGWMapInfoInsertFailed', '跨服公会对战地图登录失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1078278277, 'eErrNoMissMatchSnapShotDataFileSvrInfo', 'SnapShot数据文件生成服务器和数据生成服务器不符。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1083698780, 'eLogNoTeamRankInfo', '跨服工会战情报。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1098829504, 'eErrNoDiscipleNotExist', '目前没有师徒关系.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1100149228, 'eErrNoGuardianProtectDis', '成功防御住对守护神的进攻,攻击方阵营将回到服务器');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1100495154, 'eErrNoNotExistOccupyCastle', '没有占领中的城池.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1103179476, 'eErrNoCastleAlreadyOpenGate', '城门已经打开.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1106093238, 'eErrNoAiAssignWrong', 'AI分配错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1112600509, 'eErrNoDiscipleNotMember', '非师门内成员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1117858032, 'eScrNo000021', '拒绝');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1120030671, 'eErrNoCantSellPShop1', '被封的道具不能在个人商店中出售.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1124570821, 'eErrNoCnsmDataPoolOpenFailed', '拍卖行数据生成失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1129309473, 'eErrNoCantUnregisterTime', '现在不是可以取消跨服公会战对战登录取消的时间.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1130229300, 'eErrNoGSExchangeRegOver', '不能再登陆金币交易信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1130393813, 'eErrNoEventDungeonMemCnt', '进入封印地区条件不足，需要更多队员。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1136328162, 'eErrNoCnsmInvalidNmOrdNo', '输入关键词有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1145426376, 'eErrNoCnsmPaused', '拍卖行已终止。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1151189026, 'eErrNoPShopCantRegisterEquip', '装备中的物品无法在个人商店中出售.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1171057897, 'eScrNo000025', '已经报名.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1175086097, 'eScrNo000078', '想通后再来吧.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1183403402, 'eLogNoHeightLoadedFile', '高度文件已经加载.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1187121555, 'eErrNoRegionInvalidTerritory', 'region文件被设置了无效的领地信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1187406735, 'eErrNoCastleNotExistTower', '守护塔不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1188314856, 'eErrNoDisconnectToTeamRankSessionOver', '组队战已终止。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1190119457, 'eErrNoNoToPc', '无法找到该对象.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1194524231, 'eErrNoTeamRankCantUnregisterTime', '无法取消组队战登陆。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1196577734, 'eErrNoAchieveListCannotFind', '无法找到该业绩。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1208308683, 'eErrNoPShopCantRegistSealItem', '不能登陆个人商店购买目录里被封的道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1210671583, 'eErrNoSummonCannotLoad', '无法获取召唤兽信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1215075369, 'eErrNoSummonTooManyList', '召唤兽列表内容过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1220781101, 'eErrNoItemCantRollback', '无法恢复物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1221368335, 'eErrNoFriendTooMayMember', '好友列表已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1221717027, 'eScrNo000070', '现在要放弃的话真的有些可惜啊……');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1224356995, 'eErrNoBroadcastPaymentItemLack', '支付聊天服务的费用不足');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1224886088, 'eErrNoCnsmInvalidCntForNonstack', '道具数量有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1229396054, 'eErrNoCnsmCannotRegister', '无法登录。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1239898498, 'eErrNoGSExchangeInvalidStatus', '不能交换的金币交易目录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1240661760, 'eErrNoConsignmentNotExistItem2', '没有可取消出售的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1249089236, 'eErrNoTeamBattleStateNotRegister', '团队战不在进行中。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1262721400, 'eErrNoTeamBattleRegisterFigter', '已申请参与团队战。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1277268104, 'eErrNoGSExchangeCantExchangeGame', '金币交易中发生错误.(GAME)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1277475481, 'eErrNoDiscipleNoMemberInfo', '不存在任何师徒信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1278989312, 'eErrNoGkillNodeNotBuilded', '研究尚未完成的公会技能节点.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1282322751, 'eErrNoCantActionAdmin', '管理者无法实现此功能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1283104962, 'eScrNo000029', '没有古铜钱的话无法参加竞猜.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1288292213, 'eErrNoArenaIsNotEnterableTm', '竞技场无法入场的时间。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1290568034, 'eErrNoDisconnectToUTGWSvrJoin', '为了跨服公会对战登录将与服务器断开连接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1303473875, 'eErrNoIsNotBattleZone', '您虽然是团队战成员但对战开始后您处于非战斗区域.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1304378366, 'eErrNoSnapShotFileInvalidVersion', '非正常的SnapShot文件版本。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1305697481, 'eErrNoHeightCantParseFile', '无法分析高度文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1310478945, 'eErrNoItemNotApplyRadius', '此道具不是范围性道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1317116762, 'eErrNoNotConnectedBillingServer', '无法连接BILLING服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1318277317, 'eErrNoPcLevelLow', '角色等级低.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1329083733, 'eErrNoAlreadyGoldItemBuying', '正在结算中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1329311789, 'eErrNoItemCantSell', '此道具无法出售.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1331056130, 'eErrNoChaosPointLack', '混沌战场点数不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1334388548, 'eErrNoHeightCantOpen', '无法初始化高度信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1335540018, 'eErrNoSecurityTimeoutKey', '规定时间内安全认证KEY没有响应.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1337731130, 'eScrNo000074', '很高兴见到你.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1339837601, 'eErrNoOverflowHonorPoint', '超过了所需的名誉点数');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1340114408, 'eErrNoPcRestExpCannotLoad', '无法加载休眠经验值信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1342311735, 'eErrNoTeleportCannotFetch', '无法获取传送信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1342897912, 'eErrNoExchangeAlreadyOk', '交易已经顺利完成.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1352087745, 'eErrNoBanquetHallHave', '已经拥有公会豪华礼堂.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1354931841, 'eErrNoGuildRecruitNotExist', '公会招募情报不存在');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1357564677, 'eErrNoArenaIsNotEnterableNeedMoneyTm', '定额/定量时间不足无法参与。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1361223745, 'eErrNoAlreayUsedTryAgain', '其他玩家正在使用，请稍候再试。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1361929551, 'eScrNo000087', '等待结束');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1372249442, 'eScrNo000060', '你看起来好像很忙.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1375031719, 'eErrNoNotInAgit', '现在所在位置为公会议事厅.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1380358983, 'eErrNoEventDungeonBossLive', '进入封印地区条件不足，请先处理守护者。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1398675723, 'eErrNoWebzenShopBuyResult4', '出售时间结束.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1401706292, 'eErrNoIsSetSiegeDate', '攻城日期已被设定。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1402553025, 'eErrNoMacroDetectRespFail', '发生错误.连续3次错误输入时帐号使用受到限制');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1408327448, 'eErrNoCharTooFarMove', '移动距离过远.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1426731005, 'eErrNoDisconnectToTeamRankSvrJoin', '为了登录组队战服务器，断开当前服务器连接。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1430713900, 'eErrNoTargetTimeEnough', '剩余的有效期限还充足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1432457603, 'eScrNo000039', '安全验证卡正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1435594829, 'eErrNoGoldItemCateIdNotExist', '不存在的金币道具的ID种类.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1438020858, 'eErrNoPetitionMgrOpenFail', '无法初始化投诉系统.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1447076182, 'eScrNo000083', '将豆兑换成HappyBean.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1447222499, 'eErrNoRefineInvalidCombi', '组合不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1447477800, 'eErrNoGoldItemMustSetCashItem', '金币道具必须用现金道具设置.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1453377705, 'eErrNoUTGWHBTUserNotGuildMember', '一骑讨参与者情报中存在不是公会成员的玩家.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1453791611, 'eScrNo000064', '这个任务虽然很轻松但是也要小心啊！');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1455639651, 'eErrNoItemAlreadyEquip', '已经装备了其他物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1455700998, 'eErrNoPShopCantOpenTooManyMoney', '钱太多无法开设个人商店。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1457297196, 'eErrNoAlreadyRegisterTower', '守护塔已经登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1460492921, 'eErrNoGuildAssCantKickSelf', '您无法将自己从公会联盟中移除.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1469559730, 'eErrNoAdminInvalidItemEndDate', '没有有效期或有效期设置错误的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1478460509, 'eErrNoOccupyErrorAtBillingServer', 'BILLING服务器处理时发生异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1479045151, 'eErrNoNotExistGuildInfo', '公会情报不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1479242349, 'eErrNoCantSaveConcurToHangame', '无法记录同时在线人数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1482303145, 'eErrNoItemCantEquipShield', '装备长矛或其他双手武器后无法装备盾牌.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1486383069, 'eErrNoUnitedGulidWarCfgInvalid', '非正常的跨服公会对战设定价格.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1487752053, 'eErrNoLetterRefuse', '收信人拒绝收信的状态');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1489047449, 'eErrNoItemAlreadyConfirm', '这是已被鉴定的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1491095189, 'eErrNoSkillDelay', '使用等待时间剩余.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1493781499, 'eErrNoRegionCantSearchFile', '无法找到地区(region)文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1504589761, 'eErrNoDisconnectToChaosBattleQuit', '因接触混沌战场与服务器连接中断');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1505111196, 'eErrNoCnsmLackMemory', '拍卖行关联储存量不足。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1507803373, 'eErrNoAlreadyFierceBattleGameStart', '竞技场激战已开始。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1513309977, 'eErrNoAlreadyTeamRankGameStart', '组队战已开始。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1519076537, 'eErrNoNotExistPShopItemList', '个人商店列表内不存在要出售或要购买的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1525175584, 'eErrNoWebzenShopBuyResult8', '已超出活动商品购买次数限制.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1532088512, 'eErrNoEventSysMgrCantLoad', '未能读取活动系统信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1538922929, 'eScrNo000031', '队伍成员已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1539253243, 'eLogNoUnitedGuildWarStxInfo', '跨服公会对战开始情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1542979073, 'eErrNoWebzenUnknownProductType', '无法辨识的商品类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1547668071, 'eErrNoCantFindBillingInfoFile', '未找到BILLING服务器配置文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1548456058, 'eErrNoAlreayItemIncSysChecking', '道具增加系统正在维护中。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1555322076, 'eErrNoTeamRankEnterMemberBelow', '队友人数不足，已失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1557650856, 'eScrNo000035', '任务已经重新设定');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1558508641, 'eErrNoCharStealthed', '正处于隐身状态中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1561551890, 'eErrNoCnsmDataNotExist', '拍卖道具不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1565378361, 'eErrNoCantExchangeItem2', '交易对象处于透明状态,无法进行交易.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1570891208, 'eLogNoRegionLoadedFile', 'region 文件已经加载.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1571845502, 'eErrNoRaidAlreadyStart', '特殊战场正在进行中，无法进入。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1576715279, 'eErrNoPartyMgrCantOpen', '无法初始化组队mgr.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1577008570, 'eErrNoConsignmentCantBuyItem', '不能购买自己销售的商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1578527888, 'eErrNoInvalidHeight', '高度错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1579331603, 'eErrNoAchieveCoinCreateFail', '生成战币失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1583816214, 'eErrNoNotExistChaosBattleIngress', '入场卷信息错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1584195770, 'eErrNoPartyNotEnterSelf', '无法邀请自己加入队伍.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1587819262, 'eErrNoCharInvalidAiType', 'AI type无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1594971472, 'eScrNo000068', '已经走到这一步了,却又放弃,实在是替你惋惜啊.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1601418696, 'eErrNoCastleCantSieging', '攻城战中无法执行.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1605547454, 'eErrNoAlreadyTicket', '已经获取目标信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1606086754, 'eErrNoCnsmFeeNotEnough', '手续费不足，或无法登陆。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1613432194, 'eErrNoArenaCantSendPcInfoReq', '在竞技场服务器内该角色加载失败。请重新连接。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1614032163, 'eErrNoNoGuild', '该公会不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1615191545, 'eErrNoNotExistTeamRankMapInfo', '不存在组队战详细地图。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1621217562, 'eErrNoPcClassNotMatch', '职业不一致');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1623152093, 'eErrNoFierceBattleCantRegMaxRegCntOver', '竞技场激战超过每日可登陆的最大次数。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1623750249, 'eErrNoSecurityCantCreateKey', '无法生成安全认证KEY.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1625799916, 'eErrNoSkillInvenEmpty', '不存在技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1629748164, 'eErrNoGkillNodeOnlyOneBuild', '每个玩家只能研究一个公会技能节点.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1629782475, 'eErrNoNotExistUTGWMapInfo', '不存在跨服公会对战地图的详细情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1632772041, 'eErrNoPcNotSaveInvenQSlot', '背包，快捷栏未储存');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1634817335, 'eErrNoPathEngNotSupport', '不支持地区的Path数据');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1636930579, 'eErrNoBossBattleIsNotBattleSession', '竞技场BOSS战比赛不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1637080115, 'eLogNoWaveReloaded', 'WAVE文件已经被重新加载.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1645741787, 'eErrNoSkillGetFail', '无法再获取技能。
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1646607597, 'eErrNoSkillPackInvalidId', '技能列表ID不符.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1657818990, 'eErrNoCharBlockProhibitedWord', '您已发送3次不合法的聊天内容，聊天暂时被禁止。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1671348058, 'eErrNoCharCantTakeItem', '此角色无法获得道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1677508576, 'eScrNo000040', '获得经验值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1683325354, 'eErrNoNotExistUserInfo', '用户信息不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1685654570, 'eErrNoWebzenBillingConWaitFailed', '等待连接失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1687256057, 'eScrNo000044', '如果有需要我的地方，请随时来找我!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1687317845, 'eErrNoIsNotUTGWTournamentPartTeam', '不是参赛公会.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1688595932, 'eErrNoUserCntFull', '已超过入场人数上限。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1689005860, 'eErrNoRegionInvalidTeleport', 'region文件中被设置了无效的传送信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1692450223, 'eErrNoMonsterSpotCannotLoad', '无法获取怪物再生信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1694870936, 'eErrNoMacroDetectCannotLoad', '不能下载非官方程序限制内容.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1699589294, 'eErrNoChinaToxErrorUnknown', '防沉迷系统未知错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1699894204, 'eErrNoWebzenBillingLogoutIng', '\r服务器连接延迟较高。\r请稍后再试。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1700677954, 'eErrNoInvalidChaosBattleFieldSvrInfo', '非正常的混沌战场域服务器数据.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1705524953, 'eErrNoInvalidChaosBattleConFieldCnt', '不存在可接入的混沌战场服务器的域服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1706241750, 'eErrNoGuildAssNotBoss', '您不是公会联盟会长.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1708450019, 'eErrNoTeamBattleUserAllDie', '团队战所有成员已经死亡.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1710693312, 'eErrNoCharUnknownClass', '未知职业.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1715347131, 'eLogNoFierceBattleInfo', '竞技场激战状态信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1733467881, 'eErrNoCnsmRemoveDataFailed', '无法删除拍卖道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1733817089, 'eScrNo000019', '失败了,下次再努力吧!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1735141487, 'eErrNoFriendCannotFetch', '无法获取好友列表.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1744484923, 'eErrNoPcCashItemCantFetch', '无法读取你的电脑里的收费道具信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1747285707, 'eErrNoRegionInvalidPos', 'region文件中不存在这个位置,位置无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1747704248, 'eErrNoCnsmInvalidPrice', '价格不符合。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1749528787, 'eErrNoHeightCantSearchFile', '无法找到高度文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1751233774, 'eErrNoFriendCannotNotifyLogin', '无法通知好友我的上线信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1754846783, 'eErrNoItemEquipped', '该物品已经装备.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1755027802, 'eErrNoPrevSkillNotDev', '前置技能尚未研究或未激活状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1757306079, 'eLogNoCharReformSuccessPart', '受理部分成功,请重新登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1762397321, 'eErrNoCantReflectShortMp', 'MP不足,无法使用反射技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1770530649, 'eErrNoGoldItemOnlyPcBang1', '此道具为仅供指定网吧用户收领.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1770814259, 'eScrNo000011', '等级不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1772463759, 'eErrNoGSExchangeInvalidType', '不是金币交易时间.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1774205922, 'eErrNoDiscipleIsExist', '已经收过徒弟了.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1776103223, 'eErrNoAiCannotLoad', '无法获得AI信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1776347166, 'eErrNoGoldItemCantBuyNotDisplayItem', '未标记金币道具不能购买');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1786727099, 'eErrNoCnsmItemIdMismatch', '道具名不同。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1786872024, 'eErrNoNoEvCouponInfo', '支付限制的服务器或礼品券信息不存在');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1789966305, 'eErrNoServantInvalidName', '不正确的宠物名字。
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1798158899, 'eErrNoInvalidSiegeMinUserCnt', '攻城公会人数错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1805100181, 'eErrNoCnsmAddItemFailed', '无法添加道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1811244290, 'eErrNoCnsmItemNotEnough', '道具不足。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1811836216, 'eErrNoInvalidSnapShotDummyDataSz', '非正常的SnapShot数据。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1817439735, 'FNL_ERRNO(eErrNoBossBattleIsNotEnterableTm', '无法入场的时间。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1831286738, 'eScrNo000048', '好饿啊! 好饿啊!.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1833813493, 'eErrNoInvalidChaosBattleOccupyInfo', '异常的混沌战场占领情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1834490297, 'eErrNoCharChatLvLimit', '不满足级别限制条件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1838974027, 'eErrNoNotExistFierceBattleMapInfo', '竞技场激战地图详细信息不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1841549363, 'eErrNoRegionUnknownTerritory', '未知领地.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1841923620, 'eErrNoNotEnoughUsedPoint', '使用点数不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1848053884, 'eErrNoApplySilence', '目前处于沉默状态');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1848947263, 'eErrNoCnsmSlotFull', '拍卖栏达到上限');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1852984109, 'eErrNoInvalidAbnSiegeDfnsBenefit', '守护者异常状态等级不符合.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1860288298, 'eScrNo000015', '二');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1863009099, 'eErrNoFriendCannotNotifyLogout', '无法通知好友我的下线信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1864331610, 'eErrNoCalendarLastScheduleDate', '事件已过期。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1866276036, 'eErrNoPathEngCantLoadEngine', '无法加载path engine.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1875376149, 'eErrNoArenaIsNotEnterableMarathon', '德拉克比赛申请中无法参与。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1875572899, 'eErrNoInvalidTeamRankMapInfo', '组队战地图情报有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1882475084, 'eErrNoWeaponApplyAbn', '在武器上涂毒.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1886480164, 'eErrNoDiscipleMasterCantExit', '徒弟无法驱逐师父.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1886880216, 'eErrNoFierceBattleIsNotRegisterPc', '竞技场激战PC信息不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1888963186, 'eScrNo000001', '道具不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1889058305, 'eErrNoGoldItemNotExistAtParmDb', '在Parm DB不存在的道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1891250764, 'eErrNoCastleUseSieging', '只限在攻城战中使用。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1905481643, 'eErrNoPShopCantOpen1', '无法在透明状态下开设个人商店.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1910753257, 'eErrNoIsNotExistTeleportTower', '移动传送封印石情报不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1914607400, 'eErrNoGkillMgrCantOpen', '无法初始化公会技能mgr(guild skill mgr).');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1917067304, 'eLogNoGSExchangeInfoTotalCnt', '已登陆的金币交易所信息个数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1920055475, 'eErrNoRaidStartedBaphomet', '巴普麦特特殊战场正在进行中，无法使用传送。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1920919245, 'eErrNoMonsterRoleCannotLoad', '无法呼叫DT_MonsterRole.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1925905650, 'eErrNoWebzenShopGiftResult6', '已停售的商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1927119797, 'eErrNoTeamBattleRegisterFigterOver', '无法申请。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1928563919, 'eErrNoCharCannotDelDiscipleMem', '解散师徒关系后才可删除角色.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1929328927, 'eErrNoServantGatheringStartInvalid', '无法进行采集。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1931542878, 'eErrNoPathEngCantNewShape', '无法生成外形(shape).');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1933747716, 'eErrNoItemCantStoreExpiredType', '无法将有使用期限的道具存入仓库.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1935005045, 'eErrNoNotExistBossBattleIngress', '入场卷信息错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1942892868, 'eErrNoInvalidCodePage', 'Code Page无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1950090899, 'eScrNo000058', '!!! 你不是忽悠我呢吧!? 连这点钱都没有!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1950340260, 'eErrNoLackOfMoney', '资金不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1952789954, 'eLogNoSecurityLog', '登陆安全维护系统.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1954359130, 'eErrNoUTGWMakeIngressInternalError', '跨服公会对战入场券情报生成中发生了内部错误.请稍后再次尝试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1956840081, 'eErrNoCharCantDialogTooFar', '距离过远,无法发送消息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1957405416, 'eErrNoCnsmNotOwner', '你没有拍卖物品。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1964628641, 'eErrNoIsNotExistMonster', '这个怪物不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1967027222, 'eErrNoCantInitChaosBattleInfoMgr', '混沌战场 InfoMgr 初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1969218993, 'eLogNoCharReformFailAll', '处理过程中发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1973893355, 'eErrNoWebzenShopGiftResult2', '该商品无法赠送.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1987809157, 'eErrNoReinforceNotPopSrcItem', '无法移除强化材料道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1990748311, 'eErrNoRegionNotExistPlace', '该地区不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 1994168750, 'eErrNoCantFindWebzenProductInfo', '商品信息不符.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2004028318, 'eErrNoMissMatchSnapShotData', '与SnapShot数据不同。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2012909163, 'eScrNo000005', '中奖了.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2014318884, 'eErrNoItemCannotFetch', '无法获取道具信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2032029965, 'eErrNoMacroDetectInvalidBmpFile', '非官方制约程序文件无效');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2034281952, 'eErrNoItemTooFar', '距离过远.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2048936268, 'eErrNoTeamRankHBTUserNotLogin', '组队战参加目录中含有未上线的玩家。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2052356999, 'eErrNoNotExistPartyInfo', '不存在的信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2056329710, 'eErrNoCharInvalidSlot', '游戏角色位置信息无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2058311368, 'eErrNoAdminInvalidItemEffective', '没有持续时间或持续时间设置错误的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2060395136, 'eErrNoUserBlockedEx1', '您的帐号已经被停权.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2061995681, 'eScrNo000050', '如果有需要我的地方，请随时来找我!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2062576128, 'eErrNoIncorrectPShopInfo', '个人商店信息有误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2066450572, 'eErrNoNotExistRequestWebzenShopInfo', '邀请内容不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2067116479, 'eErrNoSkillTreeNodeItemInvalidId', '技能树节点编号不符.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2073706317, 'eErrNoAlreadyExistCastleGateIndex', '已存在的混沌战场大门索引号码.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2077090730, 'eErrNoNeedMoneyInvalidPlayTime', '累计游戏时间无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2084507322, 'eErrNoWrongParam', '参数错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2093269018, 'eErrNoCastleInvalidTax', '税金无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2105292126, 'eErrNoNeedMoneyNotMoney', '没有可使用的商品,请支付后再进行游戏.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2105721939, 'eErrNoBossBattleAlreadyRegsitered', '入场券信息不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2106215096, 'eScrNo000054', '辛苦了.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2118959680, 'eScrNo000009', '您的职业不是骑士.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2120581248, 'eErrNoIsNotItemMallCheckState', '不是道具商店维护时间');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2121435631, 'eErrNoAlreadyEffect', '效果已存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2122021058, 'eErrNoTeamRankHBTCantRegDup', '无法重复参与组队战。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2123428635, 'eErrNoLastItemMallListCheckBreak', '确认道具商店列表失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2128527663, 'eErrNoCharLakeHp', '生命值不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2140120078, 'eErrNoInvalidGiftBoxInfo', '礼品箱信息有误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2143525487, 'eErrNoAlreadyGiftBoxReceiving', '礼品盒相关内容正在更新中。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2147803383, 'eErrNoWaveCannotReload', '无法重新加载WAVE文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2150024189, 'eErrNoNoGuildAccountData', '没有找到公会帐户信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2150991798, 'eErrNoDiscipleTooManyWait', '申请拜师玩家过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2154415294, 'eErrNoAchieveCoinSlotFull', '战币槽已满。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2160056958, 'eErrNoInvalidUserCnt', '可接入玩家数量设定异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2161729742, 'eErrNoPartyNotExist', '不存在的公会联盟.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2163203590, 'eScrNo000017', '四');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2165453760, 'eErrNoCnsmInvalidCategory', '不符合的类型。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2166462614, 'eErrNoServantCombineInvalid', '无法进行合成。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2172222139, 'eErrNoEmptyPswdString', '您没有输入密码.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2173015884, 'eErrNoCantLoadPcInfo', 'PC 情报载入失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2173687684, 'eErrNoGoldItemOnlyPcBang', '网吧专用道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2178418430, 'eErrNoItemMallTooManyNewItem', '新金币道具过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2181607925, 'eErrNoUnknownWebzenPcRoomPointExistType', '查询结果为无法辨识的网吧点卷.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2192281708, 'eErrNoGuildAgitHave', '已经拥有公会议事厅.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2193455981, 'eErrNoRegionCantSeekInfo', '为了获取region文件信息,无法移动到相应领地.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2194419964, 'eErrNoReceiveOnlyTownItem', '此道具仅在镇里可以购买.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2195004490, 'eErrNoAchieveListExistItem', '该业绩道具已存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2203834918, 'eErrNoPShopItemSellCntLack', '个人商店销售道具个数不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2204113324, 'eErrNoPcInfoNotExist', '无法找到你的电脑信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2210395983, 'eErrNoCantEquipWhenSlotBreak', '被破坏的装备无法进行佩戴.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2211110140, 'eErrNoRegionTooBig', 'region文件过大.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2211319264, 'eErrNoAlreadySetGuardianInfo', '对应区域守护神数据已存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2214982616, 'eErrNoItemAlreadyHolePunched', '已存在符文槽。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2217080975, 'eErrNoUserIdErrorEqualPerson', '确认身份证号码时发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2218640962, 'eErrNoNotExistOccupySpot', '没有占领中的营地.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2219779898, 'eErrNoAlreayUseOtherPlayer', '其他玩家正在使用，请稍候再试。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2223731875, 'eErrNoIsNotSetSiegeDate', '未设定攻城日期。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2229275639, 'eErrNoGuildAssNotExist', '不存在的公会联盟.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2229405917, 'eErrNoBossBattleIsNotRegisterPc', '竞技场BOSS战PC信息不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2232959464, 'eErrNoRegionLackMemory', 'region文件用内存不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2233232171, 'eErrNoContentsEventDungeonMinPc', '2人以上才能进入活动副本。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2235585555, 'eErrNoIsNotItemIncSysCheck', '不是道具增加维护期间。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2243203092, 'eLogNoEvtNtyGoldPig', '您获得了金猪.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2244921031, 'eErrNoCantUseTeleport', '传送被封印状态下无法使用传送.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2248680687, 'eErrNoCnsmItemCntOver', '道具数已超出范围。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2250076536, 'eErrNoDistIsOut', '距离过远.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2264271254, 'eErrNoTeamBattleNoSession', '团队战场不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2271356463, 'eErrNoCnsmAddItemCntFailed', '无法添加道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2273450527, 'eScrNo000013', '创建公会失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2273758158, 'eErrNoItemMallTooManyGoldItem', '你携带的金币道具太多了');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2281570896, 'eErrNoPartyNotBoss', '您不是队长.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2283850975, 'eErrNoArenaAlreadyRegsitered', '已经登录在竞技场列表中。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2285387048, 'eErrNoArenaIsNotEnterableLimitTm', '剩余时间不足无法参与。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2286906700, 'eErrNoCantFindBillingInfoFileKey', 'BILLING服务器配置文件中未找到指定Key.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2287648022, 'eErrNoUserIdNotEqualPerson', '只有同一身份证内的帐号才可使用转服服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2289590573, 'eErrNoMoneyLack', '余额不足');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2298705893, 'eLogNoSupportChaosBattleMapInfo', '您申请的混沌战场地图信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2313653347, 'eErrNoUTGWTournamentGuildInfoCantReg', '无法登录擂台赛公会情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2315255360, 'eErrNoInvalidShopServerInfo', '异常商城服务器信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2320478486, 'eErrNoRefineBeforeItem', '强化前道具出错。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2325682901, 'eScrNo000046', '前往黑土镇。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2326153115, 'eErrNoNotExistFierceBattlePcInfo', '竞技场激战PC信息不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2326995889, 'eErrNoRegionNotPassPos', '无法到达的位置.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2327254335, 'eErrNoAlreadyRequsetAtBillingServer', '前一次在BILLING服务器操作内容尚未得到相应。请稍后尝试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2330033704, 'eErrNoItemCantUse', '无法使用此物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2332350820, 'eErrNoNeedMoneyExpiredUserDay', '您选择了包月计费产品,请重新登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2333285156, 'eErrNoPcBangCheckFailed', '判断网吧用户过程发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2333957291, 'eErrNoGoldItemMissMatchValidDate', '金币道具和DT_ITEM流通期限不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2342439104, 'eErrNoNotExistArenaMapInfo', '竞技场详细地图信息不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2342483508, 'eErrNoItemCantPush', '无法给予道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2350303462, 'eErrNoCastleCantUpdateSiegeRv', '数据库无法记录攻城战结果信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2357165814, 'eErrNoCantReadSnapShotDataFile', 'SnapShot数据文件加载失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2362579095, 'eErrNoCalendarGroupMemberCntMax', '无法继续登记。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2365539304, 'eLogNoShrineOfIlluminaStxed', '可以传送至荣光圣殿.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2366863214, 'eErrNoEventDungeonContents', '没有选择要进行的活动副本。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2381504204, 'eScrNo000042', '去哪找小矮人的毛呢…?');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2392317416, 'eErrNoClientInfoCantSaveDb', '数据库里不能存属性');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2394211338, 'eErrNoCantChangeGradeSelf', '无法变更自身的等级.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2397378578, 'eErrNoChinaToxError2', '连接错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2404014375, 'eErrNoInvalidSnapShotHeadSz', 'SnapShot标题大小有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2405983543, 'eErrNoAchieveListIsNotComplete', '该业绩未完成。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2406662657, 'eErrNoUnknownDeductionType', '无法辨识的商品减少类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2409135433, 'eErrNoNewConditionNot', '无法实施的新环境.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2412195590, 'eErrNoSeizuredItem', '物品已经被扣留.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2414202585, 'eErrNoNotExistRestoreExp', '经验值已经恢复或角色无死亡记录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2420205229, 'eErrNoCharPShopLvLimit', '等级为满足开个人商店');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2420592328, 'eErrNoEventQuestCannotLoad', '无法加载任务制作情报。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2427456277, 'eErrNoCannotUseBeadItem', '无法使用卷轴。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2427807617, 'eErrNoInvalidRecvCnt', '异常领取数量.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2442886436, 'eErrNoFierceBattleIsNotEnterableTm', '无法入场的时间。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2443590893, 'eLogNoCastleOccupyStone', '守护石已经被占领.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2447767822, 'eErrNoMonsterSpotInvalidGroup', '怪物再生信息Group错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2451554539, 'eErrNoGkillNotDeveloper', '不是研究工会技能人员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2452166501, 'eErrNoFailedInitWebzenScriptMgr', '商城脚本管理初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2454738479, 'eErrNoMapNotAssign', 'Map尚未被分配.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2456118758, 'eErrNoBeadHoleFull', '无空余位置。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2458291291, 'eErrNoInvalidChaosBattleUserCnt', '可接入混沌战场玩家数异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2462640719, 'eErrNoCalendarAgreementNotEnterSelf', '无法登记。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2463633260, 'eErrNoPShopToomanyBuyingStack', '商品中存在数量超过限制的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2470075211, 'eErrNoSkillEnhancementCannotLoad', '无法获取技能强化信息。
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2474912660, 'eScrNo000056', '你已经领过一次了，每个人只能领取一次.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2482120646, 'eErrNoNotExistLoginUserList', '在登录列表信息中对象不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2492595975, 'eErrNoFierceBattleAlreadyRegsitered', '已经在竞技场BOSS战PC信息处登录。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2498359181, 'eScrNo000052', '请帮我买解毒药水!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2498985639, 'eErrNoItemMallTooManyPackage', '金币道具包过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2500456216, 'eErrNoAlreadyUsingChargeItem', '正在使用中的道具类型');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2500881104, 'eLogNoBeginWorldAccount', '用户数据库(Account DB)已经初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2502358353, 'eErrNoRefineCannotLoad', '无法获取强化信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2503060177, 'eErrNoGameGuardKeyCannotLoad', '不能获取游戏检测程序的关键值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2507330037, 'eErrNoWebzenShopGiftResult8', '此道具为无法赠送的活动商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2508859207, 'eErrNoBroadcastingBufFull', '转播用缓存已经全部满额使用!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2512460960, 'eErrNoCantGuildExitUTGWRanker', '跨服公会战里的对战公会无法进行解散.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2524669368, 'eErrNoNoToGuild', '没有找到对方公会.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2527356813, 'eErrNoGkillCantOpen', '无法初始化公会技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2530728735, 'eErrNoGoldItemInvalidPrice', '金币道具价格无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2534478711, 'eErrNoItemIsShortExpiredTime', '物品已过期或有效期不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2534936748, 'eErrNoDiscipleIsMember', '已经拜过师了.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2537425234, 'eLogNoWebzenScriptInfo', '脚本管理信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2541430230, 'eErrNoWaveCannotOpen', '无法初始化WAVE信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2547627153, 'eErrNoInvalidWebzenPkgInfo', '异常补丁包信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2549643667, 'eErrNoCharCantChgForeverHpMp', '无法永久改变生命值/法力值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2557195156, 'eErrNoNoGuildAccount', '公会帐户不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2582966087, 'eScrNo000007', '目前城门状态良好.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2585681687, 'eErrNoSiegeGambleNotBettingTm', '现在无法参加攻城竞猜.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2590695576, 'eErrNoDisconnectToDie', '因角色死亡断开与混沌战场服务器的链接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2592939694, 'eErrNoCnsmReqIntervalTooShort', '邀请太过频繁。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2594198796, 'eErrNoUserAlreadyEnterField', '在Field中已经存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2599328729, 'eErrNoDropGrpNotExist', '无法找到此Drop Group.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2602924228, 'eErrNoTeamBattleRegisterObserver', '已在观战中。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2608458301, 'eErrNoRegionCantOpen', '无法打开地区(region)文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2614964012, 'eErrNoNoInvitationTicket', '没有找到邀请函.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2628282199, 'eErrNoFsmCannotOpenMgr', '无法初始化FSM Mgr文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2630131166, 'eErrNoWebzenShopGiftResult4', '该商品剩余数量不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2639298602, 'eErrNoBoardInvalidId', '错误的系统公告帐号.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2639359804, 'eErrNoBeadNotAddableItem', '该道具无法添加卷轴。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2640398053, 'eErrNoCharInvalidSpawnPos', '非法的出生位置.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2640764230, 'eErrNoCannotRecallSelf', '无法召唤自己.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2648848174, 'eErrNoInvalidPackageMember', '礼包构成品不包含仅在镇里可以购卖的商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2653172467, 'eErrNoGuildAssNotEnterSelf', '无法邀请自己加入公会联盟.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2660838238, 'eScrNo000003', '道具已经发放.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2673615036, 'eErrNoWebzenShopGiftResult11', '购买商品类型限制错误，请稍后尝试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2673823402, 'eLogNoSiegeOn', '攻城战支持.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2674051402, 'eErrNoInvalidBillingSalesZoneType', '异常BILLING出售领域类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2677880455, 'eErrNoPShopCantOpen3', '目前状态无法开设个人商店.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2691437954, 'eErrNoServantLevelLow', '等级不足无法进化。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2693880373, 'eErrNoConsignmentNotExistItem4', '商品列表内该物品不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2694876817, 'eErrNoCastleNotGateOwner', '这是其他地区的城门.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2700624426, 'eErrNoCharWebbed', '被蜘蛛网缠住了.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2704688510, 'eLogNoBillingServerInfo', 'BILLING服务器信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2705721738, 'eLogNoMacroDetectUserBlock', '经检查您是非法软件使用者，不能登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2712815894, 'eScrNo000076', '欢迎随时再来!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2715049883, 'eLogNoCastleOccupyStower', '守护石/守护塔已经被占领.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2725710434, 'eLogNoR2TransTimeStx', 'R2变身活动时间已开启。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2730316318, 'eErrNoServantCallInvalid', '无法进行召唤。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2731108790, 'eErrNoCastleNotSickGate', '城门完好,不需要修理.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2731732215, 'eErrNoGSExchangeInvalidGold', '低于金币最少交易量.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2735324976, 'eErrNoSnapShotDBLoadFailed', 'SnapShot数据加载时，发生错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2741184288, 'eErrNoCastleCantOpen', '无法初始化城堡.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2743973354, 'eErrNoDiffGuildStorePswd', '公会仓库密码不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2748874716, 'eErrNoItemMallNotExistNotice', '不存在的商店通知事项');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2750987197, 'eLogNoTeamRankStxInfo', '组队战开始。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2752268609, 'eErrNoBossBattleMapInfoInsertFailed', '竞技场BOSS战地图登录失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2761058832, 'eLogNoR2TransTimeStxed', '进行R2变身时间。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2761101470, 'eErrNoCastleLackAsset', '银币不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2764355221, 'eErrNoSealSlot', '不能装备.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2767140006, 'eErrNoTeamRankMgrInitFailed', '初始化组队战管理角色失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2770494067, 'eErrNoCorrAbnCannotLoad', '无法载入反应状态异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2773299028, 'eErrNoWebzenShopServerCantRecon', '商城服务器无法再链接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2775172953, 'eErrNoPShopCantWhenOpenPShop', '开设个人商店或对方正在开设个人商店的状态下无法作出此动作.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2778427306, 'eErrNoUTGWHBTUserNotLogin', '一骑讨参与者情报中存在不是登录状态的玩家.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2779286754, 'eErrNoHeightBigMap', '高度文件比地图大.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2792344966, 'eLogNoChaosBattleOccupySvrChange', '因混沌战场占领服务器变更,断开与混沌战场服务器的连接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2799668495, 'eScrNo000072', '真的要放弃吗?');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2799785271, 'eErrNoServantGatheringEndInvalid', '无法终止采集。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2801546804, 'eErrNoCnsmWithdrawCntInvalid', '取出金额不符。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2802694301, 'eLogNoQuestMakingStx', '任务制作活动已开始。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2803321901, 'eErrNoInvalidBossBattleMapInfo', '非正常显示竞技场boss战地图信息，');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2806840529, 'eErrNoItemAlreadyRegister', '这是已有的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2808752109, 'eErrNoEquipCannotFetch', '无法获取装备信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2810664319, 'eErrNoCantSellTermOfEffectivityItem', '个人商店不能出售具有有效时间的道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2810902300, 'eErrNoUTGWHBTInvalidInfo', '非正常的一骑讨参与情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2811493075, 'eErrNoItemMallCantLoad', '商店不能初始化');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2812068916, 'eErrNoInvalidMaterialEvolutionCombine', '不是进化所需的条件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2824730988, 'eErrNoChaosBattleMakeIngressFailed', '混沌对战入场券生成失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2828742698, 'eErrNoCantRegisterExpireBeadOwn', '存在有效期限未满的符文。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2834625286, 'eErrNoLowBidMoney1', '竞拍金额不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2836583241, 'eErrNoNotExistTransformCtrl', '无法变身.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2837540864, 'eErrNoAlreadyUsingEquipSlot', '正在使用中');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2838434709, 'eErrNoTargetOutOfDate', '有效期限已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2841206625, 'eErrNoDiscipleCantKickSelf', '无法将自己从师门中驱逐.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2847376191, 'eErrNoWebzenCreateSessionFailed', 'CreateSession函数失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2853451015, 'eErrNoCastleCannotLoad', '无法获取城堡/村庄信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2869937731, 'eErrNoNotAllowedAtExchanging', '无法在交换中进行。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2877635820, 'eErrNoCancelExchangeTooFar', '脱离可交易的距离，取消交换。
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2881654213, 'eScrNo000027', '现在不是报名时间.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2886895810, 'eErrNoPShopSivlerLack', '银币余额不足,无法完成购买.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2889453964, 'eErrNoMacroDetectCantSearchFile', '找不到非官方制约程序文件');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2890342971, 'eErrNoNotExistBossBattleMapInfo', '竞技场BOSS战详细信息不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2891017871, 'eErrNoIncorrectTerritory', '领地情报错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2892021234, 'eErrNoCalendarNotScheduleOwner', '不是自己创建的事件。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2897168860, 'eScrNo000023', '结束对话');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2906008990, 'eErrNoIsNotActivatePoint', '没有可用的传送点');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2913224734, 'eErrNoItemNotEquipSlot', '物品无法在这里装备.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2916807614, 'eErrNoWebzenInvalidAction', '异常商城操作类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2918323108, 'eErrNoDiffStorePassword', '个人仓库密码不符。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2918556177, 'eErrNoItemMallChecking', '正在检测物品商品, 请稍后使用.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2921605457, 'eErrNoDiscipleMgrCantLoad', '师徒管理员不能load.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2923643182, 'eErrNoInvalidWebzenStorageItemInfo', '异常礼物箱信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2929211508, 'eLogNoEventingMonster', '庆祝活动中的特殊怪物');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2932263504, 'eErrNoGuildCantKickSelf', '无法将自己移除.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2941849193, 'eErrNoCharTooFarOneStep', '一次移动的距离过远.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2951469317, 'eErrNoTeamRankIsNotRegisterPartyOrIsNotPlayingState', '组队战登陆信息有误或无法登陆。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2955237950, 'eErrNoMapNotSetExpel', '攻城地区没有设定踢人设置.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2960658048, 'eErrNoBeadNotInserted', '卷轴无法结合。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2962546518, 'eErrNoCnsmInvalidSlotCount', '符文数量不符。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2967783197, 'eErrNoItemNotConfirmed', '未确认的道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2974199767, 'eErrNoCharUnregisterId', '尚未被使用的角色名.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2975086333, 'eErrNoChaosBattleUserInfoInsertFailed', '混沌战场玩家游戏初始化失败.请稍后再操作');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2975855791, 'eErrNoRegionInvalidSz', 'region文件大小无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2981427653, 'eLogNoDoNotStartUTGW', '跨服公会对战未开始.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2984628054, 'eErrNoRaidInvalidUser', '非正常玩家。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2986993087, 'eErrNoPhQoolCannotReturn', '无法返还物理Qool.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2988837428, 'eErrNoWebzenReqRollback', '异常礼物箱回档申请信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2990538465, 'eErrNoTeamBattleIsNotRegister', '您不是团队战的选手');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2995680005, 'eErrNoNotExistFierceBattleIngress', '入场卷信息错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2996859895, 'eErrNoRegionUnknownPlace', '未知地区.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 2998709383, 'eErrNoMonsterCannotLoad', '无法获取怪物信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3000614020, 'eScrNo000037', '您尚未申请安全验证卡服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3003547070, 'eErrNoBossBattleCantRegMaxRegCntOver', '已超过每日可登陆的竞技场BOSS战最大次数。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3008609276, 'eErrNoCnsmItemExpired', '有效期限已到。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3029019427, 'eErrNoCharCantMoveByAttacked', '遭受攻击,无法移动.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3032504448, 'eErrNoEventDungeonInvalidPlace', '设置活动副本地区有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3037161916, 'eErrNoRefineTooManyMaterial', '强化材料过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3040558479, 'eErrNoTooNearDistance', '召唤兽的范围重叠不能召唤.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3040752655, 'eErrNoVolitionOfHonorPointIsMax', '名誉意志点数为达到上限');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3046749783, 'eErrNoGuildIsMemGuildAss', '公会间已经联盟.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3048503453, 'eScrNo000033', '道具删除完毕.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3050410948, 'eErrNoCantAttkState', '无法攻击状态');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3054994418, 'eErrNoItemRestrictArrow', '你当前的武器无法使用这种箭.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3063236680, 'eScrNo000089', '没有火种就没法跟我一起玩了');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3073824691, 'eErrNoNotExistSnapShotDataFile', 'SnapShot数据文件不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3074587014, 'eErrNoTeamBattleUserCantChat', '团队战场中只开放一般聊天频道.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3076139173, 'eErrNoFailedSendMsgToShopServer', '向商城服务器发送信息失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3087045947, 'eLogNoCastelVacantStower', '守护石/守护塔 空虚.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3087495391, 'eErrNoPathEngInvalidVer', 'path engine 版本错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3090892275, 'eErrNoExchangeNotIng', '没有进行交易.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3092168826, 'eScrNo000081', '请一定要小心啊!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3097489137, 'eErrNoMacroDetectCantOpenFile', '不能打开非官方制约程序文件');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3098114135, 'eScrNo000066', '已经要放弃了?');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3102695761, 'eErrNoGkillNodeNotBuilder', '您不是此节点的研究人.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3108222771, 'eErrNoDiscipleLackOfJoinCount', '拜师点数不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3111828093, 'eErrNoComsumedItemNotExist', '缺乏耗魔材料');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3117637832, 'eErrNoCantUseTarget', '无法给对方使用道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3120225437, 'eErrNoEvCouponPrizeGive', '活动礼品券对应的商品已经送出.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3124141524, 'eLogNoGuildRenameNm', '公会名已经更改, 请重新登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3124608062, 'eErrNoWebzenShopBuyResult2', '超出金币消耗上线，或不属于购买、赠送范围.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3127028199, 'eErrNoTeamBattleSessionIsFull', '申请者列表已经排满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3135034784, 'eLogNoR2TransTimeEtx', 'R2变身活动已结束。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3151191162, 'eErrNoEvCouponPrizeFail', '发送活动道具时发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3152421845, 'eErrNoMacroDetectCantReadFile', '不能读取非官方制约程序文件');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3160585400, 'eErrNoSiegeGambleFullTicket', '攻城竞猜券已经卖完.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3169177137, 'eErrNoRegionInvalidColor', 'region文件中有不适当的颜色.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3172817581, 'eErrNoCancelExchangeStealth', '在透明的状态下不能进行交易.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3175591060, 'eErrNoCalendarAgreementCntMax', '无法继续登记。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3176117013, 'eErrNoNoReturnPlace', '没有找到回程地点信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3176167463, 'eErrNoWebzenShopBuyResult6', '无法购买.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3178164477, 'eErrNoPShopItemInfoDiff', '您要登录的道具信息有误,请确认背包中的道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3181552631, 'eErrNoDiscipleTooMayMember', '可收徒弟数已达上限.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3185737598, 'eErrNoCastleCantGiveAssetToPc', '虽然找到银币数据,但无法将信息传递给PC,请恢复数据.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3186875769, 'eErrNoAlreadyRegisteredTeleportTower', '已经记录的移动传送点');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3189241391, 'eErrNoAlreadyBossBattleGameStart', '参与申请的比赛进行中无法取消');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3190060426, 'eErrNoItemNotOkExchange', '交易准备尚未完毕.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3191240325, 'eErrNoNotExistUTGWIngress', '不存在跨服公会对战入场券的情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3192072204, 'eErrNoInvalidBillingServerType', '异常BILLING服务器类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3192400635, 'eErrNoWebzenShopUseResult1', '礼物箱内不存在该商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3206743139, 'eScrNo000085', '准备中');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3210160823, 'eErrNoNotExistTeamRankIngress', '组队战入场券情报不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3212822104, 'eErrNoNeedMoneyExpirePcBangDay', '您选择了网吧包月产品,请重新登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3214792980, 'eErrNoNotEnoughCount', '数量不足。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3216077271, 'eErrNoTeamRankCfgInvalid', '组队战设定值有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3217341518, 'eScrNo000062', '一定要小心啊.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3219145098, 'eErrNoItemCantUseLimitClass', '您的职业无法使用此道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3237665846, 'eErrNoGSExchangeMaxRate', '超过了最大金币交换率.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3240172438, 'eErrNoOverflowChaosPoint', '超过混沌点数最大值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3247601886, 'eScrNo000088', '等待状态');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3266529291, 'eScrNo000032', '开始删除道具');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3267070519, 'eErrNoCannotOpenWaveMgr', '无法初始化WaveMgr.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3274826375, 'eErrNoPShopCantRegisterExpired', '无法登录具有使用期限的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3275693591, 'eErrNoCharAlreadyExistNm', '此角色名已经存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3276931402, 'eErrNoTeamRankMakeIngressInternalError', '组队战入场券情报加载中发生错误，请稍后再试。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3279307753, 'eErrNoWebzenShopGiftResult20', '无法使用金币\r请在账号中心管理中设置。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3281340318, 'eErrNoRacingCannotLoad', '无法获取马拉松信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3282567235, 'eErrNoCastleNotExistGate', '城门不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3289686435, 'eErrNoCalendarAlreadyAgreement', '已经通过了。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3290595858, 'eLogNoBossBattleInfo', '竞技场BOSS战状态信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3290810339, 'eErrNoPShopInvalidComment', '请添加物品买卖留言.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3292829315, 'eErrNoCantExchangeItem1', '在透明状态下无法进行交易.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3293485699, 'eErrNoServantCallNotEnoughEnergy', '饱腹度不足，无法召唤仆人。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3303433235, 'eErrNoMacroDetectRespFailBlock', '发生错误.已确定您是非法程序使用者,将限制您使用游戏');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3311796901, 'eErrNoItemNotEquip', '该物品未装备.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3314434902, 'eErrNoCantReadSnapShotTypeHeadInfo', 'SnapShot按标头信息类别加载失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3319696402, 'eScrNo000036', '您没有申请OTP服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3324114623, 'eLogNoEvtNtyGoldPigSubscription', '您获得了金猪认购券.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3328689555, 'eLogNoEventBegin', '活动开始.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3333987198, 'eErrNoFierceBattleMakeIngressInternalError', '竞技场激战入场券信息生成中发生错误。稍后请重新尝试。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3334463755, 'eLogNoUTGWInfo', '跨服公会对战状态情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3341881966, 'eErrNoPShopCantSellRuneSlotOpen', '有可用符文槽。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3353635226, 'eLogNoChatCount', '使用者聊天次数现状信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3356747645, 'eErrNoTargetAlmostOutOfDate', '剩余的有效期限过少.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3357816053, 'eScrNo000084', '城堡金库中银币不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3358537769, 'eErrNoConsignmentPaymentLack', '道具个数不够.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3359740934, 'eErrNoItemCantUseLimitLevel', '您目前的等级无法使用此物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3366808528, 'eErrNoCastleDestroyGate', '城门已经被摧毁.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3368258776, 'eScrNo000063', '这次的任务很简单,不再考虑一下吗?');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3374972683, 'eErrNoGoldItemFreeMustResetCashItem', '免费金币道具必须是免费道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3377073409, 'eErrNoFsmNotTransitEvt', 'FSM中无法变更的Event.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3377329717, 'eErrNoUTGWRegGuildFailed', '跨服公会对战服务器登录时发生了错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3377753973, 'eErrNoFsmCannotOpenFss', '无法初始化FSM的 FSS.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3394717873, 'eErrNoWebzenShopBuyResult7', '无法购买的活动商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3395020418, 'eLogNoQuestNotProgress', '任务不在进行中。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3395567622, 'eErrNoDiscipleCantEnterSelf', '无法向自己申请师徒关系.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3396783898, 'eErrNoRegionNotOccupyer', '您不是该地区的占领者.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3399107952, 'eErrNoWebzenUnknownItemType', '礼物箱无法辨识的道具类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3407405905, 'eErrNoExchangeAlreadyIng', '正在交易中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3414810278, 'eErrNoGkillNodeIsCooling', '技能冷却时间.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3417592348, 'eErrNoAlreadyRequsetAtShopServer', '前一次操作内容尚未得到相应。请稍后尝试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3418288656, 'eErrNoUTGWIsNotRegisterGuildOrIsNotPlayingState', '不是跨服公会对战参赛公会或不是可以登录的状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3419459878, 'eErrNoFierceBattleMapInfoInsertFailed', '竞技场激战地图登录失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3427166094, 'eErrNoRestoreCntOver', '无法再延长有效期限.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3434740746, 'eErrNoGoldItemCantBuyGame', '金币道具购买中发生错误.(GAME)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3435837666, 'eErrNoAlreadyRegisterQuickSupplication', '存在无法处理的投诉内容.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3441845269, 'eErrNoCnsmDataNotEnough', '拍卖道具不足。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3443166376, 'eErrNoWebzenShopBuyResult3', '剩余数量不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3447676652, 'eErrNoNotEnoughUnComfirmedCoin', '未鉴定战币不足。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3452742980, 'eLogNoEventingItem', '庆祝活动中的特殊道具');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3453683687, 'eErrNoNotExistGuardian', '不存在的守护神.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3457252022, 'eErrNoBillingSvrCantConnect', '无法连接BILLING服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3460145284, 'eErrNoCharBlockBoard', '论坛暂时无法留言.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3465754834, 'eErrNoExistGuildStoreItem', '公会仓库存在物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3466882449, 'eErrNoCantOpenSnapShotDataFile', 'SnapShot数据文件开启失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3468943809, 'eErrNoDisconnectToChgOccupySvr', '因混沌战场占领服务器变更,断开与混沌战场服务器的连接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3469466446, 'eErrNoContentsRuinMonsterRespawnProbale', '废弃的怪物几率有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3477729516, 'eScrNo000080', '有时间的话欢迎下次再来.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3478256335, 'eErrNoCastleAlreadyCloseGate', '城门已经关闭.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3484305601, 'eScrNo000067', '看来你是个没有耐心的家伙……');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3491839105, 'eErrNoNotExistChaosBattlePotalInfo', '不存在的混沌战场传送情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3492221446, 'eErrNoPartyInvalidItemLooter', '道具获得人错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3493707479, 'eErrNoWebzenBillingServerCantRecon', '无法重新连接BILLING服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3494532341, 'eErrNoNeedMoneyNotFind', '选择计费模式过程中发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3494650927, 'eErrNoBoardCannotFetch', '无法获取系统公告消息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3496084269, 'eErrNoRegionNotSupportPlace', '不支持的地区信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3496591068, 'eErrNoCantLogoutAllUser', '无法另所有用户登出.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3506155194, 'eErrNoConsignmentNotExistItem1', '没有可以购买的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3507093475, 'eErrNoCantCreateChaosBattlePotalInfo', '无法生成混沌战场大门.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3509401408, 'eErrNoTeamRankAlreadyRegsitered', '已被添加组队战列表。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3514756460, 'eErrNoSecurityCantOpen', '无法初始化安全维护工具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3520278548, 'eErrNoItemCantBuy', '此道具无法购买.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3520642457, 'eScrNo000073', '已经走到这一步了,却又放弃,实在是替你惋惜啊.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3523665541, 'eErrNoMacroDetectInvalidState', '无法检测出非官方程序');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3527498650, 'eErrNoInvalidHonorOfEmblemIndex', '非正常的名誉徽章排名情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3528845770, 'eLogNoNotGuildQuestTarget', '公会任务对象信息有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3529150175, 'eErrNoFailedSendMsgToBillingServer', '向BILLING服务器发送信息失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3539402275, 'eErrNoNeedGoldItemEffect', '收费道具，请购买后再使用.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3541263546, 'eErrNoUTGWDoNotRegTime', '今日的跨服公会战的报名已经完毕.无法再进行申请登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3542302535, 'eErrNoPathEngInvalidSzTok', 'tok文件大小无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3542701673, 'eErrNoCnsmItemBound', '是绑定道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3550227095, 'eErrNoContentsEventDungeonPeriod', '活动副本进行时间是最少30分钟。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3551694370, 'eErrNoNotLoginCurSubMaster', '目前公会副会长没有登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3552394151, 'eErrNoCantSendLetterAnotherGuildAndAss', '不能向其他公会或联盟发送公会信纸或礼堂邀请函.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3567152188, 'eErrNoTeamRankCantRegMaxRegCntOver', '多组队战次数过多。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3568026378, 'eErrNoAlreadyInsertGuardianInfo', '已登记的守护神情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3571878439, 'eErrNoEventDungeonIngressItem', '无法进入封印地区，需要封印解除。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3576302256, 'eErrNoNeedMoneyExpiredUserTime', '您选择了计时计费产品,请重新登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3584502085, 'eErrNoItemMallTooManyRecmdItem', '推荐金币商品过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3588755405, 'eErrNoCantGameGuardRegisterDetect', 'GameGuard无法记录非法软件使用者.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3590949149, 'eErrNoGkillExpFull', '已研究公会技能的技能经验值已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3596750322, 'eErrNoTransformCannotLoad', '无法获取变身信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3599539879, 'eLogNoCastleOccupyTower', '守护塔已经被占领.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3602217344, 'eScrNo000077', '欢迎下次再来…');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3610184820, 'eErrNoCorrAbnormalLoad', '反应状态异常没有被载入.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3610753859, 'eErrNoCantUptGuardianOccupyState', '无法更新混沌战场占领情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3612256691, 'eErrNoDisconnectedWebzenBillingServer', 'Billing服务器连接中断.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3615571923, 'eErrNoUTGWIsNotBattleSession', '不是跨服公会对战进行中的地图');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3616075529, 'eLogNoTeamBattleResult', '团队战结果记录');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3616959139, 'eErrNoConsignmentNotExistItem5', '个人商店中没有道具正在销售.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3629455891, 'eErrNoGkillNodeInvalidParm', '公会技能节点参数无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3634962406, 'eErrNoNoGuildAgit', '没有找到公会议事厅.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3635359448, 'eErrNoCnsmSubBalanceFailed', '账户金额收取失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3640537056, 'eErrNoCharSwoon', '昏厥状态下无法进行操作.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3649410123, 'eErrNoCharInvisible', '隐身状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3650573948, 'eErrNoEventDropCannotLoad', '活动掉落信息无法加载。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3655650572, 'eErrNoCnsmFeeEquipable', '手续费是可装备道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3657398887, 'eErrNoCharIsSpeedHacker', '你使用了加速齿轮.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3658735104, 'eErrNoEmptyHonorOfEmblemMark', '名誉徽章情报为空.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3660911537, 'eErrNoStorePasswordIsNotSetting', '未设置公会仓库密码.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3665866647, 'eErrNoArenaRegisterLowLevel', '等级不足，无法申请。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3675158163, 'eErrNoInvalidFierceBattleMapInfo', '非正常显示的竞技场激战信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3676737870, 'eErrNoCnsmNewItemSerialFailed', '新道具生成失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3676842773, 'eErrNoCnsmUpdateItemCntFailed', '更新道具数量失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3680274817, 'eErrNoReinforceInvalidResult', '强化道具无效/不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3681163735, 'eErrNoArenaIsNotEnterablePanalty', '受到惩罚无法进入。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3681231843, 'eErrNoGkillNotBuildSiege', '攻城战进行中,无法进行研究.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3682563006, 'eErrNoPartyFullMem', '队伍成员过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3685251402, 'eScrNo000022', '接受请求');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3686499521, 'eErrNoCharAlreadyDie', '角色已经死亡.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3688114269, 'eErrNoGuildMarkChangeTooOften', '更换公会图标后，1分钟无法进行该操作.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3690334143, 'eErrNoNotExistGiftTarget', '收领对象角色不才在，请核对角色名称.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3692563264, 'eErrNoCantRemovePcInfoInSector', '无法在该扇区中删除信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3696210429, 'eErrNoUTGWMapIsEmpty', '跨服公会对战竞技场不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3697303107, 'eErrNoItemIsCurse', '这是被诅咒的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3699773362, 'eErrNoBossBattleMapIsEmpty', '竞技场BOSS赛场不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3702171821, 'eErrNoSnapShotDataCheckFailed', 'SnapShot数据发生错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3703946579, 'eScrNo000026', '队伍成员已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3710118640, 'eErrNoCalendarGroupCntMax', '无法继续创建群。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3710200159, 'eErrNoDiscipleOverEnterLevel', '徒弟等级等于或大于师父等级.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3711330475, 'eErrNoInvalidCrewCombatUserCnt', '团队战下限人数错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3722336635, 'eErrNoTeamBattleNotExit', '该玩家不再团队战场。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3729713074, 'eErrNoDialogCannotLoad', '无法获取聊天信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3732566464, 'eErrNoCantFindSummon', '不存在的召唤兽.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3736325574, 'eErrNoItemNotUseSelf', '道具使用对象错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3736772068, 'eErrNoWarpFail', '瞬间移动失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3737168067, 'eErrNoPathEngCantSearchTok', '无法找到tok 文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3738504489, 'eLogNoLogoutAllUser', '所有用户已经登出.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3738849266, 'eErrNoIsNotOccupyTeleportTower', '该传送封印石未被占领.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3742010814, 'eErrNoPhCannotCreateBody', '无法生成物理主体(Body).');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3743894319, 'eErrNoDialogSettingFail', '设置聊天信息失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3745529657, 'eErrNoAlreadyItemMallCheck', '道具商店正在维护中。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3746473046, 'eLogNoGoldItemInfoTotalCnt', '已登陆的金币道具消息个数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3752963813, 'eErrNoSnapShotFileCreateFailed', '加载SnapShot数据文件失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3755554833, 'eErrNoIsNotWaitingState', '不在跨服公会对战列表或无法在待机列表里删除的状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3758824846, 'eErrNoOccupyErrorAtShopServer', '正在等待商城服务器处理请求时发生错误，请稍后再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3763046392, 'eErrNoHonorPointLack', '名誉点数不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3763670840, 'eErrNoGuildNotConditionCheck', '不符申请及宣战条件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3764115252, 'eErrNoSilverLack', '银币不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3765329505, 'eErrNoEventDungeonRequestNotParty', '活动副本申请中无法邀请。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3765447674, 'eErrNoCharOverLevel', '角色级别过高.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3767720275, 'eErrNoGSExchangeCantExchangePub', '金币交易中发生错误.(PUB)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3773954532, 'eErrNoInvalidRechargeCnt', '不是充值物品或已经充值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3780577242, 'eErrNoTeamRankMapInfoInsertFailed', '组队战加载失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3781671563, 'eErrNoDiscipleHasMember', '目前师徒系统中还有徒弟.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3783343433, 'eErrNoFsmCannotOpen', '无法初始化FSM.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3786861784, 'eErrNoGkillBuildingAnother', '其他技能节点正在研究中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3789906674, 'eErrNoCantPkCharLv', '无法攻击的等级.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3790112452, 'eErrNoPhQoolCannotOpen', '无法初始化物理Qool.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3794080952, 'eErrNoPartyInvalidItemMode', '错误的道具分配.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3799376227, 'eErrNoWebzenShopGiftResult9', '已超出活动商品购买次数限制.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3801860042, 'eErrNoCalendarScheduleDateMax', '事件创建的太多了。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3803218224, 'eErrNoCnsmItemSiezure', '道具被扣留。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3808192483, 'eErrNoCantFindSupplication', '无法找到投诉内容.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3809472561, 'eErrNoUTGWCantSendPcInfoReq', '角色情报登录跨服公会对战服务器失败.请重新连接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3811631602, 'eErrNoCnsmUnknownError', 'DB出错。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3824082715, 'eScrNo000053', '在你帮助我的同时你也将学会生存的方法!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3829614034, 'eErrNoNotGuildMaster', '您不是公会会长.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3831561323, 'eErrNoNotExistTeleportControl', '无法操控传送.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3833797378, 'eScrNo000057', '愿幸运女神与你同在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3844557565, 'eErrNoEvtMsgCannotLoad', '无法载入相关信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3847247452, 'eErrNoGkillInvalidNode', '公会技能节点无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3848727110, 'eErrNoAbnSiegeDfnsBenefitEmpty', 'DB内不存在守护者异常状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3862279515, 'eErrNoVolitionOfHonorLack', '意志点数不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3865764533, 'eErrNoInvalidSnapShotType', 'SnapShot类型出错。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3876499093, 'eErrNoAiNotExist', '无法找到此AI.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3881386910, 'eErrNoCnsmRemoveFeeFailed', '无法消耗手续费。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3892497502, 'eErrNoItemIsEquipDelay', '请稍后再进行装备.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3896207253, 'eErrNoUsableOnlyGuildMaster', '公会会长专用.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3896246698, 'eErrNoGoldItemNotExist', '不存在的金币道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3898282026, 'eErrNoWebzenShopGiftResult10', '收信者和发信者相同.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3902408209, 'eErrNoPShopCantOpen2', '当前地区无法开设个人商店.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3903045294, 'eErrNoBoardCannotLoad', '无法读取系统公告消息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3918797819, 'eLogNoGSExchangeCompleteExchange', '金币交易结束.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3918838260, 'eErrNoPcSkillPackCannotFetch', '无法读取技能列表.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3919453128, 'eScrNo000002', '道具制造失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3922198994, 'eErrNoPathEngCantReadTok', '无法读取tok 文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3923762820, 'eErrNoTeamBattleSessionNotBattleState', '该决斗场不在进行中。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3924912602, 'eErrNoInvalidUTGWSessionInfo', '非正常的跨服公会对战情报.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3928898218, 'eErrNoMacroDetectRespOk', '答案正确,感谢您参与非法程序管理.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3931693525, 'eErrNoFailedShopInsertReqUserInfo', '使用商场用户记录添加失败，请稍后再试.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3936379281, 'eErrNoUTGWCantRegState', '无法登录跨服公会对战的状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3943500418, 'eErrNoEventDungeonRequestingOrderParty', '其他队伍正在申请。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3951803129, 'eErrNoWebzenUnknownRecvType', '异常领取类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3955469640, 'eErrNoWebzenShopGiftResult5', '该商品已停止发售.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3955879882, 'eErrNoTeamBattleFullObserver', '观战人数已达到上限。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3957962399, 'eErrNoTeamBattleMinUserCnt', '参赛人员不够，无法开始。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3958844412, 'eErrNoTeamBattleCantJoin', '团队战已经开始.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3970853201, 'eErrNoWebzenShopGiftResult1', '持有金币不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3972786077, 'eErrNoTeamBattleMgrCantOpen', '无法初始化TeamBattle Mgr.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3974628899, 'eErrNoTooManyDeleted', '超过当日人物删除数量.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3975882412, 'eErrNoScriptCannotLoad', '无法读取脚本(Script).');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3978895962, 'eErrNoInvalidPortalNo', '入口号码无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3981692941, 'eErrNoTradeTooManyList', '交易列表内容过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3983399272, 'eErrNoCharCannotFetch', '无法获取游戏角色信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3984107123, 'eErrNoGuildRecruitLimit', '不符合招募条件');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3985247339, 'eErrNoRegionNotTeleport', '无法被传送到的地区.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3989031482, 'eErrNoNoBanquetHall', '没有找到公会豪华礼堂.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3993514317, 'eErrNoUnableInSiegeTime', '攻城战中不可以.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3993608053, 'eErrNoGkillNotExistBuildItem', '公会技能研究用道具不足或不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3994366637, 'eErrNoPartyRejectEnter', '没有响应组队邀请.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3995582970, 'eErrNoGuildAssCantOpen', '无法初始化公会联盟信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 3997163895, 'eErrNoGuildAgitNotOnSale', '公会议事厅正在出售中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4008959953, 'eScrNo000006', '错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4010721037, 'eErrNoTeamRankCantRegOnlinePartyMemCntBelow', '队员数不足，无法申请。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4019918320, 'eErrNoCantEventMsgSetMon', '无法获得怪物情报相关信息 .');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4020436721, 'eErrNoUTGWHBTUserBelow', '一骑讨参加者不足因此进行了败北处理.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4027042752, 'eErrNoGuildRecruitTooMay', '公会招募申请者过多');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4028090647, 'eErrNoMustMonNotExist', '无法找到必须存在怪物的信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4028152723, 'eErrNoItemMallEmptyPackage', '原料不足');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4034583299, 'eErrNoGkillCantDestroy', '无法移除公会技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4035259017, 'eScrNo000012', '等级过高.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4038848158, 'eLogNoCharTransfer', '角色转服成功, 连接即将断开.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4050966739, 'eErrNoAlreadyActivateRoom', '房间已经开启');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4060483164, 'eErrNoDiscipleNotSameMember', '双方不属于同一师门成员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4061089545, 'eErrNoTeamRankIsNotEnterableTm', '组队战如长时间一过，无法进入。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4062747784, 'eErrNoInvalidNpc', 'NPC出错。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4064133106, 'eErrNoPreventAddictionPenalty1', '您累积在线时间已超过5小时,您已进入不健康游戏时间,请您立即下线休息. 如不下线,您的身体健康将受到损害,您的收益已降为零. 直到您的累积下线时间满5小时后,才能恢复正常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4067909648, 'eErrNoPartyNotMem', '非该队成员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4077504528, 'eErrNoIsNotActiveGkill', '公会技能树尚未打开');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4077630729, 'eErrNoDropGrpTooMany', 'Drop Group信息过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4087890516, 'eErrNoBossBattleMakeIngressInternalError', '竞技场BOSS战入场券信息生成中发生错误。稍后请重新尝试。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4089887964, 'eErrNoCharAlreadyRegister', '此角色已被注册.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4091768400, 'eErrNoNotMemberOrLogout', '该对象不是公会成员或离线状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4092721116, 'eErrNoCalendarAlreadyGroupMember', '已登记。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4099082014, 'eErrNoCantSetGuildMasterStoreGrade', '公会会长的工会仓库等级不能变更.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4100037027, 'eErrNoFailedInitWebzenBillingMgr', 'Billing管理初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4109170552, 'eErrNoQuestCannotFetch', '无法获取任务信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4114720893, 'eErrNoCantEndItemMallCheck', '确认有效性过程中发生错误。请从新确认');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4115802106, 'eErrNoGkillNotExistApplyItem', '需要道具才可以施展此技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4119763949, 'eErrNoEventDungeonRequestOverTime', '申请入场已超时。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4125695025, 'eErrNoCantEventMsgSetRf', '无法获得强化情报相关信息 .');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4129932670, 'eErrNoUnknownResult', '未知结果.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4133142306, 'eErrNoAbnormalCannotFetch', '无法获取状态异常信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4135478480, 'eErrNoNotUseItemRestExpActivate', '没有非激活经验值，或激活经验值已满。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4135678234, 'eErrNoMacroDetectRespTimeout', '已经超过限制时间.连续3次错误输入时帐号使用受到限制');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4142805487, 'eLogNoCastlePreoccupiedStower', '守护石/守护塔 已被标记.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4149451196, 'eErrNoNotExistIngress', '没有入场券');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4153193914, 'eErrNoUTGWIsNotUTGWEnterableTm', '不是跨服公会对战入场的时间.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4158554051, 'eErrNoMacroDetectUserBlock', '查明您是非法程序使用者,连接终断');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4159237776, 'eScrNo000016', '三');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4159412092, 'eErrNoGuildAssAlreadyEnter', '您已经加入了一个公会联盟.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4159835588, 'eErrNoItemPreemptAnother', '此物品为他人所有.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4159978175, 'eErrNoCnsmCannotBuySelfItem', '无法购买自己的道具。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4162160454, 'eErrNoCantRegisterItemID', '物品中存在无法添加的商品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4162184248, 'eErrNoFierceBattleMapIsEmpty', '竞技场激战赛场不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4175165898, 'eErrNoCastleIsNotTower', '这不是守护塔.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4176411400, 'eErrNoRegionNotOccupyPlace', '还没有占领该地区.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4178857586, 'eErrNoInvalidBillingServerInfo', '异常Billing服务器信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4181031116, 'eLogNoR2DayExpEtxed', 'R2特别活动已经结束.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4181429657, 'eErrNoGuildCantRollbackExp', '无法恢复荣誉点数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4185308456, 'eErrNoItemMallTooManyBestItem', '人气金币商品过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4188823860, 'eErrNoEvCouponNotSupport', '本服务器不支持认购券,请选择其他服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4192348292, 'eErrNoChinaToxError3', '系统发生错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4207089965, 'eErrNoInvalidItemMallList', '物品商店列表已更改, 请重新打开物品商店.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4210421338, 'eScrNo000043', '请帮我找来小矮人的毛!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4213737249, 'eErrNoCharCannotDelItemCnsm', '请回收拍卖行道具或金额后删除。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4225706643, 'eErrNoCantSupportGambleSiegeRegion', '此营地不支持攻城.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4233356662, 'eErrNoChatFilterTooManyMember', '屏蔽列表已满。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4234390912, 'eErrNoGkillNotExist', '该公会技能不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4241423358, 'eErrNoEventCantLoad', '活动信息无法读取');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4241706845, 'eErrNoQuestCannotLoad', '无法读取任务信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4243148749, 'eErrNoServantEvolutionInvalid', '无法进化。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4246432357, 'eErrNoCnsmNoMoney', '银币不足。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4254163477, 'eErrNoSiegeGambleCantOpen', '无法初始化攻城竞猜.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4254607939, 'eScrNo000047', '很高兴见到你.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4256000314, 'eErrNoCalendarInvalidScheduleDate', '无法登记。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4256286985, 'eErrNoSecurityNotEqualKey', '安全认证KEY错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4257873596, 'eLogNoLogoutAllPc', '所有PC已经登出.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4258054456, 'eErrNoExchangeIsUnLock', '没有锁定目录。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4258403369, 'eErrNoCnsmDataPoolEmpty', '拍卖行数据已耗尽。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4267899671, 'eErrNoCnsmAccountLimitExpected', '账户无法收容预付金。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4280335955, 'eErrNoGkillCantBuild', '无法研究公会技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4280565548, 'eErrNoCnsmSearchKeywordShort', '搜索关键词缺少');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4281628530, 'eErrNoInvalidTeleportPoint', '传送位置异常或该地点不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4283233913, 'eErrNoAlreadyUTGWGameStart', '跨服公会战已开启.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4286070599, 'eErrNoDisconnectToArenaSvrJoin', '因连接竞技场服务器失败，与服务器终止连接。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4289703163, 'eErrNoCnsmInvalidPeriod', '期间不符合。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10002, 4294773234, 'eLogNoWebzenShopInfo', '商城客户端模块LOG.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 7788710, 'eErrNoJpn_INITED_FAIL', '模块初始化失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 9087247, 'eErrNoTaiwanAPIRvGameActivate', '游戏需激活
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 41885692, 'eErrNoMapCannotLoad', '无法读取地图.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 43174664, 'eErrNoSkillNotDeveloped', '技能尚未开发.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 57426160, 'eErrNoParmSkillTreeNodePointGroupInvalid', '技能列表的技能点号码不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 67173391, 'eErrNoItemInvalid', '道具无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 70007711, 'eErrNoItemOnlyRecharge', '只有充值道具才可以使用.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 70179742, 'eErrNoInvalidAmpValidity', '非正常的有效期扩张值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 121096063, 'eErrNoItemInvalidStatus', '道具状态无效/异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 154349920, 'eErrNoChinaBillingRvC001', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 157346394, 'eErrNoNotExistSetItemType', '不存在的礼包道具类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 159608241, 'eErrNoMustHaveSetItemEquipable', '礼包道具的构成必须包括可以装备的道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 166780708, 'eErrNoJpn_PARAM_INVALID', '认证服务器传达信息错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 167381014, 'eErrNoHannEverBillInvalidPG', '无效的PG');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 175813788, 'eErrNoGuildNotBattle', '公会间尚未宣战.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 187681727, 'eErrNoParmInappropriateCondition', '条件不准确');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 189286895, 'eErrNoJpn_unknow', '未知错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 192181483, 'eErrNoSkillDevNotAuth', '没有开发技能的权限.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 200791127, 'eErrNoInvFull', '背包已经装满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 203388968, 'eErrNoFailedInitFCSAdapter', 'FCSAdapter初始化失败
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 204197292, 'eErrNoChinaBillingRvU004', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 217690154, 'eErrNoSameItemReinforceNSource', '强化后的物品和材料道具相同.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 243609272, 'eErrNoItemNotStackable', '该物品无法堆叠.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 261644470, 'eErrNoChinaBillingRvR003', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 267425736, 'eErrNoTaiwanAPIRvNoUser', '用户ID不存在
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 267973065, 'eErrNoItemLack', '道具不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 278938616, 'eErrNoJpn_CONNPOOL_CREATEFAIL', '连接池生成失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 307074068, 'eErrNoGuildCantBattleSelf', '无法申请和自己公会开战.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 316912605, 'Invalid_Accounting_Type', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 321441400, 'eErrNoAbnormalEmptyType', '没有此状态异常类型.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 337511248, 'eErrNoJpn_DLL_UNEXPECTED', '发生未知错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 360098758, 'eErrNoSkillPackRangeInvalid', '技能的使用范围不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 368265725, 'eErrNoParmInvalidModType', '模块类型无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 369164583, 'eErrNoParmInvalidSkillNodeItemLevel', '技能列表的等级不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 380619142, 'eErrNoBeadCantStack', '符文无法重复使用。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 381974357, 'eErrNoBeadNoInfo', '无符文信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 384338374, 'eErrNoFullMoonInvalidPeriodValue', '盈月遗迹期间选项（内容表格）有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 403496318, 'eErrNoItemAlreadyUsed', '道具已经使用.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 407172962, 'eErrNoCannotConnectChaosBattleSvr', '混沌对战服务器无法连接		');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 419540357, 'eErrNoCharCannotDelGuildMem', '退出公会后才可以删除角色.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 427494218, 'eErrNoTaiwanAPIRvNotEnoughBalance', '购买余额不足
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 428662708, 'eErrNoContentsOffAtChaosBattle', '混沌战场里需关掉荣光圣殿和盈月遗迹内容。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 437398009, 'eErrNoItemNotExist', '不存在的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 457534652, 'eErrNoInvalidNeedMoneyType', '无效的收费道具种类.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 465250290, 'No_Enough_Point', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 475144238, 'eErrNoTaiwanAPIRvPurchaseFail', '购买失败
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 478598982, 'eErrNoProtectNotExist', '不存在的Protect信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 485641531, 'eErrNoJpn_PASSWORD_INCORRECT', '用户密码错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 490762504, 'eErrNoPassiveSkillPackDevFail', '无法开发技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 538155521, 'eErrNoParmNodeItemVacant', '有空缺的技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 540715935, 'eErrNoPublisherUnderMaintenance', '非运营时间,详情请登录官方网站');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 552180902, 'eErrNoInvalidBroadcastPaymentItemID', '非正常消费道具ID');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 558808737, 'eErrNoSkillPackUseLevelInvalid', '技能的使用等级不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 564078530, 'eErrNoJBill_INVALID_RESULT', '日本 Billing 错误结果.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 576310956, 'No_Such_Service', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 576464163, 'eErrNoCantUseSkillPackLimitClass', '无法使用技能的职业.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 625083747, 'eErrNoUnknownShopType', '无法登陆的物品种类');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 626762798, 'eErrNoANTITOXIC_INITED_FAIL', '模块初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 631669959, 'eErrNoGuildRefusalFail', '拒绝公会加入申请失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 636902217, 'eErrNoCnsmCfgInvalidSellPeriod', '出售期间不符合。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 643458025, 'No_Such_ServiceAccount', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 646154047, 'eErrNoCantSendUTGWSvr', '无法寄信到跨服公会对战服务器里.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 648402875, 'eErrNoDropNotExist', '掉落物品不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 659114794, 'Wrong_OTP', '结束客户端后可以使用PINGPENG登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 662136345, 'eErrNoSkillPackItemTypeInvalid', '技能的道具种类不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 670465140, 'eErrNoFailedExecWISM', 'WISM执行失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 673229740, 'eErrNoGuildRejectBattle', '对方拒绝了您的宣战申请.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 680047018, 'eErrNoBeadDupGroup', '已装备的符文。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 695724264, 'eErrNoUserNotExistId3', '3:用户ID不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 722659537, 'Invalid_Time_Format', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 727213973, 'ServiceAccount_Locked_DES', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 731766644, 'eErrNoHannEverBillInvalidGold', '非正常性质的结算金额');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 733483402, 'eErrNoInvalidBroadcastPaymentCount', '聊天道具的费用消耗量异常');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 748574902, 'eErrNoLetterNotExistMsg', '信件中没有内容.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 749565183, 'eErrNoMustSkillNotExist', '没有找到必须要存在的技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 801713924, 'eErrNoUserTimeoutCertify', '认证时间超时.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 824535540, 'eErrNoParmClassSkillTooMany', '个人技能列表里节点过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 833130612, 'eErrNoJpn_SENDCONNECTION_FAIL', '发送失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 839927658, 'eErrNoCharAlreadyEnterGuild', '已经加入了公会.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 839969165, 'eErrNoTaiwanAPIRvItemNotFound', '未登录的道具');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 849976271, 'eErrNoCharInvalidMp', '法力值无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 887750782, 'eErrNoGuildNotMaster', '您不是公会会长.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 889313754, 'eErrNoDevConditionNot', '未满足开发条件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 894512751, 'eErrNoJpn_PASSWORD_MISMATCHOVER', '超过错误输入密码限制次数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 896505532, 'eErrNoContentsNotSupport', '目前不支持的内容.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 901268595, 'Wrong_EventOTP', '结束客户端后可以使用PINGPENG登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 919085873, 'eErrNoJpn_INITED_ALREADY', '模块初始化已完成.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 925836734, 'eErrNoHannEverBillInvalidUserInfo', '无效的用户信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 946448747, 'ServiceAccount_Authorized', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 959294159, 'eErrNoCannotJoinChaosBattleSvr', '混沌对战服务器里不能进入对应的阵营		');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 971418785, 'eErrNoParmInvalidSTID', '技能列表号码不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 978584447, 'eErrNoItemNotHasSkill', '道具中没有技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 992964277, 'eErrNoContentsInChat', '非法聊天信息设定.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 998187100, 'eErrNoSkillPackNoSkill', '技能栏里不存在技能。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1001970811, 'eErrNoCantEquipSlot', '由于状态异常无法在相应装备栏中正常装备.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1021441473, 'eErrNoInvalidBroadcastPaymentType', '异常的聊天费用支付模式');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1030562754, 'eErrNoANTITOXIC_DLL_UNEXPECTED', '未定义的错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1040554035, 'eErrNoCannotOpenHanIpCheckMgr', 'IP检测软件初始化错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1050810885, 'eErrNoTaiwanAPIRvExpiredateLimteExceed', '购买超时
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1053701157, 'eErrNoHannEverBillAccountNotExist', '不存在的银行账户');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1058831228, 'eErrNoParmSkillTreeEmpty', '技能列表处于空缺的状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1061189743, 'eErrNoHannEverBillGoldDeficit', '余额不足');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1091719472, 'eErrNoInvalidUTGWPrizeItemInfo', '非正常的跨服公会战获胜奖品情报');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1098254570, 'eErrNoAbnormalInvalidType', '状态异常类型无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1104637870, 'eErrNoModuleNotExist', '不存在的模块.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1109403533, 'eErrNoChinaBillingRvNONE', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1114921470, 'Encode_Error', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1123143116, 'eErrNoJBill_CANNOT_LOAD_XML', '无法读取日本 Billing XML 设定文档.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1144093141, 'eErrNoJpn_NOT_PREMIUM', '不是高级玩家.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1145243228, 'eErrNoMapCannotOpenSector', '无法初始化Sector.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1155002399, 'eErrNoContentsARSAuthServerInvalid', '电话认证内容选项错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1155771359, 'eErrNoHannEverBillError', 'Hangame收费系统发生错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1156706028, 'eErrNoParmInvalidSTNICType', '技能列表节点道具条件种类不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1162274581, 'eErrNoSlainTooMany', '伤害信息过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1171702643, 'eErrNoParmSkillNodeItemNoSkillPack', '技能列表节点与技能未连接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1172765126, 'eErrNoHannEverBillUnKnownError', '接收到供应商不详的错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1177363798, 'eErrNoCnsmCfgInvalidFeeItem2Cnt', '上架道具数量不符合。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1181749537, 'eErrNoJpn_COOKIE_TIMEOUT', 'cookie期满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1211590384, 'eErrNoJBill_BILLNO_NOT_EXIST', '日本 Billing Billing编号 不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1217789657, 'eErrNoProtectTooMany', 'Protect信息过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1218897692, 'No_Enough_Points', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1229138128, 'eErrNoJpn_OPENCONNECTION_FAIL', '连接失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1229596876, 'eErrNoIncorrectUTGWOptions', '跨服公会战对战选择权错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1231844633, 'eErrNoHannEverBillDBError', '发生供应商DB错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1235855325, 'eErrNoHannEverBillGoldTxnFailed', '余额修正过程失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1246332966, 'eErrNoRusBlRv_INCORRECT_LOGIN', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1277535457, 'eErrNoMapInvalidNo', '地图编号无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1292723521, 'ServiceAccount_Not_Login', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1295877774, 'eErrNoUserInvalidNo', '用户编号无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1307510307, 'eErrNoLoginUnknownError', '1:开发内部错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1324795732, 'eErrNoPublisherCantClearSCFail', '开发商SC失败次数无法初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1325539862, 'eErrNoCharCannotDel', '无法删除角色.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1327772603, 'eErrNoCharInvalidExp', '经验之无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1336580836, 'eErrNoParmGuildSkillTooMany', '公会技能列表里节点过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1339977610, 'eErrNoCharInvalidHp', '生命值无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1342376447, 'eErrNoConTentsGuildRecruit', '公会招募级别设定错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1351609668, 'eErrNoDevRemainTimeShort', '技能列表节点的剩余时间过少.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1365878039, 'eErrNoGuildHasOccupy', '已占有守护石或守护塔.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1367060676, 'eErrNoTaiwanAPIRvBlockedCountryOrIP', '被禁止的国家或者IP
');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1379038305, 'eErrNoDevNoSkillPoint', '技能点不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1380468215, 'Invalid_Authorization_Type', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1381845479, 'eLogNoUnsupportedContents', '不支持的内容。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1398712471, 'eErrNoLetterCannotSend', '无法发送信件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1402386637, 'ServiceAccount_Locked_PKI', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1416524106, 'eErrNoRusBlRv_INVALID_PRICE', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1424926348, 'eErrNoExistsIlluminaContentsOption', '需要荣光圣殿内容选项。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1433936352, 'eErrNoJBill_CONN_POOL', '日本 Billing ERR_CONN_POOL');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1442705583, 'eErrNoCantUseSkillPackLimitLevel', '无法使用技能的等级.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1457475899, 'eErrNoJpn_MEMBERID_INVALID', '用户名有误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1468368853, 'eErrNoRusBlRv_FAILED', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1500713953, 'eErrNoContentsInfo', '目录信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1538955755, 'eErrNoGuildInvalidNm', '公会名称无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1583761451, 'eErrNoInvalidSkillPackId', '错误的技能号码.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1584453758, 'eErrNoUserNotExistId2', '2:用户ID不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1587645545, 'eErrNoRusBlRv_Unknown', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1638258027, 'eErrNoJBill_POINT_NOT_SUFFICIENT', '日本 Billing R2Point不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1673092550, 'eErrNoParmDupSTNID', '技能列表节点号码已重复.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1674077013, 'eErrNoContentsInSecurity', '保安工具检测时间间隔不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1676741111, 'eErrNoPassiveSkillNotUse', '无法使用技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1682803012, 'eErrNoPublisherErrorInternal', 'Publisher内部发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1683638442, 'eErrNoOwnOverMaxCnt', '仓库已达到储存上限。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1690914548, 'eErrNoContentsInPcItemDropRate', '增加掉落物品个数错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1713697987, 'eErrNoJpn_BIL_INITED_FAIL', 'Billing模块初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1718239239, 'eErrNoLoginingInternalError', '开发内部错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1725903108, 'eErrNoMapCannotSplit', '无法分割地图.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1726484963, 'eErrNoHanIpCheckError1', 'IP格式错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1749577151, 'eErrNoCantSendArenaSvr', '无法发送到竞技场服务器。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1762089424, 'eErrNoGuildNotMember', '该用户不是公会成员.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1792958108, 'eErrNoItemTooMany', '道具过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1799212118, 'eErrNoGbjInvalidType', 'Gbject类型无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1812061665, 'eErrNoUserDiffCertifiedKey', '认证KEY不同.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1812237153, 'eErrNoContentsCheckTRCnt', '客户端标记计数设置信息有误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1837849288, 'eErrNoGuildAdmissionFail', '申请加入公会被拒绝');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1860997198, 'eErrNoGuildRejectEnter', '申请加入公会被拒绝.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1886958130, 'eErrNoBindItemCantMove', '已绑定的道具不能送给他人或丢弃.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1890318556, 'eErrNoCannotInitHanAntiToxic', '未被初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1899369507, 'eErrNoModuleTooMany', '模块过多。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1904144857, 'eErrNoSpecificSvrNotARSAuth', '特定服务器未申请电话认证。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1924251270, 'eErrNoJBill_INVALID_ARGUMENT', '日本 Billing 常量错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1932519227, 'eErrNoHannEverBillInvalidItem', '没有登陆的结算代码');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1948975051, 'ServiceAccount_Authenticated', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1967145109, 'eErrNoJpn_CONNPOOL_INVALID_SIZE', '连接池大小错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1984353596, 'ServiceAccount_Already_Login', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1990683052, 'eErrNoContentsInMonDropRate', '错误的怪物掉落率.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 1998123088, 'eErrNoSkillTooMany', '技能过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2005807999, 'eErrNoSpecificServerLimitTime', '特定服务器已超过限时。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2005926190, 'eErrNoBeadIndexInvalid', '符文效果过多。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2013712661, 'eErrNoCantConnectJapan', '无法连接日本API.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2023706656, 'eErrNoChinaBillingRvR002', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2029968592, 'eErrNoANTITOXIC_DBGW_LOAD_LF_FAILED', '本地路径读取失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2030608230, 'eErrNoItemTooManyStackCnt', '物品个数太多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2032344695, 'eErrNoTaiwanAPIRvWrongPrice', '申请错误的价格');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2032477877, 'eErrNoJBill_unknown', '日本 Billing 未知错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2038995271, 'eErrNoGuildAlreadyExistNm', '公会名称已经存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2044489842, 'eErrNoRusBlRv_NOT_ENOUGH_MONEY', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2055382229, 'eErrNoCharInvalidNm', '角色名无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2058653131, 'eErrNoANTITOXIC_DBGW_CON_FAILED', 'DBGW 服务器连接失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2073131077, 'eErrNoNotExistChaosBattleIngreesInfo', '混沌对战入场卷信息不存在		');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2075216818, 'eErrNoRefuseTheLetter', '收信人拒绝收信的状态');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2079110226, 'eErrNoTooManyAbnroamlCntForSetItem', '礼包道具效果过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2084649251, 'eErrNoChinaBillingRvU001', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2126625286, 'eErrNoUserDiffPswd', '密码不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2130364605, 'eErrNoInvalidReqForSkill', '与技能关联的无效的邀请.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2130821074, 'eErrNoNotExistSkill', '不存在的技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2138861974, 'eErrNoRusBlRv_ACCOUNT_BLOCKED', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2144992237, 'eErrNoContentsCantLoad', '无法获取内容信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2160033607, 'eErrNoPublisherCantSnd', '无法输出Publisher服务器讯息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2161957727, 'eErrNoJBill_BAD_POINTER', '日本 Billing 点数计算错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2182706978, 'eErrNoANTITOXIC_ARGUMENT_INVALID', '数据传输错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2219860962, 'eErrNoJBill_CANNOT_AUTH', '日本 Billing Web sever 权限不足');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2226746356, 'eErrNoContentsInGulidMarkRegist', '公会标志搜索时间信息不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2262402111, 'eErrNoBeadEffectNotExist', '符文效果不存在。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2266706120, 'eErrNoParmTooManySkillTreeNodeItem', '技能列表节点道具过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2279968598, 'eErrNOGuildInvalidMark', '公会标志不符合条件');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2296209868, 'eErrNoSkillNotExist', '技能不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2296803535, 'eErrNoHanIpCheckError3', 'IP尚未注册');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2305469379, 'eErrNoInvalid1IPNLimitValue', '1 IP 多重连接设置值不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2311071925, 'eErrNoGuildNotLoginMaster', '公会会长没有登陆.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2316403136, 'eErrNoANTITOXIC_DBGW_SEL_FAILED', 'DBGW Select 作业失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2317665925, 'eErrNoJpn_INITED_NOT', '模块未初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2338849630, 'eErrNoSkillAlreadyDeveloped', '技能已开发.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2339803875, 'eErrNoJBill_CANNOT_CONNECT', '日本 Billing Web sever 无法连接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2343153344, 'eErrNoAbnormalAddNotExist', '不存在的状态异常加重.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2347758095, 'eErrNoGuildAlreadyMark', '公会标志已经存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2356722741, 'eErrNoSkillPackTermOfValidityInvalid', '技能有效期限不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2388232900, 'eErrNoSlainNotExist', '不存在的伤害信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2411723360, 'eErrNoTaiwanAPIRvUnknown', '未知错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2414159313, 'eErrNoJBill_COIN_NOT_SUFFICIENT', '日本 Billing R2Coin不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2419744986, 'eErrNoChinaBillingRvC002', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2428581849, 'eErrNoInvalidShopType', '不存在的物品种类');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2448202903, 'eErrNoParmSTNInvalidMaxLv', '技能列表节点的最大等级不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2449274133, 'eErrNoChinaBillingRvR004', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2453502342, 'Service_Not_Available', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2454630283, 'eErrNoSkillPackSubTypeInvalid', '技能的子型号不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2454673423, 'eErrNoChinaBillingRvU003', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2457252484, 'eErrNoJpn_SYSTEM_ERROR', '认证服务器系统发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2464083983, 'eErrNoItemInvalidCnt', '物品个数无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2464487556, 'eErrNoJpn_DIFFERENT_NATION', '不支持的国家.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2475621747, 'eErrNoBeadDupIID', '已装备的符文。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2483630080, 'eErrNoItemCantEquipLimitLevel', '您的级别无法装备此物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2492253689, 'eErrNoCharInvalidNo', '角色编号无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2493403489, 'eErrNoTaiwanAPIRvPassWordWrong', '用户密码错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2504199539, 'eErrNoParm1stNodeItemNotExist', '不存在1级技能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2507385027, 'eErrNoJpn_RECVCONNECTION_FAIL', '受领失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2511260097, 'eErrNoUserInvalidId', '用户ID无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2514937413, 'eErrNoANTITOXIC_DBGW_SEL_INVALID', 'DBGW Select 无效结果.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2520871240, 'eErrNoContentsInvalidCharge', '不能同时支持定额制区分与部分有偿服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2557601042, 'eErrNoIpFilterCantLoad', '无法读取IP过滤器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2558338631, 'eErrNoIsNotHangamePcBang1', '非合作网吧IP.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2595410174, 'eErrNoJBill_INITED_NOT', '日本 Billing 没有被初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2614335671, 'eErrNoPublisherInvalidID', 'ID(Publisher)错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2614824802, 'eErrNoJpn_COOKIE_SETTINGERR', 'cookie不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2625030547, 'ServiceAccount_Authorized_Updated', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2626425695, 'eErrNoCnsmCfgInvalidFeeItem', '上架道具不符合。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2627793618, 'eErrNoPublisherCantConnect', '无法连接Publisher服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2630006860, 'eErrNoGuildNotExistMark', '公会标志不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2639795277, 'Invalid_Length', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2662827448, 'eErrNoPublisherUnknownError', 'Publisher发生未知错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2689632236, 'eErrNoCharNotEnterGuild', '目前没有加入公会.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2696583525, 'eErrNoContentsInMacroAuto', '防外挂设定无效。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2705879053, 'eErrNoTaiwanAPIRvNoGame', '无效的服务');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2712741656, 'eErrNoTeleportFull', '传输通道已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2717331986, 'eErrNoGuildInvalidGrade', '公会成员级别异常/无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2720378583, 'eErrNoGuildAlreadyBattle', '公会间已经宣战.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2723160645, 'Time_Out', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2727489914, 'eErrNoGuildNotExist', '该公会不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2730266690, 'eErrNoContentsSpotAttackerInvalid', '营地袭击者相关设置错误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2777376462, 'eErrNoJBill_MEM_ALLOC_FAIL', '日本 Billing 内存错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2777893052, 'eErrNoRusBlRv_ITEM_NOT_SELL', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2804668462, 'eErrNoJBill_ORDERNO_NOT_EXIST', '日本 Billing 预订号码不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2810345613, 'Accounting_Request_Processing', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2810847427, 'Invalid_Character', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2814169636, 'eErrNoBeadMaxCntInvalid', '符文槽最大数量不符。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2823061036, 'eErrNoCnsmCfgInvalidChargeSlotCnt', '商城符文槽数量不符。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2825582194, 'eErrNoEmptyString', '空的字符串.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2826252632, 'eErrNoBeadEffectTooMany', '符文效果过多。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2826281508, 'eErrNoTaiwanAPIRvNotLogin', '登陆失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2833938654, 'eErrNoPublisherCantOpenSystem', '无法初始化Publish系统.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2834425494, 'eErrNoJBill_INITED_ALREADY', '日本 Billing 已经被初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2848160433, 'eErrNoChinaBillingRvUnknown', '中国Billing API呼出结果无法确认');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2851085882, 'eErrNoContentsInPcExpPenalty', '非正常的PC经验减少');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2855129384, 'eErrNoANTITOXIC_DBGW_SEL_COL_INVALID', 'DBGW Select  数据无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2860991057, 'eErrNoGuildOrAssOnlySend', '只能发送到所属公会和联合公会');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2865356892, 'eErrNoTaiwanAPIRvEmailVerification', '需要邮箱认证');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2883847981, 'ServiceAccount_Already_Waiting', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2932223929, 'eErrNoANTITOXIC_DBGW_SEL_OVER', 'DBGW Select 结果为 1个以上.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2937159665, 'eErrNoContentsInGulidStore', '公会长裤选项设置错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2961094994, 'eErrNoUserNotExistId0', '0:用户ID不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2970160158, 'ServiceAccount_Waiting', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2986875099, 'eErrNoInvalidTermOfValiditySTN', '技能列表节点的有效期限不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 2998564638, 'eErrNoParmInvalidSTNID', '技能列表节点号码不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3002509958, 'eErrNoTowerOfMeteosCfgInvalid', '非正常的米泰欧斯的塔的设定值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3028993425, 'eErrNoParmSkillTreeNodeItemDepthCircle', '技能列表节点道具里存在附属性循环.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3034517979, 'eErrNoContentsGSExchange', '金币交易所相关设置值有误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3040211154, 'eErrNoAbnormalAddTooMany', '状态异常加重过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3044164180, 'eErrNoJBill_UNEXPECTED', '日本 Billing 发生无法预知的错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3048089243, 'eErrNoSkillStateInvalid', '技能尚未激活.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3048536263, 'eErrNoGuildInvalidNo', '公会编号无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3071507787, 'eErrNoUserNotExistId4', '4:用户ID不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3072063702, 'eErrNoGuildCannotOpenMgr', '无法初始化公会Mgr.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3093164026, 'eErrNoIncorrectBaphometRaidOptions', '巴普麦特内容选项有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3113222548, 'eErrNoCantInitializeEx', 'CoInitializeEx初始化失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3117629250, 'eErrNoFailedInitARSAuthMgr', '电话认证管理员初始化失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3120303058, 'eErrNoParmInvalidValue', '参数无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3123431455, 'eErrNoSetItemMemberCntOverOne', '礼包道具应该由两个以上道具构成.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3124573894, 'eErrNoGuildTooMayMember', '公会成员已满.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3134964949, 'eErrNoContentsInCrewCombat', '团队战设定出错。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3147238411, 'eErrNoChinaBillingRvFAIL', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3150149535, 'eErrNoMustItemNotExist', '没有找到必须要存在的物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3181487098, 'eErrNoCantConnectRussia', '俄羅斯要用的');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3182340559, 'eErrNoAttributeTooMany', '属性过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3195928335, 'eErrNoIpFilterTooMany', 'IP过滤器过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3196589283, 'eErrNoJBill_WEBSVR', '日本 Billing Web sever 错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3199767525, 'eErrNoAbnormalNotExistRage', '无愤怒状态异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3203519087, 'eErrNoSkillPackUseTypeInvalid', '技能的使用类型不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3216943426, 'eErrNoNotaVIP', '非常抱歉，您的账号未达到特殊服认证条件，请咨询运营。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3222556125, 'eErrNoUserNotExistId5', '5:用户ID不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3235338393, 'eErrNoHannEverBillInvalidParamter', '无效的限度');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3237186438, 'eErrNoCantFindGuildInfo', '找不到公会的相关信息');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3239645001, 'eErrNoAbnormalResistTooMany', '状态异常抵抗过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3243969837, 'eErrNoJpn_MEMBERID_NOTEXIST', '不存在的用户.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3255958830, 'eErrNoInvalidShopPaymentType', '非正常的支付模式.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3261335808, 'eErrNoContentsPushItemLog', '物品获取日志选项错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3268480955, 'eErrNoCharInvalidLv', '级别无效/异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3280666127, 'eErrNoCnsmCfgInvalidFeeItemCnt', '上架道具数量不符合。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3308140008, 'eErrNoPublisherIpDenied', '被封的IP');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3312320984, 'eLogNoEventDropItem', '活动掉落道具信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3342248240, 'eErrNoFailedExecKISS', 'KISS 执行失败。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3345619573, 'eErrNoPublisherCantRcv', '无法收到Publisher服务器讯息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3346631108, 'eErrNoUserNotExistId1', '1:用户ID不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3364548151, 'eErrNoANTITOXIC_INITED_NOT', '模块未被初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3368810825, 'eErrNoUserNotExistIdR2', '您输入的帐号不存在或从未登陆过游戏.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3371200044, 'eErrNoCharCannotNew', '无法生成角色.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3377358065, 'eErrNoItemTooHeavy', '物品过重,无法装进背包.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3406036852, 'eErrNoParmInvalidSTNILevel', '技能列表节点道具等级不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3409822555, 'Unexpected_Error', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3416620223, 'eErrNoUserDiffIp', 'IP不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3431082556, 'eErrNoHanIPCheckRequestFailed', 'IP检测软件API发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3443692985, 'eErrNoInvalidSkillTreeId', '技能列表号码错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3449470215, 'eErrNoANTITOXIC_DBGW_RPC_CL_CR_FAILED', 'RPCClient 生成失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3468407185, 'eErrNoContentsMonsterRacingInvalid', '怪物竞赛设定值有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3480632477, 'eErrNoContentsInvalidId', '错误的ID.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3485625898, 'eErrNoDevItemConditionNot', '缺少开发时必要的道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3488281168, 'eErrNoJpn_COOKIE_NOTEXIST', 'cookie不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3497378922, 'eErrNoContentsDracoRacingInvalid', '德拉克竞赛设定值有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3512027942, 'eErrNoFailedInitWISM', 'WISM初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3519830633, 'eErrNoUserCharSlotBusy', '相应位置中已经有游戏角色存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3533330100, 'eErrNoAbnormalAttrTooMany', '状态异常属性太多');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3538268095, 'eErrNoANTITOXIC_DBGW_FUNC_INVALID', '非有效 DBGW Query函数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3538431148, 'eErrNoParmInvalidSTIDorMemoryLack', '技能列表号码不准确或者存储不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3539493687, 'eErrNoJpn_RETURNVALUE_INVALID', '认证服务器的返回值不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3569074126, 'eErrNoGuildTooManyBattle', '同一时间内无法进行过多的公会战.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3588126530, 'eErrNoANTITOXIC_DBGW_EXEC_FAILED', 'DBGW 查询失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3598354604, 'eErrNoGuildNotOccupyer', '不是占领区域!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3622132742, 'eErrNoInvalidHanIpCheckString', 'PC检测模块中返回异常数值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3639019747, 'eErrNoJBill_RECV_FAIL', '日本 Billing ERR_RECV_FAIL');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3661027306, 'eErrNoBoardNotExist', '公告版中没有内容.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3669224901, 'eErrNoJpn_IP_INVALID', 'IP地址不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3713870453, 'eContentsPcItemDropRate', '非正常的PC装备掉率');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3721763981, 'eLogNoEventMonsterSpot', '活动怪物袭击信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3724690874, 'eErrNoAnotherSkillNodeDev', '正在开发其他技能节点.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3726938527, 'eErrNoContentsInLetterLimit', '信件发送限制功能设定值错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3737439672, 'eErrNoDropTooMany', '掉落物品过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3757380123, 'eErrNoJpn_AUTHSTRING_INVALID', 'AUTHSTRING不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3758988824, 'eErrNoContentsInMonExpRate', '错误的怪物经验值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3765569158, 'eErrNoRusBlRv_SYSTEM_ERROR', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3779690018, 'eErrNoNotExistsAbnormal', '不存在的效果.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3784704410, 'eErrNoChinaBillingRvR001', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3785195131, 'eErrNoCnsmCfgInvalidFeeItem2', '上架道具不符合。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3816442927, 'eErrNoParmPrevNodeItemInvalid', '前置技能不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3817783755, 'eErrNoAbnormalTooMany', '状态异常情况过多.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3819496844, 'eErrNoItemCantEquipLimitClass', '您的职业无法装备此物品.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3822089456, 'Wrong_ID_or_Password', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3828343195, 'eErrNoJBill_INVALID_XML', '日本 Billing XML设定错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3846727833, 'eErrNoChinaBillingRvU002', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3872811972, 'eErrNoItemExpired', '道具已经过期.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3875259779, 'eErrNoChinaBillingRvR005', '中国Billing API呼出结果');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3894122816, 'eErrNoRussianInterfaceNULL', '俄羅斯要用的');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3903281472, 'eErrNoAttributeNotExist', '属性不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3913048754, 'eErrNoSkillTreeCfgSkillPointEndLvInvalid', '技能点数发生最大等级不准确的设定.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3918378842, 'eErrNoContentsInReinforceItemPenalty', '非正常的强化惩罚值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3924003826, 'eErrNoIncorrectTeamRankOptions', '组队战内容选项有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3924383575, 'eErrNoInvalidDataSetItemAbnormal', '礼包道具设置有误,状态异常.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3931114398, 'eErrNoContentsInSiege', '攻城设定内容不正错.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3957332001, 'eErrNoParmInvalidSTNICorTooManyCond', '不准确的技能列表节点道具条件或有过多的条件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3959474419, 'eErrNoAbnormalResistNotExist', '不存在的状态异常抵抗.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3969946461, 'eErrNoParmInvalidSTNIID', '技能列表节点道具号码不准确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3970510062, 'eErrNoNotExistChaosBattleAllowSvrList', '您的服务器不在混沌对战许可服务器列表里		');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3977955094, 'eErrNoChaosBattleIsNotSupport', '不支持混沌战场系统');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3988783492, 'eErrNoIsNotGoldItem3', '不是金币物品,是仅在镇里可以收领的道具.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3990747870, 'ServiceAccount_Locked_OTP', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 3998069696, 'eErrNoANTITOXIC_Unknown', 'ANTITOXIC 未知错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4000071881, 'eErrNoParmInvalidSkillPackPtr', '技能指针无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4009136910, 'eErrNoSkillUseNotAuth', '没有使用技能的权限.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4010386552, 'ServiceAccount_Locked', '台湾GASH专用');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4019502385, 'eErrNoAbnormalNotExist', '不存在的状态异常情况.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4025323899, 'eErrNoPublisherServiceClose', '非运营时间,详情请登录官方网站');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4033423766, 'eErrNoTaiwanAPIRvAuthkeyMismatch', '验证码错误');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4038483413, 'eErrNoANTITOXIC_DBGW_RPC_CON_FAILED', 'RPCConnector 生成失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4055627264, 'eErrNoModuleOnlyTransform', '只有在变身状态异常情况下才可以加载变身模块.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4106656147, 'eLogNoEventDrop', '活动掉落信息。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4122717154, 'eErrNoInvalidDropGroup', '非正常的怪物掉落模式.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4131944225, 'eErrNoJpn_GETCONNECTION_FAIL', '获取连接信息失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4138724002, 'eErrNoRusBlRv_IP_DENIED', '俄罗斯 API 呼出结果值');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4154061023, 'eErrNoCnsmCfgInvalidFeePercent', '手续费不符。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4174165543, 'eErrNoTaiwanAPIRvBlocked', '服务被停权');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4177609423, 'eErrNoAbnormalAttrDuplicate', '状态异常属性重复');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4194342401, 'eErrNoJpn_ARGUMENT_INVALID', '认证服务器传达信息错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4210129618, 'eErrNoPublisherIdNotActivated', '登录游戏前请输入验证卡号.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4219462956, 'eErrNoContentsChaosBattleConfig', '非正常的混沌战场设定.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4225707523, 'eErrNoMapCannotOpenMgr', '无法初始化Map Mgr.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4238689088, 'eErrNoItemNotRecharged', '道具没有被充值.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4258782385, 'eErrNoPublisherCantAddSCFail', '无法向开发商通知SC失败信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4259876397, 'eErrNoGuildRejectTruce', '对方拒绝了您的休战请求.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4272849866, 'eErrNoParmPrevNodeItemSelf', '前置技能是自身.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4292952153, 'eErrNoHanIpCheckError2', 'IP已注册,但目前处于终止服务状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10003, 4294289553, 'eLogNoEventQuest', '活动任务信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 17393546, 'eErrNoRscNotExist', '资源不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 60339046, 'eLogNoThdEnd', '终止Thread.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 89218599, 'eErrNoPacketBroken', '数据包受损.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 134590733, 'eErrNoLogQueCannotOpen', '无法初始化LOG Queue.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 136774124, 'eErrNoNicCannotFetch', '无法获取NIC信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 138207709, 'eErrNoThdCannotStart', '无法开始Thread.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 139605825, 'eErrNoCharSetDiff', '文字/字符设置不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 142385058, 'eErrNoUserBlockedIp', '此IP已经被禁止.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 258982713, 'eErrNoMissmatchIPAndPcInfo', 'IP和玩家的信息不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 302318361, 'eErrNoFamilyTooMany', 'Family太多啦.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 331704282, 'eErrNoRevolverCantChargeDb', 'DB无法记录子弹信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 336099184, 'eErrNoRscLack', '资源不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 348621111, 'eErrNoSvcModeDiff', '与Service模式不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 387580368, 'eErrNoTrInvalidSz', 'TR大小错误/无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 508470589, 'eLogNoSesStayed', '还有连接到服务器的Session.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 517692403, 'eErrNoPacketInvalidSeqNo', '数据包Seq#无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 538633652, 'eErrNoSvrNotExistLobby', 'Lobby服务器不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 550784421, 'eErrNoWorldInvalidNo', '世界编号无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 560921858, 'eErrNoSvcCannotUninstall', '无法卸载服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 594237236, 'eErrNoSqlCannotMakePool', '无法创建SQL Pool.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 596790510, 'eErrNoTrInvalidEncode', '加密的TR无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 609009483, 'eErrNoLoggerCannotOpen', '无法初始化Logger.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 617899105, 'eErrNoFamilyNot', '非Family.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 671579104, 'eErrNoFamilyInvalidKinship', 'Family关系无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 689616009, 'eLogNoParm', '参数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 708090742, 'eErrNoSvrInvalidNo', '服务器编号无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 711286845, 'eErrNoSqlCannotBeginTran', 'DB Transaction无法开始.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 715329877, 'eErrNoFamilyCannotLoad', '无法获取Family信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 733571858, 'eErrNoBeadHoleProbTblInvalidVal', '符文槽列表有误。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 756422045, 'eErrNoTrNonClient', '此用户使用了非法程序');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 775670973, 'eErrNoThdCannotStop', '无法停止Thread.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 862460986, 'eErrNoInvalidCountry', '错误的国家.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 866711930, 'eErrNoAccessCannotOpen', '无法初始化Access.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 924427881, 'eErrNoSehExcept', 'SHE : Exception!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 929983245, 'eErrNoFactoryCannotOpen', '无法初始化Factory.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 964302814, 'eErrNoThdCannotOpenPool', '无法初始化Thread pool.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 985290027, 'eLogNoSvcInstalled', '服务已经安装.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1032470410, 'eLogNoSesEndAccept', '不接受新进入的连接.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1044320550, 'eLogNoStatisticsNw', 'NW统计!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1146012102, 'eErrNoFamilyNotConnected', '没有连接到Family.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1159066412, 'eErrNoSendInvalidCnt', '发信个数无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1265849644, 'eErrNoParmCannotOpen', '无法初始化参数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1319503486, 'eErrNoFamilyNotExistGw', '无法找到Gateway服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1320855262, 'eErrNoDbQueCannotOpen', '无法初始化DB Queue.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1381630723, 'eErrNoUdpPortInvalid', 'UDP Port无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1382210529, 'eLogNoSvcEnded', '服务已经停止终止.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1397326731, 'eLogNoCallstack', 'Call stack(呼叫叠加?)!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1407367507, 'eErrNoWkCannotOpen', '无法初始化Worker相关内容.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1408657948, 'eErrNoTimerCannotOpen', '无法初始化Timer.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1416601416, 'eErrNoRscCannotReturn', '资源无法Return.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1443432576, 'eErrNoSvrAlreadyExistField', 'Field服务器已经存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1460440913, 'eErrNoSesCannotOpen', '无法初始化Session.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1480182920, 'eErrNoLastInitialFail', '最后的初始化失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1497646729, 'eLogNoThdBegin', '开始Thread.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1515962516, 'eErrNoObjectCantAttach', '附加对象失败');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1517205488, 'eErrNoSvcCannotInstall', '无法安装服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1553633924, 'eLogNoSvcEnd', '终止服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1574471060, 'eErrNoMemoryLeak', '内存溢出!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1604417338, 'eErrNoSesExceedLogin', 'Session连接后认证超时.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1606884452, 'eErrNoOsInvalid', '操作系统错误.(需要Win2003以上).');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1615013721, 'eErrNoNonClient', '不是客户端用户。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1627013158, 'eErrNoThdModeDiff', 'Thread 模式不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1696360927, 'eErrNoSqlCannotOpen', '无法初始化SQL.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1721362679, 'eErrNoTimerEmpty', 'Timer是空的.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1725302870, 'eErrNoSqlCannotOpenSes', '无法初始化SQL Session.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1734967436, 'eErrNoSqlCannotEndTran', 'DB Transaction无法停止.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1753022025, 'eLogNoSvcRcvStop', '收到停止服务的请求.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1787373679, 'eErrNoSvrInvalidType', '服务器类型无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1790811121, 'eErrNoSuperCannotStop', '无法停止Super-class.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1801863765, 'eErrNoSqlFailExec', '运行SQL失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1827172200, 'eErrNoClientMode', '客户端模式.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1854605200, 'eErrNoUserCantDupLoginIp', '此IP无法重复注册.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1855293908, 'eErrNoUserFull', '服务器已经满员,请连接其他服务器,谢谢.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1863095612, 'eErrNoTimerCannotRegister', '无法注册Timer.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1890711124, 'eErrNoTrCannotLoad', '无法获取TR.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1894000431, 'eErrNoIocpCannotPush', '无法在IOCP中添加信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1916995027, 'eLogNoThdEnded', 'Thread已经终止.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1966907668, 'eErrNoThdWkLack', 'Worker thread不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1972666564, 'eErrNoOptModeDiff', '与推荐模式不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 1989641920, 'eErrNoRevolverSlow', '枪械速度缓慢.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2004750363, 'eErrNoParmSvrInfo', '服务器设定信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2006650595, 'eErrNoSuperCannotOpen', '无法初始化Super-class.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2011527817, 'eErrNoRscCannotMakePool', '无法创建资源池.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2026593902, 'eErrNoVerInvalid', '版本异常/无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2036701371, 'eLogNoSvcBegin', '启动服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2036927038, 'eErrNoListenerCannotOpenRsc', '无法初始化Listener的资源.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2057671438, 'eErrNoEtcCannotOpen', '无法初始化其他信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2059837608, 'eErrNoTddModeDiff', '与TDD 模式不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2095588890, 'eErrNoThdTmLack', 'Timer thread不足.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2160599659, 'eErrNoUserAlreadyLogined', '已经完成认证.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2168558820, 'eErrNoRevolverCantOpen', '无法初始化枪械.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2213065475, 'eLogNoFamilyState', 'Family状态!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2247394457, 'eErrNoSendCannotOpen', '无法初始化邮件资源.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2273065830, 'eErrNoL_EndianNot', '非Little-endian.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2322098781, 'eErrNoTimerCannotWait', 'Timer待机状态下发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2343482095, 'eErrNoIocpCannotOpen', '无法初始化IOCP.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2379511888, 'eErrNoBuildModeDiff', 'Build Mode不一致.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2412965047, 'eErrNoUserLoginAnother', '其他用户已认证或已在其他服务器认证.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2476450285, 'eErrNoSesFailedAcceptReq', '失败的接受请求…一般不会发生的…');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2495478095, 'eErrNoCannotSetPriority', '无法设定优先顺序.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2542500627, 'eErrNoSqlCannotReset', '无法重启SQL. 因此服务被停止.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2548955364, 'eErrNoSesInvalidCnt', 'Session个数出错/无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2571980165, 'eErrNoFamilyAlreadyConnect', '已经连接到 Family之中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2596013826, 'eErrNoSqlsCannotOpen', '无法初始化SQL母音.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2597006950, 'eLogNoSvcUninstalled', '服务已经卸载.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2616784484, 'eErrNoInvalidCmd', '命令无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2627388843, 'eErrNoIocpCannotAttach', '无法附加IOCP.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2688689413, 'eErrNoSqlFailReset', 'SQL重启失败!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2723819042, 'eErrNoAuthInvalid', '权限不正确.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2776152056, 'eErrNoDisconnectFromServer', '与服务器断开连接');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2807728555, 'eErrNoLockerCannotOpen', '无法初始化LOCKER.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2828892970, 'eErrNoIocpCannotPop', '无法从IOCP中获取信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2864105256, 'eErrNoRcvBufCannotAlloc', '无法分配接收信息缓存.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2903486667, 'eErrNoSqlReset', '重启SQL.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2913952043, 'eErrNoCryptCannotOpen', '无法初始化加密功能.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2914796043, 'eErrNoPacketBrokenIntegrity', '数据包完整性受损.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2918848119, 'eErrNoRscNotOpenEle', '无法初始化资源中元素.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2930748853, 'eErrNoSqlInternalError', 'DB 内部发生错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2955528661, 'eErrNoSockCannotAccept', '无法请求接受信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2982375806, 'eErrNoSesAlreadyUse', 'Session正在使用中.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2988893229, 'eErrNoTcpPortInvalid', 'TCP Port无效.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 2993997922, 'eErrNoSvcCannotBegin', '无法启动服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3002865559, 'eErrNoSockCannotOpen', '无法初始化Socket.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3071014171, 'eErrNoSvrNotExistField', 'Field服务器不存在.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3162690020, 'eErrNoInvalidSupportServerType', '错误的服务器类型');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3217565762, 'eErrNoTimerCannotBegin', '无法开始Timer.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3218735006, 'eErrNoSockCannotRcv', '无法请求接收信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3222174785, 'eErrNoTrBrokenIntegrity', 'TR完整性受损.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3237468888, 'eLogNoSesValidLogout', '正常登出.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3240968988, 'eErrNoSockCannotListen', '无法侦听.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3255688040, 'eErrNoStatisticsCannotOpen', '无法初始化统计信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3259728876, 'eErrNoSvrNotStopped', '服务器没有被停止.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3308349431, 'eErrNoFamilyCannotConnect', '无法连接Family.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3311006013, 'eLogNoRscClearDangling', '被”Dangling”的资源已经整理完毕.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3343505923, 'eErrNoRscCannotOpen', '无法初始化资源.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3349900324, 'eErrNoSecureAlreadyExchgKey', '安全Key已经变更。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3356250641, 'eErrNoUserNotReserve', '没有预约.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3389338261, 'eErrNoParmCannotLoad', '无法获取参数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3461165659, 'eErrNoUserNotLogin', '尚未认证.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3468457943, 'eErrNoSecureNotExchgedKey', '没有交换安全密钥');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3485134240, 'eErrNoFamilyEmpty', '没有Family. 请确认服务器设置.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3505008188, 'eErrNoFileCannotOpen', '无法打开文件.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3560231164, 'eErrNoRevolverCantReturn', '无法返还子弹.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3566717040, 'eErrNoListenerCannotOpen', 'Listener 无法初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3592002847, 'eErrNoSqlErrorDesc', 'SQL 错误详细内容');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3618874344, 'eErrNoRevolverCantOpenBullet', '无法初始化子弹.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3633238181, 'eErrNoMsgMapCannotLoad', '无法获取MsgMap.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3699165742, 'eErrNoRevolverCantChargeLog', 'LOG无法记录子弹信息.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3707135471, 'eErrNoSqlCannotPrepare', '无法准备SQL.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3775585009, 'eErrNoWinSockCannotOpen', '无法初始化WINSOCK.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3850774223, 'eErrNoHackerDetected', '紧急情况!!! 检测到黑客.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3878080851, 'eErrNoSecureCantMakeKey', '无法生成安全Key。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3889497887, 'eLogNoFamilyConnected', '已经连接到Family.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3939518154, 'eErrNoOverCntLoginIp', '超过了1个IP最大的连接数.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3972547497, 'eErrNoSvrUnregister', '没有注册的服务器.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 3991141072, 'eErrNoSqlCannotOpenEnv', '无法初始化SQL环境.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4007652366, 'eErrNoSesKeepAlive', '一段时间内Network I/O不存在或者是出现了加速齿轮.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4011600965, 'eErrNoSesCannotReuse', '无法再使用Session.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4021924916, 'eLogNoSvcStop', '暂停停止.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4027301337, 'eErrNoTrUnregister', 'TR没有被注册.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4032203570, 'eErrNoUserChkAlreadyLogined', '此帐号已在服务器登录.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4041422938, 'eErrNoSqlReseted', 'SQL已经重启.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4093390362, 'eErrNoThdInvalidCnt', 'Thread个数无效.(个数必须大于1)');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4106732151, 'eErrNoSecureAlreadyInit', '安全Key已经初始化。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4155765416, 'eErrNoFamilyDisconnect', '和Family的连接已断开.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4186539339, 'eErrNoSecureNotInit', '安全Key未被初始化。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4210943706, 'eErrNoSecurityNotEqualKey1', '安全Key不同。');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4217082764, 'eErrNoTimerAlreadyRegister', '这是已经注册的Timer.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4226127394, 'eErrNoSockAcceptFailed', '请求的接受信息失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4230472401, 'eErrNoSehCannotSet', '无法设置SEH.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4250168924, 'eErrNoSqlFailFatch', 'SQL Fetch失败.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4269285234, 'eLogNoStatisticsTr', 'TR统计!');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4272566448, 'eErrNoAuthDeny', '没有权限.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10004, 4293188496, 'eLogNoTrUnsupport', '虽然已经登陆到服务器,但这是当前服务器不支持的TR.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10005, 504361180, 'eLogNoSvrRequestedStop', '已经收到停止服务器的请求.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10005, 851018799, 'eLogNoSvrRequestedStart', '已经收到运行服务器请求.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10005, 1468532984, 'eErrNoSvcCannotStart', '无法运行游戏服务.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10005, 1549049133, 'eErrNoFamilyStateMgrCannotOpen', '无法初始化参数FamilyStateMgr.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10005, 2581453845, 'eErrNoSvcCannotGetStatus', '无法判断服务器状态.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10005, 2593911598, 'eErrNoFieldNot', '领域(Field)错误.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10005, 2844802910, 'eLogNoWorldReseted', '世界已经初始化.');
GO

INSERT INTO [dbo].[TblMsg] ([mMsgGroupID], [mHashID], [mHashStr], [mDesc]) VALUES (10005, 4291402096, 'eErrNoNotExistSameIpUser', '与此ip捆绑的用户列表不存在');
GO

