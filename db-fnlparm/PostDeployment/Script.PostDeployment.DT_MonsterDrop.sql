/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_MonsterDrop
Date                  : 2023-10-07 09:07:30
*/


INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (27, 2, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (397, 352, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (493, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (485, 288, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (452, 362, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1019, 426, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (112, 314, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1019, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (45, 316, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1019, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1019, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (111, 315, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (71, 82, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (476, 190, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (110, 331, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1014, 415, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (103, 330, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (452, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (96, 334, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1014, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (438, 144, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (452, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1014, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (825, 367, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (452, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1014, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1011, 425, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (824, 366, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1011, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (48, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (48, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1011, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1011, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (706, 263, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2492, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (233, 84, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (550, 234, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (156, 29, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (130, 194, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (149, 39, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (196, 23, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (196, 26, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (706, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (706, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (820, 363, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (394, 47, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2492, 722, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2493, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (706, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2493, 723, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (499, 210, 2);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1023, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1023, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (705, 264, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (705, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (393, 44, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (825, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (825, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (705, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (705, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (529, 226, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (704, 266, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (825, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (704, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (824, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (704, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (316, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (824, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (704, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (824, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (703, 265, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (737, 357, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (703, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (703, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (703, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (737, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (737, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (702, 261, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (702, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (737, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (702, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (74, 54, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (651, 303, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (702, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (114, 48, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (701, 262, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (651, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (701, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (651, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (701, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (701, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (651, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (579, 240, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (579, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (454, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (269, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (579, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (454, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (579, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (454, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (578, 239, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (578, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (578, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (578, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (389, 170, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (46, 138, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (389, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (416, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (389, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (416, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (389, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (111, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (371, 101, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (526, 223, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (371, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (524, 222, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (371, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (371, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (517, 213, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (369, 109, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (369, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (369, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (270, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (272, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (274, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (78, 52, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (75, 82, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (79, 61, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (148, 51, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (275, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (277, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (279, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (282, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (369, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (413, 69, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (365, 108, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (365, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (475, 241, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (496, 173, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (365, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (365, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (514, 209, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (364, 107, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (118, 358, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (364, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (364, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (364, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (513, 208, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (511, 205, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (320, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (321, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (111, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (500, 206, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (363, 106, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (155, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (363, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (363, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (363, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (495, 214, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (362, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (489, 191, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (362, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (111, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (70, 244, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (322, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (323, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (108, 333, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (70, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (108, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (108, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (455, 176, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (108, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (103, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (103, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (103, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (96, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (96, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (96, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (874, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (874, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (820, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (820, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (820, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (735, 355, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (735, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (735, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (735, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (397, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (397, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (397, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (358, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (358, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (112, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (112, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (112, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (324, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (325, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (45, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (267, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (45, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (45, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (741, 342, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (741, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (741, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (741, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (659, 307, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (659, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (268, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (271, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (273, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (276, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (278, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (280, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (310, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (315, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (317, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (318, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (319, 89, 10);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (659, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (659, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (122, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (122, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (110, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (110, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (110, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 460, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 492, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 592, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (883, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (653, 283, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (883, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (882, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (882, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (531, 228, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (881, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (881, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (879, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (879, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (878, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (518, 215, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (878, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (876, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (876, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (488, 192, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (875, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (875, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2494, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2494, 723, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (870, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (870, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (742, 353, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (742, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (742, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (742, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (877, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (877, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (872, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (872, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (869, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (869, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2495, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2495, 724, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2447, 713, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2447, 714, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2447, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2448, 614, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2448, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2448, 713, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2448, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2449, 616, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2449, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2449, 713, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2449, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2450, 615, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2450, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1237, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1237, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1237, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1237, 451, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1237, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1236, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (530, 227, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1236, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1236, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1236, 450, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1236, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1235, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1235, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1235, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1235, 449, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1235, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2450, 713, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2450, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2451, 617, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2451, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2451, 713, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1233, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1233, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1233, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1233, 448, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1233, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1019, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1014, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1011, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (706, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (705, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (704, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (703, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (702, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (701, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (579, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (578, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (389, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (371, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (369, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (365, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (364, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (363, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (362, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (361, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (360, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (359, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (154, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (120, 502, 0);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (35, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2263, 662, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2263, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2451, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2498, 726, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2498, 734, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2499, 728, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2499, 734, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2500, 727, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2011, 548, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2007, 549, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2010, 552, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1882, 390, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2500, 734, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2501, 729, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2501, 734, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2502, 730, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1882, 569, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2502, 735, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2503, 732, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2264, 663, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2264, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (109, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2265, 664, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2265, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2266, 665, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2266, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1288, 461, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1288, 492, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1288, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1288, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1288, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 462, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 492, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1309, 743, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 592, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1310, 743, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1308, 459, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1308, 492, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1308, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1308, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1308, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1311, 457, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1311, 492, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1311, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1311, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1311, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1312, 463, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1312, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1312, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1312, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1318, 467, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1318, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1318, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1318, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1317, 470, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1317, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1317, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1317, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1316, 472, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1316, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1316, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1316, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1314, 464, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1314, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1314, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1314, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1313, 468, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1313, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1313, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1313, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1319, 466, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1319, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1319, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1319, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 478, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 493, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 465, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 493, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 476, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 493, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (484, 242, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (403, 260, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (391, 38, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (390, 32, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (336, 1, 30);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (77, 31, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (412, 1, 70);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (415, 188, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (209, 90, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (115, 60, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (76, 49, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (454, 304, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (740, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (693, 271, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (416, 309, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (416, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (693, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (693, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (358, 329, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (358, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (362, 105, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (362, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (122, 310, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (122, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (361, 104, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (361, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (693, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (662, 276, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (662, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (662, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (89, 348, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (89, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (48, 345, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (48, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (43, 317, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (43, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (662, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (528, 225, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (528, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (528, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (361, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (361, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (528, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (525, 238, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (883, 391, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (883, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (882, 392, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (882, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (881, 385, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (881, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (879, 386, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (879, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (878, 393, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (878, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (877, 387, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (877, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (876, 389, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (876, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (875, 388, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (875, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (874, 383, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (874, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (859, 497, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (859, 481, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (872, 394, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (872, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (360, 103, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (360, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (870, 395, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (870, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (869, 396, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (869, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (843, 400, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (841, 399, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (525, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (832, 401, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (843, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (841, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (525, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (832, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2076, 613, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2076, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (360, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (360, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (359, 102, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (359, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (128, 42, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (667, 257, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (465, 171, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (395, 50, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (411, 236, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (147, 85, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (664, 275, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (494, 185, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (502, 195, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (498, 198, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (508, 202, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (509, 203, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (510, 204, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (113, 313, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (117, 312, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (497, 24, 5);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (512, 207, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (497, 23, 2);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (497, 128, 3);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (492, 24, 5);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (492, 23, 2);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (492, 128, 3);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (491, 24, 5);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (491, 23, 2);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (491, 128, 3);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (490, 24, 5);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (490, 23, 2);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (490, 128, 3);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (467, 24, 5);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (467, 23, 2);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (467, 128, 3);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (359, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (698, 300, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (506, 200, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (504, 197, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (503, 196, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (487, 189, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (482, 186, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (480, 172, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (479, 174, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (478, 187, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (447, 136, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (446, 135, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (445, 134, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (94, 78, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (392, 41, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (359, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (154, 340, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (154, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (154, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (154, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1001, 404, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (999, 402, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1002, 405, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1000, 403, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (120, 311, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (120, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (120, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (120, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (35, 354, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1005, 408, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1003, 407, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (525, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (451, 159, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1012, 412, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1012, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1010, 421, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1010, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1007, 422, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1007, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (451, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (451, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (451, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (450, 158, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (450, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (450, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (450, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (444, 165, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (444, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (444, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (444, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (197, 169, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (197, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (197, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1022, 419, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1022, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1020, 417, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1020, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1023, 416, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1023, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (130, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (77, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (156, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (390, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1001, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1002, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1000, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (550, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (465, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (395, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (394, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (393, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (391, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (149, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (79, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (76, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1005, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1003, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (498, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (496, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (411, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (392, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (209, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (148, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (147, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (115, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (78, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (75, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1798, 531, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (480, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (479, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (197, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (35, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (35, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (35, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2268, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2265, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2266, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2267, 666, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2267, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2267, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2268, 667, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2268, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2076, 587, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2077, 614, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2077, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2077, 588, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2078, 616, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2078, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2078, 589, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2079, 615, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2079, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2079, 590, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2080, 617, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2080, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2080, 591, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2150, 626, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2151, 627, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2152, 628, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2153, 629, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2154, 630, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2155, 631, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2156, 632, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2157, 633, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2158, 634, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2159, 635, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2160, 636, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1871, 771, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2571, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2575, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2610, 772, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2611, 773, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2612, 774, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2613, 775, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2614, 777, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (859, 482, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (859, 483, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (859, 484, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (859, 485, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2047, 574, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2048, 572, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2049, 573, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2059, 575, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (859, 495, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (859, 496, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1312, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1312, 743, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1246, 689, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1247, 690, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1248, 691, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1249, 692, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2615, 778, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2616, 779, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 578, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (109, 119, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (109, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (109, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (109, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 224, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2617, 780, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2618, 776, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2619, 781, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2620, 782, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2621, 782, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2622, 786, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2623, 786, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2610, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2611, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2612, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2613, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2614, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2615, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (832, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (843, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (841, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (131, 92, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (131, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (131, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1012, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1010, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1007, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (131, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (105, 168, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (105, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (105, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (105, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (101, 193, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (101, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (101, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (101, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (91, 217, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (91, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (91, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (91, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (43, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (43, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (470, 183, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (470, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (470, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (470, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (443, 164, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (443, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (443, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (443, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (90, 336, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (90, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (90, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (90, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (40, 372, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (40, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (40, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (40, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (37, 66, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (37, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (37, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (37, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (823, 365, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (823, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (823, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (823, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (738, 341, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (738, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (738, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (738, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (699, 319, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (699, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (699, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (699, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (535, 232, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (535, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (535, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (535, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (533, 230, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (533, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (533, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (533, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (532, 321, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (532, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (532, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (532, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (527, 320, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (527, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (527, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (527, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (522, 220, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (522, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (522, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (522, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (521, 322, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (521, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (521, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (521, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (477, 325, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (477, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (477, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (477, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (473, 326, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (473, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (473, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (473, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (459, 302, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (459, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (459, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (459, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (89, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (89, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 473, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 493, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2058, 577, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2058, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2059, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2060, 576, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2060, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2071, 613, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1022, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1022, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1020, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1020, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (827, 371, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (827, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (827, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (827, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (819, 361, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (819, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (819, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (819, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (739, 343, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (739, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (739, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (739, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (736, 356, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (736, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (736, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (736, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (534, 231, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (534, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (534, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (534, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (505, 308, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (505, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (505, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (505, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (501, 339, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (501, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (501, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (501, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (493, 184, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (493, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (493, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1528, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1527, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1526, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1525, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1523, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1524, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1522, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1521, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1520, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1519, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1518, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1517, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1516, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1515, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1514, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1513, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1512, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1511, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1510, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1509, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1508, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1507, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1506, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1505, 491, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (843, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (843, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (841, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (841, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (840, 398, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (840, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (840, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (840, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (840, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (80, 116, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (80, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (80, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (80, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (80, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1013, 414, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1013, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1013, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1013, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1013, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1012, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1010, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1007, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1006, 424, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1006, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1006, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1194, 440, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1193, 439, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1192, 438, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1191, 437, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1189, 447, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1190, 446, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1187, 441, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1186, 444, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1185, 443, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1184, 445, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1188, 442, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1006, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1006, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1004, 406, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1004, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1004, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1004, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1004, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (697, 268, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (697, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (697, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (697, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (697, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (650, 285, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (650, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (650, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (650, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (650, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1280, 452, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1282, 453, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (649, 286, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (649, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (649, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (649, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1283, 454, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1285, 455, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1284, 456, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (649, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (648, 287, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (648, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (648, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (648, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (648, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (481, 175, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (481, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (481, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (481, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (481, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (463, 289, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1303, 481, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (463, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (463, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (463, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (463, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (437, 133, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (437, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (437, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (437, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (437, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (436, 163, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (436, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (436, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (436, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (436, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (418, 72, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (418, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (418, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (418, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (418, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (410, 243, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (410, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (410, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (410, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (410, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (396, 53, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (396, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (396, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1248, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1248, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1248, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1248, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1249, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1249, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1249, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1249, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1247, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1247, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1247, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1247, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1246, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1246, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1246, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1246, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (396, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (396, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (86, 127, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (86, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (86, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (86, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (86, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (83, 43, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (83, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (83, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (83, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (83, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (81, 55, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (81, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1302, 481, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1302, 482, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1302, 483, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1302, 484, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1302, 485, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1303, 482, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1303, 483, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1303, 484, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1303, 495, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1304, 481, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1304, 482, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1304, 483, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1304, 484, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1304, 496, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1305, 481, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1305, 482, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1305, 483, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1305, 484, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1305, 497, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (832, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1449, 486, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1492, 46, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1493, 43, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1472, 487, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1473, 488, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1475, 489, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1461, 490, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (81, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (81, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (81, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1017, 410, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1017, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1017, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1017, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1017, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1016, 409, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1016, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1016, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1016, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1016, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1486, 494, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1015, 413, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1015, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1015, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1015, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1015, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (835, 368, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (835, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (835, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (835, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (835, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (821, 364, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (821, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (821, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (821, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (821, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (694, 270, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (694, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (694, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (694, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (694, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (657, 279, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (657, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (657, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (657, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (657, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (507, 201, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (507, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (507, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (507, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (507, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (483, 324, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (483, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (483, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (483, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (483, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (441, 156, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (441, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (441, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (441, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (441, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (409, 67, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (409, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (409, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (409, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (409, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (405, 64, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (405, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (405, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (405, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (405, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (404, 97, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (404, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (404, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (404, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (404, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (402, 71, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (402, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (402, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (402, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (402, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (399, 74, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (399, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (399, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (399, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (399, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (398, 301, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (398, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (398, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (398, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (398, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (153, 88, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (153, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (153, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (153, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (153, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (146, 91, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (146, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (146, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (146, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (146, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (145, 87, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (145, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (145, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (145, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (145, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (125, 132, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (125, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (125, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (125, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (125, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (124, 143, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (124, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (124, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (124, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (124, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (88, 62, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (88, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (88, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (88, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (88, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (85, 56, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (85, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (85, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (85, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (85, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (41, 318, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (41, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (41, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (41, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (41, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (33, 126, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (33, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (33, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (33, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (33, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1009, 420, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1009, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1009, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1009, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1009, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1008, 423, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1008, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1008, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1008, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1008, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (696, 269, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (696, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (696, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (696, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (696, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (666, 273, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (666, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (666, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (666, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (666, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (665, 274, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (665, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (665, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (665, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (665, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (658, 278, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (658, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (658, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (658, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (658, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (656, 280, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (656, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (656, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (656, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (656, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (654, 282, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (654, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (654, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (654, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (654, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (652, 284, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (652, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (652, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (652, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (652, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (515, 211, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (515, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (515, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (515, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (515, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (486, 323, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (486, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (486, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (486, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (486, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (469, 182, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (469, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (469, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (469, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (469, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (468, 181, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (468, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (468, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (468, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (468, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (466, 180, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (466, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (466, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (466, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (466, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (462, 290, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (462, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (462, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (462, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (462, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (461, 291, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (461, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (461, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (461, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (461, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (460, 292, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (460, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (460, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (460, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (460, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (457, 177, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (457, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (457, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (457, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (457, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (456, 178, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (456, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (456, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (456, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (456, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (442, 157, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (442, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (442, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (442, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (442, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (440, 146, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (440, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (440, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (440, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (440, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (439, 145, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (439, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (439, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (439, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (439, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (406, 110, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1334, 744, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1321, 475, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1321, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1321, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1335, 744, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2570, 55, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2618, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2619, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (539, 784, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1871, 601, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2379, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2379, 715, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2399, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2399, 715, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2397, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2397, 715, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2390, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2390, 716, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2392, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (406, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (406, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (406, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (406, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (400, 75, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (400, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (400, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (400, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (400, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (123, 142, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (123, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (123, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (123, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (123, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (119, 347, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (119, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (119, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (119, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (119, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (107, 151, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (107, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (107, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (107, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (107, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (106, 162, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (106, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (106, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (106, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (106, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (104, 68, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (104, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (104, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (104, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (104, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (100, 294, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (100, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (100, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (100, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (100, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (92, 76, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (92, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (92, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (92, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (92, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (87, 73, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (87, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (87, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (87, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (87, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (82, 117, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (82, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (82, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (82, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (82, 504, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2404, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2392, 716, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2396, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2396, 716, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2398, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1336, 744, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1337, 744, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (692, 272, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (692, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (692, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (692, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (692, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (660, 277, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (660, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (660, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (660, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (660, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (655, 281, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (655, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (655, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (655, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (655, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (646, 305, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (646, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (646, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (646, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (646, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (523, 337, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (523, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (523, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (523, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (523, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (520, 338, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (520, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (520, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (520, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (520, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (519, 216, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (519, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (519, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (519, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (519, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (516, 212, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (516, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (516, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (516, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (516, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (474, 335, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (474, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (474, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (474, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (474, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (471, 332, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (471, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (471, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (471, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (471, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (458, 179, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (458, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (458, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (458, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (458, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (458, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (449, 148, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (449, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (449, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (449, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (449, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (448, 147, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (448, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (448, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (448, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (448, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (417, 98, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (417, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (417, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (417, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (417, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (407, 328, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (407, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (407, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (407, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (407, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (49, 161, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (49, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (49, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (49, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (49, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (36, 349, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (36, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (36, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (36, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (36, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (34, 350, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (34, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (34, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (34, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (34, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 480, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 493, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (740, 346, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (740, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (740, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (740, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (693, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (662, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (528, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (525, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (451, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (450, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (444, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (197, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (131, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (105, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (101, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (91, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (43, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (470, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (443, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (90, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (40, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (37, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (823, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (738, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (699, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (535, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (533, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (527, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (532, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (522, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (521, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (477, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (473, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (459, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (89, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1288, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1022, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1020, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (827, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (819, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (739, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (736, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (534, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (505, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (501, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (493, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (452, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (48, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1308, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2398, 716, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1023, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (825, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (824, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (737, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (651, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (454, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (416, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (111, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (108, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (103, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (96, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1311, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (874, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (820, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (735, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (397, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (358, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (112, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (45, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1338, 744, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (741, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (659, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (122, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (110, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 471, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 493, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (883, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (882, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (881, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (879, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (878, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (876, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (875, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2402, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (870, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (742, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (877, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (872, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (869, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2402, 716, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2380, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2380, 717, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1799, 532, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1802, 535, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1803, 536, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1804, 537, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1805, 538, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1806, 539, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1807, 540, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1808, 541, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1809, 542, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1810, 543, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1811, 544, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1812, 545, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1315, 546, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1528, 511, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1526, 511, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1525, 511, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1524, 511, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1523, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1522, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1521, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1520, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1519, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1518, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1517, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1516, 511, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1515, 511, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1514, 511, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1513, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1512, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1511, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1510, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1509, 510, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1508, 509, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1507, 509, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1506, 509, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1505, 509, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1874, 327, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2381, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2008, 550, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2381, 717, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2395, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2395, 717, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2404, 717, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2391, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2391, 718, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2393, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2393, 718, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2401, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2401, 718, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2382, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2382, 719, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2394, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2394, 719, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2400, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2400, 719, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1288, 592, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1308, 592, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (87, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (88, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (124, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (125, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (405, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (437, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (481, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (648, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (649, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (650, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1004, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1009, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1015, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1016, 593, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (33, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (41, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (80, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (81, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (82, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (83, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (85, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (86, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (98, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (100, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (104, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (105, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (107, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (119, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (123, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (127, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (129, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (145, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (146, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (153, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (396, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (398, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (399, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (400, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (402, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (404, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (406, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (409, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (410, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (418, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (436, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (439, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (440, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (441, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (442, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (448, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (449, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (456, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (457, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (460, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (462, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (463, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (483, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (486, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (507, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (515, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (528, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (646, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (652, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (656, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (657, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (658, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (660, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (665, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (666, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (694, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (696, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (697, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (832, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (840, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (841, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (843, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1006, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1527, 511, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (75, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1007, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1008, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1010, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1012, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2526, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1013, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1017, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1311, 595, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (35, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (120, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (154, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (359, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (360, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (361, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (362, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (363, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (364, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (365, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (369, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (371, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (389, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (578, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (579, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (701, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (702, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (703, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (704, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (705, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (706, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1011, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1014, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1019, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (109, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1339, 744, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 474, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 493, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (869, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (872, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (877, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (742, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (870, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (875, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (876, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (878, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (879, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (881, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (882, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (883, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1321, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (110, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (129, 293, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (129, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (129, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (129, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (127, 46, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (127, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (127, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (127, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (122, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (659, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (741, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1311, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (45, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (112, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (358, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (397, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (735, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (820, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (874, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1308, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (539, 749, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (539, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (539, 766, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2526, 746, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2526, 750, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2526, 748, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (96, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (103, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (108, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (111, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (416, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (454, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (651, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (737, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (824, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (825, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1023, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (89, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (459, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (473, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (477, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (521, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (522, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (527, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (532, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (533, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (535, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (699, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (738, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (823, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1288, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (48, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (452, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (493, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (501, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (505, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (534, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (736, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (739, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (819, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (827, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1020, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1022, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (37, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (40, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (90, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (443, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (470, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (662, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (693, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (740, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (43, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (91, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (101, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (105, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (131, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (197, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (444, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (450, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (451, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (525, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (528, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (646, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (655, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (660, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (692, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (34, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (36, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (49, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (73, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (97, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (74, 429, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (98, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (407, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (417, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (448, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (449, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (453, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (458, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (471, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (474, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (516, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (519, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1313, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (520, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1314, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (523, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1316, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (652, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1317, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (654, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1318, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (656, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1319, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (658, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1320, 477, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1320, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1320, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1320, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1320, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (665, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2526, 766, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1881, 547, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (666, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (696, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1008, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1009, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (82, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (87, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (92, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (100, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (104, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (106, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (107, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (119, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (123, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (400, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (406, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (439, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (440, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (442, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (456, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (457, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (460, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (461, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (462, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (466, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (468, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (469, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (486, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (515, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (657, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (694, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (821, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (835, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1015, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1016, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1017, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (33, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (41, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (85, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (88, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (124, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (125, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (145, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (146, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (153, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (398, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (399, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (402, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (404, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (405, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (409, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (441, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (483, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (507, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (648, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (649, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (650, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (697, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1004, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1006, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1007, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1010, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1012, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1013, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (667, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (840, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (841, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (843, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1003, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1005, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (81, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (83, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (86, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (127, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (129, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (396, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (410, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (418, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (436, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (437, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (463, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (479, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (480, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (481, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (74, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (78, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (80, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (115, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (147, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (148, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (209, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (392, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (411, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (496, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (498, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1313, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1314, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1316, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1317, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1318, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1319, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1320, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1881, 766, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (34, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (36, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (40, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (43, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (45, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (48, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (49, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (73, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (89, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (90, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (91, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (92, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (96, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (97, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (101, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (103, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (106, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (108, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (111, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (112, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (131, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (397, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (407, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (417, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (443, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (444, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (450, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (451, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (453, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (454, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (458, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (461, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (466, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (468, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (469, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (471, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (473, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (474, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (477, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (501, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (505, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (516, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (520, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (521, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (523, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (525, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (527, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (532, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (651, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (654, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (655, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (662, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (693, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (699, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (735, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (736, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (737, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (738, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (739, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (740, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (741, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (819, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (823, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (37, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (197, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (416, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (452, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (459, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (470, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (519, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (533, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (535, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (659, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (453, 306, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (453, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (453, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (453, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (692, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (742, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (821, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (827, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (835, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (879, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (534, 598, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (110, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (358, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (820, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (870, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (874, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (875, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (876, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (882, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (883, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (98, 63, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (98, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (98, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (98, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (97, 344, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (97, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (97, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (97, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (73, 167, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (73, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (73, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (73, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1023, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (122, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (493, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (522, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (824, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (825, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (877, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (878, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (881, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1020, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1022, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1805, 601, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1806, 601, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1807, 601, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1809, 601, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1798, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1808, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (869, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (872, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1313, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1314, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1316, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1317, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1799, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1800, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1801, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1802, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1803, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1804, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1810, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1882, 766, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1321, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 373, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 581, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 766, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 247, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 578, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2569, 29, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2571, 421, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (818, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2572, 409, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2573, 410, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1800, 533, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2009, 551, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2071, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2403, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1811, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1812, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1318, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1319, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1320, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2574, 407, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2403, 719, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2263, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2264, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2575, 420, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2547, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2616, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2385, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2385, 720, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2383, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2383, 720, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2384, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2384, 720, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2447, 613, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2503, 735, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2504, 735, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2505, 733, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2387, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2387, 721, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2505, 735, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2498, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2405, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2405, 724, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (579, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (578, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (362, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (389, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1011, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (369, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (361, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (365, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (364, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (363, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1014, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (705, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (701, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (702, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (706, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (703, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (704, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (359, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (371, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (360, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (35, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1019, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (154, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (109, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (120, 605, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2406, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2406, 723, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2407, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2407, 722, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2408, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2408, 725, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (70, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2500, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2501, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2502, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2149, 617, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2503, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2504, 731, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2149, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2504, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2505, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1312, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1340, 744, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2260, 681, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2260, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2260, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2260, 745, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2262, 683, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2262, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2262, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2262, 745, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (35, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (120, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (154, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (359, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (360, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (361, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (362, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (363, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (364, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (365, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (369, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (371, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (389, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (578, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (579, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (701, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (702, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (703, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (704, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (705, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (706, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1011, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1014, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1019, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1233, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2252, 673, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1235, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1236, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1237, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2252, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2252, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2252, 745, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2253, 674, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2253, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2253, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2253, 745, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (109, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2258, 679, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2258, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2258, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2258, 745, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2257, 678, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2071, 582, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2071, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2072, 614, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2072, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2257, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2257, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2257, 745, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2256, 677, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2256, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2256, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2256, 745, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2255, 676, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2255, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2255, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2255, 745, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2075, 617, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2310, 667, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2311, 668, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2312, 657, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2313, 658, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2319, 685, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2072, 583, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2072, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2073, 616, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2073, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2073, 584, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2073, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2074, 615, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2074, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2074, 585, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2074, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2075, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2075, 586, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2075, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2076, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2077, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2078, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 390, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (871, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (895, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (895, 397, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (895, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (895, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (895, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2405, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2495, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2406, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2493, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2494, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2407, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2492, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2408, 767, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 359, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (854, 768, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 360, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (855, 768, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1999, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1999, 570, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1999, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1999, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2000, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2000, 571, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2000, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2000, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2318, 684, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2318, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2318, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2319, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2319, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1636, 515, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1636, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1636, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1636, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1640, 519, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1640, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1640, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1640, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1644, 523, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1644, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1644, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1644, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1648, 527, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1648, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1648, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1648, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2372, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2372, 720, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2372, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2383, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2384, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2385, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2387, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2079, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2080, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2140, 613, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2140, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2140, 637, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2140, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2141, 614, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2141, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2141, 637, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2141, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2142, 616, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2142, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2142, 637, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2142, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2143, 615, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2143, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2143, 637, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2143, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2144, 617, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2144, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2144, 637, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2144, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2145, 613, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2145, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2145, 638, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2145, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2146, 614, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2146, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2146, 638, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2146, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2147, 616, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2147, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2147, 638, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2147, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2148, 615, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2148, 618, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2148, 638, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2148, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2149, 638, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2149, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2307, 648, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2307, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2308, 649, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2308, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2309, 650, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2309, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2315, 649, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2315, 599, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2388, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2388, 721, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2388, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2386, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2386, 720, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2386, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2389, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2389, 720, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2389, 769, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2310, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2311, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2312, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2313, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (538, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1801, 534, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2447, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1228, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1245, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2499, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1228, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1228, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1228, 505, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1228, 736, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1229, 378, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1229, 432, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1229, 498, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1229, 641, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1230, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1230, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1230, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1230, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1230, 737, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1231, 379, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1231, 433, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1231, 499, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1231, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1231, 738, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1242, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1242, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1242, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1242, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1242, 739, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1243, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1243, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2548, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2617, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1243, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1243, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2454, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2454, 693, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2454, 701, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2455, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2455, 694, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2455, 701, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2456, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2456, 695, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2456, 701, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2457, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2457, 696, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2457, 701, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2458, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2458, 697, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2458, 702, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2459, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2459, 698, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2459, 702, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2460, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2460, 699, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2460, 702, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2461, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2461, 700, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2461, 702, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1243, 740, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1244, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1244, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1244, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1244, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1244, 741, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1245, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1245, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1245, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1245, 742, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1312, 595, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (537, 95, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1321, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1633, 512, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1633, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1633, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1633, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1634, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1232, 380, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1232, 434, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1232, 500, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1232, 506, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1232, 642, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1234, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1234, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1234, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1234, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1234, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1234, 643, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1238, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1238, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1238, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1238, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1238, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1238, 644, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1239, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1239, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1239, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1239, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1239, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1239, 645, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1240, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1240, 418, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1240, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1240, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1240, 610, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1240, 646, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1241, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1241, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1241, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1241, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1241, 647, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1634, 513, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1634, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1634, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1635, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1635, 514, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1635, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1635, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1637, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1637, 516, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1637, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1637, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1638, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1638, 517, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1638, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1638, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1639, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1639, 518, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1639, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1639, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1641, 520, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1641, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1641, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1641, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1642, 521, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1642, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1642, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1642, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1643, 522, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1643, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1643, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1643, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1645, 524, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1645, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1645, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1645, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1646, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1646, 525, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1646, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1646, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1647, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1647, 526, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1647, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1647, 747, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1993, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1994, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1995, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1996, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1997, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1998, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 58, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (93, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 237, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (95, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 299, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (99, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 256, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (102, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 120, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (401, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 77, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 430, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 529, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 606, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 611, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (408, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2536, 42, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2537, 194, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2538, 32, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2539, 44, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2540, 402, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2542, 404, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2543, 405, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2544, 406, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2541, 403, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2545, 408, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2546, 424, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2547, 422, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2548, 423, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2549, 549, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2550, 550, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2552, 552, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2553, 548, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2551, 551, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 479, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 493, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1341, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1993, 558, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1993, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1994, 559, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1994, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1995, 560, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1995, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1996, 566, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1996, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1997, 567, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1997, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1998, 568, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1998, 710, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1983, 553, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1983, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1983, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1984, 554, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1984, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1984, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1985, 555, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1985, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1985, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1986, 556, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1986, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1986, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1987, 557, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1987, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1987, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1988, 561, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1988, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1988, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1989, 562, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1989, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1989, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1990, 563, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1990, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1990, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1991, 564, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1991, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1991, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1992, 565, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1992, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1992, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2254, 675, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2254, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2254, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2259, 680, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2259, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2259, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2261, 682, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2261, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2261, 711, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2269, 668, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2269, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2269, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2270, 669, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2270, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2270, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2271, 670, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2271, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2271, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2272, 671, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2272, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2272, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2273, 672, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2273, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2273, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2274, 651, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2274, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2274, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2275, 652, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2275, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2275, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2276, 653, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2276, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2276, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2277, 654, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2277, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2277, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2278, 655, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2278, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2278, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2279, 656, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2279, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2279, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2280, 657, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2280, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2280, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2281, 658, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2281, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2281, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2282, 659, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2282, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2282, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2283, 660, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2283, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2283, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2284, 661, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2284, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2284, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2285, 662, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2285, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2285, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2286, 663, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2286, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2286, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2287, 664, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2287, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2287, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2288, 665, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2288, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2288, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2289, 666, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2289, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2289, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2290, 667, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2290, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2290, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2291, 668, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2291, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2291, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2292, 669, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2292, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2292, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2293, 670, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2293, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2293, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2294, 671, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2294, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2294, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2295, 672, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2295, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2295, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2296, 651, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2296, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2296, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2297, 652, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2297, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2297, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (873, 435, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (873, 501, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (873, 384, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (873, 381, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (873, 507, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (873, 528, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (873, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2298, 653, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2298, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2298, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2299, 654, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2299, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2299, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2300, 655, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2300, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2300, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2301, 656, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2301, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2301, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2302, 657, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2302, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2302, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2303, 658, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2303, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2303, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2304, 659, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2304, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2304, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2305, 660, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2305, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2305, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2306, 661, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2306, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2306, 712, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2581, 751, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2581, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2582, 752, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2582, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2583, 753, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2583, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2584, 754, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2584, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2585, 755, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2585, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2586, 756, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2586, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2587, 757, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2587, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2588, 758, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2588, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2589, 759, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2589, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2590, 760, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2590, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2591, 761, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2591, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2592, 762, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2592, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2593, 763, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2593, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2594, 764, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2594, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2595, 765, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2595, 608, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2570, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 578, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 297, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (84, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 351, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (121, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 327, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (472, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 233, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (536, 578, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 246, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 578, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (540, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 245, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 578, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (577, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 298, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 578, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (661, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 427, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 639, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1018, 706, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 502, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 428, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 382, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 431, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 436, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 530, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 607, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 612, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 640, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (1021, 707, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2672, 787, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2672, 797, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2673, 788, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2673, 797, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2674, 789, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2674, 797, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2675, 790, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2675, 797, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2676, 791, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2676, 797, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2677, 792, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2677, 798, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2678, 793, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2678, 798, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2679, 794, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2679, 798, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2680, 795, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2680, 798, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2681, 796, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2681, 798, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2672, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2673, 594, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2674, 595, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2675, 595, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2676, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2677, 595, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2678, 595, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2679, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2680, 596, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2681, 597, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2692, 799, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2693, 800, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2694, 801, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2695, 802, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2696, 803, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2697, 804, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2698, 807, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2699, 808, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2700, 805, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2701, 806, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2702, 809, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2703, 810, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2704, 811, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2705, 812, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2706, 813, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2707, 814, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2708, 815, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2709, 816, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2710, 817, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2711, 818, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2712, 819, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2713, 820, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2714, 821, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2715, 822, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2716, 823, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2717, 824, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2718, 825, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2719, 836, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2720, 826, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2721, 827, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2722, 828, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2723, 829, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2724, 830, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2725, 831, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2726, 832, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2727, 833, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2728, 834, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2729, 835, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2730, 837, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2709, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2692, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2693, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2694, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2695, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2696, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2697, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2698, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2699, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2700, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2701, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2702, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2703, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2704, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2705, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2706, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2707, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2708, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2710, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2711, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2712, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2713, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2714, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2715, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2716, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2717, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2718, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2719, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2720, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2721, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2722, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2723, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2724, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2725, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2726, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2727, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2728, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2729, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2730, 604, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2709, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2710, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2711, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2712, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2713, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2714, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2715, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2716, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2717, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2718, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2719, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2720, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2721, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2722, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2723, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2724, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2725, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2726, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2727, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2728, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2729, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2730, 841, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2719, 840, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2730, 840, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2858, 848, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2859, 849, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2860, 850, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2861, 851, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2862, 852, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2863, 853, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2864, 854, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2865, 855, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2858, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2859, 600, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2860, 601, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2861, 601, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2862, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2863, 603, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2864, 602, 100);
GO

INSERT INTO [dbo].[DT_MonsterDrop] ([MID], [DGroup], [DPercent]) VALUES (2865, 602, 100);
GO

