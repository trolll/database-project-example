/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblPopupGuideCondition
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (1, 1, 4, 153, 0, 0, 1, 1);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (2, 2, 1, 2, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (3, 3, 1, 3, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (4, 4, 1, 4, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (5, 5, 1, 5, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (6, 6, 4, 865, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (7, 7, 1, 7, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (8, 8, 1, 10, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (9, 9, 4, 351, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (10, 9, 4, 353, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (11, 10, 4, 918, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (12, 10, 4, 919, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (13, 10, 4, 920, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (14, 10, 4, 923, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (15, 10, 4, 924, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (16, 10, 4, 925, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (17, 10, 4, 921, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (18, 10, 4, 922, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (19, 10, 4, 937, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (20, 10, 4, 973, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (21, 11, 4, 411, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (22, 12, 1, 20, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (23, 13, 1, 14, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (24, 14, 1, 11, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (25, 15, 1, 12, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (26, 16, 2, 234, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (27, 17, 2, 143, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (28, 18, 2, 9, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (29, 19, 2, 11, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (30, 20, 2, 16, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (31, 21, 2, 14, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (32, 22, 2, 21, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (33, 23, 2, 20, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (34, 24, 2, 23, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (35, 25, 2, 32, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (36, 26, 2, 18, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (37, 27, 2, 12, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (38, 28, 2, 39, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (39, 29, 2, 49, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (40, 30, 2, 59, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (41, 31, 2, 51, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (42, 32, 2, 66, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (43, 33, 2, 42, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (44, 34, 2, 50, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (45, 35, 2, 41, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (46, 36, 2, 43, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (47, 37, 2, 40, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (48, 38, 2, 73, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (49, 39, 2, 82, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (50, 40, 2, 79, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (51, 41, 2, 78, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (52, 42, 2, 77, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (53, 43, 2, 84, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (54, 44, 2, 89, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (55, 45, 2, 81, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (56, 46, 2, 74, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (57, 47, 2, 94, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (58, 48, 2, 114, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (59, 49, 2, 113, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (60, 50, 2, 119, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (61, 51, 2, 117, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (62, 52, 2, 116, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (63, 53, 2, 118, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (64, 54, 2, 126, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (65, 55, 2, 121, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (66, 56, 2, 123, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (67, 57, 2, 95, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (68, 58, 2, 166, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (69, 59, 2, 136, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (70, 60, 2, 168, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (71, 61, 2, 227, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (72, 62, 1, 30, 0, 0, 10, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (73, 63, 5, 0, 18, 22, 10, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (74, 64, 1, 13, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (75, 65, 1, 15, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (76, 66, 4, 1046, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (77, 67, 4, 6230, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (78, 67, 1, 16, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (79, 68, 4, 2836, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (80, 68, 4, 2837, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (81, 68, 4, 5901, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (82, 68, 4, 5905, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (83, 68, 4, 5906, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (84, 68, 4, 5907, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (85, 69, 4, 1938, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (86, 70, 4, 4891, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (87, 70, 4, 4892, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (88, 70, 4, 4893, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (89, 70, 4, 4894, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (90, 71, 4, 5608, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (91, 72, 4, 5573, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (92, 73, 4, 5572, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (93, 74, 1, 17, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (94, 75, 4, 5962, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (95, 75, 4, 5963, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (96, 75, 4, 5964, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (97, 75, 4, 5965, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (98, 75, 4, 5966, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (99, 75, 4, 5967, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (100, 75, 4, 5968, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (101, 75, 4, 5969, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (102, 75, 4, 5970, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (103, 75, 4, 5971, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (104, 76, 4, 5898, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (105, 76, 4, 5899, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (106, 76, 4, 5900, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (107, 77, 1, 18, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (108, 78, 4, 5569, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (110, 79, 4, 355, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (111, 80, 4, 1622, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (112, 80, 4, 1621, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (113, 16, 2, 235, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[TblPopupGuideCondition] ([mConID], [mGuideNo], [mConType], [mAParm], [mBParm], [mCParm], [mReadCnt], [mForce]) VALUES (114, 16, 2, 236, 0, 0, 1, 0);
GO

