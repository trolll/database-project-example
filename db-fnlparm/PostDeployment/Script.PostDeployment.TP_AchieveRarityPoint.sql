/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_AchieveRarityPoint
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TP_AchieveRarityPoint] ([mName], [mRarityID], [mRarityPoint], [mLevelHP], [mLevelMP], [mLevelWP], [mWeightHP], [mWeightMP], [mWeightWP]) VALUES ('레젼드', 1, 0, 7.0, 2.5, 20.0, 0.001, 0.00025, 0.01);
GO

INSERT INTO [dbo].[TP_AchieveRarityPoint] ([mName], [mRarityID], [mRarityPoint], [mLevelHP], [mLevelMP], [mLevelWP], [mWeightHP], [mWeightMP], [mWeightWP]) VALUES ('에픽', 2, 0, 5.0, 2.0, 15.0, 0.001, 0.00025, 0.01);
GO

INSERT INTO [dbo].[TP_AchieveRarityPoint] ([mName], [mRarityID], [mRarityPoint], [mLevelHP], [mLevelMP], [mLevelWP], [mWeightHP], [mWeightMP], [mWeightWP]) VALUES ('레어', 3, 0, 3.0, 1.5, 10.0, 0.001, 0.00025, 0.01);
GO

INSERT INTO [dbo].[TP_AchieveRarityPoint] ([mName], [mRarityID], [mRarityPoint], [mLevelHP], [mLevelMP], [mLevelWP], [mWeightHP], [mWeightMP], [mWeightWP]) VALUES ('노멀', 4, 0, 1.0, 1.0, 5.0, 0.001, 0.00025, 0.01);
GO

