/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblSpotAttackerSector
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (14, 380359.0, 268025.0, 394759.0, 286686.0, '?? ?????');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (18, 325161.0, 223046.0, 347276.0, 248063.0, '???');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (20, 291370.0, 286302.0, 317891.0, 344610.0, '?? ???');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (21, 271662.0, 223305.0, 305061.0, 251396.0, '?? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (24, 947614.0, 66107.0, 973425.0, 91716.0, '??? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (33, 947507.0, 693471.0, 973451.0, 722457.0, '??? ?');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (42, 250116.0, 252318.0, 267909.0, 333500.0, '??? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (43, 102063.0, 162431.0, 136148.0, 201918.0, '??? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (46, 149702.0, 295781.0, 185805.0, 337214.0, '?? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (49, 165998.0, 224486.0, 201420.0, 264201.0, '??? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (50, 179236.0, 131808.0, 217279.0, 178638.0, '??? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (52, 1010625.0, 63780.0, 1039687.0, 92340.0, '??? ?');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (77, 220341.0, 100774.0, 255729.0, 142640.0, '???? ???');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (78, 373202.0, 174613.0, 408565.0, 208881.0, '??? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (79, 446577.0, 109704.0, 485097.0, 160242.0, '????');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (81, 310131.0, 63685.0, 355131.0, 123827.0, '???? ??');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (85, 1072780.0, 693657.0, 1102421.0, 723667.0, '???? ???');
GO

INSERT INTO [dbo].[TblSpotAttackerSector] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (90, 1134831.0, 64108.0, 1165272.0, 92789.0, '???? ????');
GO

