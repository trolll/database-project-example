/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblArenaBossGradeProb
Date                  : 2023-10-07 09:09:31
*/


INSERT INTO [dbo].[TblArenaBossGradeProb] ([mPointGrade], [mMinPoint], [mMaxPoint], [mGrade1st], [mGrade2nd], [mGrade3rd], [mGrade4th]) VALUES (1, 1, 99, 25.0, 25.0, 25.0, 25.0);
GO

INSERT INTO [dbo].[TblArenaBossGradeProb] ([mPointGrade], [mMinPoint], [mMaxPoint], [mGrade1st], [mGrade2nd], [mGrade3rd], [mGrade4th]) VALUES (2, 100, 249, 25.0, 25.0, 25.0, 25.0);
GO

INSERT INTO [dbo].[TblArenaBossGradeProb] ([mPointGrade], [mMinPoint], [mMaxPoint], [mGrade1st], [mGrade2nd], [mGrade3rd], [mGrade4th]) VALUES (3, 250, 399, 25.0, 25.0, 25.0, 25.0);
GO

INSERT INTO [dbo].[TblArenaBossGradeProb] ([mPointGrade], [mMinPoint], [mMaxPoint], [mGrade1st], [mGrade2nd], [mGrade3rd], [mGrade4th]) VALUES (4, 400, 1000, 25.0, 25.0, 25.0, 25.0);
GO

