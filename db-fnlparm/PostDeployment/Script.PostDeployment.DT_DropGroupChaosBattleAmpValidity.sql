/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_DropGroupChaosBattleAmpValidity
Date                  : 2023-10-07 09:08:51
*/


INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (208, 1, 0);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (314, 1, 0);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (314, 2, 0);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (314, 3, 0);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (314, 4, 0);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (314, 5, 0);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (512, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (512, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (512, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (512, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (512, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (513, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (513, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (513, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (513, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (513, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (514, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (514, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (514, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (514, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (514, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (515, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (515, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (515, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (515, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (515, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (516, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (516, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (516, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (516, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (516, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (517, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (517, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (517, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (517, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (517, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (518, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (518, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (518, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (518, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (518, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (519, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (519, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (519, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (519, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (519, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (520, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (520, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (520, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (520, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (520, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (521, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (521, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (521, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (521, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (521, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (522, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (522, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (522, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (522, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (522, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (523, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (523, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (523, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (523, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (523, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (524, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (524, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (524, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (524, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (524, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (525, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (525, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (525, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (525, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (525, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (526, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (526, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (526, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (526, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (526, 5, 22);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (527, 1, 2);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (527, 2, 4);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (527, 3, 11);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (527, 4, 17);
GO

INSERT INTO [dbo].[DT_DropGroupChaosBattleAmpValidity] ([mDGroup], [mChaosBattleAdvLv], [mAmpValidityValue]) VALUES (527, 5, 22);
GO

