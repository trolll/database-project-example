/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMaterialItemInfo
Date                  : 2023-10-07 09:08:51
*/


INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5249, 4, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5250, 3, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5251, 5, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5252, 1, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5253, 2, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5566, 4, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5567, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5568, 3, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5569, 5, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5570, 2, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5572, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (5573, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6664, 6, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6665, 6, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6666, 6, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6667, 6, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6712, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6713, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6714, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6715, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6716, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (6717, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40000, 5, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40001, 5, 1, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40002, 5, 1, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40003, 5, 1, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40004, 5, 1, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40005, 5, 1, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40006, 5, 1, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40007, 5, 1, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40008, 5, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40009, 5, 1, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40010, 5, 1, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40011, 5, 1, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40012, 5, 1, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40013, 5, 1, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40014, 5, 1, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40015, 5, 1, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40016, 5, 1, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40017, 5, 1, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40018, 5, 1, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40019, 5, 1, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40020, 5, 1, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40021, 5, 1, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40022, 5, 1, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40023, 5, 1, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40024, 5, 1, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40025, 5, 1, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40026, 5, 1, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40027, 5, 1, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40028, 5, 1, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40029, 5, 1, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40030, 5, 2, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40031, 5, 2, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40032, 5, 2, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40033, 5, 2, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40034, 5, 2, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40035, 5, 2, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40036, 5, 2, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40037, 5, 2, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40038, 5, 2, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40039, 5, 2, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40040, 5, 2, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40041, 5, 2, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40042, 5, 2, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40043, 5, 2, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40044, 5, 2, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40045, 5, 2, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40046, 5, 2, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40047, 5, 2, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40048, 5, 2, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40049, 5, 2, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40050, 5, 2, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40051, 5, 2, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40052, 5, 2, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40053, 5, 2, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40054, 5, 2, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40055, 5, 2, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40056, 5, 2, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40057, 5, 2, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40058, 5, 2, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40059, 5, 2, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40060, 5, 3, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40061, 5, 3, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40062, 5, 3, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40063, 5, 3, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40064, 5, 3, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40065, 5, 3, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40066, 5, 3, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40067, 5, 3, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40068, 5, 3, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40069, 5, 3, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40070, 5, 3, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40071, 5, 3, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40072, 5, 3, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40073, 5, 3, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40074, 5, 3, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40075, 5, 3, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40076, 5, 3, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40077, 5, 3, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40078, 5, 3, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40079, 5, 3, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40080, 5, 3, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40081, 5, 3, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40082, 5, 3, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40083, 5, 3, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40084, 5, 3, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40085, 5, 3, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40086, 5, 3, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40087, 5, 3, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40088, 5, 3, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40089, 5, 3, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40090, 5, 4, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40091, 5, 4, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40092, 5, 4, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40093, 5, 4, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40094, 5, 4, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40095, 5, 4, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40096, 5, 4, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40097, 5, 4, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40098, 5, 4, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40099, 5, 4, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40100, 5, 4, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40101, 5, 4, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40102, 5, 4, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40103, 5, 4, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40104, 5, 4, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40105, 5, 4, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40106, 5, 4, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40107, 5, 4, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40108, 5, 4, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40109, 5, 4, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40110, 5, 4, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40111, 5, 4, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40112, 5, 4, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40113, 5, 4, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40114, 5, 4, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40115, 5, 4, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40116, 5, 4, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40117, 5, 4, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40118, 5, 4, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40119, 5, 4, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40120, 2, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40121, 2, 1, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40122, 2, 1, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40123, 2, 1, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40124, 2, 1, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40125, 2, 1, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40126, 2, 1, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40127, 2, 1, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40128, 2, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40129, 2, 1, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40130, 2, 1, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40131, 2, 1, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40132, 2, 1, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40133, 2, 1, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40134, 2, 1, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40135, 2, 1, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40136, 2, 1, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40137, 2, 1, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40138, 2, 1, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40139, 2, 1, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40140, 2, 1, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40141, 2, 1, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40142, 2, 1, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40143, 2, 1, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40144, 2, 1, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40145, 2, 1, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40146, 2, 1, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40147, 2, 1, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40148, 2, 1, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40149, 2, 1, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40150, 2, 2, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40151, 2, 2, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40152, 2, 2, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40153, 2, 2, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40154, 2, 2, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40155, 2, 2, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40156, 2, 2, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40157, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40158, 2, 2, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40159, 2, 2, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40160, 2, 2, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40161, 2, 2, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40162, 2, 2, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40163, 2, 2, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40164, 2, 2, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40165, 2, 2, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40166, 2, 2, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40167, 2, 2, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40168, 2, 2, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40169, 2, 2, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40170, 2, 2, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40171, 2, 2, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40172, 2, 2, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40173, 2, 2, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40174, 2, 2, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40175, 2, 2, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40176, 2, 2, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40177, 2, 2, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40178, 2, 2, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40179, 2, 2, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40180, 2, 3, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40181, 2, 3, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40182, 2, 3, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40183, 2, 3, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40184, 2, 3, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40185, 2, 3, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40186, 2, 3, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40187, 2, 3, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40188, 2, 3, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40189, 2, 3, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40190, 2, 3, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40191, 2, 3, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40192, 2, 3, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40193, 2, 3, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40194, 2, 3, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40195, 2, 3, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40196, 2, 3, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40197, 2, 3, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40198, 2, 3, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40199, 2, 3, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40200, 2, 3, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40201, 2, 3, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40202, 2, 3, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40203, 2, 3, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40204, 2, 3, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40205, 2, 3, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40206, 2, 3, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40207, 2, 3, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40208, 2, 3, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40209, 2, 3, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40210, 2, 4, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40211, 2, 4, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40212, 2, 4, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40213, 2, 4, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40214, 2, 4, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40215, 2, 4, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40216, 2, 4, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40217, 2, 4, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40218, 2, 4, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40219, 2, 4, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40220, 2, 4, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40221, 2, 4, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40222, 2, 4, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40223, 2, 4, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40224, 2, 4, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40225, 2, 4, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40226, 2, 4, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40227, 2, 4, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40228, 2, 4, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40229, 2, 4, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40230, 2, 4, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40231, 2, 4, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40232, 2, 4, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40233, 2, 4, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40234, 2, 4, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40235, 2, 4, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40236, 2, 4, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40237, 2, 4, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40238, 2, 4, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40239, 2, 4, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40240, 4, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40241, 4, 1, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40242, 4, 1, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40243, 4, 1, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40244, 4, 1, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40245, 4, 1, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40246, 4, 1, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40247, 4, 1, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40248, 4, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40249, 4, 1, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40250, 4, 1, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40251, 4, 1, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40252, 4, 1, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40253, 4, 1, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40254, 4, 1, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40255, 4, 1, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40256, 4, 1, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40257, 4, 1, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40258, 4, 1, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40259, 4, 1, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40260, 4, 1, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40261, 4, 1, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40262, 4, 1, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40263, 4, 1, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40264, 4, 1, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40265, 4, 1, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40266, 4, 1, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40267, 4, 1, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40268, 4, 1, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40269, 4, 1, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40270, 4, 2, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40271, 4, 2, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40272, 4, 2, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40273, 4, 2, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40274, 4, 2, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40275, 4, 2, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40276, 4, 2, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40277, 4, 2, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40278, 4, 2, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40279, 4, 2, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40280, 4, 2, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40281, 4, 2, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40282, 4, 2, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40283, 4, 2, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40284, 4, 2, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40285, 4, 2, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40286, 4, 2, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40287, 4, 2, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40288, 4, 2, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40289, 4, 2, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40290, 4, 2, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40291, 4, 2, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40292, 4, 2, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40293, 4, 2, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40294, 4, 2, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40295, 4, 2, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40296, 4, 2, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40297, 4, 2, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40298, 4, 2, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40299, 4, 2, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40300, 4, 3, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40301, 4, 3, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40302, 4, 3, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40303, 4, 3, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40304, 4, 3, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40305, 4, 3, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40306, 4, 3, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40307, 4, 3, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40308, 4, 3, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40309, 4, 3, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40310, 4, 3, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40311, 4, 3, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40312, 4, 3, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40313, 4, 3, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40314, 4, 3, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40315, 4, 3, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40316, 4, 3, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40317, 4, 3, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40318, 4, 3, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40319, 4, 3, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40320, 4, 3, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40321, 4, 3, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40322, 4, 3, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40323, 4, 3, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40324, 4, 3, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40325, 4, 3, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40326, 4, 3, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40327, 4, 3, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40328, 4, 3, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40329, 4, 3, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40330, 4, 4, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40331, 4, 4, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40332, 4, 4, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40333, 4, 4, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40334, 4, 4, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40335, 4, 4, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40336, 4, 4, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40337, 4, 4, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40338, 4, 4, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40339, 4, 4, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40340, 4, 4, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40341, 4, 4, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40342, 4, 4, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40343, 4, 4, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40344, 4, 4, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40345, 4, 4, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40346, 4, 4, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40347, 4, 4, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40348, 4, 4, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40349, 4, 4, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40350, 4, 4, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40351, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40352, 4, 4, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40353, 4, 4, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40354, 4, 4, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40355, 4, 4, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40356, 4, 4, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40357, 4, 4, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40358, 4, 4, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40359, 4, 4, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40360, 3, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40361, 3, 1, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40362, 3, 1, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40363, 3, 1, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40364, 3, 1, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40365, 3, 1, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40366, 3, 1, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40367, 3, 1, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40368, 3, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40369, 3, 1, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40370, 3, 1, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40371, 3, 1, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40372, 3, 1, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40373, 3, 1, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40374, 3, 1, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40375, 3, 1, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40376, 3, 1, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40377, 3, 1, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40378, 3, 1, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40379, 3, 1, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40380, 3, 1, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40381, 3, 1, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40382, 3, 1, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40383, 3, 1, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40384, 3, 1, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40385, 3, 1, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40386, 3, 1, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40387, 3, 1, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40388, 3, 1, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40389, 3, 1, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40390, 3, 2, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40391, 3, 2, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40392, 3, 2, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40393, 3, 2, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40394, 3, 2, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40395, 3, 2, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40396, 3, 2, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40397, 3, 2, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40398, 3, 2, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40399, 3, 2, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40400, 3, 2, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40401, 3, 2, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40402, 3, 2, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40403, 3, 2, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40404, 3, 2, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40405, 3, 2, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40406, 3, 2, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40407, 3, 2, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40408, 3, 2, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40409, 3, 2, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40410, 3, 2, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40411, 3, 2, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40412, 3, 2, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40413, 3, 2, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40414, 3, 2, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40415, 3, 2, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40416, 3, 2, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40417, 3, 2, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40418, 3, 2, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40419, 3, 2, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40420, 3, 3, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40421, 3, 3, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40422, 3, 3, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40423, 3, 3, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40424, 3, 3, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40425, 3, 3, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40426, 3, 3, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40427, 3, 3, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40428, 3, 3, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40429, 3, 3, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40430, 3, 3, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40431, 3, 3, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40432, 3, 3, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40433, 3, 3, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40434, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40435, 3, 3, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40436, 3, 3, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40437, 3, 3, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40438, 3, 3, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40439, 3, 3, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40440, 3, 3, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40441, 3, 3, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40442, 3, 3, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40443, 3, 3, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40444, 3, 3, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40445, 3, 3, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40446, 3, 3, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40447, 3, 3, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40448, 3, 3, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40449, 3, 3, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40450, 3, 4, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40451, 3, 4, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40452, 3, 4, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40453, 3, 4, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40454, 3, 4, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40455, 3, 4, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40456, 3, 4, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40457, 3, 4, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40458, 3, 4, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40459, 3, 4, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40460, 3, 4, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40461, 3, 4, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40462, 3, 4, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40463, 3, 4, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40464, 3, 4, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40465, 3, 4, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40466, 3, 4, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40467, 3, 4, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40468, 3, 4, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40469, 3, 4, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40470, 3, 4, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40471, 3, 4, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40472, 3, 4, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40473, 3, 4, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40474, 3, 4, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40475, 3, 4, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40476, 3, 4, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40477, 3, 4, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40478, 3, 4, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40479, 3, 4, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40480, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40481, 1, 1, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40482, 1, 1, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40483, 1, 1, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40484, 1, 1, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40485, 1, 1, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40486, 1, 1, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40487, 1, 1, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40488, 1, 1, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40489, 1, 1, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40490, 1, 1, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40491, 1, 1, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40492, 1, 1, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40493, 1, 1, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40494, 1, 1, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40495, 1, 1, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40496, 1, 1, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40497, 1, 1, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40498, 1, 1, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40499, 1, 1, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40500, 1, 1, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40501, 1, 1, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40502, 1, 1, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40503, 1, 1, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40504, 1, 1, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40505, 1, 1, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40506, 1, 1, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40507, 1, 1, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40508, 1, 1, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40509, 1, 1, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40510, 1, 2, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40511, 1, 2, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40512, 1, 2, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40513, 1, 2, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40514, 1, 2, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40515, 1, 2, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40516, 1, 2, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40517, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40518, 1, 2, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40519, 1, 2, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40520, 1, 2, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40521, 1, 2, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40522, 1, 2, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40523, 1, 2, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40524, 1, 2, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40525, 1, 2, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40526, 1, 2, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40527, 1, 2, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40528, 1, 2, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40529, 1, 2, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40530, 1, 2, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40531, 1, 2, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40532, 1, 2, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40533, 1, 2, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40534, 1, 2, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40535, 1, 2, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40536, 1, 2, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40537, 1, 2, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40538, 1, 2, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40539, 1, 2, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40540, 1, 3, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40541, 1, 3, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40542, 1, 3, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40543, 1, 3, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40544, 1, 3, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40545, 1, 3, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40546, 1, 3, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40547, 1, 3, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40548, 1, 3, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40549, 1, 3, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40550, 1, 3, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40551, 1, 3, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40552, 1, 3, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40553, 1, 3, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40554, 1, 3, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40555, 1, 3, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40556, 1, 3, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40557, 1, 3, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40558, 1, 3, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40559, 1, 3, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40560, 1, 3, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40561, 1, 3, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40562, 1, 3, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40563, 1, 3, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40564, 1, 3, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40565, 1, 3, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40566, 1, 3, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40567, 1, 3, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40568, 1, 3, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40569, 1, 3, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40570, 1, 4, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40571, 1, 4, 1, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40572, 1, 4, 1, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40573, 1, 4, 1, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40574, 1, 4, 1, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40575, 1, 4, 1, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40576, 1, 4, 2, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40577, 1, 4, 2, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40578, 1, 4, 2, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40579, 1, 4, 2, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40580, 1, 4, 2, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40581, 1, 4, 2, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40582, 1, 4, 3, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40583, 1, 4, 3, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40584, 1, 4, 3, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40585, 1, 4, 3, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40586, 1, 4, 3, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40587, 1, 4, 3, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40588, 1, 4, 4, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40589, 1, 4, 4, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40590, 1, 4, 4, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40591, 1, 4, 4, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40592, 1, 4, 4, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40593, 1, 4, 4, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40594, 1, 4, 5, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40595, 1, 4, 5, 2);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40596, 1, 4, 5, 3);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40597, 1, 4, 5, 4);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40598, 1, 4, 5, 5);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40599, 1, 4, 5, 6);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40600, 6, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40601, 6, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40602, 6, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40603, 7, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40604, 7, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40605, 7, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40606, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40607, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40608, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40609, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40610, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40611, 5, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40612, 2, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40613, 4, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40614, 3, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40615, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40616, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40617, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40618, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40619, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40620, 8, 1, 1, 1);
GO

INSERT INTO [dbo].[TblMaterialItemInfo] ([IID], [MType], [MGrade], [MLevel], [MEnchant]) VALUES (40621, 8, 1, 1, 1);
GO

