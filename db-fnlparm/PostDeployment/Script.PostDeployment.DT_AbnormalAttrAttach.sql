/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AbnormalAttrAttach
Date                  : 2023-10-07 09:07:22
*/


INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (2, 87, 507, 0, 0, '투명 상태이상(87)이 끝날 때 공격불가(507)가 걸린다');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (3, 508, 512, 0, 0, '빛나는 투명 상태이상(508)이 끝날 때 공격불가(512)걸림.');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (4, 509, 513, 0, 0, '찬란한 투명 상태이상(509)이 끝날 때 공격불가(513)가 걸린다.');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (5, 511, 514, 0, 0, '강렬한 투명 상태이상(511)이 끝날 때 공격불가(514)가 걸린다.');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (6, 2045, 2041, 0, 0, '공격 스킬 시전(메테오 - 스운)(2045) 끝날때 대기 상태이상(2041)으로 변경');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (7, 2047, 2042, 0, 0, '공격 스킬 시전(파이어 - 사일런스)(2047) 끝날때 대기 상태이상(2042)으로 변경');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (8, 2050, 2044, 0, 0, '공격 스킬 시전(메테오 - 스운)(2050) 끝날때 대기 상태이상(2044)으로 변경');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (9, 2025, 2033, 0, 0, '시전 모션(메테오 - 사일런스)(2025) 끝날때 성공 모션(메테오 - 사일런스)(2033)으로 변경');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (10, 2026, 2034, 0, 0, '시전 모션(파이어 - 스운)(2026) 끝날때 성공 모션(파이어 - 스운)(2034)으로 변경');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (11, 2027, 2035, 0, 0, '시전 모션(발구르기)(2027) 끝날때 성공 모션(발구르기)(2035)으로 변경');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (12, 2028, 2036, 0, 0, '시전 모션(골렘 소환 - 아머브레이크)(2028) 끝날때 성공 모션(골렘 소환 - 아머브레이크)(2036)으로 변경');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (13, 2440, 1828, 0, 0, '끈적한 트랩 강화, 끈적임(2440)이 끝난 후 이속감소(1828) 적용');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (14, 2809, 507, 0, 0, '투명1렙(2809)가 끝나면 공격불가(507)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (15, 2810, 512, 0, 0, '투명2렙(2810)가 끝나면 공격불가(512)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (16, 2811, 513, 0, 0, '투명3렙(2811)가 끝나면 공격불가(513)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (17, 2812, 514, 0, 0, '투명4렙(2812)가 끝나면 공격불가(514)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (20, 2995, 2996, 0, 0, '준비시간 종료 후 실행(블랙드래곤-브레스)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (21, 2999, 3000, 0, 0, '준비시간 종료 후 실행(메테오스-어스퀘이크)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (22, 3005, 3006, 0, 0, '준비시간 종료 후 실행(메테오스-블리자드)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (23, 3008, 3009, 0, 0, '준비시간 종료 후 실행(메테오스-토네이도)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (24, 3012, 3013, 0, 0, '준비시간 종료 후 실행(메테오스-메테오)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (25, 3027, 3028, 0, 0, '상승 종료 후 이동 실행(메테오스-낙하공격)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (26, 3031, 3032, 0, 0, '대기 후 하강 준비(메테오스-낙하공격)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (27, 3033, 3034, 0, 0, '하강 준비 후 하강 실행(메테오스-낙하공격)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (28, 3036, 3037, 0, 0, '준비시간 종료 후 실행(메테오스-지상브레스)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (29, 3039, 3040, 0, 0, '준비시간 종료 후 실행(메테오스-공중브레스)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrAttach] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAType], [mDesc]) VALUES (30, 3679, 3678, 0, 0, '아레나에서 매테오스가 플라이 상태이상 종료 시 낙하공격 실행');
GO

