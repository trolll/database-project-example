/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblTransformAbilityByServant
Date                  : 2023-10-07 09:08:53
*/


INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (0, 2, 1, 1200, 600, 5550, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (0, 3, 1, 1250, 630, 6000, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (0, 4, 1, 1300, 660, 6450, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (1, 2, 1, 1000, 630, 5550, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (1, 3, 1, 1150, 660, 6000, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (1, 4, 1, 1200, 690, 6450, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (2, 2, 1, 1000, 600, 5750, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (2, 3, 1, 1150, 630, 6150, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (2, 4, 1, 1200, 660, 6600, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (3, 2, 1, 1050, 620, 5550, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (3, 3, 1, 1200, 640, 6000, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (3, 4, 1, 1250, 660, 6450, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (4, 2, 1, 1050, 600, 5650, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (4, 3, 1, 1200, 630, 6050, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (4, 4, 1, 1250, 660, 6550, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (5, 2, 1, 1000, 620, 5650, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (5, 3, 1, 1150, 640, 6050, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (5, 4, 1, 1200, 660, 6550, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (6, 2, 1, 1350, 630, 5650, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (6, 3, 1, 1400, 660, 6100, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (6, 4, 1, 1450, 690, 6550, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (7, 2, 1, 1050, 660, 5650, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (7, 3, 1, 1200, 690, 6100, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (7, 4, 1, 1350, 720, 6550, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (8, 2, 1, 1050, 630, 5850, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (8, 3, 1, 1200, 660, 6250, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (8, 4, 1, 1350, 690, 6700, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (9, 2, 1, 1150, 650, 5650, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (9, 3, 1, 1350, 680, 6100, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (9, 4, 1, 1400, 700, 6550, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (10, 2, 1, 1150, 630, 5750, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (10, 3, 1, 1350, 660, 6150, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (10, 4, 1, 1400, 690, 6600, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (11, 2, 1, 1050, 650, 5750, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (11, 3, 1, 1200, 680, 6150, 650, 950, 415);
GO

INSERT INTO [dbo].[TblTransformAbilityByServant] ([mServantTransformType], [mType], [mLevel], [mMaxHP], [mMaxMP], [mMaxWeight], [mShortAttackRate], [mLongAttackRate], [mMoveRate]) VALUES (11, 4, 1, 1350, 700, 6600, 650, 950, 415);
GO

