/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AbnormalAttrDetach
Date                  : 2023-10-07 09:07:26
*/


INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1, 0, 107, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2, 0, 207, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (3, 0, 173, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (4, 0, 485, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (5, 0, 309, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (6, 0, 318, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (7, 0, 317, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (8, 0, 319, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (9, 0, 107, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (10, 0, 207, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (11, 0, 173, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (12, 0, 485, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (13, 0, 309, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (14, 0, 318, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (15, 0, 317, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (16, 0, 319, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (17, 0, 107, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (18, 0, 207, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (19, 0, 173, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (20, 0, 485, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (21, 0, 309, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (22, 0, 318, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (23, 0, 317, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (24, 0, 319, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (25, 0, 107, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (26, 0, 207, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (27, 0, 173, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (28, 0, 485, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (29, 0, 309, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (30, 0, 318, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (31, 0, 317, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (32, 0, 319, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (33, 0, 107, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (34, 0, 207, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (35, 0, 173, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (36, 0, 485, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (37, 0, 309, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (38, 0, 318, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (39, 0, 317, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (40, 0, 319, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (41, 0, 107, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (42, 0, 207, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (43, 0, 173, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (44, 0, 485, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (45, 0, 309, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (46, 0, 318, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (47, 0, 317, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (48, 0, 319, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (49, 0, 107, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (50, 0, 207, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (51, 0, 173, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (52, 0, 485, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (53, 0, 309, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (54, 0, 318, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (55, 0, 317, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (56, 0, 319, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (57, 0, 107, 119, 0, 0, '드라코 탑승(107) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (58, 0, 207, 119, 0, 0, '드라코 탑승(207) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (59, 0, 173, 119, 0, 0, '드라코 탑승(173) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (60, 0, 485, 119, 0, 0, '드라코 탑승(485) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (61, 0, 309, 119, 0, 0, '드라코 탑승(309) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (62, 0, 318, 119, 0, 0, '드라코 탑승(318) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (63, 0, 317, 119, 0, 0, '드라코 탑승(317) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (64, 0, 319, 119, 0, 0, '드라코 탑승(319) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (65, 0, 21, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (66, 0, 22, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (67, 0, 23, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (68, 0, 24, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (69, 0, 229, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (70, 0, 25, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (71, 0, 26, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (72, 0, 27, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (73, 0, 28, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (74, 0, 9, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (75, 0, 10, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (76, 0, 11, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (77, 0, 12, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (78, 0, 224, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (79, 0, 13, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (80, 0, 14, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (81, 0, 15, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (82, 0, 16, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (83, 0, 17, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (84, 0, 18, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (85, 0, 19, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (86, 0, 20, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (87, 0, 61, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (88, 0, 62, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (89, 0, 63, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (90, 0, 64, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (91, 0, 30, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (92, 0, 34, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (93, 0, 35, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (94, 0, 36, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (95, 0, 322, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (96, 0, 49, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (97, 0, 50, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (98, 0, 51, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (99, 0, 52, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (100, 0, 53, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (101, 0, 54, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (102, 0, 55, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (103, 0, 56, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (104, 0, 131, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (105, 0, 57, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (106, 0, 58, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107, 0, 59, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (108, 0, 60, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (109, 0, 210, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (110, 0, 212, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (111, 0, 213, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (112, 0, 271, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (113, 0, 284, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (114, 0, 274, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (115, 0, 275, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (116, 0, 287, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (117, 0, 302, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (118, 0, 214, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (119, 0, 215, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (120, 0, 211, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (121, 0, 258, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (122, 0, 248, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (123, 0, 103, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (124, 0, 205, 99, 2, 100, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (125, 0, 87, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (126, 0, 87, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (127, 0, 237, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (128, 0, 237, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (129, 0, 237, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (130, 1, 236, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (131, 0, 360, 119, 0, 0, '창룡의 목걸이가 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (132, 0, 361, 119, 0, 0, '문라이트 네메시스', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (133, 0, 362, 119, 0, 0, '스핀들 네메시스', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (134, 0, 363, 119, 0, 0, '캐노네이드 키틴', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (135, 0, 483, 119, 0, 0, '렉스 서큐버스', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (136, 0, 484, 119, 0, 0, '특수 변신 완드', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (137, 0, 308, 119, 0, 0, '한방의 비약으로 변신할 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (138, 0, 346, 119, 0, 0, '인퍼날이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (139, 0, 347, 119, 0, 0, '블러디 뱀파 변신(347)이 걸릴 때 레비테이션을 항상 없앤다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (140, 0, 259, 119, 0, 0, '룬으로 변신할 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (141, 0, 69, 119, 0, 0, '둔막이 걸릴 떄 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (142, 0, 188, 119, 0, 0, '토란이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (143, 0, 189, 119, 0, 0, '송편이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (144, 0, 87, 127, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (145, 0, 508, 127, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (146, 0, 509, 127, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (147, 0, 511, 127, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (148, 0, 500, 13, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (149, 0, 500, 12, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (150, 0, 500, 72, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (151, 0, 500, 93, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (152, 0, 500, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (153, 0, 500, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (154, 0, 500, 38, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (155, 0, 500, 119, 0, 0, '드라코 탑승(500) 이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (156, 0, 508, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (157, 0, 508, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (158, 0, 509, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (159, 0, 509, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (160, 0, 511, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (161, 0, 511, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (162, 0, 510, 94, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (163, 0, 510, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (164, 0, 505, 119, 0, 0, '몽드 빼빼로 변신과 레비테이션 중첩이 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (165, 0, 506, 119, 0, 0, '땅콩 빼빼로 변신과 레비테이션 중첩이 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (166, 0, 551, 119, 0, 0, '캡틴 라이칸 변신(551)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (167, 0, 552, 119, 0, 0, '티어스 윗치 변신(551)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (168, 0, 553, 119, 0, 0, '프린세스 변신(551)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (169, 0, 107, 125, 0, 0, '드라코 탑승(107)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (170, 0, 173, 125, 0, 0, '드라코 탑승(173)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (171, 0, 207, 125, 0, 0, '드라코 탑승(207)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (172, 0, 309, 125, 0, 0, '드라코 탑승(309)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (173, 0, 317, 125, 0, 0, '드라코 탑승(317)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (174, 0, 318, 125, 0, 0, '드라코 탑승(318)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (175, 0, 319, 125, 0, 0, '드라코 탑승(319)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (176, 0, 485, 125, 0, 0, '드라코 탑승(485)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (177, 0, 500, 125, 0, 0, '드라코 탑승(500)을 할 때 타임디스토션을끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (178, 0, 572, 119, 0, 0, '체인 인큐버스(572)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (179, 0, 573, 119, 0, 0, '인퍼날 (II)(573)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (180, 0, 574, 119, 0, 0, '인퍼날 (III)(574)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (181, 0, 575, 119, 0, 0, '인퍼날 (IV)(575)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (182, 0, 576, 119, 0, 0, '렉스 서큐버스 (II)(576)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (183, 0, 577, 119, 0, 0, '렉스 서큐버스 (III)(577)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (184, 0, 578, 119, 0, 0, '렉스 서큐버스 (IV)(578)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (185, 0, 579, 119, 0, 0, '블러디 뱀파이어 (II)(579)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (186, 0, 580, 119, 0, 0, '블러디 뱀파이어 (III)(580)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (187, 0, 581, 119, 0, 0, '블러디 뱀파이어 (IV)(581)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (188, 0, 582, 119, 0, 0, '캐노네이드 키틴 (II)(582)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (189, 0, 583, 119, 0, 0, '캐노네이드 키틴 (III)(583)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (190, 0, 584, 119, 0, 0, '캐노네이드 키틴 (IV)(584)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (191, 0, 585, 119, 0, 0, '문라이트 네메시스 (改)(585)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (192, 0, 586, 119, 0, 0, '문라이트 네메시스 (零式)(586)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (193, 0, 587, 119, 0, 0, '스핀들 네메시스 (改)(587)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (194, 0, 588, 119, 0, 0, '스핀들 네메시스 (零式)(588)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (195, 0, 589, 119, 0, 0, '티어스 윗치 (改)(589)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (196, 0, 590, 119, 0, 0, '캡틴 라이칸 (改)(590)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (197, 0, 591, 119, 0, 0, '프린세스 (改)(591)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (198, 0, 592, 119, 0, 0, '프린세스 (零式)(592)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (199, 0, 593, 119, 0, 0, '프린세스 (일반형)(593)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (200, 0, 602, 119, 0, 0, '카사 아바타 변신(602)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (201, 0, 603, 119, 0, 0, '실라페 아바타 변신(603)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (202, 0, 604, 119, 0, 0, '노움 아바타 변신(604)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (203, 0, 605, 119, 0, 0, '언딘 아바타 변신(605)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (204, 0, 614, 119, 0, 0, '사라만다 아바타 변신(614)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (205, 0, 615, 119, 0, 0, '실프 아바타 변신(615)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (206, 0, 616, 119, 0, 0, '노임 아바타 변신(616)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (207, 0, 617, 119, 0, 0, '실라페 아바타 변신(617)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (208, 0, 618, 119, 0, 0, '이그니스 아바타 변신(618)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (209, 0, 619, 119, 0, 0, '실라이론 아바타 변신(619)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (210, 0, 620, 119, 0, 0, '노이아넨 아바타 변신(620)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (211, 0, 621, 119, 0, 0, '운다임 아바타 변신(621)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (212, 0, 107, 155, 0, 0, '드라코 탑승(107)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (213, 0, 173, 155, 0, 0, '드라코 탑승(173)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (214, 0, 207, 155, 0, 0, '드라코 탑승(207)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (215, 0, 309, 155, 0, 0, '드라코 탑승(309)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (216, 0, 317, 155, 0, 0, '드라코 탑승(317)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (217, 0, 318, 155, 0, 0, '드라코 탑승(318)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (218, 0, 319, 155, 0, 0, '드라코 탑승(319)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (219, 0, 485, 155, 0, 0, '드라코 탑승(485)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (220, 0, 500, 155, 0, 0, '드라코 탑승(500)을 할 때 리플렉션 오오라를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (221, 0, 711, 119, 0, 0, '큐티 프리스트 변신(711)이 걸릴 때 레비테이션을 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (222, 0, 712, 119, 0, 0, '수련소녀 변신 (712)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (223, 0, 713, 119, 0, 0, '수련소녀 II 변신 (713)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (224, 0, 714, 119, 0, 0, '수련소녀 변신 III (714)이 걸릴 때 레비테이션을 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (225, 0, 715, 119, 0, 0, '수련소녀 변신 IV (715)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (226, 0, 716, 119, 0, 0, '큐티 프리스트 II 변신(716)이 걸릴 때 레비테이션을 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (227, 0, 717, 119, 0, 0, '큐티 프리스트 III 변신(717)이 걸릴 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (228, 0, 718, 119, 0, 0, '큐티 프리스트 IV 변신(718)이 걸릴 때 레비테이션을 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (229, 0, 725, 165, 0, 0, '텔레포트 봉인이 걸릴 때 텔레포트  봉인을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (230, 0, 107, 167, 0, 0, '드라코 탑승(107) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (231, 0, 207, 167, 0, 0, '드라코 탑승(207) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (232, 0, 173, 167, 0, 0, '드라코 탑승(173) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (233, 0, 485, 167, 0, 0, '드라코 탑승(485) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (234, 0, 309, 167, 0, 0, '드라코 탑승(309) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (235, 0, 318, 167, 0, 0, '드라코 탑승(318) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (236, 0, 317, 167, 0, 0, '드라코 탑승(317) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (237, 0, 319, 167, 0, 0, '드라코 탑승(319) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (238, 0, 500, 167, 0, 0, '드라코 탑승(500) 이 걸릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (239, 1, 360, 167, 0, 0, '창룡의 목걸이가 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (240, 1, 361, 167, 0, 0, '문라이트 네메시스이가 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (241, 1, 362, 167, 0, 0, '스핀들 네메시스이가 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (242, 1, 363, 167, 0, 0, '캐노네이드 키틴이가 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (243, 1, 483, 167, 0, 0, '렉스 서큐버스이가 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (244, 1, 484, 167, 0, 0, '특수 변신 완드이가 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (245, 1, 308, 167, 0, 0, '한방의 비약이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (246, 1, 346, 167, 0, 0, '인퍼날이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (247, 1, 347, 167, 0, 0, '블러디 뱀파 변신(347)이 풀릴 때 변신레비를 항상 없앤다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (248, 1, 259, 167, 0, 0, '룬이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (249, 1, 69, 167, 0, 0, '둔막이 풀릴 떄 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (250, 1, 188, 167, 0, 0, '토란이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (251, 1, 189, 167, 0, 0, '송편이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (252, 1, 505, 167, 0, 0, '몽드 빼빼로 변신과 변신레비 중첩이 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (253, 1, 506, 167, 0, 0, '땅콩 빼빼로 변신과 변신레비 중첩이 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (254, 1, 551, 167, 0, 0, '캡틴 라이칸 변신(551)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (255, 1, 552, 167, 0, 0, '티어스 윗치 변신(551)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (256, 1, 553, 167, 0, 0, '프린세스 변신(551)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (257, 1, 572, 167, 0, 0, '체인 인큐버스(572)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (258, 1, 573, 167, 0, 0, '인퍼날 (II)(573)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (259, 1, 574, 167, 0, 0, '인퍼날 (III)(574)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (260, 1, 575, 167, 0, 0, '인퍼날 (IV)(575)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (261, 1, 576, 167, 0, 0, '렉스 서큐버스 (II)(576)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (262, 1, 577, 167, 0, 0, '렉스 서큐버스 (III)(577)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (263, 1, 578, 167, 0, 0, '렉스 서큐버스 (IV)(578)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (264, 1, 579, 167, 0, 0, '블러디 뱀파이어 (II)(579)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (265, 1, 580, 167, 0, 0, '블러디 뱀파이어 (III)(580)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (266, 1, 581, 167, 0, 0, '블러디 뱀파이어 (IV)(581)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (267, 1, 582, 167, 0, 0, '캐노네이드 키틴 (II)(582)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (268, 1, 583, 167, 0, 0, '캐노네이드 키틴 (III)(583)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (269, 1, 584, 167, 0, 0, '캐노네이드 키틴 (IV)(584)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (270, 1, 585, 167, 0, 0, '문라이트 네메시스 (改)(585)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (271, 1, 586, 167, 0, 0, '문라이트 네메시스 (零式)(586)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (272, 1, 587, 167, 0, 0, '스핀들 네메시스 (改)(587)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (273, 1, 588, 167, 0, 0, '스핀들 네메시스 (零式)(588)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (274, 1, 589, 167, 0, 0, '티어스 윗치 (改)(589)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (275, 1, 590, 167, 0, 0, '캡틴 라이칸 (改)(590)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (276, 1, 591, 167, 0, 0, '프린세스 (改)(591)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (277, 1, 592, 167, 0, 0, '프린세스 (零式)(592)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (278, 1, 593, 167, 0, 0, '프린세스 (일반형)(593)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (279, 1, 602, 167, 0, 0, '카사 아바타 변신(602)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (280, 1, 603, 167, 0, 0, '실라페 아바타 변신(603)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (281, 1, 604, 167, 0, 0, '노움 아바타 변신(604)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (282, 1, 605, 167, 0, 0, '언딘 아바타 변신(605)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (283, 1, 614, 167, 0, 0, '사라만다 아바타 변신(614)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (284, 1, 615, 167, 0, 0, '실프 아바타 변신(615)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (285, 1, 616, 167, 0, 0, '노임 아바타 변신(616)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (286, 1, 617, 167, 0, 0, '실라페 아바타 변신(617)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (287, 1, 618, 167, 0, 0, '이그니스 아바타 변신(618)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (288, 1, 619, 167, 0, 0, '실라이론 아바타 변신(619)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (289, 1, 620, 167, 0, 0, '노이아넨 아바타 변신(620)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (290, 1, 621, 167, 0, 0, '운다임 아바타 변신(621)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (291, 1, 711, 167, 0, 0, '큐티 프리스트 변신(711)이 풀릴 때 변신레비를 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (292, 1, 712, 167, 0, 0, '수련소녀 변신 (712)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (293, 1, 713, 167, 0, 0, '수련소녀 II 변신 (713)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (294, 1, 714, 167, 0, 0, '수련소녀 변신 III (714)이 풀릴 때 변신레비를 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (295, 1, 715, 167, 0, 0, '수련소녀 변신 IV (715)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (296, 1, 716, 167, 0, 0, '큐티 프리스트 II 변신(716)이 풀릴 때 변신레비를 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (297, 1, 717, 167, 0, 0, '큐티 프리스트 III 변신(717)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (298, 1, 718, 167, 0, 0, '큐티 프리스트 IV 변신(718)이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (299, 0, 791, 119, 0, 0, '카사 변신(791)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (300, 0, 792, 119, 0, 0, '실라페 변신(792)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (301, 0, 793, 119, 0, 0, '노움 변신(793)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (302, 0, 794, 119, 0, 0, '언딘 변신(794)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (303, 0, 795, 119, 0, 0, '사라만다 변신(795)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (304, 0, 796, 119, 0, 0, '노임 변신(796)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (305, 0, 797, 119, 0, 0, '실프 변신(797)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (306, 0, 798, 119, 0, 0, '이그니스 변신(798)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (307, 0, 799, 119, 0, 0, '실라이론 변신(799)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (308, 0, 800, 119, 0, 0, '노이아넨 변신(800)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (309, 0, 801, 119, 0, 0, '운다임 변신(801)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (310, 0, 802, 119, 0, 0, '이프리트 변신(802)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (311, 0, 803, 119, 0, 0, '슈리엘 변신(803)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (312, 0, 804, 119, 0, 0, '노아스 변신(804)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (313, 0, 805, 119, 0, 0, '엘라임 변신(805)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (314, 0, 806, 119, 0, 0, '퓨리 변신(806)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (315, 0, 807, 119, 0, 0, '레프리컨 변신(807)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (316, 0, 808, 119, 0, 0, '시아페 변신(808)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (317, 0, 809, 119, 0, 0, '레이커 변신(809)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (318, 0, 810, 119, 0, 0, '휘카셀 변신(810)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (319, 0, 811, 119, 0, 0, '에리스 변신(811)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (320, 0, 812, 119, 0, 0, '릴리언스 변신(812)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (321, 0, 813, 119, 0, 0, '에리세드 변신(813)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (322, 0, 814, 119, 0, 0, '운디네 변신(814)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (328, 1, 791, 167, 0, 0, '카사 변신(791)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (329, 1, 792, 167, 0, 0, '실라페 변신(792)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (330, 1, 793, 167, 0, 0, '노움 변신(793)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (331, 1, 794, 167, 0, 0, '언딘 변신(794)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (332, 1, 795, 167, 0, 0, '사라만다 변신(795)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (333, 1, 796, 167, 0, 0, '노임 변신(796)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (334, 1, 797, 167, 0, 0, '실프 변신(797)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (336, 1, 798, 167, 0, 0, '이그니스 변신(798)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (337, 1, 799, 167, 0, 0, '실라이론 변신(799)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (338, 1, 800, 167, 0, 0, '노이아넨 변신(800)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (339, 1, 801, 167, 0, 0, '운다임 변신(801)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (340, 1, 802, 167, 0, 0, '이프리트 변신(802)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (341, 1, 803, 167, 0, 0, '슈리엘 변신(803)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (342, 1, 804, 167, 0, 0, '노아스 변신(804)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (343, 1, 805, 167, 0, 0, '엘라임 변신(805)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (344, 1, 806, 167, 0, 0, '퓨리 변신(806)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (345, 1, 807, 167, 0, 0, '레프리컨 변신(807)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (346, 1, 808, 167, 0, 0, '시아페 변신(808)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (347, 1, 809, 167, 0, 0, '레이커 변신(809)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (348, 1, 810, 167, 0, 0, '휘카셀 변신(810)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (349, 1, 811, 167, 0, 0, '에리스 변신(811)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (350, 1, 812, 167, 0, 0, '릴리언스 변신(812)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (351, 1, 813, 167, 0, 0, '에리세드 변신(813)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (352, 1, 814, 167, 0, 0, '운디네 변신(814)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (353, 0, 730, 119, 0, 0, '소라게 변신(730)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (354, 1, 730, 167, 0, 0, '소라게 변신(730)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (355, 0, 732, 119, 0, 0, '이프리트 변신(732)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (356, 0, 733, 119, 0, 0, '퓨리 변신(733))이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (357, 0, 734, 119, 0, 0, '휠카셀 변신(734)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (358, 0, 735, 119, 0, 0, '슈리엘 변신(735)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (359, 0, 736, 119, 0, 0, '레프리컨 변신(736)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (360, 0, 737, 119, 0, 0, '에리스 변신(737)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (361, 0, 738, 119, 0, 0, '노아스 변신(738)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (362, 0, 739, 119, 0, 0, '시아페 변신(739)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (363, 0, 740, 119, 0, 0, '릴리언스 변신(740)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (364, 0, 741, 119, 0, 0, '엘라임 변신(741)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (365, 0, 742, 119, 0, 0, '레이커 변신(742)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (366, 0, 743, 119, 0, 0, '에리세드 변신(743)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (367, 1, 732, 167, 0, 0, '이프리트 변신(732)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (368, 1, 733, 167, 0, 0, '퓨리 변신(733)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (369, 1, 734, 167, 0, 0, '휠카셀 변신(734)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (370, 1, 735, 167, 0, 0, '슈리엘 변신(735)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (371, 1, 736, 167, 0, 0, '레프리컨 변신(736)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (372, 1, 737, 167, 0, 0, '에리스 변신(737)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (373, 1, 738, 167, 0, 0, '노아스 변신(738)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (374, 1, 739, 167, 0, 0, '시아페변신(739)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (375, 1, 740, 167, 0, 0, '릴리언스 변신(740)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (376, 1, 741, 167, 0, 0, '엘라임 변신(741)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (377, 1, 742, 167, 0, 0, '레이커 변신(742)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (378, 1, 743, 167, 0, 0, '에리세드 변신(743)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (379, 0, 818, 119, 0, 0, '문엘프 전사(IV) (818)변신이 될 때 레이테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (380, 0, 819, 119, 0, 0, '문엘프 전사(III) (819)변신이 될 때 레이테이션을 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (381, 0, 820, 119, 0, 0, '문엘프 전사(II) (820)변신이 될 때 레이테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (382, 0, 821, 119, 0, 0, '문엘프 전사 (821)변신이 될 때 레이테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (383, 0, 822, 119, 0, 0, '문엘프 마법사(IV) (822)변신이 될 때 레이테이션을 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (384, 0, 823, 119, 0, 0, '문엘프 마법사(III) (822)변신이 될 때 레이테이션을 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (385, 0, 824, 119, 0, 0, '문엘프 마법사(II) (824)변신이 될 때 레이테이션을 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (386, 0, 825, 119, 0, 0, '문엘프 마법사 (825)변신이 될 때 레이테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (387, 0, 826, 119, 0, 0, '큐티 프리스트 (V) (826)변신이 될 때 레이테이션을 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (388, 0, 827, 119, 0, 0, '수련 소녀 (V) (827)변신이 될 때 레이테이션을 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (389, 0, 828, 119, 0, 0, '홀리 소서리스 (828)변신이 될 때 레이테이션을 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (390, 0, 829, 119, 0, 0, '바바리안 (829)변신이 될 때 레이테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (391, 0, 830, 119, 0, 0, '다크 메이지 (830)변신이 될 때 레이테이션을 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (392, 0, 831, 119, 0, 0, '문엘프 전사 (V) (831)변신이 될 때 레이테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (393, 0, 832, 119, 0, 0, '문엘프 마법사 (V) (832)변신이 될 때 레이테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (394, 1, 818, 167, 0, 0, '문엘프 전사(IV) (818)변신이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (395, 1, 819, 167, 0, 0, '문엘프 전사 (III) (819)변신이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (396, 1, 820, 167, 0, 0, '문엘프 전사(II) (820)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (397, 1, 821, 167, 0, 0, '문엘프 전사 (821)변신이 풀릴 때 변신레비를 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (398, 1, 822, 167, 0, 0, '문엘프 마법사(IV) (822)변신이 풀릴 때 변신레비를 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (399, 1, 823, 167, 0, 0, '문엘프 마법사(III)(823)변신이 풀릴 때 변신레비를 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (400, 1, 824, 167, 0, 0, '문엘프 마법사(II) (824)변신이 풀릴 때 변신레비를 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (401, 1, 825, 167, 0, 0, '문엘프 마법사 (825)변신이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (402, 1, 826, 167, 0, 0, '큐티 프리스트 (V) (826)변신이 풀릴 때 변신레비를 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (403, 1, 827, 167, 0, 0, '수련소녀 (V) (827)변신이 풀릴 때 변신레비를 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (404, 1, 828, 167, 0, 0, '홀리 소서리스 (828)변신이 풀릴 때 변신레비를 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (405, 1, 829, 167, 0, 0, '바바리안 (829)변신이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (406, 1, 830, 167, 0, 0, '다크 메이지 (830)변신이 풀릴 때 변신레비를 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (407, 1, 831, 167, 0, 0, '문엘프 전사 (V) (831)변신이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (408, 1, 832, 167, 0, 0, '문엘프 마법사 (V) (832)변신이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (409, 0, 834, 13, 0, 0, '834(프린스드라코탑승)일 때 이동속도업을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (410, 0, 834, 12, 0, 0, '834(프린스드라코탑승)일 때 공격속도업을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (411, 0, 834, 72, 0, 0, '834(프린스드라코탑승)일 때 버서크를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (412, 0, 834, 93, 0, 0, '834(프린스드라코탑승)일 때 스캠퍼를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (413, 0, 834, 94, 0, 0, '834(프린스드라코탑승)일 때 인비지빌리티를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (414, 0, 834, 98, 0, 0, '834(프린스드라코탑승)일 때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (415, 0, 834, 38, 0, 0, '834(프린스드라코탑승)일 때 투명상태를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (416, 0, 834, 119, 0, 0, '834(프린스드라코탑승)일 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (417, 0, 834, 125, 0, 0, '834(프린스드라코탑승)일 때 타임 디스토션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (418, 0, 834, 155, 0, 0, '834(프린스드라코탑승)일 때 리플렉션 오오라를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (419, 0, 834, 167, 0, 0, '834(프린스드라코탑승)일 때 변신용레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (420, 0, 851, 119, 0, 0, '진카사(851)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (421, 0, 852, 119, 0, 0, '진실라페(852)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (422, 0, 853, 119, 0, 0, '진노움(853)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (423, 0, 854, 119, 0, 0, '진언딘(854)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (424, 0, 855, 119, 0, 0, '진사라만다(855)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (425, 0, 856, 119, 0, 0, '진실프(856)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (426, 0, 857, 119, 0, 0, '진노임(857)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (427, 0, 858, 119, 0, 0, '진운디네(858)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (428, 1, 851, 167, 0, 0, '진카사(851)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (429, 1, 852, 167, 0, 0, '진실라페(852)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (430, 1, 853, 167, 0, 0, '진노움(853)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (431, 1, 854, 167, 0, 0, '진언딘(854)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (432, 1, 855, 167, 0, 0, '진사라만다(855)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (433, 1, 856, 167, 0, 0, '진실프(856)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (434, 1, 857, 167, 0, 0, '진노임(857)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (435, 1, 858, 167, 0, 0, '진운디네(858)변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (436, 0, 848, 119, 0, 0, '티어스 윗치Lv1(근거리) 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (437, 0, 849, 119, 0, 0, '티어스 윗치Lv2(근거리) 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (438, 0, 859, 119, 0, 0, '티어스 윗치Lv1(원거리) 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (439, 0, 860, 119, 0, 0, '티어스 윗치Lv2(원거리) 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (440, 1, 848, 167, 0, 0, '티어스 윗치Lv1(근거리) 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (441, 1, 849, 167, 0, 0, '티어스 윗치Lv2(근거리) 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (442, 1, 859, 167, 0, 0, '티어스 윗치Lv1(원거리) 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (443, 1, 860, 167, 0, 0, '티어스 윗치Lv2(원거리) 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (444, 0, 850, 119, 0, 0, '프린세스 (1일) (850)이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (445, 1, 850, 167, 0, 0, '프린세스 (1일) (850)이 풀릴 때 변신레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (446, 0, 912, 119, 0, 0, '사라만다(III)(912)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (447, 0, 913, 119, 0, 0, '실프(III)(913)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (448, 0, 914, 119, 0, 0, '노임(III)(914)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (449, 0, 915, 119, 0, 0, '운디네(III)(915)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (450, 0, 916, 119, 0, 0, '이그니스(III)(916)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (451, 0, 917, 119, 0, 0, '실라이론(III)(917)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (452, 0, 918, 119, 0, 0, '노이아넨(III)(918)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (453, 0, 919, 119, 0, 0, '운다임(III)(919)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (454, 1, 912, 167, 0, 0, '사라만다(III)(912)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (455, 1, 913, 167, 0, 0, '실프(III)(913)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (456, 1, 914, 167, 0, 0, '노임(III)(914)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (457, 1, 915, 167, 0, 0, '운디네(III)(915)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (458, 1, 916, 167, 0, 0, '이그니스(III)(916)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (459, 1, 917, 167, 0, 0, '실라이론(III)(917)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (460, 1, 918, 167, 0, 0, '노이아넨(III)(918)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (461, 1, 919, 167, 0, 0, '운다임(III)(919)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (462, 0, 922, 119, 0, 0, '카사(III)(922)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (463, 0, 923, 119, 0, 0, '실라페(III)(923)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (464, 0, 924, 119, 0, 0, '노움(III)(924)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (465, 1, 922, 167, 0, 0, '카사(III)(922)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (466, 1, 923, 167, 0, 0, '실라페(III)(923)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (467, 1, 924, 167, 0, 0, '노움(III)(924)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (468, 1, 925, 167, 0, 0, '언딘(III)(925)변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (469, 0, 925, 119, 0, 0, '언딘(III)(925)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (470, 0, 949, 119, 0, 0, '메테오스 쉐도우 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (471, 0, 950, 119, 0, 0, '메테오스 레인저 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (473, 0, 951, 119, 0, 0, '래서 아스타로트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (474, 0, 952, 119, 0, 0, '래서 루시퍼 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (475, 0, 953, 119, 0, 0, '그레이터 아스타로트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (476, 0, 954, 119, 0, 0, '그레이터 루시퍼 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (477, 0, 955, 119, 0, 0, '크루얼 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (478, 0, 956, 119, 0, 0, '님로드 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (479, 0, 957, 119, 0, 0, '앱솔루트 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (480, 0, 958, 119, 0, 0, '베이그란트 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (481, 0, 959, 119, 0, 0, '드래곤나이트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (482, 0, 960, 119, 0, 0, '드래곤나이트 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (483, 0, 961, 119, 0, 0, '발키리  변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (484, 0, 962, 119, 0, 0, '앨더스팅어  변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (485, 1, 949, 167, 0, 0, '메테오스 쉐도우 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (486, 1, 950, 167, 0, 0, '메테오스 레인저 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (487, 1, 951, 167, 0, 0, '래서 아스타로트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (488, 1, 952, 167, 0, 0, '래서 루시퍼 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (489, 1, 953, 167, 0, 0, '그레이터 아스타로트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (490, 1, 954, 167, 0, 0, '그레이터 루시퍼 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (491, 1, 955, 167, 0, 0, '크루얼 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (492, 1, 956, 167, 0, 0, '님로드 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (493, 1, 957, 167, 0, 0, '앱솔루트 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (494, 1, 958, 167, 0, 0, '베이그란트 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (495, 1, 959, 167, 0, 0, '드래곤나이트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (496, 1, 960, 167, 0, 0, '드래곤나이트 아처 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (497, 1, 961, 167, 0, 0, '발키리  변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (498, 1, 962, 167, 0, 0, '앨더스팅어  변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (499, 0, 963, 119, 0, 0, '바룬용병대 오크대장 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (500, 0, 964, 119, 0, 0, '데몬 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (501, 0, 965, 119, 0, 0, '파이어 데몬 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (502, 0, 966, 119, 0, 0, '플리드 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (503, 0, 967, 119, 0, 0, '다크 트레이서 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (504, 0, 968, 119, 0, 0, '오크 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (505, 0, 969, 119, 0, 0, '코볼트 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (506, 0, 970, 119, 0, 0, '용아병 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (507, 0, 971, 119, 0, 0, '뱀파이어 퀸 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (508, 0, 972, 119, 0, 0, '트로글다이트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (509, 0, 973, 119, 0, 0, '베어런 뱀파이어 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (510, 0, 974, 119, 0, 0, '가고일 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (511, 0, 975, 119, 0, 0, '바바야거 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (512, 0, 976, 119, 0, 0, '콜로복 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (513, 1, 963, 167, 0, 0, '바룬용병대 오크대장 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (514, 1, 964, 167, 0, 0, '데몬 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (515, 1, 965, 167, 0, 0, '파이어 데몬 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (516, 1, 966, 167, 0, 0, '플리드 아처 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (517, 1, 967, 167, 0, 0, '다크 트레이서 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (518, 1, 968, 167, 0, 0, '오크 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (519, 1, 969, 167, 0, 0, '코볼트 아처 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (520, 1, 970, 167, 0, 0, '용아병 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (521, 1, 971, 167, 0, 0, '뱀파이어 퀸 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (522, 1, 972, 167, 0, 0, '트로글다이트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (523, 1, 973, 167, 0, 0, '베어런 뱀파이어 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (524, 1, 974, 167, 0, 0, '가고일  변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (525, 1, 975, 167, 0, 0, '바바야거 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (526, 1, 976, 167, 0, 0, '콜로복 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (527, 0, 978, 119, 0, 0, '타이거 변신 (女)이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (528, 1, 978, 167, 0, 0, '타이거 변신 (女)이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (529, 0, 979, 119, 0, 0, '타이거 변신 (男)이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (530, 1, 979, 167, 0, 0, '타이거 변신 (男)이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (531, 0, 980, 119, 0, 0, '건방진 토끼 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (532, 1, 980, 167, 0, 0, '건방진 토끼 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (533, 0, 981, 119, 0, 0, '홍색 타이야끼 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (534, 1, 981, 167, 0, 0, '홍색 타이야끼 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (535, 0, 982, 119, 0, 0, '청색 타이야끼 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (536, 1, 982, 167, 0, 0, '홍색 타이야끼 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (537, 0, 999, 119, 0, 0, '뱅골 나이트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (538, 1, 999, 167, 0, 0, '뱅골 나이트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (539, 0, 1000, 119, 0, 0, '뱅골 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (540, 1, 1000, 167, 0, 0, '뱅골 아처 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (541, 0, 1030, 119, 0, 0, '길가메쉬 나이트로 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (542, 1, 1030, 167, 0, 0, '길가메쉬 나이트로 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (543, 0, 1031, 119, 0, 0, '길가메쉬 아처로 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (544, 1, 1031, 167, 0, 0, '길가메쉬 아처로 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (545, 0, 1032, 119, 0, 0, '다크 나이트로 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (546, 1, 1032, 167, 0, 0, '다크 나이트로 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (547, 0, 1033, 119, 0, 0, '다크 스팅어로 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (548, 1, 1033, 167, 0, 0, '다크 스팅어로 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (549, 0, 1046, 119, 0, 0, '불타는 축구공으로 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (550, 1, 1046, 167, 0, 0, '불타는 축구공으로 변신이 될 때 변신레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (551, 0, 1047, 119, 0, 0, '축축한 축구공으로 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (552, 1, 1047, 167, 0, 0, '축축한 축구공으로 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (553, 0, 1048, 119, 0, 0, '날렵한 축구공으로 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (554, 1, 1048, 167, 0, 0, '날렵한 축구공으로 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (555, 0, 1049, 119, 0, 0, '단단한 축구공으로 변신이 될 때 레비테이션 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (556, 1, 1049, 167, 0, 0, '단단한 축구공으로 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (557, 0, 1052, 119, 0, 0, '축구공 변신완드의 변신이 될 때 레비테이션 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (558, 1, 1052, 167, 0, 0, '축구공 변신완드의 변신이 될 때 변신레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (559, 0, 1051, 119, 0, 0, '능력강화 목걸이 변신이 될때 레비테이션 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (560, 1, 1051, 167, 0, 0, '능력강화 목걸이 변신이 될때 변신 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (561, 0, 1440, 119, 0, 0, '군(II) 변신이 될때 레비테이션 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (562, 1, 1440, 167, 0, 0, '군(II) 변신이 될때 변신용 레비테이션 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (563, 0, 1267, 101, 0, 0, '독 해제 1렙이 걸릴 때 만드라고라의 독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (564, 0, 1268, 102, 0, 0, '독 해제 2렙이 걸릴 때 타란튤라의 독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (566, 0, 1268, 101, 0, 0, '독 해제 2렙이 걸릴 때 만드라고라의 독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (567, 0, 1269, 87, 0, 0, '독 해제 3렙이 걸릴 때 맹독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (568, 0, 1269, 101, 0, 0, '독 해제 3렙이 걸릴 때 만드라고라의 독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (569, 0, 1269, 102, 0, 0, '독 해제 3렙이 걸릴 때 타란튤라의 독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (570, 0, 1270, 97, 0, 0, '독 해제 4렙이 걸릴 때 베놈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (571, 0, 1270, 87, 0, 0, '독 해제 4렙이 걸릴 때 맹독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (572, 0, 1270, 101, 0, 0, '독 해제 4렙이 걸릴 때 만드라고라의 독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (573, 0, 1270, 102, 0, 0, '독 해제 4렙이 걸릴 때 타란튤라의 독 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (582, 0, 1575, 13, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (583, 0, 1575, 12, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (584, 0, 1575, 72, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (585, 0, 1575, 93, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (586, 0, 1575, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (587, 0, 1575, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (588, 0, 1575, 38, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (589, 0, 1575, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (590, 0, 1576, 13, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (591, 0, 1576, 12, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (592, 0, 1576, 72, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (593, 0, 1576, 93, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (594, 0, 1576, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (595, 0, 1576, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (596, 0, 1576, 38, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (597, 0, 1576, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (598, 0, 1196, 203, 0, 0, '마나컨버전 공격 걸릴때 마나컨버전 HP 1레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (599, 0, 1197, 203, 0, 0, '마나컨버전 공격 걸릴때 마나컨버전 HP 2레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (600, 0, 1198, 203, 0, 0, '마나컨버전 공격 걸릴때 마나컨버전 HP 3레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (601, 0, 1204, 202, 0, 0, '마나컨버전 HP 걸릴때 마나컨버전 공격 1레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (602, 0, 1205, 202, 0, 0, '마나컨버전 HP 걸릴때 마나컨버전 공격 2레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (603, 0, 1206, 202, 0, 0, '마나컨버전 HP 걸릴때 마나컨버전 공격 3레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (604, 0, 1301, 125, 0, 0, '퓨인즈 걸릴 때 타임디스토션 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (605, 0, 523, 207, 0, 0, '타임디스토션 걸릴 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (606, 0, 107, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (607, 0, 207, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (608, 0, 173, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (610, 0, 309, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (611, 0, 318, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (612, 0, 317, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (613, 0, 319, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (614, 0, 485, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (615, 0, 500, 207, 0, 0, '드라코 탈 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (616, 1, 1900, 167, 0, 0, '해적 여자 바이킹 변신이 풀릴때 변신 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (617, 1, 1901, 167, 0, 0, '도적 총병 변신이 풀릴때 변신 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (618, 1, 1902, 167, 0, 0, '타락한 엘프 변신이 풀릴때 변신 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (619, 1, 1903, 167, 0, 0, '타락한 엘프 아처 변신이 풀릴때 변신 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (620, 1, 1904, 167, 0, 0, '골드 엠페러 변신이 풀릴때 변신 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (621, 1, 1905, 167, 0, 0, '골드 엠페러 아처 변신이 풀릴때 변신 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (622, 0, 1900, 119, 0, 0, '해적 여자 바이킹 변신이 될때 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (623, 0, 1901, 119, 0, 0, '도적 총병 변신이 될때 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (624, 0, 1902, 119, 0, 0, '타락한 엘프 변신이 될때 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (625, 0, 1903, 119, 0, 0, '타락한 엘프 아처 변신이 될때 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (626, 0, 1904, 119, 0, 0, '골드 엠페러 변신이 될때 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (627, 0, 1905, 119, 0, 0, '골드 엠페러 아처 변신이 될때 레비 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (628, 0, 1196, 202, 0, 0, '마나컨버전 HP 걸릴때 마나컨버전 HP 1레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (629, 0, 1197, 202, 0, 0, '마나컨버전 HP 걸릴때 마나컨버전 HP 2레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (630, 0, 1198, 202, 0, 0, '마나컨버전 HP 걸릴때 마나컨버전 HP 3레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (631, 0, 1204, 203, 0, 0, '마나컨버전 공격 걸릴때 마나컨버전 공격 1레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (632, 0, 1205, 203, 0, 0, '마나컨버전 공격 걸릴때 마나컨버전 공격 2레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (633, 0, 1206, 203, 0, 0, '마나컨버전 공격 걸릴때 마나컨버전 공격 3레벨 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (638, 0, 2011, 104, 0, 0, '면죄부 걸릴 때 살인자 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (639, 0, 2029, 247, 0, 0, '실패 모션(메테오 - 사일런스)(2029)이 걸릴때 공격 스킬 시전(247)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (640, 0, 2030, 247, 0, 0, '실패 모션(파이어 - 스운)(2030)이 걸릴때 공격 스킬 시전(247)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (641, 0, 2031, 247, 0, 0, '실패 모션(발구르기)(2031)이 걸릴때 공격 스킬 시전(247)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (642, 0, 2032, 247, 0, 0, '실패 모션(골렘 소환 - 아머브레이크)(2032)이 걸릴때 공격 스킬 시전(247)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (643, 0, 2018, 78, 0, 0, '골렘 소환 LV0(2018) 걸릴때 골렘 소환(78)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (644, 0, 2018, 243, 0, 0, '골렘 소환 LV0(2018) 걸릴때 골렘 소환(248)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (645, 0, 2033, 247, 0, 0, '성공 모션(메테오 - 사일런스)(2033)이 걸릴때 공격 스킬 시전(247)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (646, 0, 2034, 247, 0, 0, '성공 모션(파이어 - 스운)(2034)이 걸릴때 공격 스킬 시전(247)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (647, 0, 1971, 119, 0, 0, '알케미스트(1971)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (648, 1, 1971, 167, 0, 0, '알케미스트(1971)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (649, 0, 1972, 119, 0, 0, '닌자(1972)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (650, 1, 1972, 167, 0, 0, '닌자(1972)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (651, 0, 1973, 119, 0, 0, '바드(1973)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (652, 1, 1973, 167, 0, 0, '바드(1973)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (653, 0, 1974, 119, 0, 0, '나이트오브퀸(1974)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (654, 1, 1974, 167, 0, 0, '나이트오브퀸(1974)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (655, 0, 1975, 119, 0, 0, '차크람소녀(1975)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (656, 1, 1975, 167, 0, 0, '차크람소녀(1975)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (657, 0, 1976, 119, 0, 0, '여해적선장(1976)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (658, 1, 1976, 167, 0, 0, '여해적선장(1976)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (659, 0, 1977, 119, 0, 0, '뭉크(1977)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (660, 1, 1977, 167, 0, 0, '뭉크(1977)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (661, 0, 1978, 119, 0, 0, '아수라(1978)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (662, 1, 1978, 167, 0, 0, '아수라(1978)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (663, 0, 2062, 119, 0, 0, '닌자(2062)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (664, 1, 2062, 167, 0, 0, '닌자(2062)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (665, 0, 2063, 119, 0, 0, '파이어 워크스(2063)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (666, 1, 2063, 167, 0, 0, '파이어 워크스(2063)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (667, 0, 2065, 119, 0, 0, '아누비스(2065)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (668, 1, 2065, 167, 0, 0, '아누비스(2065)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (669, 0, 2066, 119, 0, 0, '아스모데우스(2066)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (670, 1, 2066, 167, 0, 0, '아스모데우스(2066)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (671, 0, 2067, 119, 0, 0, '차크람 소녀(2067)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (672, 1, 2067, 167, 0, 0, '차크람 소녀(2067)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (673, 0, 2068, 119, 0, 0, '바드(2068)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (674, 1, 2068, 167, 0, 0, '바드(2068)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (675, 0, 2064, 119, 0, 0, '알케미스트(2064)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (676, 1, 2064, 167, 0, 0, '알케미스트(2064)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (677, 0, 2069, 119, 0, 0, '아수라(2069)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (678, 1, 2069, 167, 0, 0, '아수라(2069)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (679, 0, 2070, 119, 0, 0, '무녀(2070)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (680, 1, 2070, 167, 0, 0, '무녀(2070)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (681, 0, 2071, 119, 0, 0, '아바돈(2071)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (682, 1, 2071, 167, 0, 0, '아바돈(2071)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (683, 0, 2072, 119, 0, 0, '비숍(2072)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (684, 1, 2072, 167, 0, 0, '비숍(2072)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (685, 0, 2073, 119, 0, 0, '몽크(2073)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (686, 1, 2073, 167, 0, 0, '몽크(2073)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (687, 0, 2074, 119, 0, 0, '여해적 선장(2074)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (688, 1, 2074, 167, 0, 0, '여해적 선장(2074)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (689, 0, 2075, 119, 0, 0, '나이트 오브 퀸(2075)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (690, 1, 2075, 167, 0, 0, '나이트 오브 퀸(2075)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (691, 0, 2076, 119, 0, 0, '사무라이(2076)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (692, 1, 2076, 167, 0, 0, '사무라이(2076)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (693, 0, 2077, 119, 0, 0, '네크로멘서(2077)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (694, 1, 2077, 167, 0, 0, '네크로멘서(2077)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (695, 0, 2078, 119, 0, 0, '글라디에이터(2078)변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (696, 1, 2078, 167, 0, 0, '글라디에이터(2078)변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (697, 0, 2079, 119, 0, 0, '다크 메이지 I (2079) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (698, 1, 2079, 167, 0, 0, '다크 메이지 I (2079) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (699, 0, 2080, 119, 0, 0, '길가메쉬 아처 I (2080) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (700, 1, 2080, 167, 0, 0, '길가메쉬 아처 I (2080) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (701, 0, 2081, 119, 0, 0, '홀리 소서리스 II (2081) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (702, 1, 2081, 167, 0, 0, '홀리 소서리스 II (2081) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (703, 0, 2082, 119, 0, 0, '엘리멘탈리스트II (2082) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (704, 1, 2082, 167, 0, 0, '엘리멘탈리스트II (2082) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (705, 0, 2083, 119, 0, 0, '길가메쉬 아처 II (2083) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (706, 1, 2083, 167, 0, 0, '길가메쉬 아처 II (2083) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (707, 0, 2084, 119, 0, 0, '홀리 소서리스 III (2084) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (708, 1, 2084, 167, 0, 0, '홀리 소서리스 III (2084) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (709, 0, 2085, 119, 0, 0, '엘리멘탈리스트III (2085) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (710, 1, 2085, 167, 0, 0, '엘리멘탈리스트III (2085) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (711, 0, 2086, 119, 0, 0, '다크 메이지 III (2086) 변신이 될 때 레비테이션 끈', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (712, 1, 2086, 167, 0, 0, '다크 메이지 III (2086) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (716, 0, 515, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (717, 0, 515, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (718, 0, 515, 38, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (719, 0, 516, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (720, 0, 516, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (721, 0, 516, 38, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (722, 0, 517, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (723, 0, 517, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (724, 0, 517, 38, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (725, 0, 817, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (726, 0, 817, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (727, 0, 817, 38, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (728, 0, 2094, 119, 0, 0, '아바돈  (2094) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (729, 1, 2094, 167, 0, 0, '아바돈  (2094) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (730, 0, 2095, 119, 0, 0, '메타트론 (2095) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (731, 1, 2095, 167, 0, 0, '메타트론 (2095) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (732, 0, 2096, 119, 0, 0, '메타트론 아처 (2096) 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (733, 1, 2096, 167, 0, 0, '메타트론 아처 (2096) 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (734, 0, 1575, 125, 0, 0, '키메라의 반지 탈때 타임디스토션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (735, 0, 1575, 155, 0, 0, '키메라의 반지 탈때 리플렉션오오라 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (736, 0, 1575, 167, 0, 0, '키메라의 반지 탈때 변신용 레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (737, 0, 1575, 207, 0, 0, '키메라의 반지 탈때 퓨인즈 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (738, 0, 1576, 125, 0, 0, '정복의 드라코 탈 때 타임 디스토션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (739, 0, 1576, 155, 0, 0, '정복의 드라코 탈 때 리플렉션 오오라 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (740, 0, 1576, 167, 0, 0, '정복의 드라코 탈 때 변신 레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (741, 0, 1576, 207, 0, 0, '정복의 드라코 탈 때 퓨인즈 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (742, 1, 2128, 167, 0, 0, '바룬용병대 오크대장 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (743, 0, 2128, 119, 0, 0, '바룬용병대 오크대장 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (744, 0, 2129, 119, 0, 0, '트레이서 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (745, 1, 2129, 167, 0, 0, '트레이서 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (746, 0, 2130, 119, 0, 0, '플린드 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (747, 1, 2130, 167, 0, 0, '플린드 아처 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (748, 0, 2131, 119, 0, 0, '데몬 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (749, 1, 2131, 167, 0, 0, '데몬 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (750, 0, 2132, 119, 0, 0, '다크 트레이서 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (751, 1, 2132, 167, 0, 0, '다크 트레이서 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (752, 0, 2133, 119, 0, 0, '레서 파이어 데몬 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (753, 1, 2133, 167, 0, 0, '레서 파이어 데몬 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (754, 0, 2134, 119, 0, 0, '래서 아스타로트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (755, 1, 2134, 167, 0, 0, '래서 아스타로트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (756, 0, 2135, 119, 0, 0, '래서 루시퍼 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (757, 1, 2135, 167, 0, 0, '래서 루시퍼 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (758, 0, 2136, 119, 0, 0, '그레이터 루시퍼 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (759, 1, 2136, 167, 0, 0, '그레이터 루시퍼 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (760, 0, 2137, 119, 0, 0, '그레이터 아스타로트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (761, 1, 2137, 167, 0, 0, '그레이터 아스타로트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (762, 0, 2138, 119, 0, 0, '머실리스 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (763, 1, 2138, 167, 0, 0, '머실리스 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (764, 0, 2139, 119, 0, 0, '쉬루드 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (765, 1, 2139, 167, 0, 0, '쉬루드 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (766, 0, 2140, 119, 0, 0, '크루얼 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (767, 1, 2140, 167, 0, 0, '크루얼 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (768, 0, 2141, 119, 0, 0, '님로드 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (769, 1, 2141, 167, 0, 0, '님로드 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (770, 0, 2142, 119, 0, 0, '앱솔루트 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (771, 1, 2142, 167, 0, 0, '앱솔루트 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (772, 0, 2143, 119, 0, 0, '베이그란트 크루세이더 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (773, 1, 2143, 167, 0, 0, '베이그란트 크루세이더 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (774, 0, 2144, 119, 0, 0, '드래곤나이트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (775, 1, 2144, 167, 0, 0, '드래곤나이트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (776, 0, 2145, 119, 0, 0, '드래곤나이트 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (777, 1, 2145, 167, 0, 0, '드래곤나이트 아처 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (778, 0, 2146, 119, 0, 0, '엘더 스팅어 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (779, 1, 2146, 167, 0, 0, '엘더 스팅어 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (780, 0, 2147, 119, 0, 0, '발키리 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (781, 1, 2147, 167, 0, 0, '발키리 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (782, 0, 2148, 119, 0, 0, '다크 나이트 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (783, 1, 2148, 167, 0, 0, '다크 나이트 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (784, 0, 2149, 119, 0, 0, '다크 스팅어 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (785, 1, 2149, 167, 0, 0, '다크 스팅어 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (786, 0, 2150, 119, 0, 0, '메타트론 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (787, 1, 2150, 167, 0, 0, '메타트론 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (788, 0, 2151, 119, 0, 0, '메타트론 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (789, 1, 2151, 167, 0, 0, '메타트론 아처 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (791, 1, 2152, 38, 0, 0, '투명망토 장착상태(2152)가 풀릴때 투명상태(38)을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (792, 1, 2154, 170, 0, 0, '주시자 장착상태(2154)가 풀릴때 투명감지상태(170)을', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (793, 0, 2245, 38, 0, 0, '디텍션(2245)이 될 때 투명상태를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (794, 0, 2231, 13, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (795, 0, 2231, 12, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (796, 0, 2231, 72, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (797, 0, 2231, 93, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (798, 0, 2231, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (799, 0, 2231, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (800, 0, 2231, 38, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (801, 0, 2231, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (802, 0, 2231, 125, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (803, 0, 2231, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (804, 0, 2231, 155, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (805, 0, 2231, 207, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (806, 0, 2189, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (807, 0, 2190, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (808, 0, 2191, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (809, 0, 2192, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (810, 0, 2193, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (811, 0, 2194, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (812, 0, 2195, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (813, 1, 2189, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (814, 1, 2190, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (815, 1, 2191, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (816, 1, 2192, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (817, 1, 2193, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (818, 1, 2194, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (819, 1, 2195, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (820, 1, 87, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (821, 1, 508, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (822, 1, 509, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (823, 1, 510, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (824, 1, 511, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (825, 1, 1829, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (826, 1, 236, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (827, 1, 1818, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (828, 1, 2233, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (829, 1, 2234, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (830, 1, 2235, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (831, 1, 2236, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (832, 1, 2237, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (833, 0, 107, 267, 0, 0, '드라코탑승중(107)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (834, 0, 207, 267, 0, 0, '드라코탑승중(207)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (835, 0, 173, 267, 0, 0, '드라코탑승중(173)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (836, 0, 485, 267, 0, 0, '드라코탑승중(485)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (837, 0, 309, 267, 0, 0, '드라코탑승중(309)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (838, 0, 318, 267, 0, 0, '드라코탑승중(318)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (839, 0, 317, 267, 0, 0, '드라코탑승중(317)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (840, 0, 319, 267, 0, 0, '드라코탑승중(319)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (841, 0, 500, 267, 0, 0, '드라코탑승중(500)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (842, 0, 834, 267, 0, 0, '드라코탑승중(834)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (843, 0, 1575, 267, 0, 0, '드라코탑승중(1575)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (844, 0, 1576, 267, 0, 0, '드라코탑승중(1576)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (845, 0, 2231, 267, 0, 0, '드라코탑승중(2231)일때 질주 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (846, 0, 2287, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (847, 1, 2287, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (848, 0, 2288, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (849, 1, 2288, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (850, 0, 2289, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (851, 1, 2289, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (852, 0, 2290, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (853, 1, 2290, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (854, 0, 2291, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (855, 1, 2291, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (856, 0, 2292, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (857, 1, 2292, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (858, 0, 2293, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (859, 1, 2293, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (860, 1, 2233, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (861, 1, 2234, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (862, 1, 2235, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (863, 1, 2236, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (864, 1, 2237, 98, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (865, 0, 2325, 119, 0, 0, '뮤 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (866, 1, 2325, 167, 0, 0, '뮤 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (867, 0, 2351, 119, 0, 0, '러블리 피어스 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (868, 1, 2351, 167, 0, 0, '러블리 피어스 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (869, 0, 2352, 119, 0, 0, '실피드 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (870, 1, 2352, 167, 0, 0, '실피드 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (871, 0, 2549, 119, 0, 0, '비숍 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (872, 1, 2549, 167, 0, 0, '비숍 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (873, 1, 2583, 167, 0, 0, '황진이 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (874, 0, 2583, 119, 0, 0, '황진이 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (875, 0, 2584, 119, 0, 0, '베르키오라 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (876, 1, 2584, 167, 0, 0, '베르키오라 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (877, 0, 2585, 119, 0, 0, '헌터 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (878, 1, 2585, 167, 0, 0, '헌터 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (879, 0, 2586, 119, 0, 0, '스나이퍼 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (880, 1, 2586, 167, 0, 0, '스나이퍼 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (881, 0, 2587, 119, 0, 0, '미드나잇 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (882, 1, 2587, 167, 0, 0, '미드나잇 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (883, 0, 2556, 119, 0, 0, '데몬 스토커 변신이 될 때 레이테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (884, 1, 2556, 167, 0, 0, '데몬 스토커 변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (885, 0, 2557, 119, 0, 0, '헤라클레스 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (886, 1, 2557, 167, 0, 0, '헤라클레스 변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (887, 0, 2558, 119, 0, 0, '광대 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (888, 1, 2558, 167, 0, 0, '광대 변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (889, 0, 2559, 119, 0, 0, '버서커 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (890, 1, 2559, 167, 0, 0, '버서커 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (891, 0, 2561, 119, 0, 0, '여신 칼리 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (892, 1, 2561, 167, 0, 0, '여신 칼리 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (893, 0, 2794, 119, 0, 0, '데드마스터 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (894, 1, 2794, 167, 0, 0, '데드마스터 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (895, 0, 2795, 119, 0, 0, '데드마스터 아처 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (896, 1, 2795, 167, 0, 0, '데드마스터 아처 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (897, 0, 2550, 119, 0, 0, '센티넬 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (898, 1, 2550, 167, 0, 0, '센티넬 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (899, 0, 2551, 119, 0, 0, '데빌헌터 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (900, 1, 2551, 167, 0, 0, '데빌헌터 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (901, 0, 2814, 0, 0, 0, '디텍션1(2814)를 걸 때 투명1레벨이 풀린다', 2809);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (902, 0, 2815, 0, 0, 0, '디텍션2(2815)를 걸 때 투명1레벨이 풀린다', 2809);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (903, 0, 2815, 0, 0, 0, '디텍션2(2815)를 걸 때 투명2레벨이 풀린다', 2810);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (904, 0, 2816, 0, 0, 0, '디텍션3(2816)를 걸 때 투명1레벨이 풀린다', 2809);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (905, 0, 2816, 0, 0, 0, '디텍션3(2816)를 걸 때 투명2레벨이 풀린다', 2810);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (906, 0, 2816, 0, 0, 0, '디텍션3(2816)를 걸 때 투명3레벨이 풀린다', 2811);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (907, 0, 2814, 94, 0, 0, '디텍션1(2814)를 걸 때 인비지가 풀린다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (908, 0, 2814, 98, 0, 0, '디텍션1(2814)를 걸 때 어쌔시가 풀린다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (909, 0, 2815, 94, 0, 0, '디텍션2(2815)를 걸 때 인비지가 풀린다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (910, 0, 2815, 98, 0, 0, '디텍션2(2815)를 걸 때 어쌔시가 풀린다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (911, 0, 2816, 94, 0, 0, '디텍션3(2815)를 걸 때 인비지가 풀린다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (912, 0, 2816, 98, 0, 0, '디텍션3(2816)를 걸 때 어쌔시가 풀린다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (913, 0, 2809, 127, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (914, 0, 2809, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (915, 1, 2809, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (916, 0, 2809, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (917, 0, 2810, 127, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (918, 1, 2810, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (919, 0, 2810, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (920, 0, 2810, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (921, 0, 2811, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (922, 0, 2811, 127, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (923, 1, 2811, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (924, 0, 2811, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (925, 0, 2812, 127, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (926, 0, 2812, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (927, 1, 2812, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (928, 0, 2812, 94, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (929, 0, 2731, 125, 0, 0, '퓨인즈 강화(III) 걸릴 때 타임디스토션 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (930, 0, 2814, 0, 0, 0, '디텍션1(2814)를 걸 때 (87) 마법의 숄 투명1레벨이 ?', 87);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (931, 0, 2815, 0, 0, 0, '디텍션1(2814)를 걸 때 (87) 마법의 숄 투명1레벨이 ?', 87);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (932, 0, 2816, 0, 0, 0, '디텍션1(2814)를 걸 때 (87) 마법의 숄 투명1레벨이 ?', 87);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (938, 1, 236, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (939, 1, 1818, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (940, 1, 2233, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (941, 1, 2234, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (942, 1, 2235, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (943, 1, 2236, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (944, 1, 2237, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (945, 1, 236, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (946, 1, 1818, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (947, 1, 2233, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (948, 1, 2234, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (949, 1, 2235, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (950, 1, 2236, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (951, 1, 2237, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (952, 1, 236, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (953, 1, 1818, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (954, 1, 2233, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (955, 1, 2234, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (956, 1, 2235, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (957, 1, 2236, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (958, 1, 2237, 0, 0, 0, '인비지빌리티가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (959, 0, 69, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (960, 0, 188, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (961, 0, 189, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (962, 0, 259, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (963, 0, 308, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (964, 0, 346, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (965, 0, 347, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (966, 0, 360, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (967, 0, 361, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (968, 0, 362, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (969, 0, 363, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (970, 0, 483, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (971, 0, 484, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (972, 0, 505, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (973, 0, 506, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (974, 0, 551, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (975, 0, 552, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (976, 0, 553, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (977, 0, 572, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (978, 0, 573, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (979, 0, 574, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (980, 0, 575, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (981, 0, 576, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (982, 0, 577, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (983, 0, 578, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (984, 0, 579, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (985, 0, 580, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (986, 0, 581, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (987, 0, 582, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (988, 0, 583, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (989, 0, 584, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (990, 0, 585, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (991, 0, 586, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (992, 0, 587, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (993, 0, 588, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (994, 0, 589, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (995, 0, 590, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (996, 0, 591, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (997, 0, 592, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (998, 0, 593, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (999, 0, 602, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1000, 0, 603, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1001, 0, 604, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1002, 0, 605, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1003, 0, 614, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1004, 0, 615, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1005, 0, 616, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1006, 0, 617, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1007, 0, 618, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1008, 0, 619, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1009, 0, 620, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1010, 0, 621, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1011, 0, 711, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1012, 0, 712, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1013, 0, 713, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1014, 0, 714, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1015, 0, 715, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1016, 0, 716, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1017, 0, 717, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1018, 0, 718, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1019, 0, 730, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1020, 0, 732, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1021, 0, 733, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1022, 0, 734, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1023, 0, 735, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1024, 0, 736, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1025, 0, 737, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1026, 0, 738, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1027, 0, 739, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1028, 0, 740, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1029, 0, 741, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1030, 0, 742, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1031, 0, 743, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1032, 0, 791, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1033, 0, 792, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1034, 0, 793, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1035, 0, 794, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1036, 0, 795, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1037, 0, 796, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1038, 0, 797, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1039, 0, 798, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1040, 0, 799, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1041, 0, 800, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1042, 0, 801, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1043, 0, 802, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1044, 0, 803, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1045, 0, 804, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1046, 0, 805, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1047, 0, 806, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1048, 0, 807, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1049, 0, 808, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1050, 0, 809, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1051, 0, 810, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1052, 0, 811, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1053, 0, 812, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1054, 0, 813, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1055, 0, 814, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1056, 0, 818, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1057, 0, 819, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1058, 0, 820, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1059, 0, 821, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1060, 0, 822, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1061, 0, 823, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1062, 0, 824, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1063, 0, 825, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1064, 0, 826, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1065, 0, 827, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1066, 0, 828, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1067, 0, 829, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1068, 0, 830, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1069, 0, 831, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1070, 0, 832, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1071, 0, 848, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1072, 0, 849, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1073, 0, 850, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1074, 0, 851, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1075, 0, 852, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1076, 0, 853, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1077, 0, 854, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1078, 0, 855, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1079, 0, 856, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1080, 0, 857, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1081, 0, 858, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1082, 0, 859, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1083, 0, 860, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1084, 0, 912, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1085, 0, 913, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1086, 0, 914, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1087, 0, 915, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1088, 0, 916, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1089, 0, 917, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1090, 0, 918, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1091, 0, 919, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1092, 0, 922, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1093, 0, 923, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1094, 0, 924, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1095, 0, 925, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1096, 0, 949, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1097, 0, 950, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1098, 0, 951, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1099, 0, 952, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1100, 0, 953, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1101, 0, 954, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1102, 0, 955, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1103, 0, 956, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1104, 0, 957, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1105, 0, 958, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1106, 0, 959, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1107, 0, 960, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1108, 0, 961, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1109, 0, 962, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1110, 0, 963, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1111, 0, 964, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1112, 0, 965, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1113, 0, 966, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1114, 0, 967, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1115, 0, 968, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1116, 0, 969, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1117, 0, 970, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1118, 0, 971, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1119, 0, 972, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1120, 0, 973, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1121, 0, 974, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1122, 0, 975, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1123, 0, 976, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1124, 0, 977, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1125, 0, 978, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1126, 0, 979, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1127, 0, 980, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1128, 0, 981, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1129, 0, 982, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1130, 0, 999, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1131, 0, 1000, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1132, 0, 1030, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1133, 0, 1031, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1134, 0, 1032, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1135, 0, 1033, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1136, 0, 1046, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1137, 0, 1047, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1138, 0, 1048, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1139, 0, 1049, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1140, 0, 1050, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1141, 0, 1051, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1142, 0, 1052, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1143, 0, 1440, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1144, 0, 1900, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1145, 0, 1901, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1146, 0, 1902, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1147, 0, 1903, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1148, 0, 1904, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1149, 0, 1905, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1150, 0, 1971, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1151, 0, 1972, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1152, 0, 1973, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1153, 0, 1974, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1154, 0, 1975, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1155, 0, 1976, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1156, 0, 1977, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1157, 0, 1978, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1158, 0, 1983, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1159, 0, 1987, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1160, 0, 1988, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1161, 0, 1989, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1162, 0, 1990, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1163, 0, 1991, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1164, 0, 1992, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1165, 0, 1993, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1166, 0, 1994, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1167, 0, 1995, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1168, 0, 1996, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1169, 0, 1997, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1170, 0, 1998, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1171, 0, 1999, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1172, 0, 2000, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1173, 0, 2001, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1174, 0, 2002, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1175, 0, 2003, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1176, 0, 2004, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1177, 0, 2005, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1178, 0, 2006, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1179, 0, 2007, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1180, 0, 2008, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1181, 0, 2009, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1182, 0, 2062, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1183, 0, 2063, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1184, 0, 2064, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1185, 0, 2065, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1186, 0, 2066, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1187, 0, 2067, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1188, 0, 2068, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1189, 0, 2069, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1190, 0, 2070, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1191, 0, 2071, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1192, 0, 2072, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1193, 0, 2073, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1194, 0, 2074, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1195, 0, 2075, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1196, 0, 2076, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1197, 0, 2077, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1198, 0, 2078, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1199, 0, 2079, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1200, 0, 2080, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1201, 0, 2081, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1202, 0, 2082, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1203, 0, 2083, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1204, 0, 2084, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1205, 0, 2085, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1206, 0, 2086, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1207, 0, 2094, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1208, 0, 2095, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1209, 0, 2096, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1210, 0, 2128, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1211, 0, 2129, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1212, 0, 2130, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1213, 0, 2131, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1214, 0, 2132, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1215, 0, 2133, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1216, 0, 2134, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1217, 0, 2135, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1218, 0, 2136, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1219, 0, 2137, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1220, 0, 2138, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1221, 0, 2139, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1222, 0, 2140, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1223, 0, 2141, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1224, 0, 2142, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1225, 0, 2143, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1226, 0, 2144, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1227, 0, 2145, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1228, 0, 2146, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1229, 0, 2147, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1230, 0, 2148, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1231, 0, 2149, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1232, 0, 2150, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1233, 0, 2151, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1234, 0, 2189, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1235, 0, 2190, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1236, 0, 2191, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1237, 0, 2192, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1238, 0, 2193, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1239, 0, 2194, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1240, 0, 2195, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1241, 0, 2261, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1242, 0, 2262, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1243, 0, 2263, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1244, 0, 2264, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1245, 0, 2265, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1246, 0, 2266, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1247, 0, 2267, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1248, 0, 2268, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1249, 0, 2269, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1250, 0, 2270, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1251, 0, 2271, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1252, 0, 2272, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1253, 0, 2273, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1254, 0, 2274, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1255, 0, 2275, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1256, 0, 2276, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1257, 0, 2277, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1258, 0, 2278, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1259, 0, 2279, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1260, 0, 2280, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1261, 0, 2281, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1262, 0, 2282, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1263, 0, 2283, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1264, 0, 2284, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1265, 0, 2287, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1266, 0, 2288, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1267, 0, 2289, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1268, 0, 2290, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1269, 0, 2291, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1270, 0, 2292, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1271, 0, 2293, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1272, 0, 2325, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1273, 0, 2351, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1274, 0, 2352, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1275, 0, 2549, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1276, 0, 2550, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1277, 0, 2551, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1278, 0, 2556, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1279, 0, 2557, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1280, 0, 2558, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1281, 0, 2559, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1282, 0, 2561, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1283, 0, 2583, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1284, 0, 2584, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1285, 0, 2585, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1286, 0, 2586, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1287, 0, 2587, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1288, 0, 2794, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1289, 0, 2795, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1290, 0, 2885, 119, 0, 0, '비숍 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1291, 1, 2885, 167, 0, 0, '비숍 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1292, 0, 2886, 119, 0, 0, '실피드 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1293, 1, 2886, 167, 0, 0, '실피드 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1294, 0, 2887, 119, 0, 0, '뮤 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1295, 1, 2887, 167, 0, 0, '뮤 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1296, 0, 2888, 119, 0, 0, '센티넬 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1297, 1, 2888, 167, 0, 0, '센티넬 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1298, 0, 2889, 119, 0, 0, '데빌헌터 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1299, 1, 2889, 167, 0, 0, '데빌헌터 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1300, 0, 2904, 119, 0, 0, '체인 인큐버스 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1301, 1, 2904, 167, 0, 0, '체인 인큐버스 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1302, 0, 2905, 119, 0, 0, '닌자 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1303, 1, 2905, 167, 0, 0, '닌자 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1304, 0, 2906, 119, 0, 0, '파이어워크스 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1305, 1, 2906, 167, 0, 0, '파이어워크스 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1306, 0, 2907, 119, 0, 0, '알케미스트 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1307, 1, 2907, 167, 0, 0, '알케미스트 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1308, 0, 2908, 119, 0, 0, '아스모데우스 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1309, 1, 2908, 167, 0, 0, '아스모데우스 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1310, 0, 2909, 119, 0, 0, '아누비스 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1311, 1, 2909, 167, 0, 0, '아누비스 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1312, 0, 2910, 119, 0, 0, '바바리안 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1313, 1, 2910, 167, 0, 0, '바바리안 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1314, 0, 2911, 119, 0, 0, '다크 메이지 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1315, 1, 2911, 167, 0, 0, '다크 메이지 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1316, 0, 2912, 119, 0, 0, '차크람 소녀 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1317, 1, 2912, 167, 0, 0, '차크람 소녀 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1318, 0, 2913, 119, 0, 0, '바드 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1319, 1, 2913, 167, 0, 0, '바드 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1320, 0, 2914, 119, 0, 0, '아수라 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1321, 1, 2914, 167, 0, 0, '아수라 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1322, 0, 2915, 119, 0, 0, '엘리멘탈리스트 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1323, 1, 2915, 167, 0, 0, '엘리멘탈리스트 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1324, 0, 2916, 119, 0, 0, '무녀 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1325, 1, 2916, 167, 0, 0, '무녀 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1326, 0, 2917, 119, 0, 0, '길가메쉬 나이트 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1327, 1, 2917, 167, 0, 0, '길가메쉬 나이트 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1328, 0, 2918, 119, 0, 0, '길가메쉬 아처 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1329, 1, 2918, 167, 0, 0, '길가메쉬 아처 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1330, 0, 2919, 119, 0, 0, '몽크 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1331, 1, 2919, 167, 0, 0, '몽크 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1332, 0, 2920, 119, 0, 0, '여해적 선장 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1333, 1, 2920, 167, 0, 0, '여해적 선장 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1334, 0, 2921, 119, 0, 0, '나이트 오브 퀸 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1335, 1, 2921, 167, 0, 0, '나이트 오브 퀸 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1336, 0, 2922, 119, 0, 0, '사무라이 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1337, 1, 2922, 167, 0, 0, '사무라이 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1338, 0, 2923, 119, 0, 0, '네크로멘서 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1339, 1, 2923, 167, 0, 0, '네크로멘서 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1340, 0, 2924, 119, 0, 0, '데몬 스토커 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1341, 1, 2924, 167, 0, 0, '데몬 스토커 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1342, 0, 2925, 119, 0, 0, '글라디에이터 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1343, 1, 2925, 167, 0, 0, '글라디에이터 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1344, 0, 2926, 119, 0, 0, '헤라클레스 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1345, 1, 2926, 167, 0, 0, '헤라클레스 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1346, 0, 2927, 119, 0, 0, '아바돈 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1347, 1, 2927, 167, 0, 0, '아바돈 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1348, 0, 2928, 119, 0, 0, '광대 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1349, 1, 2928, 167, 0, 0, '광대 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1350, 0, 2929, 119, 0, 0, '버서커 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1351, 1, 2929, 167, 0, 0, '버서커 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1352, 0, 2930, 119, 0, 0, '여신 칼리 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1353, 1, 2930, 167, 0, 0, '여신 칼리 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1354, 0, 2931, 119, 0, 0, '비숍 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1355, 1, 2931, 167, 0, 0, '비숍 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1356, 0, 2932, 119, 0, 0, '실피드 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1357, 1, 2932, 167, 0, 0, '실피드 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1358, 0, 2933, 119, 0, 0, '뮤 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1359, 1, 2933, 167, 0, 0, '뮤 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1360, 0, 2934, 119, 0, 0, '센티넬 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1361, 1, 2934, 167, 0, 0, '센티넬 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1362, 0, 2935, 119, 0, 0, '데빌헌터 변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1363, 1, 2935, 167, 0, 0, '데빌헌터 변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1364, 0, 2937, 119, 0, 0, '캣츠메이드 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1365, 0, 2938, 119, 0, 0, '데빌리스 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1366, 0, 2939, 119, 0, 0, '엔젤리스 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1367, 0, 2944, 119, 0, 0, '드래곤룰러 오페르시나 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1368, 0, 2945, 119, 0, 0, '드래곤룰러 오페르시나 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1369, 1, 2937, 167, 0, 0, '캣츠메이드 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1370, 1, 2938, 167, 0, 0, '데빌리스 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1371, 1, 2939, 167, 0, 0, '엔젤리스 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1372, 1, 2944, 167, 0, 0, '드래곤룰러 오페르시나 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1373, 1, 2945, 167, 0, 0, '드래곤룰러 오페르시나 아처 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1374, 0, 2937, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1375, 0, 2938, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1376, 0, 2939, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1377, 0, 2944, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1378, 0, 2945, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1379, 0, 2960, 119, 0, 0, '유피테르의 사념 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1380, 0, 2961, 119, 0, 0, '바알베크의 사령관 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1381, 1, 2960, 167, 0, 0, '유피테르의 사념 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1382, 1, 2961, 167, 0, 0, '바알베크의 사령관 사념 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1383, 0, 2960, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1384, 0, 2961, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1391, 0, 2969, 119, 0, 0, '아누비스 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1392, 0, 2970, 119, 0, 0, '문엘프 마법사 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1393, 1, 2969, 167, 0, 0, '아누비스 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1394, 1, 2970, 167, 0, 0, '문엘프 마법사 변신이 될 때 변신레비 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1395, 0, 2969, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1396, 0, 2970, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1398, 0, 2967, 119, 0, 0, '인페르노 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1399, 0, 2968, 119, 0, 0, '인페르노 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1400, 1, 2967, 167, 0, 0, '인페르노 변신이 될 때 변신레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1401, 1, 2968, 167, 0, 0, '인페르노 아처 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1402, 0, 2967, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1403, 0, 2968, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1404, 0, 2975, 119, 0, 0, '메타트론 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1405, 0, 2976, 119, 0, 0, '메타트론 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1406, 1, 2975, 167, 0, 0, '메타트론 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1407, 1, 2976, 167, 0, 0, '메타트론 아처 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1408, 0, 2975, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1409, 0, 2976, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1410, 0, 2977, 119, 0, 0, '넵튠 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1411, 0, 2978, 119, 0, 0, '넵튠 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1412, 1, 2977, 167, 0, 0, '넵튠 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1413, 1, 2978, 167, 0, 0, '넵튠 아처 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1414, 0, 2977, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1415, 0, 2978, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1416, 0, 2979, 119, 0, 0, '오페르시나 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1417, 0, 2980, 119, 0, 0, '오페르시나 아처 변신이 될 때 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1418, 1, 2979, 167, 0, 0, '오페르시나 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1419, 1, 2980, 167, 0, 0, '오페르시나 아처 변신이 될 때 변신 레비테이션 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1420, 0, 2979, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1421, 0, 2980, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1422, 0, 489, 167, 0, 0, '레비테이션을 사용하면 변신용 레비테이션 해제', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1423, 0, 3033, 171, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1424, 0, 3034, 300, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1425, 0, 3034, 301, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1426, 0, 3046, 119, 0, 0, '캣츠 메이드 (3046)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1427, 0, 3046, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1428, 1, 3046, 167, 0, 0, '캣츠 메이드 (3046)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1429, 1, 3047, 167, 0, 0, '실피드 (3047)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1430, 0, 3047, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1431, 0, 3047, 119, 0, 0, '실피드 (3047)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1432, 1, 3048, 167, 0, 0, '황진이 (3048)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1433, 0, 3048, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1434, 0, 3048, 119, 0, 0, '황진이 (3048)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1435, 1, 3049, 167, 0, 0, '비숍 (3049)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1436, 0, 3049, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1437, 0, 3049, 119, 0, 0, '비숍 (3049)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1438, 1, 3050, 167, 0, 0, '헌터 리바인저 (3050)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1439, 0, 3050, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1440, 0, 3050, 119, 0, 0, '헌터 리바인저 (3050)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1441, 1, 3051, 167, 0, 0, '스나이퍼 빅스웰 (3051)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1442, 0, 3051, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1443, 0, 3051, 119, 0, 0, '스나이퍼 빅스웰 (3051)변신이 될 때레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1444, 1, 3052, 167, 0, 0, '러블리 피어스 (3052)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1445, 0, 3052, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1446, 0, 3052, 119, 0, 0, '러블리 피어스 (3052)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1447, 1, 3053, 167, 0, 0, '홀리 소서리스 (3053)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1448, 0, 3053, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1449, 0, 3053, 119, 0, 0, '홀리 소서리스 (3053)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1450, 1, 3054, 167, 0, 0, '문엘프 전사(V) (3054)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1451, 0, 3054, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1452, 0, 3054, 119, 0, 0, '문엘프 전사(V) (3054)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1453, 1, 3055, 167, 0, 0, '미드나잇 에르메스 (3055)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1454, 0, 3055, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1455, 0, 3055, 119, 0, 0, '미드나잇 에르메스 (3055)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1456, 1, 3056, 167, 0, 0, '베르키오라 (3056)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1457, 0, 3056, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1458, 0, 3056, 119, 0, 0, '베르키오라 (3056)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1459, 1, 3057, 167, 0, 0, '데빌리스 (3057)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1460, 0, 3057, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1461, 0, 3057, 119, 0, 0, '데빌리스 (3057)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1462, 1, 3058, 167, 0, 0, '엔젤리스 (3058)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1463, 0, 3058, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1464, 0, 3058, 119, 0, 0, '엔젤리스 (3058)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1465, 1, 3059, 167, 0, 0, '아레스 (3059)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1466, 0, 3059, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1467, 0, 3059, 119, 0, 0, '아레스 (3059)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1468, 1, 3060, 167, 0, 0, '아레스 아처 (3060)변신이 될 때 변신 레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1469, 0, 3060, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1470, 0, 3060, 119, 0, 0, '아레스 아처 (3060)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1471, 1, 3061, 167, 0, 0, '유피테르의 사념 (3061)변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1472, 0, 3061, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1473, 0, 3061, 119, 0, 0, '유피테르의 사념 (3061)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1474, 1, 3062, 167, 0, 0, '바알베크의 사령관 (3062)변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1475, 0, 3062, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1476, 0, 3062, 119, 0, 0, '바알베크의 사령관 (3062)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1477, 0, 3082, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1478, 1, 3082, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1479, 0, 3082, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1480, 0, 3083, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1481, 1, 3083, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1482, 0, 3083, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1483, 0, 3084, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1484, 1, 3084, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1485, 0, 3084, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1486, 0, 3085, 119, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1487, 1, 3085, 167, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1488, 0, 3085, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1489, 1, 3137, 312, 0, 0, '어쌔시네이션(3137)이 풀릴 때, 어쌔시네이션 - 매의 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1490, 1, 3138, 308, 0, 0, '리플렉션 오오라(3138)이 풀릴 때, 리플렉션 오오라 - ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1491, 1, 3139, 309, 0, 0, '오리에드 토템(3139)이 풀릴 때, 오리에드 토템 소환 -', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1492, 0, 3136, 84, 0, 0, '실프윈드 - 폭풍의 시간을 쓸 때, 실프윈드를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1493, 1, 3140, 311, 0, 0, '레비테이션(3140)이 풀릴 때, 레비테이션 - 회피 증가를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1494, 1, 3141, 311, 0, 0, '변신용 레비테이션(3141)이 풀릴 때, 레비테이션 - 회피 증가를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1495, 0, 3232, 12, 0, 0, '3232일 때 공격속도업을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1496, 0, 3232, 13, 0, 0, '3232일 때 이동속도업을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1497, 0, 3232, 38, 0, 0, '3232일 때 투명상태을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1498, 0, 3232, 72, 0, 0, '3232일 때 버서크을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1499, 0, 3232, 93, 0, 0, '3232일 때 스캠퍼을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1500, 0, 3232, 94, 0, 0, '3232일 때 인비지빌리티을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1501, 0, 3232, 98, 0, 0, '3232일 때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1502, 0, 3232, 119, 0, 0, '3232일 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1503, 0, 3232, 125, 0, 0, '3232일 때 타임 디스토션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1504, 0, 3232, 155, 0, 0, '3232일 때 리플렉션 오오라을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1505, 0, 3232, 167, 0, 0, '3232일 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1506, 0, 3232, 207, 0, 0, '3232일 때 퓨인즈을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1507, 0, 3232, 267, 0, 0, '3232일 때 질주을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1508, 0, 3233, 12, 0, 0, '3233일 때 공격속도업을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1509, 0, 3233, 13, 0, 0, '3233일 때 이동속도업을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1510, 0, 3233, 38, 0, 0, '3233일 때 투명상태을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1511, 0, 3233, 72, 0, 0, '3233일 때 버서크을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1512, 0, 3233, 93, 0, 0, '3233일 때 스캠퍼을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1513, 0, 3233, 94, 0, 0, '3233일 때 인비지빌리티을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1514, 0, 3233, 98, 0, 0, '3233일 때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1515, 0, 3233, 119, 0, 0, '3233일 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1516, 0, 3233, 125, 0, 0, '3233일 때 타임 디스토션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1517, 0, 3233, 155, 0, 0, '3233일 때 리플렉션 오오라을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1518, 0, 3233, 167, 0, 0, '3233일 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1519, 0, 3233, 207, 0, 0, '3233일 때 퓨인즈을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1520, 0, 3233, 267, 0, 0, '3233일 때 질주을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1521, 0, 3234, 12, 0, 0, '3234일 때 공격속도업을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1522, 0, 3234, 13, 0, 0, '3234일 때 이동속도업을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1523, 0, 3234, 38, 0, 0, '3234일 때 투명상태을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1524, 0, 3234, 72, 0, 0, '3234일 때 버서크을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1525, 0, 3234, 93, 0, 0, '3234일 때 스캠퍼을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1526, 0, 3234, 94, 0, 0, '3234일 때 인비지빌리티을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1527, 0, 3234, 98, 0, 0, '3234일 때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1528, 0, 3234, 119, 0, 0, '3234일 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1529, 0, 3234, 125, 0, 0, '3234일 때 타임 디스토션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1530, 0, 3234, 155, 0, 0, '3234일 때 리플렉션 오오라을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1531, 0, 3234, 167, 0, 0, '3234일 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1532, 0, 3234, 207, 0, 0, '3234일 때 퓨인즈을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1533, 0, 3234, 267, 0, 0, '3234일 때 질주을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1573, 1, 3133, 312, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1574, 1, 236, 312, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1575, 1, 3096, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1576, 1, 3096, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1577, 1, 3096, 0, 0, 0, '인비지 강화가 꺼질때 이동속도 속도 업을 끈다', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1578, 1, 3096, 0, 0, 0, '인비지 강화가 꺼질때 이동속도 속도 업을 끈다', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1579, 1, 3096, 0, 0, 0, '인비지 강화가 꺼질때 이동속도 속도 업을 끈다', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1580, 1, 3096, 312, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1581, 1, 3235, 167, 0, 0, '수련소녀 (VI) (3235) 변신이 될 때 변신 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1582, 0, 3235, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1583, 0, 3235, 119, 0, 0, '수련소녀 (VI) (3235) 변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1584, 1, 3236, 167, 0, 0, '큐티 프리스트 (VI) (3236) 변신이 될 때 변신 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1585, 0, 3236, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1586, 0, 3236, 119, 0, 0, '큐티 프리스트 (VI) (3236) 변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1587, 1, 3237, 167, 0, 0, '가이아 (3237) 변신이 될 때 변신 레베테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1588, 0, 3237, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1590, 0, 3237, 119, 0, 0, '가이아 (3237) 변신이 될 때 레베테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1591, 1, 3238, 167, 0, 0, '가이아 거너 (3238) 변신이 될 때 변신 레베테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1592, 0, 3238, 291, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1593, 0, 3238, 119, 0, 0, '가이아 거너 (3238) 변신이 될 때 레베테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1597, 1, 2814, 0, 0, 0, '디텍션1(2814)를 걸 때 이동속도1 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1598, 1, 2814, 0, 0, 0, '디텍션1(2814)를 걸 때 이동속도2 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1599, 1, 2814, 0, 0, 0, '디텍션1(2814)를 걸 때 이동속도3 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1600, 1, 2815, 0, 0, 0, '디텍션2(2815)를 걸 때 이동속도1 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1601, 1, 2815, 0, 0, 0, '디텍션2(2815)를 걸 때 이동속도2 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1602, 1, 2815, 0, 0, 0, '디텍션2(2815)를 걸 때 이동속도3 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1603, 1, 2816, 0, 0, 0, '디텍션3(2816)를 걸 때 이동속도1 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1604, 1, 2816, 0, 0, 0, '디텍션3(2816)를 걸 때 이동속도2 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1605, 1, 2816, 0, 0, 0, '디텍션3(2816)를 걸 때 이동속도3 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1606, 1, 87, 0, 0, 0, '마법의 숄 장착해지시 이동속도1 업을 끈다', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1607, 1, 87, 0, 0, 0, '마법의 숄 장착해지시 이동속도2 업을 끈다', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1608, 1, 87, 0, 0, 0, '마법의 숄 장착해지시 이동속도3 업을 끈다', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1609, 1, 2809, 0, 0, 0, '투명상태1 해지시 이동속도1 업을 끈다', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1610, 1, 2809, 0, 0, 0, '투명상태1 해지시 이동속도2 업을 끈다', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1611, 1, 2809, 0, 0, 0, '투명상태1 해지시 이동속도3 업을 끈다', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1612, 1, 2810, 0, 0, 0, '투명상태2 해지시 이동속도1 업을 끈다', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1613, 1, 2810, 0, 0, 0, '투명상태2 해지시 이동속도2 업을 끈다', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1614, 1, 2810, 0, 0, 0, '투명상태2 해지시 이동속도3 업을 끈다', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1615, 1, 2811, 0, 0, 0, '투명상태3 해지시 이동속도1 업을 끈다', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1616, 1, 2811, 0, 0, 0, '투명상태3 해지시 이동속도2 업을 끈다', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1617, 1, 2811, 0, 0, 0, '투명상태3 해지시 이동속도3 업을 끈다', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1618, 1, 2812, 0, 0, 0, '투명상태4 해지시 이동속도1 업을 끈다', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1619, 1, 2812, 0, 0, 0, '투명상태4 해지시 이동속도2 업을 끈다', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1620, 1, 2812, 0, 0, 0, '투명상태4 해지시 이동속도3 업을 끈다', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1621, 1, 3243, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1622, 1, 3243, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1623, 1, 3243, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1624, 1, 3244, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1625, 1, 3244, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1626, 1, 3244, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1627, 1, 3245, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1628, 1, 3245, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1629, 1, 3245, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1630, 1, 3246, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1631, 1, 3246, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1632, 1, 3246, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1633, 1, 3247, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1608);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1634, 1, 3247, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1609);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1635, 1, 3247, 0, 0, 0, ' 인비지빌리티(안식)가 꺼질 때 이동속도 업을 끈다.', 1610);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1636, 1, 3243, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1637, 1, 3244, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1638, 1, 3245, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1639, 1, 3246, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1640, 1, 3247, 266, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1642, 1, 1818, 98, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1643, 1, 1818, 312, 0, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1644, 0, 3213, 328, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1645, 1, 3213, 328, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1646, 0, 3213, 319, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1647, 1, 3213, 319, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1648, 0, 3213, 324, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1649, 1, 3213, 324, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1650, 0, 3213, 329, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1651, 1, 3213, 329, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1652, 0, 3213, 325, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1653, 1, 3213, 325, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1654, 0, 3213, 326, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1655, 1, 3213, 326, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1656, 0, 3213, 321, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1657, 1, 3213, 321, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1658, 0, 3213, 317, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1659, 1, 3213, 317, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1660, 0, 3213, 320, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1661, 1, 3213, 320, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1662, 0, 3213, 327, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1663, 1, 3213, 327, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1664, 0, 3213, 316, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1665, 1, 3213, 316, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1666, 0, 3213, 323, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1667, 1, 3213, 323, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1668, 0, 3213, 322, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1669, 1, 3213, 322, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1670, 0, 3213, 315, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1671, 1, 3213, 315, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1672, 0, 3367, 12, 0, 0, '강인한 대지의 드라코(I)일 때 공격 속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1673, 0, 3367, 13, 0, 0, '강인한 대지의 드라코(I)일 때 이동 속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1674, 0, 3367, 38, 0, 0, '강인한 대지의 드라코(I)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1675, 0, 3367, 72, 0, 0, '강인한 대지의 드라코(I)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1676, 0, 3367, 93, 0, 0, '강인한 대지의 드라코(I)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1677, 0, 3367, 94, 0, 0, '강인한 대지의 드라코(I)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1678, 0, 3367, 98, 0, 0, '강인한 대지의 드라코(I)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1679, 0, 3367, 119, 0, 0, '강인한 대지의 드라코(I)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1680, 0, 3367, 125, 0, 0, '강인한 대지의 드라코(I)일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1681, 0, 3367, 155, 0, 0, '강인한 대지의 드라코(I)일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1682, 0, 3367, 167, 0, 0, '강인한 대지의 드라코(I)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1683, 0, 3367, 207, 0, 0, '강인한 대지의 드라코(I)일 때 휴인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1684, 0, 3367, 267, 0, 0, '강인한 대지의 드라코(I)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1685, 0, 3369, 12, 0, 0, '강인한 대지의 드라코 (II)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1686, 0, 3369, 13, 0, 0, '강인한 대지의 드라코 (II)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1687, 0, 3369, 38, 0, 0, '강인한 대지의 드라코 (II)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1688, 0, 3369, 72, 0, 0, '강인한 대지의 드라코 (II)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1689, 0, 3369, 93, 0, 0, '강인한 대지의 드라코 (II)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1690, 0, 3369, 94, 0, 0, '강인한 대지의 드라코 (II)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1691, 0, 3369, 98, 0, 0, '강인한 대지의 드라코 (II)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1692, 0, 3369, 119, 0, 0, '강인한 대지의 드라코 (II)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1693, 0, 3369, 125, 0, 0, '강인한 대지의 드라코 (II)일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1694, 0, 3369, 155, 0, 0, '강인한 대지의 드라코 (II)일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1695, 0, 3369, 167, 0, 0, '강인한 대지의 드라코 (II)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1696, 0, 3369, 207, 0, 0, '강인한 대지의 드라코 (II)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1697, 0, 3369, 267, 0, 0, '강인한 대지의 드라코 (II)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1698, 0, 3371, 12, 0, 0, '강인한 태양의 드라코 (I)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1699, 0, 3371, 13, 0, 0, '강인한 태양의 드라코 (I)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1700, 0, 3371, 38, 0, 0, '강인한 태양의 드라코 (I)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1701, 0, 3371, 72, 0, 0, '강인한 태양의 드라코 (I)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1702, 0, 3371, 93, 0, 0, '강인한 태양의 드라코 (I)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1703, 0, 3371, 94, 0, 0, '강인한 태양의 드라코 (I)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1704, 0, 3371, 98, 0, 0, '강인한 태양의 드라코 (I)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1705, 0, 3371, 119, 0, 0, '강인한 태양의 드라코 (I)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1706, 0, 3371, 125, 0, 0, '강인한 태양의 드라코 (I)일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1707, 0, 3371, 155, 0, 0, '강인한 태양의 드라코 (I)일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1708, 0, 3371, 167, 0, 0, '강인한 태양의 드라코 (I)일 때 변신레비  종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1709, 0, 3371, 207, 0, 0, '강인한 태양의 드라코 (I)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1710, 0, 3371, 267, 0, 0, '강인한 태양의 드라코 (I)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1711, 0, 3373, 12, 0, 0, '강인한 태양의 드라코 (II)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1712, 0, 3373, 13, 0, 0, '강인한 태양의 드라코 (II)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1713, 0, 3373, 38, 0, 0, '강인한 태양의 드라코 (II)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1714, 0, 3373, 72, 0, 0, '강인한 태양의 드라코 (II)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1715, 0, 3373, 93, 0, 0, '강인한 태양의 드라코 (II)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1716, 0, 3373, 94, 0, 0, '강인한 태양의 드라코 (II)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1717, 0, 3373, 98, 0, 0, '강인한 태양의 드라코 (II)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1718, 0, 3373, 119, 0, 0, '강인한 태양의 드라코 (II)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1719, 0, 3373, 125, 0, 0, '강인한 태양의 드라코 (II)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1720, 0, 3373, 155, 0, 0, '강인한 태양의 드라코 (II)일 때 리플렉션오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1721, 0, 3373, 167, 0, 0, '강인한 태양의 드라코 (II)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1722, 0, 3373, 207, 0, 0, '강인한 태양의 드라코 (II)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1723, 0, 3373, 267, 0, 0, '강인한 태양의 드라코 (II)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1724, 0, 3375, 12, 0, 0, '강인한 바람의 드라코 (I)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1725, 0, 3375, 13, 0, 0, '강인한 바람의 드라코 (I)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1726, 0, 3375, 38, 0, 0, '강인한 바람의 드라코 (I)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1727, 0, 3375, 72, 0, 0, '강인한 바람의 드라코 (I)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1728, 0, 3375, 93, 0, 0, '강인한 바람의 드라코 (I)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1729, 0, 3375, 94, 0, 0, '강인한 바람의 드라코 (I)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1730, 0, 3375, 98, 0, 0, '강인한 바람의 드라코 (I)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1731, 0, 3375, 119, 0, 0, '강인한 바람의 드라코 (I)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1732, 0, 3375, 125, 0, 0, '강인한 바람의 드라코 (I)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1733, 0, 3375, 155, 0, 0, '강인한 바람의 드라코 (I)일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1734, 0, 3375, 167, 0, 0, '강인한 바람의 드라코 (I)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1735, 0, 3375, 207, 0, 0, '강인한 바람의 드라코 (I)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1736, 0, 3375, 267, 0, 0, '강인한 바람의 드라코 (I)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1737, 0, 3377, 12, 0, 0, '강인한 바람의 드라코 (II)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1738, 0, 3377, 13, 0, 0, '강인한 바람의 드라코 (II)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1739, 0, 3377, 38, 0, 0, '강인한 바람의 드라코 (II)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1740, 0, 3377, 72, 0, 0, '강인한 바람의 드라코 (II)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1741, 0, 3377, 93, 0, 0, '강인한 바람의 드라코 (II)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1742, 0, 3377, 94, 0, 0, '강인한 바람의 드라코 (II)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1743, 0, 3377, 98, 0, 0, '강인한 바람의 드라코 (II)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1744, 0, 3377, 119, 0, 0, '강인한 바람의 드라코 (II)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1745, 0, 3377, 125, 0, 0, '강인한 바람의 드라코 (II)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1746, 0, 3377, 155, 0, 0, '강인한 바람의 드라코 (II)일 때 리플렉션오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1747, 0, 3377, 167, 0, 0, '강인한 바람의 드라코 (II)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1748, 0, 3377, 207, 0, 0, '강인한 바람의 드라코 (II)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1749, 0, 3377, 267, 0, 0, '강인한 바람의 드라코 (II)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1750, 0, 3379, 12, 0, 0, '맹렬한 대지의 드라코 (I)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1751, 0, 3379, 13, 0, 0, '맹렬한 대지의 드라코 (I)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1752, 0, 3379, 38, 0, 0, '맹렬한 대지의 드라코 (I)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1753, 0, 3379, 72, 0, 0, '맹렬한 대지의 드라코 (I)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1754, 0, 3379, 93, 0, 0, '맹렬한 대지의 드라코 (I)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1755, 0, 3379, 94, 0, 0, '맹렬한 대지의 드라코 (I)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1756, 0, 3379, 98, 0, 0, '맹렬한 대지의 드라코 (I)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1757, 0, 3379, 119, 0, 0, '맹렬한 대지의 드라코 (I)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1758, 0, 3379, 125, 0, 0, '맹렬한 대지의 드라코 (I)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1759, 0, 3379, 155, 0, 0, '맹렬한 대지의 드라코 (I)일 때 리플렉션오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1760, 0, 3379, 167, 0, 0, '맹렬한 대지의 드라코 (I)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1761, 0, 3379, 207, 0, 0, '맹렬한 대지의 드라코 (I)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1762, 0, 3379, 267, 0, 0, '맹렬한 대지의 드라코 (I)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1763, 0, 3381, 12, 0, 0, '맹렬한 대지의 드라코 (II)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1764, 0, 3381, 13, 0, 0, '맹렬한 대지의 드라코 (II)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1765, 0, 3381, 38, 0, 0, '맹렬한 대지의 드라코 (II)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1766, 0, 3381, 72, 0, 0, '맹렬한 대지의 드라코 (II)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1767, 0, 3381, 93, 0, 0, '맹렬한 대지의 드라코 (II)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1768, 0, 3381, 94, 0, 0, '맹렬한 대지의 드라코 (II)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1769, 0, 3381, 98, 0, 0, '맹렬한 대지의 드라코 (II)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1770, 0, 3381, 119, 0, 0, '맹렬한 대지의 드라코 (II)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1771, 0, 3381, 125, 0, 0, '맹렬한 대지의 드라코 (II)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1772, 0, 3381, 155, 0, 0, '맹렬한 대지의 드라코 (II)일 때 리플렉션오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1773, 0, 3381, 167, 0, 0, '맹렬한 대지의 드라코 (II)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1774, 0, 3381, 207, 0, 0, '맹렬한 대지의 드라코 (II)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1775, 0, 3381, 267, 0, 0, '맹렬한 대지의 드라코 (II)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1776, 0, 3383, 12, 0, 0, '맹렬한 태양의 드라코 (I)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1777, 0, 3383, 13, 0, 0, '맹렬한 태양의 드라코 (I)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1778, 0, 3383, 38, 0, 0, '맹렬한 태양의 드라코 (I)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1779, 0, 3383, 72, 0, 0, '맹렬한 태양의 드라코 (I)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1780, 0, 3383, 93, 0, 0, '맹렬한 태양의 드라코 (I)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1781, 0, 3383, 94, 0, 0, '맹렬한 태양의 드라코 (I)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1782, 0, 3383, 98, 0, 0, '맹렬한 태양의 드라코 (I)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1783, 0, 3383, 119, 0, 0, '맹렬한 태양의 드라코 (I)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1784, 0, 3383, 125, 0, 0, '맹렬한 태양의 드라코 (I)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1785, 0, 3383, 155, 0, 0, '맹렬한 태양의 드라코 (I)일 때 리플렉션오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1786, 0, 3383, 167, 0, 0, '맹렬한 태양의 드라코 (I)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1787, 0, 3383, 207, 0, 0, '맹렬한 태양의 드라코 (I)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1788, 0, 3383, 267, 0, 0, '맹렬한 태양의 드라코 (I)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1789, 0, 3385, 12, 0, 0, '맹렬한 태양의 드라코 (II)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1790, 0, 3385, 13, 0, 0, '맹렬한 태양의 드라코 (II)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1791, 0, 3385, 38, 0, 0, '맹렬한 태양의 드라코 (II)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1792, 0, 3385, 93, 0, 0, '맹렬한 태양의 드라코 (II)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1793, 0, 3385, 94, 0, 0, '맹렬한 태양의 드라코 (II)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1794, 0, 3385, 98, 0, 0, '맹렬한 태양의 드라코 (II)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1795, 0, 3385, 119, 0, 0, '맹렬한 태양의 드라코 (II)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1796, 0, 3385, 125, 0, 0, '맹렬한 태양의 드라코 (II)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1797, 0, 3385, 155, 0, 0, '맹렬한 태양의 드라코 (II)일 때 리플렉션오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1798, 0, 3385, 167, 0, 0, '맹렬한 태양의 드라코 (II)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1799, 0, 3385, 72, 0, 0, '맹렬한 태양의 드라코 (II)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1800, 0, 3385, 207, 0, 0, '맹렬한 태양의 드라코 (II)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1801, 0, 3385, 267, 0, 0, '맹렬한 태양의 드라코 (II)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1802, 0, 3387, 12, 0, 0, '맹렬한 바람의 드라코 (I)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1803, 0, 3387, 13, 0, 0, '맹렬한 바람의 드라코 (I)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1804, 0, 3387, 38, 0, 0, '맹렬한 바람의 드라코 (I)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1805, 0, 3387, 72, 0, 0, '맹렬한 바람의 드라코 (I)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1806, 0, 3387, 93, 0, 0, '맹렬한 바람의 드라코 (I)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1807, 0, 3387, 94, 0, 0, '맹렬한 바람의 드라코 (I)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1808, 0, 3387, 98, 0, 0, '맹렬한 바람의 드라코 (I)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1809, 0, 3387, 119, 0, 0, '맹렬한 바람의 드라코 (I)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1810, 0, 3387, 125, 0, 0, '맹렬한 바람의 드라코 (I)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1811, 0, 3387, 155, 0, 0, '맹렬한 바람의 드라코 (I)일 때 리플렉션오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1812, 0, 3387, 167, 0, 0, '맹렬한 바람의 드라코 (I)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1813, 0, 3387, 207, 0, 0, '맹렬한 바람의 드라코 (I)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1814, 0, 3387, 267, 0, 0, '맹렬한 바람의 드라코 (I)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1815, 0, 3389, 12, 0, 0, '맹렬한 바람의 드라코 (II)일 때 공격속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1816, 0, 3389, 13, 0, 0, '맹렬한 바람의 드라코 (II)일 때 이동속도업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1817, 0, 3389, 38, 0, 0, '맹렬한 바람의 드라코 (II)일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1818, 0, 3389, 72, 0, 0, '맹렬한 바람의 드라코 (II)일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1819, 0, 3389, 93, 0, 0, '맹렬한 바람의 드라코 (II)일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1820, 0, 3389, 94, 0, 0, '맹렬한 바람의 드라코 (II)일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1821, 0, 3389, 98, 0, 0, '맹렬한 바람의 드라코 (II)일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1822, 0, 3389, 119, 0, 0, '맹렬한 바람의 드라코 (II)일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1823, 0, 3389, 125, 0, 0, '맹렬한 바람의 드라코 (II)일 때 타임디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1824, 0, 3389, 155, 0, 0, '맹렬한 바람의 드라코 (II)일 때 리플렉션오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1825, 0, 3389, 167, 0, 0, '맹렬한 바람의 드라코 (II)일 때 변신레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1826, 0, 3389, 207, 0, 0, '맹렬한 바람의 드라코 (II)일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1827, 0, 3389, 267, 0, 0, '맹렬한 바람의 드라코 (II)일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1831, 1, 3217, 330, 0, 0, '포션 회복 증가가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1832, 1, 3218, 330, 0, 0, '포션 회복 증가가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1833, 1, 3219, 333, 0, 0, '친밀도 근거리 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1834, 1, 3220, 334, 0, 0, '친밀도 원거리 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1835, 1, 3221, 335, 0, 0, '친밀도 마법 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1836, 1, 3222, 336, 0, 0, '친밀도 크리티컬 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1837, 1, 3223, 337, 0, 0, '친밀도 크리대미지 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1838, 1, 3224, 338, 0, 0, '친밀도 대미지 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1839, 1, 3225, 339, 0, 0, '친밀도 마나소모감소 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1840, 1, 3226, 340, 0, 0, '친밀도 마법 명중 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1841, 1, 3227, 341, 0, 0, '친밀도 이펙트 추가가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1842, 1, 3228, 342, 0, 0, '친밀도 경험치 증가가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1843, 1, 3229, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1844, 1, 3230, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1845, 1, 3231, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1846, 1, 3348, 333, 0, 0, '친밀도 근거리 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1847, 1, 3349, 334, 0, 0, '친밀도 원거리 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1848, 1, 3350, 335, 0, 0, '친밀도 마법 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1849, 1, 3351, 330, 0, 0, '친밀도 포션 회복이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1850, 1, 3352, 336, 0, 0, '친밀도 크리티컬 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1851, 1, 3353, 337, 0, 0, '친밀도 크리대미지 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1852, 1, 3354, 338, 0, 0, '친밀도 대미지 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1853, 1, 3355, 333, 0, 0, '친밀도 근거리 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1854, 1, 3356, 334, 0, 0, '친밀도 원거리 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1855, 1, 3357, 335, 0, 0, '친밀도 마법 피해 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1856, 1, 3358, 330, 0, 0, '친밀도 포션 회복이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1857, 1, 3359, 336, 0, 0, '친밀도 크리티컬 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1858, 1, 3360, 337, 0, 0, '친밀도 크리대미지 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1859, 1, 3361, 338, 0, 0, '친밀도 대미지 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1860, 1, 3362, 340, 0, 0, '친밀도 마법 명중 증가 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1861, 1, 3363, 339, 0, 0, '친밀도 마나 소모 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1862, 1, 3364, 340, 0, 0, '친밀도 마법 명중 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1863, 1, 3365, 339, 0, 0, '친밀도 마나 소모 감소가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1864, 1, 3366, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1865, 1, 3368, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1866, 1, 3370, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1867, 1, 3372, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1868, 1, 3374, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1869, 1, 3376, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1870, 1, 3378, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1871, 1, 3380, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1872, 1, 3382, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1873, 1, 3384, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1874, 1, 3386, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1875, 1, 3388, 343, 0, 0, '친밀도 드라코 외형 변경이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1876, 1, 3390, 342, 0, 0, '친밀도 경험치 증가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1877, 1, 3391, 341, 0, 0, '친밀도 이펙트 추가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1878, 1, 3402, 341, 0, 0, '친밀도 이펙트 추가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1879, 1, 3232, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1880, 1, 3233, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1881, 1, 3234, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1882, 1, 3367, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1883, 1, 3369, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1884, 1, 3371, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1885, 1, 3373, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1886, 1, 3375, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1887, 1, 3377, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1888, 1, 3379, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1889, 1, 3381, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1890, 1, 3383, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1891, 1, 3385, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1892, 1, 3387, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1893, 1, 3389, 343, 0, 0, '서번트 드라코 탑승 중이 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1894, 0, 3440, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1895, 1, 3440, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1896, 0, 3440, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1897, 0, 3441, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1898, 1, 3441, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1899, 0, 3441, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1900, 1, 3404, 342, 0, 0, '친밀도 경험치 증가가 꺼질 때 가변 가중치 상태이상을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1901, 0, 3213, 318, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1902, 1, 3213, 318, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1903, 1, 3447, 167, 0, 0, '크로노스 (3447)변신이 될 때 변신 레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1904, 0, 3447, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1905, 0, 3447, 119, 0, 0, '크로노스 (3447)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1906, 1, 3448, 167, 0, 0, '크로노스 아처 (3448)변신이 될 때 변신 레비를 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1907, 0, 3448, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1908, 0, 3448, 119, 0, 0, '크로노스 아처 (3448)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1909, 0, 3480, 119, 0, 0, '알케미스트(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1910, 1, 3480, 167, 0, 0, '알케미스트(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1911, 0, 3480, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1912, 0, 3481, 119, 0, 0, '닌자(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1913, 1, 3481, 167, 0, 0, '닌자(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1914, 0, 3481, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1915, 0, 3482, 119, 0, 0, '바드(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1916, 1, 3482, 167, 0, 0, '바드(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1917, 0, 3482, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1918, 0, 3483, 119, 0, 0, '나이트 오브 퀸(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1919, 1, 3483, 167, 0, 0, '나이트 오브 퀸(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1920, 0, 3483, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1921, 0, 3484, 119, 0, 0, '여해적 선장(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1922, 1, 3484, 167, 0, 0, '여해적 선장(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1923, 0, 3484, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1924, 0, 3485, 119, 0, 0, '몽크(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1925, 1, 3485, 167, 0, 0, '몽크(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1926, 0, 3485, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1927, 0, 3486, 119, 0, 0, '아수라(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1928, 1, 3486, 167, 0, 0, '아수라(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1929, 0, 3486, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1930, 0, 3487, 119, 0, 0, '헤라클레스(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1931, 1, 3487, 167, 0, 0, '헤라클레스(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1932, 0, 3487, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1933, 0, 3488, 119, 0, 0, '버서커(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1934, 1, 3488, 167, 0, 0, '버서커(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1935, 0, 3488, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1936, 0, 3489, 119, 0, 0, '헌터 리바인저(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1937, 1, 3489, 167, 0, 0, '헌터 리바인저(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1938, 0, 3489, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1939, 0, 3490, 119, 0, 0, '스나이퍼 빅스웰(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1940, 1, 3490, 167, 0, 0, '스나이퍼 빅스웰(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1941, 0, 3490, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1942, 0, 3491, 119, 0, 0, '미드나잇 에르메스(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1943, 1, 3491, 167, 0, 0, '미드나잇 에르메스(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1944, 0, 3491, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1945, 0, 3492, 119, 0, 0, '데빌리스(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1946, 1, 3492, 167, 0, 0, '데빌리스(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1947, 0, 3492, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1948, 0, 3493, 119, 0, 0, '엔젤리스(II)변신이 될 때 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1949, 1, 3493, 167, 0, 0, '엔젤리스(II)변신이 될 때 변신용 레비테이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1950, 0, 3493, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1951, 0, 3578, 12, 0, 0, '황야의 드라코일 때 공격속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1952, 0, 3578, 13, 0, 0, '황야의 드라코일 때 이동속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1953, 0, 3578, 38, 0, 0, '황야의 드라코일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1954, 0, 3578, 72, 0, 0, '황야의 드라코일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1955, 0, 3578, 93, 0, 0, '황야의 드라코일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1956, 0, 3578, 94, 0, 0, '황야의 드라코일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1957, 0, 3578, 98, 0, 0, '황야의 드라코일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1958, 0, 3578, 119, 0, 0, '황야의 드라코일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1959, 0, 3578, 125, 0, 0, '황야의 드라코일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1960, 0, 3578, 155, 0, 0, '황야의 드라코일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1961, 0, 3578, 167, 0, 0, '황야의 드라코일 때 변신용 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1962, 0, 3578, 207, 0, 0, '황야의 드라코일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1963, 0, 3578, 267, 0, 0, '황야의 드라코일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1964, 0, 3579, 12, 0, 0, '평야의 드라코일 때 공격속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1965, 0, 3579, 13, 0, 0, '평야의 드라코일 때 이동속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1966, 0, 3579, 38, 0, 0, '평야의 드라코일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1967, 0, 3579, 72, 0, 0, '평야의 드라코일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1968, 0, 3579, 93, 0, 0, '평야의 드라코일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1969, 0, 3579, 94, 0, 0, '평야의 드라코일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1970, 0, 3579, 98, 0, 0, '평야의 드라코일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1971, 0, 3579, 119, 0, 0, '평야의 드라코일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1972, 0, 3579, 125, 0, 0, '평야의 드라코일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1973, 0, 3579, 155, 0, 0, '평야의 드라코일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1974, 0, 3579, 167, 0, 0, '평야의 드라코일 때 변신용 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1975, 0, 3579, 207, 0, 0, '평야의 드라코일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1976, 0, 3579, 267, 0, 0, '평야의 드라코일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1977, 0, 3584, 12, 0, 0, '평야의 드라코일 때 공격속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1978, 0, 3584, 13, 0, 0, '평야의 드라코일 때 이동속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1979, 0, 3584, 38, 0, 0, '홍염의 드라코일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1980, 0, 3584, 72, 0, 0, '홍염의 드라코일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1981, 0, 3584, 93, 0, 0, '홍염의 드라코일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1982, 0, 3584, 94, 0, 0, '홍염의 드라코일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1983, 0, 3584, 98, 0, 0, '홍염의 드라코일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1984, 0, 3584, 119, 0, 0, '홍염의 드라코일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1985, 0, 3584, 125, 0, 0, '홍염의 드라코일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1986, 0, 3584, 155, 0, 0, '홍염의 드라코일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1987, 0, 3584, 167, 0, 0, '홍염의 드라코일 때 변신용 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1988, 0, 3584, 207, 0, 0, '홍염의 드라코일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1989, 0, 3584, 267, 0, 0, '홍염의 드라코일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1990, 0, 3585, 12, 0, 0, '백광의 드라코일 때 공격속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1991, 0, 3585, 13, 0, 0, '백광의 드라코일 때 이동속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1992, 0, 3585, 38, 0, 0, '백광의 드라코일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1993, 0, 3585, 72, 0, 0, '백광의 드라코일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1994, 0, 3585, 93, 0, 0, '백광의 드라코일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1995, 0, 3585, 94, 0, 0, '백광의 드라코일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1996, 0, 3585, 98, 0, 0, '백광의 드라코일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1997, 0, 3585, 119, 0, 0, '백광의 드라코일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1998, 0, 3585, 125, 0, 0, '백광의 드라코일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (1999, 0, 3585, 155, 0, 0, '백광의 드라코일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2000, 0, 3585, 167, 0, 0, '백광의 드라코일 때 변신용 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2001, 0, 3585, 207, 0, 0, '백광의 드라코일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2002, 0, 3585, 267, 0, 0, '백광의 드라코일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2003, 0, 3590, 12, 0, 0, '질풍의 드라코일 때 공격속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2004, 0, 3590, 13, 0, 0, '질풍의 드라코일 때 이동속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2005, 0, 3590, 38, 0, 0, '질풍의 드라코일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2006, 0, 3590, 72, 0, 0, '질풍의 드라코일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2007, 0, 3590, 93, 0, 0, '질풍의 드라코일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2008, 0, 3590, 94, 0, 0, '질풍의 드라코일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2009, 0, 3590, 98, 0, 0, '질풍의 드라코일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2010, 0, 3590, 119, 0, 0, '질풍의 드라코일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2011, 0, 3590, 125, 0, 0, '질풍의 드라코일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2012, 0, 3590, 155, 0, 0, '질풍의 드라코일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2013, 0, 3590, 167, 0, 0, '질풍의 드라코일 때 변신용 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2014, 0, 3590, 207, 0, 0, '질풍의 드라코일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2015, 0, 3590, 267, 0, 0, '질풍의 드라코일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2016, 0, 3591, 12, 0, 0, '돌풍의 드라코일 때 공격속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2017, 0, 3591, 13, 0, 0, '돌풍의 드라코일 때 이동속도 업 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2018, 0, 3591, 38, 0, 0, '돌풍의 드라코일 때 투명상태 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2019, 0, 3591, 72, 0, 0, '돌풍의 드라코일 때 버서크 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2020, 0, 3591, 93, 0, 0, '돌풍의 드라코일 때 스캠퍼 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2021, 0, 3591, 94, 0, 0, '돌풍의 드라코일 때 인비지빌리티 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2022, 0, 3591, 98, 0, 0, '돌풍의 드라코일 때 어쌔시네이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2023, 0, 3591, 119, 0, 0, '돌풍의 드라코일 때 레비테이션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2024, 0, 3591, 125, 0, 0, '돌풍의 드라코일 때 타임 디스토션 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2025, 0, 3591, 155, 0, 0, '돌풍의 드라코일 때 리플렉션 오오라 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2026, 0, 3591, 167, 0, 0, '돌풍의 드라코일 때 변신용 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2027, 0, 3591, 207, 0, 0, '돌풍의 드라코일 때 퓨인즈 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2028, 0, 3591, 267, 0, 0, '돌풍의 드라코일 때 질주 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2029, 1, 3552, 333, 0, 0, '근거리 공격 피해 감소가 꺼질 때 가변 근거리 공격 피해 감소 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2030, 1, 3554, 334, 0, 0, '원거리 공격 피해 감소가 꺼질 때 가변 원거리 공격 피해 감소 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2031, 1, 3556, 335, 0, 0, '마법 공격 피해 감소가 꺼질 때 가변 마법 공격 피해 감소 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2032, 1, 3558, 330, 0, 0, '포션 회복이 꺼질 때 가변 포션 회복 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2033, 1, 3560, 336, 0, 0, '친밀도 크리증가 꺼질 때 가변 크리증가 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2034, 1, 3562, 337, 0, 0, '친밀도 크리대미지 증가 꺼질 때 가변 크리대미지 증가 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2035, 1, 3564, 338, 0, 0, '친밀도 대미지 감소 증가 꺼질 때 가변 친밀도 대미지 감소 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2036, 1, 3566, 340, 0, 0, '친밀도 마법 명중 꺼질 때 가변 마법 명중 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2037, 1, 3568, 339, 0, 0, '친밀도 마나소모 감소 꺼질 때 가변 마나소모 감소 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2038, 1, 3570, 342, 0, 0, '친밀도 경험치 증가 꺼질 때 가변 경험치 증가 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2039, 1, 3572, 341, 0, 0, '친밀도 이펙트 증가 꺼질 때 가변 이펙트 증가 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2040, 1, 3574, 343, 0, 0, '친밀도 드라코 외형 변경 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2041, 1, 3576, 343, 0, 0, '친밀도 드라코 외형 변경 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2042, 1, 3580, 343, 0, 0, '친밀도 드라코 외형 변경 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2043, 1, 3582, 343, 0, 0, '친밀도 드라코 외형 변경 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2044, 1, 3586, 343, 0, 0, '친밀도 드라코 외형 변경 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2045, 1, 3588, 343, 0, 0, '친밀도 드라코 외형 변경 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2046, 1, 3578, 343, 0, 0, '드라코 탑승 중 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2047, 1, 3579, 343, 0, 0, '드라코 탑승 중 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2048, 1, 3584, 343, 0, 0, '드라코 탑승 중 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2049, 1, 3585, 343, 0, 0, '드라코 탑승 중 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2050, 1, 3590, 343, 0, 0, '드라코 탑승 중 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2051, 1, 3591, 343, 0, 0, '드라코 탑승 중 꺼질 때 가변 드라코 외형 변경 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2052, 1, 3599, 167, 0, 0, '화염의 이그리트 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2053, 0, 3599, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2054, 0, 3599, 119, 0, 0, '화염의 이그리트 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2055, 1, 3601, 167, 0, 0, '화염의 이그리트 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2056, 0, 3601, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2057, 0, 3601, 119, 0, 0, '화염의 이그리트 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2058, 1, 3603, 167, 0, 0, '화마의 이그리트 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2059, 0, 3603, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2060, 0, 3603, 119, 0, 0, '화마의 이그리트 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2061, 1, 3605, 167, 0, 0, '화마의 이그리트 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2062, 0, 3605, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2063, 0, 3605, 119, 0, 0, '화마의 이그리트 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2064, 1, 3607, 167, 0, 0, '심해의 레비아탄 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2065, 0, 3607, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2066, 0, 3607, 119, 0, 0, '심해의 레비아탄 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2067, 1, 3609, 167, 0, 0, '심해의 레비아탄 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2068, 0, 3609, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2069, 0, 3609, 119, 0, 0, '심해의 레비아탄 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2070, 1, 3611, 167, 0, 0, '천해의 레비아탄 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2071, 0, 3611, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2072, 0, 3611, 119, 0, 0, '천해의 레비아탄 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2073, 1, 3613, 167, 0, 0, '천해의 레비아탄 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2074, 0, 3613, 119, 0, 0, '천해의 레비아탄 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2075, 0, 3613, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2076, 1, 3615, 167, 0, 0, '황야의 테이아 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2077, 0, 3615, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2078, 0, 3615, 119, 0, 0, '황야의 테이아 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2079, 1, 3617, 167, 0, 0, '황야의 테이아 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2080, 0, 3617, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2081, 0, 3617, 119, 0, 0, '황야의 테이아 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2082, 1, 3619, 167, 0, 0, '평야의 테이아 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2083, 0, 3619, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2084, 0, 3619, 119, 0, 0, '평야의 테이아 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2085, 1, 3621, 167, 0, 0, '평야의 테이아 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2086, 0, 3621, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2087, 0, 3621, 119, 0, 0, '평야의 테이아 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2088, 1, 3623, 167, 0, 0, '월광의 칼리고 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2089, 0, 3623, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2090, 0, 3623, 119, 0, 0, '월광의 칼리고 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2091, 1, 3625, 167, 0, 0, '월광의 칼리고 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2092, 0, 3625, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2093, 0, 3625, 119, 0, 0, '월광의 칼리고 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2094, 1, 3627, 167, 0, 0, '현월의 칼리고 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2095, 0, 3627, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2096, 0, 3627, 119, 0, 0, '현월의 칼리고 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2097, 1, 3629, 167, 0, 0, '현월의 칼리고 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2098, 0, 3629, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2099, 0, 3629, 119, 0, 0, '현월의 칼리고 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2100, 1, 3631, 167, 0, 0, '홍염의 루멘 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2101, 0, 3631, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2102, 0, 3631, 119, 0, 0, '홍염의 루멘 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2103, 1, 3633, 167, 0, 0, '홍염의 루멘 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2104, 0, 3633, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2105, 0, 3633, 119, 0, 0, '홍염의 루멘 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2106, 1, 3635, 167, 0, 0, '백광의 루멘 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2107, 0, 3635, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2108, 0, 3635, 119, 0, 0, '백광의 루멘 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2109, 1, 3637, 167, 0, 0, '백광의 루멘 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2110, 0, 3637, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2111, 0, 3637, 119, 0, 0, '백광의 루멘 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2112, 1, 3639, 167, 0, 0, '돌풍의 웬투스 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2113, 0, 3639, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2114, 0, 3639, 119, 0, 0, '돌풍의 웬투스 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2115, 1, 3641, 167, 0, 0, '돌풍의 웬투스 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2116, 0, 3641, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2117, 0, 3641, 119, 0, 0, '돌풍의 웬투스 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2118, 1, 3643, 167, 0, 0, '질풍의 웬투스 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2119, 0, 3643, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2120, 0, 3643, 119, 0, 0, '질풍의 웬투스 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2121, 1, 3645, 167, 0, 0, '질풍의 웬투스 아처 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2122, 0, 3645, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2123, 0, 3645, 119, 0, 0, '질풍의 웬투스 아처 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2130, 1, 3650, 167, 0, 0, '광휘의 엘윈더 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2131, 0, 3650, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2132, 0, 3650, 119, 0, 0, '광휘의 엘윈더 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2133, 1, 3651, 167, 0, 0, '광휘의 엘로라 변신 시 변신 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2134, 0, 3651, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2135, 0, 3651, 119, 0, 0, '광휘의 엘로라 변신 시 레비 종료', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2136, 1, 3599, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2137, 1, 3601, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2138, 1, 3603, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2139, 1, 3605, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2140, 1, 3607, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2141, 1, 3609, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2142, 1, 3611, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2143, 1, 3613, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2144, 1, 3615, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2145, 1, 3617, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2146, 1, 3619, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2147, 1, 3621, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2148, 1, 3623, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2149, 1, 3625, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2150, 1, 3627, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2151, 1, 3629, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2152, 1, 3631, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2153, 1, 3633, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2154, 1, 3635, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2155, 1, 3637, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2156, 1, 3639, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2157, 1, 3641, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2158, 1, 3643, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2159, 1, 3645, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2160, 1, 3654, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2161, 0, 3654, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2162, 0, 3654, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2163, 1, 3655, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2164, 0, 3655, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2165, 0, 3655, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2166, 1, 3656, 167, 0, 0, '아폴론(3656)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2167, 0, 3656, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2168, 0, 3656, 119, 0, 0, '아폴론(3656)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2169, 1, 3657, 167, 0, 0, '아르테미스(3657)변신이 될 때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2170, 0, 3657, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2171, 0, 3657, 119, 0, 0, '아르테미스(3657)변신이 될 때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2172, 1, 3243, 98, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2173, 1, 3243, 312, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션(매의눈)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2174, 1, 3244, 98, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2175, 1, 3244, 312, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션(매의눈)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2176, 1, 3245, 98, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2177, 1, 3245, 312, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션(매의눈)을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2178, 1, 3246, 98, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2179, 1, 3246, 312, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션(매의눈)을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2180, 1, 3247, 98, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션을 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2181, 1, 3247, 312, 0, 0, '인비지(안식)이 꺼질때 어쌔시네이션(매의눈)을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2182, 0, 3140, 167, 0, 0, '레비테이션-회피를 사용될때 변신 레비테이션 해제', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2183, 0, 3680, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2184, 0, 3680, 119, 0, 0, '암살단 암흑(3680)변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2185, 1, 3680, 167, 0, 0, '암살단 암흑(3680)변신이 될때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2186, 0, 3681, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2187, 0, 3681, 119, 0, 0, '암살단 칠흑(3681)변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2188, 1, 3681, 167, 0, 0, '암살단 칠흑(3681)변신이 될때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2189, 0, 3686, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2190, 0, 3686, 119, 0, 0, '암살단 암흑스크롤(3686)변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2191, 1, 3686, 167, 0, 0, '암살단 암흑스크롤(3686)변신이 될때 변신레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2192, 0, 3687, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2193, 0, 3687, 119, 0, 0, '암살단 칠흑스크롤(3687)변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2194, 1, 3687, 167, 0, 0, '암살단 칠흑스크롤(3687)변신이 될때 변신레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2195, 1, 3713, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2196, 0, 3714, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2197, 0, 3715, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2198, 0, 3716, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2199, 0, 3717, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2200, 0, 3718, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2201, 0, 3719, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2202, 0, 3720, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2203, 0, 3721, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2204, 0, 3722, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2205, 0, 3723, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2206, 0, 3724, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2207, 0, 3725, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2208, 0, 3726, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2209, 0, 3727, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2210, 0, 3728, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2211, 0, 3729, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2212, 0, 3730, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2213, 0, 3731, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2214, 0, 3732, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2215, 0, 3733, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2216, 0, 3734, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2217, 0, 3735, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2218, 0, 3736, 167, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2219, 0, 3713, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2220, 0, 3714, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2221, 0, 3715, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2222, 0, 3716, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2223, 0, 3717, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2224, 0, 3718, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2225, 0, 3719, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2226, 0, 3720, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2227, 0, 3721, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2228, 0, 3722, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2229, 0, 3723, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2230, 0, 3724, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2231, 0, 3725, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2232, 0, 3726, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2233, 0, 3727, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2234, 0, 3728, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2235, 0, 3729, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2236, 0, 3730, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2237, 0, 3731, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2238, 0, 3732, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2239, 0, 3733, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2240, 0, 3734, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2241, 0, 3735, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2242, 0, 3736, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2243, 0, 3713, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2244, 0, 3714, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2245, 0, 3715, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2246, 0, 3716, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2247, 0, 3717, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2248, 0, 3718, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2249, 0, 3719, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2250, 0, 3720, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2251, 0, 3721, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2252, 0, 3722, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2253, 0, 3723, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2254, 0, 3724, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2255, 0, 3725, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2256, 0, 3726, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2257, 0, 3727, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2258, 0, 3728, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2259, 0, 3729, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2260, 0, 3730, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2261, 0, 3731, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2262, 0, 3732, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2263, 0, 3733, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2264, 0, 3734, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2265, 0, 3735, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2266, 0, 3736, 119, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2267, 0, 3713, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2268, 0, 3714, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2269, 0, 3715, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2270, 0, 3716, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2271, 0, 3717, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2272, 0, 3718, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2273, 0, 3719, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2274, 0, 3720, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2275, 0, 3721, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2276, 0, 3722, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2277, 0, 3723, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2278, 0, 3724, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2279, 0, 3725, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2280, 0, 3726, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2281, 0, 3727, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2282, 0, 3728, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2283, 0, 3729, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2284, 0, 3730, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2285, 0, 3731, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2286, 0, 3732, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2287, 0, 3733, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2288, 0, 3734, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2289, 0, 3735, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2290, 0, 3736, 358, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2291, 0, 3809, 119, 0, 0, 'GM전용 변신 완드', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2292, 1, 3809, 167, 0, 0, 'GM전용 변신 완드가 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2293, 0, 3809, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2294, 0, 3813, 119, 0, 0, '어드민툴 전용 콜로복 변신', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2295, 1, 3813, 167, 0, 0, '어드민툴 전용 콜로복 변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2296, 0, 3813, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2297, 0, 3814, 119, 0, 0, '어드민툴 전용 바바야거 변신', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2298, 1, 3814, 167, 0, 0, '어드민툴 전용 바바야거 변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2299, 0, 3814, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2300, 0, 3815, 119, 0, 0, '어드민툴 전용 곰인형 변신', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2301, 1, 3815, 167, 0, 0, '어드민툴 전용 곰인형 변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2302, 0, 3815, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2303, 0, 3816, 119, 0, 0, '어드민툴 전용 선인장 변신', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2304, 1, 3816, 167, 0, 0, '어드민툴 전용 선인장 변신이 풀릴 때 변신레비를 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2305, 0, 3816, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (2306, 0, 3113, 207, 0, 0, '타임디스토션 차원의지배자 걸릴 때 퓨인즈 끈다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107236, 0, 107234, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107237, 0, 107234, 119, 0, 0, '서번트 근거리 데빌 변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107238, 1, 107234, 167, 0, 0, '서번트 근거리 데빌 변신이 될때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107239, 0, 107235, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107240, 0, 107235, 119, 0, 0, '서번트 원거리 홀리 변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107241, 1, 107235, 167, 0, 0, '서번트 원거리 홀리 변신이 될때 변신 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107261, 0, 107259, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107262, 0, 107259, 119, 0, 0, '암살단 암흑 변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107263, 1, 107259, 167, 0, 0, '암살단 암흑 변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107264, 0, 107260, 291, 0, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107265, 0, 107260, 119, 0, 0, '암살단 칠흑 변신이 될때 레비테이션을 끈다', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrDetach] ([mID], [mAppType], [mOriginAID], [mEffectAType], [mConditionType], [mConditionAType], [mDesc], [mEffectAID]) VALUES (107266, 1, 107260, 167, 0, 0, '암살단 칠흑 변신이 될때 레비테이션을 끈다', 0);
GO

