/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMaterialEvolutionMaterial
Date                  : 2023-10-07 09:08:51
*/


INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (1, 5, 1, 1, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (2, 5, 1, 2, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (3, 5, 1, 3, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (4, 5, 1, 4, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (5, 5, 2, 1, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (6, 5, 2, 2, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (7, 5, 2, 3, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (8, 5, 2, 4, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (9, 5, 3, 1, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (10, 5, 3, 2, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (11, 5, 3, 3, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (12, 5, 3, 4, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (13, 5, 4, 1, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (14, 5, 4, 2, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (15, 5, 4, 3, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (16, 5, 4, 4, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (17, 2, 1, 1, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (18, 2, 1, 2, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (19, 2, 1, 3, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (20, 2, 1, 4, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (21, 2, 2, 1, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (22, 2, 2, 2, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (23, 2, 2, 3, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (24, 2, 2, 4, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (25, 2, 3, 1, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (26, 2, 3, 2, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (27, 2, 3, 3, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (28, 2, 3, 4, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (29, 2, 4, 1, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (30, 2, 4, 2, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (31, 2, 4, 3, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (32, 2, 4, 4, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (33, 4, 1, 1, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (34, 4, 1, 2, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (35, 4, 1, 3, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (36, 4, 1, 4, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (37, 4, 2, 1, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (38, 4, 2, 2, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (39, 4, 2, 3, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (40, 4, 2, 4, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (41, 4, 3, 1, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (42, 4, 3, 2, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (43, 4, 3, 3, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (44, 4, 3, 4, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (45, 4, 4, 1, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (46, 4, 4, 2, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (47, 4, 4, 3, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (48, 4, 4, 4, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (49, 3, 1, 1, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (50, 3, 1, 2, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (51, 3, 1, 3, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (52, 3, 1, 4, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (53, 3, 2, 1, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (54, 3, 2, 2, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (55, 3, 2, 3, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (56, 3, 2, 4, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (57, 3, 3, 1, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (58, 3, 3, 2, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (59, 3, 3, 3, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (60, 3, 3, 4, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (61, 3, 4, 1, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (62, 3, 4, 2, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (63, 3, 4, 3, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (64, 3, 4, 4, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (65, 1, 1, 1, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (66, 1, 1, 2, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (67, 1, 1, 3, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (68, 1, 1, 4, 50.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (69, 1, 2, 1, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (70, 1, 2, 2, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (71, 1, 2, 3, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (72, 1, 2, 4, 33.33000183105469);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (73, 1, 3, 1, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (74, 1, 3, 2, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (75, 1, 3, 3, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (76, 1, 3, 4, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (77, 1, 4, 1, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (78, 1, 4, 2, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (79, 1, 4, 3, 16.670000076293945);
GO

INSERT INTO [dbo].[TblMaterialEvolutionMaterial] ([MEID], [MType], [MLevel], [MGrade], [mSuccess]) VALUES (80, 1, 4, 4, 16.670000076293945);
GO

