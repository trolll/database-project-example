/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblBeadHoleProb
Date                  : 2023-10-07 09:08:49
*/


INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 1, 1, 100);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 2, 1, 90);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 2, 2, 10);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 3, 1, 80);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 3, 2, 17);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 3, 3, 3);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 4, 1, 64);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 4, 2, 20);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 4, 3, 13);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 4, 4, 3);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 5, 1, 50);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 5, 2, 30);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 5, 3, 10);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 5, 4, 7);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6328, 5, 5, 3);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 1, 1, 100);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 2, 1, 80);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 2, 2, 20);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 3, 1, 72);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 3, 2, 23);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 3, 3, 5);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 4, 1, 47);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 4, 2, 30);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 4, 3, 18);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 4, 4, 5);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 5, 1, 33);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 5, 2, 33);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 5, 3, 20);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 5, 4, 10);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6329, 5, 5, 4);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 1, 1, 100);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 2, 1, 70);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 2, 2, 30);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 3, 1, 65);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 3, 2, 28);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 3, 3, 7);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 4, 1, 35);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 4, 2, 35);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 4, 3, 20);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 4, 4, 10);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 5, 1, 30);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 5, 2, 30);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 5, 3, 22);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 5, 4, 13);
GO

INSERT INTO [dbo].[TblBeadHoleProb] ([IID], [mMaxHoleCount], [mHoleCount], [mProb]) VALUES (6330, 5, 5, 5);
GO

