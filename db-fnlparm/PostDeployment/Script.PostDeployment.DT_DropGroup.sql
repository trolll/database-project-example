/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_DropGroup
Date                  : 2023-10-07 09:09:33
*/


INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3939, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3940, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 678, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (86, 294, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (129, 1384, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (24, 62, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (161, 2312, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (161, 2313, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (118, 671, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (24, 672, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (238, 2445, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3941, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3942, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1110, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (128, 690, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 271, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 708, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (228, 1488, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (2, 508, 86.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 1823, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (161, 2314, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (63, 2315, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 514, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 515, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 516, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 517, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 518, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 519, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 520, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 3603, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 522, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 523, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 484, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1112, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (300, 1827, 88.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 684, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 607, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (86, 277, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2914, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (1, 499, 86.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (63, 2316, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2915, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 524, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 2962, 82.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 526, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (42, 138, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 527, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (44, 528, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (47, 529, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (86, 278, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 1437, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (50, 531, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (201, 1548, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (86, 608, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 1116, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 673, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (300, 1831, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (86, 609, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (86, 610, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (237, 1118, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 534, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 535, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 687, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 1119, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (128, 1385, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (223, 1489, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (300, 1832, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 546, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 1120, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 439, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (54, 166, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (92, 1490, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 578, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 613, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (200, 1121, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (300, 1833, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (238, 1123, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 174, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (73, 617, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 176, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 1124, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 178, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 2968, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 2969, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (158, 2970, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (75, 4025, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (118, 552, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 628, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1125, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 630, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (3, 636, 86.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (4, 637, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2916, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2917, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2918, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2919, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2920, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1126, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (257, 1632, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1127, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1128, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 273, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (228, 2446, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (124, 662, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 202, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (124, 663, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 664, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (118, 553, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 1819, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1130, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 1491, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (125, 670, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 674, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1131, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2921, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 676, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (128, 691, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (129, 692, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (129, 693, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 547, 9.999999747378752e-06);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 3604, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (130, 694, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (130, 695, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 1383, 0.12999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (131, 696, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 224, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (230, 1492, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 2337, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (29, 697, 2.700000047683716);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 709, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (29, 698, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (29, 699, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (237, 1135, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (29, 700, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 494, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 701, 1.7999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 702, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 703, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 704, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 705, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 710, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 711, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 706, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 707, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (358, 2922, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 712, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (29, 551, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 713, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 249, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 714, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 715, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (63, 2317, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (291, 2318, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 1351, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2923, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (300, 1836, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (291, 2319, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (291, 2320, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1145, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1146, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 441, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1147, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 452, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 1148, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (118, 557, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 427, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (89, 301, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (284, 2321, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (84, 309, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 3943, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (230, 2971, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (118, 559, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (118, 560, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 1152, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (92, 315, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 320, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 1153, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 1154, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (138, 1155, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 328, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 2425, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 1157, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 1158, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (78, 317, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 318, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (284, 2322, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (29, 561, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (205, 1367, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 2344, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (223, 1162, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (211, 1163, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 1164, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (167, 1165, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 562, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (156, 2492, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (229, 1167, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (229, 1168, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (206, 1549, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (230, 1493, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1176, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 1177, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 1178, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 1494, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 359, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 1495, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (54, 568, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 574, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 575, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 571, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 1181, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 1182, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 572, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 1183, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 1184, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 1185, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 2875, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 418, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 419, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 2876, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 335, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1354, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1355, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (136, 1356, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (200, 1357, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 1358, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (144, 2482, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (204, 1360, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 343, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 1361, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 2558, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (207, 1363, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 469, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 428, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 1496, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 754, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (24, 816, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (24, 817, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 430, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 2408, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 2411, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1201, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 2409, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 431, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 573, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 2410, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (54, 470, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 436, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 437, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 576, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 2412, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 2413, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 2416, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 2417, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 2418, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 2420, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 2421, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 2422, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 2423, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (110, 1364, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (284, 2323, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (24, 504, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 2424, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 2426, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (75, 1550, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 2404, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 1551, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 2405, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (48, 1552, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (44, 1553, 1.7999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (168, 1556, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 2427, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 1557, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 1558, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1246, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1247, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (282, 2324, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (282, 2325, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 1250, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 1251, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1252, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 1253, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (282, 2326, 1.7000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 1255, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 1256, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (236, 2480, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 2327, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 1259, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 1848, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 1261, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 1262, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 1263, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 1264, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 2479, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 1266, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 1267, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 1268, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 2328, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 1270, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 1271, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 1854, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 1273, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 1274, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 1275, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 1276, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (48, 2484, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 1278, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 1279, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 1280, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 1281, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 1282, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 1283, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (144, 1853, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (145, 2486, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 1286, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 1287, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 1288, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 1289, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (142, 1857, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 2329, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 1292, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 1293, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 1294, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 1295, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (142, 2485, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 1297, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 1298, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 1299, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (48, 1860, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (269, 2333, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 1302, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 1303, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 1304, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 1305, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 1306, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (158, 1365, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 1368, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (191, 1369, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (151, 1497, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (176, 1370, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 887, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 563, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (269, 2331, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (44, 565, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 1371, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (146, 2487, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (162, 1498, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 570, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 888, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (269, 2332, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (75, 633, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 1375, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (202, 1376, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 1377, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 1379, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 1380, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 1381, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 2561, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (146, 1866, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2925, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 1382, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (54, 1544, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 1501, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 1502, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (124, 661, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 3946, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (92, 1503, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (167, 1504, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (182, 1505, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 1506, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 1507, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (180, 1508, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (192, 1509, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 1510, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (222, 1511, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 1512, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 1513, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (185, 1514, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 1515, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 1516, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 1517, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 1518, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (138, 1520, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (201, 1521, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (47, 1554, 1.2999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (176, 1555, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (157, 1559, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 1560, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (110, 1561, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (147, 1562, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 1563, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 1564, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (213, 1565, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (223, 1566, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (206, 1567, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 1568, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 1569, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 1570, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 1571, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (215, 1572, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 1573, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 1574, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 1575, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (311, 2562, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (252, 1577, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (252, 1578, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (252, 1579, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 1580, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1585, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (253, 1582, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (254, 1583, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (255, 1584, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1586, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 1587, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 1588, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 1589, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 1590, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 1591, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 1592, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 1593, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 1594, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (234, 2160, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 1596, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 1597, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 1598, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 2563, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1600, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1601, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (244, 1602, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 1869, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 579, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (59, 1098, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2926, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 582, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (75, 1100, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (50, 665, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 2877, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (44, 666, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 755, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 2974, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 1630, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 590, 0.10999999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1102, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1103, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 593, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 594, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (248, 1390, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 1106, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 1107, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 599, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 1108, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 3500, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 1307, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1308, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (237, 1309, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1310, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 758, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 763, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 760, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 761, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 762, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 764, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 765, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 766, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 767, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 768, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 769, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 770, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 771, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 772, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 2564, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 774, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 775, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 776, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 777, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 794, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 795, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 796, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 801, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 782, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 783, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 784, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 785, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 786, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 787, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 788, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 789, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 790, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 791, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 792, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 2565, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 797, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 798, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 799, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 800, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 802, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 803, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 804, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 805, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 806, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 807, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 808, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 809, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 810, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 811, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 812, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 813, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 814, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 815, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 2927, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 2928, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 2929, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1311, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1628, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1629, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 1341, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1631, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (156, 1872, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 1318, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 1320, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (237, 1324, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (237, 1328, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (248, 1391, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 1342, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (187, 1463, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 1334, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (234, 2162, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 1336, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 2566, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (234, 2163, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 1344, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 1345, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 2930, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (130, 1348, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (131, 1349, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (47, 1392, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (249, 1393, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (250, 1394, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 1395, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (136, 1396, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (200, 1397, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 1398, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (189, 1399, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 1400, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 1401, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 1402, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 1403, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (190, 1404, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (251, 1405, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (143, 1406, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 1407, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 1408, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 1409, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 1410, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (99, 1411, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 1412, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 1413, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (151, 1414, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 1415, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 1416, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 1417, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 1418, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (168, 1419, 0.550000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (78, 1420, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 1421, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (249, 1422, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (250, 1423, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (194, 1424, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 1425, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 1426, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 1427, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 1428, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (191, 1429, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (205, 1430, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 1431, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (234, 1432, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 1433, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 1434, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 1435, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 1436, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 1464, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 2931, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (198, 1466, 0.0020000000949949026);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (195, 1467, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (202, 1468, 0.0020000000949949026);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (237, 2932, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (270, 2438, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (273, 2363, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (206, 1472, 0.004000000189989805);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (203, 1473, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (208, 1474, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (211, 1475, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 1476, 0.3799999952316284);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 2933, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (215, 1478, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (214, 1479, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 2934, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 2935, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (201, 1482, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 1483, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 1484, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 1485, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 2936, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 1487, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (47, 1026, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 890, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 895, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 892, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 893, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 2336, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 896, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (157, 1876, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 1053, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 1050, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 900, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (146, 902, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 2338, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 2339, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 2340, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 2937, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (137, 907, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 2341, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (139, 909, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (140, 910, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (141, 911, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 1877, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 2342, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 2938, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (144, 2037, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (48, 2038, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (149, 918, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (150, 919, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 2343, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (152, 921, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (153, 922, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (154, 923, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (155, 924, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (142, 2039, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (145, 2040, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (146, 2041, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 2042, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (160, 929, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (156, 2043, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 2345, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 2428, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 2346, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 2347, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 3387, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (157, 2044, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (168, 2045, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (75, 2046, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 2348, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (179, 2047, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 2883, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 2349, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 2048, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (167, 2049, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 2050, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (180, 2051, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (181, 2052, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 2053, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (182, 2054, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 2350, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 2351, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 2352, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 2353, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 2055, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 2056, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (190, 2057, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (110, 2058, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 2354, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 2355, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 2356, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 2891, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 2898, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (292, 2357, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (285, 2810, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (292, 2358, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (292, 2359, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (290, 2360, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (290, 2361, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (290, 2362, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 2059, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (273, 2364, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (147, 2060, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (191, 2061, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (192, 2062, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (210, 976, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 2063, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1642, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (207, 2064, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (205, 2065, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (208, 2066, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 2067, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (211, 2068, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (218, 984, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (219, 985, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (273, 2365, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (221, 987, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 2069, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (213, 2070, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 2998, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (214, 2071, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (215, 2072, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 2073, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 2074, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (229, 995, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (274, 2366, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (171, 2180, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (145, 1863, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (311, 2567, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (50, 2181, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1617, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1618, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 2075, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (238, 2076, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 1783, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 2232, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (236, 2182, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (171, 1010, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (73, 1012, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 1013, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (185, 1014, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 1015, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 1016, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (285, 2813, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (187, 1025, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 1027, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3947, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (48, 1030, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (223, 2077, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 1032, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 3607, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2846, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (228, 2078, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 2079, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 2298, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (274, 2367, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 2497, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 1040, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 2403, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 1042, 0.1599999964237213);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (69, 2863, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (274, 2368, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (73, 2203, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 2213, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 2236, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (227, 2080, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (80, 1049, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 1051, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 1052, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (92, 2081, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (93, 1056, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (293, 2389, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (280, 2082, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 1059, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 2083, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (283, 2084, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (99, 1062, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (100, 1063, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 1064, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 1065, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 1066, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 2085, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (222, 2086, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 2294, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 1638, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 2087, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (143, 1073, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (236, 1097, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (144, 1075, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (236, 2183, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (146, 1077, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (151, 1078, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (156, 3496, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (157, 1080, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (157, 1081, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 1082, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (171, 1083, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (172, 1084, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (236, 2184, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 2191, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 1087, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (172, 2185, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 1090, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (198, 1091, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (208, 1092, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 1093, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 1094, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (234, 1096, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (131, 1175, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 1386, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (227, 1387, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (179, 1388, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (181, 1389, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 1438, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (143, 1439, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 1440, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 1442, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (172, 1443, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 1444, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 1445, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (172, 1446, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (172, 1447, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 1448, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (190, 1449, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 3883, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (171, 1451, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (48, 1459, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 2498, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (142, 1455, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (145, 1456, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (145, 1457, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 1458, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (193, 1461, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (147, 1529, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 1530, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 1531, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 1532, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 1545, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (236, 1441, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 1522, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 1528, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (190, 1524, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (185, 1525, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (234, 1526, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 1527, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (201, 1546, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (144, 1547, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 1519, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 1533, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (162, 1534, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 1535, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 1536, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 1537, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 1540, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 1541, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 1542, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 2499, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 1604, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (238, 1605, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 1606, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 1607, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 1608, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 1609, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 1805, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 1611, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 1612, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 1615, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 1616, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1619, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1620, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1621, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 1806, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1623, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1625, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 1627, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 2500, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 2189, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (260, 1641, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1643, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1644, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1645, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1646, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1647, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1648, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1649, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1650, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1651, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1652, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1653, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1654, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1655, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1656, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1657, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1658, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1659, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1660, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1661, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1662, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1682, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1664, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1665, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1666, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1667, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1668, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1669, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 1670, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 1671, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 1672, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 1673, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 1674, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 1675, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 1676, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 1677, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 1678, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 1679, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 1680, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 1681, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 1820, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 1684, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1784, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 1785, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 2369, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 1786, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 2190, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 1691, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 2192, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 1789, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (275, 1694, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 1790, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 1791, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 1697, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 1792, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 1793, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (282, 1701, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 1795, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (284, 1703, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (285, 1704, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (286, 1705, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 1796, 66.66999816894531);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 1798, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 1799, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 1800, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 1801, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (293, 1712, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 1713, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 1802, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 1803, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 1804, 0.125);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 1807, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 1808, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 1809, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 1810, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 1812, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 1817, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 2501, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 2194, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 2195, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (286, 2814, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 2569, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 2198, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 2199, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 2200, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 2201, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 2202, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (73, 2204, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (73, 2205, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (73, 2206, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (143, 2207, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (143, 2208, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (143, 2209, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (151, 2210, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (151, 2211, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (151, 2212, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 2214, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 2215, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (138, 2216, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (138, 2217, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (138, 2218, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (176, 2219, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (176, 2220, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (176, 2221, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (162, 2222, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (162, 2223, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (162, 2224, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 2225, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 2226, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 2227, 9.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 2228, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 2229, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 2230, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 2231, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 2233, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 2234, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 2235, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 2237, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 2238, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (185, 2239, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (185, 2240, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 2241, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 2242, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 2243, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (187, 2244, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (187, 2245, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (187, 2246, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 2249, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 2248, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 2250, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 2251, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 2252, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 2152, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 2253, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 2254, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 2255, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 2256, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (193, 2257, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (193, 2258, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (193, 2259, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (195, 2260, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (195, 2261, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (198, 2262, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (198, 2263, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (201, 2264, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (201, 2265, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (201, 2266, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (202, 2267, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (202, 2268, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (202, 2269, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (203, 2270, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (203, 2271, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (203, 2272, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (204, 2273, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (204, 2274, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (204, 2275, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (206, 2276, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (206, 2277, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (206, 2278, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 2279, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 2280, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 2281, 9.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (200, 2282, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (200, 2283, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (200, 2284, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (230, 2285, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 2453, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (230, 2287, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 2454, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 2289, 8.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 2290, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (228, 2455, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 2292, 8.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 2293, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 2295, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 2296, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 2297, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 2299, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 2300, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (271, 2301, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (271, 2302, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (271, 2303, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 2304, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 2305, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 2306, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (275, 2307, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (275, 2308, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (275, 2309, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 2310, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 2311, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 2370, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 2371, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 2372, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 2373, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (116, 2374, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 2375, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 2376, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 2377, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 2378, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 2379, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 2380, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 2381, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 2382, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (300, 1829, 0.14000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 2334, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 2335, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 1818, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (119, 1821, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 2383, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 2384, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 2385, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 2386, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (293, 2390, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (293, 2391, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (293, 2392, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 2393, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 2394, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 2395, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (301, 2396, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (301, 2397, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (301, 2398, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (301, 2399, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 2400, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 2401, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 2402, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 2429, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 2430, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 2431, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (289, 2432, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (289, 2433, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (289, 2434, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (288, 2435, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (288, 2436, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (288, 2437, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (270, 2439, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (270, 2440, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 2441, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 2442, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 2443, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (227, 2448, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 2449, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 2450, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (238, 2451, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (230, 2452, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 2456, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (223, 2457, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 2458, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (227, 2459, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (92, 2460, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (238, 2461, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 2462, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 2463, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 2464, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 3635, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (54, 2470, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (54, 2471, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (54, 2472, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (54, 2473, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (234, 2474, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 2476, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (172, 2477, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 2478, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 2481, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (144, 2483, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 2488, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 2489, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 2490, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (156, 2491, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (157, 2493, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 2494, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 2495, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 2496, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 2576, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (356, 2588, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 2591, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 2804, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 2594, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 2596, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (189, 2805, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (198, 2806, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (285, 2811, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (285, 2812, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (286, 2815, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (286, 2816, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (286, 2817, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 2818, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 2819, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 2820, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 2823, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (340, 2581, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 2822, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 2824, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2825, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2826, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2827, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (356, 2640, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2828, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2829, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 2642, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 2621, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2830, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2831, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2832, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 2601, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 2638, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (52, 2833, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 2834, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 2835, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 2836, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 2837, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 2838, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 2839, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 2840, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (316, 2631, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 2626, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 2613, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 2841, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 2842, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (311, 2577, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (311, 2560, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 2559, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (304, 2578, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (307, 2647, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (304, 2579, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 2843, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 2580, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 2600, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 2844, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (340, 2582, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (340, 2583, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (340, 2584, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 2593, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 2595, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 2602, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (356, 2589, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 2590, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (356, 2592, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 2603, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 2604, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 2605, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 2606, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 2607, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 2609, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 2610, 8.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 2611, 8.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 2612, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 2614, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 2624, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 2616, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 2617, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 2908, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (356, 2619, 9.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 2620, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 2622, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 2623, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 2625, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 2627, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 2628, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 2629, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (316, 2630, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (316, 2632, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (316, 2633, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (316, 2634, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 2635, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 2636, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 2637, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 2639, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 2641, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 2643, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 2644, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 2645, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (307, 2646, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (307, 2648, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (307, 2649, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 2650, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 2651, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 2652, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 2653, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 2654, 10.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 2655, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 2656, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 2658, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (61, 2845, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2847, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2848, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2849, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2850, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2851, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2852, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2853, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (51, 2854, 0.949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 2855, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 2856, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 2857, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 2858, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 2859, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 2860, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 2861, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 2862, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (69, 2864, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (69, 2865, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (69, 2866, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (69, 2867, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 2868, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 2869, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (134, 2870, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 2871, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 2872, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 2873, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (135, 2874, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 2878, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 2879, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 2880, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 2881, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (241, 2882, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 2884, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 2885, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 2886, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 2887, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 2888, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 2892, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 2890, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 2893, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 2894, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 2895, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 2896, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (196, 2897, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 2899, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 2900, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 2901, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 2902, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (197, 2903, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 2907, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (73, 2909, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (162, 2910, 0.8500000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (179, 2911, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 2912, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 2939, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3948, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3949, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 7127, 0.07500000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 4012, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 4013, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 4014, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 4015, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 4016, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 2963, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 2964, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 2965, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 2966, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 2967, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 2972, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 2975, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 2976, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 2977, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 2978, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 2979, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 2983, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 2984, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 2986, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 2987, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 2988, 0.125);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 2989, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 2991, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 2992, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 2996, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 2999, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 3000, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 3001, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 3002, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 3005, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3007, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3008, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3009, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3010, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3011, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3012, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 3013, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 3014, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 3015, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 3016, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 3017, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 3018, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 3019, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 3020, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 3021, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 3022, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 3023, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 3024, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 3025, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 3026, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3027, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3028, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3029, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3030, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3031, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3032, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 3033, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 3034, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 3035, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 3036, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 3037, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 3038, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 3039, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3040, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3041, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3042, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3043, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3044, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3045, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3046, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3047, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3048, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3049, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3050, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3051, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3052, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3053, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3054, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3055, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 3056, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (368, 3057, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (368, 3058, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (368, 3059, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (368, 3060, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (368, 3061, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (368, 3062, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (368, 3063, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (350, 3103, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 3561, 0.004999999888241291);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (350, 3105, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (349, 3107, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (349, 3108, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (349, 3109, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 3111, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 3112, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 3113, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 3114, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 3115, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (345, 3116, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (345, 3117, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (345, 3118, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (161, 3120, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (161, 3121, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 2904, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (180, 2905, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (191, 2906, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2924, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (271, 2973, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 3123, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 3124, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 3125, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 3557, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (336, 3127, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (336, 3128, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 3129, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 3130, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 3131, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (78, 3133, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (78, 3134, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (78, 3135, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (78, 3136, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 3137, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 3138, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 3139, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 3140, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 3141, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 3142, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 3143, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 3144, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 3145, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 3146, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 3147, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 3148, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 3149, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (142, 3150, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (142, 3151, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (142, 3152, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 3608, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 3154, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 3155, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 3156, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 3157, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3610, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 3159, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 3160, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 3161, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 3162, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (340, 3164, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (340, 3165, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 3167, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (102, 3168, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 3170, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (103, 3171, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 3172, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 3174, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 3176, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (104, 3177, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 3179, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (105, 3180, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 3182, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (106, 3183, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 3185, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (107, 3186, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 3188, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (108, 3189, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 3191, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (109, 3192, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 3194, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (101, 3195, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 3562, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 3198, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 3199, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 3200, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 3201, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 3202, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 3203, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 3204, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (328, 3205, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (328, 3206, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 3207, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 3208, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 3209, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 3210, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (145, 3213, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (145, 3215, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (146, 3216, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (146, 3217, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (136, 3219, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (136, 3220, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (136, 3221, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (136, 3222, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 3223, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 3224, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 3225, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (158, 3226, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (158, 3227, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (158, 3229, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 3230, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (290, 3558, 0.008999999612569809);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 3232, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (304, 3233, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (304, 3234, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 3237, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 3238, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 3239, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 3240, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (179, 3241, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (179, 3242, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (291, 3244, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (321, 3402, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (291, 3246, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 3559, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (290, 3248, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (290, 3249, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (180, 3250, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (332, 3253, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 3254, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 3255, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 3256, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 3257, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 3258, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 3259, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 3260, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 3261, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 3262, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 3263, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (323, 3265, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (323, 3266, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (323, 3267, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (323, 3268, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (323, 3269, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (304, 3270, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 3271, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 3272, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 3273, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 3274, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 3275, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (207, 3277, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (208, 3280, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (208, 3281, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (208, 3283, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 3284, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 3285, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 3286, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 3287, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (338, 3288, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (322, 3289, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (322, 3290, 0.12999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (322, 3292, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (322, 3293, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (322, 3294, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 3295, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 3296, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 3298, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 3299, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 3300, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 3301, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 3302, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 3303, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 3304, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 3305, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (321, 3306, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (321, 3307, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (321, 3308, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 3309, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 3310, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 3311, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (239, 3312, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 3313, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 3314, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 3315, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (240, 3316, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 3317, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 3318, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 3319, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 3320, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 3321, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 3322, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 3324, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 3325, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 3326, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 3327, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 3328, 0.0020000000949949026);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 3329, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 3330, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 3331, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 3332, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 3333, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 3336, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 3337, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 3338, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 3339, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 3340, 1.7999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 3341, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 3563, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 3343, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 3344, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 3345, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 3346, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 3348, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 3349, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 3350, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 3351, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 3352, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 3353, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 3354, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 3355, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 3356, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 3357, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 3359, 0.10999999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 3361, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 3362, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 3363, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 3364, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3611, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 3367, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 3368, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 3369, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 3370, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (350, 3560, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3612, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 3374, 0.10999999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 3375, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 3376, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 3377, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3378, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3380, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3381, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3383, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3384, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3385, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3386, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 3389, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 3390, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 3391, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 3392, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 3393, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 3394, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 3395, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 3396, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 3397, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 3398, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (328, 3399, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (328, 3400, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (328, 3401, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (321, 3403, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (317, 3404, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (167, 3405, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3406, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3407, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3408, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3409, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3410, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3411, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3412, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3413, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3414, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3415, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3416, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3417, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3418, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3419, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3420, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3421, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3422, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3423, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3424, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3425, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3426, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3427, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3428, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3429, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3430, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3431, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3432, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3433, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (32, 3434, 2.299999952316284);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 3435, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 3436, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 3437, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 3438, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 3439, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 3440, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (31, 3441, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 3442, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 3443, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 3444, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 3445, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 3446, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 3447, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 3448, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 3449, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 3450, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 3451, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 3452, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 3453, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 3454, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3455, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3456, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3457, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3458, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3459, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3460, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3461, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3462, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3463, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3464, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3465, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3466, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3467, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3468, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3469, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3470, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3471, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3472, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3473, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (348, 3475, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (348, 3476, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 3478, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 3479, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 3480, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 3481, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 3482, 8.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (311, 3483, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 3484, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 3485, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (275, 1816, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 2444, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 2466, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 2467, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 2468, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 2469, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 2782, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2660, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2668, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2662, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2664, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2665, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2666, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 2667, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 2670, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (343, 2671, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (343, 2672, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 2673, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 2674, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 2675, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 2676, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (343, 2677, 8.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 2678, 9.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 2679, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (340, 2680, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (336, 2681, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (336, 2682, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (336, 2683, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 2684, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 2685, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 2686, 9.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 2687, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (338, 2688, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (338, 2689, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (338, 2690, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (338, 2691, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 2692, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 2693, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 2694, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 2695, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 2696, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 2697, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 2783, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (345, 2784, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (350, 2785, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (348, 2786, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (349, 2787, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 2788, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (335, 2789, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (332, 2790, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 2791, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 2792, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (317, 2793, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (237, 1822, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (284, 1715, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (284, 1716, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (269, 1717, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (269, 1718, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 1719, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 1720, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (291, 1721, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (290, 1722, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (292, 1723, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (292, 1724, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (289, 1725, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (292, 1726, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (289, 1727, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (290, 1728, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 1751, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 1826, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 1731, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 1732, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (289, 1733, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 1734, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (286, 1735, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 1736, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (282, 1738, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (282, 1739, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (282, 1740, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (285, 1741, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 1742, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (275, 1759, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (63, 1744, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (161, 1745, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (283, 1746, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (283, 1747, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (274, 1748, 0.125);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (273, 1749, 0.125);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 1750, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (268, 1752, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (270, 1753, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (270, 1754, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (270, 1755, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (271, 1756, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (271, 1757, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 1758, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 1760, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 1761, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 1769, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 1763, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 1764, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 1765, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (288, 1766, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (288, 1767, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (288, 1768, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 1770, 0.12999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (280, 1771, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (280, 1772, 0.125);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1773, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1774, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1775, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1776, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 1777, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 1779, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 1780, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 1781, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 1782, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 1824, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (238, 1825, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 2155, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 2158, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (158, 2159, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (312, 2801, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 3486, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 3594, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 3595, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 3596, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 3489, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (170, 3490, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (50, 3491, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 3492, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 3493, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 3494, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (156, 3497, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 3498, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 3499, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 3501, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (171, 3502, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (324, 3504, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 3506, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 3507, 0.02500000037252903);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 3508, 0.02500000037252903);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 3510, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (305, 3514, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (305, 3515, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (307, 3516, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (307, 3517, 0.12999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 3518, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 3519, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 3521, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 3522, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (262, 3524, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 3525, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (265, 3527, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 3528, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (266, 3529, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 3531, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (264, 3533, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 3534, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 3536, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (343, 3537, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (343, 3538, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 3539, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3648, 0.550000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3649, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3650, 0.6700000166893005);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3651, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (336, 3556, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 3581, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 3598, 0.125);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (188, 3599, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 3600, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (189, 3601, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 3602, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 3605, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3613, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3614, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3615, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3616, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3617, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3618, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3619, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3620, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3621, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3622, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3623, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3624, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3625, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3652, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (194, 3634, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 3636, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 3637, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 3638, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 3639, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 3640, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (92, 3641, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (211, 3642, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 3643, 0.03500000014901161);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (271, 3644, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 3645, 0.054999999701976776);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 3646, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 3647, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3653, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3654, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3655, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3656, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3657, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3658, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3659, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3660, 95.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3661, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3662, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3663, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3664, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3665, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3666, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3667, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3668, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3669, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3670, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3671, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3672, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3673, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3675, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3676, 0.07500000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3677, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3678, 0.4699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3679, 0.550000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3680, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3681, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3682, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3683, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3684, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3685, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3686, 0.06499999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3687, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3688, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3689, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3690, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3691, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3692, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3693, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3694, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3695, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3696, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3697, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3698, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3868, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (332, 3807, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3701, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3702, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3703, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 4017, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 4018, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3884, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3885, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3814, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3711, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3712, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3713, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3714, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3715, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3716, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3717, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3718, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3719, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3720, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3721, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3722, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3723, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3724, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3725, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3726, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3727, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3728, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3729, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3730, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3731, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3937, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3735, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3734, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3736, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3737, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3738, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3739, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3740, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3741, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3742, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3743, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3744, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3745, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3746, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3747, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3748, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3750, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3751, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3752, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3753, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 3938, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3755, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3756, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3787, 0.017000000923871994);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3788, 0.07500000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3789, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3790, 0.75);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3791, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3792, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3793, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3794, 0.07500000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3817, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3816, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3818, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3819, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3820, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3821, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3822, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3823, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3824, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3825, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3826, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3827, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3828, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3829, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3830, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3831, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3832, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3833, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 3076, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 3069, 9.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 3070, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 3071, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 3072, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 3073, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 3583, 0.17000000178813934);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 3075, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 3078, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 3079, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 3080, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 3081, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (38, 3082, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3083, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3084, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3085, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3086, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3087, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3088, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3089, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3090, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3091, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3092, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (92, 3585, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (182, 3586, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (181, 3589, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 3592, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 3593, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3674, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3808, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3809, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3834, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3835, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3836, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3837, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3838, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3839, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3840, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3841, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3842, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3843, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3844, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3850, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3853, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3854, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3855, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3856, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3857, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3858, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3859, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3860, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3861, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3862, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 3867, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3869, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3870, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3871, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3872, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3873, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3874, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3875, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3876, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3877, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3878, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3879, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3880, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3881, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3882, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3886, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3887, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3888, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3889, 0.014399999752640724);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3890, 0.02160000056028366);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3891, 0.04500000178813934);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3892, 0.08100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3893, 0.14399999380111694);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3894, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3895, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3896, 0.8500000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3897, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3898, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3899, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3900, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 3901, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3902, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3903, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 3904, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3905, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3906, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 3907, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3908, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3909, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 3910, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3911, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3912, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 3913, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (26, 3936, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 3944, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 3945, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3950, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (23, 3935, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 3951, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (237, 3997, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (120, 3998, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 3999, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 4000, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 4001, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 4002, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 4019, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 4020, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 4021, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 4022, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 4023, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 4024, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 4026, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (211, 4027, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 4028, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 4029, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 4030, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 4031, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (316, 4032, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (39, 4033, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 4034, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (172, 4035, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (234, 4036, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 4037, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 4038, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 4039, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 4040, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 4041, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 4042, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 4043, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 4044, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 4045, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 4046, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (324, 4047, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 4048, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 4049, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 4050, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 4051, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 4052, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 4053, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 4054, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 4055, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 4056, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 4057, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (402, 4058, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4059, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4060, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4130, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4062, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4131, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (406, 4151, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4139, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4157, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (409, 4252, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4260, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (411, 4069, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4070, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4071, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (414, 4072, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4073, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4074, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (414, 4075, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4076, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4077, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4078, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4079, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4080, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4081, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4082, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (419, 4086, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (419, 4087, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (419, 4088, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4089, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4090, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (421, 4091, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4092, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4093, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4094, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4095, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (406, 4096, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4097, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4098, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4099, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (406, 4100, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4101, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4102, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4103, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4104, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4105, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4106, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4107, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4108, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4109, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4110, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4111, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4112, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4113, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4114, 82.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4115, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4116, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4117, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4118, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4119, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4120, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4121, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4122, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4123, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4124, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4125, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4126, 2.299999952316284);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4127, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4128, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4129, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4132, 1.7999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4133, 2.200000047683716);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4134, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4135, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4136, 1.7999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4137, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4138, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4140, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4141, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4142, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4143, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4144, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4145, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4146, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4147, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4148, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (407, 4149, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (406, 4150, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (406, 4152, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (406, 4153, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (406, 4154, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (406, 4155, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4156, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4158, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4159, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4160, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 4382, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4162, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4163, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4164, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4165, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4166, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4167, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4168, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4169, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4170, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4171, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4172, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4173, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4174, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4175, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4176, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4177, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4178, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4179, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4180, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4181, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4182, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4183, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4184, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4185, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4186, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4187, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (421, 4188, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (421, 4189, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4195, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (421, 4191, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (421, 4192, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (421, 4193, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (421, 4194, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4196, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4197, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4199, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4200, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4201, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4202, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4203, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4204, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4205, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4206, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4207, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4208, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4209, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4210, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4211, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4212, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4213, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4214, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4215, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4216, 0.12999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4217, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4218, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4219, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (414, 4220, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (414, 4221, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (414, 4222, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (414, 4223, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (414, 4224, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (414, 4225, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4226, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4227, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4228, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4229, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4230, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4232, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4233, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4234, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4235, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4236, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4237, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4238, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4239, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4240, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4241, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4242, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4243, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4244, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4245, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4246, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4247, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4248, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4249, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4250, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (409, 4251, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (409, 4253, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (409, 4258, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (409, 4255, 0.004999999888241291);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (409, 4257, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4259, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4261, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4262, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4263, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4264, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4265, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4266, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4267, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4268, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4269, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4270, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (168, 1878, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (168, 1879, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 2597, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (75, 1881, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (75, 1882, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (186, 2913, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 1884, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 1885, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (303, 2599, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (179, 1887, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (179, 1888, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 2940, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (167, 1890, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (167, 1891, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 2941, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 1893, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 1894, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (398, 3757, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (180, 1896, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (180, 1897, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (398, 3758, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (181, 1899, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (181, 1900, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (398, 3759, 0.949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 1902, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 1903, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (398, 3760, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (182, 1905, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (182, 1906, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (398, 3761, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 1908, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 1909, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (398, 3762, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 1911, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 1912, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (398, 3763, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 1914, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (136, 1915, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (136, 1916, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3764, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (189, 1918, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (189, 1919, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3765, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (78, 1921, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (78, 1922, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3766, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (190, 1924, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (190, 1925, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3767, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (110, 1927, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (110, 1928, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3768, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 1930, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 1931, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3769, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (147, 1933, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (147, 1934, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3770, 0.75);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (147, 1936, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (191, 1937, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (191, 1938, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3771, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 1940, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 1941, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 3772, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (158, 1943, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (158, 1944, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (400, 3773, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 1946, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 1947, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (400, 3774, 1.2999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (192, 1949, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (400, 3775, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (192, 1951, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (207, 1952, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (207, 1953, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (400, 3776, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (205, 1955, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (205, 1956, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (400, 3777, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (208, 1958, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (208, 1959, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (400, 3778, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 1961, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (209, 1962, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (400, 3779, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (211, 1964, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (211, 1965, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (400, 3780, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (213, 1967, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (213, 1968, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (401, 3781, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (214, 1970, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (214, 1971, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (401, 3782, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 1973, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 1974, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (401, 3783, 0.949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (215, 1976, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (215, 1977, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (401, 3784, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 1979, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 1980, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (401, 3785, 2.200000047683716);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 1982, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 1983, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (401, 3786, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3795, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 1986, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (284, 3796, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (269, 3797, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 3798, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (238, 1990, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 3799, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 3800, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (223, 1993, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 3801, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (335, 3802, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (228, 2010, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 3803, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (225, 1998, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (317, 3804, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 3805, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (226, 2001, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 3806, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 3952, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (227, 2004, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 3953, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3954, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (92, 2007, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3955, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 2011, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 2012, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 3956, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (283, 2014, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (283, 2015, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 3957, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 2017, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 2018, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3958, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (280, 2020, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (280, 2021, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3959, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 2023, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 2024, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3960, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (222, 2026, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (222, 2027, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3961, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3962, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 3963, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3964, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3965, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3966, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 3968, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 3969, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 3970, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 3971, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 3972, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 3973, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 3974, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 3975, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 3976, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 3977, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 3978, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 3979, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 3980, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 3981, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 3982, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 3983, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 3984, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 3985, 0.14000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (67, 3986, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 3987, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 3988, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 3989, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (76, 3990, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 3991, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 3992, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 3993, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 3994, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 3995, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 3996, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 4011, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4271, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4273, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4274, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4384, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4278, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4280, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4281, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4282, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4283, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4284, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4285, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4286, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4287, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4288, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4290, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4291, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4292, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4293, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4294, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4295, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4296, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4297, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4298, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4299, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4300, 9.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4301, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4302, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (417, 4303, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (419, 4304, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (419, 4305, 8.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (419, 4306, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (419, 4307, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4308, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4309, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4310, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4311, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4312, 6.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4313, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (416, 4314, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4336, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4370, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4318, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4319, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4320, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4322, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4323, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4324, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4325, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4326, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4328, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4329, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4330, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4385, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4333, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (29, 2164, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (29, 2165, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4272, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (194, 2167, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (194, 2169, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (194, 2170, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (194, 2171, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (194, 2172, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (194, 2173, 0.41999998688697815);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (47, 2174, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (47, 2175, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (47, 2176, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (44, 2177, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (44, 2178, 1.7999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (44, 2179, 2.200000047683716);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 3626, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 3627, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 3628, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (304, 2570, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 2571, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 2572, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (306, 2573, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 2575, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 2698, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 2699, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 2700, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 2701, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2702, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2705, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2706, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2707, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2708, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2709, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 2710, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (323, 2711, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (323, 2712, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (189, 2713, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (328, 2794, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (350, 2715, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (350, 2716, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (328, 2795, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (349, 2718, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (349, 2719, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (328, 2796, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (348, 2721, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (348, 2722, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (348, 2723, 0.550000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 2724, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 2725, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 2726, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 2727, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 2728, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 2729, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 2797, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 2731, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 2732, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (345, 2733, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 2798, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (345, 2735, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (322, 2799, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 2737, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 2738, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (313, 2800, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (335, 2740, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (335, 2741, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (332, 2743, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (332, 2744, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (332, 2745, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 2746, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 2747, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 2748, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 2749, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (324, 2750, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (324, 2751, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (324, 2752, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (324, 2753, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (321, 2754, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (321, 2755, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (321, 2756, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 2758, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 2759, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (318, 2760, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (313, 2802, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (317, 2762, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (317, 2763, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (317, 2764, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (305, 2768, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (305, 2766, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (305, 2767, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (322, 2803, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 2770, 5.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 2771, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 2772, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 2773, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 2780, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 2775, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 2776, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 2777, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (354, 2778, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 2779, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 2781, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 2807, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 2808, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (193, 2809, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 3574, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 3575, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 3576, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 3577, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (364, 3578, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (49, 3579, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (242, 3580, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 3570, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 3566, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 3567, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 3568, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 3569, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 3571, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 3810, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (390, 3813, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4339, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4338, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4340, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (412, 4341, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (415, 4342, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4343, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4344, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 4345, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 4346, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 4347, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (373, 4348, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (422, 4349, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4350, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4351, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4352, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (425, 4353, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (420, 4354, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (409, 4355, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (410, 4356, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 4357, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4358, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 4359, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4360, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (404, 4361, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4362, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4363, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4364, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (426, 4365, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (413, 4366, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (419, 4367, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (423, 4368, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 4369, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4383, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 4387, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 4388, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 4389, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 4390, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 4391, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (429, 4616, 9.999999974752427e-07);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (418, 4394, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (431, 4395, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (430, 4397, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4399, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4400, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4401, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4402, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4403, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4404, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4405, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4406, 0.07199999690055847);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4407, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4408, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4409, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4410, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4411, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4412, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4413, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4414, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4415, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4416, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4417, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4418, 0.1080000028014183);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4419, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4420, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4421, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4422, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4423, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4424, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4425, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4426, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4427, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4428, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4429, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4430, 0.22499999403953552);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4431, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4432, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4433, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4434, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4435, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4436, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4437, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4438, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4439, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4440, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4441, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4442, 0.4050000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4443, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4444, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4445, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4446, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4447, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9057, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 9329, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9259, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9060, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9061, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9062, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9063, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9064, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9260, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9261, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9067, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9068, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9069, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9070, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9071, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9262, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9276, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9074, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9075, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9076, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9077, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9078, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9277, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9278, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9081, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9082, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9083, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9084, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9085, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9279, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9280, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9088, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9089, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9090, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9091, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9092, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9281, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9282, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9095, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9096, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9097, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9098, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9099, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9100, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9101, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9283, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9284, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9104, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9105, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9106, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9107, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9108, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9109, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9110, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9111, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9112, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9285, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9286, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9115, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9116, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9117, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9118, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9119, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9120, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9121, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9122, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9123, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9287, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9288, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9126, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9127, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9128, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9129, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9130, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9131, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9132, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9133, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9289, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9290, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9291, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9137, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9138, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9139, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9140, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9141, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9142, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9143, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9144, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (821, 9292, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9293, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9294, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9149, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9150, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9151, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9152, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9153, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9154, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9155, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9156, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9295, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9296, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9159, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9160, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9161, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9162, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9163, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9164, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9165, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9166, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9167, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (824, 9297, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9298, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9170, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9171, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9172, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9173, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9174, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9175, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9176, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9177, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9178, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9299, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9300, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9181, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9182, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9183, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9184, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9185, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9186, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9187, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9188, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9301, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9190, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 9330, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9192, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9193, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9194, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (848, 9447, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9449, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9450, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9451, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9452, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (851, 9453, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9455, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9457, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9458, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9460, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9461, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9462, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9463, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9464, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9466, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9467, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9468, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9469, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9471, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9472, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9195, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9196, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9197, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9198, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (825, 9302, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9349, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9201, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9202, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9203, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9204, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9205, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9206, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9207, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9208, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9209, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9303, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (851, 9440, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9212, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9213, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9214, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9215, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9216, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9217, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9218, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9219, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9220, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9221, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9304, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (851, 9441, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 9242, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9225, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9226, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 9227, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 9228, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 9229, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 9230, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 9231, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 9232, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 9233, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 9234, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 9235, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 9236, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 9237, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (110, 9238, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 9239, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 9240, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 9241, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 9243, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9191, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (840, 9244, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9305, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9306, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (822, 9307, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9308, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9309, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9310, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9311, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (823, 9312, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 9331, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 9332, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 9333, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 9334, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 9335, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9337, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 9345, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 9347, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 9348, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (851, 9442, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (851, 9443, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (851, 9444, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (851, 9445, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (851, 9446, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (848, 9364, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (848, 9365, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (848, 9366, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (848, 9367, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (848, 9368, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (848, 9369, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (848, 9371, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9372, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9373, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9374, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9375, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9376, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9377, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (849, 9379, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9380, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9381, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9382, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9383, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9384, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9385, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (850, 9387, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9148, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9245, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9246, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9247, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9248, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9249, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9250, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9251, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9252, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9253, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (841, 9254, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 9346, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9313, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9314, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9315, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (836, 9316, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9317, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (826, 9318, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (827, 9319, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (828, 9320, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (829, 9321, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (830, 9322, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (831, 9323, 0.02500000037252903);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (834, 9324, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (835, 9325, 0.02500000037252903);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (832, 9326, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (833, 9327, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (837, 9328, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 9350, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 9351, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9388, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9389, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9390, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9391, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9392, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9393, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9394, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (854, 9395, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9397, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9398, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9399, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9400, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9401, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9402, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9403, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (855, 9404, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9406, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9407, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9408, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9409, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9410, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9411, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9412, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (852, 9413, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9415, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9416, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9417, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9418, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9419, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9420, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9421, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (853, 9422, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 9424, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (338, 9425, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 9426, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 9427, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 9428, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 9429, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 9430, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 9431, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 9432, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 9433, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 9434, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 9435, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 9436, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 9437, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 9438, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4448, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4449, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4450, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4451, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4452, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4453, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4454, 0.7200000286102295);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4455, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4456, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4457, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4458, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (403, 4459, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (405, 4460, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4461, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4462, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (424, 4463, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (421, 4464, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 4465, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (173, 4466, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 4467, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 4468, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 4469, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 4470, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 4471, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 4472, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 4473, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 4474, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (441, 4475, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (441, 4476, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (442, 4477, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (442, 4478, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (443, 4479, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (443, 4480, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (444, 4481, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (444, 4482, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (445, 4483, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (445, 4484, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (446, 4485, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (447, 4486, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 4487, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 4488, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 4489, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 4490, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 4491, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 4492, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 4493, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 4494, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 4495, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 4496, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 4497, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 4498, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 4499, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 4500, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 4501, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 4502, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (441, 4503, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (441, 4504, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (442, 4505, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (443, 4506, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (444, 4507, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (445, 4508, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (446, 4509, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (447, 4510, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4511, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (427, 4512, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4513, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (428, 4514, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7642, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (743, 8200, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (744, 8201, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (745, 8202, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8362, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8363, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7643, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 8297, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 8298, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 8299, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 8300, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8364, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8365, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8366, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8367, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8368, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8369, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8370, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8371, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8372, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8373, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8374, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8375, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8376, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8377, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (752, 8378, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7979, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (753, 8379, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (753, 8380, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (753, 8381, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (753, 8382, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (753, 8383, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (753, 8384, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (753, 8385, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (753, 8386, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8387, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8388, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8389, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7645, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8390, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8391, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8392, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8393, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8394, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8395, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (754, 8396, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8397, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8398, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8399, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8400, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8401, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8402, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4567, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4568, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4569, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4570, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4571, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4572, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4573, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4574, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4575, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4576, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4577, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4578, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4579, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4580, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4581, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4582, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4583, 9.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4584, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4585, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4586, 9.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4587, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4588, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4589, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4590, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4591, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 4597, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (452, 4617, 0.0010000000474974513);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4618, 0.0010000000474974513);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 4619, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (409, 4620, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 4598, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 4599, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 4600, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 4601, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 4602, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 4603, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (193, 4604, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 4605, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 4606, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 4607, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 4608, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 4609, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 4610, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 4611, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 4612, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (261, 4613, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (319, 4614, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 4615, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 4621, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 4622, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (497, 5136, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (497, 5137, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 5138, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 5139, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 5140, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (457, 4684, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4629, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4630, 0.0010000000474974513);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (457, 4685, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4632, 0.0010000000474974513);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4633, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (454, 4634, 0.0010000000474974513);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (457, 4686, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (457, 4687, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4637, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (457, 4688, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4639, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 5063, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4641, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4642, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4643, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4644, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4645, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4646, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4647, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4648, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4649, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4650, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (455, 4651, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4652, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4653, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4654, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4655, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4656, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4657, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4658, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4659, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4660, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (456, 4661, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (452, 4662, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (452, 4663, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (452, 4664, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (452, 4665, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (452, 4666, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (452, 4667, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (452, 4668, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4669, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4670, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4671, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4672, 0.44999998807907104);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4673, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4674, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4675, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4676, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (453, 4677, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (454, 4678, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (454, 4679, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (454, 4680, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (454, 4681, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (454, 4682, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (454, 4683, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 5064, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 5065, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (495, 5066, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (495, 5067, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 5141, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (496, 5069, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (496, 5070, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 5142, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 5143, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 4699, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 4700, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 4701, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 4702, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 4703, 0.006000000052154064);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 4704, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (408, 4371, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 4372, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 4373, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 4374, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 4375, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 4376, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 4377, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 4378, 3.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 4379, 4.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 4380, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 4381, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 4705, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 4706, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 4707, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 4708, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5173, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (461, 4710, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (461, 4712, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 4713, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 4714, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 4715, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 4716, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 4717, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 4718, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 4719, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 4720, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 4721, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 4722, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (464, 4723, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (464, 4724, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (464, 4725, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (464, 4726, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 4727, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (464, 4728, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 4729, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 4730, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 5146, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 4732, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 4733, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 4734, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5174, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 4736, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 4737, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 4738, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 4739, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 4740, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 4741, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 4742, 0.6700000166893005);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 4743, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 4744, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5222, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 4746, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 4747, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 4748, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 4749, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 4750, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 4751, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 4752, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (469, 4753, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (469, 4754, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (469, 4755, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (469, 4756, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (469, 4757, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (469, 4758, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (469, 4759, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 4760, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 4761, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 4762, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 4763, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 4764, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 4765, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 4766, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5466, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 4768, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 4769, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 4770, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 4771, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 4772, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 4773, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 4774, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 4775, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 4776, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 4777, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 4778, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 4779, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 4780, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 4781, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 4782, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 4783, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 4784, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 4785, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 4786, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 4787, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 4788, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 4789, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 4790, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 4791, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 4792, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 4793, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 4794, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 4795, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 4796, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 4797, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 4798, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 4799, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 4800, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 4801, 0.1599999964237213);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 4802, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4803, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4804, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4805, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4806, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4807, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4808, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4809, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4810, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 5295, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4812, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 4813, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 4814, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5175, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5176, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (578, 6172, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (464, 5149, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 5150, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 5151, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 5152, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 5153, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 4823, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 4824, 0.007000000216066837);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 4825, 0.007000000216066837);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 4826, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 4827, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5177, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5178, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 4830, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 4832, 0.006000000052154064);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 4833, 0.006000000052154064);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 4834, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4837, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4836, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4838, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4839, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4840, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4841, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4842, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4843, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4844, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4845, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4846, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4847, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4848, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4849, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (481, 4850, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4851, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4852, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4853, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4854, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4855, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4856, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4857, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4858, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4859, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4860, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4861, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4862, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4863, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4864, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (482, 4865, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 4866, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 4867, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 4868, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 4869, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 4870, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 4871, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 4872, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 7137, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 4874, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4876, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4877, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4878, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4879, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4880, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4881, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4882, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4883, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4884, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4885, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (485, 4886, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (485, 4887, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 5154, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (486, 4889, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5179, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5180, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 4892, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 4893, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 4894, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 4895, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4896, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4897, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4898, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4899, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (484, 4900, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (432, 4901, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 4909, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 4910, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 4911, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 4912, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 4913, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 4914, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 4915, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 4916, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 4917, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (156, 4918, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (528, 5467, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5037, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (156, 4921, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 4922, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 4923, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 4924, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 4925, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 4926, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 4927, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (162, 4928, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 4929, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 4930, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (182, 4931, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (75, 4932, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (177, 4933, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (178, 4934, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (182, 4935, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (182, 4936, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (180, 4937, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (181, 4938, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (168, 4939, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (167, 4940, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (98, 4941, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 4942, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 4943, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 4944, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (68, 4945, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 4946, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (368, 4947, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 4948, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 4949, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 4950, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (217, 4951, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 4952, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (230, 4953, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 5487, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 5697, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (487, 4956, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 4957, 24.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 4958, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 4959, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (271, 4960, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (488, 4961, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 5700, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 5701, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 5702, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (489, 4965, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 4966, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 4967, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 4968, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (336, 4969, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 4970, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 4971, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 4972, 24.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (337, 4973, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (338, 4974, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (343, 4975, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 4976, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 4977, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 4978, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (338, 4979, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 4980, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (343, 4981, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 4982, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 4983, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 4984, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 4985, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 4986, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 4987, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 4988, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 4989, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 4990, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (355, 4991, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 4992, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 4993, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (356, 4994, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 4995, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 4996, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (356, 4997, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 4998, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 4999, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 5000, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 5001, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 5002, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (307, 5003, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 5004, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 5005, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 5006, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 5007, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (175, 5008, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 5009, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (132, 5010, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 5011, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 5012, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 5013, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 5014, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 5015, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 5016, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (272, 5017, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (490, 5018, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 5019, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 5020, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (216, 5021, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (212, 5022, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 5023, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 5024, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 5025, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 5026, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (491, 5027, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (491, 5028, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (491, 5029, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (491, 5030, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (491, 5031, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (491, 5032, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (491, 5033, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (491, 5034, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5038, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5039, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5040, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5041, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5042, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5043, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5044, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5045, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (492, 5046, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 5052, 24.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 5053, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (494, 5054, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 5055, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 5056, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 5057, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 5058, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 5059, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (433, 4902, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (434, 4903, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (435, 4904, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (436, 4905, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (418, 4906, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (431, 4907, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (430, 4908, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (146, 5035, 16.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (145, 5036, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 5060, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 5061, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 5073, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 5074, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 5075, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 5076, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5108, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5109, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5110, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5111, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5112, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (418, 5113, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (431, 5114, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (430, 5115, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (418, 5116, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (418, 5117, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (431, 5118, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (431, 5119, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (430, 5120, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (430, 5121, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6828, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5123, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6829, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5125, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6830, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5127, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6831, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5129, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6832, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5131, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5132, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5133, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5134, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5135, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 5144, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 5145, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 5155, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 5156, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 5157, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 5158, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 5159, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 5160, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 5161, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 5162, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 5163, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 5164, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 5165, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 5166, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 5167, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 5168, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 5169, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 5170, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 5171, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 5172, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5181, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5182, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5183, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5184, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5185, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5186, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5187, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5188, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5189, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5190, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5191, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (95, 5192, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (367, 5193, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (284, 5194, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (269, 5195, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 5196, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (286, 5197, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (287, 5198, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (285, 5199, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (445, 5200, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (444, 5201, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (443, 5202, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (442, 5203, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 5204, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (457, 5205, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 5206, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 5207, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 5208, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 5209, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 5210, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 5211, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 5212, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 5213, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5223, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5224, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5225, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5226, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5227, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5228, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5229, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5230, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5231, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5232, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5233, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5234, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5235, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5236, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5237, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5238, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5239, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5240, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (509, 5241, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 5296, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5297, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5298, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5258, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5246, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5247, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5248, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5249, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5250, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5251, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5252, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5253, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5254, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5255, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5256, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5257, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5259, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5260, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5261, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (510, 5262, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5263, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5264, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5265, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5266, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5267, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5268, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5269, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5270, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5271, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5272, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5273, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (511, 5274, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5299, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5300, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5301, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5302, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5303, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5304, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5305, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5306, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5307, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5308, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5309, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5310, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5311, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5312, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5313, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5314, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5315, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5316, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5317, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5318, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5319, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5320, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5321, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5322, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5323, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5324, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5325, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5326, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5327, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5328, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5329, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5330, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5331, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5332, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 5333, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5334, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5335, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5336, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5337, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5338, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (512, 5339, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (512, 5341, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (512, 5343, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 5344, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 5345, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 5346, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 5347, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 5348, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 5349, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 5350, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 5351, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 5352, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 5353, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 5354, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 5355, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5356, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5357, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5358, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5359, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5360, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5361, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5362, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5363, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5364, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5365, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (516, 5366, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (516, 5367, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (516, 5368, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (516, 5369, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (516, 5370, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (517, 5371, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (517, 5372, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (517, 5373, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (517, 5374, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (517, 5375, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 5376, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 5377, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 5378, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 5379, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 5380, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 5381, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 5382, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5383, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5384, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5385, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5386, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5387, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5388, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5389, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5390, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5391, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5392, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 5393, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 5394, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 5395, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 5396, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 5397, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 5398, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 5399, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 5400, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 5401, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 5402, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 5403, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 5404, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5412, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5413, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5414, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5415, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5416, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5417, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5418, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (528, 5472, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5420, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5421, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5422, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5423, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5424, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (524, 5425, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (524, 5426, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (524, 5427, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (281, 6802, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (524, 5429, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (525, 5430, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (525, 5431, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (529, 5473, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (525, 5433, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (525, 5434, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (526, 5435, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 5686, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (526, 5437, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (526, 5438, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (526, 5439, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (526, 5440, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5441, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5442, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5443, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5444, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5445, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5446, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5447, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5448, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5449, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5687, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (512, 5451, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 5452, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (517, 5453, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (524, 5454, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (525, 5455, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (441, 5456, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 5457, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 5458, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 5459, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 5460, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 5689, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 5690, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (529, 5478, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (530, 5479, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 5691, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 5692, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (346, 5705, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (345, 5706, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (530, 5484, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (305, 5707, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (347, 5708, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 5709, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 5710, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (311, 5711, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 5712, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (304, 5713, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (320, 5714, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (361, 5715, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 5716, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (333, 5717, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (263, 5747, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (277, 5748, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (50, 5773, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 5821, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 5822, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 5823, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 5824, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 5825, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (569, 5886, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 5887, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 5888, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 5889, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 5890, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 5891, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 6058, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 7130, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 6060, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 7131, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 6062, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 7132, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 6064, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 7133, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 6066, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 7134, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 6068, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 7135, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 6070, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 7136, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 6072, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (493, 5047, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (493, 5048, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (493, 5049, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (493, 5050, 0.00800000037997961);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (493, 5051, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (512, 5340, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (528, 5468, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (528, 5469, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (529, 5474, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (529, 5475, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (529, 5476, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (529, 5477, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (530, 5480, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (530, 5481, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (530, 5482, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (530, 5483, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (528, 5485, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (528, 5486, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (528, 5470, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (528, 5471, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (311, 5703, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (91, 5595, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 5634, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 5635, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (352, 5636, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 5637, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (461, 5638, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (461, 5639, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 5640, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 5641, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (344, 5704, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 5643, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 5644, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 5645, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 5750, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 5647, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 5648, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 5649, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 5650, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 5651, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 5652, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 5653, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 5654, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 5655, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 5656, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 5657, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 5658, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 5659, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 5660, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (546, 5661, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (325, 5662, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 5663, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 5664, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (316, 5665, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (314, 5666, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 5667, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 5668, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 5669, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 5670, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 5671, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 5672, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 5673, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 5674, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 5675, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 5676, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 5677, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 5678, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 5679, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 5680, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5751, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5752, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 5753, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5754, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 5755, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 5756, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5757, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5758, 0.550000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5759, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5760, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5761, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5762, 0.550000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5763, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5764, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5765, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5766, 0.07500000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5767, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5768, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5769, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5770, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5771, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5772, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (548, 5774, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5826, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (460, 5827, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8403, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8404, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5830, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (459, 5831, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 7189, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 6074, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 6075, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 6076, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 6077, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 6078, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 6079, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7235, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7236, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (573, 6084, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (573, 6085, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (573, 6086, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (573, 6093, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 6114, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (573, 6089, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (573, 6090, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (573, 6091, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (573, 6092, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (572, 6113, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6095, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6096, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6097, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6098, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6099, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6100, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6101, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6102, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6103, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 6159, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (572, 6105, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (572, 6106, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (572, 6107, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (572, 6108, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (572, 6109, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (572, 6110, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (572, 6111, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (574, 6112, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 6161, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (581, 6193, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (581, 6195, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 6319, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 6320, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6833, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6835, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 7190, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 7191, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 7192, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 7193, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 7194, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 7195, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 7196, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 7197, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (463, 7198, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (457, 7199, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (462, 7200, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 7201, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 7202, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 7203, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 7204, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 7205, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 7206, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (331, 7207, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (315, 7208, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 7209, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 7210, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 7211, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 7212, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 7213, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 7214, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 7215, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 7216, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 7217, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 7218, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 7219, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 7220, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 7221, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 7222, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 7223, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 7224, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 6386, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 6387, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 6388, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 6389, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (610, 6421, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (611, 6422, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (612, 6423, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (378, 6424, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (379, 6425, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (380, 6426, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (381, 6427, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (382, 6428, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7984, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7237, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7238, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7239, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7240, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7241, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7242, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (648, 7243, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (649, 7244, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (649, 7245, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (649, 7246, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (649, 7247, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (649, 7248, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (649, 7249, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (649, 7250, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (649, 7251, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (650, 7252, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (650, 7253, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (650, 7254, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (650, 7255, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (650, 7256, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (650, 7257, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (650, 7258, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (650, 7259, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (651, 7260, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (651, 7261, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (651, 7262, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (651, 7263, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (651, 7264, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (651, 7265, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (652, 7266, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (652, 7267, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (652, 7268, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (652, 7269, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (755, 8405, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (652, 7271, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (653, 7272, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (653, 7273, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (653, 7274, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (653, 7275, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (653, 7276, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (653, 7277, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (654, 7278, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (654, 7279, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (654, 7280, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (654, 7281, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (654, 7282, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (654, 7283, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (654, 7284, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (655, 7285, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (655, 7286, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (655, 7287, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (655, 7288, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (655, 7289, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (655, 7290, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (655, 7291, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (656, 7292, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (656, 7293, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (656, 7294, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (656, 7295, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (656, 7296, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (656, 7297, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (657, 7298, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (657, 7299, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (657, 7300, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (657, 7301, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (657, 7302, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (657, 7303, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (658, 7304, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (658, 7305, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (658, 7306, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (658, 7307, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (658, 7308, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (658, 7309, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (659, 7310, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (659, 7311, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8406, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (659, 7313, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (659, 7314, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (659, 7315, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (659, 7316, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (659, 7317, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (660, 7318, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (660, 7319, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (660, 7320, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8407, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (660, 7322, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (660, 7323, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (660, 7324, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7325, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7326, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7327, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7328, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7329, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8408, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7331, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (662, 7332, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (662, 7333, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (662, 7334, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (662, 7335, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (662, 7336, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (662, 7337, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (662, 7338, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (663, 7339, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (663, 7340, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (663, 7341, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (663, 7342, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (663, 7531, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (663, 7344, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (663, 7345, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (664, 7346, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (664, 7347, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (664, 7348, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (664, 7349, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (664, 7350, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (664, 7351, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (664, 7352, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (665, 7353, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (665, 7354, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (665, 7355, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (665, 7356, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (665, 7357, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (665, 7358, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (666, 7359, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (666, 7360, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (666, 7361, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (666, 7362, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (666, 7363, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (666, 7364, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (667, 7365, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (667, 7366, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (667, 7367, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (667, 7368, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8409, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (667, 7370, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (667, 7371, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 5405, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 5406, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 5407, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 5408, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 5409, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 5410, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 5411, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (525, 5461, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (526, 5462, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5463, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 5464, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (87, 5683, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 5684, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 5685, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (384, 5834, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 5835, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 5836, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 5837, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 5838, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 5839, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 5840, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 5841, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 5842, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (387, 5843, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (389, 5844, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (386, 5845, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (388, 5846, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (385, 5847, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 5848, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 5849, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 5850, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 5851, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5852, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 5853, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 5854, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5855, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 6082, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 5857, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 5858, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5859, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 5860, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5861, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 5862, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 5863, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5864, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5865, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5866, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 5867, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 5868, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5869, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5870, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5871, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 5872, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5873, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 5874, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5875, 99.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 5876, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (668, 7372, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (668, 7373, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (668, 7374, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (668, 7375, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (668, 7376, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (668, 7377, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (669, 7378, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (529, 8334, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (530, 8335, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 6083, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6567, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6568, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6569, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6570, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6571, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6572, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6573, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6574, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6575, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (618, 6576, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (669, 7379, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (669, 7380, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (669, 7381, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (669, 7382, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (669, 7383, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7384, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7385, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7386, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (652, 7537, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7388, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7389, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7390, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (671, 7392, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (671, 7393, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (667, 7538, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (671, 7395, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (671, 7396, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (671, 7397, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7399, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7400, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7401, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7402, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7403, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7404, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7406, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7407, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7408, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7409, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7410, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7411, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7412, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7413, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 7414, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7415, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7416, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7417, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7418, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7419, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7420, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7421, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7422, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (674, 7423, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (675, 7424, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (675, 7425, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (675, 7426, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (675, 7427, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (675, 7428, 1.6699999570846558);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (675, 7429, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (675, 7430, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (675, 7431, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7432, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7433, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7434, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7435, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7436, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7437, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7438, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7439, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (676, 7440, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (677, 7441, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (677, 7442, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (677, 7443, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (677, 7444, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (677, 7445, 1.6699999570846558);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (677, 7446, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7447, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7448, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7449, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7450, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7451, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7452, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7453, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7454, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 7455, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (679, 7456, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (679, 7457, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (679, 7458, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (679, 7459, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (679, 7460, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (679, 7461, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (679, 7462, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 7463, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 7464, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 7465, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 7466, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 7467, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 7468, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 7469, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 7470, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (671, 7474, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7475, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7476, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7478, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7480, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7482, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7483, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7484, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7485, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7486, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7487, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7488, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7489, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7490, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7491, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7492, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7493, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7494, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7495, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (685, 7496, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7497, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7498, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7499, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7500, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7501, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7502, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7503, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7504, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7505, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7506, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7507, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7508, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7509, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7510, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (684, 7511, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (683, 7512, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (683, 7513, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (683, 7514, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (683, 7515, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (683, 7516, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (683, 7517, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (682, 7518, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (682, 7519, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (682, 7520, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (682, 7521, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (682, 7522, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (682, 7523, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (681, 7524, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (681, 7525, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (681, 7526, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (681, 7527, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (681, 7528, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (681, 7529, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (681, 7530, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7691, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7692, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7694, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7695, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7981, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7697, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7698, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7699, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7700, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7701, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7702, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7703, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7704, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7705, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (736, 8069, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7707, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7708, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7709, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7710, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7711, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (706, 7714, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (707, 7715, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (706, 7721, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (512, 7723, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (710, 7718, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (711, 7719, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (712, 7720, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (707, 7722, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 7724, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 7725, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 7726, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 7727, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 7728, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 7746, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (713, 7730, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (714, 7731, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7732, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (671, 7733, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7734, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (659, 7735, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (660, 7736, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7737, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7738, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7739, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7740, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7741, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7742, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7743, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7744, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7745, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7836, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7837, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7838, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7839, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7840, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (736, 8070, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (736, 8071, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7844, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7845, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7846, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7847, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (736, 8072, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7850, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7851, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7852, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7853, 95.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7854, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7855, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7856, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7857, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7858, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (641, 8073, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (641, 8074, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7862, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7863, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7864, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7865, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (641, 8075, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (737, 8076, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7869, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7870, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7871, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7872, 95.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7873, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7874, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7875, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7876, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7877, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (737, 8077, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (737, 8078, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (737, 8079, 95.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7881, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7882, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7883, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7884, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (737, 8080, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (737, 8081, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7888, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7889, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7890, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7891, 95.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7892, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (738, 8082, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7894, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7895, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7896, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (738, 8084, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (738, 8085, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7900, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7901, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7902, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7903, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (642, 8086, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (642, 8087, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7907, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5077, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5078, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5079, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5080, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5081, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5082, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5083, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5084, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5085, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5086, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5087, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5088, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5089, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5090, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5091, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5092, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (498, 5093, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5094, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (499, 5095, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5096, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (500, 5097, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5098, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (501, 5099, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5100, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (502, 5101, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (418, 5102, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (418, 5103, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (431, 5104, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (431, 5105, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (430, 5106, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (430, 5107, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 5275, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 5276, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 5277, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 5278, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 5279, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 5280, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 5281, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 5282, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 5283, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (256, 5284, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 5285, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 5286, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 5287, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (58, 5288, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (299, 5289, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (77, 5290, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 5291, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 5292, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 5293, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 5294, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 5718, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 5719, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (53, 5720, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (85, 5721, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 5722, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 5723, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 5724, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (174, 5725, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (133, 5726, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (62, 5727, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6473, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (72, 5729, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 5730, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6432, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 5732, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 5733, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 5892, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 5893, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 5894, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 5895, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 5896, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 5897, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 5898, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 5899, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 5900, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 5901, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 5902, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 5903, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 5904, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 5905, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 5908, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 5907, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 5909, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 5910, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 5911, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 5912, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 5913, 1.6699999570846558);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 5914, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 5915, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5916, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5917, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5918, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5919, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5920, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5921, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5922, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 5923, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 5924, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 5925, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 5926, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 5927, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 5928, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 5929, 1.6699999570846558);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5930, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5931, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5932, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5933, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5934, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5935, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5936, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5937, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 5938, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5939, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5940, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5941, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5942, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5943, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5944, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5945, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 5946, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5947, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5948, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5949, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5950, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5951, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5952, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5953, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 5954, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 5955, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 5956, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 5957, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 5958, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 5959, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 5960, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6433, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 5962, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 5963, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 5964, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 5965, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 5966, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 5967, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 5968, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 5969, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 5970, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 5971, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 5972, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 5973, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 5974, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 5975, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5976, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5977, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5978, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5979, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5980, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5981, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5982, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 5983, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5984, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5985, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5986, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5987, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5988, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5989, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 5990, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5991, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5992, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5993, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5994, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5995, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5996, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5997, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 5998, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 5999, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 6000, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 6001, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 6002, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 6003, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 6004, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 6005, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 6006, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 6007, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 6008, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 6009, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 6010, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 6011, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6434, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 6013, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6014, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6015, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6016, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6017, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7908, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6019, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6020, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6021, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6435, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7909, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6024, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6025, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6026, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6027, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6028, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6029, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6030, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6031, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6436, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7910, 95.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (570, 6034, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6437, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6036, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6037, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6038, 45.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6039, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 7977, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6041, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6042, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6043, 65.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6438, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6045, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6046, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6047, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6048, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6049, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6050, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6051, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6052, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7978, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6439, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 8301, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (571, 6056, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6440, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6441, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6442, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6443, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6444, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6445, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 8302, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 8303, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 8304, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 8305, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 8306, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 8307, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6474, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6453, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6454, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6455, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6456, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6457, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6458, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6459, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6460, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6461, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6462, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6463, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6464, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6465, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6466, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 8308, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 8309, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 8310, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 8311, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 8312, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 8313, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6475, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6476, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6477, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6478, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6479, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6480, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6481, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6482, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6483, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6484, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 8314, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 8315, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 8316, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 8317, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 8318, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 8319, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 8320, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 8321, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6493, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6494, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6495, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6496, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6497, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6498, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 8322, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 8323, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 8324, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 8325, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 8326, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 8327, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 8328, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 8329, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 8330, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6508, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6509, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6510, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6511, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6512, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6513, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6514, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6515, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6516, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6517, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6518, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6519, 0.004999999888241291);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6520, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6521, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6522, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6523, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6524, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6525, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 8331, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8415, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8416, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8417, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8418, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8419, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8420, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8421, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8422, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8423, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6536, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6537, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6538, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6539, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6540, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6541, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6542, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6543, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6544, 0.004999999888241291);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6545, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6546, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6547, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6548, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6549, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6550, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (757, 8424, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8425, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8426, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8427, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8428, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8429, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8430, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8431, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8432, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (613, 6562, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (504, 5214, 1.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (504, 5215, 1.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (505, 5216, 1.75);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (505, 5217, 1.75);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (506, 5218, 2.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (506, 5219, 2.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (507, 5220, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (507, 5221, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5488, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5489, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5490, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5491, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5492, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5493, 0.75);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 5494, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5511, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5496, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5497, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5498, 0.003000000026077032);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5499, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5500, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 5501, 0.75);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5512, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5503, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5504, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5505, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5506, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5507, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5508, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5509, 0.550000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 5510, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5513, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5514, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5515, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5516, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5517, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5518, 0.8399999737739563);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 5519, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5520, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5521, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5522, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5594, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5524, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5525, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5526, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 5527, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5528, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5529, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5530, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5531, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5532, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5533, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 5534, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5535, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5536, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5537, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5538, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5539, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5540, 0.004000000189989805);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 5541, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 5542, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 5543, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 5544, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 5545, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 5546, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 5547, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 5548, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 5549, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 5550, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 5551, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 5552, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 5553, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5554, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5555, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5556, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5557, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5558, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5559, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5560, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 5561, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 5562, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 5563, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 5564, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 5565, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 5566, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 5567, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 5568, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 5569, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 5570, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 5571, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 5572, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 5573, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 5574, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 5575, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 5576, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 5577, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (616, 6564, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 5579, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 5580, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 5581, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 5582, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 5583, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (615, 6565, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 5585, 0.05999999865889549);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 5586, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5587, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5588, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5589, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5590, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5591, 0.004999999888241291);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 5592, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (617, 6566, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5734, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5735, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5736, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5737, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5738, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5739, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5740, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5741, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5742, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5743, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5744, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5745, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 5746, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 6812, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 6813, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 6814, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 6815, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 6816, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 6817, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 6966, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 6967, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (201, 6820, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (56, 6821, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 6822, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 6823, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (71, 6824, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (282, 6825, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (317, 6826, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (332, 6827, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (360, 6968, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (297, 6969, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 6970, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (748, 8252, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8359, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (758, 8433, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8434, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8435, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8436, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8437, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8438, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8439, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8440, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8441, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (759, 8442, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8443, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8444, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8445, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8446, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8447, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (629, 6890, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (629, 6891, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (629, 6892, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (629, 6893, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (629, 6894, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8448, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8449, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8452, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (760, 8451, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (630, 6901, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (630, 6902, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (630, 6903, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8453, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (630, 6905, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (630, 6906, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8454, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8455, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8456, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (631, 6911, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (631, 6912, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (631, 6913, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (631, 6914, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (631, 6915, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8457, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8458, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8459, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8460, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (632, 6926, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (632, 6921, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8461, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (761, 8462, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8463, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8464, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8465, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (633, 6928, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (633, 6929, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8466, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8467, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (633, 6932, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (633, 6933, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (633, 6934, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (633, 6935, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8468, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6937, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6938, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6939, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6940, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6941, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6942, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6943, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6944, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6945, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 6946, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8469, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8470, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (635, 6950, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (635, 6951, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (635, 6952, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (635, 6953, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (635, 6954, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8471, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (762, 8472, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8473, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8474, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (636, 6959, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (636, 6960, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (636, 6961, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (636, 6962, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (636, 6965, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8475, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 6971, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 6972, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 6973, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 6974, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (298, 6975, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 6976, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8476, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8477, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8478, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8479, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8480, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (763, 8481, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8482, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8483, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8484, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8485, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8486, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8487, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8488, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8489, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8490, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (764, 8491, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8492, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8493, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8496, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8495, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8497, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8498, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8499, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8500, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (765, 8501, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 8502, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7582, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7583, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7584, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7597, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7587, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7588, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7589, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 8504, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7591, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7592, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 7593, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 8505, 0.0010000000474974513);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 8506, 0.0010000000474974513);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 8507, 0.0010000000474974513);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7599, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7600, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7601, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7602, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7603, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7604, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7605, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7606, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 7607, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 8508, 0.006000000052154064);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 8509, 0.006000000052154064);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 8510, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 8511, 0.007000000216066837);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (691, 7612, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (691, 7613, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (691, 7614, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (691, 7615, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 8512, 0.007000000216066837);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8770, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (691, 7618, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (691, 7619, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (691, 7620, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (692, 7630, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (692, 7631, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (692, 7634, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7952, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7914, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7915, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7916, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7917, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7918, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7919, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7920, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7921, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7922, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7923, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7926, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7927, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7928, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7929, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7932, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7933, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7934, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7935, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (127, 5596, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (46, 5597, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (243, 5598, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (55, 5599, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (171, 5600, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (43, 5601, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (88, 5602, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (64, 5603, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (73, 5604, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (143, 5605, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 5606, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (60, 5607, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (90, 5608, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (342, 7129, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7693, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (126, 5611, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7936, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7937, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (41, 5614, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (279, 5615, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (293, 5616, 35.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (294, 5617, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7938, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7939, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 5620, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7969, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (724, 7970, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7942, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7943, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7944, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7945, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7946, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7947, 70.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7948, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7949, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7971, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (723, 7972, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (326, 5681, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (461, 5682, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (330, 5688, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (172, 5693, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (97, 5694, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (399, 5695, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (82, 5696, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (372, 5698, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 5699, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (592, 6391, 0.12999999523162842);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (593, 6391, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (594, 6391, 0.23999999463558197);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (595, 6391, 0.2800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (596, 6391, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (597, 6391, 0.38999998569488525);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (598, 6391, 0.4399999976158142);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (599, 6391, 0.5899999737739563);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (600, 6391, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (601, 6391, 0.8799999952316284);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (602, 6391, 1.1699999570846558);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (603, 6391, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (604, 6391, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (605, 6391, 7.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (606, 6391, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (607, 6391, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (608, 6391, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7953, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7973, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7955, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (722, 7974, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7957, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7958, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7959, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7960, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7975, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7962, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (725, 7976, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7964, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7965, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7966, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7967, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7968, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7983, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7989, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7991, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8203, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8204, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8205, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8206, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8207, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8208, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8209, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8210, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8211, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8212, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8213, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8214, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8215, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8216, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8217, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8218, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8361, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 8523, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 8525, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 8544, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 8546, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 8754, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 8755, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (547, 8771, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8773, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8774, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8775, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8776, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8777, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8778, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8779, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8780, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8781, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8782, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8783, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (784, 8784, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8785, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (746, 8245, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (383, 5749, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6837, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6838, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6839, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6840, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6841, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6842, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6843, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6844, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6845, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 6846, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 6847, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 6848, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (302, 6849, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 6850, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 6851, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 6852, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 6853, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (512, 6854, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 6855, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 6856, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 6857, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (516, 6858, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (517, 6859, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 6860, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 6861, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 6862, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 6863, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 6864, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 6865, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (524, 6866, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (525, 6867, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (526, 6868, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 6869, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (304, 7071, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (638, 6980, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (308, 7072, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (341, 7069, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (343, 7070, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 7138, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 7139, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 7140, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 7141, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 7142, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 7143, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (556, 7144, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 7145, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 7146, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 7147, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 7148, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 7149, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 7150, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 7151, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 7152, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 7153, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (464, 7154, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 7155, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 7156, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 7157, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 7158, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 7159, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 7160, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 7161, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (512, 7162, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (513, 7163, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (514, 7164, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (516, 7165, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (518, 7166, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (517, 7167, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 7168, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (522, 7169, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (524, 7170, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 7171, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (526, 7172, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (525, 7173, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 7174, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 7175, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 7176, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 7177, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 7178, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 7179, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 7180, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 7181, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 7182, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 7183, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 7184, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 7185, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 7186, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 7187, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 7188, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (701, 7712, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (702, 7713, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 8526, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (327, 8527, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 8528, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 8547, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 8548, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 8551, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 8552, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 8553, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8554, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 8555, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8556, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8557, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8558, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8559, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8560, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8561, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8562, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8563, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8564, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8565, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8566, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8567, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8568, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8569, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8570, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (772, 8571, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8572, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8573, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8574, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8575, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8576, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8577, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8578, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8579, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8580, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8581, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8582, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8583, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8584, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8585, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (773, 8586, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8587, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8588, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8589, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8590, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8591, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8592, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8593, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8594, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8595, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8596, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8597, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8598, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8599, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8600, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8601, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8602, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8603, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (774, 8604, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8605, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8606, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8607, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8608, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8609, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8610, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8611, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8612, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8613, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8614, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8615, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8616, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8617, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8618, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8619, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8620, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8621, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8622, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (775, 8623, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8624, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8625, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8626, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8627, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8628, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8629, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8630, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8631, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8632, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8633, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8634, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8635, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8636, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8637, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8638, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8639, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8640, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8641, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8642, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (776, 8643, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8644, 0.014999999664723873);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8645, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8646, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8647, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8648, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8649, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8650, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8651, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8652, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8653, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8654, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8655, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8656, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8657, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (777, 8658, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8659, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8660, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8661, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8662, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8663, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8664, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8665, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8666, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8667, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8668, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8669, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8670, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8671, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8672, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (778, 8673, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8674, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8675, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8676, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8677, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8678, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8679, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8680, 0.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8681, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8682, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8683, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8684, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8685, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8686, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8687, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8688, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8689, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8690, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (779, 8691, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8692, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8693, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8694, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8695, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8696, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8697, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8698, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8699, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8700, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8701, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8702, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8703, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8704, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8705, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8706, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8707, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8708, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (780, 8709, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8710, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8711, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8712, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8713, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8714, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8715, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8716, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8717, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8718, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8719, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8720, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8721, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8722, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8723, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8724, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8725, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8726, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (781, 8728, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (782, 8729, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (548, 5775, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (548, 5776, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (548, 5777, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (548, 5778, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (548, 5779, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (548, 5780, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (548, 5781, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5782, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5783, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5784, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5785, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5786, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5787, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5788, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5789, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5790, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5791, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (549, 5792, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5793, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5794, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5795, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5796, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5797, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5798, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5799, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5800, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5801, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5802, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (550, 5803, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5804, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5805, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5806, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5807, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5808, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5809, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5810, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5811, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5812, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5813, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5814, 1.2000000476837158);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5815, 0.8999999761581421);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5816, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5817, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5818, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 6057, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (551, 5820, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (552, 5832, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (552, 5833, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6115, 0.003000000026077032);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6116, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6117, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6118, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6119, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6120, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6121, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6122, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6123, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6124, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6125, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6126, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6127, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6128, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6129, 0.003000000026077032);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6130, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6131, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6132, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6133, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6134, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6135, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6136, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6137, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6138, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6139, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6140, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (576, 6141, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6142, 0.003000000026077032);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6143, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6144, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6145, 0.09000000357627869);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6146, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6147, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6148, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6149, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6150, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6151, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6152, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6153, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6154, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (577, 6155, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (438, 6160, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 6157, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (437, 6158, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (439, 6162, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 6163, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (440, 6164, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (441, 6165, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (445, 6166, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (446, 6167, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (443, 6168, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (442, 6169, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (444, 6170, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (447, 6171, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (578, 6173, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (578, 6174, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (578, 6175, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (578, 6176, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (578, 6177, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (578, 6178, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (74, 4018, 0.800000011920929);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (581, 6194, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (581, 6196, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (581, 6197, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (581, 6198, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (581, 6199, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6834, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (575, 6836, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (247, 6870, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 6871, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (584, 6374, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (584, 6375, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (582, 6376, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (639, 6981, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (583, 6377, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (640, 6982, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (585, 6378, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (586, 6379, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (637, 6983, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (747, 8247, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (515, 8248, 6.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (587, 6380, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (588, 6381, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (519, 8249, 6.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (523, 8250, 6.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (588, 6382, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (589, 6383, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (590, 6384, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (591, 6385, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (527, 8251, 6.25);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (642, 8088, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (163, 6390, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (520, 6429, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (521, 6430, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (614, 6563, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (278, 6803, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 6804, 12.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (748, 8260, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (748, 8261, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (748, 8262, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (748, 8263, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8264, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8265, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8266, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (350, 6805, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (349, 6806, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (339, 6807, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8267, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8268, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8269, 60.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8270, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (671, 7532, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (224, 6809, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8271, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (245, 6810, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (184, 8336, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (183, 8337, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8274, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (233, 6811, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8275, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8276, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8277, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8278, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (397, 6984, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8279, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8280, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8281, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8282, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (749, 8283, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8285, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8286, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8287, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8288, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8289, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8290, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8291, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8292, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8293, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (646, 7039, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (646, 7040, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (646, 7041, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (750, 8294, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (169, 8338, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (164, 8339, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (165, 8340, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (66, 8341, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (193, 8342, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (362, 8343, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (220, 8344, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (232, 8345, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (363, 8346, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (231, 8347, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (148, 8348, 0.004999999888241291);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (371, 8349, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (159, 8350, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (276, 8351, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (647, 7057, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (365, 8352, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (366, 8353, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (316, 8354, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (357, 8355, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (353, 8356, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (307, 8357, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (334, 8358, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 6321, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 6322, 75.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (329, 7073, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 7074, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 7075, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 7076, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 7077, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 7078, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 7079, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (541, 7080, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 7081, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 7082, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 7083, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 7084, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (543, 7085, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (544, 7086, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (545, 7087, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (560, 7088, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (554, 7089, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (555, 7090, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (559, 7091, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (553, 7092, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (557, 7093, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (558, 7094, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (565, 7095, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (562, 7096, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (563, 7097, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (566, 7098, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (561, 7099, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (564, 7100, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (568, 7101, 40.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (567, 7102, 33.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (394, 7103, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (396, 7104, 14.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (393, 7105, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (391, 7106, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (392, 7107, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (395, 7108, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (464, 7109, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (465, 7110, 36.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (466, 7111, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (467, 7112, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (470, 7113, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 7114, 36.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 7115, 36.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (468, 7116, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (472, 7117, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 7118, 36.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (473, 7119, 36.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (475, 7120, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (476, 7121, 36.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (477, 7122, 18.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (478, 7123, 36.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 7124, 36.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (351, 7125, 0.02500000037252903);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (359, 7126, 0.07500000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (483, 7128, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (670, 7533, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (672, 7534, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7535, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (661, 7536, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8411, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8412, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8413, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (756, 8414, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7646, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7647, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7648, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7649, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 7650, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7651, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7652, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7653, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7980, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7655, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7656, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7657, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7658, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 7659, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7660, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7661, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7662, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7663, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7664, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7665, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7666, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7667, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 7668, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7669, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7670, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7671, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7672, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7673, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7674, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7675, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 7676, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7677, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7678, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7679, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7680, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7681, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7682, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7683, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7684, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 7685, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7686, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7687, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7688, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7689, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 7690, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8763, 1.100000023841858);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8764, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8765, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8766, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8767, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8768, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (771, 8769, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (786, 8772, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7747, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7748, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7749, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7750, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7751, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7752, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7753, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7754, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7755, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 7982, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7757, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7758, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7759, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7760, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7761, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (715, 7762, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7763, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7764, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7765, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7766, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7767, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7768, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7769, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7770, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7771, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (529, 7985, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (530, 7986, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7774, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7775, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7776, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7777, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7778, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (716, 7779, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7780, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7781, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7782, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7783, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7784, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7785, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7786, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7787, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7990, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7992, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7790, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7791, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7792, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7793, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7794, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (717, 7795, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7796, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7797, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7798, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7799, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7800, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7801, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7802, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7803, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7804, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7806, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7807, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7808, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7809, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7810, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (718, 7811, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7812, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7994, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7814, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7815, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7995, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7817, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7818, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7819, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7820, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7996, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7822, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7823, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7824, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7825, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7826, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (719, 7827, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7997, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (720, 7829, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (726, 7998, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 7999, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 8000, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (721, 7833, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 8001, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 8002, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 8004, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 8005, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 8006, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 8007, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (727, 8008, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8009, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8010, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8011, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8012, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8013, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8014, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8015, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8016, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8017, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (728, 8018, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (729, 8019, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (729, 8020, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (729, 8021, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (729, 8023, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (729, 8024, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (729, 8025, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (729, 8026, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (729, 8027, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8028, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8029, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8030, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8031, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8033, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8034, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8035, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8036, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (730, 8037, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8038, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8039, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8040, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8041, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8043, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8044, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8045, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8046, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (731, 8047, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8048, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8049, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8050, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8051, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8052, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8053, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8054, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8055, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8056, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (732, 8057, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (733, 8058, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (733, 8059, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (733, 8060, 0.15000000596046448);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (733, 8062, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (733, 8063, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (733, 8064, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (733, 8065, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (733, 8066, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (734, 8067, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (735, 8068, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (642, 8089, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (626, 8090, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (626, 8091, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (626, 8092, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (626, 8093, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (627, 8094, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (627, 8095, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (627, 8096, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (627, 8097, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (627, 8098, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (628, 8099, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (628, 8100, 1.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (628, 8101, 85.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (628, 8102, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8103, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8104, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8105, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8106, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8107, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8108, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8109, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8111, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (448, 8112, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (643, 8113, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (643, 8114, 80.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (643, 8115, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (643, 8116, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (643, 8117, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8118, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8119, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8120, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8121, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8122, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8123, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8124, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8126, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (449, 8127, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (450, 8128, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (450, 8129, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (450, 8130, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (450, 8131, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (450, 8132, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (450, 8134, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (450, 8135, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (451, 8136, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (451, 8137, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (451, 8138, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (451, 8139, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (451, 8140, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (451, 8142, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (451, 8143, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8144, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8145, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8146, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8147, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8148, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8149, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8150, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8151, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (644, 8152, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8153, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8154, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8155, 30.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8156, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8157, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8158, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8159, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8160, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8161, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (645, 8162, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (646, 8163, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (646, 8164, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (646, 8165, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (646, 8166, 50.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (646, 8168, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (629, 8169, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (630, 8170, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (631, 8171, 13.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (647, 8172, 7.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (739, 8173, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (739, 8174, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (740, 8175, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (740, 8176, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (740, 8177, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (740, 8178, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (740, 8179, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (740, 8180, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (741, 8181, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (741, 8182, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (741, 8183, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (741, 8184, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (742, 8185, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (742, 8186, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (742, 8187, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (742, 8188, 90.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (742, 8189, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (742, 8190, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (632, 8191, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (633, 8192, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (689, 8193, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (690, 8194, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (691, 8195, 6.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (692, 8196, 15.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (634, 8197, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (635, 8198, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (636, 8199, 39.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (751, 8360, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (766, 8518, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (767, 8519, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (768, 8520, 8.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (769, 8521, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (770, 8522, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (246, 8524, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (673, 9338, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 8534, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (531, 8535, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 8536, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (532, 8537, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 8538, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 8539, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 8540, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 8541, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 8542, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (534, 8543, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (533, 8545, 2.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 8730, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (117, 8731, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (310, 8732, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (309, 8733, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (479, 8734, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (480, 8735, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (474, 8736, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (471, 8737, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (535, 8738, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (536, 8739, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (537, 8740, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (538, 8741, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (539, 8742, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (540, 8743, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (542, 8744, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (693, 8745, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (694, 8746, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (695, 8747, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (696, 8748, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (697, 8749, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (698, 8750, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (699, 8751, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (700, 8752, 0.009999999776482582);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (787, 8786, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (787, 8787, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (787, 8788, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (788, 8789, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (788, 8790, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (788, 8791, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (788, 8792, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (789, 8793, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (789, 8794, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (789, 8795, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (789, 8796, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (790, 8797, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (790, 8798, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (790, 8799, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (790, 8800, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (790, 8801, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (791, 8802, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (791, 8803, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (791, 8804, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (791, 8805, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (791, 8806, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (791, 8807, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (792, 8808, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (792, 8809, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (792, 8810, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (793, 8811, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (793, 8812, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (793, 8813, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (793, 8814, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (794, 8815, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (794, 8816, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (794, 8817, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (794, 8818, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (795, 8819, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (795, 8820, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (795, 8821, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (795, 8822, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (795, 8823, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (796, 8824, 5.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (796, 8825, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (796, 8826, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (796, 8827, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (796, 8828, 0.07000000029802322);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (796, 8829, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (797, 8830, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (797, 8831, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (797, 8832, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (797, 8833, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (797, 8834, 25.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (798, 8835, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (798, 8836, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (798, 8837, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (798, 8838, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (798, 8839, 20.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8840, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8841, 0.1599999964237213);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8842, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8843, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8844, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8845, 0.1599999964237213);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8846, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8847, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8848, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8849, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8850, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8851, 0.1599999964237213);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8852, 0.3499999940395355);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8853, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8854, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8855, 0.23999999463558197);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8856, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8857, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8858, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8859, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8860, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8861, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8862, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8863, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8864, 0.23999999463558197);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8865, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8866, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8867, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9263, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8869, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8870, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8871, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8872, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8873, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8874, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8875, 0.23999999463558197);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8876, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8877, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8878, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9264, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8880, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8881, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8882, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8883, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8884, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8885, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8886, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8887, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8888, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8889, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8890, 0.23999999463558197);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8891, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8892, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8893, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8894, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8895, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8896, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8897, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8898, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8899, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8900, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8901, 0.23999999463558197);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8902, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8903, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8904, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8905, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8906, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8907, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8908, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8909, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8910, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8911, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8912, 0.23999999463558197);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8913, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8914, 0.18000000715255737);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8915, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8916, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8917, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8918, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8919, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8920, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8921, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8922, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8923, 0.1599999964237213);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8924, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8925, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8926, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8927, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8928, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8929, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8930, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8931, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8932, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8933, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8934, 0.1599999964237213);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8935, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8936, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8937, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8938, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8939, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8940, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8941, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8942, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8943, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8944, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8945, 0.1599999964237213);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8946, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8947, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8948, 0.699999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8949, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8950, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8951, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (799, 8952, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (800, 8953, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (801, 8954, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (802, 8955, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (803, 8956, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (804, 8957, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (805, 8958, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (806, 8959, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (807, 8960, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (808, 8961, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (809, 8962, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (809, 8963, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (809, 8964, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (809, 8965, 0.5);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (809, 8966, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9265, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (809, 8968, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (809, 8969, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (809, 8970, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (810, 8971, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (810, 8972, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (810, 8973, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (810, 8974, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9266, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (810, 8976, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (810, 8977, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8978, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8979, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8980, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8981, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8982, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8983, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8984, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8985, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9267, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8987, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (811, 8988, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (812, 8989, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (812, 8990, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (812, 8991, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (812, 8992, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (812, 8993, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (812, 8994, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9268, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (812, 8996, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (812, 8997, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (813, 8998, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (813, 8999, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (813, 9000, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (813, 9001, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (813, 9002, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (813, 9003, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9269, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (813, 9005, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (813, 9006, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (814, 9007, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (814, 9008, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (814, 9009, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (814, 9010, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (814, 9011, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (814, 9012, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9270, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (814, 9014, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (814, 9015, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (815, 9016, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (815, 9017, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (815, 9018, 0.30000001192092896);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (815, 9019, 0.4000000059604645);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (815, 9020, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9271, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (815, 9022, 0.20000000298023224);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (815, 9023, 1.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9024, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9025, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9026, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9027, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9028, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (816, 9029, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (682, 9339, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (681, 9340, 10.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9032, 0.11999999731779099);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9033, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9034, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9035, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9036, 4.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (680, 9341, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (679, 9342, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9039, 0.07999999821186066);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9040, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9041, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9042, 0.0949999988079071);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (817, 9272, 0.029999999329447746);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (678, 9343, 3.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (677, 9344, 2.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9046, 0.019999999552965164);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9047, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9048, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (819, 9049, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9273, 0.10000000149011612);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9274, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (818, 9275, 0.03999999910593033);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9053, 0.05000000074505806);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9054, 100.0);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9055, 0.6000000238418579);
GO

INSERT INTO [dbo].[DT_DropGroup] ([DGroup], [DDrop], [DPercent]) VALUES (820, 9056, 0.0949999988079071);
GO

