/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMaterialEvolutionGrade
Date                  : 2023-10-07 09:08:48
*/


INSERT INTO [dbo].[TblMaterialEvolutionGrade] ([MGrade], [MProbabilityAdd]) VALUES (1, 0.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionGrade] ([MGrade], [MProbabilityAdd]) VALUES (2, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionGrade] ([MGrade], [MProbabilityAdd]) VALUES (3, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionGrade] ([MGrade], [MProbabilityAdd]) VALUES (4, 20.0);
GO

