/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AbnormalAttrChangeAbnormal
Date                  : 2023-10-07 09:07:30
*/


INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (1, 237, 515, 1, 508, '디텍션(237)이 걸릴 때 투명상태(508)이 존재하면 디', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (2, 237, 516, 1, 509, '디텍션(237)이 걸릴 때 투명상태(509)이 존재하면 디?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (3, 237, 517, 1, 511, '디텍션(237)이 걸릴 때 투명상태(511)이 존재하면 디?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (4, 237, 516, 1, 236, '디텍션(237)이 걸릴 때 인비지빌리티(236)이 존재하면', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (117, 489, 723, 1, 0, '489(레비)를 사용 할 때 =변신중= 상태이상이 있으면 ', 18);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (118, 258, 997, 1, 258, '베놈1단계(258)이 걸릴때 베놈1단계(258)이 존재하면 베놈2단계(997)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (119, 258, 998, 1, 997, '베놈1단계(258)이 걸릴때 베놈2단계(997)이 존재하면 베놈3단계(998)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (123, 1419, 1420, 1, 1419, '에임드샷 1단계(1419)이 걸릴때 에임드샷 1단계(1419)이 존재하면 에임드샷 2단계(1420)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (124, 1419, 1429, 1, 1420, '에임드샷 1단계(1419)이 걸릴때 에임드샷 2단계(1420)이 존재하면 에임드샷 3단계(1429)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (125, 1436, 1437, 1, 1436, '소닉 방어력 1단계(1436)이 걸릴때 소닉 방어력 1단계(1436)이 존재하면 소닉 방어력 2단계(1437)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (126, 1436, 1438, 1, 1437, '소닉 방어력 1단계(1436)이 걸릴때 소닉 방어력 2단계(1437)이 존재하면 소닉 방어력 3단계(1438)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (128, 1068, 1894, 1, 1411, '불화살 1레벨 있을때 폭발 1레벨 걸리면 폭발 11레벨로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (129, 1069, 1894, 1, 1411, '불화살 1레벨 있을때 폭발 2레벨 걸리면 폭발 11레벨로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (130, 1070, 1894, 1, 1411, '불화살 1레벨 있을때 폭발 3레벨 걸리면 폭발 11레벨로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (131, 1071, 1894, 1, 1411, '불화살 1레벨 있을때 폭발 4레벨 걸리면 폭발 11레벨로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (132, 1072, 1894, 1, 1411, '불화살 1레벨 있을때 폭발 5레벨 걸리면 폭발 11레벨로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (133, 57, 2029, 1, 2025, '사일런스(57) 걸릴때 시전 모션(메테오 - 사일런스)(2025)이 존재하면, 실패 모션(메테오 - 사일런스)(2029)으로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (134, 58, 2029, 1, 2025, '사일런스(58) 걸릴때 시전 모션(메테오 - 사일런스)(2025)이 존재하면, 실패 모션(메테오 - 사일런스)(2029)으로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (135, 59, 2029, 1, 2025, '사일런스(59) 걸릴때 시전 모션(메테오 - 사일런스)(2025)이 존재하면, 실패 모션(메테오 - 사일런스)(2029)으로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (136, 60, 2029, 1, 2025, '사일런스(60) 걸릴때 시전 모션(메테오 - 사일런스)(2025)이 존재하면, 실패 모션(메테오 - 사일런스)(2029)으로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (137, 241, 2030, 1, 2026, '스운(241) 걸릴때 시전 모션(파이어 - 스운)(2026)이 존재하면, 실패 모션(파이어 - 스운)(2030)으로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (138, 488, 2020, 1, 2013, '아머 브레이크(488) 걸릴때 골렘 소환 5레벨(2013)이 존재하면, 골렘 소환 4레벨 스킬 사용(2020)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (139, 488, 2021, 1, 2014, '아머 브레이크(488) 걸릴때 골렘 소환 4레벨(2014)이 존재하면, 골렘 소환 3레벨 스킬 사용(2021)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (140, 488, 2022, 1, 2015, '아머 브레이크(488) 걸릴때 골렘 소환 3레벨(2015)이 존재하면, 골렘 소환 2레벨 스킬 사용(2022)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (141, 488, 2023, 1, 2016, '아머 브레이크(488) 걸릴때 골렘 소환 2레벨(2016)이 존재하면, 골렘 소환 1레벨 스킬 사용(2023)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (142, 488, 2024, 1, 2017, '아머 브레이크(488) 걸릴때 골렘 소환 1레벨(2017)이 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (143, 488, 2046, 1, 2041, '아머 브레이크(488) 걸릴때 공격 스킬 대기(메테오 - ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (144, 488, 2048, 1, 2042, '아머 브레이크(488) 걸릴때 공격 스킬 대기(파이어 - 스운)(2042)이 존재하면, 공격 스킬 시전(파이어 - 아머 브레이크)(2048)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (145, 241, 2045, 1, 2041, '스운(241) 걸릴때 공격 스킬 대기(메테오 - 사일런스)(2041)이 존재하면, 공격 스킬 시전(메테오 - 스운)(2045)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (146, 57, 2047, 1, 2042, '사일런스(57) 걸릴때 공격 스킬 대기(파이어 - 스운)(2042)이 존재하면, 공격 스킬 시전(파이어 - 사일런스)(2047)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (147, 58, 2047, 1, 2042, '사일런스(57) 걸릴때 공격 스킬 대기(파이어 - 스운)(2042)이 존재하면, 공격 스킬 시전(파이어 - 사일런스)(2047)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (148, 59, 2047, 1, 2042, '사일런스(57) 걸릴때 공격 스킬 대기(파이어 - 스운)(2042)이 존재하면, 공격 스킬 시전(파이어 - 사일런스)(2047)로 변경', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (149, 60, 2047, 1, 2042, '사일런스(57) 걸릴때 공격 스킬 대기(파이어 - 스운)(', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (151, 602, 1983, 1, 2059, '카사변신(602)일때 4대정령의 혼(2059) 있으면 카사능', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (152, 603, 1987, 1, 2059, '실라페변신(603)일때 4대정령의 혼(2059) 있으면 실라페능력강화(1987)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (153, 604, 1988, 1, 2059, '노움변신(604)일때 4대정령의 혼(2059) 있으면 노움능력강화(1988)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (154, 605, 1989, 1, 2059, '언딘변신(605)일때 4대정령의 혼(2059) 있으면 언딘능력강화(1989)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (155, 614, 1990, 1, 2059, '살라만다변신(614)일때 4대정령의 혼(2059) 있으면 사라만다능력강화(1990)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (156, 615, 1991, 1, 2059, '실프변신(615)일때 4대정령의 혼(2059) 있으면 실프능', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (157, 616, 1992, 1, 2059, '노임변신(616)일때 4대정령의 혼(2059) 있으면 노임능력강화(1992)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (158, 617, 1993, 1, 2059, '운디네변신(617)일때 4대정령의 혼(2059) 있으면 운디네능력강화(1993)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (159, 618, 1994, 1, 2059, '이그니스변신(618)일때 4대정령의 혼(2059) 있으면 이그니스능력강화(1994)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (160, 619, 1995, 1, 2059, '실라이론변신(619)일때 4대정령의 혼(2059) 있으면 실라이론능력강화(1995)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (161, 620, 1996, 1, 2059, '노이아넨변신(620)일때 4대정령의 혼(2059) 있으면 노이아넨능력강화(1996)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (162, 621, 1997, 1, 2059, '운다임변신(621)일때 4대정령의 혼(2059) 있으면 운다임능력강화(1997)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (163, 732, 1998, 1, 2059, '이프리트변신(732)일때 4대정령의 혼(2059) 있으면 이프리트능력강화(1998)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (164, 733, 1999, 1, 2059, '퓨리변신(733)일때 4대정령의 혼(2059) 있으면 퓨리능력강화(1999)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (165, 734, 2000, 1, 2059, '휠카셀변신(734)일때 4대정령의 혼(2059) 있으면 휠카셀능력강화(2000)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (166, 735, 2001, 1, 2059, '슈리엘변신(735)일때 4대정령의 혼(2059) 있으면 슈리엘능력강화(2001)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (167, 736, 2002, 1, 2059, '레프리컨변신(736)일때 4대정령의 혼(2059) 있으면 레프리컨능력강화(2002)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (168, 737, 2003, 1, 2059, '에리스변신(737)일때 4대정령의 혼(2059) 있으면 에리스능력강화(2003)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (169, 738, 2004, 1, 2059, '노아스변신(738)일때 4대정령의 혼(2059) 있으면 노아스능력강화(2004)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (170, 739, 2005, 1, 2059, '시아페변신(739)일때 4대정령의 혼(2059) 있으면 시아페능력강화(2005)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (171, 740, 2006, 1, 2059, '릴리언스변신(740)일때 4대정령의 혼(2059) 있으면 릴리언스능력강화(2006)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (172, 741, 2007, 1, 2059, '엘라임변신(741)일때 4대정령의 혼(2059) 있으면 엘라임능력강화(2007)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (173, 742, 2008, 1, 2059, '레이커변신(742)일때 4대정령의 혼(2059) 있으면 레이커능력강화(2008)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (174, 743, 2009, 1, 2059, '에리세드변신(743)일때 4대정령의 혼(2059) 있으면 에리세드능력강화(2009)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (176, 236, 2233, 1, 2232, '안식1상태(2232)일때 인비지(236)쓰면 인비지2(2233)?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (177, 236, 2234, 1, 2239, '안식2상태(2239)일때 인비지(236)쓰면 인비지2(2234)?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (178, 236, 2235, 1, 2240, '안식3상태(2240)일때 인비지(236)쓰면 인비지2(2235)?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (179, 236, 2236, 1, 2241, '안식4상태(2241)일때 인비지(236)쓰면 인비지2(2236)?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (180, 236, 2237, 1, 2242, '안식5상태(2242)일때 인비지(236)쓰면 인비지2(2237)?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (184, 602, 2261, 1, 2285, '카사변신(602)일때 고대(2285)있으면 카사 강화(2261)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (185, 603, 2262, 1, 2285, '실라변신(603)일때 고대(2285)있으면 실라 강화(2262)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (186, 604, 2263, 1, 2285, '노움변신(604)일때 고대(2285)있으면 노움강화(2263)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (187, 605, 2264, 1, 2285, '언딘변신(605)일때 고대(2285)있으면 언딘강화(2264)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (188, 614, 2265, 1, 2285, '사라변신(614)일때 고대(2285)있으면 사라강화(2265)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (189, 615, 2266, 1, 2285, '실프변신(615)일때 고대(2285)있으면 실프강화(2266)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (190, 616, 2267, 1, 2285, '노임변신(616)일때 고대(2285)있으면 노임강화(2267)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (191, 617, 2268, 1, 2285, '운디변신(617)일때 고대(2285)있으면 운디강화(2268)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (192, 618, 2269, 1, 2285, '이그변신(618)일때 고대(2285)있으면 이그강화(2269)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (193, 619, 2270, 1, 2285, '실라변신(619)일때 고대(2285)있으면 실라강화(2270)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (194, 620, 2271, 1, 2285, '노이변신(620)일때 고대(2285)있으면 노이강화(2271)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (195, 621, 2272, 1, 2285, '운다변신(621)일때 고대(2285)있으면 운다강화(2272)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (196, 732, 2273, 1, 2285, '이프변신(732)일때 고대(2285)있으면 이프강화(2273)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (197, 733, 2274, 1, 2285, '퓨리변신(733)일때 고대(2285)있으면 퓨리강화(2274)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (198, 734, 2275, 1, 2285, '휠카변신(734)일때 고대(2285)있으면 휠카강화(2275)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (199, 735, 2276, 1, 2285, '슈리변신(735)일때 고대(2285)있으면 슈리강화(2276)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (200, 736, 2277, 1, 2285, '레프변신(736)일때 고대(2285)있으면 레프강화(2277)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (201, 737, 2278, 1, 2285, '에리변신(737)일때 고대(2285)있으면 에리강화(2278)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (202, 738, 2279, 1, 2285, '노아변신(738)일때 고대(2285)있으면 노아강화(2279)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (203, 739, 2280, 1, 2285, '시아변신(739)일때 고대(2285)있으면 시아강화(2280)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (204, 740, 2281, 1, 2285, '릴리변신(740)일때 고대(2285)있으면 릴리강화(2281)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (205, 741, 2282, 1, 2285, '엘라변신(741)일때 고대(2285)있으면 엘라강화(2282)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (206, 742, 2283, 1, 2285, '레이변신(742)일때 고대(2285)있으면 레이강화(2283)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (207, 743, 2284, 1, 2285, '세드변신(743)일때 고대(2285)있으면 세드강화(2284)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (234, 2815, 515, 1, 2810, '2레벨디텍(2815)일때 2레벨 투망(2810)이면 60초 디텍(515)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (235, 2816, 515, 1, 2810, '3레벨디텍(2816)일때 2레벨 투망(2810)이면 60초 디텍(515)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (236, 2816, 516, 1, 2811, '3레벨디텍(2816)일때 3레벨 투망(2811)이면 30초 디텍(516)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (237, 2245, 515, 1, 2810, '투명해제(2245)일때 2레벨 투망(2810)이면 60초 디텍(515)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (238, 2245, 516, 1, 2811, '투명해제(2245)일때 3레벨 투망(2811)이면 30초 디텍(516)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (239, 2245, 517, 1, 2812, '투명해제(2245)일때 4레벨 투망(2812)이면 20초 디텍(517)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (240, 3140, 3141, 1, 0, '레비(3140)를 사용 할 때 변신중 상태이상이면 변신중', 18);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (242, 3096, 3243, 1, 2232, '안식1단계일때인비지강화쓰면 5분 안식인비지 1', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (243, 3096, 3244, 1, 2239, '안식2단계일때인비지강화쓰면 5분 안식인비지 2', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (244, 3096, 3245, 1, 2240, '안식3단계일때인비지강화쓰면 5분 안식인비지 3', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (245, 3096, 3246, 1, 2241, '안식4단계일때인비지강화쓰면 5분 안식인비지 4', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (246, 3096, 3247, 1, 2242, '안식5단계일때인비지강화쓰면5분 안식인비지 5', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (247, 3100, 2020, 1, 2013, '특성강화아머 걸릴때 골렘소환 5레벨 실패', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (248, 3100, 2021, 1, 2014, '특성강화아머 걸릴때 골렘소환 4레벨 실패', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (249, 3100, 2022, 1, 2015, '특성강화아머 걸릴때 골렘소환 3레벨 실패', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (250, 3100, 2023, 1, 2016, '특성강화아머 걸릴때 골렘소환 2레벨 실패', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (251, 3100, 2024, 1, 2017, '특성강화아머 걸릴때 골렘소환 1레벨 실패', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (252, 3100, 2046, 1, 2041, '특성강화아머 걸릴때 공격 스킬대기(메테오)', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeAbnormal] ([mID], [mOriginAID], [mEffectAID], [mConditionType], [mConditionAID], [mDesc], [mConditionAType]) VALUES (253, 3100, 2048, 1, 2042, '특성강화아머 걸릴때 공격 파이어', 0);
GO

