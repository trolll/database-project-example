/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Region
Date                  : 2023-10-07 09:07:32
*/


INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (1, 0, 0, 8, '블랙랜드마을                                  ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (2, 0, 0, 16, '슈바르츠성                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (3, 0, 0, 24, '에쉬번마을                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (4, 0, 0, 32, '푸리에성                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (5, 0, 0, 40, '로덴마을                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (6, 0, 0, 48, '로덴성                                     ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (7, 0, 0, 56, '인버고든마을                                  ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (8, 0, 0, 64, '바이런성                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (9, 0, 0, 72, '낙오자의마을                                  ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (10, 0, 0, 80, '혼돈의신전                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (11, 0, 0, 88, '흑룡의늪                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (12, 0, 0, 96, '질서의 신전                                  ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (13, 0, 0, 112, '왕의무덤                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (14, 0, 0, 120, '어두운동굴                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (15, 0, 0, 128, '망자의 대지                                  ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (16, 0, 0, 136, '애쉬번항구                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (17, 0, 0, 144, '화염의탑                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (18, 0, 0, 152, '콜로세움                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (19, 0, 0, 160, '이름없는동굴                                  ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (20, 0, 0, 168, '붉은계곡                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (21, 0, 0, 184, '약속의돌                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (22, 0, 0, 192, '수중던전                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (23, 0, 0, 200, '승리의여신 거대석상                              ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (24, 0, 0, 208, '고대신전유물                                  ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (25, 0, 0, 216, '딱정벌레구덩이                                 ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (26, 0, 0, 224, '언데드동굴                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (27, 0, 0, 232, '오크캠프                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (28, 0, 0, 240, '거미의 숲                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (29, 0, 0, 248, '그램린 숲                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (30, 0, 0, 4, '놀의 산적 아지트                               ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (31, 0, 0, 12, '머랜서식지                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (32, 0, 0, 20, '라르곤의다리                                  ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (33, 0, 1, 8, '애쉬번                                     ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (34, 0, 1, 16, '인버고든                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (35, 0, 1, 24, '로덴                                      ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (36, 0, 1, 32, '블랙랜드                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (37, 0, 2, 0, '순간이동 불가능                                ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (38, 0, 2, 1, '순간이동 가능                                 ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (39, 0, 2, 2, '기억불가                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (40, 0, 2, 3, '성내부                                     ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (41, 0, 3, 0, '노멀존                                     ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (42, 0, 3, 1, '컴뱃존                                     ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (43, 0, 3, 2, '세이프존                                    ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (44, 30, 0, 8, '동굴거미방                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (45, 30, 0, 16, '난파선                                     ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (46, 131102, 0, 8, '동굴거미방                                   ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (47, 131102, 0, 16, '그램린 서식지                                 ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (48, 50, 0, 8, '애쉬번 아이템 상점                              ');
GO

INSERT INTO [dbo].[DT_Region] ([RegNo], [RegionID], [Type], [ID], [Name]) VALUES (49, 131122, 0, 8, '애쉬번 무기 상점                               ');
GO

