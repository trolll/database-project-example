/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblFieldEventMonsterList
Date                  : 2023-10-07 09:09:31
*/


INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1, 0, 2, 577, 0.0, 0.0, 0.0, 2372, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2, 0, 1, 0, 0.0, 0.0, 0.0, 2407, 397388.15625, 15837.696289, 293219.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (3, 0, 1, 0, 0.0, 0.0, 0.0, 2407, 423315.40625, 17972.412109, 287828.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (4, 0, 1, 0, 0.0, 0.0, 0.0, 2407, 422421.78125, 17780.914063, 254739.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (5, 0, 0, 86, 434497.0625, 18642.064453, 287380.6875, 2379, 434549.5, 18692.337891, 287757.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (6, 0, 0, 86, 435074.59375, 18498.863281, 285599.625, 2379, 434915.96875, 18491.847656, 285581.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (7, 0, 0, 86, 434007.8125, 18451.5625, 283442.6875, 2379, 433848.1875, 18480.757813, 283320.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (8, 0, 0, 86, 433579.125, 18390.398438, 284659.25, 2379, 433414.15625, 18417.605469, 285071.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (9, 0, 0, 86, 432033.0, 18385.529297, 284939.75, 2379, 431692.875, 18381.919922, 284877.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (10, 0, 0, 86, 430097.96875, 18381.671875, 285920.71875, 2379, 430377.75, 18389.072266, 285950.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (11, 0, 0, 86, 428120.15625, 18314.951172, 284790.84375, 2379, 427705.9375, 18379.601563, 284620.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (12, 0, 0, 86, 428805.0, 18219.621094, 287506.78125, 2379, 428741.0, 18192.742188, 287816.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (13, 0, 0, 86, 425526.8125, 18107.394531, 287399.40625, 2379, 425595.03125, 18121.84375, 287681.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (14, 0, 0, 86, 424300.65625, 18043.333984, 289115.375, 2379, 424229.65625, 18041.109375, 289401.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (15, 0, 0, 86, 422277.71875, 17819.271484, 289254.78125, 2379, 422315.40625, 17808.039063, 289618.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (16, 0, 0, 86, 420080.53125, 17521.080078, 291610.65625, 2379, 420508.9375, 17578.365234, 291591.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (17, 0, 0, 86, 424407.75, 18099.638672, 286552.09375, 2379, 424604.9375, 18179.601563, 286187.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (18, 0, 0, 86, 422941.65625, 18033.998047, 286356.84375, 2379, 422835.75, 18085.253906, 285878.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (19, 0, 0, 86, 421402.1875, 18009.263672, 287096.03125, 2379, 421308.59375, 18000.009766, 287266.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (20, 0, 0, 86, 420179.90625, 17891.285156, 288007.78125, 2379, 419994.53125, 17850.267578, 288331.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (21, 0, 0, 86, 420061.71875, 18008.292969, 285649.9375, 2379, 419849.96875, 18051.638672, 285969.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (22, 0, 0, 86, 421186.84375, 18135.191406, 284417.71875, 2379, 421640.75, 18226.789063, 284317.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (23, 0, 0, 86, 419393.5, 18105.507813, 284166.34375, 2379, 418992.53125, 18182.722656, 283695.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (24, 0, 0, 86, 420385.96875, 18346.693359, 282324.0, 2379, 420411.4375, 18365.023438, 282021.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (25, 0, 0, 86, 418912.6875, 18355.115234, 281844.96875, 2379, 418709.5, 18344.748047, 282033.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (26, 0, 0, 86, 418209.53125, 18346.148438, 280116.21875, 2379, 418120.28125, 18350.347656, 280308.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (27, 0, 0, 86, 419723.15625, 18412.669922, 279252.8125, 2379, 419602.03125, 18405.458984, 278899.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (28, 0, 0, 86, 417552.125, 18350.708984, 277089.34375, 2379, 417475.1875, 18335.144531, 276663.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (29, 0, 0, 86, 420213.84375, 18472.28125, 275991.65625, 2379, 420388.21875, 18478.160156, 276301.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (30, 0, 0, 86, 420927.71875, 18584.689453, 274299.46875, 2379, 420539.53125, 18510.390625, 274124.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (31, 0, 0, 86, 422744.1875, 18775.199219, 273894.96875, 2379, 422949.28125, 18793.152344, 273858.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (32, 0, 0, 86, 418417.53125, 18078.201172, 273378.5625, 2379, 418667.625, 18116.792969, 273569.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (33, 0, 0, 86, 416921.3125, 17968.851563, 273174.71875, 2379, 416999.6875, 17984.191406, 273526.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (34, 0, 0, 86, 417272.375, 17975.369141, 271937.90625, 2379, 417474.4375, 17989.974609, 271559.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (35, 0, 0, 86, 414790.15625, 17855.886719, 268970.46875, 2379, 414828.71875, 17924.580078, 268449.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (36, 0, 0, 86, 414005.96875, 17938.423828, 269940.96875, 2379, 413804.21875, 17992.064453, 269626.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (37, 0, 0, 86, 415746.78125, 17829.371094, 270048.53125, 2379, 415964.21875, 17799.021484, 269974.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (38, 0, 0, 86, 415330.84375, 17723.638672, 264851.03125, 2379, 415102.28125, 17713.779297, 264589.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (39, 0, 0, 86, 416287.21875, 17879.916016, 266695.84375, 2379, 416264.125, 17859.300781, 266923.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (40, 0, 0, 86, 416741.625, 17906.3125, 265262.5625, 2379, 417078.84375, 17944.300781, 265072.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (41, 0, 0, 86, 421682.0625, 18000.0, 265206.53125, 2379, 421512.71875, 18019.658203, 264974.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (42, 0, 0, 86, 420745.78125, 18024.480469, 266810.21875, 2379, 420647.71875, 18000.001953, 266475.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (43, 0, 0, 86, 419522.90625, 18042.4375, 267674.5625, 2379, 419447.6875, 17999.998047, 267357.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (44, 0, 0, 86, 423357.0625, 18068.636719, 265255.9375, 2379, 423579.34375, 18076.064453, 265374.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (45, 0, 0, 86, 422601.78125, 18006.818359, 266763.59375, 2379, 422964.0, 18000.0, 266773.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (46, 0, 0, 86, 420358.53125, 18047.205078, 269306.03125, 2379, 420165.78125, 18049.837891, 269061.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (47, 0, 0, 86, 426153.96875, 18054.847656, 266893.3125, 2379, 425958.3125, 18050.144531, 266601.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (48, 0, 0, 86, 425035.53125, 18079.439453, 268597.5, 2379, 425225.21875, 18013.082031, 268228.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (49, 0, 0, 86, 423992.25, 18047.830078, 268239.21875, 2379, 423719.71875, 18046.978516, 268091.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (50, 0, 0, 86, 410867.1875, 17654.054688, 259387.109375, 2379, 411181.03125, 17709.535156, 259322.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (51, 0, 0, 86, 411338.6875, 17764.724609, 261621.1875, 2379, 411002.40625, 17716.550781, 261638.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (52, 0, 0, 86, 411504.6875, 17814.078125, 263270.96875, 2379, 411294.71875, 17786.78125, 263524.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (53, 0, 0, 86, 412950.0, 17958.164063, 262280.125, 2379, 413385.5625, 17924.085938, 262296.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (54, 0, 0, 86, 426839.75, 17918.519531, 260596.046875, 2379, 426571.84375, 18370.396484, 260525.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (55, 0, 0, 86, 426096.3125, 18008.583984, 262405.25, 2379, 425681.28125, 18093.691406, 262359.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (56, 0, 0, 86, 427632.34375, 18035.53125, 261777.484375, 2379, 427449.0625, 18514.544922, 262054.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (57, 0, 0, 86, 423635.9375, 17890.931641, 258973.109375, 2379, 423911.0, 17943.917969, 258848.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (58, 0, 0, 86, 422588.84375, 18017.150391, 259786.25, 2379, 422380.21875, 18029.701172, 259901.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (59, 0, 0, 86, 419046.84375, 18163.832031, 260961.765625, 2379, 419154.40625, 18100.740234, 260700.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (60, 0, 0, 86, 423124.375, 17999.679688, 260984.640625, 2379, 423115.625, 18028.160156, 261290.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (61, 0, 0, 86, 420668.6875, 18089.496094, 261901.609375, 2379, 421003.0, 18191.527344, 262000.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (62, 0, 0, 86, 423471.1875, 17683.408203, 251913.796875, 2379, 423181.5625, 17685.589844, 251862.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (63, 0, 0, 86, 423705.3125, 17758.710938, 253454.546875, 2379, 423945.4375, 17775.609375, 253386.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (64, 0, 0, 86, 424190.1875, 17814.419922, 254928.1875, 2379, 423969.71875, 17797.789063, 255082.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (65, 0, 0, 86, 424471.28125, 17835.414063, 256680.484375, 2379, 424214.6875, 17833.84375, 257012.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (66, 0, 0, 86, 421056.625, 17786.714844, 254279.46875, 2379, 421446.15625, 17778.832031, 254090.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (67, 0, 0, 86, 419008.53125, 17824.492188, 253887.828125, 2379, 418759.96875, 17825.4375, 253769.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (68, 0, 0, 86, 418904.0625, 17795.96875, 255095.671875, 2379, 419282.875, 17808.015625, 254830.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (69, 0, 0, 86, 416860.8125, 17648.732422, 255130.71875, 2379, 416588.15625, 17631.798828, 255034.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (70, 0, 0, 86, 413830.0, 17633.974609, 255462.609375, 2379, 414107.9375, 17677.175781, 255153.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (71, 0, 0, 86, 420781.1875, 17844.404297, 256631.96875, 2379, 421151.8125, 17861.035156, 256764.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (72, 0, 0, 86, 418874.46875, 17804.613281, 256717.71875, 2379, 419331.0, 17858.310547, 257028.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (73, 0, 0, 86, 417169.375, 17562.173828, 256799.4375, 2379, 417007.8125, 17571.945313, 257104.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (74, 0, 0, 86, 415182.03125, 17733.988281, 256895.65625, 2379, 414638.8125, 17758.050781, 257210.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (75, 0, 0, 86, 418492.53125, 17903.4375, 258440.53125, 2379, 418284.3125, 17904.832031, 258718.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (76, 0, 0, 86, 414637.3125, 17771.644531, 244655.484375, 2379, 415097.21875, 17556.751953, 244372.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (77, 0, 0, 86, 415539.25, 17671.023438, 246974.828125, 2379, 415981.15625, 17530.164063, 246833.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (78, 0, 0, 86, 413909.5625, 17925.267578, 246338.5625, 2379, 413720.96875, 17949.5625, 246488.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (79, 0, 0, 86, 415327.65625, 17892.882813, 249154.921875, 2379, 415672.71875, 17883.255859, 249484.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (80, 0, 0, 86, 413426.6875, 18066.054688, 248503.828125, 2379, 413236.71875, 18076.421875, 248303.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (81, 0, 0, 127, 423153.84375, 18274.390625, 271322.03125, 2390, 423650.1875, 18297.671875, 271406.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (82, 0, 0, 127, 421789.375, 18395.023438, 272112.15625, 2390, 421200.0625, 18274.96875, 272080.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (83, 0, 0, 127, 418960.9375, 17992.179688, 270242.125, 2390, 419404.125, 17999.140625, 270280.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (84, 0, 0, 127, 416434.0, 17851.949219, 271285.84375, 2390, 415853.1875, 17849.433594, 271449.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (85, 0, 0, 127, 424332.59375, 18093.171875, 269143.90625, 2390, 423971.0, 18096.550781, 269555.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (86, 0, 0, 127, 426433.1875, 18171.728516, 265958.34375, 2390, 427022.46875, 18262.160156, 266024.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (87, 0, 0, 127, 424616.65625, 18037.988281, 266905.75, 2390, 424360.96875, 18044.34375, 266950.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (88, 0, 0, 127, 422736.96875, 18047.429688, 267865.90625, 2390, 423259.375, 18059.783203, 268036.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (89, 0, 0, 127, 421512.46875, 18020.335938, 266744.15625, 2390, 421109.84375, 18018.976563, 266572.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (90, 0, 0, 127, 419843.53125, 18020.167969, 268564.9375, 2390, 419323.1875, 18000.0, 268592.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (91, 0, 0, 127, 417720.9375, 18033.666016, 267864.71875, 2390, 418152.125, 18032.511719, 267728.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (92, 0, 0, 127, 416717.90625, 17900.917969, 268645.34375, 2390, 416169.375, 17852.998047, 268639.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (93, 0, 0, 127, 425562.40625, 18041.972656, 264531.15625, 2390, 426105.71875, 18124.761719, 264468.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (94, 0, 0, 127, 422296.5, 18061.804688, 264462.53125, 2390, 422717.75, 18104.804688, 263951.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (95, 0, 0, 127, 419166.8125, 18005.730469, 264884.46875, 2390, 419564.5, 18050.0, 264749.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (96, 0, 0, 127, 415973.90625, 17749.667969, 264060.5625, 2390, 416511.5, 17848.583984, 263730.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (97, 0, 0, 127, 415948.90625, 17823.548828, 265689.75, 2390, 415648.40625, 17776.1875, 266092.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (98, 0, 0, 127, 414419.875, 17873.898438, 265440.3125, 2390, 414086.40625, 17946.257813, 265860.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (99, 0, 0, 127, 412796.8125, 17975.226563, 263203.28125, 2390, 413206.21875, 17926.316406, 263350.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (100, 0, 0, 127, 410821.8125, 17765.261719, 263850.5, 2390, 410581.96875, 17798.796875, 264213.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (101, 0, 0, 127, 411556.90625, 17957.693359, 264613.34375, 2390, 412223.25, 17997.328125, 264937.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (102, 0, 0, 127, 411169.75, 18000.621094, 266674.25, 2390, 410800.03125, 18000.046875, 266627.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (103, 0, 0, 127, 412389.375, 17978.759766, 267680.5, 2390, 412818.28125, 17974.425781, 267700.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (104, 0, 0, 127, 426179.53125, 17842.644531, 258654.640625, 2390, 425909.96875, 18362.460938, 259067.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (105, 0, 0, 127, 423542.375, 18007.759766, 259957.4375, 2390, 424032.84375, 18019.027344, 260087.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (106, 0, 0, 127, 421946.0625, 17956.048828, 259014.390625, 2390, 422227.53125, 17988.976563, 258835.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (107, 0, 0, 127, 420712.65625, 18060.125, 259204.4375, 2390, 420128.09375, 18060.78125, 259144.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (108, 0, 0, 127, 420260.09375, 18056.666016, 260956.0625, 2390, 420008.09375, 18084.013672, 261005.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (109, 0, 0, 127, 418073.78125, 18072.519531, 260651.859375, 2390, 417678.125, 18032.976563, 260942.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (110, 0, 0, 127, 415684.96875, 17629.509766, 261540.765625, 2390, 415962.03125, 17654.902344, 261690.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (111, 0, 0, 127, 414789.78125, 17752.087891, 260768.640625, 2390, 414399.625, 17800.480469, 260805.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (112, 0, 0, 127, 412969.6875, 17951.144531, 260913.515625, 2390, 412648.0625, 17968.818359, 260848.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (113, 0, 0, 127, 415606.0625, 17769.134766, 258100.0, 2390, 415262.71875, 17830.554688, 258248.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (114, 0, 0, 127, 416068.53125, 17635.882813, 259500.609375, 2390, 416501.3125, 17667.271484, 259597.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (115, 0, 0, 127, 418158.84375, 17795.761719, 257474.484375, 2390, 418500.8125, 17860.890625, 257563.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (116, 0, 0, 127, 424827.625, 17915.808594, 255828.484375, 2390, 425314.3125, 17941.078125, 255914.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (117, 0, 0, 127, 421761.25, 17814.376953, 255744.5625, 2390, 422114.59375, 17815.054688, 255958.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (118, 0, 0, 127, 419553.5, 17710.714844, 255975.703125, 2390, 419897.15625, 17793.007813, 256090.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (119, 0, 0, 127, 418356.8125, 17641.830078, 255860.328125, 2390, 417954.40625, 17638.441406, 256083.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (120, 0, 0, 127, 415918.78125, 17541.648438, 256099.359375, 2390, 416263.53125, 17532.103516, 255839.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (121, 0, 0, 127, 414034.46875, 17606.208984, 256323.703125, 2390, 413483.53125, 17603.53125, 256701.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (122, 0, 0, 127, 423004.09375, 17760.279297, 254266.859375, 2390, 423385.25, 17753.136719, 254170.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (123, 0, 0, 127, 420261.25, 17782.611328, 253458.40625, 2390, 420461.3125, 17777.175781, 253371.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (124, 0, 0, 127, 416701.53125, 17892.136719, 253875.875, 2390, 417346.4375, 17882.365234, 253932.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (125, 0, 0, 127, 413236.0, 17921.501953, 253941.015625, 2390, 413499.90625, 17922.894531, 253836.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (126, 0, 0, 127, 411838.96875, 17817.464844, 255086.46875, 2390, 412159.1875, 17868.5625, 255193.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (127, 0, 0, 127, 423990.75, 17692.431641, 251862.8125, 2390, 424582.4375, 17763.111328, 252170.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (128, 0, 0, 127, 420917.3125, 17736.785156, 251659.09375, 2390, 421459.09375, 17727.394531, 251680.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (129, 0, 0, 127, 418941.84375, 17776.097656, 250133.71875, 2390, 419156.875, 17756.853516, 250612.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (130, 0, 0, 127, 416974.71875, 17875.822266, 250796.03125, 2390, 416697.34375, 17879.996094, 251110.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (131, 0, 0, 127, 413835.0, 18072.882813, 252570.625, 2390, 414178.09375, 18054.189453, 252588.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (132, 0, 0, 127, 417842.78125, 17648.353516, 247840.46875, 2390, 418233.53125, 17715.191406, 247937.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (133, 0, 0, 127, 417137.65625, 17696.248047, 248956.625, 2390, 417206.875, 17736.320313, 249298.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (134, 0, 0, 127, 415244.28125, 17915.210938, 248419.859375, 2390, 415575.375, 17839.65625, 248242.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (135, 0, 0, 127, 414090.40625, 18109.195313, 249219.515625, 2390, 413759.40625, 18113.816406, 249457.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (136, 0, 0, 127, 416570.4375, 17225.611328, 243304.890625, 2390, 416888.46875, 17329.632813, 243883.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (137, 0, 0, 127, 414472.90625, 17776.734375, 245686.421875, 2390, 414026.34375, 17875.277344, 245506.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (138, 0, 0, 127, 412742.21875, 17946.839844, 244065.390625, 2390, 413130.40625, 17934.148438, 244309.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (139, 0, 0, 127, 410746.59375, 17942.074219, 243048.96875, 2390, 411055.21875, 17947.980469, 243153.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (140, 0, 0, 127, 411544.375, 17975.447266, 244934.890625, 2390, 411824.03125, 17959.953125, 245175.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (141, 0, 0, 127, 411052.5, 18090.765625, 246522.71875, 2390, 411356.4375, 18094.849609, 246659.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (142, 0, 0, 127, 414769.78125, 17928.525391, 269944.1875, 2390, 414648.59375, 17964.503906, 270175.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (143, 0, 0, 74, 407221.78125, 16420.416016, 294432.46875, 2382, 407156.4375, 16457.648438, 294634.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (144, 0, 0, 74, 407311.75, 16336.552734, 293473.0, 2382, 407318.1875, 16351.361328, 293634.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (145, 0, 0, 74, 407859.0, 16378.078125, 292689.4375, 2382, 407732.125, 16352.59375, 292708.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (146, 0, 0, 74, 407859.25, 16355.766602, 291723.03125, 2382, 407759.90625, 16326.248047, 291830.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (147, 0, 0, 74, 407693.59375, 16366.803711, 291048.28125, 2382, 407630.59375, 16346.613281, 291012.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (148, 0, 0, 74, 408199.5, 16541.337891, 290347.65625, 2382, 408136.78125, 16515.650391, 290123.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (149, 0, 0, 74, 406138.6875, 16284.540039, 294506.6875, 2382, 406144.34375, 16310.712891, 294684.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (150, 0, 0, 74, 406495.0625, 16222.543945, 293457.625, 2382, 406451.15625, 16233.949219, 293599.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (151, 0, 0, 74, 406855.03125, 16187.344727, 292412.40625, 2382, 406892.375, 16201.992188, 292521.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (152, 0, 0, 74, 406707.03125, 16128.816406, 291656.0625, 2382, 406767.5, 16138.304688, 291499.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (153, 0, 0, 74, 406863.96875, 16191.860352, 290652.03125, 2382, 406829.96875, 16181.210938, 290735.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (154, 0, 0, 74, 407189.9375, 16291.805664, 289975.15625, 2382, 407127.28125, 16273.027344, 289788.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (155, 0, 0, 74, 406745.03125, 16295.425781, 288934.78125, 2382, 406795.625, 16321.97168, 288772.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (156, 0, 0, 74, 404749.90625, 16211.864258, 295173.0625, 2382, 404710.1875, 16185.529297, 295112.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (157, 0, 0, 74, 405364.3125, 16174.359375, 294424.4375, 2382, 405382.53125, 16216.068359, 294658.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (158, 0, 0, 74, 405336.5, 16100.773438, 293663.65625, 2382, 405337.1875, 16086.638672, 293635.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (159, 0, 0, 74, 405342.46875, 16029.511719, 292964.375, 2382, 405207.09375, 16000.063477, 292769.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (160, 0, 0, 74, 405645.46875, 15934.055664, 291511.78125, 2382, 405499.28125, 15930.810547, 291582.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (161, 0, 0, 74, 405778.4375, 15963.004883, 290827.9375, 2382, 405694.53125, 15966.799805, 290749.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (162, 0, 0, 74, 405796.53125, 16030.891602, 289930.90625, 2382, 405735.375, 16004.051758, 289991.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (163, 0, 0, 74, 405698.125, 16026.660156, 289189.90625, 2382, 405615.0625, 16052.43457, 289070.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (164, 0, 0, 74, 405917.0, 16279.021484, 288213.625, 2382, 405802.90625, 16228.517578, 288262.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (165, 0, 0, 74, 403772.90625, 16093.068359, 294857.375, 2382, 403650.875, 16089.960938, 295101.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (166, 0, 0, 74, 404083.4375, 16064.172852, 294207.9375, 2382, 403966.1875, 16082.873047, 294275.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (167, 0, 0, 74, 404146.25, 16011.615234, 293261.0625, 2382, 403992.0, 16007.125, 293233.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (168, 0, 0, 74, 404042.625, 15952.135742, 292495.46875, 2382, 403964.15625, 15929.964844, 292312.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (169, 0, 0, 74, 404472.25, 15907.551758, 291724.59375, 2382, 404465.28125, 15898.859375, 291548.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (170, 0, 0, 74, 404685.125, 15916.837891, 290535.875, 2382, 404676.59375, 15913.402344, 290688.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (171, 0, 0, 74, 404268.15625, 15933.800781, 289739.0625, 2382, 404384.71875, 15945.960938, 289643.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (172, 0, 0, 74, 404739.15625, 16032.996094, 288754.25, 2382, 404658.15625, 16045.060547, 288636.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (173, 0, 0, 74, 402424.25, 16084.399414, 294892.09375, 2382, 402474.28125, 16073.413086, 295101.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (174, 0, 0, 74, 403085.71875, 16039.845703, 293904.71875, 2382, 403152.3125, 16036.625, 293818.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (175, 0, 0, 74, 403219.28125, 15923.133789, 292596.375, 2382, 403220.03125, 15937.972656, 292748.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (176, 0, 0, 74, 403371.90625, 15847.041992, 291245.03125, 2382, 403391.34375, 15860.117188, 291376.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (177, 0, 0, 74, 403531.6875, 15872.992188, 290448.9375, 2382, 403482.5, 15882.954102, 290244.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (178, 0, 0, 74, 403217.40625, 15918.162109, 289277.09375, 2382, 403296.90625, 15913.576172, 289319.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (179, 0, 0, 74, 403699.6875, 15982.860352, 288593.96875, 2382, 403684.90625, 16001.857422, 288399.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (180, 0, 0, 74, 403056.0, 16003.209961, 287975.96875, 2382, 402984.125, 16011.290039, 287816.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (181, 0, 0, 74, 402602.21875, 15907.499023, 291824.34375, 2382, 402582.125, 15864.59668, 291718.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (182, 0, 0, 74, 402539.59375, 15858.736328, 290386.1875, 2382, 402455.71875, 15858.958984, 290349.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (183, 0, 0, 74, 402253.875, 15861.948242, 289512.03125, 2382, 402333.375, 15864.864258, 289473.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (184, 0, 0, 74, 402504.8125, 15900.358398, 288764.03125, 2382, 402563.0, 15931.925781, 288661.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (185, 0, 0, 74, 401732.5625, 15897.724609, 291710.75, 2382, 401769.53125, 15905.099609, 291934.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (186, 0, 0, 74, 402023.6875, 15877.675781, 291054.9375, 2382, 401986.4375, 15874.621094, 291011.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (187, 0, 0, 74, 401568.0, 15852.598633, 290316.40625, 2382, 401553.90625, 15847.222656, 290225.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (188, 0, 0, 74, 401682.25, 15868.931641, 289027.5, 2382, 401647.1875, 15848.703125, 289162.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (189, 0, 0, 74, 401219.46875, 15907.967773, 288509.875, 2382, 401227.90625, 15910.558594, 288345.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (190, 0, 0, 74, 400855.6875, 16045.80957, 287358.90625, 2382, 400849.125, 16056.293945, 287252.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (191, 0, 0, 74, 401423.46875, 16053.678711, 286643.59375, 2382, 401283.78125, 16089.681641, 286560.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (192, 0, 0, 74, 400408.21875, 16081.404297, 295300.25, 2382, 400417.53125, 16086.828125, 295487.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (193, 0, 0, 74, 400392.5625, 16018.28125, 294139.21875, 2382, 400423.96875, 16015.070313, 294050.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (194, 0, 0, 74, 400807.4375, 15942.366211, 292816.5625, 2382, 400738.125, 15967.835938, 292919.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (195, 0, 0, 74, 400373.21875, 15919.166016, 291704.28125, 2382, 400293.8125, 15942.367188, 291845.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (196, 0, 0, 74, 400680.65625, 15911.536133, 290731.78125, 2382, 400651.3125, 15913.941406, 290605.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (197, 0, 0, 74, 400545.0, 15880.923828, 289798.84375, 2382, 400479.71875, 15885.229492, 289766.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (198, 0, 0, 74, 400196.46875, 15925.425781, 289015.71875, 2382, 400132.4375, 15912.082031, 289095.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (199, 0, 0, 74, 400222.9375, 16016.972656, 288162.46875, 2382, 400165.25, 15991.415039, 288210.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (200, 0, 0, 74, 400104.28125, 16128.402344, 286847.4375, 2382, 400165.3125, 16134.264648, 286694.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (201, 0, 0, 74, 400201.125, 16167.492188, 285572.375, 2382, 400065.15625, 16176.248047, 285694.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (202, 0, 0, 74, 398193.8125, 15864.928711, 302525.0, 2382, 398241.09375, 15903.639648, 302367.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (203, 0, 0, 74, 398501.21875, 15930.164063, 301107.5625, 2382, 398495.34375, 15905.313477, 301283.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (204, 0, 0, 74, 398721.09375, 15863.856445, 300286.40625, 2382, 398679.4375, 15863.217773, 300112.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (205, 0, 0, 74, 398958.0, 15802.066406, 299180.3125, 2382, 398925.4375, 15795.830078, 299338.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (206, 0, 0, 74, 397107.21875, 15999.28418, 303013.9375, 2382, 397180.8125, 15983.830078, 303213.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (207, 0, 0, 74, 397535.1875, 15972.642578, 302400.34375, 2382, 397419.875, 15948.554688, 302341.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (208, 0, 0, 74, 397609.9375, 15992.353516, 301577.1875, 2382, 397608.84375, 16006.894531, 301462.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (209, 0, 0, 74, 397903.6875, 15952.874023, 300150.15625, 2382, 397941.6875, 15965.158203, 300295.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (210, 0, 0, 74, 397978.78125, 15933.649414, 299353.59375, 2382, 397903.6875, 15945.25, 299240.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (211, 0, 0, 74, 397872.53125, 15988.079102, 298014.03125, 2382, 397771.90625, 15997.089844, 297812.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (212, 0, 0, 74, 398444.4375, 15995.696289, 296889.3125, 2382, 398297.4375, 16054.130859, 296919.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (213, 0, 0, 74, 398991.21875, 15993.862305, 295886.75, 2382, 398935.03125, 16059.367188, 296009.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (214, 0, 0, 74, 399650.03125, 16034.441406, 295156.9375, 2382, 399492.0625, 16035.217773, 295258.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (215, 0, 0, 74, 399094.5, 15963.853516, 294569.71875, 2382, 398999.46875, 15943.421875, 294429.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (216, 0, 0, 74, 399440.84375, 15946.71875, 293775.5625, 2382, 399457.0625, 15936.033203, 293595.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (217, 0, 0, 74, 399906.3125, 15943.544922, 292801.78125, 2382, 399726.53125, 15946.519531, 292825.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (218, 0, 0, 74, 399111.15625, 15950.900391, 292204.8125, 2382, 399048.84375, 15913.395508, 292351.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (219, 0, 0, 74, 399221.4375, 15949.875, 291393.5, 2382, 399219.40625, 15943.238281, 291567.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (220, 0, 0, 74, 399880.3125, 15927.802734, 290983.90625, 2382, 399819.375, 15937.827148, 290816.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (221, 0, 0, 74, 398733.5, 15952.234375, 290820.28125, 2382, 398667.96875, 15993.117188, 291016.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (222, 0, 0, 74, 399284.40625, 15943.40625, 290142.8125, 2382, 399428.65625, 15955.275391, 290062.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (223, 0, 0, 74, 399182.1875, 16035.363281, 289038.375, 2382, 399116.1875, 16059.669922, 288905.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (224, 0, 0, 74, 399065.40625, 16126.057617, 287888.40625, 2382, 398921.125, 16170.250977, 287969.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (225, 0, 0, 74, 399064.625, 16191.263672, 286942.09375, 2382, 399029.125, 16206.941406, 286696.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (226, 0, 0, 74, 396312.625, 15991.285156, 302896.15625, 2382, 396267.0, 16028.657227, 302745.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (227, 0, 0, 74, 396683.28125, 16023.446289, 301840.34375, 2382, 396611.5625, 16028.556641, 302008.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (228, 0, 0, 74, 396805.0625, 16020.390625, 300666.75, 2382, 396714.21875, 16032.861328, 300846.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (229, 0, 0, 74, 396910.65625, 16052.915039, 299132.0, 2382, 396760.375, 16072.098633, 299129.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (230, 0, 0, 74, 396455.65625, 16035.176758, 298457.75, 2382, 396265.21875, 16021.30957, 298445.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (231, 0, 0, 74, 395461.0625, 15926.163086, 303250.0, 2382, 395356.75, 15942.263672, 303107.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (232, 0, 0, 74, 395884.90625, 16051.193359, 301922.9375, 2382, 395906.875, 16089.407227, 302189.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (233, 0, 0, 74, 395943.46875, 16074.387695, 301061.1875, 2382, 395780.59375, 16080.740234, 301210.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (234, 0, 0, 74, 395996.75, 16064.350586, 300005.4375, 2382, 395895.4375, 16043.892578, 300203.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (235, 0, 0, 74, 395628.9375, 16048.733398, 299153.40625, 2382, 395592.96875, 16048.739258, 298914.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (236, 0, 0, 74, 395148.875, 15993.71582, 300549.21875, 2382, 395114.3125, 16001.098633, 300317.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (237, 0, 0, 74, 394354.9375, 15952.068359, 299747.1875, 2382, 394340.6875, 15972.344727, 299966.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (238, 0, 0, 74, 394651.71875, 15706.917969, 304075.4375, 2382, 394623.96875, 15702.510742, 303910.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (239, 0, 0, 74, 394314.5, 15813.439453, 302985.84375, 2382, 394229.25, 15851.318359, 302785.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (240, 0, 0, 74, 393551.03125, 15797.513672, 301648.71875, 2382, 393502.6875, 15935.237305, 301444.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (241, 0, 0, 74, 393015.3125, 15842.473633, 300546.9375, 2382, 393144.5625, 15847.975586, 300366.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (242, 0, 0, 74, 392974.4375, 15861.057617, 299217.96875, 2382, 393134.34375, 15873.456055, 299421.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (243, 0, 0, 74, 393919.65625, 15882.563477, 298317.15625, 2382, 394163.28125, 15906.083984, 298441.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (244, 0, 0, 74, 394894.25, 15890.251953, 297825.0625, 2382, 394667.5, 15893.838867, 297766.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (245, 0, 0, 74, 397196.75, 15944.737305, 296632.09375, 2382, 397190.9375, 15936.056641, 296466.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (246, 0, 0, 74, 397714.53125, 15907.625, 295239.375, 2382, 397518.09375, 15905.040039, 295426.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (247, 0, 0, 74, 397676.8125, 15861.444336, 294326.53125, 2382, 397499.1875, 15855.643555, 294504.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (248, 0, 0, 74, 398550.5, 15868.645508, 293477.28125, 2382, 398364.125, 15870.375, 293764.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (249, 0, 0, 74, 398275.3125, 15859.24707, 292679.6875, 2382, 398198.75, 15872.25293, 292499.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (250, 0, 0, 74, 397819.53125, 15967.976563, 291511.71875, 2382, 397884.75, 15942.952148, 291698.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (251, 0, 0, 74, 397900.9375, 15997.639648, 290597.5625, 2382, 397792.75, 16031.22168, 290808.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (252, 0, 0, 74, 398079.5, 16021.811523, 289724.0, 2382, 397881.8125, 16054.47168, 289820.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (253, 0, 0, 74, 397900.90625, 16071.998047, 288770.96875, 2382, 397824.03125, 16074.859375, 288573.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (254, 0, 0, 74, 398064.375, 16147.995117, 287406.34375, 2382, 397979.59375, 16152.935547, 287571.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (255, 0, 0, 74, 397164.96875, 16068.638672, 287907.375, 2382, 396988.28125, 16070.492188, 287762.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (256, 0, 0, 74, 396489.875, 16055.74707, 287249.84375, 2382, 396256.25, 16036.725586, 287182.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (257, 0, 0, 74, 396846.375, 16076.540039, 289938.875, 2382, 396963.65625, 16046.503906, 290203.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (258, 0, 0, 74, 396360.15625, 16118.874023, 289341.15625, 2382, 396149.96875, 16113.198242, 289355.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (259, 0, 0, 74, 395979.0625, 16076.735352, 288484.53125, 2382, 395780.34375, 16075.233398, 288436.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (260, 0, 0, 74, 397126.03125, 15895.150391, 292127.65625, 2382, 396963.09375, 15918.714844, 292001.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (261, 0, 0, 74, 396501.125, 16026.268555, 291157.21875, 2382, 396327.09375, 16039.53125, 291119.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (262, 0, 0, 74, 395926.3125, 16090.125, 290472.15625, 2382, 395774.65625, 16126.405273, 290514.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (263, 0, 0, 74, 395417.625, 16207.064453, 289843.5, 2382, 395244.75, 16226.483398, 289825.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (264, 0, 0, 74, 394743.15625, 16242.949219, 288793.25, 2382, 394672.3125, 16278.998047, 288969.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (265, 0, 0, 74, 396369.625, 15867.335938, 295803.75, 2382, 396337.03125, 15872.101563, 295980.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (266, 0, 0, 74, 396887.28125, 15848.642578, 295002.53125, 2382, 396848.90625, 15853.5625, 295125.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (267, 0, 0, 74, 396208.5, 15837.617188, 294812.1875, 2382, 396074.96875, 15839.165039, 295027.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (268, 0, 0, 74, 396408.03125, 15833.74707, 294144.0625, 2382, 396283.25, 15824.818359, 294024.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (269, 0, 0, 74, 396330.71875, 15850.738281, 293072.21875, 2382, 396106.75, 15860.00293, 293135.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (270, 0, 0, 74, 395608.34375, 15864.766602, 296376.0, 2382, 395561.71875, 15859.720703, 296291.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (271, 0, 0, 74, 395399.90625, 15843.499023, 295240.78125, 2382, 395335.75, 15839.629883, 295535.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (272, 0, 0, 74, 395323.0625, 15852.019531, 294508.96875, 2382, 395211.75, 15852.810547, 294657.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (273, 0, 0, 74, 395516.1875, 15867.429688, 293729.71875, 2382, 395334.8125, 15875.349609, 293813.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (274, 0, 0, 74, 394882.6875, 16116.123047, 291016.96875, 2382, 395038.46875, 16100.521484, 291129.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (275, 0, 0, 74, 394394.03125, 16214.700195, 290399.8125, 2382, 394290.0, 16206.947266, 290491.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (276, 0, 0, 74, 393912.15625, 16408.753906, 289803.84375, 2382, 393719.28125, 16492.519531, 289764.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (277, 0, 0, 74, 393675.3125, 16165.947266, 291220.625, 2382, 393744.71875, 16137.677734, 291462.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (278, 0, 0, 74, 392709.40625, 16347.931641, 290364.625, 2382, 392744.84375, 16282.992188, 290522.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (279, 0, 0, 74, 393757.53125, 16041.260742, 292693.875, 2382, 393719.15625, 16046.382813, 292630.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (280, 0, 0, 74, 393097.09375, 16081.25293, 292191.28125, 2382, 392878.75, 16089.213867, 292183.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (281, 0, 0, 74, 392433.40625, 16191.602539, 291541.4375, 2382, 392250.15625, 16215.130859, 291457.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (282, 0, 0, 74, 394678.0, 15842.50293, 295169.8125, 2382, 394637.875, 15845.212891, 295344.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (283, 0, 0, 74, 394673.625, 15909.927734, 293932.34375, 2382, 394593.84375, 15902.335938, 294155.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (284, 0, 0, 74, 394227.59375, 15814.867188, 296583.96875, 2382, 394132.875, 15820.496094, 296474.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (285, 0, 0, 74, 393840.5625, 15849.494141, 295213.71875, 2382, 393858.875, 15844.335938, 295367.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (286, 0, 0, 74, 393650.5625, 15961.964844, 293918.3125, 2382, 393679.15625, 15947.120117, 294092.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (287, 0, 0, 74, 391739.6875, 16020.59668, 292432.9375, 2382, 391638.3125, 16019.163086, 292412.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (288, 0, 0, 74, 390612.5, 15872.904297, 292748.125, 2382, 390749.03125, 15892.788086, 292722.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (289, 0, 0, 74, 389972.8125, 15750.431641, 293287.5625, 2382, 389941.53125, 15747.124023, 293436.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (290, 0, 0, 74, 388663.4375, 15648.566406, 294471.4375, 2382, 388757.6875, 15643.697266, 294426.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (291, 0, 0, 74, 392449.78125, 15986.611328, 293155.84375, 2382, 392527.25, 16002.886719, 292934.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (292, 0, 0, 74, 391928.34375, 15942.214844, 293845.40625, 2382, 391737.5625, 15928.134766, 293777.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (293, 0, 0, 74, 391035.21875, 15881.28125, 293764.78125, 2382, 390800.40625, 15868.726563, 293804.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (294, 0, 0, 74, 390010.84375, 15808.546875, 294621.21875, 2382, 389788.46875, 15779.988281, 294664.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (295, 0, 0, 74, 389479.25, 15762.279297, 295827.21875, 2382, 389524.1875, 15764.59082, 295663.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (296, 0, 0, 74, 392513.46875, 15756.675781, 301974.75, 2382, 392428.0625, 15706.435547, 301816.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (297, 0, 0, 74, 392321.75, 15837.214844, 299830.90625, 2382, 392260.375, 15835.967773, 299952.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (298, 0, 0, 74, 392305.28125, 15823.923828, 298803.84375, 2382, 392201.03125, 15809.732422, 298866.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (299, 0, 0, 74, 391436.09375, 15804.275391, 298460.875, 2382, 391511.6875, 15794.341797, 298636.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (300, 0, 0, 74, 390824.15625, 15823.513672, 299023.96875, 2382, 390990.3125, 15790.400391, 299035.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (301, 0, 0, 74, 390821.4375, 15794.400391, 299651.375, 2382, 390934.90625, 15770.441406, 299782.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (302, 0, 0, 74, 392589.0625, 15819.84375, 297351.03125, 2382, 392408.53125, 15829.185547, 297489.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (303, 0, 0, 74, 390113.09375, 15776.09375, 297863.125, 2382, 389878.4375, 15756.760742, 297895.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (304, 0, 0, 74, 391031.46875, 15794.442383, 297344.40625, 2382, 390956.15625, 15792.572266, 297563.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (305, 0, 0, 74, 391752.53125, 15837.492188, 296901.28125, 2382, 391707.8125, 15828.410156, 297090.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (306, 0, 0, 74, 391777.0, 15881.089844, 296109.84375, 2382, 391898.5, 15876.74707, 296167.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (307, 0, 0, 74, 391819.8125, 15926.064453, 295132.96875, 2382, 391873.5625, 15926.099609, 295274.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (308, 0, 0, 74, 392498.96875, 15906.270508, 295060.0625, 2382, 392593.5625, 15887.910156, 295126.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (309, 0, 0, 74, 392632.15625, 15949.020508, 294323.75, 2382, 392769.3125, 15949.05957, 294456.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (310, 0, 0, 74, 389905.8125, 15761.645508, 297090.28125, 2382, 389813.21875, 15755.71582, 297002.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (311, 0, 0, 74, 390663.125, 15801.267578, 296849.84375, 2382, 390575.375, 15799.9375, 296815.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (312, 0, 0, 74, 390702.78125, 15823.530273, 296076.625, 2382, 390895.125, 15837.96875, 296149.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (313, 0, 0, 74, 390866.0625, 15862.131836, 295061.1875, 2382, 390751.6875, 15870.570313, 295219.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (314, 1, 2, 540, 0.0, 0.0, 0.0, 2384, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (315, 1, 1, 0, 0.0, 0.0, 0.0, 2492, 306990.21875, 14911.675781, 214331.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (316, 1, 1, 0, 0.0, 0.0, 0.0, 2492, 376195.84375, 15191.352539, 233120.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (317, 1, 1, 0, 0.0, 0.0, 0.0, 2492, 356713.84375, 19053.980469, 252585.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (318, 1, 0, 841, 351165.875, 18622.173828, 224786.96875, 2391, 351451.9375, 18551.609375, 224612.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (319, 1, 0, 841, 350341.21875, 18801.017578, 225757.34375, 2391, 350608.40625, 18843.847656, 225814.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (320, 1, 0, 841, 349259.21875, 18612.8125, 226661.5625, 2391, 349439.9375, 18579.515625, 226797.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (321, 1, 0, 841, 352409.125, 18573.117188, 224974.671875, 2391, 352655.5, 18584.757813, 224756.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (322, 1, 0, 841, 351499.03125, 18873.365234, 226017.03125, 2391, 351459.84375, 18831.402344, 225740.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (323, 1, 0, 841, 351126.3125, 18769.484375, 226880.984375, 2391, 350919.28125, 18682.857422, 227101.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (324, 1, 0, 841, 353803.96875, 18628.679688, 224747.984375, 2391, 354087.0625, 18611.6875, 224775.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (325, 1, 0, 841, 353281.96875, 18648.472656, 225412.359375, 2391, 353515.875, 18664.0625, 225454.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (326, 1, 0, 841, 352932.1875, 18788.589844, 226439.0, 2391, 353212.15625, 18733.005859, 226277.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (327, 1, 0, 841, 352152.8125, 18759.0, 227592.734375, 2391, 352287.40625, 18702.451172, 227824.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (328, 1, 0, 841, 351323.53125, 18559.146484, 228181.484375, 2391, 351402.90625, 18569.507813, 228428.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (329, 1, 0, 841, 353937.71875, 18730.474609, 226314.65625, 2391, 354141.3125, 18727.828125, 226330.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (330, 1, 0, 841, 353382.8125, 18753.283203, 227159.265625, 2391, 353405.75, 18748.072266, 227360.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (331, 1, 0, 841, 352975.78125, 18572.720703, 228488.296875, 2391, 352822.75, 18607.691406, 228708.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (332, 1, 0, 841, 353970.5625, 18653.328125, 228260.734375, 2391, 354197.25, 18662.847656, 228166.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (333, 1, 0, 841, 353747.25, 18667.765625, 229230.0625, 2391, 353618.28125, 18672.8125, 229399.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (334, 1, 0, 841, 357842.6875, 18294.765625, 225530.515625, 2391, 357667.90625, 18442.28125, 225669.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (335, 1, 0, 841, 356477.09375, 18695.978516, 227830.390625, 2391, 356636.0, 18738.582031, 227635.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (336, 1, 0, 841, 355976.71875, 18695.888672, 228355.8125, 2391, 356117.03125, 18704.095703, 228417.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (337, 1, 0, 841, 354662.46875, 18638.183594, 230908.375, 2391, 354714.75, 18612.769531, 231063.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (338, 1, 0, 841, 357851.34375, 18564.40625, 227400.25, 2391, 357875.84375, 18588.65625, 227218.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (339, 1, 0, 841, 356152.53125, 18707.185547, 230453.078125, 2391, 356121.4375, 18680.048828, 230649.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (340, 1, 0, 841, 358720.78125, 18367.414063, 227299.765625, 2391, 358848.78125, 18221.451172, 227106.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (341, 1, 0, 841, 357073.9375, 18609.929688, 230249.6875, 2391, 357136.21875, 18588.058594, 230052.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (342, 1, 0, 841, 358444.0625, 18597.585938, 229046.4375, 2391, 358223.03125, 18649.050781, 229146.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (343, 1, 0, 841, 358599.0625, 18485.660156, 230169.734375, 2391, 358460.9375, 18491.316406, 230361.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (344, 1, 0, 841, 357768.15625, 18371.125, 231619.265625, 2391, 357634.875, 18351.324219, 231792.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (345, 1, 0, 841, 359772.53125, 18154.181641, 231054.90625, 2391, 359616.6875, 18153.488281, 230884.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (346, 1, 0, 841, 359048.96875, 18224.849609, 231614.65625, 2391, 358949.90625, 18229.884766, 231794.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (347, 1, 0, 841, 360066.84375, 18124.578125, 232157.59375, 2391, 359913.9375, 18119.802734, 232259.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (348, 1, 0, 841, 361864.03125, 15081.500977, 221320.125, 2391, 361857.0625, 15086.613281, 221514.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (349, 1, 0, 841, 363267.5, 15099.870117, 222361.4375, 2391, 363335.4375, 15100.037109, 222611.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (350, 1, 0, 841, 363967.375, 15100.338867, 223488.9375, 2391, 363929.9375, 15100.1875, 223749.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (351, 1, 0, 841, 364236.875, 15107.512695, 224786.1875, 2391, 364283.375, 15115.853516, 224988.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (352, 1, 0, 841, 365160.28125, 15115.993164, 221940.0625, 2391, 365072.1875, 15114.445313, 222116.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (353, 1, 0, 841, 365585.5625, 15149.896484, 223362.21875, 2391, 365666.78125, 15160.5625, 223536.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (354, 1, 0, 841, 365480.75, 15159.063477, 224920.296875, 2391, 365435.84375, 15163.78125, 225177.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (355, 1, 0, 841, 372139.28125, 15175.920898, 220919.46875, 2391, 372072.8125, 15190.414063, 221087.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (356, 1, 0, 841, 372081.34375, 15300.121094, 223200.390625, 2391, 372030.75, 15250.921875, 223426.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (357, 1, 0, 841, 371709.46875, 15163.753906, 225506.25, 2391, 371717.5, 15168.626953, 225398.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (358, 1, 0, 841, 374048.53125, 15271.827148, 222841.015625, 2391, 373890.375, 15258.927734, 222954.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (359, 1, 0, 841, 373297.46875, 15227.62207, 224388.890625, 2391, 373361.75, 15229.416992, 224250.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (360, 1, 0, 841, 373636.625, 15234.0, 225332.046875, 2391, 373434.4375, 15234.0, 225486.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (361, 1, 0, 841, 375791.90625, 15257.383789, 222961.03125, 2391, 375706.125, 15242.277344, 223074.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (362, 1, 0, 841, 376593.46875, 15158.501953, 226086.609375, 2391, 376483.53125, 15159.454102, 225931.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (363, 1, 0, 841, 377455.78125, 15397.118164, 221857.953125, 2391, 377613.78125, 15376.629883, 221983.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (364, 1, 0, 841, 377305.71875, 15244.116211, 223169.015625, 2391, 377354.625, 15239.529297, 223339.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (365, 1, 0, 841, 377249.625, 15137.158203, 225514.15625, 2391, 377387.125, 15125.401367, 225676.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (366, 1, 0, 841, 378941.28125, 15096.907227, 224234.34375, 2391, 378923.6875, 15074.553711, 224435.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (367, 1, 0, 841, 368993.78125, 15234.0, 229244.984375, 2391, 369065.8125, 15205.479492, 228981.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (368, 1, 0, 841, 368631.0, 15331.608398, 231391.59375, 2391, 368456.125, 15358.841797, 231559.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (369, 1, 0, 841, 370732.9375, 15154.0, 228846.84375, 2391, 370668.3125, 15154.0, 228989.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (370, 1, 0, 841, 369533.3125, 15192.532227, 230644.359375, 2391, 369612.25, 15176.5, 230849.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (371, 1, 0, 841, 369814.59375, 15194.697266, 232044.453125, 2391, 369898.09375, 15212.459961, 232162.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (372, 1, 0, 841, 375466.625, 15065.613281, 231127.765625, 2391, 375464.125, 15091.017578, 231286.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (373, 1, 0, 841, 374805.875, 15033.469727, 232059.40625, 2391, 374856.59375, 15030.255859, 232215.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (374, 1, 0, 841, 374196.125, 15022.243164, 233514.5, 2391, 374366.5, 15016.869141, 233424.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (375, 1, 0, 841, 376194.5, 15130.232422, 232054.671875, 2391, 376403.75, 15217.25293, 231961.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (376, 1, 0, 841, 376122.46875, 15186.841797, 233536.1875, 2391, 375988.6875, 15168.141602, 233653.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (377, 1, 0, 841, 378768.78125, 15227.148438, 233441.515625, 2391, 378842.46875, 15261.762695, 233260.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (378, 1, 0, 841, 377582.0625, 15214.704102, 233712.65625, 2391, 377758.84375, 15227.378906, 233629.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (379, 1, 0, 841, 376933.46875, 15225.949219, 234933.71875, 2391, 376973.09375, 15220.929688, 234800.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (380, 1, 0, 841, 381470.4375, 15248.708008, 235386.609375, 2391, 381422.4375, 15189.493164, 235531.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (381, 1, 0, 841, 380230.96875, 15228.894531, 235416.21875, 2391, 380405.375, 15171.723633, 235209.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (382, 1, 0, 841, 378650.875, 15220.022461, 235090.84375, 2391, 378839.96875, 15229.186523, 234909.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (383, 1, 0, 841, 379846.25, 15273.452148, 236622.078125, 2391, 380154.34375, 15209.674805, 236641.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (384, 1, 0, 841, 378128.125, 15261.62793, 236837.078125, 2391, 378188.59375, 15280.724609, 237055.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (385, 1, 0, 841, 376856.40625, 15237.173828, 238527.421875, 2391, 377098.3125, 15226.112305, 238477.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (386, 1, 0, 841, 380632.46875, 15265.025391, 230979.359375, 2391, 380351.125, 15251.509766, 230849.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (387, 1, 0, 841, 382098.625, 15276.758789, 232200.953125, 2391, 382229.75, 15290.092773, 232367.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (388, 1, 0, 841, 383749.5, 15245.374023, 233807.71875, 2391, 383642.65625, 15238.821289, 233667.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (389, 1, 0, 841, 383376.0625, 15216.029297, 231773.140625, 2391, 383312.0, 15229.71875, 231993.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (390, 1, 0, 841, 379452.0625, 15477.793945, 221619.34375, 2391, 379444.25, 15464.554688, 221756.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (391, 1, 0, 841, 380641.3125, 15399.517578, 223196.484375, 2391, 380752.40625, 15402.96582, 223262.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (392, 1, 0, 841, 382315.96875, 15494.985352, 224511.15625, 2391, 382178.875, 15510.642578, 224266.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (393, 1, 0, 841, 382788.25, 15464.71582, 225647.375, 2391, 382593.25, 15441.0625, 225604.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (394, 1, 0, 841, 383689.90625, 15513.005859, 226608.515625, 2391, 383663.90625, 15494.035156, 226824.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (395, 1, 0, 841, 384904.53125, 15498.49707, 227727.453125, 2391, 384738.78125, 15490.15918, 227630.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (396, 1, 0, 841, 384325.71875, 15376.650391, 229167.859375, 2391, 384639.75, 15382.035156, 229296.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (397, 1, 0, 841, 385998.0, 15459.535156, 229844.109375, 2391, 385856.09375, 15461.189453, 229739.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (398, 1, 0, 841, 386243.90625, 15312.15918, 231716.515625, 2391, 386163.3125, 15325.387695, 231515.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (399, 1, 0, 841, 386728.5, 15232.915039, 233500.140625, 2391, 386682.84375, 15235.244141, 233332.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (400, 1, 0, 841, 387785.8125, 15534.822266, 232304.84375, 2391, 387719.40625, 15522.904297, 232125.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (401, 1, 0, 841, 388122.40625, 15246.605469, 234567.640625, 2391, 388369.8125, 15278.126953, 234742.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (402, 1, 0, 841, 384374.625, 15272.191406, 238196.484375, 2391, 384393.4375, 15273.521484, 237896.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (403, 1, 0, 841, 383141.125, 15298.12793, 238602.5625, 2391, 383171.9375, 15291.140625, 238296.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (404, 1, 0, 841, 380446.625, 15422.832031, 241612.921875, 2391, 380623.34375, 15414.995117, 241705.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (405, 1, 0, 841, 385209.9375, 15414.449219, 241197.859375, 2391, 385267.21875, 15402.925781, 241012.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (406, 1, 0, 841, 383106.75, 15426.832031, 241336.140625, 2391, 383323.15625, 15422.072266, 241254.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (407, 1, 0, 841, 382093.4375, 15395.803711, 243179.859375, 2391, 381986.3125, 15416.15625, 243007.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (408, 1, 0, 841, 388067.78125, 15412.240234, 238363.15625, 2391, 388181.59375, 15418.863281, 238475.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (409, 1, 0, 841, 388066.25, 15390.380859, 240253.984375, 2391, 388179.71875, 15394.498047, 240414.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (410, 1, 0, 841, 388563.75, 15325.557617, 242306.40625, 2391, 388526.0, 15327.957031, 242138.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (411, 1, 0, 841, 389479.25, 15517.070313, 245006.140625, 2391, 389330.6875, 15496.532227, 244925.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (412, 1, 0, 841, 389808.8125, 15472.082031, 241448.859375, 2391, 389836.78125, 15451.392578, 241630.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (413, 1, 0, 841, 389988.1875, 15414.762695, 242742.1875, 2391, 390007.5625, 15431.173828, 242935.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (414, 1, 0, 841, 390647.46875, 15603.026367, 245043.1875, 2391, 390813.0, 15565.985352, 245319.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (415, 1, 0, 841, 391228.75, 15570.040039, 242622.875, 2391, 391267.65625, 15571.271484, 242431.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (416, 1, 0, 841, 391177.375, 15720.226563, 243865.296875, 2391, 391301.25, 15722.722656, 243741.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (417, 1, 0, 841, 373984.5625, 15148.18457, 230517.046875, 2391, 374102.90625, 15153.912109, 230353.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (418, 1, 0, 841, 372319.96875, 15065.789063, 232971.0625, 2391, 372224.46875, 15082.616211, 233168.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (419, 1, 0, 149, 375525.0625, 17782.15625, 243388.3125, 2392, 375721.46875, 17735.667969, 243462.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (420, 1, 0, 149, 375220.34375, 17903.15625, 242435.515625, 2392, 375038.25, 17947.478516, 242441.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (421, 1, 0, 149, 374329.3125, 18283.716797, 241409.6875, 2392, 374254.78125, 18287.859375, 241442.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (422, 1, 0, 149, 372043.28125, 18085.597656, 240526.71875, 2392, 371981.1875, 18047.480469, 240584.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (423, 1, 0, 149, 369614.59375, 18262.398438, 238830.5, 2392, 369353.5625, 18224.482422, 238824.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (424, 1, 0, 149, 368908.875, 18329.476563, 237995.921875, 2392, 368680.78125, 18317.105469, 237952.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (425, 1, 0, 149, 373637.09375, 18113.255859, 242628.296875, 2392, 373614.40625, 18133.511719, 242524.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (426, 1, 0, 149, 372034.1875, 17840.335938, 241759.5625, 2392, 371921.53125, 17806.441406, 241682.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (427, 1, 0, 149, 370287.0625, 17861.505859, 240517.78125, 2392, 370144.78125, 17884.154297, 240398.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (428, 1, 0, 149, 367797.4375, 17730.927734, 239015.9375, 2392, 367957.21875, 17786.414063, 239095.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (429, 1, 0, 149, 367755.875, 17303.107422, 254549.359375, 2392, 367903.125, 17339.332031, 254435.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (430, 1, 0, 149, 365908.3125, 17511.597656, 254823.25, 2392, 366055.28125, 17488.208984, 254725.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (431, 1, 0, 149, 366620.71875, 17391.898438, 256097.140625, 2392, 366823.03125, 17357.0, 256148.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (432, 1, 0, 149, 370540.15625, 17669.646484, 257006.6875, 2392, 370574.8125, 17672.734375, 257161.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (433, 1, 0, 149, 369213.53125, 17451.113281, 257200.9375, 2392, 369119.3125, 17425.679688, 257336.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (434, 1, 0, 149, 366208.78125, 17377.216797, 257224.765625, 2392, 366053.5625, 17390.605469, 257314.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (435, 1, 0, 149, 370420.09375, 17573.115234, 259510.0, 2392, 370268.4375, 17508.761719, 259487.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (436, 1, 0, 149, 369048.0625, 17289.697266, 258649.6875, 2392, 369248.5, 17314.916016, 258724.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (437, 1, 0, 149, 367990.0, 17239.166016, 258478.984375, 2392, 367900.4375, 17241.107422, 258549.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (438, 1, 0, 149, 366628.65625, 17285.195313, 258411.25, 2392, 366835.1875, 17272.349609, 258449.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (439, 1, 0, 149, 366460.03125, 17284.279297, 259658.46875, 2392, 366399.3125, 17314.998047, 259836.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (440, 1, 0, 149, 366600.21875, 17397.367188, 260957.234375, 2392, 366609.71875, 17447.59375, 261094.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (441, 1, 0, 149, 367427.875, 17692.746094, 263605.71875, 2392, 367448.4375, 17673.492188, 263422.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (442, 1, 0, 149, 367675.84375, 17287.658203, 260027.09375, 2392, 367805.78125, 17288.943359, 260156.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (443, 1, 0, 149, 368328.625, 17539.119141, 262028.03125, 2392, 368288.375, 17509.027344, 261886.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (444, 1, 0, 149, 369741.9375, 18190.236328, 264358.875, 2392, 369644.875, 18221.9375, 264266.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (445, 1, 0, 149, 371195.78125, 18698.025391, 264539.75, 2392, 371109.0625, 18635.4375, 264530.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (446, 1, 0, 149, 377105.28125, 18424.816406, 249836.609375, 2392, 377112.0, 18453.667969, 249984.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (447, 1, 0, 149, 375772.9375, 18054.060547, 249671.9375, 2392, 375987.5625, 18054.832031, 249569.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (448, 1, 0, 149, 374013.5625, 18264.349609, 248936.15625, 2392, 374006.4375, 18033.808594, 249096.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (449, 1, 0, 149, 372220.21875, 17986.205078, 248017.234375, 2392, 372187.875, 18004.445313, 248207.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (450, 1, 0, 149, 376088.15625, 18076.789063, 253640.8125, 2392, 375966.34375, 18124.109375, 253558.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (451, 1, 0, 149, 376339.53125, 18314.361328, 252535.40625, 2392, 376335.3125, 18340.511719, 252343.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (452, 1, 0, 149, 375430.75, 18468.826172, 251266.171875, 2392, 375518.28125, 18570.769531, 251536.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (453, 1, 0, 149, 373845.0, 18510.525391, 251964.265625, 2392, 374015.21875, 18502.232422, 252121.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (454, 1, 0, 149, 370781.9375, 18412.566406, 250544.453125, 2392, 370795.59375, 18456.828125, 250763.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (455, 1, 0, 149, 370432.28125, 18058.748047, 248508.453125, 2392, 370202.09375, 18068.234375, 248514.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (456, 1, 0, 149, 368726.46875, 18138.966797, 247382.234375, 2392, 368856.03125, 18112.839844, 247537.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (457, 1, 0, 149, 367395.34375, 18176.126953, 247304.21875, 2392, 367308.375, 18147.566406, 247455.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (458, 1, 0, 149, 365977.96875, 18308.771484, 248043.40625, 2392, 366115.21875, 18338.966797, 248170.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (459, 1, 0, 149, 369448.0, 18157.392578, 249679.921875, 2392, 369277.375, 18149.835938, 249708.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (460, 1, 0, 149, 367531.65625, 18181.414063, 249684.453125, 2392, 367412.125, 18196.953125, 249659.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (461, 1, 0, 149, 368048.65625, 18042.230469, 250692.984375, 2392, 368138.46875, 18074.824219, 250551.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (462, 1, 0, 149, 366909.8125, 18055.613281, 245738.75, 2392, 366776.65625, 18022.507813, 245576.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (463, 1, 0, 149, 365185.96875, 18051.560547, 244611.21875, 2392, 365203.5, 17984.082031, 244749.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (464, 1, 0, 149, 363607.25, 18135.705078, 244633.640625, 2392, 363594.5625, 18226.576172, 244802.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (465, 1, 0, 149, 361994.65625, 18356.466797, 245245.6875, 2392, 362168.34375, 18395.40625, 245301.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (466, 1, 0, 149, 363962.59375, 18452.310547, 246931.484375, 2392, 363770.46875, 18460.921875, 246926.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (467, 1, 0, 149, 362119.03125, 18731.664063, 247428.375, 2392, 362193.9375, 18719.695313, 247287.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (468, 1, 0, 149, 363718.8125, 18618.755859, 247948.046875, 2392, 363558.21875, 18589.556641, 247806.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (469, 1, 0, 149, 362128.71875, 18857.847656, 249355.9375, 2392, 362064.3125, 18858.886719, 249183.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (470, 1, 0, 149, 360054.71875, 18281.431641, 242917.375, 2392, 359839.5, 18281.449219, 242865.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (471, 1, 0, 149, 358297.6875, 18400.105469, 242903.015625, 2392, 358079.375, 18437.164063, 242879.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (472, 1, 0, 149, 358902.96875, 18469.951172, 244295.71875, 2392, 358728.75, 18524.9375, 244404.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (473, 1, 0, 149, 353880.0625, 18479.533203, 242536.59375, 2392, 353998.84375, 18516.912109, 242650.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (474, 1, 0, 149, 354141.71875, 18865.880859, 246594.953125, 2392, 353975.59375, 18865.601563, 246501.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (475, 1, 0, 149, 352389.875, 18780.892578, 245730.84375, 2392, 352183.875, 18789.976563, 245700.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (476, 1, 0, 149, 360555.34375, 19222.882813, 251398.8125, 2392, 360462.40625, 19190.550781, 251365.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (477, 1, 0, 149, 358439.40625, 19102.734375, 250526.09375, 2392, 358283.40625, 19126.212891, 250474.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (478, 1, 0, 149, 355625.46875, 19121.207031, 248670.6875, 2392, 355719.28125, 19147.496094, 248757.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (479, 1, 0, 149, 360378.375, 19343.474609, 252976.65625, 2392, 360248.71875, 19328.011719, 252998.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (480, 1, 0, 149, 358523.0, 19326.462891, 252041.859375, 2392, 358384.40625, 19330.826172, 252159.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (481, 1, 0, 149, 356125.1875, 19023.8125, 250406.9375, 2392, 356196.65625, 19009.746094, 250602.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (482, 1, 0, 149, 354003.28125, 18993.466797, 249622.375, 2392, 353794.0625, 18967.783203, 249496.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (483, 1, 0, 149, 358449.65625, 19218.158203, 254488.484375, 2392, 358330.28125, 19230.722656, 254507.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (484, 1, 0, 149, 358376.125, 19306.447266, 255949.9375, 2392, 358606.1875, 19291.675781, 256031.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (485, 1, 0, 149, 356860.28125, 19366.123047, 255015.9375, 2392, 356730.8125, 19354.070313, 254988.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (486, 1, 0, 149, 354794.34375, 19002.830078, 252545.3125, 2392, 354836.21875, 19009.205078, 252692.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (487, 1, 0, 149, 353517.90625, 18801.111328, 252082.71875, 2392, 353334.75, 18766.787109, 252157.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (488, 1, 0, 149, 353764.9375, 18849.578125, 253943.90625, 2392, 353917.9375, 18884.097656, 253967.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (489, 1, 0, 149, 365010.375, 18443.929688, 265693.90625, 2392, 364822.5625, 18123.955078, 265733.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (490, 1, 0, 149, 363073.9375, 18829.730469, 266633.03125, 2392, 362916.28125, 18713.816406, 266558.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (491, 1, 0, 149, 361367.96875, 18870.298828, 266149.53125, 2392, 361402.0, 18858.328125, 266298.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (492, 1, 0, 149, 364483.90625, 18358.513672, 267515.375, 2392, 364575.15625, 18174.652344, 267672.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (493, 1, 0, 149, 361650.46875, 18796.019531, 267528.125, 2392, 361530.75, 18720.626953, 267642.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (494, 1, 0, 149, 359712.25, 18720.578125, 267006.25, 2392, 359698.90625, 18707.849609, 267132.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (495, 1, 0, 149, 361674.96875, 19303.6875, 264153.84375, 2392, 361585.65625, 19273.273438, 264207.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (496, 1, 0, 149, 360317.3125, 19330.326172, 263578.90625, 2392, 360204.34375, 19329.142578, 263569.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (497, 1, 0, 149, 358738.71875, 19256.048828, 263444.59375, 2392, 358827.75, 19287.347656, 263334.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (498, 1, 0, 149, 356957.375, 18864.994141, 264016.71875, 2392, 356830.15625, 18821.455078, 264130.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (499, 1, 0, 149, 357044.8125, 18651.042969, 265808.375, 2392, 357182.34375, 18676.59375, 265842.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (500, 1, 0, 149, 357293.65625, 19371.226563, 261960.4375, 2392, 357134.53125, 19337.357422, 262007.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (501, 1, 0, 149, 357947.125, 19631.916016, 261126.265625, 2392, 357943.46875, 19639.748047, 260969.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (502, 1, 0, 149, 358265.0625, 19537.111328, 259678.4375, 2392, 358171.96875, 19582.427734, 259822.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (503, 1, 0, 149, 357082.21875, 19357.033203, 258758.265625, 2392, 356949.9375, 19369.738281, 258817.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (504, 1, 0, 149, 355568.34375, 19277.378906, 257891.703125, 2392, 355438.75, 19264.095703, 257973.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (505, 1, 0, 79, 338786.1875, 15406.257813, 218086.265625, 2380, 338752.875, 15363.074219, 217925.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (506, 1, 0, 79, 338120.03125, 15037.172852, 216965.625, 2380, 338003.4375, 14995.748047, 216862.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (507, 1, 0, 79, 337082.4375, 14757.935547, 216236.046875, 2380, 336972.65625, 14733.279297, 216132.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (508, 1, 0, 79, 335859.375, 14536.382813, 215294.828125, 2380, 335683.59375, 14509.260742, 215162.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (509, 1, 0, 79, 337244.9375, 14922.898438, 217373.1875, 2380, 337172.1875, 14892.74707, 217274.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (510, 1, 0, 79, 336053.09375, 14601.886719, 216459.078125, 2380, 335952.65625, 14581.837891, 216415.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (511, 1, 0, 79, 334870.59375, 14360.861328, 215808.09375, 2380, 334695.25, 14336.808594, 215705.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (512, 1, 0, 79, 333857.375, 14343.4375, 215055.28125, 2380, 333676.59375, 14348.385742, 214920.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (513, 1, 0, 79, 337392.6875, 15202.066406, 218315.484375, 2380, 337302.9375, 15164.580078, 218244.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (514, 1, 0, 79, 336383.75, 14849.736328, 217733.40625, 2380, 336296.28125, 14792.294922, 217583.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (515, 1, 0, 79, 334627.0625, 14427.529297, 217023.234375, 2380, 334604.6875, 14393.410156, 216822.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (516, 1, 0, 79, 333044.46875, 14253.335938, 215971.0625, 2380, 333173.96875, 14257.158203, 215943.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (517, 1, 0, 79, 322259.1875, 13836.863281, 217288.15625, 2380, 322419.8125, 13842.025391, 217274.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (518, 1, 0, 79, 324787.15625, 13981.357422, 217179.6875, 2380, 324663.125, 13968.249023, 217118.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (519, 1, 0, 79, 323784.71875, 13983.092773, 216466.46875, 2380, 323764.65625, 13971.535156, 216538.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (520, 1, 0, 79, 322597.75, 13984.375, 216407.6875, 2380, 322457.34375, 13971.876953, 216460.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (521, 1, 0, 79, 321082.5625, 13901.117188, 216750.875, 2380, 320988.1875, 13884.239258, 216872.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (522, 1, 0, 79, 319715.5625, 13788.990234, 217147.40625, 2380, 319531.25, 13778.902344, 217209.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (523, 1, 0, 79, 324446.9375, 14103.464844, 218167.765625, 2380, 324359.0625, 14076.655273, 218135.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (524, 1, 0, 79, 323383.46875, 13903.820313, 217834.015625, 2380, 323225.4375, 13876.80957, 217931.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (525, 1, 0, 79, 321960.90625, 13743.592773, 218194.671875, 2380, 321832.78125, 13754.910156, 218257.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (526, 1, 0, 79, 320842.625, 13786.271484, 217844.875, 2380, 320980.71875, 13751.080078, 217793.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (527, 1, 0, 79, 320267.6875, 13703.197266, 218380.75, 2380, 320066.875, 13703.798828, 218286.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (528, 1, 0, 79, 319159.09375, 13757.060547, 218145.640625, 2380, 318986.90625, 13767.483398, 218092.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (529, 1, 0, 79, 308416.59375, 15185.751953, 211476.90625, 2380, 308309.96875, 15165.084961, 211480.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (530, 1, 0, 79, 307392.40625, 14952.874023, 211843.0625, 2380, 307248.53125, 14915.137695, 211906.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (531, 1, 0, 79, 306366.25, 14922.43457, 213414.09375, 2380, 306435.3125, 14945.197266, 213215.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (532, 1, 0, 79, 305817.96875, 14906.09375, 214284.96875, 2380, 305733.71875, 14919.676758, 214433.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (533, 1, 0, 79, 304491.28125, 15133.865234, 215868.484375, 2380, 304604.96875, 15108.321289, 215814.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (534, 1, 0, 79, 307617.53125, 14895.176758, 213276.421875, 2380, 307786.71875, 14897.128906, 213182.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (535, 1, 0, 79, 306105.0, 14961.974609, 215790.15625, 2380, 306073.0625, 14955.609375, 215636.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (536, 1, 0, 79, 304931.0, 15090.341797, 217323.171875, 2380, 304902.40625, 15084.59375, 217186.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (537, 1, 0, 79, 309332.84375, 15271.298828, 212465.375, 2380, 309249.125, 15254.039063, 212531.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (538, 1, 0, 79, 307986.625, 14921.53418, 214563.234375, 2380, 307802.75, 14921.751953, 214566.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (539, 1, 0, 79, 307161.28125, 14991.624023, 215535.453125, 2380, 307105.125, 14951.629883, 215722.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (540, 1, 0, 79, 306467.0625, 15139.226563, 216885.0, 2380, 306287.34375, 15104.007813, 216833.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (541, 1, 0, 79, 316807.0, 13873.958984, 217129.40625, 2380, 316675.46875, 13882.503906, 217073.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (542, 1, 0, 79, 315419.6875, 13976.982422, 216793.265625, 2380, 315290.625, 13988.177734, 216923.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (543, 1, 0, 79, 313871.65625, 14156.425781, 216751.46875, 2380, 313998.71875, 14142.636719, 216898.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (544, 1, 0, 79, 312744.65625, 14298.169922, 216393.890625, 2380, 312546.40625, 14357.508789, 216265.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (545, 1, 0, 79, 311494.03125, 14502.248047, 215288.515625, 2380, 311588.8125, 14476.126953, 215430.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (546, 1, 0, 79, 316079.4375, 13939.425781, 218412.828125, 2380, 316252.34375, 13909.1875, 218350.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (547, 1, 0, 79, 314628.8125, 14036.487305, 218028.765625, 2380, 314832.75, 13999.953125, 218124.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (548, 1, 0, 79, 312996.15625, 14216.558594, 217787.796875, 2380, 313156.71875, 14181.012695, 217847.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (549, 1, 0, 79, 311866.9375, 14344.296875, 216778.328125, 2380, 311829.40625, 14375.543945, 216977.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (550, 1, 0, 79, 310538.1875, 14462.646484, 216187.21875, 2380, 310569.09375, 14458.626953, 216341.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (551, 1, 0, 79, 305514.65625, 15051.833984, 209609.3125, 2380, 305504.46875, 15053.277344, 209491.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (552, 1, 0, 79, 305292.21875, 15089.076172, 208412.09375, 2380, 305361.0625, 15088.761719, 208252.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (553, 1, 0, 79, 304644.40625, 15087.666016, 207794.46875, 2380, 304605.25, 15090.121094, 207545.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (554, 1, 0, 79, 304357.75, 15290.457031, 206059.703125, 2380, 304364.53125, 15266.267578, 206198.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (555, 1, 0, 79, 304267.09375, 15435.369141, 204915.875, 2380, 304105.5625, 15435.107422, 204926.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (556, 1, 0, 79, 304001.6875, 15152.59668, 208865.09375, 2380, 304193.84375, 15158.019531, 209006.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (557, 1, 0, 79, 303562.5, 15143.301758, 208253.359375, 2380, 303632.1875, 15133.504883, 208109.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (558, 1, 0, 79, 302855.25, 15162.118164, 207086.1875, 2380, 302944.875, 15179.769531, 206919.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (559, 1, 0, 79, 303015.5625, 15364.445313, 205488.578125, 2380, 303058.25, 15363.898438, 205676.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (560, 1, 0, 79, 303008.1875, 15383.429688, 204006.40625, 2380, 302889.90625, 15336.713867, 204070.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (561, 1, 0, 79, 297707.0625, 15293.646484, 194199.5, 2380, 297757.34375, 15288.28418, 194462.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (562, 1, 0, 79, 297622.3125, 15310.138672, 193420.59375, 2380, 297695.5625, 15313.104492, 193282.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (563, 1, 0, 79, 297317.625, 15254.755859, 192139.59375, 2380, 297225.71875, 15243.969727, 191998.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (564, 1, 0, 79, 296610.5, 15219.772461, 191064.59375, 2380, 296644.59375, 15252.674805, 190878.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (565, 1, 0, 79, 296757.78125, 15263.076172, 195049.921875, 2380, 296957.1875, 15265.730469, 195179.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (566, 1, 0, 79, 296117.03125, 15315.113281, 193014.328125, 2380, 296312.9375, 15320.064453, 193091.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (567, 1, 0, 79, 295835.8125, 15267.128906, 192024.765625, 2380, 295900.40625, 15230.149414, 191814.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (568, 1, 0, 79, 295493.0625, 15260.619141, 190431.515625, 2380, 295554.21875, 15265.414063, 190343.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (569, 1, 0, 79, 331234.9375, 14333.46875, 214391.453125, 2380, 331182.625, 14312.025391, 214491.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (570, 1, 0, 79, 329503.5625, 14219.639648, 214581.15625, 2380, 329496.5625, 14204.248047, 214720.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (571, 1, 0, 79, 328311.40625, 14136.19043, 215261.546875, 2380, 328490.1875, 14138.740234, 215209.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (572, 1, 0, 79, 327883.03125, 14145.894531, 215019.6875, 2380, 327674.28125, 14147.300781, 215037.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (573, 1, 0, 79, 331751.71875, 14171.899414, 215943.15625, 2380, 331693.9375, 14167.742188, 215854.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (574, 1, 0, 79, 330401.75, 14163.546875, 215409.90625, 2380, 330543.53125, 14145.957031, 215539.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (575, 1, 0, 79, 329144.3125, 14061.607422, 216014.015625, 2380, 329279.59375, 14065.480469, 215971.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (576, 1, 0, 79, 328359.625, 14054.624023, 216331.65625, 2380, 328138.90625, 14056.400391, 216309.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (577, 1, 0, 79, 302446.0625, 15404.614258, 201504.390625, 2380, 302384.65625, 15430.998047, 201432.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (578, 1, 0, 79, 302105.625, 15459.735352, 200152.59375, 2380, 302006.96875, 15479.655273, 200279.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (579, 1, 0, 79, 302163.71875, 15349.208984, 199141.984375, 2380, 302156.0625, 15329.396484, 198935.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (580, 1, 0, 79, 301580.5, 15354.060547, 197952.96875, 2380, 301456.59375, 15360.380859, 197958.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (581, 1, 0, 79, 300395.71875, 15345.482422, 197380.640625, 2380, 300539.375, 15345.535156, 197366.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (582, 1, 0, 79, 301472.03125, 15325.556641, 202110.15625, 2380, 301353.09375, 15309.81543, 202148.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (583, 1, 0, 79, 300915.8125, 15327.009766, 201123.828125, 2380, 301094.375, 15332.455078, 201191.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (584, 1, 0, 79, 300706.9375, 15311.146484, 200101.96875, 2380, 300532.46875, 15291.908203, 200052.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (585, 1, 0, 79, 299643.71875, 15216.289063, 198408.484375, 2380, 299596.4375, 15205.520508, 198602.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (586, 1, 0, 79, 299401.53125, 15226.973633, 200435.90625, 2380, 299287.96875, 15213.092773, 200358.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (587, 1, 0, 79, 298755.09375, 15136.253906, 199360.234375, 2380, 298696.03125, 15144.759766, 199527.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (588, 1, 0, 79, 298743.75, 15197.789063, 198669.8125, 2380, 298622.28125, 15200.8125, 198552.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (589, 1, 0, 79, 298617.71875, 15241.212891, 197279.453125, 2380, 298513.8125, 15233.808594, 197300.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (590, 1, 0, 79, 297936.75, 15208.138672, 196276.53125, 2380, 297801.5, 15200.632813, 196090.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (591, 1, 0, 79, 297886.53125, 15090.09375, 199486.328125, 2380, 297867.375, 15088.719727, 199320.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (592, 1, 0, 79, 297299.3125, 15136.087891, 197304.359375, 2380, 297273.34375, 15136.858398, 197426.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (593, 1, 0, 79, 310315.6875, 14920.775391, 214175.828125, 2380, 310374.6875, 14858.759766, 214363.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (594, 1, 0, 79, 309350.375, 14995.138672, 214005.5625, 2380, 309240.3125, 15029.394531, 213815.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (595, 1, 0, 79, 309257.78125, 14795.322266, 214934.890625, 2380, 309149.65625, 14790.119141, 215004.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (596, 1, 0, 79, 298896.09375, 15326.174805, 194898.5, 2380, 298744.5625, 15312.996094, 194813.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (597, 1, 0, 79, 298704.75, 15321.28125, 192355.046875, 2380, 298540.9375, 15310.867188, 192458.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (598, 1, 0, 79, 298255.5625, 15207.993164, 191387.65625, 2380, 298075.6875, 15224.841797, 191249.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (599, 1, 0, 79, 297528.0, 15307.685547, 190068.390625, 2380, 297649.34375, 15320.855469, 190189.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (600, 1, 0, 79, 296643.96875, 15355.331055, 189387.421875, 2380, 296623.0625, 15343.759766, 189540.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (601, 1, 0, 79, 318546.625, 13850.501953, 216566.953125, 2380, 318358.84375, 13833.935547, 216615.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (602, 1, 0, 79, 318032.1875, 13791.080078, 217506.015625, 2380, 318089.90625, 13795.828125, 217352.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (603, 1, 0, 79, 317494.375, 13807.689453, 217952.359375, 2380, 317390.59375, 13793.494141, 218121.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (604, 1, 0, 79, 325655.9375, 14084.349609, 215164.828125, 2380, 325813.0, 14104.443359, 215188.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (605, 1, 0, 79, 325137.78125, 14053.19043, 215740.421875, 2380, 325202.625, 14041.255859, 215879.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (606, 1, 0, 79, 326859.09375, 14077.197266, 215936.296875, 2380, 326805.0625, 14089.511719, 215769.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (607, 1, 0, 79, 326307.0, 13965.636719, 216833.671875, 2380, 326481.84375, 13967.762695, 216879.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (608, 1, 0, 79, 333244.21875, 14485.90625, 214027.796875, 2380, 333016.65625, 14490.306641, 213978.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (609, 1, 0, 79, 332231.84375, 14265.525391, 214941.265625, 2380, 332269.09375, 14288.708984, 214728.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (610, 1, 0, 79, 330642.15625, 14136.148438, 216626.25, 2380, 330377.84375, 14127.021484, 216672.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (611, 1, 0, 79, 304858.28125, 15296.087891, 212567.4375, 2380, 305049.09375, 15228.041992, 212482.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (612, 1, 0, 79, 306076.15625, 14971.890625, 211627.03125, 2380, 306178.625, 14948.05957, 211421.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (613, 1, 0, 79, 306835.46875, 15076.740234, 210119.71875, 2380, 306879.34375, 15050.101563, 210362.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (614, 1, 0, 79, 304505.34375, 15238.527344, 210907.375, 2380, 304614.0625, 15208.080078, 210724.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (615, 1, 0, 79, 301497.8125, 15283.019531, 203580.125, 2380, 301675.65625, 15272.040039, 203722.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (616, 1, 0, 79, 308215.5, 14880.054688, 216651.9375, 2380, 307976.5, 14928.390625, 216655.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (617, 1, 0, 79, 308747.90625, 15229.25, 217733.03125, 2380, 308536.71875, 15284.65918, 217832.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (618, 1, 0, 79, 307291.28125, 15387.6875, 218149.390625, 2380, 307063.03125, 15354.574219, 218087.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (619, 1, 0, 79, 305226.625, 15226.433594, 218710.28125, 2380, 305427.5625, 15237.476563, 218674.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (620, 1, 0, 79, 303954.96875, 15166.697266, 218403.046875, 2380, 303852.4375, 15160.208984, 218248.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (621, 1, 0, 79, 296454.8125, 15228.027344, 196189.609375, 2380, 296355.78125, 15195.358398, 196005.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (622, 1, 0, 79, 295726.28125, 15216.357422, 196821.625, 2380, 295684.4375, 15202.279297, 197019.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (623, 1, 0, 79, 294813.34375, 15249.623047, 195950.3125, 2380, 295026.53125, 15208.455078, 195824.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (624, 1, 0, 149, 347923.9375, 19017.400391, 248850.796875, 2392, 348145.6875, 18985.59375, 248702.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (625, 1, 0, 149, 348083.90625, 19057.691406, 250121.203125, 2392, 348271.96875, 19045.205078, 250253.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (626, 1, 0, 149, 347256.9375, 19087.966797, 251099.75, 2392, 347466.71875, 19076.615234, 251091.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (627, 1, 0, 149, 346972.59375, 19109.949219, 253014.90625, 2392, 347185.78125, 19086.638672, 253021.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (628, 1, 0, 149, 349314.375, 18955.681641, 250711.03125, 2392, 349463.75, 18934.816406, 250899.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (629, 1, 0, 149, 349228.40625, 18730.703125, 252438.0, 2392, 349326.90625, 18684.96875, 252678.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (630, 1, 0, 149, 348457.75, 19007.125, 255232.046875, 2392, 348707.625, 19002.363281, 255378.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (631, 1, 0, 149, 347851.46875, 19028.886719, 257504.875, 2392, 348087.0625, 19083.984375, 257594.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (632, 1, 0, 79, 337205.84375, 14514.697266, 214404.28125, 2380, 337004.75, 14508.501953, 214367.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (633, 1, 0, 79, 335544.90625, 14573.521484, 213692.234375, 2380, 335371.90625, 14578.910156, 213613.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (634, 1, 0, 79, 331044.0625, 14459.147461, 213015.328125, 2380, 331251.1875, 14479.21582, 212983.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (635, 1, 0, 79, 330153.6875, 14369.561523, 213341.21875, 2380, 329946.875, 14351.732422, 213430.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (636, 1, 0, 79, 328830.0, 14196.65918, 213325.140625, 2380, 328615.5625, 14193.668945, 213442.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (637, 1, 0, 79, 324413.65625, 14173.791016, 214491.71875, 2380, 324241.0, 14198.789063, 214506.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (638, 1, 0, 79, 322683.21875, 14208.1875, 215328.59375, 2380, 322544.34375, 14199.157227, 215417.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (639, 1, 0, 79, 321965.21875, 14432.444336, 214656.15625, 2380, 321753.28125, 14476.655273, 214672.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (640, 1, 0, 79, 320921.25, 14236.522461, 215445.109375, 2380, 320712.40625, 14252.699219, 215431.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (641, 1, 0, 79, 319084.625, 14080.527344, 215806.515625, 2380, 318810.375, 14107.335938, 215753.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (642, 1, 0, 79, 316909.5, 13993.883789, 216104.59375, 2380, 317095.46875, 13993.526367, 216081.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (643, 1, 0, 79, 315552.4375, 14591.569336, 215777.859375, 2380, 315696.625, 14360.43457, 215969.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (644, 1, 0, 79, 312882.4375, 14572.568359, 215227.46875, 2380, 312759.65625, 14568.257813, 215138.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (645, 1, 0, 79, 311545.71875, 14985.224609, 214052.921875, 2380, 311394.96875, 15021.246094, 213947.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (646, 1, 0, 79, 310520.40625, 15382.129883, 212944.796875, 2380, 310482.40625, 15339.337891, 213109.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (647, 1, 0, 79, 305839.71875, 15418.359375, 206143.09375, 2380, 305679.5625, 15396.907227, 205963.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (648, 1, 0, 79, 300439.28125, 15296.919922, 205138.609375, 2380, 300646.9375, 15241.06543, 205306.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (649, 1, 0, 79, 299853.5, 15251.671875, 203736.125, 2380, 300000.75, 15269.751953, 203871.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (650, 1, 0, 79, 301630.25, 15426.994141, 196432.15625, 2380, 301586.4375, 15459.095703, 196310.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (651, 1, 0, 79, 300438.125, 15507.025391, 196133.515625, 2380, 300575.28125, 15487.351563, 196325.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (652, 1, 0, 79, 300234.84375, 15521.208008, 195399.03125, 2380, 300247.90625, 15537.556641, 195210.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (653, 1, 0, 79, 300006.625, 15543.962891, 193754.84375, 2380, 299999.375, 15551.685547, 193980.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (654, 1, 0, 79, 299763.90625, 15452.320313, 192288.84375, 2380, 299884.25, 15474.109375, 192421.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (655, 1, 0, 79, 299334.0625, 15576.720703, 190523.0625, 2380, 299267.28125, 15593.548828, 190627.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (656, 1, 0, 79, 298309.65625, 15448.292969, 189395.34375, 2380, 298446.84375, 15470.978516, 189421.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (657, 2, 2, 818, 0.0, 0.0, 0.0, 2385, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (658, 2, 1, 0, 0.0, 0.0, 0.0, 2406, 288115.375, 16620.527344, 213762.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (659, 2, 1, 0, 0.0, 0.0, 0.0, 2406, 250157.5, 19379.179688, 196411.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (660, 2, 0, 86, 224857.09375, 19479.410156, 190480.40625, 2379, 224788.828125, 19439.214844, 190669.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (661, 2, 0, 86, 225549.640625, 19436.054688, 190570.0, 2379, 225662.0625, 19461.066406, 190611.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (662, 2, 0, 86, 225279.96875, 19298.332031, 191154.578125, 2379, 225324.25, 19278.162109, 191357.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (663, 2, 0, 86, 226786.9375, 19689.921875, 189757.078125, 2379, 226675.296875, 19714.697266, 189792.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (664, 2, 0, 86, 227266.53125, 20022.039063, 189001.8125, 2379, 227236.671875, 20160.001953, 188751.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (665, 2, 0, 86, 227698.265625, 19636.15625, 189749.359375, 2379, 227870.375, 19663.986328, 189785.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (666, 2, 0, 86, 229213.703125, 20217.689453, 188216.28125, 2379, 229219.078125, 20272.109375, 187951.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (667, 2, 0, 86, 229270.453125, 19996.875, 189804.1875, 2379, 229381.796875, 20002.976563, 190233.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (668, 2, 0, 86, 229801.203125, 20283.972656, 188141.25, 2379, 229929.546875, 20292.574219, 187869.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (669, 2, 0, 86, 229662.328125, 20223.433594, 188615.671875, 2379, 229824.59375, 20280.052734, 188347.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (670, 2, 0, 86, 229913.59375, 20141.439453, 189503.90625, 2379, 229898.203125, 20176.166016, 189227.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (671, 2, 0, 86, 229865.078125, 20095.673828, 190144.75, 2379, 230054.96875, 20136.703125, 190276.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (672, 2, 0, 86, 231564.6875, 20503.242188, 189326.640625, 2379, 231530.171875, 20546.125, 189061.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (673, 2, 0, 86, 232051.828125, 20384.224609, 189779.984375, 2379, 232252.40625, 20438.550781, 189607.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (674, 2, 0, 86, 232962.3125, 20283.966797, 190592.0, 2379, 232744.875, 20276.404297, 190503.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (675, 2, 0, 86, 233094.578125, 20304.853516, 191033.8125, 2379, 233088.046875, 20284.886719, 191201.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (676, 2, 0, 86, 231604.703125, 19170.888672, 198349.703125, 2379, 231343.6875, 19202.169922, 198365.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (677, 2, 0, 86, 232150.1875, 19172.425781, 198606.65625, 2379, 232266.203125, 19130.123047, 198396.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (678, 2, 0, 86, 233915.796875, 19027.085938, 197078.078125, 2379, 233739.171875, 19043.871094, 196948.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (679, 2, 0, 86, 234152.6875, 18940.541016, 196419.171875, 2379, 233900.984375, 18954.863281, 196400.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (680, 2, 0, 86, 234560.65625, 18993.925781, 195160.28125, 2379, 234690.125, 18973.101563, 195320.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (681, 2, 0, 86, 234874.359375, 19093.994141, 194732.203125, 2379, 234856.296875, 19117.933594, 194510.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (682, 2, 0, 86, 233524.328125, 18970.681641, 195317.0, 2379, 233362.15625, 18967.980469, 195211.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (683, 2, 0, 86, 233613.65625, 19030.71875, 194698.984375, 2379, 233356.28125, 19009.138672, 194638.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (684, 2, 0, 86, 240736.453125, 18834.068359, 192822.46875, 2379, 240782.546875, 18855.023438, 192652.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (685, 2, 0, 86, 239397.828125, 18777.486328, 193108.875, 2379, 239428.328125, 18819.015625, 192904.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (686, 2, 0, 86, 241456.953125, 18706.480469, 193199.609375, 2379, 241713.609375, 18632.371094, 193148.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (687, 2, 0, 86, 240945.453125, 18743.708984, 193543.78125, 2379, 241133.703125, 18658.332031, 193720.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (688, 2, 0, 86, 240096.171875, 18771.568359, 193396.71875, 2379, 240263.6875, 18812.675781, 193515.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (689, 2, 0, 86, 239634.6875, 18661.728516, 193761.5625, 2379, 239344.25, 18740.25, 193497.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (690, 2, 0, 86, 238659.609375, 18690.300781, 193931.578125, 2379, 238911.828125, 18690.269531, 193772.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (691, 2, 0, 86, 237462.640625, 18734.501953, 194079.5, 2379, 237653.59375, 18712.845703, 193857.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (692, 2, 0, 86, 238975.046875, 18643.197266, 194377.875, 2379, 238786.15625, 18652.205078, 194489.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (693, 2, 0, 86, 237736.453125, 18676.0625, 194544.53125, 2379, 237950.328125, 18662.998047, 194574.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (694, 2, 0, 86, 237081.078125, 18771.203125, 194777.625, 2379, 237189.65625, 18739.630859, 194599.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (695, 2, 0, 86, 240818.5625, 18631.404297, 194831.8125, 2379, 240842.984375, 18640.765625, 194591.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (696, 2, 0, 86, 240277.109375, 18663.992188, 194844.484375, 2379, 240391.0, 18667.880859, 194592.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (697, 2, 0, 86, 239381.734375, 18571.09375, 195076.859375, 2379, 239533.109375, 18556.691406, 195185.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (698, 2, 0, 86, 240972.78125, 18530.810547, 195775.765625, 2379, 240988.203125, 18510.392578, 195998.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (699, 2, 0, 86, 239199.09375, 18586.275391, 195575.921875, 2379, 239120.8125, 18623.617188, 195786.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (700, 2, 0, 86, 238269.359375, 18632.287109, 195513.21875, 2379, 238351.421875, 18655.740234, 195657.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (701, 2, 0, 86, 240927.515625, 19016.279297, 200655.953125, 2379, 240903.296875, 19072.777344, 200912.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (702, 2, 0, 86, 241320.46875, 18858.707031, 200154.234375, 2379, 241492.21875, 18849.785156, 200186.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (703, 2, 0, 86, 240703.015625, 18886.375, 199991.5, 2379, 240565.734375, 18950.453125, 200236.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (704, 2, 0, 86, 241276.390625, 18546.623047, 198840.578125, 2379, 241457.46875, 18513.589844, 198729.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (705, 2, 0, 86, 239711.921875, 18984.888672, 200108.046875, 2379, 239807.546875, 19031.367188, 200350.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (706, 2, 0, 86, 239904.125, 18760.724609, 198714.65625, 2379, 239974.75, 18711.333984, 198498.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (707, 2, 0, 86, 239914.75, 18604.779297, 197539.640625, 2379, 240064.640625, 18580.970703, 197661.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (708, 2, 0, 86, 239210.21875, 19137.259766, 200493.40625, 2379, 238997.671875, 19204.039063, 200576.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (709, 2, 0, 86, 239151.578125, 19011.443359, 199875.578125, 2379, 238940.546875, 19085.886719, 200012.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (710, 2, 0, 86, 238999.296875, 18934.021484, 199137.484375, 2379, 238833.078125, 18945.220703, 199171.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (711, 2, 0, 86, 239132.109375, 18864.066406, 198646.3125, 2379, 238982.171875, 18870.810547, 198616.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (712, 2, 0, 86, 237849.34375, 19013.429688, 198992.671875, 2379, 237905.359375, 19085.496094, 199217.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (713, 2, 0, 86, 238183.28125, 18827.220703, 198109.875, 2379, 238340.640625, 18833.839844, 198282.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (714, 2, 0, 86, 238466.546875, 18774.636719, 197724.703125, 2379, 238517.375, 18753.636719, 197490.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (715, 2, 0, 86, 237506.671875, 19008.199219, 198860.5625, 2379, 237357.859375, 19081.603516, 199022.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (716, 2, 0, 86, 237051.59375, 18839.5, 197531.609375, 2379, 236911.953125, 18850.019531, 197350.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (717, 2, 0, 841, 259941.546875, 19918.738281, 203698.328125, 2391, 259742.1875, 19841.597656, 203736.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (718, 2, 0, 841, 261086.03125, 20015.328125, 202587.65625, 2391, 261072.25, 20038.226563, 202400.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (719, 2, 0, 841, 261579.453125, 19994.783203, 199247.625, 2391, 261649.5625, 19999.597656, 199144.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (720, 2, 0, 841, 262483.21875, 20063.107422, 197405.734375, 2391, 262356.28125, 20067.039063, 197621.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (721, 2, 0, 841, 263947.8125, 20426.203125, 196263.234375, 2391, 263998.3125, 20443.109375, 196069.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (722, 2, 0, 841, 266121.0625, 20603.613281, 194060.421875, 2391, 266047.375, 20599.837891, 194176.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (723, 2, 0, 841, 260437.890625, 19708.400391, 198998.203125, 2391, 260453.390625, 19693.701172, 198858.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (724, 2, 0, 841, 263088.5625, 20182.617188, 194993.875, 2391, 262922.96875, 20200.111328, 195186.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (725, 2, 0, 841, 265134.5, 20023.234375, 193521.78125, 2391, 265013.1875, 19939.183594, 193397.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (726, 2, 0, 841, 267610.03125, 20017.958984, 192573.890625, 2391, 267350.59375, 19960.429688, 192408.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (727, 2, 0, 841, 267482.53125, 19583.707031, 191064.375, 2391, 267229.34375, 19610.013672, 191069.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (728, 2, 0, 841, 268470.125, 19679.28125, 191608.9375, 2391, 268522.0, 19620.193359, 191413.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (729, 2, 0, 841, 269571.5, 19826.298828, 191647.875, 2391, 269799.0, 19820.177734, 191584.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (730, 2, 0, 841, 270706.65625, 19969.128906, 192184.609375, 2391, 270876.3125, 19942.060547, 192074.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (731, 2, 0, 841, 271916.625, 20120.314453, 192411.5, 2391, 272047.875, 20103.380859, 192284.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (732, 2, 0, 841, 271143.53125, 19748.900391, 191047.359375, 2391, 270974.96875, 19754.332031, 190995.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (733, 2, 0, 841, 250597.65625, 19241.759766, 201798.9375, 2391, 250300.78125, 19224.808594, 201965.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (734, 2, 0, 841, 252468.796875, 19517.759766, 201904.34375, 2391, 252574.671875, 19531.330078, 201743.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (735, 2, 0, 841, 252089.03125, 19509.267578, 201132.09375, 2391, 251886.078125, 19476.474609, 200886.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (736, 2, 0, 841, 253160.265625, 19549.064453, 201582.28125, 2391, 253266.515625, 19568.292969, 201378.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (737, 2, 0, 841, 251907.90625, 19466.894531, 200181.9375, 2391, 251641.515625, 19443.160156, 200149.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (738, 2, 0, 841, 253152.8125, 19574.761719, 200760.703125, 2391, 253387.921875, 19559.142578, 200783.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (739, 2, 0, 841, 254516.703125, 19431.359375, 201358.359375, 2391, 254314.671875, 19456.828125, 201189.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (740, 2, 0, 841, 253990.90625, 19527.857422, 200230.828125, 2391, 254190.171875, 19516.804688, 200142.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (741, 2, 0, 841, 255738.4375, 19278.0625, 199938.546875, 2391, 255523.078125, 19292.0625, 199993.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (742, 2, 0, 841, 256295.59375, 19256.832031, 199601.9375, 2391, 256497.84375, 19242.0, 199677.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (743, 2, 0, 841, 252650.515625, 19578.142578, 199530.78125, 2391, 252631.625, 19573.126953, 199352.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (744, 2, 0, 841, 254343.25, 19549.976563, 198860.546875, 2391, 254184.25, 19581.382813, 198925.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (745, 2, 0, 841, 256497.578125, 19242.490234, 197895.328125, 2391, 256755.546875, 19242.015625, 197933.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (746, 2, 0, 841, 248045.5625, 19564.431641, 198915.171875, 2391, 247859.625, 19602.509766, 198743.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (747, 2, 0, 841, 248591.171875, 19621.351563, 198045.46875, 2391, 248319.0625, 19663.933594, 198109.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (748, 2, 0, 841, 249186.109375, 19477.455078, 198186.875, 2391, 249085.359375, 19494.642578, 197930.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (749, 2, 0, 841, 251477.03125, 19536.316406, 198128.875, 2391, 251379.859375, 19535.226563, 197947.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (750, 2, 0, 841, 252376.171875, 19593.998047, 198381.390625, 2391, 252362.921875, 19581.175781, 198619.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (751, 2, 0, 841, 248290.546875, 19755.982422, 197434.9375, 2391, 248432.09375, 19716.332031, 197288.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (752, 2, 0, 841, 249117.484375, 19491.017578, 196797.1875, 2391, 249348.796875, 19444.830078, 196727.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (753, 2, 0, 841, 250329.765625, 19391.552734, 197251.5625, 2391, 250299.96875, 19401.990234, 197049.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (754, 2, 0, 841, 251264.59375, 19453.791016, 196811.5625, 2391, 251500.625, 19476.046875, 196808.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (755, 2, 0, 841, 252454.921875, 19503.169922, 196474.28125, 2391, 252518.40625, 19528.734375, 196665.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (756, 2, 0, 841, 251303.75, 19416.441406, 196028.875, 2391, 251107.28125, 19411.210938, 196104.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (757, 2, 0, 841, 253227.703125, 19670.875, 197371.171875, 2391, 253245.671875, 19645.882813, 197151.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (758, 2, 0, 841, 248032.0, 19592.607422, 193015.625, 2391, 247999.875, 19580.869141, 192785.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (759, 2, 0, 841, 248825.375, 19584.720703, 193463.6875, 2391, 248727.296875, 19608.802734, 193265.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (760, 2, 0, 841, 249133.96875, 19476.648438, 194704.578125, 2391, 249097.0, 19494.763672, 194960.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (761, 2, 0, 841, 249146.390625, 19582.994141, 192101.734375, 2391, 249162.8125, 19590.576172, 192234.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (762, 2, 0, 841, 250129.71875, 19384.720703, 194585.65625, 2391, 250129.34375, 19391.539063, 194889.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (763, 2, 0, 841, 252104.8125, 19742.0625, 189839.59375, 2391, 251864.625, 19761.972656, 189745.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (764, 2, 0, 841, 251791.671875, 19828.476563, 190658.375, 2391, 251773.859375, 19833.443359, 190812.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (765, 2, 0, 841, 252223.671875, 19729.882813, 191817.65625, 2391, 252095.5625, 19758.523438, 191988.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (766, 2, 0, 841, 252273.625, 19539.457031, 193833.234375, 2391, 252212.671875, 19555.064453, 193654.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (767, 2, 0, 841, 252191.734375, 19470.601563, 194507.90625, 2391, 252384.015625, 19448.214844, 194698.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (768, 2, 0, 841, 253103.03125, 19730.984375, 190308.046875, 2391, 253022.375, 19735.439453, 190088.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (769, 2, 0, 841, 253547.734375, 19476.236328, 195317.90625, 2391, 253348.078125, 19484.619141, 195495.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (770, 2, 0, 841, 254524.875, 19458.550781, 186565.046875, 2391, 254716.8125, 19436.386719, 186511.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (771, 2, 0, 841, 254425.53125, 19567.958984, 187913.21875, 2391, 254509.484375, 19533.449219, 187704.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (772, 2, 0, 841, 253803.390625, 19579.443359, 188296.0, 2391, 253790.828125, 19600.927734, 188520.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (773, 2, 0, 841, 256073.484375, 19577.375, 188972.625, 2391, 256096.75, 19576.683594, 188795.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (774, 2, 0, 841, 255523.890625, 19639.298828, 190664.421875, 2391, 255721.4375, 19636.060547, 190465.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (775, 2, 0, 841, 255049.59375, 19584.947266, 191244.59375, 2391, 254791.15625, 19584.378906, 191359.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (776, 2, 0, 841, 257997.21875, 19823.083984, 189012.40625, 2391, 258011.171875, 19806.994141, 189198.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (777, 2, 0, 841, 257218.484375, 19585.447266, 190387.171875, 2391, 257156.453125, 19579.679688, 190101.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (778, 2, 0, 841, 257220.1875, 19599.365234, 192213.65625, 2391, 257057.578125, 19605.90625, 192420.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (779, 2, 0, 841, 256313.8125, 19426.640625, 195273.296875, 2391, 256263.78125, 19427.867188, 195581.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (780, 2, 0, 841, 258639.71875, 19863.052734, 188908.84375, 2391, 258710.703125, 19854.277344, 189099.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (781, 2, 0, 841, 258576.3125, 19821.939453, 190384.984375, 2391, 258774.75, 19842.576172, 190452.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (782, 2, 0, 841, 258185.71875, 19677.494141, 191791.203125, 2391, 258161.5, 19694.333984, 191526.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (783, 2, 0, 841, 258377.421875, 19705.447266, 192394.8125, 2391, 258164.984375, 19702.175781, 192388.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (784, 2, 0, 841, 257965.734375, 19561.089844, 195182.203125, 2391, 258116.953125, 19581.867188, 195160.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (785, 2, 0, 841, 259933.015625, 19886.277344, 189977.71875, 2391, 260057.390625, 19859.681641, 189814.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (786, 2, 0, 841, 260419.78125, 19698.115234, 191383.359375, 2391, 260366.296875, 19682.074219, 191699.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (787, 2, 0, 841, 260166.515625, 19875.046875, 187962.203125, 2391, 260169.640625, 19882.679688, 187658.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (788, 2, 0, 841, 260683.71875, 19834.822266, 188425.28125, 2391, 260604.875, 19843.859375, 188653.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (789, 2, 0, 841, 261338.90625, 19687.882813, 189566.421875, 2391, 261592.40625, 19628.265625, 189614.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (790, 2, 0, 841, 261332.921875, 19625.767578, 190346.96875, 2391, 261576.484375, 19598.382813, 190427.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (791, 2, 0, 841, 259805.578125, 19910.675781, 186472.875, 2391, 259579.140625, 19915.902344, 186416.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (792, 2, 0, 841, 261972.84375, 19816.009766, 187640.1875, 2391, 262001.296875, 19841.015625, 187489.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (793, 2, 0, 841, 263467.34375, 19671.1875, 188151.859375, 2391, 263660.5, 19640.660156, 188145.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (794, 2, 0, 841, 259847.53125, 19921.902344, 183888.21875, 2391, 259576.609375, 19919.609375, 183781.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (795, 2, 0, 841, 260477.546875, 19917.527344, 184399.65625, 2391, 260572.34375, 19918.0, 184537.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (796, 2, 0, 841, 262377.40625, 19907.142578, 185465.046875, 2391, 262188.5625, 19909.357422, 185417.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (797, 2, 0, 841, 262975.40625, 19895.9375, 185810.046875, 2391, 262906.5625, 19891.806641, 185969.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (798, 2, 0, 841, 263717.28125, 19761.193359, 186961.40625, 2391, 263960.9375, 19739.935547, 187146.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (799, 2, 0, 841, 261432.859375, 19928.228516, 183625.84375, 2391, 261223.859375, 19924.023438, 183768.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (800, 2, 0, 841, 263103.9375, 19918.398438, 185068.453125, 2391, 263254.0625, 19912.527344, 185247.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (801, 2, 0, 841, 264463.71875, 19800.857422, 186424.953125, 2391, 264363.34375, 19838.345703, 186262.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (802, 2, 0, 841, 262117.3125, 19937.029297, 183090.625, 2391, 261779.59375, 19939.34375, 182977.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (803, 2, 0, 841, 262219.46875, 19932.896484, 183640.921875, 2391, 261950.15625, 19927.90625, 183677.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (804, 2, 0, 841, 263505.46875, 19931.966797, 183732.390625, 2391, 263208.09375, 19998.0, 183719.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (805, 2, 0, 841, 264142.03125, 19926.748047, 184511.453125, 2391, 264376.9375, 19918.0, 184589.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (806, 2, 0, 841, 265577.125, 19889.064453, 185418.03125, 2391, 265398.25, 19905.929688, 185268.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (807, 2, 0, 841, 266375.875, 19906.5, 185378.0, 2391, 266424.40625, 19870.621094, 185660.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (808, 2, 0, 841, 264147.375, 19923.494141, 183507.34375, 2391, 264364.9375, 19922.416016, 183417.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (809, 2, 0, 841, 268159.6875, 19912.470703, 185851.859375, 2391, 267985.0, 19907.84375, 185919.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (810, 2, 0, 841, 269428.6875, 19933.279297, 185193.953125, 2391, 269636.875, 19937.824219, 185149.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (811, 2, 0, 841, 267279.4375, 19938.310547, 183929.203125, 2391, 267076.5625, 19966.335938, 184045.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (812, 2, 0, 841, 267741.34375, 19951.013672, 184202.5, 2391, 267574.125, 19942.871094, 184428.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (813, 2, 0, 841, 269146.28125, 20083.273438, 184094.09375, 2391, 269284.3125, 20101.414063, 183912.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (814, 2, 0, 841, 265469.46875, 19918.0, 181883.1875, 2391, 265342.6875, 19918.0, 182084.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (815, 2, 0, 841, 267170.46875, 19920.691406, 181301.734375, 2391, 267169.4375, 19924.15625, 181472.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (816, 2, 0, 841, 268024.71875, 19918.0, 180841.78125, 2391, 268060.6875, 19920.015625, 181033.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (817, 2, 0, 841, 268947.09375, 19963.820313, 180934.90625, 2391, 268938.65625, 19972.041016, 181120.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (818, 2, 0, 841, 269545.21875, 20085.679688, 182227.484375, 2391, 269381.3125, 20070.246094, 182150.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (819, 2, 0, 841, 267826.5625, 19946.488281, 175707.484375, 2391, 267861.03125, 19946.007813, 175421.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (820, 2, 0, 841, 267025.0625, 19920.443359, 175715.09375, 2391, 266838.34375, 19916.472656, 175654.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (821, 2, 0, 841, 265989.15625, 19917.988281, 176712.421875, 2391, 266012.96875, 19917.982422, 176525.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (822, 2, 0, 841, 265431.40625, 19918.0, 178202.796875, 2391, 265415.3125, 19918.0, 178023.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (823, 2, 0, 841, 268144.125, 19972.623047, 176118.03125, 2391, 268352.21875, 19979.648438, 176075.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (824, 2, 0, 841, 267551.40625, 19940.914063, 176402.78125, 2391, 267674.1875, 19930.769531, 176581.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (825, 2, 0, 841, 266522.90625, 19918.0, 176929.796875, 2391, 266693.25, 19918.0, 177020.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (826, 2, 0, 841, 266050.40625, 19918.0, 178613.625, 2391, 266230.625, 19918.0, 178510.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (827, 2, 0, 841, 268848.21875, 19956.404297, 177165.9375, 2391, 268975.5, 19948.367188, 176993.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (828, 2, 0, 841, 268461.9375, 19962.869141, 177628.640625, 2391, 268465.75, 19975.214844, 177830.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (829, 2, 0, 841, 267872.3125, 19923.789063, 178662.28125, 2391, 268101.78125, 19941.275391, 178699.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (830, 2, 0, 841, 267609.53125, 19918.0, 179280.609375, 2391, 267511.5625, 19918.0, 179509.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (831, 2, 0, 83, 284822.25, 17733.890625, 219208.859375, 2381, 284887.84375, 17742.007813, 219236.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (832, 2, 0, 83, 286077.75, 17530.65625, 219233.046875, 2381, 286187.25, 17516.691406, 219207.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (833, 2, 0, 83, 283614.1875, 17178.076172, 217853.421875, 2381, 283684.28125, 17189.078125, 217885.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (834, 2, 0, 83, 284826.5625, 17536.339844, 218825.734375, 2381, 284913.09375, 17516.566406, 218805.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (835, 2, 0, 83, 286564.625, 17236.296875, 218251.5, 2381, 286676.53125, 17232.824219, 218216.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (836, 2, 0, 83, 287998.9375, 17327.828125, 217940.78125, 2381, 288132.96875, 17336.910156, 217939.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (837, 2, 0, 83, 284061.0, 17051.044922, 217150.90625, 2381, 284113.90625, 17054.199219, 217169.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (838, 2, 0, 83, 284776.21875, 17095.662109, 217481.46875, 2381, 284747.90625, 17084.126953, 217407.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (839, 2, 0, 83, 285888.0, 17105.304688, 217682.0625, 2381, 285933.875, 17099.738281, 217615.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (840, 2, 0, 83, 287236.21875, 16992.416016, 217372.59375, 2381, 287324.6875, 17021.216797, 217462.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (841, 2, 0, 83, 289120.78125, 17093.488281, 217154.40625, 2381, 289082.03125, 17112.386719, 217213.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (842, 2, 0, 83, 284092.84375, 16876.253906, 216149.953125, 2381, 283970.125, 16901.832031, 216294.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (843, 2, 0, 83, 283652.625, 16807.804688, 215251.9375, 2381, 283751.1875, 16807.933594, 215223.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (844, 2, 0, 83, 284675.65625, 16802.712891, 215348.390625, 2381, 284780.78125, 16785.474609, 215340.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (845, 2, 0, 83, 286369.6875, 16648.25, 215411.046875, 2381, 286283.53125, 16640.761719, 215387.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (846, 2, 0, 83, 288362.1875, 16806.5625, 215573.765625, 2381, 288314.03125, 16799.480469, 215544.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (847, 2, 0, 83, 284245.03125, 16486.865234, 213957.5625, 2381, 284332.9375, 16464.732422, 213879.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (848, 2, 0, 83, 285413.34375, 16594.851563, 214736.328125, 2381, 285520.6875, 16571.828125, 214699.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (849, 2, 0, 83, 286573.40625, 16552.931641, 214410.84375, 2381, 286468.78125, 16546.931641, 214412.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (850, 2, 0, 83, 287344.28125, 16637.691406, 214685.28125, 2381, 287291.6875, 16622.759766, 214606.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (851, 2, 0, 83, 288657.34375, 16726.25, 215107.4375, 2381, 288789.4375, 16728.501953, 215103.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (852, 2, 0, 83, 289135.0, 16702.683594, 214396.671875, 2381, 289106.625, 16701.041016, 214455.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (853, 2, 0, 83, 294296.15625, 17394.277344, 213821.0, 2381, 294348.09375, 17398.005859, 213779.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (854, 2, 0, 83, 294537.71875, 17581.279297, 212527.140625, 2381, 294487.625, 17539.833984, 212660.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (855, 2, 0, 83, 295145.1875, 17880.222656, 211961.125, 2381, 295076.71875, 17870.085938, 211954.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (856, 2, 0, 83, 295647.46875, 17912.669922, 212270.390625, 2381, 295801.4375, 17927.976563, 212269.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (857, 2, 0, 83, 292785.59375, 17179.884766, 212947.921875, 2381, 292775.125, 17179.892578, 213125.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (858, 2, 0, 83, 293740.21875, 17326.3125, 213032.359375, 2381, 293743.53125, 17330.523438, 212952.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (859, 2, 0, 83, 294148.9375, 17488.765625, 211975.671875, 2381, 294106.25, 17467.925781, 212071.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (860, 2, 0, 83, 294595.8125, 17775.800781, 211375.875, 2381, 294606.625, 17795.095703, 211238.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (861, 2, 0, 83, 294044.375, 17580.777344, 210886.390625, 2381, 294115.5625, 17632.791016, 210918.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (862, 2, 0, 83, 292535.625, 17284.853516, 215745.359375, 2381, 292572.59375, 17334.421875, 215877.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (863, 2, 0, 83, 292636.6875, 17234.589844, 214970.828125, 2381, 292702.59375, 17242.169922, 215066.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (864, 2, 0, 83, 292022.0, 17020.439453, 213252.5625, 2381, 292007.46875, 17025.601563, 213311.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (865, 2, 0, 83, 292140.5625, 17042.003906, 212234.96875, 2381, 292064.15625, 17028.558594, 212153.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (866, 2, 0, 83, 292160.90625, 17203.648438, 215318.21875, 2381, 292171.84375, 17207.808594, 215470.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (867, 2, 0, 83, 291288.40625, 16931.929688, 212216.046875, 2381, 291273.75, 16934.646484, 212297.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (868, 2, 0, 83, 292042.375, 17357.419922, 216481.296875, 2381, 291993.96875, 17330.505859, 216428.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (869, 2, 0, 83, 291615.03125, 17176.197266, 215501.84375, 2381, 291619.4375, 17180.5, 215639.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (870, 2, 0, 83, 291507.9375, 17078.539063, 214604.515625, 2381, 291434.625, 17079.347656, 214529.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (871, 2, 0, 83, 291039.71875, 16962.375, 213559.578125, 2381, 291053.09375, 16967.199219, 213659.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (872, 2, 0, 83, 290526.8125, 16871.072266, 212626.015625, 2381, 290567.25, 16877.484375, 212708.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (873, 2, 0, 83, 290228.84375, 16844.085938, 211306.046875, 2381, 290153.0625, 16848.171875, 211164.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (874, 2, 0, 83, 290024.09375, 16777.207031, 212952.71875, 2381, 289893.625, 16748.695313, 212972.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (875, 2, 0, 83, 289060.8125, 16644.136719, 212395.390625, 2381, 288950.21875, 16642.132813, 212397.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (876, 2, 0, 83, 288450.09375, 16631.068359, 211572.9375, 2381, 288439.40625, 16617.28125, 211662.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (877, 2, 0, 83, 287429.5625, 16480.537109, 210756.359375, 2381, 287325.90625, 16456.039063, 210821.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (878, 2, 0, 83, 286384.53125, 16331.933594, 210867.890625, 2381, 286451.5, 16326.985352, 210888.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (879, 2, 0, 83, 285302.65625, 16276.582031, 210111.359375, 2381, 285306.90625, 16257.845703, 210175.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (880, 2, 0, 83, 284644.8125, 16196.120117, 209648.0, 2381, 284566.40625, 16177.330078, 209718.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (881, 2, 0, 83, 289451.84375, 16696.171875, 213622.1875, 2381, 289530.0625, 16717.439453, 213730.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (882, 2, 0, 83, 288559.90625, 16647.673828, 213221.703125, 2381, 288454.34375, 16616.888672, 213231.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (883, 2, 0, 83, 287756.09375, 16506.1875, 212217.21875, 2381, 287707.78125, 16502.519531, 212308.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (884, 2, 0, 83, 286742.53125, 16339.956055, 211937.9375, 2381, 286805.96875, 16351.984375, 211983.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (885, 2, 0, 83, 285729.21875, 16279.995117, 211903.421875, 2381, 285604.90625, 16275.915039, 211803.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (886, 2, 0, 83, 284614.1875, 16213.71875, 210899.046875, 2381, 284615.96875, 16206.431641, 210959.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (887, 2, 0, 83, 285944.15625, 16399.064453, 213239.4375, 2381, 285921.0625, 16387.724609, 213168.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (888, 2, 0, 83, 284815.21875, 16254.919922, 212339.875, 2381, 284943.34375, 16255.698242, 212361.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (889, 2, 0, 83, 283568.03125, 16205.65625, 211184.578125, 2381, 283619.375, 16191.97168, 211238.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (890, 2, 0, 83, 283695.59375, 16246.560547, 212620.40625, 2381, 283566.625, 16256.818359, 212727.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (891, 2, 0, 83, 282412.5625, 16308.060547, 210386.9375, 2381, 282469.5625, 16294.056641, 210303.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (892, 2, 0, 83, 283229.25, 16224.563477, 209759.0, 2381, 283310.59375, 16222.131836, 209708.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (893, 2, 0, 83, 283712.6875, 16274.303711, 208931.1875, 2381, 283699.71875, 16262.994141, 209050.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (894, 2, 0, 83, 284938.40625, 16354.401367, 208683.03125, 2381, 284977.90625, 16366.230469, 208572.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (895, 2, 0, 83, 281286.71875, 16573.257813, 210276.4375, 2381, 281329.96875, 16562.861328, 210262.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (896, 2, 0, 83, 284394.84375, 16356.744141, 207900.390625, 2381, 284494.34375, 16366.451172, 207786.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (897, 2, 0, 83, 285262.4375, 16486.917969, 207618.875, 2381, 285198.25, 16476.4375, 207563.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (898, 2, 0, 83, 281952.96875, 16579.484375, 208228.171875, 2381, 281979.65625, 16577.871094, 208167.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (899, 2, 0, 83, 282754.84375, 16426.738281, 208337.28125, 2381, 282687.21875, 16441.34375, 208306.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (900, 2, 0, 83, 283411.75, 16414.273438, 207628.0, 2381, 283435.21875, 16416.490234, 207521.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (901, 2, 0, 83, 284259.71875, 16386.761719, 207047.6875, 2381, 284217.53125, 16383.257813, 207013.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (902, 2, 0, 83, 281312.125, 16882.890625, 207737.15625, 2381, 281383.0, 16845.808594, 207769.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (903, 2, 0, 83, 281938.15625, 16769.996094, 207264.109375, 2381, 282013.84375, 16715.261719, 207328.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (904, 2, 0, 83, 282714.78125, 16571.072266, 206574.421875, 2381, 282819.125, 16537.378906, 206516.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (905, 2, 0, 83, 283625.34375, 16356.303711, 205869.25, 2381, 283549.40625, 16363.011719, 205944.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (906, 2, 0, 83, 282541.9375, 16330.802734, 205045.265625, 2381, 282605.375, 16320.927734, 205058.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (907, 2, 0, 83, 283346.59375, 16306.748047, 204527.8125, 2381, 283427.0625, 16292.666016, 204594.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (908, 2, 0, 83, 284237.4375, 16344.712891, 204673.578125, 2381, 284172.1875, 16337.149414, 204625.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (909, 2, 0, 83, 285139.09375, 16404.910156, 204977.21875, 2381, 285178.90625, 16416.855469, 204904.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (910, 2, 0, 83, 282131.21875, 16275.432617, 203934.640625, 2381, 282168.90625, 16275.174805, 203887.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (911, 2, 0, 83, 283771.0, 16305.205078, 203443.421875, 2381, 283902.9375, 16311.69043, 203378.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (912, 2, 0, 83, 284619.96875, 16308.884766, 203479.296875, 2381, 284748.5, 16318.920898, 203418.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (913, 2, 0, 83, 281105.0, 17029.126953, 206626.234375, 2381, 281106.125, 17022.539063, 206544.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (914, 2, 0, 83, 282047.875, 16780.486328, 206076.46875, 2381, 281939.3125, 16818.494141, 206063.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (915, 2, 0, 83, 281543.03125, 16854.242188, 205648.734375, 2381, 281641.59375, 16779.316406, 205530.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (916, 2, 0, 83, 281607.4375, 16494.304688, 202950.828125, 2381, 281591.125, 16504.615234, 203142.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (917, 2, 0, 83, 282755.71875, 16368.509766, 202843.1875, 2381, 282638.75, 16375.015625, 202877.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (918, 2, 0, 83, 283823.34375, 16415.453125, 202226.46875, 2381, 283729.1875, 16417.228516, 202275.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (919, 2, 0, 83, 284758.71875, 16434.558594, 201689.796875, 2381, 284881.9375, 16444.417969, 201604.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (920, 2, 0, 83, 281698.84375, 16478.195313, 201903.5, 2381, 281561.40625, 16507.736328, 202006.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (921, 2, 0, 83, 282477.28125, 16426.308594, 201981.65625, 2381, 282490.71875, 16438.453125, 201911.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (922, 2, 0, 83, 284295.09375, 16508.398438, 201118.109375, 2381, 284226.59375, 16511.521484, 201144.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (923, 2, 0, 83, 281894.84375, 16594.943359, 200833.65625, 2381, 282001.03125, 16622.951172, 200807.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (924, 2, 0, 83, 282538.15625, 16571.285156, 200924.15625, 2381, 282607.9375, 16571.9375, 200822.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (925, 2, 0, 83, 283216.90625, 16599.46875, 200415.6875, 2381, 283236.03125, 16606.363281, 200319.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (926, 2, 0, 83, 283978.71875, 16606.925781, 199741.484375, 2381, 284121.59375, 16615.035156, 199682.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (927, 2, 0, 83, 275426.6875, 17304.300781, 196642.28125, 2381, 275401.53125, 17288.476563, 196747.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (928, 2, 0, 83, 275891.125, 17097.595703, 197779.53125, 2381, 275785.4375, 17098.621094, 197673.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (929, 2, 0, 83, 277059.28125, 16995.59375, 198571.984375, 2381, 277122.5, 16992.271484, 198674.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (930, 2, 0, 83, 278223.6875, 16938.091797, 199531.578125, 2381, 278127.15625, 16943.453125, 199474.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (931, 2, 0, 83, 279366.96875, 16940.933594, 200107.078125, 2381, 279282.46875, 16944.736328, 200093.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (932, 2, 0, 83, 276843.625, 17136.695313, 196809.609375, 2381, 276872.34375, 17124.091797, 196886.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (933, 2, 0, 83, 277621.5625, 16906.853516, 197978.1875, 2381, 277709.0625, 16908.792969, 198031.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (934, 2, 0, 83, 278974.96875, 16906.996094, 198764.46875, 2381, 278931.53125, 16892.253906, 198652.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (935, 2, 0, 83, 280895.53125, 16698.636719, 200159.375, 2381, 280821.84375, 16709.527344, 200143.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (936, 2, 0, 83, 282248.09375, 16678.710938, 199597.71875, 2381, 282136.0625, 16675.9375, 199707.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (937, 2, 0, 83, 283000.1875, 16706.28125, 198897.59375, 2381, 282888.90625, 16715.275391, 198869.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (938, 2, 0, 83, 284338.03125, 16712.841797, 198675.234375, 2381, 284248.84375, 16713.330078, 198681.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (939, 2, 0, 83, 285482.75, 16617.484375, 198297.828125, 2381, 285525.65625, 16587.253906, 198238.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (940, 2, 0, 83, 286347.5625, 16474.9375, 198215.828125, 2381, 286380.65625, 16457.59375, 198146.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (941, 2, 0, 83, 282108.09375, 16700.501953, 198412.375, 2381, 281969.59375, 16708.925781, 198414.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (942, 2, 0, 83, 282844.8125, 16657.884766, 197984.0, 2381, 282872.5625, 16658.990234, 198005.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (943, 2, 0, 83, 283874.5625, 16662.064453, 197806.875, 2381, 283893.65625, 16651.484375, 197718.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (944, 2, 0, 83, 285010.09375, 16590.880859, 197706.109375, 2381, 284912.5, 16600.310547, 197650.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (945, 2, 0, 83, 287239.5625, 16179.305664, 196896.84375, 2381, 287328.3125, 16169.578125, 196813.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (946, 2, 0, 83, 277388.34375, 17279.734375, 195783.5625, 2381, 277413.8125, 17305.947266, 195696.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (947, 2, 0, 83, 278312.21875, 16990.511719, 196832.671875, 2381, 278361.28125, 16982.710938, 196843.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (948, 2, 0, 83, 279111.28125, 16881.839844, 197568.375, 2381, 279156.5, 16879.212891, 197650.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (949, 2, 0, 83, 279080.625, 17139.089844, 195296.25, 2381, 278971.75, 17158.541016, 195213.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (950, 2, 0, 83, 280071.90625, 16819.173828, 196809.296875, 2381, 280037.0, 16841.199219, 196723.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (951, 2, 0, 83, 280663.375, 16720.242188, 197362.421875, 2381, 280703.40625, 16688.503906, 197502.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (952, 2, 0, 83, 280795.625, 16717.59375, 198305.71875, 2381, 280802.09375, 16704.974609, 198257.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (953, 2, 0, 83, 281588.375, 16676.724609, 197586.203125, 2381, 281646.8125, 16660.458984, 197606.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (954, 2, 0, 83, 282354.46875, 16593.527344, 196865.234375, 2381, 282407.53125, 16576.845703, 196880.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (955, 2, 0, 83, 283536.9375, 16583.996094, 196776.96875, 2381, 283628.625, 16586.185547, 196776.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (956, 2, 0, 83, 284522.5625, 16568.0625, 196927.09375, 2381, 284538.25, 16566.193359, 196831.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (957, 2, 0, 83, 285371.15625, 16475.783203, 196331.5, 2381, 285501.21875, 16458.84375, 196325.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (958, 2, 0, 83, 286450.03125, 16370.625977, 196260.78125, 2381, 286414.40625, 16372.844727, 196277.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (959, 2, 0, 83, 280924.71875, 16829.757813, 196057.953125, 2381, 280885.1875, 16836.566406, 195985.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (960, 2, 0, 83, 282091.28125, 16646.382813, 195906.28125, 2381, 281979.59375, 16678.630859, 195896.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (961, 2, 0, 83, 284538.78125, 16594.572266, 195826.703125, 2381, 284669.0, 16571.820313, 195853.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (962, 2, 0, 83, 281644.0, 16996.533203, 194916.203125, 2381, 281522.5625, 17006.105469, 194913.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (963, 2, 0, 83, 283195.1875, 16870.765625, 195289.453125, 2381, 283184.3125, 16887.226563, 195229.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (964, 2, 0, 83, 284357.15625, 16847.884766, 195082.71875, 2381, 284306.5, 16867.390625, 195056.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (965, 2, 0, 83, 285438.53125, 16679.699219, 195028.203125, 2381, 285479.9375, 16698.601563, 194934.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (966, 3, 2, 536, 0.0, 0.0, 0.0, 2386, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (967, 3, 1, 0, 0.0, 0.0, 0.0, 2493, 157467.765625, 17015.207031, 274737.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (968, 3, 1, 0, 0.0, 0.0, 0.0, 2493, 162466.765625, 11569.753906, 340902.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (969, 3, 0, 81, 182451.640625, 13371.594727, 277398.84375, 2394, 182329.140625, 13305.601563, 277233.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (970, 3, 0, 81, 181811.953125, 13752.224609, 279280.5, 2394, 181630.140625, 13733.232422, 279259.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (971, 3, 0, 81, 180914.0, 13937.413086, 281027.15625, 2394, 180824.578125, 13926.989258, 280831.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (972, 3, 0, 81, 182620.03125, 14023.917969, 281157.65625, 2394, 182651.875, 14034.007813, 281001.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (973, 3, 0, 81, 181466.125, 14189.188477, 282541.6875, 2394, 181644.640625, 14174.253906, 282433.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (974, 3, 0, 81, 185406.40625, 11600.167969, 268910.25, 2394, 185382.484375, 11598.285156, 269050.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (975, 3, 0, 81, 184906.34375, 11743.722656, 271060.3125, 2394, 184886.484375, 11742.378906, 270884.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (976, 3, 0, 81, 183438.703125, 11909.582031, 272138.4375, 2394, 183591.609375, 11897.394531, 272253.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (977, 3, 0, 81, 182583.578125, 12425.633789, 273668.15625, 2394, 182724.265625, 12439.287109, 273589.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (978, 3, 0, 81, 181412.703125, 12350.780273, 273298.15625, 2394, 181431.84375, 12407.904297, 273503.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (979, 3, 0, 81, 181485.015625, 12665.930664, 274710.78125, 2394, 181560.28125, 12688.796875, 274870.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (980, 3, 0, 81, 187257.734375, 11647.620117, 265469.875, 2394, 187125.890625, 11645.734375, 265558.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (981, 3, 0, 81, 187922.015625, 11598.098633, 265536.65625, 2394, 187930.953125, 11578.480469, 265711.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (982, 3, 0, 81, 188351.953125, 11460.369141, 267155.84375, 2394, 188223.6875, 11460.5, 267021.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (983, 3, 0, 81, 187807.6875, 11437.232422, 267905.75, 2394, 187653.53125, 11422.78125, 267822.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (984, 3, 0, 81, 186946.140625, 11618.490234, 269704.90625, 2394, 187035.1875, 11603.87793, 269561.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (985, 3, 0, 81, 186827.0, 11633.966797, 271467.125, 2394, 186754.59375, 11648.837891, 271354.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (986, 3, 0, 81, 187553.25, 11439.814453, 272191.03125, 2394, 187408.359375, 11466.336914, 272293.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (987, 3, 0, 81, 186484.8125, 11650.364258, 272674.90625, 2394, 186311.921875, 11692.910156, 272627.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (988, 3, 0, 81, 186992.4375, 11444.931641, 274077.15625, 2394, 186819.15625, 11440.555664, 274125.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (989, 3, 0, 81, 187110.84375, 11501.220703, 275587.28125, 2394, 186949.859375, 11503.822266, 275621.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (990, 3, 0, 81, 187949.796875, 11418.264648, 277944.15625, 2394, 187823.796875, 11416.87207, 277776.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (991, 3, 0, 81, 187195.53125, 11472.444336, 278699.59375, 2394, 187072.703125, 11470.337891, 278814.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (992, 3, 0, 81, 188047.71875, 11426.80957, 279694.6875, 2394, 187909.546875, 11430.006836, 279697.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (993, 3, 0, 81, 187581.171875, 11428.461914, 280674.0625, 2394, 187419.046875, 11431.367188, 280659.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (994, 3, 0, 81, 187940.34375, 11424.65625, 281713.5625, 2394, 187798.5, 11434.539063, 281875.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (995, 3, 0, 81, 187847.6875, 11463.450195, 283193.15625, 2394, 187727.765625, 11449.166016, 283122.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (996, 3, 0, 81, 187053.5, 11514.706055, 285394.65625, 2394, 186813.5, 11528.59668, 285236.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (997, 3, 0, 81, 186659.234375, 11534.035156, 286311.96875, 2394, 186537.875, 11552.196289, 286452.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (998, 3, 0, 81, 187147.046875, 11622.306641, 287722.90625, 2394, 186970.703125, 11650.28418, 287744.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (999, 3, 0, 81, 187924.859375, 11508.932617, 288174.28125, 2394, 187843.546875, 11534.208008, 288341.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1000, 3, 0, 81, 188342.234375, 11511.944336, 290163.28125, 2394, 188220.75, 11517.618164, 290082.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1001, 3, 0, 81, 188636.359375, 11518.125, 291617.59375, 2394, 188547.03125, 11518.260742, 291817.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1002, 3, 0, 81, 189959.109375, 11513.391602, 293133.5, 2394, 189819.25, 11514.379883, 293129.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1003, 3, 0, 81, 186372.859375, 11563.501953, 292694.46875, 2394, 186242.1875, 11579.207031, 292780.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1004, 3, 0, 81, 187794.25, 11531.076172, 292915.9375, 2394, 187830.96875, 11537.955078, 293073.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1005, 3, 0, 81, 188909.828125, 11593.885742, 294131.71875, 2394, 188834.484375, 11593.591797, 294229.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1006, 3, 0, 81, 188857.84375, 11743.643555, 295513.3125, 2394, 188648.734375, 11735.626953, 295483.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1007, 3, 0, 81, 189956.09375, 11724.80957, 296843.5625, 2394, 189958.28125, 11730.648438, 297024.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1008, 3, 0, 81, 189589.328125, 11816.65625, 298269.21875, 2394, 189449.390625, 11840.060547, 298154.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1009, 3, 0, 81, 191049.15625, 11608.494141, 298446.78125, 2394, 190941.5, 11611.830078, 298602.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1010, 3, 0, 81, 191767.234375, 11670.453125, 296996.28125, 2394, 191878.3125, 11650.480469, 297057.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1011, 3, 0, 81, 193164.90625, 11780.084961, 295995.90625, 2394, 193274.203125, 11789.760742, 295928.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1012, 3, 0, 81, 194161.09375, 11555.458984, 297124.40625, 2394, 193983.359375, 11590.328125, 297050.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1013, 3, 0, 81, 195280.765625, 11502.860352, 296359.53125, 2394, 195519.25, 11485.292969, 296346.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1014, 3, 0, 81, 196717.828125, 11632.545898, 295320.875, 2394, 196611.09375, 11639.472656, 295213.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1015, 3, 0, 81, 192381.59375, 11549.959961, 300647.09375, 2394, 192486.03125, 11508.00293, 300690.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1016, 3, 0, 81, 194389.3125, 11591.619141, 300791.15625, 2394, 194210.109375, 11639.610352, 300829.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1017, 3, 0, 81, 196037.9375, 11656.338867, 301128.59375, 2394, 196203.46875, 11662.515625, 301009.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1018, 3, 0, 81, 193040.109375, 11499.654297, 298950.46875, 2394, 192832.921875, 11488.257813, 298969.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1019, 3, 0, 81, 195183.671875, 11589.31543, 299103.6875, 2394, 195032.421875, 11601.283203, 299141.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1020, 3, 0, 81, 192677.578125, 11565.552734, 308392.28125, 2394, 192900.21875, 11585.798828, 308485.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1021, 3, 0, 81, 193689.46875, 11540.083984, 310851.6875, 2394, 193565.828125, 11540.379883, 310741.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1022, 3, 0, 81, 192386.203125, 11458.795898, 304107.96875, 2394, 192417.609375, 11453.378906, 304282.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1023, 3, 0, 81, 193703.546875, 11575.206055, 306641.03125, 2394, 193730.890625, 11552.770508, 306418.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1024, 3, 0, 81, 194453.25, 11677.447266, 308352.03125, 2394, 194631.875, 11675.994141, 308382.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1025, 3, 0, 81, 195011.53125, 11563.143555, 310331.78125, 2394, 195223.8125, 11517.099609, 310330.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1026, 3, 0, 81, 193363.4375, 11492.651367, 302633.09375, 2394, 193204.875, 11501.724609, 302474.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1027, 3, 0, 81, 194582.734375, 11533.09375, 304457.5625, 2394, 194456.328125, 11525.676758, 304313.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1028, 3, 0, 81, 195362.578125, 11671.361328, 306429.75, 2394, 195470.625, 11694.066406, 306607.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1029, 3, 0, 81, 196925.234375, 11609.293945, 308508.375, 2394, 196826.421875, 11616.360352, 308348.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1030, 3, 0, 81, 191766.515625, 11650.899414, 319976.59375, 2394, 191601.75, 11671.750977, 319789.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1031, 3, 0, 81, 191590.640625, 11614.724609, 321668.15625, 2394, 191471.0625, 11601.053711, 321737.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1032, 3, 0, 81, 190178.984375, 11520.579102, 325543.125, 2394, 190210.875, 11520.750977, 325349.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1033, 3, 0, 81, 187791.734375, 11605.075195, 326206.875, 2394, 187611.046875, 11595.448242, 326412.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1034, 3, 0, 81, 184043.0625, 11580.436523, 329214.28125, 2394, 183820.03125, 11563.725586, 329068.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1035, 3, 0, 81, 185247.578125, 11529.098633, 330223.59375, 2394, 185214.328125, 11529.141602, 329996.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1036, 3, 0, 81, 181424.296875, 11635.498047, 331922.125, 2394, 181539.28125, 11589.731445, 332015.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1037, 3, 0, 81, 183491.046875, 11568.40918, 333642.625, 2394, 183353.859375, 11574.762695, 333485.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1038, 3, 0, 81, 177869.78125, 11511.385742, 335387.3125, 2394, 177636.578125, 11493.999023, 335398.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1039, 3, 0, 81, 180965.484375, 11552.848633, 334796.75, 2394, 180808.609375, 11523.948242, 334593.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1040, 3, 0, 81, 177343.25, 11533.618164, 337701.53125, 2394, 177431.921875, 11519.120117, 337926.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1041, 3, 0, 81, 180565.28125, 11527.074219, 337796.21875, 2394, 180400.71875, 11531.120117, 337671.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1042, 3, 0, 81, 174501.34375, 11613.079102, 336616.96875, 2394, 174572.734375, 11635.96875, 336391.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1043, 3, 0, 81, 173731.046875, 11536.708008, 338022.0, 2394, 173736.28125, 11564.185547, 337742.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1044, 3, 0, 81, 171587.75, 11202.178711, 339855.3125, 2394, 171740.015625, 11175.070313, 339741.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1045, 3, 0, 81, 169764.5, 11034.068359, 340862.59375, 2394, 169933.84375, 11020.666016, 340722.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1046, 3, 0, 81, 168802.046875, 11361.929688, 342148.5, 2394, 168834.21875, 11366.069336, 342398.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1047, 3, 0, 81, 167617.96875, 11410.677734, 340996.65625, 2394, 167803.109375, 11394.929688, 341014.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1048, 3, 0, 81, 167180.75, 11187.688477, 339554.03125, 2394, 167050.34375, 11192.324219, 339590.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1049, 3, 0, 81, 166065.328125, 11400.806641, 338771.03125, 2394, 165948.265625, 11469.207031, 338606.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1050, 3, 0, 81, 167631.9375, 11493.332031, 343764.9375, 2394, 167570.578125, 11480.765625, 343936.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1051, 3, 0, 81, 166599.171875, 11562.732422, 342357.0625, 2394, 166470.3125, 11551.390625, 342238.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1052, 3, 0, 81, 165224.484375, 11295.34082, 340593.59375, 2394, 165288.359375, 11281.135742, 340729.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1053, 3, 0, 81, 165302.546875, 11529.522461, 344520.6875, 2394, 165528.65625, 11520.385742, 344500.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1054, 3, 0, 81, 165364.28125, 11504.868164, 342805.875, 2394, 165533.015625, 11497.740234, 342921.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1055, 3, 0, 81, 163878.375, 11470.260742, 340855.71875, 2394, 163811.265625, 11472.28418, 340916.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1056, 3, 0, 81, 163216.25, 11641.541992, 338875.65625, 2394, 163120.109375, 11666.143555, 338657.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1057, 3, 0, 81, 160432.046875, 11520.000977, 343193.9375, 2394, 160553.28125, 11520.157227, 343350.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1058, 3, 0, 81, 159033.609375, 11477.574219, 342966.71875, 2394, 159127.5, 11591.799805, 342855.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1059, 3, 0, 81, 160992.5625, 11748.295898, 340449.25, 2394, 160915.125, 11770.742188, 340203.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1060, 3, 0, 81, 159718.9375, 11805.171875, 339965.90625, 2394, 159632.90625, 11809.094727, 340087.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1061, 3, 0, 82, 187963.21875, 11528.389648, 291085.21875, 2395, 187735.28125, 11544.016602, 291068.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1062, 3, 0, 82, 187170.75, 11527.592773, 292501.0625, 2395, 187115.234375, 11530.37207, 292321.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1063, 3, 0, 82, 194114.6875, 11543.514648, 298527.9375, 2395, 193975.34375, 11530.208008, 298340.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1064, 3, 0, 82, 193297.0, 11470.700195, 303831.78125, 2395, 193328.09375, 11473.199219, 304074.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1065, 3, 0, 82, 196223.765625, 11555.211914, 298554.96875, 2395, 196273.78125, 11526.288086, 298341.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1066, 3, 0, 82, 195324.328125, 11628.273438, 300042.5625, 2395, 195401.09375, 11646.09375, 300230.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1067, 3, 0, 82, 194794.609375, 11554.415039, 303230.875, 2395, 194846.578125, 11560.509766, 303125.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1068, 3, 0, 82, 194824.671875, 11545.227539, 305279.40625, 2395, 194702.1875, 11547.337891, 305486.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1069, 3, 0, 82, 193791.296875, 11590.608398, 309527.5625, 2395, 193604.125, 11574.417969, 309584.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1070, 3, 0, 82, 193156.140625, 11587.402344, 311710.8125, 2395, 193007.078125, 11621.859375, 311907.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1071, 3, 0, 82, 194519.734375, 11517.474609, 311349.25, 2395, 194654.28125, 11517.464844, 311562.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1072, 3, 0, 82, 190742.515625, 11781.418945, 321304.8125, 2395, 190581.03125, 11841.654297, 321193.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1073, 3, 0, 82, 192262.828125, 11547.402344, 321049.3125, 2395, 192431.265625, 11533.620117, 320942.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1074, 3, 0, 82, 191605.46875, 11523.173828, 322761.03125, 2395, 191459.609375, 11524.302734, 322962.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1075, 3, 0, 82, 188083.984375, 11654.984375, 325299.96875, 2395, 188260.109375, 11652.935547, 325053.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1076, 3, 0, 82, 188833.875, 11557.732422, 326267.375, 2395, 188865.609375, 11547.615234, 326443.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1077, 3, 0, 82, 187274.890625, 11541.426758, 328073.21875, 2395, 187380.21875, 11530.554688, 327921.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1078, 3, 0, 82, 185579.296875, 11529.783203, 328746.46875, 2395, 185664.109375, 11530.455078, 328593.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1079, 3, 0, 82, 185786.5625, 11529.007813, 330683.78125, 2395, 185971.640625, 11529.001953, 330606.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1080, 3, 0, 82, 184050.953125, 11530.810547, 330637.6875, 2395, 183931.578125, 11530.851563, 330838.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1081, 3, 0, 82, 184266.390625, 11567.143555, 332547.59375, 2395, 184317.5625, 11562.474609, 332443.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1082, 3, 0, 82, 182070.1875, 11574.515625, 329946.90625, 2395, 181883.921875, 11602.65332, 329846.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1083, 3, 0, 82, 180700.546875, 11680.777344, 330919.15625, 2395, 180545.125, 11713.316406, 330973.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1084, 3, 0, 82, 177355.34375, 11505.740234, 334159.90625, 2395, 177502.234375, 11481.65625, 333985.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1085, 3, 0, 82, 180709.40625, 11645.09375, 332989.28125, 2395, 180489.84375, 11671.027344, 333056.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1086, 3, 0, 82, 179562.546875, 11395.953125, 334802.84375, 2395, 179324.6875, 11389.875977, 334779.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1087, 3, 0, 82, 178085.515625, 11465.398438, 336411.875, 2395, 178240.0625, 11455.764648, 336406.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1088, 3, 0, 82, 176584.140625, 11555.53418, 336873.90625, 2395, 176318.046875, 11573.37793, 336950.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1089, 3, 0, 82, 182163.140625, 11602.461914, 334251.53125, 2395, 182174.84375, 11590.128906, 334422.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1090, 3, 0, 82, 180548.828125, 11513.448242, 335967.15625, 2395, 180352.6875, 11511.904297, 336083.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1091, 3, 0, 82, 178541.390625, 11477.967773, 337813.53125, 2395, 178662.734375, 11473.878906, 337761.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1092, 3, 0, 82, 176617.921875, 11503.00293, 338660.5, 2395, 176492.25, 11480.179688, 338931.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1093, 3, 0, 92, 174785.78125, 11472.228516, 338965.0, 2399, 174821.53125, 11461.250977, 339133.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1094, 3, 0, 92, 173033.703125, 11458.691406, 338531.96875, 2399, 172836.28125, 11450.525391, 338392.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1095, 3, 0, 92, 172549.984375, 11147.24707, 340935.5, 2399, 172698.921875, 11152.557617, 340785.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1096, 3, 0, 92, 171421.109375, 11087.782227, 340602.59375, 2399, 171206.5, 11083.206055, 340494.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1097, 3, 0, 92, 169671.21875, 11155.598633, 341867.34375, 2399, 169504.546875, 11190.607422, 341706.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1098, 3, 0, 92, 168973.59375, 11090.248047, 339700.96875, 2399, 168928.1875, 11101.004883, 339501.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1099, 3, 0, 92, 167712.453125, 11367.587891, 338334.4375, 2399, 167764.34375, 11423.181641, 338125.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1100, 3, 0, 92, 168510.15625, 11404.675781, 344239.09375, 2399, 168641.875, 11367.014648, 344418.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1101, 3, 0, 92, 167933.890625, 11482.354492, 342875.4375, 2399, 167835.109375, 11475.533203, 342655.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1102, 3, 0, 92, 166546.0, 11544.630859, 343474.5, 2399, 166503.546875, 11545.109375, 343577.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1103, 3, 0, 92, 165612.390625, 11454.353516, 345680.5625, 2399, 165802.703125, 11443.134766, 345627.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1104, 3, 0, 92, 164650.015625, 11609.239258, 344030.84375, 2399, 164491.296875, 11625.757813, 344108.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1105, 3, 0, 92, 164741.359375, 11558.18457, 342771.75, 2399, 164535.671875, 11572.207031, 342673.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1106, 3, 0, 92, 161447.625, 11574.255859, 336767.375, 2399, 161187.140625, 11553.769531, 336677.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1107, 3, 0, 92, 165000.140625, 11388.803711, 339856.09375, 2399, 164876.921875, 11413.654297, 339779.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1108, 3, 0, 92, 162859.390625, 11596.618164, 339794.8125, 2399, 162662.578125, 11609.287109, 339768.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1109, 3, 0, 92, 161086.03125, 11773.657227, 339034.0625, 2399, 161232.09375, 11760.091797, 338978.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1110, 3, 0, 92, 160093.890625, 11753.549805, 338260.25, 2399, 159913.015625, 11743.624023, 338326.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1111, 3, 0, 92, 157664.875, 11765.194336, 338347.09375, 2399, 157780.03125, 11751.083984, 338384.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1112, 3, 0, 92, 162134.5, 11573.411133, 343301.65625, 2399, 162078.890625, 11563.610352, 343456.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1113, 3, 0, 92, 160915.5625, 11667.907227, 342450.78125, 2399, 160785.140625, 11697.853516, 342341.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1114, 3, 0, 92, 161203.984375, 11732.074219, 341206.375, 2399, 161394.4375, 11702.767578, 341351.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1115, 3, 0, 92, 159891.265625, 11424.0625, 344254.25, 2399, 159790.78125, 11396.808594, 344320.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1116, 3, 0, 92, 157938.171875, 11404.055664, 343377.59375, 2399, 157965.453125, 11386.643555, 343621.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1117, 3, 0, 92, 158276.375, 11584.302734, 342241.03125, 2399, 158306.0, 11621.28125, 342055.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1118, 3, 0, 92, 156452.40625, 11477.005859, 340542.21875, 2399, 156371.28125, 11444.875, 340654.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1119, 3, 0, 92, 155078.40625, 11515.388672, 339096.71875, 2399, 155242.84375, 11514.796875, 339116.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1120, 3, 0, 74, 163020.171875, 17024.324219, 266420.125, 2382, 162934.046875, 17094.662109, 266568.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1121, 3, 0, 74, 162044.5625, 16925.943359, 265314.15625, 2382, 161881.09375, 16938.742188, 265530.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1122, 3, 0, 74, 164571.125, 17373.484375, 270620.65625, 2382, 164770.40625, 17344.546875, 270815.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1123, 3, 0, 74, 162828.015625, 17309.050781, 270434.625, 2382, 162708.421875, 17287.246094, 270355.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1124, 3, 0, 74, 161542.890625, 17100.4375, 269202.625, 2382, 161400.5625, 17081.269531, 269086.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1125, 3, 0, 74, 161752.59375, 17103.654297, 267635.5, 2382, 161773.234375, 17118.558594, 267882.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1126, 3, 0, 74, 160355.921875, 17107.449219, 267686.46875, 2382, 160152.296875, 17108.013672, 267723.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1127, 3, 0, 74, 158883.421875, 17098.914063, 266753.21875, 2382, 158637.0625, 17082.085938, 266697.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1128, 3, 0, 74, 159432.546875, 17194.685547, 265003.96875, 2382, 159367.6875, 17167.509766, 265138.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1129, 3, 0, 74, 156882.328125, 17066.335938, 267143.21875, 2382, 156685.0625, 17091.046875, 267152.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1130, 3, 0, 74, 153575.328125, 17039.839844, 268834.46875, 2382, 153731.625, 17032.240234, 268905.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1131, 3, 0, 74, 152648.15625, 16851.996094, 270402.53125, 2382, 152838.609375, 16833.144531, 270294.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1132, 3, 0, 74, 156711.171875, 16999.566406, 268361.0, 2382, 156680.65625, 16973.101563, 268510.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1133, 3, 0, 74, 155406.234375, 16988.970703, 269176.65625, 2382, 155389.3125, 16923.783203, 269353.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1134, 3, 0, 74, 154268.640625, 16843.667969, 270121.65625, 2382, 154247.8125, 16783.3125, 270270.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1135, 3, 0, 74, 157946.890625, 17002.203125, 269324.0, 2382, 158193.34375, 17010.820313, 269358.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1136, 3, 0, 74, 156803.6875, 16955.585938, 270085.6875, 2382, 156644.21875, 16924.488281, 269971.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1137, 3, 0, 74, 154767.65625, 16767.894531, 271553.8125, 2382, 155021.296875, 16744.808594, 271557.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1138, 3, 0, 74, 163804.15625, 17337.210938, 273128.75, 2382, 163690.40625, 17345.056641, 273010.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1139, 3, 0, 74, 161613.359375, 17141.78125, 271660.3125, 2382, 161842.203125, 17166.074219, 271697.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1140, 3, 0, 74, 159473.546875, 17040.160156, 269933.5, 2382, 159226.375, 17030.753906, 269851.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1141, 3, 0, 74, 161612.703125, 17116.150391, 273038.03125, 2382, 161858.265625, 17108.753906, 273109.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1142, 3, 0, 74, 160467.046875, 17077.230469, 272241.375, 2382, 160277.71875, 17072.197266, 272187.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1143, 3, 0, 74, 158017.8125, 16979.804688, 270718.875, 2382, 157898.390625, 16969.294922, 270884.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1144, 3, 0, 74, 151858.515625, 16696.480469, 275408.28125, 2382, 151932.46875, 16662.152344, 275589.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1145, 3, 0, 74, 154571.0, 16856.498047, 279365.15625, 2382, 154519.75, 16830.382813, 279218.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1146, 3, 0, 74, 153170.53125, 16824.505859, 274027.8125, 2382, 153077.890625, 16872.09375, 274122.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1147, 3, 0, 74, 153312.9375, 16643.996094, 275715.625, 2382, 153394.703125, 16642.109375, 275889.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1148, 3, 0, 74, 154415.9375, 16688.65625, 277418.59375, 2382, 154267.453125, 16655.408203, 277468.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1149, 3, 0, 74, 155820.328125, 16976.490234, 278744.53125, 2382, 155722.28125, 16931.982422, 278639.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1150, 3, 0, 74, 157365.296875, 16940.972656, 272310.34375, 2382, 157421.1875, 16939.335938, 272063.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1151, 3, 0, 74, 156531.0625, 16854.552734, 273000.71875, 2382, 156703.828125, 16875.160156, 273090.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1152, 3, 0, 74, 156399.21875, 16860.011719, 274124.09375, 2382, 156220.21875, 16840.714844, 274214.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1153, 3, 0, 74, 155702.234375, 16829.972656, 275378.78125, 2382, 155564.296875, 16793.935547, 275552.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1154, 3, 0, 74, 157954.78125, 17018.40625, 273554.6875, 2382, 157947.09375, 17017.941406, 273291.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1155, 3, 0, 74, 156927.265625, 16968.976563, 275361.84375, 2382, 156737.40625, 16952.193359, 275494.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1156, 3, 0, 74, 164797.671875, 17122.839844, 277824.0, 2382, 164920.328125, 17097.734375, 278058.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1157, 3, 0, 74, 164459.578125, 17223.804688, 276375.75, 2382, 164544.828125, 17212.916016, 276580.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1158, 3, 0, 74, 163202.765625, 17175.199219, 275391.625, 2382, 163148.796875, 17162.417969, 275568.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1159, 3, 0, 74, 164195.640625, 17053.185547, 279409.65625, 2382, 164367.875, 17046.833984, 279544.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1160, 3, 0, 74, 163609.78125, 17068.671875, 278468.84375, 2382, 163756.3125, 17059.302734, 278470.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1161, 3, 0, 74, 162280.484375, 17045.0, 276684.875, 2382, 162408.25, 17045.0, 276738.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1162, 3, 0, 74, 160409.046875, 17052.832031, 274244.09375, 2382, 160242.109375, 17046.669922, 274374.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1163, 3, 0, 74, 159114.546875, 17054.460938, 275673.34375, 2382, 158886.96875, 17059.324219, 275620.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1164, 3, 0, 74, 157773.453125, 16992.382813, 276594.96875, 2382, 157775.4375, 17005.431641, 276306.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1165, 3, 0, 74, 160883.046875, 17045.0, 276672.34375, 2382, 161090.90625, 17045.0, 276521.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1166, 3, 0, 74, 160836.40625, 17094.841797, 277865.90625, 2382, 160700.140625, 17064.644531, 277742.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1167, 3, 0, 74, 159768.234375, 17052.664063, 278339.3125, 2382, 159768.296875, 17050.076172, 278197.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1168, 3, 0, 74, 175763.03125, 16348.349609, 268015.09375, 2382, 175932.078125, 16349.195313, 268159.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1169, 3, 0, 74, 174003.734375, 16523.820313, 267876.90625, 2382, 174183.546875, 16502.544922, 267813.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1170, 3, 0, 74, 173580.8125, 16635.570313, 268991.125, 2382, 173562.34375, 16615.447266, 268772.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1171, 3, 0, 74, 171439.03125, 16576.171875, 269786.84375, 2382, 171186.359375, 16580.763672, 269884.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1172, 3, 0, 74, 175761.765625, 16631.402344, 270194.1875, 2382, 175898.140625, 16601.654297, 270386.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1173, 3, 0, 74, 174812.296875, 16888.066406, 270233.96875, 2382, 174716.859375, 16912.441406, 270417.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1174, 3, 0, 74, 173579.984375, 16788.636719, 270691.90625, 2382, 173619.828125, 16784.269531, 270535.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1175, 3, 0, 74, 172095.765625, 16664.853516, 270575.125, 2382, 172274.1875, 16672.101563, 270619.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1176, 3, 0, 74, 176380.453125, 16505.103516, 271343.625, 2382, 176339.296875, 16505.6875, 271433.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1177, 3, 0, 74, 175243.546875, 16725.425781, 271432.28125, 2382, 175100.84375, 16782.892578, 271306.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1178, 3, 0, 74, 174092.046875, 16754.537109, 272048.1875, 2382, 174014.125, 16719.226563, 272184.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1179, 3, 0, 74, 171656.671875, 16649.755859, 273145.15625, 2382, 171717.453125, 16656.082031, 273315.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1180, 3, 0, 74, 176665.140625, 16445.683594, 272684.0625, 2382, 176715.015625, 16435.011719, 272855.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1181, 3, 0, 74, 171432.953125, 16734.435547, 274513.5, 2382, 171497.671875, 16726.863281, 274388.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1182, 3, 0, 74, 170904.765625, 16771.304688, 277201.3125, 2382, 170710.859375, 16787.339844, 277139.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1183, 3, 0, 74, 170582.421875, 16845.044922, 278690.59375, 2382, 170730.078125, 16833.878906, 278777.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1184, 3, 0, 74, 170803.65625, 16931.859375, 280099.46875, 2382, 170617.296875, 16965.171875, 280117.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1185, 3, 0, 74, 170614.578125, 17063.476563, 281318.0625, 2382, 170420.3125, 17043.955078, 281221.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1186, 3, 0, 74, 170276.875, 17130.46875, 283086.375, 2382, 170241.1875, 17139.304688, 283338.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1187, 3, 0, 74, 172264.21875, 16645.378906, 277111.09375, 2382, 172155.078125, 16671.642578, 277139.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1188, 3, 0, 74, 172005.8125, 16809.261719, 279617.0625, 2382, 171834.703125, 16798.628906, 279526.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1189, 3, 0, 74, 172330.671875, 17587.982422, 283776.875, 2382, 172377.109375, 17612.378906, 283579.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1190, 3, 0, 74, 173751.125, 16749.609375, 274734.03125, 2382, 173821.703125, 16782.460938, 274815.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1191, 3, 0, 74, 173778.71875, 16675.115234, 277205.125, 2382, 173616.59375, 16651.412109, 277169.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1192, 3, 0, 74, 174711.03125, 16891.976563, 275564.71875, 2382, 174858.859375, 16907.199219, 275521.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1193, 3, 0, 74, 174635.15625, 16834.273438, 276667.75, 2382, 174676.96875, 16821.710938, 276852.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1194, 3, 0, 74, 174711.890625, 17028.697266, 280003.9375, 2382, 174749.96875, 16994.916016, 279839.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1195, 3, 0, 74, 175769.625, 16863.5, 275112.0625, 2382, 175842.484375, 16874.480469, 275235.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1196, 3, 0, 74, 176088.890625, 16880.408203, 277584.09375, 2382, 176167.4375, 16896.873047, 277758.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1197, 3, 0, 74, 175977.953125, 17345.164063, 279282.65625, 2382, 176129.515625, 17368.925781, 279367.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1198, 3, 0, 74, 175738.015625, 17516.214844, 280770.0, 2382, 175753.140625, 17479.291016, 280634.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1199, 4, 2, 538, 0.0, 0.0, 0.0, 2383, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1200, 4, 1, 0, 0.0, 0.0, 0.0, 2494, 86957.085938, 11403.643555, 261219.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1201, 4, 1, 0, 0.0, 0.0, 0.0, 2494, 118585.296875, 23069.019531, 250261.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1202, 4, 0, 418, 72730.523438, 11215.501953, 260610.75, 2396, 72537.046875, 11194.181641, 260592.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1203, 4, 0, 418, 75043.28125, 11132.366211, 261175.125, 2396, 74913.164063, 11137.654297, 261168.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1204, 4, 0, 418, 75127.726563, 11166.981445, 260359.828125, 2396, 75023.875, 11172.37207, 260218.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1205, 4, 0, 418, 75739.4375, 11107.169922, 260768.21875, 2396, 75903.773438, 11104.087891, 260670.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1206, 4, 0, 418, 72598.921875, 11264.237305, 259141.34375, 2396, 72673.453125, 11288.4375, 258956.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1207, 4, 0, 418, 74178.921875, 11225.396484, 259420.4375, 2396, 74103.875, 11215.400391, 259259.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1208, 4, 0, 418, 75461.765625, 11161.0, 259317.140625, 2396, 75330.210938, 11164.919922, 259342.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1209, 4, 0, 418, 76313.046875, 11130.34668, 259708.34375, 2396, 76174.257813, 11138.902344, 259682.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1210, 4, 0, 418, 74424.109375, 11243.856445, 255144.125, 2396, 74540.6875, 11221.097656, 255012.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1211, 4, 0, 418, 75377.953125, 11229.77832, 256089.265625, 2396, 75579.210938, 11194.423828, 256155.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1212, 4, 0, 418, 76012.765625, 11315.248047, 248618.015625, 2396, 76107.359375, 11319.178711, 248662.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1213, 4, 0, 418, 75054.296875, 11266.154297, 247416.71875, 2396, 75084.328125, 11253.149414, 247285.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1214, 4, 0, 418, 75691.101563, 11329.930664, 249655.9375, 2396, 75578.429688, 11331.515625, 249678.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1215, 4, 0, 418, 74936.609375, 11342.058594, 248303.15625, 2396, 74810.804688, 11346.373047, 248344.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1216, 4, 0, 418, 74567.742188, 11309.361328, 249208.6875, 2396, 74538.09375, 11290.068359, 249355.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1217, 4, 0, 418, 74046.890625, 11282.472656, 247930.625, 2396, 73835.898438, 11275.397461, 247898.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1218, 4, 0, 418, 85982.148438, 11281.149414, 250474.625, 2396, 85850.375, 11286.603516, 250510.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1219, 4, 0, 418, 85048.789063, 11431.12793, 251390.3125, 2396, 85047.65625, 11382.042969, 251149.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1220, 4, 0, 418, 84600.289063, 11429.173828, 252536.28125, 2396, 84477.304688, 11344.476563, 252447.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1221, 4, 0, 418, 84111.132813, 11317.731445, 253112.078125, 2396, 83982.265625, 11284.519531, 253169.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1222, 4, 0, 418, 86323.617188, 11367.099609, 251778.25, 2396, 86362.054688, 11318.191406, 251903.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1223, 4, 0, 418, 85596.21875, 11405.942383, 252644.390625, 2396, 85614.226563, 11377.34375, 252808.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1224, 4, 0, 418, 85266.054688, 11342.506836, 253677.515625, 2396, 85142.796875, 11328.728516, 253790.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1225, 4, 0, 418, 86245.375, 11253.841797, 253928.609375, 2396, 86161.664063, 11265.041992, 254029.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1226, 4, 0, 418, 86500.125, 11164.302734, 254850.640625, 2396, 86383.03125, 11156.999023, 254955.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1227, 4, 0, 418, 86569.609375, 11663.530273, 259008.40625, 2396, 86651.125, 11710.302734, 259162.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1228, 4, 0, 418, 85759.546875, 11336.916016, 259519.84375, 2396, 85640.976563, 11282.230469, 259541.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1229, 4, 0, 418, 86052.21875, 11317.455078, 260501.515625, 2396, 86175.382813, 11317.244141, 260577.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1230, 4, 0, 418, 85493.710938, 11333.317383, 261122.96875, 2396, 85384.617188, 11337.214844, 261102.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1231, 4, 0, 418, 85201.453125, 11411.481445, 261873.703125, 2396, 85258.320313, 11423.84375, 262094.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1232, 4, 0, 418, 86330.414063, 11400.283203, 261620.890625, 2396, 86221.585938, 11408.082031, 261746.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1233, 4, 0, 418, 87594.179688, 11455.250977, 260590.046875, 2396, 87574.671875, 11522.441406, 260479.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1234, 4, 0, 418, 86981.203125, 11532.276367, 262031.140625, 2396, 87136.414063, 11563.121094, 262016.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1235, 4, 0, 418, 94899.242188, 11898.716797, 260349.453125, 2396, 95031.023438, 11835.816406, 260534.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1236, 4, 0, 418, 93652.695313, 12071.555664, 259610.40625, 2396, 93536.773438, 12100.547852, 259568.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1237, 4, 0, 418, 93858.203125, 11778.550781, 260937.0625, 2396, 93807.414063, 11816.56543, 261050.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1238, 4, 0, 418, 92498.734375, 11559.399414, 261964.6875, 2396, 92466.15625, 11574.650391, 261876.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1239, 4, 0, 418, 90828.953125, 11857.761719, 260187.9375, 2396, 90953.398438, 11842.496094, 260255.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1240, 4, 0, 418, 91999.710938, 11734.836914, 263270.6875, 2396, 91939.140625, 11745.244141, 263189.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1241, 4, 0, 418, 90537.367188, 11714.145508, 261605.84375, 2396, 90716.367188, 11726.998047, 261655.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1242, 4, 0, 418, 89207.570313, 11656.889648, 260708.03125, 2396, 89280.90625, 11718.115234, 260582.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1243, 4, 0, 418, 92388.101563, 12273.417969, 268162.90625, 2396, 92436.023438, 12241.895508, 268318.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1244, 4, 0, 418, 92493.742188, 12468.459961, 266736.875, 2396, 92519.578125, 12468.076172, 266889.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1245, 4, 0, 418, 92149.460938, 12180.44043, 265926.96875, 2396, 92276.859375, 12306.605469, 265797.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1246, 4, 0, 418, 92202.84375, 12055.080078, 264621.84375, 2396, 92116.695313, 12115.287109, 264715.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1247, 4, 0, 418, 91525.789063, 11722.924805, 270075.9375, 2396, 91395.6875, 11689.28125, 270146.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1248, 4, 0, 418, 91266.492188, 11928.285156, 267239.75, 2396, 91205.398438, 11895.157227, 267378.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1249, 4, 0, 418, 90932.804688, 11946.789063, 264115.84375, 2396, 90786.15625, 11935.072266, 264224.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1250, 4, 0, 418, 89491.046875, 11845.665039, 265434.1875, 2396, 89612.007813, 11849.667969, 265556.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1251, 4, 0, 418, 89757.359375, 11853.855469, 264438.15625, 2396, 89732.234375, 11836.676758, 264206.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1252, 4, 0, 418, 88164.367188, 11557.436523, 266001.15625, 2396, 88236.0625, 11576.571289, 266177.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1253, 4, 0, 418, 88444.835938, 11703.600586, 264631.46875, 2396, 88326.921875, 11642.393555, 264735.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1254, 4, 0, 418, 88551.851563, 11701.601563, 263865.84375, 2396, 88733.757813, 11745.668945, 263876.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1255, 4, 0, 418, 84873.765625, 11518.998047, 265442.6875, 2396, 84813.1875, 11516.537109, 265644.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1256, 4, 0, 418, 85635.929688, 11489.930664, 264960.0625, 2396, 85576.890625, 11489.787109, 264823.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1257, 4, 0, 418, 86233.140625, 11426.053711, 264499.8125, 2396, 86417.648438, 11449.761719, 264498.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1258, 4, 0, 418, 87193.921875, 11524.848633, 263994.34375, 2396, 87285.328125, 11534.248047, 263816.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1259, 4, 0, 418, 84680.851563, 11470.375, 264702.03125, 2396, 84584.929688, 11477.091797, 264583.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1260, 4, 0, 418, 86416.085938, 11460.134766, 263430.15625, 2396, 86317.234375, 11420.8125, 263306.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1261, 4, 0, 418, 77489.070313, 11284.512695, 267794.3125, 2396, 77677.804688, 11280.203125, 267805.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1262, 4, 0, 418, 80140.71875, 11428.481445, 265809.25, 2396, 79965.992188, 11422.901367, 265936.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1263, 4, 0, 418, 82635.703125, 11631.694336, 265439.59375, 2396, 82833.390625, 11560.524414, 265284.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1264, 4, 0, 418, 83266.882813, 11384.110352, 271271.0, 2396, 83054.234375, 11341.373047, 271436.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1265, 4, 0, 418, 83544.71875, 11492.884766, 269402.5625, 2396, 83594.367188, 11487.111328, 269484.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1266, 4, 0, 418, 83925.210938, 11542.734375, 268877.71875, 2396, 83989.867188, 11556.535156, 268705.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1267, 4, 0, 418, 82961.28125, 11526.557617, 268869.65625, 2396, 82838.554688, 11526.800781, 268784.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1268, 4, 0, 418, 83464.8125, 11645.302734, 267577.0, 2396, 83578.414063, 11647.964844, 267454.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1269, 4, 0, 418, 101983.054688, 12487.480469, 278411.59375, 2396, 102078.726563, 12490.316406, 278221.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1270, 4, 0, 418, 100766.75, 12172.99707, 277129.5625, 2396, 100578.3125, 12093.287109, 277105.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1271, 4, 0, 418, 102148.507813, 12670.289063, 276041.1875, 2396, 101963.53125, 12601.21875, 276115.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1272, 4, 0, 418, 95564.742188, 11773.973633, 282036.0625, 2396, 95723.890625, 11763.121094, 281944.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1273, 4, 0, 418, 97662.585938, 11753.37207, 281621.09375, 2396, 97538.070313, 11732.525391, 281593.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1274, 4, 0, 418, 94756.289063, 11349.417969, 280555.375, 2396, 94650.757813, 11332.376953, 280438.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1275, 4, 0, 418, 96160.546875, 11479.825195, 280823.71875, 2396, 96134.304688, 11473.085938, 280662.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1276, 4, 0, 418, 98293.820313, 11616.447266, 280162.0, 2396, 98103.875, 11544.875, 280012.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1277, 4, 0, 418, 98272.835938, 12693.78125, 284396.59375, 2396, 98443.15625, 12679.861328, 284336.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1278, 4, 0, 418, 99746.109375, 12618.46582, 283900.375, 2396, 99932.640625, 12647.804688, 283958.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1279, 4, 0, 418, 98695.023438, 12230.820313, 282736.375, 2396, 98884.070313, 12242.447266, 282691.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1280, 4, 0, 418, 120810.492188, 18273.164063, 293630.6875, 2396, 120901.6875, 18287.369141, 293855.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1281, 4, 0, 418, 121090.234375, 18193.726563, 292278.375, 2396, 121201.359375, 18200.144531, 292381.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1282, 4, 0, 418, 121306.125, 18476.833984, 291103.0, 2396, 121248.78125, 18512.210938, 290930.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1283, 4, 0, 418, 120133.742188, 18238.744141, 291298.25, 2396, 120132.15625, 18225.732422, 291440.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1284, 4, 0, 418, 119203.679688, 18237.773438, 289618.90625, 2396, 119338.125, 18262.28125, 289695.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1285, 4, 0, 418, 118522.4375, 18185.453125, 288856.0625, 2396, 118617.289063, 18195.505859, 288714.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1286, 4, 0, 418, 118740.65625, 18334.861328, 284590.6875, 2396, 118876.617188, 18336.150391, 284453.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1287, 4, 0, 418, 117628.632813, 18311.019531, 285111.15625, 2396, 117699.757813, 18312.851563, 285290.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1288, 4, 0, 418, 116399.710938, 18347.308594, 287011.53125, 2396, 116257.28125, 18372.679688, 287158.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1289, 4, 0, 418, 115524.695313, 18447.316406, 285999.78125, 2396, 115365.703125, 18456.15625, 285899.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1290, 4, 0, 418, 116298.429688, 18351.380859, 284692.375, 2396, 116122.34375, 18369.046875, 284712.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1291, 4, 0, 418, 115947.71875, 18307.652344, 282706.6875, 2396, 115857.984375, 18310.228516, 282569.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1292, 4, 0, 418, 114335.53125, 18348.712891, 280718.28125, 2396, 114330.117188, 18375.839844, 280942.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1293, 4, 0, 418, 114979.71875, 18298.082031, 279716.5625, 2396, 115131.054688, 18312.716797, 279676.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1294, 4, 0, 418, 113767.0, 18299.039063, 276575.90625, 2396, 113601.789063, 18313.8125, 276540.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1295, 4, 0, 418, 114877.414063, 18442.349609, 273708.21875, 2396, 114918.25, 18439.482422, 273910.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1296, 4, 0, 418, 113784.835938, 18248.681641, 271019.59375, 2396, 113954.09375, 18242.072266, 271011.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1297, 4, 0, 418, 114261.414063, 18391.166016, 269833.21875, 2396, 114367.601563, 18441.861328, 269641.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1298, 4, 0, 418, 114843.023438, 18579.808594, 267611.53125, 2396, 114940.523438, 18570.832031, 267517.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1299, 4, 0, 418, 114288.445313, 18390.025391, 266916.59375, 2396, 114110.757813, 18380.84375, 266933.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1300, 4, 0, 418, 114212.78125, 18342.882813, 265590.65625, 2396, 114193.5625, 18349.552734, 265760.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1301, 4, 0, 418, 113525.617188, 18262.898438, 264636.21875, 2396, 113595.859375, 18270.681641, 264465.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1302, 4, 0, 418, 113274.195313, 18405.398438, 263387.40625, 2396, 113157.632813, 18395.943359, 263494.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1303, 4, 0, 418, 113461.523438, 18438.431641, 262634.0625, 2396, 113630.429688, 18445.611328, 262720.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1304, 4, 0, 418, 112658.570313, 18424.023438, 265425.4375, 2396, 112535.890625, 18468.923828, 265627.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1305, 4, 0, 418, 112299.882813, 18501.019531, 262599.34375, 2396, 112253.390625, 18501.117188, 262379.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1306, 4, 0, 418, 111683.03125, 18542.550781, 259046.28125, 2396, 111861.929688, 18528.210938, 259224.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1307, 4, 0, 418, 112039.101563, 18397.740234, 257886.375, 2396, 112260.492188, 18405.083984, 257907.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1308, 4, 0, 418, 111095.054688, 18397.208984, 257550.921875, 2396, 111133.351563, 18364.519531, 257383.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1309, 4, 0, 418, 110348.773438, 18319.691406, 255301.390625, 2396, 110467.710938, 18334.179688, 255464.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1310, 4, 0, 418, 110718.960938, 18335.539063, 254135.4375, 2396, 110576.25, 18346.257813, 253983.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1311, 4, 0, 418, 108795.296875, 18376.501953, 252188.296875, 2396, 108966.765625, 18350.853516, 252032.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1312, 4, 0, 418, 109494.796875, 18510.230469, 245398.28125, 2396, 109394.40625, 18497.095703, 245568.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1313, 4, 0, 418, 109154.359375, 18425.210938, 244066.453125, 2396, 108985.898438, 18451.140625, 243954.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1314, 4, 0, 418, 107274.3125, 18456.001953, 247117.625, 2396, 107440.796875, 18447.642578, 247192.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1315, 4, 0, 418, 107523.289063, 18484.152344, 249764.015625, 2396, 107657.476563, 18459.867188, 249988.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1316, 4, 0, 418, 106253.890625, 18477.349609, 247609.46875, 2396, 106284.085938, 18476.769531, 247877.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1317, 4, 0, 418, 108639.164063, 18172.830078, 232420.921875, 2396, 108453.398438, 18198.404297, 232345.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1318, 4, 0, 418, 109438.851563, 18245.484375, 235442.984375, 2396, 109318.726563, 18247.835938, 235547.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1319, 4, 0, 418, 108721.132813, 18093.996094, 234510.953125, 2396, 108599.34375, 18091.201172, 234484.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1320, 4, 0, 418, 108000.414063, 18168.808594, 233283.6875, 2396, 107928.914063, 18173.347656, 233394.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1321, 4, 0, 418, 107420.625, 18286.638672, 232417.046875, 2396, 107245.1875, 18296.59375, 232345.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1322, 4, 0, 418, 106723.820313, 18242.076172, 233271.96875, 2396, 106732.804688, 18251.759766, 233416.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1323, 4, 0, 418, 105645.289063, 18100.46875, 232392.484375, 2396, 105523.320313, 18060.011719, 232228.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1324, 4, 0, 418, 114564.03125, 18376.644531, 238595.75, 2396, 114654.273438, 18354.464844, 238872.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1325, 4, 0, 418, 114439.664063, 18408.658203, 237768.515625, 2396, 114364.125, 18415.105469, 237677.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1326, 4, 0, 418, 114770.085938, 18374.554688, 236637.25, 2396, 114800.421875, 18354.753906, 236473.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1327, 4, 0, 418, 114990.171875, 18362.798828, 233923.34375, 2396, 115146.835938, 18384.097656, 233954.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1328, 4, 0, 418, 113412.3125, 18494.542969, 238292.90625, 2396, 113364.171875, 18499.474609, 238150.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1329, 4, 0, 418, 113335.0625, 18493.914063, 236455.828125, 2396, 113276.046875, 18502.220703, 236689.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1330, 4, 0, 418, 113988.179688, 18395.191406, 235564.234375, 2396, 113876.695313, 18397.101563, 235485.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1331, 4, 0, 418, 113905.890625, 18304.523438, 234226.21875, 2396, 113991.070313, 18321.814453, 234372.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1332, 4, 0, 418, 114356.054688, 18176.771484, 232673.5, 2396, 114263.359375, 18159.875, 232578.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1333, 4, 0, 418, 112341.359375, 18524.6875, 235984.71875, 2396, 112420.257813, 18505.898438, 235816.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1334, 4, 0, 418, 111482.976563, 18591.642578, 236634.640625, 2396, 111522.890625, 18600.390625, 236755.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1335, 4, 0, 418, 111172.65625, 18459.185547, 235604.5625, 2396, 111303.359375, 18487.681641, 235718.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1336, 4, 0, 418, 112205.101563, 18461.886719, 239708.484375, 2396, 112371.117188, 18405.923828, 239853.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1337, 4, 0, 418, 111373.804688, 18557.923828, 239272.609375, 2396, 111238.21875, 18553.701172, 239333.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1338, 4, 0, 418, 61724.339844, 11377.938477, 254937.875, 2396, 61762.941406, 11414.875, 254683.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1339, 4, 0, 418, 62001.148438, 11418.082031, 257284.453125, 2396, 62064.546875, 11437.105469, 257492.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1340, 4, 0, 418, 61012.519531, 11792.821289, 259426.546875, 2396, 61064.1875, 11561.646484, 259275.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1341, 4, 0, 418, 60104.171875, 11798.102539, 261396.140625, 2396, 60234.261719, 12219.185547, 261428.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1342, 4, 0, 418, 63420.152344, 11285.18457, 255963.25, 2396, 63501.988281, 11326.095703, 255746.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1343, 4, 0, 418, 63022.808594, 11548.880859, 259073.09375, 2396, 63103.796875, 11357.811523, 258939.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1344, 4, 0, 418, 61864.242188, 11836.245117, 261071.328125, 2396, 61980.054688, 11468.990234, 261077.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1345, 4, 0, 418, 61347.027344, 11640.553711, 263139.75, 2396, 61246.46875, 11722.290039, 263251.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1346, 4, 0, 418, 61197.972656, 11580.045898, 264522.21875, 2396, 61030.574219, 11944.203125, 264544.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1347, 4, 0, 418, 62539.988281, 11474.321289, 264158.21875, 2396, 62661.019531, 11824.267578, 264069.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1348, 4, 0, 418, 62089.824219, 11533.552734, 265861.28125, 2396, 61998.785156, 12376.251953, 265702.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1349, 4, 0, 145, 120017.171875, 23031.265625, 247259.1875, 2397, 120280.515625, 23025.0, 247090.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1350, 4, 0, 145, 118862.375, 23057.597656, 246704.640625, 2397, 118818.742188, 23025.0, 246472.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1351, 4, 0, 145, 118044.554688, 22992.820313, 247543.390625, 2397, 117920.039063, 22982.744141, 247662.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1352, 4, 0, 145, 116975.421875, 22987.833984, 246601.25, 2397, 117027.226563, 23025.0, 246287.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1353, 4, 0, 145, 119285.898438, 23009.251953, 248369.640625, 2397, 119512.140625, 23023.373047, 248437.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1354, 4, 0, 145, 117828.890625, 23013.083984, 248728.765625, 2397, 117665.078125, 23015.265625, 248808.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1355, 4, 0, 145, 121216.710938, 22998.617188, 249271.421875, 2397, 121347.484375, 23009.988281, 249220.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1356, 4, 0, 145, 119314.867188, 23018.925781, 249614.40625, 2397, 119459.78125, 23037.617188, 249659.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1357, 4, 0, 145, 117921.125, 23069.429688, 250038.59375, 2397, 117794.328125, 23074.705078, 250158.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1358, 4, 0, 145, 116772.195313, 23048.691406, 250113.84375, 2397, 116685.484375, 23061.503906, 250384.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1359, 4, 0, 145, 122585.648438, 22760.623047, 250658.1875, 2397, 122781.671875, 22743.669922, 250548.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1360, 4, 0, 145, 121277.804688, 22930.964844, 250753.109375, 2397, 121157.601563, 22908.763672, 250709.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1361, 4, 0, 145, 119642.609375, 23022.714844, 250993.484375, 2397, 119884.835938, 23045.056641, 251124.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1362, 4, 0, 145, 118257.054688, 23050.96875, 251105.40625, 2397, 118421.796875, 23054.263672, 251324.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1363, 4, 0, 145, 121592.984375, 22901.859375, 252381.125, 2397, 121762.375, 22913.15625, 252517.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1364, 4, 0, 145, 119739.570313, 22999.613281, 252379.640625, 2397, 120023.125, 23004.199219, 252474.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1365, 4, 0, 145, 117552.78125, 23064.382813, 252017.328125, 2397, 117255.0625, 23074.458984, 252101.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1366, 4, 0, 145, 120209.054688, 22970.412109, 253149.21875, 2397, 120510.914063, 22956.050781, 253272.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1367, 4, 0, 145, 118815.171875, 22966.84375, 252901.09375, 2397, 118543.40625, 22966.246094, 252889.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1368, 4, 0, 145, 117480.21875, 22940.332031, 253722.125, 2397, 117351.335938, 22933.123047, 253800.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1369, 4, 0, 145, 118336.335938, 22753.132813, 255810.40625, 2397, 117986.296875, 22724.931641, 255639.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1370, 4, 0, 145, 118714.992188, 22749.363281, 257498.0, 2397, 118453.382813, 22749.863281, 257667.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1371, 4, 0, 145, 119095.585938, 22883.873047, 254302.78125, 2397, 119264.109375, 22883.966797, 254277.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1372, 4, 0, 145, 119829.976563, 22945.560547, 255824.390625, 2397, 119902.085938, 22952.8125, 255661.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1373, 4, 0, 145, 119915.773438, 22914.503906, 257213.5, 2397, 119972.414063, 22911.246094, 257377.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1374, 4, 0, 145, 119814.28125, 22887.757813, 258698.453125, 2397, 119633.851563, 22867.613281, 258894.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1375, 4, 0, 145, 122705.921875, 22804.169922, 253347.375, 2397, 122774.21875, 22810.654297, 253135.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1376, 4, 0, 145, 121666.648438, 22880.574219, 254240.046875, 2397, 121556.679688, 22888.675781, 254281.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1377, 4, 0, 145, 121969.828125, 22915.433594, 255467.9375, 2397, 121866.71875, 22905.384766, 255385.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1378, 4, 0, 145, 120909.835938, 23007.730469, 256406.8125, 2397, 121112.085938, 23011.714844, 256450.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1379, 4, 0, 145, 121154.4375, 22991.048828, 257821.40625, 2397, 121266.648438, 22970.617188, 257992.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1380, 4, 0, 145, 121305.546875, 22732.714844, 259161.953125, 2397, 121257.835938, 22729.869141, 259404.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1381, 4, 0, 145, 123916.335938, 22576.488281, 253763.265625, 2397, 124061.875, 22496.972656, 253484.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1382, 4, 0, 145, 123114.21875, 22720.050781, 254610.9375, 2397, 123136.015625, 22734.568359, 254777.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1383, 4, 0, 145, 122809.929688, 22821.009766, 256898.8125, 2397, 122831.359375, 22829.203125, 256703.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1384, 4, 0, 145, 122450.585938, 22720.449219, 258237.109375, 2397, 122376.9375, 22708.140625, 258412.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1385, 4, 0, 145, 125299.632813, 22020.738281, 253415.5625, 2397, 125456.539063, 21916.160156, 253139.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1386, 4, 0, 145, 124658.203125, 22413.240234, 255043.328125, 2397, 124714.539063, 22400.28125, 254891.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1387, 4, 0, 145, 124054.578125, 22585.382813, 256108.90625, 2397, 124107.515625, 22571.142578, 256230.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1388, 4, 0, 145, 123743.625, 22596.382813, 257267.015625, 2397, 123786.054688, 22577.722656, 257527.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1389, 4, 0, 145, 126983.898438, 21765.671875, 253342.8125, 2397, 127305.78125, 21693.242188, 253076.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1390, 4, 0, 145, 126241.476563, 22129.175781, 255016.671875, 2397, 126385.0, 22067.099609, 254728.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1391, 4, 0, 145, 125448.898438, 22308.767578, 256530.4375, 2397, 125526.484375, 22298.636719, 256408.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1392, 4, 0, 145, 125514.015625, 22298.4375, 257539.921875, 2397, 125548.960938, 22293.261719, 257718.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1393, 4, 0, 145, 124815.4375, 22393.783203, 258514.59375, 2397, 124618.867188, 22447.28125, 258820.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1394, 4, 0, 145, 128245.171875, 21829.169922, 254296.921875, 2397, 128416.296875, 21802.976563, 254106.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1395, 4, 0, 145, 127756.710938, 21966.671875, 255490.890625, 2397, 127764.851563, 21982.876953, 255726.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1396, 4, 0, 145, 126673.601563, 22161.941406, 256863.875, 2397, 126732.640625, 22160.849609, 257191.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1397, 4, 0, 145, 129358.835938, 21817.416016, 254929.34375, 2397, 129647.328125, 21792.863281, 254747.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1398, 4, 0, 145, 129069.601563, 21889.533203, 255905.9375, 2397, 129297.015625, 21879.306641, 256025.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1399, 4, 0, 145, 128124.851563, 22030.625, 257278.40625, 2397, 128235.53125, 22023.65625, 257476.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1400, 4, 0, 145, 129684.132813, 17561.126953, 236925.765625, 2397, 130127.585938, 17447.5, 237058.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1401, 4, 0, 145, 126424.171875, 18063.712891, 235304.265625, 2397, 126684.789063, 18056.423828, 235290.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1402, 4, 0, 145, 123749.734375, 18123.734375, 232243.578125, 2397, 123926.539063, 18093.808594, 232247.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1403, 4, 0, 145, 128624.304688, 17671.4375, 238827.03125, 2397, 128544.828125, 17655.917969, 238535.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1404, 4, 0, 145, 125474.460938, 18102.257813, 237125.875, 2397, 125308.875, 18055.931641, 236907.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1405, 4, 0, 145, 128101.40625, 17985.255859, 232473.28125, 2397, 128463.296875, 17934.105469, 232772.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1406, 4, 0, 145, 126593.226563, 18060.019531, 231090.265625, 2397, 126358.078125, 18070.304688, 230786.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1407, 4, 0, 145, 127141.34375, 17987.140625, 229002.9375, 2397, 127446.25, 17940.976563, 228833.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1408, 4, 0, 145, 127603.398438, 17692.886719, 226679.125, 2397, 127595.867188, 17614.027344, 226055.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1409, 4, 0, 145, 124466.9375, 18284.095703, 228892.015625, 2397, 124302.953125, 18293.167969, 229099.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1410, 4, 0, 145, 125120.90625, 18076.207031, 226503.953125, 2397, 125125.726563, 18042.332031, 226553.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1411, 4, 0, 145, 125159.023438, 17676.240234, 224653.890625, 2397, 124796.054688, 17591.388672, 224343.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1412, 4, 0, 145, 123637.5, 19433.888672, 226396.96875, 2397, 123519.351563, 18010.427734, 226081.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1413, 4, 0, 145, 128010.101563, 18526.996094, 223682.171875, 2397, 128302.984375, 17573.476563, 223881.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1414, 4, 0, 145, 126584.53125, 17481.484375, 222171.171875, 2397, 126529.796875, 17504.779297, 222454.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1415, 4, 0, 145, 128629.820313, 17438.455078, 221465.5625, 2397, 128902.273438, 17454.222656, 221674.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1416, 4, 0, 145, 130216.359375, 17166.892578, 219701.28125, 2397, 130522.726563, 17181.480469, 219828.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1417, 4, 0, 145, 124913.929688, 17330.005859, 221078.328125, 2397, 124902.34375, 17387.041016, 221405.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1418, 4, 0, 145, 126388.789063, 16963.751953, 219336.125, 2397, 126931.148438, 16993.613281, 219208.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1419, 4, 0, 145, 126793.476563, 16564.869141, 217316.953125, 2397, 127144.515625, 16612.789063, 217424.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1420, 4, 0, 145, 124094.25, 16918.710938, 219528.296875, 2397, 123914.679688, 16994.009766, 219790.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1421, 4, 0, 145, 125362.09375, 16755.773438, 218882.359375, 2397, 125248.359375, 16665.882813, 218684.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1422, 4, 0, 145, 125001.203125, 16422.607422, 217035.390625, 2397, 125055.898438, 16361.425781, 216541.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1423, 4, 0, 145, 122409.476563, 17493.917969, 223361.0, 2397, 122818.6875, 17485.230469, 223510.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1424, 4, 0, 145, 121014.648438, 17202.158203, 221018.9375, 2397, 121204.34375, 17122.875, 220540.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1425, 4, 0, 145, 121262.71875, 19298.371094, 228457.4375, 2397, 121457.195313, 18301.214844, 228292.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1426, 4, 0, 145, 120569.703125, 18498.998047, 224922.09375, 2397, 120551.257813, 17577.21875, 225203.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1427, 4, 0, 145, 119847.125, 17434.595703, 222883.90625, 2397, 119605.210938, 17424.773438, 222625.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1428, 4, 0, 145, 118110.273438, 16847.175781, 219564.171875, 2397, 118166.890625, 16708.228516, 219056.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1429, 4, 0, 145, 117028.828125, 17925.666016, 226599.5625, 2397, 116737.453125, 17831.867188, 226479.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1430, 4, 0, 145, 117761.015625, 17382.462891, 224458.203125, 2397, 117410.546875, 17431.207031, 224577.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1431, 4, 0, 145, 114715.953125, 17391.177734, 225400.203125, 2397, 114459.0, 17375.287109, 225534.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1432, 4, 0, 145, 115652.882813, 16949.699219, 220731.390625, 2397, 115332.0625, 16977.59375, 221017.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1433, 4, 0, 145, 115613.65625, 16623.755859, 219047.546875, 2397, 115359.304688, 16550.824219, 218504.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1434, 4, 0, 145, 113789.640625, 16516.474609, 219794.40625, 2397, 113545.484375, 16410.300781, 219372.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1435, 4, 0, 145, 136485.625, 15794.473633, 210359.0625, 2397, 136727.609375, 15836.267578, 210538.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1436, 4, 0, 145, 140174.90625, 15911.405273, 205419.015625, 2397, 140295.015625, 15849.795898, 205772.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1437, 4, 0, 145, 132648.5, 15808.626953, 210182.625, 2397, 132435.921875, 15798.757813, 210523.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1438, 4, 0, 145, 133611.640625, 15907.889648, 208394.390625, 2397, 133790.328125, 15861.341797, 208567.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1439, 4, 0, 145, 137100.4375, 15914.399414, 203673.71875, 2397, 136902.109375, 15942.96875, 203956.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1440, 4, 0, 145, 139898.46875, 16510.216797, 201590.75, 2397, 140043.71875, 16357.81543, 201847.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1441, 4, 0, 145, 123857.757813, 15400.077148, 209789.046875, 2397, 123878.632813, 15355.863281, 210258.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1442, 4, 0, 145, 127838.664063, 15318.12207, 208613.078125, 2397, 127675.078125, 15526.819336, 208892.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1443, 4, 0, 145, 122178.304688, 15372.024414, 209382.171875, 2397, 121764.757813, 15337.206055, 209406.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1444, 4, 0, 145, 123461.867188, 15702.417969, 208156.078125, 2397, 123325.328125, 15736.522461, 207787.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1445, 4, 0, 145, 125678.351563, 16167.163086, 205188.453125, 2397, 125400.53125, 16307.985352, 205491.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1446, 5, 2, 661, 0.0, 0.0, 0.0, 2387, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1447, 5, 1, 0, 0.0, 0.0, 0.0, 2405, 430468.03125, 22147.419922, 178851.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1448, 5, 1, 0, 0.0, 0.0, 0.0, 2405, 445211.90625, 15972.908203, 201699.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1449, 5, 0, 392, 446276.5625, 16449.396484, 212519.1875, 2398, 446447.90625, 16469.109375, 212531.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1450, 5, 0, 392, 447035.15625, 16178.533203, 211336.453125, 2398, 447050.65625, 16131.921875, 211150.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1451, 5, 0, 392, 448579.0625, 16133.595703, 209404.546875, 2398, 448454.0, 16056.174805, 209339.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1452, 5, 0, 392, 443447.8125, 16046.574219, 212335.484375, 2398, 443231.4375, 16051.443359, 212448.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1453, 5, 0, 392, 445395.9375, 15950.884766, 211456.796875, 2398, 445513.625, 15931.183594, 211376.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1454, 5, 0, 392, 447156.28125, 15668.583984, 209328.578125, 2398, 447239.0625, 15708.240234, 209131.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1455, 5, 0, 392, 448114.65625, 16040.101563, 207930.3125, 2398, 447999.4375, 16027.900391, 207896.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1456, 5, 0, 392, 448925.84375, 16716.908203, 205507.609375, 2398, 449069.40625, 16729.566406, 205452.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1457, 5, 0, 392, 449683.34375, 17209.701172, 204164.15625, 2398, 449595.6875, 17225.087891, 204067.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1458, 5, 0, 392, 449862.9375, 17143.449219, 202546.8125, 2398, 449810.34375, 17128.660156, 202641.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1459, 5, 0, 392, 446798.0625, 15594.927734, 207218.578125, 2398, 446903.4375, 15646.560547, 207198.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1460, 5, 0, 392, 447328.5625, 16001.869141, 206166.9375, 2398, 447189.3125, 15984.181641, 206187.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1461, 5, 0, 392, 447804.78125, 16301.056641, 205051.484375, 2398, 447926.6875, 16328.709961, 205030.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1462, 5, 0, 392, 448329.96875, 16733.267578, 203982.625, 2398, 448178.6875, 16661.623047, 203949.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1463, 5, 0, 392, 448521.03125, 16719.96875, 202627.09375, 2398, 448381.09375, 16685.480469, 202467.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1464, 5, 0, 392, 448399.6875, 16374.474609, 201127.390625, 2398, 448396.96875, 16379.822266, 201159.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1465, 5, 0, 392, 445483.65625, 15356.105469, 207386.203125, 2398, 445526.15625, 15354.893555, 207171.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1466, 5, 0, 392, 445896.40625, 15811.658203, 205845.75, 2398, 445766.03125, 15777.286133, 205768.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1467, 5, 0, 392, 442345.28125, 15996.705078, 210468.34375, 2398, 442187.40625, 16036.886719, 210472.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1468, 5, 0, 392, 442561.28125, 16025.167969, 208824.3125, 2398, 442645.0625, 15962.757813, 208731.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1469, 5, 0, 392, 441319.59375, 16114.083984, 209417.1875, 2398, 441175.4375, 16101.841797, 209350.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1470, 5, 0, 392, 441767.4375, 16038.044922, 207782.5625, 2398, 441799.90625, 16048.701172, 207905.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1471, 5, 0, 392, 442434.78125, 15987.236328, 206067.5625, 2398, 442442.1875, 15977.109375, 206170.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1472, 5, 0, 392, 443441.90625, 15977.335938, 205360.59375, 2398, 443322.9375, 15988.048828, 205354.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1473, 5, 0, 392, 447522.40625, 16473.300781, 202860.78125, 2398, 447423.03125, 16443.568359, 202972.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1474, 5, 0, 392, 446922.71875, 15981.123047, 201287.984375, 2398, 446803.6875, 15937.382813, 201135.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1475, 5, 0, 392, 446252.96875, 15944.990234, 200003.578125, 2398, 446190.40625, 15949.705078, 200066.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1476, 5, 0, 392, 446213.03125, 16051.757813, 202396.78125, 2398, 446241.09375, 16061.851563, 202541.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1477, 5, 0, 392, 445360.28125, 16015.964844, 201009.546875, 2398, 445367.59375, 16019.511719, 200844.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1478, 5, 0, 392, 444901.90625, 15901.320313, 199118.703125, 2398, 444942.625, 15904.348633, 199159.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1479, 5, 0, 392, 444826.1875, 15934.175781, 202628.578125, 2398, 444896.40625, 15924.833984, 202599.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1480, 5, 0, 392, 444322.75, 16012.517578, 200537.671875, 2398, 444196.09375, 16020.228516, 200581.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1481, 5, 0, 392, 445272.5, 16056.015625, 197919.921875, 2398, 445096.53125, 16020.048828, 197996.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1482, 5, 0, 392, 443825.875, 16235.050781, 196327.234375, 2398, 443793.5, 16265.298828, 196194.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1483, 5, 0, 392, 445262.9375, 16573.4375, 196195.46875, 2398, 445328.375, 16592.880859, 196086.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1484, 5, 0, 392, 446919.40625, 16718.947266, 196281.390625, 2398, 446880.5, 16729.035156, 196161.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1485, 5, 0, 392, 444646.78125, 16503.779297, 194940.546875, 2398, 444505.46875, 16491.265625, 194972.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1486, 5, 0, 392, 446217.5, 16675.970703, 195256.875, 2398, 446162.1875, 16668.568359, 195114.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1487, 5, 0, 392, 441702.28125, 16328.886719, 202510.859375, 2398, 441790.28125, 16303.561523, 202387.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1488, 5, 0, 392, 443354.75, 16138.011719, 202390.5625, 2398, 443228.78125, 16140.989258, 202491.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1489, 5, 0, 392, 443025.375, 16106.050781, 203217.890625, 2398, 443099.34375, 16064.615234, 203307.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1490, 5, 0, 392, 442322.8125, 16103.060547, 204360.171875, 2398, 442199.5625, 16124.255859, 204463.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1491, 5, 0, 392, 440831.25, 16493.947266, 204871.40625, 2398, 440858.34375, 16476.712891, 205078.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1492, 5, 0, 392, 437221.59375, 17276.523438, 200189.5625, 2398, 437092.875, 17292.363281, 200034.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1493, 5, 0, 392, 439052.3125, 16787.316406, 201872.484375, 2398, 438846.4375, 16829.458984, 201931.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1494, 5, 0, 392, 438965.0625, 16121.173828, 200027.078125, 2398, 439140.4375, 16079.019531, 200062.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1495, 5, 0, 392, 440637.9375, 16545.820313, 200958.59375, 2398, 440795.78125, 16517.171875, 200865.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1496, 5, 0, 392, 431114.9375, 16416.585938, 198030.765625, 2398, 430958.4375, 16367.265625, 197900.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1497, 5, 0, 392, 432354.5625, 16721.558594, 199115.828125, 2398, 432207.40625, 16710.205078, 199116.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1498, 5, 0, 392, 433339.0625, 17310.082031, 200030.578125, 2398, 433411.5, 17320.583984, 199971.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1499, 5, 0, 392, 432727.6875, 16440.416016, 197581.359375, 2398, 432605.46875, 16405.892578, 197489.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1500, 5, 0, 392, 434261.96875, 17054.501953, 198575.21875, 2398, 434415.96875, 17262.458984, 198656.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1501, 5, 0, 392, 434830.21875, 17434.589844, 200166.78125, 2398, 434745.71875, 17487.648438, 200085.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1502, 5, 0, 392, 437233.4375, 16594.652344, 193439.515625, 2398, 437017.0625, 16613.302734, 193440.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1503, 5, 0, 392, 440256.90625, 16100.400391, 195325.359375, 2398, 440348.3125, 16127.178711, 195423.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1504, 5, 0, 392, 438024.28125, 16250.862305, 194976.515625, 2398, 438079.5, 16233.128906, 195052.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1505, 5, 0, 392, 436393.46875, 16296.241211, 195120.484375, 2398, 436255.4375, 16278.114258, 195239.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1506, 5, 0, 392, 438712.28125, 16109.291016, 196166.640625, 2398, 438857.125, 16133.648438, 196328.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1507, 5, 0, 392, 434984.09375, 16283.631836, 195764.671875, 2398, 434847.34375, 16286.238281, 195888.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1508, 5, 0, 392, 438477.78125, 16614.302734, 197584.46875, 2398, 438643.0, 16618.501953, 197655.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1509, 5, 0, 392, 437057.8125, 16438.269531, 196875.53125, 2398, 437048.71875, 16481.126953, 197039.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1510, 5, 0, 392, 435706.3125, 16478.755859, 196637.234375, 2398, 435787.4375, 16461.824219, 196575.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1511, 5, 0, 392, 434669.78125, 16560.148438, 196849.5625, 2398, 434724.71875, 16593.839844, 197012.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1512, 5, 0, 481, 414569.34375, 21405.613281, 178284.328125, 2382, 414667.0625, 21420.998047, 178090.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1513, 5, 0, 481, 415103.71875, 21495.285156, 181244.421875, 2382, 415306.84375, 21529.630859, 181288.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1514, 5, 0, 481, 416440.25, 21891.320313, 176545.578125, 2382, 416219.25, 21807.455078, 176483.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1515, 5, 0, 481, 416999.4375, 21610.253906, 180107.8125, 2382, 416866.53125, 21666.173828, 179893.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1516, 5, 0, 481, 417259.28125, 21911.150391, 174157.125, 2382, 417191.25, 21816.748047, 173871.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1517, 5, 0, 481, 418379.84375, 21455.769531, 175947.765625, 2382, 418592.125, 21357.152344, 175956.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1518, 5, 0, 481, 419218.28125, 21719.322266, 178757.265625, 2382, 419308.21875, 21723.554688, 178630.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1519, 5, 0, 481, 419922.90625, 20875.984375, 180270.5, 2382, 420110.46875, 20814.384766, 180315.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1520, 5, 0, 481, 428443.65625, 21862.710938, 178065.46875, 2382, 428335.96875, 21857.625, 178061.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1521, 5, 0, 481, 426944.4375, 21591.476563, 179246.078125, 2382, 426832.8125, 21571.054688, 179210.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1522, 5, 0, 481, 429010.78125, 21842.652344, 179406.515625, 2382, 429146.1875, 21883.572266, 179291.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1523, 5, 0, 481, 426706.875, 21273.931641, 180814.125, 2382, 426572.15625, 21281.771484, 180786.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1524, 5, 0, 481, 425063.40625, 20912.378906, 181664.765625, 2382, 425102.59375, 20938.320313, 181558.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1525, 5, 0, 481, 423761.3125, 20688.509766, 183224.15625, 2382, 423682.53125, 20693.929688, 183386.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1526, 5, 0, 481, 429619.84375, 21770.162109, 182329.46875, 2382, 429741.6875, 21828.003906, 182199.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1527, 5, 0, 481, 428285.9375, 21409.960938, 182557.25, 2382, 428392.03125, 21455.707031, 182643.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1528, 5, 0, 481, 428050.0, 21506.851563, 183930.421875, 2382, 428049.375, 21521.664063, 184116.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1529, 5, 0, 481, 426182.65625, 21207.869141, 184573.90625, 2382, 426192.71875, 21254.339844, 184791.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1530, 5, 0, 481, 425080.375, 21069.421875, 184287.296875, 2382, 425003.0, 21059.425781, 184483.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1531, 5, 0, 481, 436848.40625, 21956.388672, 177773.75, 2382, 436736.46875, 22076.085938, 177963.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1532, 5, 0, 481, 435296.34375, 22552.255859, 177589.328125, 2382, 435208.0625, 22597.638672, 177740.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1533, 5, 0, 481, 431678.375, 22133.386719, 177709.4375, 2382, 431770.25, 22189.671875, 177874.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1534, 5, 0, 481, 430465.0, 21988.851563, 177789.453125, 2382, 430340.8125, 22021.800781, 178002.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1535, 5, 0, 481, 433888.375, 22734.492188, 178740.078125, 2382, 434054.21875, 22730.972656, 178667.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1536, 5, 0, 481, 431146.4375, 22261.853516, 179327.703125, 2382, 431074.03125, 22262.226563, 179545.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1537, 5, 0, 481, 434943.9375, 22601.455078, 179830.96875, 2382, 435103.875, 22592.068359, 179854.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1538, 5, 0, 481, 433177.34375, 22459.011719, 180503.71875, 2382, 433122.6875, 22415.207031, 180690.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1539, 5, 0, 481, 443768.78125, 20446.988281, 182312.265625, 2382, 443658.8125, 20711.580078, 182110.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1540, 5, 0, 481, 443114.59375, 21195.503906, 180735.734375, 2382, 442930.59375, 21285.630859, 180655.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1541, 5, 0, 481, 441626.75, 21211.228516, 178569.125, 2382, 441544.5625, 21312.925781, 178705.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1542, 5, 0, 481, 440154.71875, 21816.867188, 178476.015625, 2382, 439995.1875, 21883.78125, 178565.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1543, 5, 0, 481, 429793.3125, 21830.339844, 174201.203125, 2382, 429709.84375, 21857.535156, 174093.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1544, 5, 0, 481, 427890.21875, 21671.267578, 174863.15625, 2382, 427940.8125, 21678.65625, 174639.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1545, 5, 0, 481, 425506.21875, 21789.841797, 175697.203125, 2382, 425532.625, 21793.367188, 175562.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1546, 5, 0, 481, 429281.15625, 21592.652344, 175689.40625, 2382, 429456.8125, 21607.816406, 175648.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1547, 5, 0, 481, 426710.3125, 21771.123047, 176324.453125, 2382, 426650.75, 21782.556641, 176440.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1548, 5, 0, 481, 425556.375, 21782.541016, 177402.90625, 2382, 425609.53125, 21767.044922, 177495.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1549, 5, 0, 481, 423110.3125, 22264.283203, 172096.125, 2382, 423154.34375, 22245.359375, 171969.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1550, 5, 0, 481, 423557.0625, 21892.632813, 174587.828125, 2382, 423701.0, 21833.802734, 174769.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1551, 5, 0, 481, 424162.46875, 21784.216797, 168624.0625, 2382, 424260.125, 21863.423828, 168784.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1552, 5, 0, 481, 424386.59375, 22041.917969, 170995.109375, 2382, 424418.09375, 22015.890625, 170842.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1553, 5, 0, 481, 425023.40625, 21919.974609, 172705.453125, 2382, 425152.6875, 21910.320313, 172656.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1554, 5, 0, 481, 425781.09375, 21784.6875, 173927.703125, 2382, 425810.03125, 21792.503906, 173856.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1555, 5, 0, 481, 433164.625, 22036.564453, 173416.4375, 2382, 433026.46875, 22040.044922, 173457.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1556, 5, 0, 481, 433211.4375, 22156.015625, 171997.09375, 2382, 433307.5625, 22150.945313, 171828.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1557, 5, 0, 481, 428519.75, 22106.925781, 171298.5625, 2382, 428374.5625, 22087.0625, 171358.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1558, 5, 0, 481, 431725.25, 22259.738281, 170258.625, 2382, 431828.6875, 22255.109375, 170076.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1559, 5, 0, 481, 426939.59375, 21896.974609, 171048.0625, 2382, 426970.15625, 21926.664063, 170921.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1560, 5, 0, 481, 428270.375, 22005.361328, 170086.25, 2382, 428358.125, 22015.820313, 170096.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1561, 5, 0, 481, 430762.375, 22108.419922, 169135.703125, 2382, 430586.125, 22090.222656, 169076.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1562, 5, 0, 481, 432370.6875, 22128.744141, 168739.90625, 2382, 432311.8125, 22065.556641, 168588.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1563, 5, 0, 481, 445031.375, 20524.212891, 169751.03125, 2382, 445054.46875, 20486.263672, 169894.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1564, 5, 0, 481, 444606.96875, 20963.419922, 167969.90625, 2382, 444456.59375, 21068.623047, 167918.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1565, 5, 0, 481, 444396.3125, 21100.671875, 166305.734375, 2382, 444297.4375, 21154.925781, 166457.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1566, 5, 0, 481, 443486.9375, 21001.818359, 171228.484375, 2382, 443488.21875, 21146.96875, 171079.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1567, 5, 0, 481, 443416.90625, 21498.376953, 169867.71875, 2382, 443429.125, 21498.220703, 169680.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1568, 5, 0, 481, 443141.8125, 21537.585938, 167436.1875, 2382, 443056.0, 21640.480469, 167600.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1569, 5, 0, 481, 441854.84375, 21379.328125, 172236.59375, 2382, 441681.90625, 21491.814453, 172301.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1570, 5, 0, 481, 440664.53125, 22342.064453, 171060.671875, 2382, 440497.46875, 22352.744141, 171068.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1571, 5, 0, 481, 440355.90625, 22433.410156, 169837.109375, 2382, 440189.84375, 22452.789063, 169876.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1572, 5, 0, 481, 440200.75, 21580.847656, 173042.78125, 2382, 440169.0625, 21646.552734, 172922.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1573, 5, 0, 481, 439078.40625, 22592.806641, 170665.90625, 2382, 439055.78125, 22601.269531, 170790.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1574, 5, 0, 481, 437641.0, 21961.6875, 168336.421875, 2382, 437694.15625, 22001.691406, 168416.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1575, 5, 0, 481, 438602.125, 22091.9375, 172424.421875, 2382, 438657.96875, 22186.763672, 172305.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1576, 5, 0, 481, 437321.46875, 22116.445313, 169388.265625, 2382, 437414.09375, 22176.695313, 169415.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1577, 5, 0, 481, 432408.8125, 22007.205078, 163609.359375, 2382, 432669.21875, 22023.822266, 163476.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1578, 5, 0, 481, 431951.0625, 21614.578125, 162406.84375, 2382, 432059.84375, 21713.859375, 162546.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1579, 5, 0, 481, 433314.375, 20982.09375, 161688.875, 2382, 433333.90625, 21093.148438, 161926.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1580, 5, 0, 481, 429616.0, 20928.474609, 162553.921875, 2382, 429498.21875, 20770.666016, 162424.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1581, 5, 0, 481, 431911.25, 20796.3125, 161374.578125, 2382, 431735.71875, 20738.466797, 161246.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1582, 5, 0, 481, 433849.25, 20651.257813, 160518.78125, 2382, 433978.65625, 20624.507813, 160330.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1583, 5, 0, 481, 444290.78125, 20878.724609, 162873.203125, 2382, 444383.90625, 20661.919922, 162716.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1584, 5, 0, 481, 442274.46875, 21684.599609, 163996.8125, 2382, 442137.5625, 21712.429688, 164176.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1585, 5, 0, 481, 441395.53125, 21866.5, 165051.15625, 2382, 441215.3125, 21930.144531, 165023.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1586, 5, 0, 481, 439615.375, 22209.214844, 164532.828125, 2382, 439763.625, 22183.933594, 164536.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1587, 5, 0, 481, 438104.15625, 21868.726563, 165524.125, 2382, 438196.96875, 21884.753906, 165698.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1588, 5, 0, 481, 437001.4375, 21817.554688, 164526.984375, 2382, 436984.71875, 21864.201172, 164708.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1589, 5, 0, 481, 440804.71875, 21292.332031, 160999.28125, 2382, 440969.71875, 21220.107422, 160872.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1590, 5, 0, 481, 437895.65625, 21268.296875, 160798.40625, 2382, 437945.84375, 21306.578125, 160912.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1591, 5, 0, 481, 436172.8125, 21042.796875, 161009.140625, 2382, 435971.875, 21015.865234, 161117.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1592, 5, 0, 481, 439819.6875, 22149.542969, 162564.171875, 2382, 439694.96875, 22141.746094, 162540.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1593, 5, 0, 481, 448344.84375, 20748.597656, 160003.734375, 2382, 448525.5, 20778.019531, 160164.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1594, 5, 0, 481, 447527.03125, 20600.078125, 160256.484375, 2382, 447376.78125, 20664.546875, 160130.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1595, 5, 0, 481, 448888.25, 20910.191406, 163307.03125, 2382, 448973.90625, 20955.560547, 163192.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1596, 5, 0, 481, 447292.75, 19956.894531, 161091.90625, 2382, 447103.03125, 19964.796875, 160995.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1597, 5, 0, 481, 445021.25, 20699.894531, 158550.203125, 2382, 444928.8125, 20700.501953, 158676.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1598, 5, 0, 115, 417486.8125, 20152.714844, 159396.359375, 2393, 417383.21875, 20132.703125, 159246.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1599, 5, 0, 115, 415437.875, 19932.085938, 157704.265625, 2393, 415663.8125, 19926.972656, 157751.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1600, 5, 0, 115, 414001.78125, 19711.289063, 155889.65625, 2393, 414028.0625, 19735.287109, 156055.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1601, 5, 0, 115, 419055.25, 21791.753906, 163436.375, 2393, 418943.90625, 21657.121094, 163631.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1602, 5, 0, 115, 416357.25, 20438.136719, 160971.28125, 2393, 416292.875, 20389.441406, 160760.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1603, 5, 0, 115, 415284.25, 20358.023438, 160431.703125, 2393, 415271.4375, 20338.28125, 160169.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1604, 5, 0, 115, 413697.75, 20016.554688, 157618.171875, 2393, 413610.34375, 19962.611328, 157326.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1605, 5, 0, 115, 415075.125, 20530.050781, 162099.203125, 2393, 415156.84375, 20582.753906, 162353.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1606, 5, 0, 115, 412715.21875, 20586.074219, 158813.65625, 2393, 412426.84375, 20687.742188, 158843.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1607, 5, 0, 115, 413494.0, 20798.3125, 163102.890625, 2393, 413340.125, 20849.166016, 163233.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1608, 5, 0, 115, 411290.71875, 20985.652344, 160633.21875, 2393, 411422.03125, 20971.054688, 160866.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1609, 5, 0, 115, 408858.03125, 21131.378906, 159367.8125, 2393, 408642.5625, 21122.882813, 159262.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1610, 5, 0, 115, 408036.0, 20437.041016, 156798.84375, 2393, 407836.5, 20397.183594, 156643.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1611, 5, 0, 115, 411543.0625, 21663.550781, 164965.09375, 2393, 411386.84375, 21789.253906, 165118.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1612, 5, 0, 115, 411564.40625, 21372.541016, 163324.046875, 2393, 411702.28125, 21351.519531, 163508.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1613, 5, 0, 115, 407579.28125, 21681.980469, 161614.75, 2393, 407370.5, 21671.695313, 161520.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1614, 5, 0, 115, 407572.5625, 21410.636719, 160333.96875, 2393, 407358.34375, 21404.015625, 160245.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1615, 5, 0, 115, 406268.5625, 21071.128906, 158627.359375, 2393, 406340.1875, 21060.050781, 158786.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1616, 5, 0, 115, 405483.0, 20759.474609, 157112.3125, 2393, 405458.6875, 20799.666016, 157237.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1617, 5, 0, 115, 410044.40625, 22097.804688, 166115.328125, 2393, 409909.0, 22088.679688, 166046.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1618, 5, 0, 115, 406633.59375, 21776.691406, 162355.921875, 2393, 406491.75, 21777.601563, 162454.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1619, 5, 0, 115, 401698.15625, 21127.845703, 158121.3125, 2393, 401665.40625, 21078.458984, 158251.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1620, 5, 0, 115, 405466.9375, 21829.382813, 163404.78125, 2393, 405289.84375, 21826.78125, 163453.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1621, 5, 0, 115, 403672.53125, 21620.433594, 162211.390625, 2393, 403827.875, 21610.394531, 162357.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1622, 5, 0, 115, 400723.5, 21235.488281, 159821.40625, 2393, 400796.875, 21269.4375, 160009.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1623, 5, 0, 115, 398643.96875, 20975.150391, 158272.28125, 2393, 398448.3125, 20949.464844, 157989.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1624, 5, 0, 115, 401617.3125, 21657.039063, 163185.8125, 2393, 401560.40625, 21690.347656, 163395.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1625, 5, 0, 115, 400512.59375, 21374.339844, 161746.53125, 2393, 400774.0, 21397.78125, 161717.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1626, 5, 0, 115, 397736.03125, 20970.195313, 159754.90625, 2393, 397805.96875, 20990.3125, 159917.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1627, 5, 0, 115, 396071.4375, 20807.304688, 158395.890625, 2393, 396141.03125, 20818.683594, 158601.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1628, 5, 0, 115, 398505.59375, 21322.9375, 161749.0625, 2393, 398577.90625, 21278.884766, 161449.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1629, 5, 0, 115, 395552.78125, 20906.900391, 160358.328125, 2393, 395652.125, 20941.857422, 160520.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1630, 5, 0, 115, 393497.25, 20454.222656, 159117.0, 2393, 393243.53125, 20410.283203, 159077.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1631, 5, 0, 115, 399321.65625, 22077.3125, 166128.03125, 2393, 399178.75, 22141.818359, 166319.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1632, 5, 0, 115, 397969.75, 21731.419922, 163854.34375, 2393, 397916.15625, 21767.667969, 164042.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1633, 5, 0, 115, 395620.40625, 21170.625, 162221.875, 2393, 395422.34375, 21128.203125, 162223.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1634, 5, 0, 115, 393474.96875, 20767.701172, 160905.875, 2393, 393539.1875, 20797.404297, 161067.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1635, 5, 0, 115, 396422.53125, 21664.253906, 165090.59375, 2393, 396397.75, 21705.519531, 165361.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1636, 5, 0, 115, 393010.4375, 21244.101563, 163928.640625, 2393, 393128.65625, 21263.675781, 163953.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1637, 5, 0, 115, 388990.1875, 20366.363281, 161217.953125, 2393, 388856.4375, 20342.072266, 161228.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1638, 5, 0, 115, 385555.5, 20039.597656, 160794.34375, 2393, 385631.15625, 20043.705078, 160898.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1639, 5, 0, 115, 388683.90625, 20915.857422, 163844.859375, 2393, 388746.34375, 20930.072266, 164010.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1640, 5, 0, 115, 382098.21875, 20598.400391, 164269.296875, 2393, 382134.125, 20612.964844, 164399.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1641, 5, 0, 115, 386150.375, 21021.492188, 166146.234375, 2393, 385942.9375, 21050.214844, 166212.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1642, 5, 0, 115, 383013.34375, 20787.923828, 166193.734375, 2393, 383143.3125, 20942.091797, 166334.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1643, 5, 0, 115, 380828.28125, 20599.529297, 166384.09375, 2393, 380940.59375, 20670.742188, 166288.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1644, 5, 0, 115, 392208.8125, 21605.617188, 170009.046875, 2393, 392193.875, 21672.949219, 170229.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1645, 5, 0, 115, 390615.0, 21599.224609, 171240.625, 2393, 390759.625, 21640.763672, 171422.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1646, 5, 0, 115, 389873.75, 21343.054688, 169456.015625, 2393, 390079.3125, 21350.005859, 169483.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1647, 5, 0, 115, 387747.0, 21211.289063, 167541.75, 2393, 387945.4375, 21229.197266, 167380.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1648, 5, 0, 115, 385322.28125, 21031.146484, 167840.71875, 2393, 385407.34375, 21008.394531, 167635.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1649, 5, 0, 115, 386810.96875, 21336.708984, 172638.265625, 2393, 386757.0625, 21297.0625, 172823.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1650, 5, 0, 115, 385192.15625, 21146.007813, 171210.65625, 2393, 385011.5625, 21218.875, 171231.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1651, 5, 0, 115, 383593.46875, 21000.619141, 170059.03125, 2393, 383378.09375, 21058.328125, 170115.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1652, 5, 0, 115, 381485.84375, 20731.158203, 169098.625, 2393, 381641.1875, 20764.246094, 169191.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1653, 5, 0, 115, 384821.75, 21144.587891, 173696.25, 2393, 384636.09375, 21092.552734, 173864.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1654, 5, 0, 115, 381916.8125, 20920.378906, 171076.75, 2393, 381647.5, 20900.675781, 171113.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1655, 5, 0, 657, 444976.0625, 16997.943359, 213471.109375, 2398, 445168.53125, 17016.628906, 213469.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1656, 5, 0, 657, 447080.5, 16418.949219, 212008.078125, 2398, 447034.5, 16502.96875, 212270.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1657, 5, 0, 657, 442515.15625, 16035.515625, 211847.65625, 2398, 442555.375, 16022.492188, 211651.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1658, 5, 0, 657, 444193.15625, 16082.791016, 212215.03125, 2398, 444349.0625, 16042.000977, 212137.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1659, 5, 0, 657, 445293.625, 16090.724609, 211999.46875, 2398, 445520.6875, 16140.382813, 212113.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1660, 5, 0, 657, 446139.75, 15848.576172, 211027.21875, 2398, 446314.1875, 15794.583984, 210829.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1661, 5, 0, 657, 447594.84375, 16020.058594, 209988.15625, 2398, 447542.21875, 16032.65332, 210200.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1662, 5, 0, 657, 449492.15625, 16695.460938, 208364.046875, 2398, 449375.96875, 16603.480469, 208214.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1663, 5, 0, 657, 446898.125, 15504.582031, 208229.9375, 2398, 446740.875, 15451.080078, 208433.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1664, 5, 0, 657, 448111.96875, 16138.230469, 206760.828125, 2398, 447921.8125, 16062.322266, 206838.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1665, 5, 0, 657, 449216.4375, 17150.908203, 203427.0625, 2398, 449204.28125, 17070.886719, 203211.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1666, 5, 0, 657, 449068.3125, 16736.714844, 201460.1875, 2398, 449222.53125, 16782.486328, 201566.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1667, 5, 0, 657, 446464.75, 15895.113281, 206023.859375, 2398, 446474.40625, 15924.324219, 205880.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1668, 5, 0, 657, 447376.625, 16362.800781, 204284.6875, 2398, 447235.65625, 16310.091797, 204231.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1669, 5, 0, 657, 447150.78125, 16205.103516, 202093.078125, 2398, 446995.78125, 16184.539063, 202194.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1670, 5, 0, 657, 447824.71875, 16090.365234, 200743.8125, 2398, 447652.0625, 16039.269531, 200758.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1671, 5, 0, 657, 445853.625, 16039.541016, 203448.484375, 2398, 446084.125, 16058.850586, 203610.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1672, 5, 0, 657, 446108.65625, 16023.814453, 199280.734375, 2398, 445954.6875, 16041.980469, 199171.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1673, 5, 0, 657, 446384.53125, 16249.539063, 197896.890625, 2398, 446216.84375, 16275.857422, 197811.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1674, 5, 0, 657, 444125.8125, 16063.013672, 202271.4375, 2398, 444054.53125, 16056.053711, 202596.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1675, 5, 0, 657, 444080.90625, 15942.322266, 199889.203125, 2398, 443911.4375, 15963.203125, 199768.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1676, 5, 0, 657, 444253.9375, 16149.291016, 197021.46875, 2398, 444207.46875, 16106.101563, 197186.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1677, 5, 0, 657, 443153.21875, 15790.962891, 197480.4375, 2398, 443110.75, 15888.880859, 197262.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1678, 5, 0, 657, 443105.25, 16316.828125, 195555.03125, 2398, 443058.9375, 16294.740234, 195694.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1679, 5, 0, 657, 443654.28125, 15500.015625, 206601.09375, 2398, 443553.25, 15574.939453, 206423.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1680, 5, 0, 657, 442778.125, 16041.851563, 204833.09375, 2398, 442715.03125, 16041.232422, 204969.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1681, 5, 0, 657, 441990.03125, 16276.638672, 203051.703125, 2398, 441852.53125, 16303.001953, 203070.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1682, 5, 0, 657, 441332.3125, 16408.439453, 201599.578125, 2398, 441219.1875, 16424.595703, 201641.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1683, 5, 0, 657, 442321.78125, 15936.994141, 207111.71875, 2398, 442418.125, 15901.898438, 207245.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1684, 5, 0, 657, 441592.8125, 16315.166016, 204445.53125, 2398, 441565.0625, 16335.835938, 204273.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1685, 5, 0, 657, 440203.4375, 16607.896484, 202080.75, 2398, 440234.96875, 16608.796875, 202244.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1686, 5, 0, 657, 442106.6875, 16141.0, 209394.015625, 2398, 441967.5, 16148.254883, 209301.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1687, 5, 0, 657, 441267.9375, 16069.212891, 208343.375, 2398, 441113.65625, 16080.25293, 208541.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1688, 5, 0, 657, 438259.625, 16327.988281, 194066.265625, 2398, 438101.21875, 16371.025391, 193984.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1689, 5, 0, 657, 441527.625, 16205.162109, 195120.59375, 2398, 441678.78125, 16172.904297, 195289.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1690, 5, 0, 657, 439328.5, 16011.671875, 195344.296875, 2398, 439480.34375, 16021.119141, 195536.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1691, 5, 0, 657, 437355.5, 16235.515625, 195879.484375, 2398, 437183.125, 16264.732422, 196020.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1692, 5, 0, 657, 434553.8125, 16240.363281, 195159.984375, 2398, 434396.46875, 16216.195313, 195001.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1693, 5, 0, 657, 431403.21875, 16045.439453, 195640.625, 2398, 431165.78125, 16043.529297, 195637.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1694, 5, 0, 657, 441049.0625, 16165.646484, 196641.53125, 2398, 440990.3125, 16175.787109, 196797.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1695, 5, 0, 657, 433136.59375, 16330.679688, 196755.6875, 2398, 433259.1875, 16377.574219, 196921.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1696, 5, 0, 657, 430916.5, 16149.892578, 196685.6875, 2398, 431058.125, 16153.697266, 196853.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1697, 5, 0, 657, 435679.0625, 17334.949219, 200836.4375, 2398, 435852.75, 17309.523438, 200782.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1698, 5, 0, 657, 438898.5625, 16691.191406, 201097.796875, 2398, 439151.75, 16651.771484, 201126.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1699, 5, 0, 657, 430798.75, 16568.306641, 198988.359375, 2398, 430932.53125, 16602.40625, 198908.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1700, 5, 0, 657, 433406.875, 17010.580078, 199027.78125, 2398, 433301.75, 16885.433594, 198882.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1701, 5, 0, 657, 435043.34375, 17117.189453, 198278.53125, 2398, 435240.25, 17277.158203, 198419.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1702, 5, 0, 657, 436382.0625, 17224.818359, 199397.9375, 2398, 436538.625, 17225.847656, 199282.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1703, 5, 0, 657, 437834.625, 17097.591797, 199245.875, 2398, 437760.25, 17101.871094, 199080.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1704, 5, 0, 657, 439650.78125, 16035.890625, 199510.671875, 2398, 439916.84375, 16217.095703, 199618.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1705, 6, 2, 84, 0.0, 0.0, 0.0, 2388, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1706, 6, 1, 0, 0.0, 0.0, 0.0, 2495, 305408.15625, 11565.451172, 57378.011719);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1707, 6, 1, 0, 0.0, 0.0, 0.0, 2495, 349759.8125, 17347.390625, 171072.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1708, 6, 0, 697, 357643.28125, 18098.773438, 178113.96875, 2400, 357875.6875, 18037.339844, 177961.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1709, 6, 0, 697, 354089.28125, 18178.119141, 178625.921875, 2400, 353847.15625, 18196.115234, 178954.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1710, 6, 0, 697, 355467.625, 17994.560547, 176331.453125, 2400, 355593.59375, 18032.46875, 176100.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1711, 6, 0, 697, 357336.90625, 17966.238281, 173718.484375, 2400, 357335.75, 17970.332031, 173970.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1712, 6, 0, 697, 359347.125, 17903.707031, 172169.09375, 2400, 359088.625, 17911.941406, 172433.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1713, 6, 0, 697, 362429.25, 17568.740234, 169372.484375, 2400, 362699.3125, 17613.435547, 169463.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1714, 6, 0, 697, 364735.40625, 17756.070313, 166978.828125, 2400, 364678.46875, 17787.054688, 167204.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1715, 6, 0, 697, 347514.53125, 17837.574219, 177642.15625, 2400, 347303.25, 17848.628906, 177450.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1716, 6, 0, 697, 350679.4375, 17920.669922, 174542.25, 2400, 350821.4375, 17920.273438, 174333.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1717, 6, 0, 697, 344822.96875, 17363.658203, 172961.25, 2400, 344693.625, 17364.78125, 173116.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1718, 6, 0, 697, 347482.09375, 17327.347656, 171782.828125, 2400, 347853.75, 17344.173828, 171981.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1719, 6, 0, 697, 351169.21875, 17486.84375, 171266.046875, 2400, 351061.46875, 17489.304688, 171685.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1720, 6, 0, 697, 357621.4375, 17659.546875, 169069.59375, 2400, 357805.6875, 17615.011719, 168838.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1721, 6, 0, 697, 360246.6875, 17533.662109, 167765.3125, 2400, 360000.375, 17536.783203, 167843.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1722, 6, 0, 697, 359983.53125, 17253.0625, 163375.125, 2400, 360192.3125, 17321.183594, 163643.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1723, 6, 0, 697, 354330.78125, 17748.386719, 169005.9375, 2400, 354493.6875, 17757.515625, 168924.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1724, 6, 0, 697, 355566.40625, 16798.619141, 163998.125, 2400, 355460.84375, 16834.855469, 164273.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1725, 6, 0, 697, 356156.09375, 16466.078125, 160660.53125, 2400, 356122.46875, 16484.011719, 160868.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1726, 6, 0, 697, 332582.34375, 16499.728516, 169318.09375, 2400, 332935.28125, 16517.089844, 169391.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1727, 6, 0, 697, 336257.25, 16299.935547, 167673.578125, 2400, 336632.1875, 16356.604492, 167891.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1728, 6, 0, 697, 339005.9375, 16448.902344, 168625.140625, 2400, 338738.40625, 16455.023438, 168635.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1729, 6, 0, 697, 339133.71875, 16770.210938, 170725.734375, 2400, 338853.09375, 16747.707031, 170643.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1730, 6, 0, 697, 322610.21875, 16227.085938, 166983.328125, 2400, 322231.71875, 16215.1875, 167035.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1731, 6, 0, 697, 324585.4375, 16306.589844, 164095.453125, 2400, 324406.53125, 16267.69043, 163958.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1732, 6, 0, 697, 325611.25, 15549.835938, 161893.109375, 2400, 325590.4375, 15558.219727, 161708.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1733, 6, 0, 697, 326134.5625, 15192.576172, 159833.265625, 2400, 326133.03125, 15115.064453, 159463.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1734, 6, 0, 697, 328585.15625, 14245.828125, 159651.171875, 2400, 328763.8125, 14176.318359, 159845.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1735, 6, 0, 697, 327682.1875, 14752.306641, 162741.078125, 2400, 327877.34375, 14621.68457, 162629.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1736, 6, 0, 697, 330086.65625, 13309.026367, 162120.21875, 2400, 329772.625, 13480.813477, 161892.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1737, 6, 0, 697, 331815.46875, 12793.492188, 160020.09375, 2400, 331754.9375, 12875.18457, 160355.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1738, 6, 0, 697, 334590.46875, 11661.134766, 158058.203125, 2400, 334249.5, 11809.689453, 158320.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1739, 6, 0, 697, 329863.9375, 11430.418945, 154817.5625, 2400, 329462.21875, 11477.347656, 154911.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1740, 6, 0, 697, 333031.8125, 11387.916016, 155421.3125, 2400, 332718.6875, 11380.563477, 155504.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1741, 6, 0, 697, 336098.03125, 11441.107422, 155335.453125, 2400, 335833.90625, 11411.760742, 155375.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1742, 6, 0, 697, 337886.8125, 11515.113281, 153650.734375, 2400, 338153.8125, 11520.760742, 153820.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1743, 6, 0, 697, 339864.90625, 11386.866211, 152112.640625, 2400, 339584.84375, 11356.441406, 152140.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1744, 6, 0, 697, 338276.3125, 11544.878906, 155460.71875, 2400, 337811.65625, 11538.148438, 155606.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1745, 6, 0, 697, 338639.75, 11725.611328, 157814.0625, 2400, 338809.46875, 11735.467773, 158000.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1746, 6, 0, 697, 339819.21875, 11783.1875, 160516.703125, 2400, 339814.0, 11775.494141, 160236.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1747, 6, 0, 697, 344416.375, 12766.150391, 162190.6875, 2400, 344200.90625, 12732.639648, 162021.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1748, 6, 0, 697, 348244.0, 14061.675781, 163336.296875, 2400, 348530.65625, 14205.458984, 163062.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1749, 6, 0, 92, 375995.5, 20387.226563, 163601.703125, 2399, 375913.9375, 20402.021484, 163448.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1750, 6, 0, 92, 375204.875, 20308.267578, 161987.09375, 2399, 375475.53125, 20339.208984, 162020.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1751, 6, 0, 92, 374942.46875, 20329.148438, 162327.59375, 2399, 374850.84375, 20356.138672, 162508.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1752, 6, 0, 92, 374920.71875, 20282.386719, 161818.4375, 2399, 374774.65625, 20255.904297, 161658.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1753, 6, 0, 92, 376735.40625, 20272.232422, 168297.875, 2399, 376886.75, 20290.650391, 168373.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1754, 6, 0, 92, 376566.6875, 20208.117188, 167967.40625, 2399, 376603.5, 20244.585938, 167760.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1755, 6, 0, 92, 376285.9375, 20227.035156, 168295.265625, 2399, 376242.5, 20225.847656, 168417.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1756, 6, 0, 92, 376804.1875, 20010.304688, 171959.84375, 2399, 377037.53125, 20016.613281, 171933.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1757, 6, 0, 92, 375232.25, 20153.1875, 169073.5625, 2399, 375371.6875, 20184.025391, 169244.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1758, 6, 0, 92, 375039.78125, 20062.230469, 167739.234375, 2399, 375167.28125, 20084.931641, 167818.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1759, 6, 0, 92, 374784.15625, 19991.628906, 167419.75, 2399, 374786.875, 20011.511719, 167156.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1760, 6, 0, 92, 374741.71875, 19986.882813, 167532.34375, 2399, 374552.875, 19953.59375, 167668.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1761, 6, 0, 92, 372111.34375, 19349.484375, 166867.703125, 2399, 372176.4375, 19371.570313, 167128.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1762, 6, 0, 92, 372130.34375, 19349.302734, 166495.109375, 2399, 372337.625, 19412.095703, 166409.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1763, 6, 0, 92, 371874.28125, 19292.701172, 166412.578125, 2399, 371759.59375, 19295.939453, 166302.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1764, 6, 0, 92, 370172.375, 18949.746094, 167477.578125, 2399, 370004.625, 18914.560547, 167507.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1765, 6, 0, 92, 370078.125, 19072.919922, 165838.53125, 2399, 369900.53125, 18976.712891, 165908.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1766, 6, 0, 92, 371124.125, 20486.289063, 163881.53125, 2399, 370946.03125, 20490.320313, 163875.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1767, 6, 0, 92, 371376.15625, 20283.90625, 162250.9375, 2399, 371269.78125, 20303.908203, 162347.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1768, 6, 0, 92, 371648.3125, 20207.796875, 161918.40625, 2399, 371797.65625, 20162.943359, 161777.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1769, 6, 0, 92, 371238.90625, 20192.113281, 161741.015625, 2399, 371112.375, 20128.859375, 161560.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1770, 6, 0, 92, 365738.96875, 18034.419922, 167012.609375, 2399, 365805.28125, 18056.0625, 167187.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1771, 6, 0, 92, 364337.25, 17574.728516, 165645.625, 2399, 364165.25, 17547.527344, 165611.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1772, 6, 0, 92, 362346.96875, 17310.449219, 163020.578125, 2399, 362261.5, 17324.333984, 163107.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1773, 6, 0, 92, 363634.9375, 17702.646484, 167820.59375, 2399, 363690.40625, 17718.744141, 168030.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1774, 6, 0, 92, 350294.5625, 17994.28125, 179476.21875, 2399, 350384.875, 17996.195313, 179368.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1775, 6, 0, 92, 352373.75, 18218.339844, 179108.140625, 2399, 352318.53125, 18215.089844, 179004.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1776, 6, 0, 92, 355231.625, 18121.251953, 177873.234375, 2399, 355150.28125, 18099.675781, 177797.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1777, 6, 0, 92, 356404.84375, 18032.197266, 176459.765625, 2399, 356279.34375, 18033.707031, 176536.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1778, 6, 0, 92, 356668.875, 18022.675781, 176460.890625, 2399, 356950.6875, 18021.492188, 176425.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1779, 6, 0, 92, 350907.53125, 18125.03125, 177632.75, 2399, 350724.8125, 18118.675781, 177646.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1780, 6, 0, 92, 351220.15625, 18133.8125, 177553.625, 2399, 351325.78125, 18159.113281, 177426.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1781, 6, 0, 92, 354581.0625, 17953.015625, 175764.90625, 2399, 354532.125, 17955.210938, 175881.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1782, 6, 0, 92, 354868.3125, 18069.925781, 175516.234375, 2399, 355058.21875, 17943.183594, 175460.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1783, 6, 0, 92, 354584.71875, 17963.173828, 175274.3125, 2399, 354494.25, 17969.671875, 175153.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1784, 6, 0, 92, 354467.59375, 18102.121094, 171639.15625, 2399, 354220.78125, 18111.443359, 171494.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1785, 6, 0, 92, 354955.6875, 18050.617188, 171083.078125, 2399, 354887.34375, 18021.111328, 170933.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1786, 6, 0, 92, 357197.34375, 17786.695313, 169905.640625, 2399, 357035.21875, 17809.289063, 169954.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1787, 6, 0, 92, 356759.0, 17685.978516, 169083.0, 2399, 356600.625, 17665.671875, 168921.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1788, 6, 0, 92, 357640.90625, 17322.494141, 166529.3125, 2399, 357429.34375, 17318.509766, 166531.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1789, 6, 0, 92, 357074.625, 17061.324219, 164745.734375, 2399, 357146.78125, 17044.478516, 164466.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1790, 6, 0, 92, 356705.1875, 17078.021484, 165114.359375, 2399, 356490.0, 17051.306641, 165064.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1791, 6, 0, 92, 354348.40625, 16365.78125, 162718.6875, 2399, 354343.46875, 16382.858398, 162855.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1792, 6, 0, 92, 352963.21875, 16044.583984, 162424.578125, 2399, 352732.71875, 15978.788086, 162433.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1793, 6, 0, 92, 348094.34375, 17854.9375, 176439.15625, 2399, 348213.75, 17905.019531, 176333.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1794, 6, 0, 92, 350115.9375, 17973.179688, 175852.171875, 2399, 350136.5, 18029.623047, 176076.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1795, 6, 0, 92, 351466.78125, 17973.748047, 174849.25, 2399, 351568.6875, 18016.71875, 175023.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1796, 6, 0, 92, 349789.21875, 17910.964844, 175392.171875, 2399, 349834.6875, 17931.191406, 175224.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1797, 6, 0, 92, 347817.09375, 17716.189453, 174775.5625, 2399, 347955.375, 17706.242188, 174673.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1798, 6, 0, 92, 349916.46875, 17831.931641, 173924.46875, 2399, 349766.3125, 17856.007813, 173864.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1799, 6, 0, 92, 349662.6875, 17398.609375, 171665.265625, 2399, 349449.125, 17367.207031, 171820.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1800, 6, 0, 92, 349956.65625, 17457.144531, 171643.546875, 2399, 350132.65625, 17409.589844, 171757.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1801, 6, 0, 92, 349730.65625, 17367.5, 171255.09375, 2399, 349661.375, 17339.25, 171154.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1802, 6, 0, 92, 348756.5625, 17236.027344, 170714.265625, 2399, 348610.4375, 17264.960938, 170880.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1803, 6, 0, 92, 350713.375, 17428.804688, 170613.109375, 2399, 350850.96875, 17452.910156, 170686.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1804, 6, 0, 92, 348453.9375, 17127.789063, 170131.078125, 2399, 348307.5625, 17147.107422, 170042.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1805, 6, 0, 92, 348922.6875, 17250.486328, 170458.265625, 2399, 349038.375, 17249.386719, 170319.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1806, 6, 0, 92, 350411.125, 17402.359375, 170375.59375, 2399, 350283.34375, 17384.21875, 170344.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1807, 6, 0, 92, 350750.40625, 17427.720703, 170317.5, 2399, 350837.9375, 17439.017578, 170131.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1808, 6, 0, 92, 346045.03125, 17627.033203, 174774.96875, 2399, 346318.84375, 17643.566406, 174816.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1809, 6, 0, 92, 345690.0, 17545.09375, 174413.46875, 2399, 345679.8125, 17522.212891, 174218.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1810, 6, 0, 92, 345938.8125, 17530.871094, 174225.75, 2399, 346210.15625, 17540.445313, 174142.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1811, 6, 0, 92, 345780.9375, 17414.082031, 172979.59375, 2399, 345844.3125, 17402.556641, 172799.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1812, 6, 0, 92, 345399.09375, 17300.494141, 171393.5625, 2399, 345270.375, 17311.777344, 171359.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1813, 6, 0, 92, 344363.09375, 17153.464844, 169980.375, 2399, 344457.53125, 17201.326172, 170110.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1814, 6, 0, 92, 342028.25, 17331.947266, 173521.484375, 2399, 342053.5625, 17379.738281, 173745.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1815, 6, 0, 92, 324073.28125, 16367.048828, 165223.828125, 2399, 324222.59375, 16387.464844, 165215.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1816, 6, 0, 92, 324222.9375, 16377.142578, 166424.90625, 2399, 324162.5, 16378.419922, 166245.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1817, 6, 0, 92, 324138.78125, 16363.513672, 166765.859375, 2399, 323936.53125, 16355.764648, 166899.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1818, 6, 0, 92, 324324.53125, 16378.970703, 166630.515625, 2399, 324473.71875, 16392.304688, 166676.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1819, 6, 0, 92, 326549.0, 16476.292969, 166770.609375, 2399, 326430.28125, 16475.179688, 166638.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1820, 6, 0, 92, 329691.71875, 16459.130859, 167828.578125, 2399, 329540.75, 16444.828125, 167706.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1821, 6, 0, 92, 330775.65625, 16376.283203, 169079.625, 2399, 330872.8125, 16446.769531, 168952.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1822, 6, 0, 92, 333340.90625, 16435.511719, 168230.328125, 2399, 333164.4375, 16439.28125, 168196.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1823, 6, 0, 92, 336013.59375, 16781.779297, 170679.484375, 2399, 335894.46875, 16741.285156, 170690.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1824, 6, 0, 92, 337531.4375, 16619.982422, 170141.453125, 2399, 337613.65625, 16641.945313, 169991.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1825, 6, 0, 92, 326909.875, 15066.064453, 161939.3125, 2399, 327066.3125, 14990.861328, 161917.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1826, 6, 0, 92, 330139.5625, 13471.875977, 160783.703125, 2399, 329955.65625, 13545.407227, 160722.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1827, 6, 0, 92, 332743.46875, 12492.31543, 160353.484375, 2399, 332940.375, 12437.189453, 160265.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1828, 6, 0, 92, 336169.59375, 11860.667969, 159894.59375, 2399, 336070.75, 11877.670898, 159772.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1829, 6, 0, 92, 325423.5625, 15608.442383, 160566.171875, 2399, 325213.75, 15678.978516, 160682.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1830, 6, 0, 92, 327811.0, 14518.234375, 159990.484375, 2399, 327625.625, 14603.0625, 160106.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1831, 6, 0, 92, 331210.4375, 12961.81543, 159530.28125, 2399, 331030.4375, 13010.167969, 159485.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1832, 6, 0, 92, 334742.1875, 11956.833008, 159134.875, 2399, 334963.5625, 11943.985352, 159078.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1833, 6, 0, 92, 336391.4375, 11813.667969, 158822.546875, 2399, 336270.0625, 11798.599609, 158659.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1834, 6, 0, 92, 333146.5, 11871.1875, 157926.765625, 2399, 332852.8125, 11951.277344, 158077.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1835, 6, 0, 92, 338754.6875, 11688.125, 157069.59375, 2399, 338594.59375, 11670.138672, 156973.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1836, 6, 0, 92, 339103.46875, 11720.337891, 157118.921875, 2399, 339350.28125, 11730.764648, 156993.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1837, 6, 0, 92, 335990.9375, 11485.44043, 156381.125, 2399, 335748.1875, 11474.867188, 156490.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1838, 6, 0, 92, 330371.78125, 11394.768555, 155172.890625, 2399, 330461.5, 11374.161133, 155299.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1839, 6, 0, 92, 334222.34375, 11381.368164, 155483.9375, 2399, 334107.25, 11374.764648, 155389.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1840, 6, 0, 92, 336738.5, 11488.493164, 154679.828125, 2399, 336906.5, 11450.84082, 154755.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1841, 6, 0, 92, 339720.71875, 11403.527344, 152925.515625, 2399, 339692.4375, 11419.665039, 153075.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1842, 6, 0, 92, 340020.59375, 11332.851563, 151517.0625, 2399, 340174.90625, 11332.791016, 151283.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1843, 6, 0, 92, 348073.53125, 14916.325195, 165831.75, 2399, 347889.0625, 14952.673828, 166011.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1844, 6, 0, 92, 349488.03125, 14932.072266, 164845.921875, 2399, 349655.8125, 14949.334961, 164752.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1845, 6, 0, 92, 341507.1875, 11979.163086, 161033.59375, 2399, 341256.1875, 11976.898438, 161173.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1846, 6, 0, 92, 341769.21875, 11957.994141, 159523.109375, 2399, 341532.53125, 11934.823242, 159477.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1847, 6, 0, 92, 345606.53125, 13011.19043, 162298.046875, 2399, 345742.78125, 13058.754883, 162235.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1848, 6, 0, 92, 347306.65625, 13707.475586, 163034.375, 2399, 347193.875, 13657.240234, 162832.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1849, 6, 0, 92, 350576.53125, 15149.711914, 162877.734375, 2399, 350523.71875, 15121.574219, 163040.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1850, 6, 0, 92, 350557.5, 15127.233398, 162220.109375, 2399, 350701.4375, 15186.978516, 162083.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1851, 6, 0, 92, 343542.875, 11697.291016, 84708.375, 2399, 343336.75, 11760.400391, 84428.070313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1852, 6, 0, 92, 344077.21875, 11758.245117, 85049.03125, 2399, 344265.625, 11754.734375, 85251.367188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1853, 6, 0, 92, 346471.78125, 11793.118164, 89210.015625, 2399, 346216.9375, 11762.414063, 89068.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1854, 6, 0, 92, 347328.125, 11867.552734, 89522.34375, 2399, 347603.03125, 11904.097656, 89579.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1855, 6, 0, 92, 350625.6875, 12567.380859, 92653.390625, 2399, 350503.90625, 12479.306641, 92320.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1856, 6, 0, 92, 350860.84375, 12715.987305, 93222.96875, 2399, 350989.75, 12780.597656, 93495.367188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1857, 6, 0, 92, 354575.5, 13477.484375, 95722.71875, 2399, 354579.34375, 13493.350586, 96091.757813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1858, 6, 0, 92, 354755.9375, 13462.707031, 95095.203125, 2399, 355144.5625, 13477.916992, 95304.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1859, 6, 0, 92, 358121.03125, 13358.365234, 93986.140625, 2399, 357799.90625, 13398.935547, 94150.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1860, 6, 0, 92, 358702.65625, 13291.550781, 93384.296875, 2399, 358714.90625, 13238.422852, 93139.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1861, 6, 0, 92, 349494.53125, 12821.405273, 76246.304688, 2399, 349203.875, 12840.651367, 76378.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1862, 6, 0, 92, 349731.71875, 12805.176758, 75351.3125, 2399, 349955.3125, 12789.298828, 75147.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1863, 6, 0, 92, 352287.625, 12840.533203, 73968.234375, 2399, 352500.65625, 12838.899414, 73912.398438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1864, 6, 0, 92, 355049.59375, 12685.203125, 75401.71875, 2399, 354760.34375, 12679.77832, 75401.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1865, 6, 0, 92, 355605.28125, 12766.117188, 75036.84375, 2399, 355891.21875, 12784.665039, 74984.210938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1866, 6, 0, 92, 360596.25, 12948.257813, 78920.109375, 2399, 360178.78125, 12954.21582, 78998.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1867, 6, 0, 92, 361027.78125, 12973.049805, 78518.351563, 2399, 361338.59375, 13008.193359, 78374.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1868, 6, 0, 92, 362653.59375, 13040.426758, 83033.898438, 2399, 362658.96875, 13066.167969, 82791.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1869, 6, 0, 92, 361270.0625, 13038.529297, 88876.992188, 2399, 360919.75, 13071.126953, 88992.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1870, 6, 0, 92, 361667.1875, 12992.530273, 88201.171875, 2399, 362008.5, 12954.231445, 88048.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1871, 6, 0, 92, 344287.75, 12259.9375, 81611.53125, 2399, 344223.40625, 12151.138672, 81987.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1872, 6, 0, 92, 346713.03125, 12806.513672, 79557.96875, 2399, 346863.21875, 12832.725586, 79908.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1873, 6, 0, 92, 346369.0625, 12775.666016, 79010.90625, 2399, 346304.09375, 12790.920898, 78799.648438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1874, 6, 0, 92, 346283.125, 13081.222656, 76074.703125, 2399, 346569.4375, 13031.316406, 76328.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1875, 6, 0, 92, 345806.34375, 13175.767578, 75518.945313, 2399, 345848.1875, 13192.555664, 75228.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1876, 6, 0, 92, 345389.5625, 11424.351563, 70111.203125, 2399, 345592.53125, 11411.828125, 69819.304688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1877, 6, 0, 92, 344684.25, 11432.568359, 70793.476563, 2399, 344490.375, 11435.342773, 70939.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1878, 6, 0, 92, 339411.875, 11885.739258, 81161.453125, 2399, 339760.96875, 11897.073242, 81351.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1879, 6, 0, 92, 338723.125, 11877.75, 81108.289063, 2399, 338541.71875, 11889.110352, 80992.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1880, 6, 0, 92, 341096.96875, 12154.5, 77477.40625, 2399, 341359.28125, 12191.758789, 77327.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1881, 6, 0, 92, 340688.90625, 12075.568359, 78053.46875, 2399, 340426.03125, 12022.68457, 78322.992188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1882, 6, 0, 92, 341914.1875, 11594.005859, 74605.289063, 2399, 342193.21875, 11634.431641, 74566.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1883, 6, 0, 92, 341214.65625, 11517.303711, 74969.867188, 2399, 340957.5625, 11531.368164, 74855.554688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1884, 6, 0, 92, 341245.1875, 11384.755859, 71705.25, 2399, 341508.15625, 11397.442383, 71547.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1885, 6, 0, 92, 340604.1875, 11392.632813, 71880.367188, 2399, 340289.40625, 11350.15918, 71787.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1886, 6, 0, 92, 325983.625, 11760.800781, 76558.015625, 2399, 325631.9375, 11777.592773, 76319.414063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1887, 6, 0, 92, 326487.3125, 11792.654297, 76754.484375, 2399, 326655.6875, 11812.008789, 76857.132813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1888, 6, 0, 92, 330512.71875, 12338.731445, 78102.265625, 2399, 330401.59375, 12449.401367, 78449.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1889, 6, 0, 92, 330634.21875, 12181.435547, 77525.867188, 2399, 330945.4375, 12131.264648, 77399.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1890, 6, 0, 92, 332993.5625, 11781.085938, 77047.617188, 2399, 332743.59375, 11811.720703, 77177.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1891, 6, 0, 92, 333359.3125, 11682.553711, 76502.015625, 2399, 333669.125, 11682.291992, 76490.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1892, 6, 0, 92, 335090.1875, 12282.8125, 79733.289063, 2399, 335024.5625, 12406.701172, 80169.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1893, 6, 0, 92, 335189.21875, 12147.222656, 79098.90625, 2399, 335274.0625, 12118.339844, 78896.335938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1894, 6, 0, 92, 327703.6875, 11458.161133, 72056.75, 2399, 327520.5, 11477.001953, 72367.320313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1895, 6, 0, 92, 327827.46875, 11428.626953, 71484.695313, 2399, 327915.1875, 11404.943359, 71170.398438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1896, 6, 0, 92, 331815.96875, 11445.224609, 73711.976563, 2399, 331567.625, 11370.342773, 73754.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1897, 6, 0, 92, 332446.34375, 11320.068359, 73421.1875, 2399, 332783.65625, 11288.369141, 73509.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1898, 6, 0, 92, 336320.375, 11415.113281, 74514.953125, 2399, 336196.25, 11441.629883, 74731.429688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1899, 6, 0, 92, 336222.46875, 11335.155273, 73777.328125, 2399, 336404.03125, 11337.617188, 73535.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1900, 6, 0, 92, 337293.46875, 11835.797852, 76992.617188, 2399, 337051.875, 11824.230469, 77199.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1901, 6, 0, 92, 337759.65625, 11792.261719, 76471.5625, 2399, 338198.34375, 11777.477539, 76309.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1902, 6, 0, 92, 313338.75, 11439.638672, 66147.515625, 2399, 313168.25, 11468.181641, 66491.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1903, 6, 0, 92, 313495.875, 11405.710938, 65749.984375, 2399, 313612.46875, 11384.714844, 65499.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1904, 6, 0, 92, 314595.53125, 11777.475586, 68070.21875, 2399, 314165.15625, 11757.348633, 68017.539063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1905, 6, 0, 92, 314855.4375, 11780.867188, 68158.382813, 2399, 315161.3125, 11800.650391, 68185.585938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1906, 6, 0, 92, 318949.71875, 11422.554688, 70001.375, 2399, 318653.6875, 11470.071289, 69904.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1907, 6, 0, 92, 319422.59375, 11449.927734, 70318.734375, 2399, 319664.5625, 11430.043945, 70482.867188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1908, 6, 0, 92, 323503.96875, 11531.4375, 73712.96875, 2399, 323138.46875, 11644.529297, 73823.929688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1909, 6, 0, 92, 324053.90625, 11489.581055, 73215.4375, 2399, 324242.4375, 11481.017578, 73043.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1910, 6, 0, 92, 315010.9375, 11447.117188, 64752.300781, 2399, 314805.78125, 11420.948242, 65032.070313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1911, 6, 0, 92, 315312.96875, 11368.501953, 64175.730469, 2399, 315382.0, 11353.924805, 63888.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1912, 6, 0, 92, 318172.34375, 11316.663086, 65587.492188, 2399, 318161.78125, 11301.776367, 65923.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1913, 6, 0, 92, 318712.5, 11300.980469, 65179.296875, 2399, 318668.71875, 11293.625, 64836.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1914, 6, 0, 92, 320802.15625, 11364.790039, 68171.117188, 2399, 320419.65625, 11387.81543, 68333.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1915, 6, 0, 92, 321402.53125, 11365.464844, 67735.359375, 2399, 321655.40625, 11355.958984, 67565.226563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1916, 6, 0, 92, 322893.84375, 11413.208008, 70417.492188, 2399, 322606.9375, 11412.990234, 70594.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1917, 6, 0, 92, 323469.46875, 11431.271484, 70490.132813, 2399, 323623.71875, 11423.691406, 70964.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1918, 6, 0, 92, 325720.15625, 11433.679688, 68779.921875, 2399, 325394.8125, 11449.499023, 68966.507813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1919, 6, 0, 92, 326021.9375, 11431.836914, 68658.078125, 2399, 326375.71875, 11401.708008, 68399.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1920, 6, 0, 92, 304288.28125, 12507.030273, 67565.125, 2399, 304098.375, 12521.736328, 67938.148438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1921, 6, 0, 92, 304616.09375, 12345.020508, 66973.03125, 2399, 304933.15625, 12218.520508, 66989.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1922, 6, 0, 92, 310556.6875, 12186.150391, 68926.445313, 2399, 311048.46875, 12120.37207, 68959.117188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1923, 6, 0, 92, 310423.8125, 11997.017578, 68281.1875, 2399, 310190.5625, 11954.828125, 68034.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1924, 6, 0, 92, 301127.6875, 12199.956055, 61842.847656, 2399, 301121.375, 12169.628906, 61454.574219);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1925, 6, 0, 92, 301134.03125, 12241.40332, 62369.457031, 2399, 301109.78125, 12271.930664, 62705.132813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1926, 6, 0, 92, 304408.46875, 12213.800781, 64036.242188, 2399, 304165.96875, 12260.225586, 64095.054688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1927, 6, 0, 92, 305128.15625, 12056.308594, 64217.515625, 2399, 305462.4375, 12028.595703, 64152.785156);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1928, 6, 0, 92, 307114.28125, 11785.824219, 66510.96875, 2399, 306991.71875, 11757.566406, 66295.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1929, 6, 0, 92, 307670.15625, 11901.831055, 66824.328125, 2399, 307970.5, 12033.587891, 67033.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1930, 6, 0, 92, 310901.03125, 11566.25, 65304.484375, 2399, 310641.34375, 11571.525391, 65529.617188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1931, 6, 0, 92, 311401.25, 11451.955078, 64913.367188, 2399, 311624.21875, 11419.751953, 64721.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1932, 6, 0, 92, 304798.84375, 11840.603516, 61095.839844, 2399, 304742.59375, 11818.603516, 60793.523438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1933, 6, 0, 92, 304989.34375, 11888.303711, 61713.070313, 2399, 304782.09375, 11950.873047, 62094.195313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1934, 6, 0, 92, 307524.46875, 11655.6875, 62560.175781, 2399, 307167.0, 11690.069336, 62593.539063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1935, 6, 0, 92, 308338.4375, 11602.02832, 62475.929688, 2399, 308636.5625, 11578.640625, 62436.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1936, 6, 0, 92, 310851.71875, 11327.363281, 62115.460938, 2399, 310474.59375, 11354.970703, 62185.414063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1937, 6, 0, 92, 311260.71875, 11292.341797, 61954.832031, 2399, 311670.125, 11249.958984, 61929.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1938, 6, 0, 92, 313707.34375, 11198.966797, 61560.125, 2399, 313853.46875, 11221.963867, 61922.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1939, 6, 0, 92, 313151.75, 11156.974609, 61065.433594, 2399, 312869.6875, 11164.344727, 60952.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1940, 6, 0, 92, 313758.40625, 11268.732422, 60857.980469, 2399, 314015.34375, 11160.792969, 60606.136719);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1941, 6, 0, 92, 300732.3125, 11472.131836, 54077.867188, 2399, 300480.125, 11367.166992, 53800.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1942, 6, 0, 92, 301557.625, 11437.542969, 54482.429688, 2399, 301604.15625, 11467.858398, 54840.003906);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1943, 6, 0, 92, 301677.3125, 11740.207031, 57719.257813, 2399, 301618.125, 11713.273438, 57377.070313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1944, 6, 0, 92, 301602.375, 11827.270508, 58532.367188, 2399, 301787.0, 11849.46875, 58835.300781);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1945, 6, 0, 92, 304216.46875, 11501.55957, 52333.585938, 2399, 304181.8125, 11550.22168, 51972.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1946, 6, 0, 92, 304396.6875, 11530.050781, 52921.710938, 2399, 304441.65625, 11515.179688, 53226.144531);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1947, 6, 0, 92, 303012.84375, 11560.575195, 55800.078125, 2399, 302788.71875, 11576.500977, 56050.449219);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1948, 6, 0, 92, 303887.25, 11555.483398, 56187.914063, 2399, 304207.875, 11552.105469, 56055.496094);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1949, 6, 0, 92, 303566.03125, 11752.586914, 58800.753906, 2399, 303259.34375, 11779.709961, 58997.523438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1950, 6, 0, 92, 304239.625, 11705.047852, 58963.480469, 2399, 304530.71875, 11697.810547, 58889.511719);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1951, 6, 0, 92, 307311.3125, 11516.541992, 52961.75, 2399, 306922.5, 11543.701172, 52740.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1952, 6, 0, 92, 307871.8125, 11475.744141, 53240.152344, 2399, 308190.15625, 11446.279297, 53256.398438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1953, 6, 0, 92, 306917.5625, 11445.59375, 55504.699219, 2399, 306871.21875, 11434.725586, 55058.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1954, 6, 0, 92, 306925.09375, 11451.571289, 56062.273438, 2399, 307152.53125, 11442.544922, 56211.574219);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1955, 6, 0, 92, 307389.53125, 11467.006836, 59065.460938, 2399, 307117.65625, 11480.326172, 58777.957031);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1956, 6, 0, 92, 307780.09375, 11439.844727, 59539.363281, 2399, 307904.125, 11433.908203, 59947.601563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1957, 6, 0, 92, 309540.84375, 11326.607422, 54899.726563, 2399, 309124.21875, 11339.785156, 54882.550781);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1958, 6, 0, 92, 310177.71875, 11293.452148, 54969.648438, 2399, 310544.84375, 11278.613281, 54954.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1959, 6, 0, 92, 309174.46875, 11344.285156, 57542.214844, 2399, 308754.46875, 11376.373047, 57454.933594);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1960, 6, 0, 92, 309712.21875, 11323.308594, 57370.929688, 2399, 310012.875, 11323.681641, 57347.398438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1961, 6, 0, 92, 304424.1875, 11695.505859, 49040.960938, 2399, 304093.09375, 11709.783203, 48777.910156);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1962, 6, 0, 92, 304636.53125, 11691.571289, 49718.335938, 2399, 304818.125, 11709.012695, 50084.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1963, 6, 0, 92, 305896.65625, 11666.261719, 47165.675781, 2399, 305484.375, 11688.818359, 46883.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1964, 6, 0, 92, 306349.15625, 11636.913086, 47233.316406, 2399, 306763.46875, 11622.53125, 47395.222656);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1965, 6, 0, 92, 307493.40625, 11569.829102, 49899.417969, 2399, 307202.25, 11615.738281, 49587.527344);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1966, 6, 0, 92, 307877.0625, 11528.757813, 50574.5625, 2399, 308221.5625, 11495.293945, 50821.074219);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1967, 6, 0, 82, 348605.3125, 12061.388672, 87914.9375, 2395, 348570.875, 11860.771484, 88063.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1968, 6, 0, 82, 349068.4375, 12181.634766, 91205.070313, 2395, 349148.78125, 12148.293945, 91176.617188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1969, 6, 0, 82, 353113.375, 13277.230469, 95351.390625, 2395, 353143.875, 13265.286133, 95208.617188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1970, 6, 0, 82, 354049.09375, 13606.323242, 98049.875, 2395, 354116.625, 13648.636719, 98234.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1971, 6, 0, 82, 356295.75, 13483.606445, 93546.796875, 2395, 356447.25, 13512.158203, 93504.507813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1972, 6, 0, 82, 355729.0, 13548.640625, 96615.78125, 2395, 355892.28125, 13550.476563, 96740.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1973, 6, 0, 82, 354235.1875, 12658.845703, 76319.320313, 2395, 354395.0625, 12728.164063, 76409.835938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1974, 6, 0, 82, 357501.40625, 12679.863281, 76333.796875, 2395, 357391.3125, 12660.606445, 76472.179688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1975, 6, 0, 82, 358979.5625, 12765.945313, 78614.914063, 2395, 358939.40625, 12688.78418, 78439.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1976, 6, 0, 82, 359476.59375, 12711.113281, 76737.851563, 2395, 359531.28125, 12726.692383, 76662.195313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1977, 6, 0, 82, 362618.75, 13126.744141, 81487.476563, 2395, 362747.6875, 13109.731445, 81473.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1978, 6, 0, 82, 363102.65625, 12866.197266, 85684.3125, 2395, 363274.71875, 12833.477539, 85805.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1979, 6, 0, 82, 360531.375, 13037.572266, 90488.046875, 2395, 360678.4375, 13032.978516, 90688.195313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1980, 6, 0, 82, 346679.84375, 12485.813477, 82799.351563, 2395, 346697.71875, 12496.380859, 82647.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1981, 6, 0, 82, 348232.375, 13069.779297, 79538.367188, 2395, 348299.28125, 13080.257813, 79392.507813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1982, 6, 0, 82, 351207.03125, 12676.902344, 75900.09375, 2395, 351121.5625, 12686.871094, 75944.960938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1983, 6, 0, 82, 344962.03125, 12594.321289, 79616.085938, 2395, 344873.375, 12611.920898, 79525.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1984, 6, 0, 82, 348494.875, 12908.503906, 75261.226563, 2395, 348482.375, 12915.306641, 75058.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1985, 6, 0, 82, 350220.46875, 12823.324219, 73234.992188, 2395, 350112.75, 12832.544922, 73242.507813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1986, 6, 0, 82, 341877.25, 12098.03418, 80470.632813, 2395, 341798.5625, 12074.90625, 80392.367188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1987, 6, 0, 82, 341811.40625, 12202.780273, 78149.195313, 2395, 341841.15625, 12198.803711, 78331.382813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1988, 6, 0, 82, 344223.5625, 11452.022461, 72207.882813, 2395, 344141.625, 11480.117188, 72161.835938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1989, 6, 0, 82, 339574.9375, 11914.257813, 79228.117188, 2395, 339536.125, 11925.837891, 79046.445313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1990, 6, 0, 82, 340651.5, 11630.40918, 75697.835938, 2395, 340673.65625, 11670.516602, 75847.460938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1991, 6, 0, 82, 336314.125, 11924.919922, 78276.296875, 2395, 336181.8125, 11905.185547, 78246.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1992, 6, 0, 82, 338343.28125, 11383.494141, 74728.0, 2395, 338333.71875, 11386.712891, 74573.398438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1993, 6, 0, 82, 333539.40625, 11934.386719, 77685.53125, 2395, 333498.625, 11992.668945, 77831.945313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1994, 6, 0, 82, 334056.90625, 11460.371094, 75020.484375, 2395, 334016.71875, 11417.905273, 74913.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1995, 6, 0, 82, 335592.59375, 11275.121094, 72993.460938, 2395, 335469.8125, 11259.890625, 72807.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1996, 6, 0, 82, 330827.78125, 11903.835938, 76495.695313, 2395, 330892.3125, 11834.136719, 76354.882813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1997, 6, 0, 82, 332425.78125, 11236.117188, 72491.0625, 2395, 332378.5625, 11237.255859, 72331.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1998, 6, 0, 82, 328036.0, 11944.866211, 76752.390625, 2395, 327923.0625, 11978.038086, 76879.492188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (1999, 6, 0, 82, 330088.46875, 11486.424805, 73855.5, 2395, 330043.46875, 11466.083008, 73684.085938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2000, 6, 0, 82, 325680.5, 11603.195313, 74789.15625, 2395, 325536.875, 11615.984375, 74883.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2001, 6, 0, 82, 326302.1875, 11505.837891, 73094.859375, 2395, 326244.4375, 11499.166016, 72861.226563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2002, 6, 0, 82, 328907.375, 11419.0625, 72063.617188, 2395, 328957.40625, 11404.543945, 71908.429688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2003, 6, 0, 82, 321468.6875, 11411.585938, 70075.117188, 2395, 321535.625, 11398.037109, 69974.804688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2004, 6, 0, 82, 323948.78125, 11435.521484, 70271.515625, 2395, 324105.65625, 11437.09082, 70219.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2005, 6, 0, 82, 322939.46875, 11387.876953, 68309.539063, 2395, 323005.46875, 11398.052734, 68191.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2006, 6, 0, 82, 311815.90625, 11581.091797, 67380.445313, 2395, 311635.4375, 11599.138672, 67329.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2007, 6, 0, 82, 313713.9375, 11605.59375, 67324.320313, 2395, 313813.28125, 11594.445313, 67239.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2008, 6, 0, 82, 316243.875, 11449.632813, 66552.296875, 2395, 316059.59375, 11486.726563, 66612.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2009, 6, 0, 82, 318829.71875, 11331.550781, 67115.5, 2395, 318857.53125, 11329.011719, 67028.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2010, 6, 0, 82, 312640.84375, 11332.314453, 65915.789063, 2395, 312469.6875, 11346.167969, 65813.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2011, 6, 0, 82, 314859.40625, 11468.847656, 65745.421875, 2395, 314991.28125, 11473.529297, 65794.242188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2012, 6, 0, 82, 317450.3125, 11342.722656, 65564.703125, 2395, 317382.65625, 11336.441406, 65520.835938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2013, 6, 0, 82, 313474.875, 11359.892578, 64369.6875, 2395, 313329.375, 11334.607422, 64310.441406);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2014, 6, 0, 82, 316009.75, 11369.111328, 64453.898438, 2395, 316146.65625, 11407.845703, 64425.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2015, 6, 0, 82, 319485.0, 11295.119141, 64820.046875, 2395, 319648.53125, 11302.495117, 64723.816406);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2016, 6, 0, 82, 303138.59375, 12700.203125, 67708.046875, 2395, 303001.90625, 12708.087891, 67661.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2017, 6, 0, 82, 305115.65625, 12205.644531, 67730.460938, 2395, 305255.90625, 12156.974609, 67719.929688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2018, 6, 0, 82, 306892.90625, 11945.091797, 67548.421875, 2395, 306783.375, 11923.670898, 67527.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2019, 6, 0, 82, 302329.59375, 12631.439453, 66656.929688, 2395, 302433.84375, 12667.0, 66574.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2020, 6, 0, 82, 304742.9375, 12270.845703, 66299.664063, 2395, 304825.90625, 12238.816406, 66157.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2021, 6, 0, 82, 308201.84375, 12064.082031, 66090.171875, 2395, 308409.28125, 12080.390625, 66025.476563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2022, 6, 0, 82, 304663.65625, 12234.863281, 64947.261719, 2395, 304481.9375, 12279.056641, 64950.089844);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2023, 6, 0, 82, 307126.25, 12032.234375, 64605.125, 2395, 307264.4375, 12053.898438, 64497.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2024, 6, 0, 82, 309607.75, 11788.849609, 64329.660156, 2395, 309491.4375, 11798.347656, 64341.238281);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2025, 6, 0, 82, 303613.25, 12223.667969, 63217.375, 2395, 303429.46875, 12240.571289, 63247.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2026, 6, 0, 82, 306180.75, 11884.476563, 63211.644531, 2395, 306047.25, 11880.149414, 63239.613281);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2027, 6, 0, 82, 308472.125, 11764.763672, 63184.507813, 2395, 308330.9375, 11801.876953, 63270.492188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2028, 6, 0, 82, 310793.90625, 11418.761719, 62948.914063, 2395, 310649.6875, 11452.429688, 63003.472656);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2029, 6, 0, 82, 312720.71875, 11287.966797, 62912.109375, 2395, 312634.40625, 11305.080078, 62904.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2030, 6, 0, 82, 300458.1875, 12280.097656, 61478.546875, 2395, 300262.59375, 12319.505859, 61579.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2031, 6, 0, 82, 302264.8125, 12113.365234, 61794.605469, 2395, 302334.46875, 12124.8125, 61701.398438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2032, 6, 0, 82, 305498.15625, 11821.148438, 61852.726563, 2395, 305622.9375, 11797.349609, 61807.167969);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2033, 6, 0, 82, 307513.6875, 11546.682617, 61673.640625, 2395, 307430.75, 11528.699219, 61551.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2034, 6, 0, 82, 309411.09375, 11363.001953, 61374.484375, 2395, 309320.375, 11356.431641, 61276.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2035, 6, 0, 82, 311561.34375, 11283.681641, 61150.441406, 2395, 311422.9375, 11223.072266, 61115.460938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2036, 6, 0, 82, 301791.1875, 11949.371094, 59803.203125, 2395, 301587.9375, 11971.140625, 59894.929688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2037, 6, 0, 82, 303154.28125, 11910.185547, 60292.671875, 2395, 303118.40625, 11901.991211, 60182.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2038, 6, 0, 82, 304662.3125, 11744.03125, 59902.105469, 2395, 304767.5, 11751.283203, 59824.363281);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2039, 6, 0, 82, 306288.15625, 11629.109375, 60197.417969, 2395, 306349.71875, 11594.005859, 60101.929688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2040, 6, 0, 82, 308549.09375, 11394.0625, 59712.03125, 2395, 308686.4375, 11387.317383, 59610.617188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2041, 6, 0, 82, 309961.5, 11315.816406, 59491.324219, 2395, 310031.15625, 11313.282227, 59358.484375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2042, 6, 0, 82, 301156.625, 11881.34375, 58778.445313, 2395, 300947.59375, 11919.900391, 58827.527344);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2043, 6, 0, 82, 303810.09375, 11702.345703, 58222.546875, 2395, 303715.71875, 11696.880859, 58078.351563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2044, 6, 0, 82, 306095.0, 11557.003906, 58553.214844, 2395, 306257.125, 11550.227539, 58542.554688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2045, 6, 0, 82, 307651.625, 11434.009766, 57547.496094, 2395, 307559.46875, 11437.595703, 57614.675781);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2046, 6, 0, 82, 309346.6875, 11349.910156, 58358.898438, 2395, 309209.09375, 11357.787109, 58362.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2047, 6, 0, 82, 303263.3125, 11654.866211, 57233.75, 2395, 303198.75, 11653.546875, 57133.917969);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2048, 6, 0, 82, 305982.09375, 11470.3125, 55429.234375, 2395, 305821.46875, 11478.597656, 55438.648438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2049, 6, 0, 82, 308158.75, 11375.644531, 54425.710938, 2395, 308103.0625, 11376.460938, 54361.378906);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2050, 6, 0, 82, 302652.34375, 11515.242188, 55393.480469, 2395, 302637.34375, 11508.751953, 55241.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2051, 6, 0, 82, 305379.59375, 11499.080078, 54336.617188, 2395, 305372.6875, 11518.853516, 54192.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2052, 6, 0, 82, 307537.90625, 11466.0, 53538.355469, 2395, 307414.4375, 11476.535156, 53581.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2053, 6, 0, 82, 304903.9375, 11550.402344, 52938.542969, 2395, 305039.875, 11557.763672, 52952.535156);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2054, 6, 0, 82, 305624.1875, 11603.980469, 52201.054688, 2395, 305652.96875, 11635.873047, 52082.011719);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2055, 6, 0, 82, 307336.65625, 11579.791992, 51967.933594, 2395, 307276.125, 11590.025391, 51847.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2056, 6, 0, 82, 307720.4375, 11549.392578, 50188.382813, 2395, 307585.71875, 11570.382813, 50270.167969);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2057, 6, 0, 82, 301998.6875, 11430.529297, 54263.953125, 2395, 302116.6875, 11429.132813, 54197.707031);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2058, 6, 0, 82, 303485.5625, 11526.400391, 52254.410156, 2395, 303370.65625, 11483.706055, 52333.273438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2059, 6, 0, 82, 305236.1875, 11738.660156, 49681.882813, 2395, 305374.5625, 11738.154297, 49559.851563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2060, 6, 0, 82, 306642.9375, 11636.367188, 48914.964844, 2395, 306597.28125, 11639.208008, 48816.152344);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2061, 6, 0, 82, 300237.09375, 11438.164063, 54870.136719, 2395, 300110.71875, 11429.642578, 54834.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2062, 6, 0, 82, 302950.90625, 11499.617188, 51406.976563, 2395, 302911.5625, 11492.166992, 51268.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2063, 6, 0, 82, 304667.84375, 11734.375, 48805.3125, 2395, 304856.34375, 11743.15625, 48723.949219);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2064, 6, 0, 82, 306112.5625, 11668.75, 47992.804688, 2395, 306040.375, 11673.038086, 47926.714844);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2065, 7, 2, 472, 0.0, 0.0, 0.0, 2389, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2066, 7, 1, 0, 0.0, 0.0, 0.0, 2408, 40192.507813, 16594.0, 163627.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2067, 7, 1, 0, 0.0, 0.0, 0.0, 2408, 101471.046875, 19904.667969, 163693.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2068, 7, 1, 0, 0.0, 0.0, 0.0, 2408, 157526.40625, 30900.0, 69818.132813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2069, 7, 0, 97, 215063.796875, 24113.0, 49350.085938, 2401, 215219.984375, 24113.0, 49493.945313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2070, 7, 0, 97, 214145.0625, 24113.003906, 49312.765625, 2401, 214137.5, 24113.0, 49489.707031);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2071, 7, 0, 97, 213297.328125, 24113.0, 50106.90625, 2401, 213120.578125, 24113.0, 50201.355469);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2072, 7, 0, 97, 215681.6875, 24112.996094, 51183.390625, 2401, 215683.296875, 24113.0, 51389.289063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2073, 7, 0, 97, 215022.875, 24113.0, 50857.125, 2401, 214930.796875, 24113.0, 50722.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2074, 7, 0, 97, 213976.78125, 24113.0, 51388.363281, 2401, 213932.296875, 24113.0, 51216.855469);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2075, 7, 0, 97, 208328.6875, 24919.542969, 51371.671875, 2401, 208353.984375, 24919.894531, 51212.144531);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2076, 7, 0, 97, 207632.6875, 24903.998047, 50168.507813, 2401, 207670.09375, 24904.0, 49988.601563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2077, 7, 0, 97, 207520.953125, 24903.996094, 48274.25, 2401, 207491.421875, 24904.0, 48444.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2078, 7, 0, 97, 206977.96875, 24906.171875, 47142.0, 2401, 207153.46875, 24904.0, 47154.667969);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2079, 7, 0, 97, 206901.859375, 24904.0, 51452.445313, 2401, 206926.296875, 24904.0, 51318.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2080, 7, 0, 97, 206551.390625, 24903.996094, 49027.160156, 2401, 206642.8125, 24904.0, 49150.730469);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2081, 7, 0, 97, 206047.59375, 24904.0, 47754.542969, 2401, 206087.328125, 24904.0, 47942.144531);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2082, 7, 0, 97, 206163.140625, 24904.001953, 46465.105469, 2401, 206087.859375, 24904.0, 46609.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2083, 7, 0, 97, 197810.203125, 25926.371094, 62007.222656, 2401, 197783.1875, 25947.396484, 62282.808594);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2084, 7, 0, 97, 199373.75, 25991.332031, 57984.664063, 2401, 199208.03125, 25898.0, 58015.746094);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2085, 7, 0, 97, 200085.921875, 25935.667969, 56610.515625, 2401, 200103.796875, 25898.0, 56395.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2086, 7, 0, 97, 198867.625, 25897.996094, 56963.78125, 2401, 198721.515625, 25898.0, 56860.667969);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2087, 7, 0, 97, 200663.15625, 25899.191406, 50646.929688, 2401, 200587.859375, 25898.0, 50804.371094);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2088, 7, 0, 97, 197886.859375, 25900.933594, 47499.238281, 2401, 197876.921875, 25950.507813, 47245.871094);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2089, 7, 0, 97, 199105.265625, 25897.787109, 51645.699219, 2401, 199268.984375, 25897.619141, 51605.269531);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2090, 7, 0, 97, 199054.359375, 25904.382813, 50135.0, 2401, 198938.96875, 25897.705078, 50219.195313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2091, 7, 0, 97, 197191.859375, 25900.890625, 48601.113281, 2401, 197262.390625, 26016.65625, 48731.019531);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2092, 7, 0, 97, 196396.546875, 25899.050781, 47750.585938, 2401, 196396.40625, 25899.265625, 47868.367188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2093, 7, 0, 97, 194737.359375, 25897.998047, 45269.359375, 2401, 194988.734375, 25898.484375, 45219.449219);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2094, 7, 0, 97, 193580.28125, 25897.996094, 44371.320313, 2401, 193701.28125, 25898.0, 44456.042969);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2095, 7, 0, 97, 192241.640625, 25898.0, 44794.578125, 2401, 192183.21875, 25898.0, 44951.847656);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2096, 7, 0, 97, 191870.140625, 26900.587891, 58638.988281, 2401, 192001.125, 26901.001953, 58847.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2097, 7, 0, 97, 192607.234375, 26935.714844, 57052.113281, 2401, 192528.828125, 26920.101563, 56963.277344);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2098, 7, 0, 97, 191642.0625, 26938.738281, 53590.257813, 2401, 191770.890625, 26922.707031, 53764.179688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2099, 7, 0, 97, 190922.65625, 26900.050781, 58581.257813, 2401, 190808.515625, 26900.119141, 58810.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2100, 7, 0, 97, 191017.671875, 26899.998047, 57307.101563, 2401, 190919.828125, 26900.054688, 57536.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2101, 7, 0, 97, 190720.65625, 26900.525391, 54750.554688, 2401, 190576.90625, 26900.009766, 54651.039063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2102, 7, 0, 97, 190102.09375, 26900.826172, 53891.972656, 2401, 189996.203125, 26900.056641, 53683.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2103, 7, 0, 97, 190471.6875, 26911.083984, 52563.976563, 2401, 190406.703125, 26906.474609, 52703.304688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2104, 7, 0, 97, 183976.28125, 27781.009766, 52872.652344, 2401, 184137.9375, 27771.142578, 53014.714844);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2105, 7, 0, 97, 182912.578125, 27782.246094, 52226.429688, 2401, 182765.15625, 27796.785156, 52372.050781);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2106, 7, 0, 97, 180182.515625, 27770.615234, 53309.5, 2401, 180388.796875, 27770.8125, 53086.382813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2107, 7, 0, 97, 179285.578125, 27771.001953, 53010.742188, 2401, 179050.203125, 27795.613281, 53004.554688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2108, 7, 0, 97, 183430.3125, 27787.011719, 56619.621094, 2401, 183642.859375, 27826.580078, 56649.179688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2109, 7, 0, 97, 182700.015625, 27840.560547, 57265.011719, 2401, 182487.90625, 27864.150391, 57342.726563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2110, 7, 0, 97, 172155.71875, 28841.001953, 65239.503906, 2401, 172114.59375, 28841.001953, 65520.261719);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2111, 7, 0, 97, 172434.203125, 28841.009766, 61172.691406, 2401, 172627.59375, 28864.808594, 61168.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2112, 7, 0, 97, 173209.4375, 28850.679688, 60207.015625, 2401, 173444.71875, 28842.570313, 60299.757813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2113, 7, 0, 97, 169361.875, 28844.894531, 57796.835938, 2401, 169220.703125, 28841.832031, 57977.144531);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2114, 7, 0, 97, 164118.25, 29515.263672, 56255.207031, 2401, 164302.890625, 29524.111328, 56126.957031);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2115, 7, 0, 97, 163962.75, 29507.003906, 54785.304688, 2401, 163802.71875, 29502.541016, 54658.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2116, 7, 0, 97, 163685.09375, 29500.453125, 53080.625, 2401, 163870.078125, 29500.960938, 52980.769531);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2117, 7, 0, 97, 163135.90625, 29500.0, 52367.386719, 2401, 163301.859375, 29500.0, 52187.601563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2118, 7, 0, 97, 163054.609375, 29500.638672, 55523.675781, 2401, 163017.046875, 29527.667969, 55295.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2119, 7, 0, 97, 160697.109375, 29500.0, 52279.910156, 2401, 160484.296875, 29500.001953, 52201.433594);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2120, 7, 0, 97, 162593.671875, 29500.121094, 56264.917969, 2401, 162428.90625, 29503.146484, 56334.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2121, 7, 0, 97, 162122.765625, 29522.457031, 55342.054688, 2401, 161971.71875, 29500.257813, 55398.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2122, 7, 0, 97, 160770.296875, 29510.960938, 54029.917969, 2401, 160611.046875, 29500.001953, 54116.617188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2123, 7, 0, 97, 155563.59375, 30899.998047, 74823.945313, 2401, 155431.953125, 31060.773438, 74703.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2124, 7, 0, 97, 156550.5625, 30943.503906, 74331.828125, 2401, 156387.5625, 30966.726563, 74449.960938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2125, 7, 0, 97, 159126.140625, 30900.0, 72726.617188, 2401, 159114.046875, 30900.0, 72926.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2126, 7, 0, 97, 156418.375, 30898.363281, 71590.351563, 2401, 156247.25, 30898.914063, 71622.226563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2127, 7, 0, 97, 157633.875, 30900.001953, 70631.367188, 2401, 157806.828125, 30900.0, 70625.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2128, 7, 0, 97, 158126.46875, 30900.0, 69580.15625, 2401, 158342.84375, 30900.0, 69448.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2129, 7, 0, 97, 156298.328125, 30900.0, 70432.109375, 2401, 156126.46875, 30900.0, 70432.570313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2130, 7, 0, 97, 157715.65625, 30900.0, 68498.546875, 2401, 157530.875, 30901.984375, 68377.953125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2131, 7, 0, 97, 159178.484375, 30914.582031, 65592.289063, 2401, 159110.515625, 30908.505859, 65482.855469);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2132, 7, 0, 97, 159511.515625, 30901.441406, 63356.339844, 2401, 159336.203125, 31186.789063, 63475.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2133, 7, 0, 97, 159041.25, 30900.001953, 61774.453125, 2401, 158937.15625, 30943.0, 61579.585938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2134, 7, 0, 97, 157919.953125, 30900.0, 65183.734375, 2401, 157766.140625, 30900.001953, 65381.664063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2135, 7, 0, 97, 158082.09375, 30921.675781, 62409.496094, 2401, 158122.859375, 30910.792969, 62612.824219);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2136, 7, 0, 97, 186769.015625, 20619.0, 77662.414063, 2401, 186692.421875, 20619.0, 77300.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2137, 7, 0, 97, 184794.21875, 20639.371094, 77522.578125, 2401, 184617.703125, 20650.84375, 77773.710938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2138, 7, 0, 97, 182327.4375, 20761.058594, 78834.828125, 2401, 182530.859375, 20724.78125, 78573.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2139, 7, 0, 97, 179635.40625, 20832.316406, 79893.585938, 2401, 179622.453125, 20837.046875, 79538.070313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2140, 7, 0, 97, 187725.3125, 20619.0, 81284.34375, 2401, 187528.140625, 20619.0, 81610.257813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2141, 7, 0, 97, 186222.65625, 20894.693359, 81761.9375, 2401, 185934.25, 20939.382813, 82015.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2142, 7, 0, 97, 184085.578125, 21115.566406, 84287.78125, 2401, 183852.71875, 20834.746094, 84681.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2143, 7, 0, 97, 185130.875, 20683.0, 85981.90625, 2401, 184706.546875, 20693.591797, 86073.867188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2144, 7, 0, 97, 180071.90625, 20744.986328, 86785.710938, 2401, 179792.84375, 20754.230469, 86818.835938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2145, 7, 0, 97, 179682.25, 20736.0, 84923.734375, 2401, 179919.765625, 20736.0, 85183.632813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2146, 7, 0, 97, 178224.75, 20736.0, 82953.351563, 2401, 178050.84375, 20736.0, 82884.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2147, 7, 0, 97, 177382.4375, 20793.910156, 80559.4375, 2401, 177717.6875, 20804.566406, 80517.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2148, 7, 0, 97, 178209.921875, 20738.984375, 86910.15625, 2401, 178040.03125, 20738.466797, 86718.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2149, 7, 0, 97, 175345.875, 20798.076172, 82892.648438, 2401, 175366.796875, 20828.078125, 83237.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2150, 7, 0, 97, 176122.125, 20801.539063, 81238.507813, 2401, 176170.859375, 20792.519531, 81485.945313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2151, 7, 0, 97, 174892.421875, 20736.833984, 84857.15625, 2401, 174809.546875, 20736.855469, 85023.039063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2152, 7, 0, 97, 173283.1875, 20736.0, 85447.484375, 2401, 173022.21875, 20736.0, 85567.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2153, 7, 0, 97, 171701.859375, 20736.0, 85392.226563, 2401, 171350.0, 20736.0, 85485.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2154, 7, 0, 97, 175237.671875, 20736.0, 86727.585938, 2401, 175018.5, 20736.0, 86677.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2155, 7, 0, 97, 173311.421875, 20736.0, 87100.914063, 2401, 173091.640625, 20736.0, 86881.820313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2156, 7, 0, 97, 171628.71875, 20736.0, 87216.882813, 2401, 171662.21875, 20736.0, 86927.835938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2157, 7, 0, 97, 169275.609375, 20736.0, 87397.125, 2401, 169306.125, 20736.0, 87132.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2158, 7, 0, 97, 166447.984375, 20736.0, 88556.53125, 2401, 166663.046875, 20736.0, 88783.664063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2159, 7, 0, 97, 166006.53125, 20736.0, 90527.90625, 2401, 166282.734375, 20736.0, 90617.101563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2160, 7, 0, 97, 166118.765625, 21159.65625, 95054.929688, 2401, 166421.21875, 21226.970703, 95040.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2161, 7, 0, 97, 167762.09375, 20736.0, 90198.25, 2401, 167817.46875, 20736.0, 90543.125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2162, 7, 0, 97, 167243.796875, 21241.060547, 93860.617188, 2401, 167560.21875, 21241.091797, 93812.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2163, 7, 0, 97, 167627.9375, 21317.039063, 95656.179688, 2401, 167961.703125, 21277.621094, 95502.398438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2164, 7, 0, 97, 154771.734375, 22210.591797, 90371.398438, 2401, 155043.109375, 22211.0, 90421.8125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2165, 7, 0, 97, 154658.296875, 22211.0, 92051.515625, 2401, 154595.578125, 22211.0, 92324.945313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2166, 7, 0, 97, 156049.953125, 22211.0, 91509.140625, 2401, 156036.515625, 22211.0, 91794.835938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2167, 7, 0, 97, 155431.109375, 22210.324219, 94082.140625, 2401, 155470.75, 22210.517578, 94358.390625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2168, 7, 0, 97, 154445.515625, 22318.71875, 96817.25, 2401, 154686.53125, 22244.675781, 96630.789063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2169, 7, 0, 97, 156319.25, 22209.685547, 97044.320313, 2401, 156199.359375, 22210.996094, 96885.734375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2170, 7, 0, 97, 155224.65625, 22210.998047, 99954.609375, 2401, 155496.703125, 22211.0, 100025.304688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2171, 7, 0, 97, 155189.4375, 22211.0, 102279.734375, 2401, 154979.234375, 22211.0, 101850.320313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2172, 7, 0, 97, 153512.390625, 22211.0, 101512.78125, 2401, 153304.765625, 22211.0, 101635.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2173, 7, 0, 97, 151807.359375, 22212.001953, 101118.296875, 2401, 151510.265625, 22213.826172, 100954.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2174, 7, 0, 97, 149345.984375, 22211.0, 100649.398438, 2401, 149725.71875, 22211.0, 100678.789063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2175, 7, 0, 97, 151438.15625, 22217.916016, 103765.796875, 2401, 151097.25, 22211.0, 103580.789063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2176, 7, 0, 97, 150592.84375, 22211.0, 102197.414063, 2401, 150873.8125, 22211.0, 102272.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2177, 7, 0, 97, 148229.28125, 22210.371094, 101958.726563, 2401, 148000.78125, 22211.0, 101530.601563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2178, 7, 0, 97, 145155.140625, 22258.632813, 99198.273438, 2401, 144960.359375, 22375.8125, 98940.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2179, 7, 0, 97, 147040.71875, 22211.0, 98168.671875, 2401, 147097.046875, 22211.0, 97894.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2180, 7, 0, 97, 146664.265625, 22211.0, 96352.09375, 2401, 146923.3125, 22211.0, 96144.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2181, 7, 0, 97, 147558.0, 22211.0, 94457.109375, 2401, 147823.09375, 22211.0, 94175.476563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2182, 7, 0, 97, 149917.75, 22211.675781, 93024.421875, 2401, 149574.171875, 22211.0, 92988.914063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2183, 7, 0, 97, 151968.796875, 23512.128906, 98601.109375, 2401, 151654.765625, 23440.763672, 98764.789063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2184, 7, 0, 97, 152109.1875, 23614.125, 95950.765625, 2401, 151697.328125, 23643.982422, 95945.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2185, 7, 0, 97, 164453.984375, 26398.585938, 83486.007813, 2401, 164369.53125, 26394.826172, 83791.070313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2186, 7, 0, 97, 158604.046875, 26393.359375, 86263.289063, 2401, 158922.046875, 26390.777344, 86116.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2187, 7, 0, 97, 154417.75, 26480.576172, 86651.96875, 2401, 154080.6875, 26468.371094, 86806.773438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2188, 7, 0, 97, 149146.359375, 26506.517578, 86668.34375, 2401, 149307.609375, 26503.275391, 86828.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2189, 7, 0, 97, 148006.09375, 26509.34375, 86615.445313, 2401, 147613.453125, 26510.271484, 86750.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2190, 7, 0, 97, 172154.5, 23789.728516, 107913.007813, 2401, 171959.609375, 23788.546875, 107727.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2191, 7, 0, 97, 170676.140625, 23830.916016, 107115.679688, 2401, 170318.8125, 23825.503906, 106988.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2192, 7, 0, 97, 168031.171875, 23836.109375, 103934.351563, 2401, 167834.40625, 23816.039063, 103578.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2193, 7, 0, 97, 167505.765625, 23848.144531, 102548.132813, 2401, 167567.921875, 23820.068359, 102094.804688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2194, 7, 0, 97, 170799.984375, 23771.394531, 108344.539063, 2401, 170537.84375, 23765.095703, 108533.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2195, 7, 0, 97, 166833.625, 23826.287109, 104316.226563, 2401, 166533.15625, 23869.511719, 104153.226563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2196, 7, 0, 97, 166083.515625, 24130.851563, 101639.4375, 2401, 165833.59375, 24180.894531, 101853.101563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2197, 7, 0, 97, 163654.65625, 23555.474609, 109235.484375, 2401, 163976.28125, 23572.222656, 109216.992188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2198, 7, 0, 97, 168301.3125, 23688.917969, 111428.273438, 2401, 168365.5, 23650.822266, 111129.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2199, 7, 0, 97, 166819.484375, 23523.015625, 110839.078125, 2401, 166579.25, 23519.166016, 110582.335938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2200, 7, 0, 97, 164316.578125, 23440.585938, 110535.789063, 2401, 164072.15625, 23414.925781, 110707.429688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2201, 7, 0, 97, 162704.0, 23341.267578, 110414.8125, 2401, 162357.75, 23310.279297, 110583.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2202, 7, 0, 97, 167061.421875, 23963.957031, 112571.039063, 2401, 166781.65625, 23970.160156, 112703.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2203, 7, 0, 97, 163800.125, 23455.683594, 113242.148438, 2401, 163985.953125, 23488.107422, 113072.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2204, 7, 0, 97, 161725.546875, 23196.287109, 112481.359375, 2401, 161531.859375, 23177.935547, 112312.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2205, 7, 0, 97, 162513.828125, 23441.230469, 116113.203125, 2401, 162245.03125, 23369.445313, 116223.320313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2206, 7, 0, 97, 160504.234375, 23050.875, 114805.484375, 2401, 160235.625, 23027.630859, 114785.53125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2207, 7, 0, 97, 160510.421875, 22948.998047, 118564.421875, 2401, 160430.09375, 22948.363281, 118992.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2208, 7, 0, 97, 159957.421875, 22834.488281, 117260.351563, 2401, 159992.640625, 22871.208984, 116972.289063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2209, 7, 0, 97, 158753.46875, 22751.419922, 115879.03125, 2401, 158452.375, 22716.011719, 115756.554688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2210, 7, 0, 97, 156675.171875, 22813.753906, 114867.0, 2401, 156376.21875, 22804.970703, 114691.164063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2211, 7, 0, 97, 158988.65625, 22461.380859, 118380.242188, 2401, 158674.921875, 22332.076172, 118473.710938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2212, 7, 0, 97, 156848.140625, 22494.390625, 116569.070313, 2401, 156705.765625, 22451.923828, 116762.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2213, 7, 0, 97, 155286.6875, 22611.65625, 115794.140625, 2401, 155030.03125, 22669.041016, 115564.320313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2214, 7, 0, 97, 153715.5625, 23004.699219, 115053.375, 2401, 153296.375, 22957.738281, 115127.570313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2215, 7, 0, 97, 148392.140625, 23094.355469, 116863.320313, 2401, 148705.8125, 23104.732422, 116936.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2216, 7, 0, 97, 148676.625, 23373.644531, 114299.867188, 2401, 148471.859375, 23393.003906, 114260.023438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2217, 7, 0, 97, 146625.328125, 23156.042969, 117618.523438, 2401, 146893.140625, 23217.597656, 117486.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2218, 7, 0, 97, 146686.9375, 22867.730469, 116189.539063, 2401, 146490.328125, 22864.980469, 116037.195313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2219, 7, 0, 97, 146129.671875, 23127.572266, 114201.328125, 2401, 146417.140625, 23148.726563, 114238.632813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2220, 7, 0, 97, 142588.859375, 23047.539063, 115947.171875, 2401, 142589.6875, 22989.816406, 116302.054688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2221, 7, 0, 97, 143118.6875, 23715.589844, 112975.492188, 2401, 142878.53125, 23673.113281, 113015.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2222, 7, 0, 97, 141234.078125, 23054.234375, 115906.960938, 2401, 141042.125, 22935.601563, 116314.367188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2223, 7, 0, 97, 141547.40625, 23384.242188, 114377.125, 2401, 141433.3125, 23435.199219, 114035.632813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2224, 7, 0, 97, 136299.5625, 24053.121094, 110135.109375, 2401, 136601.421875, 24050.769531, 110065.234375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2225, 7, 0, 97, 135930.359375, 24278.392578, 108843.226563, 2401, 135712.296875, 24417.914063, 108607.164063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2226, 7, 0, 97, 133912.5, 25302.574219, 105353.921875, 2401, 133940.859375, 25242.013672, 105566.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2227, 7, 0, 97, 133048.5625, 25232.837891, 104366.023438, 2401, 133108.21875, 25208.083984, 104079.367188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2228, 7, 0, 97, 134724.515625, 24055.115234, 110225.429688, 2401, 134345.375, 24075.121094, 110193.101563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2229, 7, 0, 97, 132673.25, 24822.169922, 106238.570313, 2401, 132552.859375, 24710.427734, 106600.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2230, 7, 0, 97, 132279.484375, 25207.792969, 101854.453125, 2401, 132515.71875, 25143.189453, 101972.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2231, 7, 0, 97, 137085.171875, 25169.900391, 100436.867188, 2401, 137324.625, 25202.933594, 100664.210938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2232, 7, 0, 97, 131378.296875, 25778.072266, 100880.960938, 2401, 131529.171875, 25786.275391, 100572.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2233, 7, 0, 97, 133220.390625, 25262.167969, 100449.921875, 2401, 133307.15625, 25284.058594, 100099.851563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2234, 7, 0, 97, 135856.765625, 25371.472656, 98881.882813, 2401, 135589.984375, 25457.191406, 98733.476563);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2235, 7, 0, 97, 137411.65625, 25262.058594, 98550.140625, 2401, 137271.859375, 25274.447266, 98202.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2236, 7, 0, 97, 140127.390625, 25624.160156, 94974.445313, 2401, 140246.359375, 25693.128906, 94699.414063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2237, 7, 0, 97, 141774.765625, 26303.152344, 89121.359375, 2401, 141727.546875, 26259.732422, 89426.789063);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2238, 7, 0, 97, 144205.265625, 26432.195313, 84531.835938, 2401, 144009.328125, 26442.001953, 84693.882813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2239, 7, 0, 97, 139106.0625, 25575.681641, 93656.695313, 2401, 139149.421875, 25624.339844, 93368.804688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2240, 7, 0, 97, 140333.078125, 26032.089844, 88762.375, 2401, 140224.046875, 25987.955078, 89071.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2241, 7, 0, 97, 141632.453125, 26312.484375, 87553.289063, 2401, 141444.90625, 26263.185547, 87258.460938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2242, 7, 0, 97, 142746.0, 26513.830078, 84461.546875, 2401, 142455.796875, 26488.703125, 84308.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2243, 7, 0, 97, 138531.046875, 27197.068359, 80468.921875, 2401, 138240.375, 27215.261719, 80812.695313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2244, 7, 0, 97, 140119.0, 27106.433594, 79686.148438, 2401, 140402.28125, 27097.044922, 79620.257813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2245, 7, 0, 97, 140285.109375, 27365.724609, 75773.546875, 2401, 139943.875, 27359.15625, 75704.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2246, 7, 0, 97, 139089.671875, 27397.236328, 74122.398438, 2401, 138917.125, 27366.0, 73834.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2247, 7, 0, 97, 137370.953125, 27395.433594, 73918.320313, 2401, 137106.84375, 27395.943359, 73675.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2248, 7, 0, 97, 138480.953125, 27427.246094, 71577.515625, 2401, 138179.53125, 27418.210938, 71517.320313);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2249, 7, 0, 97, 137381.3125, 27404.019531, 70054.148438, 2401, 137073.078125, 27405.466797, 69848.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2250, 7, 0, 97, 135493.1875, 27366.429688, 69364.40625, 2401, 135207.125, 27355.625, 69366.382813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2251, 7, 0, 97, 136035.109375, 27410.017578, 64644.605469, 2401, 135836.03125, 27404.615234, 64917.972656);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2252, 7, 0, 97, 132249.78125, 27806.886719, 79638.8125, 2401, 132265.90625, 27807.433594, 79321.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2253, 7, 0, 97, 133511.734375, 27532.201172, 77451.984375, 2401, 133225.34375, 27557.0, 77458.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2254, 7, 0, 97, 134491.140625, 27416.369141, 76212.828125, 2401, 134542.1875, 27406.001953, 75900.585938);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2255, 7, 0, 97, 132455.9375, 27608.099609, 75976.398438, 2401, 132518.59375, 27512.304688, 75593.742188);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2256, 7, 0, 97, 133339.765625, 27403.916016, 72656.335938, 2401, 133368.171875, 27405.314453, 72342.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2257, 7, 0, 97, 129641.195313, 28134.087891, 80289.328125, 2401, 129610.796875, 28097.125, 80034.882813);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2258, 7, 0, 97, 128291.828125, 28169.335938, 79102.453125, 2401, 128500.742188, 28118.212891, 78833.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2259, 7, 0, 97, 127356.0, 27957.210938, 74250.523438, 2401, 127298.59375, 28008.466797, 74541.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2260, 7, 0, 97, 128932.882813, 27902.150391, 74010.757813, 2401, 128713.03125, 27922.824219, 73655.859375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2261, 7, 0, 97, 129593.367188, 27564.160156, 71884.8125, 2401, 129812.523438, 27563.267578, 72003.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2262, 7, 0, 474, 93906.601563, 20795.099609, 159330.15625, 2404, 93778.140625, 20792.816406, 159357.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2263, 7, 0, 474, 92753.960938, 20801.650391, 159032.875, 2404, 92618.09375, 20806.335938, 159014.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2264, 7, 0, 474, 89455.398438, 20953.039063, 159067.125, 2404, 89300.710938, 20955.166016, 159034.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2265, 7, 0, 474, 88522.835938, 20978.773438, 158551.5625, 2404, 88399.09375, 20985.597656, 158592.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2266, 7, 0, 474, 93193.234375, 20731.550781, 160178.828125, 2404, 93115.78125, 20733.390625, 160310.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2267, 7, 0, 474, 88669.320313, 20973.523438, 159605.6875, 2404, 88562.867188, 20966.417969, 159716.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2268, 7, 0, 474, 109669.445313, 20757.259766, 158867.09375, 2404, 109651.953125, 20764.316406, 158727.1875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2269, 7, 0, 474, 108790.054688, 20784.705078, 157637.078125, 2404, 108764.765625, 20799.117188, 157457.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2270, 7, 0, 474, 105858.515625, 20816.105469, 156450.09375, 2404, 105936.632813, 20809.386719, 156550.828125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2271, 7, 0, 474, 104944.3125, 20834.324219, 156204.890625, 2404, 104797.125, 20846.824219, 156210.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2272, 7, 0, 474, 100609.15625, 20836.048828, 156667.921875, 2404, 100688.148438, 20817.492188, 156631.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2273, 7, 0, 474, 99486.46875, 20846.333984, 156840.234375, 2404, 99291.570313, 20851.191406, 156891.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2274, 7, 0, 474, 108341.9375, 20742.261719, 158792.296875, 2404, 108250.265625, 20742.191406, 158814.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2275, 7, 0, 474, 105156.507813, 20783.078125, 157102.515625, 2404, 104991.984375, 20793.099609, 157134.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2276, 7, 0, 474, 99970.875, 20791.791016, 157634.328125, 2404, 100056.78125, 20784.517578, 157650.4375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2277, 7, 0, 474, 98490.140625, 20685.367188, 173564.1875, 2404, 98483.25, 20667.945313, 173769.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2278, 7, 0, 474, 100096.398438, 20707.113281, 172845.015625, 2404, 100182.101563, 20707.064453, 172831.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2279, 7, 0, 474, 102535.828125, 20716.601563, 171710.5, 2404, 102545.070313, 20714.472656, 171573.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2280, 7, 0, 474, 103953.734375, 20737.041016, 170782.75, 2404, 104057.085938, 20708.640625, 170684.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2281, 7, 0, 474, 105614.984375, 20750.707031, 169952.265625, 2404, 105784.734375, 20731.998047, 169931.40625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2282, 7, 0, 474, 107161.882813, 20758.152344, 168439.09375, 2404, 107394.570313, 20762.328125, 168387.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2283, 7, 0, 474, 108012.546875, 20740.339844, 167782.90625, 2404, 108197.054688, 20746.542969, 167852.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2284, 7, 0, 474, 109171.484375, 20722.294922, 166755.015625, 2404, 109208.359375, 20729.181641, 166872.359375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2285, 7, 0, 474, 109923.710938, 20779.904297, 165078.171875, 2404, 110124.78125, 20769.466797, 164973.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2286, 7, 0, 474, 98340.625, 20707.640625, 172416.109375, 2404, 98119.0, 20706.976563, 172480.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2287, 7, 0, 474, 101839.703125, 20707.859375, 171184.015625, 2404, 101878.695313, 20707.757813, 171087.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2288, 7, 0, 474, 106266.09375, 20742.980469, 168500.375, 2404, 106289.140625, 20741.513672, 168389.078125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2289, 7, 0, 474, 107198.375, 20760.949219, 167485.625, 2404, 107268.976563, 20760.878906, 167343.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2290, 7, 0, 474, 92634.085938, 20601.582031, 169180.4375, 2404, 92555.070313, 20585.273438, 169258.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2291, 7, 0, 474, 93403.414063, 20374.257813, 172020.0625, 2404, 93389.867188, 20378.609375, 171887.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2292, 7, 0, 474, 93395.515625, 20267.427734, 173090.328125, 2404, 93465.273438, 20257.09375, 173242.15625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2293, 7, 0, 474, 94081.625, 20671.029297, 169611.140625, 2404, 94130.328125, 20683.802734, 169746.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2294, 7, 0, 474, 94344.421875, 20406.951172, 172571.65625, 2404, 94403.914063, 20388.371094, 172731.25);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2295, 7, 0, 474, 103162.359375, 19797.482422, 164739.125, 2404, 103040.460938, 19792.183594, 164771.265625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2296, 7, 0, 474, 102033.109375, 19850.25, 164329.78125, 2404, 101938.789063, 19883.232422, 164219.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2297, 7, 0, 474, 102754.296875, 19792.945313, 163894.734375, 2404, 102633.890625, 19812.009766, 163826.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2298, 7, 0, 474, 102952.820313, 19759.769531, 160230.15625, 2404, 102861.320313, 19759.224609, 160139.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2299, 7, 0, 474, 102371.664063, 19754.201172, 161050.75, 2404, 102266.085938, 19739.433594, 161000.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2300, 7, 0, 474, 101586.289063, 19730.554688, 160162.0, 2404, 101501.570313, 19723.890625, 160256.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2301, 7, 0, 474, 101077.3125, 19925.408203, 163991.40625, 2404, 101006.210938, 19948.564453, 163898.765625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2302, 7, 0, 474, 100241.625, 19880.214844, 161980.890625, 2404, 100122.140625, 19900.503906, 161971.625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2303, 7, 0, 474, 100339.71875, 19887.902344, 164578.09375, 2404, 100256.335938, 19933.132813, 164550.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2304, 7, 0, 474, 100321.023438, 19939.138672, 163851.015625, 2404, 100201.390625, 19947.46875, 163847.90625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2305, 7, 0, 474, 99660.976563, 19907.386719, 162598.4375, 2404, 99608.679688, 19930.841797, 162725.421875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2306, 7, 0, 474, 99271.632813, 19835.826172, 161471.890625, 2404, 99393.445313, 19861.195313, 161476.890625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2307, 7, 0, 474, 100896.1875, 19812.650391, 167979.421875, 2404, 100760.820313, 19801.699219, 168086.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2308, 7, 0, 474, 100237.3125, 19815.105469, 167364.90625, 2404, 100058.695313, 19812.833984, 167278.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2309, 7, 0, 474, 101368.453125, 19807.40625, 167314.6875, 2404, 101504.078125, 19802.832031, 167196.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2310, 7, 0, 474, 97464.179688, 19774.355469, 167828.796875, 2404, 97384.8125, 19778.876953, 167727.109375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2311, 7, 0, 474, 97705.898438, 19774.726563, 166935.421875, 2404, 97591.90625, 19775.015625, 167053.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2312, 7, 0, 474, 98206.664063, 19806.599609, 165464.890625, 2404, 98308.90625, 19809.369141, 165317.984375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2313, 7, 0, 474, 97968.5625, 19807.375, 164318.875, 2404, 97866.539063, 19804.958984, 164247.296875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2314, 7, 0, 474, 96877.984375, 19807.75, 166509.671875, 2404, 96817.09375, 19787.419922, 166649.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2315, 7, 0, 474, 97175.289063, 19776.4375, 165244.875, 2404, 97111.4375, 19773.773438, 165088.9375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2316, 7, 0, 474, 96402.539063, 19742.189453, 164868.53125, 2404, 96391.054688, 19731.8125, 164753.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2317, 7, 0, 474, 96597.054688, 19746.238281, 163629.421875, 2404, 96519.859375, 19732.332031, 163802.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2318, 7, 0, 474, 97426.6875, 19776.96875, 162500.671875, 2404, 97428.453125, 19777.753906, 162339.515625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2319, 7, 0, 474, 97798.9375, 19788.935547, 161571.09375, 2404, 97726.585938, 19784.335938, 161459.46875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2320, 7, 0, 474, 95448.757813, 19627.021484, 164349.734375, 2404, 95446.796875, 19733.0, 164215.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2321, 7, 0, 474, 96752.890625, 19775.1875, 161775.671875, 2404, 96635.21875, 19773.972656, 161832.375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2322, 7, 0, 474, 111185.4375, 20831.722656, 163660.140625, 2404, 111170.007813, 20856.109375, 163539.578125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2323, 7, 0, 474, 110801.53125, 20841.9375, 162597.09375, 2404, 110701.085938, 20856.916016, 162567.28125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2324, 7, 0, 474, 110240.851563, 20838.642578, 163393.421875, 2404, 110142.492188, 20839.021484, 163259.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2325, 7, 0, 122, 34622.820313, 19886.486328, 120159.078125, 2402, 34907.679688, 19903.933594, 120376.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2326, 7, 0, 122, 34884.039063, 19835.560547, 122356.804688, 2402, 35024.558594, 19894.439453, 121990.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2327, 7, 0, 122, 36513.589844, 20797.976563, 127674.15625, 2402, 36449.894531, 20796.646484, 127197.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2328, 7, 0, 122, 36761.859375, 20797.890625, 129451.304688, 2402, 36983.40625, 20800.320313, 129199.648438);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2329, 7, 0, 122, 37702.117188, 20798.876953, 134849.109375, 2402, 37630.386719, 20794.441406, 134486.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2330, 7, 0, 122, 37273.097656, 20816.027344, 136758.140625, 2402, 37350.066406, 20808.597656, 136425.546875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2331, 7, 0, 122, 37616.304688, 20795.775391, 141648.203125, 2402, 37414.992188, 20790.871094, 141379.0);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2332, 7, 0, 122, 36272.835938, 20785.355469, 143620.46875, 2402, 36106.734375, 20785.800781, 143355.21875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2333, 7, 0, 122, 37889.277344, 20793.773438, 147045.171875, 2402, 37789.632813, 20796.115234, 147317.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2334, 7, 0, 122, 39777.621094, 20770.652344, 148183.65625, 2402, 40124.644531, 20856.707031, 147982.046875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2335, 7, 0, 122, 42237.8125, 20380.1875, 152328.171875, 2402, 42663.421875, 20370.574219, 152264.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2336, 7, 0, 122, 37846.253906, 20506.414063, 152288.71875, 2402, 37856.484375, 20478.046875, 152662.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2337, 7, 0, 122, 32679.191406, 20115.076172, 155067.953125, 2402, 32756.917969, 20120.417969, 154745.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2338, 7, 0, 122, 29829.773438, 19974.367188, 157114.1875, 2402, 29688.357422, 19971.003906, 157498.203125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2339, 7, 0, 122, 39358.648438, 20303.484375, 154515.171875, 2402, 39554.90625, 20259.644531, 154659.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2340, 7, 0, 122, 32609.099609, 19948.332031, 158116.625, 2402, 32917.664063, 19939.339844, 158211.0625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2341, 7, 0, 122, 27939.712891, 19871.339844, 166156.140625, 2402, 28171.052734, 19880.910156, 166121.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2342, 7, 0, 122, 30217.609375, 19959.146484, 167597.375, 2402, 30551.974609, 19921.109375, 167574.03125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2343, 7, 0, 122, 34207.417969, 20212.257813, 172375.078125, 2402, 34198.558594, 20220.763672, 172053.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2344, 7, 0, 122, 35609.980469, 20227.685547, 175092.390625, 2402, 35232.421875, 20251.742188, 175274.140625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2345, 7, 0, 122, 37239.03125, 20408.751953, 172162.046875, 2402, 37333.488281, 20463.453125, 171886.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2346, 7, 0, 122, 40029.03125, 20237.972656, 175433.015625, 2402, 39612.105469, 20240.972656, 175716.796875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2347, 7, 0, 122, 42250.582031, 20454.208984, 173380.609375, 2402, 42650.488281, 20478.878906, 173319.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2348, 7, 0, 122, 43875.210938, 20357.722656, 176414.625, 2402, 44277.617188, 20399.470703, 176259.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2349, 7, 0, 122, 43671.441406, 20129.345703, 178716.3125, 2402, 44131.972656, 20151.855469, 178750.5);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2350, 7, 0, 122, 53848.203125, 18802.794922, 158976.0625, 2402, 53681.085938, 18784.845703, 159303.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2351, 7, 0, 122, 56053.019531, 18920.423828, 163664.40625, 2402, 55757.335938, 18932.529297, 163924.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2352, 7, 0, 122, 56883.71875, 19263.705078, 168066.40625, 2402, 56502.558594, 19318.0625, 167975.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2353, 7, 0, 122, 55023.800781, 18812.482422, 172367.0625, 2402, 55326.386719, 18901.523438, 172585.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2354, 7, 0, 122, 56758.539063, 18879.425781, 160200.28125, 2402, 56888.539063, 18916.207031, 159782.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2355, 7, 0, 122, 59072.101563, 19035.777344, 165696.6875, 2402, 59456.769531, 19027.695313, 165991.34375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2356, 7, 0, 122, 60044.429688, 19045.001953, 169785.953125, 2402, 60373.484375, 19045.427734, 169549.75);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2357, 7, 0, 122, 46661.851563, 16533.751953, 164156.28125, 2402, 46286.007813, 16592.152344, 164309.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2358, 7, 0, 122, 45889.632813, 16597.755859, 166489.890625, 2402, 45592.703125, 16593.994141, 166775.3125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2359, 7, 0, 122, 48683.179688, 16665.798828, 168210.4375, 2402, 49057.230469, 16627.90625, 168017.71875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2360, 7, 0, 122, 47926.859375, 16858.349609, 170253.9375, 2402, 48135.011719, 16930.980469, 170673.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2361, 7, 0, 122, 34036.277344, 16609.042969, 162583.75, 2402, 34412.839844, 16598.125, 162608.171875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2362, 7, 0, 122, 35938.5, 16594.617188, 166419.671875, 2402, 36320.851563, 16595.658203, 166532.65625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2363, 7, 0, 122, 37110.59375, 16594.0, 163431.34375, 2402, 37375.246094, 16594.09375, 163152.453125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2364, 7, 0, 122, 39453.964844, 16595.0, 162165.578125, 2402, 39295.730469, 16595.0, 162325.84375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2365, 7, 0, 122, 39092.414063, 16594.0, 165017.859375, 2402, 39049.976563, 16594.0, 164676.015625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2366, 7, 0, 416, 34817.109375, 19956.205078, 118414.023438, 2403, 34575.984375, 19978.851563, 117731.921875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2367, 7, 0, 416, 35650.007813, 20402.669922, 124348.5625, 2403, 36371.285156, 20700.792969, 124574.429688);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2368, 7, 0, 416, 37017.433594, 20796.996094, 131080.765625, 2403, 37488.703125, 20797.0, 131638.5625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2369, 7, 0, 416, 37648.359375, 20794.140625, 143839.796875, 2403, 37789.558594, 20802.755859, 143029.328125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2370, 7, 0, 416, 40688.386719, 20476.822266, 152069.359375, 2403, 40527.4375, 20532.884766, 151248.09375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2371, 7, 0, 416, 31605.054688, 19947.703125, 156681.53125, 2403, 32102.382813, 20020.464844, 156705.96875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2372, 7, 0, 416, 29750.667969, 20057.023438, 169243.25, 2403, 30244.587891, 20075.056641, 169696.609375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2373, 7, 0, 416, 35652.402344, 20155.921875, 173614.25, 2403, 36098.53125, 20157.806641, 173833.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2374, 7, 0, 416, 42423.097656, 20278.246094, 176127.890625, 2403, 42110.640625, 20299.443359, 175544.59375);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2375, 7, 0, 416, 35535.449219, 16594.074219, 164566.015625, 2403, 35150.410156, 16596.564453, 164832.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2376, 7, 0, 416, 41372.617188, 16593.992188, 164212.09375, 2403, 41588.378906, 16593.998047, 164872.6875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2377, 7, 0, 416, 47183.214844, 16656.59375, 167543.65625, 2403, 47025.441406, 16663.105469, 168210.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2378, 7, 0, 416, 50537.578125, 16859.115234, 169625.1875, 2403, 50209.710938, 16924.488281, 170120.78125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2379, 7, 0, 416, 53703.601563, 18286.275391, 172802.09375, 2403, 53024.824219, 18050.095703, 173087.640625);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2380, 7, 0, 416, 52336.550781, 18868.660156, 157252.28125, 2403, 53216.507813, 18862.136719, 157129.875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2381, 7, 0, 416, 55360.300781, 18793.705078, 160428.953125, 2403, 55128.476563, 18839.396484, 161144.671875);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2382, 7, 0, 416, 57214.609375, 19001.910156, 164884.734375, 2403, 56846.996094, 19054.933594, 165494.703125);
GO

INSERT INTO [dbo].[TblFieldEventMonsterList] ([mKey], [mEventType], [mEventMonType], [mOrgMonMid], [mOrgMonPosX], [mOrgMonPosY], [mOrgMonPosZ], [mEventMonMid], [mEventMonPosX], [mEventMonPosY], [mEventMonPosZ]) VALUES (2383, 7, 0, 416, 58500.875, 19099.390625, 169090.703125, 2403, 59203.851563, 19043.822266, 168308.859375);
GO

