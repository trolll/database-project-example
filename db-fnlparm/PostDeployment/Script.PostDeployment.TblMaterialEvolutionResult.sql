/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMaterialEvolutionResult
Date                  : 2023-10-07 09:08:51
*/


INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (1, 40006, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (1, 40036, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (1, 40066, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (1, 40096, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (2, 40036, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (2, 40066, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (2, 40096, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (3, 40066, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (3, 40096, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (4, 40096, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (5, 40012, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (5, 40042, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (5, 40072, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (5, 40102, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (6, 40042, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (6, 40072, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (6, 40102, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (7, 40072, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (7, 40102, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (8, 40102, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (9, 40018, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (9, 40048, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (9, 40078, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (9, 40108, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (10, 40048, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (10, 40078, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (10, 40108, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (11, 40078, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (11, 40108, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (12, 40108, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (13, 40024, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (13, 40054, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (13, 40084, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (13, 40114, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (14, 40054, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (14, 40084, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (14, 40114, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (15, 40084, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (15, 40114, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (16, 40114, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (17, 40126, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (17, 40156, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (17, 40186, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (17, 40216, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (18, 40156, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (18, 40186, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (18, 40216, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (19, 40186, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (19, 40216, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (20, 40216, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (21, 40132, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (21, 40162, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (21, 40192, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (21, 40222, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (22, 40162, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (22, 40192, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (22, 40222, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (23, 40192, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (23, 40222, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (24, 40222, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (25, 40138, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (25, 40168, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (25, 40198, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (25, 40228, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (26, 40168, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (26, 40198, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (26, 40228, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (27, 40198, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (27, 40228, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (28, 40228, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (29, 40144, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (29, 40174, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (29, 40204, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (29, 40234, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (30, 40174, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (30, 40204, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (30, 40234, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (31, 40204, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (31, 40234, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (32, 40234, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (33, 40246, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (33, 40276, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (33, 40306, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (33, 40336, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (34, 40276, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (34, 40306, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (34, 40336, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (35, 40306, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (35, 40336, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (36, 40336, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (37, 40252, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (37, 40282, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (37, 40312, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (37, 40342, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (38, 40282, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (38, 40312, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (38, 40342, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (39, 40312, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (39, 40342, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (40, 40342, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (41, 40258, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (41, 40288, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (41, 40318, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (41, 40348, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (42, 40288, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (42, 40318, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (42, 40348, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (43, 40318, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (43, 40348, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (44, 40348, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (45, 40264, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (45, 40294, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (45, 40324, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (45, 40354, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (46, 40294, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (46, 40324, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (46, 40354, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (47, 40324, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (47, 40354, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (48, 40354, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (49, 40366, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (49, 40396, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (49, 40426, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (49, 40456, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (50, 40396, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (50, 40426, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (50, 40456, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (51, 40426, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (51, 40456, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (52, 40456, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (53, 40372, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (53, 40402, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (53, 40432, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (53, 40462, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (54, 40402, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (54, 40432, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (54, 40462, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (55, 40432, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (55, 40462, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (56, 40462, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (57, 40378, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (57, 40408, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (57, 40438, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (57, 40468, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (58, 40408, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (58, 40438, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (58, 40468, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (59, 40438, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (59, 40468, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (60, 40468, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (61, 40384, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (61, 40414, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (61, 40444, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (61, 40474, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (62, 40414, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (62, 40444, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (62, 40474, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (63, 40444, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (63, 40474, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (64, 40474, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (65, 40486, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (65, 40516, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (65, 40546, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (65, 40576, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (66, 40516, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (66, 40546, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (66, 40576, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (67, 40546, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (67, 40576, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (68, 40576, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (69, 40492, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (69, 40522, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (69, 40552, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (69, 40582, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (70, 40522, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (70, 40552, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (70, 40582, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (71, 40552, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (71, 40582, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (72, 40582, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (73, 40498, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (73, 40528, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (73, 40558, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (73, 40588, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (74, 40528, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (74, 40558, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (74, 40588, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (75, 40558, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (75, 40588, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (76, 40588, 100.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (77, 40504, 55.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (77, 40534, 25.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (77, 40564, 15.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (77, 40594, 5.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (78, 40534, 70.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (78, 40564, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (78, 40594, 10.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (79, 40564, 80.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (79, 40594, 20.0);
GO

INSERT INTO [dbo].[TblMaterialEvolutionResult] ([MEID], [IID], [mPercent]) VALUES (80, 40594, 100.0);
GO

