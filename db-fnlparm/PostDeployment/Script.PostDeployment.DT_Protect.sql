/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Protect
Date                  : 2023-10-07 09:07:31
*/


INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (8, 1, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (9, 1, 2, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (10, 1, 3, 4, 4, 4, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (11, 2, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (12, 2, 2, 1, 1, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (13, 2, 3, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (14, 3, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (15, 3, 2, 1, 1, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (16, 3, 3, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (17, 4, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (18, 4, 2, 1, 1, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (19, 4, 3, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (20, 5, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (21, 5, 2, 1, 1, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (22, 5, 3, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (23, 6, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (24, 6, 2, 1, 1, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (25, 6, 3, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (26, 7, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (27, 7, 2, 1, 1, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (28, 7, 3, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (29, 9, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (30, 9, 2, 1, 1, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (31, 9, 3, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (32, 8, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (33, 8, 2, 1, 1, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (34, 8, 3, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (39, 1, 4, 3, 3, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (40, 1, 5, 4, 4, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (41, 1, 6, 5, 5, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (45, 9, 4, 3, 3, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (46, 9, 5, 4, 4, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (47, 9, 6, 5, 5, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (48, 13, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (49, 13, 2, 2, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (57, 13, 51, 1, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (58, 13, 52, 2, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (59, 13, 53, 0, 0, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (60, 13, 54, 0, 0, 2, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (61, 13, 55, 0, 1, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (62, 13, 56, 0, 2, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (63, 10, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (64, 10, 2, 3, 3, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (65, 10, 3, 5, 5, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (66, 10, 4, 7, 7, 7, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (67, 10, 5, 10, 10, 10, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (68, 11, 1, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (69, 11, 2, 3, 3, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (70, 11, 3, 5, 5, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (71, 11, 4, 7, 7, 7, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (72, 11, 5, 10, 10, 10, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (73, 12, 1, 5, 5, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (50, 13, 3, 3, 3, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (51, 13, 4, 4, 4, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (52, 13, 5, 5, 5, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (53, 13, 6, 6, 6, 6, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (54, 13, 7, 7, 7, 7, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (55, 13, 8, 9, 9, 9, 8, 8, 8);
GO

INSERT INTO [dbo].[DT_Protect] ([SID], [SType], [SLevel], [SDPV], [SMPV], [SRPV], [SDDV], [SMDV], [SRDV]) VALUES (56, 13, 9, 11, 11, 11, 9, 9, 9);
GO

