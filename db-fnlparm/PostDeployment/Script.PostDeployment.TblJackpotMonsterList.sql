/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblJackpotMonsterList
Date                  : 2023-10-07 09:08:57
*/


INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2454, 0, 0, 0);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2455, 0, 0, 0);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2456, 0, 0, 2);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2457, 0, 0, 1);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2458, 1, 0, 0);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2459, 1, 0, 0);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2460, 1, 0, 2);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2461, 1, 0, 1);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2498, 0, 1, 0);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2499, 0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2500, 0, 1, 2);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2501, 0, 1, 0);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2502, 1, 1, 0);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2503, 1, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2504, 1, 1, 0);
GO

INSERT INTO [dbo].[TblJackpotMonsterList] ([mMID], [mCamp], [mLevel], [mPoint]) VALUES (2505, 1, 1, 2);
GO

