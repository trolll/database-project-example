/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_SkillCastingDelayGroup
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (0, '설정되지 않은 스킬류');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (1, '공격형 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (2, '강화형 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (3, '마법 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (4, '음식, 포션류');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (5, '완드');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (6, '스크롤');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (7, '스킬 트리 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (8, '아이템 장착');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (9, '딜레이가 없는 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (10, '스운형 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (11, '마법 이뮤니티형 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (12, '마나번 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (13, '디스펠 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (14, '웹 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (15, '저주형 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (16, '포이즌 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (17, '엘리멘탈 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (18, '마법공격 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (19, '토템(공방) 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (20, '토템(라이프) 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (21, '토템(오리에드) 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (22, '토템(커버링) 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (23, '소환수 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (24, '토템(스피드) 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (25, '세인트 아머');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (26, '데스 그랩');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (27, '서번트 친밀도 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (28, '서번트 스킬 트리');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (29, '서번트 모임 스킬');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (30, '서번트 어뷰징 방지');
GO

INSERT INTO [dbo].[TP_SkillCastingDelayGroup] ([SGroupNo], [SDesc]) VALUES (31, '서번트 합성 스킬');
GO

