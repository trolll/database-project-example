/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Slain
Date                  : 2023-10-07 09:08:46
*/


INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (10, 1, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (11, 1, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (12, 1, 3, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (13, 2, 1, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (14, 2, 2, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (15, 2, 3, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (16, 3, 1, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (17, 3, 2, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (18, 3, 3, 5, 3, 5, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (19, 4, 1, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (20, 4, 2, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (21, 4, 3, 5, 3, 5, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (22, 5, 1, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (23, 5, 2, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (24, 5, 3, 5, 3, 5, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (25, 6, 1, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (26, 6, 2, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (27, 6, 3, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (28, 9, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (29, 13, 1, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (30, 13, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (31, 13, 3, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (32, 13, 4, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (33, 13, 5, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (34, 13, 6, 6, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (35, 13, 7, 7, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (36, 13, 8, 8, 9, 8, 9);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (37, 13, 9, 9, 11, 9, 11);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (38, 9, 2, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (39, 9, 3, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (40, 13, 10, 10, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (41, 13, 20, 10, 20, 10, 20);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (42, 13, 40, 10, 40, 10, 40);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (43, 13, 51, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (44, 13, 52, 0, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (45, 13, 53, 0, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (46, 13, 54, 0, 0, 0, 2);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (47, 13, 55, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (48, 13, 56, 2, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (49, 13, 57, 0, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (50, 13, 58, 0, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (51, 13, 59, 0, 3, 0, 3);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (52, 13, 60, 0, 4, 0, 4);
GO

INSERT INTO [dbo].[DT_Slain] ([SID], [SType], [SLevel], [SHitPlus], [SDDPlus], [SRDDPlus], [SRHitPlus]) VALUES (53, 12, 1, 5, 5, 5, 5);
GO

