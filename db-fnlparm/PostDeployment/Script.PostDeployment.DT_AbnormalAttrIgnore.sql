/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AbnormalAttrIgnore
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (48, 308, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (49, 346, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (50, 347, 1, 46, 0, '드라코 탑승 중 블러디 뱀파 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (51, 360, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (52, 361, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (53, 362, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (54, 363, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (55, 483, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (56, 484, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (57, 259, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (58, 69, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (59, 188, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (60, 189, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (61, 6, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (62, 167, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (63, 168, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (64, 297, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (65, 298, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (66, 201, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (67, 204, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (68, 7, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (69, 474, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (70, 180, 1, 46, 0, '드라코 탑승중 버서크 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (71, 242, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (72, 236, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (73, 240, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (74, 87, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (75, 236, 1, 38, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (76, 236, 1, 95, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (77, 240, 1, 38, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (78, 240, 1, 95, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (79, 87, 1, 95, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (80, 87, 1, 92, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (81, 240, 2, 94, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (85, 237, 2, 38, 1, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (86, 515, 2, 38, 1, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (87, 516, 2, 38, 1, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (88, 517, 2, 38, 1, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (89, 237, 2, 94, 1, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (90, 515, 2, 94, 1, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (91, 516, 2, 94, 1, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (92, 517, 2, 94, 1, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (93, 508, 1, 95, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (94, 509, 1, 95, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (95, 510, 1, 95, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (96, 511, 1, 95, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (97, 508, 1, 92, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (98, 509, 1, 92, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (99, 510, 1, 92, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (100, 511, 1, 92, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (114, 505, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (115, 506, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (116, 107, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (117, 173, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (118, 207, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (119, 309, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (120, 317, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (121, 318, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (122, 319, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (123, 485, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (124, 500, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (125, 508, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (126, 509, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (127, 510, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (128, 511, 1, 46, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (129, 489, 1, 46, 0, '드라코 중인 상태에서 레비테이션(489) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (135, 551, 1, 46, 0, '드라코 탑승 중인 상태에서 캡틴 라이칸 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (136, 552, 1, 46, 0, '드라코 탑승 중인 상태에서 티어스 윗치 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (137, 553, 1, 46, 0, '드라코 탑승 중인 상태에서 프린세스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (138, 61, 1, 130, 0, '커버링 토템 중인 상태에서 물약파괴 1렙(61) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (139, 62, 1, 130, 0, '커버링 토템 중인 상태에서 물약파괴 2렙(62) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (140, 63, 1, 130, 0, '커버링 토템 중인 상태에서 물약파괴 3렙(63) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (141, 64, 1, 130, 0, '커버링 토템 중인 상태에서 물약파괴 4렙(64) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (142, 523, 1, 46, 0, '드라코 탑승 중에 타임디스(523)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (144, 488, 1, 123, 0, '파워워드러스트상태일때 아머뷁(488)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (145, 572, 1, 46, 0, '드라코 탑승 중인 상태에서 체인 인큐버스(572)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (148, 544, 1, 136, 0, '미티어스웜(둔감화)상태에서 둔감화(544)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (149, 543, 1, 136, 0, '미티어스웜(둔감화)상태에서 무력감(543)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (150, 545, 1, 136, 0, '미티어스웜(둔감화)상태에서 정신충격(545)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (151, 546, 1, 136, 0, '미티어스웜(둔감화)상태에서 혼돈(546)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (152, 547, 1, 136, 0, '미티어스웜(둔감화)상태에서 혼란(547)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (153, 544, 1, 135, 0, '미티어스웜(무력감)상태에서 둔감화(544)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (154, 543, 1, 135, 0, '미티어스웜(무력감)상태에서 무력감(543)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (155, 545, 1, 135, 0, '미티어스웜(무력감)상태에서 정신충격(545)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (156, 546, 1, 135, 0, '미티어스웜(무력감)상태에서 혼돈(546)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (157, 547, 1, 135, 0, '미티어스웜(무력감)상태에서 혼란(547)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (158, 544, 1, 137, 0, '미티어스웜(정신충격)상태에서 둔감화(544)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (159, 543, 1, 137, 0, '미티어스웜(정신충격)상태에서 무력감(543)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (160, 545, 1, 137, 0, '미티어스웜(정신충격)상태에서 정신충격(545)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (161, 546, 1, 137, 0, '미티어스웜(정신충격)상태에서 혼돈(546)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (162, 547, 1, 137, 0, '미티어스웜(정신충격)상태에서 혼란(547)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (163, 544, 1, 139, 0, '미티어스웜(혼돈)상태에서 둔감화(544)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (164, 543, 1, 139, 0, '미티어스웜(혼돈)상태에서 무력감(543)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (165, 545, 1, 139, 0, '미티어스웜(혼돈)상태에서 정신충격(545)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (166, 546, 1, 139, 0, '미티어스웜(혼돈)상태에서 혼돈(546)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (167, 547, 1, 139, 0, '미티어스웜(혼돈)상태에서 혼란(547)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (168, 544, 1, 138, 0, '미티어스웜(혼란)상태에서 둔감화(544)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (169, 543, 1, 138, 0, '미티어스웜(혼란)상태에서 무력감(543)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (170, 545, 1, 138, 0, '미티어스웜(혼란)상태에서 정신충격(545)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (171, 546, 1, 138, 0, '미티어스웜(혼란)상태에서 혼돈(546)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (172, 547, 1, 138, 0, '미티어스웜(혼란)상태에서 혼란(547)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (173, 573, 1, 46, 0, '드라코 탑승 중인 상태에서 인퍼날 (II) (573)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (174, 574, 1, 46, 0, '드라코 탑승 중인 상태에서 인퍼날 (III) (574)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (175, 575, 1, 46, 0, '드라코 탑승 중인 상태에서 인퍼날 (IV) (575)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (176, 576, 1, 46, 0, '드라코 탑승 중인 상태에서 렉스 서큐버스(II)(576)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (177, 577, 1, 46, 0, '드라코 탑승 중인 상태에서 렉스 서큐버스(III)(577)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (178, 578, 1, 46, 0, '드라코 탑승 중인 상태에서 렉스 서큐버스(IV)(578)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (179, 579, 1, 46, 0, '드라코 탑승 중인 상태에서 블러디 뱀파이어(II)(579)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (180, 580, 1, 46, 0, '드라코 탑승 중인 상태에서 블러디 뱀파이어(III)(580)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (181, 581, 1, 46, 0, '드라코 탑승 중인 상태에서 블러디 뱀파이어(IV)(581)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (182, 582, 1, 46, 0, '드라코 탑승 중인 상태에서 캐노네이드 키틴(II)(582)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (183, 583, 1, 46, 0, '드라코 탑승 중인 상태에서 캐노네이드 키틴(III)(583)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (184, 584, 1, 46, 0, '드라코 탑승 중인 상태에서 캐노네이드 키틴(IV)(584)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (185, 585, 1, 46, 0, '드라코 탑승 중인 상태에서 문라이트 네메시스(改)(585)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (186, 586, 1, 46, 0, '드라코 탑승 중인 상태에서 문라이트 네메시스(零式)(586)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (187, 587, 1, 46, 0, '드라코 탑승 중인 상태에서 스핀들 네메시스(改)(587)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (188, 588, 1, 46, 0, '드라코 탑승 중인 상태에서 스핀들 네메시스(零式)(588)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (189, 589, 1, 46, 0, '드라코 탑승 중인 상태에서 티어스 윗치(改)(589)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (190, 590, 1, 46, 0, '드라코 탑승 중인 상태에서 캡틴 라이칸(改)(590)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (191, 591, 1, 46, 0, '드라코 탑승 중인 상태에서 프린세스(改)(591)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (192, 592, 1, 46, 0, '드라코 탑승 중인 상태에서 프린세스(零式)(592)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (193, 593, 1, 46, 0, '드라코 탑승 중인 상태에서 프린세스(일반형)(593)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (194, 711, 1, 46, 0, '드라코 탑승 중인 상태에서 큐티 프리스트 (711)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (195, 712, 1, 46, 0, '드라코 탑승 중인 상태에서 수련소녀 (712)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (196, 713, 1, 46, 0, '드라코 탑승 중인 상태에서 수련소녀(II)(713)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (197, 714, 1, 46, 0, '드라코 탑승 중인 상태에서 수련소녀(III)(714)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (198, 715, 1, 46, 0, '드라코 탑승 중인 상태에서 수련소녀(IV)(715)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (199, 716, 1, 46, 0, '드라코 탑승 중인 상태에서 큐티 프리스트(II)(716)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (200, 717, 1, 46, 0, '드라코 탑승 중인 상태에서 큐티 프리스트(III)(717)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (201, 718, 1, 46, 0, '드라코 탑승 중인 상태에서 큐티 프리스트(IV)(718)무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (203, 730, 1, 46, 0, '드라코 탑승 중인 상태에서 소라게 변신(730) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (204, 791, 1, 46, 0, '드라코 탑승 중인 상태에서 카사 변신(791) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (205, 792, 1, 46, 0, '드라코 탑승 중인 상태에서 실라페 변신(792) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (206, 793, 1, 46, 0, '드라코 탑승 중인 상태에서 노움 변신(793) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (207, 794, 1, 46, 0, '드라코 탑승 중인 상태에서  언딘(794)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (209, 796, 1, 46, 0, '드라코 탑승 중인 상태에서  노임(796)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (210, 797, 1, 46, 0, '드라코 탑승 중인 상태에서  실프(797)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (211, 798, 1, 46, 0, '드라코 탑승 중인 상태에서  이그니스(798)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (212, 799, 1, 46, 0, '드라코 탑승 중인 상태에서  실라이론(799)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (213, 800, 1, 46, 0, '드라코 탑승 중인 상태에서  노이아넨(800)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (214, 801, 1, 46, 0, '드라코 탑승 중인 상태에서  운다임(801)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (215, 802, 1, 46, 0, '드라코 탑승 중인 상태에서  이프리트(802)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (216, 803, 1, 46, 0, '드라코 탑승 중인 상태에서  슈리엘(803)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (218, 804, 1, 46, 0, '드라코 탑승 중인 상태에서  노아스(804)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (219, 805, 1, 46, 0, '드라코 탑승 중인 상태에서  엘라임(805)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (220, 806, 1, 46, 0, '드라코 탑승 중인 상태에서  퓨리(806)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (221, 807, 1, 46, 0, '드라코 탑승 중인 상태에서  레프리컨(807)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (222, 808, 1, 46, 0, '드라코 탑승 중인 상태에서  시아페(808)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (223, 809, 1, 46, 0, '드라코 탑승 중인 상태에서  레이커(809)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (224, 810, 1, 46, 0, '드라코 탑승 중인 상태에서  휘카셀(810)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (225, 811, 1, 46, 0, '드라코 탑승 중인 상태에서  에리스(811)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (226, 812, 1, 46, 0, '드라코 탑승 중인 상태에서  릴리언스(812)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (227, 813, 1, 46, 0, '드라코 탑승 중인 상태에서  에리세드(813)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (228, 814, 1, 46, 0, '드라코 탑승 중인 상태에서  운디네(814)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (229, 795, 1, 46, 0, '드라코 탑승 중인 상태에서  사라만다(795)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (230, 732, 1, 46, 0, '드라코 탑승 중인 상태에서 이프리트(732)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (231, 733, 1, 46, 0, '드라코 탑승 중인 상태에서 퓨리(733)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (232, 734, 1, 46, 0, '드라코 탑승 중인 상태에서 휠카셀(734)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (233, 735, 1, 46, 0, '드라코 탑승 중인 상태에서 슈리엘(735)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (234, 736, 1, 46, 0, '드라코 탑승 중인 상태에서 레프리컨(736)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (235, 737, 1, 46, 0, '드라코 탑승 중인 상태에서 에리스(737)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (236, 738, 1, 46, 0, '드라코 탑승 중인 상태에서 노아스(738)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (237, 739, 1, 46, 0, '드라코 탑승 중인 상태에서 시아페(739)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (238, 740, 1, 46, 0, '드라코 탑승 중인 상태에서 릴리언스(740)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (239, 741, 1, 46, 0, '드라코 탑승 중인 상태에서 엘라임(741)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (240, 742, 1, 46, 0, '드라코 탑승 중인 상태에서 레이커(742)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (241, 743, 1, 46, 0, '드라코 탑승 중인 상태에서 에리세드(743)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (242, 818, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 전사 (IV) (818)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (243, 819, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 전사 (III) (819)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (244, 820, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 전사 (II) (820)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (245, 821, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 전사 (821)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (246, 822, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 마법사(IV) (822)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (247, 823, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 마법사(III)(823)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (248, 824, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 마법사(II) (824)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (249, 825, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 마법사 (825)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (250, 826, 1, 46, 0, '드라코 탑승 중인 상태에서 큐티 프리스트 (V) (826)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (251, 827, 1, 46, 0, '드라코 탑승 중인 상태에서 수련소녀 (V) (827) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (252, 828, 1, 46, 0, '드라코 탑승 중인 상태에서 홀리 소서리스 (828) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (253, 829, 1, 46, 0, '드라코 탑승 중인 상태에서 바바리안 (829)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (254, 830, 1, 46, 0, '드라코 탑승 중인 상태에서 다크 메이지 (830)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (255, 831, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 전사 (V) (831)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (256, 832, 1, 46, 0, '드라코 탑승 중인 상태에서 문엘프 마법사 (V) (832)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (257, 834, 1, 18, 0, '변신 중 상태이상  일 때 834(드라코 탑승 상태이상)을 무시한다.', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (258, 851, 1, 46, 0, '드라코 탑승중인 상태에서 진카사(851)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (260, 852, 1, 46, 0, '드라코 탑승중인 상태에서 진실라페(852)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (261, 853, 1, 46, 0, '드라코 탑승중인 상태에서 진노움(853)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (262, 854, 1, 46, 0, '드라코 탑승중인 상태에서 진언딘(854)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (263, 855, 1, 46, 0, '드라코 탑승중인 상태에서 진사라만다(855)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (264, 856, 1, 46, 0, '드라코 탑승중인 상태에서 진실프(856)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (265, 857, 1, 46, 0, '드라코 탑승중인 상태에서 진노임(857)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (266, 858, 1, 46, 0, '드라코 탑승중인 상태에서 진운디네(858)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (267, 848, 1, 46, 0, '드라코 탑승중 상태에서 티어스윗치Lv.1(근거리)_848변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (268, 849, 1, 46, 0, '드라코 탑승중 상태에서 티어스윗치Lv.2(근거리)_848변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (269, 859, 1, 46, 0, '드라코 탑승중 상태에서 티어스윗치Lv.1(원거리)_848변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (270, 860, 1, 46, 0, '드라코 탑승중 상태에서 티어스윗치Lv.2(원거리)_848', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (271, 850, 1, 46, 0, '드라코 탑승중 상태에서 프린세스 (1일)변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (272, 912, 1, 46, 0, '드라코 탑승중 상태에서 사라만다 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (273, 913, 1, 46, 0, '드라코 탑승중 상태에서 실프 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (274, 914, 1, 46, 0, '드라코 탑승중 상태에서 노임 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (275, 915, 1, 46, 0, '드라코 탑승중 상태에서 운디네(III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (276, 916, 1, 46, 0, '드라코 탑승중 상태에서 이그니스 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (277, 917, 1, 46, 0, '드라코 탑승중 상태에서 실라이론 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (278, 918, 1, 46, 0, '드라코 탑승중 상태에서 노이아넨 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (279, 919, 1, 46, 0, '드라코 탑승중 상태에서 운다임 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (280, 922, 1, 46, 0, '드라코 탑승중 상태에서 카사 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (281, 923, 1, 46, 0, '드라코 탑승중 상태에서 실라페 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (282, 924, 1, 46, 0, '드라코 탑승중 상태에서 노움 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (283, 925, 1, 46, 0, '드라코 탑승중 상태에서 언딘 (III) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (298, 949, 1, 46, 0, '드라코 탑승중 상태에서 메테오스 쉐도우 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (299, 950, 1, 46, 0, '드라코 탑승중 상태에서 메테오스 레인저 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (300, 951, 1, 46, 0, '드라코 탑승중 상태에서 래서 아스타로트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (301, 952, 1, 46, 0, '드라코 탑승중 상태에서 래서 루시퍼 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (302, 953, 1, 46, 0, '드라코 탑승중 상태에서 그레이터 아스타로트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (303, 954, 1, 46, 0, '드라코 탑승중 상태에서 그래이터 루시퍼 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (304, 955, 1, 46, 0, '드라코 탑승중 상태에서 크루얼 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (305, 956, 1, 46, 0, '드라코 탑승중 상태에서 님로드 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (306, 957, 1, 46, 0, '드라코 탑승중 상태에서 엡솔루트 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (307, 958, 1, 46, 0, '드라코 탑승중 상태에서 베이그란트 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (308, 959, 1, 46, 0, '드라코 탑승중 상태에서드래곤나이트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (309, 960, 1, 46, 0, '드라코 탑승중 상태에서 드래곤나이트 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (310, 961, 1, 46, 0, '드라코 탑승중 상태에서 발키리 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (311, 962, 1, 46, 0, '드라코 탑승중 상태에서 엘더스팅어 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (312, 963, 1, 46, 0, '드라코 탑승중 상태에서 바룬용병대 오크대장 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (313, 964, 1, 46, 0, '드라코 탑승중 상태에서 데몬 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (314, 965, 1, 46, 0, '드라코 탑승중 상태에서 파이어 데몬 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (315, 966, 1, 46, 0, '드라코 탑승중 상태에서 플린드 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (316, 967, 1, 46, 0, '드라코 탑승중 상태에서 다크 트레이서 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (317, 968, 1, 46, 0, '드라코 탑승중 상태에서 오크 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (318, 969, 1, 46, 0, '드라코 탑승중 상태에서 코볼트 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (319, 970, 1, 46, 0, '드라코 탑승중 상태에서 용아병 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (320, 971, 1, 46, 0, '드라코 탑승중 상태에서 뱀파이어 퀸 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (321, 972, 1, 46, 0, '드라코 탑승중 상태에서 트로글다이트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (322, 973, 1, 46, 0, '드라코 탑승중 상태에서 베어런 뱀파이어 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (323, 974, 1, 46, 0, '드라코 탑승중 상태에서 가고일 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (324, 975, 1, 46, 0, '드라코 탑승중 상태에서 바바야거 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (325, 976, 1, 46, 0, '드라코 탑승중 상태에서 콜로복 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (326, 978, 1, 46, 0, '드라코 탑승중인 상태에서 타이거 변신 (女) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (327, 979, 1, 46, 0, '드라코 탑승중인 상태에서 타이거 변신 (男) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (328, 980, 1, 46, 0, '드라코 탑승중인 상태에서 건방진 토끼 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (329, 981, 1, 46, 0, '드라코 탑승중인 상태에서 적색 타이야끼 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (330, 982, 1, 46, 0, '드라코 탑승중인 상태에서 청색 타이야끼 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (332, 999, 1, 46, 0, '드라코 탑승중인 상태에서 뱅골 나이트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (333, 1000, 1, 46, 0, '드라코 탑승중인 상태에서 뱅골 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (344, 1030, 1, 46, 0, '드라코 탑승중 상태에서 길가메쉬 나이트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (345, 1031, 1, 46, 0, '드라코 탑승중 상태에서 길가메쉬 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (346, 1032, 1, 46, 0, '드라코 탑승중 상태에서 다크 나이트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (347, 1033, 1, 46, 0, '드라코 탑승중 상태에서 다크 스팅어 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (348, 1046, 1, 46, 0, '드라코 탑승중 상태에서 불타는 축구공 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (349, 1047, 1, 46, 0, '드라코 탑승중 상태에서 축축한 축구공 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (350, 1048, 1, 46, 0, '드라코 탑승중 상태에서 날렵한 축구공 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (351, 1049, 1, 46, 0, '드라코 탑승중 상태에서 단단한 축구공 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (352, 1052, 1, 46, 0, '드라코 탑승중 상태에서 축구공 변신 완드 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (353, 1051, 1, 46, 0, '드라코 탑승중 상태에서 능력강화목걸이 변신 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (354, 1440, 1, 46, 0, '드라코 탑승중 상태에서 군(II) 변신 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (357, 1575, 1, 18, 0, '변신중일때 키메라 탑승 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (358, 1576, 1, 18, 0, '변신중일때 정복의 드라코 탑승 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (360, 1301, 1, 46, 0, '드라코 탑승중인 상태에서 퓨인즈 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (361, 1900, 1, 46, 0, '드라코 탑승중 상태에서 해적 여자 바이킹 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (362, 1901, 1, 46, 0, '드라코 탑승중 상태에서 도적 총병 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (363, 1902, 1, 46, 0, '드라코 탑승중 상태에서 타락한 엘프 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (364, 1903, 1, 46, 0, '드라코 탑승중 상태에서 타락한 엘프 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (365, 1904, 1, 46, 0, '드라코 탑승중 상태에서 골드 엠페러 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (366, 1905, 1, 46, 0, '드라코 탑승중 상태에서 골드 엠페러 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (367, 1923, 1, 229, 0, '엘리멘탈 이뮤니티 상태에서 엘리멘탈 블라스트 생성 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (368, 57, 1, 242, 0, '특정 상태이상만 적용(바포메트) 상태에서는 사일런스 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (369, 58, 1, 242, 0, '특정 상태이상만 적용(바포메트) 상태에서는 사일런스 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (370, 59, 1, 242, 0, '특정 상태이상만 적용(바포메트) 상태에서는 사일런스 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (371, 60, 1, 242, 0, '특정 상태이상만 적용(바포메트) 상태에서는 사일런스 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (372, 241, 1, 242, 0, '특정 상태이상만 적용(바포메트) 상태에서는 스운 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (373, 488, 1, 242, 0, '특정 상태이상만 적용(바포메트) 상태에서는 아머 브레이크 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (374, 2033, 1, 245, 0, '실패 모션(메테오 - 사일런스)에서는 성공 모션(바포메트) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (375, 2034, 1, 245, 0, '실패 모션(파이어 - 스운)에서는 성공 모션(바포메트) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (376, 2062, 1, 46, 0, '드라코 탑승중 상태에서 닌자 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (377, 2063, 1, 46, 0, '드라코 탑승중 상태에서 파이어워크스 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (378, 2065, 1, 46, 0, '드라코 탑승중 상태에서 아누비스 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (379, 2066, 1, 46, 0, '드라코 탑승중 상태에서 아모데우스 변신 목걸이 사용', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (380, 2067, 1, 46, 0, '드라코 탑승중 상태에서 차크람 소녀 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (381, 2068, 1, 46, 0, '드라코 탑승중 상태에서 바드 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (382, 2064, 1, 46, 0, '드라코 탑승중 상태에서 알케미스트 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (383, 2069, 1, 46, 0, '드라코 탑승중 상태에서 아수라 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (384, 2070, 1, 46, 0, '드라코 탑승중 상태에서 무녀 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (385, 2071, 1, 46, 0, '드라코 탑승중 상태에서 아바돈 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (386, 2072, 1, 46, 0, '드라코 탑승중 상태에서 비숍 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (387, 2074, 1, 46, 0, '드라코 탑승중 상태에서 여해적 선장 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (388, 2073, 1, 46, 0, '드라코 탑승중 상태에서 몽크 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (389, 2075, 1, 46, 0, '드라코 탑승중 상태에서 나이트 오브 퀸 변신 목걸이  사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (390, 2076, 1, 46, 0, '드라코 탑승중 상태에서 사무라이 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (391, 2077, 1, 46, 0, '드라코 탑승중 상태에서 네크로맨서 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (392, 2078, 1, 46, 0, '드라코 탑승중 상태에서 글라디에이터 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (393, 679, 1, 46, 0, '드라코 탑승중 상태에서 리플렉션 오오라 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (394, 2079, 1, 46, 0, '드라코 탑승중 상태에서 다크 메이지 I 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (395, 2080, 1, 46, 0, '드라코 탑승중 상태에서 길가메쉬 아처 I 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (396, 2081, 1, 46, 0, '드라코 탑승중 상태에서 홀리 소서리스 II 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (398, 2083, 1, 46, 0, '드라코 탑승중 상태에서 길가메쉬 아처 II 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (399, 2084, 1, 46, 0, '드라코 탑승중 상태에서 홀리 소서리스 III 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (400, 2085, 1, 46, 0, '드라코 탑승중 상태에서 엘리멘탈리스트 III 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (401, 2086, 1, 46, 0, '드라코 탑승중 상태에서 다크 메이지 II 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (402, 2082, 1, 46, 0, '드라코 탑승중 상태에서 엘리멘탈리스트 II 변신 목걸', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (403, 2094, 1, 46, 0, '드라코 탑승중 상태에서 아바돈 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (404, 2095, 1, 46, 0, '드라코 탑승중 상태에서 메타트론 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (405, 2096, 1, 46, 0, '드라코 탑승중 상태에서 메타트론 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (406, 1971, 1, 46, 0, '드라코 탑승중 상태에서 알케미스트 하드코어 스킬변', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (407, 1972, 1, 46, 0, '드라코 탑승중 상태에서 닌자 하드코어 스킬변', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (408, 1973, 1, 46, 0, '드라코 탑승중 상태에서 바드 하드코어 스킬변', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (409, 1974, 1, 46, 0, '드라코 탑승중 상태에서 나이트 오브 퀸 하드코어 스킬변', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (410, 1975, 1, 46, 0, '드라코 탑승중 상태에서 차크람 스킬변', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (411, 1976, 1, 46, 0, '드라코 탑승중 상태에서 여해적 선장 하드코어 스킬변', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (412, 1977, 1, 46, 0, '드라코 탑승중 상태에서 뭉크 하드코어 스킬변', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (413, 1978, 1, 46, 0, '드라코 탑승중 상태에서 아수라 하드코어 스킬변', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (414, 2128, 1, 46, 0, '드라코 탑승중 상태에서 바룬용병대 오크대장 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (415, 2129, 1, 46, 0, '드라코 탑승중 상태에서 트레이서 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (416, 2130, 1, 46, 0, '드라코 탑승중 상태에서 플린드 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (417, 2131, 1, 46, 0, '드라코 탑승중 상태에서 데몬 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (418, 2132, 1, 46, 0, '드라코 탑승중 상태에서 다크 트레이서 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (419, 2133, 1, 46, 0, '드라코 탑승중 상태에서 레서 파이어 데몬 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (420, 2134, 1, 46, 0, '드라코 탑승중 상태에서 래서 아스타로트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (422, 2135, 1, 46, 0, '드라코 탑승중 상태에서 래서 루시퍼 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (423, 2136, 1, 46, 0, '드라코 탑승중 상태에서 그레이터 루시퍼 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (424, 2137, 1, 46, 0, '드라코 탑승중 상태에서 그레이터 아스타로트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (425, 2138, 1, 46, 0, '드라코 탑승중 상태에서 머실리스 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (426, 2139, 1, 46, 0, '드라코 탑승중 상태에서 쉬루드 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (427, 2140, 1, 46, 0, '드라코 탑승중 상태에서 크루얼 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (428, 2141, 1, 46, 0, '드라코 탑승중 상태에서 님로드 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (429, 2142, 1, 46, 0, '드라코 탑승중 상태에서 앱솔루트 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (430, 2143, 1, 46, 0, '드라코 탑승중 상태에서 베이그란트 크루세이더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (431, 2144, 1, 46, 0, '드라코 탑승중 상태에서 드래곤나이트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (432, 2145, 1, 46, 0, '드라코 탑승중 상태에서 드래곤나이트 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (433, 2146, 1, 46, 0, '드라코 탑승중 상태에서 엘더 스팅어 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (434, 2147, 1, 46, 0, '드라코 탑승중 상태에서 발키리 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (435, 2148, 1, 46, 0, '드라코 탑승중 상태에서 다크 나이트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (436, 2149, 1, 46, 0, '드라코 탑승중 상태에서 다크 스팅어 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (437, 2150, 1, 46, 0, '드라코 탑승중 상태에서 메타트론 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (438, 2151, 1, 46, 0, '드라코 탑승중 상태에서 메타트론 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (439, 2189, 1, 46, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (440, 2190, 1, 46, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (441, 2191, 1, 46, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (442, 2192, 1, 46, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (443, 2193, 1, 46, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (444, 2194, 1, 46, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (445, 2195, 1, 46, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (446, 2231, 1, 18, 0, '변신중일때 그리폰 탑승 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (447, 2287, 1, 46, 0, '드라코 탑승중 상태에서 아스모데우스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (448, 2288, 1, 46, 0, '드라코 탑승중 상태에서 엘리멘탈 리스트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (449, 2289, 1, 46, 0, '드라코 탑승중 상태에서 네크로멘서 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (450, 2290, 1, 46, 0, '드라코 탑승중 상태에서 아누비스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (451, 2291, 1, 46, 0, '드라코 탑승중 상태에서 무녀 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (452, 2292, 1, 46, 0, '드라코 탑승중 상태에서 글라디에이터 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (453, 2293, 1, 46, 0, '드라코 탑승중 상태에서 아바돈 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (461, 2325, 1, 46, 0, '드라코 탑승중 상태에서 뮤 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (463, 2549, 1, 46, 0, '드라코 탑승중 상태에서 비숍 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (465, 2556, 1, 46, 0, '드라코 탑승중 데몬 스토커 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (466, 2557, 1, 46, 0, '드라코 탑승중 헤라클레스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (467, 2558, 1, 46, 0, '드라코 탑승중 광대 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (468, 2559, 1, 46, 0, '드라코 탑승중 버서커 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (469, 2561, 1, 46, 0, '드라코 탑승중 여신 칼리 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (470, 2794, 1, 46, 0, '드라코 탑승중 데드마스터 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (471, 2795, 1, 46, 0, '드라코 탑승중 데드마스터 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (472, 2550, 1, 46, 0, '드라코 탑승중 센티넬 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (473, 2551, 1, 46, 0, '드라코 탑승중 데빌헌터 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (474, 2783, 1, 130, 0, '커버링 토템있을 때 플레임 스트라이크 Lv1 물약 파괴 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (475, 2784, 1, 130, 0, '커버링 토템있을 때 플레임 스트라이크 Lv2 물약 파괴 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (476, 2785, 1, 130, 0, '커버링 토템있을 때 플레임 스트라이크 Lv3 물약 파괴 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (477, 2786, 1, 130, 0, '커버링 토템있을 때 플레임 스트라이크 Lv4 물약 파괴 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (478, 2787, 1, 130, 0, '커버링 토템있을 때 플레임 스트라이크 Lv5 물약 파괴 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (479, 2814, 2, 94, 1, '인비지 상태가 아닐때 디텍션 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (480, 2815, 2, 94, 1, '인비지 상태가 아닐때 디텍션 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (481, 2816, 2, 94, 1, '인비지 상태가 아닐때 디텍션 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (482, 2814, 2, 38, 1, '투명상태가 아닐때 디텍션 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (483, 2815, 2, 38, 1, '투명상태가 아닐때 디텍션 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (484, 2816, 2, 38, 1, '투명상태가 아닐때 디텍션 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (485, 2809, 1, 95, 0, '디텍션 사용시 나의 투명상태(2809) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (486, 2810, 1, 95, 0, '디텍션 사용시 나의 투명상태(2810) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (487, 2811, 1, 95, 0, '디텍션 사용시 나의 투명상태(2811) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (488, 2812, 1, 95, 0, '디텍션 사용시 나의 투명상태(2812) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (489, 2814, 1, 0, 0, '', 2810);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (490, 2814, 1, 0, 0, '', 2811);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (491, 2814, 1, 0, 0, '', 2812);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (492, 2815, 1, 0, 0, '', 2811);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (493, 2815, 1, 0, 0, '', 2812);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (494, 2816, 1, 0, 0, '', 2812);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (495, 2809, 1, 46, 0, '드라코 탑승중 투명상태 Lv1(2809) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (496, 2810, 1, 46, 0, '드라코 탑승중 투명상태 Lv2(2810) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (497, 2811, 1, 46, 0, '드라코 탑승중 투명상태 Lv3(2811) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (498, 2812, 1, 46, 0, '드라코 탑승중 투명상태 Lv4(2812) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (499, 2809, 1, 92, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (500, 2810, 1, 92, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (501, 2811, 1, 92, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (502, 2812, 1, 92, 0, '드라코 탑승중인 상태에서 퓨인즈 강화(III) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (504, 2731, 1, 46, 0, '드라코 탑승중 퓨인즈 강화(III) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (505, 2221, 2, 38, 1, '투명상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (506, 2222, 2, 38, 1, '투명상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (507, 2223, 2, 38, 1, '투명상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (508, 2316, 2, 38, 1, '투명상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (509, 2317, 2, 38, 1, '투명상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (510, 2221, 2, 94, 1, '인비지상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (511, 2222, 2, 94, 1, '인비지상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (512, 2223, 2, 94, 1, '인비지상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (513, 2316, 2, 94, 1, '인비지상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (514, 2317, 2, 94, 1, '인비지상태가 아닐때 은둔자의 보폭을 받지 않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (516, 1854, 1, 46, 0, '드라코 탑승중 버서크 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (517, 1855, 1, 46, 0, '드라코 탑승중 버서크 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (518, 1856, 1, 46, 0, '드라코 탑승중 버서크 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (519, 1857, 1, 46, 0, '드라코 탑승중 버서크 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (520, 2217, 1, 46, 0, '드라코 탑승중 질주 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (521, 2218, 1, 46, 0, '드라코 탑승중 질주 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (522, 2219, 1, 46, 0, '드라코 탑승중 질주 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (523, 2308, 1, 46, 0, '드라코 탑승중 질주 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (524, 2309, 1, 46, 0, '드라코 탑승중 질주 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (525, 1608, 1, 46, 0, '드라코 탑승중 이동속도 업 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (526, 1609, 1, 46, 0, '드라코 탑승중 이동속도 업 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (527, 1610, 1, 46, 0, '드라코 탑승중 이동속도 업 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (528, 2848, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (529, 2849, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (530, 2850, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (531, 2851, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (532, 2852, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (533, 2853, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (538, 2878, 1, 46, 0, '드라코 탑승중인 상태에서 초신속의 영약 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (539, 2885, 1, 46, 0, '드라코 탑승중 비숍 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (540, 2886, 1, 46, 0, '드라코 탑승중 실피드 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (541, 2887, 1, 46, 0, '드라코 탑승중 뮤 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (542, 2888, 1, 46, 0, '드라코 탑승중 센티넬 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (543, 2889, 1, 46, 0, '드라코 탑승중 데빌헌터 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (544, 2904, 1, 46, 0, '드라코 탑승중 체인 인큐버스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (545, 2905, 1, 46, 0, '드라코 탑승중 닌자 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (546, 2906, 1, 46, 0, '드라코 탑승중 파이어워크스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (547, 2907, 1, 46, 0, '드라코 탑승중 알케미스트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (548, 2908, 1, 46, 0, '드라코 탑승중 아스모데우스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (549, 2909, 1, 46, 0, '드라코 탑승중 아누비스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (550, 2910, 1, 46, 0, '드라코 탑승중 바바리안 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (551, 2911, 1, 46, 0, '드라코 탑승중 다크 메이지 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (552, 2912, 1, 46, 0, '드라코 탑승중 차크람 소녀 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (553, 2913, 1, 46, 0, '드라코 탑승중 바드 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (554, 2914, 1, 46, 0, '드라코 탑승중 아수라 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (555, 2915, 1, 46, 0, '드라코 탑승중 엘리멘탈리스트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (556, 2916, 1, 46, 0, '드라코 탑승중 무녀 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (557, 2917, 1, 46, 0, '드라코 탑승중 길가메쉬 나이트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (558, 2918, 1, 46, 0, '드라코 탑승중 길가메쉬 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (559, 2919, 1, 46, 0, '드라코 탑승중 몽크 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (560, 2920, 1, 46, 0, '드라코 탑승중 여해적 선장 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (561, 2921, 1, 46, 0, '드라코 탑승중 나이트 오브 퀸 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (562, 2922, 1, 46, 0, '드라코 탑승중 사무라이 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (563, 2923, 1, 46, 0, '드라코 탑승중 네크로멘서 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (564, 2924, 1, 46, 0, '드라코 탑승중 데몬 스토커 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (565, 2925, 1, 46, 0, '드라코 탑승중 글라디에이터 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (566, 2926, 1, 46, 0, '드라코 탑승중 헤라클레스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (567, 2927, 1, 46, 0, '드라코 탑승중 아바돈 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (568, 2928, 1, 46, 0, '드라코 탑승중 광대 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (569, 2929, 1, 46, 0, '드라코 탑승중 버서커 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (570, 2930, 1, 46, 0, '드라코 탑승중 여신 칼리 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (571, 2931, 1, 46, 0, '드라코 탑승중 비숍 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (572, 2932, 1, 46, 0, '드라코 탑승중 실피드 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (573, 2933, 1, 46, 0, '드라코 탑승중 뮤 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (574, 2934, 1, 46, 0, '드라코 탑승중 센티넬 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (575, 2935, 1, 46, 0, '드라코 탑승중 데빌헌터 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (576, 2937, 1, 46, 0, '드라코 탑승중 캣츠메이드 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (577, 2938, 1, 46, 0, '드라코 탑승중 데빌리스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (578, 2939, 1, 46, 0, '드라코 탑승중 엔젤리스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (579, 2944, 1, 46, 0, '드라코 탑승중 드래곤룰러 오페르시나 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (580, 2945, 1, 46, 0, '드라코 탑승중 드래곤룰러 오페르시나 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (581, 2960, 1, 46, 0, '드라코 탑승중 유피테르의 사념 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (582, 2961, 1, 46, 0, '드라코 탑승중 바알베크의 사령관 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (585, 2969, 1, 46, 0, '드라코 탑승중 아누비스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (586, 2968, 1, 46, 0, '드라코 탑승중 인페르노 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (591, 2967, 1, 46, 0, '드라코 탑승중 인페르노 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (592, 2970, 1, 46, 0, '드라코 탑승중 문엘프 마법사 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (593, 2975, 1, 46, 0, '드라코 탑승중 메타트론 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (594, 2976, 1, 46, 0, '드라코 탑승중 메타트론 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (595, 2977, 1, 46, 0, '드라코 탑승중 넵튠 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (596, 2978, 1, 46, 0, '드라코 탑승중 넵튠 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (597, 2979, 1, 46, 0, '드라코 탑승중 해신 오페르시나 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (598, 2980, 1, 46, 0, '드라코 탑승중 해신 오페르시나 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (599, 3046, 1, 46, 0, '드라코 탑승중 캣츠메이드 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (600, 3047, 1, 46, 0, '드라코 탑승중 실피드 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (601, 3048, 1, 46, 0, '드라코 탑승중 황진이 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (602, 3049, 1, 46, 0, '드라코 탑승중 비숍 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (603, 3050, 1, 46, 0, '드라코 탑승중 헌터 리바인저 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (604, 3051, 1, 46, 0, '드라코 탑승중 스나이퍼 빅스웰 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (605, 3052, 1, 46, 0, '드라코 탑승중 러블리 피어스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (606, 3053, 1, 46, 0, '드라코 탑승중 홀리소서리스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (607, 3054, 1, 46, 0, '드라코 탑승중 문엘프 전사 (V) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (608, 3055, 1, 46, 0, '드라코 탑승중 미드나잇 에르메스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (609, 3056, 1, 46, 0, '드라코 탑승중 베르키오라 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (610, 3057, 1, 46, 0, '드라코 탑승중 데빌리스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (620, 3061, 1, 46, 0, '드라코 탑승중 유피테르의 사념 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (621, 3062, 1, 46, 0, '드라코 탑승중 바알베크의 사령관 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (622, 3082, 1, 46, 0, '드라코 탑승중 광대 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (623, 3083, 1, 46, 0, '드라코 탑승중 여신 칼리 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (624, 3084, 1, 46, 0, '드라코 탑승중 차크람 소녀 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (669, 236, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (670, 1818, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (671, 2233, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (672, 2234, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (673, 2235, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (674, 2236, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (675, 2237, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (676, 2161, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (677, 87, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (678, 508, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (679, 509, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (680, 510, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (681, 511, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (682, 1829, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (683, 2809, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (684, 2810, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (685, 2811, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (686, 2812, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (687, 2813, 1, 303, 0, '투명 제한 상태일때 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (688, 197, 1, 310, 0, '실프윈드 - 폭풍의 시간 상태일때 실프윈드 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (689, 3232, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (690, 3233, 1, 18, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (691, 3234, 1, 18, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (692, 3137, 2, 94, 0, '인비지 상태가 아닐때 어쌔시네이션-매의눈 사용 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (693, 3137, 1, 46, 0, '드라코 탑승중 어쌔시네이션-매의눈 사용불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (694, 3137, 1, 38, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (695, 3137, 1, 95, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (696, 3133, 2, 94, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (697, 3133, 1, 46, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (698, 3133, 1, 38, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (699, 3133, 1, 95, 0, NULL, 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (700, 3235, 1, 46, 0, '드라코 탑승중 수련소녀 (VI) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (701, 3236, 1, 46, 0, '드라코 탑승중 큐티 프리스트 (VI) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (702, 3237, 1, 46, 0, '드라코 탑승중 가이아 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (703, 3238, 1, 46, 0, '드라코 탑승중 가이아 거너 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (704, 3059, 1, 46, 0, '드라코 탑승중 아레스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (705, 3060, 1, 46, 0, '드라코 탑승중 아레스 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (706, 3058, 1, 46, 0, '드라코 탑승중 엔젤리스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (707, 3096, 1, 303, 0, '인비지빌리티-안식의 시간 투명화 제한 상태 투명화 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (710, 3239, 1, 46, 0, '드라코 탑승중 넵튠 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (711, 3240, 1, 46, 0, '드라코 탑승중 넵튠 아처변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (712, 3241, 1, 46, 0, '드라코 탑승중 아레스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (713, 3242, 1, 46, 0, '드라코 탑승중 아레스 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (714, 3096, 1, 46, 0, '드라코 탑승중 인비지 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (715, 3096, 1, 95, 0, '디텍션 상태 인비지 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (716, 3096, 1, 38, 0, '투명상태 인비지 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (717, 1608, 1, 38, 0, '투명상태일 경우 이동속도1증가 버프 받지않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (719, 1609, 1, 38, 0, '투명상태일 경우 이동속도2증가 버프 받지않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (720, 1610, 1, 38, 0, '투명상태일 경우 이동속도3증가 버프 받지않음', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (723, 1608, 1, 95, 0, '디텍션 상태 이동속도1 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (724, 1609, 1, 95, 0, '디텍션 상태 이동속도2 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (725, 1610, 1, 95, 0, '디텍션 상태 이동속도3 불가', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (727, 2233, 1, 95, 0, '디텍션 상태 일때 안식 인비지 1 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (728, 2234, 1, 95, 0, '디텍션 상태 일때 안식 인비지 2 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (729, 2235, 1, 95, 0, '디텍션 상태 일때 안식 인비지 3 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (730, 2236, 1, 95, 0, '디텍션 상태 일때 안식 인비지 4 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (731, 2237, 1, 95, 0, '디텍션 상태 일때 안식 인비지 5 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (732, 3243, 1, 95, 0, '디텍션 상태 일때 안식 강화 인비지 1 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (733, 3244, 1, 95, 0, '디텍션 상태 일때 안식 강화 인비지 2 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (734, 3245, 1, 95, 0, '디텍션 상태 일때 안식 강화 인비지 3 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (735, 3246, 1, 95, 0, '디텍션 상태 일때 안식 강화 인비지 4 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (736, 3247, 1, 95, 0, '디텍션 상태 일때 안식 강화 인비지 5 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (737, 2233, 1, 46, 0, '드라코 탑승중인 상태에서 안식 인비지 1 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (738, 2234, 1, 46, 0, '드라코 탑승중인 상태에서 안식 인비지 2 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (739, 2235, 1, 46, 0, '드라코 탑승중인 상태에서 안식 인비지 3 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (740, 2236, 1, 46, 0, '드라코 탑승중인 상태에서 안식 인비지 4 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (741, 2237, 1, 46, 0, '드라코 탑승중인 상태에서 안식 인비지 5 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (742, 3243, 1, 46, 0, '드라코 탑승중인 상태에서 안식 강화 인비지 1 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (743, 3244, 1, 46, 0, '드라코 탑승중인 상태에서 안식 강화 인비지 2 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (744, 3245, 1, 46, 0, '드라코 탑승중인 상태에서 안식 강화 인비지 3 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (745, 3246, 1, 46, 0, '드라코 탑승중인 상태에서 안식 강화 인비지 4 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (746, 3247, 1, 46, 0, '드라코 탑승중인 상태에서 안식 강화 인비지 5 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (747, 3104, 1, 46, 0, '드라코 탑승중인 상태에서 특성강화 스캠퍼 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (748, 3068, 1, 46, 0, '드라코 탑승중인 상태에서 특성강화 버서크 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (749, 3100, 1, 123, 0, '파워워드 러스트 상태에서 아머(내면의파괴) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (751, 3140, 1, 46, 0, '드라코 탑승중인 상태에서 특성강화 레비 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (752, 3138, 1, 46, 0, '드라코 탑승중인 상태에서 특성강화 리플렉션오오라 ?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (753, 3113, 1, 46, 0, '드라코 탑승중인 상태에서 특성강화 타임디스토션 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (754, 3131, 1, 46, 0, '드라코 탑승중인 상태에서 특성강화 레비 회피 무시(?', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (755, 3132, 1, 46, 0, '드라코 탑승중인 상태에서 특성강화 리플렉션 이펙트 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (756, 3124, 1, 123, 0, '파워워드 러스트 상태에서 아머(내면의파괴) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (757, 2715, 1, 123, 0, '파워워드 러스트 상태에서 스킬트리강화아머 방어감소 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (758, 2716, 1, 123, 0, '파워워드 러스트 상태에서 스킬트리강화아머 방어감소 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (759, 2351, 1, 46, 0, '드라코 탑승중 러블리 피어스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (760, 2352, 1, 46, 0, '드라코 탑승중 실피드 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (761, 2583, 1, 46, 0, '드라코 탑승중 황진이 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (762, 2584, 1, 46, 0, '드라코 탑승중 베르키오라 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (763, 2585, 1, 46, 0, '드라코 탑승중 헌터 리바인져 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (764, 2586, 1, 46, 0, '드라코 탑승중 스나이퍼 빅스웰 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (765, 2587, 1, 46, 0, '드라코 탑승중 미드나잇 에르메스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (766, 3085, 1, 46, 0, '드라코 탑승중 데몬 스토커 (II) 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (790, 36, 1, 0, 0, '특수스킬저항 1Lv 때 저주 무시', 3427);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (791, 322, 1, 0, 0, '특수스킬저항 1Lv 때 저주 무시', 3427);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (792, 1207, 1, 0, 0, '특수스킬저항 1Lv 때 저주 무시', 3427);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (793, 1208, 1, 0, 0, '특수스킬저항 1Lv 때 저주 무시', 3427);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (794, 1209, 1, 0, 0, '특수스킬저항 1Lv 때 저주 무시', 3427);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (795, 36, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (796, 54, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (797, 55, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (798, 131, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (799, 322, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (800, 905, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (801, 906, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (802, 907, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (803, 908, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (804, 1207, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (805, 1208, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (806, 1209, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (807, 1210, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (808, 1211, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (809, 1212, 1, 0, 0, '특수스킬저항 2Lv 때 저주, 공포 무시', 3428);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (810, 3440, 1, 46, 0, '드라코 탑승중 아르테미스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (811, 3441, 1, 46, 0, '드라코 탑승중 아폴론 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (812, 3406, 2, 329, 0, '친밀도 외형 변경 대지의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (813, 3407, 2, 329, 0, '친밀도 외형 변경 태양의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (814, 3408, 2, 329, 0, '친밀도 외형 변경 바람의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (815, 3410, 2, 329, 0, '친밀도 외형 변경 강인한 대지의 드라코 (I) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (816, 3411, 2, 329, 0, '친밀도 외형 변경 강인한 대지의 드라코 (II) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (817, 3412, 2, 329, 0, '친밀도 외형 변경 강인한 태양의 드라코 (I) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (818, 3413, 2, 329, 0, '친밀도 외형 변경 강인한 태양의 드라코 (II) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (819, 3414, 2, 329, 0, '친밀도 외형 변경 강인한 바람의 드라코 (I) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (820, 3415, 2, 329, 0, '친밀도 외형 변경 강인한 바람의 드라코 (II) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (821, 3416, 2, 329, 0, '친밀도 외형 변경 맹렬한 대지의 드라코 (I) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (822, 3417, 2, 329, 0, '친밀도 외형 변경 맹렬한 대지의 드라코 (II) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (823, 3418, 2, 329, 0, '친밀도 외형 변경 맹렬한 태양의 드라코 (I) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (824, 3419, 2, 329, 0, '친밀도 외형 변경 맹렬한 태양의 드라코 (II) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (825, 3420, 2, 329, 0, '친밀도 외형 변경 맹렬한 바람의 드라코 (I) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (826, 3421, 2, 329, 0, '친밀도 외형 변경 맹렬한 바람의 드라코 (II) 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (827, 3367, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (828, 3369, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (829, 3371, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (830, 3373, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (831, 3375, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (832, 3377, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (833, 3379, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (834, 3381, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (835, 3383, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (836, 3385, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (837, 3387, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (838, 3389, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (839, 3447, 1, 46, 0, '드라코 탑승중 크로노스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (840, 3448, 1, 46, 0, '드라코 탑승중 크로노스 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (841, 3449, 1, 46, 0, '드라코 탑승중 가이아 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (842, 3450, 1, 46, 0, '드라코 탑승중 가이아 거너 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (843, 3480, 1, 46, 0, '드라코 탑승중 알케미스트(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (844, 3481, 1, 46, 0, '드라코 탑승중 닌자(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (845, 3482, 1, 46, 0, '드라코 탑승중 바드(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (846, 3483, 1, 46, 0, '드라코 탑승중 나이트 오브 퀸(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (847, 3484, 1, 46, 0, '드라코 탑승중 여해적 선장(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (848, 3485, 1, 46, 0, '드라코 탑승중 몽크(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (849, 3486, 1, 46, 0, '드라코 탑승중 아수라(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (850, 3487, 1, 46, 0, '드라코 탑승중 헤라클레스(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (851, 3488, 1, 46, 0, '드라코 탑승중 버서커(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (852, 3489, 1, 46, 0, '드라코 탑승중 헌터 리바인저(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (853, 3490, 1, 46, 0, '드라코 탑승중 스나이퍼 빅스웰(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (854, 3491, 1, 46, 0, '드라코 탑승중 미드나잇 에르메스(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (855, 3492, 1, 46, 0, '드라코 탑승중 데빌리스(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (856, 3493, 1, 46, 0, '드라코 탑승중 엔젤리스(II) 변신 무시 ', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (857, 3575, 2, 329, 0, '친밀도 외형 변경 황야의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (858, 3577, 2, 329, 0, '친밀도 외형 변경 평야의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (859, 3581, 2, 329, 0, '친밀도 외형 변경 홍염의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (860, 3583, 2, 329, 0, '친밀도 외형 변경 백광의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (861, 3587, 2, 329, 0, '친밀도 외형 변경 질풍의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (862, 3589, 2, 329, 0, '친밀도 외형 변경 돌풍의 드라코 가변 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (863, 3599, 1, 46, 0, '드라코 탑승중 화염의 이그리트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (864, 3601, 1, 46, 0, '드라코 탑승중 화염의 이그리트 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (865, 3603, 1, 46, 0, '드라코 탑승중 화마의 이그리트 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (866, 3605, 1, 46, 0, '드라코 탑승중 화마의 이그리트 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (867, 3607, 1, 46, 0, '드라코 탑승중 심해의 레비아탄 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (868, 3609, 1, 46, 0, '드라코 탑승중 심해의 레비아탄 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (869, 3611, 1, 46, 0, '드라코 탑승중 천해의 레비아탄 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (870, 3613, 1, 46, 0, '드라코 탑승중 천해의 레비아탄 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (871, 3615, 1, 46, 0, '드라코 탑승중 황야의 테이아 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (872, 3617, 1, 46, 0, '드라코 탑승중 황야의 테이아 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (873, 3619, 1, 46, 0, '드라코 탑승중 평야의 테이아 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (874, 3621, 1, 46, 0, '드라코 탑승중 평야의 테이아 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (875, 3623, 1, 46, 0, '드라코 탑승중 월광의 칼리고 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (876, 3625, 1, 46, 0, '드라코 탑승중 월광의 칼리고 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (877, 3627, 1, 46, 0, '드라코 탑승중 현월의 칼리고 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (878, 3629, 1, 46, 0, '드라코 탑승중 현월의 칼리고 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (879, 3631, 1, 46, 0, '드라코 탑승중 홍염의 루멘 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (880, 3633, 1, 46, 0, '드라코 탑승중 홍염의 루멘 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (881, 3635, 1, 46, 0, '드라코 탑승중 백광의 루멘 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (882, 3637, 1, 46, 0, '드라코 탑승중 백광의 루멘 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (883, 3639, 1, 46, 0, '드라코 탑승중 돌풍의 웬투스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (884, 3641, 1, 46, 0, '드라코 탑승중 돌풍의 웬투스 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (885, 3643, 1, 46, 0, '드라코 탑승중 질풍의 웬투스 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (886, 3645, 1, 46, 0, '드라코 탑승중 질풍의 웬투스 아처 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (889, 3650, 1, 46, 0, '드라코 탑승중 광휘의 엘윈더 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (890, 3651, 1, 46, 0, '드라코 탑승중 광휘의 엘로라 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (891, 3578, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (892, 3579, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (893, 3584, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (894, 3585, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (895, 3590, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (896, 3591, 1, 18, 0, '', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (922, 3120, 1, 46, 0, '드라코 탑승중 일 경우 버서크 상태 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (923, 3121, 1, 46, 0, '드라코 탑승중 일 경우 버서크 상태 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (924, 3122, 1, 46, 0, '드라코 탑승중 일 경우 버서크 상태 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (925, 3123, 1, 46, 0, '드라코 탑승중 일 경우 버서크 상태 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (926, 3654, 1, 46, 0, '드라코 탑승중 블레어 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (927, 3655, 1, 46, 0, '드라코 탑승중 캐스퍼 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (928, 1279, 1, 46, 0, '드라코 탑승중 소멸 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (929, 3656, 1, 46, 0, '드라코 탑승중 상태에서 아폴론 변신목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (930, 3657, 1, 46, 0, '드라코 탑승중 상태에서 아르테미스 변신 목걸이 사용 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (931, 3135, 2, 161, 0, '오리에드토템 없을때 오리에드 토템흡수강화 이펙트 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (932, 3680, 1, 46, 0, '드라코 탑승중 상태에서 암살단 암흑 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (933, 3681, 1, 46, 0, '드라코 탑승중 상태에서 암살단 칠흑 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (934, 3686, 1, 46, 0, '드라코 탑승중 상태에서 암살단 암흑 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (935, 3687, 1, 46, 0, '드라코 탑승중 상태에서 암살단 칠흑 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (936, 3713, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (937, 3714, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (938, 3715, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (939, 3716, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (940, 3717, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (941, 3718, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (942, 3719, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (943, 3720, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (944, 3721, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (945, 3722, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (946, 3723, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (947, 3724, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (948, 3725, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (949, 3726, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (950, 3727, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (951, 3728, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (952, 3729, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (953, 3730, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (954, 3731, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (955, 3732, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (956, 3733, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (957, 3734, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (958, 3735, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (959, 3736, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (960, 3809, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (961, 3813, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (962, 3814, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (963, 3815, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (964, 3816, 1, 46, 0, '드라코 탑승중 변신 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (107251, 107234, 1, 46, 0, '드라코 탑승중 상태에서 서번트 변목 (근거리) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (107252, 107235, 1, 46, 0, '드라코 탑승중 상태에서 서번트 변목 (원거리) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (107275, 107259, 1, 46, 0, '드라코 탑승중 상태에서 암살단 암흑 (근거리) 무시', 0);
GO

INSERT INTO [dbo].[DT_AbnormalAttrIgnore] ([mID], [mOriginAID], [mConditionType], [mConditionAType], [mIsComplex], [mDesc], [mConditionAID]) VALUES (107276, 107260, 1, 46, 0, '드라코 탑승중 상태에서 암살단 칠흑 (원거리) 무시', 0);
GO

