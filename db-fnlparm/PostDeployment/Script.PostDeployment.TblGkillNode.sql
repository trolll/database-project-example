/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblGkillNode
Date                  : 2023-10-07 09:09:33
*/


INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (0, '血之誓言Lv1');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (1, '贤者智慧');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (2, '魔法之手');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (3, '魔法之力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (4, '巨人之力Lv1');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (5, '自然守护');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (6, '巨人之力Lv2');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (7, '血之誓言Lv2');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (8, '猎豹之眼');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (9, '联合之力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (10, '神圣祝福');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (11, '鹰之眼');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (12, '风之力量');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (20, '英雄之血Lv1');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (21, '巨人之力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (22, '圣壁');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (23, '英雄之血Lv2');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (24, '天诛');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (25, '猛虎之爪');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (26, '神奇秘药');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (27, '专注');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (28, '魔法研究');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (29, '霸王之眼');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (30, '霸主威严');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (31, '骨龙研究');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (40, '钢铁软化');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (41, '自然法则');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (42, '治愈法则');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (43, '恶魔的诅咒');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (44, '恶魔的愤怒');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (61, '水元素');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (62, '元素力量');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (63, '亚特兰蒂斯神力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (80, '兽人盾牌');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (81, '兽人知识');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (82, '兽人愤怒');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (100, '豺狼之血');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (101, '豺狼之弓');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (102, '豺狼专注');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (103, '豺狼统率');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (120, '坚硬蜘蛛丝');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (121, '狼蛛之血');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (122, '蜘蛛魔爪');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (123, '蜘蛛剧毒');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (140, '魔窟幻想');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (141, '魔窟神力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (142, '吸血鬼之血');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (143, '锋利石笋');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (144, '黑暗恐惧');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (160, '地精行囊');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (161, '地精勇气');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (162, '地精之弓');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (163, '地精愤怒');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (180, '极乐之羽');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (181, '极乐之翼');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (182, '极乐勇气');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (183, '极乐之爪');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (200, '亡灵之泪');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (201, '硬骨');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (202, '死亡恐惧');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (203, '亡灵力量');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (220, '沼泽神力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (221, '黑龙之血');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (222, '黑龙装束');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (223, '黑龙气息');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (240, '秩序法则');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (241, '秩序之源');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (242, '永恒秩序');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (243, '神殿威严');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (260, '混乱无序');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (261, '混乱法则');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (262, '混乱黑洞');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (263, '混乱之光');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (280, '英雄之血Lv1');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (281, '巨人之力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (282, '坚如磐石');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (283, '英雄之血Lv2');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (284, '天诛');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (285, '猛虎之爪');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (286, '神奇秘药');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (287, '专注');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (288, '魔法研究');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (289, '霸主之眼');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (290, '霸主威严');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (291, '骨龙研究');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (300, '英雄之血Lv1');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (301, '巨人之力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (302, '坚如磐石');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (303, '英雄之血Lv2');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (304, '天诛');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (305, '猛虎之爪');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (306, '神奇秘药');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (307, '专注');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (308, '魔法研究');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (309, '霸王之眼');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (310, '霸主威严');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (311, '骨龙研究');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (320, '力压千钧');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (321, '造水术');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (322, '焚化');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (323, '恐惧');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (340, '石像体质');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (341, '石像净化');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (342, '巨石轰击');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (343, '彗星攻击');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (360, '哈斯特之墙');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (361, '召唤神灵');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (362, '圣域');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (363, '真空状态');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (380, '漂浮术');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (381, '火炬');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (382, '永恒生命');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (383, '水晶之眼');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (400, '加大行囊');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (401, '咒语');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (402, '复仇');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (403, '钢铁护甲');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (404, '臭云术');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (420, '臭云术');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (421, '虚幻之水');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (422, '水之呼吸');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (423, '强化装备');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (424, '符咒');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (440, '英雄之血Lv1');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (441, '巨人之力');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (442, '坚如磐石');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (443, '英雄止血Lv2');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (444, '天诛');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (445, '猛虎之爪');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (446, '神奇秘药');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (447, '专注');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (448, '魔法研究');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (449, '霸王之眼');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (450, '霸主威严');
GO

INSERT INTO [dbo].[TblGkillNode] ([mNode], [mDesc]) VALUES (451, '骨龙研究');
GO

