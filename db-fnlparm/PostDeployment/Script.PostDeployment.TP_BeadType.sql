/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_BeadType
Date                  : 2023-10-07 09:08:58
*/


INSERT INTO [dbo].[TP_BeadType] ([mBeadType], [mDesc], [mDescA], [mDescB], [mDescC], [mDescD], [mDescE]) VALUES (1, '스킬 발동(클래스 체크)', '나이트SPID', '레인저SPID', '엘프SPID', '어쌔신SPID', '서모너SPID');
GO

INSERT INTO [dbo].[TP_BeadType] ([mBeadType], [mDesc], [mDescA], [mDescB], [mDescC], [mDescD], [mDescE]) VALUES (2, '스킬 발동', 'SPID', '', '', '', '');
GO

INSERT INTO [dbo].[TP_BeadType] ([mBeadType], [mDesc], [mDescA], [mDescB], [mDescC], [mDescD], [mDescE]) VALUES (3, '대미지 변경', '대미지(+/-)', '', '', '', '');
GO

INSERT INTO [dbo].[TP_BeadType] ([mBeadType], [mDesc], [mDescA], [mDescB], [mDescC], [mDescD], [mDescE]) VALUES (4, '무효화', '', '', '', '', '');
GO

INSERT INTO [dbo].[TP_BeadType] ([mBeadType], [mDesc], [mDescA], [mDescB], [mDescC], [mDescD], [mDescE]) VALUES (5, '상태이상 시간 변경', '시간(+-, ms단위)', '', '', '', '');
GO

