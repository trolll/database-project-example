/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AchieveItemCoin
Date                  : 2023-10-07 09:08:50
*/


INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7426, 1, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7427, 1, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7428, 1, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7429, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7430, 2, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7431, 2, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7432, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7433, 2, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7434, 3, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7435, 3, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7436, 3, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7437, 3, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7438, 4, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7439, 4, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7440, 4, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7441, 4, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7442, 5, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7443, 5, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7444, 5, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7445, 5, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7446, 6, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7447, 6, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7448, 6, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7449, 6, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7450, 7, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7451, 7, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7452, 7, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7453, 7, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7454, 8, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7455, 8, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7456, 8, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7457, 8, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7458, 9, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7459, 9, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7460, 9, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7461, 9, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7462, 10, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7463, 10, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7464, 10, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7465, 10, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7466, 11, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7467, 11, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7468, 11, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7469, 11, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7470, 12, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7471, 12, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7472, 12, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7473, 12, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7474, 13, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7475, 13, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7476, 13, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7477, 13, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7478, 14, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7479, 14, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7480, 14, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7481, 14, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7482, 15, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7483, 15, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7484, 15, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7485, 15, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7486, 16, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7487, 16, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7488, 16, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7489, 16, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7490, 17, 4);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7491, 17, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7492, 17, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemCoin] ([IID], [mGrade], [mRarity]) VALUES (7493, 17, 1);
GO

