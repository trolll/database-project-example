/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblRegionQuest
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2501, '화염의 탑', 'RCLIENT_MSG_REGION_QUEST_TOWER_OF_FLAME');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2502, '언데드 던전', 'RCLIENT_MSG_REGION_QUEST_UNDEAD_DUNGEON');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2503, '어두운 동굴', 'RCLIENT_MSG_REGION_QUEST_DARK_CAVE');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2504, '흑룡의 늪', 'RCLIENT_MSG_REGION_QUEST_SWAMP_OF_BLACK_DRAGON');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2505, '왕의 무덤', 'RCLIENT_MSG_REGION_QUEST_KING_TOMB');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2506, '에기르의 수중동굴', 'RCLIENT_MSG_REGION_QUEST_AEGIRS_UNDERWATER_CAVE');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2507, '에르테스 동굴', 'RCLIENT_MSG_REGION_QUEST_ERTES');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2508, '딱정벌레 구덩이', 'RCLIENT_MSG_REGION_QUEST_PITS_OF_GIANT_BEETLE');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2509, '발레포르 던전', 'RCLIENT_MSG_REGION_QUEST_VALEFOR_DUNGEON');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2510, '고대의 계단', 'RCLIENT_MSG_REGION_QUEST_ANCIENT_STAIRS');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2511, '일루미나의 성지', 'RCLIENT_MSG_REGION_QUEST_ILLUMINA_SHRINE');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2512, '메테오스의 레어', 'RCLIENT_MSG_REGION_QUEST_METEOS');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2513, '메테오스의 탑', 'RCLIENT_MSG_REGION_QUEST_TOWER_OF_METEOS');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2514, '원시해적의 동굴', 'RCLIENT_MSG_REGION_QUEST_CAVE_OF_PRIMORDIAL_PIRATES');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2515, '만월의 유적지', 'RCLIENT_MSG_REGION_QUEST_FULL_MOON_HISTORIC_SITE');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2516, '화염의 탑 봉인지', 'RCLIENT_MSG_REGION_QUEST_TOWER_OF_FLAME_5F');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2517, '언데드 던전 봉인지', 'RCLIENT_MSG_REGION_QUEST_UNDEAD_DUNGEON_4F');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2518, '어두운 동굴 봉인지', 'RCLIENT_MSG_REGION_QUEST_DARK_CAVE_5F');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2519, '흑룡의 늪 봉인지', 'RCLIENT_MSG_REGION_QUEST_SWAMP_OF_BLACK_DRAGON_5F');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2520, '왕의 무덤 봉인지', 'RCLIENT_MSG_REGION_QUEST_KING_TOMB_4F');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2521, '유피테르 던전', 'RCLIENT_MSG_REGION_QUEST_JUPITER_DUNGEON');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2522, '바알베크 던전', 'RCLIENT_MSG_REGION_QUEST_BAALBEK_DUNGEON');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2523, '만월의 지평선', 'RCLIENT_MSG_REGION_QUEST_FULL_MOON_HORIZON');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2524, '발레포르 공중던전', 'RCLIENT_MSG_REGION_QUEST_VALEFOR_AIR_DUNGEON');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2525, '발레포르 지하광장', 'RCLIENT_MSG_REGION_QUEST_VALEFOR_UNDER_SQUARE');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2526, '아벨루스의 요새', 'RCLIENT_MSG_REGION_QUEST_FORT_OF_ABELRUTH');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2527, '영혼의 시험장', 'RCLIENT_MSG_REGION_QUEST_TRIAL_OF_THE_SOUL');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2528, '암흑사제 사원 외부', 'RCLIENT_MSG_REGION_QUEST_EXTERIOR_OF_DARK_PRIEST_TEMPLE');
GO

INSERT INTO [dbo].[TblRegionQuest] ([mQuestNo], [mQuestNm], [mQuestNmKey]) VALUES (2529, '암흑사제 사원 내부', 'RCLIENT_MSG_REGION_QUEST_INTERIOR_OF_DARK_PRIEST_TEMPLE');
GO

