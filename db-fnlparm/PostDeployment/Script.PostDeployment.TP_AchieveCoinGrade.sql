/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_AchieveCoinGrade
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (1, 0);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (2, 138);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (3, 200);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (4, 247);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (5, 287);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (6, 321);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (7, 352);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (8, 381);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (9, 408);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (10, 433);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (11, 456);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (12, 479);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (13, 500);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (14, 521);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (15, 541);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (16, 560);
GO

INSERT INTO [dbo].[TP_AchieveCoinGrade] ([mGrade], [mCoinPoint]) VALUES (17, 578);
GO

