/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblQuestReward
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1, 150, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (2, 300, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (4, 0, 1006, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (4, 500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (5, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (5, 0, 1164, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (5, 0, 1499, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (5, 800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (6, 0, 2878, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (6, 800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (7, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (7, 100, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (8, 500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (9, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (9, 0, 1164, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (9, 0, 1499, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (9, 700, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (10, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (10, 0, 1164, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (10, 0, 1499, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (10, 2400, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (11, 3000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (12, 25000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (13, 0, 865, 50, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (13, 15000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (14, 15000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (15, 0, 865, 50, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (15, 20000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (16, 0, 4384, 2, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (16, 20000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (17, 25000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (18, 0, 4384, 2, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (18, 25000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (19, 170000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (20, 200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (21, 300000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (22, 1000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (23, 2500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (24, 3000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (25, 3800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (26, 4500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (27, 5500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (28, 6500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (29, 0, 3500, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (29, 0, 3501, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (29, 10000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (30, 0, 2491, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (31, 0, 2492, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (32, 0, 2493, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (33, 0, 2494, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (34, 0, 2495, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (35, 0, 3357, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (36, 0, 3358, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (37, 0, 409, 500, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (37, 5000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (38, 0, 813, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (38, 10000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (39, 0, 978, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (39, 10000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (40, 0, 5296, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (40, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (41, 0, 5302, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (41, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (42, 0, 5308, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (42, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (43, 0, 5314, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (43, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (44, 0, 5318, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (44, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (45, 0, 2694, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (45, 1000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (46, 0, 2704, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (46, 1000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (47, 0, 1493, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (47, 2000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (48, 0, 815, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (48, 20000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (49, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (342, 0, 865, 50, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (342, 9000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (343, 0, 1170, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (343, 13000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (344, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (344, 17000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (345, 0, 5329, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (345, 21000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (346, 25000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (347, 0, 489, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (347, 29000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (348, 0, 1164, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (348, 0, 1499, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (348, 33000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (349, 0, 5614, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (349, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (382, 0, 40600, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (436, 0, 922, 10, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (436, 50000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (437, 0, 1481, 100, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (437, 50000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (438, 0, 1522, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (438, 100000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (439, 0, 1522, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (439, 100000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (440, 0, 922, 10, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (440, 100000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (441, 0, 922, 10, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (441, 100000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (442, 0, 1522, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (442, 100000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (443, 0, 1522, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (443, 100000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (444, 0, 4422, 1, 1, 1, 1, 7);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (444, 837000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (445, 0, 4430, 1, 1, 1, 1, 7);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (445, 837000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (446, 0, 4434, 1, 1, 1, 1, 7);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (446, 837000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (447, 0, 4435, 1, 1, 1, 1, 7);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (447, 837000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (448, 0, 4442, 1, 1, 1, 1, 7);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (448, 837000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (449, 0, 4436, 1, 1, 1, 1, 7);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (449, 837000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (450, 0, 4442, 1, 1, 1, 1, 7);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (450, 1700000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (451, 0, 4442, 1, 1, 1, 1, 7);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (451, 1700000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (471, 0, 3977, 2, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (471, 200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (472, 250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (473, 300000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (474, 0, 3977, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (474, 250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (475, 300000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (476, 350000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (477, 0, 3977, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (477, 400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (478, 500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (479, 600000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (480, 750000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (481, 200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (482, 0, 1210, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (482, 200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (483, 150000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (484, 250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (485, 500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (486, 0, 4907, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (486, 700000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (487, 150000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (488, 0, 5942, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (488, 260000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (489, 150000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (490, 0, 2445, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (490, 0, 2446, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (490, 0, 5946, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (490, 250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (491, 250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (492, 150000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (493, 0, 4916, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (493, 800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (494, 250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (495, 0, 5943, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (495, 300000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (496, 200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (497, 400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (498, 500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (499, 0, 5945, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (499, 500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (500, 0, 4919, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (500, 1000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (501, 700000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (502, 700000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (503, 0, 5944, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (503, 2500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (504, 500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (505, 1000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (506, 1000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (507, 0, 5972, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (507, 2000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (508, 0, 4919, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (508, 2000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (509, 400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (510, 2000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (511, 500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (512, 700000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (513, 0, 2777, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (513, 2000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (514, 900000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (515, 2000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (516, 500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (517, 2000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (518, 2000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (519, 400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (520, 700000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (521, 0, 2777, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (521, 2500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (522, 800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (523, 1000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (524, 400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (525, 0, 5961, 1, 1, 1, 0, 10);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (525, 800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (526, 0, 2777, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (526, 1500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (527, 2500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (528, 2700000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (529, 50000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (530, 4500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (531, 4500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (532, 0, 2777, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (532, 4500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (540, 2092500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (541, 2092500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (542, 3804400, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (543, 1711950, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (544, 4500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (545, 989100, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (546, 0, 4910, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (546, 4500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (547, 2124800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (548, 2124800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (549, 2124800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (550, 2124800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (551, 0, 3971, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (551, 4500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (552, 2550000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (553, 2550000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (554, 4000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (555, 2400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (556, 0, 5681, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (556, 4500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (557, 7500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (558, 9000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (559, 2550000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (560, 5250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (561, 0, 2584, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (561, 6200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (562, 7500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (563, 10000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (564, 0, 6465, 1, 1, 1, 0, 10);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (564, 12000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (565, 7500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (566, 10000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (567, 0, 6533, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (567, 12000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (578, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (578, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (579, 0, 1164, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (579, 300, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (580, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (580, 600, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (581, 0, 1499, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (581, 800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (582, 0, 1170, 3, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (582, 1500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (583, 0, 1006, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (583, 3600, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (584, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (584, 800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (585, 0, 988, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (585, 1500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (586, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (586, 2500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (587, 0, 2878, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (587, 5000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (588, 0, 1499, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (588, 800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (589, 0, 1170, 3, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (589, 1500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (590, 0, 1006, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (590, 3600, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (591, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (591, 800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (592, 0, 778, 1000, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (592, 0, 1176, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (592, 1500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (593, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (593, 2500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (594, 0, 2878, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (594, 5000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (595, 0, 1499, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (595, 800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (596, 0, 1170, 3, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (596, 1500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (597, 0, 1006, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (597, 3600, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (598, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (598, 800, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (599, 0, 988, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (599, 1500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (600, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (600, 2500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (601, 0, 2878, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (601, 5000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (602, 0, 829, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (603, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (603, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (604, 0, 1164, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (604, 300, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (605, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (605, 700, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (606, 0, 1499, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (606, 500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (607, 0, 1170, 3, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (607, 1000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (608, 0, 1006, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (608, 2000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (609, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (609, 500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (610, 1000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (611, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (611, 1500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (612, 0, 2878, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (612, 4500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (615, 100, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (616, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (617, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (618, 400, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (619, 700, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (620, 1000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (621, 400, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (622, 700, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (623, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (623, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (624, 0, 1164, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (624, 300, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (625, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (625, 600, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (626, 100, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (627, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (628, 400, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (629, 700, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (630, 1000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (631, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (632, 400, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (633, 700, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (634, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (634, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (635, 0, 1164, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (635, 300, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (636, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (636, 600, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (637, 100, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (638, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (639, 400, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (640, 700, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (641, 1000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (642, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (643, 400, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (644, 700, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (645, 100, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (646, 150, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (647, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (648, 300, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (649, 450, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (650, 600, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (651, 100, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (652, 200, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (653, 300, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (654, 500, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (655, 5000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (656, 5000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (657, 5000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (662, 4000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (663, 5000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (664, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (665, 6000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (666, 0, 865, 30, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (666, 15000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (667, 7000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (668, 8000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (669, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (669, 18000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (670, 0, 1164, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (670, 20000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (671, 0, 1499, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (671, 25000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (672, 12000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (673, 0, 5329, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (673, 30000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (674, 15000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (675, 0, 5614, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (675, 35000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (676, 20000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (677, 38000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (678, 0, 1170, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (678, 24000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (679, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (681, 0, 4961, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (683, 0, 4964, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (685, 0, 4961, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (687, 0, 4961, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (690, 0, 6678, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (691, 0, 6679, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (692, 0, 865, 50, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (692, 50000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (693, 20000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (694, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (694, 55000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (695, 25000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (696, 0, 5557, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (696, 60000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (697, 30000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (698, 0, 5329, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (698, 70000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (699, 80000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (700, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (701, 0, 5559, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (701, 90000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (702, 50000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (703, 0, 865, 50, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (703, 105000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (704, 65000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (705, 0, 1998, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (705, 120000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (706, 75000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (707, 0, 5614, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (707, 130000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (708, 85000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (709, 0, 5542, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (709, 150000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (710, 0, 1170, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (710, 90000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (711, 170000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (712, 150000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (713, 0, 5566, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (713, 190000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (714, 160000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (715, 0, 5567, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (715, 210000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (716, 170000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (717, 0, 5568, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (717, 230000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (718, 190000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (719, 0, 5570, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (719, 250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (720, 210000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (721, 0, 5569, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (721, 270000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (722, 230000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (723, 250000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (724, 0, 5542, 5, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (724, 290000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (725, 270000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (726, 0, 4392, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (726, 290000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (727, 310000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (728, 0, 860, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (728, 320000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (729, 300000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (730, 200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (731, 0, 5614, 15, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (731, 330000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (732, 380000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (733, 0, 5325, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (733, 300000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (734, 420000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (735, 0, 5558, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (735, 350000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (736, 450000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (737, 0, 5324, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (737, 460000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (738, 470000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (739, 0, 5560, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (739, 480000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (740, 490000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (741, 0, 5327, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (741, 500000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (742, 550000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (743, 0, 3977, 10, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (743, 600000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (744, 650000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (745, 0, 3990, 3, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (745, 660000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (746, 40000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (748, 0, 4958, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (750, 0, 4958, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (752, 0, 4964, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (754, 0, 4958, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (757, 0, 6980, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (760, 0, 6981, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (761, 0, 6976, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (762, 0, 6977, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (769, 0, 4954, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (772, 0, 4943, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (776, 0, 4955, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (779, 0, 4943, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (784, 0, 2583, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (790, 0, 4018, 1, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (790, 240000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (796, 0, 4018, 2, 0, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (796, 280000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (797, 10000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (798, 13400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (799, 13800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (800, 14200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (801, 14600000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (802, 15000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (803, 15400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (804, 15800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (805, 16200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (806, 0, 4947, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (806, 0, 4951, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (806, 0, 4955, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (806, 17000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (807, 17400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (808, 17800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (809, 18200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (810, 18600000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (811, 19000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (812, 19400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (813, 19800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (814, 0, 4935, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (814, 0, 4939, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (814, 0, 4943, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (814, 21000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (815, 17400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (816, 17800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (817, 18200000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (818, 18600000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (819, 19000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (820, 19400000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (821, 19800000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (822, 0, 4935, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (822, 0, 4939, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (822, 0, 4943, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (822, 21000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (831, 0, 7423, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (834, 0, 7578, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (834, 29000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (835, 0, 7576, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (837, 0, 4964, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (839, 0, 6197, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (841, 0, 4961, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (843, 0, 6199, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (845, 0, 4961, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (847, 0, 6202, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (848, 27000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (849, 28000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (850, 29000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (851, 0, 4924, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (851, 0, 4928, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (851, 0, 4932, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (851, 32000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (852, 27000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (853, 28000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (854, 29000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (855, 0, 4948, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (855, 0, 4952, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (855, 0, 4956, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (855, 32000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (856, 6000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (857, 6000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (865, 10000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (866, 20000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (867, 0, 4958, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (867, 24000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (868, 0, 4964, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (868, 28000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (869, 0, 4961, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (869, 32000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (870, 0, 2777, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (870, 0, 8383, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (870, 36000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (871, 20000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (872, 0, 4958, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (872, 24000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (873, 0, 4964, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (873, 28000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (874, 0, 4961, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (874, 32000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (875, 0, 2777, 1, 1, 1, 0, 1);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (875, 0, 8382, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (875, 36000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (876, 6243457, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (877, 9843457, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (878, 13017972, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (879, 17216268, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (880, 22768514, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (881, 5882593, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (882, 8489982, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (883, 11228001, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (884, 14849031, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (885, 17076385, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (886, 19637843, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (887, 22583520, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (888, 29866705, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (889, 39842184, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (890, 53611643, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (891, 72139827, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (892, 17314032, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (893, 22897807, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (894, 30811289, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (895, 41459671, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (896, 48093218, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (897, 57230929, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1000, 8300000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1001, 8300000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1002, 0, 4910, 1, 1, 1, 0, 3);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1002, 8300000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1003, 0, 5687, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1003, 11100000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1004, 0, 8448, 1, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1004, 19500000000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1005, 12000, 0, 0, 0, 0, 0, 0);
GO

INSERT INTO [dbo].[TblQuestReward] ([mRewardNo], [mExp], [mID], [mCnt], [mBinding], [mStatus], [mEffTime], [mValTime]) VALUES (1006, 500000, 0, 0, 0, 1, 0, 0);
GO

