/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblJackpotRewardList
Date                  : 2023-10-07 09:08:58
*/


INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 6477, 8.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 6491, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 1637, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3904, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3946, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3890, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3932, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3862, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3876, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3918, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 5723, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 5737, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 5751, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 5765, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 5779, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 1627, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3498, 0.10000000149011612, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7590, 3499, 0.4000000059604645, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5723, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5737, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5751, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5765, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5779, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 1627, 7.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 6477, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 6491, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 1637, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3904, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3946, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3890, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3932, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3862, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3876, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3918, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5723, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5737, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5751, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5765, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 5779, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 1627, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 498, 0.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3498, 0.10000000149011612, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7591, 3499, 0.4000000059604645, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 6477, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 6491, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 1637, 6.900000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3904, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3946, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3890, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3932, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3862, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3876, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3918, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 5723, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 5737, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 5751, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 5765, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 5779, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 1627, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 498, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3498, 0.10000000149011612, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7592, 3499, 0.4000000059604645, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 6477, 8.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 6491, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 1637, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3904, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3946, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3890, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3932, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3862, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3876, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3918, 9.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 5723, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 5737, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 5751, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 5765, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 5779, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 1627, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3498, 0.10000000149011612, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7593, 3499, 0.4000000059604645, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5723, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5737, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5751, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5765, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5779, 8.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 1627, 7.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 6477, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 6491, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 1637, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3904, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3946, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3890, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3932, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3862, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3876, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3918, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5723, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5737, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5751, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5765, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 5779, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 1627, 2.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 498, 0.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3498, 0.10000000149011612, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7594, 3499, 0.4000000059604645, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 6477, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 6491, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 1637, 6.900000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3904, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3946, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3890, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3932, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3862, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3876, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3918, 7.400000095367432, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 5723, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 5737, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 5751, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 5765, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 5779, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 1627, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 498, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3498, 0.10000000149011612, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7595, 3499, 0.4000000059604645, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 6477, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 6491, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 1637, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3904, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3946, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3890, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3932, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3862, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3876, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3918, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 5723, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 5737, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 5751, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 5765, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 5779, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 1627, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3498, 0.20000000298023224, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7650, 3499, 0.800000011920929, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5723, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5737, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5751, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5765, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5779, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 1627, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 6477, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 6491, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 1637, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3904, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3946, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3890, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3932, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3862, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3876, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3918, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5723, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5737, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5751, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5765, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 5779, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 1627, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 498, 1.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 6295, 1.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3498, 0.20000000298023224, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7651, 3499, 0.800000011920929, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 6477, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 6491, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 1637, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3904, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3946, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3890, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3932, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3862, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3876, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3918, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 5723, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 5737, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 5751, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 5765, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 5779, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 1627, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 498, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 6295, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3498, 0.20000000298023224, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7652, 3499, 0.800000011920929, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 6477, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 6491, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 1637, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3904, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3946, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3890, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3932, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3862, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3876, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3918, 7.800000190734863, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 5723, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 5737, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 5751, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 5765, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 5779, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 1627, 3.5, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3498, 0.20000000298023224, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7653, 3499, 0.800000011920929, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5723, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5737, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5751, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5765, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5779, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 1627, 5.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 6477, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 6491, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 1637, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3904, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3946, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3890, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3932, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3862, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3876, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3918, 4.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5723, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5737, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5751, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5765, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 5779, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 1627, 4.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 498, 1.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 6295, 1.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3498, 0.20000000298023224, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7654, 3499, 0.800000011920929, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 6477, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 6491, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 1637, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3904, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3946, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3890, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3932, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3862, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3876, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3918, 6.5, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 5723, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 5737, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 5751, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 5765, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 5779, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 1627, 5.0, 2, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 498, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 6295, 2.0, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3498, 0.20000000298023224, 1, 1);
GO

INSERT INTO [dbo].[TblJackpotRewardList] ([mConfirmIID], [mIID], [mRate], [mItemStatus], [mCnt]) VALUES (7655, 3499, 0.800000011920929, 1, 1);
GO

