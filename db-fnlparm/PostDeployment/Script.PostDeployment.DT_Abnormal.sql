/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Abnormal
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (4, 10, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (5, 11, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (6, 12, 1, 1, 300000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (7, 13, 1, 1, 600000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (8, 14, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (9, 3, 1, 76, 30000, 0, '中毒.\n\nHP慢慢减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (10, 3, 2, 61, 30000, 0, '中毒.\n\nHP慢慢减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (11, 3, 3, 51, 30000, 0, '中毒.\n\nHP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (12, 3, 4, 26, 20000, 0, '中毒.\n\nHP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (13, 4, 1, 96, 120000, 0, '生病.\n\nHP减少, 力量大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (14, 4, 2, 91, 120000, 0, '生病.\n\nHP减少, 力量大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (15, 4, 3, 86, 120000, 0, '生病.\n\nHP减少, 力量大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (16, 4, 4, 81, 120000, 0, '生病.\n\nHP减少, 力量大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (17, 5, 1, 76, 60000, 0, '出血.\n\n每秒HP少量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (18, 5, 2, 66, 40000, 0, '出血.\n\n每秒HP少量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (19, 5, 3, 51, 50000, 0, '出血.\n\n每秒HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (20, 5, 4, 26, 60000, 0, '出血.\n\n每秒HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (21, 1, 1, 91, 20000, 0, '命中率和防御力小幅下降.\n\n防御力 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (22, 1, 2, 76, 30000, 0, '命中率和防御力下降.\n\n防御力 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (23, 1, 3, 61, 100000, 0, '命中率和防御力下降.\n\n防御力 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (24, 1, 4, 51, 100000, 0, '命中率和防御力大幅下降.\n\n防御力 -4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (25, 2, 1, 61, 18000, 0, '全身麻痹.\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (26, 2, 2, 51, 20000, 0, '全身麻痹.\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (27, 2, 3, 46, 25000, 0, '全身麻痹.\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (28, 2, 4, 41, 30000, 0, '全身麻痹.\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (30, 7, 1, 91, 30000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (34, 7, 2, 81, 30000, 0, '攻击速度大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (35, 7, 3, 71, 30000, 0, '攻击速度大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (36, 7, 4, 61, 5000, 0, '攻击速度大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (49, 8, 1, 71, 10000, 0, '全身难受.\n\n移动速度减慢.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (50, 8, 2, 41, 10000, 0, '全身难受.\n\n移动速度减慢.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (51, 8, 3, 41, 15000, 0, '全身难受.\n\n移动速度减慢.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (52, 8, 4, 1, 15000, 0, '全身难受.\n\n移动速度减慢.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (53, 15, 1, 81, 30000, 0, '攻击速度和力量大幅下降.\n\n力量 -6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (54, 15, 2, 81, 30000, 0, '攻击速度和力量大幅下降.\n\n力量 -9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (55, 15, 3, 81, 60000, 0, '攻击速度和力量大幅下降.\n\n力量 -12');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (56, 15, 4, 81, 30000, 0, '攻击速度和力量大幅下降.\n\n力量 -15');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (57, 16, 1, 26, 30000, 0, '喊不出声音.\n\n无法使用魔法及技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (58, 16, 2, 41, 30000, 0, '喊不出声音.\n\n无法使用魔法及技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (59, 16, 3, 51, 30000, 0, '喊不出声音.\n\n无法使用魔法及技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (60, 16, 4, 76, 30000, 0, '喊不出声音.\n\n无法使用魔法及技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (61, 6, 1, 91, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (62, 6, 2, 86, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (63, 6, 3, 71, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (64, 6, 4, 51, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (65, 17, 1, 1, 300000, 0, '力量上升.\n\n力量 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (66, 19, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (67, 20, 1, 1, 300000, 0, '强化对火焰的抵抗力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (68, 21, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (69, 18, 1, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (70, 11, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (71, 22, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (72, 23, 1, 1, 300000, 0, '强化对寒冷的抵抗力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (73, 23, 2, 1, 300000, 0, '强化对寒冷的抵抗力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (74, 23, 3, 1, 300000, 0, '强化对寒冷的抵抗力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (75, 10, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (76, 10, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (78, 32, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (79, 33, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (80, 31, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (81, 34, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (84, 35, 1, 1, 0, 0, '回城中.\n\n移动到附近的城镇.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (85, 36, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (86, 37, 1, 1, 0, 0, '可以看见隐身状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (87, 38, 1, 1, 86400000, 0, '可以看见隐身状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (88, 39, 1, 1, 0, 0, '可传送到你想去的地方.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (89, 40, 1, 1, 0, 0, '可变身为你想要的模样.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (90, 11, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (91, 11, 4, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (92, 11, 5, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (93, 26, 11, 1, 1200000, 0, '用魔法的力量增加防御力.\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (94, 26, 2, 1, 1200000, 0, '用魔法的力量增加防御力.\n\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (95, 26, 3, 1, 1200000, 0, '用魔法的力量增加防御力.\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (96, 27, 11, 1, 1200000, 0, '全身像岩石一样坚硬.\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (97, 27, 2, 1, 1200000, 0, '全身像岩石一样坚硬.\n\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (98, 27, 3, 1, 1200000, 0, '全身像岩石一样坚硬.\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (99, 30, 11, 1, 1200000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (100, 30, 2, 1, 1200000, 0, '精神爽快提高集中力.\n\n命中率增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (101, 30, 3, 1, 1200000, 0, '精神爽快提高集中力.\n\n命中率小幅增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (102, 41, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (103, 42, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (104, 43, 1, 1, 300000, 0, '攻击力小幅上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (105, 44, 11, 1, 1200000, 0, '神圣的力量在邪恶的气息中保护你全身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (106, 45, 1, 1, 1200000, 0, '对各种属性攻击的抵抗力变高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (107, 46, 1, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (113, 47, 1, 1, 1200000, 0, '攻击速度,移动速度增加.\n\n力量 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (115, 48, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (116, 43, 2, 1, 300000, 0, '攻击力上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (117, 43, 3, 1, 300000, 0, '攻击力上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (118, 49, 1, 1, 300000, 0, '命中率小幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (119, 49, 2, 1, 300000, 0, '命中率上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (120, 49, 3, 1, 300000, 0, '命中率大幅上升..');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (121, 48, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (122, 50, 1, 1, 300000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (123, 24, 1, 1, 300000, 0, '敏捷上升.\n\n敏捷 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (124, 25, 1, 1, 300000, 0, '智力上升.\n\n智力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (130, 52, 1, 26, 300000, 0, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (131, 15, 5, 26, 90000, 0, '攻击速度大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (141, 55, 1, 1, 300000, 0, '近战伤害小幅减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (142, 56, 1, 1, 300000, 0, '远程攻击伤害小幅减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (143, 57, 1, 1, 300000, 0, '防御力下降.\n\n防御力 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (144, 54, 1, 1, 2400000, 0, '防御力上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (145, 58, 1, 1, 300000, 0, '未体现');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (147, 59, 1, 1, 300000, 0, '未体现');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (148, 60, 1, 1, 300000, 0, '魔法命中率大幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (149, 61, 1, 1, 300000, 0, '魔法命中率小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (150, 62, 1, 1, 300000, 0, '未体现');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (151, 63, 1, 1, 300000, 0, '未体现');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (152, 58, 2, 1, 1800000, 0, '远程攻击命中率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (153, 59, 2, 1, 1800000, 0, '近战命中率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (154, 59, 3, 1, 1800000, 0, '近战命中率大幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (155, 62, 2, 1, 1800000, 0, '远程攻击力增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (156, 60, 2, 1, 1800000, 0, '魔法命中率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (159, 64, 2, 1, 3600000, 0, 'HP 恢复量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (160, 65, 1, 1, 180000, 0, 'MP 恢复量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (161, 53, 1, 1, 2400000, 0, '攻击力增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (165, 66, 1, 1, 3600000, 0, '攻击力小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (166, 67, 1, 1, 3600000, 0, '防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (167, 12, 2, 1, 300000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (168, 12, 3, 1, 300000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (169, 17, 2, 1, 600000, 0, '力量上升.\n\n力量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (170, 24, 2, 1, 600000, 0, '力量上升.\n\n力量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (171, 25, 2, 1, 600000, 0, '力量上升.\n\n力量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (172, 68, 1, 1, 600000, 0, '负重上线增加.\n\n负重 +250');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (173, 46, 2, 1, 600000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (175, 67, 2, 1, 1800000, 0, '防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (176, 69, 1, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (177, 70, 1, 1, 60000, 0, '力量增加.\n\n力量 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (178, 70, 2, 16, 0, 0, '未体现');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (179, 71, 1, 1, 60000, 0, '防御力增加.\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (180, 72, 1, 1, 30000, 0, '狂奔状态.\n\n防御力 -4\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (181, 73, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (182, 74, 1, 1, 20000, 0, '一定时间段内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (183, 75, 1, 1, 180000, 0, '敏捷 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (184, 65, 2, 1, 1200000, 0, '负重状态 MP恢复 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (185, 64, 1, 1, 3600000, 0, 'HP 恢复量 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (186, 68, 2, 1, 36000, 0, '负重上线增加.\n\n负重 +200');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (187, 77, 1, 1, 30000, 0, 'HP 恢复量 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (188, 18, 2, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (189, 18, 3, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (190, 82, 1, 1, 0, 0, 'MP +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (191, 81, 1, 1, 2400000, 0, '负重状态 MP恢复 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (192, 80, 1, 1, 2400000, 0, 'HP 恢复 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (193, 25, 3, 1, 30000, 0, '智力上升.\n\n智力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (194, 78, 1, 1, 30000, 0, '防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (195, 76, 1, 1, 180000, 0, '攻击速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (196, 83, 1, 1, 60000, 0, '智力增加.\n\n智力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (197, 84, 1, 1, 60000, 0, '身体变轻.\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (198, 85, 1, 1, 1200000, 0, 'MP 恢复 +2\n\n负重状态 MP恢复 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (199, 60, 3, 1, 1800000, 0, '魔法命中率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (200, 57, 2, 1, 300000, 0, '防御力减少.\n\n防御力 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (201, 13, 2, 1, 1800000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (202, 30, 4, 1, 1800000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (204, 13, 3, 1, 900000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (205, 42, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (206, 40, 1, 1, 0, 0, '可变身为你想要的模样.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (207, 46, 3, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (210, 86, 1, 96, 15000, 0, '中毒.\n\n每秒 HP少量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (211, 87, 1, 96, 1000, 0, '中了猛毒.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (212, 86, 2, 96, 15000, 0, '中毒.\n\n每秒 HP少量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (213, 86, 3, 96, 15000, 0, '中毒.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (214, 87, 2, 96, 1000, 0, '中了猛毒.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (215, 87, 3, 96, 1000, 0, '中了猛毒.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (216, 51, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (217, 51, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (218, 51, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (219, 51, 4, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (220, 88, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (221, 89, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (224, 3, 51, 1, 120000, 0, '中了猛毒.\n\n每秒 HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (225, 52, 51, 1, 120000, 0, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (227, 90, 50, 1, 86400000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (228, 43, 4, 1, 20000, 0, '攻击力和命中率大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (229, 1, 5, 96, 10000, 0, '命中率和防御力减少.\n\n防御力 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (230, 77, 2, 1, 0, 0, 'HP 恢复量 +20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (231, 82, 2, 1, 0, 0, 'MP 恢复 +20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (232, 68, 3, 1, 1200000, 0, '负重的上线增加.\n\n负重 +300');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (233, 91, 1, 1, 20000, 0, '移动速度大幅上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (234, 92, 49, 1, 600000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (236, 94, 1, 1, 180000, 0, '使自己隐身逃出敌人的视线.\n\n近距离可被别人发现.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (237, 95, 1, 1, 90000, 0, '被敌人发现.\n\n不可隐身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (238, 96, 1, 1, 15000, 0, '减轻身体回避率大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (240, 98, 1, 1, 600000, 0, '下一次攻击使用强烈一击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (241, 99, 49, 1, 15000, 0, '晕过去了.\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (242, 93, 1, 1, 240000, 0, '移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (244, 100, 49, 1, 4000, 0, '无敌状态.\n\n无法移动, 不被敌人攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (245, 101, 1, 96, 30000, 0, '中了曼陀铃毒.\n\n在施展魔法时将会收到制约.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (246, 68, 4, 1, 1200000, 0, '负重上线增加.\n\n负重 +500');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (248, 102, 49, 96, 30000, 0, '中了狼蛛的毒.\n\n移动速度减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (253, 103, 1, 1, 0, 0, '毒瓶使用中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (254, 103, 2, 1, 0, 0, '猛毒瓶使用中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (255, 103, 3, 1, 0, 0, '狼蛛毒瓶使用中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (256, 103, 4, 1, 0, 0, '曼陀铃毒瓶使用中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (258, 97, 47, 1, 30000, 0, '中毒I阶段.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (259, 18, 4, 1, 1200000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (260, 43, 5, 1, 60000, 0, '攻击力上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (261, 60, 4, 1, 600000, 0, '魔法命中率大幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (262, 67, 3, 1, 900000, 0, '未体现');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (264, 104, 51, 1, 86000000, 0, '杀人犯状态.\n\n攻击和防御中受不利影响.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (267, 77, 3, 1, 600000, 0, 'HP 恢复 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (268, 17, 3, 1, 600000, 0, '力量上升.\n\n力量 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (269, 24, 3, 1, 600000, 0, '敏捷上升.\n\n敏捷 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (270, 25, 4, 1, 600000, 0, '智力上升.\n\n智力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (271, 86, 4, 1, 15000, 0, '中毒.\n\nHP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (272, 19, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (273, 19, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (274, 87, 4, 96, 0, 0, '中了猛毒.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (275, 87, 5, 96, 0, 0, '中了猛毒.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (277, 105, 1, 1, 0, 0, 'HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (278, 106, 1, 1, 0, 0, 'HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (279, 107, 1, 1, 0, 0, 'HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (280, 108, 1, 1, 1800000, 0, '暴击率大幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (281, 19, 4, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (284, 86, 5, 1, 30000, 0, '中毒.\n\n每秒 HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (285, 109, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (286, 110, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (287, 87, 6, 1, 0, 0, '中了猛毒.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (288, 31, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (293, 103, 51, 1, 0, 0, '毒瓶使用中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (294, 103, 52, 1, 0, 0, '猛毒瓶使用中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (295, 103, 53, 1, 0, 0, '狼蛛毒瓶使用中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (296, 103, 54, 1, 0, 0, '曼陀铃毒瓶使用中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (297, 12, 4, 1, 600000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (298, 13, 4, 1, 1800000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (299, 111, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (300, 112, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (301, 19, 5, 1, 0, 0, '力量上升.\n\n力量 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (302, 87, 7, 1, 0, 0, '中了猛毒.\n\n每秒 HP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (303, 113, 51, 1, 82800000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (308, 18, 99, 1, 43200000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (309, 46, 8, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (310, 17, 10, 1, 600000, 0, '力量上升.\n\n力量 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (311, 24, 10, 1, 600000, 0, '敏捷上升.\n\n敏捷 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (312, 25, 10, 1, 600000, 0, '智力上升.\n\n智力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (315, 68, 5, 1, 3600000, 0, '负重上线增加.\n\n负重 +1000');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (316, 77, 4, 1, 3600000, 0, '负重状态 HP恢复 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (317, 46, 4, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (318, 46, 9, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (319, 46, 25, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (320, 19, 6, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (322, 7, 5, 26, 15000, 0, '攻击速度大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (324, 109, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (325, 52, 2, 1, 45000, 0, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (326, 77, 10, 1, 600000, 0, '负重状态 HP恢复 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (327, 65, 2, 1, 600000, 0, '负重状态  MP恢复 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (328, 108, 2, 1, 600000, 0, '暴击率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (329, 54, 10, 1, 0, 0, '未体现');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (330, 67, 10, 1, 600000, 0, '防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (331, 17, 11, 1, 600000, 0, '力量上升.\n\n力量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (332, 25, 11, 1, 600000, 0, '智力上升.\n\n智力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (333, 24, 11, 1, 600000, 0, '敏捷上升.\n\n敏捷 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (334, 61, 10, 1, 600000, 0, '近战攻击力小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (335, 59, 10, 1, 600000, 0, '近战攻击力小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (336, 62, 10, 1, 600000, 0, '远程攻击力小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (337, 58, 10, 1, 600000, 0, '远程攻击命中率小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (338, 63, 10, 1, 600000, 0, '魔法攻击力小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (339, 60, 10, 1, 600000, 0, '魔法攻击力小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (342, 52, 10, 96, 2500, 0, '移动速度大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (343, 17, 15, 1, 1200000, 0, '力量上升.\n\n力量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (344, 24, 15, 1, 1200000, 0, '敏捷上升.\n\n敏捷 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (345, 25, 15, 1, 1200000, 0, '智力上升.\n\n智力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (346, 18, 10, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (347, 18, 11, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (348, 114, 110, 1, 36000000, 0, 'MP 消耗减少 5%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (349, 114, 111, 1, 36000000, 0, 'MP 消耗减少 7%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (350, 114, 112, 1, 36000000, 0, 'MP 消耗减少 9%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (351, 114, 113, 1, 36000000, 0, 'MP 消耗减少 11%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (352, 114, 114, 1, 36000000, 0, 'MP 消耗减少 13%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (353, 114, 115, 1, 36000000, 0, 'MP 消耗减少 15%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (354, 114, 116, 1, 36000000, 0, 'MP 消耗减少 10%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (355, 114, 117, 1, 36000000, 0, 'MP 消耗减少 12%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (356, 114, 118, 1, 36000000, 0, 'MP 消耗减少 14%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (357, 114, 119, 1, 36000000, 0, 'MP 消耗减少 16%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (358, 65, 3, 1, 600000, 0, '负重状态 MP恢复 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (359, 19, 7, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (360, 18, 12, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (361, 18, 13, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (362, 18, 14, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (363, 18, 15, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (364, 115, 1, 1, 0, 0, '狩猎时获得经验值增加 1% ');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (365, 116, 1, 1, 0, 0, '狩猎时物品掉落几率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (366, 115, 2, 1, 0, 0, '狩猎时获得经验值增加 2%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (367, 115, 3, 1, 0, 0, '狩猎时获得经验值增加 3%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (368, 115, 4, 1, 0, 0, '狩猎时获得经验值增加 4%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (369, 115, 5, 1, 0, 0, '狩猎时获得经验值增加 5%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (370, 115, 6, 1, 0, 0, '狩猎时获得经验值增加 6%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (371, 115, 7, 1, 0, 0, '狩猎时获得经验值增加 7%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (372, 115, 8, 1, 0, 0, '狩猎时获得经验值增加 8%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (373, 115, 9, 1, 0, 0, '狩猎时获得经验值增加 9%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (374, 115, 10, 1, 0, 0, '狩猎时获得经验值增加 10%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (375, 115, 11, 1, 0, 0, '狩猎时获得经验值增加 11%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (376, 115, 12, 1, 0, 0, '狩猎时获得经验值增加 12%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (377, 115, 13, 1, 0, 0, '狩猎时获得经验值增加 13%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (378, 115, 14, 1, 0, 0, '狩猎时获得经验值增加 14%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (379, 115, 15, 1, 0, 0, '狩猎时获得经验值增加 15%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (380, 115, 16, 1, 0, 0, '狩猎时获得经验值增加 16%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (381, 115, 17, 1, 0, 0, '狩猎时获得经验值增加 17%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (382, 115, 18, 1, 0, 0, '狩猎时获得经验值增加 18%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (383, 115, 19, 1, 0, 0, '狩猎时获得经验值增加 19%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (384, 115, 20, 1, 0, 0, '狩猎时获得经验值增加 20%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (385, 115, 21, 1, 0, 0, '狩猎时获得经验值增加 21%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (386, 115, 22, 1, 0, 0, '狩猎时获得经验值增加 22%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (387, 115, 23, 1, 0, 0, '狩猎时获得经验值增加 23%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (388, 115, 24, 1, 0, 0, '狩猎时获得经验值增加 24%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (389, 115, 25, 1, 0, 0, '狩猎时获得经验值增加 25%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (390, 115, 26, 1, 0, 0, '狩猎时获得经验值增加 26%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (391, 115, 27, 1, 0, 0, '狩猎时获得经验值增加 27%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (392, 115, 28, 1, 0, 0, '狩猎时获得经验值增加 28%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (393, 115, 29, 1, 0, 0, '狩猎时获得经验值增加 29%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (394, 115, 30, 1, 0, 0, '狩猎时获得经验值增加 30%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (395, 115, 31, 1, 0, 0, '狩猎时获得经验值增加 31%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (396, 115, 32, 1, 0, 0, '狩猎时获得经验值增加 32%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (397, 115, 33, 1, 0, 0, '狩猎时获得经验值增加 33%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (398, 115, 34, 1, 0, 0, '狩猎时获得经验值增加 34%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (399, 115, 35, 1, 0, 0, '狩猎时获得经验值增加 35%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (400, 115, 36, 1, 0, 0, '狩猎时获得经验值增加 36%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (401, 115, 37, 1, 0, 0, '狩猎时获得经验值增加 37%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (402, 115, 38, 1, 0, 0, '狩猎时获得经验值增加 38%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (403, 115, 39, 1, 0, 0, '狩猎时获得经验值增加 39%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (404, 115, 40, 1, 0, 0, '狩猎时获得经验值增加 40%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (405, 115, 41, 1, 0, 0, '狩猎时获得经验值增加 41%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (406, 115, 42, 1, 0, 0, '狩猎时获得经验值增加 42%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (407, 115, 43, 1, 0, 0, '狩猎时获得经验值增加 43%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (408, 115, 44, 1, 0, 0, '狩猎时获得经验值增加 44%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (409, 115, 45, 1, 0, 0, '狩猎时获得经验值增加 45%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (410, 115, 46, 1, 0, 0, '狩猎时获得经验值增加 46%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (411, 115, 47, 1, 0, 0, '狩猎时获得经验值增加 47%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (412, 115, 48, 1, 0, 0, '狩猎时获得经验值增加 48%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (413, 115, 49, 1, 0, 0, '狩猎时获得经验值增加 49%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (414, 115, 50, 1, 0, 0, '狩猎时获得经验值增加 50%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (415, 115, 51, 1, 0, 0, '狩猎时获得经验值增加 51%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (416, 115, 52, 1, 0, 0, '狩猎时获得经验值增加 52%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (417, 115, 53, 1, 0, 0, '狩猎时获得经验值增加 53%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (418, 115, 54, 1, 0, 0, '狩猎时获得经验值增加 54%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (419, 115, 55, 1, 0, 0, '狩猎时获得经验值增加 55%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (420, 115, 56, 1, 0, 0, '狩猎时获得经验值增加 56%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (421, 115, 57, 1, 0, 0, '狩猎时获得经验值增加 57%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (422, 115, 58, 1, 0, 0, '狩猎时获得经验值增加 58%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (423, 115, 59, 1, 0, 0, '狩猎时获得经验值增加 59%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (424, 115, 60, 1, 0, 0, '狩猎时获得经验值增加 60%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (425, 115, 61, 1, 0, 0, '狩猎时获得经验值增加 61%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (426, 115, 62, 1, 0, 0, '狩猎时获得经验值增加 62%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (427, 115, 63, 1, 0, 0, '狩猎时获得经验值增加 63%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (428, 115, 64, 1, 0, 0, '狩猎时获得经验值增加 64%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (429, 115, 65, 1, 0, 0, '狩猎时获得经验值增加 65%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (430, 115, 66, 1, 0, 0, '狩猎时获得经验值增加 66%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (431, 115, 67, 1, 0, 0, '狩猎时获得经验值增加 67%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (432, 115, 68, 1, 0, 0, '狩猎时获得经验值增加 68%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (433, 115, 69, 1, 0, 0, '狩猎时获得经验值增加 69%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (434, 115, 70, 1, 0, 0, '狩猎时获得经验值增加 70%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (435, 115, 71, 1, 0, 0, '狩猎时获得经验值增加 71%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (436, 115, 72, 1, 0, 0, '狩猎时获得经验值增加 72%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (437, 115, 73, 1, 0, 0, '狩猎时获得经验值增加 73%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (438, 115, 74, 1, 0, 0, '狩猎时获得经验值增加 74%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (439, 115, 75, 1, 0, 0, '狩猎时获得经验值增加 75%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (440, 115, 76, 1, 0, 0, '狩猎时获得经验值增加 76%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (441, 115, 77, 1, 0, 0, '狩猎时获得经验值增加 77%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (442, 115, 78, 1, 0, 0, '狩猎时获得经验值增加 78%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (443, 115, 79, 1, 0, 0, '狩猎时获得经验值增加 79%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (444, 115, 80, 1, 0, 0, '狩猎时获得经验值增加 80%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (445, 115, 81, 1, 0, 0, '狩猎时获得经验值增加 81%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (446, 115, 82, 1, 0, 0, '狩猎时获得经验值增加 82%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (447, 115, 83, 1, 0, 0, '狩猎时获得经验值增加 83%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (448, 115, 84, 1, 0, 0, '狩猎时获得经验值增加 84%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (449, 115, 85, 1, 0, 0, '狩猎时获得经验值增加 85%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (450, 115, 86, 1, 0, 0, '狩猎时获得经验值增加 86%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (451, 115, 87, 1, 0, 0, '狩猎时获得经验值增加 87%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (452, 115, 88, 1, 0, 0, '狩猎时获得经验值增加 88%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (453, 115, 89, 1, 0, 0, '狩猎时获得经验值增加 89%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (454, 115, 90, 1, 0, 0, '狩猎时获得经验值增加 90%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (455, 116, 2, 1, 0, 0, '狩猎时物品掉落几率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (456, 116, 3, 1, 0, 0, '狩猎时物品掉落几率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (457, 114, 120, 1, 36000000, 0, 'MP 消耗减少 18%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (458, 114, 121, 1, 36000000, 0, 'MP 消耗减少 20%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (459, 90, 51, 1, 86300000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (460, 51, 5, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (461, 51, 6, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (462, 51, 7, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (463, 51, 8, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (464, 51, 9, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (465, 51, 10, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (466, 51, 11, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (467, 51, 12, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (468, 51, 13, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (469, 26, 4, 1, 3600000, 0, '用魔法力量增加防御力.\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (470, 27, 4, 1, 3600000, 0, '全身像岩石一样坚硬.\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (471, 30, 5, 1, 3600000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (472, 44, 2, 1, 3600000, 0, '神圣的力量在邪恶的气息中保护你全身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (473, 68, 10, 1, 3600000, 0, '负重上线增加.\n\n负重 +250');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (474, 13, 5, 1, 3600000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (475, 17, 16, 1, 600000, 0, '力量上升.\n\n力量 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (476, 24, 16, 1, 600000, 0, '敏捷上升.\n\n敏捷 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (477, 25, 16, 1, 600000, 0, '智力上升.\n\n智力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (478, 67, 11, 1, 600000, 0, '防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (479, 91, 2, 1, 40000, 0, '移动速度大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (480, 68, 28, 1, 1200000, 0, '负重上线增加.\n\n负重  +500');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (481, 68, 29, 1, 1200000, 0, '负重上线增加.\n\n负重  +1000');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (482, 108, 3, 1, 600000, 0, '暴击率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (483, 18, 16, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (484, 18, 20, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (485, 46, 10, 1, 1800000, 0, '移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (486, 90, 52, 1, 86400000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (487, 117, 1, 1, 300000, 0, ' HP最大值提高 50%.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (488, 118, 49, 1, 20000, 0, '暂时无法带上装备.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (489, 119, 1, 1, 1800000, 0, '用魔法的力量使身体变轻.\n\n移动速度, 攻击速度大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (490, 120, 1, 1, 3000, 0, '储备魔力中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (491, 121, 1, 1, 1000, 0, '运用全身力量打出一击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (492, 82, 3, 1, 0, 0, 'MP +50');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (493, 49, 4, 1, 300000, 0, '命中率大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (494, 43, 6, 11, 300000, 0, '攻击力上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (495, 51, 14, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (496, 51, 15, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (497, 51, 16, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (498, 5, 5, 1, 30000, 0, '出血.\n\n每秒 HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (499, 52, 3, 1, 30000, 0, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (500, 46, 11, 1, 1800000, 0, '移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (501, 26, 5, 1, 600000, 0, '用魔法增加防御力.\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (502, 27, 5, 1, 600000, 0, '身体像岩石一样坚硬.\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (503, 30, 6, 1, 600000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (504, 44, 3, 1, 600000, 0, '神圣的力量在邪恶的气息中保护你全身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (505, 18, 17, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (506, 18, 18, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (507, 127, 50, 1, 2000, 2, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (508, 38, 2, 1, 0, 0, '隐身中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (509, 38, 3, 1, 0, 0, '隐身中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (510, 38, 10, 1, 0, 0, '隐身中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (511, 38, 4, 1, 0, 0, '隐身中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (512, 127, 51, 1, 2000, 0, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (513, 127, 52, 1, 1500, 0, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (514, 127, 53, 1, 1000, 0, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (515, 95, 2, 1, 60000, 0, '被敌人发现.\n\n无法变成透明状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (516, 95, 3, 1, 30000, 0, '被敌人发现.\n\n无法变成透明状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (517, 95, 4, 1, 20000, 0, '被敌人发现.\n\n无法变成透明状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (518, 122, 1, 1, 100, 0, '用暴击攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (519, 123, 1, 1, 7000, 0, '暂时无法穿戴装备.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (520, 124, 1, 1, 600000, 0, '魔法攻击力和命中率大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (523, 125, 1, 1, 600000, 0, '时间扭曲.\n\n移动速度, 攻击速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (527, 126, 1, 1, 120000, 0, 'HP持续恢复.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (528, 128, 1, 1, 2000, 0, '正在召唤哥布林爆破兵.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (529, 128, 2, 1, 2000, 0, '正在召唤嗜血曼陀罗.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (530, 128, 3, 1, 2000, 0, '正在召唤魔法曼陀罗.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (531, 129, 1, 1, 200, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (532, 130, 1, 1, 60000, 0, '受防御图腾的效果.\n\n药水不会被烈焰风暴破坏.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (533, 131, 1, 1, 60000, 0, '受生命图腾的效果.\n\nHP恢复.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (534, 128, 4, 1, 2000, 0, '正在召唤防御图腾');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (535, 128, 5, 1, 2000, 0, '正在召唤生命图腾');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (536, 132, 1, 1, 10000, 0, '因地狱烈焰防御力下降, HP减少.\n\n防御力 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (537, 133, 1, 1, 5000, 0, '因深渊攻击速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (538, 134, 1, 1, 100, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (543, 135, 1, 1, 60000, 0, '力量和饱腹度下降.\n\n力量 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (544, 136, 1, 1, 60000, 0, '敏捷和移动速度减少.\n\n敏捷 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (545, 137, 1, 1, 60000, 0, '智力 MP瞬间减少.\n\n智力 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (546, 139, 1, 1, 60000, 0, '混沌.\n\n所有属性 -2\n攻击力下降\n防御力下降');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (547, 138, 1, 1, 60000, 0, '混乱.\n\n所有属性 -2\n攻击力下降\n防御力下降');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (550, 129, 2, 1, 200, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (551, 18, 21, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (552, 18, 22, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (553, 18, 23, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (554, 140, 1, 1, 10000, 0, '等待回城中.\n\n如果移动或采取特定行动时回城可能被取消.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (555, 140, 2, 1, 10000, 0, '等待回城中.\n\n如果移动或采取特定行动时回城可能被取消.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (556, 140, 3, 1, 10000, 0, '等待回城中.\n\n如果移动或采取特定行动时回城可能被取消.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (557, 140, 4, 1, 10000, 0, '等待回城中.\n\n如果移动或采取特定行动时回城可能被取消.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (558, 140, 5, 1, 10000, 0, '等待回城中.\n\n如果移动或采取特定行动时回城可能被取消.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (559, 92, 50, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (560, 92, 51, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (561, 92, 52, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (562, 67, 12, 1, 3600000, 0, '防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (563, 25, 17, 1, 1800000, 0, '智力上升.\n\n智力 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (564, 17, 17, 1, 1800000, 0, '力量上升.\n\n力量 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (565, 24, 17, 1, 1800000, 0, '敏捷上升.\n\n敏捷 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (566, 51, 17, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (567, 51, 18, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (568, 141, 101, 1, 6000000, 0, '受守护祝福.\n\n攻击力 +1\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (569, 141, 102, 1, 6000000, 0, '受守护祝福.\n\n攻击力 +3\n防御力  +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (570, 141, 103, 1, 6000000, 0, '受守护祝福.\n\n攻击力 +5\n防御力  +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (572, 18, 24, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (573, 18, 25, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (574, 18, 26, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (575, 18, 27, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (576, 18, 28, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (577, 18, 29, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (578, 18, 30, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (579, 18, 31, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (580, 18, 32, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (581, 18, 33, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (582, 18, 34, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (583, 18, 35, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (584, 18, 36, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (585, 18, 37, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (586, 18, 38, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (587, 18, 39, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (588, 18, 40, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (589, 18, 41, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (590, 18, 42, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (591, 18, 43, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (592, 18, 44, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (593, 18, 45, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (594, 142, 10, 1, 300000, 1, '生命力大幅增加.\n\nHP +250\nMP +125');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (595, 143, 1, 1, 300000, 1, '防御力增加.\n\n防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (596, 144, 1, 1, 300000, 1, '攻击速度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (597, 145, 1, 1, 300000, 1, '移动速度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (598, 146, 1, 1, 60000, 1, '受魔法效果的保护.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (599, 147, 1, 1, 0, 1, '解除敌人的魔法防御.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (600, 151, 1, 1, 5000, 2, '所有属性 -3\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (601, 149, 1, 1, 3000, 1, '回避所有物理攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (602, 18, 50, 1, 300000, 2, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (603, 18, 51, 1, 300000, 2, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (604, 18, 52, 1, 300000, 2, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (605, 18, 53, 1, 300000, 2, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (606, 150, 1, 1, 10000, 2, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (607, 153, 1, 1, 60000, 2, '被攻击时使敌人出血.\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (608, 5, 6, 1, 30000, 0, '出血.\n\n每秒HP大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (609, 152, 50, 1, 60000, 2, '伤害减少25%.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (610, 148, 1, 1, 0, 0, '解除B级以下的异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (611, 10, 4, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (612, 67, 4, 1, 3600000, 0, '防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (613, 67, 5, 1, 3600000, 0, '防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (614, 18, 54, 1, 300000, 3, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (615, 18, 55, 1, 300000, 3, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (616, 18, 56, 1, 300000, 3, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (617, 18, 57, 1, 300000, 3, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (618, 18, 58, 1, 300000, 4, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (619, 18, 59, 1, 300000, 4, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (620, 18, 60, 1, 300000, 4, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (621, 18, 61, 1, 300000, 4, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (623, 154, 1, 1, 60000, 2, '使周边敌人出血.\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (624, 10, 5, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (625, 10, 6, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (626, 10, 7, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (627, 5, 7, 51, 1000, 0, '出血.\n\n每秒HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (628, 82, 4, 1, 0, 0, 'MP +30');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (629, 82, 5, 1, 0, 0, 'MP +30');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (630, 150, 2, 1, 10000, 3, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (631, 150, 3, 1, 10000, 4, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (632, 151, 2, 1, 8000, 3, '所有属性 -5\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (633, 10, 8, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (634, 10, 9, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (635, 156, 1, 1, 1000, 2, '吸收敌人的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (636, 156, 2, 1, 1000, 3, '吸收敌人的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (637, 151, 3, 1, 8000, 4, '所有属性 -5\n\n防御力 -4\n不能移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (638, 127, 54, 1, 4000, 3, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (639, 5, 9, 51, 1000, 0, '出血.\n\n每秒HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (640, 153, 2, 1, 180000, 3, '被攻击时使敌人出血.\n\n防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (641, 153, 3, 1, 180000, 4, '被攻击时使敌人出血.\n\n防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (643, 5, 11, 51, 1000, 0, '出血.\n\n每秒HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (644, 154, 2, 1, 180000, 3, '使周围敌人出血.\n\n防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (645, 52, 4, 1, 300000, 0, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (646, 152, 51, 1, 60000, 3, '伤害减少35%.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (647, 152, 52, 1, 60000, 4, '伤害减少35%,部分伤害用HP吸收.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (648, 51, 19, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (649, 51, 20, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (650, 51, 21, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (651, 51, 22, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (652, 51, 23, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (653, 51, 24, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (654, 51, 25, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (655, 51, 26, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (656, 51, 27, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (657, 51, 28, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (658, 51, 29, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (659, 51, 30, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (660, 51, 31, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (661, 51, 32, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (662, 51, 33, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (663, 51, 34, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (664, 51, 35, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (665, 51, 36, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (666, 51, 37, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (667, 51, 38, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (668, 51, 39, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (669, 51, 40, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (670, 51, 41, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (671, 51, 42, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (672, 51, 43, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (673, 51, 44, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (674, 148, 2, 1, 0, 0, '解除C级以下异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (675, 148, 3, 1, 0, 0, '解除C+级以下异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (676, 159, 1, 1, 1000, 1, '无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (677, 158, 50, 1, 7200000, 10, '增加可获得的名誉点数.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (678, 157, 50, 1, 7200000, 10, '角色死亡时保护持有的福分名誉点数.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (679, 155, 11, 1, 60000, 0, '部分魔法反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (680, 160, 1, 1, 0, 0, '攻击力 +2\n\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (681, 160, 2, 1, 0, 0, '攻击力 +4\n\n防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (682, 160, 3, 1, 0, 0, '攻击力 +6\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (683, 160, 4, 1, 0, 0, '攻击力 +8\n\n防御力 +8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (684, 160, 5, 1, 0, 0, '攻击力 +10\n\n防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (685, 160, 6, 1, 0, 0, '攻击力 +12\n\n防御力 +12\n\n人形系杀戮 LV1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (686, 160, 7, 1, 0, 0, '攻击力 +18\n\n防御力 +18\n\n人形系杀戮 LV2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (687, 161, 1, 1, 30000, 0, '正在召唤奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (688, 128, 6, 1, 2000, 0, '正在召唤奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (689, 154, 3, 1, 180000, 4, '使周围敌人出血.\n\n防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (690, 19, 8, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (691, 140, 50, 1, 5000, 0, '等待回城中.\n\n如果移动或采取特定行动时回城可能被取消.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (692, 127, 55, 1, 4000, 4, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (693, 5, 8, 51, 23000, 0, '出血.\n\nHP减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (694, 5, 10, 51, 23000, 0, '出血.\n\nHP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (695, 5, 12, 51, 23000, 0, '出血.\n\nHP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (696, 162, 2, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (697, 162, 3, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (698, 162, 4, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (699, 162, 5, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (700, 162, 6, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (701, 162, 7, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (702, 162, 8, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (703, 162, 9, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (704, 86, 6, 51, 1000, 0, '因中毒瞬间HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (705, 86, 7, 51, 1000, 0, '因中毒瞬间HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (706, 86, 8, 51, 1000, 0, '因中毒瞬间HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (707, 163, 50, 1, 1000000, 0, 'HP +100\n\nMP +100');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (708, 164, 51, 1, 300000, 0, '负重 +500');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (709, 164, 52, 1, 300000, 0, '负重 +500');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (710, 164, 53, 1, 300000, 0, '负重 +500');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (711, 18, 62, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (712, 18, 63, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (713, 18, 64, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (714, 18, 65, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (715, 18, 66, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (716, 18, 67, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (717, 18, 68, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (718, 18, 69, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (719, 42, 4, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (720, 42, 5, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (721, 42, 6, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (722, 42, 7, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (723, 167, 1, 1, 1800000, 0, '用魔法使身体变轻.\n\n移动速度,攻击速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (724, 42, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (725, 165, 60, 1, 30000, 0, '封印心灵传送术魔法.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (726, 166, 60, 1, 600000, 0, '无法使用心灵传送术封印魔法.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (727, 168, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (728, 169, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (729, 169, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (730, 18, 9, 1, 1200000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (731, 170, 1, 1, 0, 0, '可看见装备透明的披风的使用者.\n\n不可攻击隐身者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (732, 18, 70, 1, 300000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (733, 18, 71, 1, 300000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (734, 18, 72, 1, 420000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (735, 18, 73, 1, 300000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (736, 18, 74, 1, 300000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (737, 18, 75, 1, 420000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (738, 18, 76, 1, 300000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (739, 18, 77, 1, 300000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (740, 18, 78, 1, 420000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (741, 18, 79, 1, 300000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (742, 18, 80, 1, 300000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (743, 18, 81, 1, 420000, 5, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (744, 150, 4, 1, 10000, 5, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (745, 150, 5, 1, 3000, 6, '移动速度大幅下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (746, 150, 6, 1, 10000, 7, '移动速度下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (747, 86, 9, 51, 1000, 0, '因中毒瞬间H大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (748, 86, 10, 51, 1000, 0, '因中毒瞬间H大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (749, 156, 3, 1, 1000, 5, '吸收敌人的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (750, 156, 4, 1, 1000, 6, '吸收敌人的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (751, 156, 6, 1, 1000, 7, '吸收敌人的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (752, 10, 10, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (753, 10, 11, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (754, 151, 4, 1, 8000, 5, '所有属性 -7\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (755, 151, 5, 1, 8000, 6, '所有属性 -9\n\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (756, 151, 6, 1, 8000, 7, '所有属性 -9\n\n防御力 -8\n无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (757, 162, 10, 1, 600000, 0, '装备了反击之盾.\n\n部分魔法按几率反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (758, 127, 56, 1, 6000, 5, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (759, 127, 57, 1, 8000, 6, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (760, 127, 58, 1, 8000, 7, '无法攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (764, 5, 13, 51, 1000, 0, '出血.\n\n每秒HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (765, 5, 15, 51, 1000, 0, '出血.\n\n每秒HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (766, 5, 17, 51, 1000, 0, '出血.\n\n每秒HP致命的减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (767, 5, 14, 51, 23000, 0, '出血.\n\n每秒HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (768, 5, 16, 51, 23000, 0, '出血.\n\n每秒HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (769, 5, 18, 51, 23000, 0, '出血.\n\n每秒HP大量减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (770, 153, 4, 1, 300000, 5, '被攻击时使敌人出血.\n\n防御力 +14');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (771, 153, 5, 1, 300000, 6, '被攻击时使敌人出血.\n\n防御力 +14');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (772, 153, 6, 1, 300000, 7, '被攻击时使敌人出血.\n\n防御力 +14');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (773, 154, 4, 1, 300000, 5, '使周围敌人出血.\n\n防御力 +14');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (774, 154, 5, 1, 300000, 6, '使周围敌人出血.\n\n防御力 +14');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (775, 154, 6, 1, 300000, 7, '使周围敌人出血.\n\n防御力 +14');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (776, 152, 53, 1, 60000, 5, '伤害减少45%.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (777, 152, 54, 1, 60000, 6, '伤害减少45%,部分伤害用HP吸收.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (778, 152, 55, 1, 60000, 7, '伤害减少45%,部分伤害用HP吸收.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (779, 148, 4, 1, 0, 0, '解除D级以下异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (780, 148, 5, 1, 0, 0, '解除D级以下异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (781, 148, 6, 1, 0, 0, '解除D级以下异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (782, 171, 10, 1, 0, 0, '您是运营者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (783, 160, 11, 1, 0, 0, '攻击力 +3\n\nHP +20\nMP+20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (784, 43, 11, 1, 5000, 0, '攻击力上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (785, 108, 11, 1, 5000, 0, '暴击率小幅增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (786, 68, 30, 1, 3600000, 0, '负重上线增加.\n\n负重 +1000');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (787, 49, 11, 1, 5000, 0, '命中率上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (788, 67, 15, 1, 5000, 0, '为体现');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (790, 172, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (791, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (792, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (793, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (794, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (795, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (796, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (797, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (798, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (799, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (800, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (801, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (802, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (803, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (804, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (805, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (806, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (807, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (808, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (809, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (810, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (811, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (812, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (813, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (814, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (815, 160, 16, 1, 0, 0, '攻击力 +3\n\nHP +20\nMP+20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (816, 5, 50, 51, 80000000, 0, '出血.\n\nHP小幅减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (817, 95, 5, 1, 5000, 0, '被敌人发现.\n\n无法隐身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (818, 18, 82, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (819, 18, 83, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (820, 18, 84, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (821, 18, 85, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (822, 18, 86, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (823, 18, 87, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (824, 18, 88, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (825, 18, 89, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (826, 18, 90, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (827, 18, 91, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (828, 18, 92, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (829, 18, 93, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (830, 18, 94, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (831, 18, 95, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (832, 18, 96, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (833, 173, 1, 1, 60000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (834, 46, 12, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (835, 174, 1, 1, 60000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (836, 175, 50, 1, 600000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (837, 19, 9, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (838, 19, 10, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (839, 19, 11, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (840, 19, 12, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (841, 19, 13, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (842, 19, 14, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (843, 19, 15, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (844, 19, 16, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (845, 19, 17, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (846, 19, 18, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (847, 19, 19, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (848, 18, 100, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (849, 18, 101, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (850, 18, 125, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (851, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (852, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (853, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (854, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (855, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (856, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (857, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (858, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (859, 18, 97, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (860, 18, 98, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (862, 68, 11, 1, 600000, 0, '负重上线增加.\n\n负重 +450');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (864, 68, 12, 1, 600000, 0, '负重上线增加.\n\n负重 +500');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (865, 68, 13, 1, 600000, 0, '负重上线增加.\n\n负重 +650');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (866, 68, 14, 1, 600000, 0, '负重上线增加.\n\n负重 +750');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (867, 26, 12, 1, 1200000, 0, '用魔法力量增加防御力.\n\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (868, 26, 13, 1, 1200000, 0, '用魔法力量增加防御力.\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (869, 26, 14, 1, 1200000, 0, '用魔法力量增加防御力.\n\n防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (870, 26, 15, 1, 1200000, 0, '用魔法力量增加防御力.\n\n防御力 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (872, 27, 13, 1, 1200000, 0, '身体像岩石一样坚硬.\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (873, 27, 15, 1, 1200000, 0, '身体像岩石一样坚硬.\n\n防御力 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (874, 27, 12, 1, 1200000, 0, '身体像岩石一样坚硬.\n\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (875, 27, 14, 1, 1200000, 0, '身体像岩石一样坚硬.\n\n防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (877, 44, 13, 1, 1200000, 0, '神圣的力量在邪恶的气息中保护你全身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (878, 44, 15, 1, 1200000, 0, '神圣的力量在邪恶的气息中保护你全身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (879, 44, 12, 1, 1200000, 0, '神圣的力量在邪恶的气息中保护你全身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (880, 44, 14, 1, 1200000, 0, '神圣的力量在邪恶的气息中保护你全身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (885, 30, 12, 1, 1200000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (886, 30, 13, 1, 1200000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (887, 30, 14, 1, 1200000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (888, 30, 15, 1, 1200000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (889, 30, 16, 1, 18000000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (890, 49, 5, 1, 300000, 0, '命中率大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (891, 43, 7, 11, 300000, 0, '攻击力大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (892, 43, 12, 1, 18000000, 0, '攻击力大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (894, 65, 1, 1, 180000, 0, 'MP恢复增加 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (895, 65, 1, 1, 180000, 0, 'MP恢复增加 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (896, 65, 1, 1, 180000, 0, 'MP恢复增加 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (897, 65, 1, 1, 180000, 0, 'MP恢复增加 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (899, 68, 15, 1, 3600000, 0, '负重上线增加.\n\n负重  +500');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (900, 17, 21, 1, 3600000, 0, '力量上升.\n\n力量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (901, 24, 21, 1, 3600000, 0, '敏捷上升.\n\n敏捷 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (902, 25, 21, 1, 3600000, 0, '智力上升.\n\n智力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (903, 163, 10, 1, 3600000, 0, 'HP +100\n\nMP +100');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (904, 132, 5, 1, 10000, 0, '因地狱烈焰防御力下降, HP减少.\n\n防御力 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (905, 15, 6, 81, 35000, 0, '攻击速度和力量大幅下降.\n\n力量 -9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (906, 15, 7, 81, 35000, 0, '攻击速度和力量大幅下降.\n\n力量 -9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (907, 15, 8, 81, 35000, 0, '攻击速度和力量大幅下降.\n\n力量-12');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (908, 15, 9, 81, 40000, 0, '攻击速度和力量长时间大幅下降.\n\n力量 -12');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (909, 8, 5, 31, 11000, 0, '全身难受.\n\n移动速度减慢.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (910, 8, 6, 21, 12000, 0, '全身难受.\n\n移动速度减慢.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (912, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (913, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (914, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (915, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (916, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (917, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (918, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (919, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (920, 82, 6, 1, 0, 0, 'MP +15');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (921, 82, 7, 1, 0, 0, 'MP +25');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (922, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (923, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (924, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (925, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (926, 177, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (927, 160, 8, 1, 0, 0, '攻击力 +14\n\n防御力 +14\n\n人形系杀戮 LV1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (928, 160, 9, 1, 0, 0, '攻击力 +16\n\n防御力 +16\n\n人形系杀戮 LV1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (929, 160, 10, 1, 0, 0, '攻击力 +20\n\n防御力 +20\n\n人形系杀戮 LV2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (930, 160, 12, 1, 0, 0, '攻击力 +22\n\n防御力 +22\n\n人形系杀戮 LV2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (931, 15, 10, 81, 300000, 0, '攻击速度和力量长时间大幅下降.\n\n力量');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (932, 5, 51, 96, 1000, 0, '出血.\n\n每秒HP大幅减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (933, 5, 52, 96, 1000, 0, '出血.\n\n每秒HP大幅减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (936, 176, 51, 1, 6000000, 1, '\n守护者 Lv 1\n\n负重 +500');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (937, 176, 53, 1, 6000000, 2, '\n守护者 Lv 2\n\n负重 +500\n药剂恢复量增加 Lv 1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (938, 176, 55, 1, 6000000, 3, '\n守护者 Lv 3\n\n负重 +500\n药剂恢复量增加 Lv 1\n属性 +1, HP/MP +20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (939, 176, 57, 1, 6000000, 4, '\n守护者 Lv 4\n\n负重 +500\n药剂恢复量增加 Lv 1\n所有属性 +1, HP/MP +20\n所有攻击力/命中率小幅增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (940, 12, 5, 1, 1800000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (942, 26, 16, 1, 3600000, 0, '用魔法的力量增加防御力.\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (943, 27, 16, 1, 3600000, 0, '全身像岩石一样坚硬.\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (944, 30, 16, 1, 3600000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (945, 44, 16, 1, 3600000, 0, '神圣的力量在邪恶的气息中保护你全身.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (946, 68, 16, 1, 1800000, 0, '负重上线增加.\n\n负重  +250');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (947, 13, 6, 1, 1800000, 0, '攻击速度和移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (948, 26, 17, 1, 300000, 0, 'test专用 禁止使用');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (949, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (950, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (951, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (952, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (953, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (954, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (955, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (956, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (957, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (958, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (959, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (960, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (961, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (962, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (963, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (964, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (965, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (966, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (967, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (968, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (969, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (970, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (971, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (972, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (973, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (974, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (975, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (976, 18, 6, 1, 3600000, 0, '变身中.\n\n不可召唤德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (977, 18, 5, 1, 2400000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (978, 18, 127, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (979, 18, 127, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (980, 18, 127, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (981, 18, 128, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (982, 18, 128, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (983, 51, 49, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (984, 51, 50, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (985, 51, 51, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (986, 51, 52, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (987, 51, 53, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (988, 51, 54, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (989, 51, 55, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (990, 51, 56, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (991, 51, 57, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (992, 140, 51, 1, 3000, 0, '返回待机状态中.\n\n移动或进行特殊动作时返回状态将会被取消.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (993, 10, 12, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (995, 91, 5, 1, 25000, 0, '移动速度大幅度提升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (996, 12, 1, 1, 3000, 0, '攻击速度提升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (997, 97, 48, 26, 30000, 0, '中了[II阶段]毒.\n\n每秒减少 HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (998, 97, 49, 51, 30000, 0, '中了[III阶段]毒.\n\n每秒大幅度减少 HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (999, 18, 5, 1, 3600000, 0, '变身中.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1000, 18, 5, 1, 3600000, 0, '变身中.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1001, 146, 2, 1, 4000, 1, '通过魔法效果得到保护.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1003, 151, 7, 1, 5000, 8, '所有能力值-3\n\n无法进行移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1004, 159, 2, 1, 5000, 2, '无法移动.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1005, 146, 3, 1, 5000, 1, '通过魔法效果得到保护.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1006, 153, 7, 1, 10000, 8, '受到攻击时对方将会进入出血状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1007, 178, 1, 1, 3000, 0, '攻击速度提升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1008, 103, 55, 1, 0, 0, '使用毒瓶中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1009, 103, 56, 1, 0, 0, '使用猛毒瓶中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1010, 103, 57, 1, 0, 0, '使用狼蛛毒瓶中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1011, 103, 58, 1, 0, 0, '使用曼陀铃毒瓶中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1012, 52, 11, 76, 2500, 0, '移动速度大幅度减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1014, 179, 1, 1, 3600000, 0, '所有攻击力大幅度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1015, 180, 1, 1, 3600000, 0, '所有命中率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1016, 181, 1, 1, 3600000, 0, '闪避攻击的概率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1017, 182, 1, 1, 3600000, 0, '暴击率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1018, 183, 1, 1, 3600000, 0, '智力增加.\n智力+5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1019, 184, 1, 1, 3600000, 0, '负重增加\n负重+1000');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1020, 185, 1, 1, 3600000, 0, '力量增加.\n力量+5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1021, 186, 1, 1, 3600000, 0, '伤害减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1022, 187, 1, 1, 3600000, 0, '敏捷增加.\n敏捷+5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1023, 156, 7, 1, 6000, 8, '被抢去HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1024, 149, 2, 1, 6000, 1, '闪避所有物理攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1025, 77, 11, 1, 3600000, 0, 'HP恢复量+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1026, 65, 12, 1, 3600000, 0, 'MP恢复量+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1027, 17, 4, 1, 1200000, 0, '力量提升.\n\n力量+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1028, 24, 4, 1, 1200000, 0, '敏捷提升.\n\n敏捷+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1029, 25, 5, 1, 1200000, 0, '智力提升.\n\n智力+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1030, 18, 131, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1031, 18, 132, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1032, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1033, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1035, 65, 3, 1, 1200000, 0, '无视负重 MP恢复+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1036, 65, 4, 1, 1200000, 0, '无视负重 MP恢复+3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1037, 114, 122, 1, 36000000, 0, 'MP消耗量减少25%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1038, 77, 12, 1, 3600000, 0, '无视负重 HP恢复+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1039, 77, 13, 1, 3600000, 0, '无视负重 HP恢复+3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1040, 26, 18, 1, 3600000, 0, '用魔法之力增加防御力.\n\n防御力+5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1041, 27, 17, 1, 3600000, 0, '全身像石头一样坚硬.\n\n防御力+5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1042, 30, 17, 1, 3600000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1043, 44, 17, 1, 3600000, 0, '神圣的力量进行保护不受到邪恶的气息的干扰.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1044, 68, 17, 1, 3600000, 0, '负重最大值增加.\n\n负重增加+750');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1046, 18, 135, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1047, 18, 136, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1048, 18, 137, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1049, 18, 138, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1050, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1051, 18, 139, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1052, 18, 191, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1053, 191, 1, 1, 0, 0, '在公会议事厅结束游戏时会产生休眠经验值. \n\n在休眠经验值区间内进行狩猎时可获得2倍经验值.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1054, 191, 2, 1, 0, 0, '在公会议事厅结束游戏时会产生休眠经验值. \n\n在休眠经验值区间内进行狩猎时可获得2倍经验值.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1055, 189, 1, 1, 0, 0, '音速屏障的持续时间变长.\n\n音速屏障持续时间 增加+10秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1056, 189, 2, 1, 0, 0, '音速屏障的持续时间变长.\n\n音速屏障持续时间 增加+20秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1057, 189, 3, 1, 0, 0, '音速屏障的持续时间变长.\n\n音速屏障持续时间 增加+30秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1058, 189, 4, 1, 0, 0, '音速屏障的持续时间变长.\n\n没有习得音速屏障技能时将无法开发该技能.\n\n音速屏障持续时间 增加+40秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1059, 189, 5, 1, 0, 0, '音速屏障的持续时间变长.\n\n没有习得音速屏障技能时将无法开发该技能.\n\n音速屏障持续时间 增加+50秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1063, 189, 1, 1, 0, 0, '使用疾速射击时减少MP的消耗量. \n\n没有习得疾速射击技能时将无法开发该技能.\n\n使用疾速射击时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1064, 189, 2, 1, 0, 0, '使用疾速射击时减少MP的消耗量. \n\n没有习得疾速射击技能时将无法开发该技能.\n\n使用疾速射击时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1065, 189, 3, 1, 0, 0, '使用疾速射击时减少MP的消耗量. \n\n没有习得疾速射击技能时将无法开发该技能.\n\n使用疾速射击时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1066, 194, 1, 1, 0, 0, '让目标锁定更好的显示.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1067, 191, 1, 1, 0, 0, '增加公会荣誉勋章的生成量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1068, 199, 1, 1, 0, 0, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人造成附加伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1069, 199, 2, 1, 0, 0, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人造成附加伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1070, 199, 3, 1, 0, 0, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人造成附加伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1071, 199, 4, 1, 0, 0, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人造成附加伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1072, 199, 5, 1, 0, 0, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人造成附加伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1073, 191, 2, 1, 0, 0, '增加公会荣誉勋章的生成量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1074, 191, 3, 1, 0, 0, '增加公会荣誉勋章的生成量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1075, 191, 4, 1, 0, 0, '增加公会荣誉勋章的生成量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1076, 191, 5, 1, 0, 0, '增加公会荣誉勋章的生成量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1077, 189, 1, 1, 0, 0, '增加疾速射击的持续时间. \n\n没有习得疾速射击技能时将无法开发该技能.\n\n疾速射击持续时间 增加+10秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1078, 189, 2, 1, 0, 0, '增加疾速射击的持续时间. \n\n没有习得疾速射击技能时将无法开发该技能.\n\n疾速射击持续时间 增加+20秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1079, 189, 3, 1, 0, 0, '增加疾速射击的持续时间. \n\n没有习得疾速射击技能时将无法开发该技能.\n\n疾速射击持续时间 增加+30秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1080, 189, 4, 1, 0, 0, '增加疾速射击的持续时间. \n\n没有习得疾速射击技能时将无法开发该技能.\n\n疾速射击持续时间 增加+40秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1081, 189, 5, 1, 0, 0, '增加疾速射击的持续时间. \n\n没有习得疾速射击技能时将无法开发该技能.\n\n疾速射击持续时间 增加+50秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1082, 191, 1, 1, 0, 0, '生成可以增加公会技能经验值的荣誉勋章.\n\n不是开发公会技能中的公会成员时无法使用.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1083, 190, 1, 1, 0, 0, '提高躲避振荡射击的成功率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1084, 190, 2, 1, 0, 0, '提高躲避振荡射击的成功率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1085, 190, 3, 1, 0, 0, '提高躲避振荡射击的成功率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1086, 190, 4, 1, 0, 0, '提高躲避振荡射击的成功率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1087, 195, 1, 1, 0, 0, '将指定公会成员召唤至公会议事厅.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1088, 190, 5, 1, 0, 0, '提高躲避振荡射击的成功率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1089, 189, 1, 1, 0, 0, '使用振荡射击时减少MP消耗量. \n\n没有习得振荡射击技能时将无法开发该技能.\n\n使用振荡射击时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1090, 189, 2, 1, 0, 0, '使用振荡射击时减少MP消耗量. \n\n没有习得振荡射击技能时将无法开发该技能.\n\n使用振荡射击时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1091, 189, 3, 1, 0, 0, '使用振荡射击时减少MP消耗量. \n\n没有习得振荡射击技能时将无法开发该技能.\n\n使用振荡射击时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1092, 190, 1, 1, 0, 0, '提高回避昏厥攻击和蛛网术的几率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1093, 190, 2, 1, 0, 0, '提高回避昏厥攻击和蛛网术的几率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1094, 190, 3, 1, 0, 0, '提高回避昏厥攻击和蛛网术的几率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1095, 190, 4, 1, 0, 0, '提高回避昏厥攻击和蛛网术的几率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1096, 190, 5, 1, 0, 0, '提高回避昏厥攻击和蛛网术的几率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1097, 189, 1, 1, 0, 0, '提高振荡射击的命中率. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1098, 189, 2, 1, 0, 0, '提高振荡射击的命中率. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1099, 189, 3, 1, 0, 0, '提高振荡射击的命中率. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1100, 189, 4, 1, 0, 0, '提高振荡射击的命中率. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1101, 189, 5, 1, 0, 0, '提高振荡射击的命中率. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1102, 189, 1, 1, 0, 0, '使用振荡射击时增大被击对象的移动速度的减少幅度. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1103, 189, 2, 1, 0, 0, '使用振荡射击时增大被击对象的移动速度的减少幅度. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1104, 189, 3, 1, 0, 0, '使用振荡射击时增大被击对象的移动速度的减少幅度. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1105, 189, 4, 1, 0, 0, '使用振荡射击时增大被击对象的移动速度的减少幅度. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1106, 189, 5, 1, 0, 0, '使用振荡射击时增大被击对象的移动速度的减少幅度. \n\n没有习得振荡射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1107, 200, 1, 1, 0, 0, '设置对目标造成伤害的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1108, 200, 2, 1, 0, 0, '设置对目标造成伤害的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1109, 200, 3, 1, 0, 0, '设置对目标造成伤害的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1110, 200, 4, 1, 0, 0, '设置对目标造成伤害的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1111, 200, 5, 1, 0, 0, '设置对目标造成伤害的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1112, 191, 1, 1, 0, 0, '减少声望值的减少量. \n\n增加声望值获得量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1113, 191, 2, 1, 0, 0, '减少声望值的减少量. \n\n增加声望值获得量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1114, 191, 3, 1, 0, 0, '减少声望值的减少量. \n\n增加声望值获得量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1115, 191, 4, 1, 0, 0, '减少声望值的减少量. \n\n增加声望值获得量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1116, 191, 5, 1, 0, 0, '减少声望值的减少量. \n\n增加声望值获得量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1117, 188, 1, 1, 0, 0, '近战攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1118, 188, 2, 1, 0, 0, '近战攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1119, 188, 3, 1, 0, 0, '近战攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1120, 188, 4, 1, 0, 0, '近战攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1121, 188, 5, 1, 0, 0, '近战攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1122, 188, 1, 1, 0, 0, '远程攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1123, 201, 1, 1, 0, 0, '设置减少对象移动速度的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1124, 188, 2, 1, 0, 0, '远程攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1125, 201, 2, 1, 0, 0, '设置减少对象移动速度的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1126, 188, 3, 1, 0, 0, '远程攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1127, 201, 3, 1, 0, 0, '设置减少对象移动速度的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1128, 201, 4, 1, 0, 0, '设置减少对象移动速度的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1129, 188, 4, 1, 0, 0, '远程攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1131, 188, 5, 1, 0, 0, '远程攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1132, 201, 5, 1, 0, 0, '设置减少对象移动速度的陷阱.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1133, 189, 1, 1, 0, 0, '使用魔法盾时额外增加防御力. \n\n 没有习得魔法盾(I)技能时无法开发该技能.\n\n使用魔法盾时防御力额外增加+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1134, 189, 2, 1, 0, 0, '使用魔法盾时额外增加防御力. \n\n 没有习得魔法盾(I)技能时无法开发该技能.\n\n使用魔法盾时防御力额外增加+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1135, 188, 1, 1, 0, 0, '魔法攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1136, 188, 2, 1, 0, 0, '魔法攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1137, 188, 3, 1, 0, 0, '魔法攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1138, 188, 4, 1, 0, 0, '魔法攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1139, 188, 5, 1, 0, 0, '魔法攻击回避几率提高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1140, 188, 1, 1, 0, 0, '增加MP. \n\nMP 增加+5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1141, 188, 2, 1, 0, 0, '增加MP. \n\nMP 增加+10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1142, 188, 3, 1, 0, 0, '增加MP. \n\nMP 增加+15');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1143, 188, 4, 1, 0, 0, '增加MP. \n\nMP 增加+20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1144, 188, 5, 1, 0, 0, '增加MP. \n\nMP 增加+25');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1145, 188, 1, 1, 0, 0, '减少近战攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1146, 188, 2, 1, 0, 0, '减少近战攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1147, 189, 1, 1, 0, 0, '增加剑术精通的持续时间. \n\n 没有习得剑术精通(I)技能时无法开发该技能.\n\n剑术精通持续时间 增加+1分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1148, 188, 3, 1, 0, 0, '减少近战攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1149, 189, 2, 1, 0, 0, '增加剑术精通的持续时间. \n\n 没有习得剑术精通(I)技能时无法开发该技能.\n\n剑术精通持续时间 增加+2分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1150, 188, 4, 1, 0, 0, '减少近战攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1151, 189, 3, 1, 0, 0, '增加剑术精通的持续时间. \n\n 没有习得剑术精通(I)技能时无法开发该技能.\n\n剑术精通持续时间 增加+3分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1152, 188, 5, 1, 0, 0, '减少近战攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1153, 189, 4, 1, 0, 0, '增加剑术精通的持续时间. \n\n 没有习得剑术精通(I)技能时无法开发该技能.\n\n剑术精通持续时间 增加+4分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1154, 189, 5, 1, 0, 0, '增加剑术精通的持续时间. \n\n 没有习得剑术精通(I)技能时无法开发该技能.\n\n剑术精通持续时间 增加+5分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1155, 188, 1, 1, 0, 0, '减少远程攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1156, 188, 2, 1, 0, 0, '减少远程攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1157, 188, 3, 1, 0, 0, '减少远程攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1158, 188, 4, 1, 0, 0, '减少远程攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1159, 188, 5, 1, 0, 0, '减少远程攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1160, 188, 1, 1, 0, 0, '减少魔法攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1161, 188, 2, 1, 0, 0, '减少魔法攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1162, 188, 3, 1, 0, 0, '减少魔法攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1163, 188, 4, 1, 0, 0, '减少魔法攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1164, 188, 5, 1, 0, 0, '减少魔法攻击所受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1165, 191, 1, 1, 0, 0, '增加变身状态的持续时间. \n\n变身持续时间 增加+2分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1166, 191, 2, 1, 0, 0, '增加变身状态的持续时间. \n\n变身持续时间 增加+2分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1167, 191, 3, 1, 0, 0, '增加变身状态的持续时间. \n\n变身持续时间 增加+2分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1168, 191, 4, 1, 0, 0, '增加变身状态的持续时间. \n\n变身持续时间 增加+2分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1169, 191, 5, 1, 0, 0, '增加变身状态的持续时间. \n\n变身持续时间 增加+2分钟');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1170, 189, 1, 1, 0, 0, '减少使用重击时的MP消耗量. \n\n没有习得重击技能时将无法开发该技能.使用重击时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1171, 189, 2, 1, 0, 0, '减少使用重击时的MP消耗量. \n\n没有习得重击技能时将无法开发该技能.使用重击时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1172, 189, 3, 1, 0, 0, '减少使用重击时的MP消耗量. \n\n没有习得重击技能时将无法开发该技能.使用重击时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1173, 188, 1, 1, 0, 0, '受到近战攻击伤害量一定比率转化为近战攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1174, 188, 2, 1, 0, 0, '受到近战攻击伤害量一定比率转化为近战攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1175, 188, 3, 1, 0, 0, '受到近战攻击伤害量一定比率转化为近战攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1176, 188, 4, 1, 0, 0, '受到近战攻击伤害量一定比率转化为近战攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1177, 188, 5, 1, 0, 0, '受到近战攻击伤害量一定比率转化为近战攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1178, 189, 1, 1, 0, 0, '使用石化皮肤时额外增加防御力. \n\n 没有习得石化皮肤(I)技能时无法开发该技能.\n\n使用石化皮肤时防御力额外增加+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1179, 189, 2, 1, 0, 0, '使用石化皮肤时追加性的增加防御力. \n\n 没有习得石化皮肤(I)技能时无法开发该技能.\n\n使用石化皮肤时防御力额外增加+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1180, 188, 1, 1, 0, 0, '增加MP恢复量. \n\nMP恢复 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1181, 188, 2, 1, 0, 0, '增加MP恢复量. \n\nMP恢复 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1182, 188, 3, 1, 0, 0, '增加MP恢复量. \n\nMP恢复 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1183, 188, 4, 1, 0, 0, '增加MP恢复量. \n\nMP恢复 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1184, 188, 5, 1, 0, 0, '增加MP恢复量. \n\nMP恢复 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1185, 189, 1, 1, 0, 0, '使用元素铠甲时提高回避魔法攻击几率 \n\n 没有习得元素铠甲技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1186, 189, 2, 1, 0, 0, '使用元素铠甲时提高回避魔法攻击几率 \n\n 没有习得元素铠甲技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1187, 189, 3, 1, 0, 0, '使用元素铠甲时提高回避魔法攻击几率 \n\n 没有习得元素铠甲技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1188, 189, 4, 1, 0, 0, '使用元素铠甲时提高回避魔法攻击几率 \n\n 没有习得元素铠甲技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1189, 189, 5, 1, 0, 0, '使用元素铠甲时提高回避魔法攻击几率 \n\n 没有习得元素铠甲技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1190, 188, 1, 1, 0, 0, '无视负重恢复MP. \n\n无视负重的MP恢复+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1191, 188, 2, 1, 0, 0, '无视负重恢复MP. \n\n无视负重的MP恢复+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1192, 188, 3, 1, 0, 0, '无视负重恢复MP. \n\n无视负重的MP恢复+3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1193, 189, 1, 1, 0, 0, '使用魔法驱散时一定几率多解除1个异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1194, 189, 2, 1, 0, 0, '使用魔法驱散时一定几率多解除2个异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1195, 189, 3, 1, 0, 0, '使用魔法驱散时一定几率多解除3个异常状态.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1196, 202, 1, 1, 1000, 0, '消耗自身的所有MP从而增加HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1197, 202, 2, 1, 1000, 0, '消耗自身的所有MP从而增加HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1198, 202, 3, 1, 1000, 0, '消耗自身的所有MP从而增加HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1199, 189, 1, 1, 0, 0, '使用群体拯救时增加持续时间. \n\n 没有习得群体拯救技能时无法开发该技能.\n\n群体拯救持续时间增加10秒.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1200, 189, 2, 1, 0, 0, '使用群体拯救时增加持续时间. \n\n 没有习得群体拯救技能时无法开发该技能.\n\n群体拯救持续时间增加20秒.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1201, 189, 3, 1, 0, 0, '使用群体拯救时增加持续时间. \n\n 没有习得群体拯救技能时无法开发该技能.\n\n群体拯救持续时间增加30秒.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1202, 189, 4, 1, 0, 0, '使用群体拯救时增加持续时间. \n\n 没有习得群体拯救技能时无法开发该技能.\n\n群体拯救持续时间增加40秒.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1203, 189, 5, 1, 0, 0, '使用群体拯救时增加持续时间. \n\n 没有习得群体拯救技能时无法开发该技能.\n\n群体拯救持续时间增加50秒.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1204, 203, 1, 1, 1000, 0, '消耗自身的所有MP从而增加攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1205, 203, 2, 1, 1000, 0, '消耗自身的所有MP从而增加攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1206, 203, 3, 1, 1000, 0, '消耗自身的所有MP从而增加攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1207, 189, 1, 1, 0, 0, '增加诅咒的命中率. \n\n 没有习得诅咒技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1208, 189, 2, 1, 0, 0, '增加诅咒的命中率. \n\n 没有习得诅咒技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1209, 189, 3, 1, 0, 0, '增加诅咒的命中率. \n\n 没有习得诅咒技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1210, 189, 1, 1, 0, 0, '增加厄运之触的命中率. \n\n 没有习得厄运之触技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1211, 189, 2, 1, 0, 0, '增加厄运之触的命中率. \n\n 没有习得厄运之触技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1212, 189, 3, 1, 0, 0, '增加厄运之触的命中率. \n\n 没有习得厄运之触技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1213, 189, 1, 1, 0, 0, '增加沉默的命中率. \n\n 没有习得沉默技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1214, 189, 2, 1, 0, 0, '增加沉默的命中率. \n\n 没有习得沉默技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1215, 189, 3, 1, 0, 0, '增加沉默的命中率. \n\n 没有习得沉默技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1216, 189, 1, 1, 0, 0, '增加剧毒之云的命中率. \n\n 没有习得剧毒之云技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1217, 189, 2, 1, 0, 0, '增加剧毒之云的命中率. \n\n 没有习得剧毒之云技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1218, 189, 3, 1, 0, 0, '增加剧毒之云的命中率. \n\n 没有习得剧毒之云技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1219, 189, 1, 1, 0, 0, '增加烈焰风暴的伤害值. \n\n 没有习得烈焰风暴(I)技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1220, 189, 2, 1, 0, 0, '增加烈焰风暴的伤害值. \n\n 没有习得烈焰风暴(I)技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1221, 189, 3, 1, 0, 0, '增加烈焰风暴的伤害值. \n\n 没有习得烈焰风暴(I)技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1222, 189, 4, 1, 0, 0, '增加烈焰风暴的伤害值. \n\n 没有习得烈焰风暴(I)技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1223, 189, 5, 1, 0, 0, '增加烈焰风暴的伤害值. \n\n 没有习得烈焰风暴(I)技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1224, 204, 1, 1, 100, 0, '减少对方的MP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1225, 204, 2, 1, 100, 0, '减少对方的MP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1226, 204, 3, 1, 100, 0, '减少对方的MP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1227, 204, 4, 1, 100, 0, '减少对方的MP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1228, 204, 5, 1, 100, 0, '减少对方的MP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1229, 189, 1, 1, 0, 0, '使用回避时减少MP消耗量. \n\n 没有习得回避技能时无法开发该技能.\n\n使用回避技能时MP消耗 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1230, 189, 2, 1, 0, 0, '使用回避时减少MP消耗量. \n\n 没有习得回避技能时无法开发该技能.\n\n使用回避技能时MP消耗 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1231, 189, 3, 1, 0, 0, '使用回避时减少MP消耗量. \n\n 没有习得回避技能时无法开发该技能.\n\n使用回避技能时MP消耗 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1232, 189, 4, 1, 0, 0, '使用回避时减少MP消耗量. \n\n 没有习得回避技能时无法开发该技能.\n\n使用回避技能时MP消耗 -4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1233, 189, 5, 1, 0, 0, '使用回避时减少MP消耗量. \n\n 没有习得回避技能时无法开发该技能.\n\n使用回避技能时MP消耗 -5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1234, 189, 1, 1, 0, 0, '使用回避时增加持续时间. \n\n 没有习得回避技能时无法开发该技能.\n\n使用回避技能时持续时间+5秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1235, 189, 2, 1, 0, 0, '使用回避时增加持续时间. \n\n 没有习得回避技能时无法开发该技能.\n\n使用回避技能时持续时间+10秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1236, 189, 3, 1, 0, 0, '使用回避时增加持续时间. \n\n 没有习得回避技能时无法开发该技能.\n\n使用回避技能时持续时间+15秒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1237, 189, 1, 1, 0, 0, '使用毒牙时减少MP消耗量. \n\n 没有习得毒牙技能时无法开发该技能.\n\n使用毒牙时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1238, 189, 2, 1, 0, 0, '使用毒牙时减少MP消耗量. \n\n 没有习得毒牙技能时无法开发该技能.\n\n使用毒牙时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1239, 189, 3, 1, 0, 0, '使用毒牙时减少MP消耗量. \n\n 没有习得毒牙技能时无法开发该技能.\n\n使用毒牙时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1240, 189, 4, 1, 0, 0, '使用毒牙时减少MP消耗量. \n\n 没有习得毒牙技能时无法开发该技能.\n\n使用毒牙时MP消耗-4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1241, 189, 5, 1, 0, 0, '使用毒牙时减少MP消耗量. \n\n 没有习得毒牙技能时无法开发该技能.\n\n使用毒牙时MP消耗-5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1242, 192, 1, 1, 0, 0, '使用要害攻击时附加强力攻击. \n\n 没有习得要害攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1243, 192, 2, 1, 0, 0, '使用要害攻击时附加强力攻击. \n\n 没有习得要害攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1244, 192, 3, 1, 0, 0, '使用要害攻击时附加强力攻击. \n\n 没有习得要害攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1245, 192, 4, 1, 0, 0, '使用要害攻击时附加强力攻击. \n\n 没有习得要害攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1246, 192, 5, 1, 0, 0, '使用要害攻击时附加强力攻击. \n\n 没有习得要害攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1247, 189, 1, 1, 0, 0, '使用昏厥攻击时减少MP消耗量. \n\n 没有习得昏厥攻击技能时无法开发该技能.\n\n使用昏厥攻击时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1248, 189, 2, 1, 0, 0, '使用昏厥攻击时减少MP消耗量. \n\n 没有习得昏厥攻击技能时无法开发该技能.\n\n使用昏厥攻击时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1249, 189, 3, 1, 0, 0, '使用昏厥攻击时减少MP消耗量. \n\n 没有习得昏厥攻击技能时无法开发该技能.\n\n使用昏厥攻击时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1250, 189, 4, 1, 0, 0, '使用昏厥攻击时减少MP消耗量. \n\n 没有习得昏厥攻击技能时无法开发该技能.\n\n使用昏厥攻击时MP消耗-4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1251, 189, 5, 1, 0, 0, '使用昏厥攻击时减少MP消耗量. \n\n 没有习得昏厥攻击技能时无法开发该技能.\n\n使用昏厥攻击时MP消耗-5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1252, 189, 1, 1, 0, 0, '使用疾行时延长攻击速度增加的持续时间. \n\n 没有习得疾行技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1253, 189, 2, 1, 0, 0, '使用疾行时延长攻击速度增加的持续时间. \n\n 没有习得疾行技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1254, 189, 3, 1, 0, 0, '使用疾行时延长攻击速度增加的持续时间. \n\n 没有习得疾行技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1255, 189, 4, 1, 0, 0, '使用疾行时延长攻击速度增加的持续时间. \n\n 没有习得疾行技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1256, 189, 5, 1, 0, 0, '使用疾行时延长攻击速度增加的持续时间. \n\n 没有习得疾行技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1257, 189, 1, 1, 0, 0, '增加在未隐身的状态下使用的暗杀技能的命中率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1258, 189, 2, 1, 0, 0, '增加在未隐身的状态下使用的暗杀技能的命中率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1259, 189, 3, 1, 0, 0, '增加在未隐身的状态下使用的暗杀技能的命中率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1262, 189, 1, 1, 0, 0, '使用暗杀时减少MP消耗量. \n\n 没有习得暗杀技能时无法开发该技能.\n\n使用暗杀时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1263, 189, 2, 1, 0, 0, '使用暗杀时减少MP消耗量. \n\n 没有习得暗杀技能时无法开发该技能.\n\n使用暗杀时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1264, 189, 3, 1, 0, 0, '使用暗杀时减少MP消耗量. \n\n 没有习得暗杀技能时无法开发该技能.\n\n使用暗杀时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1265, 189, 4, 1, 0, 0, '使用暗杀时减少MP消耗量. \n\n 没有习得暗杀技能时无法开发该技能.\n\n使用暗杀时MP消耗-4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1266, 189, 5, 1, 0, 0, '使用暗杀时减少MP消耗量. \n\n 没有习得暗杀技能时无法开发该技能.\n\n使用暗杀时MP消耗-5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1267, 205, 1, 1, 0, 0, '解除一般的中毒效果(曼陀铃,狼蛛).');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1268, 205, 2, 1, 0, 0, '在解毒 Lv1的效果上可以追加性的解除毒牙(I)的效果.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1269, 205, 3, 1, 0, 0, '在解毒 Lv2的效果上可以追加性的解除毒牙(II)的效果.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1270, 205, 4, 1, 0, 0, '在解毒 Lv3的效果上可以追加性的解除毒牙(III)的效果.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1271, 189, 1, 1, 0, 0, '使用暗杀时增加暴击几率. \n\n 没有习得暗杀技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1272, 189, 2, 1, 0, 0, '使用暗杀时增加暴击几率. \n\n 没有习得暗杀技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1273, 189, 3, 1, 0, 0, '使用暗杀时增加暴击几率. \n\n 没有习得暗杀技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1274, 189, 4, 1, 0, 0, '使用暗杀时增加暴击几率. \n\n 没有习得暗杀技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1275, 189, 5, 1, 0, 0, '使用暗杀时增加暴击几率. \n\n 没有习得暗杀技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1276, 189, 1, 1, 0, 0, '增加使用隐身术状态下的移动速度. \n\n 没有习得隐身术技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1277, 189, 2, 1, 0, 0, '增加使用隐身术状态下的移动速度. \n\n 没有习得隐身术技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1278, 189, 3, 1, 0, 0, '增加使用隐身术状态下的移动速度. \n\n 没有习得隐身术技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1279, 206, 1, 1, 10000, 0, '解除所有负面状态同时进入隐身状态\n\n 被侦查时，也可使用该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1280, 189, 1, 1, 0, 0, '增加猛力攻击的伤害. \n\n 没有习得猛力攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1281, 189, 2, 1, 0, 0, '增加猛力攻击的伤害. \n\n 没有习得猛力攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1282, 189, 3, 1, 0, 0, '增加猛力攻击的伤害. \n\n 没有习得猛力攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1283, 189, 4, 1, 0, 0, '增加猛力攻击的伤害. \n\n 没有习得猛力攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1284, 189, 5, 1, 0, 0, '增加猛力攻击的伤害. \n\n 没有习得猛力攻击技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1285, 188, 1, 1, 0, 0, '减少2点敏捷同时增加2点力量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1286, 188, 2, 1, 0, 0, '减少4点敏捷同时增加4点力量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1287, 188, 3, 1, 0, 0, '减少6点敏捷同时增加6点力量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1288, 189, 1, 1, 0, 0, '增加流星火的伤害. \n\n 没有习得流星火技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1289, 189, 2, 1, 0, 0, '增加流星火的伤害. \n\n 没有习得流星火技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1290, 189, 3, 1, 0, 0, '增加流星火的伤害. \n\n 没有习得流星火技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1291, 189, 4, 1, 0, 0, '增加流星火的伤害. \n\n 没有习得流星火技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1292, 189, 5, 1, 0, 0, '增加流星火的伤害. \n\n 没有习得流星火技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1293, 189, 1, 1, 0, 0, '增加海神之怒的攻击力. \n\n 没有习得海神之怒技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1294, 189, 2, 1, 0, 0, '增加海神之怒的攻击力. \n\n 没有习得海神之怒技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1295, 189, 3, 1, 0, 0, '增加海神之怒的攻击力. \n\n 没有习得海神之怒技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1296, 189, 4, 1, 0, 0, '增加海神之怒的攻击力. \n\n 没有习得海神之怒技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1297, 189, 5, 1, 0, 0, '增加海神之怒的攻击力. \n\n 没有习得海神之怒技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1298, 188, 1, 1, 0, 0, '在减少远程攻击力的同时,增加一定的近战攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1299, 188, 2, 1, 0, 0, '在减少远程攻击力的同时,增加一定的近战攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1300, 188, 3, 1, 0, 0, '在减少远程攻击力的同时,增加一定的近战攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1301, 207, 1, 1, 30000, 0, '移动速度和攻击速度大幅度上升. \n\n没有习得时间扭曲技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1302, 189, 1, 1, 0, 0, '增加重击的伤害值.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1303, 189, 2, 1, 0, 0, '增加重击的伤害值.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1304, 189, 3, 1, 0, 0, '增加重击的伤害值.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1305, 189, 4, 1, 0, 0, '增加重击的伤害值.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1306, 189, 5, 1, 0, 0, '增加重击的伤害值.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1307, 189, 1, 1, 0, 0, '召唤战斗用生命图腾.(瞬间释放) \n\n没有习得生命图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1308, 189, 2, 1, 0, 0, '召唤战斗用生命图腾.(瞬间释放) \n\n没有习得生命图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1309, 189, 3, 1, 0, 0, '召唤战斗用生命图腾.(瞬间释放) \n\n没有习得生命图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1310, 189, 1, 1, 0, 0, '召唤出可以防止被侵蚀火焰攻击导致药水被破坏的强化防御图腾\n\n没有习得防御图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1311, 188, 1, 1, 0, 0, '减少2点力量同时增加2点敏捷');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1312, 188, 2, 1, 0, 0, '减少4点力量同时增加4点敏捷');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1313, 188, 3, 1, 0, 0, '减少6点力量同时增加6点敏捷');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1314, 189, 1, 1, 0, 0, '召唤已强化的奥利爱德图腾.比原来的奥利爱德图腾增加了一定的HP. \n\n没有习得奥利爱德图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1315, 189, 2, 1, 0, 0, '召唤已强化的奥利爱德图腾.比原来的奥利爱德图腾增加了一定的HP. \n\n没有习得奥利爱德图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1316, 189, 3, 1, 0, 0, '召唤已强化的奥利爱德图腾.比原来的奥利爱德图腾增加了一定的HP. \n\n没有习得奥利爱德图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1317, 189, 4, 1, 0, 0, '召唤已强化的奥利爱德图腾.比原来的奥利爱德图腾增加了一定的HP. \n\n没有习得奥利爱德图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1318, 189, 5, 1, 0, 0, '召唤已强化的奥利爱德图腾.比原来的奥利爱德图腾增加了一定的HP. \n\n没有习得奥利爱德图腾技能时无法开发该技能.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1319, 188, 1, 1, 0, 0, '在减少近战攻击力的同时,增加一定的远程攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1320, 188, 2, 1, 0, 0, '在减少近战攻击力的同时,增加一定的远程攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1321, 188, 3, 1, 0, 0, '在减少近战攻击力的同时,增加一定的远程攻击力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1322, 208, 1, 1, 60000, 0, '召唤出提升友军攻击力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1323, 208, 2, 1, 60000, 0, '召唤出提升友军攻击力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1324, 208, 3, 1, 60000, 0, '召唤出提升友军攻击力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1325, 208, 4, 1, 60000, 0, '召唤出提升友军攻击力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1326, 208, 5, 1, 60000, 0, '召唤出提升友军攻击力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1327, 209, 1, 1, 60000, 0, '召唤出提升友军防御力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1328, 209, 2, 1, 60000, 0, '召唤出提升友军防御力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1329, 209, 3, 1, 60000, 0, '召唤出提升友军防御力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1330, 209, 4, 1, 60000, 0, '召唤出提升友军防御力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1331, 209, 5, 1, 60000, 0, '召唤出提升友军防御力的图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1332, 189, 1, 1, 0, 0, '减少使用猛烈攻击时的MP消耗量. \n\n没有习得猛烈攻击技能时将无法开发该技能.\n\n使用猛烈攻击时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1333, 189, 2, 1, 0, 0, '减少使用猛烈攻击时的MP消耗量. \n\n没有习得猛烈攻击技能时将无法开发该技能.\n\n使用猛烈攻击时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1334, 189, 3, 1, 0, 0, '减少使用猛烈攻击时的MP消耗量. \n\n没有习得猛烈攻击技能时将无法开发该技能.\n\n使用猛烈攻击时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1335, 188, 1, 1, 0, 0, '根据近战攻击回避率为比例增加一定的近战攻击命中率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1336, 188, 2, 1, 0, 0, '根据近战攻击回避率为比例增加一定的近战攻击命中率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1337, 188, 3, 1, 0, 0, '根据近战攻击回避率为比例增加一定的近战攻击命中率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1338, 188, 4, 1, 0, 0, '根据近战攻击回避率为比例增加一定的近战攻击命中率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1339, 188, 5, 1, 0, 0, '根据近战攻击回避率为比例增加一定的近战攻击命中率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1340, 189, 1, 1, 0, 0, '使用猛力攻击时追加性的增加力量.\n\n使用猛力攻击时增加力量+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1341, 189, 2, 1, 0, 0, '使用猛力攻击时追加性的增加力量.\n\n使用猛力攻击时增加力量+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1342, 189, 3, 1, 0, 0, '使用猛力攻击时追加性的增加力量.\n\n使用猛力攻击时增加力量+3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1343, 189, 1, 1, 0, 0, '减少使用暴走时的MP消耗量. \n\n没有习得暴走技能时将无法开发该技能.\n\n使用暴走时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1344, 189, 2, 1, 0, 0, '减少使用暴走时的MP消耗量. \n\n没有习得暴走技能时将无法开发该技能.\n\n使用暴走时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1345, 189, 3, 1, 0, 0, '减少使用暴走时的MP消耗量. \n\n没有习得暴走技能时将无法开发该技能.\n\n使用暴走时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1346, 188, 1, 1, 0, 0, '根据防御力增加一定的力量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1347, 188, 2, 1, 0, 0, '根据防御力增加一定的力量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1348, 188, 3, 1, 0, 0, '根据防御力增加一定的力量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1349, 188, 4, 1, 0, 0, '根据防御力增加一定的力量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1350, 188, 5, 1, 0, 0, '根据防御力增加一定的力量.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1351, 192, 1, 1, 0, 0, '使用重击技能时一定几率追加暴击攻击. \n\n没有习得重击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1352, 192, 2, 1, 0, 0, '使用重击技能时一定几率追加暴击攻击. \n\n没有习得重击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1353, 192, 3, 1, 0, 0, '使用重击技能时一定几率追加暴击攻击. \n\n没有习得重击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1354, 192, 4, 1, 0, 0, '使用重击技能时一定几率追加暴击攻击. \n\n没有习得重击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1355, 192, 5, 1, 0, 0, '使用重击技能时一定几率追加暴击攻击. \n\n没有习得重击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1356, 189, 1, 1, 0, 0, '使用暴走技能时将减少防御的效果降低1点.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1357, 189, 2, 1, 0, 0, '使用暴走技能时将减少防御的效果降低2点.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1358, 189, 3, 1, 0, 0, '使用暴走技能时将减少防御的效果降低3点.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1359, 189, 4, 1, 0, 0, '使用暴走技能时将减少防御的效果降低4点.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1361, 189, 1, 1, 0, 0, '减少使用痛苦麻痹时的MP消耗量. \n\n没有习得痛苦麻痹技能时将无法开发该技能.\n\n使用痛苦麻痹时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1362, 189, 2, 1, 0, 0, '减少使用痛苦麻痹时的MP消耗量. \n\n没有习得痛苦麻痹技能时将无法开发该技能.\n\n使用痛苦麻痹时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1363, 189, 3, 1, 0, 0, '减少使用痛苦麻痹时的MP消耗量. \n\n没有习得痛苦麻痹技能时将无法开发该技能.\n\n使用痛苦麻痹时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1364, 189, 4, 1, 0, 0, '减少使用痛苦麻痹时的MP消耗量. \n\n没有习得痛苦麻痹技能时将无法开发该技能.\n\n使用痛苦麻痹时MP消耗-4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1365, 189, 5, 1, 0, 0, '减少使用痛苦麻痹时的MP消耗量. \n\n没有习得痛苦麻痹技能时将无法开发该技能.\n\n使用痛苦麻痹时MP消耗-5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1366, 188, 1, 1, 0, 0, '根据近战攻击命中率为比例增加近战攻击回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1367, 188, 2, 1, 0, 0, '根据近战攻击命中率为比例增加近战攻击回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1368, 188, 3, 1, 0, 0, '根据近战攻击命中率为比例增加近战攻击回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1369, 188, 4, 1, 0, 0, '根据近战攻击命中率为比例增加近战攻击回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1370, 188, 5, 1, 0, 0, '根据近战攻击命中率为比例增加近战攻击回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1371, 189, 1, 1, 0, 0, '使用痛苦麻痹技能时增加防御力1.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1372, 189, 2, 1, 0, 0, '使用痛苦麻痹技能时增加防御力2.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1373, 189, 3, 1, 0, 0, '使用痛苦麻痹技能时增加防御力3.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1374, 188, 1, 1, 0, 0, '根据近战攻击力为比例来减少近战攻击受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1375, 188, 2, 1, 0, 0, '根据近战攻击力为比例来减少近战攻击受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1376, 188, 3, 1, 0, 0, '根据近战攻击力为比例来减少近战攻击受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1377, 188, 4, 1, 0, 0, '根据近战攻击力为比例来减少近战攻击受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1378, 188, 5, 1, 0, 0, '根据近战攻击力为比例来减少近战攻击受到的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1379, 189, 1, 1, 0, 0, '减少使用生命绽放时的MP消耗量. \n\n没有习得生命绽放技能时将无法开发该技能.\n\n使用生命绽放时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1380, 189, 2, 1, 0, 0, '减少使用生命绽放时的MP消耗量. \n\n没有习得生命绽放技能时将无法开发该技能.\n\n使用生命绽放时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1381, 189, 3, 1, 0, 0, '减少使用生命绽放时的MP消耗量. \n\n没有习得生命绽放技能时将无法开发该技能.\n\n使用生命绽放时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1382, 189, 4, 1, 0, 0, '减少使用生命绽放时的MP消耗量. \n\n没有习得生命绽放技能时将无法开发该技能.\n\n使用生命绽放时MP消耗-4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1383, 189, 5, 1, 0, 0, '减少使用生命绽放时的MP消耗量. \n\n没有习得生命绽放技能时将无法开发该技能.\n\n使用生命绽放时MP消耗-5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1389, 188, 1, 1, 0, 0, '根据力量为比例增加对所有攻击的回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1390, 188, 2, 1, 0, 0, '根据力量为比例增加对所有攻击的回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1391, 188, 3, 1, 0, 0, '根据力量为比例增加对所有攻击的回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1392, 188, 4, 1, 0, 0, '根据力量为比例增加对所有攻击的回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1393, 188, 5, 1, 0, 0, '根据力量为比例增加对所有攻击的回避率.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1394, 189, 1, 1, 0, 0, '增加生命绽放的持续时间1分钟.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1395, 189, 2, 1, 0, 0, '增加生命绽放的持续时间2分钟.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1396, 189, 3, 1, 0, 0, '增加生命绽放的持续时间3分钟.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1397, 189, 4, 1, 0, 0, '增加生命绽放的持续时间4分钟.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1398, 189, 5, 1, 0, 0, '增加生命绽放的持续时间5分钟.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1399, 197, 1, 1, 10000, 0, '持续时间内无视所有魔法效果.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1400, 189, 1, 1, 0, 0, '减少使用瞄准射击时的MP消耗量. \n\n没有习得瞄准射击技能时将无法开发该技能.\n\n使用瞄准射击时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1401, 189, 2, 1, 0, 0, '减少使用瞄准射击时的MP消耗量. \n\n没有习得瞄准射击技能时将无法开发该技能.\n\n使用瞄准射击时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1402, 189, 3, 1, 0, 0, '减少使用瞄准射击时的MP消耗量. \n\n没有习得瞄准射击技能时将无法开发该技能.\n\n使用瞄准射击时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1403, 189, 1, 1, 0, 0, '强化瞄准射击的攻击力.\n\n没有习得瞄准射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1404, 189, 2, 1, 0, 0, '强化瞄准射击的攻击力.\n\n没有习得瞄准射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1405, 189, 3, 1, 0, 0, '强化瞄准射击的攻击力.\n\n没有习得瞄准射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1406, 189, 4, 1, 0, 0, '强化瞄准射击的攻击力.\n\n没有习得瞄准射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1407, 189, 5, 1, 0, 0, '强化瞄准射击的攻击力.\n\n没有习得瞄准射击技能时将无法开发该技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1408, 189, 1, 1, 0, 0, '减少使用音速屏障时的MP消耗量. \n\n没有习得音速屏障技能时将无法开发该技能.\n\n使用音速屏障时MP消耗-1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1409, 189, 2, 1, 0, 0, '减少使用音速屏障时的MP消耗量. \n\n没有习得音速屏障技能时将无法开发该技能.\n\n使用音速屏障时MP消耗-2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1410, 189, 3, 1, 0, 0, '减少使用音速屏障时的MP消耗量. \n\n没有习得音速屏障技能时将无法开发该技能.\n\n使用音速屏障时MP消耗-3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1411, 198, 1, 1, 20000, 0, '将火焰箭的异常状态附加给对方.\n\n被攻击的对方每秒会减少一定量的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1412, 198, 2, 1, 20000, 0, '将火焰箭的异常状态附加给对方.\n\n被攻击的对方每秒会减少一定量的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1413, 198, 3, 1, 20000, 0, '将火焰箭的异常状态附加给对方.\n\n被攻击的对方每秒会减少一定量的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1414, 198, 4, 1, 20000, 0, '将火焰箭的异常状态附加给对方.\n\n被攻击的对方每秒会减少一定量的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1415, 198, 5, 1, 20000, 0, '将火焰箭的异常状态附加给对方.\n\n被攻击的对方每秒会减少一定量的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1416, 87, 8, 1, 0, 0, '处于中毒状态.\n\n每秒将减少HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1417, 87, 9, 1, 0, 0, '处于中毒状态.\n\n每秒将减少HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1418, 210, 1, 96, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1419, 73, 47, 1, 10000, 0, '处于瞄准射击[I阶段]状态中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1420, 73, 48, 1, 10000, 0, '处于瞄准射击[II阶段]状态中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1421, 75, 2, 1, 180000, 0, '敏捷,回避率增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1422, 75, 3, 1, 180000, 0, '敏捷,回避率大幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1423, 213, 1, 1, 20000, 0, '狩猎用杀戮');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1424, 108, 12, 1, 20000, 0, '暴击几率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1425, 49, 12, 1, 20000, 0, '命中率上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1426, 86, 11, 96, 15000, 0, '处于中毒状态.\n\n每秒将减少HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1427, 87, 10, 96, 1000, 0, '处于被猛毒中毒的状态.\n\n每秒将减少HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1428, 214, 1, 76, 15000, 0, '使用药剂时恢复量下降.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1429, 73, 49, 1, 100, 0, '处于瞄准射击[III阶段]状态中.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1430, 103, 59, 1, 0, 0, '正在使用红色灵魂净水.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1431, 103, 60, 1, 0, 0, '正在使用黑色灵魂净水.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1432, 103, 61, 1, 0, 0, '正在使用毒瓶.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1433, 103, 62, 1, 0, 0, '正在使用猛毒瓶.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1434, 103, 63, 1, 0, 0, '正在使用精炼毒瓶.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1435, 103, 64, 1, 0, 0, '正在使用精炼猛毒瓶.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1436, 67, 16, 1, 10000, 0, '防御力小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1437, 67, 17, 1, 10000, 0, '防御力增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1438, 67, 18, 1, 10000, 0, '防御力大幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1439, 223, 1, 1, 1800000, 0, '作为救出普尔菲因的报答赐予的神圣状态.\n\nHP/MP+50');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1440, 18, 7, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1441, 17, 22, 1, 1200000, 0, '力量提升.\n\n力量 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1442, 24, 22, 1, 1200000, 0, '敏捷提升.\n\n敏捷 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1443, 25, 22, 1, 1200000, 0, '智力提升.\n\n智力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1444, 17, 23, 1, 1800000, 0, '力量提升.\n\n力量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1445, 108, 13, 1, 1800000, 0, '暴击几率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1446, 49, 14, 1, 1800000, 0, '命中率上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1447, 24, 23, 1, 1800000, 0, '敏捷提升.\n\n敏捷 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1448, 163, 11, 1, 1800000, 0, 'MP +20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1449, 25, 23, 1, 1800000, 0, '智力提升.\n\n智力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1450, 130, 2, 1, 60000, 0, '受到防御图腾的效果.\n\n防止药水被破坏, 且获得对侵蚀火焰的抵抗力');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1451, 117, 2, 1, 3000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1452, 163, 1, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1453, 163, 2, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1454, 163, 3, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1455, 163, 4, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1456, 163, 5, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1457, 163, 6, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1458, 163, 7, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1459, 163, 8, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1460, 163, 9, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1461, 163, 10, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1462, 68, 21, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1463, 68, 22, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1464, 68, 23, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1465, 68, 24, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1466, 68, 25, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1467, 77, 21, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1468, 77, 22, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1469, 77, 23, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1470, 77, 24, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1471, 77, 25, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1472, 65, 21, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1473, 65, 22, 1, 1200000, 0, '无视负重的MP恢复+5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1474, 65, 23, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1475, 65, 24, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1476, 65, 25, 1, 1000, 1, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1477, 77, 26, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1478, 77, 27, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1479, 77, 28, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1480, 65, 26, 1, 1000, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1481, 65, 27, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1482, 65, 28, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1483, 17, 31, 1, 60000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1484, 17, 32, 1, 60000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1485, 17, 33, 1, 60000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1486, 25, 31, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1487, 25, 32, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1488, 25, 33, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1489, 108, 16, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1490, 108, 17, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1491, 108, 18, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1492, 108, 19, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1493, 108, 20, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1494, 78, 11, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1495, 78, 12, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1496, 78, 13, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1497, 78, 14, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1498, 78, 15, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1499, 78, 21, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1500, 78, 22, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1501, 78, 23, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1502, 78, 24, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1503, 78, 25, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1504, 78, 31, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1505, 78, 32, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1506, 78, 33, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1507, 78, 34, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1508, 78, 35, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1509, 79, 11, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1510, 79, 12, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1511, 79, 13, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1512, 79, 21, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1513, 79, 22, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1514, 79, 23, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1515, 79, 31, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1516, 79, 32, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1517, 79, 33, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1518, 43, 21, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1519, 43, 22, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1520, 43, 23, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1521, 43, 24, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1522, 43, 25, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1523, 43, 26, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1524, 43, 27, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1525, 43, 28, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1526, 43, 29, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1527, 43, 30, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1528, 43, 31, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1529, 43, 32, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1530, 43, 33, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1531, 43, 34, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1532, 43, 35, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1533, 49, 21, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1534, 49, 22, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1535, 49, 23, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1536, 49, 24, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1537, 49, 25, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1538, 49, 26, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1539, 49, 27, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1540, 49, 28, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1541, 49, 29, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1542, 163, 11, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1543, 163, 12, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1544, 163, 13, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1545, 163, 14, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1546, 163, 15, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1547, 68, 26, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1548, 68, 27, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1549, 68, 28, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1550, 24, 31, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1551, 24, 32, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1552, 24, 33, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1553, 188, 1, 1, 0, 0, '经验值获得量增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1554, 188, 2, 1, 0, 0, '经验值获得量增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1555, 196, 1, 1, 30000, 0, '防御力大幅度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1556, 196, 2, 1, 30000, 0, '防御力大幅度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1557, 196, 3, 1, 30000, 0, '防御力大幅度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1558, 196, 4, 1, 30000, 0, '防御力大幅度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1559, 196, 5, 1, 30000, 0, '防御力大幅度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1560, 188, 1, 1, 0, 0, '远程攻击力小幅度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1561, 188, 2, 1, 0, 0, '远程攻击力小幅度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1562, 188, 3, 1, 0, 0, '远程攻击力上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1563, 188, 4, 1, 0, 0, '远程攻击力上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1564, 188, 5, 1, 0, 0, '远程攻击力大幅度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1565, 58, 11, 1, 0, 0, '远程攻击命中率小幅度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1566, 58, 12, 1, 0, 0, '远程攻击命中率上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1567, 58, 13, 1, 0, 0, '远程攻击命中率大幅度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1568, 78, 41, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1569, 78, 42, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1570, 78, 43, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1571, 78, 44, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1572, 78, 45, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1573, 114, 123, 1, 36000000, 0, 'MP消耗量减少30%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1574, 77, 29, 1, 1000, 0, '无视负重的HP恢复+6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1575, 46, 14, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1576, 46, 13, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1577, 189, 1, 1, 0, 0, '哥布林爆破兵的召唤等级增加. \n\n哥布林爆破兵等级+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1578, 189, 2, 1, 0, 0, '哥布林爆破兵的召唤等级增加. \n\n哥布林爆破兵等级+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1579, 189, 3, 1, 0, 0, '哥布林爆破兵的召唤等级增加. \n\n哥布林爆破兵等级+3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1580, 189, 2, 1, 0, 0, '哥布林爆破兵的召唤等级增加. \n\n哥布林爆破兵等级+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1581, 189, 1, 1, 0, 0, '哥布林爆破兵的召唤等级增加. \n\n哥布林爆破兵等级+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1582, 189, 1, 1, 0, 0, '曼陀罗的召唤等级增加. \n\n曼陀罗等级+1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1583, 189, 2, 1, 0, 0, '曼陀罗的召唤等级增加. \n\n曼陀罗等级+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1584, 189, 3, 1, 0, 0, '曼陀罗的召唤等级增加. \n\n曼陀罗等级+3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1585, 214, 11, 1, 0, 0, '药剂恢复量小幅度增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1586, 214, 12, 1, 0, 0, '药剂恢复量增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1587, 214, 13, 1, 0, 0, '药剂恢复量增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1588, 212, 1, 1, 0, 0, '被锁定为目标.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1594, 78, 6, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1595, 78, 7, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1596, 189, 1, 1, 0, 0, '使用魔法盾时额外增加防御力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1597, 189, 2, 1, 0, 0, '使用魔法盾时额外增加防御力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1598, 79, 6, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1599, 79, 7, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1600, 189, 1, 1, 0, 0, '使用魔法盾时额外增加防御力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1601, 189, 2, 1, 0, 0, '使用魔法盾时额外增加防御力.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1602, 216, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1603, 216, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1604, 216, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1605, 216, 11, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1606, 216, 12, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1607, 216, 13, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1608, 145, 11, 1, 5000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1609, 145, 12, 1, 5000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1610, 145, 13, 1, 5000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1611, 193, 1, 1, 0, 0, '1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1612, 193, 1, 1, 0, 0, '2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1613, 193, 1, 1, 0, 0, '3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1614, 193, 1, 1, 0, 0, '4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1615, 193, 1, 1, 0, 0, '5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1616, 193, 1, 1, 0, 0, '6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1617, 193, 1, 1, 0, 0, '7');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1618, 193, 1, 1, 0, 0, '8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1619, 193, 1, 1, 0, 0, '9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1620, 193, 1, 1, 0, 0, '10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1621, 193, 1, 1, 0, 0, '11');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1622, 193, 1, 1, 0, 0, '12');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1623, 193, 1, 1, 0, 0, '13');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1624, 193, 1, 1, 0, 0, '14');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1625, 193, 1, 1, 0, 0, '15');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1626, 193, 1, 1, 0, 0, '16');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1627, 193, 1, 1, 0, 0, '17');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1628, 193, 1, 1, 0, 0, '18');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1629, 193, 1, 1, 0, 0, '19');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1630, 193, 1, 1, 0, 0, '20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1631, 193, 1, 1, 0, 0, '21');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1632, 193, 1, 1, 0, 0, '22');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1633, 193, 1, 1, 0, 0, '23');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1634, 193, 1, 1, 0, 0, '24');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1635, 193, 1, 1, 0, 0, '25');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1636, 193, 1, 1, 0, 0, '26');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1637, 193, 1, 1, 0, 0, '27');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1638, 193, 1, 1, 0, 0, '28');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1639, 193, 1, 1, 0, 0, '29');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1640, 193, 1, 1, 0, 0, '30');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1641, 193, 1, 1, 0, 0, '31');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1642, 193, 1, 1, 0, 0, '32');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1643, 193, 1, 1, 0, 0, '33');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1644, 193, 1, 1, 0, 0, '34');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1645, 193, 1, 1, 0, 0, '35');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1646, 193, 1, 1, 0, 0, '36');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1647, 193, 1, 1, 0, 0, '37');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1648, 193, 1, 1, 0, 0, '38');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1649, 193, 1, 1, 0, 0, '39');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1650, 193, 1, 1, 0, 0, '40');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1651, 193, 1, 1, 0, 0, '41');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1652, 193, 1, 1, 0, 0, '42');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1653, 193, 1, 1, 0, 0, '43');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1654, 193, 1, 1, 0, 0, '44');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1655, 193, 1, 1, 0, 0, '45');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1656, 193, 1, 1, 0, 0, '46');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1657, 193, 1, 1, 0, 0, '47');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1658, 193, 1, 1, 0, 0, '48');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1659, 193, 1, 1, 0, 0, '49');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1660, 193, 1, 1, 0, 0, '50');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1661, 193, 1, 1, 0, 0, '51');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1662, 193, 1, 1, 0, 0, '52');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1663, 193, 1, 1, 0, 0, '53');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1664, 193, 1, 1, 0, 0, '54');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1665, 193, 1, 1, 0, 0, '55');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1666, 193, 1, 1, 0, 0, '56');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1667, 193, 1, 1, 0, 0, '57');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1668, 193, 1, 1, 0, 0, '58');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1669, 193, 1, 1, 0, 0, '59');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1670, 193, 1, 1, 0, 0, '60');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1671, 193, 1, 1, 0, 0, '61');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1672, 193, 1, 1, 0, 0, '62');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1673, 193, 1, 1, 0, 0, '63');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1674, 193, 1, 1, 0, 0, '64');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1675, 193, 1, 1, 0, 0, '65');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1676, 193, 1, 1, 0, 0, '66');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1677, 193, 1, 1, 0, 0, '67');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1678, 193, 1, 1, 0, 0, '68');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1679, 193, 1, 1, 0, 0, '69');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1680, 193, 1, 1, 0, 0, '70');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1681, 193, 1, 1, 0, 0, '71');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1682, 193, 1, 1, 0, 0, '72');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1683, 193, 1, 1, 0, 0, '73');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1684, 193, 1, 1, 0, 0, '74');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1685, 193, 1, 1, 0, 0, '75');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1686, 193, 1, 1, 0, 0, '76');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1687, 193, 1, 1, 0, 0, '77');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1688, 193, 1, 1, 0, 0, '78');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1689, 193, 1, 1, 0, 0, '79');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1690, 193, 1, 1, 0, 0, '80');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1691, 193, 1, 1, 0, 0, '81');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1692, 193, 1, 1, 0, 0, '82');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1693, 193, 1, 1, 0, 0, '83');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1694, 193, 1, 1, 0, 0, '84');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1695, 193, 1, 1, 0, 0, '85');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1696, 193, 1, 1, 0, 0, '86');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1697, 193, 1, 1, 0, 0, '87');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1698, 193, 1, 1, 0, 0, '88');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1699, 193, 1, 1, 0, 0, '89');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1700, 193, 1, 1, 0, 0, '90');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1701, 193, 1, 1, 0, 0, '91');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1702, 193, 1, 1, 0, 0, '92');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1703, 193, 1, 1, 0, 0, '93');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1704, 193, 1, 1, 0, 0, '97');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1705, 193, 1, 1, 0, 0, '95');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1706, 193, 1, 1, 0, 0, '96');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1707, 193, 1, 1, 0, 0, '97');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1708, 193, 1, 1, 0, 0, '98');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1709, 193, 1, 1, 0, 0, '99');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1710, 193, 1, 1, 0, 0, '100');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1711, 193, 1, 1, 0, 0, '101');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1712, 193, 1, 1, 0, 0, '102');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1713, 193, 1, 1, 0, 0, '103');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1714, 193, 1, 1, 0, 0, '104');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1715, 193, 1, 1, 0, 0, '105');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1716, 193, 1, 1, 0, 0, '106');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1717, 193, 1, 1, 0, 0, '107');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1718, 193, 1, 1, 0, 0, '108');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1719, 193, 1, 1, 0, 0, '109');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1720, 193, 1, 1, 0, 0, '110');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1721, 193, 1, 1, 0, 0, '111');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1722, 193, 1, 1, 0, 0, '112');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1723, 193, 1, 1, 0, 0, '113');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1724, 193, 1, 1, 0, 0, '114');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1725, 193, 1, 1, 0, 0, '115');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1726, 193, 1, 1, 0, 0, '116');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1727, 193, 1, 1, 0, 0, '117');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1728, 193, 1, 1, 0, 0, '118');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1729, 193, 1, 1, 0, 0, '119');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1730, 193, 1, 1, 0, 0, '120');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1731, 193, 1, 1, 0, 0, '121');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1732, 193, 1, 1, 0, 0, '122');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1733, 193, 1, 1, 0, 0, '123');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1734, 193, 1, 1, 0, 0, '124');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1735, 193, 1, 1, 0, 0, '125');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1736, 193, 1, 1, 0, 0, '126');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1737, 193, 1, 1, 0, 0, '127');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1738, 193, 1, 1, 0, 0, '128');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1739, 193, 1, 1, 0, 0, '129');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1740, 193, 1, 1, 0, 0, '130');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1741, 193, 1, 1, 0, 0, '131');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1742, 193, 1, 1, 0, 0, '132');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1743, 193, 1, 1, 0, 0, '133');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1744, 193, 1, 1, 0, 0, '134');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1745, 193, 1, 1, 0, 0, '135');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1746, 193, 1, 1, 0, 0, '136');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1747, 193, 1, 1, 0, 0, '137');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1748, 193, 1, 1, 0, 0, '138');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1749, 193, 1, 1, 0, 0, '139');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1750, 193, 1, 1, 0, 0, '140');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1751, 193, 1, 1, 0, 0, '141');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1752, 193, 1, 1, 0, 0, '142');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1753, 193, 1, 1, 0, 0, '143');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1754, 193, 1, 1, 0, 0, '144');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1755, 193, 1, 1, 0, 0, '145');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1756, 193, 1, 1, 0, 0, '146');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1757, 193, 1, 1, 0, 0, '147');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1758, 193, 1, 1, 0, 0, '148');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1759, 193, 1, 1, 0, 0, '149');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1760, 193, 1, 1, 0, 0, '150');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1761, 193, 1, 1, 0, 0, '151');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1762, 193, 1, 1, 0, 0, '152');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1763, 193, 1, 1, 0, 0, '153');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1764, 193, 1, 1, 0, 0, '154');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1765, 193, 1, 1, 0, 0, '155');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1766, 193, 1, 1, 0, 0, '156');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1767, 193, 1, 1, 0, 0, '157');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1768, 193, 1, 1, 0, 0, '158');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1769, 193, 1, 1, 0, 0, '159');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1770, 193, 1, 1, 0, 0, '160');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1771, 193, 1, 1, 0, 0, '161');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1772, 193, 1, 1, 0, 0, '162');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1773, 193, 1, 1, 0, 0, '163');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1774, 193, 1, 1, 0, 0, '164');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1775, 193, 1, 1, 0, 0, '165');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1776, 193, 1, 1, 0, 0, '166');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1777, 198, 6, 1, 20000, 0, '将火焰箭的异常状态附加给对方.\n\n被攻击的对方每秒会减少一定量的HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1778, 52, 16, 1, 20000, 0, '移动速度大幅度减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1779, 52, 17, 1, 20000, 0, '移动速度大幅度减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1780, 52, 18, 1, 20000, 0, '移动速度大幅度减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1781, 52, 19, 1, 20000, 0, '移动速度大幅度减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1782, 52, 20, 1, 20000, 0, '移动速度大幅度减少.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1783, 131, 2, 1, 20000, 0, '受到生命图腾的效果.\n\nHP被恢复.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1784, 131, 3, 1, 20000, 0, '受到生命图腾的效果.\n\nHP被恢复.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1785, 131, 4, 1, 20000, 0, '受到生命图腾的效果.\n\nHP被恢复.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1786, 128, 11, 1, 0, 0, '正在召唤战斗用生命图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1787, 128, 12, 1, 0, 0, '正在召唤战斗用生命图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1788, 128, 13, 1, 0, 0, '正在召唤战斗用生命图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1789, 128, 14, 1, 2000, 0, '正在召唤已强化的防御图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1790, 128, 21, 1, 2000, 0, '正在召唤已强化的奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1791, 128, 22, 1, 2000, 0, '正在召唤已强化的奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1792, 128, 23, 1, 2000, 0, '正在召唤已强化的奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1793, 128, 24, 1, 2000, 0, '正在召唤已强化的奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1794, 128, 25, 1, 2000, 0, '正在召唤已强化的奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1795, 128, 31, 1, 2000, 0, '正在召唤攻击图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1796, 128, 32, 1, 2000, 0, '正在召唤攻击图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1797, 128, 33, 1, 2000, 0, '正在召唤攻击图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1798, 128, 34, 1, 2000, 0, '正在召唤攻击图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1799, 128, 35, 1, 2000, 0, '正在召唤攻击图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1800, 128, 36, 1, 2000, 0, '正在召唤防御图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1801, 128, 37, 1, 2000, 0, '正在召唤防御图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1802, 128, 38, 1, 2000, 0, '正在召唤防御图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1803, 128, 39, 1, 2000, 0, '正在召唤防御图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1804, 128, 40, 1, 2000, 0, '正在召唤防御图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1805, 78, 8, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1806, 78, 9, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1807, 78, 10, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1809, 87, 11, 1, 1000, 0, '处于中毒状态.\n\n每秒将减少HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1810, 87, 12, 1, 1000, 0, '处于中毒状态.\n\n每秒将减少HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1811, 87, 13, 1, 1000, 0, '处于中毒状态.\n\n每秒将减少HP.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1813, 108, 21, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1814, 108, 22, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1815, 108, 23, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1816, 108, 24, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1817, 108, 25, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1818, 94, 2, 1, 10000, 0, '将身体变成透明状态从而离开敌人的视野.\n\n如果距离过近将会被发现.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1819, 200, 11, 1, 0, 0, '施加给对方一定的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1820, 200, 12, 1, 0, 0, '施加给对方一定的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1821, 200, 13, 1, 0, 0, '施加给对方一定的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1822, 200, 14, 1, 0, 0, '施加给对方一定的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1823, 200, 15, 1, 0, 0, '施加给对方一定的伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1824, 201, 11, 1, 15000, 0, '减少移动速度.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1825, 201, 12, 1, 15000, 0, '减少移动速度.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1826, 201, 13, 1, 15000, 0, '减少移动速度.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1827, 201, 14, 1, 15000, 0, '减少移动速度.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1828, 201, 15, 1, 15000, 0, '减少移动速度.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1829, 38, 21, 1, 30000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1830, 215, 1, 1, 30000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1833, 45, 1, 1, 30000, 0, '怪物用hit/dd');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1834, 45, 1, 1, 30000, 0, '怪物用dv/pv');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1835, 219, 1, 1, 60000, 0, '抵抗侵蚀火焰.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1836, 210, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1837, 68, 31, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1838, 68, 32, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1839, 68, 33, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1840, 68, 34, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1841, 68, 35, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1842, 222, 1, 1, 1800000, 0, '智力,最大MP值,命中率增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1843, 19, 20, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1844, 19, 21, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1845, 19, 22, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1846, 19, 23, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1847, 130, 3, 1, 60000, 0, '受到防御图腾的效果.\n\n防止药水被破坏, 且获得对侵蚀火焰的抵抗力');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1848, 191, 11, 1, 0, 0, '个人技能初始化');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1849, 191, 12, 1, 0, 0, '工会技能初始化');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1850, 220, 1, 1, 1800000, 0, '力量,暴击几率,命中率增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1852, 221, 1, 1, 1800000, 0, '敏捷,暴击几率,命中率增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1853, 191, 15, 1, 0, 0, '获得公会技能经验值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1854, 72, 2, 1, 30000, 0, '处于暴走状态.\n\n防御力 -3\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1855, 72, 3, 1, 30000, 0, '处于暴走状态.\n\n防御力 -2\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1856, 72, 4, 1, 30000, 0, '处于暴走状态.\n\n防御力 -1\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1857, 72, 5, 1, 30000, 0, '处于暴走状态.\n\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1858, 70, 11, 1, 60000, 0, '力量得到提升.\n\n力量 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1859, 70, 12, 1, 60000, 0, '力量得到提升.\n\n力量 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1860, 70, 13, 1, 60000, 0, '力量得到提升.\n\n力量 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1861, 74, 2, 1, 20000, 0, '一定时间内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1862, 74, 3, 1, 20000, 0, '一定时间内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1863, 74, 4, 1, 20000, 0, '一定时间内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1864, 74, 5, 1, 20000, 0, '一定时间内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1865, 74, 6, 1, 20000, 0, '一定时间内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1866, 45, 11, 1, 1200000, 0, '对各种属性攻击及魔法攻击的抵抗力变高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1867, 45, 12, 1, 1200000, 0, '对各种属性攻击及魔法攻击的抵抗力变高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1868, 45, 13, 1, 1200000, 0, '对各种属性攻击及魔法攻击的抵抗力变高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1869, 45, 14, 1, 1200000, 0, '对各种属性攻击及魔法攻击的抵抗力变高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1870, 45, 15, 1, 1200000, 0, '对各种属性攻击及魔法攻击的抵抗力变高.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1871, 26, 16, 1, 1200000, 0, '使用魔法之力增加防御力.\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1872, 26, 17, 1, 1200000, 0, '使用魔法之力增加防御力.\n\n防御力 +7');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1873, 27, 16, 1, 1200000, 0, '身体像石头一样坚硬.\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1874, 27, 17, 1, 1200000, 0, '身体像石头一样坚硬.\n\n防御力 +7');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1875, 17, 41, 1, 1000, 0, '力量得到提升.\n\n力量 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1876, 17, 42, 1, 1000, 0, '力量得到提升.\n\n力量 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1877, 17, 43, 1, 1000, 0, '力量得到提升.\n\n力量 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1878, 65, 13, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1880, 71, 2, 1, 60000, 0, '防御力增加.\n\n防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1881, 71, 3, 1, 60000, 0, '防御力增加.\n\n防御力 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1882, 71, 4, 1, 60000, 0, '防御力增加.\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1883, 224, 11, 1, 5000, 0, '暴击几率瞬间增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1884, 78, 51, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1885, 78, 52, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1886, 78, 53, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1887, 78, 54, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1888, 78, 55, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1889, 79, 51, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1890, 79, 52, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1891, 79, 53, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1892, 211, 15, 1, 5000, 0, '防御力增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1893, 225, 20, 1, 20000, 0, '暴击几率增加.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1894, 199, 11, 1, 0, 0, '对已有火焰箭异常状态的对象使用时给对方及对方周围的其他人造成附加伤害.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1895, 226, 51, 1, 86400000, 0, '由于在主题网吧上线从而受到120%经验值的增益效果.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1896, 227, 51, 1, 86400000, 0, '使用高级服务从而受到120%经验值的增益效果.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1897, 64, 3, 1, 3600000, 0, 'HP恢复量+3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1898, 53, 2, 1, 2400000, 0, '攻击力增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1899, 54, 2, 1, 2400000, 0, '防御力增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1900, 18, 5, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1901, 18, 5, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1902, 18, 5, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1903, 18, 5, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1904, 18, 5, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1905, 18, 5, 1, 3600000, 0, '处于变身形态.\n\n无法骑乘德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1906, 232, 1, 11, 10000, 0, '火焰环绕 I\n激活火焰伤害\n使目标药水恢复无效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1907, 232, 2, 11, 10000, 0, '火焰环绕 II\n激活火焰伤害\n使目标药水恢复无效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1908, 232, 3, 11, 5000, 0, '火焰环绕 III\n激活火焰伤害\n使目标药水恢复无效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1909, 233, 1, 11, 10000, 0, '寒气环绕 I\n激活寒气伤害\n使目标法力恢复无效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1910, 233, 2, 11, 10000, 0, '寒气环绕 II\n激活寒气伤害\n使目标法力恢复无效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1911, 233, 3, 11, 5000, 0, '寒气环绕 III\n激活寒气伤害\n使目标法力恢复无效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1912, 235, 1, 11, 10000, 0, '闪电环绕 I\n激活闪电伤害\n环绕效果消失时，解除魔法效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1913, 235, 2, 11, 10000, 0, '闪电环绕 II\n激活闪电伤害\n环绕效果消失时，解除魔法效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1914, 235, 3, 11, 5000, 0, '闪电环绕 III\n激活闪电伤害\n环绕效果消失时，解除魔法效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1915, 234, 1, 11, 10000, 0, '侵蚀环绕 I\n减少目标的防御');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1916, 234, 2, 11, 10000, 0, '侵蚀环绕 II\n减少目标的防御');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1917, 234, 3, 11, 5000, 0, '侵蚀环绕 III\n减少目标的防御');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1918, 236, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1919, 237, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1920, 239, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1921, 238, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1922, 229, 1, 1, 120000, 0, '使元素爆炸失效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1923, 228, 1, 1, 180000, 0, '元素增幅');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1924, 230, 1, 1, 180000, 0, '对拥有元素爆炸的目标进行攻击时，激活元素爆发。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1925, 231, 1, 26, 0, 0, '元素爆发');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1926, 225, 1, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1927, 225, 2, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1928, 225, 3, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1929, 225, 4, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1930, 225, 5, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1931, 231, 2, 1, 0, 0, '火焰爆发');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1932, 231, 6, 1, 0, 0, '火焰爆发II');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1933, 231, 3, 1, 0, 0, '寒气爆发');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1934, 231, 4, 1, 0, 0, '闪电爆发');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1935, 231, 5, 1, 0, 0, '侵蚀爆发');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1936, 231, 7, 1, 0, 0, '寒气爆发II');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1937, 231, 8, 1, 0, 0, '闪电爆发II');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1938, 231, 9, 1, 0, 0, '侵蚀爆发II');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1941, 193, 1, 1, 0, 0, '699');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1942, 193, 1, 1, 0, 0, '700');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1946, 43, 15, 1, 5000, 0, '攻击力提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1947, 224, 16, 1, 5000, 0, '瞬间增加暴击率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1948, 211, 17, 1, 5000, 0, '防御力增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1949, 49, 15, 1, 5000, 0, '命中率提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1951, 240, 1, 1, 30000, 0, '似乎可以获得很好的道具。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1952, 240, 2, 1, 60000, 0, '似乎可以获得很好的道具。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1953, 240, 3, 1, 90000, 0, '似乎可以获得很好的道具。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1954, 193, 1, 1, 0, 0, '701');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1955, 193, 1, 1, 0, 0, '702');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1956, 193, 1, 1, 0, 0, '703');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1957, 193, 1, 1, 0, 0, '704');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1958, 193, 1, 1, 0, 0, '705');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1959, 193, 1, 1, 0, 0, '706');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1960, 193, 1, 1, 0, 0, '707');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1961, 193, 1, 1, 0, 0, '708');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1963, 248, 49, 1, 6000000, 0, '赋予炼金术士力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1964, 248, 49, 1, 6000000, 0, '赋予忍者少女力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1965, 248, 49, 1, 6000000, 0, '赋予巴德力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1966, 248, 49, 1, 6000000, 0, '赋予女王骑士的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1967, 248, 49, 1, 6000000, 0, '赋予春丽力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1968, 248, 49, 1, 6000000, 0, '赋予女海盗船长力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1969, 248, 49, 1, 6000000, 0, '赋予修道士力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1970, 248, 49, 1, 6000000, 0, '赋予阿修罗力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1971, 18, 140, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1972, 18, 141, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1973, 18, 142, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1974, 18, 143, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1975, 18, 144, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1976, 18, 145, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1977, 18, 146, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1978, 18, 147, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1980, 78, 56, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1981, 78, 57, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1982, 78, 58, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1983, 18, 139, 1, 300000, 2, '蕴含卡萨的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1984, 250, 1, 1, 0, 0, '累积的休眠经验被激活。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1985, 250, 2, 1, 0, 0, '累积的休眠经验被激活。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1986, 250, 3, 1, 0, 0, '累积的休眠经验被激活。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1987, 18, 139, 1, 300000, 2, '蕴含斯拉培的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1988, 18, 139, 1, 300000, 2, '蕴含诺恩的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1989, 18, 139, 1, 300000, 2, '蕴含安迪的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1990, 18, 139, 1, 300000, 3, '蕴含萨拉曼德的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1991, 18, 139, 1, 300000, 3, '蕴含希尔比的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1992, 18, 139, 1, 300000, 3, '蕴含诺因的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1993, 18, 139, 1, 300000, 3, '蕴含昂黛恩的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1994, 18, 139, 1, 300000, 4, '蕴含伊戈尼斯的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1995, 18, 139, 1, 300000, 4, '蕴含希尔莱格的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1996, 18, 139, 1, 300000, 4, '蕴含诺伊阿内的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1997, 18, 139, 1, 300000, 4, '蕴含乌达因的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1998, 18, 139, 1, 300000, 5, '蕴含伊芙利特的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (1999, 18, 139, 1, 300000, 5, '蕴含普里的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2000, 18, 139, 1, 420000, 5, '蕴含菲卡斯的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2001, 18, 139, 1, 300000, 5, '蕴含修利艾尔的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2002, 18, 139, 1, 300000, 5, '蕴含莱芙利肯的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2003, 18, 139, 1, 420000, 5, '蕴含艾丽斯的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2004, 18, 139, 1, 300000, 5, '蕴含努斯的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2005, 18, 139, 1, 300000, 5, '蕴含锡菲的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2006, 18, 139, 1, 420000, 5, '蕴含莉恩斯的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2007, 18, 139, 1, 300000, 5, '蕴含艾尔拉恩的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2008, 18, 139, 1, 300000, 5, '蕴含莱伊克的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2009, 18, 139, 1, 420000, 5, '蕴含艾丽塞德的灵魂。\n\n未进行变身也会获得远程能力值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2010, 82, 8, 1, 0, 0, 'MP +10000');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2011, 249, 1, 1, 0, 0, '解除杀人犯状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2012, 242, 1, 1, 86400000, 0, '只能赋予指定异常状态（昏厥攻击、沉默、无视护甲）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2013, 243, 2, 1, 86400000, 0, '召唤魔像 LV5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2014, 243, 3, 1, 86400000, 0, '召唤魔像 LV4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2015, 243, 4, 1, 86400000, 0, '召唤魔像 LV3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2016, 243, 5, 1, 86400000, 0, '召唤魔像 LV2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2017, 243, 6, 1, 86400000, 0, '召唤魔像 LV1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2018, 243, 12, 1, 0, 0, '召唤魔像 LV0');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2020, 243, 7, 1, 0, 0, '召唤魔像 LV4使用技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2021, 243, 8, 1, 0, 0, '召唤魔像 LV3使用技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2022, 243, 9, 1, 0, 0, '召唤魔像 LV2使用技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2023, 243, 10, 1, 0, 0, '召唤魔像 LV1使用技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2024, 243, 11, 1, 0, 0, '召唤魔像 LV0使用技能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2025, 244, 1, 1, 4500, 0, '施展动作（陨石 - 沉默）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2026, 244, 2, 1, 4500, 0, '施展动作（火球术 - 昏厥攻击）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2027, 244, 3, 1, 4500, 0, '施展动作（震地）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2028, 244, 4, 1, 4500, 0, '施展动作（召唤魔像 - 无视护甲）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2029, 245, 1, 1, 6000, 0, '失败动作（陨石 - 沉默）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2030, 245, 2, 1, 6000, 0, '失败动作（火球术 - 昏厥攻击）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2031, 245, 3, 1, 6000, 0, '失败动作（震地）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2032, 245, 4, 1, 6000, 0, '失败动作（召唤魔像 - 无视护甲）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2033, 246, 1, 1, 0, 0, '成功动作（陨石 - 沉默）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2034, 246, 2, 1, 0, 0, '成功动作（火球术 - 昏厥攻击）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2035, 246, 3, 1, 0, 0, '成功动作（震地）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2036, 246, 4, 1, 0, 0, '成功动作（召唤魔像 - 无视护甲）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2037, 246, 5, 1, 0, 0, '成功动作（陨石）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2038, 246, 6, 1, 0, 0, '成功动作（火球术）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2039, 246, 7, 1, 0, 0, '成功动作（震地）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2040, 246, 8, 1, 0, 0, '成功动作（召唤魔像）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2041, 247, 1, 1, 4500, 0, '攻击技能准备（陨石 - 沉默）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2042, 247, 2, 1, 4500, 0, '攻击技能准备（火球术 - 昏厥攻击）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2043, 247, 3, 1, 4500, 0, '攻击技能准备（震地）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2044, 247, 4, 1, 4500, 0, '攻击技能准备（召唤魔像 - 无视护甲）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2045, 247, 5, 1, 0, 0, '攻击技能施展（陨石 - 昏厥攻击）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2046, 247, 6, 1, 0, 0, '攻击技能施展（陨石 - 无视护甲）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2047, 247, 7, 1, 0, 0, '攻击技能施展（火球术 - 沉默）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2048, 247, 8, 1, 0, 0, '攻击技能施展（火球术 - 无视护甲）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2049, 247, 9, 1, 0, 0, '攻击技能施展（召唤魔像 - 昏厥攻击）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2050, 247, 10, 1, 0, 0, '攻击技能施展（召唤魔像 - 沉默）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2051, 78, 60, 1, 86400000, 0, '召唤魔像 Lv5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2052, 78, 61, 1, 86400000, 0, '召唤魔像 Lv4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2053, 78, 62, 1, 86400000, 0, '召唤魔像 Lv3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2054, 78, 63, 1, 86400000, 0, '召唤魔像 Lv2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2055, 78, 64, 1, 86400000, 0, '召唤魔像 Lv1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2056, 78, 65, 1, 86400000, 0, '召唤魔像 Lv0');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2057, 132, 6, 1, 30000, 0, '受到火焰伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2058, 78, 59, 1, 120000, 0, '防御提升（净化）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2059, 251, 1, 1, 600000, 0, '跨服战场变身时，外形保持不变，\n只赋予变身能力值，\n可进行远程攻击。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2060, 193, 1, 1, 0, 0, '709');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2061, 190, 1, 1, 3600000, 0, '闪避怪物使用的诅咒和恐惧几率提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2062, 18, 151, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2063, 18, 152, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2064, 18, 153, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2065, 18, 154, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2066, 18, 155, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2067, 18, 156, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2068, 18, 157, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2069, 18, 158, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2070, 18, 159, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2071, 18, 160, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2072, 18, 161, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2073, 18, 162, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2074, 18, 163, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2075, 18, 164, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2076, 18, 165, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2077, 18, 166, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2078, 18, 167, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2079, 18, 168, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2080, 18, 169, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2081, 18, 170, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2082, 18, 171, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2083, 18, 172, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2084, 18, 173, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2085, 18, 174, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2086, 18, 175, 1, 60000, 2, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2087, 225, 6, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2088, 225, 7, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2089, 225, 8, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2090, 225, 9, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2091, 225, 10, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2092, 2, 5, 1, 30000, 0, '全身麻痹。\n\n无法移动。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2093, 16, 5, 1, 30000, 0, '喊不出声音。\n\n无法使用魔法及技能。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2094, 18, 176, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2095, 18, 177, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2096, 18, 178, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2097, 99, 50, 1, 10000, 0, '巴普麦特刷新');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2098, 100, 50, 1, 10000, 0, '巴普麦特刷新');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2099, 78, 99, 1, 86400000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2100, 241, 1, 1, 86400000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2101, 241, 2, 1, 86400000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2102, 241, 3, 1, 86400000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2103, 241, 4, 1, 86400000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2104, 241, 5, 1, 86400000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2105, 79, 52, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2108, 65, 5, 1, 1200000, 0, '无视负重MP恢复+4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2109, 225, 11, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2110, 225, 12, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2111, 225, 13, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2112, 225, 14, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2113, 225, 15, 1, 10000, 0, '增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2114, 252, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2115, 252, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2116, 252, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2117, 252, 4, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2118, 252, 5, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2119, 252, 6, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2120, 252, 7, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2121, 252, 8, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2122, 253, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2123, 253, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2124, 253, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2125, 253, 4, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2126, 253, 5, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2127, 253, 6, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2128, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2129, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2130, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2131, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2132, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2133, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2134, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2135, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2136, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2137, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2138, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2139, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2140, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2141, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2142, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2143, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2144, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2145, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2146, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2147, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2148, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2149, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2150, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2151, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2152, 254, 50, 1, 0, 0, '透明披风装备状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2154, 255, 50, 1, 0, 0, '注视者披风装备状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2156, 255, 1, 1, 120000, 0, '注视目标');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2157, 224, 13, 1, 3000, 0, '瞬间增加暴击率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2158, 224, 14, 1, 3000, 0, '瞬间增加暴击率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2159, 224, 15, 1, 3000, 0, '瞬间增加暴击率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2160, 43, 14, 1, 3000, 0, '攻击力提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2161, 254, 1, 1, 0, 0, '转换为透明');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2162, 256, 1, 1, 3000, 0, '所有攻击力上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2165, 257, 1, 1, 3000, 0, '暴击率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2166, 258, 1, 1, 2000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2167, 258, 2, 1, 4000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2168, 258, 3, 1, 8000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2169, 259, 1, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2170, 259, 2, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2171, 259, 3, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2172, 260, 1, 1, 2000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2173, 260, 2, 1, 4000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2174, 260, 3, 1, 8000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2175, 261, 1, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2176, 261, 2, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2177, 261, 3, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2178, 262, 1, 1, 10000, 0, '增加药水恢复量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2179, 262, 2, 1, 10000, 0, '增加药水恢复量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2180, 262, 3, 1, 10000, 0, '增加药水恢复量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2181, 263, 1, 1, 10000, 0, '增加回避率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2182, 263, 2, 1, 10000, 0, '增加回避率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2183, 263, 3, 1, 10000, 0, '增加回避率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2184, 264, 1, 1, 6000, 0, '降低暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2185, 264, 2, 1, 7000, 0, '降低暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2186, 264, 3, 1, 8000, 0, '降低暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2189, 18, 179, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2190, 18, 180, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2191, 18, 181, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2192, 18, 182, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2193, 18, 183, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2194, 18, 184, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2195, 18, 185, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2197, 248, 49, 1, 6000000, 0, '赋予恶魔小丑力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2198, 248, 49, 1, 6000000, 0, '赋予大力神力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2199, 248, 49, 1, 6000000, 0, '赋予地狱魔兽力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2200, 248, 49, 1, 6000000, 0, '赋予地狱弓箭手力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2201, 248, 49, 1, 6000000, 0, '赋予女神咖莉力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2202, 248, 49, 1, 6000000, 0, '赋予恶魔追猎者力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2203, 248, 49, 1, 6000000, 0, '赋予狂战士力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2204, 193, 1, 1, 0, 0, '714');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2205, 193, 1, 1, 0, 0, '715');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2206, 193, 1, 1, 0, 0, '716');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2207, 193, 1, 1, 0, 0, '717');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2208, 193, 1, 1, 0, 0, '718');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2209, 193, 1, 1, 0, 0, '719');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2210, 193, 1, 1, 0, 0, '720');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2211, 42, 8, 1, 0, 0, '解除效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2212, 251, 1, 1, 0, 0, '精神分裂');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2213, 251, 1, 1, 0, 0, '觉醒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2214, 265, 1, 1, 10000, 0, '降低火焰箭、施毒、毒牙、地狱烈焰的\n持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2215, 265, 2, 1, 10000, 0, '降低火焰箭、施毒、毒牙、地狱烈焰的\n持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2216, 265, 3, 1, 10000, 0, '降低火焰箭、施毒、毒牙、地狱烈焰的\n持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2217, 267, 1, 1, 12000, 0, '额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2218, 267, 2, 1, 12000, 0, '额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2219, 267, 3, 1, 12000, 0, '额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2220, 251, 1, 1, 0, 0, '复制');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2221, 266, 51, 1, 86400000, 0, '透明状态下，增加移动速度');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2222, 266, 52, 1, 86400000, 0, '透明状态下，增加移动速度');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2223, 266, 53, 1, 86400000, 0, '透明状态下，增加移动速度');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2224, 251, 1, 1, 0, 0, '物理反击1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2225, 268, 1, 1, 10000, 0, '额外增加近程防御');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2226, 268, 2, 1, 10000, 0, '额外增加近程防御');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2227, 268, 3, 1, 10000, 0, '额外增加近程防御');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2228, 269, 1, 1, 10000, 0, '额外增加所有属性+1。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2229, 269, 2, 1, 15000, 0, '额外增加所有属性+2。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2230, 269, 3, 1, 20000, 0, '额外增加所有属性+3。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2231, 46, 15, 1, 1800000, 0, '移动速度上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2232, 270, 1, 1, 86400000, 0, '安息中。\n使用隐身术时，\n额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2233, 94, 3, 1, 180000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2234, 94, 4, 1, 180000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2235, 94, 5, 1, 180000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2236, 94, 6, 1, 180000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2237, 94, 7, 1, 180000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2239, 270, 2, 1, 86400000, 0, '安息中。\n使用隐身术时，\n额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2240, 270, 3, 1, 86400000, 0, '安息中。\n使用隐身术时，\n额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2241, 270, 4, 1, 86400000, 0, '安息中。\n使用隐身术时，\n额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2242, 270, 5, 1, 86400000, 0, '安息中。\n使用隐身术时，\n额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2243, 19, 24, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2244, 171, 11, 11, 3000, 0, '塔纳托斯透明技能专用');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2245, 95, 6, 1, 120000, 0, '被敌人发现。\n\n不可隐身。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2246, 251, 1, 1, 0, 0, '物理反击2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2247, 251, 1, 1, 0, 0, '物理反击3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2248, 251, 1, 1, 0, 0, '魔法反击1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2249, 251, 1, 1, 0, 0, '魔法反击2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2250, 251, 1, 1, 0, 0, '魔法反击3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2251, 127, 59, 1, 1700, 2, '无法攻击。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2252, 127, 60, 1, 1400, 2, '无法攻击。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2253, 127, 61, 1, 1100, 2, '无法攻击。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2254, 162, 11, 1, 600000, 0, '装备了反击之盾。\n\n部分魔法按几率反射给释放者。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2261, 18, 99, 1, 300000, 6, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2262, 18, 99, 1, 300000, 6, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2263, 18, 99, 1, 300000, 6, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2264, 18, 99, 1, 300000, 6, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2265, 18, 99, 1, 300000, 7, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2266, 18, 99, 1, 300000, 7, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2267, 18, 99, 1, 300000, 7, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2268, 18, 99, 1, 300000, 7, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2269, 18, 99, 1, 300000, 8, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2270, 18, 99, 1, 300000, 8, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2271, 18, 99, 1, 300000, 8, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2272, 18, 99, 1, 300000, 8, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2273, 18, 99, 1, 300000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2274, 18, 99, 1, 300000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2275, 18, 99, 1, 420000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2276, 18, 99, 1, 300000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2277, 18, 99, 1, 300000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2278, 18, 99, 1, 420000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2279, 18, 99, 1, 300000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2280, 18, 99, 1, 300000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2281, 18, 99, 1, 420000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2282, 18, 99, 1, 300000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2283, 18, 99, 1, 300000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2284, 18, 99, 1, 420000, 9, '狂暴变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2285, 271, 1, 1, 300000, 0, '混沌降神的\n能力值增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2287, 18, 186, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2288, 18, 187, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2289, 18, 188, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2290, 18, 189, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2291, 18, 190, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2292, 18, 192, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2293, 18, 193, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2294, 269, 4, 1, 25000, 0, '额外增加所有属性+4。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2295, 269, 5, 1, 30000, 0, '额外增加所有属性+5。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2296, 265, 4, 1, 10000, 0, '降低火焰箭、施毒、毒牙、地狱烈焰的\n持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2297, 265, 5, 1, 10000, 0, '降低火焰箭、施毒、毒牙、地狱烈焰的\n持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2298, 268, 4, 1, 10000, 0, '额外增加近程防御');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2299, 268, 5, 1, 10000, 0, '额外增加近程防御');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2300, 258, 4, 1, 10000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2301, 258, 5, 1, 12000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2302, 259, 4, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2303, 259, 5, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2304, 260, 4, 1, 10000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2305, 260, 5, 1, 12000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2306, 261, 4, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2307, 261, 5, 1, 5000, 0, '所有命中率上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2308, 267, 4, 1, 12000, 0, '额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2309, 267, 5, 1, 12000, 0, '额外增加移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2310, 127, 62, 1, 800, 2, '无法攻击。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2311, 127, 63, 1, 500, 2, '无法攻击。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2312, 262, 4, 1, 10000, 0, '增加药水恢复量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2313, 262, 5, 1, 10000, 0, '增加药水恢复量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2314, 263, 4, 1, 10000, 0, '增加回避率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2315, 263, 5, 1, 10000, 0, '增加回避率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2316, 266, 54, 1, 86400000, 0, '透明状态下，增加移动速度');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2317, 266, 55, 1, 86400000, 0, '透明状态下，增加移动速度');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2318, 251, 1, 1, 0, 0, '物理反击4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2319, 251, 1, 1, 0, 0, '物理反击5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2320, 251, 1, 1, 0, 0, '魔法反击4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2321, 251, 1, 1, 0, 0, '魔法反击5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2322, 264, 4, 1, 9000, 0, '降低暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2323, 264, 5, 1, 10000, 0, '降低暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2325, 18, 194, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2327, 116, 4, 1, 0, 0, '狩猎时物品掉落几率增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2328, 116, 5, 1, 0, 0, '狩猎时物品掉落几率增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2329, 116, 6, 1, 0, 0, '狩猎时物品掉落几率增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2330, 116, 7, 1, 0, 0, '狩猎时物品掉落几率增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2333, 226, 52, 1, 86400000, 0, '由于在主题网吧上线从而受到140%经验值的增益效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2334, 226, 53, 1, 86400000, 0, '由于在主题网吧上线从而受到160%经验值的增益效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2335, 273, 51, 1, 86400000, 0, '土豪服务从而受到120%经验值的增益效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2336, 273, 52, 1, 86400000, 0, '土豪服务从而受到140%经验值的增益效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2337, 273, 53, 1, 86400000, 0, '土豪服务从而受到160%经验值的增益效果');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2339, 272, 51, 1, 1800000, 0, '黑暗洞窟封印地区\n\n可进入黑暗洞窟封印地区。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2340, 272, 51, 1, 1800000, 0, '不死地牢封印地区\n\n可进入不死地牢封印地区。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2341, 193, 1, 1, 0, 0, '805');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2342, 193, 1, 1, 0, 0, '806');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2347, 77, 30, 1, 600000, 0, '无视负重HP恢复+2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2349, 248, 49, 1, 6000000, 0, '赋予粉嫩护士力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2350, 248, 49, 1, 6000000, 0, '赋予风精灵力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2351, 18, 99, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2352, 18, 99, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2353, 82, 9, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2355, 272, 61, 1, 1800000, 0, '黑龙沼泽封印地区\n\n可进入黑龙沼泽封印地区。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2356, 272, 61, 1, 1800000, 0, '帝王墓穴封印地区\n\n可进入帝王墓穴封印地区。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2357, 252, 9, 1, 0, 0, '狩猎时，可对怪物造成额外伤害。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2358, 252, 10, 1, 0, 0, '狩猎时，可对怪物造成额外伤害。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2359, 130, 4, 1, 180000, 0, '暂时获得防止药水被烈焰风暴破坏的效果。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2361, 276, 51, 1, 300000, 0, '增加攻击力、防御力\n\n怪物狩猎中胜方的部分玩家可以获得。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2362, 251, 1, 1, 600000, 0, '受到精灵的祝福，使攻击力提高。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2363, 251, 2, 1, 600000, 0, '受到精灵的祝福，使攻击力提高。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2364, 274, 1, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的蓝色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2365, 274, 2, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的蓝色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2366, 274, 3, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的蓝色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2367, 274, 4, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的蓝色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2368, 274, 5, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的蓝色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2369, 275, 1, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的红色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2370, 275, 2, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的红色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2371, 275, 3, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的红色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2372, 275, 4, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的红色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2373, 275, 5, 1, 60000, 0, '增加攻击力、防御力\n\n不能与鲁普的红色气息叠加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2374, 52, 15, 71, 3000, 0, '移动速度下降。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2375, 189, 1, 1, 0, 0, '重击命中时，冷却时间会初始化。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2376, 189, 2, 1, 0, 0, '重击命中时，冷却时间会初始化。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2377, 189, 3, 1, 0, 0, '重击命中时，冷却时间会初始化。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2378, 189, 1, 1, 0, 0, '重击命中时，降低目标的移动速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2379, 189, 1, 1, 0, 0, '增加猛力攻击的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2380, 189, 2, 1, 0, 0, '增加猛力攻击的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2381, 189, 3, 1, 0, 0, '增加猛力攻击的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2382, 189, 4, 1, 0, 0, '增加猛力攻击的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2383, 189, 5, 1, 0, 0, '增加猛力攻击的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2384, 189, 1, 1, 0, 0, '增加暴走的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2385, 189, 2, 1, 0, 0, '增加暴走的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2386, 189, 3, 1, 0, 0, '增加暴走的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2388, 189, 11, 1, 0, 0, '增加疼痛麻痹的持续时间，追加额外防御。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2389, 189, 12, 1, 0, 0, '增加疼痛麻痹的持续时间，追加额外防御。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2390, 189, 13, 1, 0, 0, '增加疼痛麻痹的持续时间，追加额外防御。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2391, 189, 1, 1, 0, 0, '额外增加生命绽放的生命值。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2392, 189, 1, 1, 0, 0, '减少魔法抵抗的冷却时间，立即施展。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2393, 189, 2, 1, 0, 0, '减少魔法抵抗的冷却时间，立即施展。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2394, 189, 3, 1, 0, 0, '减少魔法抵抗的冷却时间，立即施展。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2395, 78, 70, 1, 5000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2396, 189, 1, 1, 0, 0, '使用祝福光环时，增加持续时间，额外增加命中率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2397, 189, 2, 1, 0, 0, '使用祝福光环时，增加持续时间，额外增加命中率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2398, 189, 3, 1, 0, 0, '使用祝福光环时，增加持续时间，额外增加命中率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2399, 49, 1, 1, 1000, 0, '命中率提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2400, 189, 1, 1, 0, 0, '使用凝神术时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2401, 189, 2, 1, 0, 0, '使用凝神术时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2402, 189, 3, 1, 0, 0, '使用凝神术时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2403, 189, 4, 1, 0, 0, '使用凝神术时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2404, 189, 5, 1, 0, 0, '使用凝神术时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2405, 189, 1, 1, 0, 0, '使用法力转换时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2406, 189, 2, 1, 0, 0, '使用法力转换时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2407, 189, 3, 1, 0, 0, '使用法力转换时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2408, 189, 1, 1, 0, 0, '侵蚀火焰的药水破坏数增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2409, 189, 2, 1, 0, 0, '侵蚀火焰的药水破坏数增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2410, 189, 3, 1, 0, 0, '侵蚀火焰的药水破坏数增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2412, 277, 1, 1, 0, 0, '集中精神，减少所有技能的MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2413, 277, 2, 1, 0, 0, '集中精神，减少所有技能的MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2414, 277, 3, 1, 0, 0, '集中精神，减少所有技能的MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2415, 49, 1, 1, 5000, 0, '魔法命中率提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2416, 189, 1, 1, 0, 0, '恢复魔力吸食消耗的部分MP。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2417, 189, 2, 1, 0, 0, '恢复魔力吸食消耗的部分MP。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2418, 189, 3, 1, 0, 0, '恢复魔力吸食消耗的部分MP。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2419, 189, 1, 1, 0, 0, '侦查范围增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2420, 189, 1, 1, 0, 0, '增加魔法蛛丝的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2421, 189, 2, 1, 0, 0, '增加魔法蛛丝的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2422, 189, 3, 1, 0, 0, '增加魔法蛛丝的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2423, 189, 1, 1, 0, 0, '增加火焰箭爆炸伤害。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2424, 189, 1, 1, 0, 0, '使用音速屏障时，增加最大生命力。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2425, 189, 2, 1, 0, 0, '使用音速屏障时，增加最大生命力。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2426, 189, 3, 1, 0, 0, '使用音速屏障时，增加最大生命力。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2427, 12, 1, 1, 1000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2428, 12, 2, 1, 1000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2429, 12, 3, 1, 1000, 0, '攻击速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2430, 189, 1, 1, 0, 0, '使用疾速射击时，额外增加攻击速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2431, 189, 2, 1, 0, 0, '使用疾速射击时，额外增加攻击速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2432, 189, 3, 1, 0, 0, '使用疾速射击时，额外增加攻击速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2433, 43, 1, 1, 1000, 0, '攻击力提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2434, 43, 2, 1, 1000, 0, '攻击力提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2435, 43, 3, 1, 1000, 0, '攻击力提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2436, 189, 1, 1, 0, 0, '增加陷阱的范围。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2437, 189, 2, 1, 0, 0, '增加陷阱的范围。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2438, 189, 3, 1, 0, 0, '增加陷阱的范围。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2440, 8, 1, 1, 500, 0, '受到粘性陷阱的阻碍，无法移动。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2441, 189, 1, 1, 0, 0, '增加粘性陷阱的减速效果。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2442, 189, 1, 1, 0, 0, '使用无视护甲时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2443, 189, 2, 1, 0, 0, '使用无视护甲时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2444, 189, 3, 1, 0, 0, '使用无视护甲时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2445, 189, 1, 1, 0, 0, '使用无视护甲时，延长攻击力增加的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2446, 189, 2, 1, 0, 0, '使用无视护甲时，延长攻击力增加的持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2447, 79, 1, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2448, 79, 2, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2449, 189, 1, 1, 0, 0, '无视护甲命中时，额外降低目标的防御力。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2450, 189, 2, 1, 0, 0, '无视护甲命中时，额外降低目标的防御力。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2451, 189, 1, 1, 0, 0, '使用要害攻击时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2452, 189, 2, 1, 0, 0, '使用要害攻击时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2453, 189, 3, 1, 0, 0, '使用要害攻击时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2454, 189, 4, 1, 0, 0, '使用要害攻击时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2455, 189, 5, 1, 0, 0, '使用要害攻击时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2456, 278, 1, 1, 0, 0, '暴击时，额外增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2457, 278, 2, 1, 0, 0, '暴击时，额外增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2458, 278, 3, 1, 0, 0, '暴击时，额外增加暴击率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2459, 279, 1, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2460, 279, 2, 1, 2000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2461, 279, 3, 1, 3000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2462, 97, 1, 1, 180000, 0, '蕴含猛毒的气息，增加毒瓶发动几率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2464, 189, 1, 1, 0, 0, '增加毒牙的发动几率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2465, 189, 1, 1, 0, 0, '使用昏厥攻击时，减少冷却时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2466, 189, 2, 1, 0, 0, '使用昏厥攻击时，减少冷却时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2467, 189, 3, 1, 0, 0, '使用昏厥攻击时，减少冷却时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2468, 97, 1, 1, 10000, 0, '中毒。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2469, 189, 1, 1, 0, 0, '暗杀技能命中后，使目标中毒。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2470, 189, 1, 1, 0, 0, '使用疾行时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2471, 189, 2, 1, 0, 0, '使用疾行时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2472, 189, 3, 1, 0, 0, '使用疾行时，减少MP消耗量。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2488, 78, 1, 1, 1000, 0, '增加近程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2489, 78, 2, 1, 1000, 0, '增加近程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2490, 78, 3, 1, 1000, 0, '增加近程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2491, 78, 4, 1, 1000, 0, '增加近程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2492, 78, 5, 1, 1000, 0, '增加近程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2493, 78, 1, 1, 1000, 0, '增加远程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2494, 78, 2, 1, 1000, 0, '增加远程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2495, 78, 3, 1, 1000, 0, '增加远程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2496, 78, 4, 1, 1000, 0, '增加远程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2497, 78, 5, 1, 1000, 0, '增加远程回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2498, 78, 1, 1, 1000, 0, '增加魔法回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2499, 78, 2, 1, 1000, 0, '增加魔法回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2500, 78, 3, 1, 1000, 0, '增加魔法回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2501, 78, 4, 1, 1000, 0, '增加魔法回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2502, 78, 5, 1, 1000, 0, '增加魔法回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2503, 78, 1, 1, 1000, 0, '使用回避技能时，额外增加回避率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2504, 189, 1, 1, 0, 0, '使用回避技能时，额外增加回避率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2505, 189, 1, 1, 0, 0, '增加陨石术的命中和伤害。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2506, 189, 2, 1, 0, 0, '增加陨石术的命中和伤害。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2507, 189, 3, 1, 0, 0, '增加陨石术的命中和伤害。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2511, 189, 1, 1, 0, 0, '增加海神之怒的命中和攻击速度降低效果。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2512, 189, 2, 1, 0, 0, '增加海神之怒的命中和攻击速度降低效果。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2513, 189, 3, 1, 0, 0, '增加海神之怒的命中和攻击速度降低效果。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2514, 132, 2, 1, 1000, 0, '受到地狱烈焰伤害，HP下降。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2515, 189, 1, 1, 0, 0, '增加地狱烈焰的初始伤害。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2516, 189, 1, 1, 0, 0, '增加地狱烈焰的命中和持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2517, 189, 2, 1, 0, 0, '增加地狱烈焰的命中和持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2518, 189, 3, 1, 0, 0, '增加地狱烈焰的命中和持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2519, 189, 1, 1, 0, 0, '使用狂暴时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2520, 189, 2, 1, 0, 0, '使用狂暴时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2521, 189, 3, 1, 0, 0, '使用狂暴时，增加持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2522, 79, 1, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2523, 61, 1, 1, 1000, 0, '受到狂暴效果，攻击力大幅提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2524, 189, 1, 1, 0, 0, '使用狂暴时，增加攻击力和防御力。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2525, 49, 1, 1, 1000, 0, '受到攻击图腾的效果，命中率提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2526, 49, 2, 1, 1000, 0, '受到攻击图腾的效果，命中率提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2527, 78, 1, 1, 1000, 0, '受到防御图腾的效果，防御提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2528, 78, 2, 1, 1000, 0, '受到防御图腾的效果，防御提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2529, 189, 1, 1, 0, 0, '攻击图腾附加命中。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2530, 189, 2, 1, 0, 0, '攻击图腾附加命中。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2531, 189, 3, 1, 0, 0, '攻击图腾附加命中。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2532, 189, 1, 1, 0, 0, '防御图腾附加回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2533, 189, 2, 1, 0, 0, '防御图腾附加回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2534, 12, 1, 1, 1000, 0, '受到图腾的效果，攻击速度增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2535, 12, 2, 1, 1000, 0, '受到图腾的效果，攻击速度增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2536, 12, 3, 1, 1000, 0, '受到图腾的效果，攻击速度增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2537, 12, 4, 1, 1000, 0, '受到图腾的效果，攻击速度增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2538, 12, 5, 1, 1000, 0, '受到图腾的效果，攻击速度增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2539, 128, 41, 1, 2000, 0, '正在召唤疾风图腾图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2540, 128, 42, 1, 2000, 0, '正在召唤疾风图腾图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2541, 128, 43, 1, 2000, 0, '正在召唤疾风图腾图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2542, 128, 44, 1, 2000, 0, '正在召唤疾风图腾图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2543, 128, 45, 1, 2000, 0, '正在召唤疾风图腾图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2544, 189, 1, 1, 0, 0, '减少所有图腾的冷却时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2545, 189, 2, 1, 0, 0, '减少所有图腾的冷却时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2549, 18, 195, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2550, 18, 196, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2551, 18, 197, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2552, 41, 1, 1, 0, 0, '强效毒瓶生成[短剑]。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2553, 41, 2, 1, 0, 0, '强效毒瓶生成[拳剑]。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2554, 189, 1, 1, 0, 0, '删除魔法抵抗强化动作。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2555, 189, 1, 1, 0, 0, '删除陷阱的动作。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2556, 18, 100, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2557, 18, 101, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2558, 18, 102, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2559, 18, 103, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2561, 18, 104, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2563, 248, 49, 1, 6000000, 0, '赋予名妓黄真伊力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2564, 248, 49, 1, 6000000, 0, '赋予暴走机车女力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2565, 248, 49, 1, 6000000, 0, '赋予男爵火枪手力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2566, 248, 49, 1, 6000000, 0, '赋予惊艳女狙击手力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2567, 248, 49, 1, 6000000, 0, '赋予暮色黑寡妇力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2568, 193, 1, 1, 0, 0, '952');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2569, 193, 1, 1, 0, 0, '953');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2570, 193, 1, 1, 0, 0, '954');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2571, 193, 1, 1, 0, 0, '955');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2572, 193, 1, 1, 0, 0, '956');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2573, 189, 1, 1, 0, 0, '使用祝福光环时，命中率增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2574, 57, 11, 100, 1000, 0, '受到致命伤害，防御力下降。\n\n防御力 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2575, 57, 12, 99, 2000, 0, '受到致命伤害，防御力下降。\n\n防御力 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2576, 57, 13, 98, 3000, 0, '受到致命伤害，防御力下降。\n\n防御力 -4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2577, 57, 14, 98, 3000, 0, '受到致命伤害，防御力下降。\n\n防御力 -5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2578, 57, 15, 98, 3000, 0, '受到致命伤害，防御力下降。\n\n防御力 -6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2579, 57, 16, 98, 3000, 0, '受到致命伤害，防御力下降。\n\n防御力 -8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2580, 57, 17, 98, 3000, 0, '受到致命伤害，防御力下降。\n\n防御力 -9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2581, 57, 18, 98, 3000, 0, '受到致命伤害，防御力下降。\n\n防御力 -10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2582, 57, 19, 98, 3000, 0, '受到致命伤害，防御力下降。\n\n防御力 -12');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2583, 18, 99, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2584, 18, 99, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2585, 18, 99, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2586, 18, 99, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2587, 18, 99, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2591, 1, 11, 100, 1000, 0, '全身缠绕着电流。\n\n命中率 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2592, 1, 12, 99, 2000, 0, '全身缠绕着电流。\n\n命中率 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2593, 1, 13, 98, 3000, 0, '全身缠绕着电流。\n\n命中率 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2594, 1, 14, 98, 3000, 0, '全身缠绕着电流。\n\n命中率 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2595, 1, 15, 98, 3000, 0, '全身缠绕着电流。\n\n命中率 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2596, 1, 16, 98, 3000, 0, '全身缠绕着电流。\n\n命中率 -4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2597, 1, 17, 98, 3000, 0, '全身缠绕着电流。\n\n命中率 -5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2598, 1, 18, 98, 3000, 0, '全身缠绕着电流。\n\n命中率 -6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2599, 1, 19, 98, 3000, 0, '全身缠绕着电流。\n\n命中率 -6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2600, 7, 11, 100, 1000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2601, 7, 12, 99, 2000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2602, 7, 13, 98, 3000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2603, 7, 14, 98, 3000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2604, 7, 15, 98, 3000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2605, 7, 16, 98, 3000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2606, 7, 17, 98, 3000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2607, 7, 18, 98, 3000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2608, 7, 19, 98, 3000, 0, '受到巴普麦特的诅咒，身体迟钝。\n\n回避率 -12');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2618, 132, 11, 100, 1000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2619, 132, 12, 99, 2000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2620, 132, 13, 98, 3000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2621, 132, 14, 98, 3000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -25');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2622, 132, 15, 98, 3000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -30');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2623, 132, 16, 98, 3000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -40');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2624, 132, 17, 98, 3000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -45');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2625, 132, 18, 98, 3000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -50');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2626, 132, 19, 98, 3000, 0, '受到伊芙利特的火焰伤害，HP减少。\n\n HP减少 -60');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2627, 15, 11, 100, 1000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -1、敏捷 -1、智力 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2628, 15, 12, 99, 2000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -1、敏捷 -2、智力 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2629, 15, 13, 98, 3000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -2、敏捷 -2、智力 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2630, 15, 14, 98, 3000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -2、敏捷 -3、智力 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2631, 15, 15, 98, 3000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -3、敏捷 -3、智力 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2632, 15, 16, 98, 3000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -3、敏捷 -4、智力 -4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2633, 15, 17, 98, 3000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -4、敏捷 -4、智力 -4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2634, 15, 18, 98, 3000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -4、敏捷 -5、智力 -5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2635, 15, 19, 98, 3000, 0, '受到盈月的气息，全身颤抖。\n\n力量 -5、敏捷 -5、智力 -5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2636, 142, 1, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2637, 142, 2, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2638, 142, 3, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +30');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2639, 142, 4, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +40');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2640, 286, 1, 1, 230000, 0, '提升最大HP 3%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2641, 286, 2, 1, 230000, 0, '提升最大HP 6%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2642, 142, 5, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +50');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2643, 142, 6, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +60');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2644, 286, 3, 1, 230000, 0, '提升最大HP 10%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2645, 142, 7, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +70');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2646, 142, 8, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +80');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2647, 142, 9, 1, 0, 0, '受到洛利肯的气息，增加生命值。\n\nHP +100');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2648, 189, 1, 1, 0, 0, '音速屏障强化2、提升最大HP 3%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2649, 189, 2, 1, 0, 0, '音速屏障强化2、提升最大HP 6%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2650, 189, 3, 1, 0, 0, '音速屏障强化2、提升最大HP 10%');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2651, 154, 1, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2652, 154, 2, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2653, 154, 3, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2654, 154, 4, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2655, 154, 5, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2656, 154, 6, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +7');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2657, 154, 7, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2658, 154, 8, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2659, 154, 9, 1, 0, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2660, 152, 1, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2661, 152, 2, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2662, 152, 3, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2663, 152, 4, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2664, 152, 5, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2665, 152, 6, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2666, 152, 7, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +7');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2667, 152, 8, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2668, 152, 9, 1, 0, 0, '用魔力保护身体，小幅减少所受到的魔法伤害。\n\n魔法防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2669, 152, 11, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2670, 152, 12, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2671, 152, 13, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2672, 152, 14, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2673, 152, 15, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2674, 152, 16, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +12');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2675, 152, 17, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +14');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2676, 152, 18, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +16');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2677, 152, 19, 1, 0, 0, '用魔力保护身体，大幅减少所受到的魔法伤害。\n\n魔法防御力 +20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2678, 154, 1, 1, 0, 0, '受到披风神秘的力量，防御力小幅增加。\n\n防御力 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2679, 154, 2, 1, 0, 0, '受到披风神秘的力量，防御力小幅增加。\n\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2680, 154, 3, 1, 0, 0, '受到披风神秘的力量，防御力小幅增加。\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2681, 154, 4, 1, 0, 0, '受到披风神秘的力量，防御力小幅增加。\n\n防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2682, 154, 5, 1, 0, 0, '受到披风神秘的力量，防御力小幅增加。\n\n防御力 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2683, 154, 6, 1, 0, 0, '受到披风神秘的力量，防御力小幅增加。\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2684, 154, 7, 1, 0, 0, '受到披风神秘的力量，防御力小幅增加。\n\n防御力 +7');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2685, 154, 8, 1, 0, 0, '受到披风神秘的力量，防御力小幅增加。\n\n防御力 +8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2687, 163, 1, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +10、MP +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2688, 163, 2, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +15、MP +15');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2689, 163, 3, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +20、MP +20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2690, 163, 4, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +30、MP +30');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2691, 163, 5, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +40、MP +40');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2692, 163, 6, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +50、MP +50');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2693, 163, 7, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +60、MP +60');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2694, 163, 8, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +70、MP +70');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2695, 163, 9, 1, 0, 0, '受到盈月的影响，充满不明的力量。\n\nHP +80、MP +80');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2696, 153, 1, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2697, 153, 2, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2698, 153, 3, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2699, 153, 4, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2700, 153, 5, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2701, 153, 6, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +7');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2702, 153, 7, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +8');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2703, 153, 8, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2704, 153, 9, 1, 1000, 0, '受到巴普麦特的力量，防御力大幅增加。\n\n防御力 +10');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2705, 281, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2706, 281, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2707, 87, 1, 96, 1000, 0, '中毒。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2708, 87, 1, 96, 1000, 0, '中毒。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2709, 189, 1, 1, 0, 0, '增加振荡射击的减速持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2710, 189, 2, 1, 0, 0, '增加振荡射击的减速持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2711, 189, 3, 1, 0, 0, '增加振荡射击的减速持续时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2712, 103, 70, 1, 0, 0, '正在使用强效猛毒瓶[短剑]。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2714, 103, 71, 1, 0, 0, '正在使用强效猛毒瓶[拳剑]。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2715, 57, 1, 1, 1000, 0, '防御力下降。\n\n防御力 -1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2716, 57, 2, 1, 1000, 0, '防御力下降。\n\n防御力 -2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2717, 96, 2, 1, 15000, 0, '减轻身体，大幅提升回避率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2718, 71, 5, 1, 60000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2719, 282, 1, 1, 5000, 0, '祝福目标提升魔法回避。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2720, 283, 1, 1, 5000, 0, '增加自身魔法命中率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2721, 280, 1, 1, 300000, 0, '召唤提升攻速的图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2722, 280, 2, 1, 300000, 0, '召唤提升攻速的图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2723, 280, 3, 1, 300000, 0, '召唤提升攻速的图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2724, 280, 4, 1, 300000, 0, '召唤提升攻速的图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2725, 280, 5, 1, 300000, 0, '召唤提升攻速的图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2726, 189, 1, 1, 0, 0, '召唤强化的疾风图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2727, 189, 2, 1, 0, 0, '召唤强化的疾风图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2728, 189, 3, 1, 0, 0, '召唤强化的疾风图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2729, 189, 4, 1, 0, 0, '召唤强化的疾风图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2730, 189, 5, 1, 0, 0, '召唤强化的疾风图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2731, 207, 2, 1, 30000, 0, '大幅提升移动速度和攻击速度。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2732, 189, 1, 1, 0, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2733, 189, 2, 1, 0, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2734, 189, 3, 1, 0, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2737, 208, 11, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2738, 208, 12, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2739, 208, 13, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2740, 208, 14, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2741, 208, 15, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2742, 208, 16, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2743, 208, 17, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2744, 208, 18, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2745, 208, 19, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2746, 208, 20, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2747, 209, 11, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2748, 209, 12, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2749, 209, 13, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2750, 209, 14, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2751, 209, 15, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2752, 209, 16, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2753, 209, 17, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2754, 209, 18, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2755, 209, 19, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2756, 209, 20, 1, 60000, 0, '召唤强化的攻击图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2757, 189, 1, 1, 0, 0, '召唤强化的防御图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2758, 189, 2, 1, 0, 0, '召唤强化的防御图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2759, 189, 3, 1, 0, 0, '召唤强化的防御图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2760, 189, 1, 1, 0, 0, '召唤强化的疾风图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2761, 189, 2, 1, 0, 0, '召唤强化的疾风图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2762, 189, 3, 1, 0, 0, '召唤强化的疾风图腾。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2763, 284, 1, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2764, 284, 2, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2765, 284, 3, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2766, 284, 4, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2767, 284, 5, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2768, 284, 6, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2769, 284, 7, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2770, 284, 8, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2771, 284, 9, 1, 0, 0, '感受到雷神的庇护。\n\n命中 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2772, 285, 1, 1, 0, 0, '感受到巴普麦特的血气。\n\nHP +100');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2773, 285, 2, 1, 0, 0, '感受到巴普麦特的血气。\n\nHP +110');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2774, 285, 3, 1, 0, 0, '感受到巴普麦特的血气。\n\nHP +120');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2775, 285, 4, 1, 0, 0, '感受到巴普麦特的血气。\n\nHP +130');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2776, 285, 5, 1, 0, 0, '感受到巴普麦特的血气。\n\nHP +140');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2777, 285, 6, 1, 0, 0, '感受到巴普麦特的血气。\n\nHP +150');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2778, 6, 1, 96, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2779, 6, 2, 91, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2780, 6, 3, 86, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2781, 6, 4, 81, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2782, 6, 5, 76, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2783, 6, 1, 91, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2784, 6, 2, 86, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2785, 6, 3, 81, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2786, 6, 4, 76, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2787, 6, 5, 71, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2788, 189, 3, 1, 0, 0, '减少所有图腾的冷却时间。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2789, 38, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2794, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2795, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2796, 189, 1, 1, 0, 0, '烈焰风暴的药水破坏数增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2797, 189, 2, 1, 0, 0, '烈焰风暴的药水破坏数增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2798, 189, 3, 1, 0, 0, '烈焰风暴的药水破坏数增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2799, 87, 15, 1, 0, 0, '雷击系列特效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2800, 87, 16, 1, 0, 0, '巴普麦特系列特效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2801, 87, 17, 1, 0, 0, '伊芙利特系列特效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2802, 87, 18, 1, 0, 0, '大马士革及帕蒂邪特效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2803, 87, 19, 1, 0, 0, '盈月系列特效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2809, 38, 11, 1, 86400000, 0, '透明状态。\n透明 Lv1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2810, 38, 12, 1, 86400000, 0, '透明状态。\n透明 Lv2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2811, 38, 13, 1, 86400000, 0, '透明状态。\n透明 Lv3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2812, 38, 14, 1, 86400000, 0, '透明状态。\n透明 Lv4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2813, 38, 99, 1, 0, 0, '转换为透明状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2814, 95, 11, 1, 90000, 0, '被敌人发现。\n\n不可隐身。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2815, 95, 12, 1, 90000, 0, '被敌人发现。\n\n不可隐身。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2816, 95, 13, 1, 90000, 0, '被敌人发现。\n\n不可隐身。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2817, 170, 11, 1, 0, 0, '可看见装备透明的披风的使用者。\n\n不可攻击隐身者。\n侦查 Lv1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2818, 170, 12, 1, 0, 0, '可看见装备透明的披风的使用者。\n\n不可攻击隐身者。\n侦查 Lv2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2819, 170, 13, 1, 0, 0, '可看见装备透明的披风的使用者。\n\n不可攻击隐身者。\n侦查 Lv3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2820, 170, 14, 1, 0, 0, '可看见装备透明的披风的使用者。\n\n不可攻击隐身者。\n侦查 Lv4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2821, 193, 1, 1, 0, 0, '960');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2822, 193, 1, 1, 0, 0, '961');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2826, 287, 1, 1, 1000, 0, '受到海神之怒的影响，攻击速度下降。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2827, 287, 2, 1, 1000, 0, '受到海神之怒的影响，攻击速度下降。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2828, 287, 3, 1, 1000, 0, '受到海神之怒的影响，攻击速度下降。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2834, 40, 2, 1, 0, 0, '可变身为你想要的模样。（稀有）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2835, 40, 3, 1, 0, 0, '可变身为你想要的模样。（史诗）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2836, 40, 4, 1, 0, 0, '可变身为你想要的模样。（传说）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2837, 39, 2, 1, 0, 0, '可传送到你想去的地方。（稀有）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2838, 39, 3, 1, 0, 0, '可传送到你想去的地方。（史诗）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2839, 39, 4, 1, 0, 0, '可传送到你想去的地方。（传说）');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2844, 288, 1, 1, 0, 0, '可进行升华的变身操纵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2845, 288, 2, 1, 0, 0, '可进行升华的变身操纵（稀有）。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2846, 288, 3, 1, 0, 0, '可进行升华的变身操纵（史诗）。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2847, 288, 4, 1, 0, 0, '可进行升华的变身操纵（传说）。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2848, 291, 1, 1, 86400000, 0, '赐予一骑讨排名第1的披风');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2849, 291, 2, 1, 86400000, 0, '赐予一骑讨排名第2的披风');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2850, 291, 3, 1, 86400000, 0, '赐予一骑讨排名第3的披风');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2851, 291, 4, 1, 86400000, 0, '赐予一骑讨排名第4~7的披风');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2852, 291, 5, 1, 86400000, 0, '赐予一骑讨排名第8~11的披风');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2853, 291, 6, 1, 86400000, 0, '赐予一骑讨排名第12~15的披风');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2854, 128, 46, 1, 1000, 0, '召唤佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2856, 128, 47, 1, 1000, 0, '召唤佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2858, 128, 48, 1, 1000, 0, '召唤佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2860, 128, 49, 1, 1000, 0, '召唤佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2862, 128, 50, 1, 1000, 0, '召唤佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2864, 128, 51, 1, 1000, 0, '召唤佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2866, 128, 52, 1, 1000, 0, '召唤佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2868, 128, 53, 1, 1000, 0, '召唤佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2869, 292, 51, 1, 86000000, 0, '拥有皇冠。\n\n标记中。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2870, 292, 52, 1, 86000000, 0, '拥有皇冠。\n\n已被标记。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2871, 289, 1, 1, 1200000, 0, '负重增加 +750\nMP恢复+6\n神圣铠甲（III）\n\n对所有怪物造成的伤害\n大幅增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2872, 290, 1, 1, 1200000, 0, '负重增加 +500\nMP恢复+4\n神圣铠甲（III）\n\n对所有怪物造成的伤害增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2873, 224, 12, 1, 6000, 0, '暴击率瞬间爆发');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2874, 43, 13, 1, 6000, 0, '攻击力提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2876, 211, 16, 1, 6000, 0, '防御力增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2877, 49, 13, 1, 6000, 0, '命中率提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2878, 91, 4, 1, 23000, 0, '移动速度大幅提升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2880, 88, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2881, 293, 1, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2882, 293, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2883, 293, 3, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2884, 293, 4, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2885, 18, 105, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2886, 18, 106, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2887, 18, 107, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2888, 18, 108, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2889, 18, 109, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2891, 253, 7, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2897, 193, 1, 1, 0, 0, '962');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2898, 193, 1, 1, 0, 0, '963');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2899, 193, 1, 1, 0, 0, '964');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2900, 193, 1, 1, 0, 0, '965');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2901, 193, 1, 1, 0, 0, '966');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2902, 193, 1, 1, 0, 0, '967');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2903, 251, 3, 1, 600000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2904, 18, 110, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2905, 18, 111, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2906, 18, 112, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2907, 18, 113, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2908, 18, 114, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2909, 18, 115, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2910, 18, 116, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2911, 18, 117, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2912, 18, 118, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2913, 18, 119, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2914, 18, 120, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2915, 18, 121, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2916, 18, 122, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2917, 18, 123, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2918, 18, 124, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2919, 18, 125, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2920, 18, 126, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2921, 18, 127, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2922, 18, 128, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2923, 18, 129, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2924, 18, 130, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2925, 18, 131, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2926, 18, 132, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2927, 18, 133, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2928, 18, 134, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2929, 18, 135, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2930, 18, 136, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2931, 18, 137, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2932, 18, 138, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2933, 18, 139, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2934, 18, 140, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2935, 18, 141, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2936, 272, 61, 1, 1800000, 0, '烈焰塔封印地区\n\n可进入烈焰塔封印地区。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2937, 18, 199, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2938, 18, 199, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2939, 18, 199, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2940, 193, 1, 1, 0, 0, '968');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2941, 193, 1, 1, 0, 0, '969');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2942, 193, 1, 1, 0, 0, '970');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2943, 109, 3, 51, 80000000, 0, '状态异常');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2944, 18, 199, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2945, 18, 199, 1, 60000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2946, 248, 49, 1, 6000000, 0, '赋予女佣力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2947, 248, 49, 1, 6000000, 0, '赋予暗黑侍卫力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2948, 248, 49, 1, 6000000, 0, '赋予玲珑天使力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2949, 32, 1, 1, 80000000, 0, '朱庇特和巴贝克箱子的功能');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2950, 5, 53, 51, 10000, 0, '出血.\n\n每秒HP少量减少');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2951, 159, 1, 51, 3000, 1, '无法移动');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2952, 19, 25, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2953, 19, 26, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2954, 19, 27, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2955, 19, 28, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2956, 19, 29, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2957, 19, 30, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2958, 19, 31, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2959, 19, 32, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2960, 18, 199, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2961, 18, 199, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2962, 248, 49, 1, 6000000, 0, '朱庇特的邪念力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2963, 248, 49, 1, 6000000, 0, '巴贝克的司令官力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2964, 193, 1, 1, 0, 0, '971');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2965, 193, 1, 1, 0, 0, '972');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2967, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2968, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2969, 18, 199, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2970, 18, 199, 1, 60000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2971, 248, 49, 1, 6000000, 0, '阿努比斯力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2972, 248, 49, 1, 6000000, 0, '月光精灵魔法师力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2973, 193, 1, 1, 0, 0, '973');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2974, 193, 1, 1, 0, 0, '974');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2975, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2976, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2977, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2978, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2979, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2980, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2981, 141, 101, 1, 6000000, 0, '被赋予艾泰尔守护祝福。\n\n攻击力 +2\n防御力 +2');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2982, 141, 102, 1, 6000000, 0, '被赋予艾泰尔守护祝福。\n\n攻击力 +4\n防御力 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2983, 176, 52, 1, 6000000, 2, '\n艾泰尔的守护者 Lv 1\n\n负重 +500\n药剂恢复量增加 Lv 1');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2984, 176, 54, 1, 6000000, 3, '\n艾泰尔的守护者 Lv 2\n\n负重 +500\n药剂恢复量增加 Lv 1\n所有属性 +1, HP/MP +20');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2985, 176, 56, 1, 6000000, 4, '\n艾泰尔的守护者 Lv 3\n\n负重 +500\n药剂恢复量增加 Lv 1\n所有属性 +1, HP/MP +20\n所有攻击力/命中率小幅增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2986, 128, 54, 1, 1000, 0, '召唤亚特兰佣兵。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2987, 302, 99, 1, 13500, 0, '黑龙出场动画时的无敌状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2990, 297, 99, 1, 0, 0, '闪电链-启动(黑龙)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2993, 297, 99, 1, 0, 0, '火球-启动(黑龙)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2994, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(气息)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2995, 296, 99, 1, 3100, 0, '气息-准备(黑龙)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2996, 297, 99, 1, 0, 0, '气息-启动(黑龙)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2997, 299, 99, 1, 5000, 0, '弱化(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2998, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(地震)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (2999, 296, 99, 1, 3700, 0, '地震-准备(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3000, 297, 99, 1, 0, 0, '地震-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3001, 298, 99, 1, 0, 0, '地震 追加伤害(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3002, 294, 99, 1, 0, 0, '非目标技能(暴风雪)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3003, 298, 99, 1, 0, 0, '暴风雪 追加伤害(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3004, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(暴风雪)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3005, 296, 99, 1, 3400, 0, '暴风雪-准备(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3006, 297, 99, 1, 0, 0, '暴风雪-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3007, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(龙卷风)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3008, 296, 99, 1, 4900, 0, '龙卷风-准备(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3009, 297, 99, 1, 0, 0, '龙卷风-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3010, 294, 99, 1, 0, 0, '非目标技能(龙卷风)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3011, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(陨石)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3012, 296, 99, 1, 3400, 0, '米泰欧斯技能准备-陨石(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3013, 297, 99, 1, 0, 0, '陨石-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3014, 294, 99, 1, 0, 0, '非目标技能(陨石)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3015, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(飞行)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3016, 10, 99, 1, 0, 0, '??恢复(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3017, 300, 99, 1, 3600000, 0, '近程攻击完美闪避(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3018, 301, 99, 1, 86000000, 0, 'Fly(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3019, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(召唤-大地)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3020, 298, 99, 1, 0, 0, '召唤-大地(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3021, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(召唤-大气)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3022, 298, 99, 1, 0, 0, '召唤-大气(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3023, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(召唤-大海)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3024, 298, 99, 1, 0, 0, '召唤-大海(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3025, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(召唤-太阳)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3026, 298, 99, 1, 0, 0, '召唤-太阳(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3027, 298, 99, 1, 1400, 0, '坠落攻击-上升(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3028, 297, 99, 1, 0, 0, '坠落攻击-移动-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3029, 171, 99, 1, 10000, 0, '隐身-坠落攻击(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3030, 31, 99, 1, 0, 0, '移动-坠落攻击(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3031, 296, 99, 1, 3000, 0, '坠落攻击-大气(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3032, 297, 99, 1, 0, 0, '坠落攻击-大气-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3033, 296, 99, 1, 900, 0, '坠落攻击-下降-准备(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3034, 297, 99, 1, 0, 0, '坠落攻击-下降-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3035, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(地面气息)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3036, 296, 99, 1, 3300, 0, '地面气息-准备(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3037, 297, 99, 1, 0, 0, '地面气息-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3038, 295, 99, 1, 0, 0, '米泰欧斯使用技能提示(空中气息)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3039, 296, 99, 1, 3300, 0, '空中气息-准备(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3040, 297, 99, 1, 0, 0, '空中气息-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3041, 297, 99, 1, 0, 0, '地面火球-使用(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3043, 297, 99, 1, 0, 0, '闪电链-启动(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3044, 297, 99, 1, 0, 0, '空中火球-使用(米泰欧斯)');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3045, 302, 99, 1, 6000, 0, '米泰欧斯出场动画时的无敌状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3046, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3047, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3048, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3049, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3050, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3051, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3052, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3053, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3054, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3055, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3056, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3057, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3058, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3059, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3060, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3061, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3062, 18, 199, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3065, 70, 3, 1, 120000, 0, '力量增加.\n\n力量 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3066, 82, 10, 1, 0, 0, 'MP +100~250');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3067, 71, 6, 1, 120000, 0, '防御力增加.\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3068, 72, 6, 1, 60000, 0, '狂暴状态.\n\n防御力 -4\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3069, 117, 3, 1, 480000, 0, 'HP最大值提高 50%.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3071, 75, 4, 1, 300000, 0, '敏捷 +3\n\n受到伤害时回避率上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3072, 74, 7, 1, 30000, 0, '一定时间段内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3073, 76, 2, 1, 300000, 0, '攻击速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3074, 193, 1, 1, 0, 0, '1074');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3075, 193, 1, 1, 0, 0, '1075');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3076, 193, 1, 1, 0, 0, '1076');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3077, 193, 1, 1, 0, 0, '1077');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3078, 248, 49, 1, 6000000, 0, '赋予恶魔小丑(II)力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3079, 248, 49, 1, 6000000, 0, '赋予女神咖莉(II)力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3080, 248, 49, 1, 6000000, 0, '赋予春丽(II)力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3081, 248, 49, 1, 6000000, 0, '赋予恶魔追猎者(II)力量的状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3082, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3083, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3084, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3085, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3086, 69, 2, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3088, 69, 3, 1, 1000, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3090, 83, 2, 1, 120000, 0, '智力增加.\n\n智力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3091, 85, 2, 1, 1200000, 0, 'MP 恢复 +6\n\n无视负重 MP恢复 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3094, 120, 2, 1, 3000, 0, '储备魔力中. ');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3095, 96, 3, 1, 30000, 0, '减轻身体回避率大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3096, 94, 8, 1, 300000, 0, '使自己隐身逃出敌人的视线.\n\n近距离可被别人发现.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3097, 304, 1, 1, 1000, 0, '控制时间.\n\n无法移动');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3098, 303, 1, 1, 5000, 0, '限制隐身状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3099, 73, 2, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3100, 118, 50, 1, 20000, 0, '暂时无法带上装备.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3102, 305, 1, 1, 30000, 0, '减少魔法回避率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3104, 93, 2, 1, 240000, 0, '移动速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3105, 178, 2, 1, 30000, 0, '攻击速度提升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3106, 97, 2, 1, 300000, 0, '蕴含猛毒的气息，增加毒瓶发动几率。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3108, 121, 2, 1, 1000, 0, '运用全身力量打出一击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3109, 306, 1, 1, 5000, 0, '受到持续伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3110, 122, 2, 1, 100, 0, '用暴击攻击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3111, 124, 2, 1, 600000, 0, '魔法攻击力和命中率大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3112, 123, 2, 1, 12000, 0, '暂时无法穿戴装备.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3113, 125, 2, 1, 900000, 0, '时间扭曲.\n\n移动速度, 攻击速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3117, 70, 14, 1, 120000, 0, '力量得到提升.\n\n力量 +4');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3118, 70, 15, 1, 120000, 0, '力量得到提升.\n\n力量 +5');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3119, 70, 16, 1, 120000, 0, '力量得到提升.\n\n力量 +6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3120, 72, 7, 1, 60000, 0, '处于暴走状态.\n\n防御力 -3\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3121, 72, 8, 1, 60000, 0, '处于暴走状态.\n\n防御力 -2\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3122, 72, 9, 1, 60000, 0, '处于暴走状态.\n\n防御力 -1\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3123, 72, 10, 1, 60000, 0, '处于暴走状态.\n\n攻击速度增加, 移动速度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3124, 307, 1, 1, 20000, 0, '力量, 敏捷, 智力根据随机数值减少');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3125, 74, 8, 1, 30000, 0, '一定时间段内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3126, 74, 9, 1, 30000, 0, '一定时间段内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3127, 74, 10, 1, 30000, 0, '一定时间段内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3128, 74, 11, 1, 30000, 0, '一定时间段内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3129, 74, 12, 1, 30000, 0, '一定时间段内移动速度降低.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3130, 96, 4, 1, 30000, 0, '减轻身体回避率大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3131, 311, 1, 1, 600000, 0, '增加魔法回避率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3132, 308, 1, 1, 60000, 0, '特效 输出专用');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3133, 312, 1, 1, 600000, 0, '特效 输出专用');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3134, 128, 7, 1, 2000, 0, '正在召唤奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3135, 309, 1, 1, 30000, 0, '特效 输出专用');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3136, 310, 1, 1, 120000, 0, '身体变轻.\n\n防御力 +3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3137, 98, 2, 1, 600000, 0, '下一次攻击使用强烈一击.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3138, 155, 12, 1, 60000, 0, '部分魔法反射给释放者.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3139, 161, 2, 1, 30000, 0, '正在召唤奥利爱德图腾.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3140, 119, 2, 1, 1800000, 0, '用魔法的力量使身体变轻.\n\n移动速度, 攻击速度大幅上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3141, 167, 2, 1, 1800000, 0, '用魔法使身体变轻.\n\n移动速度,攻击速度上升.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3142, 163, 20, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3143, 163, 21, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3144, 163, 22, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3145, 163, 23, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3146, 163, 24, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3147, 163, 25, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3148, 163, 26, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3149, 163, 27, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3150, 163, 28, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3151, 163, 29, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3152, 163, 30, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3153, 163, 31, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3154, 163, 32, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3155, 163, 33, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3156, 163, 34, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3157, 163, 35, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3158, 163, 36, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3159, 163, 37, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3160, 163, 38, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3161, 163, 39, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3162, 163, 40, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3163, 163, 41, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3164, 163, 42, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3165, 163, 43, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3166, 163, 44, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3167, 163, 45, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3168, 68, 40, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3169, 68, 41, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3170, 68, 42, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3171, 68, 43, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3172, 68, 44, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3173, 68, 45, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3174, 68, 46, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3175, 68, 47, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3176, 68, 48, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3177, 68, 49, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3178, 68, 50, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3179, 77, 40, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3180, 77, 41, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3181, 77, 42, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3182, 77, 43, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3183, 77, 44, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3184, 77, 45, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3185, 77, 46, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3186, 77, 47, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3187, 77, 48, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3188, 77, 49, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3189, 77, 50, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3190, 65, 40, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3191, 65, 41, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3192, 65, 42, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3193, 65, 43, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3194, 65, 44, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3195, 65, 45, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3196, 65, 46, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3197, 65, 47, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3198, 65, 48, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3199, 65, 49, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3200, 65, 50, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3201, 191, 20, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3202, 191, 21, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3203, 191, 22, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3204, 191, 23, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3205, 191, 24, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3206, 191, 25, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3207, 191, 26, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3208, 191, 27, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3209, 191, 28, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3210, 191, 29, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3211, 191, 30, 1, 0, 0, NULL);
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3212, 314, 1, 1, 0, 0, '亲密度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3213, 313, 1, 1, 0, 0, '确认宠物装备');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3214, 315, 1, 1, 180000, 0, '暂时的增加力量');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3215, 317, 1, 1, 180000, 0, '暂时的增加敏捷');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3216, 316, 1, 1, 180000, 0, '暂时的增加智力');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3217, 318, 1, 1, 180000, 0, '暂时的增加恢复量的药水');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3218, 318, 2, 1, 180000, 0, '暂时的增加恢复量的药水');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3219, 319, 1, 1, 180000, 0, '暂时受近距离攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3220, 320, 1, 1, 180000, 0, '暂时受远距离攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3221, 321, 1, 1, 180000, 0, '暂时受魔法攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3222, 322, 1, 1, 60000, 0, '暂时增加暴击几率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3223, 323, 1, 1, 60000, 0, '暂时的暴击时增加伤害值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3224, 324, 1, 1, 60000, 0, '暂时减少伤害量');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3225, 325, 1, 1, 60000, 0, '暂时的技能使用时减少魔力消耗');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3226, 326, 1, 1, 60000, 0, '暂时的增加魔法命中率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3227, 327, 1, 1, 300000, 0, '暂时的角色周边产生特效');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3228, 328, 1, 1, 180000, 0, '暂时狩猎时增加经验值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3229, 329, 1, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3230, 329, 1, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3231, 329, 1, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3232, 46, 16, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3233, 46, 24, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3234, 46, 20, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3235, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3236, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3237, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3238, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3239, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3240, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3241, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3242, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3243, 94, 9, 1, 300000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3244, 94, 10, 1, 300000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3245, 94, 11, 1, 300000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3246, 94, 12, 1, 300000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3247, 94, 13, 1, 300000, 0, '使自己隐身逃出敌人的视线。\n\n近距离可被别人发现。\n\n获得盈月披风效果，移动速度上升。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3248, 163, 46, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3249, 163, 47, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3250, 163, 48, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3251, 163, 49, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3252, 163, 50, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3253, 163, 51, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3254, 163, 52, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3255, 163, 53, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3256, 163, 54, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3257, 163, 55, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3258, 163, 56, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3259, 163, 57, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3260, 163, 58, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3261, 77, 50, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3262, 77, 51, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3263, 77, 52, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3264, 77, 53, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3265, 77, 54, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3266, 77, 55, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3267, 77, 56, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3268, 77, 57, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3269, 77, 58, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3270, 77, 59, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3271, 77, 60, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3272, 77, 61, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3273, 77, 62, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3274, 163, 59, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3275, 163, 60, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3276, 163, 61, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3277, 163, 62, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3278, 163, 63, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3279, 163, 64, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3280, 163, 65, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3281, 163, 66, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3282, 163, 67, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3283, 163, 68, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3284, 163, 69, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3285, 163, 70, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3286, 163, 71, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3287, 65, 51, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3288, 65, 52, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3289, 65, 53, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3290, 65, 54, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3291, 65, 55, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3292, 65, 56, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3293, 65, 57, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3294, 65, 58, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3295, 65, 59, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3296, 65, 60, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3297, 65, 61, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3298, 65, 62, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3299, 65, 63, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3300, 191, 31, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3301, 191, 32, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3302, 191, 33, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3303, 191, 34, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3304, 191, 35, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3305, 191, 36, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3306, 191, 37, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3307, 191, 38, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3308, 191, 39, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3309, 191, 40, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3310, 191, 41, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3311, 191, 42, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3312, 191, 43, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3313, 68, 51, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3314, 68, 52, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3315, 68, 53, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3316, 68, 54, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3317, 68, 55, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3318, 68, 56, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3319, 68, 57, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3320, 68, 58, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3321, 68, 59, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3322, 68, 60, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3323, 68, 61, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3324, 68, 62, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3325, 68, 63, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3326, 163, 72, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3327, 163, 73, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3328, 163, 74, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3329, 163, 75, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3330, 163, 76, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3331, 163, 77, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3332, 163, 78, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3333, 163, 79, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3334, 163, 80, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3335, 163, 81, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3336, 163, 82, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3337, 163, 83, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3338, 163, 84, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3339, 65, 64, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3340, 65, 65, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3341, 77, 63, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3342, 163, 85, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3343, 68, 64, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3344, 68, 65, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3345, 191, 44, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3346, 191, 45, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3347, 163, 85, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3348, 319, 2, 1, 180000, 0, '暂时受近距离攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3349, 320, 2, 1, 180000, 0, '暂时受远距离攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3350, 321, 2, 1, 180000, 0, '暂时受魔法攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3351, 318, 3, 1, 180000, 0, '暂时的增加恢复量的药水');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3352, 322, 2, 1, 60000, 0, '暂时增加暴击几率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3353, 323, 2, 1, 60000, 0, '暂时的暴击时增加伤害值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3354, 324, 2, 1, 60000, 0, '暂时减少伤害量');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3355, 319, 3, 1, 180000, 0, '暂时受近距离攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3356, 320, 3, 1, 180000, 0, '暂时受远距离攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3357, 321, 3, 1, 180000, 0, '暂时受魔法攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3358, 318, 4, 1, 180000, 0, '暂时的增加恢复量的药水');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3359, 322, 3, 1, 60000, 0, '暂时增加暴击几率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3360, 323, 3, 1, 60000, 0, '暂时的暴击时增加伤害值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3361, 324, 3, 1, 60000, 0, '暂时减少伤害量');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3362, 326, 2, 1, 60000, 0, '暂时的增加魔法命中率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3363, 325, 2, 1, 60000, 0, '暂时的技能使用时减少魔力消耗');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3364, 326, 3, 1, 60000, 0, '暂时的增加魔法命中率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3365, 325, 3, 1, 60000, 0, '暂时的技能使用时减少魔力消耗');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3366, 329, 2, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3367, 46, 17, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3368, 329, 2, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3369, 46, 29, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3370, 329, 2, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3371, 46, 26, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3372, 329, 2, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3373, 46, 35, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3374, 329, 2, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3375, 46, 21, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3376, 329, 2, 1, 300000, 0, '暂时装备德拉克戒指时外形变更及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3377, 46, 32, 1, 1800000, 0, '提升移动速度及回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3378, 329, 3, 1, 300000, 0, '暂时使用德拉克戒指时德拉克外形变更，远距离攻击伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3379, 46, 18, 1, 1800000, 0, '提升移动速度，减少远距离攻击伤害, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3380, 329, 3, 1, 300000, 0, '暂时使用德拉克戒指时德拉克外形变更，远距离攻击伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3381, 46, 30, 1, 1800000, 0, '提升移动速度，减少远距离攻击伤害, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3382, 329, 3, 1, 300000, 0, '暂时使用德拉克戒指时德拉克外形变更，近距离攻击伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3383, 46, 27, 1, 1800000, 0, '提升移动速度，减少近距离攻击伤害, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3384, 329, 3, 1, 300000, 0, '暂时使用德拉克戒指时德拉克外形变更，近距离攻击伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3385, 46, 36, 1, 1800000, 0, '提升移动速度，减少近距离攻击伤害, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3386, 329, 3, 1, 300000, 0, '暂时使用德拉克戒指时德拉克外形变更，魔法攻击伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3387, 46, 22, 1, 1800000, 0, '提升移动速度，减少魔法攻击的伤害, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3388, 329, 3, 1, 300000, 0, '暂时使用德拉克戒指时德拉克外形变更，魔法攻击伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3389, 46, 33, 1, 1800000, 0, '提升移动速度，减少魔法攻击的伤害, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3390, 328, 2, 1, 180000, 0, '暂时狩猎时增加经验值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3391, 327, 2, 1, 300000, 0, '暂时角色周边产生特效回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3392, 333, 1, 1, 180000, 0, '宠物的庇护 (I) 近距离伤害减少');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3393, 334, 1, 1, 180000, 0, '宠物的庇护 (II) 远距离伤害减少');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3394, 335, 1, 1, 180000, 0, '宠物的庇护 (III) 魔法伤害减少');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3395, 330, 1, 1, 180000, 0, '增加药水恢复量 (III) 增加药水恢复量');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3396, 336, 1, 1, 60000, 0, '双倍暴击 (III) 暴击率增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3397, 337, 1, 1, 60000, 0, '双倍伤害 (III) 暴击时伤害增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3398, 338, 1, 1, 60000, 0, '双倍盾 (III) 伤害减少');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3399, 340, 1, 1, 60000, 0, '提升魔法命中率 (III) 提升魔法命中率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3400, 339, 1, 1, 60000, 0, '魔力的流动 (III) 魔法消耗减少');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3401, 343, 1, 1, 1800000, 0, '德拉克技能回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3402, 327, 3, 1, 300000, 0, '暂时角色周边产生特效回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3403, 341, 1, 1, 300000, 0, '双倍特效 (III) 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3404, 328, 3, 1, 180000, 0, '暂时狩猎时增加经验值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3405, 342, 1, 1, 180000, 0, '宠物的祝福 (III) 经验值增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3406, 343, 1, 1, 300000, 0, '大地之德拉克 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3407, 343, 1, 1, 300000, 0, '太阳之德拉克 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3408, 343, 1, 1, 300000, 0, '风之德拉克 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3409, 331, 99, 1, 86400000, 0, '盈月遗址突袭怪物无敌状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3410, 343, 1, 1, 300000, 0, '超强大地之德拉克 (I) 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3411, 343, 1, 1, 300000, 0, '超强大地之德拉克 (II) 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3412, 343, 1, 1, 300000, 0, '超强太阳之德拉克 (I) 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3413, 343, 1, 1, 300000, 0, '超强太阳之德拉克 (II) 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3414, 343, 1, 1, 300000, 0, '超强风之德拉克 (I) 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3415, 343, 1, 1, 300000, 0, '超强风之德拉克 (II) 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3416, 343, 1, 1, 300000, 0, '凶猛的大地之德拉克 (I) 远距离伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3417, 343, 1, 1, 300000, 0, '凶猛的大地之德拉克 (II) 远距离伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3418, 343, 1, 1, 300000, 0, '凶猛的太阳之德拉克 (I) 近距离伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3419, 343, 1, 1, 300000, 0, '凶猛的太阳之德拉克 (II) 近距离伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3420, 343, 1, 1, 300000, 0, '凶猛的风之德拉克 (I) 魔法伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3421, 343, 1, 1, 300000, 0, '凶猛的风之德拉克 (II) 魔法伤害减少, 回避增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3422, 331, 99, 1, 86400000, 0, '盈月遗址突袭怪物无敌状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3423, 331, 99, 1, 86400000, 0, '盈月遗址突袭怪物无敌状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3424, 331, 99, 1, 86400000, 0, '盈月遗址突袭怪物无敌状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3427, 190, 1, 1, 3600000, 0, '不被诅咒');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3428, 190, 2, 1, 3600000, 0, '不被诅咒和恐惧');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3429, 1, 1, 98, 2000, 0, '太阳之光命中率降弱.\n所有命中率 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3430, 1, 2, 98, 4000, 0, '太阳之光命中率降弱.\n所有命中率 -3');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3431, 1, 3, 98, 4000, 0, '太阳之光命中率降弱.\n所有命中率 -6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3432, 1, 4, 97, 4000, 0, '太阳之光命中率降弱.\n所有命中率 -6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3433, 1, 5, 97, 6000, 0, '太阳之光命中率降弱.\n所有命中率 -6');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3434, 1, 6, 97, 6000, 0, '太阳之光命中率降弱.\n所有命中率 -9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3435, 1, 7, 96, 6000, 0, '太阳之光命中率降弱.\n所有命中率 -9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3436, 1, 8, 96, 8000, 0, '太阳之光命中率降弱.\n所有命中率 -9');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3437, 1, 9, 96, 8000, 0, '太阳之光命中率降弱.\n所有命中率 -12');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3438, 252, 1, 1, 3600000, 0, '对盈月遗址怪物的攻击率和命中率上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3439, 253, 1, 1, 3600000, 0, '对盈月遗址怪物的防御率和回避率上升');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3440, 18, 1, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3441, 18, 1, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3442, 248, 49, 1, 6000000, 0, '赋予阿尔忒弥斯力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3443, 248, 49, 1, 6000000, 0, '赋予阿波罗力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3444, 193, 1, 1, 0, 0, '1230');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3445, 193, 1, 1, 0, 0, '1231');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3446, 332, 99, 1, 86400000, 0, '盈月遗址突袭怪物恢复状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3447, 18, 199, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3448, 18, 199, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3449, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3450, 18, 5, 1, 3600000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3451, 344, 1, 1, 3600000, 0, '狩猎时获取的经验值增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3452, 193, 1, 1, 0, 0, '1232');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3453, 193, 1, 1, 0, 0, '1233');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3454, 193, 1, 1, 0, 0, '1234');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3455, 193, 1, 1, 0, 0, '1235');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3456, 193, 1, 1, 0, 0, '1236');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3457, 193, 1, 1, 0, 0, '1237');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3458, 193, 1, 1, 0, 0, '1238');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3459, 193, 1, 1, 0, 0, '1239');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3460, 193, 1, 1, 0, 0, '1240');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3461, 193, 1, 1, 0, 0, '1241');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3462, 193, 1, 1, 0, 0, '1242');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3463, 193, 1, 1, 0, 0, '2143');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3464, 193, 1, 1, 0, 0, '1244');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3465, 193, 1, 1, 0, 0, '1245');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3466, 248, 49, 1, 6000000, 0, '赋予炼金术士(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3467, 248, 49, 1, 6000000, 0, '赋予忍者少女(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3468, 248, 49, 1, 6000000, 0, '赋予巴德(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3469, 248, 49, 1, 6000000, 0, '赋予女王骑士(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3470, 248, 49, 1, 6000000, 0, '赋予女海盗船长(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3471, 248, 49, 1, 6000000, 0, '赋予修道士(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3472, 248, 49, 1, 6000000, 0, '赋予阿修罗(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3473, 248, 49, 1, 6000000, 0, '赋予大力神(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3474, 248, 49, 1, 6000000, 0, '赋予狂战士(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3475, 248, 49, 1, 6000000, 0, '赋予男爵火枪手(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3476, 248, 49, 1, 6000000, 0, '赋予惊艳女狙击手(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3477, 248, 49, 1, 6000000, 0, '赋予暮色黑寡妇(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3478, 248, 49, 1, 6000000, 0, '赋予黑暗侍卫(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3479, 248, 49, 1, 6000000, 0, '赋予玲珑天使(II)力量的状态');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3480, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3481, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3482, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3483, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3484, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3485, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3486, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3487, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3488, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3489, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3490, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3491, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3492, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3493, 18, 1, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3494, 346, 1, 1, 300000, 0, '光的力量使魔法回避增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3495, 347, 1, 1, 30000, 0, '近距离攻击， 命中小幅度提升
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3496, 347, 2, 1, 30000, 0, '近距离攻击， 命中大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3497, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3498, 348, 1, 1, 30000, 0, '药剂恢复小幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3499, 348, 2, 1, 30000, 0, '药剂恢复大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3500, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3501, 349, 1, 1, 30000, 0, '远距离攻击，命中小幅度提升
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3502, 349, 2, 1, 30000, 0, '远距离攻击， 命中大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3503, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3504, 350, 1, 1, 30000, 0, '所有回避小幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3505, 350, 2, 1, 30000, 0, '所有回避大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3506, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3507, 355, 1, 1, 30000, 0, '近距离攻击， 命中小幅度提升
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3508, 355, 2, 1, 30000, 0, '近距离攻击， 命中大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3509, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3510, 356, 1, 1, 30000, 0, '魔法命中小幅度提升
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3511, 356, 2, 1, 30000, 0, '魔法命中大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3512, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3513, 351, 1, 1, 30000, 0, '近距离命中，暴击伤害小幅度提升
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3514, 351, 2, 1, 30000, 0, '近距离命中，暴击伤害大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3515, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3516, 352, 1, 1, 30000, 0, '所有回避小幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3517, 352, 2, 1, 30000, 0, '所有回避大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3518, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3519, 353, 1, 1, 30000, 0, '近距离命中， 暴击率小幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3520, 353, 2, 1, 30000, 0, '近距离命中，暴击率大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3521, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3522, 354, 1, 1, 30000, 0, '所有防御力小幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3523, 354, 2, 1, 30000, 0, '所有防御力大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3524, 345, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3525, 193, 1, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3526, 357, 51, 1, 900000, 0, '灵魂试炼场\n\n可进入灵魂试炼场。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3528, 163, 86, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3529, 163, 87, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3530, 163, 88, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3531, 163, 89, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3532, 163, 90, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3533, 163, 91, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3534, 77, 64, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3535, 77, 65, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3536, 65, 66, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3537, 65, 67, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3538, 68, 66, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3539, 68, 67, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3540, 191, 46, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3541, 191, 47, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3542, 77, 66, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3543, 65, 68, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3544, 163, 92, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3545, 163, 93, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3546, 68, 68, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3547, 68, 69, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3548, 77, 66, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3549, 65, 68, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3550, 191, 48, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3551, 191, 49, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3552, 319, 4, 1, 180000, 0, '暂时受近距离攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3553, 333, 2, 1, 180000, 0, '宠物的奇迹 (I) 近距离伤害减少
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3554, 320, 4, 1, 180000, 0, '暂时受远距离攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3555, 334, 2, 1, 180000, 0, '宠物的奇迹 (II) 远距离伤害减少
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3556, 321, 4, 1, 180000, 0, '暂时受魔法攻击减少的伤害');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3557, 335, 2, 1, 180000, 0, '宠物的奇迹 (III) 魔法伤害减少
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3558, 318, 4, 1, 180000, 0, '暂时的增加恢复量的药水
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3559, 330, 2, 1, 180000, 0, '药剂恢复增加 (IV) 药剂恢复增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3560, 322, 4, 1, 60000, 0, '暂时增加暴击几率');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3561, 336, 2, 1, 60000, 0, '双倍暴击 (IV) 暴击率增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3562, 323, 4, 1, 60000, 0, '暂时的暴击时增加伤害值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3563, 337, 2, 1, 60000, 0, '双倍伤害 (IV) 暴击时伤害增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3564, 324, 4, 1, 60000, 0, '暂时减少伤害量');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3565, 338, 2, 1, 60000, 0, '双重盾(IV) 减少伤害
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3566, 326, 4, 1, 60000, 0, '暂时的增加魔法命中率
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3567, 340, 2, 1, 60000, 0, '魔法命中率增加(IV) 魔法命中率增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3568, 325, 4, 1, 60000, 0, '暂时的技能使用时减少魔力消耗');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3569, 339, 2, 1, 60000, 0, '魔力的流动 (IV) 魔力消耗减少
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3570, 328, 4, 1, 180000, 0, '暂时狩猎时增加经验值');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3571, 342, 2, 1, 180000, 0, '宠物的祝福 (IV) 经验值增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3572, 327, 4, 1, 300000, 0, '暂时角色周边产生特效回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3573, 341, 2, 1, 300000, 0, '双倍特效 (IV) 回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3574, 329, 4, 1, 300000, 0, '暂时佩戴德拉克戒指时德拉克外形变更，远距离攻击伤害减少，振荡射击回避率，回避增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3575, 343, 2, 1, 300000, 0, '荒野德拉克远距离伤害减少，振荡射击回避率，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3576, 329, 4, 1, 300000, 0, '暂时佩戴德拉克戒指时德拉克外形变更，远距离攻击伤害减少，振荡射击回避率，回避增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3577, 343, 2, 1, 300000, 0, '平原德拉克远距离伤害减少，振荡射击回避率， 回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3578, 46, 19, 1, 1800000, 0, '移动速度提升，远距离攻击伤害减少，振荡射击回避率增加，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3579, 46, 31, 1, 1800000, 0, '移动速度提升，远距离攻击伤害减少，振荡射击回避率增加，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3580, 329, 4, 1, 300000, 0, '暂时佩戴德拉克戒指时德拉克外形变更，近距离攻击伤害减少，晕厥攻击回避概率，回避增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3581, 343, 2, 1, 300000, 0, '红焰德拉克近距离伤害减少，晕厥攻击回避概率，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3582, 329, 4, 1, 300000, 0, '暂时佩戴德拉克戒指时德拉克外形变更，近距离攻击伤害减少，晕厥攻击回避概率，回避增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3583, 343, 2, 1, 300000, 0, '白光的德拉克近距离伤害减少，晕厥攻击回避概率，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3584, 46, 28, 1, 1800000, 0, '移动速度提升，近距离攻击伤害减少，晕厥攻击回避概率增加，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3585, 46, 37, 1, 1800000, 0, '移动速度提升，近距离攻击伤害减少，晕厥攻击回避概率增加，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3586, 329, 4, 1, 300000, 0, '暂时佩戴德拉克戒指时德拉克外形变更，魔法攻击伤害减少，蛛网术回避概率，回避增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3587, 343, 2, 1, 300000, 0, '疾风德拉克魔法伤害减少，蛛网术回避概率，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3588, 329, 4, 1, 300000, 0, '暂时佩戴德拉克戒指时德拉克外形变更，魔法攻击伤害减少，蛛网术回避概率，回避增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3589, 343, 2, 1, 300000, 0, '旋风德拉克魔法伤害减少，蛛网术回避概率，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3590, 46, 23, 1, 1800000, 0, '移动速度提升，魔法攻击伤害减少，蛛网术回避率增加，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3591, 46, 34, 1, 1800000, 0, '移动速度提升，魔法攻击伤害减少，蛛网术回避率增加，回避增加
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3592, 359, 1, 1, 300000, 0, '宠物组队确认状态异常
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3593, 358, 1, 1, 300000, 0, '启动宠物力量 Lv1。\n因宠物变身参与组队角色能力小幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3594, 358, 2, 1, 300000, 0, '启动宠物力量 Lv2。\n因宠物变身参与组队角色能力小幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3595, 358, 3, 1, 300000, 0, '启动宠物力量 Lv3\n参与组队中的角色因宠物变身，能力提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3596, 358, 4, 1, 300000, 0, '启动宠物力量 Lv4。\n参与组队中的角色因宠物变身，能力提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3597, 358, 5, 1, 300000, 0, '启动宠物力量 Lv5。\n参与组队中的角色因宠物变身，能力大幅度提升。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3598, 248, 50, 1, 6000000, 0, '火焰伊格里特的力量赋予的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3599, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3600, 248, 50, 1, 6000000, 0, '??? ???? ??? ?? ??? ??.
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3601, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3602, 248, 50, 1, 6000000, 0, '赋予火魔伊格里特的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3603, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3604, 248, 50, 1, 6000000, 0, '赋予火魔伊格里特弓箭手的力量状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3605, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3606, 248, 50, 1, 6000000, 0, '赋予深海利维坦的力量状态
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3607, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3608, 248, 50, 1, 6000000, 0, '赋予深海利维坦弓箭手的力量状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3609, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3610, 248, 50, 1, 6000000, 0, '赋予天海利维坦的力量状态
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3611, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3612, 248, 50, 1, 6000000, 0, '赋予天海利维坦弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3613, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3614, 248, 50, 1, 6000000, 0, '荒野忒亚的力量赋予的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3615, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3616, 248, 50, 1, 6000000, 0, '赋予荒野忒亚弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3617, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3618, 248, 50, 1, 6000000, 0, '平原忒亚的力量赋予的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3619, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3620, 248, 50, 1, 6000000, 0, '赋予平原忒亚弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3621, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3622, 248, 50, 1, 6000000, 0, '月光卡利戈的力量赋予的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3623, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3624, 248, 50, 1, 6000000, 0, '赋予月光卡利戈弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3625, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3626, 248, 50, 1, 6000000, 0, '赋予弦月卡利戈的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3627, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3628, 248, 50, 1, 6000000, 0, '赋予弦月卡利戈的力量状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3629, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3630, 248, 50, 1, 6000000, 0, '赋予红焰鲁门的 力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3631, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3632, 248, 50, 1, 6000000, 0, '赋予红焰鲁门弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3633, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3634, 248, 50, 1, 6000000, 0, '赋予白光鲁门的力量状态
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3635, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3636, 248, 50, 1, 6000000, 0, '赋予白光鲁门弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3637, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3638, 248, 50, 1, 6000000, 0, '赋予旋风文图斯的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3639, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3640, 248, 50, 1, 6000000, 0, '赋予旋风文图斯弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3641, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3642, 248, 50, 1, 6000000, 0, '赋予疾风文图斯的力量状态
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3643, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3644, 248, 50, 1, 6000000, 0, '赋予疾风文图斯弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3645, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3646, 77, 31, 1, 3600000, 0, '负重状态 HP恢复 +5
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3647, 77, 32, 1, 3600000, 0, '无视负重的HP恢复+6
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3650, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3651, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3652, 64, 4, 1, 3600000, 0, 'HP恢复量+4
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3653, 64, 5, 1, 3600000, 0, 'HP恢复量+6
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3654, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3655, 18, 5, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3656, 18, 199, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3657, 18, 199, 1, 300000, 0, '变身中.\n\n不可乘坐德拉克.');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3658, 10, 2, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3659, 10, 3, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3660, 360, 8, 1, 0, 0, '');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3661, 361, 1, 1, 120000, 0, '2分钟无敌状态。\n\n不受敌人攻击。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3662, 361, 2, 1, 5000, 0, '5秒钟无敌状态。\n\n不受敌人攻击。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3663, 362, 1, 1, 600000, 0, '无敌状态。\n\n不受到攻击。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3664, 363, 1, 1, 600000, 0, '无敌状态。\n\n不受到攻击。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3665, 364, 50, 1, 60000, 0, '使用药剂时恢复量减少10%。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3666, 365, 50, 1, 60000, 0, '药剂使用时恢复量减少20%。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3667, 366, 50, 1, 60000, 0, '使用药剂时恢复量减少30%。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3668, 367, 50, 1, 60000, 0, '使用药剂时恢复量减少40%。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3669, 368, 50, 1, 60000, 0, '使用药剂时恢复量减少50%。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3670, 43, 50, 1, 60000, 0, '破坏敌方的守护石。\n\n攻击力大幅度增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3671, 65, 50, 1, 30000, 0, 'MP恢复量大幅度增加。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3672, 301, 99, 1, 90000, 0, '竞技场米泰欧斯飞行
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3673, 300, 99, 1, 60000, 0, '竞技场米泰欧斯飞行
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3674, 298, 99, 1, 0, 0, '召唤-大地(竞技场米泰欧斯)
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3675, 298, 99, 1, 0, 0, '召唤-大气(竞技场米泰欧斯)
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3676, 298, 99, 1, 0, 0, '召唤-大海(竞技场米泰欧斯)
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3677, 298, 99, 1, 0, 0, '召唤-太阳(竞技场米泰欧斯)
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3678, 297, 99, 1, 0, 0, '竞技场堕落攻击 - 上升(米泰欧斯)
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3679, 296, 99, 1, 60000, 0, '竞技场堕落攻击准备(米泰欧斯)
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3680, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3681, 18, 199, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3682, 193, 1, 1, 0, 0, '1323');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3683, 193, 1, 1, 0, 0, '1324
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3684, 248, 50, 1, 6000000, 0, '赋予暗黑暗杀团力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3685, 248, 50, 1, 6000000, 0, '漆黑暗杀团的力量赋予的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3686, 18, 5, 1, 7200000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3687, 18, 5, 1, 7200000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3688, 369, 1, 1, 1000, 0, '暗黑祭司暴击技能
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3689, 248, 50, 1, 6000000, 0, '赋予(真)火焰伊格里特力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3690, 248, 50, 1, 6000000, 0, '赋予(真)火焰伊格里特弓箭手的力量状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3691, 248, 50, 1, 6000000, 0, '赋予(真)火魔伊格里特的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3692, 248, 50, 1, 6000000, 0, '赋予(真)火魔伊格里特弓箭手的力量状态。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3693, 248, 50, 1, 6000000, 0, '赋予(真)深海利维坦的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3694, 248, 50, 1, 6000000, 0, '赋予(真)深海利维坦弓箭手的力量状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3695, 248, 50, 1, 6000000, 0, '赋予(真)天海利维坦的力量状态，
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3696, 248, 50, 1, 6000000, 0, '(真)天海利维坦弓箭手的力量赋予的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3697, 248, 50, 1, 6000000, 0, '赋予(真)荒野忒亚的力量状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3698, 248, 50, 1, 6000000, 0, '赋予(真)荒野忒亚弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3699, 248, 50, 1, 6000000, 0, '赋予(真)平原忒亚的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3700, 248, 50, 1, 6000000, 0, '赋予(真)平原忒亚弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3701, 248, 50, 1, 6000000, 0, '赋予(真)月光卡利戈的力量状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3702, 248, 50, 1, 6000000, 0, '(真)月光卡利戈弓箭手的力量赋予的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3703, 248, 50, 1, 6000000, 0, '赋予(真)弦月卡利戈的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3704, 248, 50, 1, 6000000, 0, '赋予(真)弦月卡利戈弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3705, 248, 50, 1, 6000000, 0, '赋予(真)红焰鲁门的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3706, 248, 50, 1, 6000000, 0, '赋予(真)红焰鲁门弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3707, 248, 50, 1, 6000000, 0, '(真)白光鲁门的力量赋予的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3708, 248, 50, 1, 6000000, 0, '赋予(真)白光鲁门弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3709, 248, 50, 1, 6000000, 0, '赋予(真)旋风文图斯的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3710, 248, 50, 1, 6000000, 0, '赋予(真)旋风文图斯弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3711, 248, 50, 1, 6000000, 0, '赋予(真)疾风文图斯的力量状态
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3712, 248, 50, 1, 6000000, 0, '赋予(真)疾风文图斯弓箭手的力量的状态。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3713, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3714, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3715, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3716, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3717, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3718, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3719, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3720, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3721, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3722, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3723, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3724, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3725, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3726, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3727, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3728, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3729, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3730, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3731, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3732, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3733, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3734, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3735, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3736, 18, 1, 1, 300000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3737, 370, 51, 1, 300000, 0, '宠物反力场 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3738, 370, 52, 1, 300000, 0, '宠物反力场 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3739, 370, 53, 1, 300000, 0, '宠物反力场 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3740, 370, 51, 1, 300000, 0, '宠物反力场 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3741, 370, 52, 1, 300000, 0, '宠物反力场 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3742, 370, 53, 1, 300000, 0, '宠物反力场 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3743, 370, 51, 1, 300000, 0, '宠物反力场 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3744, 370, 52, 1, 300000, 0, '宠物反力场 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3745, 370, 53, 1, 300000, 0, '宠物反力场 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3746, 370, 51, 1, 300000, 0, '宠物反力场 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3747, 370, 52, 1, 300000, 0, '宠物反力场 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3748, 370, 53, 1, 300000, 0, '宠物反力场 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3749, 370, 51, 1, 300000, 0, '宠物反力场 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3750, 370, 52, 1, 300000, 0, '宠物反力场 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3751, 370, 53, 1, 300000, 0, '宠物反力场 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3752, 370, 51, 1, 300000, 0, '宠物反力场 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3753, 370, 52, 1, 300000, 0, '宠物反力场 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3754, 370, 53, 1, 300000, 0, '宠物反力场 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3755, 371, 51, 1, 60000, 0, '宠物力量 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3756, 371, 52, 1, 60000, 0, '宠物力量 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3757, 371, 53, 1, 60000, 0, '宠物力量 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3758, 371, 51, 1, 60000, 0, '宠物力量 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3759, 371, 52, 1, 60000, 0, '宠物力量 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3760, 371, 53, 1, 60000, 0, '宠物力量 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3761, 371, 51, 1, 60000, 0, '宠物力量 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3762, 371, 52, 1, 60000, 0, '宠物力量 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3763, 371, 53, 1, 60000, 0, '??? ?? Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3764, 371, 51, 1, 60000, 0, '宠物力量 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3765, 371, 52, 1, 60000, 0, '宠物力量 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3766, 371, 53, 1, 60000, 0, '宠物力量 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3767, 371, 51, 1, 60000, 0, '宠物力量 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3768, 371, 52, 1, 60000, 0, '宠物力量 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3769, 371, 53, 1, 60000, 0, '宠物力量 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3770, 371, 51, 1, 60000, 0, '宠物力量 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3771, 371, 52, 1, 60000, 0, '宠物力量 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3772, 371, 53, 1, 60000, 0, '宠物力量 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3773, 372, 1, 1, 0, 0, '宠物反力场确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3774, 372, 2, 1, 0, 0, '宠物反力场确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3775, 372, 3, 1, 0, 0, '宠物反力场确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3776, 372, 1, 1, 0, 0, '宠物反力场确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3777, 372, 2, 1, 0, 0, '宠物反力场确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3778, 372, 3, 1, 0, 0, '宠物反力场确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3779, 372, 1, 1, 0, 0, '宠物反力场确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3780, 372, 2, 1, 0, 0, '宠物反力场确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3781, 372, 3, 1, 0, 0, '宠物反力场确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3782, 372, 1, 1, 0, 0, '宠物反力场确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3783, 372, 2, 1, 0, 0, '宠物反力场确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3784, 372, 3, 1, 0, 0, '宠物反力场确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3785, 372, 1, 1, 0, 0, '宠物反力场确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3786, 372, 2, 1, 0, 0, '宠物反力场确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3787, 372, 3, 1, 0, 0, '宠物反力场确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3788, 372, 1, 1, 0, 0, '宠物反力场确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3789, 372, 2, 1, 0, 0, '宠物反力场确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3790, 372, 3, 1, 0, 0, '宠物反力场确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3791, 372, 1, 1, 0, 0, '宠物力量确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3792, 372, 2, 1, 0, 0, '宠物力量确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3793, 372, 3, 1, 0, 0, '宠物力量确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3794, 372, 1, 1, 0, 0, '宠物力量确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3795, 372, 2, 1, 0, 0, '宠物力量确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3796, 372, 3, 1, 0, 0, '宠物力量确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3797, 372, 1, 1, 0, 0, '宠物力量确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3798, 372, 2, 1, 0, 0, '宠物力量确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3799, 372, 3, 1, 0, 0, '宠物力量确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3800, 372, 1, 1, 0, 0, '宠物力量确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3801, 372, 2, 1, 0, 0, '宠物力量确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3802, 372, 3, 1, 0, 0, '宠物力量确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3803, 372, 1, 1, 0, 0, '宠物力量确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3804, 372, 2, 1, 0, 0, '宠物力量确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3805, 372, 3, 1, 0, 0, '宠物力量确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3806, 372, 1, 1, 0, 0, '宠物力量确认 Lv1
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3807, 372, 2, 1, 0, 0, '宠物力量确认 Lv2
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3808, 372, 3, 1, 0, 0, '宠物力量确认 Lv3
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3809, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3810, 26, 18, 1, 3600000, 0, '使用魔法之力增加防御力。\n\n防御力 +7
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3811, 27, 17, 1, 3600000, 0, '身体像石头一样坚硬。\n\n防御力 +7
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3812, 30, 17, 1, 3600000, 0, '注意力增加.\n\n命中率小幅度增加');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3813, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3814, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3815, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (3816, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (107234, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。
');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (107235, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (107259, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

INSERT INTO [dbo].[DT_Abnormal] ([AID], [AType], [ALevel], [APercent], [ATime], [AGrade], [ADesc]) VALUES (107260, 18, 20, 1, 3600000, 0, '变身中。\n\n不可乘坐德拉克。');
GO

