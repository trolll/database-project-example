/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblQuestInfo
Date                  : 2023-10-07 09:09:29
*/


INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (2, 1, 0, 0, 0, NULL, 152);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (42, 1, 0, 0, 0, NULL, 153);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (43, 1, 0, 0, 0, NULL, 154);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (44, 1, 0, 0, 0, NULL, 155);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (45, 1, 0, 0, 0, NULL, 156);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (46, 1, 0, 0, 0, NULL, 157);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (56, 1, 0, 0, 0, NULL, 158);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (85, 1, 0, 0, 0, NULL, 55);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (90, 1, 0, 0, 0, NULL, 57);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (93, 1, 0, 0, 0, NULL, 159);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (97, 1, 0, 0, 0, NULL, 160);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (98, 1, 0, 0, 0, NULL, 161);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (99, 1, 0, 0, 0, NULL, 162);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (116, 1, 0, 0, 0, NULL, 48);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (117, 1, 0, 0, 0, NULL, 49);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (118, 1, 0, 0, 0, NULL, 50);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (119, 1, 0, 0, 0, NULL, 51);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (120, 1, 0, 0, 0, NULL, 52);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (125, 1, 0, 0, 0, NULL, 25);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (126, 1, 0, 0, 0, NULL, 26);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (127, 1, 0, 0, 0, NULL, 27);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (128, 1, 0, 0, 0, NULL, 30);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (129, 1, 0, 0, 0, NULL, 31);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (131, 1, 0, 0, 0, NULL, 28);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (132, 1, 0, 0, 0, NULL, 29);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (134, 1, 0, 0, 0, NULL, 21);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (135, 1, 0, 0, 0, NULL, 22);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (136, 1, 0, 0, 0, NULL, 23);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (157, 1, 0, 0, 0, NULL, 53);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (158, 1, 0, 0, 0, NULL, 54);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (160, 1, 0, 0, 0, NULL, 32);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (161, 1, 0, 0, 0, NULL, 33);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (162, 1, 0, 0, 0, NULL, 34);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (163, 1, 0, 0, 0, NULL, 35);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (164, 1, 0, 0, 0, NULL, 36);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (165, 1, 0, 0, 0, NULL, 37);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (166, 1, 0, 0, 0, NULL, 38);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (167, 1, 0, 0, 0, NULL, 39);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (168, 1, 0, 0, 0, NULL, 40);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (169, 1, 0, 0, 0, NULL, 43);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (170, 1, 0, 0, 0, NULL, 44);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (171, 1, 0, 0, 0, NULL, 45);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (172, 1, 0, 0, 0, NULL, 46);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (173, 1, 0, 0, 0, NULL, 47);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (188, 1, 0, 0, 0, NULL, 68);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (189, 1, 0, 0, 0, NULL, 69);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (190, 1, 0, 0, 0, NULL, 70);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (191, 1, 0, 0, 0, NULL, 71);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (192, 1, 0, 0, 0, NULL, 72);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (193, 1, 0, 0, 0, NULL, 73);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (194, 1, 0, 0, 0, NULL, 74);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (195, 1, 0, 0, 0, NULL, 75);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (196, 1, 0, 0, 0, NULL, 76);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (197, 1, 0, 0, 0, NULL, 77);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (199, 1, 0, 0, 0, NULL, 125);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (199, 2, 99, 0, 0, NULL, 79);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (201, 1, 0, 0, 0, NULL, 81);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (202, 1, 0, 0, 0, NULL, 82);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (203, 1, 0, 0, 0, NULL, 83);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (204, 1, 0, 0, 0, NULL, 84);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (205, 1, 0, 0, 0, NULL, 85);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (206, 1, 0, 0, 0, NULL, 126);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (206, 2, 99, 0, 0, NULL, 86);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (207, 1, 0, 0, 0, NULL, 87);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (208, 1, 0, 0, 0, NULL, 88);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (209, 1, 0, 0, 0, NULL, 89);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (211, 1, 0, 0, 0, NULL, 91);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (213, 1, 0, 0, 0, NULL, 127);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (213, 2, 99, 0, 0, NULL, 93);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (214, 1, 0, 0, 0, NULL, 94);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (215, 1, 0, 0, 0, NULL, 95);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (216, 1, 0, 0, 0, NULL, 96);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (217, 1, 0, 0, 0, NULL, 97);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (218, 1, 0, 0, 0, NULL, 128);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (218, 2, 99, 0, 0, NULL, 98);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (219, 1, 0, 0, 0, NULL, 99);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (220, 1, 0, 0, 0, NULL, 129);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (220, 2, 99, 0, 0, NULL, 100);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (221, 1, 0, 0, 0, NULL, 107);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (222, 1, 0, 0, 0, NULL, 108);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (224, 1, 0, 0, 0, NULL, 101);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (225, 1, 0, 0, 0, NULL, 102);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (226, 1, 0, 0, 0, NULL, 103);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (227, 1, 0, 0, 0, NULL, 104);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (229, 1, 0, 0, 0, NULL, 106);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (233, 1, 0, 0, 0, NULL, 110);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (234, 1, 0, 0, 0, NULL, 111);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (235, 1, 0, 0, 0, NULL, 112);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (236, 1, 0, 0, 0, NULL, 113);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (237, 1, 0, 0, 0, NULL, 114);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (238, 1, 0, 0, 0, NULL, 115);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (239, 1, 0, 0, 0, NULL, 130);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (239, 2, 99, 0, 0, NULL, 116);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (241, 1, 0, 0, 0, NULL, 117);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (242, 1, 0, 0, 0, NULL, 118);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (243, 1, 0, 0, 0, NULL, 119);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (244, 2, 99, 0, 0, NULL, 120);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (245, 1, 0, 0, 0, NULL, 121);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (246, 1, 0, 0, 0, NULL, 122);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (247, 1, 0, 0, 0, NULL, 123);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (249, 1, 0, 0, 0, NULL, 132);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (249, 2, 99, 0, 0, NULL, 80);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (250, 1, 0, 0, 0, NULL, 90);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (251, 1, 0, 0, 0, NULL, 92);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (301, 0, 50, 0, 0, '猎杀大地之层的怪物50只', 59);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (301, 2, 99, 0, 0, '猎杀大地之层的怪物50只', 61);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (302, 0, 50, 0, 0, '猎杀大气之层的怪物50只', 62);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (302, 2, 99, 0, 0, '猎杀大气之层的怪物50只', 63);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (303, 0, 50, 0, 0, '猎杀大海之层的怪物50只', 64);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (303, 2, 99, 0, 0, '猎杀大海之层的怪物50只', 65);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (304, 0, 50, 0, 0, '猎杀太阳之层的怪物50只', 66);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (304, 2, 99, 0, 0, '猎杀太阳之层的怪物50只', 67);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (311, 0, 0, 0, 0, NULL, 133);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (311, 2, 99, 0, 0, NULL, 181);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (312, 0, 0, 0, 0, NULL, 134);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (312, 2, 99, 0, 0, NULL, 182);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (314, 0, 0, 0, 0, NULL, 136);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (314, 2, 99, 0, 0, NULL, 183);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (315, 0, 0, 0, 0, NULL, 137);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (315, 2, 99, 0, 0, NULL, 184);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (316, 0, 0, 0, 0, NULL, 138);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (316, 2, 99, 0, 0, NULL, 185);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (318, 0, 0, 0, 0, NULL, 140);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (318, 2, 99, 0, 0, NULL, 186);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (319, 0, 0, 0, 0, NULL, 141);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (319, 2, 99, 0, 0, NULL, 187);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (320, 0, 0, 0, 0, NULL, 142);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (320, 2, 99, 0, 0, NULL, 188);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (321, 0, 0, 0, 0, NULL, 143);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (321, 2, 99, 0, 0, NULL, 189);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (322, 0, 0, 0, 0, NULL, 144);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (322, 2, 99, 0, 0, NULL, 190);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (323, 0, 0, 0, 0, NULL, 145);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (323, 2, 99, 0, 0, NULL, 191);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (324, 0, 0, 0, 0, NULL, 146);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (324, 2, 99, 0, 0, NULL, 192);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (325, 0, 0, 0, 0, NULL, 147);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (325, 2, 99, 0, 0, NULL, 193);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (331, 0, 30, 0, 0, '击杀豺狼人', 163);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (331, 2, 99, 0, 0, NULL, 171);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (332, 0, 30, 0, 0, '击杀小矮人', 164);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (332, 2, 99, 0, 0, NULL, 172);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (333, 0, 0, 0, 0, NULL, 165);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (333, 2, 99, 0, 0, NULL, 173);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (334, 0, 0, 0, 0, NULL, 166);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (334, 2, 99, 0, 0, NULL, 174);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (335, 0, 0, 0, 0, NULL, 167);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (335, 2, 99, 0, 0, '', 175);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (336, 0, 0, 0, 0, NULL, 168);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (336, 2, 99, 0, 0, NULL, 176);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (337, 0, 0, 0, 0, NULL, 169);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (337, 2, 99, 0, 0, NULL, 177);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (338, 1, 0, 0, 0, NULL, 170);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (338, 2, 99, 0, 0, NULL, 178);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (382, 2, 99, 0, 0, NULL, 351);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (386, 1, 0, 0, 0, NULL, 263);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (387, 0, 0, 0, 0, NULL, 264);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (388, 0, 0, 0, 0, NULL, 265);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (388, 2, 99, 0, 0, NULL, 309);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (389, 0, 0, 0, 0, NULL, 266);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (390, 0, 0, 0, 0, NULL, 267);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (391, 0, 0, 0, 0, NULL, 268);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (391, 2, 99, 0, 0, NULL, 310);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (392, 0, 0, 0, 0, NULL, 269);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (393, 0, 0, 0, 0, NULL, 270);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (393, 2, 99, 0, 0, NULL, 311);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (394, 0, 0, 0, 0, NULL, 271);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (395, 0, 0, 0, 0, NULL, 272);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (395, 2, 99, 0, 0, NULL, 312);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (396, 0, 0, 0, 0, NULL, 273);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (397, 0, 0, 0, 0, NULL, 274);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (397, 2, 99, 0, 0, NULL, 313);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (398, 0, 0, 0, 0, NULL, 275);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (399, 0, 0, 0, 0, NULL, 276);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (399, 2, 99, 0, 0, NULL, 314);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (400, 0, 0, 0, 0, NULL, 277);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (400, 2, 99, 0, 0, NULL, 315);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (401, 0, 0, 0, 0, NULL, 278);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (401, 2, 99, 0, 0, NULL, 316);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (402, 0, 0, 0, 0, NULL, 279);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (402, 2, 99, 0, 0, NULL, 317);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (403, 1, 0, 0, 0, NULL, 280);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (404, 0, 0, 0, 0, NULL, 281);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (405, 0, 0, 0, 0, NULL, 282);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (405, 2, 99, 0, 0, NULL, 318);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (406, 0, 0, 0, 0, NULL, 283);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (407, 0, 0, 0, 0, NULL, 284);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (408, 0, 0, 0, 0, NULL, 285);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (409, 0, 0, 0, 0, NULL, 286);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (409, 2, 99, 0, 0, NULL, 319);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (410, 0, 0, 0, 0, NULL, 287);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (411, 0, 0, 0, 0, NULL, 288);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (412, 0, 0, 0, 0, NULL, 289);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (412, 2, 99, 0, 0, NULL, 320);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (413, 0, 0, 0, 0, NULL, 290);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (414, 0, 0, 0, 0, NULL, 291);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (415, 0, 0, 0, 0, NULL, 292);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (416, 0, 0, 0, 0, NULL, 293);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (417, 0, 0, 0, 0, NULL, 294);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (417, 2, 99, 0, 0, NULL, 321);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (418, 0, 0, 0, 0, NULL, 295);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (419, 0, 0, 0, 0, NULL, 296);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (420, 0, 0, 0, 0, NULL, 297);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (420, 2, 99, 0, 0, NULL, 322);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (421, 0, 0, 0, 0, NULL, 323);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (422, 0, 0, 0, 0, NULL, 324);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (423, 0, 0, 0, 0, NULL, 325);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (424, 0, 0, 0, 0, NULL, 326);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (424, 2, 99, 0, 0, NULL, 327);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (425, 0, 0, 0, 0, NULL, 328);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (426, 0, 0, 0, 0, NULL, 329);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (427, 0, 0, 0, 0, NULL, 330);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (428, 0, 0, 0, 0, NULL, 331);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (428, 2, 99, 0, 0, NULL, 332);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (430, 0, 0, 0, 0, NULL, 343);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (431, 0, 0, 0, 0, NULL, 344);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (431, 2, 99, 0, 0, NULL, 346);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (432, 0, 0, 0, 0, NULL, 347);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (433, 0, 0, 0, 0, NULL, 348);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (433, 2, 99, 0, 0, NULL, 349);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (436, 1, 0, 0, 0, NULL, 236);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (437, 1, 0, 0, 0, NULL, 237);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (438, 1, 0, 0, 0, NULL, 238);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (439, 1, 0, 0, 0, NULL, 239);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (440, 1, 0, 0, 0, NULL, 240);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (441, 1, 0, 0, 0, NULL, 241);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (442, 1, 0, 0, 0, NULL, 242);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (443, 1, 0, 0, 0, NULL, 243);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (444, 1, 0, 0, 0, NULL, 244);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (445, 1, 0, 0, 0, NULL, 245);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (446, 1, 0, 0, 0, NULL, 246);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (447, 1, 0, 0, 0, NULL, 247);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (448, 1, 0, 0, 0, NULL, 248);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (449, 1, 0, 0, 0, NULL, 249);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (450, 1, 0, 0, 0, NULL, 250);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (451, 1, 0, 0, 0, NULL, 251);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (452, 2, 99, 0, 0, '', 972);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (453, 2, 99, 0, 0, '', 973);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (467, 0, 20, 0, 0, '狩猎变质的精灵30只', 333);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (467, 2, 99, 0, 0, '', 342);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (468, 0, 20, 0, 0, '狩猎德拉古兰 军团兵/火焰术士20只', 335);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (468, 2, 99, 0, 0, '', 336);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (469, 0, 20, 0, 0, '狩猎双足翼龙/格雷芬特种类20只', 337);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (469, 2, 99, 0, 0, '', 338);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (470, 0, 20, 0, 0, '狩猎黄泉骑士/弓箭手/咒术师20只', 339);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (470, 2, 99, 0, 0, '', 340);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (471, 1, 0, 0, 0, '', 253);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (472, 1, 0, 0, 0, '', 254);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (473, 1, 0, 0, 0, NULL, 255);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (474, 1, 0, 0, 0, NULL, 256);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (475, 1, 0, 0, 0, NULL, 257);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (476, 1, 0, 0, 0, NULL, 258);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (477, 1, 0, 0, 0, NULL, 259);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (478, 1, 0, 0, 0, NULL, 260);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (479, 1, 0, 0, 0, NULL, 261);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (480, 1, 0, 0, 0, NULL, 262);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (481, 0, 500, 0, 0, '狩猎豺狼人', 368);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (482, 1, 0, 0, 0, NULL, 369);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (484, 1, 0, 0, 0, NULL, 370);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (485, 0, 0, 0, 0, '', 371);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (486, 0, 0, 0, 0, '', 373);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (487, 0, 0, 0, 0, '', 375);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (488, 1, 0, 0, 0, NULL, 376);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (490, 1, 0, 0, 0, NULL, 377);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (491, 1, 0, 0, 0, NULL, 378);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (493, 0, 0, 0, 0, '', 398);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (494, 0, 1000, 0, 0, '消灭小仙子', 399);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (495, 0, 1000, 0, 0, '消灭帕伦佣兵队兽人', 400);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (497, 0, 0, 0, 0, '', 401);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (498, 0, 1000, 0, 0, '消灭混血豺狼', 402);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (499, 0, 1000, 0, 0, '消灭莱芙利肯', 403);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (500, 0, 0, 0, 0, '', 404);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (500, 1, 0, 0, 0, '', 405);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (501, 0, 600, 0, 0, '消灭狼人', 355);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (502, 0, 0, 0, 0, NULL, 356);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (503, 1, 0, 0, 0, NULL, 357);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (505, 1, 0, 0, 0, NULL, 359);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (506, 1, 0, 0, 0, NULL, 360);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (507, 1, 0, 0, 0, NULL, 361);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (508, 1, 0, 0, 0, NULL, 362);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (509, 1, 0, 0, 0, NULL, 379);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (510, 0, 1000, 0, 0, '消灭黑暗洞窟4层怪物', 383);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (510, 1, 0, 0, 0, NULL, 384);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (512, 0, 500, 0, 0, '消灭黑龙沼泽幻觉怪兽', 385);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (512, 1, 0, 0, 0, NULL, 386);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (513, 0, 1000, 0, 0, '消灭黑龙沼泽4层怪物', 387);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (513, 1, 0, 0, 0, NULL, 388);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (514, 0, 1000, 0, 0, '消灭不死地牢2层怪物', 389);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (515, 0, 1000, 0, 0, '消灭不死地牢3层怪物', 390);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (515, 1, 0, 0, 0, NULL, 391);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (517, 0, 1000, 0, 0, '消灭烈焰塔2层怪物', 392);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (518, 0, 1000, 0, 0, '消灭烈焰塔3层怪物', 393);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (518, 1, 0, 0, 0, NULL, 394);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (520, 0, 1000, 0, 0, '消灭帝王墓穴2层怪物', 395);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (521, 0, 500, 0, 0, '消灭帝王墓穴3层怪物', 396);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (521, 1, 0, 0, 0, NULL, 397);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (522, 0, 0, 0, 0, '', 406);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (523, 0, 0, 0, 0, '', 407);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (525, 0, 500, 0, 0, '消灭圣甲虫谷怪物', 408);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (525, 1, 0, 0, 0, NULL, 409);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (526, 0, 2000, 0, 0, '消灭苍蝇人', 410);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (527, 0, 2000, 0, 0, '教训无名洞窟的家伙们', 363);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (528, 1, 0, 0, 0, NULL, 364);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (530, 0, 2500, 0, 0, '古代祭坛1层怪物', 365);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (531, 0, 2500, 0, 0, '古代祭坛2层怪物', 366);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (532, 0, 3000, 0, 0, '古代祭坛3层怪物', 367);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (533, 1, 0, 0, 0, NULL, 412);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (533, 2, 99, 0, 0, NULL, 415);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (534, 1, 0, 0, 0, NULL, 413);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (534, 2, 99, 0, 0, NULL, 416);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (535, 1, 0, 0, 0, NULL, 414);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (535, 2, 99, 0, 0, NULL, 417);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (536, 2, 99, 0, 0, NULL, 418);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (540, 0, 1000, 0, 0, '狩猎卢比伽/卢比伽幼虫/奎特普特', 456);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (541, 0, 700, 0, 0, '狩猎小翼龙/石化蜥蜴', 457);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (542, 0, 1000, 0, 0, '狩猎屠杀者/暗影、侦察兵', 458);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (543, 0, 700, 0, 0, '狩猎喝格奴役', 459);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (544, 0, 500, 0, 0, '狩猎米泰欧斯弓箭手/破坏者/狂战士', 460);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (545, 0, 200, 0, 0, '狩猎米泰欧斯的诅咒师', 461);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (546, 1, 0, 0, 0, '', 462);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (547, 0, 1000, 0, 0, '狩猎大地之层的怪物', 463);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (548, 0, 1000, 0, 0, '狩猎大气之层的怪物', 464);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (549, 0, 1000, 0, 0, '狩猎大海之层的怪物', 465);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (550, 0, 1000, 0, 0, '狩猎太阳之层的怪物', 466);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (551, 1, 0, 0, 0, '', 467);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (552, 0, 1000, 0, 0, '狩猎半人鱼提鲁斯/卢萨勒', 468);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (553, 1, 0, 0, 0, NULL, 478);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (554, 0, 1000, 0, 0, '狩猎潘瑟/天蝎/剑龙', 479);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (555, 1, 0, 0, 0, NULL, 480);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (556, 1, 0, 0, 0, NULL, 481);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (557, 0, 1000, 0, 0, '狩猎斯尼米尔/蝲蛄/深海鱼', 482);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (558, 1, 0, 0, 0, NULL, 483);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (560, 0, 1000, 0, 0, '狩猎海盗兵/巨魔奴隶', 484);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (561, 1, 0, 0, 0, NULL, 496);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (562, 0, 1000, 0, 0, '狩猎大地双足翼龙/暴走的双头巨人', 497);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (563, 1, 0, 0, 0, NULL, 498);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (564, 0, 1, 0, 0, '狩猎塔纳托斯', 499);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (565, 0, 1000, 0, 0, '狩猎堕落的精灵/食人魔', 500);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (566, 1, 0, 0, 0, NULL, 501);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (567, 0, 1, 0, 0, '狩猎骑士团长拉尔卡', 504);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (568, 0, 30, 0, 0, '消灭双足翼龙/双头巨人/城堡魔像', 506);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (568, 2, 99, 0, 0, NULL, 518);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (569, 0, 20, 0, 0, '消灭塔纳托斯侦察兵/弓箭手', 507);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (569, 2, 99, 0, 0, NULL, 519);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (570, 0, 20, 0, 0, '消灭塔纳托斯暗杀者/长枪兵', 508);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (570, 2, 99, 0, 0, NULL, 520);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (571, 0, 30, 0, 0, '消灭塔纳托斯狂战士', 509);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (571, 2, 99, 0, 0, NULL, 521);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (572, 0, 30, 0, 0, '消灭暗黑精灵/食人魔/夜之梦魔', 510);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (572, 2, 99, 0, 0, NULL, 514);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (573, 0, 20, 0, 0, '消灭拉尔卡高级军官/钢铁弓兵', 511);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (573, 2, 99, 0, 0, NULL, 515);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (574, 0, 20, 0, 0, '消灭拉尔卡大祭司/钢铁步兵', 512);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (574, 2, 99, 0, 0, NULL, 516);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (575, 0, 30, 0, 0, '消灭拉尔卡工兵战士', 513);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (575, 2, 99, 0, 0, NULL, 517);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (576, 2, 99, 0, 0, NULL, 522);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (578, 1, 0, 0, 0, '收集1个兔子肉', 419);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (579, 1, 0, 0, 0, '收集1个果狸的毛', 420);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (580, 1, 0, 0, 0, '收集3个野猪肉', 421);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (581, 0, 10, 0, 0, '狩猎10只小哥布林', 422);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (582, 0, 15, 0, 0, '狩猎15只蝙蝠', 423);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (583, 0, 12, 0, 0, '狩猎12只吉内亚双足翼龙', 424);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (584, 1, 0, 0, 0, '收集3个小兽人的饭盒', 428);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (585, 1, 0, 0, 0, '收集3个龟角蛤蟆的毒', 426);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (586, 1, 0, 0, 0, '收集4个吉内亚双足翼龙的指甲', 427);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (587, 1, 0, 0, 0, '收集3个古代监视者的眼睛', 429);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (588, 0, 10, 0, 0, '狩猎10只小哥布林', 430);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (589, 0, 15, 0, 0, '狩猎15只蝙蝠', 431);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (590, 0, 12, 0, 0, '狩猎12只吉内亚双足翼龙', 432);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (591, 1, 0, 0, 0, '收集3个小兽人的饭盒', 433);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (592, 1, 0, 0, 0, '收集3个龟角蛤蟆的毒', 434);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (593, 1, 0, 0, 0, '收集4个吉内亚双足翼龙的指甲', 435);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (594, 1, 0, 0, 0, '收集3个古代监视者的眼睛', 436);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (595, 0, 10, 0, 0, '狩猎10只小哥布林', 437);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (596, 0, 15, 0, 0, '狩猎15只蝙蝠', 438);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (597, 0, 12, 0, 0, '狩猎12只吉内亚双足翼龙', 439);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (598, 1, 0, 0, 0, '收集3个小兽人的饭盒', 440);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (599, 1, 0, 0, 0, '收集3个龟角蛤蟆的毒', 441);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (600, 1, 0, 0, 0, '收集4个吉内亚双足翼龙的指甲', 442);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (601, 1, 0, 0, 0, '收集3个古代监视者的眼睛', 443);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (603, 1, 0, 0, 0, '收集1个兔子肉', 445);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (604, 1, 0, 0, 0, '收集1个带有气味的花', 446);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (605, 1, 0, 0, 0, '收集3个火烈鸟的喙', 447);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (606, 1, 0, 0, 0, '收集3个千年参根', 448);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (607, 1, 0, 0, 0, '收集5个魂块', 449);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (608, 1, 0, 0, 0, '收集3个水晶球', 450);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (609, 0, 6, 0, 0, '狩猎6只火狐狸', 451);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (610, 0, 8, 0, 0, '狩猎8只鱼头蟹', 452);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (611, 0, 0, 0, 0, '狩猎5只卡尼巴尔战士/5只卡尼巴尔勇士', 453);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (612, 0, 10, 0, 0, '狩猎10只硬刺龟', 454);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (614, 1, 0, 0, 0, '收集10个兔子肉', 455);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (615, 1, 0, 0, 0, '转达介绍信', 527);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (616, 1, 0, 0, 0, '转达介绍信', 528);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (617, 1, 0, 0, 0, '转达介绍信', 529);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (618, 1, 0, 0, 0, '转达介绍信', 530);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (619, 1, 0, 0, 0, '转达介绍信', 531);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (620, 1, 0, 0, 0, '转达介绍信', 532);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (621, 1, 0, 0, 0, '转达介绍信', 533);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (622, 1, 0, 0, 0, '转达介绍信', 534);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (623, 1, 0, 0, 0, '收集1个兔子肉', 561);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (624, 1, 0, 0, 0, '收集1个果狸的毛', 562);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (625, 1, 0, 0, 0, '收集3个野猪肉', 563);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (626, 1, 0, 0, 0, '转达介绍信', 535);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (627, 1, 0, 0, 0, '转达介绍信', 536);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (628, 1, 0, 0, 0, '转达介绍信', 537);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (629, 1, 0, 0, 0, '转达介绍信', 538);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (630, 1, 0, 0, 0, '转达介绍信', 539);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (631, 1, 0, 0, 0, '转达介绍信', 540);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (632, 1, 0, 0, 0, '转达介绍信', 541);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (633, 1, 0, 0, 0, '转达介绍信', 542);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (634, 1, 0, 0, 0, '收集1个兔子肉', 564);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (635, 1, 0, 0, 0, '收集1个果狸的毛', 565);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (636, 1, 0, 0, 0, '收集3个野猪肉', 566);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (637, 1, 0, 0, 0, '转达介绍信', 543);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (638, 1, 0, 0, 0, '转达介绍信', 544);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (639, 1, 0, 0, 0, '转达介绍信', 545);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (640, 1, 0, 0, 0, '转达介绍信', 546);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (641, 1, 0, 0, 0, '转达介绍信', 547);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (642, 1, 0, 0, 0, '转达介绍信', 548);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (643, 1, 0, 0, 0, '转达介绍信', 549);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (644, 1, 0, 0, 0, '转达介绍信', 550);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (645, 1, 0, 0, 0, '转达介绍信', 551);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (646, 1, 0, 0, 0, '转达介绍信', 552);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (647, 1, 0, 0, 0, '转达介绍信', 553);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (648, 1, 0, 0, 0, '转达介绍信', 554);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (649, 1, 0, 0, 0, '转达介绍信', 555);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (650, 1, 0, 0, 0, '转达介绍信', 556);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (651, 1, 0, 0, 0, '转达介绍信', 557);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (652, 1, 0, 0, 0, '转达介绍信', 558);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (653, 1, 0, 0, 0, '转达介绍信', 559);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (654, 1, 0, 0, 0, '转达介绍信', 560);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (655, 0, 100, 0, 0, '狩猎钢铁审判者', 524);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (655, 2, 99, 0, 0, NULL, 567);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (656, 0, 100, 0, 0, '狩猎混沌破坏者', 525);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (656, 2, 99, 0, 0, NULL, 568);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (657, 0, 100, 0, 0, '狩猎堕落的霸主', 526);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (657, 2, 99, 0, 0, NULL, 569);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (658, 1, 0, 0, 0, '收集50个赛诺的魔力碎片', 616);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (659, 1, 0, 0, 0, '收集100个魔力粘合剂', 620);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (660, 0, 300, 0, 0, '狩猎150只钢铁审判者', 621);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (660, 2, 99, 0, 0, NULL, 630);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (661, 1, 0, 0, 0, '收集50个狱特斯的魔力碎片', 622);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (662, 1, 0, 0, 0, '转达冒险家的印章', 576);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (663, 1, 0, 0, 0, '转达冒险家的印章', 579);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (664, 0, 2, 0, 0, '狩猎2只监视者和法米利亚', 580);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (665, 1, 0, 0, 0, '转达冒险家的印章', 581);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (666, 1, 0, 0, 0, '收集5个矮人的毛', 589);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (667, 1, 0, 0, 0, '转达拜德.莱特的介绍信', 583);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (668, 1, 0, 0, 0, '转达10个支援粮食', 584);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (669, 0, 15, 0, 0, '狩猎15只哥布林', 585);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (670, 0, 5, 0, 0, '狩猎10只骷髅', 588);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (671, 1, 0, 0, 0, '收集5个死者心脏', 590);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (672, 1, 0, 0, 0, '转达乌里安的信', 591);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (673, 0, 0, 0, 0, '狩猎小蜘蛛/蜘蛛', 592);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (674, 0, 15, 0, 0, '狩猎15只灰色小矮人', 594);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (675, 1, 0, 0, 0, '收集5个变异能量', 595);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (676, 1, 0, 0, 0, '转达巴蒙的介绍信', 596);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (677, 0, 0, 0, 0, '狩猎20只半人鱼', 597);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (677, 1, 0, 0, 0, '收集1个密函', 615);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (678, 1, 0, 0, 0, '转达1个密函', 599);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (679, 1, 0, 0, 0, '转达安内德的介绍信', 600);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (680, 0, 150, 0, 0, '狩猎150只恐怖的夜之梦魔', 607);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (681, 1, 0, 0, 0, '收集150个女巫的魔法球', 608);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (682, 0, 0, 0, 0, '狩猎150只嗜血追踪者/守护猎犬', 609);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (683, 1, 0, 0, 0, '收集250个磨损的碎石', 610);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (684, 0, 150, 0, 0, '狩猎150只绝望的亡灵巫师', 611);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (685, 1, 0, 0, 0, '收集150个漆黑的法杖', 612);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (686, 0, 0, 0, 0, '狩猎150只格鲁姆勇士/巫妖王的手下', 613);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (687, 1, 0, 0, 0, '收集250个水晶碎片', 614);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (688, 1, 0, 0, 0, '收集100个魔力组合剂', 623);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (689, 0, 300, 0, 0, '狩猎150混沌破坏者', 624);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (689, 2, 99, 0, 0, NULL, 629);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (690, 1, 0, 0, 0, '收集赛诺的不详魔力之石', 625);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (690, 2, 99, 0, 0, NULL, 631);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (691, 1, 0, 0, 0, '收集狱特斯的不详魔力之石', 626);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (691, 2, 99, 0, 0, NULL, 632);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (692, 0, 2, 0, 0, '狩猎2只监视者', 642);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (693, 1, 0, 0, 0, '转达介绍信', 645);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (694, 1, 0, 0, 0, '收集5个守护石碎片', 646);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (695, 0, 10, 0, 0, '狩猎10只半蟹人', 647);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (696, 0, 20, 0, 0, '狩猎20只兽人/兽人弓箭手/兽人战士', 648);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (697, 1, 0, 0, 0, '转达特雷诺斯的信', 649);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (698, 0, 10, 0, 0, '狩猎亡者骷髅/骸骨弓箭手', 650);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (699, 0, 15, 0, 0, '狩猎15只嗜血骷髅', 651);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (699, 1, 0, 0, 0, '收集1个研究日志', 654);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (700, 1, 0, 0, 0, '转达毁损的研究日志', 655);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (701, 1, 0, 0, 0, '收集5个记忆碎片', 656);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (702, 0, 15, 0, 0, '狩猎15只小恶魔', 657);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (703, 0, 20, 0, 0, '狩猎20只巨鹰', 658);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (704, 1, 0, 0, 0, '转达神圣的净化剂', 659);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (705, 0, 0, 0, 0, '狩猎蜥蜴人/沼泽小矮人', 660);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (706, 1, 0, 0, 0, '转达介绍信', 661);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (707, 1, 0, 0, 0, '收集1个古代文献', 663);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (708, 0, 15, 0, 0, '狩猎15只半人鱼', 664);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (709, 0, 10, 0, 0, '狩猎10只鳞刺鱼人', 665);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (710, 1, 0, 0, 0, '转达港口地图', 666);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (711, 1, 0, 0, 0, '转达兵力支援申请书', 667);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (713, 0, 10, 0, 0, '食人族哥布林/哥布林族长', 668);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (714, 1, 0, 0, 0, '转达乌拉克兰的信', 669);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (715, 1, 0, 0, 0, '收集10个甲虫的皮', 670);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (716, 1, 0, 0, 0, '转达乌拉克兰的信', 671);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (717, 1, 0, 0, 0, '收集5个妖精翼的粉末', 672);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (718, 0, 10, 0, 0, '狩猎10只帕伦佣兵队兽人士兵', 673);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (719, 0, 0, 0, 0, '狩猎帕伦佣兵队兽人战士/帕伦佣兵队兽人队长', 677);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (720, 1, 0, 0, 0, '收集1个石化蜥蜴的心脏', 676);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (721, 0, 0, 0, 0, '消灭女妖拉米娅/嗜血曼陀罗', 678);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (722, 0, 10, 0, 0, '狩猎10只混血豺狼', 679);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (723, 1, 0, 0, 0, '转达警告', 680);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (724, 1, 0, 0, 0, '收集3个翼龙之尾', 681);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (725, 1, 0, 0, 0, '转达承诺证明', 682);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (726, 1, 0, 0, 0, '转达承诺证明', 683);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (727, 0, 0, 0, 0, '狩猎石之兽人勇士/石之兽人弓箭手', 684);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (728, 0, 30, 0, 0, '讨伐莱普力卡族', 685);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (729, 0, 0, 0, 0, '狩猎卡蓬克/土狼', 686);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (730, 1, 0, 0, 0, '转达出入证', 687);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (732, 0, 5, 0, 0, '狩猎钢铁兽人族', 689);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (733, 1, 0, 0, 0, '转达书籍碎片', 690);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (734, 0, 0, 0, 0, '狩猎巨大的天蝎/狼人', 691);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (735, 1, 0, 0, 0, '转达解析完成的书籍碎片', 692);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (736, 0, 0, 0, 0, '狩猎15只婴儿树人', 693);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (737, 1, 0, 0, 0, '收集3个挪动的碎木', 694);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (738, 1, 0, 0, 0, '转达英雄探测器', 695);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (739, 0, 0, 0, 0, '狩猎15只怪物蟹', 696);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (740, 1, 0, 0, 0, '转达石碑的记忆碎片', 697);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (741, 1, 0, 0, 0, '收集10个克拉肯的蟹钳', 698);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (742, 0, 0, 0, 0, '狩猎地狱的看门狗/牛头人', 699);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (743, 0, 0, 0, 0, '狩猎巨人族', 700);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (744, 0, 0, 0, 0, '狩猎3只巨大的火焰恶魔', 701);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (745, 1, 0, 0, 0, '转达书信', 702);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (746, 0, 15, 0, 0, '狩猎15只骷髅骑士', 703);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (746, 2, 99, 0, 0, NULL, 773);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (747, 0, 150, 0, 0, '消灭150只骑士帕朱兹', 705);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (748, 0, 200, 0, 0, '消灭200只米泰欧斯侍女长', 706);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (749, 0, 0, 0, 0, '消灭米泰欧斯贵妇人/米泰欧斯的魔剑', 707);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (750, 1, 0, 0, 0, '收集250个石板碎片', 708);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (751, 0, 150, 0, 0, '消灭150只吸血鬼公主', 709);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (752, 0, 200, 0, 0, '消灭200只颂唱者', 710);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (753, 0, 0, 0, 0, '消灭王的魔法师/王的军团长', 711);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (754, 1, 0, 0, 0, '收集250个诅咒的水晶球', 712);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (755, 1, 0, 0, 0, '收集50个赫尔墨斯的魔力碎片', 713);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (756, 1, 0, 0, 0, '收集100个魔力粘合剂', 714);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (757, 0, 300, 0, 0, '消灭150只堕落的霸主', 715);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (757, 2, 99, 0, 0, NULL, 776);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (758, 1, 0, 0, 0, '收集50个阿特努斯的魔力碎片', 716);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (759, 1, 0, 0, 0, '收集100个魔力组合剂', 717);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (760, 0, 300, 0, 0, '消灭150只钢铁审判者', 718);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (760, 2, 99, 0, 0, NULL, 779);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (761, 1, 0, 0, 0, '收集赫尔墨斯的不详魔力之石', 719);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (761, 2, 99, 0, 0, NULL, 780);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (762, 1, 0, 0, 0, '收集阿特努斯的不详魔力之石', 720);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (762, 2, 99, 0, 0, NULL, 781);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (763, 0, 600, 0, 0, '狩猎600只兽人', 721);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (764, 0, 700, 0, 0, '狩猎700只亡者骷髅', 722);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (765, 1, 0, 0, 0, '转达阿德的信', 723);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (766, 0, 800, 0, 0, '狩猎800只极乐鸟', 724);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (767, 1, 0, 0, 0, '转达奥博拉斯的信', 725);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (768, 0, 1000, 0, 0, '狩猎1000只蜥蜴人', 726);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (769, 0, 0, 0, 0, '狩猎鹰爪人女王/大块头巨魔战士/卡帕罗/森林巨鹰', 727);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (770, 0, 600, 0, 0, '狩猎600只钢铁兽人', 728);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (771, 0, 0, 0, 0, '狩猎树人/巨型龟', 729);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (772, 1, 0, 0, 0, '转达德科的指示', 730);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (773, 1, 0, 0, 0, '收集50个迷宫的秘籍', 731);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (774, 0, 0, 0, 0, '狩猎巨人战士/巨人巫师', 732);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (775, 1, 0, 0, 0, '收集60个火焰恶魔的角', 733);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (776, 0, 700, 0, 0, '狩猎1000只巨大的火焰恶魔', 786);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (777, 0, 1000, 0, 0, '狩猎1000只不死地牢2层怪物', 749);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (778, 0, 500, 0, 0, '狩猎500只不死地牢3层怪物', 736);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (778, 1, 0, 0, 0, '收集1个巫妖王的魔法书', 743);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (779, 1, 0, 0, 0, '转达哈比亚的信', 737);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (780, 0, 1000, 0, 0, '狩猎1000只烈焰塔2层怪物', 738);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (781, 0, 500, 0, 0, '狩猎500只烈焰塔3层怪物', 739);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (781, 1, 0, 0, 0, '收集1个烈火匹斯的心脏', 744);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (782, 1, 0, 0, 0, '转达努比亚的信', 740);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (783, 0, 1000, 0, 0, '狩猎1000只帝王墓穴2层怪物', 741);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (784, 0, 500, 0, 0, '狩猎500只帝王墓穴3层怪物', 742);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (784, 1, 0, 0, 0, '收集1个巴力毗珥的皇冠', 745);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (785, 0, 30, 0, 0, '狩猎30只博加特', 752);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (786, 1, 0, 0, 0, '收集10个博加特的翅膀', 753);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (787, 1, 0, 0, 0, '转达博加特的翅膀', 755);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (788, 0, 50, 0, 0, '狩猎50只骷髅骑士弓箭手', 757);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (789, 1, 0, 0, 0, '收集15个翼龙之尾', 758);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (790, 1, 0, 0, 0, '收集50个阿斯特拉鲁顿的蟹钳', 759);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (790, 2, 99, 0, 0, NULL, 782);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (791, 0, 40, 0, 0, '狩猎40只雪地兽人', 761);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (792, 1, 0, 0, 0, '收集20个兽人的雪地帽', 762);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (793, 1, 0, 0, 0, '收集20个帝企鹅的长喙', 763);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (794, 1, 0, 0, 0, '转达帝企鹅的长喙', 764);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (795, 0, 0, 0, 0, '狩猎恶魔史雷得/大脚怪', 765);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (796, 1, 0, 0, 0, '收集50个兽人队长的勋章', 766);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (796, 2, 99, 0, 0, NULL, 783);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (797, 0, 20, 0, 0, '狩猎虚空兽人族', 787);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (798, 1, 0, 0, 0, '向呃齐温转达介绍信', 788);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (799, 0, 0, 0, 0, '狩猎帕萨德/托乌德', 789);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (800, 0, 40, 0, 0, '狩猎40只艾米克', 790);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (801, 0, 0, 0, 0, '狩猎卡克隆/斯兰德', 791);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (802, 0, 30, 0, 0, '狩猎30只佛莱科', 792);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (803, 0, 0, 0, 0, '狩猎艾德博德/洛亚格雷芬特', 793);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (804, 1, 0, 0, 0, '向哈尔曼转达信', 794);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (805, 0, 80, 0, 0, '狩猎荒野峡谷的怪物', 795);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (806, 1, 0, 0, 0, '转达贝利恩的勋章', 796);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (807, 1, 0, 0, 0, '转达巴贝克阵营支援书', 797);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (808, 0, 0, 0, 0, '狩猎巢穴守护者/巢穴狩猎者', 798);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (809, 0, 50, 0, 0, '狩猎50只疾病之源安德斯', 799);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (810, 1, 0, 0, 0, '收集20个疾病的气息', 800);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (811, 0, 0, 0, 0, '狩猎悲惨的怨灵/愤怒的怨灵', 801);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (812, 1, 0, 0, 0, '收集20个怨恨的气息', 802);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (813, 1, 0, 0, 0, '转达疾病与怨恨的珠子', 803);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (814, 0, 100, 0, 0, '狩猎100只巴贝克祭坛怪物', 804);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (815, 1, 0, 0, 0, '转达朱庇特阵营支援书', 805);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (816, 0, 0, 0, 0, '狩猎纳托勒/海克坦', 806);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (817, 0, 50, 0, 0, '狩猎50只魔力的上古岩石', 807);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (818, 1, 0, 0, 0, '收集20个魔力之光', 808);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (819, 0, 0, 0, 0, '狩猎光之艾罗拉/光之艾罗德', 809);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (820, 1, 0, 0, 0, '收集20个耀眼之光', 810);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (821, 1, 0, 0, 0, '转达凝聚光芒的魔力', 811);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (822, 0, 100, 0, 0, '狩猎100只朱庇特圣所怪物', 812);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (823, 0, 30, 0, 0, '狩猎巢穴守护者/巢穴狩猎者', 813);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (823, 2, 99, 0, 0, NULL, 830);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (824, 0, 30, 0, 0, '狩猎疾病怪群', 814);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (824, 2, 99, 0, 0, NULL, 829);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (825, 0, 30, 0, 0, '狩猎怨恨怪群', 815);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (825, 2, 99, 0, 0, NULL, 828);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (826, 0, 40, 0, 0, '狩猎祭坛周围的怪物', 816);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (826, 2, 99, 0, 0, NULL, 827);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (827, 0, 30, 0, 0, '狩猎天使的天籁草丛怪物', 817);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (827, 2, 99, 0, 0, NULL, 826);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (828, 0, 30, 0, 0, '狩猎魔力怪群', 818);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (828, 2, 99, 0, 0, NULL, 825);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (829, 0, 30, 0, 0, '狩猎光之怪群', 819);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (829, 2, 99, 0, 0, NULL, 824);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (830, 0, 40, 0, 0, '狩猎圣所周围的怪物', 820);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (830, 2, 99, 0, 0, NULL, 823);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (831, 0, 30, 0, 0, '狩猎30只豺狼人', 821);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (831, 2, 99, 0, 0, NULL, 822);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (832, 1, 0, 0, 0, '收集50个赛欧托的魔力碎片', 870);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (833, 1, 0, 0, 0, '收集100个魔力净化水', 872);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (834, 0, 300, 0, 0, '狩猎300只混沌破坏者', 873);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (834, 2, 99, 0, 0, NULL, 874);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (835, 1, 0, 0, 0, '获得赛欧托的魔力核', 898);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (835, 2, 99, 0, 0, NULL, 876);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (836, 0, 150, 0, 0, '狩猎150只光辉骷髅骑士', 877);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (837, 1, 0, 0, 0, '收集100个雷电石英', 878);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (838, 0, 200, 0, 0, '狩猎200只光辉信徒', 879);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (839, 1, 0, 0, 0, '收集250个劫难之火', 880);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (840, 0, 700, 0, 0, '狩猎700只守护者艾丽业', 881);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (841, 0, 800, 0, 0, '狩猎800只狂魔枪护卫', 882);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (842, 0, 800, 0, 0, '狩猎800只古代芭姬丽', 883);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (843, 1, 0, 0, 0, '收集200个朱庇特的痕迹', 884);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (844, 0, 700, 0, 0, '狩猎700只血腥的邪恶女皇', 885);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (845, 0, 800, 0, 0, '狩猎800只堕落的邪恶女皇', 887);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (846, 0, 800, 0, 0, '狩猎800只觉醒的邪恶魔王', 888);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (847, 1, 0, 0, 0, '收集200个巴贝克的残骸', 889);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (848, 1, 0, 0, 0, '收集10个巴贝克的气息', 890);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (849, 0, 0, 0, 0, '狩猎30只堕落的邪恶女皇，30只觉醒的邪恶魔王', 891);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (850, 1, 0, 0, 0, '收集20个恶魔触角', 892);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (851, 0, 100, 0, 0, '狩猎100只巴贝克副本怪物', 893);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (852, 1, 0, 0, 0, '收集10个朱庇特的气息', 894);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (853, 0, 0, 0, 0, '狩猎30只狂魔枪护卫，30只古代芭姬丽', 895);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (854, 1, 0, 0, 0, '收集20个天使羽翼', 896);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (855, 0, 100, 0, 0, '狩猎100只朱庇特副本怪物', 897);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (856, 0, 20, 0, 0, '狩猎哥布林 20只', 917);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (857, 0, 40, 0, 0, '狩猎豺狼人40只', 918);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (857, 2, 99, 0, 0, NULL, 920);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (858, 0, 200, 0, 0, '变质的沼泽怪物 200只', 921);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (859, 1, 0, 0, 0, '德拉古兰碎石 20个', 922);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (860, 1, 0, 0, 0, '变种动物血液的标本 20个', 923);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (861, 1, 0, 0, 0, '传达变种动物血液的标本块', 924);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (862, 0, 0, 0, 0, '黄泉骑士, 游侠, 咒术师各 100只', 925);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (863, 1, 0, 0, 0, '转达巴林的备忘录', 926);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (864, 1, 0, 0, 0, '变质的珍珠 20个', 927);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (864, 2, 99, 0, 0, '', 928);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (865, 0, 0, 0, 0, '狩猎蜘蛛/蜘蛛仔 50只', 929);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (866, 0, 800, 0, 0, '狩猎塔纳托斯盾牌兵800只', 930);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (867, 1, 0, 0, 0, '塔纳托斯的痕迹 100个', 931);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (868, 0, 1000, 0, 0, '狩猎塔纳托斯锤子兵 1000只', 932);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (869, 1, 0, 0, 0, '塔纳托斯的证据 200个', 933);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (870, 1, 0, 0, 0, '塔纳托斯的灵魂 1个', 941);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (871, 0, 800, 0, 0, '狩猎拉尔卡近卫骑士 800只 ', 935);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (872, 1, 0, 0, 0, '拉尔卡的痕迹 100个', 936);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (873, 0, 1000, 0, 0, '狩猎拉尔卡高级骑士 1000只', 937);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (874, 1, 0, 0, 0, '拉尔卡的证据 200个', 938);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (875, 1, 0, 0, 0, '拉尔卡的灵魂 1个', 939);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (876, 1, 0, 0, 0, '勇士的证明 10个', 942);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (877, 1, 0, 0, 0, '勇士的证明 15个', 943);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (878, 1, 0, 0, 0, '勇士的证明 20个', 944);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (879, 1, 0, 0, 0, '勇士的证明 30个', 945);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (880, 1, 0, 0, 0, '勇士的证明 50个', 946);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (881, 0, 30, 0, 0, '狩猎天空之守护者 30只', 947);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (882, 0, 50, 0, 0, '狩猎天空之剑 50只 ', 948);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (883, 0, 30, 0, 0, '狩猎守护石碑 30只', 949);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (884, 0, 50, 0, 0, '狩猎天空之鬼 50只 ', 950);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (885, 0, 100, 0, 0, '狩猎岩石巨人 100只', 951);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (886, 0, 500, 0, 0, '狩猎空中地下城怪物 500只 ', 952);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (887, 1, 0, 0, 0, '次元的勋章 10个', 953);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (888, 1, 0, 0, 0, '次元的勋章 15个', 954);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (889, 1, 0, 0, 0, '次元的勋章 20个', 955);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (890, 1, 0, 0, 0, '次元的勋章 30个', 956);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (891, 1, 0, 0, 0, '次元的勋章 50个', 957);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (892, 0, 30, 0, 0, '狩猎大地之守护者 30只', 958);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (893, 0, 50, 0, 0, '狩猎大地之剑 50只 ', 959);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (894, 0, 30, 0, 0, '狩猎章鱼巫师 30只', 960);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (895, 0, 50, 0, 0, '狩猎黑暗的追踪者 50只', 961);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (896, 0, 100, 0, 0, '狩猎眼镜蛇战士 100只', 962);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (897, 0, 500, 0, 0, '地下城怪物 500只', 963);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (900, 0, 50, 0, 0, '狩猎食尸鬼/僵尸/僵尸狗', 298);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (901, 0, 50, 0, 0, '消灭死亡克鲁', 299);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (902, 0, 75, 0, 0, '狩猎兽人', 300);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (903, 0, 75, 0, 0, '击杀嗜血骷髅', 301);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (904, 0, 75, 0, 0, '清扫帝王墓穴1层', 302);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (905, 0, 100, 0, 0, '狩猎100只巨大的黄蜂', 303);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (906, 0, 100, 0, 0, '狩猎巨大的火焰恶魔/巨大的天蝎', 304);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (907, 0, 100, 0, 0, '消灭水晶傀儡/牵线木偶/莱本', 305);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (908, 0, 150, 0, 0, '消灭卢比伽和卢比伽幼虫', 306);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (909, 0, 200, 0, 0, '狩猎狂战士/破坏者', 307);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (910, 0, 200, 0, 0, '狩猎米泰欧斯的侦察兵', 637);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (911, 0, 200, 0, 0, '狩猎米泰欧斯的暗影', 638);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (912, 0, 200, 0, 0, '狩猎米泰欧斯的弓箭手', 639);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (913, 0, 200, 0, 0, '狩猎米泰欧斯的诅咒师', 640);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (914, 0, 0, 0, 0, '狩猎米泰欧斯的诅咒师/米泰欧斯的弓箭手/米泰欧斯的破坏者', 785);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1000, 0, 0, 0, 0, '狩猎方舟天使/恶魔800只', 964);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1001, 0, 0, 0, 0, '狩猎食人魔/食人魔黑法师/石像魔。磊/黑暗独角兽/双足翼龙 
', 965);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1002, 1, 0, 0, 0, '收集任务道具
', 966);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1003, 0, 4000, 0, 0, '狩猎异界的怪物
', 967);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1004, 1, 0, 0, 0, '收集原石海贼的洞窟2层海贼的气息
', 968);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1005, 0, 0, 0, 0, '狩猎小仙子/拉米亚100只
', 969);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1005, 2, 99, 0, 0, '', 970);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1006, 1, 0, 0, 0, '收集被偷走的粮食20个
', 974);
GO

INSERT INTO [dbo].[TblQuestInfo] ([mQuestNo], [mType], [mParmA], [mParmB], [mParmC], [mDesc], [mSeqNo]) VALUES (1006, 2, 99, 0, 0, '', 975);
GO

