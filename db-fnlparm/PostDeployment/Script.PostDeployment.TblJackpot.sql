/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblJackpot
Date                  : 2023-10-07 09:08:51
*/


INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1147, 0, 4827);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1147, 1, 5022);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1148, 0, 76151);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1148, 1, 113473);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1149, 0, 7791);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1149, 1, 5775);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1150, 0, 61229);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1150, 1, 94752);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1151, 0, 366953);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1151, 1, 381358);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1152, 0, 53106);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1152, 1, 91930);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1154, 0, 5664);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1154, 1, 19702);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1155, 0, 173827);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1155, 1, 907517);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1156, 0, 116854);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1156, 1, 99389);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1157, 0, 329132);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1157, 1, 341856);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1158, 0, 69696);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1158, 1, 218815);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1159, 0, 25753);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1159, 1, 691322);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1160, 0, 178553);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1160, 1, 385135);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1161, 0, 250634);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1161, 1, 503828);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1162, 0, 127894);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1162, 1, 206681);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1163, 0, 9301656);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1163, 1, 7233910);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1164, 0, 75108);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1164, 1, 53257);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1165, 0, 108254);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1165, 1, 16137);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1166, 0, 158139);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1166, 1, 88751);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1167, 0, 79447);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1167, 1, 294212);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1168, 0, 187546);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1168, 1, 540821);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1169, 0, 878);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1169, 1, 2526);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1170, 0, 60732);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1170, 1, 178347);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1171, 0, 73582);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1171, 1, 71901);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1172, 0, 22068);
GO

INSERT INTO [dbo].[TblJackpot] ([mSvrNo], [mCamp], [mStackPoint]) VALUES (1172, 1, 6577);
GO

