/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AttributeAdd
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (6, 1, 1, '1D3', 3);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (7, 1, 2, '1D6', 6);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (8, 1, 3, '1D9', 9);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (9, 2, 1, '1D3', 3);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (10, 2, 2, '1D6', 6);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (11, 2, 3, '1D9', 9);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (13, 3, 1, '1D3', 3);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (14, 3, 2, '1D6', 6);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (15, 3, 3, '1D9', 9);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (16, 4, 1, '1D6', 3);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (17, 4, 2, '1D6+6', 6);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (18, 4, 3, '1D6+12', 9);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (20, 5, 1, '1D6', 3);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (21, 5, 2, '1D6+6', 6);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (22, 5, 3, '1D6+12', 9);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (23, 6, 1, '1D6', 3);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (24, 6, 2, '1D6+6', 6);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (25, 6, 3, '1D6+12', 9);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (36, 3, 20, '1D14', 14);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (37, 3, 21, '1D14+1', 15);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (38, 3, 22, '1D14+2', 16);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (39, 3, 23, '1D14+4', 18);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (40, 3, 24, '1D14+6', 20);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (41, 3, 25, '2D8+8', 24);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (42, 3, 26, '2D8+10', 26);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (43, 3, 27, '2D8+12', 28);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (44, 3, 28, '2D9+13', 31);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (45, 3, 29, '2D10+14', 34);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (46, 1, 4, '2D6', 12);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (47, 1, 5, '2D8', 16);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (48, 2, 4, '2D6', 12);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (49, 2, 5, '2D8', 16);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (50, 3, 4, '2D6', 12);
GO

INSERT INTO [dbo].[DT_AttributeAdd] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (51, 3, 5, '2D8', 16);
GO

