/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_SkillTreeNodeItemCondition
Date                  : 2023-10-07 09:08:51
*/


INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (3, 5, 5000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (4, 1, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (4, 5, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (5, 1, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (5, 5, 20000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (6, 1, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (6, 5, 40000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (7, 1, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (8, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (8, 4, 137, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (9, 1, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (10, 1, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (11, 5, 5000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (12, 1, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (12, 5, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (13, 1, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (13, 5, 20000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (14, 1, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (14, 5, 40000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (15, 1, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (16, 5, 5000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (17, 1, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (17, 5, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (18, 1, 17, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (18, 5, 20000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (19, 1, 18, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (19, 5, 40000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (20, 1, 19, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (21, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (24, 1, 21, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (25, 1, 24, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (26, 1, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (27, 1, 26, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (28, 2, 1, 1, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (28, 5, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (29, 1, 28, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (30, 2, 1, 1, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (31, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (31, 2, 5, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (32, 2, 1, 5, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (32, 5, 20000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (34, 1, 32, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (34, 5, 40000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (35, 1, 31, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (36, 1, 34, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (36, 5, 80000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (37, 1, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (38, 1, 37, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (39, 1, 38, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (40, 2, 5, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (40, 4, 138, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (41, 1, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (42, 1, 41, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (43, 1, 27, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (43, 2, 5, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (44, 1, 43, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (45, 1, 44, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (46, 1, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (47, 1, 46, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (48, 1, 42, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (48, 2, 5, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (49, 1, 36, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (49, 5, 160000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (50, 1, 48, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (51, 1, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (52, 1, 49, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (53, 2, 5, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (53, 4, 140, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (54, 1, 53, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (55, 1, 54, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (56, 1, 52, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (56, 2, 1, 10, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (57, 1, 47, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (57, 2, 5, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (58, 1, 57, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (59, 1, 58, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (60, 1, 59, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (61, 1, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (62, 1, 39, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (62, 2, 5, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (63, 1, 62, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (64, 1, 63, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (65, 1, 64, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (66, 1, 65, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (67, 1, 55, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (67, 2, 5, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (68, 1, 67, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (69, 1, 68, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (70, 1, 69, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (71, 2, 1, 10, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (72, 1, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (72, 2, 1, 15, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (72, 5, 80000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (73, 1, 72, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (73, 5, 160000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (74, 1, 73, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (74, 5, 320000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (75, 1, 74, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (75, 5, 640000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (76, 1, 75, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (77, 1, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (77, 2, 1, 15, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (77, 5, 80000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (78, 1, 77, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (78, 5, 160000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (79, 1, 78, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (79, 5, 320000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (80, 1, 79, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (80, 5, 640000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (81, 1, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (82, 1, 76, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (82, 2, 1, 20, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (82, 5, 160000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (83, 1, 82, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (83, 5, 320000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (84, 1, 83, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (85, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (85, 4, 139, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (86, 1, 85, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (87, 1, 86, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (88, 1, 87, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (89, 1, 88, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (90, 1, 81, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (90, 2, 1, 20, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (90, 5, 160000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (91, 1, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (91, 5, 320000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (92, 1, 91, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (93, 2, 1, 25, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (93, 5, 320000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (94, 1, 93, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (94, 5, 640000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (95, 1, 94, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (96, 2, 1, 25, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (96, 5, 320000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (97, 1, 96, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (97, 5, 640000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (98, 1, 97, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (99, 2, 1, 25, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (99, 5, 320000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (100, 1, 99, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (100, 5, 640000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (101, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (102, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (103, 1, 102, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (104, 1, 103, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (105, 1, 104, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (106, 1, 105, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (107, 1, 89, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (107, 2, 6, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (108, 1, 107, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (109, 1, 108, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (110, 2, 6, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (111, 1, 110, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (112, 1, 111, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (113, 1, 106, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (113, 2, 6, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (114, 1, 113, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (115, 1, 114, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (116, 1, 115, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (117, 1, 116, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (118, 2, 6, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (118, 4, 157, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (119, 1, 118, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (120, 1, 119, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (121, 1, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (122, 1, 121, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (123, 2, 1, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (123, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (123, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (124, 1, 123, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (124, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (124, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (125, 1, 124, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (125, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (126, 2, 6, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (127, 1, 126, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (128, 1, 127, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (129, 1, 128, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (130, 1, 129, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (131, 1, 117, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (131, 2, 6, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (132, 1, 131, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (133, 1, 132, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (134, 1, 133, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (135, 1, 134, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (136, 2, 1, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (136, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (136, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (137, 1, 136, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (137, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (137, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (138, 1, 122, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (138, 2, 6, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (139, 1, 138, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (140, 1, 137, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (140, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (141, 1, 139, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (142, 1, 141, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (143, 1, 142, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (144, 2, 1, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (144, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (144, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (145, 1, 130, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (145, 2, 6, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (146, 1, 144, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (146, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (146, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (147, 1, 146, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (147, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (148, 2, 2, 50, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (148, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (148, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (149, 1, 148, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (149, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (149, 5, 20480000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (150, 1, 149, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (150, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (150, 5, 40960000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (151, 1, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (151, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (151, 5, 81920000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (152, 1, 151, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (152, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (153, 2, 2, 50, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (153, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (153, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (154, 1, 153, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (154, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (154, 5, 20480000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (155, 1, 154, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (155, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (155, 5, 40960000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (156, 1, 155, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (156, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (156, 5, 81920000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (157, 1, 156, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (157, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (158, 2, 2, 50, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (158, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (158, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (159, 1, 158, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (159, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (159, 5, 20480000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (160, 1, 159, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (160, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (160, 5, 40960000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (161, 1, 160, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (161, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (161, 5, 81920000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (163, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (163, 4, 141, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (164, 1, 163, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (165, 1, 164, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (166, 1, 161, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (166, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (167, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (168, 1, 167, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (169, 1, 168, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (170, 1, 169, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (171, 1, 170, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (172, 1, 165, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (172, 2, 7, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (174, 1, 172, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (175, 1, 174, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (176, 1, 175, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (177, 1, 176, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (178, 2, 7, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (178, 4, 143, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (179, 1, 178, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (180, 1, 179, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (181, 2, 7, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (182, 1, 181, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (183, 1, 182, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (184, 1, 183, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (185, 1, 184, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (186, 1, 180, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (186, 2, 7, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (187, 1, 186, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (188, 1, 187, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (189, 1, 188, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (190, 1, 189, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (194, 1, 171, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (194, 2, 7, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (195, 1, 194, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (196, 1, 195, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (197, 1, 185, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (197, 2, 7, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (198, 1, 197, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (199, 1, 198, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (200, 1, 199, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (201, 1, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (207, 1, 152, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (207, 2, 2, 55, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (207, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (207, 5, 20480000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (208, 1, 207, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (208, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (208, 5, 40960000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (209, 1, 208, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (209, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (210, 1, 157, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (210, 2, 2, 55, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (210, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (210, 5, 20480000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (211, 1, 210, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (211, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (211, 5, 40960000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (212, 1, 211, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (212, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (213, 1, 166, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (213, 2, 2, 55, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (213, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (213, 5, 2048000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (214, 1, 213, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (214, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (214, 5, 40960000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (215, 1, 214, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (215, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (217, 1, 216, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (218, 1, 217, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (219, 1, 218, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (220, 1, 219, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (221, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (222, 1, 221, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (223, 1, 222, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (224, 1, 223, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (225, 1, 224, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (226, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (227, 1, 226, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (228, 1, 227, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (229, 1, 228, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (230, 1, 229, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (231, 2, 8, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (231, 4, 142, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (232, 1, 231, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (233, 1, 232, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (235, 2, 8, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (236, 1, 235, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (237, 1, 234, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (238, 1, 236, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (239, 1, 230, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (239, 2, 8, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (240, 1, 237, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (241, 1, 239, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (242, 1, 241, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (243, 1, 240, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (244, 1, 242, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (245, 1, 244, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (246, 1, 243, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (247, 2, 4, 1, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (248, 1, 247, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (249, 1, 233, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (249, 2, 8, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (250, 1, 248, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (251, 1, 249, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (252, 1, 250, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (253, 1, 252, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (254, 1, 251, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (255, 1, 254, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (256, 1, 255, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (257, 1, 233, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (257, 2, 8, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (258, 1, 257, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (259, 1, 258, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (260, 1, 259, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (261, 1, 260, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (262, 2, 8, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (263, 1, 262, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (264, 1, 263, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (265, 1, 264, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (266, 1, 265, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (267, 1, 266, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (267, 2, 8, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (268, 1, 267, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (269, 1, 268, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (270, 1, 269, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (271, 1, 270, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (273, 1, 220, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (273, 2, 4, 5, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (274, 1, 273, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (275, 1, 274, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (276, 1, 275, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (277, 1, 276, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (278, 1, 246, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (278, 2, 4, 5, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (279, 1, 278, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (280, 1, 279, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (281, 1, 280, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (282, 1, 281, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (283, 2, 4, 10, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (284, 1, 283, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (285, 1, 284, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (286, 1, 285, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (287, 1, 286, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (288, 1, 277, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (288, 2, 4, 15, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (289, 1, 288, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (290, 1, 289, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (291, 1, 282, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (291, 2, 4, 15, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (292, 1, 291, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (293, 1, 292, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (294, 2, 4, 20, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (295, 1, 294, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (296, 1, 295, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (297, 1, 296, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (298, 1, 297, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (299, 2, 4, 25, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (300, 1, 299, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (301, 1, 300, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (302, 2, 4, 25, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (303, 1, 302, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (304, 1, 303, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (305, 2, 4, 25, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (306, 1, 305, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (307, 1, 306, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (308, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (308, 4, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (309, 1, 308, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (311, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (312, 1, 311, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (313, 1, 312, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (314, 1, 313, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (315, 1, 314, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (316, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (316, 4, 24, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (317, 1, 316, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (318, 1, 317, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (319, 1, 318, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (320, 1, 319, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (321, 1, 309, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (321, 2, 9, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (321, 4, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (322, 1, 321, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (323, 1, 315, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (323, 2, 9, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (324, 1, 323, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (325, 1, 324, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (326, 1, 325, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (327, 1, 326, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (328, 1, 322, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (328, 2, 9, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (328, 4, 19, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (329, 1, 328, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (330, 1, 329, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (331, 1, 330, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (332, 1, 331, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (333, 1, 327, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (333, 2, 9, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (334, 1, 333, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (335, 1, 334, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (336, 2, 9, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (336, 4, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (337, 1, 336, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (338, 1, 337, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (339, 2, 9, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (340, 1, 339, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (341, 1, 340, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (342, 2, 9, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (342, 4, 144, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (343, 1, 342, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (344, 1, 343, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (345, 1, 344, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (346, 1, 345, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (347, 1, 341, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (347, 2, 9, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (348, 1, 347, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (349, 1, 348, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (350, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (351, 1, 350, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (352, 1, 351, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (353, 1, 352, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (354, 1, 353, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (355, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (355, 4, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (356, 1, 355, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (357, 1, 356, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (358, 2, 10, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (358, 4, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (359, 1, 358, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (360, 1, 359, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (361, 1, 354, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (361, 2, 10, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (362, 1, 361, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (363, 1, 362, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (364, 1, 363, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (366, 1, 364, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (367, 2, 10, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (367, 4, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (368, 1, 367, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (369, 1, 368, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (370, 2, 10, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (370, 4, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (371, 1, 370, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (372, 1, 371, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (373, 2, 10, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (374, 1, 373, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (375, 1, 374, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (376, 1, 375, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (377, 1, 376, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (378, 2, 10, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (378, 4, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (379, 1, 378, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (380, 1, 379, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (381, 1, 380, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (382, 1, 381, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (383, 1, 377, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (383, 2, 10, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (384, 1, 383, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (385, 1, 384, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (386, 1, 382, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (386, 2, 10, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (387, 1, 386, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (388, 1, 387, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (389, 1, 388, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (390, 1, 389, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (391, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (391, 4, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (392, 1, 391, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (393, 1, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (394, 1, 393, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (395, 1, 394, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (396, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (397, 1, 396, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (398, 1, 397, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (399, 1, 398, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (400, 1, 399, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (401, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (402, 1, 401, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (403, 1, 402, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (404, 1, 403, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (405, 1, 404, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (406, 1, 405, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (406, 2, 11, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (407, 1, 406, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (408, 1, 407, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (409, 1, 408, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (410, 1, 409, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (411, 1, 410, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (411, 2, 11, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (412, 1, 411, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (413, 1, 412, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (414, 1, 413, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (415, 1, 414, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (416, 1, 395, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (416, 2, 11, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (417, 1, 416, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (418, 1, 417, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (419, 2, 11, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (419, 4, 151, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (420, 1, 419, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (421, 1, 420, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (422, 1, 421, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (423, 1, 422, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (424, 1, 400, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (424, 2, 11, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (425, 1, 424, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (426, 1, 425, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (427, 1, 426, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (428, 1, 427, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (429, 2, 11, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (429, 4, 159, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (430, 1, 429, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (431, 1, 430, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (432, 1, 431, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (434, 1, 432, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (435, 2, 12, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (435, 4, 154, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (436, 1, 435, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (437, 1, 436, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (438, 1, 437, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (439, 1, 438, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (440, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (440, 4, 148, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (441, 1, 440, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (442, 1, 441, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (443, 1, 442, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (444, 1, 443, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (455, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (456, 1, 455, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (457, 1, 456, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (458, 1, 457, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (459, 2, 12, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (459, 4, 152, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (460, 1, 459, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (461, 1, 460, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (462, 1, 461, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (463, 1, 462, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (464, 1, 439, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (464, 2, 12, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (465, 1, 464, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (466, 1, 465, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (467, 1, 463, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (467, 2, 12, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (468, 1, 467, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (469, 1, 468, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (470, 1, 469, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (471, 1, 470, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (472, 2, 12, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (472, 4, 149, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (473, 1, 472, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (474, 1, 473, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (475, 1, 474, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (475, 2, 12, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (476, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (476, 4, 86, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (477, 1, 476, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (478, 1, 477, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (479, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (480, 1, 479, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (481, 1, 480, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (482, 1, 481, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (483, 1, 482, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (484, 1, 483, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (484, 2, 13, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (485, 1, 484, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (486, 1, 485, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (487, 1, 486, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (488, 1, 487, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (489, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (489, 4, 163, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (490, 1, 489, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (491, 1, 490, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (492, 1, 491, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (493, 1, 492, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (494, 2, 13, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (494, 4, 87, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (495, 1, 494, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (496, 1, 495, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (497, 2, 13, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (498, 1, 497, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (499, 1, 498, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (505, 2, 13, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (505, 4, 92, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (506, 1, 505, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (507, 1, 506, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (508, 1, 507, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (509, 1, 508, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (510, 1, 499, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (510, 2, 13, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (511, 1, 510, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (512, 1, 511, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (513, 1, 512, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (513, 2, 13, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (513, 4, 85, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (514, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (514, 4, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (515, 1, 514, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (516, 1, 515, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (517, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (518, 1, 517, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (519, 1, 518, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (520, 1, 519, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (521, 1, 520, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (522, 1, 521, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (522, 2, 14, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (523, 1, 522, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (524, 1, 523, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (525, 1, 524, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (526, 1, 525, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (527, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (527, 4, 89, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (528, 2, 14, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (529, 1, 528, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (530, 1, 529, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (531, 2, 14, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (531, 4, 95, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (532, 1, 531, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (533, 1, 532, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (534, 1, 533, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (535, 1, 534, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (536, 1, 530, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (536, 2, 14, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (537, 1, 536, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (538, 1, 537, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (539, 2, 14, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (540, 1, 539, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (541, 1, 540, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (542, 1, 541, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (543, 1, 542, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (544, 2, 14, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (545, 1, 544, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (546, 1, 545, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (547, 1, 546, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (548, 1, 547, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (550, 1, 95, 98, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (550, 2, 1, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (550, 5, 640000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (551, 1, 550, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (551, 5, 1280000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (552, 1, 551, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (552, 5, 2560000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (553, 1, 552, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (553, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (554, 1, 553, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (555, 1, 554, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (555, 2, 1, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (555, 5, 1280000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (556, 1, 555, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (556, 5, 2560000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (557, 1, 556, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (557, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (558, 1, 557, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (558, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (559, 1, 558, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (560, 2, 1, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (560, 5, 1280000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (561, 1, 560, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (561, 5, 2560000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (562, 1, 561, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (562, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (563, 1, 562, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (563, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (564, 1, 563, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (565, 2, 1, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (565, 5, 2560000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (566, 1, 565, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (566, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (567, 1, 566, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (567, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (568, 1, 567, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (568, 5, 20480000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (569, 1, 568, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (570, 2, 1, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (570, 5, 2560000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (571, 1, 570, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (571, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (572, 1, 571, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (572, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (573, 1, 572, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (573, 5, 20480000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (574, 1, 573, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (575, 2, 1, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (575, 5, 2560000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (576, 1, 575, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (576, 5, 5120000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (577, 1, 576, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (577, 5, 10240000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (578, 1, 577, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (578, 5, 20480000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (579, 1, 578, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (580, 1, 95, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (580, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (581, 1, 580, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (581, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (582, 1, 98, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (582, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (583, 1, 582, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (583, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (584, 1, 101, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (584, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (585, 1, 584, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (585, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (586, 1, 92, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (586, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (587, 1, 586, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (587, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (588, 1, 125, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (588, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (589, 1, 588, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (589, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (590, 1, 140, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (590, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (591, 1, 590, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (591, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (592, 1, 147, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (592, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (593, 1, 592, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (593, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (594, 1, 209, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (594, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (595, 1, 594, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (595, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (596, 1, 212, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (596, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (597, 1, 596, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (597, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (598, 1, 215, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (598, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (599, 1, 598, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (599, 6, 4825, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (600, 2, 7, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (600, 4, 147, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (601, 1, 600, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (602, 1, 601, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (603, 1, 602, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (603, 2, 7, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (604, 1, 603, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (605, 1, 604, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (606, 1, 605, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (607, 1, 606, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (608, 1, 66, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (608, 2, 5, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (609, 1, 608, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (610, 1, 609, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (611, 1, 610, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (611, 2, 5, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (612, 1, 51, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (612, 2, 5, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (613, 1, 612, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (614, 1, 613, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (615, 1, 614, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (616, 1, 615, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (617, 1, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (617, 2, 5, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (618, 1, 617, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (619, 1, 618, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (620, 1, 61, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (620, 2, 5, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (621, 1, 620, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (622, 1, 621, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (623, 1, 109, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (623, 2, 6, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (624, 1, 623, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (625, 1, 624, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (626, 1, 143, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (626, 2, 6, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (627, 1, 145, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (627, 2, 6, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (628, 1, 627, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (629, 1, 628, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (630, 1, 135, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (630, 2, 6, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (631, 1, 630, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (632, 1, 631, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (633, 1, 332, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (633, 2, 9, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (634, 1, 320, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (634, 2, 9, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (634, 4, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (635, 1, 634, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (636, 1, 635, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (637, 1, 346, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (637, 2, 9, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (637, 4, 145, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (638, 1, 637, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (639, 1, 638, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (640, 1, 639, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (641, 1, 640, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (642, 1, 349, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (642, 2, 9, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (643, 1, 642, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (644, 1, 643, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (645, 2, 10, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (645, 4, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (646, 1, 645, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (647, 1, 646, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (648, 1, 385, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (648, 2, 10, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (649, 1, 648, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (650, 1, 649, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (651, 1, 650, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (651, 2, 10, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (652, 1, 390, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (652, 2, 10, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (653, 1, 652, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (654, 1, 653, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (655, 2, 10, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (655, 4, 153, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (656, 1, 655, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (656, 2, 10, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (656, 4, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (657, 1, 656, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (658, 1, 657, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (659, 1, 201, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (659, 2, 7, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (660, 1, 190, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (660, 2, 7, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (661, 1, 660, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (662, 1, 661, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (663, 1, 607, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (663, 2, 7, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (664, 1, 663, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (665, 1, 664, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (666, 1, 196, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (666, 2, 7, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (667, 1, 666, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (668, 1, 667, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (669, 1, 261, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (669, 2, 8, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (670, 1, 669, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (671, 1, 670, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (672, 1, 671, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (673, 1, 672, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (674, 1, 271, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (674, 2, 8, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (675, 1, 674, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (676, 1, 675, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (677, 1, 676, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (677, 2, 8, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (678, 1, 245, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (678, 2, 8, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (678, 4, 158, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (679, 1, 678, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (680, 1, 679, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (681, 1, 680, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (681, 2, 8, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (682, 1, 681, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (683, 1, 682, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (683, 2, 8, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (684, 1, 683, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (685, 1, 684, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (686, 1, 434, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (686, 2, 11, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (687, 1, 686, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (688, 1, 687, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (689, 1, 688, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (690, 1, 689, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (691, 1, 428, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (691, 2, 11, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (692, 1, 691, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (693, 1, 692, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (694, 1, 693, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (695, 1, 694, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (696, 1, 697, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (696, 2, 11, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (697, 1, 423, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (697, 2, 11, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (698, 1, 696, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (698, 2, 11, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (698, 4, 151, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (699, 1, 466, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (699, 2, 12, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (700, 1, 699, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (701, 1, 700, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (702, 1, 471, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (702, 2, 12, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (706, 2, 4, 30, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (707, 1, 706, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (708, 1, 707, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (709, 1, 708, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (710, 1, 709, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (711, 1, 710, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (711, 2, 12, 35, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (712, 1, 711, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (713, 1, 712, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (714, 1, 713, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (715, 1, 714, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (716, 1, 715, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (716, 2, 12, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (717, 1, 716, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (718, 1, 717, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (719, 1, 718, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (720, 1, 719, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (721, 1, 720, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (721, 2, 12, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (722, 1, 1008, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (722, 2, 13, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (723, 1, 722, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (724, 1, 723, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (725, 1, 509, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (725, 2, 13, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (726, 1, 725, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (727, 1, 726, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (729, 1, 1003, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (729, 2, 13, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (730, 1, 729, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (731, 1, 730, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (732, 1, 513, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (732, 2, 13, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (733, 1, 732, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (734, 1, 733, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (735, 1, 734, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (735, 2, 13, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (736, 1, 543, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (736, 2, 14, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (737, 1, 736, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (738, 1, 737, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (739, 1, 548, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (739, 2, 14, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (740, 1, 739, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (741, 1, 740, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (742, 2, 14, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (743, 1, 742, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (744, 1, 743, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (745, 1, 744, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (746, 1, 745, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (747, 1, 746, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (747, 2, 14, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (748, 1, 747, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (749, 1, 748, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (750, 2, 14, 55, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (751, 1, 444, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (751, 2, 12, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (752, 1, 751, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (753, 1, 752, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (754, 1, 647, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (754, 2, 10, 50, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (754, 4, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (755, 1, 754, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (756, 1, 755, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (758, 1, 757, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (759, 1, 758, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (760, 1, 759, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (761, 1, 760, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (762, 1, 761, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (763, 1, 762, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (765, 1, 764, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (766, 1, 765, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (767, 1, 766, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (768, 1, 767, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (769, 1, 768, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (770, 1, 769, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (772, 1, 771, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (773, 1, 772, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (774, 1, 773, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (775, 1, 774, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (776, 1, 775, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (777, 1, 776, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (779, 1, 778, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (780, 1, 779, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (781, 1, 780, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (782, 1, 781, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (783, 1, 782, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (784, 1, 783, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (786, 1, 785, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (787, 1, 786, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (788, 1, 787, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (789, 1, 788, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (790, 1, 789, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (791, 1, 790, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (793, 1, 792, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (794, 1, 793, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (795, 1, 794, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (796, 1, 795, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (797, 1, 796, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (798, 1, 797, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (800, 1, 799, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (801, 1, 800, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (802, 1, 801, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (804, 1, 803, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (805, 1, 804, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (806, 1, 805, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (808, 1, 807, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (809, 1, 808, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (810, 1, 809, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (812, 1, 811, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (813, 1, 812, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (814, 1, 813, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (816, 1, 815, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (817, 1, 816, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (818, 1, 817, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (820, 1, 819, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (821, 1, 820, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (822, 1, 821, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (824, 1, 823, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (825, 1, 824, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (826, 1, 825, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (828, 1, 827, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (829, 1, 828, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (830, 1, 829, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (832, 1, 831, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (833, 1, 832, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (834, 1, 833, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (836, 1, 835, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (837, 1, 836, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (838, 1, 837, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (840, 1, 839, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (841, 1, 840, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (842, 1, 841, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (844, 1, 843, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (845, 1, 844, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (846, 1, 845, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (848, 1, 847, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (849, 1, 848, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (850, 1, 849, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (851, 1, 850, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (852, 1, 851, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (853, 1, 852, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (855, 1, 854, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (856, 1, 855, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (857, 1, 856, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (858, 1, 857, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (859, 1, 858, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (860, 1, 859, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (862, 1, 861, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (863, 1, 862, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (864, 1, 863, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (865, 1, 864, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (866, 1, 865, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (867, 1, 866, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (869, 1, 868, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (870, 1, 869, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (871, 1, 870, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (872, 1, 871, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (873, 1, 872, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (874, 1, 873, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (876, 1, 875, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (877, 1, 876, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (878, 1, 877, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (879, 1, 878, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (880, 1, 879, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (881, 1, 880, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (883, 1, 882, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (884, 1, 883, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (885, 1, 884, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (886, 1, 885, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (887, 1, 886, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (888, 1, 887, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (890, 1, 889, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (891, 1, 890, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (892, 1, 891, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (893, 1, 892, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (894, 1, 893, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (895, 1, 894, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (897, 1, 896, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (898, 1, 897, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (899, 1, 898, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (900, 1, 899, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (901, 1, 900, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (902, 1, 901, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (904, 1, 903, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (905, 1, 904, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (906, 1, 905, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (907, 1, 906, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (908, 1, 907, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (909, 1, 908, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (911, 1, 910, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (912, 1, 911, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (913, 1, 912, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (914, 1, 913, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (915, 1, 914, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (916, 1, 915, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (918, 1, 917, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (919, 1, 918, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (920, 1, 919, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (921, 1, 920, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (922, 1, 921, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (923, 1, 922, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (925, 1, 924, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (926, 1, 925, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (927, 1, 926, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (928, 1, 927, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (929, 1, 928, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (930, 1, 929, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (932, 1, 931, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (933, 1, 932, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (934, 1, 933, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (935, 1, 934, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (936, 1, 935, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (938, 1, 937, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (939, 1, 938, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (940, 1, 939, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (941, 1, 940, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (942, 1, 941, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (944, 1, 943, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (945, 1, 944, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (946, 1, 945, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (947, 1, 946, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (948, 1, 947, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (950, 1, 949, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (951, 1, 950, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (952, 1, 951, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (953, 1, 952, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (954, 1, 953, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (956, 1, 955, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (957, 1, 956, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (958, 1, 957, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (959, 1, 958, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (960, 1, 959, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (962, 1, 961, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (963, 1, 962, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (964, 1, 963, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (965, 1, 964, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (966, 1, 965, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (968, 1, 967, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (969, 1, 968, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (970, 1, 969, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (971, 1, 970, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (972, 1, 971, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (974, 1, 973, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (975, 1, 974, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (976, 1, 975, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (977, 1, 976, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (978, 1, 977, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (980, 1, 979, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (981, 1, 980, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (982, 1, 981, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (983, 1, 982, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (984, 1, 983, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (986, 1, 985, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (987, 1, 986, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (988, 1, 987, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (989, 1, 988, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (990, 1, 989, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (992, 1, 991, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (993, 1, 992, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (994, 1, 993, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (995, 1, 994, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (996, 1, 995, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (998, 1, 997, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (999, 1, 998, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1000, 1, 999, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1001, 1, 1000, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1002, 1, 1001, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1003, 2, 13, 45, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1003, 4, 91, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1004, 2, 13, 40, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1004, 4, 93, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1005, 1, 1004, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1006, 1, 1005, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1007, 1, 1006, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1008, 1, 1007, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1020, 1, 1019, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1022, 1, 1021, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1024, 1, 1023, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1026, 1, 1025, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1028, 1, 1027, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1030, 1, 1029, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1032, 1, 1031, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1034, 1, 1033, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1036, 1, 1035, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1038, 1, 1037, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1040, 1, 1039, 0, 0);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItemCondition] ([mSTNIID], [mSTNICType], [mParamA], [mParamB], [mParamC]) VALUES (1042, 1, 1041, 0, 0);
GO

