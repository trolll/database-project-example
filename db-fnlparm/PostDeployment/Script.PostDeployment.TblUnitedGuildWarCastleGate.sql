/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblUnitedGuildWarCastleGate
Date                  : 2023-10-07 09:07:31
*/


INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.550', 601, 848189.75, 12160.522461, 35108.203125, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.540', 602, 839063.375, 12187.53125, 30845.730469, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.573', 603, 852990.0, 12169.208984, 28026.107422, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.577', 604, 862122.3125, 12176.285156, 32269.191406, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.170', 605, 28989.9375, 12160.522461, 665261.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.153', 606, 19863.5625, 12187.53125, 660999.4375, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.190', 607, 33790.1875, 12169.208984, 658179.8125, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.193', 608, 42922.5, 12176.285156, 662422.875, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.227', 609, 123513.0, 12160.522461, 665261.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.217', 610, 114386.625, 12187.53125, 660999.4375, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.257', 611, 128313.25, 12169.208984, 658179.8125, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.260', 612, 137445.5625, 12176.285156, 662422.875, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.290', 613, 218036.0625, 12160.522461, 665261.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.283', 614, 208909.6875, 12187.53125, 660999.4375, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.307', 615, 222836.3125, 12169.208984, 658179.8125, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.310', 616, 231968.625, 12176.285156, 662422.875, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.340', 617, 312559.125, 12160.522461, 665261.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.333', 618, 303432.75, 12187.53125, 660999.4375, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.363', 619, 317359.375, 12169.208984, 658179.8125, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.367', 620, 326491.6875, 12176.285156, 662422.875, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.403', 621, 407082.15625, 12160.522461, 665261.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.400', 622, 397955.78125, 12187.53125, 660999.4375, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.420', 623, 411882.40625, 12169.208984, 658179.8125, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.423', 624, 421014.71875, 12176.285156, 662422.875, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.447', 625, 501605.21875, 12160.522461, 665261.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.440', 626, 492478.84375, 12187.53125, 660999.4375, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.460', 627, 506405.46875, 12169.208984, 658179.8125, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.467', 628, 515537.78125, 12176.285156, 662422.875, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.487', 629, 596128.25, 12160.522461, 665261.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.480', 630, 587001.875, 12187.53125, 660999.4375, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.493', 631, 600928.5, 12169.208984, 658179.8125, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.497', 632, 610060.8125, 12176.285156, 662422.875, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.183', 633, 28989.9375, 12160.522461, 759784.9375, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.177', 634, 19863.5625, 12187.53125, 755522.5, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.200', 635, 33790.1875, 12169.208984, 752702.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.203', 636, 42922.5, 12176.285156, 756945.9375, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.240', 637, 123513.0, 12160.522461, 759784.9375, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.230', 638, 114386.625, 12187.53125, 755522.5, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.267', 639, 128313.25, 12169.208984, 752702.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.270', 640, 137445.5625, 12176.285156, 756945.9375, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.300', 641, 218036.0625, 12160.522461, 759784.9375, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.293', 642, 208909.6875, 12187.53125, 755522.5, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.317', 643, 222836.3125, 12169.208984, 752702.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.320', 644, 231968.625, 12176.285156, 756945.9375, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.350', 645, 312559.125, 12160.522461, 759784.9375, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.343', 646, 303432.75, 12187.53125, 755522.5, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.373', 647, 317359.375, 12169.208984, 752702.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.377', 648, 326491.6875, 12176.285156, 756945.9375, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.413', 649, 407082.15625, 12160.522461, 759784.9375, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.407', 650, 397955.78125, 12187.53125, 755522.5, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.430', 651, 411882.40625, 12169.208984, 752702.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.433', 652, 421014.71875, 12176.285156, 756945.9375, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.457', 653, 501605.21875, 12160.522461, 759784.9375, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 1, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.450', 654, 492478.84375, 12187.53125, 755522.5, 1601.133179, 1.64061, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 1, 1);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.473', 655, 506405.46875, 12169.208984, 752702.875, 623.825806, 5.585053, 'c1_chaos_battlefield_gatedoor_01.r3m              ', 2, 0);
GO

INSERT INTO [dbo].[TblUnitedGuildWarCastleGate] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup], [mGateType]) VALUES ('2013-05-20 14:58:47.477', 656, 515537.78125, 12176.285156, 756945.9375, 1601.133179, 4.764749, 'c1_chaos_battlefield_gatedoor_02.r3m              ', 2, 1);
GO

