/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_NPCClass
Date                  : 2023-10-07 09:08:48
*/


INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (3, '던전과 스팟상인');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (4, '1.푸리에 영지');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (5, '공성탑');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (6, '6.아크라 지역');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (7, '2.블랙랜드 영지');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (8, '5.낙오자의 마을');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (9, '메테오스의 레어');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (10, '0.기네아섬');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (11, '테스트 NPC');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (12, '3.바이런 영지');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (13, '미사용');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (14, '4.로덴 영지');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (15, '기타 NPC');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (16, '이벤트 공용');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (17, '카오스 배틀');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (18, '이상한던전');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (19, '7.중고랩 던젼');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (20, '8.동선퀘스트');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (21, '통합 길드전 NPC');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (22, '캐릭터');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (23, '자경단 일일퀘');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (24, '시나리오 퀘스트');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (25, '9.일루미나의 성지');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (26, '10. 만월의 유적지');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (27, '봉인지 NPC');
GO

INSERT INTO [dbo].[TP_NPCClass] ([MClass], [MName]) VALUES (28, '천공섬 엘테르');
GO

