/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblCastleStone
Date                  : 2023-10-07 09:08:50
*/


INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (3, 391375.6875, 17048.552734, 276162.90625, 400.342133, 3.124139, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.390');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (4, 332552.34375, 17995.716797, 240121.0, 400.342133, 6.265732, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.357');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (5, 296661.84375, 12299.287109, 334561.25, 400.342133, 0.05236, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.330');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (6, 276337.8125, 16563.947266, 232257.84375, 400.342133, 0.017453, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.323');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (7, 956331.75, 14491.93457, 68665.640625, 400.342133, 1.570796, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.580');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (8, 950670.9375, 16760.621094, 695113.4375, 400.342133, 4.747295, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.583');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (53, 259409.46875, 20259.84375, 301153.53125, 400.342133, 6.178465, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.327');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (54, 113014.570313, 17515.601563, 178598.78125, 400.342133, 4.677482, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.210');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (55, 162901.125, 17478.939453, 325178.4375, 400.342133, 4.799655, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.277');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (56, 183518.09375, 17333.601563, 241229.28125, 400.342133, 4.764749, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.273');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (57, 196021.078125, 16447.751953, 149260.90625, 400.342133, 6.265732, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.280');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (58, 1025792.875, 13947.293945, 69147.796875, 400.342133, 4.660029, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.590');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (103, 237135.625, 11328.001953, 115417.484375, 400.342133, 6.213372, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.300');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (104, 393907.21875, 24956.994141, 196642.828125, 400.342133, 4.607669, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.387');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (105, 471909.9375, 22604.285156, 133723.078125, 400.342133, 4.625123, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.437');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (106, 327232.40625, 23155.976563, 87358.335938, 400.342133, 4.729842, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.353');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (107, 1095474.75, 12913.358398, 703610.6875, 400.342133, 4.712389, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.603');
GO

INSERT INTO [dbo].[TblCastleStone] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (108, 1147901.625, 14830.024414, 75060.515625, 400.342133, 4.729842, 'r1_ob_battleston_t1_test.r3m                      ', '2013-05-20 14:58:47.607');
GO

