/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Bead
Date                  : 2023-10-07 09:08:49
*/


INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6340, 0, 70.0, 1, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6341, 0, 50.0, 1, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6342, 0, 30.0, 1, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6343, 0, 70.0, 2, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6344, 0, 50.0, 2, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6345, 0, 30.0, 2, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6346, 0, 70.0, 3, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6347, 0, 50.0, 3, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6348, 0, 30.0, 3, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6349, 0, 70.0, 4, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6350, 0, 50.0, 4, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6351, 0, 30.0, 4, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6352, 0, 70.0, 5, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6353, 0, 50.0, 5, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6354, 0, 30.0, 5, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6355, 8, 70.0, 6, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6356, 8, 50.0, 6, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6357, 8, 30.0, 6, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6358, 8, 70.0, 7, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6359, 8, 50.0, 7, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6360, 8, 30.0, 7, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6361, 8, 70.0, 8, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6362, 8, 50.0, 8, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6363, 8, 30.0, 8, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6364, 8, 70.0, 9, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6365, 8, 50.0, 9, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6366, 8, 30.0, 9, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6367, 8, 70.0, 10, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6368, 8, 50.0, 10, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6369, 8, 30.0, 10, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6370, 2, 70.0, 11, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6371, 2, 50.0, 11, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6372, 2, 30.0, 11, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6373, 2, 70.0, 12, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6374, 2, 50.0, 12, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6375, 2, 30.0, 12, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6376, 2, 70.0, 13, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6377, 2, 50.0, 13, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6378, 2, 30.0, 13, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6379, 2, 70.0, 14, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6380, 2, 50.0, 14, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6381, 2, 30.0, 14, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6382, 2, 70.0, 15, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6383, 2, 50.0, 15, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6384, 2, 30.0, 15, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6385, 7, 70.0, 16, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6386, 7, 50.0, 16, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6387, 7, 30.0, 16, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6388, 7, 70.0, 17, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6389, 7, 50.0, 17, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6390, 7, 30.0, 17, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6391, 7, 70.0, 18, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6392, 7, 50.0, 18, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6393, 7, 30.0, 18, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6394, 7, 70.0, 19, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6395, 7, 50.0, 19, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6396, 7, 30.0, 19, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6397, 7, 70.0, 20, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6398, 7, 50.0, 20, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6399, 7, 30.0, 20, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6400, 6, 70.0, 21, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6401, 6, 50.0, 21, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6402, 6, 30.0, 21, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6403, 6, 70.0, 22, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6404, 6, 50.0, 22, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6405, 6, 30.0, 22, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6406, 6, 70.0, 23, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6407, 6, 50.0, 23, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6408, 6, 30.0, 23, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6409, 10, 70.0, 24, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6410, 10, 50.0, 24, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6411, 10, 30.0, 24, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6412, 6, 70.0, 25, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6413, 6, 50.0, 25, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6414, 6, 30.0, 25, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6415, 10, 70.0, 26, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6416, 10, 50.0, 26, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6417, 10, 30.0, 26, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6418, 10, 70.0, 27, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6419, 10, 50.0, 27, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6420, 10, 30.0, 27, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6421, 10, 70.0, 28, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6422, 10, 50.0, 28, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6423, 10, 30.0, 28, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6424, 10, 70.0, 29, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6425, 10, 50.0, 29, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6426, 10, 30.0, 29, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6427, 6, 70.0, 30, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6428, 6, 50.0, 30, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6429, 6, 30.0, 30, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6430, 1, 70.0, 31, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6431, 1, 50.0, 31, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6432, 1, 30.0, 31, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6433, 1, 70.0, 32, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6434, 1, 50.0, 32, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6435, 1, 30.0, 32, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6436, 1, 70.0, 33, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6437, 1, 50.0, 33, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6438, 1, 30.0, 33, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6439, 1, 70.0, 34, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6440, 1, 50.0, 34, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6441, 1, 30.0, 34, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6442, 1, 70.0, 35, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6443, 1, 50.0, 35, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6444, 1, 30.0, 35, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6560, 0, 30.0, 1, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6561, 0, 30.0, 1, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6562, 0, 30.0, 2, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6563, 0, 30.0, 2, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6564, 0, 30.0, 3, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6565, 0, 30.0, 3, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6566, 0, 30.0, 4, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6567, 0, 30.0, 4, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6568, 0, 30.0, 5, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6569, 0, 30.0, 5, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6570, 8, 30.0, 6, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6571, 8, 30.0, 6, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6572, 8, 30.0, 7, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6573, 8, 30.0, 7, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6574, 8, 30.0, 8, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6575, 8, 30.0, 8, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6576, 8, 30.0, 9, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6577, 8, 30.0, 9, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6578, 8, 30.0, 10, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6579, 8, 30.0, 10, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6580, 2, 30.0, 11, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6581, 2, 30.0, 11, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6582, 2, 30.0, 12, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6583, 2, 30.0, 12, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6584, 2, 30.0, 13, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6585, 2, 30.0, 13, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6586, 2, 30.0, 14, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6587, 2, 30.0, 14, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6588, 2, 30.0, 15, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6589, 2, 30.0, 15, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6590, 7, 30.0, 16, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6591, 7, 30.0, 16, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6592, 7, 30.0, 17, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6593, 7, 30.0, 17, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6594, 7, 30.0, 18, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6595, 7, 30.0, 18, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6596, 7, 30.0, 19, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6597, 7, 30.0, 19, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6598, 7, 30.0, 20, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6599, 7, 30.0, 20, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6600, 6, 30.0, 21, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6601, 6, 30.0, 21, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6602, 6, 30.0, 22, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6603, 6, 30.0, 22, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6604, 6, 30.0, 23, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6605, 6, 30.0, 23, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6606, 10, 30.0, 24, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6607, 10, 30.0, 24, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6608, 6, 30.0, 25, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6609, 6, 30.0, 25, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6610, 10, 30.0, 26, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6611, 10, 30.0, 26, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6612, 10, 30.0, 27, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6613, 10, 30.0, 27, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6614, 10, 30.0, 28, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6615, 10, 30.0, 28, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6616, 10, 30.0, 29, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6617, 10, 30.0, 29, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6618, 6, 30.0, 30, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6619, 6, 30.0, 30, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6620, 1, 30.0, 31, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6621, 1, 30.0, 31, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6622, 1, 30.0, 32, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6623, 1, 30.0, 32, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6624, 1, 30.0, 33, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6625, 1, 30.0, 33, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6626, 1, 30.0, 34, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6627, 1, 30.0, 34, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6628, 1, 30.0, 35, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (6629, 1, 30.0, 35, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7030, 0, 100.0, 36, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7033, 0, 100.0, 37, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7066, 3, 100.0, 36, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7067, 3, 95.0, 36, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7068, 3, 90.0, 36, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7069, 3, 85.0, 36, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7070, 3, 80.0, 36, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7071, 3, 100.0, 37, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7072, 3, 95.0, 37, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7073, 3, 90.0, 37, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7074, 3, 85.0, 37, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7075, 3, 80.0, 37, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7076, 3, 100.0, 38, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7077, 3, 95.0, 38, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7078, 3, 90.0, 38, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7079, 3, 85.0, 38, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7080, 3, 80.0, 38, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7081, 3, 100.0, 39, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7082, 3, 95.0, 39, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7083, 3, 90.0, 39, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7084, 3, 85.0, 39, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7085, 3, 80.0, 39, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7086, 3, 100.0, 40, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7087, 3, 95.0, 40, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7088, 3, 90.0, 40, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7089, 3, 85.0, 40, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7090, 3, 80.0, 40, 2);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7091, 5, 100.0, 41, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7092, 5, 95.0, 41, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7093, 5, 90.0, 41, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7094, 5, 85.0, 41, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7095, 5, 80.0, 41, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7096, 5, 100.0, 42, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7097, 5, 95.0, 42, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7098, 5, 90.0, 42, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7099, 5, 85.0, 42, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7100, 5, 80.0, 42, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7101, 5, 100.0, 43, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7102, 5, 95.0, 43, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7103, 5, 90.0, 43, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7104, 5, 85.0, 43, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7105, 5, 80.0, 43, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7106, 5, 100.0, 44, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7107, 5, 95.0, 44, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7108, 5, 90.0, 44, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7109, 5, 85.0, 44, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7110, 5, 80.0, 44, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7111, 5, 100.0, 45, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7112, 5, 95.0, 45, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7113, 5, 90.0, 45, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7114, 5, 85.0, 45, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7115, 5, 80.0, 45, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7116, 5, 100.0, 46, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7117, 5, 95.0, 46, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7118, 5, 90.0, 46, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7119, 5, 85.0, 46, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7120, 5, 80.0, 46, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7121, 5, 100.0, 47, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7122, 5, 95.0, 47, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7123, 5, 90.0, 47, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7124, 5, 85.0, 47, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7125, 5, 80.0, 47, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7126, 3, 100.0, 48, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7127, 3, 95.0, 48, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7128, 3, 90.0, 48, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7129, 3, 100.0, 49, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7130, 3, 95.0, 49, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7131, 3, 90.0, 49, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7132, 3, 100.0, 50, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7133, 3, 95.0, 50, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7134, 3, 90.0, 50, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7135, 3, 100.0, 51, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7136, 3, 95.0, 51, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7137, 3, 90.0, 51, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7138, 3, 100.0, 52, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7139, 3, 95.0, 52, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7140, 3, 90.0, 52, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7141, 3, 100.0, 53, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7142, 3, 95.0, 53, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7143, 3, 90.0, 53, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7144, 3, 100.0, 54, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7145, 3, 95.0, 54, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7146, 3, 90.0, 54, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7147, 3, 100.0, 55, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7148, 3, 95.0, 55, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7149, 3, 90.0, 55, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7150, 5, 100.0, 56, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7151, 5, 95.0, 56, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7152, 5, 90.0, 56, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7153, 5, 100.0, 57, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7154, 5, 95.0, 57, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7155, 5, 90.0, 57, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7156, 5, 100.0, 58, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7157, 5, 95.0, 58, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7158, 5, 90.0, 58, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7159, 5, 100.0, 59, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7160, 5, 95.0, 59, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7161, 5, 90.0, 59, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7162, 5, 100.0, 60, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7163, 5, 95.0, 60, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7164, 5, 90.0, 60, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7165, 9, 100.0, 61, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7166, 9, 95.0, 61, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7167, 9, 90.0, 61, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7168, 9, 100.0, 62, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7169, 9, 95.0, 62, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7170, 9, 90.0, 62, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7171, 9, 100.0, 63, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7172, 9, 95.0, 63, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7173, 9, 90.0, 63, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7174, 9, 100.0, 64, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7175, 9, 95.0, 64, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7176, 9, 90.0, 64, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7177, 9, 100.0, 65, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7178, 9, 95.0, 65, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7179, 9, 90.0, 65, 0);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7502, 5, 100.0, 66, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7503, 5, 95.0, 66, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7504, 5, 90.0, 66, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7505, 5, 85.0, 66, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7506, 5, 80.0, 66, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7507, 5, 100.0, 67, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7508, 5, 95.0, 67, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7509, 5, 90.0, 67, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7510, 5, 85.0, 67, 7);
GO

INSERT INTO [dbo].[DT_Bead] ([IID], [mTargetIPos], [mProb], [mGroup], [mItemSubType]) VALUES (7511, 5, 80.0, 67, 7);
GO

