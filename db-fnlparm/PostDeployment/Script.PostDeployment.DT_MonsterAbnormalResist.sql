/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_MonsterAbnormalResist
Date                  : 2023-10-07 09:07:30
*/


INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (134, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (397, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (154, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (154, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (120, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (120, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (35, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (35, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1021, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1021, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (70, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (70, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1021, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (70, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (70, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (70, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1021, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1021, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1018, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1018, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1018, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1018, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1018, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (818, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (818, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (818, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (818, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (818, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (661, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (661, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (661, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (661, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (397, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (112, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (112, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (111, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (111, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (110, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (234, 4);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (234, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (234, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (234, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (234, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (234, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (169, 4);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (169, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (169, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (169, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (169, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (169, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (661, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (577, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (577, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (110, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (103, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (103, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (96, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (96, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (45, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (45, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (529, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (529, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (87, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (439, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (82, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (660, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (660, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (696, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (658, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (658, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (654, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (654, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (469, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (653, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (653, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (577, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (531, 28);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (525, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (518, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (518, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (488, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (468, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (466, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (442, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (458, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (106, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (577, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (441, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (417, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1245, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (98, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (73, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (49, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (108, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (693, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (90, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (693, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (692, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (692, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (662, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (662, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (577, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (540, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (540, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (540, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (655, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (540, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (540, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (655, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (530, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (530, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (538, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (538, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (538, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (538, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (519, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (519, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (519, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (516, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (516, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (516, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (451, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (451, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (451, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (444, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (444, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (444, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (443, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (443, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (443, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (131, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (101, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (101, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (101, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (91, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (91, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (91, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (37, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (37, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (37, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (535, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (535, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (535, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (534, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (534, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (534, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (533, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (533, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (533, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (470, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (470, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (470, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (197, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (197, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (197, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (493, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (493, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (493, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (741, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (741, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (739, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (739, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (738, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (738, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (527, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (527, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (523, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (520, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (501, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (501, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (794, 4);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (794, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (794, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (794, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (794, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (795, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (795, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (795, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (795, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (795, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (538, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (536, 2);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (536, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (536, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (472, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (472, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (472, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (472, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (472, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (121, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (121, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (121, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (121, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (121, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (84, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (108, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (742, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (742, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (737, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (737, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (736, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (736, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (735, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (735, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (659, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (659, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (477, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (477, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (473, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (473, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (740, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (699, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (651, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (651, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (532, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (505, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (505, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (459, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (459, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (454, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (454, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (453, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (453, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (416, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (416, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (358, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (358, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (84, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (84, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (122, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (122, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (536, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (536, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (97, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (97, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (89, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (89, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (48, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (48, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (43, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (43, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (36, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (36, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (855, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (855, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (34, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (34, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (855, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (855, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (855, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (854, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (854, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (854, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (854, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (854, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (408, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (907, 4);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (914, 4);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (914, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (914, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (914, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (914, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (907, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (907, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (907, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (907, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (401, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (401, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (109, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (109, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (99, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (99, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (95, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (95, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (93, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (93, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1194, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1194, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1194, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1194, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1194, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1193, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1193, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1193, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1193, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1193, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1192, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1192, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1192, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1192, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1192, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1191, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1191, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1191, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1191, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1191, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (696, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1245, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1230, 7);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1230, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1231, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1243, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1243, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1242, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1242, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1247, 36);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1248, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1248, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1248, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1508, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1508, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1508, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1508, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1508, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1507, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1507, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1507, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1507, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1507, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1506, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1506, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1506, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1506, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1506, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1505, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1505, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1505, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1505, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1505, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1874, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1874, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1874, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1874, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1874, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (536, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (537, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (537, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (537, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (537, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1636, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1636, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1636, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1636, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1636, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1644, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1640, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1640, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1640, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1640, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1640, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1644, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1644, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1644, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1644, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1648, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1648, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1648, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1648, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1648, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (871, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (871, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (871, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (871, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (871, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (537, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1248, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1248, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1249, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1249, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1249, 21);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1249, 29);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1249, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (84, 12);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1232, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1232, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1241, 8);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (1241, 37);
GO

INSERT INTO [dbo].[DT_MonsterAbnormalResist] ([MID], [AID]) VALUES (84, 21);
GO

