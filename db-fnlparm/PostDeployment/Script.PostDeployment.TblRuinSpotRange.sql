/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblRuinSpotRange
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (14, 380397.0, 270040.0, 398135.0, 287397.0, '?? ?????');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (18, 322576.0, 228391.0, 344311.0, 250529.0, '???');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (20, 290951.0, 326435.0, 310065.0, 342669.0, '?? ???');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (21, 272415.0, 231289.0, 287912.0, 238914.0, '?? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (24, 947614.0, 66107.0, 973425.0, 91716.0, '??? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (33, 947507.0, 695771.0, 973451.0, 722457.0, '??? ?');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (42, 253586.0, 284965.0, 280154.0, 310815.0, '??? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (43, 101319.0, 174040.0, 127604.0, 188943.0, '??? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (46, 151504.0, 310348.0, 172118.0, 326939.0, '?? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (49, 169877.0, 232183.0, 188978.0, 246798.0, '??? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (50, 187698.0, 137757.0, 208854.0, 159190.0, '??? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (52, 1010625.0, 65780.0, 1036687.0, 92340.0, '??? ?');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (77, 227078.0, 108519.0, 270973.0, 131375.0, '???? ???');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (78, 383899.0, 174494.0, 411847.0, 195429.0, '??? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (79, 449505.0, 125192.0, 474259.0, 145862.0, '????');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (81, 314337.0, 78682.0, 340033.0, 103419.0, '???? ??');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (85, 1073780.0, 695657.0, 1100321.0, 722267.0, '???? ???');
GO

INSERT INTO [dbo].[TblRuinSpotRange] ([mSpotPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (90, 1136831.0, 66008.0, 1162272.0, 92189.0, '???? ????');
GO

