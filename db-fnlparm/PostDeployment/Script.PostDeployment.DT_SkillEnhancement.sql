/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_SkillEnhancement
Date                  : 2023-10-07 09:09:24
*/


INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (975, 137, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (976, 138, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (977, 139, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (978, 140, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (979, 157, 1, 0);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (980, 141, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (981, 143, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (982, 142, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (983, 147, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (984, 158, 1, 1);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (985, 137, 1, 2);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (986, 145, 1, 2);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (987, 146, 1, 2);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (988, 144, 1, 2);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (989, 42, 1, 2);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (990, 43, 1, 2);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (991, 148, 1, 3);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (992, 150, 1, 3);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (993, 149, 1, 3);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (994, 151, 1, 3);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (995, 152, 1, 3);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (996, 154, 1, 3);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (997, 159, 1, 3);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (998, 163, 1, 4);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (999, 84, 1, 4);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (1000, 164, 1, 4);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (1001, 85, 1, 4);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (1002, 165, 1, 4);
GO

INSERT INTO [dbo].[DT_SkillEnhancement] ([mESPID], [mSPID], [mOrderNo], [mUseClass]) VALUES (1003, 95, 1, 4);
GO

