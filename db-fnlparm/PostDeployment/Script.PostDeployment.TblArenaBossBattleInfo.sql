/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblArenaBossBattleInfo
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TblArenaBossBattleInfo] ([mPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (308, 882315.1, 94623.0, 913622.8, 125930.7, '1???');
GO

INSERT INTO [dbo].[TblArenaBossBattleInfo] ([mPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (309, 882315.1, 157638.4, 913622.8, 188946.1, '2???');
GO

INSERT INTO [dbo].[TblArenaBossBattleInfo] ([mPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (310, 882315.1, 220653.7, 913622.8, 251961.4, '3???');
GO

INSERT INTO [dbo].[TblArenaBossBattleInfo] ([mPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (311, 882315.1, 283669.1, 913622.8, 314976.8, '4???');
GO

INSERT INTO [dbo].[TblArenaBossBattleInfo] ([mPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (312, 882315.1, 346684.5, 913622.8, 377992.2, '5???');
GO

INSERT INTO [dbo].[TblArenaBossBattleInfo] ([mPlace], [mStartX], [mStartZ], [mEndX], [mEndZ], [mDesc]) VALUES (313, 882315.1, 409699.9, 913622.8, 441007.5, '6???');
GO

