/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_SlainType
Date                  : 2023-10-07 09:08:49
*/


INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (1, '언데드');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (2, '드래곤');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (3, '크리쳐');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (4, '공성');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (5, '마족');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (6, '휴먼');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (7, '엘프');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (8, '잡것');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (9, '데몬');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (10, '천사');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (11, '악마');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (12, '만월의 유적지');
GO

INSERT INTO [dbo].[TP_SlainType] ([SType], [SName]) VALUES (13, '휴먼을 제외한 모든것');
GO

