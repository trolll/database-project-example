/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AchieveItemTrophy
Date                  : 2023-10-07 09:08:51
*/


INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7267, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7268, 2, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7269, 3, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7270, 4, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7271, 1, 2, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7272, 2, 2, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7273, 3, 2, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7274, 4, 2, 1, 1);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7275, 1, 1, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7276, 2, 1, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7277, 3, 1, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7278, 4, 1, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7279, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7280, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7281, 3, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7282, 4, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7283, 1, 1, 3, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7284, 2, 1, 3, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7285, 3, 1, 3, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7286, 4, 1, 3, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7287, 1, 2, 3, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7288, 2, 2, 3, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7289, 3, 2, 3, 3);
GO

INSERT INTO [dbo].[DT_AchieveItemTrophy] ([IID], [mRarity], [mEquipType], [mEquipPos], [mAbilityType]) VALUES (7290, 4, 2, 3, 3);
GO

