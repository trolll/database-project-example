/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblFieldEventMonsterRatio
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[TblFieldEventMonsterRatio] ([mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster], [mDesc]) VALUES (0, 45, 35, 20, '??? ? ?? ???');
GO

INSERT INTO [dbo].[TblFieldEventMonsterRatio] ([mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster], [mDesc]) VALUES (1, 45, 30, 25, '??? ?? ?? ???');
GO

INSERT INTO [dbo].[TblFieldEventMonsterRatio] ([mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster], [mDesc]) VALUES (2, 40, 35, 25, '??? ?? ?? ???');
GO

INSERT INTO [dbo].[TblFieldEventMonsterRatio] ([mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster], [mDesc]) VALUES (3, 35, 45, 20, '?? ?? ?? ???');
GO

INSERT INTO [dbo].[TblFieldEventMonsterRatio] ([mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster], [mDesc]) VALUES (4, 40, 40, 20, '??? ? ?? ???');
GO

INSERT INTO [dbo].[TblFieldEventMonsterRatio] ([mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster], [mDesc]) VALUES (5, 35, 40, 25, '???? ??? ?? ???');
GO

INSERT INTO [dbo].[TblFieldEventMonsterRatio] ([mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster], [mDesc]) VALUES (6, 40, 30, 30, '???? ???? ?? ???');
GO

INSERT INTO [dbo].[TblFieldEventMonsterRatio] ([mEventType], [mPerOrgMonster], [mPerChgMonster], [mPerDelMonster], [mDesc]) VALUES (7, 40, 40, 20, '??? ?? ?? ???');
GO

