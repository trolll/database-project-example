/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Attribute
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (1, 1, 1, '2D5+9', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (2, 1, 2, '5D6', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (9, 1, 3, '6D6', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (10, 1, 4, '7D6+5', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (11, 2, 2, '5D6+2', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (12, 2, 3, '6D6+2', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (13, 2, 1, '2D8+3', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (14, 2, 4, '7D6+2', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (15, 3, 1, '2D6', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (16, 3, 2, '3D6+3', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (17, 3, 3, '6D6+3', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (18, 3, 4, '7D6+3', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (19, 4, 2, '4D6+2', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (20, 4, 3, '6D6+2', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (21, 4, 4, '7D6+2', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (24, 5, 1, '3D6', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (25, 5, 2, '5D6', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (26, 5, 3, '6D6', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (27, 5, 4, '7D6', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (28, 6, 1, '3D6', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (29, 6, 2, '5D6', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (30, 6, 3, '6D6', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (31, 6, 4, '7D6', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (32, 4, 1, '3D6+2', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (33, 1, 5, '1D100+250', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (34, 3, 5, '1D6+15', 50);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (35, 4, 5, '1D6+20', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (36, 2, 5, '2D6+30', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (37, 2, 6, '1D100+150', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (38, 1, 6, '1D100+300', 50);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (41, 1, 7, '1D10+10000', 1000);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (42, 3, 6, '1D100+150', 100);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (53, 1, 12, '2D6+16', 15);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (54, 1, 13, '3D5+23', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (55, 1, 14, '3D6+30', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (56, 1, 15, '3D7+37', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (57, 2, 12, '2D10+8', 15);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (58, 2, 13, '3D12+13', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (59, 2, 14, '3D14+18', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (60, 2, 15, '3D16+23', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (61, 3, 11, '2D2+7', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (62, 3, 12, '2D3+14', 15);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (63, 3, 13, '3D2+21', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (64, 3, 14, '3D3+28', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (65, 3, 15, '3D4+35', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (66, 3, 21, '2D3+10', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (67, 3, 22, '2D4+15', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (68, 3, 23, '3D3+20', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (69, 3, 24, '3D4+25', 35);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (70, 3, 25, '3D5+30', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (71, 4, 11, '1D14+1', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (72, 4, 12, '1D19+4', 15);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (73, 4, 13, '2D24+7', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (74, 4, 14, '2D29+10', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (75, 4, 15, '2D34+13', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (76, 4, 21, '1D15+6', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (77, 4, 22, '1D20+10', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (78, 4, 23, '2D25+14', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (79, 4, 24, '2D30+18', 35);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (80, 4, 25, '2D35+22', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (81, 2, 21, '2D23+27', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (82, 2, 22, '2D24+30', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (83, 2, 23, '3D20+30', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (84, 2, 24, '3D21+33', 35);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (85, 2, 25, '3D22+36', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (86, 1, 21, '2D8+15', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (87, 1, 22, '2D9+20', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (88, 1, 23, '3D8+25', 30);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (89, 1, 24, '3D9+30', 35);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (90, 1, 25, '3D10+35', 40);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (91, 7, 11, '2D15+20', 50);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (92, 1, 31, '1D1+1', 50);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (93, 1, 32, '1D1+1', 75);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (94, 1, 33, '1D1+1', 100);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (95, 1, 34, '1D1+1', 125);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (96, 1, 35, '1D1+1', 150);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (97, 1, 36, '1D300+700', 600);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (98, 1, 37, '1D400+500', 500);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (99, 1, 38, '1D300+800', 600);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (100, 7, 12, '0', 2);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (101, 7, 13, '0', 4);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (102, 7, 14, '0', 6);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (103, 7, 15, '0', 5);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (104, 7, 16, '0', 10);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (106, 7, 18, '0', 8);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (107, 7, 19, '0', 20);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (108, 7, 20, '0', 25);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (109, 3, 1, '1D200+200', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (110, 1, 1, '1D300+300', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (111, 1, 1, '1D500+600', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (112, 4, 1, '1D300+700', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (113, 2, 1, '1D200+250', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (114, 2, 1, '1D200+250', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (115, 7, 1, '1D200+250', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (116, 1, 1, '1D300+700', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (117, 4, 1, '1D300+800', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (118, 1, 1, '1D600+700', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (119, 1, 1, '1D300+700', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (120, 3, 1, '1D300+300', 0);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (105, 7, 17, '0', 15);
GO

INSERT INTO [dbo].[DT_Attribute] ([AID], [AType], [ALevel], [ADiceDamage], [ADamage]) VALUES (121, 7, 1, '1D100+150', 0);
GO

