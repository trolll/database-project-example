/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblRegionOptions
Date                  : 2023-10-07 09:09:31
*/


INSERT INTO [dbo].[TblRegionOptions] ([mPlace], [mIsSupport], [mExpRate], [mMonsterItemDropRate], [mShowPlayerName], [mMonSilverDropRate], [mNoDropItemOnDeath], [mNoExpDescOnDeath]) VALUES (69, 1, 2.0, 2.0, 1, 1.0, 0, 0);
GO

INSERT INTO [dbo].[TblRegionOptions] ([mPlace], [mIsSupport], [mExpRate], [mMonsterItemDropRate], [mShowPlayerName], [mMonSilverDropRate], [mNoDropItemOnDeath], [mNoExpDescOnDeath]) VALUES (92, 1, 2.0, 2.0, 1, 1.0, 0, 0);
GO

