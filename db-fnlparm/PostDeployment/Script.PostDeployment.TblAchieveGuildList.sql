/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblAchieveGuildList
Date                  : 2023-10-07 09:09:31
*/


SET IDENTITY_INSERT [dbo].[TblAchieveGuildList] ON;

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (1, '2015-09-15 13:18:01.513', 1, '푸리에', '셀린', 243, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (2, '2015-09-15 13:18:01.513', 1, '푸리에', '브랜든', 240, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (3, '2015-09-15 13:18:01.513', 1, '푸리에', '브라운', 238, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (4, '2015-09-15 13:18:01.513', 1, '푸리에', '배리', 236, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (5, '2015-09-15 13:18:01.513', 1, '푸리에', '클레멘트', 234, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (6, '2015-09-15 13:18:01.513', 1, '푸리에', '메트', 232, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (7, '2015-09-15 13:18:01.513', 1, '푸리에', '러스', 230, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (8, '2015-09-15 13:18:01.513', 1, '푸리에', '워렐', 228, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (9, '2015-09-15 13:18:01.513', 1, '푸리에', '새미', 226, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (10, '2015-09-15 13:18:01.513', 1, '푸리에', '리나', 224, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (11, '2015-09-15 13:18:01.513', 1, '푸리에', '슈만', 221, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (12, '2015-09-15 13:18:01.513', 1, '푸리에', '하트', 219, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (13, '2015-09-15 13:18:01.513', 1, '푸리에', '베드넷', 217, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (14, '2015-09-15 13:18:01.513', 1, '푸리에', '포포', 215, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (15, '2015-09-15 13:18:01.513', 1, '푸리에', '에바', 213, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (16, '2015-09-15 13:18:01.513', 1, '푸리에', '토드', 211, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (17, '2015-09-15 13:18:01.513', 1, '푸리에', '알프레드', 209, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (18, '2015-09-15 13:18:01.513', 1, '푸리에', '루터', 207, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (19, '2015-09-15 13:18:01.513', 1, '푸리에', '마쉬', 205, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (20, '2015-09-15 13:18:01.513', 1, '푸리에', '크레아', 203, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (21, '2015-09-15 13:18:01.513', 1, '푸리에', '케빈', 200, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (22, '2015-09-15 13:18:01.513', 1, '푸리에', '세실', 198, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (23, '2015-09-15 13:18:01.513', 1, '푸리에', '짐머맨', 196, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (24, '2015-09-15 13:18:01.513', 1, '푸리에', '던컨', 194, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (25, '2015-09-15 13:18:01.513', 1, '푸리에', '로버슨', 192, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (26, '2015-09-15 13:18:01.513', 1, '푸리에', '버드', 190, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (27, '2015-09-15 13:18:01.513', 1, '푸리에', '필립스', 188, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (28, '2015-09-15 13:18:01.513', 1, '푸리에', '키르', 186, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (29, '2015-09-15 13:18:01.513', 1, '푸리에', '크저브', 184, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (30, '2015-09-15 13:18:01.513', 1, '푸리에', '모튼', 182, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (31, '2015-09-15 13:18:01.513', 1, '푸리에', '클라크', 179, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (32, '2015-09-15 13:18:01.513', 1, '푸리에', '녹스', 177, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (33, '2015-09-15 13:18:01.513', 1, '푸리에', '아르투', 175, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (34, '2015-09-15 13:18:01.513', 1, '푸리에', '레오', 173, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (35, '2015-09-15 13:18:01.513', 1, '푸리에', '세바', 171, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (36, '2015-09-15 13:18:01.513', 1, '푸리에', '게일', 169, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (37, '2015-09-15 13:18:01.513', 1, '푸리에', '피셔', 167, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (38, '2015-09-15 13:18:01.513', 1, '푸리에', '보라스', 165, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (39, '2015-09-15 13:18:01.513', 1, '푸리에', '우셔', 163, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (40, '2015-09-15 13:18:01.513', 1, '푸리에', '올렌버그', 161, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (41, '2015-09-15 13:18:01.513', 1, '푸리에', '타샤', 158, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (42, '2015-09-15 13:18:01.513', 1, '푸리에', '카잔타루', 156, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (43, '2015-09-15 13:18:01.513', 1, '푸리에', '로이터', 154, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (44, '2015-09-15 13:18:01.513', 1, '푸리에', '페티오', 152, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (45, '2015-09-15 13:18:01.513', 1, '푸리에', '라만', 150, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (46, '2015-09-15 13:18:01.513', 1, '푸리에', '스카라', 148, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (47, '2015-09-15 13:18:01.513', 1, '푸리에', '잔쿠', 146, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (48, '2015-09-15 13:18:01.513', 1, '푸리에', '로마타', 144, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (49, '2015-09-15 13:18:01.513', 1, '푸리에', '우타루', 142, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (50, '2015-09-15 13:18:01.513', 1, '푸리에', '몰린', 140, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (51, '2015-09-15 13:18:01.513', 2, '블랙랜드', '토마', 234, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (52, '2015-09-15 13:18:01.513', 2, '블랙랜드', '엘렌', 232, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (53, '2015-09-15 13:18:01.513', 2, '블랙랜드', '존슨', 230, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (54, '2015-09-15 13:18:01.513', 2, '블랙랜드', '카트먼', 228, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (55, '2015-09-15 13:18:01.513', 2, '블랙랜드', '모드마', 226, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (56, '2015-09-15 13:18:01.513', 2, '블랙랜드', '길모어', 224, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (57, '2015-09-15 13:18:01.513', 2, '블랙랜드', '마리아', 222, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (58, '2015-09-15 13:18:01.513', 2, '블랙랜드', '에밀리', 220, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (59, '2015-09-15 13:18:01.513', 2, '블랙랜드', '세나', 218, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (60, '2015-09-15 13:18:01.513', 2, '블랙랜드', '러츠', 216, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (61, '2015-09-15 13:18:01.513', 2, '블랙랜드', '에드', 213, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (62, '2015-09-15 13:18:01.513', 2, '블랙랜드', '롭', 211, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (63, '2015-09-15 13:18:01.513', 2, '블랙랜드', '엘윈', 209, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (64, '2015-09-15 13:18:01.513', 2, '블랙랜드', '로딘', 207, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (65, '2015-09-15 13:18:01.513', 2, '블랙랜드', '오웬스', 205, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (66, '2015-09-15 13:18:01.513', 2, '블랙랜드', '에이츠', 203, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (67, '2015-09-15 13:18:01.513', 2, '블랙랜드', '피에르', 201, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (68, '2015-09-15 13:18:01.513', 2, '블랙랜드', '스판', 199, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (69, '2015-09-15 13:18:01.513', 2, '블랙랜드', '시에라', 197, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (70, '2015-09-15 13:18:01.513', 2, '블랙랜드', '셀비', 195, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (71, '2015-09-15 13:18:01.513', 2, '블랙랜드', '시어스', 193, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (72, '2015-09-15 13:18:01.513', 2, '블랙랜드', '랩코', 191, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (73, '2015-09-15 13:18:01.513', 2, '블랙랜드', '리드', 189, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (74, '2015-09-15 13:18:01.513', 2, '블랙랜드', '포그', 187, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (75, '2015-09-15 13:18:01.513', 2, '블랙랜드', '퀸튼', 185, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (76, '2015-09-15 13:18:01.513', 2, '블랙랜드', '레드키', 183, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (77, '2015-09-15 13:18:01.513', 2, '블랙랜드', '레스포', 181, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (78, '2015-09-15 13:18:01.513', 2, '블랙랜드', '제레미', 179, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (79, '2015-09-15 13:18:01.513', 2, '블랙랜드', '밀스', 177, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (80, '2015-09-15 13:18:01.513', 2, '블랙랜드', '모리아', 175, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (81, '2015-09-15 13:18:01.513', 2, '블랙랜드', '럭셔스', 172, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (82, '2015-09-15 13:18:01.513', 2, '블랙랜드', '올가', 170, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (83, '2015-09-15 13:18:01.513', 2, '블랙랜드', '뮬러', 168, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (84, '2015-09-15 13:18:01.513', 2, '블랙랜드', '넬슨', 166, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (85, '2015-09-15 13:18:01.513', 2, '블랙랜드', '코넬리아', 164, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (86, '2015-09-15 13:18:01.513', 2, '블랙랜드', '카일', 162, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (87, '2015-09-15 13:18:01.513', 2, '블랙랜드', '켈스', 160, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (88, '2015-09-15 13:18:01.513', 2, '블랙랜드', '빅스톤', 158, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (89, '2015-09-15 13:18:01.513', 2, '블랙랜드', '도노반', 156, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (90, '2015-09-15 13:18:01.513', 2, '블랙랜드', '크리에', 154, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (91, '2015-09-15 13:18:01.513', 2, '블랙랜드', '아로', 152, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (92, '2015-09-15 13:18:01.513', 2, '블랙랜드', '코난', 150, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (93, '2015-09-15 13:18:01.513', 2, '블랙랜드', '제피', 148, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (94, '2015-09-15 13:18:01.513', 2, '블랙랜드', '키라', 146, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (95, '2015-09-15 13:18:01.513', 2, '블랙랜드', '라이코', 144, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (96, '2015-09-15 13:18:01.513', 2, '블랙랜드', '죠쉬', 142, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (97, '2015-09-15 13:18:01.513', 2, '블랙랜드', '보리시', 140, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (98, '2015-09-15 13:18:01.513', 2, '블랙랜드', '폰챠카', 138, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (99, '2015-09-15 13:18:01.513', 2, '블랙랜드', '사비나', 136, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (100, '2015-09-15 13:18:01.513', 2, '블랙랜드', '샤라', 134, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (101, '2015-09-15 13:18:01.513', 3, '바이런', '코덴', 226, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (102, '2015-09-15 13:18:01.513', 3, '바이런', '제니', 224, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (103, '2015-09-15 13:18:01.513', 3, '바이런', '코로니', 222, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (104, '2015-09-15 13:18:01.513', 3, '바이런', '보시', 220, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (105, '2015-09-15 13:18:01.513', 3, '바이런', '컬레이', 218, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (106, '2015-09-15 13:18:01.513', 3, '바이런', '맥스', 216, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (107, '2015-09-15 13:18:01.513', 3, '바이런', '해머', 214, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (108, '2015-09-15 13:18:01.513', 3, '바이런', '콴치', 212, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (109, '2015-09-15 13:18:01.513', 3, '바이런', '보로샤', 210, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (110, '2015-09-15 13:18:01.513', 3, '바이런', '보린', 208, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (111, '2015-09-15 13:18:01.513', 3, '바이런', '코라', 206, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (112, '2015-09-15 13:18:01.513', 3, '바이런', '브루노', 204, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (113, '2015-09-15 13:18:01.513', 3, '바이런', '오튼', 202, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (114, '2015-09-15 13:18:01.513', 3, '바이런', '호건', 200, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (115, '2015-09-15 13:18:01.513', 3, '바이런', '캐미', 198, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (116, '2015-09-15 13:18:01.513', 3, '바이런', '레미', 196, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (117, '2015-09-15 13:18:01.513', 3, '바이런', '주다스', 194, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (118, '2015-09-15 13:18:01.513', 3, '바이런', '루니', 192, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (119, '2015-09-15 13:18:01.513', 3, '바이런', '지코', 190, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (120, '2015-09-15 13:18:01.513', 3, '바이런', '잠보', 188, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (121, '2015-09-15 13:18:01.513', 3, '바이런', '코코', 186, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (122, '2015-09-15 13:18:01.513', 3, '바이런', '루커', 184, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (123, '2015-09-15 13:18:01.513', 3, '바이런', '울버린', 182, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (124, '2015-09-15 13:18:01.513', 3, '바이런', '트리거잭', 180, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (125, '2015-09-15 13:18:01.513', 3, '바이런', '스탈린', 178, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (126, '2015-09-15 13:18:01.513', 3, '바이런', '실비아', 176, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (127, '2015-09-15 13:18:01.513', 3, '바이런', '민트', 174, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (128, '2015-09-15 13:18:01.513', 3, '바이런', '카멕', 172, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (129, '2015-09-15 13:18:01.513', 3, '바이런', '더들리', 170, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (130, '2015-09-15 13:18:01.513', 3, '바이런', '크리스티나', 168, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (131, '2015-09-15 13:18:01.513', 3, '바이런', '크루거', 166, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (132, '2015-09-15 13:18:01.513', 3, '바이런', '사티', 164, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (133, '2015-09-15 13:18:01.513', 3, '바이런', '루나', 162, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (134, '2015-09-15 13:18:01.513', 3, '바이런', '켈린', 160, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (135, '2015-09-15 13:18:01.513', 3, '바이런', '에이엔', 158, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (136, '2015-09-15 13:18:01.513', 3, '바이런', '릭서스', 156, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (137, '2015-09-15 13:18:01.513', 3, '바이런', '바투스', 154, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (138, '2015-09-15 13:18:01.513', 3, '바이런', '에아르윈', 152, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (139, '2015-09-15 13:18:01.513', 3, '바이런', '렌드에윈', 150, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (140, '2015-09-15 13:18:01.513', 3, '바이런', '케러백', 148, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (141, '2015-09-15 13:18:01.513', 3, '바이런', '론젤', 146, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (142, '2015-09-15 13:18:01.513', 3, '바이런', '켄트힐', 144, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (143, '2015-09-15 13:18:01.513', 3, '바이런', '아노스', 142, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (144, '2015-09-15 13:18:01.513', 3, '바이런', '테츠', 140, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (145, '2015-09-15 13:18:01.513', 3, '바이런', '시그네스', 138, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (146, '2015-09-15 13:18:01.513', 3, '바이런', '카사노아', 136, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (147, '2015-09-15 13:18:01.513', 3, '바이런', '테라노아', 134, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (148, '2015-09-15 13:18:01.513', 3, '바이런', '바르누이', 132, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (149, '2015-09-15 13:18:01.513', 3, '바이런', '카르노스', 130, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (150, '2015-09-15 13:18:01.513', 3, '바이런', '엘라노미아', 128, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (151, '2015-09-15 13:18:01.513', 4, '로덴', '코로', 217, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (152, '2015-09-15 13:18:01.513', 4, '로덴', '로트', 215, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (153, '2015-09-15 13:18:01.513', 4, '로덴', '잼머', 213, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (154, '2015-09-15 13:18:01.513', 4, '로덴', '로이', 211, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (155, '2015-09-15 13:18:01.513', 4, '로덴', '루우', 209, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (156, '2015-09-15 13:18:01.513', 4, '로덴', '카인', 207, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (157, '2015-09-15 13:18:01.513', 4, '로덴', '파스파', 205, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (158, '2015-09-15 13:18:01.513', 4, '로덴', '사루나', 203, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (159, '2015-09-15 13:18:01.513', 4, '로덴', '칸다루', 201, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (160, '2015-09-15 13:18:01.513', 4, '로덴', '라마', 199, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (161, '2015-09-15 13:18:01.513', 4, '로덴', '수잔', 198, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (162, '2015-09-15 13:18:01.513', 4, '로덴', '다루', 196, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (163, '2015-09-15 13:18:01.513', 4, '로덴', '빌로', 194, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (164, '2015-09-15 13:18:01.513', 4, '로덴', '코이', 192, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (165, '2015-09-15 13:18:01.513', 4, '로덴', '세키', 190, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (166, '2015-09-15 13:18:01.513', 4, '로덴', '기드', 188, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (167, '2015-09-15 13:18:01.513', 4, '로덴', '찰시', 186, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (168, '2015-09-15 13:18:01.513', 4, '로덴', '앨버스트', 184, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (169, '2015-09-15 13:18:01.513', 4, '로덴', '애비나', 182, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (170, '2015-09-15 13:18:01.513', 4, '로덴', '알바트론', 180, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (171, '2015-09-15 13:18:01.513', 4, '로덴', '록센들', 178, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (172, '2015-09-15 13:18:01.513', 4, '로덴', '앵글란드', 176, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (173, '2015-09-15 13:18:01.513', 4, '로덴', '에델코', 174, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (174, '2015-09-15 13:18:01.513', 4, '로덴', '레인다스', 172, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (175, '2015-09-15 13:18:01.513', 4, '로덴', '하르누보', 170, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (176, '2015-09-15 13:18:01.513', 4, '로덴', '라곤다스', 168, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (177, '2015-09-15 13:18:01.513', 4, '로덴', '케르빔', 166, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (178, '2015-09-15 13:18:01.513', 4, '로덴', '나르시스', 164, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (179, '2015-09-15 13:18:01.513', 4, '로덴', '에르스', 162, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (180, '2015-09-15 13:18:01.513', 4, '로덴', '마르스', 160, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (181, '2015-09-15 13:18:01.513', 4, '로덴', '하르네스', 159, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (182, '2015-09-15 13:18:01.513', 4, '로덴', '우르네', 157, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (183, '2015-09-15 13:18:01.513', 4, '로덴', '바네슈', 155, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (184, '2015-09-15 13:18:01.513', 4, '로덴', '가데사', 153, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (185, '2015-09-15 13:18:01.513', 4, '로덴', '세라핌', 151, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (186, '2015-09-15 13:18:01.513', 4, '로덴', '스트라토스', 149, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (187, '2015-09-15 13:18:01.513', 4, '로덴', '바리오스', 147, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (188, '2015-09-15 13:18:01.513', 4, '로덴', '안티오스', 145, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (189, '2015-09-15 13:18:01.513', 4, '로덴', '세이버투스', 143, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (190, '2015-09-15 13:18:01.513', 4, '로덴', '딥스트로크', 141, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (191, '2015-09-15 13:18:01.513', 4, '로덴', '지스파트', 139, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (192, '2015-09-15 13:18:01.513', 4, '로덴', '크레타리스', 137, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (193, '2015-09-15 13:18:01.513', 4, '로덴', '다이젠가', 135, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (194, '2015-09-15 13:18:01.513', 4, '로덴', '다르투스', 133, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (195, '2015-09-15 13:18:01.513', 4, '로덴', '시난주', 131, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (196, '2015-09-15 13:18:01.513', 4, '로덴', '소레스티안', 129, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (197, '2015-09-15 13:18:01.513', 4, '로덴', '바르쿠쉬', 127, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (198, '2015-09-15 13:18:01.513', 4, '로덴', '바이오넷', 125, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (199, '2015-09-15 13:18:01.513', 4, '로덴', '다이모스', 123, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (200, '2015-09-15 13:18:01.513', 4, '로덴', '체르노스', 122, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (201, '2015-09-15 13:18:01.513', 5, '엘테르', '칼', 209, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (202, '2015-09-15 13:18:01.513', 5, '엘테르', '엘기온', 207, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (203, '2015-09-15 13:18:01.513', 5, '엘테르', '헬리', 205, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (204, '2015-09-15 13:18:01.513', 5, '엘테르', '가이라', 203, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (205, '2015-09-15 13:18:01.513', 5, '엘테르', '하르만', 201, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (206, '2015-09-15 13:18:01.513', 5, '엘테르', '부르만', 199, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (207, '2015-09-15 13:18:01.513', 5, '엘테르', '아르간', 197, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (208, '2015-09-15 13:18:01.513', 5, '엘테르', '라비로즈', 195, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (209, '2015-09-15 13:18:01.513', 5, '엘테르', '알베르크', 193, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (210, '2015-09-15 13:18:01.513', 5, '엘테르', '엘리온느', 191, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (211, '2015-09-15 13:18:01.513', 5, '엘테르', '에비앙', 190, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (212, '2015-09-15 13:18:01.513', 5, '엘테르', '라쟈', 188, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (213, '2015-09-15 13:18:01.513', 5, '엘테르', '할바스', 186, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (214, '2015-09-15 13:18:01.513', 5, '엘테르', '아리엘', 184, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (215, '2015-09-15 13:18:01.513', 5, '엘테르', '엘미온', 182, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (216, '2015-09-15 13:18:01.513', 5, '엘테르', '리즈', 180, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (217, '2015-09-15 13:18:01.513', 5, '엘테르', '파미스', 178, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (218, '2015-09-15 13:18:01.513', 5, '엘테르', '루디', 176, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (219, '2015-09-15 13:18:01.513', 5, '엘테르', '루젤', 174, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (220, '2015-09-15 13:18:01.513', 5, '엘테르', '라비앙', 172, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (221, '2015-09-15 13:18:01.513', 5, '엘테르', '벨라', 171, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (222, '2015-09-15 13:18:01.513', 5, '엘테르', '안델슨', 169, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (223, '2015-09-15 13:18:01.513', 5, '엘테르', '델포르', 167, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (224, '2015-09-15 13:18:01.513', 5, '엘테르', '헤이든', 165, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (225, '2015-09-15 13:18:01.513', 5, '엘테르', '단테스', 163, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (226, '2015-09-15 13:18:01.513', 5, '엘테르', '델리', 161, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (227, '2015-09-15 13:18:01.513', 5, '엘테르', '벨리', 159, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (228, '2015-09-15 13:18:01.513', 5, '엘테르', '타렐', 157, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (229, '2015-09-15 13:18:01.513', 5, '엘테르', '일리언', 155, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (230, '2015-09-15 13:18:01.513', 5, '엘테르', '다오', 153, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (231, '2015-09-15 13:18:01.513', 5, '엘테르', '에일린', 152, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (232, '2015-09-15 13:18:01.513', 5, '엘테르', '베이슨', 150, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (233, '2015-09-15 13:18:01.513', 5, '엘테르', '루지나', 148, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (234, '2015-09-15 13:18:01.513', 5, '엘테르', '마이애나', 146, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (235, '2015-09-15 13:18:01.513', 5, '엘테르', '그웬델시아', 144, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (236, '2015-09-15 13:18:01.513', 5, '엘테르', '벨리사', 142, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (237, '2015-09-15 13:18:01.513', 5, '엘테르', '오빌리언', 140, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (238, '2015-09-15 13:18:01.513', 5, '엘테르', '케일', 138, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (239, '2015-09-15 13:18:01.513', 5, '엘테르', '아그네스', 136, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (240, '2015-09-15 13:18:01.513', 5, '엘테르', '엔지', 134, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (241, '2015-09-15 13:18:01.513', 5, '엘테르', '켄트', 133, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (242, '2015-09-15 13:18:01.513', 5, '엘테르', '램시', 131, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (243, '2015-09-15 13:18:01.513', 5, '엘테르', '콜린', 129, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (244, '2015-09-15 13:18:01.513', 5, '엘테르', '돌리', 127, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (245, '2015-09-15 13:18:01.513', 5, '엘테르', '엠마', 125, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (246, '2015-09-15 13:18:01.513', 5, '엘테르', '지나', 123, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (247, '2015-09-15 13:18:01.513', 5, '엘테르', '린다', 121, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (248, '2015-09-15 13:18:01.513', 5, '엘테르', '나니', 119, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (249, '2015-09-15 13:18:01.513', 5, '엘테르', '알렉스', 117, 0);
GO

INSERT INTO [dbo].[TblAchieveGuildList] ([mAchieveGuildID], [mRegDate], [mGuildRank], [mGuildName], [mMemberName], [mEquipPoint], [mUpdateIndex]) VALUES (250, '2015-09-15 13:18:01.513', 5, '엘테르', '스티브', 116, 0);
GO

SET IDENTITY_INSERT [dbo].[TblAchieveGuildList] OFF;