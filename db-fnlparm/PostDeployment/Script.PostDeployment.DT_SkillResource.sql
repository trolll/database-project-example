/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_SkillResource
Date                  : 2023-10-07 09:08:46
*/


INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (7, 14, 3, 'skill.dds', 0, 0);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (16, 9, 3, 'skill.dds', 0, 96);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (18, 11, 3, 'skill.dds', 288, 144);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (23, 16, 3, 'skill.dds', 48, 0);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (25, 17, 3, 'skill.dds', 48, 48);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (26, 18, 3, 'skill.dds', 96, 0);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (28, 19, 3, 'skill.dds', 192, 144);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (34, 20, 3, 'skill.dds', 48, 192);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (36, 13, 3, 'skill.dds', 432, 96);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (38, 15, 3, 'skill.dds', 240, 144);
GO

INSERT INTO [dbo].[DT_SkillResource] ([RID], [ROwnerID], [RType], [RFileName], [RPosX], [RPosY]) VALUES (41, 4, 1, '3', 1, 2);
GO

