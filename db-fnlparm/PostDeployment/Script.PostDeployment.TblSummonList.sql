/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblSummonList
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (1, 1, 233, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (2, 1, 145, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (3, 1, 146, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (4, 1, 149, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (5, 1, 396, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (6, 1, 83, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (7, 1, 127, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (8, 1, 153, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (9, 1, 131, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (10, 1, 115, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (11, 1, 418, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (12, 1, 75, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (13, 1, 74, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (14, 1, 209, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (15, 1, 33, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (16, 1, 80, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (17, 1, 79, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (18, 1, 78, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (19, 1, 77, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (20, 1, 81, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (21, 2, 793, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (22, 3, 792, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (23, 4, 131, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (24, 5, 443, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (25, 6, 874, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (26, 7, 879, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (28, 8, 1184, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (29, 9, 2041, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (30, 9, 2042, 1);
GO

INSERT INTO [dbo].[TblSummonList] ([mNo], [mGroupID], [mMonID], [mLevel]) VALUES (31, 9, 2043, 1);
GO

