/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblCastleGate
Date                  : 2023-10-07 09:08:49
*/


INSERT INTO [dbo].[TblCastleGate] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (2, 396275.3125, 15494.583008, 325356.0, 813.175598, 4.764749, 'castle_door.r3m                                   ', '2013-05-20 14:58:47.393');
GO

INSERT INTO [dbo].[TblCastleGate] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (52, 138443.875, 21347.226563, 267732.09375, 813.175598, 5.794493, 'castle_door.r3m                                   ', '2013-05-20 14:58:47.247');
GO

INSERT INTO [dbo].[TblCastleGate] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (102, 401210.28125, 19012.693359, 103914.859375, 813.175598, 5.532694, 'castle_door.r3m                                   ', '2013-05-20 14:58:47.383');
GO

INSERT INTO [dbo].[TblCastleGate] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (153, 1023993.1875, 28604.181641, 524075.03125, 813.175598, 4.712389, 'castle_door.r3m                                   ', '2013-05-20 14:58:47.593');
GO

INSERT INTO [dbo].[TblCastleGate] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (801, 835635.5625, 33781.9375, 1147399.5, 492.630615, 3.944444, 'r8_Battle_gate_01.r3m                             ', '2013-05-20 14:58:47.557');
GO

INSERT INTO [dbo].[TblCastleGate] ([mNo], [mPosx], [mPosy], [mPosz], [mWidth], [mDir], [mDesc], [mRegDate]) VALUES (802, 834467.9375, 33782.921875, 1148490.375, 492.630615, 3.944444, 'r8_Battle_gate_01.r3m                             ', '2013-05-20 14:58:47.560');
GO

