/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Module
Date                  : 2023-10-07 09:07:29
*/


INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (214, 45, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (215, 46, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (216, 47, 1, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (217, 48, 1, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (218, 49, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (219, 50, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (220, 4, 1, 0, 0, 70);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (221, 15, 50, 30, 30, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (222, 4, 6, 0, 0, -70);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (223, 51, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (225, 30, 4, 500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (4, 1, 1, 19, 10, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (6, 2, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (7, 3, 1, 0, 50, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (8, 4, 1, 0, 100, 70);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (9, 5, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (10, 6, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (11, 7, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (12, 8, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (13, 9, 1, 3, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (14, 10, 1, 60, 5, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (15, 10, 2, 60, 4, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (16, 10, 3, 60, 3, -8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (17, 10, 4, 60, 1, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (18, 10, 1, 30, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (19, 10, 2, 40, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (20, 10, 3, 50, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (21, 10, 4, 60, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (22, 10, 1, 1200, 10, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (23, 10, 2, 1200, 9, -11);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (24, 10, 3, 1200, 8, -12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (25, 10, 4, 1200, 7, -13);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (26, 4, 1, 0, 0, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (27, 4, 2, 0, -15, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (28, 4, 3, 0, -50, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (29, 4, 4, 0, -70, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (30, 3, 1, 0, -300, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (31, 3, 2, 0, -15, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (32, 3, 3, 0, -20, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (33, 3, 4, 0, -200, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (34, 11, 1, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (35, 11, 2, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (36, 11, 3, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (37, 11, 4, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (38, 12, 1, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (39, 12, 2, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (40, 12, 3, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (41, 12, 4, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (42, 13, 1, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (43, 13, 2, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (44, 13, 3, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (45, 13, 4, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (46, 14, 1, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (47, 14, 2, -2, -2, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (48, 14, 3, -3, -3, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (49, 14, 4, -4, -4, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (50, 15, 1, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (51, 15, 2, -2, -2, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (52, 15, 3, -3, -3, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (53, 15, 4, -4, -4, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (54, 16, 1, 0, -1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (55, 16, 2, 0, -2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (56, 16, 3, 0, -3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (57, 16, 4, 0, -4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (58, 17, 1, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (59, 17, 2, -2, -2, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (60, 17, 3, -3, -3, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (61, 17, 4, -4, -4, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (62, 5, 1, 0, -6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (63, 5, 2, 0, -9, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (64, 5, 3, 0, -12, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (65, 5, 4, 0, -15, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (66, 32, 1, 1, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (67, 32, 2, 2, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (68, 32, 3, 3, 6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (69, 32, 4, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (74, 19, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (75, 19, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (76, 19, 3, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (77, 19, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (78, 20, 1, 7, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (79, 21, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (80, 22, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (81, 22, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (82, 22, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (83, 2, 2, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (84, 2, 3, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (85, 32, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (86, 18, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (87, 18, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (88, 18, 3, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (89, 23, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (90, 23, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (91, 23, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (92, 1, 2, 30, 13, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (93, 1, 3, 45, 11, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (94, 34, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (102, 36, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (105, 38, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (108, 21, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (109, 39, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (110, 2, 4, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (111, 2, 5, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (98, 35, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (103, 33, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (104, 37, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (112, 9, 2, 2, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (113, 15, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (114, 15, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (115, 15, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (116, 17, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (117, 17, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (118, 17, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (119, 24, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (120, 24, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (121, 24, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (122, 26, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (123, 26, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (124, 26, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (125, 14, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (126, 14, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (127, 14, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (128, 40, 1, 859, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (129, 41, 1, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (130, 41, 2, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (131, 41, 3, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (132, 42, 1, 46, 18, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (134, 41, 4, 4, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (135, 41, 5, 5, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (140, 43, 1, 800, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (146, 4, 2, 0, 0, 150);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (136, 41, 6, 6, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (137, 41, 7, 7, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (138, 41, 8, 8, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (139, 41, 9, 9, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (141, 2, 1, -20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (142, 16, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (143, 25, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (144, 6, 1, 0, -1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (145, 7, 1, 0, -1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (148, 44, 1, 1, 70, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (149, 14, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (150, 14, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (151, 14, 6, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (152, 16, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (153, 16, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (154, 44, 2, 30000, 40, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (165, 14, 9, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (156, 20, 2, 5, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (166, 14, 10, 0, 0, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (163, 14, 7, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (164, 14, 8, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (167, 16, 4, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (168, 16, 5, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (172, 28, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (173, 27, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (174, 3, 2, 0, 65, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (175, 3, 3, 0, 100, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (176, 5, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (177, 6, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (178, 7, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (179, 30, 1, 250, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (180, 43, 2, 700, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (181, 31, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (182, 31, 2, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (183, 5, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (184, 15, 4, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (185, 27, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (186, 28, 2, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (187, 30, 2, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (188, 6, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (189, 20, 3, 6, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (190, 29, 1, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (194, 3, 4, 0, 65, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (196, 39, 4, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (199, 10, 1, 0, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (201, 10, 3, 0, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (202, 10, 1, 0, 1, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (203, 10, 2, 0, 1, -15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (204, 10, 3, 0, 1, -20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (205, 8, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (206, 8, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (207, 10, 5, 60, 1, -15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (208, 4, 5, 0, 0, -100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (209, 27, 90, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (210, 28, 90, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (211, 30, 3, 300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (213, 4, 6, 0, 0, -150);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (191, 28, 1, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (192, 27, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (193, 7, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (200, 10, 2, 0, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (212, 17, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (226, 14, 11, 0, 0, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (227, 14, 12, -1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (228, 16, 6, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (229, 14, 13, 0, 0, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (230, 17, 50, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (231, 10, 6, 0, 15, -1100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (232, 21, 3, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (233, 52, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (234, 21, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (235, 10, 7, 0, 1, -30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (236, 53, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (238, 35, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (241, 55, 1, 1900, 5700, 9500);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (242, 21, 5, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (243, 10, 8, 0, 1, -1000);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (246, 20, 99, 99, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (249, 28, 100, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (251, 27, 3, 5, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (252, 21, 6, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (254, 27, 10, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (257, 14, 20, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (258, 14, 21, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (259, 14, 22, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (260, 16, 10, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (261, 16, 11, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (262, 16, 12, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (263, 4, 10, 0, 0, -500);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (264, 20, 10, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (265, 20, 11, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (268, 28, 112, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (269, 28, 113, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (270, 28, 114, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (271, 28, 115, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (272, 28, 116, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (273, 28, 117, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (274, 28, 118, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (275, 28, 119, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (276, 28, 11, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (278, 20, 13, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (279, 20, 14, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (280, 20, 15, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (281, 59, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (282, 58, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (297, 58, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (298, 58, 3, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (299, 58, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (300, 58, 5, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (301, 58, 6, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (302, 58, 7, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (303, 58, 8, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (304, 58, 9, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (305, 58, 10, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (306, 58, 11, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (307, 58, 12, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (308, 58, 13, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (309, 58, 14, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (310, 58, 15, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (311, 58, 16, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (312, 58, 17, 17, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (313, 58, 18, 18, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (314, 58, 19, 19, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (315, 58, 20, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (316, 58, 21, 21, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (317, 58, 22, 22, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (237, 54, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (244, 56, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (250, 30, 5, 1000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (255, 28, 10, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (266, 28, 110, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (267, 28, 111, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (277, 20, 12, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (318, 58, 23, 23, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (319, 58, 24, 24, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (320, 58, 25, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (321, 58, 26, 26, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (322, 58, 27, 27, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (323, 58, 28, 28, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (324, 58, 29, 29, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (325, 58, 30, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (326, 58, 31, 31, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (327, 58, 32, 32, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (328, 58, 33, 33, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (329, 58, 34, 34, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (330, 58, 35, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (331, 58, 36, 36, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (332, 58, 37, 37, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (333, 58, 38, 38, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (334, 58, 39, 39, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (335, 58, 40, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (336, 58, 41, 41, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (337, 58, 42, 42, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (338, 58, 43, 43, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (339, 58, 44, 44, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (340, 58, 45, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (341, 58, 46, 46, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (342, 58, 47, 47, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (343, 58, 48, 48, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (344, 58, 49, 49, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (345, 58, 50, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (346, 58, 51, 51, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (347, 58, 52, 52, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (348, 58, 53, 53, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (349, 58, 54, 54, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (350, 58, 55, 55, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (351, 58, 56, 56, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (352, 58, 57, 57, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (353, 58, 58, 58, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (354, 58, 59, 59, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (355, 58, 60, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (356, 58, 61, 61, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (357, 58, 62, 62, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (358, 58, 63, 63, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (359, 58, 64, 64, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (360, 58, 65, 65, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (361, 58, 66, 66, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (362, 58, 67, 67, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (363, 58, 68, 68, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (364, 58, 69, 69, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (365, 58, 70, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (366, 58, 71, 71, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (367, 58, 72, 72, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (368, 58, 73, 73, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (369, 58, 74, 74, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (370, 58, 75, 75, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (371, 58, 76, 76, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (372, 58, 77, 77, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (373, 58, 78, 78, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (374, 58, 79, 79, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (375, 58, 80, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (376, 58, 81, 81, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (377, 58, 82, 82, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (378, 58, 83, 83, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (379, 58, 84, 84, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (380, 58, 85, 85, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (381, 58, 86, 86, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (382, 58, 87, 87, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (383, 58, 88, 88, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (384, 58, 89, 89, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (385, 58, 90, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (386, 59, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (387, 59, 3, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (390, 52, 3, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (391, 20, 16, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (392, 20, 20, 20, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (393, 60, 1, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (394, 61, 1, -4, -6, -9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (395, 62, 1, 0, 100, -130);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (396, 63, 1, 2496, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (397, 64, 1, 200, 300, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (398, 29, 2, 50, 200, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (399, 16, 13, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (400, 20, 17, 17, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (401, 20, 18, 18, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (402, 31, 3, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (414, 66, 1, 1, 969, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (415, 66, 1, 2, 977, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (416, 66, 2, 3, 985, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (417, 67, 1, 0, 450, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (418, 52, 1, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (440, 70, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (420, 10, 1, 0, 1, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (428, 7, 1, 0, -2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (429, 7, 2, 0, -5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (430, 5, 2, 0, -5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (431, 6, 2, 0, -5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (434, 16, 1, 0, 0, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (435, 14, 1, 0, 0, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (436, 67, 1, 0, 800, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (245, 57, 51, -10, -10, -5000);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (253, 21, 7, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (388, 28, 120, 18, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (389, 28, 121, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (256, 52, 2, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (403, 5, 1, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (404, 5, 2, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (405, 5, 3, 0, 8, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (406, 6, 1, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (407, 6, 2, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (408, 6, 3, 0, 6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (409, 3, 1, 0, 165, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (410, 3, 2, 0, -90, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (411, 3, 3, 0, -110, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (412, 3, 4, 0, -140, 110);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (413, 65, 1, -5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (426, 5, 1, 0, -2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (427, 6, 1, 0, -2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (421, 66, 3, 4, 993, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (422, 66, 4, 4, 994, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (423, 1, 1, 70, 11, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (424, 68, 1, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (425, 69, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (437, 20, 21, 21, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (438, 20, 22, 22, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (439, 20, 23, 23, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (441, 70, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (442, 70, 3, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (443, 70, 4, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (444, 70, 5, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (445, 47, 2, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (446, 2, 6, -50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (447, 29, 3, -50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (452, 20, 24, 24, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (449, 7, 5, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (450, 5, 5, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (451, 6, 5, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (453, 20, 25, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (454, 20, 26, 26, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (455, 20, 27, 27, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (456, 20, 28, 28, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (457, 20, 29, 29, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (458, 20, 30, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (459, 20, 31, 31, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (460, 20, 32, 32, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (461, 20, 33, 33, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (462, 20, 34, 34, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (463, 20, 35, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (464, 20, 36, 36, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (465, 20, 37, 37, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (466, 20, 38, 38, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (467, 20, 39, 39, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (468, 20, 40, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (469, 20, 41, 41, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (470, 20, 42, 42, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (471, 20, 43, 43, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (472, 20, 44, 44, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (473, 20, 45, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (474, 71, 1, 250, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (475, 72, 1, 125, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (476, 4, 11, 0, 0, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (477, 73, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (478, 12, 5, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (479, 20, 46, 46, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (480, 20, 47, 47, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (481, 20, 48, 48, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (482, 20, 49, 49, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (483, 16, 14, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (484, 3, 5, 0, 15, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (485, 15, 7, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (486, 17, 8, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (487, 71, 2, 500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (488, 72, 2, 250, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (489, 47, 3, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (490, 5, 6, 0, -3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (491, 6, 6, 0, -3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (492, 7, 6, 0, -3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (493, 10, 9, 0, 1, -50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (494, 79, 1, 627, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (495, 77, 1, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (496, 74, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (530, 10, 10, 0, 1, -20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (531, 47, 4, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (532, 1, 8, 30, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (533, 1, 9, 60, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (534, 47, 5, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (535, 10, 12, 0, 1, -40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (536, 79, 2, 639, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (537, 3, 8, 0, -20, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (538, 79, 3, 643, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (539, 77, 2, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (540, 83, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (541, 74, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (542, 74, 3, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (543, 12, 6, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (544, 88, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (545, 87, 1, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (546, 16, 4, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (547, 16, 5, 9, 9, 9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (548, 16, 6, 12, 12, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (549, 16, 7, 15, 15, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (550, 16, 8, 18, 18, 18);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (551, 16, 9, 21, 21, 21);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (555, 15, 5, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (556, 15, 6, 8, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (557, 15, 8, 12, 12, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (558, 15, 9, 14, 14, 14);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (559, 17, 10, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (560, 17, 6, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (561, 17, 7, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (562, 76, 1, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (563, 76, 2, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (564, 76, 3, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (565, 82, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (566, 89, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (567, 66, 5, 4, 1195, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (574, 21, 8, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (575, 81, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (576, 80, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (577, 10, 11, 0, 7, -50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (578, 10, 13, 0, 7, -70);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (581, 10, 15, 0, 7, -90);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (582, 10, 15, 0, 1, -150);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (583, 4, 15, 0, 0, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (584, 3, 9, 0, 15, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (585, 4, 16, 0, 0, -50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (588, 30, 6, 1500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (589, 20, 58, 58, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (590, 20, 59, 59, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (604, 90, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (605, 91, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (606, 91, 2, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (610, 20, 67, 67, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (611, 20, 68, 68, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (612, 20, 69, 69, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (613, 20, 70, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (614, 20, 71, 71, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (615, 20, 72, 72, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (616, 20, 73, 73, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (617, 20, 74, 74, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (618, 20, 75, 75, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (619, 20, 76, 76, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (620, 20, 77, 77, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (621, 16, 16, 25, 25, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (622, 16, 16, 30, 30, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (623, 16, 16, 35, 35, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (624, 14, 32, 13, 13, 13);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (625, 14, 32, 16, 16, 16);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (626, 14, 32, 19, 19, 19);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (627, 3, 7, 0, 60, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (628, 3, 7, 0, 75, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (629, 3, 7, 0, 90, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (630, 4, 13, 0, 0, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (497, 1, 4, 60, 11, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (498, 15, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (499, 17, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (569, 78, 1, 444, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (570, 78, 2, 486, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (571, 78, 3, 488, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (572, 75, 1, 633, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (573, 75, 2, 634, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (579, 4, 14, 0, 0, -150);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (580, 10, 14, 0, 1, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (586, 71, 5, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (587, 72, 5, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (608, 92, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (609, 20, 66, 66, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (631, 4, 13, 0, 0, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (632, 4, 13, 0, 0, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (633, 15, 11, 25, 25, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (634, 15, 11, 30, 30, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (635, 15, 51, 35, 35, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (636, 17, 7, 25, 25, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (637, 17, 7, 30, 30, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (638, 17, 7, 35, 35, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (639, 71, 4, 1400, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (640, 71, 4, 1700, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (641, 71, 4, 2000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (642, 72, 4, 700, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (643, 72, 4, 850, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (644, 72, 4, 1000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (645, 4, 17, 0, 0, -75);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (646, 4, 18, 0, 0, -500);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (647, 76, 4, 250, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (648, 76, 5, 300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (649, 1, 10, 90, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (650, 1, 11, 120, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (651, 75, 4, 752, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (652, 75, 5, 753, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (653, 6, 7, 0, -7, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (654, 6, 8, 0, -9, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (655, 7, 7, 0, -7, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (656, 7, 8, 0, -9, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (657, 5, 7, 0, -7, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (658, 5, 8, 0, -9, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (659, 15, 6, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (672, 77, 3, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (673, 83, 2, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (674, 83, 3, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (675, 74, 4, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (676, 74, 5, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (677, 74, 6, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (678, 93, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (681, 52, 11, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (682, 94, 1, 15, 7, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (707, 10, 10, 0, 10, -12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (708, 20, 78, 78, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (709, 20, 79, 79, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (710, 20, 80, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (711, 20, 81, 81, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (712, 20, 82, 82, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (713, 20, 83, 83, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (714, 20, 84, 84, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (715, 20, 85, 85, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (716, 20, 86, 86, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (717, 20, 87, 87, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (718, 20, 88, 88, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (719, 20, 89, 89, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (720, 20, 90, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (721, 20, 91, 91, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (722, 20, 92, 92, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (723, 95, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (724, 96, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (726, 97, 1, 1527, 1527, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (727, 97, 1, 1526, 1526, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (728, 97, 1, 1525, 1525, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (729, 97, 1, 1523, 1523, 1523);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (730, 97, 1, 1522, 1522, 1522);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (731, 97, 1, 1521, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (732, 97, 1, 1519, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (733, 97, 1, 1524, 1524, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (734, 97, 1, 1520, 1520, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (735, 97, 1, 1518, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (736, 97, 1, 1517, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (740, 20, 126, 126, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (741, 20, 127, 127, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (742, 20, 128, 128, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (743, 20, 129, 129, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (744, 20, 130, 130, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (745, 20, 131, 131, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (746, 20, 132, 132, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (747, 20, 133, 133, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (748, 20, 95, 95, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (749, 20, 96, 96, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (750, 30, 7, 450, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (751, 30, 8, 650, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (752, 30, 9, 750, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (753, 41, 10, 1, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (754, 41, 11, 1, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (755, 41, 12, 1, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (756, 41, 13, 1, 6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (757, 41, 14, 9, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (758, 41, 15, 9, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (759, 41, 16, 9, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (760, 41, 17, 9, 6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (763, 28, 13, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (764, 28, 14, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (765, 28, 15, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (766, 28, 16, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (793, 17, 8, 8, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (794, 17, 9, 9, 9, 9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (795, 17, 11, 11, 11, 11);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (798, 16, 30, 45, 45, 45);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (799, 16, 31, 50, 50, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (800, 16, 32, 55, 55, 55);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (801, 14, 35, 25, 25, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (802, 14, 36, 28, 28, 28);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (803, 14, 37, 31, 31, 31);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (804, 10, 22, 0, 1, -30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (805, 10, 23, 0, 1, -50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (806, 79, 7, 932, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (807, 79, 8, 933, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (810, 3, 15, 0, 115, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (811, 3, 16, 0, 125, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (812, 3, 17, 0, 140, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (813, 4, 20, 0, 0, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (814, 4, 21, 0, 0, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (815, 4, 22, 0, 0, 70);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (816, 15, 45, 45, 45, 45);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (817, 15, 45, 50, 50, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (818, 15, 45, 55, 55, 55);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (819, 17, 45, 45, 45, 45);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (820, 17, 45, 50, 50, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (821, 17, 45, 55, 55, 55);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (824, 71, 12, 2600, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (825, 71, 13, 2900, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (826, 71, 14, 3200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (827, 72, 12, 1300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (828, 72, 13, 1450, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (829, 72, 14, 1600, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (830, 27, 11, 20, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (831, 27, 12, 28, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (832, 28, 17, 10, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (833, 28, 18, 14, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (836, 20, 146, 146, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (837, 20, 147, 147, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (838, 20, 148, 148, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (839, 20, 149, 149, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (840, 20, 150, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (841, 20, 151, 151, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (842, 20, 152, 152, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (843, 20, 153, 153, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (844, 20, 154, 154, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (845, 20, 155, 155, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (846, 20, 156, 156, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (847, 20, 157, 157, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (848, 20, 158, 158, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (849, 20, 159, 159, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (850, 100, 6, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (852, 16, 14, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (867, 20, 174, 174, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (868, 20, 175, 175, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (869, 20, 176, 176, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (870, 20, 177, 177, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (871, 20, 178, 178, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (872, 1, 12, 67, 12, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (873, 4, 23, 0, 0, 150);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (877, 20, 179, 179, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (878, 20, 180, 180, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (889, 28, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (893, 20, 184, 184, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (894, 28, 3, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (895, 27, 13, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (896, 27, 14, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (898, 20, 185, 185, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (899, 20, 186, 186, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (900, 20, 187, 187, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (901, 20, 188, 188, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (902, 20, 189, 189, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (906, 10, 24, 0, 1, -30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (907, 14, 39, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (908, 52, 13, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (909, 15, 12, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (910, 15, 13, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (911, 15, 14, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (912, 115, 1, 29, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (913, 114, 1, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (914, 100, 13, -5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (915, 75, 6, 1419, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (916, 75, 7, 1423, 1, 315);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (917, 75, 8, 1893, 1, 315);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (918, 75, 9, 1425, 1, 315);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (921, 20, 192, 192, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (923, 71, 17, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (924, 71, 18, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (925, 71, 19, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (926, 71, 20, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (927, 72, 16, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (928, 72, 17, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (929, 72, 18, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (930, 72, 19, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (931, 72, 20, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (932, 30, 11, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (933, 30, 12, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (934, 30, 13, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (935, 30, 14, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (936, 30, 15, 250, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (937, 27, 21, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (938, 27, 22, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (939, 27, 23, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (940, 27, 24, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (941, 27, 25, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (942, 28, 21, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (943, 28, 22, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (944, 28, 23, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (945, 28, 24, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (946, 28, 25, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (947, 27, 26, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (948, 27, 27, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (949, 27, 28, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (950, 28, 26, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (951, 28, 27, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (952, 28, 28, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (953, 5, 11, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (954, 5, 12, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (955, 5, 13, 0, 6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (956, 6, 11, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (957, 6, 12, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (958, 6, 13, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (959, 7, 11, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (960, 7, 12, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (961, 7, 13, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (962, 52, 16, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (963, 52, 17, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (964, 52, 18, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (965, 52, 19, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (966, 52, 20, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (967, 106, 51, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (968, 106, 52, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (969, 106, 53, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (970, 106, 54, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (971, 106, 55, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (972, 108, 51, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (973, 108, 52, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (974, 108, 53, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (975, 108, 54, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (976, 108, 55, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (977, 107, 51, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (978, 107, 52, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (979, 107, 53, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (980, 107, 54, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (981, 107, 55, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (982, 109, 51, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (983, 109, 52, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (984, 109, 53, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (985, 111, 51, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (986, 111, 52, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (987, 111, 53, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (988, 110, 51, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (989, 110, 52, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (500, 20, 50, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (501, 20, 51, 51, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (502, 20, 52, 52, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (503, 20, 53, 53, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (504, 20, 54, 54, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (505, 20, 55, 55, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (506, 20, 56, 56, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (507, 20, 57, 57, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (508, 16, 15, 15, 15, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (509, 16, 16, 20, 20, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (510, 14, 31, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (511, 14, 32, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (512, 3, 6, 0, 30, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (513, 3, 7, 0, 45, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (514, 4, 12, 0, 0, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (515, 4, 13, 0, 0, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (516, 15, 10, 15, 15, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (517, 15, 11, 20, 20, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (518, 17, 9, 15, 15, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (519, 17, 7, 20, 20, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (520, 71, 3, 800, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (521, 71, 4, 1100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (522, 72, 3, 400, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (523, 72, 4, 550, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (524, 86, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (607, 20, 4, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (783, 99, 1, 2000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (851, 100, 12, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (874, 3, 1, 0, 80, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (875, 10, 5, 60, 1, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (876, 10, 6, 60, 1, -20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (892, 20, 183, 183, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (897, 28, 122, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (903, 20, 190, 190, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (922, 71, 16, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (990, 110, 53, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (991, 16, 41, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (992, 16, 42, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (993, 16, 43, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (994, 16, 44, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (995, 16, 45, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (996, 16, 46, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (997, 16, 47, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (998, 16, 48, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (999, 16, 49, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1000, 16, 50, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1001, 16, 51, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1002, 16, 52, 0, 0, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1003, 16, 53, 0, 0, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1004, 16, 54, 0, 0, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1005, 16, 55, 0, 0, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1006, 14, 51, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1007, 14, 52, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1008, 14, 53, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1009, 14, 54, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1010, 14, 55, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1011, 14, 56, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1012, 14, 57, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1013, 14, 58, 0, 0, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1014, 14, 59, 0, 0, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1015, 71, 21, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1016, 71, 22, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1017, 71, 23, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1018, 72, 21, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1019, 72, 22, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1020, 72, 23, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1021, 30, 16, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1022, 30, 17, 75, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1023, 30, 18, 125, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1024, 112, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1170, 102, 1, 67, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1171, 102, 1, 78, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1172, 102, 1, 79, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1173, 102, 1, 321, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1174, 102, 1, 723, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1175, 102, 2, 67, 2, 120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1176, 102, 2, 78, 2, 120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1177, 102, 2, 79, 2, 120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1178, 102, 2, 321, 2, 120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1179, 102, 2, 723, 2, 120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1180, 102, 3, 67, 2, 180);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1181, 102, 3, 78, 2, 180);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1182, 102, 3, 79, 2, 180);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1183, 102, 3, 321, 2, 180);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1184, 102, 3, 723, 2, 180);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1185, 102, 4, 67, 2, 240);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1186, 102, 4, 78, 2, 240);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1187, 102, 4, 79, 2, 240);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1188, 102, 4, 321, 2, 240);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1189, 102, 4, 723, 2, 240);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1190, 102, 5, 67, 2, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1191, 102, 5, 78, 2, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1192, 102, 5, 79, 2, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1193, 102, 5, 321, 2, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1194, 102, 5, 723, 2, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1195, 102, 1, 59, 6, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1196, 102, 1, 559, 6, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1197, 102, 1, 555, 6, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1198, 102, 1, 556, 6, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1199, 102, 1, 557, 6, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1200, 102, 1, 558, 6, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1201, 102, 2, 59, 6, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1202, 102, 2, 559, 6, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1203, 102, 2, 555, 6, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1204, 102, 2, 556, 6, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1205, 102, 2, 557, 6, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1206, 102, 2, 558, 6, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1207, 102, 3, 59, 6, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1208, 102, 3, 559, 6, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1209, 102, 3, 555, 6, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1210, 102, 3, 556, 6, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1211, 102, 3, 557, 6, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1212, 102, 3, 558, 6, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1213, 102, 1, 137, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1214, 102, 2, 137, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1215, 102, 3, 137, 2, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1216, 102, 4, 137, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1217, 102, 5, 137, 2, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1218, 102, 1, 13, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1219, 102, 2, 13, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1220, 102, 3, 13, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1221, 102, 1, 19, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1222, 102, 1, 733, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1223, 102, 1, 734, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1224, 102, 1, 735, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1225, 102, 1, 736, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1226, 102, 2, 19, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1227, 102, 2, 733, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1228, 102, 2, 734, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1229, 102, 2, 735, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1230, 102, 2, 736, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1231, 102, 3, 19, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1232, 102, 3, 733, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1233, 102, 3, 734, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1234, 102, 3, 735, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1235, 102, 3, 736, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1236, 102, 1, 20, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1237, 102, 2, 20, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1238, 102, 3, 20, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1239, 102, 1, 14, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1240, 102, 2, 14, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1241, 102, 3, 14, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1242, 102, 1, 57, 8, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1243, 102, 1, 355, 8, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1244, 102, 1, 356, 8, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1245, 102, 1, 357, 8, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1246, 102, 1, 358, 8, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1247, 102, 2, 57, 8, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1248, 102, 2, 355, 8, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1249, 102, 2, 356, 8, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1250, 102, 2, 357, 8, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1251, 102, 2, 358, 8, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1252, 102, 3, 57, 8, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1253, 102, 3, 355, 8, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1254, 102, 3, 356, 8, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1255, 102, 3, 357, 8, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1256, 102, 3, 358, 8, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1257, 102, 4, 57, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1258, 102, 4, 355, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1259, 102, 4, 356, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1260, 102, 4, 357, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1261, 102, 4, 358, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1262, 102, 5, 57, 8, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1263, 102, 5, 355, 8, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1264, 102, 5, 356, 8, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1266, 102, 5, 357, 8, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1267, 102, 5, 358, 8, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1268, 102, 1, 176, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1269, 102, 2, 176, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1270, 102, 3, 176, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1271, 102, 4, 176, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1272, 102, 5, 176, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1273, 102, 1, 176, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1274, 102, 2, 176, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1275, 102, 3, 176, 2, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1276, 102, 1, 177, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1277, 102, 2, 177, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1278, 102, 3, 177, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1279, 102, 4, 177, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1280, 102, 5, 177, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1281, 102, 1, 179, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1282, 102, 2, 179, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1283, 102, 3, 179, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1284, 102, 4, 179, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1285, 102, 5, 179, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1286, 102, 1, 1952, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1287, 102, 2, 1952, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1288, 102, 3, 1952, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1289, 102, 4, 1952, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1290, 102, 5, 1952, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1291, 102, 1, 178, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1292, 102, 2, 178, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1293, 102, 3, 178, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1294, 102, 4, 178, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1295, 102, 5, 178, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1296, 102, 1, 179, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1297, 102, 2, 179, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1298, 102, 3, 179, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1299, 102, 1, 382, 7, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1300, 102, 2, 382, 7, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1301, 102, 3, 382, 7, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1302, 102, 1, 377, 3, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1303, 102, 2, 377, 3, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1304, 102, 3, 377, 3, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1305, 102, 4, 377, 3, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1306, 102, 5, 377, 3, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1307, 102, 1, 373, 7, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1308, 102, 2, 373, 7, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1309, 102, 3, 373, 7, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1310, 104, 1, 20, 19, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1311, 104, 2, 20, 19, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1312, 104, 3, 20, 19, 9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1313, 102, 1, 394, 8, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1314, 102, 2, 394, 8, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1315, 102, 3, 394, 8, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1316, 102, 4, 394, 8, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1317, 102, 5, 394, 8, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1318, 102, 1, 393, 8, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1319, 102, 2, 393, 8, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1320, 102, 3, 393, 8, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1321, 102, 4, 393, 8, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1322, 102, 5, 393, 8, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1323, 104, 1, 15, 13, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1324, 104, 2, 15, 13, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1325, 104, 3, 15, 13, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1326, 104, 1, 19, 20, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1327, 104, 2, 19, 20, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1328, 104, 3, 19, 20, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1329, 104, 1, 13, 15, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1330, 104, 2, 13, 15, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1331, 104, 3, 13, 15, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1332, 122, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1333, 122, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1334, 102, 1, 314, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1335, 102, 2, 314, 2, 120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1336, 102, 3, 314, 2, 180);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1337, 102, 4, 314, 2, 240);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1338, 102, 5, 314, 2, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1380, 116, 1, 1825, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1381, 116, 2, 1826, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1382, 116, 3, 1827, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1383, 116, 4, 1828, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1384, 116, 5, 1829, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1385, 116, 11, 1830, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1386, 116, 12, 1831, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1387, 116, 13, 1832, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1388, 116, 14, 1833, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1389, 116, 15, 1834, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1390, 102, 1, 63, 4, 867);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1391, 102, 1, 323, 4, 867);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1392, 102, 1, 700, 4, 869);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1393, 102, 1, 701, 4, 1871);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1394, 102, 1, 706, 4, 868);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1395, 102, 1, 707, 4, 870);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1396, 102, 2, 63, 4, 868);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1397, 102, 2, 323, 4, 868);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1398, 102, 2, 700, 4, 870);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (525, 1, 5, 75, 12, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (526, 1, 6, 90, 13, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (527, 1, 7, 105, 14, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (528, 29, 4, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (529, 29, 5, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (683, 20, 101, 101, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (684, 20, 102, 102, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (685, 20, 103, 103, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (686, 20, 104, 104, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (687, 20, 105, 105, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (688, 20, 106, 106, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (689, 20, 107, 107, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (690, 20, 108, 108, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (691, 20, 109, 109, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (692, 20, 110, 110, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (693, 20, 111, 111, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (694, 20, 112, 112, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (695, 20, 113, 113, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (696, 20, 114, 114, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (697, 20, 115, 115, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (698, 20, 116, 116, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (699, 20, 117, 117, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (700, 20, 118, 118, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (701, 20, 119, 119, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (702, 20, 120, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (703, 20, 121, 121, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (704, 20, 122, 122, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (705, 20, 123, 123, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (706, 20, 124, 124, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (767, 10, 3, 50, 1, -25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (834, 100, 1, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (835, 100, 2, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (853, 20, 160, 160, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (854, 20, 161, 161, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (855, 20, 162, 162, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (856, 20, 163, 163, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (857, 20, 164, 164, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (858, 20, 165, 165, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (859, 20, 166, 166, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (860, 20, 167, 167, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (861, 20, 168, 168, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (862, 20, 169, 169, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (863, 20, 170, 170, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (864, 20, 171, 171, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (865, 20, 172, 172, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (866, 20, 173, 173, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1399, 102, 2, 701, 4, 1872);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1400, 102, 2, 706, 4, 869);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1401, 102, 2, 707, 4, 1871);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1402, 102, 1, 64, 4, 874);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1403, 102, 1, 708, 4, 875);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1404, 102, 1, 709, 4, 1873);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1405, 102, 1, 324, 4, 874);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1406, 102, 1, 710, 4, 872);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1407, 102, 1, 711, 4, 873);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1408, 102, 2, 64, 4, 872);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1409, 102, 2, 708, 4, 873);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1410, 102, 2, 709, 4, 1874);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1411, 102, 2, 324, 4, 872);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1412, 102, 2, 710, 4, 875);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1413, 102, 2, 711, 4, 1873);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1414, 102, 1, 69, 4, 1866);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1415, 102, 2, 69, 4, 1867);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1416, 102, 3, 69, 4, 1868);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1417, 102, 4, 69, 4, 1869);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1418, 102, 5, 69, 4, 1870);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1419, 118, 1, 300, 1196, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1420, 118, 2, 600, 1197, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1421, 118, 3, 1000, 1198, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1422, 118, 11, 300, 1204, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1423, 118, 12, 600, 1205, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1424, 118, 13, 1000, 1206, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1425, 120, 1, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1426, 120, 2, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1427, 120, 3, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1428, 119, 1, 500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1429, 119, 2, 1000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1430, 119, 3, 1500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1431, 121, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1432, 121, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1433, 121, 3, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1434, 121, 4, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1435, 121, 5, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1436, 102, 1, 178, 4, 1489);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1437, 102, 2, 178, 4, 1490);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1438, 102, 3, 178, 4, 1491);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1439, 102, 4, 178, 4, 1492);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1440, 102, 5, 178, 4, 1493);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1441, 4, 31, 0, 0, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1442, 4, 32, 0, 0, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1443, 4, 33, 0, 0, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1444, 102, 1, 175, 4, 1608);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1445, 102, 2, 175, 4, 1609);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1446, 102, 3, 175, 4, 1610);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1447, 3, 21, 0, 130, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1448, 101, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1449, 101, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1450, 101, 1, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1451, 101, 1, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1452, 101, 1, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1453, 101, 1, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1454, 101, 1, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1455, 101, 1, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1456, 101, 1, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1457, 101, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1458, 101, 1, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1459, 101, 1, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1460, 101, 1, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1461, 101, 1, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1462, 101, 1, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1463, 101, 1, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1464, 101, 1, 17, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1465, 101, 1, 18, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1466, 101, 1, 19, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1467, 101, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1468, 101, 1, 21, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1469, 101, 1, 22, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1470, 101, 1, 23, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1471, 101, 1, 24, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1472, 101, 1, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1473, 101, 1, 26, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1474, 101, 1, 27, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1475, 101, 1, 28, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1476, 101, 1, 29, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1477, 101, 1, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1478, 101, 1, 31, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1479, 101, 1, 32, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1480, 101, 1, 33, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1481, 101, 1, 34, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1482, 101, 1, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1483, 101, 1, 36, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1484, 101, 1, 37, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1485, 101, 1, 38, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1486, 101, 1, 39, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1487, 101, 1, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1488, 101, 1, 41, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1489, 101, 1, 42, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1490, 101, 1, 43, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1491, 101, 1, 44, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1492, 101, 1, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1493, 101, 1, 46, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1494, 101, 1, 47, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1495, 101, 1, 48, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1496, 101, 1, 49, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1497, 101, 1, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1498, 101, 1, 51, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1499, 101, 1, 52, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1500, 101, 1, 53, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1501, 101, 1, 54, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1502, 101, 1, 55, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1503, 101, 1, 56, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1504, 101, 1, 57, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1505, 101, 1, 58, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1506, 101, 1, 59, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1507, 101, 1, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1508, 101, 1, 61, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1509, 101, 1, 62, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1510, 101, 1, 63, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1511, 101, 1, 64, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1512, 101, 1, 65, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1513, 101, 1, 66, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1514, 101, 1, 67, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1515, 101, 1, 68, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1516, 101, 1, 69, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1517, 101, 1, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1518, 101, 1, 71, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1519, 101, 1, 72, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1520, 101, 1, 73, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1521, 101, 1, 74, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1522, 101, 1, 75, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1523, 101, 1, 76, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1524, 101, 1, 77, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1525, 101, 1, 78, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1526, 101, 1, 79, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1527, 101, 1, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1528, 101, 1, 81, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1529, 101, 1, 82, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1530, 101, 1, 83, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1531, 101, 1, 84, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1532, 101, 1, 85, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1533, 101, 1, 86, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1534, 101, 1, 87, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1535, 101, 1, 88, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1536, 101, 1, 89, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1537, 101, 1, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1538, 101, 1, 91, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1539, 101, 1, 92, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1540, 101, 1, 93, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1541, 101, 1, 94, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1542, 101, 1, 95, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1543, 101, 1, 96, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1544, 101, 1, 97, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1545, 101, 1, 98, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1546, 101, 1, 99, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1547, 101, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1548, 101, 1, 101, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1549, 101, 1, 102, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1550, 101, 1, 103, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1551, 101, 1, 104, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1552, 101, 1, 105, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1553, 101, 1, 106, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1554, 101, 1, 107, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1555, 101, 1, 108, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1556, 101, 1, 109, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1557, 101, 1, 110, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1558, 101, 1, 111, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1559, 101, 1, 112, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1560, 101, 1, 113, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1561, 101, 1, 114, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1562, 101, 1, 115, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1563, 101, 1, 116, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1564, 101, 1, 117, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1565, 101, 1, 118, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1566, 101, 1, 119, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1567, 101, 1, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1568, 101, 1, 121, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1569, 101, 1, 122, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1570, 101, 1, 123, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1571, 101, 1, 124, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1572, 101, 1, 125, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1573, 101, 1, 126, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1574, 101, 1, 127, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1575, 101, 1, 128, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1576, 101, 1, 129, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1577, 101, 1, 130, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1578, 101, 1, 131, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1579, 101, 1, 132, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1580, 101, 1, 133, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1581, 101, 1, 134, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1582, 101, 1, 135, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1583, 101, 1, 136, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1584, 101, 1, 137, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1585, 101, 1, 138, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1586, 101, 1, 139, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1587, 101, 1, 140, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1588, 101, 1, 141, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1589, 101, 1, 142, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1590, 101, 1, 143, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1591, 101, 1, 144, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1592, 101, 1, 145, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1593, 101, 1, 146, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1594, 101, 1, 147, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1595, 101, 1, 148, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1596, 101, 1, 149, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1597, 101, 1, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1598, 101, 1, 151, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1599, 101, 1, 152, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1600, 101, 1, 153, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1601, 101, 1, 154, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1602, 101, 1, 155, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1603, 101, 1, 156, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1604, 101, 1, 157, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1605, 101, 1, 158, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1606, 101, 1, 159, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1607, 101, 1, 160, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1608, 101, 1, 161, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1609, 101, 1, 162, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (591, 20, 60, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (592, 20, 61, 61, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (593, 20, 62, 62, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (594, 20, 63, 63, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (595, 20, 64, 64, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (596, 20, 65, 65, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (597, 42, 2, 46, 18, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (598, 42, 3, 46, 18, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (599, 42, 4, 46, 18, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (600, 42, 5, 46, 18, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (602, 42, 51, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (679, 71, 11, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (680, 72, 11, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (725, 98, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (808, 15, 51, 50, 50, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (809, 15, 52, 55, 55, 55);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (905, 105, 1, 1695, 1402, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (919, 71, 15, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (920, 72, 15, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1025, 102, 1, 120, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1026, 102, 2, 120, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1027, 102, 3, 120, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1028, 103, 1, 4, 13, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1029, 103, 2, 4, 13, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1031, 103, 3, 4, 13, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1032, 103, 4, 4, 13, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1033, 103, 5, 4, 13, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1034, 102, 1, 120, 3, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1035, 102, 2, 120, 3, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1036, 102, 3, 120, 3, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1037, 102, 4, 120, 3, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1038, 102, 5, 120, 3, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1039, 102, 1, 122, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1040, 102, 2, 122, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1041, 102, 3, 122, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1042, 103, 1, 1, 14, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1043, 103, 2, 1, 14, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1044, 103, 3, 1, 14, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1045, 103, 4, 1, 14, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1046, 103, 5, 1, 14, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1047, 102, 1, 125, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1048, 102, 2, 125, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1049, 102, 3, 125, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1050, 103, 1, 23, 19, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1051, 103, 2, 23, 19, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1052, 103, 3, 23, 19, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1053, 103, 4, 23, 19, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1054, 103, 5, 23, 19, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1055, 102, 1, 120, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1056, 102, 2, 120, 10, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1057, 102, 3, 120, 10, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1058, 102, 4, 120, 10, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1059, 102, 5, 120, 10, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1060, 102, 1, 121, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1061, 102, 2, 121, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1062, 102, 3, 121, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1063, 102, 4, 121, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1064, 102, 5, 121, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1065, 103, 1, 14, 1, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1066, 103, 2, 14, 1, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1067, 103, 3, 14, 1, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1068, 103, 4, 14, 1, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1069, 103, 5, 14, 1, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1070, 100, 21, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1071, 100, 22, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1072, 100, 23, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1073, 103, 1, 13, 4, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1074, 103, 2, 13, 4, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1075, 103, 3, 13, 4, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1076, 103, 4, 13, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1077, 103, 5, 13, 4, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1078, 102, 1, 314, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1079, 102, 2, 314, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1080, 102, 3, 314, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1081, 102, 4, 314, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1082, 102, 5, 314, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1083, 15, 55, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1084, 15, 56, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1085, 15, 57, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1086, 15, 58, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1087, 15, 59, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1088, 17, 55, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1089, 17, 56, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1090, 17, 57, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1091, 17, 58, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1092, 17, 59, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1093, 103, 1, 19, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1094, 103, 1, 19, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1095, 103, 1, 19, 3, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1096, 103, 2, 19, 1, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1097, 103, 2, 19, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1098, 103, 2, 19, 3, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1099, 103, 3, 19, 1, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1100, 103, 3, 19, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1101, 103, 3, 19, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1102, 103, 4, 19, 1, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1103, 103, 4, 19, 2, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1104, 103, 4, 19, 3, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1105, 103, 5, 19, 1, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1106, 103, 5, 19, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1107, 103, 5, 19, 3, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1108, 102, 1, 126, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1109, 102, 2, 126, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1110, 102, 3, 126, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1111, 102, 1, 126, 3, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1112, 102, 2, 126, 3, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1113, 102, 3, 126, 3, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1114, 102, 4, 126, 3, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1115, 102, 5, 126, 3, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1116, 102, 1, 128, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1117, 102, 2, 128, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1118, 102, 3, 128, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1119, 102, 1, 128, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1120, 102, 2, 128, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1121, 102, 3, 128, 2, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1122, 102, 4, 128, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1123, 102, 5, 128, 2, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1124, 102, 1, 140, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1125, 102, 2, 140, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1126, 102, 3, 140, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1127, 102, 1, 140, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1128, 102, 2, 140, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1129, 102, 3, 140, 2, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1130, 102, 4, 140, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1131, 102, 5, 140, 2, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1132, 102, 1, 127, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1133, 102, 2, 127, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1134, 102, 3, 127, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1135, 102, 4, 127, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1136, 102, 5, 127, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1137, 102, 1, 127, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1138, 102, 2, 127, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1139, 102, 3, 127, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1140, 102, 1, 18, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1141, 102, 1, 740, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1142, 102, 1, 739, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1143, 102, 1, 738, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1144, 102, 1, 737, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1145, 102, 1, 179, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1146, 102, 2, 18, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1147, 102, 2, 740, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1148, 102, 2, 739, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1149, 102, 2, 738, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1150, 102, 2, 737, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1151, 102, 2, 179, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1152, 102, 3, 18, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1153, 102, 3, 740, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1154, 102, 3, 739, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1155, 102, 3, 738, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1156, 102, 3, 737, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1157, 102, 3, 179, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1158, 102, 4, 18, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1159, 102, 4, 740, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1160, 102, 4, 739, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1161, 102, 4, 738, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1162, 102, 4, 737, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1163, 102, 4, 179, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1164, 102, 5, 18, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1165, 102, 5, 740, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1166, 102, 5, 739, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1167, 102, 5, 738, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1168, 102, 5, 737, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1169, 102, 5, 179, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1610, 101, 1, 163, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1611, 101, 1, 164, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1612, 101, 1, 165, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1613, 101, 1, 166, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1672, 102, 1, 127, 4, 1861);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1673, 102, 2, 127, 4, 1862);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1674, 102, 3, 127, 4, 1863);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1675, 102, 4, 127, 4, 1864);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1676, 102, 5, 127, 4, 1865);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1677, 10, 36, 0, 1, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1678, 10, 37, 0, 1, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1679, 10, 38, 0, 1, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1680, 66, 11, 4, 1844, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1681, 66, 12, 4, 1845, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1682, 66, 13, 4, 1846, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1683, 66, 14, 4, 1847, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1684, 66, 21, 4, 1848, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1685, 66, 22, 4, 1849, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1686, 66, 23, 4, 1850, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1687, 66, 24, 4, 1851, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1688, 66, 25, 4, 1852, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1689, 66, 31, 4, 1853, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1690, 66, 32, 4, 1854, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1691, 66, 33, 4, 1855, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1692, 66, 34, 4, 1856, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1693, 66, 35, 4, 1857, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1694, 66, 36, 4, 1858, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1695, 66, 37, 4, 1859, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1696, 66, 38, 4, 1860, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1697, 66, 39, 4, 1861, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1698, 66, 40, 4, 1862, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1699, 16, 61, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1700, 16, 62, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1701, 16, 63, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1702, 16, 64, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1703, 16, 65, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1705, 10, 39, 0, 1, -25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1706, 10, 40, 0, 1, -35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1707, 10, 41, 0, 1, -50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1709, 125, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1710, 125, 2, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1711, 125, 3, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1712, 125, 4, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1713, 125, 5, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1714, 117, 1, 1818, 233, 244);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1727, 127, 1, 30000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1736, 97, 1, 1809, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1737, 97, 1, 1871, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1745, 15, 5, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1746, 17, 5, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1747, 17, 6, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1748, 102, 1, 125, 4, 1854);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1749, 102, 2, 125, 4, 1855);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1750, 102, 3, 125, 4, 1856);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1751, 102, 4, 125, 4, 1857);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1752, 15, 6, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1753, 15, 7, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1754, 17, 6, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1755, 17, 7, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1766, 20, 193, 193, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1767, 20, 194, 194, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1768, 20, 195, 195, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1769, 20, 196, 196, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1770, 20, 197, 197, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1771, 20, 198, 198, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1785, 135, 1, 1, 100, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1786, 135, 2, 1, 200, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1787, 135, 3, 1, 300, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1788, 135, 1, 2, 200, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1789, 135, 2, 2, 300, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1790, 135, 3, 2, 400, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1791, 135, 1, 3, 50, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1792, 135, 2, 3, 100, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1793, 135, 3, 3, 150, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1794, 136, 1, 46, 18, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1795, 136, 2, 46, 18, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1796, 136, 3, 46, 18, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1784, 130, 1, 2, 3, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1797, 17, 5, -5, -5, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1798, 15, 5, -5, -5, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1799, 134, 1, 1906, 1907, 1908);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1800, 134, 2, 1909, 1910, 1911);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1801, 134, 3, 1912, 1913, 1914);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1802, 134, 4, 1915, 1916, 1917);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1813, 132, 1, 1922, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1838, 143, 1, 3000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1839, 143, 2, 5000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1840, 143, 3, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1820, 52, 5, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1821, 101, 1, 701, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1822, 101, 1, 702, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1823, 101, 1, 703, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1824, 101, 1, 704, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1825, 101, 1, 705, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (601, 62, 2, 0, 70, -65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (603, 75, 3, 726, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (737, 20, 93, 93, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (738, 20, 94, 94, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (879, 12, 7, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (904, 20, 191, 191, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1803, 3, 1, 0, -150, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1804, 75, 10, 1925, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1826, 101, 1, 706, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1827, 101, 1, 707, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1828, 101, 1, 708, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1841, 20, 208, 208, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1842, 20, 209, 209, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1843, 20, 210, 210, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1844, 20, 211, 211, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1845, 20, 212, 212, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1846, 20, 213, 213, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1847, 20, 214, 214, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1848, 20, 215, 215, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1849, 20, 216, 216, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1850, 20, 217, 217, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1851, 20, 218, 218, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1852, 20, 219, 219, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1853, 20, 220, 220, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1854, 20, 221, 221, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1855, 20, 222, 222, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1856, 20, 223, 223, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1857, 20, 224, 224, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1858, 20, 225, 225, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1859, 20, 226, 226, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1860, 20, 227, 227, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1861, 20, 228, 228, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1862, 20, 229, 229, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1863, 20, 230, 230, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1884, 17, 21, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1885, 17, 22, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1886, 17, 23, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1887, 15, 45, 40, 40, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1909, 20, 248, 248, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1910, 20, 249, 249, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1911, 20, 250, 250, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1912, 20, 251, 251, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1913, 20, 252, 252, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1914, 20, 253, 253, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1915, 20, 254, 254, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1916, 20, 255, 255, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1931, 20, 256, 256, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1932, 20, 257, 257, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1933, 20, 258, 258, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1934, 15, 49, 99, 99, 99);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1941, 142, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1942, 142, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1943, 142, 3, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1944, 142, 4, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1945, 142, 5, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1946, 142, 6, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1949, 28, 4, 4, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1956, 144, 1, 43, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1957, 144, 2, 44, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1958, 144, 3, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1959, 144, 4, 46, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1960, 144, 5, 47, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1961, 144, 6, 48, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1962, 144, 7, 49, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1963, 144, 8, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1964, 145, 1, 57, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1965, 145, 2, 58, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1966, 145, 3, 59, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1967, 145, 4, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1968, 145, 5, 61, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1969, 145, 6, 62, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1975, 146, 1, 87, -1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1979, 20, 265, 265, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1980, 20, 266, 266, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1981, 20, 268, 268, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1982, 20, 269, 269, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1983, 20, 270, 270, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1984, 20, 267, 267, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1985, 101, 1, 714, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1986, 101, 1, 715, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1987, 101, 1, 716, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1988, 101, 1, 717, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1989, 101, 1, 718, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1990, 101, 1, 719, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1991, 101, 1, 720, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2003, 4, 59, 0, 0, -65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2004, 4, 60, 0, 0, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2005, 4, 61, 0, 0, -55);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2006, 4, 62, 0, 0, -50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2007, 4, 63, 0, 0, -40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2009, 10, 42, 0, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2010, 10, 43, 0, 1, -6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2011, 10, 44, 0, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2012, 10, 45, 0, 1, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2013, 10, 46, 0, 1, -15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2089, 14, 33, 8, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2090, 14, 33, 11, 11, 11);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2091, 14, 33, 15, 15, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2092, 14, 33, 38, 38, 38);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2093, 14, 33, 42, 42, 42);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2094, 14, 33, 47, 47, 47);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2095, 16, 33, 15, 15, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2096, 16, 33, 23, 23, 23);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2097, 16, 33, 30, 30, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2098, 16, 33, 68, 68, 68);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2099, 16, 33, 75, 75, 75);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2100, 16, 33, 83, 83, 83);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2101, 15, 33, 15, 15, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2102, 15, 33, 23, 23, 23);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2103, 15, 33, 30, 30, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2104, 15, 33, 75, 75, 75);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2105, 15, 33, 68, 68, 68);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2106, 15, 33, 81, 81, 81);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2107, 17, 33, 15, 15, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2108, 17, 33, 23, 23, 23);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2109, 17, 33, 30, 30, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2110, 17, 33, 68, 68, 68);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2111, 17, 33, 75, 75, 75);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2112, 17, 33, 81, 81, 81);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2113, 4, 33, 0, 0, 23);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2114, 4, 33, 0, 0, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2115, 4, 33, 0, 0, 38);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2116, 4, 33, 0, 0, 90);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2117, 4, 33, 0, 0, 98);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2118, 4, 33, 0, 0, 105);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2119, 3, 33, 0, 23, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2120, 3, 33, 0, 45, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2121, 3, 33, 0, 68, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2122, 3, 33, 0, 173, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2123, 3, 33, 0, 188, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2131, 72, 33, 375, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2132, 72, 33, 600, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2133, 72, 33, 775, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2134, 72, 33, 1950, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2135, 72, 33, 2175, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2137, 77, 33, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2138, 77, 33, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2124, 3, 33, 0, 210, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2125, 71, 33, 750, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2126, 71, 33, 1200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2127, 71, 33, 1650, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2128, 71, 33, 3900, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2129, 71, 33, 4350, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2130, 71, 33, 4800, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2136, 72, 33, 2400, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2139, 27, 33, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2140, 27, 33, 42, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2141, 28, 33, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2142, 28, 33, 21, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2143, 100, 33, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2144, 100, 33, 18, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2145, 15, 33, 83, 83, 83);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2171, 20, 278, 278, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2172, 59, 4, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2173, 59, 5, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2174, 59, 6, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2175, 59, 7, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2176, 101, 1, 805, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2177, 101, 1, 806, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2178, 20, 279, 279, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2179, 20, 280, 280, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2180, 29, 9, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2181, 144, 9, 29, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2182, 144, 10, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2212, 102, 1, 719, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2213, 102, 1, 720, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2214, 102, 1, 326, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2215, 102, 1, 721, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2216, 102, 1, 722, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2217, 102, 2, 65, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2218, 102, 2, 719, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2219, 102, 2, 720, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2220, 102, 2, 326, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2221, 102, 2, 721, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2222, 102, 2, 722, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2223, 102, 3, 65, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2224, 102, 3, 719, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2225, 102, 3, 720, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2226, 102, 3, 326, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2227, 102, 3, 721, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2228, 102, 3, 722, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (660, 10, 16, 0, 1, -80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (661, 10, 18, 0, 1, -100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (662, 10, 20, 0, 1, -120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (663, 10, 17, 0, 7, -110);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (664, 10, 19, 0, 7, -130);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (665, 10, 21, 0, 7, -150);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (666, 79, 4, 764, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (667, 79, 5, 765, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (668, 79, 6, 766, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (669, 78, 4, 605, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (670, 78, 5, 607, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (671, 78, 6, 609, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (739, 20, 125, 125, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (769, 20, 134, 134, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (770, 20, 135, 135, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (771, 20, 136, 136, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (772, 20, 137, 137, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (773, 20, 138, 138, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (774, 20, 139, 139, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (775, 20, 140, 140, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (776, 20, 141, 141, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (777, 29, 6, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (778, 29, 7, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (784, 16, 8, 8, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1641, 126, 1, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1642, 126, 2, 240, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1643, 126, 3, 360, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1644, 126, 4, 480, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1645, 126, 5, 600, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1646, 10, 30, 0, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1647, 10, 31, 0, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1648, 10, 32, 0, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1649, 10, 33, 0, 1, -6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1650, 10, 34, 0, 1, -8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1651, 10, 35, 0, 1, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1652, 113, 1, 300, 3, 1411);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1653, 113, 2, 300, 6, 1411);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1654, 113, 3, 300, 9, 1411);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1655, 113, 4, 300, 12, 1411);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1656, 113, 5, 300, 15, 1411);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1657, 76, 11, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1658, 76, 12, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1659, 76, 13, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1660, 76, 14, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1661, 76, 15, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1662, 102, 1, 127, 12, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1663, 102, 2, 127, 12, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1664, 102, 3, 127, 12, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1665, 102, 4, 127, 12, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1666, 102, 5, 127, 12, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1667, 4, 41, 0, 0, -155);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1668, 4, 42, 0, 0, -160);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1669, 4, 43, 0, 0, -165);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1670, 4, 44, 0, 0, -170);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1671, 4, 45, 0, 0, -175);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1715, 76, 21, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1716, 76, 22, 75, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1717, 76, 23, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1718, 76, 24, 125, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1719, 76, 25, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1720, 4, 51, 0, 0, -50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1721, 4, 52, 0, 0, -100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1722, 4, 53, 0, 0, -150);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1723, 4, 54, 0, 0, -200);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1724, 4, 55, 0, 0, -250);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1734, 97, 1, 1869, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1735, 97, 1, 1870, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1865, 78, 7, 1801, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1866, 78, 8, 1783, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1867, 78, 9, 1784, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1868, 78, 10, 1785, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1869, 78, 11, 1786, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1870, 78, 12, 1787, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1871, 78, 13, 1788, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1872, 78, 14, 1789, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1873, 78, 15, 1794, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1874, 78, 16, 1795, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1875, 78, 17, 1796, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1876, 78, 18, 1797, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1877, 78, 19, 1798, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1878, 78, 20, 1799, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1879, 78, 21, 1802, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1880, 138, 1, 30, 10, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1881, 139, 1, 1801, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1882, 140, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1883, 141, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1892, 20, 231, 231, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1893, 20, 232, 232, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1894, 20, 233, 233, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1895, 20, 234, 234, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1896, 20, 235, 235, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1897, 20, 236, 236, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1898, 20, 237, 237, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1899, 20, 238, 238, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1900, 20, 239, 239, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1901, 20, 240, 240, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1902, 20, 241, 241, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1903, 20, 242, 242, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1904, 20, 243, 243, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1905, 20, 244, 244, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1906, 20, 245, 245, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1907, 20, 246, 246, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1908, 20, 247, 247, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1917, 52, 6, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1918, 52, 7, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1919, 52, 8, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1920, 52, 9, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1921, 52, 10, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1978, 20, 264, 264, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1995, 150, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1996, 150, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1997, 150, 3, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1998, 4, 56, 0, 0, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1999, 4, 57, 0, 0, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2000, 4, 58, 0, 0, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2150, 20, 275, 275, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2210, 15, 70, 0, 15, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2211, 102, 1, 65, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2308, 102, 99, 18, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2309, 102, 98, 57, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2407, 102, 11, 394, 8, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2408, 102, 12, 394, 8, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2409, 102, 13, 394, 8, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2410, 102, 1, 394, 24, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2411, 102, 2, 394, 24, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2412, 3, 1, 0, -20, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2413, 3, 2, 0, -40, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2414, 3, 3, 0, -60, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2960, 66, 46, 6, 2322, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2961, 66, 47, 6, 2323, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2962, 66, 48, 7, 2320, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2418, 102, 1, 393, 24, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2419, 102, 2, 393, 24, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2420, 102, 1, 392, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2421, 102, 1, 392, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2422, 102, 2, 392, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2423, 102, 3, 392, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2424, 102, 1, 392, 24, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2425, 102, 2, 392, 24, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2426, 102, 1, 1375, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2427, 102, 2, 1375, 2, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2428, 102, 3, 1375, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2429, 16, 70, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2430, 17, 7, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2431, 102, 1, 1375, 4, 2731);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2894, 20, 289, 289, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2433, 14, 70, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2434, 14, 70, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2435, 15, 70, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2436, 15, 70, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2437, 102, 1, 1685, 4, 2525);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2438, 102, 1, 1686, 4, 2525);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2439, 102, 1, 1687, 4, 2525);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2440, 102, 1, 1688, 4, 2525);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2441, 102, 1, 1689, 4, 2525);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2442, 102, 2, 1685, 4, 2526);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2443, 102, 2, 1686, 4, 2526);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2444, 102, 2, 1687, 4, 2526);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2445, 102, 2, 1688, 4, 2526);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2446, 102, 2, 1689, 4, 2526);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2447, 102, 1, 1690, 4, 2527);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2448, 102, 1, 1691, 4, 2527);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2449, 102, 1, 1692, 4, 2527);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2450, 102, 1, 1693, 4, 2527);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2451, 102, 1, 1694, 4, 2527);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2452, 102, 2, 1691, 4, 2528);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2453, 102, 2, 1690, 4, 2528);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2454, 102, 2, 1692, 4, 2528);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2455, 102, 2, 1693, 4, 2528);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2456, 102, 2, 1694, 4, 2528);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2457, 3, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2458, 3, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2459, 3, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2460, 3, 4, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2461, 3, 5, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2462, 66, 41, 4, 2165, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2463, 66, 42, 4, 2166, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2464, 66, 43, 4, 2167, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2465, 66, 44, 4, 2168, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2466, 66, 45, 4, 2169, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2467, 102, 1, 1391, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2468, 102, 1, 1392, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2469, 102, 1, 1393, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2470, 102, 1, 1394, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2471, 102, 1, 1396, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2472, 102, 1, 1397, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2473, 102, 1, 1398, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2474, 102, 1, 1399, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2475, 102, 1, 1400, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2476, 102, 1, 391, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2477, 102, 1, 525, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2478, 102, 1, 390, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2479, 102, 1, 1376, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2480, 102, 1, 1377, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2481, 102, 1, 1378, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2483, 102, 1, 1395, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2484, 102, 4, 121, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2895, 20, 290, 290, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2508, 101, 1, 952, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2509, 101, 1, 953, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2510, 101, 1, 954, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2511, 101, 1, 955, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2512, 101, 1, 956, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2513, 102, 7, 18, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2514, 102, 7, 737, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2515, 102, 7, 738, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2516, 102, 7, 739, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2517, 102, 7, 740, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2518, 102, 8, 18, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2519, 102, 8, 737, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2520, 102, 8, 738, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2521, 102, 8, 739, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2522, 102, 8, 740, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2523, 102, 9, 18, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2524, 102, 9, 737, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2525, 102, 9, 738, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2526, 102, 9, 739, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2527, 102, 9, 740, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2528, 102, 10, 18, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2529, 102, 10, 737, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2530, 102, 10, 738, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2531, 102, 10, 739, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2532, 102, 10, 740, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2533, 102, 11, 18, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2534, 102, 11, 737, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2535, 102, 11, 738, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2536, 102, 11, 739, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2537, 102, 11, 740, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2538, 102, 12, 14, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2539, 102, 12, 177, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2540, 102, 13, 14, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2541, 102, 13, 177, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2542, 102, 14, 14, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2543, 102, 14, 177, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2544, 102, 15, 14, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2545, 102, 15, 177, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2546, 102, 16, 14, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2547, 102, 16, 177, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2548, 102, 17, 179, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2549, 102, 17, 127, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2550, 102, 17, 394, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2963, 66, 49, 7, 2321, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2964, 66, 50, 8, 2326, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2965, 66, 51, 8, 2327, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2966, 66, 52, 9, 2324, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2967, 66, 53, 9, 2325, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2968, 66, 54, 10, 2328, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2969, 66, 55, 10, 2329, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2970, 66, 56, 11, 2330, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2971, 66, 57, 11, 2331, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2972, 66, 58, 12, 2332, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2973, 66, 59, 12, 2333, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2974, 66, 60, 13, 2334, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (762, 16, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (768, 3, 4, 0, -25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (779, 20, 142, 142, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (780, 20, 143, 143, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (781, 20, 144, 144, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (782, 20, 145, 145, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (788, 15, 9, 9, 9, 9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (789, 15, 11, 11, 11, 11);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (790, 16, 14, 14, 14, 14);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (791, 16, 16, 16, 16, 16);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (792, 16, 22, 22, 22, 22);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (796, 41, 6, 6, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (797, 41, 6, 6, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (822, 77, 4, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (823, 77, 5, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (880, 16, 33, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (881, 14, 38, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (882, 15, 53, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (883, 17, 51, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (884, 52, 12, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (885, 30, 10, 1000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (886, 5, 9, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (887, 6, 9, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (888, 7, 9, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (890, 20, 181, 181, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (891, 20, 182, 182, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1342, 102, 1, 318, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1343, 102, 2, 318, 10, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1344, 102, 3, 318, 10, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1345, 102, 4, 318, 10, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1346, 102, 5, 318, 10, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1347, 102, 1, 383, 7, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1348, 102, 2, 383, 7, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1349, 102, 3, 383, 7, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1350, 123, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1351, 123, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1352, 123, 3, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1353, 123, 4, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1354, 123, 5, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1355, 124, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1356, 102, 3, 122, 4, 1860);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1357, 102, 1, 122, 4, 1858);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1358, 102, 2, 122, 4, 1859);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1359, 102, 1, 125, 4, 1494);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1360, 102, 1, 125, 4, 1499);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1361, 102, 1, 125, 4, 1504);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1362, 102, 2, 125, 4, 1495);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1363, 102, 2, 125, 4, 1500);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3631, 101, 1, 1234, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3632, 101, 1, 1235, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3633, 101, 1, 1236, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3634, 101, 1, 1237, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3635, 101, 1, 1238, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3636, 101, 1, 1239, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3637, 101, 1, 1240, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3638, 101, 1, 1241, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3639, 101, 1, 1242, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3640, 101, 1, 1243, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3641, 101, 1, 1244, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3642, 101, 1, 1245, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3643, 20, 199, 199, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3644, 20, 200, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3645, 20, 201, 201, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3646, 20, 202, 202, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3647, 20, 204, 204, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3648, 20, 205, 205, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3649, 20, 206, 206, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3650, 20, 265, 265, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3651, 20, 270, 270, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3652, 20, 286, 286, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3653, 20, 287, 287, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3654, 20, 288, 288, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3655, 20, 334, 334, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3656, 20, 335, 335, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3657, 14, 1, 0, 0, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3658, 15, 51, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3659, 185, 1, 50, 3495, 3496);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3660, 185, 1, 50, 3498, 3499);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3661, 185, 1, 50, 3501, 3502);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3662, 185, 1, 50, 3504, 3505);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3663, 185, 1, 50, 3507, 3508);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3664, 185, 1, 50, 3510, 3511);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3665, 185, 1, 50, 3513, 3514);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3666, 185, 1, 50, 3516, 3517);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3667, 185, 1, 50, 3519, 3520);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3668, 185, 1, 50, 3522, 3523);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3669, 101, 1, 1246, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3670, 71, 1, 110, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3694, 111, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3695, 102, 1, 127, 9, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3696, 102, 2, 2622, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3697, 102, 1, 179, 9, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3698, 102, 2, 2636, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3699, 102, 1, 18, 9, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3700, 75, 1, 3575, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3701, 75, 1, 3577, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3702, 109, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3703, 102, 2, 737, 9, 55);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3704, 102, 3, 738, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3705, 102, 4, 739, 9, 45);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3706, 75, 1, 3581, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3707, 75, 1, 3583, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3708, 110, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3709, 102, 5, 740, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3779, 15, 51, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3780, 27, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3712, 75, 1, 3587, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3713, 75, 1, 3589, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3714, 17, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3715, 15, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3716, 58, 1, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3717, 16, 1, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3718, 17, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3719, 15, 2, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3720, 16, 2, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3721, 58, 2, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3722, 187, 1, 500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3723, 186, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3724, 17, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3725, 15, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3726, 16, 3, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3727, 58, 3, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3728, 17, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3729, 15, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3730, 16, 4, 8, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3731, 58, 4, 8, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3732, 17, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3733, 15, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3734, 16, 5, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3735, 58, 5, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3771, 27, 1, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3772, 27, 23, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3781, 27, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3782, 27, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3783, 27, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3784, 27, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3785, 28, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3786, 28, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3787, 28, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3788, 28, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3789, 27, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3790, 27, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3791, 27, 4, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3792, 28, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3793, 28, 3, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3794, 28, 4, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3795, 27, 1, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3796, 27, 2, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3797, 20, 1, 400, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3798, 20, 1, 401, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3801, 20, 97, 97, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3802, 20, 98, 98, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3803, 1, 2, 60, 13, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3804, 1, 3, 90, 11, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3805, 189, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3806, 16, 50, 50, 50, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3807, 28, 50, 400, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3808, 28, 50, 400, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3809, 158, 1, 1, 1700, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3810, 158, 1, 2, 1700, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3811, 158, 1, 3, 1700, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3812, 158, 1, 4, 1700, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3813, 78, 1, 2577, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3814, 20, 1, 402, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3815, 20, 1, 403, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3816, 101, 1, 1323, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3817, 101, 1, 1324, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3818, 20, 1, 404, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3819, 20, 1, 405, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3822, 192, 1, 3737, 18, 18);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3823, 192, 2, 3738, 18, 18);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3824, 192, 3, 3739, 18, 18);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3825, 192, 1, 3740, 12, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3826, 192, 2, 3741, 12, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3827, 192, 3, 3742, 12, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3828, 192, 1, 3743, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3829, 192, 2, 3744, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3830, 192, 3, 3745, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3831, 192, 1, 3746, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3832, 192, 2, 3747, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3833, 192, 3, 3748, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3834, 192, 1, 3749, 14, 14);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3835, 192, 2, 3750, 14, 14);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3836, 192, 3, 3751, 14, 14);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3837, 192, 1, 3752, 16, 16);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3838, 192, 2, 3753, 16, 16);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3839, 192, 3, 3754, 16, 16);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3840, 192, 1, 3755, 17, 18);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3841, 192, 2, 3756, 17, 18);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3842, 192, 3, 3757, 17, 18);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3843, 192, 1, 3770, 11, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3844, 192, 2, 3771, 11, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3845, 192, 3, 3772, 11, 12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3846, 192, 1, 3761, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3847, 192, 2, 3762, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3848, 192, 3, 3763, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3849, 192, 1, 3758, 7, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3850, 192, 2, 3759, 7, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3851, 192, 3, 3760, 7, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3852, 192, 1, 3767, 13, 14);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3853, 192, 2, 3768, 13, 14);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3854, 192, 3, 3769, 13, 14);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3855, 192, 1, 3764, 15, 16);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3856, 192, 2, 3765, 15, 16);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3857, 192, 3, 3766, 15, 16);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3858, 71, 1, -50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3859, 71, 2, -100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3860, 71, 3, -150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3861, 71, 1, -100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3862, 71, 2, -300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3863, 71, 3, -500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3864, 10, 1, 0, 0, -20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3865, 10, 2, 0, 0, -40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3866, 10, 3, 0, 0, -80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3867, 10, 1, 0, 0, -100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3868, 10, 2, 0, 0, -200);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3869, 10, 3, 0, 0, -300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3870, 27, 1, -1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3871, 27, 2, -2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3872, 27, 3, -3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3873, 27, 1, -4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3874, 27, 2, -8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3875, 27, 3, -12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3876, 72, 1, -50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3877, 72, 2, -100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3878, 72, 3, -150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3879, 72, 1, -100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3880, 72, 2, -200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3881, 72, 3, -300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3882, 29, 1, -5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3883, 29, 2, -10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3884, 29, 3, -15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3885, 29, 1, -10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3886, 29, 2, -20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3887, 29, 3, -30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3888, 28, 1, -1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3889, 28, 2, -2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3890, 28, 3, -3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3891, 28, 1, -3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3892, 28, 2, -6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3893, 28, 3, -9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3894, 30, 1, -100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3895, 30, 2, -250, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3896, 30, 3, -400, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3897, 30, 1, -500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3898, 30, 2, -700, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3899, 30, 3, -900, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3900, 126, 1, -180, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3901, 126, 2, -300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3902, 126, 3, -600, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3903, 126, 1, -360, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3904, 126, 2, -600, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3905, 126, 3, -1200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3906, 102, 1, 3140, 24, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3907, 102, 2, 3140, 24, -80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3908, 102, 3, 3140, 24, -95);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3909, 102, 1, 3141, 24, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3910, 102, 2, 3141, 24, -80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3911, 102, 3, 3141, 24, -95);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3912, 102, 1, 3142, 24, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3913, 102, 2, 3142, 24, -80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3914, 102, 3, 3142, 24, -95);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3915, 102, 1, 3139, 24, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3916, 102, 2, 3139, 24, -80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3917, 102, 3, 3139, 24, -95);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3918, 102, 1, 3144, 24, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3919, 102, 2, 3144, 24, -80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3920, 102, 3, 3144, 24, -95);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3921, 102, 1, 3143, 24, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3922, 102, 2, 3143, 24, -80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3923, 102, 3, 3143, 24, -95);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3924, 27, 1, 15, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3925, 28, 1, 10, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3926, 126, 1, 1500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3927, 27, 1, 13, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3928, 28, 1, 8, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3929, 126, 1, 1320, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3930, 28, 1, 7, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3935, 20, 1, 406, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3936, 20, 1, 407, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3937, 20, 1, 408, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3938, 20, 1, 409, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3939, 20, 1, 410, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3940, 20, 1, 411, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3941, 20, 1, 412, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3942, 20, 1, 413, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3943, 20, 1, 414, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3944, 20, 1, 415, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3945, 20, 1, 416, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3671, 71, 1, 130, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3672, 72, 1, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3673, 72, 1, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3674, 71, 1, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3675, 71, 1, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3676, 171, 1, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3677, 173, 1, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3678, 172, 1, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3679, 72, 1, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3680, 72, 1, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3681, 27, 1, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3682, 27, 2, 4, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3683, 28, 1, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3684, 28, 2, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3685, 30, 1, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3686, 30, 2, 230, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3687, 126, 1, 300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3688, 27, 1, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3689, 28, 1, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3690, 71, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3691, 72, 1, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3692, 30, 1, 180, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3693, 126, 1, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3770, 188, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3736, 20, 372, 372, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3737, 27, 1, 5, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3738, 28, 1, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3739, 126, 1, 600, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3740, 20, 373, 373, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3741, 27, 1, 10, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3742, 20, 374, 374, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3743, 20, 375, 375, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3744, 20, 376, 376, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3745, 20, 377, 377, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3746, 28, 1, 5, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3747, 20, 378, 378, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3748, 20, 379, 379, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3749, 20, 380, 380, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3750, 20, 381, 381, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3751, 20, 382, 382, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3752, 20, 383, 383, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3753, 126, 1, 900, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3754, 20, 384, 384, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3755, 20, 385, 385, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3756, 27, 1, 7, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3757, 28, 1, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3758, 20, 386, 386, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3759, 20, 387, 387, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3760, 20, 388, 388, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3761, 20, 389, 389, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3762, 126, 1, 720, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3763, 20, 390, 390, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3764, 20, 391, 391, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3765, 20, 392, 392, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3766, 20, 393, 393, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3767, 20, 394, 394, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3768, 20, 395, 395, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3931, 20, 20, 430, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3932, 15, 4, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3775, 20, 398, 398, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3776, 20, 399, 399, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3777, 27, 24, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3778, 27, 25, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3933, 17, 5, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3934, 14, 5, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3820, 190, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3821, 191, 1, 1, 8, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3959, 27, 1, 8, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3960, 126, 1, 1200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3946, 20, 1, 417, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3947, 20, 1, 418, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3948, 20, 1, 419, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3949, 20, 1, 420, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3950, 20, 1, 421, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3951, 20, 1, 422, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3952, 20, 1, 423, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3953, 20, 1, 424, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3954, 20, 1, 425, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3955, 20, 1, 426, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3956, 20, 1, 427, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3957, 20, 1, 428, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3958, 20, 1, 429, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1364, 102, 2, 125, 4, 1505);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1365, 102, 3, 125, 4, 1496);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1366, 102, 3, 125, 4, 1501);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1367, 102, 3, 125, 4, 1506);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1368, 102, 4, 125, 4, 1497);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1369, 102, 4, 125, 4, 1502);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1370, 102, 4, 125, 4, 1507);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1371, 102, 1, 121, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2551, 102, 18, 127, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2552, 102, 18, 179, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1374, 102, 2, 121, 21, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2553, 102, 18, 394, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2554, 102, 19, 127, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1377, 102, 3, 121, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2555, 102, 19, 179, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2556, 102, 19, 394, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1738, 128, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1739, 128, 11, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1740, 129, 1, 4, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1741, 129, 2, 8, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1742, 129, 3, 12, 6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1743, 129, 4, 16, 8, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1744, 129, 5, 20, 10, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1888, 17, 45, 40, 40, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1950, 52, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1951, 52, 2, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1952, 52, 3, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1953, 52, 4, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1954, 52, 5, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1970, 20, 259, 259, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1971, 20, 260, 260, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1972, 20, 261, 261, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1973, 20, 262, 262, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1974, 20, 263, 263, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2008, 97, 1, 2041, 2042, 2043);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2151, 20, 276, 276, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2152, 20, 277, 277, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2557, 102, 20, 127, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2558, 102, 20, 179, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2559, 102, 20, 394, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2560, 102, 21, 127, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2561, 102, 21, 179, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2562, 102, 21, 394, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2563, 102, 22, 58, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2564, 102, 22, 347, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2565, 102, 22, 348, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2566, 102, 22, 349, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2567, 102, 22, 350, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2568, 102, 22, 57, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2569, 102, 22, 355, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2570, 102, 22, 356, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2571, 102, 22, 357, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2572, 102, 22, 358, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2573, 102, 23, 58, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2574, 102, 23, 347, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2575, 102, 23, 348, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2576, 102, 23, 349, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2577, 102, 23, 350, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2578, 102, 23, 57, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2579, 102, 23, 355, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2580, 102, 23, 356, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2581, 102, 23, 357, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2582, 102, 23, 358, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2583, 102, 24, 58, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2584, 102, 24, 347, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2585, 102, 24, 348, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2586, 102, 24, 349, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2587, 102, 24, 350, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2588, 102, 24, 57, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2589, 102, 24, 355, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2590, 102, 24, 356, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2591, 102, 24, 357, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2592, 102, 24, 358, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2593, 102, 25, 58, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2594, 102, 25, 347, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2595, 102, 25, 348, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2596, 102, 25, 349, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2597, 102, 25, 350, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2598, 102, 25, 57, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2599, 102, 25, 355, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2600, 102, 25, 356, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2601, 102, 25, 357, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2602, 102, 25, 358, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2603, 102, 26, 58, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2604, 102, 26, 347, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2605, 102, 26, 348, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2606, 102, 26, 349, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2607, 102, 26, 350, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2608, 102, 26, 57, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2609, 102, 26, 355, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2610, 102, 26, 356, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2611, 102, 26, 357, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2612, 102, 26, 358, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2613, 102, 2, 394, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2614, 102, 3, 394, 9, 35);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2615, 102, 4, 394, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2616, 102, 5, 394, 9, 65);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2617, 102, 6, 394, 9, 80);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2642, 5, 14, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2643, 5, 15, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2644, 5, 16, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2645, 6, 15, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2646, 6, 16, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2647, 6, 17, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2648, 7, 15, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2649, 7, 16, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2650, 7, 17, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2651, 71, 24, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2652, 71, 25, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2653, 71, 26, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2654, 71, 27, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2655, 71, 28, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2656, 72, 24, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2657, 72, 25, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2658, 72, 26, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2659, 72, 27, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2660, 72, 28, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2661, 58, 91, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2662, 58, 92, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2663, 58, 93, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2664, 58, 94, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2665, 58, 95, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2666, 59, 8, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2667, 59, 9, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2668, 59, 10, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2669, 59, 11, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2670, 59, 12, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2671, 16, 17, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2672, 16, 18, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2673, 16, 19, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2674, 14, 14, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2675, 14, 15, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2676, 14, 16, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2677, 52, 11, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2678, 52, 12, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2679, 52, 13, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2680, 125, 11, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2681, 125, 12, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2682, 125, 13, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2683, 102, 3, 65, 4, 2399);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2684, 102, 3, 719, 4, 2399);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2685, 102, 3, 720, 4, 2399);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2686, 102, 3, 326, 4, 2399);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2687, 102, 3, 721, 4, 2399);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2688, 102, 3, 722, 4, 2399);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2691, 17, 1, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2692, 17, 2, -2, -2, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2697, 17, 8, -8, -8, -8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2694, 17, 4, -4, -4, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2695, 17, 5, -5, -5, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2696, 17, 6, -6, -6, -6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2698, 17, 9, -9, -9, -9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2699, 17, 10, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2700, 17, 12, -12, -12, -12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2701, 20, 284, 284, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2702, 20, 285, 285, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2703, 20, 286, 286, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2704, 20, 287, 287, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2705, 20, 288, 288, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2805, 102, 5, 121, 21, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2866, 14, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2867, 14, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2868, 14, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2869, 14, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2870, 14, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2871, 14, 6, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2872, 71, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2873, 71, 2, 110, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2874, 71, 3, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2875, 71, 4, 130, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2876, 71, 5, 140, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2877, 71, 6, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2906, 102, 1, 57, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2907, 102, 2, 57, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2908, 102, 3, 57, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2909, 102, 1, 355, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2910, 102, 2, 355, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2911, 102, 3, 355, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2912, 102, 1, 356, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2913, 102, 2, 356, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2914, 102, 3, 356, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2915, 102, 1, 357, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2916, 102, 2, 357, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2917, 102, 3, 357, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2918, 102, 1, 358, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2919, 102, 2, 358, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2920, 102, 3, 358, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2921, 102, 30, 58, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2922, 102, 30, 347, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2923, 102, 30, 348, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2924, 102, 30, 349, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2925, 102, 30, 350, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2926, 102, 30, 57, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1339, 28, 33, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1340, 28, 123, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1341, 27, 29, 6, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1728, 114, 2, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2155, 150, 4, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2156, 150, 5, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2157, 109, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2158, 109, 5, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2159, 4, 59, 0, 0, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2160, 4, 60, 0, 0, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2161, 100, 24, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2162, 100, 25, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2183, 16, 70, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2184, 16, 70, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2185, 17, 70, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2186, 17, 70, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2187, 4, 70, 0, 0, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2188, 4, 70, 0, 0, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2189, 4, 70, 0, 0, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2190, 4, 70, 0, 0, 75);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2488, 102, 1, 1134, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2489, 102, 1, 1208, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2490, 102, 2, 1209, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2491, 102, 3, 1210, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2492, 102, 4, 1211, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2493, 102, 5, 1212, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2494, 102, 6, 1213, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2495, 102, 7, 1214, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2496, 102, 8, 1215, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2497, 102, 9, 1216, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2498, 102, 10, 1217, 25, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2757, 60, 1, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2758, 60, 2, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2759, 60, 3, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2927, 102, 30, 355, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2928, 102, 30, 356, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2929, 102, 30, 357, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2930, 102, 30, 358, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2931, 99, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2932, 100, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2933, 102, 19, 2595, 13, 2799);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2944, 154, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2948, 58, 99, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2949, 3, 5, 0, -220, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2950, 3, 6, 0, -240, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2951, 3, 7, 0, -260, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2952, 102, 1, 393, 4, 2826);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2953, 102, 2, 393, 4, 2827);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2954, 102, 3, 393, 4, 2828);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2955, 20, 291, 291, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2956, 20, 292, 292, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2957, 20, 293, 293, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2958, 20, 294, 294, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2959, 20, 295, 295, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2975, 66, 61, 13, 2335, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2976, 30, 26, 500, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2977, 28, 124, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2978, 28, 125, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2979, 30, 27, 750, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2980, 144, 11, 51, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2981, 144, 12, 52, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2982, 8, 1, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2983, 155, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2984, 155, 2, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2985, 155, 3, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2986, 155, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2987, 20, 296, 296, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2988, 20, 297, 297, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2989, 20, 298, 298, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2990, 20, 299, 299, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2991, 20, 300, 300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3005, 144, 13, 31, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3006, 20, 301, 301, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3007, 20, 302, 302, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3008, 20, 303, 303, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3038, 20, 333, 333, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3039, 20, 334, 334, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3040, 20, 335, 335, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3041, 101, 1, 968, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3042, 101, 1, 969, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3043, 101, 1, 970, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3044, 20, 336, 336, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3045, 20, 337, 337, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3046, 10, 1, 0, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3047, 12, 1, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3052, 97, 1, 2382, 2393, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3053, 97, 1, 2395, 2400, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3054, 97, 1, 2396, 2397, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3055, 97, 1, 2401, 2404, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3056, 20, 333, 333, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3057, 20, 334, 334, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3058, 101, 1, 968, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3059, 20, 338, 338, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3060, 20, 339, 339, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3061, 101, 1, 971, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3062, 101, 1, 972, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3065, 20, 342, 342, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3067, 101, 1, 973, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3069, 20, 344, 344, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3070, 20, 345, 345, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3071, 66, 62, 14, 2520, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3117, 20, 346, 346, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3118, 20, 347, 347, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3132, 21, 1, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3135, 29, 10, 100, 250, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3136, 101, 1, 1074, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3137, 101, 1, 1075, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3138, 101, 1, 1076, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3139, 101, 1, 1077, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3140, 71, 10, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3147, 75, 22, 3124, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3417, 58, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3434, 20, 365, 365, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3435, 20, 366, 366, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3443, 75, 7, 1423, 1, 2624);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3444, 75, 8, 1893, 1, 2624);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3445, 75, 9, 1425, 1, 2624);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3446, 10, 1, 0, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3447, 10, 2, 0, 2, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3448, 10, 3, 0, 3, -20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3449, 10, 4, 0, 3, -25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3450, 10, 5, 0, 3, -30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3451, 10, 6, 0, 3, -40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3452, 10, 7, 0, 3, -45);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3453, 10, 8, 0, 3, -50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3454, 10, 9, 0, 3, -60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3455, 17, 10, -10, -10, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3571, 170, 1, 1871, 1809, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3572, 180, 1, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3609, 101, 1, 1230, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3610, 101, 1, 1231, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3627, 20, 370, 370, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1704, 105, 1, 1698, 1697, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1708, 105, 1, 1701, 1700, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1992, 149, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1993, 148, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1994, 147, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2812, 102, 2, 1214, 20, 2373);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2813, 102, 3, 1215, 20, 2374);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2888, 28, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2815, 102, 4, 1216, 20, 2375);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2816, 102, 5, 1217, 20, 2376);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3145, 166, 1, 1, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3418, 102, 1, 2615, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3419, 102, 2, 2615, 10, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3420, 102, 3, 2615, 10, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3421, 102, 4, 2615, 10, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3422, 102, 5, 2615, 10, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3423, 102, 1, 2637, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3424, 102, 2, 2637, 10, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3425, 102, 3, 2637, 10, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3426, 102, 4, 2637, 10, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3427, 102, 5, 2637, 10, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3436, 20, 365, 365, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3437, 20, 366, 366, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3573, 177, 1, 4000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3598, 14, 1, -3, -3, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3599, 14, 2, -6, -6, -6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3600, 14, 3, -9, -9, -9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3601, 14, 4, -12, -12, -12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3604, 27, 1, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3602, 144, 1, 53, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3603, 145, 1, 73, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1725, 17, 52, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1726, 15, 60, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1729, 30, 21, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1730, 30, 22, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1731, 30, 23, 75, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1732, 30, 24, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1733, 30, 25, 125, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1764, 15, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1765, 15, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1814, 101, 1, 699, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1815, 101, 1, 700, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1816, 75, 19, 1924, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1890, 102, 1, 254, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1891, 102, 1, 255, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1922, 125, 6, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1923, 125, 7, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1926, 125, 8, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1927, 125, 9, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1928, 125, 10, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2148, 20, 273, 273, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2149, 20, 274, 274, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2163, 27, 11, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2164, 27, 12, 28, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2165, 27, 33, 30, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2166, 27, 33, 42, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2167, 28, 33, 15, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2168, 28, 33, 21, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2169, 28, 18, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2170, 28, 17, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2310, 102, 1, 1167, 8, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2311, 102, 1, 1168, 8, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2312, 102, 1, 1169, 8, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2313, 102, 1, 1170, 8, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2314, 102, 1, 1171, 8, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2315, 102, 1, 128, 4, 2640);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2316, 102, 2, 128, 4, 2641);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2317, 102, 3, 128, 4, 2644);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2318, 3, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2319, 3, 2, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2320, 3, 3, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2321, 102, 1, 140, 4, 2427);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2322, 102, 2, 140, 4, 2428);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2323, 102, 3, 140, 4, 2429);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2324, 16, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2325, 16, 2, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2326, 16, 3, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2327, 102, 1, 1208, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2328, 102, 2, 1208, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2329, 102, 3, 1208, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2330, 102, 1, 1209, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2331, 102, 2, 1209, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2332, 102, 3, 1209, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2333, 102, 1, 1210, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2334, 102, 2, 1210, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2335, 102, 3, 1210, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2336, 102, 1, 1211, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2337, 102, 2, 1211, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2338, 102, 3, 1211, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2339, 102, 1, 1212, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2340, 102, 2, 1212, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2341, 102, 3, 1212, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2342, 102, 1, 1213, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2343, 102, 2, 1213, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2344, 102, 3, 1213, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2345, 102, 1, 1214, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2346, 102, 2, 1214, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2347, 102, 3, 1214, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2348, 102, 1, 1215, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2349, 102, 2, 1215, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2350, 102, 3, 1215, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2351, 102, 1, 1216, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2352, 102, 2, 1216, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2353, 102, 3, 1216, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2354, 102, 1, 1217, 23, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2355, 102, 2, 1217, 23, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2356, 102, 3, 1217, 23, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2357, 12, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2358, 102, 1, 1213, 20, 2372);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2359, 102, 1, 315, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2360, 102, 2, 315, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2361, 102, 3, 315, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2362, 102, 1, 315, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2363, 102, 2, 315, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2364, 17, 1, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2365, 17, 2, -2, -2, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2366, 102, 1, 315, 4, 2715);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2367, 102, 2, 315, 4, 2716);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2368, 102, 1, 318, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2369, 102, 2, 318, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2370, 102, 3, 318, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2371, 102, 4, 318, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2372, 102, 5, 318, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2373, 52, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2374, 52, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2375, 52, 3, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2378, 40, 1, 7031, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2379, 40, 1, 7032, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2380, 102, 1, 182, 24, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2381, 102, 1, 183, 24, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2382, 102, 1, 177, 24, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2383, 102, 1, 179, 16, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2384, 102, 2, 179, 16, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2385, 102, 3, 179, 16, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2386, 102, 1, 178, 27, 2468);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2387, 102, 1, 174, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2388, 102, 2, 174, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2389, 102, 3, 174, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2390, 15, 70, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2391, 15, 70, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2392, 15, 70, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2393, 15, 70, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2394, 15, 70, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2395, 15, 70, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2396, 15, 70, 0, 0, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2397, 15, 70, 0, 0, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2398, 15, 70, 0, 0, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2399, 15, 70, 0, 0, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2400, 15, 70, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2401, 15, 70, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2402, 15, 70, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2403, 15, 70, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2404, 15, 70, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2405, 15, 70, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2406, 102, 1, 176, 4, 2717);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2935, 75, 30, 2799, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2936, 75, 31, 2800, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2937, 75, 33, 2802, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2938, 75, 32, 2801, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2939, 75, 34, 2803, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3048, 97, 1, 2379, 2390, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3049, 97, 1, 2380, 2392, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3050, 97, 1, 2381, 2391, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3051, 97, 1, 2394, 2399, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3159, 102, 1, 2615, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3160, 102, 2, 2615, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3161, 102, 3, 2615, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3162, 102, 1, 2615, 3, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3163, 102, 2, 2615, 3, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3164, 102, 3, 2615, 3, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3165, 102, 4, 2615, 3, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3166, 102, 5, 2615, 3, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3167, 102, 1, 2616, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3168, 102, 2, 2616, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3169, 102, 3, 2616, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3170, 102, 1, 2616, 4, 3117);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3171, 102, 2, 2616, 4, 3118);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3172, 102, 3, 2616, 4, 3119);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3173, 102, 1, 2618, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3174, 102, 2, 2618, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3175, 102, 3, 2618, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3176, 102, 1, 2615, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3177, 102, 2, 2615, 10, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3178, 102, 3, 2615, 10, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3179, 102, 4, 2615, 10, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3180, 102, 5, 2615, 10, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3181, 102, 1, 2618, 4, 3120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3182, 102, 2, 2618, 4, 3121);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3183, 102, 3, 2618, 4, 3122);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3184, 102, 4, 2618, 4, 3123);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3185, 102, 1, 2617, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3186, 102, 2, 2617, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3187, 102, 3, 2617, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3188, 102, 4, 2617, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3189, 102, 5, 2617, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3190, 102, 1, 2617, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3191, 102, 2, 2617, 21, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3192, 102, 3, 2617, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3193, 102, 1, 2619, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3194, 102, 2, 2619, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3195, 102, 3, 2619, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3196, 102, 4, 2619, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3197, 102, 5, 2619, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3198, 102, 1, 2619, 2, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3199, 102, 2, 2619, 2, 120);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3200, 102, 3, 2619, 2, 180);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3201, 102, 4, 2619, 2, 240);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3202, 102, 5, 2619, 2, 300);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3203, 102, 1, 2615, 17, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3204, 102, 2, 2615, 17, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3205, 102, 3, 2615, 17, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3206, 102, 1, 2615, 13, 2374);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3207, 102, 1, 2616, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3208, 102, 2, 2616, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3209, 102, 3, 2616, 2, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3210, 102, 4, 2616, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3211, 102, 5, 2616, 2, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3212, 102, 1, 2618, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3213, 102, 2, 2618, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3214, 102, 3, 2618, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3215, 102, 1, 2617, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3216, 102, 2, 2617, 2, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3217, 102, 4, 2617, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3218, 102, 3, 2617, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3219, 102, 5, 2617, 21, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3220, 102, 1, 2619, 18, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3349, 102, 1, 2638, 3, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3350, 102, 2, 2638, 3, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3351, 102, 3, 2638, 3, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3352, 102, 4, 2638, 3, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3353, 102, 5, 2638, 3, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3354, 102, 1, 2632, 4, 3130);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1756, 106, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1757, 106, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1758, 106, 3, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1759, 106, 4, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1760, 106, 5, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1761, 109, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1762, 109, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1763, 109, 3, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1889, 101, 1, 709, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2146, 20, 271, 271, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2153, 6, 14, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2154, 7, 14, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2191, 102, 1, 120, 17, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2192, 102, 2, 120, 17, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2193, 102, 3, 120, 17, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2194, 102, 1, 120, 13, 2374);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2195, 102, 1, 122, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2196, 102, 2, 122, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2197, 102, 3, 122, 2, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2198, 102, 4, 122, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2199, 102, 5, 122, 2, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2200, 102, 1, 125, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2201, 102, 2, 125, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2202, 102, 3, 125, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2203, 102, 1, 121, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2204, 102, 2, 121, 2, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2205, 102, 3, 121, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2206, 102, 1, 314, 18, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2207, 102, 1, 1134, 16, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2208, 102, 2, 1134, 16, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2209, 102, 3, 1134, 16, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2889, 102, 1, 2201, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2890, 102, 2, 2208, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2891, 102, 3, 2209, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2892, 102, 4, 2210, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2893, 102, 5, 2211, 16, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2896, 102, 27, 58, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2897, 102, 27, 348, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2898, 102, 27, 347, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2899, 102, 27, 349, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2900, 102, 27, 350, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2901, 102, 27, 57, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2902, 102, 27, 355, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2903, 102, 27, 356, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2904, 102, 27, 357, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2905, 102, 27, 358, 9, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3356, 71, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3368, 71, 1, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3369, 71, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3370, 72, 1, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3371, 72, 2, 7, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3372, 72, 1, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3373, 72, 2, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3374, 72, 1, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3375, 72, 2, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3376, 72, 3, 17, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3377, 72, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3378, 72, 2, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3379, 72, 1, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3380, 72, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3381, 72, 1, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3382, 72, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3383, 30, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3384, 30, 2, 130, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3385, 30, 1, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3386, 30, 2, 300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3387, 30, 3, 350, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3388, 30, 1, 380, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3389, 30, 2, 400, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3390, 30, 1, 420, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3391, 30, 2, 450, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3392, 27, 3, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3393, 27, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3394, 27, 1, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3395, 27, 2, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3396, 27, 1, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3397, 27, 2, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3398, 28, 2, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3399, 28, 3, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3400, 28, 1, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3401, 28, 2, 2, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3402, 28, 2, 3, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3403, 126, 1, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3404, 126, 2, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3405, 126, 3, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3406, 126, 2, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3407, 126, 2, 180, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3408, 169, 1, 5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3409, 100, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3410, 111, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3411, 110, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3412, 125, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3413, 77, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3414, 14, 1, 0, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3415, 44, 1, 2, 99, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3416, 15, 1, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3456, 71, 1, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3457, 71, 2, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3458, 71, 3, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3459, 71, 1, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3460, 71, 2, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3461, 71, 3, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3462, 71, 4, 110, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3463, 71, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3464, 71, 2, 110, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3465, 71, 3, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3466, 71, 1, 110, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3467, 71, 2, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3468, 71, 3, 160, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3469, 27, 1, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3470, 27, 2, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3471, 27, 3, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3472, 27, 4, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3473, 72, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3474, 72, 2, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3475, 72, 3, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3476, 72, 1, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3477, 72, 2, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3478, 72, 3, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3479, 72, 4, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3480, 72, 1, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3481, 72, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3482, 72, 3, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3483, 72, 1, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3484, 72, 2, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3485, 72, 3, 65, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3486, 28, 1, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3487, 28, 2, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3488, 28, 3, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3489, 28, 4, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3490, 126, 1, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3491, 126, 2, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3492, 126, 3, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3493, 126, 4, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3494, 126, 5, 120, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3495, 126, 6, 180, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3496, 126, 7, 240, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3497, 126, 8, 360, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3498, 30, 1, 130, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3499, 30, 2, 140, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3500, 30, 3, 170, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3501, 30, 1, 140, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3502, 30, 2, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3503, 30, 3, 160, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3504, 30, 4, 190, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3505, 30, 1, 180, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3506, 30, 2, 190, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3507, 30, 3, 210, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3508, 30, 1, 190, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3509, 30, 2, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3510, 30, 3, 230, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3511, 71, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3512, 71, 2, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3513, 71, 3, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3514, 71, 1, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3515, 71, 2, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3516, 71, 3, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3517, 71, 4, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3518, 71, 1, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3519, 71, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3520, 71, 3, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3521, 71, 1, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3522, 71, 2, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3523, 71, 3, 55, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3524, 72, 1, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3525, 72, 2, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3526, 72, 3, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3527, 72, 1, 25, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3528, 72, 2, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3529, 72, 3, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3530, 72, 4, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3531, 72, 1, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3532, 72, 2, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3533, 72, 3, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3534, 72, 1, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3535, 72, 2, 45, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3536, 72, 3, 55, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3537, 28, 3, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3538, 28, 3, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3539, 27, 3, 1, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3611, 75, 1, 3406, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3612, 75, 1, 3407, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3613, 75, 1, 3408, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3614, 75, 1, 3410, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3615, 75, 1, 3411, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3616, 75, 1, 3412, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3617, 75, 1, 3413, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3618, 75, 1, 3414, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3619, 75, 1, 3415, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3620, 75, 1, 3416, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3621, 75, 1, 3417, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3622, 75, 1, 3418, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3623, 75, 1, 3419, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3624, 75, 1, 3420, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3625, 75, 1, 3421, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1805, 75, 11, 1931, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1806, 75, 12, 1932, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1807, 75, 13, 1933, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1808, 75, 14, 1934, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1809, 75, 15, 1935, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1810, 75, 16, 1936, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1811, 75, 17, 1937, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1812, 75, 18, 1938, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1829, 20, 199, 199, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1830, 20, 200, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1831, 20, 201, 201, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1832, 20, 202, 202, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1833, 20, 203, 203, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1834, 20, 204, 204, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1835, 20, 205, 205, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1836, 20, 206, 206, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1837, 20, 207, 207, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1864, 29, 8, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1976, 3, 10, 0, 10, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1977, 142, 7, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2482, 20, 281, 281, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2486, 20, 282, 282, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2487, 20, 283, 283, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2706, 153, 1, 2459, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2707, 153, 2, 2460, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2708, 153, 3, 2461, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2803, 27, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2804, 27, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2821, 102, 1, 1391, 11, 2181);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2822, 102, 2, 1392, 11, 2182);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2823, 102, 3, 1393, 11, 2183);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2824, 102, 4, 1394, 11, 2184);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2825, 102, 5, 1395, 11, 2185);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2826, 102, 1, 1391, 11, 2186);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2827, 102, 2, 1392, 11, 2187);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2828, 102, 3, 1393, 11, 2188);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2829, 102, 4, 1394, 11, 2189);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2830, 102, 5, 1395, 11, 2190);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2831, 102, 1, 1391, 11, 2191);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2832, 102, 2, 1392, 11, 2192);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2833, 102, 3, 1393, 11, 2193);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2834, 102, 4, 1394, 11, 2194);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2835, 102, 5, 1395, 11, 2195);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2836, 102, 1, 1396, 11, 2196);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2837, 102, 2, 1397, 11, 2197);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2838, 102, 3, 1398, 11, 2198);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2839, 102, 4, 1399, 11, 2199);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2840, 102, 5, 1400, 11, 2200);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2841, 102, 1, 1396, 11, 2201);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2842, 102, 2, 1397, 11, 2202);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2843, 102, 3, 1398, 11, 2203);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2844, 102, 4, 1399, 11, 2204);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2845, 102, 5, 1400, 11, 2205);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2846, 102, 1, 1396, 11, 2206);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2847, 102, 2, 1397, 11, 2207);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2848, 102, 3, 1398, 11, 2208);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2849, 102, 4, 1399, 11, 2209);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2850, 102, 5, 1400, 11, 2210);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2851, 102, 1, 2201, 11, 2178);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2852, 102, 2, 2208, 11, 2179);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2853, 102, 3, 2209, 11, 2180);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2854, 102, 4, 2210, 11, 2211);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2855, 102, 5, 2211, 11, 2212);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2856, 102, 1, 2201, 11, 2213);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2857, 102, 2, 2208, 11, 2214);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2858, 102, 3, 2209, 11, 2215);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2859, 102, 4, 2210, 11, 2216);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2860, 102, 5, 2211, 11, 2217);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2861, 102, 1, 2201, 11, 2218);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2862, 102, 2, 2208, 11, 2219);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2863, 102, 3, 2209, 11, 2220);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2864, 102, 4, 2210, 11, 2221);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2865, 102, 5, 2211, 11, 2222);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2945, 101, 1, 960, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2946, 101, 1, 961, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3605, 20, 367, 367, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3606, 20, 368, 368, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3607, 101, 1, 1078, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3608, 101, 1, 1079, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (1955, 52, 12, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2001, 149, 2, 1, 0, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2002, 10, 41, 0, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2147, 20, 272, 272, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3141, 75, 20, 3097, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3142, 107, 1, -5, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3143, 75, 21, 3098, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3355, 66, 6, 4, 2580, 600);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2229, 14, 70, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2230, 102, 1, 138, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2231, 102, 2, 138, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2232, 102, 3, 138, 2, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2233, 102, 4, 138, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2234, 102, 5, 138, 2, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2235, 102, 1, 1250, 26, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2236, 102, 2, 1250, 26, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2237, 102, 3, 1250, 26, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2238, 102, 1, 1258, 26, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2239, 102, 2, 1258, 26, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2240, 102, 3, 1258, 26, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2241, 102, 1, 1248, 26, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2242, 102, 2, 1248, 26, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2243, 102, 3, 1248, 26, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2244, 102, 1, 1249, 26, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2245, 102, 2, 1249, 26, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2246, 102, 3, 1249, 26, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2247, 102, 1, 1256, 26, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2248, 102, 2, 1256, 26, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2249, 102, 3, 1256, 26, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2250, 102, 1, 1257, 26, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2251, 102, 2, 1257, 26, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2252, 102, 3, 1257, 26, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2253, 102, 1, 58, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2254, 102, 2, 58, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2255, 102, 3, 58, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2256, 102, 1, 347, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2257, 102, 2, 347, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2258, 102, 3, 347, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2259, 102, 1, 348, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2260, 102, 2, 348, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2261, 102, 3, 348, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2262, 102, 1, 349, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2263, 102, 2, 349, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2264, 102, 3, 349, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2265, 102, 1, 350, 21, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2266, 102, 2, 350, 21, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2267, 102, 3, 350, 21, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2268, 151, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2269, 151, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2270, 151, 3, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2271, 14, 70, 0, 0, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2277, 102, 1, 1284, 22, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2278, 102, 2, 1284, 22, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2279, 102, 3, 1284, 22, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2280, 102, 1, 1285, 22, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2281, 102, 2, 1285, 22, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2282, 102, 3, 1285, 22, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2283, 102, 1, 1286, 22, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2284, 102, 2, 1286, 22, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2285, 102, 3, 1286, 22, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2286, 102, 1, 1287, 22, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2287, 102, 2, 1287, 22, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2288, 102, 3, 1287, 22, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2289, 102, 1, 1288, 22, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2290, 102, 2, 1288, 22, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2291, 102, 3, 1288, 22, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2292, 102, 1, 180, 23, 100);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2293, 102, 1, 18, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2294, 102, 2, 18, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2295, 102, 3, 18, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2296, 102, 1, 737, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2297, 102, 2, 737, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2298, 102, 3, 737, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2299, 102, 1, 738, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2300, 102, 2, 738, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2301, 102, 3, 738, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2302, 102, 1, 739, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2303, 102, 2, 739, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2304, 102, 3, 739, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2305, 102, 1, 740, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2306, 102, 2, 740, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2307, 102, 3, 740, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2715, 15, 51, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2716, 15, 52, -2, -2, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2717, 15, 54, -4, -4, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2709, 14, 1, -1, -1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2710, 14, 2, -2, -2, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2711, 14, 3, -3, -3, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2712, 14, 4, -4, -4, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2713, 14, 5, -5, -5, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2714, 14, 6, -6, -6, -6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2718, 15, 55, -5, -5, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2719, 15, 56, -6, -6, -6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2720, 15, 58, -8, -8, -8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2721, 15, 59, -9, -9, -9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2722, 15, 60, -10, -10, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2723, 15, 62, -12, -12, -12);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2724, 10, 1, 0, 1, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2725, 10, 2, 0, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2726, 10, 3, 0, 3, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2727, 10, 4, 0, 3, 25);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2728, 10, 5, 0, 3, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2729, 10, 6, 0, 3, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2730, 10, 7, 0, 3, 45);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2731, 10, 8, 0, 3, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2732, 10, 9, 0, 3, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2733, 5, 1, 0, -1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2734, 5, 2, 0, -2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2735, 5, 3, 0, -3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2736, 5, 4, 0, -4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2737, 5, 5, 0, -5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2738, 7, 1, 0, -1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2739, 7, 2, 0, -2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2740, 7, 3, 0, -3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2741, 7, 4, 0, -4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2742, 7, 5, 0, -5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2743, 6, 1, 0, -1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2744, 6, 2, 0, -2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2745, 6, 3, 0, -3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2746, 6, 4, 0, -4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2747, 6, 5, 0, -5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2748, 71, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2749, 71, 2, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2750, 71, 3, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2751, 71, 4, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2752, 71, 5, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2753, 71, 6, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2754, 71, 7, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2755, 71, 8, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2756, 71, 9, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2760, 17, 1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2761, 17, 2, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2762, 17, 3, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2763, 17, 4, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2764, 17, 5, 5, 5, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2765, 17, 6, 6, 6, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2766, 17, 7, 7, 7, 7);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2767, 17, 8, 8, 8, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2768, 17, 9, 9, 9, 9);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2769, 17, 10, 10, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2770, 17, 51, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2771, 17, 52, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2772, 17, 53, 0, 3, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2773, 17, 54, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2774, 17, 55, 0, 5, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2775, 17, 56, 0, 6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2776, 17, 57, 0, 7, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2777, 17, 58, 0, 8, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2778, 17, 59, 0, 10, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2779, 17, 61, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2780, 17, 62, 0, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2781, 17, 63, 0, 6, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2782, 17, 64, 0, 8, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2783, 17, 65, 0, 10, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2784, 17, 66, 0, 12, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2785, 17, 67, 0, 14, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2786, 17, 68, 0, 16, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2787, 17, 69, 0, 20, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2788, 72, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2789, 72, 2, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2790, 72, 3, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2791, 72, 4, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2792, 72, 5, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2793, 72, 6, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2794, 72, 7, 70, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2795, 72, 8, 80, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2796, 72, 2, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2797, 71, 2, 15, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2798, 102, 1, 127, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2799, 102, 2, 127, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2800, 102, 3, 127, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2801, 102, 1, 2346, 24, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2802, 102, 1, 2347, 24, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2878, 19, 1, 1, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2879, 19, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2880, 19, 3, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2881, 19, 4, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2882, 19, 5, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2883, 19, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2884, 19, 2, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2885, 19, 3, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2886, 19, 4, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2887, 19, 5, 32, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2992, 137, 1, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2993, 137, 2, 4, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2994, 137, 3, 6, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2995, 137, 4, 8, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2996, 137, 5, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2999, 101, 1, 962, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3000, 101, 1, 963, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3001, 101, 1, 964, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3002, 101, 1, 965, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3003, 101, 1, 966, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3004, 101, 1, 967, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3009, 20, 304, 304, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3010, 20, 305, 305, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3011, 20, 306, 306, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3012, 20, 307, 307, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3013, 20, 308, 308, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3014, 20, 309, 309, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3015, 20, 310, 310, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3016, 20, 311, 311, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3017, 20, 312, 312, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3018, 20, 313, 313, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3019, 20, 314, 314, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3020, 20, 315, 315, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3021, 20, 316, 316, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3022, 20, 317, 317, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3023, 20, 318, 318, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3024, 20, 319, 319, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3025, 20, 320, 320, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3026, 20, 321, 321, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3027, 20, 322, 322, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3028, 20, 323, 323, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3029, 20, 324, 324, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3030, 20, 325, 325, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3031, 20, 326, 326, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3032, 20, 327, 327, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3033, 20, 328, 328, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3034, 20, 329, 329, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3035, 20, 330, 330, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3036, 20, 331, 331, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3037, 20, 332, 332, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3357, 71, 2, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3358, 71, 1, 40, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3359, 71, 2, 50, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3360, 71, 1, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3361, 71, 2, 150, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3362, 71, 3, 200, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3363, 71, 1, 210, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3364, 71, 2, 220, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3365, 71, 1, 240, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3366, 71, 2, 300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3367, 71, 2, 30, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3579, 97, 1, 2622, 2622, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3580, 97, 2, 2622, 2622, 2622);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (2947, 129, 6, 0, 2, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3063, 20, 340, 340, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3064, 20, 341, 341, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3066, 20, 343, 343, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3068, 101, 1, 974, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3119, 20, 358, 358, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3073, 164, 1, 2554, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3120, 20, 351, 351, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3075, 165, 1, 2556, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3076, 157, 1, 9, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3077, 164, 1, 2558, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3078, 17, 1, -10, -10, -10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3079, 27, 1, -10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3080, 157, 1, 10, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3081, 78, 1, 2560, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3082, 159, 1, 900, 300, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3083, 160, 1, 2562, 8, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3084, 159, 1, 500, 200, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3085, 157, 1, 11, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3086, 78, 1, 2561, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3087, 157, 1, 12, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3088, 78, 1, 2566, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3089, 161, 1, 15, 4, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3090, 157, 1, 13, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3091, 78, 1, 2569, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3092, 156, 1, 2570, 1700, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3093, 157, 1, 15, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3094, 1, 1, 30000, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3095, 162, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3096, 157, 1, 16, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3097, 158, 1, 1, 1700, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3098, 157, 1, 17, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3099, 158, 1, 2, 1700, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3100, 157, 1, 18, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3101, 158, 1, 3, 1700, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3102, 157, 1, 19, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3103, 158, 1, 4, 1700, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3104, 78, 1, 2578, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3105, 93, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3106, 163, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3107, 78, 1, 2579, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3108, 157, 1, 20, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3109, 78, 1, 2580, 0, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3110, 157, 1, 14, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3111, 164, 1, 2583, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3112, 157, 1, 21, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3113, 164, 1, 2585, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3114, 156, 1, 2587, 1700, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3115, 156, 1, 2589, 1700, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3116, 164, 1, 2591, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3121, 20, 354, 354, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3122, 20, 352, 352, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3123, 20, 355, 355, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3124, 20, 356, 356, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3125, 20, 350, 350, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3126, 20, 349, 349, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3127, 20, 348, 348, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3128, 20, 357, 357, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3129, 20, 353, 353, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3130, 20, 359, 359, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3131, 20, 360, 360, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3133, 20, 361, 361, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3134, 20, 362, 362, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3144, 28, 6, 6, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3148, 75, 23, 3102, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3149, 48, 2, 300, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3150, 167, 1, 0, 1, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3151, 75, 24, 3109, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3152, 168, 1, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3153, 16, 2, 0, 0, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3154, 14, 2, 0, 0, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3155, 81, 2, 0, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3157, 89, 2, 20, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3158, 63, 2, 7941, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3221, 102, 1, 2620, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3222, 102, 2, 2620, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3223, 102, 3, 2620, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3224, 102, 1, 2620, 3, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3225, 102, 2, 2620, 3, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3226, 102, 3, 2620, 3, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3227, 102, 4, 2620, 3, 8);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3228, 102, 5, 2620, 3, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3229, 102, 1, 2621, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3230, 102, 2, 2621, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3231, 102, 3, 2621, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3232, 102, 1, 2621, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3233, 102, 2, 2621, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3234, 102, 3, 2621, 2, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3235, 102, 4, 2621, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3236, 102, 5, 2621, 2, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3237, 102, 1, 2623, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3238, 102, 2, 2623, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3239, 102, 3, 2623, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3240, 102, 1, 2623, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3241, 102, 2, 2623, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3242, 102, 3, 2623, 2, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3243, 102, 4, 2623, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3244, 102, 5, 2623, 2, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3245, 102, 1, 2622, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3246, 102, 2, 2622, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3247, 102, 3, 2622, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3248, 102, 4, 2622, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3249, 102, 5, 2622, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3250, 102, 1, 2622, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3251, 102, 2, 2622, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3252, 102, 3, 2622, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3253, 102, 1, 2636, 9, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3254, 102, 2, 2636, 9, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3255, 102, 3, 2636, 9, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3256, 102, 4, 2636, 9, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3257, 102, 5, 2636, 9, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3258, 102, 1, 2622, 12, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3259, 102, 2, 2622, 12, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3260, 102, 3, 2622, 12, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3261, 102, 4, 2622, 12, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3262, 102, 5, 2622, 12, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3263, 102, 1, 2622, 4, 3125);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3264, 102, 2, 2622, 4, 3126);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3265, 102, 3, 2622, 4, 3127);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3266, 102, 4, 2622, 4, 3128);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3267, 102, 5, 2622, 4, 3129);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3268, 102, 1, 2621, 4, 2640);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3269, 102, 2, 2621, 4, 2641);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3270, 102, 3, 2621, 4, 2644);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3271, 102, 1, 2623, 4, 2427);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3272, 102, 2, 2623, 4, 2428);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3273, 102, 3, 2623, 4, 2429);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3274, 102, 1, 2622, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3275, 102, 2, 2622, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3276, 102, 3, 2622, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3277, 102, 1, 2624, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3278, 102, 2, 2624, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3279, 102, 3, 2624, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3280, 102, 1, 2624, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3281, 102, 2, 2624, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3282, 102, 1, 2624, 4, 2715);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3283, 102, 2, 2624, 4, 2716);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3284, 102, 1, 2628, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3285, 102, 2, 2628, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3286, 102, 3, 2628, 2, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3287, 102, 4, 2628, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3288, 102, 5, 2628, 2, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3289, 102, 1, 2626, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3290, 102, 2, 2626, 2, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3291, 102, 3, 2626, 2, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3292, 102, 4, 2626, 2, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3293, 102, 5, 2626, 2, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3294, 102, 1, 2632, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3295, 102, 2, 2632, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3296, 102, 3, 2632, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3297, 102, 4, 2632, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3298, 102, 5, 2632, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3299, 102, 1, 2632, 2, 5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3300, 102, 2, 2632, 2, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3301, 102, 3, 2632, 2, 15);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3302, 102, 1, 2634, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3303, 102, 2, 2634, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3304, 102, 3, 2634, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3305, 102, 4, 2634, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3306, 102, 5, 2634, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3307, 102, 1, 2637, 10, 10);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3308, 102, 2, 2637, 10, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3309, 102, 3, 2637, 10, 30);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3310, 102, 4, 2637, 10, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3311, 102, 5, 2637, 10, 50);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3312, 102, 1, 2636, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3313, 102, 2, 2636, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3314, 102, 3, 2636, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3315, 102, 4, 2636, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3316, 102, 5, 2636, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3317, 102, 1, 2747, 2, 1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3318, 102, 2, 2747, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3319, 102, 3, 2747, 2, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3320, 102, 1, 2635, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3321, 102, 2, 2635, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3322, 102, 3, 2635, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3323, 102, 4, 2635, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3324, 102, 5, 2635, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3325, 102, 1, 2636, 5, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3326, 102, 2, 2636, 5, 4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3327, 102, 3, 2636, 5, 6);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3328, 102, 1, 2635, 4, 1489);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3329, 102, 2, 2635, 4, 1490);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3330, 102, 3, 2635, 4, 1491);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3331, 102, 4, 2635, 4, 1492);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3332, 102, 5, 2635, 4, 1493);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3333, 102, 1, 2633, 4, 1608);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3334, 102, 2, 2633, 4, 1609);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3335, 102, 3, 2633, 4, 1610);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3336, 102, 1, 2637, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3337, 102, 2, 2637, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3338, 102, 3, 2637, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3339, 102, 4, 2637, 1, -4);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3340, 102, 5, 2637, 1, -5);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3341, 102, 1, 2634, 24, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3342, 102, 1, 2636, 16, 20);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3343, 102, 2, 2636, 16, 40);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3344, 102, 3, 2636, 16, 60);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3345, 102, 1, 2635, 27, 2468);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3346, 102, 1, 2631, 1, -1);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3347, 102, 2, 2631, 1, -2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3348, 102, 3, 2631, 1, -3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3432, 20, 363, 363, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3433, 20, 364, 364, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3442, 15, 51, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3540, 71, 3, 90, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3541, 30, 3, 170, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3542, 30, 3, 160, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3543, 126, 3, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3544, 126, 3, 60, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3545, 72, 3, 35, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3546, 109, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3547, 111, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3548, 110, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3549, 100, 3, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3550, 52, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3551, 125, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3552, 77, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3553, 182, 1, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3554, 181, 1, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3555, 179, 1, 3000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3556, 176, 1, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3557, 183, 1, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3558, 178, 1, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3559, 174, 1, 0, 0, 10000);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3560, 14, 2, 0, 0, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3561, 151, 2, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3562, 184, 1, 10000, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3626, 20, 369, 369, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3564, 15, 52, 3, 3, 3);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3565, 111, 3, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3566, 175, 52, 4000, 4000, 4000);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3567, 109, 3, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3568, 110, 3, 2, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3569, 58, 2, 3, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3570, 15, 51, 2, 2, 2);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3574, 171, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3575, 172, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3576, 173, 1, 0, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3577, 97, 1, 2620, 2620, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3578, 97, 2, 2620, 2620, 2620);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3628, 58, 100, 100, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3629, 101, 1, 1232, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3630, 101, 1, 1233, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3962, 20, 1, 431, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3963, 20, 1, 432, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3964, 20, 1, 433, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (3965, 20, 1, 434, 1, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (107249, 20, 1, 464, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (107250, 20, 1, 465, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (107273, 20, 1, 466, 0, 0);
GO

INSERT INTO [dbo].[DT_Module] ([MID], [MType], [MLevel], [MAParam], [MBParam], [MCParam]) VALUES (107274, 20, 1, 467, 0, 0);
GO

