/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_CharItem
Date                  : 2023-10-07 09:09:33
*/


INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1693, 68, 270, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1694, 68, 143, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1695, 68, 93, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1696, 68, 212, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1697, 68, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1698, 68, 229, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2137, 78, 322, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2138, 78, 101, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2139, 78, 62, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2140, 78, 212, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2141, 78, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2142, 78, 231, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2163, 69, 288, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2164, 69, 122, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2165, 69, 74, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2166, 69, 163, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2167, 69, 163, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2168, 69, 188, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2169, 69, 454, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2170, 69, 600, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2171, 69, 457, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2172, 69, 259, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2193, 70, 287, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2194, 70, 122, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2195, 70, 74, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2196, 70, 179, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2197, 70, 179, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2198, 70, 201, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2199, 70, 449, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2200, 70, 576, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2201, 70, 457, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2202, 70, 259, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2205, 80, 272, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2206, 80, 187, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2207, 80, 542, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2208, 80, 212, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2209, 80, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2210, 80, 232, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2211, 80, 253, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1851, 74, 323, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1852, 74, 101, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1853, 74, 61, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1854, 74, 212, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1855, 74, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1886, 72, 311, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1887, 72, 143, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1888, 72, 93, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1889, 72, 166, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1890, 72, 166, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1891, 72, 201, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1892, 72, 567, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1893, 72, 606, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1894, 72, 688, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1895, 72, 254, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1896, 72, 409, 23790, 17);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1897, 72, 705, 4, 18);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1898, 75, 321, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1899, 75, 143, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1900, 75, 93, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1901, 75, 182, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1902, 75, 182, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1903, 75, 201, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1904, 75, 567, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1905, 75, 606, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1906, 75, 688, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1907, 75, 254, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1919, 76, 287, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1920, 76, 143, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1921, 76, 93, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1922, 76, 179, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1923, 76, 182, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1924, 76, 201, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1925, 76, 573, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1926, 76, 482, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1927, 76, 700, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1928, 76, 254, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1929, 77, 272, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1930, 77, 120, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1931, 77, 72, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1932, 77, 215, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1933, 77, 474, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1934, 77, 455, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1981, 73, 271, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1982, 73, 101, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1983, 73, 71, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1984, 73, 210, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1985, 73, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1986, 73, 229, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1997, 71, 290, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1998, 71, 137, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1999, 71, 89, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2000, 71, 165, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2001, 71, 163, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2002, 71, 188, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2003, 71, 485, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2004, 71, 579, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2005, 71, 662, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2006, 71, 253, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1693, 68, 270, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1694, 68, 143, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1695, 68, 93, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1696, 68, 212, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1697, 68, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1698, 68, 229, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2137, 78, 322, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2138, 78, 101, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2139, 78, 62, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2140, 78, 212, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2141, 78, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2142, 78, 231, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2163, 69, 288, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2164, 69, 122, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2165, 69, 74, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2166, 69, 163, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2167, 69, 163, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2168, 69, 188, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2169, 69, 454, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2170, 69, 600, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2171, 69, 457, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2172, 69, 259, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2193, 70, 287, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2194, 70, 122, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2195, 70, 74, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2196, 70, 179, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2197, 70, 179, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2198, 70, 201, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2199, 70, 449, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2200, 70, 576, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2201, 70, 457, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2202, 70, 259, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2205, 80, 272, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2206, 80, 187, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2207, 80, 542, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2208, 80, 212, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2209, 80, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2210, 80, 232, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2211, 80, 253, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1851, 74, 323, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1852, 74, 101, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1853, 74, 61, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1854, 74, 212, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1855, 74, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1886, 72, 311, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1887, 72, 143, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1888, 72, 93, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1889, 72, 166, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1890, 72, 166, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1891, 72, 201, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1892, 72, 567, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1893, 72, 606, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1894, 72, 688, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1895, 72, 254, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1896, 72, 409, 23790, 17);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1897, 72, 705, 4, 18);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1898, 75, 321, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1899, 75, 143, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1900, 75, 93, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1901, 75, 182, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1902, 75, 182, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1903, 75, 201, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1904, 75, 567, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1905, 75, 606, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1906, 75, 688, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1907, 75, 254, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1919, 76, 287, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1920, 76, 143, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1921, 76, 93, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1922, 76, 179, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1923, 76, 182, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1924, 76, 201, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1925, 76, 573, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1926, 76, 482, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1927, 76, 700, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1928, 76, 254, 1, 10);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1929, 77, 272, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1930, 77, 120, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1931, 77, 72, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1932, 77, 215, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1933, 77, 474, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1934, 77, 455, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1981, 73, 271, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1982, 73, 101, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1983, 73, 71, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1984, 73, 210, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1985, 73, 217, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1986, 73, 229, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1997, 71, 290, 1, 1);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1998, 71, 137, 1, 2);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (1999, 71, 89, 1, 3);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2000, 71, 165, 1, 4);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2001, 71, 163, 1, 5);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2002, 71, 188, 1, 6);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2003, 71, 485, 1, 7);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2004, 71, 579, 1, 8);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2005, 71, 662, 1, 9);
GO

INSERT INTO [dbo].[DT_CharItem] ([ISerial], [IOwnerID], [IItemID], [IStack], [IPos]) VALUES (2006, 71, 253, 1, 10);
GO

