/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_Servant
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (0, '기본형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_BASE');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (1, 'HP형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HP');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (2, 'MP형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_MP');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (3, '기타형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_ETC');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (4, 'HPMP형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HPMP');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (5, 'HP기타형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HPETC');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (6, 'MP기타형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_MPETC');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (7, 'HP형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HP_1');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (8, 'HP회복형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HP_2');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (9, 'MP형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_MP_1');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (10, 'MP회복형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_MP_2');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (11, '무게형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_ETC_1');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (12, '변신시간형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_ETC_2');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (13, 'HPMP형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HPMP_1');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (14, 'HPMP회복형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HPMP_2');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (15, 'HP무게형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HPETC_1');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (16, 'HP회복/변신형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_HPETC_2');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (17, 'MP/무게형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_MPETC_1');
GO

INSERT INTO [dbo].[TP_Servant] ([SType], [STypeName], [STypeNameKey]) VALUES (18, 'MP회복/변신형', 'RCLIENT_MSG_GUI_INVENTORY_SERVANT_TYPE_MPETC_2');
GO

