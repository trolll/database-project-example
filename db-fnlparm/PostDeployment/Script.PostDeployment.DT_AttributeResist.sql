/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AttributeResist
Date                  : 2023-10-07 09:09:33
*/


INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (6, 1, 1, 2, '1D3');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (7, 1, 2, 4, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (8, 1, 3, 6, '2D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (9, 1, 6, 24, '4D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (10, 2, 1, 3, '1D3');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (11, 2, 2, 6, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (12, 2, 3, 9, '1D9');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (13, 2, 5, 16, '2D8');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (14, 3, 1, 3, '1D3');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (15, 3, 2, 6, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (16, 3, 3, 9, '1D9');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (17, 3, 10, 1000, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (18, 4, 1, 2, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (19, 4, 2, 4, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (20, 4, 3, 6, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (21, 4, 10, 1000, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (22, 5, 1, 2, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (23, 5, 2, 4, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (24, 5, 3, 6, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (25, 5, 10, 1000, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (26, 6, 1, 2, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (27, 6, 2, 4, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (28, 6, 3, 6, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (29, 6, 10, 1000, '1D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (31, 3, 20, 20, '20');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (32, 1, 4, 12, '2D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (33, 1, 5, 16, '2D8');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (34, 2, 4, 12, '2D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (35, 3, 4, 12, '2D6');
GO

INSERT INTO [dbo].[DT_AttributeResist] ([AID], [AType], [ALevel], [ADamage], [ADiceDamage]) VALUES (36, 3, 5, 16, '2D8');
GO

