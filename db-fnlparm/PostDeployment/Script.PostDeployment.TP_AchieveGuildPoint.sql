/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_AchieveGuildPoint
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TP_AchieveGuildPoint] ([mRank], [mPoint], [mChoiceProb], [mLegendProb], [mEpicProb], [mRareProb], [mNormalProb]) VALUES (1, 92, 6, 23, 24, 26, 27);
GO

INSERT INTO [dbo].[TP_AchieveGuildPoint] ([mRank], [mPoint], [mChoiceProb], [mLegendProb], [mEpicProb], [mRareProb], [mNormalProb]) VALUES (2, 80, 13, 16, 22, 30, 32);
GO

INSERT INTO [dbo].[TP_AchieveGuildPoint] ([mRank], [mPoint], [mChoiceProb], [mLegendProb], [mEpicProb], [mRareProb], [mNormalProb]) VALUES (3, 65, 20, 10, 18, 32, 40);
GO

INSERT INTO [dbo].[TP_AchieveGuildPoint] ([mRank], [mPoint], [mChoiceProb], [mLegendProb], [mEpicProb], [mRareProb], [mNormalProb]) VALUES (4, 46, 27, 5, 15, 34, 46);
GO

INSERT INTO [dbo].[TP_AchieveGuildPoint] ([mRank], [mPoint], [mChoiceProb], [mLegendProb], [mEpicProb], [mRareProb], [mNormalProb]) VALUES (5, 1, 34, 2, 10, 38, 50);
GO

