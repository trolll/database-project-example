/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblArenaTower
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.263', 750, 515442.71875, 14345.376953, 1277144.375, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 0);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.250', 751, 524019.15625, 14345.376953, 1277156.5, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 1);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.313', 753, 578457.0625, 14345.376953, 1277155.0, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 0);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.303', 754, 587031.4375, 14345.376953, 1277154.625, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 1);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.350', 756, 641471.25, 14345.376953, 1277159.0, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 0);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.353', 757, 650046.25, 14345.376953, 1277155.5, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 1);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.433', 759, 704487.1875, 14345.376953, 1277154.625, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 0);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.423', 760, 713062.5625, 14345.376953, 1277155.375, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 1);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.433', 762, 767504.0, 14345.376953, 1277152.375, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 0);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.443', 763, 776078.3125, 14345.376953, 1277154.625, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 1);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.517', 765, 830519.125, 14345.376953, 1277154.875, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 0);
GO

INSERT INTO [dbo].[TblArenaTower] ([mRegDate], [mNo], [mPosX], [mPosY], [mPosZ], [mWidth], [mDir], [mDesc], [mGroup]) VALUES ('2016-09-07 15:04:39.523', 766, 839094.4375, 14345.376953, 1277152.25, 430.061279, 4.712389, 'C1_Chaos_Battlefield_guardianstone.r3m            ', 1);
GO

