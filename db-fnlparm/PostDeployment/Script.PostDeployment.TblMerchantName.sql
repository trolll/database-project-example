/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMerchantName
Date                  : 2023-10-07 09:09:23
*/


INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (1, '에쉬번_잡화상인                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (2, '에쉬번_무구상인                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (3, '마법충전                                              ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (11, '테스트                                               ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (12, '떠돌이상인                                             ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (13, '기네아잡화상                                            ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (14, '기네아무기상                                            ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (15, '무구_매입목록                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (16, '잡화_매입목록                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (17, '던전상인                                              ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (18, '레이싱표상인                                            ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (19, '보석상인                                              ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (20, '독상인                                               ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (21, '홀상인                                               ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (22, '길드하우스상인                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (23, '폭죽상인                                              ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (24, '거미숲                                               ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (25, '놀아지트                                              ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (26, '화염의 탑                                             ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (27, '어두운 동굴                                            ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (28, '머맨 서식지                                            ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (29, '오크캠프                                              ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (30, '코볼트캠프                                             ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (31, '흑룡의 늪                                             ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (32, '혼돈의 신전                                            ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (33, '질서의 신전                                            ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (34, '하피의 둥지                                            ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (35, '왕의 무덤                                             ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (36, '요정의 언덕                                            ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (37, '딱정벌레구덩이                                           ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (38, '스톤해머                                              ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (39, '하스트의 제단                                           ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (40, '에기르의 수중동굴                                         ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (41, '고대도시 나르투                                          ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (42, '푸리에성                                              ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (43, '블랙랜드성                                             ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (44, '바이런성                                              ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (45, '로덴성                                               ', 1, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (46, '로덴마을상점                                            ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (47, 'PC방                                               ', 2, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (48, '메테오스 레어 캠프 무구                                     ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (49, '메테오스 레어 떠돌이상인                                     ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (50, '메테오스 레어 캠프 잡화                                     ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (51, '한중공성대전                                            ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (52, '스킬북상인 판매                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (53, '재료상인 판매                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (54, '재료상인 매입                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (55, '마법책상인 판매                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (56, '마법책상인 매입                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (57, '스킬북상인 매입                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (58, '이벤트_폭죽상인                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (59, '이벤트_큐피트                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (60, '이벤트_빼빼로                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (61, '아크라 잡화상                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (62, '카오스배틀-명예                                          ', 0, 1);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (63, '본섭-명예                                             ', 0, 1);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (64, '카오스배틀-장비                                          ', 0, 1);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (65, '카오스배틀-카오스                                         ', 0, 2);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (66, '이상한던전판매                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (67, '본섭-카오스                                            ', 0, 2);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (68, '[중고랩]나우킬                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (69, '[중고랩]나마르                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (70, '[중고랩]나이쉘                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (71, '[중고랩]누이빌,누위브                                      ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (72, '길드하우스_스팟상인                                        ', 3, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (73, '길드하우스_성상인                                         ', 4, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (74, '길드하우스_잡화상인                                        ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (75, '프리미엄서비스도우미                                        ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (76, '통합_매입목록                                           ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (77, '카오스배틀-보급상인                                        ', 0, 3);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (78, '큐브상인                                              ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (79, '엘테르_잡화상인                                          ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (80, '엘테르 성아지트 상인                                       ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (81, '엘테르_떠돌이상인                                         ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (82, '카오스물약_상인                                          ', 0, 2);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (83, '[만월]떠돌이 상인                                        ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (84, '이계의 장사꾼 이프리트                                      ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (85, '이계의 장사꾼 바포메트                                      ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (86, '이계의 장사꾼 메테오스                                      ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (87, '이계의 장사꾼 공용 1번                                     ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (88, '이계의 장사꾼 공용 2번                                     ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (89, '이계의 장사꾼 공용 3번                                     ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (90, '[만월]떠돌이 상인2                                       ', 0, 0);
GO

INSERT INTO [dbo].[TblMerchantName] ([mID], [mDesc], [mShopType], [mPaymentType]) VALUES (91, '떠돌이 교환상인                                          ', 0, 2);
GO

