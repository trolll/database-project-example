/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AbnormalAttrApply
Date                  : 2023-10-07 09:07:30
*/


INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (1, 0, 243, 2012, '특정 상태이상 적용(바포메트) 중, 골렘 소환 (바포메트 전용) 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (2, 0, 244, 2012, '특정 상태이상 적용(바포메트) 중, 시전모션(바포메트 전용) 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (3, 0, 245, 2012, '특정 상태이상 적용(바포메트) 중, 실패 모션 (바포메트 전용) 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (4, 0, 246, 2012, '특정 상태이상 적용(바포메트) 중, 성공 모션 (바포메트 전용) 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (5, 0, 247, 2012, '특정 상태이상 적용(바포메트) 중, 공격 스킬 시전 (바포메트 전용) 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (6, 2051, 0, 2012, '특정 상태이상 적용(바포메트) 중, 골렘 소환 방어도 상승 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (7, 2052, 0, 2012, '특정 상태이상 적용(바포메트) 중, 골렘 소환 방어도 상승 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (8, 2053, 0, 2012, '특정 상태이상 적용(바포메트) 중, 골렘 소환 방어도 상승 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (9, 2054, 0, 2012, '특정 상태이상 적용(바포메트) 중, 골렘 소환 방어도 상승 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (10, 2055, 0, 2012, '특정 상태이상 적용(바포메트) 중, 골렘 소환 방어도 상승 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (11, 2056, 0, 2012, '특정 상태이상 적용(바포메트) 중, 골렘 소환 방어도 상승 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (12, 0, 16, 2012, '특정 상태이상 적용(바포메트) 중, 사일런스 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (13, 0, 99, 2012, '특정 상태이상 적용(바포메트) 중, 스운 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (14, 0, 100, 2012, '특정 상태이상 적용(바포메트) 중, 무적 통과');
GO

INSERT INTO [dbo].[DT_AbnormalAttrApply] ([mID], [mOriginAID], [mOriginAType], [mConditionAID], [mDesc]) VALUES (15, 0, 118, 2012, '특정 상태이상 적용(바포메트) 중, 아머 브레이크 통과');
GO

