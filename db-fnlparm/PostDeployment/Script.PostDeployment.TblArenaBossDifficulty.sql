/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblArenaBossDifficulty
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (1, 1, 460, 80, 80, 100, 138, 100, 100, 100, 100, 100, 100, 60);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (2, 461, 630, 80, 80, 100, 195, 100, 100, 100, 100, 100, 100, 60);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (3, 631, 700, 80, 80, 100, 251, 100, 100, 100, 100, 100, 100, 60);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (4, 701, 750, 82, 82, 100, 268, 100, 100, 100, 100, 100, 100, 60);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (5, 751, 800, 84, 84, 100, 284, 100, 100, 100, 100, 100, 100, 60);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (6, 801, 852, 86, 86, 100, 300, 100, 100, 100, 100, 100, 100, 60);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (7, 853, 882, 88, 88, 100, 316, 100, 100, 100, 100, 100, 100, 61);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (8, 883, 920, 90, 90, 100, 331, 100, 100, 100, 100, 100, 100, 62);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (9, 921, 954, 91, 91, 100, 347, 100, 100, 100, 100, 100, 100, 63);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (10, 955, 994, 92, 92, 100, 362, 100, 100, 100, 100, 100, 100, 64);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (11, 995, 1020, 93, 93, 100, 403, 100, 100, 100, 100, 100, 100, 65);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (12, 1021, 1050, 94, 94, 100, 459, 100, 100, 100, 100, 100, 100, 65);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (13, 1051, 1092, 95, 95, 100, 542, 100, 100, 100, 100, 100, 100, 66);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (14, 1093, 1130, 96, 96, 100, 601, 100, 100, 100, 100, 100, 100, 66);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (15, 1131, 1140, 97, 97, 100, 663, 100, 100, 100, 100, 100, 100, 67);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (16, 1141, 1194, 98, 98, 100, 745, 100, 100, 100, 100, 100, 100, 67);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (17, 1195, 1248, 99, 99, 100, 820, 100, 100, 100, 100, 100, 100, 68);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (18, 1249, 1280, 100, 100, 100, 894, 100, 100, 100, 100, 100, 100, 68);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (19, 1281, 1350, 102, 102, 100, 974, 100, 100, 100, 100, 100, 100, 69);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (20, 1351, 1400, 104, 104, 100, 1051, 100, 100, 100, 100, 100, 100, 69);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (21, 1401, 1458, 106, 106, 100, 1148, 100, 100, 100, 100, 100, 100, 70);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (22, 1459, 1530, 108, 108, 100, 1232, 100, 100, 100, 100, 100, 100, 70);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (23, 1531, 1584, 110, 110, 100, 1325, 100, 100, 100, 100, 100, 100, 71);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (24, 1585, 1656, 112, 112, 100, 1449, 100, 100, 100, 100, 100, 100, 72);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (25, 1657, 1755, 114, 114, 100, 1605, 100, 100, 100, 100, 100, 100, 73);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (26, 1756, 1850, 116, 116, 100, 1821, 100, 100, 100, 100, 100, 100, 74);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (27, 1851, 1980, 118, 118, 100, 2132, 100, 100, 100, 100, 100, 100, 75);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (28, 1981, 2120, 120, 120, 100, 2249, 100, 100, 100, 100, 100, 100, 76);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (29, 2121, 2630, 125, 125, 100, 2365, 100, 100, 100, 100, 100, 100, 78);
GO

INSERT INTO [dbo].[TblArenaBossDifficulty] ([mPointGrade], [mMinPoint], [mMaxPoint], [mMinD], [mMaxD], [mHIT], [mHP], [mDPV], [mMPV], [mRPV], [mDDV], [mMDV], [mRDV], [mSkillDmg]) VALUES (30, 2631, 99999, 130, 130, 100, 2614, 100, 100, 100, 100, 100, 100, 80);
GO

