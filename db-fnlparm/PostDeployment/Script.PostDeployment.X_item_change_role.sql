/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : X_item_change_role
Date                  : 2023-10-07 09:07:22
*/


SET IDENTITY_INSERT [dbo].[X_item_change_role] ON;

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1, 41, 42);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2, 42, 43);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3, 43, 44);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (4, 61, 512);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (5, 62, 927);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (6, 71, 72);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (7, 71, 4681);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (8, 71, 2694);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (9, 72, 73);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (10, 73, 74);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (11, 74, 75);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (12, 75, 77);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (13, 77, 78);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (14, 78, 79);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (15, 82, 83);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (16, 82, 2704);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (17, 82, 4659);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (18, 83, 85);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (19, 85, 86);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (20, 86, 87);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (21, 87, 88);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (22, 88, 89);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (23, 89, 90);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (24, 91, 90);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (25, 101, 102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (26, 102, 103);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (27, 103, 104);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (28, 104, 105);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (29, 119, 120);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (30, 120, 121);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (31, 121, 122);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (32, 122, 123);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (33, 123, 124);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (34, 124, 125);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (35, 130, 131);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (36, 130, 2145);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (37, 131, 132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (38, 132, 133);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (39, 133, 135);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (40, 135, 136);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (41, 136, 137);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (42, 137, 138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (43, 138, 139);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (44, 139, 143);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (45, 164, 1079);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (46, 166, 1516);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (47, 167, 1514);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (48, 169, 1518);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (49, 179, 1512);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (50, 182, 5644);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (51, 183, 184);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (52, 210, 437);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (53, 210, 2135);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (54, 210, 2125);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (55, 210, 2900);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (56, 210, 4789);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (57, 215, 445);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (58, 216, 447);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (59, 220, 648);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (60, 223, 628);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (61, 229, 6800);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (62, 229, 692);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (63, 234, 1186);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (64, 237, 460);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (65, 259, 2481);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (66, 261, 262);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (67, 262, 263);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (68, 263, 264);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (69, 264, 265);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (70, 281, 282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (71, 282, 283);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (72, 283, 284);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (73, 284, 285);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (74, 285, 286);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (75, 286, 287);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (76, 287, 288);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (77, 288, 289);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (78, 291, 292);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (79, 292, 293);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (80, 293, 294);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (81, 294, 295);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (82, 295, 296);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (83, 296, 297);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (84, 297, 298);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (85, 298, 299);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (86, 301, 1190);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (87, 301, 1189);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (88, 302, 303);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (89, 303, 304);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (90, 304, 305);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (91, 305, 306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (92, 306, 307);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (93, 307, 308);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (94, 308, 309);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (95, 309, 310);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (96, 312, 313);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (97, 313, 314);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (98, 314, 315);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (99, 315, 316);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (100, 316, 317);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (101, 326, 978);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (102, 371, 474);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (103, 371, 2095);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (104, 371, 4717);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (105, 371, 2105);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (106, 418, 4508);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (107, 419, 4489);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (108, 421, 2531);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (109, 437, 438);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (110, 438, 449);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (111, 445, 446);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (112, 446, 453);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (113, 447, 448);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (114, 448, 454);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (115, 449, 483);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (116, 453, 550);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (117, 455, 456);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (118, 456, 457);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (119, 457, 237);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (120, 460, 461);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (121, 461, 462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (122, 461, 460);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (123, 462, 463);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (124, 464, 463);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (125, 465, 455);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (126, 465, 2075);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (127, 465, 2085);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (128, 465, 4753);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (129, 474, 475);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (130, 475, 476);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (131, 476, 477);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (132, 477, 478);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (133, 478, 479);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (134, 479, 480);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (135, 480, 481);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (136, 483, 484);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (137, 484, 485);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (138, 485, 486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (139, 486, 487);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (140, 487, 488);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (141, 498, 7241);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (142, 512, 513);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (143, 555, 215);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (144, 598, 599);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (145, 599, 600);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (146, 600, 601);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (147, 601, 602);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (148, 608, 598);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (149, 628, 629);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (150, 629, 630);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (151, 630, 631);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (152, 648, 649);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (153, 649, 650);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (154, 650, 651);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (155, 651, 652);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (156, 692, 693);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (157, 693, 694);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (158, 694, 695);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (159, 695, 696);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (160, 704, 2523);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (161, 717, 6793);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (162, 717, 1500);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (163, 718, 1198);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (164, 721, 724);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (165, 727, 728);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (166, 728, 729);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (167, 729, 730);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (168, 730, 731);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (169, 731, 732);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (170, 732, 733);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (171, 733, 734);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (172, 734, 735);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (173, 741, 742);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (174, 742, 743);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (175, 757, 758);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (176, 758, 759);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (177, 759, 760);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (178, 760, 761);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (179, 761, 762);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (180, 762, 763);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (181, 763, 764);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (182, 764, 765);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (183, 782, 1503);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (184, 784, 785);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (185, 785, 786);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (186, 786, 787);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (187, 787, 788);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (188, 788, 789);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (189, 789, 790);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (190, 790, 791);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (191, 791, 792);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (192, 795, 881);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (193, 797, 846);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (194, 832, 4450);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (195, 833, 4454);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (196, 834, 4479);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (197, 834, 2502);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (198, 837, 2501);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (199, 837, 4459);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (200, 841, 2515);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (201, 846, 890);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (202, 881, 882);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (203, 882, 883);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (204, 883, 884);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (205, 890, 891);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (206, 891, 892);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (207, 892, 893);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (208, 893, 894);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (209, 894, 895);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (210, 895, 896);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (211, 898, 1192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (212, 900, 1195);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (213, 916, 2497);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (214, 945, 1183);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (215, 946, 1180);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (216, 949, 1059);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (217, 950, 1062);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (218, 988, 1171);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (219, 1025, 4446);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (220, 1055, 1135);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (221, 1056, 1136);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (222, 1059, 1060);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (223, 1060, 1061);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (224, 1061, 1100);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (225, 1072, 1081);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (226, 1074, 1087);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (227, 1075, 1085);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (228, 1076, 1083);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (229, 1077, 182);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (230, 1078, 1086);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (231, 1079, 1078);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (232, 1081, 1077);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (233, 1083, 183);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (234, 1088, 1072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (235, 1088, 1076);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (236, 1088, 164);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (237, 1096, 1462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (238, 1100, 1101);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (239, 1101, 1102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (240, 1132, 1137);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (241, 1133, 1146);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (242, 1134, 1155);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (243, 1137, 1138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (244, 1138, 1139);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (245, 1139, 1140);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (246, 1140, 1141);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (247, 1141, 1142);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (248, 1142, 1143);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (249, 1146, 1147);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (250, 1147, 1148);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (251, 1148, 1149);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (252, 1149, 1150);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (253, 1150, 1151);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (254, 1151, 1152);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (255, 1152, 1153);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (256, 1155, 1156);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (257, 1156, 1157);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (258, 1157, 1158);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (259, 1158, 1159);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (260, 1171, 1172);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (261, 1172, 1173);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (262, 1173, 1777);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (263, 1173, 1767);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (264, 1176, 1177);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (265, 1177, 1178);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (266, 1178, 1179);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (267, 1180, 1181);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (268, 1181, 1182);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (269, 1183, 1184);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (270, 1184, 1185);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (271, 1186, 1187);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (272, 1187, 1188);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (273, 1192, 1193);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (274, 1193, 1194);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (275, 1195, 1196);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (276, 1196, 1197);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (277, 1198, 1199);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (278, 1246, 7191);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (279, 1268, 1269);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (280, 1269, 1270);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (281, 1270, 1271);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (282, 1271, 1272);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (283, 1272, 1273);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (284, 1278, 1279);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (285, 1279, 1280);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (286, 1280, 1281);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (287, 1281, 1282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (288, 1282, 1283);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (289, 1283, 1284);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (290, 1284, 1285);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (291, 1312, 1313);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (292, 1313, 1314);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (293, 1314, 1315);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (294, 1315, 1316);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (295, 1316, 1317);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (296, 1317, 1318);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (297, 1322, 1323);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (298, 1323, 1324);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (299, 1324, 1325);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (300, 1325, 1326);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (301, 1326, 1327);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (302, 1327, 1328);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (303, 1332, 1333);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (304, 1333, 1334);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (305, 1334, 1335);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (306, 1335, 1336);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (307, 1336, 1337);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (308, 1337, 1338);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (309, 1338, 1339);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (310, 1339, 1340);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (311, 1340, 1341);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (312, 1352, 1353);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (313, 1353, 1354);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (314, 1354, 1355);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (315, 1355, 1356);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (316, 1364, 1365);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (317, 1365, 1366);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (318, 1366, 1367);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (319, 1367, 1368);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (320, 1368, 1369);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (321, 1369, 1370);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (322, 1374, 1375);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (323, 1375, 1376);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (324, 1376, 1377);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (325, 1377, 1378);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (326, 1378, 1379);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (327, 1379, 1380);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (328, 1380, 1381);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (329, 1412, 1413);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (330, 1413, 1414);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (331, 1414, 1415);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (332, 1415, 1416);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (333, 1416, 1417);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (334, 1417, 1418);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (335, 1418, 1419);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (336, 1419, 1420);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (337, 1422, 1423);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (338, 1423, 1424);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (339, 1424, 1425);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (340, 1425, 1426);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (341, 1426, 1427);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (342, 1427, 1428);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (343, 1428, 1429);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (344, 1429, 1440);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (345, 1430, 1431);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (346, 1431, 1432);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (347, 1432, 1433);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (348, 1433, 1434);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (349, 1434, 1435);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (350, 1435, 1436);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (351, 1436, 1437);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (352, 1437, 1438);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (353, 1438, 1439);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (354, 1442, 1443);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (355, 1443, 1444);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (356, 1444, 1445);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (357, 1445, 1446);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (358, 1446, 1447);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (359, 1462, 1463);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (360, 1463, 1464);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (361, 1464, 1465);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (362, 1465, 1466);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (363, 1466, 1467);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (364, 1467, 1468);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (365, 1468, 1469);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (366, 1469, 1470);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (367, 1492, 1497);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (368, 1500, 1501);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (369, 1501, 1502);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (370, 1503, 1504);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (371, 1512, 1513);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (372, 1518, 1519);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (373, 1524, 1890);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (374, 1525, 1899);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (375, 1526, 1879);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (376, 1529, 1926);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (377, 1530, 1917);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (378, 1532, 2185);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (379, 1532, 2195);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (380, 1532, 1908);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (381, 1541, 1542);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (382, 1542, 1543);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (383, 1543, 1544);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (384, 1544, 1545);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (385, 1551, 1552);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (386, 1552, 1553);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (387, 1553, 1554);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (388, 1554, 1555);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (389, 1555, 1556);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (390, 1561, 1562);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (391, 1561, 4807);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (392, 1562, 1563);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (393, 1563, 1564);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (394, 1564, 1565);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (395, 1565, 1566);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (396, 1571, 1572);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (397, 1572, 1573);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (398, 1573, 1574);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (399, 1574, 1575);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (400, 1575, 1576);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (401, 1582, 5682);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (402, 1582, 6534);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (403, 1583, 5682);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (404, 1583, 6534);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (405, 1584, 5682);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (406, 1584, 6534);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (407, 1627, 1628);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (408, 1628, 1629);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (409, 1629, 1630);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (410, 1630, 1631);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (411, 1631, 1632);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (412, 1632, 1633);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (413, 1633, 1634);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (414, 1634, 1635);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (415, 1637, 1638);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (416, 1638, 1639);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (417, 1639, 1640);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (418, 1640, 1641);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (419, 1641, 1642);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (420, 1642, 1643);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (421, 1643, 1644);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (422, 1644, 1645);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (423, 1645, 1644);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (424, 1647, 1648);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (425, 1648, 1649);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (426, 1649, 1650);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (427, 1650, 1651);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (428, 1651, 1652);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (429, 1652, 1653);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (430, 1653, 1654);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (431, 1657, 1658);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (432, 1658, 1659);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (433, 1659, 1660);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (434, 1660, 1661);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (435, 1661, 1662);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (436, 1662, 1663);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (437, 1663, 1664);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (438, 1664, 1665);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (439, 1665, 1666);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (440, 1667, 1668);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (441, 1668, 1669);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (442, 1669, 1670);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (443, 1670, 1671);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (444, 1671, 1672);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (445, 1672, 1673);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (446, 1673, 1674);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (447, 1674, 1675);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (448, 1677, 1678);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (449, 1678, 1679);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (450, 1679, 1680);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (451, 1680, 1681);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (452, 1681, 1682);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (453, 1682, 1683);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (454, 1683, 1684);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (455, 1684, 1685);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (456, 1685, 1686);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (457, 1687, 1688);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (458, 1688, 1689);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (459, 1689, 1690);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (460, 1690, 1691);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (461, 1691, 1692);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (462, 1692, 1693);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (463, 1693, 1694);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (464, 1694, 1695);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (465, 1697, 1698);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (466, 1698, 1699);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (467, 1699, 1700);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (468, 1700, 1701);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (469, 1701, 1702);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (470, 1702, 1703);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (471, 1703, 1704);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (472, 1707, 1708);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (473, 1708, 1709);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (474, 1709, 1710);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (475, 1710, 1711);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (476, 1711, 1712);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (477, 1712, 1713);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (478, 1714, 1715);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (479, 1717, 1718);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (480, 1718, 1719);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (481, 1719, 1720);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (482, 1720, 1721);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (483, 1721, 1722);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (484, 1722, 1723);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (485, 1723, 1724);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (486, 1724, 1725);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (487, 1727, 1728);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (488, 1728, 1729);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (489, 1729, 1730);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (490, 1730, 1731);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (491, 1731, 1732);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (492, 1807, 1808);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (493, 1808, 1809);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (494, 1809, 1810);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (495, 1879, 1880);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (496, 1880, 1881);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (497, 1881, 1882);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (498, 1882, 1883);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (499, 1883, 1884);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (500, 1890, 1891);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (501, 1891, 1892);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (502, 1892, 1893);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (503, 1893, 1894);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (504, 1899, 1900);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (505, 1900, 1901);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (506, 1901, 1902);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (507, 1902, 1903);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (508, 1903, 1904);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (509, 1917, 1918);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (510, 1918, 1919);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (511, 1919, 1920);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (512, 1920, 1921);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (513, 1921, 1922);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (514, 1926, 1927);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (515, 1927, 1928);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (516, 1928, 1929);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (517, 1941, 6534);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (518, 1941, 5682);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (519, 2002, 2003);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (520, 2003, 2004);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (521, 2004, 2005);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (522, 2005, 2006);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (523, 2006, 2007);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (524, 2012, 2013);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (525, 2013, 2014);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (526, 2014, 2015);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (527, 2015, 2016);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (528, 2016, 2017);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (529, 2017, 2018);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (530, 2018, 2019);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (531, 2019, 2020);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (532, 2020, 2021);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (533, 2075, 2076);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (534, 2076, 2077);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (535, 2077, 2078);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (536, 2078, 2079);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (537, 2079, 2080);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (538, 2080, 2081);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (539, 2081, 2082);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (540, 2085, 2086);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (541, 2086, 2087);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (542, 2087, 2088);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (543, 2088, 2089);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (544, 2089, 2090);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (545, 2095, 2096);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (546, 2096, 2097);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (547, 2097, 2098);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (548, 2098, 2099);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (549, 2099, 2100);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (550, 2100, 2101);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (551, 2105, 2106);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (552, 2106, 2107);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (553, 2107, 2108);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (554, 2108, 2109);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (555, 2109, 2110);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (556, 2110, 2111);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (557, 2115, 2116);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (558, 2116, 2117);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (559, 2117, 2118);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (560, 2118, 2119);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (561, 2119, 2120);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (562, 2120, 2121);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (563, 2122, 2123);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (564, 2125, 2126);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (565, 2126, 2127);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (566, 2127, 2128);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (567, 2128, 2129);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (568, 2129, 2130);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (569, 2130, 2131);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (570, 2131, 2132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (571, 2132, 2133);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (572, 2135, 2136);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (573, 2136, 2137);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (574, 2137, 2138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (575, 2138, 2139);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (576, 2139, 2140);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (577, 2140, 2141);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (578, 2145, 2146);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (579, 2146, 2147);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (580, 2147, 2148);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (581, 2148, 2149);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (582, 2149, 2150);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (583, 2150, 2151);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (584, 2151, 2150);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (585, 2151, 2152);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (586, 2165, 2166);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (587, 2166, 2167);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (588, 2167, 2168);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (589, 2175, 2176);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (590, 2176, 2177);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (591, 2177, 2178);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (592, 2178, 2179);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (593, 2185, 2186);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (594, 2186, 2187);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (595, 2187, 2188);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (596, 2188, 2189);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (597, 2189, 2190);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (598, 2190, 2191);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (599, 2191, 2192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (600, 2195, 2196);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (601, 2198, 2199);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (602, 2215, 2216);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (603, 2216, 2217);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (604, 2217, 2218);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (605, 2218, 2219);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (606, 2220, 2221);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (607, 2225, 2226);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (608, 2226, 2227);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (609, 2227, 2228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (610, 2228, 2229);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (611, 2229, 2230);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (612, 2230, 2231);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (613, 2235, 2236);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (614, 2236, 2237);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (615, 2237, 2238);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (616, 2238, 2239);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (617, 2239, 2240);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (618, 2265, 2266);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (619, 2266, 2267);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (620, 2267, 2268);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (621, 2285, 2286);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (622, 2286, 2287);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (623, 2287, 2288);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (624, 2305, 2306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (625, 2306, 2307);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (626, 2307, 2308);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (627, 2308, 2309);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (628, 2309, 2310);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (629, 2310, 2311);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (630, 2311, 2312);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (631, 2315, 2316);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (632, 2316, 2317);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (633, 2317, 2318);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (634, 2318, 2319);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (635, 2319, 2320);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (636, 2320, 2321);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (637, 2325, 2326);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (638, 2326, 2327);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (639, 2327, 2328);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (640, 2328, 2329);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (641, 2329, 2330);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (642, 2330, 2331);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (643, 2331, 2332);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (644, 2332, 2333);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (645, 2335, 2336);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (646, 2336, 2337);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (647, 2337, 2338);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (648, 2338, 2339);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (649, 2339, 2340);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (650, 2340, 2341);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (651, 2341, 2342);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (652, 2342, 2343);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (653, 2345, 2346);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (654, 2346, 2347);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (655, 2347, 2348);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (656, 2348, 2349);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (657, 2355, 2356);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (658, 2356, 2357);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (659, 2365, 2366);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (660, 2405, 2406);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (661, 2406, 2407);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (662, 2407, 2408);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (663, 2408, 2409);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (664, 2466, 2467);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (665, 2467, 7658);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (666, 2474, 2475);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (667, 2481, 2482);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (668, 2484, 2485);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (669, 2485, 2486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (670, 2497, 4483);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (671, 2500, 4456);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (672, 2502, 4481);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (673, 2523, 2524);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (674, 2524, 2525);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (675, 2531, 2532);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (676, 2532, 2533);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (677, 2615, 2616);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (678, 2616, 2617);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (679, 2617, 2618);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (680, 2618, 2619);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (681, 2639, 2640);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (682, 2640, 2641);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (683, 2641, 2642);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (684, 2642, 2643);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (685, 2643, 2644);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (686, 2644, 2645);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (687, 2645, 2646);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (688, 2646, 2647);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (689, 2649, 2650);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (690, 2650, 2651);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (691, 2651, 2652);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (692, 2652, 2653);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (693, 2653, 2654);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (694, 2654, 2655);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (695, 2655, 2656);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (696, 2656, 2657);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (697, 2659, 2660);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (698, 2660, 2661);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (699, 2661, 2662);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (700, 2662, 2663);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (701, 2663, 2664);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (702, 2664, 2665);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (703, 2665, 2666);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (704, 2666, 2667);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (705, 2669, 2670);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (706, 2670, 2671);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (707, 2671, 2672);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (708, 2672, 2673);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (709, 2673, 2674);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (710, 2674, 2675);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (711, 2675, 2676);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (712, 2679, 2680);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (713, 2680, 2681);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (714, 2681, 2682);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (715, 2682, 2683);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (716, 2683, 2684);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (717, 2684, 2685);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (718, 2685, 2686);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (719, 2686, 2687);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (720, 2687, 2688);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (721, 2694, 2695);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (722, 2695, 2696);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (723, 2696, 2697);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (724, 2697, 2698);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (725, 2698, 2699);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (726, 2704, 2705);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (727, 2705, 2706);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (728, 2706, 2707);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (729, 2707, 2708);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (730, 2708, 2709);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (731, 2709, 2710);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (732, 2710, 2711);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (733, 2737, 2738);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (734, 2738, 2739);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (735, 2739, 2740);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (736, 2740, 2741);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (737, 2741, 2742);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (738, 2742, 2743);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (739, 2743, 2744);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (740, 2744, 2745);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (741, 2745, 2746);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (742, 2747, 2748);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (743, 2748, 2749);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (744, 2749, 2750);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (745, 2750, 2751);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (746, 2757, 2758);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (747, 2758, 2759);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (748, 2759, 2760);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (749, 2760, 2761);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (750, 2761, 2762);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (751, 2762, 2763);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (752, 2900, 2901);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (753, 2901, 2902);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (754, 2902, 2903);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (755, 2903, 2904);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (756, 2904, 2905);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (757, 3400, 3401);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (758, 3401, 3402);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (759, 3402, 3403);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (760, 3403, 3404);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (761, 3410, 3411);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (762, 3411, 3412);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (763, 3412, 3413);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (764, 3413, 3414);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (765, 3430, 3431);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (766, 3434, 3430);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (767, 3686, 3687);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (768, 3687, 3688);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (769, 3688, 3689);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (770, 3689, 3690);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (771, 3690, 3691);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (772, 3691, 3692);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (773, 3692, 3693);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (774, 3728, 3729);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (775, 3729, 3730);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (776, 3730, 3731);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (777, 3731, 3732);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (778, 3738, 3739);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (779, 3739, 3740);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (780, 3740, 3741);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (781, 3741, 3742);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (782, 3752, 3753);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (783, 3766, 3767);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (784, 3767, 3768);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (785, 3768, 3769);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (786, 3769, 3770);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (787, 3780, 3781);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (788, 3781, 3782);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (789, 3782, 3783);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (790, 3783, 3784);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (791, 3784, 3785);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (792, 3785, 3786);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (793, 3786, 3787);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (794, 3787, 3788);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (795, 3794, 3795);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (796, 3795, 3796);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (797, 3796, 3797);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (798, 3797, 3798);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (799, 3798, 3799);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (800, 3799, 3800);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (801, 3808, 3809);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (802, 3809, 3810);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (803, 3810, 3811);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (804, 3811, 3812);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (805, 3812, 3813);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (806, 3813, 3814);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (807, 3832, 3833);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (808, 3833, 3834);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (809, 3834, 3835);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (810, 3835, 3836);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (811, 3836, 3837);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (812, 3837, 3838);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (813, 3842, 3843);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (814, 3843, 3844);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (815, 3844, 3845);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (816, 3845, 3846);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (817, 3846, 3847);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (818, 3852, 3853);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (819, 3853, 3854);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (820, 3854, 3855);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (821, 3862, 3863);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (822, 3863, 3864);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (823, 3864, 3865);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (824, 3865, 3866);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (825, 3866, 3867);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (826, 3867, 3868);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (827, 3868, 3869);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (828, 3869, 3870);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (829, 3870, 3871);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (830, 3876, 3877);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (831, 3877, 3878);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (832, 3878, 3879);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (833, 3879, 3880);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (834, 3880, 3881);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (835, 3881, 3882);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (836, 3882, 3883);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (837, 3883, 3884);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (838, 3890, 3891);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (839, 3891, 3892);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (840, 3892, 3893);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (841, 3893, 3894);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (842, 3894, 3895);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (843, 3895, 3896);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (844, 3904, 3905);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (845, 3905, 3906);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (846, 3906, 3907);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (847, 3907, 3908);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (848, 3908, 3909);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (849, 3909, 3910);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (850, 3918, 3919);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (851, 3919, 3920);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (852, 3920, 3921);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (853, 3921, 3922);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (854, 3922, 3923);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (855, 3923, 3924);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (856, 3924, 3925);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (857, 3932, 3933);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (858, 3933, 3934);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (859, 3934, 3935);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (860, 3935, 3936);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (861, 3936, 3937);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (862, 3937, 3938);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (863, 3946, 3947);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (864, 3947, 3948);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (865, 3948, 3949);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (866, 3949, 3950);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (867, 3950, 3951);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (868, 3951, 3952);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (869, 3952, 3953);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (870, 3953, 3954);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (871, 4446, 4447);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (872, 4447, 4449);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (873, 4450, 4451);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (874, 4454, 4455);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (875, 4459, 4460);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (876, 4479, 4480);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (877, 4479, 4481);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (878, 4480, 4482);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (879, 4484, 4487);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (880, 4484, 4485);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (881, 4489, 4490);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (882, 4508, 4509);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (883, 4509, 4510);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (884, 4659, 4660);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (885, 4660, 4661);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (886, 4661, 4662);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (887, 4662, 4663);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (888, 4663, 4664);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (889, 4664, 4665);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (890, 4681, 4682);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (891, 4682, 4683);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (892, 4683, 4684);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (893, 4684, 4685);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (894, 4685, 4686);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (895, 4699, 4700);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (896, 4700, 4701);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (897, 4701, 4702);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (898, 4702, 4703);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (899, 4703, 4704);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (900, 4704, 4705);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (901, 4705, 4706);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (902, 4706, 4707);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (903, 4717, 4718);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (904, 4718, 4719);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (905, 4719, 4720);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (906, 4720, 4721);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (907, 4721, 4722);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (908, 4735, 4736);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (909, 4736, 4737);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (910, 4737, 4738);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (911, 4738, 4739);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (912, 4739, 4740);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (913, 4739, 4738);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (914, 4740, 4741);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (915, 4753, 4754);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (916, 4754, 4755);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (917, 4755, 4756);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (918, 4756, 4757);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (919, 4757, 4758);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (920, 4758, 4759);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (921, 4759, 4760);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (922, 4771, 4772);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (923, 4772, 4773);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (924, 4773, 4774);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (925, 4774, 4775);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (926, 4775, 4776);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (927, 4789, 4790);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (928, 4790, 4791);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (929, 4791, 4792);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (930, 4792, 4793);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (931, 4794, 4795);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (932, 4807, 4808);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (933, 4808, 4809);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (934, 4809, 4810);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (935, 4810, 4811);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (936, 4811, 4812);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (937, 4812, 4813);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (938, 4813, 4814);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (939, 4890, 5682);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (940, 4890, 6534);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (941, 4906, 4907);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (942, 4907, 4908);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (943, 4929, 4930);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (944, 4933, 4934);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (945, 4934, 4935);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (946, 4941, 4942);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (947, 4945, 4946);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (948, 4949, 4950);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (949, 4957, 4958);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (950, 4960, 4961);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (951, 4963, 4964);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (952, 5212, 5213);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (953, 5213, 5214);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (954, 5214, 5215);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (955, 5215, 5216);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (956, 5216, 5217);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (957, 5226, 5227);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (958, 5227, 5228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (959, 5228, 5229);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (960, 5229, 5230);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (961, 5230, 5231);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (962, 5231, 5232);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (963, 5232, 5233);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (964, 5266, 5267);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (965, 5267, 5268);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (966, 5268, 5269);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (967, 5269, 5270);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (968, 5270, 5271);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (969, 5272, 5273);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (970, 5273, 5274);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (971, 5274, 5275);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (972, 5275, 5276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (973, 5276, 5277);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (974, 5284, 5285);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (975, 5285, 5286);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (976, 5286, 5287);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (977, 5287, 5288);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (978, 5288, 5289);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (979, 5290, 5291);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (980, 5291, 5292);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (981, 5292, 5293);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (982, 5293, 5294);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (983, 5294, 5295);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (984, 5296, 5297);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (985, 5297, 5298);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (986, 5298, 5299);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (987, 5302, 5303);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (988, 5303, 5304);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (989, 5304, 5305);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (990, 5308, 5309);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (991, 5309, 5310);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (992, 5310, 5311);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (993, 5314, 5315);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (994, 5315, 5316);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (995, 5316, 5317);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (996, 5318, 5319);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (997, 5319, 5320);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (998, 5320, 5321);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (999, 5653, 5654);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1000, 5654, 5655);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1001, 5655, 5656);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1002, 5667, 5668);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1003, 5668, 5669);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1004, 5669, 5670);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1005, 5670, 5671);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1006, 5723, 5724);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1007, 5724, 5725);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1008, 5725, 5726);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1009, 5726, 5727);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1010, 5727, 5728);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1011, 5728, 5729);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1012, 5737, 5738);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1013, 5738, 5739);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1014, 5739, 5740);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1015, 5740, 5741);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1016, 5741, 5742);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1017, 5742, 5743);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1018, 5751, 5752);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1019, 5752, 5753);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1020, 5753, 5754);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1021, 5754, 5755);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1022, 5755, 5756);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1023, 5765, 5766);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1024, 5766, 5767);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1025, 5767, 5768);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1026, 5768, 5769);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1027, 5769, 5770);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1028, 5770, 5771);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1029, 5771, 5772);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1030, 5779, 5780);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1031, 5780, 5781);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1032, 5781, 5782);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1033, 5782, 5783);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1034, 5783, 5784);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1035, 5784, 5785);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1036, 5793, 5794);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1037, 5794, 5795);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1038, 5795, 5796);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1039, 5807, 5808);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1040, 5808, 5809);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1041, 5809, 5810);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1042, 5821, 5822);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1043, 5822, 5823);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1044, 5823, 5824);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1045, 5825, 5826);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1046, 5835, 5836);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1047, 5836, 5837);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1048, 5863, 5864);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1049, 5864, 5865);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1050, 5865, 5866);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1051, 5866, 5867);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1052, 5867, 5868);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1053, 5868, 5869);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1054, 5877, 5878);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1055, 5878, 5879);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1056, 5879, 5880);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1057, 5880, 5881);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1058, 5881, 5882);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1059, 6003, 6004);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1060, 6009, 6010);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1061, 6010, 6011);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1062, 6023, 6024);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1063, 6024, 6025);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1064, 6025, 6026);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1065, 6037, 6038);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1066, 6038, 6039);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1067, 6039, 6040);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1068, 6040, 6041);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1069, 6041, 6042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1070, 6051, 6052);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1071, 6052, 6053);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1072, 6065, 6066);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1073, 6066, 6067);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1074, 6067, 6068);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1075, 6068, 6069);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1076, 6069, 6070);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1077, 6079, 6080);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1078, 6080, 6081);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1079, 6107, 6108);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1080, 6108, 6109);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1081, 6185, 1627);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1082, 6188, 5667);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1083, 6188, 5653);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1084, 6477, 6478);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1085, 6478, 6479);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1086, 6479, 6480);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1087, 6480, 6481);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1088, 6481, 6482);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1089, 6482, 6483);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1090, 6483, 6484);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1091, 6484, 6485);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1092, 6485, 6486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1093, 6491, 6492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1094, 6492, 6493);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1095, 6493, 6494);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1096, 6494, 6495);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1097, 6495, 6496);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1098, 6496, 6497);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1099, 6497, 6498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1100, 6500, 6499);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1101, 6505, 6506);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1102, 6506, 6507);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1103, 6507, 6508);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1104, 6508, 6509);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1105, 6509, 6510);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1106, 6510, 6511);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1107, 6511, 6512);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1108, 6519, 6520);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1109, 6520, 6521);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1110, 6521, 6522);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1111, 6522, 6523);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1112, 6668, 4717);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1113, 6669, 4753);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1114, 6670, 4789);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1115, 6671, 4659);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1116, 6672, 4681);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1117, 6673, 4735);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1118, 6674, 4771);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1119, 6675, 4807);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1120, 6676, 4699);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1121, 6806, 6807);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1122, 6807, 6808);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1123, 6808, 6809);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1124, 6809, 6810);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1125, 6810, 6811);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1126, 6820, 6821);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1127, 6821, 6822);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1128, 6822, 6823);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1129, 6823, 6824);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1130, 6825, 6826);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1131, 6834, 6835);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1132, 6835, 6836);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1133, 6836, 6837);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1134, 6837, 6838);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1135, 6838, 6839);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1136, 6848, 6849);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1137, 6849, 6850);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1138, 6850, 6851);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1139, 6851, 6852);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1140, 6852, 6853);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1141, 6862, 6863);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1142, 6863, 6864);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1143, 6864, 6865);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1144, 6865, 6866);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1145, 6866, 6867);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1146, 6876, 6877);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1147, 6877, 6878);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1148, 6878, 6879);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1149, 6879, 6880);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1150, 6890, 6891);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1151, 6891, 6892);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1152, 6892, 6893);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1153, 6893, 6894);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1154, 6894, 6895);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1155, 6895, 6896);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1156, 6896, 6897);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1157, 6904, 6905);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1158, 6905, 6906);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1159, 6906, 6907);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1160, 6907, 6908);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1161, 6918, 6919);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1162, 6919, 6920);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1163, 6920, 6921);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1164, 6921, 6922);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1165, 6936, 6937);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1166, 6937, 6938);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1167, 6938, 6939);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1168, 6944, 6945);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1169, 6945, 6946);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1170, 6946, 6947);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1171, 7005, 6806);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1172, 7006, 6820);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1173, 7007, 6848);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1174, 7008, 6876);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1175, 7009, 6904);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1176, 7010, 6834);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1177, 7011, 6862);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1178, 7012, 6890);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1179, 7013, 6918);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1180, 7302, 7303);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1181, 7303, 7304);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1182, 7304, 7305);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1183, 7305, 7306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1184, 7316, 7317);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1185, 7317, 7318);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1186, 7318, 7319);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1187, 7330, 7331);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1188, 7331, 7332);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1189, 7332, 7333);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1190, 7333, 7334);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1191, 7334, 7335);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1192, 7344, 7345);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1193, 7345, 7346);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1194, 7346, 7347);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1195, 7347, 7348);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1196, 7348, 7349);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1197, 7358, 7359);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1198, 7359, 7360);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1199, 7360, 7361);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1200, 7361, 7362);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1201, 7362, 7363);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1202, 7363, 7364);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1203, 7372, 7373);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1204, 7373, 7374);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1205, 7374, 7375);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1206, 7375, 7376);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1207, 7376, 7377);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1208, 7386, 7387);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1209, 7387, 7388);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1210, 7388, 7389);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1211, 7389, 7390);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1212, 7390, 7391);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1213, 7400, 7401);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1214, 7401, 7402);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1215, 7402, 7403);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1216, 7403, 7404);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1217, 7404, 7405);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1218, 7658, 7659);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1219, 7794, 7797);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1220, 8104, 8105);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1221, 8188, 8189);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1222, 8189, 8190);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1223, 8199, 8200);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1224, 8200, 8201);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1225, 8201, 8202);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1226, 8214, 8215);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1227, 8227, 8228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1228, 8228, 8229);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1229, 8229, 8230);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1230, 8241, 8242);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1231, 8255, 8256);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1232, 8256, 8257);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1233, 8269, 8270);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1234, 8270, 8271);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1235, 8271, 8272);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1236, 8272, 8273);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1237, 8273, 8274);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1238, 8274, 8275);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1239, 8283, 8284);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1240, 8284, 8285);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1241, 8289, 8288);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1242, 8297, 8298);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1243, 8298, 8299);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1244, 8299, 8300);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1245, 8300, 8301);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1246, 8301, 8302);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1247, 8303, 8302);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1248, 8311, 8312);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1249, 8312, 8313);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1250, 8313, 8314);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1251, 8325, 8326);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1252, 8326, 8327);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1253, 8327, 8328);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1254, 8344, 8345);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1255, 40000, 40006);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1256, 40000, 40066);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1257, 40000, 40036);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1258, 40000, 40096);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1259, 40006, 40042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1260, 40006, 40012);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1261, 40006, 40102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1262, 40006, 40072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1263, 40006, 40007);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1264, 40012, 40018);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1265, 40018, 40019);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1266, 40030, 40006);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1267, 40030, 40036);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1268, 40030, 40066);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1269, 40036, 40012);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1270, 40036, 40042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1271, 40036, 40072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1272, 40036, 40102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1273, 40042, 40078);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1274, 40042, 40043);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1275, 40042, 40048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1276, 40043, 40044);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1277, 40048, 40084);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1278, 40048, 40114);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1279, 40048, 40049);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1280, 40048, 40024);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1281, 40049, 40050);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1282, 40050, 40051);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1283, 40066, 40042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1284, 40066, 40012);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1285, 40066, 40072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1286, 40066, 40067);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1287, 40066, 40102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1288, 40072, 40073);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1289, 40072, 40018);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1290, 40072, 40048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1291, 40078, 40079);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1292, 40079, 40080);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1293, 40084, 40085);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1294, 40096, 40012);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1295, 40096, 40097);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1296, 40096, 40102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1297, 40096, 40072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1298, 40096, 40042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1299, 40097, 40098);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1300, 40098, 40099);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1301, 40102, 40103);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1302, 40103, 40104);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1303, 40104, 40105);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1304, 40105, 40106);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1305, 40108, 40109);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1306, 40109, 40110);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1307, 40110, 40111);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1308, 40111, 40112);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1309, 40114, 40115);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1310, 40120, 40126);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1311, 40120, 40186);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1312, 40120, 40156);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1313, 40120, 40216);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1314, 40126, 40162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1315, 40126, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1316, 40126, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1317, 40126, 40222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1318, 40132, 40133);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1319, 40132, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1320, 40132, 40168);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1321, 40138, 40139);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1322, 40139, 40140);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1323, 40144, 40145);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1324, 40145, 40146);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1325, 40150, 40126);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1326, 40150, 40216);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1327, 40150, 40186);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1328, 40150, 40156);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1329, 40156, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1330, 40156, 40162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1331, 40156, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1332, 40156, 40222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1333, 40156, 40157);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1334, 40162, 40168);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1335, 40162, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1336, 40162, 40228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1337, 40168, 40174);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1338, 40168, 40169);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1339, 40169, 40170);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1340, 40170, 40171);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1341, 40174, 40175);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1342, 40186, 40162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1343, 40186, 40222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1344, 40186, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1345, 40186, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1346, 40186, 40187);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1347, 40192, 40228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1348, 40192, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1349, 40192, 40193);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1350, 40192, 40168);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1351, 40193, 40194);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1352, 40194, 40195);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1353, 40198, 40199);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1354, 40198, 40144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1355, 40216, 40162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1356, 40216, 40222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1357, 40216, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1358, 40216, 40217);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1359, 40216, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1360, 40217, 40218);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1361, 40218, 40219);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1362, 40222, 40223);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1363, 40222, 40168);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1364, 40222, 40198);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1365, 40223, 40224);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1366, 40224, 40225);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1367, 40225, 40226);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1368, 40228, 40229);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1369, 40229, 40230);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1370, 40230, 40231);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1371, 40240, 40241);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1372, 40240, 40246);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1373, 40240, 40276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1374, 40240, 40306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1375, 40241, 40242);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1376, 40242, 40243);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1377, 40246, 40247);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1378, 40246, 40342);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1379, 40246, 40312);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1380, 40247, 40248);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1381, 40248, 40249);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1382, 40250, 40251);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1383, 40252, 40253);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1384, 40253, 40254);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1385, 40254, 40255);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1386, 40256, 40257);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1387, 40258, 40259);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1388, 40259, 40260);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1389, 40260, 40261);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1390, 40270, 40271);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1391, 40276, 40277);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1392, 40276, 40252);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1393, 40277, 40278);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1394, 40288, 40289);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1395, 40289, 40290);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1396, 40306, 40307);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1397, 40307, 40308);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1398, 40308, 40309);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1399, 40309, 40310);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1400, 40310, 40312);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1401, 40312, 40313);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1402, 40313, 40314);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1403, 40318, 40319);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1404, 40319, 40320);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1405, 40320, 40321);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1406, 40321, 40322);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1407, 40342, 40343);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1408, 40343, 40344);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1409, 40344, 40345);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1410, 40360, 40366);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1411, 40360, 40426);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1412, 40360, 40456);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1413, 40360, 40396);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1414, 40360, 40361);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1415, 40361, 40362);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1416, 40366, 40367);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1417, 40366, 40402);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1418, 40367, 40368);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1419, 40368, 40369);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1420, 40369, 40370);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1421, 40372, 40373);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1422, 40373, 40374);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1423, 40374, 40375);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1424, 40378, 40379);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1425, 40379, 40380);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1426, 40380, 40381);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1427, 40381, 40382);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1428, 40396, 40397);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1429, 40396, 40372);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1430, 40396, 40402);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1431, 40398, 40399);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1432, 40402, 40403);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1433, 40408, 40409);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1434, 40409, 40410);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1435, 40426, 40427);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1436, 40426, 40372);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1437, 40438, 40439);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1438, 40439, 40440);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1439, 40440, 40441);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1440, 40456, 40457);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1441, 40457, 40458);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1442, 40468, 40469);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1443, 40480, 40546);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1444, 40480, 40486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1445, 40480, 40516);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1446, 40480, 40481);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1447, 40480, 40576);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1448, 40481, 40482);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1449, 40482, 40483);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1450, 40483, 40484);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1451, 40486, 40487);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1452, 40486, 40522);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1453, 40486, 40552);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1454, 40487, 40488);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1455, 40488, 40489);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1456, 40492, 40493);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1457, 40493, 40494);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1458, 40494, 40495);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1459, 40495, 40496);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1460, 40510, 40511);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1461, 40510, 40486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1462, 40516, 40517);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1463, 40516, 40522);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1464, 40516, 40552);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1465, 40516, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1466, 40517, 40518);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1467, 40522, 40523);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1468, 40523, 40524);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1469, 40527, 40558);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1470, 40528, 40529);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1471, 40529, 40530);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1472, 40530, 40531);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1473, 40531, 40532);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1474, 40546, 40547);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1475, 40546, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1476, 40546, 40522);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1477, 40546, 40582);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1478, 40552, 40553);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1479, 40558, 40559);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1480, 40559, 40560);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1481, 40560, 40561);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1482, 40562, 40563);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1483, 40576, 40577);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1484, 40579, 40580);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1485, 40582, 40583);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1486, 40588, 40589);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1487, 40589, 40590);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1488, 513, 514);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1489, 7991, 7992);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1490, 8242, 8243);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1491, 5838, 5839);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1492, 5837, 5838);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1493, 1318, 1319);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1494, 7993, 8034);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1495, 1504, 1505);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1496, 1713, 1714);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1497, 7992, 8032);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1498, 4777, 4778);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1499, 4490, 4491);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1500, 6498, 6499);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1501, 1285, 1286);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1502, 5871, 5872);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1503, 6499, 6498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1504, 8032, 8044);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1506, 2189, 2188);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1507, 2190, 2189);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1508, 2092, 2093);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1509, 8035, 8047);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1510, 7991, 7993);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1511, 8231, 8232);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1512, 6499, 6500);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1517, 6813, 6814);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1518, 5756, 5757);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1519, 5796, 5797);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1520, 602, 603);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1521, 5810, 5811);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1505, 2093, 2094);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1513, 2168, 2169);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1514, 6908, 6909);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1515, 40428, 40429);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1516, 2241, 2240);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1563, 1516, 1517);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1564, 6839, 6840);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1565, 1456, 1457);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1566, 1455, 1456);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1567, 1453, 1454);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1568, 1452, 1453);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1569, 7995, 8039);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1570, 1545, 1546);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1571, 2699, 2700);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1572, 3938, 3939);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1573, 2357, 2358);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1574, 8102, 8103);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1575, 6922, 6923);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1576, 1761, 1762);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1577, 1760, 1761);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1578, 1006, 1757);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1579, 454, 543);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1580, 8033, 8045);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1581, 40175, 40176);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1582, 1173, 1797);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1583, 1006, 1737);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1584, 442, 450);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1585, 441, 442);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1586, 213, 441);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1587, 450, 562);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1588, 3855, 3856);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1589, 3753, 3754);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1590, 5280, 5281);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1591, 5281, 5282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1592, 1645, 1646);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1593, 4455, 4457);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1594, 4451, 4453);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1595, 2007, 2008);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1596, 44, 47);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1615, 1344, 1345);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1616, 658, 659);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1617, 1457, 1458);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1618, 4964, 4965);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1619, 6824, 6825);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1620, 833, 2500);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1621, 657, 658);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1622, 8243, 8244);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1623, 40235, 40236);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1624, 40120, 40121);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1625, 5811, 5812);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1627, 7992, 7991);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1628, 8032, 7992);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1629, 7995, 7991);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1631, 7994, 8036);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1633, 40578, 40579);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1634, 8034, 7993);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1635, 8035, 7993);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1636, 255, 2466);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1639, 40249, 40250);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1640, 40577, 40578);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1641, 4953, 4954);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1642, 5234, 5235);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1643, 5234, 5233);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1644, 1080, 1075);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1646, 5729, 5730);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1647, 2090, 2091);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1648, 2525, 2526);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1649, 6868, 6869);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1650, 8259, 8260);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1651, 40246, 40252);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1652, 40222, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1653, 5217, 5218);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1654, 2121, 2122);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1655, 8258, 8259);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1656, 8232, 8233);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1657, 40306, 40252);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1658, 929, 930);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1659, 928, 929);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1660, 927, 928);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1681, 2769, 2770);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1682, 2768, 2769);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1683, 2767, 2768);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1690, 8246, 8247);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1691, 40098, 40072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1692, 1447, 1448);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1693, 5221, 5222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1706, 2375, 2376);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1707, 1088, 1084);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1708, 1458, 1459);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1709, 289, 290);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1710, 1087, 5646);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1711, 8034, 8046);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1712, 1556, 1557);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1713, 299, 300);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1714, 2468, 2469);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1715, 8036, 8048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1716, 1929, 1930);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1717, 7992, 8033);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1718, 40550, 40551);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1719, 40481, 40486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1720, 6942, 6943);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1721, 4814, 4815);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1722, 873, 874);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1723, 872, 873);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1724, 796, 872);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1725, 1546, 1547);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1729, 2152, 2151);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1730, 1903, 1902);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1731, 40261, 40262);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1732, 1732, 1733);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1733, 1871, 1872);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1734, 1870, 1871);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1735, 1523, 1870);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1736, 40012, 40013);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1737, 1370, 1371);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1738, 562, 563);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1739, 885, 886);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1740, 884, 885);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1741, 4510, 4511);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1742, 256, 2468);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1743, 6880, 6881);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1744, 40426, 40402);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1755, 40192, 40198);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1756, 40132, 40198);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1757, 4950, 4951);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1758, 930, 931);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1759, 266, 267);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1760, 265, 266);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1761, 2228, 2227);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1762, 2229, 2228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1763, 2153, 2154);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1764, 2230, 2229);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1765, 2152, 2153);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1766, 1724, 1723);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1767, 1725, 1724);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1768, 603, 604);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1769, 5218, 5219);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1770, 1273, 1274);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1771, 40191, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1772, 40012, 40048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1776, 40336, 40337);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1777, 4954, 4955);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1778, 40487, 40582);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1779, 40486, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1780, 40241, 40246);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1781, 3630, 3631);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1782, 4921, 4922);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1783, 40510, 40576);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1784, 40240, 40336);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1866, 2091, 2092);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1867, 4915, 4916);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1868, 2179, 2180);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1869, 8216, 8217);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1888, 90, 91);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1889, 40217, 40162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1890, 40102, 40048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1891, 40106, 40107);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1892, 734, 733);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1893, 318, 319);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1894, 317, 318);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1895, 417, 2503);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1896, 2397, 2398);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1897, 2396, 2397);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1898, 2395, 2396);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1904, 874, 875);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1905, 1571, 4735);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1906, 1088, 1073);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1907, 1894, 1895);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1908, 40068, 40069);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1909, 40434, 40435);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1910, 1738, 1739);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1911, 40367, 40372);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1912, 40255, 40256);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1913, 1739, 1740);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1914, 3732, 3733);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1915, 486, 485);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1916, 289, 288);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1917, 138, 137);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1918, 1643, 1642);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1919, 766, 3700);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1920, 1342, 1343);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1921, 2290, 2291);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1922, 2289, 2290);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1923, 2288, 2289);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1924, 40243, 40244);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1935, 40306, 40282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1936, 1063, 1064);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1937, 1062, 1063);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1938, 1306, 1307);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1939, 1371, 1372);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1940, 8302, 8303);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1941, 3896, 3895);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1942, 2712, 2711);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1943, 2279, 2280);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1944, 2277, 2278);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1945, 2276, 2277);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1946, 2275, 2276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1947, 2278, 2279);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1948, 985, 986);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1949, 984, 985);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1950, 983, 984);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1951, 978, 983);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1952, 5852, 5853);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1953, 2180, 2181);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1954, 4454, 4456);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1955, 2482, 2483);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1956, 4930, 4931);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1957, 40045, 40048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1958, 1909, 1910);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1959, 40036, 40037);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1960, 791, 790);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1961, 8261, 8262);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1962, 8260, 8261);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1963, 4918, 4919);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1964, 7993, 7991);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1965, 7991, 7997);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1966, 3896, 3897);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1967, 8219, 8220);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1968, 2499, 4452);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1969, 2498, 4448);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1975, 1642, 1641);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1976, 1440, 1441);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1977, 1873, 1874);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1978, 1872, 1873);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1979, 2171, 2172);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1980, 1769, 1770);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1981, 1768, 1769);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1982, 1767, 1768);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1983, 1341, 3704);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1984, 4452, 4453);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1985, 3432, 3433);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1986, 310, 311);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1987, 6013, 6014);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1988, 6012, 6013);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1989, 40338, 40339);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1990, 765, 766);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1991, 1470, 3638);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1992, 1467, 1466);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1993, 40283, 40284);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1994, 40498, 40499);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1995, 40552, 40498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1996, 40492, 40528);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1997, 40368, 40432);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1998, 2487, 2488);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1999, 40276, 40282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2000, 4961, 4962);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2001, 1084, 1082);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2002, 1006, 1747);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2003, 3910, 3911);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2004, 2359, 2360);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2005, 2369, 2370);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2006, 1496, 1498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2007, 896, 897);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2008, 488, 3532);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2009, 2770, 2771);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2010, 2368, 2369);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2011, 2358, 2359);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2012, 40012, 40108);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2013, 7191, 7192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2014, 40127, 40128);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2015, 2519, 2520);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2016, 420, 2519);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2017, 8091, 8092);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2018, 89, 88);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2019, 5219, 5220);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2020, 634, 635);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2021, 633, 634);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2022, 273, 274);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2023, 1704, 1705);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2024, 40458, 40459);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2025, 40421, 40422);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2026, 40420, 40421);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2027, 40308, 40254);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2028, 40495, 40498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2029, 40497, 40498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2030, 40426, 40432);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2031, 3870, 3869);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2032, 3871, 3870);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2033, 90, 89);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2034, 2103, 2104);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2035, 8286, 8287);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2036, 40496, 40497);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2037, 1631, 1630);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2038, 1630, 1629);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2039, 7242, 7243);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2040, 3754, 3755);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2041, 7405, 7406);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2042, 463, 464);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2043, 1320, 1321);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2044, 1319, 1320);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2045, 5853, 5854);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2046, 1143, 1144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2047, 1762, 1763);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2048, 1381, 1380);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2049, 1705, 1706);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2050, 2129, 2128);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2051, 2241, 2242);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2052, 2242, 2243);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2053, 8039, 8051);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2054, 40361, 40396);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2055, 40068, 40072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2056, 40555, 40556);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2057, 40219, 40220);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2058, 2141, 2142);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2059, 40195, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2060, 8288, 8289);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2061, 3693, 3694);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2062, 7997, 8042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2063, 7994, 7991);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2064, 2186, 2185);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2065, 8287, 8288);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2069, 8303, 8304);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2070, 6923, 6924);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2071, 276, 277);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2072, 275, 276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2073, 274, 275);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2075, 2142, 2141);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2076, 1771, 1772);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2077, 1770, 1771);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2078, 40141, 40144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2079, 5711, 5712);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2080, 5710, 5711);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2081, 5709, 5710);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2082, 2632, 2633);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2083, 2631, 2632);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2084, 2630, 2631);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2085, 2629, 2630);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2086, 5657, 5658);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2087, 5656, 5657);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2088, 4935, 4936);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2089, 8355, 8356);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2090, 8354, 8355);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2091, 8353, 8354);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2092, 300, 3646);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2093, 2511, 2512);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2094, 2273, 2274);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2095, 2272, 2273);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2096, 2271, 2272);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2097, 2270, 2271);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2098, 2269, 2270);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2099, 2268, 2269);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2100, 40376, 40377);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2101, 2633, 2634);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2109, 40554, 40555);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2111, 40487, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2117, 40404, 40405);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2118, 40074, 40050);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2119, 40164, 40140);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2120, 5658, 5659);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2121, 40014, 40018);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2122, 40121, 40122);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2123, 40134, 40200);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2124, 3866, 3865);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2125, 40134, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2126, 3826, 3827);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2127, 3825, 3826);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2128, 40075, 40076);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2129, 40074, 40075);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2130, 40193, 40168);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2131, 653, 654);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2132, 1514, 1515);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2133, 652, 653);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2134, 2207, 2208);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2135, 2206, 2207);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2136, 2205, 2206);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2137, 1557, 1558);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2138, 106, 107);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1522, 371, 2115);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1523, 5712, 5713);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1524, 2102, 2103);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1525, 2101, 2102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1526, 4776, 4777);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1527, 2247, 2248);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1528, 2246, 2247);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1529, 2245, 2246);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1530, 5233, 5234);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1531, 1567, 1568);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1532, 1566, 1567);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1533, 5279, 5280);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1534, 5278, 5279);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1535, 8090, 8091);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1536, 40519, 40520);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1537, 40583, 40584);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1538, 8230, 8231);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1539, 8089, 8090);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1540, 8088, 8089);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1541, 8087, 8088);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1542, 2240, 2241);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1543, 5797, 5798);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1544, 1578, 1579);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1545, 1577, 1578);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1546, 1576, 1577);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1547, 2196, 2197);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1548, 1454, 1455);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1549, 6941, 6942);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1550, 6940, 6941);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1551, 127, 128);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1552, 125, 127);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1553, 4741, 4742);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1554, 2676, 2677);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1555, 7377, 7378);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1556, 1759, 1760);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1557, 1758, 1759);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1558, 1757, 1758);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1559, 2249, 2250);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1560, 2248, 2249);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1561, 1199, 1200);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1562, 2312, 2313);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1597, 5282, 5283);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1598, 1343, 1344);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1599, 40500, 40501);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1600, 40499, 40500);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1601, 8101, 8102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1602, 7991, 7995);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1603, 2257, 2258);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1604, 40345, 40346);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1605, 2256, 2257);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1606, 2255, 2256);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1607, 457, 456);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1608, 1345, 1346);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1609, 659, 660);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1610, 3431, 3432);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1611, 5824, 5825);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1612, 1908, 1909);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1613, 272, 273);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1614, 271, 272);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1626, 7991, 7994);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1630, 8039, 7995);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1632, 7993, 8035);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1637, 6867, 6868);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1638, 543, 544);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1645, 8257, 8258);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1661, 2906, 2907);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1662, 5826, 5827);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1663, 2299, 2300);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1664, 40375, 40376);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1665, 4793, 4794);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1666, 1102, 1103);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1667, 258, 2484);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1668, 2298, 2299);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1669, 2297, 2298);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1670, 2296, 2297);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1671, 2295, 2296);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1672, 2905, 2906);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1673, 2262, 2263);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1674, 2250, 2251);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1675, 2258, 2259);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1676, 4450, 4452);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1677, 2261, 2262);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1678, 2251, 2252);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1679, 2260, 2261);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1680, 2259, 2260);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1684, 550, 551);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1685, 3824, 3825);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1686, 3823, 3824);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1687, 3822, 3823);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1688, 40162, 40163);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1689, 40429, 40430);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1694, 2367, 2368);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1695, 2366, 2367);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1696, 40435, 40436);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1697, 8245, 8246);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1698, 8244, 8245);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1699, 40015, 40021);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1700, 5757, 5758);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1701, 2380, 2381);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1702, 2379, 2380);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1703, 2378, 2379);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1704, 2377, 2378);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1705, 2376, 2377);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1726, 1420, 1421);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1727, 6501, 6502);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1728, 2231, 2230);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1745, 2501, 4461);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1746, 176, 1520);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1747, 1304, 1305);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1748, 1303, 1304);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1749, 1302, 1303);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1750, 2387, 2388);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1751, 2386, 2387);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1752, 2385, 2386);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1753, 47, 48);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1754, 4456, 4457);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1773, 886, 887);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1774, 40019, 40020);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1775, 40576, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1785, 6026, 6027);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1786, 40390, 40426);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1787, 40547, 40548);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1788, 40242, 40246);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1789, 40397, 40398);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1790, 6011, 6012);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1791, 40042, 40018);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1792, 40366, 40372);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1793, 184, 5643);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1794, 40072, 40108);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1795, 1921, 1920);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1796, 40510, 40546);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1797, 40518, 40519);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1798, 40067, 40068);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1799, 40073, 40074);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1800, 40030, 40096);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1801, 666, 667);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1802, 40510, 40516);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1803, 242, 666);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1804, 40548, 40549);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1805, 40433, 40434);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1806, 40432, 40433);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1807, 40456, 40432);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1808, 3787, 3786);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1809, 1305, 1306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1810, 2082, 2081);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1811, 2111, 2112);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1812, 2469, 7668);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1813, 8328, 8329);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1814, 40126, 40127);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1815, 40013, 40014);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1816, 40187, 40188);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1817, 8215, 8216);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1818, 40278, 40279);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1819, 40140, 40141);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1820, 40241, 40306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1821, 40396, 40432);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1822, 40012, 40078);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1823, 2169, 2170);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1824, 40217, 40223);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1825, 40337, 40338);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1826, 1666, 1665);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1827, 40427, 40428);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1828, 40486, 40582);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1829, 299, 298);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1830, 40044, 40045);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1831, 40072, 40078);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1832, 40042, 40108);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1833, 40390, 40391);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1834, 40488, 40582);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1835, 8329, 8330);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1836, 1635, 1636);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1837, 6027, 6028);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1838, 1644, 1643);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1839, 8285, 8286);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1840, 8285, 8284);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1841, 462, 461);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1842, 40390, 40366);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1843, 40553, 40554);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1844, 40246, 40282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1845, 40492, 40498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1846, 40163, 40164);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1847, 40133, 40134);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1848, 40366, 40432);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1849, 40141, 40142);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1850, 40243, 40276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1851, 40315, 40316);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1852, 40362, 40426);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1853, 2515, 2516);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1854, 7319, 7320);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1855, 1448, 1449);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1856, 604, 605);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1857, 40549, 40550);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1858, 40282, 40283);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1859, 40247, 40282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1860, 40168, 40204);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1861, 40396, 40462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1862, 40516, 40582);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1863, 40546, 40552);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1864, 1159, 1160);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1865, 40218, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1870, 40188, 40189);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1871, 40000, 40001);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1872, 40584, 40585);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1873, 40162, 40198);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1874, 40102, 40018);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1875, 8247, 8248);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1876, 8233, 8234);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1877, 40164, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1878, 1640, 1639);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1879, 6910, 6911);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1880, 40199, 40200);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1881, 8217, 8218);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1882, 40463, 40464);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1883, 40205, 40206);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1884, 40524, 40525);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1885, 40204, 40205);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1886, 40462, 40463);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1887, 1715, 1716);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1899, 2398, 2399);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1900, 40241, 40276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1901, 1737, 1738);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1902, 876, 877);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1903, 875, 876);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1925, 2170, 2171);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1926, 667, 668);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1927, 40489, 40490);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1928, 1368, 1367);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1929, 40080, 40081);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1930, 3770, 3771);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1931, 4722, 4723);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1932, 5851, 5852);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1933, 5850, 5851);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1934, 5849, 5850);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2143, 40403, 40404);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2144, 40306, 40312);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2145, 1060, 1059);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2146, 40276, 40342);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2147, 2512, 2513);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2189, 3755, 3756);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2190, 40370, 40371);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2196, 8218, 8219);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2241, 832, 2499);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2242, 1307, 1308);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2243, 40127, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2261, 481, 482);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2262, 840, 2511);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2263, 4446, 4448);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2264, 3910, 3909);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2265, 3883, 3882);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2266, 80, 81);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2267, 79, 80);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2268, 4937, 4938);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2269, 735, 736);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2270, 735, 734);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2273, 267, 268);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2274, 7379, 7380);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2275, 40099, 40100);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2276, 3897, 3898);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2277, 3783, 3782);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2278, 6922, 6921);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2281, 7380, 7381);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2282, 1286, 1287);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2283, 5839, 5840);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2287, 4925, 4926);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2288, 1469, 1468);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2289, 40046, 40018);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2290, 40040, 40046);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2291, 40102, 40108);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2292, 1541, 4699);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2293, 40459, 40460);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2294, 40336, 40252);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2295, 40456, 40402);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2296, 40522, 40588);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2297, 40390, 40396);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2299, 40522, 40528);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2300, 40276, 40312);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2301, 7659, 7660);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2302, 40251, 40287);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2303, 40245, 40251);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2304, 40244, 40245);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2305, 1024, 4484);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2306, 40366, 40462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2307, 542, 938);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2308, 3925, 3924);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2309, 3788, 3787);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2310, 3415, 3416);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2311, 3414, 3415);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2312, 3404, 3405);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2313, 40362, 40363);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2314, 449, 438);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2315, 483, 449);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2316, 6498, 6497);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2317, 6826, 6827);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2318, 3801, 3800);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2319, 1531, 2472);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2320, 40193, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2321, 1705, 1704);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2323, 3868, 3867);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2324, 40410, 40411);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2325, 40290, 40291);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2326, 40082, 40083);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2331, 40150, 40151);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2332, 3954, 3953);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2333, 3952, 3951);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2334, 1328, 1329);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2335, 1725, 1726);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2336, 87, 86);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2337, 4662, 4661);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2338, 2360, 2361);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2339, 40350, 40351);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2340, 40349, 40350);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2342, 40469, 40470);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2343, 2142, 2143);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2344, 40243, 40309);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2345, 2475, 7663);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2346, 2711, 2712);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2347, 8105, 8106);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2348, 1788, 1789);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2349, 40316, 40317);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2350, 8047, 8035);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2351, 4461, 4462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2352, 4459, 4461);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2353, 40488, 40494);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2354, 6811, 6812);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2355, 1025, 2498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2356, 236, 657);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2357, 4708, 4709);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2358, 632, 633);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2359, 40007, 40012);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2360, 1748, 1749);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2361, 1747, 1748);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2362, 6840, 6841);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2363, 1558, 1559);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2364, 201, 2462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2365, 2623, 2624);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2366, 2622, 2623);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2367, 2621, 2622);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2368, 2620, 2621);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2369, 2619, 2620);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2370, 2252, 2253);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2371, 40411, 40412);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2372, 40470, 40471);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2373, 5716, 5717);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2374, 40195, 40196);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2375, 464, 3544);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2376, 40371, 40372);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2377, 482, 3559);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2378, 1900, 1899);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2379, 2149, 2148);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2380, 8033, 7992);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2381, 8045, 8033);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2382, 7668, 7669);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2383, 4742, 4743);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2384, 2771, 2772);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2385, 2219, 2220);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2386, 1532, 2205);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2387, 1179, 1787);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2400, 1328, 1327);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2408, 40218, 40222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2409, 40157, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2410, 2712, 2713);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2411, 1763, 1764);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2412, 2143, 2144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2413, 7406, 7407);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2414, 40157, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2415, 2133, 2132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2416, 3955, 3954);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1970, 2112, 2113);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1971, 887, 888);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1972, 5671, 5672);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1973, 746, 747);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (1974, 740, 746);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2066, 40242, 40306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2067, 40465, 40466);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2068, 40464, 40465);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2074, 40556, 40557);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2102, 40222, 40228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2103, 2634, 2635);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2104, 1274, 1275);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2105, 40135, 40136);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2106, 8275, 8276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2107, 5713, 5714);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2108, 40206, 40207);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2110, 40134, 40135);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2112, 294, 293);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2113, 839, 2507);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2114, 40284, 40285);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2115, 40314, 40315);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2116, 40405, 40406);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2197, 631, 632);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2198, 793, 3716);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2199, 1646, 3654);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2200, 792, 793);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2201, 792, 791);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2202, 787, 786);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2203, 3786, 3785);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2204, 1633, 1632);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2205, 2082, 2083);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2206, 2102, 2101);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2207, 2081, 2080);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2208, 2008, 2009);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2251, 6295, 6296);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2252, 5672, 5673);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2253, 2752, 2753);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2279, 3953, 3952);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2327, 40081, 40082);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2328, 40014, 40048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2329, 40399, 40400);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2330, 40165, 40166);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2341, 40348, 40349);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2425, 1248, 1249);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2426, 1356, 1357);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2427, 1161, 1162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2428, 1160, 1161);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2429, 40361, 40366);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2430, 6096, 6097);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2431, 6095, 6096);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2432, 6094, 6095);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2434, 6924, 6925);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2435, 6813, 6812);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2436, 6814, 6813);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2437, 1741, 1742);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2438, 1740, 1741);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2439, 1772, 1773);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2440, 40481, 40546);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2441, 5785, 5786);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2442, 40001, 40006);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2443, 3924, 3923);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2445, 288, 287);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2446, 40043, 40078);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2447, 736, 3662);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2448, 1733, 1734);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2449, 3884, 3885);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2450, 40049, 40055);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2451, 40157, 40222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2452, 6842, 6843);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2454, 8107, 8108);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2455, 6157, 6158);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2456, 4790, 4789);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2457, 6156, 6157);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2458, 6155, 6156);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2459, 6154, 6155);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2460, 1883, 1882);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2461, 8106, 8107);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2462, 8174, 8175);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2463, 8173, 8174);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2464, 8172, 8173);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2469, 6843, 6844);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2470, 4744, 4745);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2471, 3700, 766);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2472, 6159, 6158);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2473, 4755, 4754);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2474, 268, 269);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2475, 7349, 7350);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2476, 2409, 2410);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2477, 6158, 6159);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2478, 6497, 6496);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2479, 1675, 1676);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2480, 278, 279);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2481, 2624, 2625);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2485, 40291, 40292);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2486, 3814, 3815);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2487, 40234, 40235);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2488, 40367, 40402);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2489, 40243, 40279);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2490, 3898, 3899);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2491, 8229, 8228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2492, 7303, 7302);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2493, 8213, 8214);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2494, 40085, 40086);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2495, 40055, 40056);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2496, 40054, 40055);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2497, 40247, 40252);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2498, 40078, 40054);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2499, 5800, 5801);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2500, 8233, 8232);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2501, 8136, 8137);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2502, 8135, 8136);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2503, 8134, 8135);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2504, 40490, 40491);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2505, 8356, 8357);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2506, 8342, 8343);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2507, 8341, 8342);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2508, 8340, 8341);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2509, 8339, 8340);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2510, 8202, 8203);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2511, 8133, 8134);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2512, 8234, 8235);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2513, 2321, 2322);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2514, 7391, 7392);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2515, 8132, 8133);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2516, 8131, 8132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2517, 8130, 8131);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2518, 8129, 8130);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2519, 6909, 6908);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2520, 6811, 6810);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2521, 6082, 6083);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2522, 8343, 8344);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2523, 6081, 6082);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2524, 6109, 6110);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2525, 2108, 2107);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2526, 143, 139);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2527, 1719, 1718);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2528, 8204, 8205);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2529, 8203, 8204);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2530, 8315, 8316);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2531, 8314, 8315);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2532, 6825, 6824);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2533, 40051, 40052);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2534, 40138, 40174);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2535, 8135, 8134);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2536, 8108, 8109);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2537, 8305, 8304);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2538, 8304, 8305);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2539, 8304, 8303);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2540, 1896, 1897);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2541, 1895, 1896);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2542, 8109, 8110);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2543, 1904, 1905);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2544, 2231, 2232);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2545, 2702, 2703);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2546, 2701, 2702);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2547, 8093, 8094);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2548, 8093, 8092);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2549, 3869, 3868);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2550, 40591, 40592);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2551, 6485, 6484);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2552, 1793, 1794);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2553, 6055, 6056);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2554, 1792, 1793);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2555, 1791, 1792);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2556, 1790, 1791);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2557, 1789, 1790);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2558, 1787, 1788);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2559, 6054, 6055);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2560, 6053, 6054);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2561, 8358, 8359);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2562, 8357, 8358);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2563, 8271, 8270);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2564, 6881, 6882);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2565, 6909, 6910);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2566, 40586, 40587);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2567, 8136, 8135);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2568, 1694, 1693);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2569, 8137, 8136);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2570, 3911, 3910);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2571, 40549, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2572, 40024, 40025);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2573, 1810, 1811);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2574, 1910, 1911);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2575, 6110, 6111);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2576, 40068, 40012);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2577, 2753, 2754);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2578, 5674, 5675);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2579, 6496, 6495);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2580, 1579, 1580);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2581, 1919, 1918);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2582, 8214, 8213);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2583, 3923, 3922);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2584, 8133, 8132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2585, 5659, 5660);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2586, 5714, 5713);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2587, 696, 697);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2588, 40198, 40204);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2589, 1676, 3708);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2590, 2130, 2129);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2591, 1101, 1100);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2592, 8262, 8261);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2593, 8215, 8214);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2594, 40363, 40364);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2595, 7660, 7661);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2596, 5869, 5870);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2597, 8259, 8258);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2598, 8244, 8243);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2599, 8245, 8244);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2600, 1892, 1891);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2601, 6029, 6030);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2602, 6028, 6029);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2603, 8248, 8249);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2604, 8290, 8291);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2605, 8262, 8263);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2606, 40189, 40190);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2607, 2091, 2090);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2608, 2381, 2382);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2611, 644, 645);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2612, 643, 644);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2614, 3784, 3783);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2615, 40194, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2616, 895, 894);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2617, 7392, 7393);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2618, 5997, 5998);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2619, 8134, 8133);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2620, 40176, 40177);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2621, 40198, 40174);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2622, 40522, 40498);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2623, 40426, 40462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2624, 1911, 1912);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2625, 2188, 2187);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2626, 3856, 3857);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2627, 91, 93);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2648, 8300, 8299);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2649, 1106, 1107);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2650, 1064, 1106);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2651, 40229, 40234);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2652, 40228, 40234);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2653, 40115, 40116);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2658, 8243, 8242);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2659, 8247, 8246);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2666, 5870, 5871);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2672, 1636, 1635);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2675, 133, 132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2676, 6495, 6494);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2677, 40488, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2678, 8230, 8229);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2679, 40245, 40246);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2680, 40074, 40078);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2681, 1929, 1928);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2686, 8316, 8317);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2687, 6043, 6044);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2704, 6148, 6149);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2705, 8314, 8313);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2706, 7660, 7659);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2707, 1145, 5120);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2708, 290, 289);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2709, 6309, 6310);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2714, 1310, 1311);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2725, 6146, 6145);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2728, 40363, 40396);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2729, 2084, 6237);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2730, 7336, 7337);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2731, 7335, 7336);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2732, 3405, 3406);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2733, 2104, 6245);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2734, 2084, 2083);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2735, 2144, 6261);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2736, 4723, 4724);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2737, 40482, 40486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2738, 3951, 3950);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2739, 3950, 3949);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2740, 896, 895);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2741, 8104, 8103);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2742, 8105, 8104);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2744, 2332, 2331);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2745, 3810, 3809);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2746, 7306, 7307);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2748, 40157, 40158);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2750, 40292, 40293);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2760, 40112, 40113);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2761, 40351, 40352);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2762, 40441, 40442);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2763, 456, 455);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2764, 40279, 40280);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2765, 40585, 40586);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2766, 8218, 8217);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2767, 40217, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2768, 8094, 8093);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2769, 40231, 40232);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2770, 2093, 2092);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2788, 8091, 8090);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2798, 6523, 6524);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2812, 487, 486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2815, 8094, 8095);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2816, 6084, 6085);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2817, 1734, 1735);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2818, 6083, 6084);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2819, 1884, 1885);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2820, 1922, 1923);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2821, 1930, 1931);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2822, 3694, 3695);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2823, 1726, 3608);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2824, 6098, 6099);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2825, 6125, 6126);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2826, 8291, 8292);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2827, 85, 83);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2828, 8187, 8188);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2829, 8186, 8187);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2831, 6083, 6082);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2832, 4756, 4755);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2833, 3899, 3898);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2834, 2682, 2681);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2835, 40097, 40042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2836, 40001, 40007);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2837, 8189, 8188);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2838, 8270, 8269);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2839, 1690, 1689);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2840, 40098, 40104);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2841, 40165, 40198);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2842, 286, 285);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2843, 40244, 40276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2844, 697, 698);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2845, 8190, 8189);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2846, 6143, 6142);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2847, 3882, 3881);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2848, 2391, 2392);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2850, 790, 789);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2851, 1874, 1875);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2852, 4660, 4659);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2853, 6512, 6513);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2854, 7679, 7680);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2855, 6511, 6510);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2857, 8191, 8190);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2860, 7664, 7665);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2861, 2134, 2133);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2862, 8222, 8223);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2863, 8343, 8342);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2864, 143, 3575);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2867, 8221, 8222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2868, 1894, 1893);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2869, 7390, 7389);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2870, 8315, 8314);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2871, 8357, 8356);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2872, 7391, 7390);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2874, 128, 127);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2880, 8192, 8193);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2881, 40121, 40126);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2882, 8223, 8224);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2883, 3708, 3709);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2885, 2128, 2127);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2886, 2078, 2077);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2887, 8193, 8192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2888, 6826, 6825);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2889, 2702, 2701);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2890, 2092, 2091);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2891, 4666, 4667);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2892, 4667, 4666);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2893, 1459, 1460);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2896, 8203, 8202);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2897, 6126, 6127);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2898, 40483, 40519);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2899, 1382, 1381);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2900, 40133, 40168);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2901, 40362, 40366);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2902, 6127, 6128);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2903, 2302, 2303);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2139, 105, 106);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2140, 4460, 4462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2141, 48, 144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2142, 40014, 40015);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2148, 2521, 2522);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2149, 4491, 4492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2150, 40590, 40591);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2151, 40561, 40562);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2152, 4485, 4488);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2153, 40501, 40502);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2154, 40200, 40201);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2155, 40456, 40462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2156, 40492, 40558);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2157, 40492, 40588);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2158, 40522, 40558);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2159, 1800, 1801);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2160, 1799, 1800);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2161, 1798, 1799);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2162, 1797, 1798);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2163, 6809, 6808);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2164, 2520, 2521);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2165, 7320, 7321);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2166, 2667, 2668);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2167, 8037, 8049);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2168, 7994, 8037);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2169, 40164, 40165);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2170, 40007, 40008);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2171, 40018, 40024);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2172, 40044, 40018);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2173, 2533, 2534);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2174, 1632, 1631);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2175, 6807, 6806);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2176, 1641, 1640);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2177, 2635, 2636);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2178, 2132, 2131);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2179, 2282, 2283);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2180, 1634, 1633);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2181, 1635, 1634);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2182, 2133, 2134);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2183, 3631, 3630);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2184, 2281, 2282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2185, 139, 138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2186, 2280, 2281);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2187, 2187, 2186);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2188, 40074, 40108);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2191, 40164, 40198);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2192, 40511, 40512);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2193, 40513, 40514);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2194, 40512, 40513);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2195, 2516, 2517);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2209, 277, 278);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2210, 8289, 8290);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2211, 40068, 40042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2212, 4686, 4687);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2213, 3839, 3840);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2214, 3838, 3839);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2215, 1918, 1917);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2216, 5714, 5715);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2217, 3733, 3734);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2218, 1654, 1655);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2219, 3771, 3772);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2220, 8092, 8093);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2221, 40525, 40526);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2222, 40547, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2223, 1144, 1145);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2224, 1470, 1469);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2225, 7241, 7242);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2226, 7378, 7379);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2227, 40483, 40516);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2228, 40361, 40426);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2229, 3646, 300);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2230, 6500, 6501);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2231, 132, 131);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2232, 1381, 1382);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2233, 5235, 5236);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2234, 5798, 5799);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2235, 298, 297);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2236, 300, 299);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2237, 2101, 2100);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2238, 2103, 2102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2239, 2083, 2084);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2240, 3788, 3789);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2244, 8103, 8104);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2245, 1012, 1013);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2246, 1011, 1012);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2247, 1003, 1011);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2248, 1568, 1569);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2249, 8044, 8032);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2250, 1569, 1570);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2254, 2751, 2752);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2255, 2503, 2504);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2256, 3800, 3801);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2257, 40067, 40073);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2258, 40481, 40576);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2259, 6808, 6807);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2260, 4909, 4910);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2271, 5799, 5800);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2272, 463, 462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2665, 3789, 3788);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2667, 3865, 3864);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2668, 40412, 40413);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2669, 6143, 6144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2670, 8261, 8260);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2671, 290, 3630);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2673, 2083, 2082);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2674, 2688, 3724);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2830, 8185, 8186);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2906, 40038, 40104);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2907, 40232, 40233);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2908, 40201, 40202);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2909, 3884, 3883);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2910, 3859, 3860);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2911, 40363, 40366);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2912, 1372, 1373);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2913, 6071, 6072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2914, 6070, 6071);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2917, 40098, 40042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2918, 40198, 40234);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2919, 4707, 4708);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2920, 40187, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2921, 40037, 40102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2922, 2191, 2190);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2924, 8231, 8230);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2931, 40251, 40282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2932, 40251, 40257);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2933, 40376, 40442);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2936, 40158, 40222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2937, 40251, 40312);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2938, 40465, 40471);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2939, 40584, 40588);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2940, 40464, 40468);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2941, 40442, 40444);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2942, 8299, 8298);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2943, 40008, 40009);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2945, 1874, 1873);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2946, 1875, 1874);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2947, 40037, 40038);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2948, 7664, 7663);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2949, 1922, 1921);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2950, 1661, 1660);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2951, 6142, 6141);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2952, 40517, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2953, 40248, 40282);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2954, 40243, 40306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2955, 1917, 1530);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2956, 5979, 5980);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2959, 1378, 1377);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2960, 40242, 40276);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2961, 40223, 40138);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2962, 40270, 40246);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2963, 40249, 40342);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2964, 40370, 40376);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2965, 40482, 40548);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2966, 5985, 5986);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2967, 2104, 2103);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2968, 4760, 4761);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2969, 4760, 4759);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2970, 1923, 1922);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2972, 1905, 1906);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2973, 3646, 3647);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2974, 2154, 6265);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2975, 2113, 2112);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2976, 4759, 4758);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2977, 2111, 2110);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2978, 2114, 2113);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2980, 2113, 2114);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2981, 4761, 4762);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2982, 5973, 5974);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2983, 5991, 5992);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2984, 2098, 2097);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2985, 3858, 3859);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2986, 7680, 7681);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2987, 3743, 3744);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2988, 1281, 1280);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2989, 1251, 1252);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2990, 2703, 4677);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2991, 8102, 8101);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2994, 1646, 1645);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2995, 2710, 2709);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2996, 3638, 3639);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2997, 2700, 2699);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2998, 6111, 6112);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2999, 40045, 40046);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3000, 40142, 40144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3001, 8192, 8191);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3002, 1716, 3612);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3007, 897, 896);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3010, 128, 129);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3011, 40135, 40168);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3012, 40471, 40472);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3013, 8044, 8462);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3014, 8040, 8052);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3015, 8782, 8783);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3016, 8781, 8782);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3017, 3885, 3886);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3019, 3626, 3627);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3020, 1656, 3634);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3021, 1655, 1656);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3022, 8306, 8305);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3023, 1666, 3642);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3024, 5813, 5814);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3025, 1254, 1255);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3028, 3704, 3705);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3029, 3913, 3914);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2280, 1308, 1309);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2284, 5715, 5716);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2285, 4796, 4797);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2286, 1013, 1014);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2388, 40132, 40228);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2389, 40001, 40002);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2390, 1547, 1548);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2391, 1082, 1074);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2392, 40021, 40022);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2393, 6123, 6124);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2394, 6122, 6123);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2395, 6121, 6122);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2396, 5673, 5674);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2397, 40048, 40054);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2398, 40218, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2407, 2700, 2701);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2417, 3954, 3955);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2418, 40481, 40516);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2419, 2390, 2391);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2420, 2389, 2390);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2421, 2388, 2389);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2422, 1287, 3678);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2423, 1250, 1251);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2424, 1249, 1250);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2433, 6093, 6094);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2444, 2677, 2678);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2453, 6841, 6842);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2465, 8171, 8172);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2466, 137, 136);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2467, 40069, 40070);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2468, 5743, 5744);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2482, 7663, 7664);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2483, 2112, 2111);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2484, 2197, 2198);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2628, 5670, 5669);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2629, 40014, 40050);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2630, 670, 671);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2631, 669, 670);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2632, 668, 669);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2633, 1358, 1359);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2634, 1357, 1358);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2635, 2701, 2700);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2636, 8284, 8283);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2637, 8298, 8297);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2638, 1930, 1929);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2639, 1931, 1930);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2640, 641, 642);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2641, 640, 641);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2642, 639, 640);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2643, 1346, 1347);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2644, 642, 643);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2645, 218, 639);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2646, 4682, 4681);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2647, 1884, 1883);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2654, 40108, 40114);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2655, 1103, 1102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2660, 6140, 6141);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2661, 564, 565);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2662, 563, 564);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2663, 6142, 6143);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2664, 6141, 6142);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2697, 1059, 949);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2698, 3925, 3926);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2699, 3897, 3896);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2700, 136, 135);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2701, 1674, 1673);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2702, 3785, 3784);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2703, 488, 487);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2710, 8288, 8287);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2711, 6147, 6148);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2712, 6146, 6147);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2713, 40188, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2715, 1309, 1310);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2716, 8048, 8036);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2717, 730, 729);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2718, 2291, 2292);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2719, 1468, 1467);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2720, 1292, 1293);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2721, 311, 3626);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2722, 7659, 7658);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2723, 6147, 6146);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2724, 6145, 6146);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2726, 6144, 6145);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2727, 8305, 8306);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2806, 1103, 1104);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2807, 8234, 8233);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2808, 40549, 40552);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2809, 2322, 2323);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2810, 6812, 6813);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2811, 40188, 40162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2813, 40226, 40227);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2814, 40037, 40072);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3018, 1549, 1550);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3026, 1253, 1254);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3027, 1252, 1253);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2298, 40552, 40528);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2322, 2208, 2209);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2609, 40200, 40206);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2610, 2100, 2099);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2613, 40168, 40144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2656, 8276, 8277);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2657, 8220, 8219);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2682, 6854, 6855);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2683, 6853, 6854);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2684, 6056, 6057);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2685, 6042, 6043);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2688, 8317, 8318);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2399, 40189, 40132);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2401, 1327, 1326);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2402, 1928, 1927);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2403, 6851, 6850);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2404, 2263, 2262);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2405, 1749, 1750);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2406, 1382, 1383);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2689, 40371, 40432);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2690, 482, 481);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2691, 6501, 6500);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2692, 6486, 6485);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2693, 793, 792);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2694, 8249, 8248);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2695, 40490, 40492);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2696, 40485, 40486);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2743, 8219, 8218);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2747, 40104, 40048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2749, 3796, 3795);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2751, 877, 878);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2752, 661, 662);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2753, 1347, 1348);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2754, 2400, 2401);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2755, 660, 661);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2756, 2399, 2400);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2757, 40339, 40340);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2758, 40019, 40024);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2759, 1691, 1690);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2771, 986, 994);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2772, 2232, 2233);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2773, 7995, 8038);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2774, 8235, 8236);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2775, 40027, 40028);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2776, 8123, 8122);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2777, 8122, 8123);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2778, 438, 437);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2779, 2147, 2146);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2780, 3827, 3828);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2781, 40026, 40027);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2782, 40025, 40026);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2783, 40078, 40024);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2784, 40218, 40162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2785, 2350, 2351);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2786, 2349, 2350);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2787, 7996, 8041);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2789, 2632, 2631);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2790, 1636, 3658);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2791, 40361, 40397);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2792, 40138, 40144);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2793, 40187, 40162);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2794, 40220, 40221);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2795, 40247, 40312);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2796, 6124, 6125);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2797, 6097, 6098);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2799, 8220, 8221);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2800, 1548, 1549);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2801, 40146, 40147);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2802, 40013, 40048);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2803, 1380, 1379);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2804, 3867, 3866);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2805, 40158, 40192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2849, 40250, 40256);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2856, 8190, 8191);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2858, 8092, 8091);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2859, 295, 294);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2865, 6149, 6150);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2866, 8224, 8225);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2873, 1897, 1898);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2875, 8193, 8194);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2876, 1873, 1872);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2877, 8191, 8192);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2878, 8095, 8094);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2879, 6869, 6870);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2884, 2131, 2130);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2894, 1904, 1903);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2895, 4665, 4666);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2904, 2301, 2302);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2905, 2300, 2301);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2915, 8330, 8331);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2916, 8257, 8256);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2923, 3857, 3858);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2925, 40038, 40042);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2926, 40585, 40588);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2927, 40345, 40348);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2928, 40465, 40468);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2929, 40344, 40348);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2930, 40585, 40591);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2934, 40376, 40382);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2935, 40257, 40288);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2944, 6055, 6054);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2957, 4661, 4660);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2958, 1377, 1376);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2971, 1923, 1924);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2979, 1885, 1884);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2992, 8103, 8102);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (2993, 40580, 40581);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3003, 40223, 40169);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3004, 3654, 1646);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3005, 40221, 40222);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3006, 897, 3712);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3008, 2708, 2707);
GO

INSERT INTO [dbo].[X_item_change_role] ([id], [lost_item_id], [got_item_id]) VALUES (3009, 2711, 2710);
GO

SET IDENTITY_INSERT [dbo].[X_item_change_role] OFF;