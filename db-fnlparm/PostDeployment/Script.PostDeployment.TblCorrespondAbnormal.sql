/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblCorrespondAbnormal
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 36, 1, 529, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 50, 1, 531, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 54, 1, 532, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 57, 1, 530, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 103, 1, 533, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 131, 1, 532, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 322, 1, 529, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 719, 1, 561, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 720, 1, 562, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 721, 1, 563, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 722, 1, 564, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (679, 724, 1, 560, 0, 40, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 36, 1, 529, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 50, 1, 531, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 54, 1, 532, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 57, 1, 530, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 103, 1, 533, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 719, 1, 561, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 720, 1, 562, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 721, 1, 563, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 722, 1, 564, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 724, 1, 560, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 905, 1, 1955, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 906, 1, 1956, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 907, 1, 1957, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 908, 1, 1958, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 909, 1, 1959, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (696, 910, 1, 1960, 0, 0, 5, 100);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 36, 1, 529, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 50, 1, 531, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 54, 1, 532, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 57, 1, 530, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 103, 1, 533, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 719, 1, 561, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 720, 1, 562, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 721, 1, 563, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 722, 1, 564, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 724, 1, 560, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 905, 1, 1955, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 906, 1, 1956, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 907, 1, 1957, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 908, 1, 1958, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 909, 1, 1959, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (697, 910, 1, 1960, 0, 0, 5, 18);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 36, 1, 529, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 50, 1, 531, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 54, 1, 532, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 57, 1, 530, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 103, 1, 533, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 719, 1, 561, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 720, 1, 562, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 721, 1, 563, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 722, 1, 564, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 724, 1, 560, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 905, 1, 1955, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 906, 1, 1956, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 907, 1, 1958, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 908, 1, 1957, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 909, 1, 1959, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (698, 910, 1, 1960, 0, 0, 5, 14);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 36, 1, 529, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 50, 1, 531, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 54, 1, 532, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 57, 1, 530, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 103, 1, 533, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 719, 1, 561, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 720, 1, 562, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 721, 1, 563, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 722, 1, 564, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 724, 1, 560, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 905, 1, 1955, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 906, 1, 1956, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 907, 1, 1957, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 908, 1, 1958, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 909, 1, 1959, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (699, 910, 1, 1960, 0, 0, 5, 11);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 36, 1, 529, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 50, 1, 531, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 54, 1, 532, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 57, 1, 530, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 103, 1, 533, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 719, 1, 561, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 720, 1, 562, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 721, 1, 563, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 722, 1, 564, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 724, 1, 560, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 905, 1, 1955, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 906, 1, 1956, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 907, 1, 1957, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 908, 1, 1958, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 909, 1, 1959, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (700, 910, 1, 1960, 0, 0, 5, 8);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 36, 1, 529, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 50, 1, 531, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 54, 1, 532, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 57, 1, 530, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 103, 1, 533, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 719, 1, 561, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 720, 1, 562, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 721, 1, 563, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 722, 1, 564, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 724, 1, 560, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 905, 1, 1955, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 906, 1, 1956, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 907, 1, 1957, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 908, 1, 1958, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 909, 1, 1959, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (701, 910, 1, 1960, 0, 0, 5, 7);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 36, 1, 529, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 50, 1, 531, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 54, 1, 532, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 57, 1, 530, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 103, 1, 533, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 719, 1, 561, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 720, 1, 562, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 721, 1, 563, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 722, 1, 564, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 724, 1, 560, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 905, 1, 1955, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 906, 1, 1956, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 907, 1, 1957, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 908, 1, 1958, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 909, 1, 1959, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (702, 910, 1, 1960, 0, 0, 5, 6);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 36, 1, 529, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 50, 1, 531, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 54, 1, 532, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 57, 1, 530, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 103, 1, 533, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 719, 1, 561, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 720, 1, 562, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 721, 1, 563, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 722, 1, 564, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 724, 1, 560, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 905, 1, 1955, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 906, 1, 1956, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 907, 1, 1957, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 908, 1, 1958, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 909, 1, 1959, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (703, 910, 1, 1960, 0, 0, 5, 5);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 36, 1, 529, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 50, 1, 531, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 54, 1, 532, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 57, 1, 530, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 103, 1, 533, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 719, 1, 561, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 720, 1, 562, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 721, 1, 563, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 722, 1, 564, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 724, 1, 560, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 905, 1, 1955, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 906, 1, 1956, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 907, 1, 1957, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 908, 1, 1958, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 909, 1, 1959, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (757, 910, 1, 1960, 0, 0, 5, 4);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 36, 1, 529, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 50, 1, 531, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 54, 1, 532, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 57, 1, 530, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 103, 1, 533, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 719, 1, 561, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 720, 1, 562, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 721, 1, 563, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 722, 1, 564, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 724, 1, 560, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 905, 1, 1955, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 906, 1, 1956, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 907, 1, 1957, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 908, 1, 1958, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 909, 1, 1959, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (2254, 910, 1, 1960, 0, 0, 5, 3);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 36, 1, 529, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 50, 1, 531, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 54, 1, 532, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 57, 1, 530, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 103, 1, 533, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 131, 1, 532, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 322, 1, 529, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 719, 1, 561, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 720, 1, 562, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 721, 1, 563, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 722, 1, 564, 0, 25, 5, 1);
GO

INSERT INTO [dbo].[TblCorrespondAbnormal] ([mOrigAID], [mCorrAID], [mType], [mCorrSkill], [mUsePerHP], [mUsePerMP], [mMaxCnt], [mPercent]) VALUES (3138, 724, 1, 560, 0, 25, 5, 1);
GO

