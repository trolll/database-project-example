/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_DiscipleExp
Date                  : 2023-10-07 09:09:31
*/


INSERT INTO [dbo].[DT_DiscipleExp] ([mLevel], [mExp], [mMaxDiscipleCount]) VALUES (1, 2000000, 1);
GO

INSERT INTO [dbo].[DT_DiscipleExp] ([mLevel], [mExp], [mMaxDiscipleCount]) VALUES (2, 5000000, 2);
GO

INSERT INTO [dbo].[DT_DiscipleExp] ([mLevel], [mExp], [mMaxDiscipleCount]) VALUES (3, 10000000, 4);
GO

INSERT INTO [dbo].[DT_DiscipleExp] ([mLevel], [mExp], [mMaxDiscipleCount]) VALUES (4, 25000000, 6);
GO

INSERT INTO [dbo].[DT_DiscipleExp] ([mLevel], [mExp], [mMaxDiscipleCount]) VALUES (5, 50000000, 8);
GO

