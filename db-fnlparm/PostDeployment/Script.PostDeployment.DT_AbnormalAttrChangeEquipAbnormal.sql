/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AbnormalAttrChangeEquipAbnormal
Date                  : 2023-10-07 09:07:29
*/


INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (1, 1963, 18, 0, 1971, '알케미스트 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (2, 1964, 18, 0, 1972, '닌자 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (3, 1965, 18, 0, 1973, '바드 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (4, 1966, 18, 0, 1974, '나이트 오브 퀸 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (5, 1967, 18, 0, 1975, '차크람 소녀 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (6, 1968, 18, 0, 1976, '여해적 선장 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (7, 1969, 18, 0, 1977, '몽크 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (8, 1970, 18, 0, 1978, '아수라 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (9, 2197, 18, 0, 2189, '광대 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (10, 2198, 18, 0, 2190, '헤라클래스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (11, 2199, 18, 0, 2191, '인페르노 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (12, 2200, 18, 0, 2192, '인페르노 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (13, 2201, 18, 0, 2193, '여신 칼리');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (14, 2202, 18, 0, 2194, '데몬 스토커');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (15, 2203, 18, 0, 2195, '버서커');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (16, 2349, 18, 0, 2351, '러블리 피어스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (17, 2350, 18, 0, 2352, '실피드 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (18, 2563, 18, 0, 2583, '황진이 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (19, 2564, 18, 0, 2584, '베르키오라 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (20, 2565, 18, 0, 2585, '헌터 리바인저 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (21, 2566, 18, 0, 2586, '스나이퍼 빅스웰 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (22, 2567, 18, 0, 2587, '미드나잇 에르메스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (23, 2946, 18, 0, 2937, '캣츠메이드 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (24, 2947, 18, 0, 2938, '데빌리스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (25, 2948, 18, 0, 2939, '엔젤리스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (26, 2962, 18, 0, 2960, '유피테르의 사념 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (27, 2963, 18, 0, 2961, '바알베크의 사령관 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (28, 2971, 18, 0, 2969, '아누비스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (29, 2972, 18, 0, 2970, '문엘프 마법사 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (30, 3078, 18, 0, 3082, '광대(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (31, 3079, 18, 0, 3083, '여신 칼리(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (32, 3080, 18, 0, 3084, '차크람 소녀(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (33, 3081, 18, 0, 3085, '데몬 스토커(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (34, 3229, 46, 0, 3232, '대지의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (35, 3230, 46, 0, 3233, '태양의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (36, 3231, 46, 0, 3234, '바람의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (37, 3366, 46, 0, 3367, '강인한 대지의 드라코 (I)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (38, 3368, 46, 0, 3369, '강인한 대지의 드라코 (II)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (39, 3370, 46, 0, 3371, '강인한 태양의 드라코 (I)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (40, 3372, 46, 0, 3373, '강인한 태양의 드라코 (I)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (41, 3374, 46, 0, 3375, '강인한 바람의 드라코 (I)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (42, 3376, 46, 0, 3377, '강인한 바람의 드라코 (II)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (43, 3378, 46, 0, 3379, '맹렬한 대지의 드라코 (I)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (44, 3380, 46, 0, 3381, '맹렬한 대지의 드라코 (II)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (45, 3382, 46, 0, 3383, '맹렬한 태양의 드라코 (I)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (46, 3384, 46, 0, 3385, '맹렬한 태양의 드라코 (II)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (47, 3386, 46, 0, 3387, '맹렬한 바람의 드라코 (I)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (48, 3388, 46, 0, 3389, '맹렬한 바람의 드라코 (II)');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (49, 3442, 18, 0, 3440, '아르테미스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (50, 3443, 18, 0, 3441, '아폴론 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (51, 3466, 18, 0, 3480, '알케미스트(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (52, 3467, 18, 0, 3481, '닌자(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (53, 3468, 18, 0, 3482, '바드(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (54, 3469, 18, 0, 3483, '나이트 오브 퀸(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (55, 3470, 18, 0, 3484, '여해적 선장(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (56, 3471, 18, 0, 3485, '몽크(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (57, 3472, 18, 0, 3486, '아수라(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (58, 3473, 18, 0, 3487, '헤라클레스(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (59, 3474, 18, 0, 3488, '버서커(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (60, 3475, 18, 0, 3489, '헌터 리바인저(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (61, 3476, 18, 0, 3490, '스나이퍼 빅스웰(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (62, 3477, 18, 0, 3491, '미드나잇 에르메스(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (63, 3478, 18, 0, 3492, '데빌리스(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (64, 3479, 18, 0, 3493, '엔젤리스(II) 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (65, 3574, 46, 0, 3578, '황야의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (66, 3576, 46, 0, 3579, '평야의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (67, 3580, 46, 0, 3584, '홍염의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (68, 3582, 46, 0, 3585, '백광의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (69, 3586, 46, 0, 3590, '질풍의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (70, 3588, 46, 0, 3591, '돌풍의 드라코');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (71, 3598, 0, 107228, 3599, '화염의 이그리트 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (72, 3600, 0, 107229, 3601, '화염의 이그리트 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (73, 3602, 0, 107228, 3603, '화마의 이그리트 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (74, 3604, 0, 107229, 3605, '화마의 이그리트 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (75, 3606, 0, 107228, 3607, '심해의 레비아탄 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (76, 3608, 0, 107229, 3609, '심해의 레비아탄 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (77, 3610, 0, 107228, 3611, '천해의 레비아탄 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (78, 3612, 0, 107229, 3613, '천해의 레비아탄 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (79, 3614, 0, 107228, 3615, '황야의 테이아 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (80, 3616, 0, 107229, 3617, '황야의 테이아 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (81, 3618, 0, 107228, 3619, '평야의 테이아 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (82, 3620, 0, 107229, 3621, '평야의 테이아 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (83, 3622, 0, 107228, 3623, '월광의 칼리고 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (84, 3624, 0, 107229, 3625, '월광의 칼리고 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (85, 3626, 0, 107228, 3627, '현월의 칼리고 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (86, 3628, 0, 107229, 3629, '현월의 칼리고 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (87, 3630, 0, 107228, 3631, '홍염의 루멘 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (88, 3632, 0, 107229, 3633, '홍염의 루멘 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (89, 3634, 0, 107228, 3635, '백광의 루멘 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (90, 3636, 0, 107229, 3637, '백광의 루멘 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (91, 3638, 0, 107228, 3639, '돌풍의 웬투스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (92, 3640, 0, 107229, 3641, '돌풍의 웬투스 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (93, 3642, 0, 107228, 3643, '질풍의 웬투스 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (94, 3644, 0, 107229, 3645, '질풍의 웬투스 아처 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (95, 3684, 18, 0, 3680, '암살단 암흑 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (96, 3685, 18, 0, 3681, '암살단 칠흑 변신');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (97, 3689, 0, 107228, 3713, '화염의 이그리트');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (98, 3690, 0, 107229, 3714, '화염의 이그리트 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (99, 3691, 0, 107228, 3715, '화마의 이그리트');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (100, 3692, 0, 107229, 3716, '화마의 이그리트 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (101, 3693, 0, 107228, 3717, '심해의 레비아탄');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (102, 3694, 0, 107229, 3718, '심해의 레비아탄 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (103, 3695, 0, 107228, 3719, '천해의 레비아탄');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (104, 3696, 0, 107229, 3720, '천해의 레비아탄 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (105, 3697, 0, 107228, 3721, '황야의 테이아');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (106, 3698, 0, 107229, 3722, '황야의 테이아 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (107, 3699, 0, 107228, 3723, '평야의 테이아');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (108, 3700, 0, 107229, 3724, '평야의 테이아 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (109, 3701, 0, 107228, 3725, '월광의 칼리고');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (110, 3702, 0, 107229, 3726, '월광의 칼리고 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (111, 3703, 0, 107228, 3727, '현월의 칼리고');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (112, 3704, 0, 107229, 3728, '현월의 칼리고 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (113, 3705, 0, 107228, 3729, '홍염의 루멘');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (114, 3706, 0, 107229, 3730, '홍염의 루멘 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (115, 3707, 0, 107228, 3731, '백광의 루멘');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (116, 3708, 0, 107229, 3732, '백광의 루멘 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (117, 3709, 0, 107228, 3733, '돌풍의 웬투스');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (118, 3710, 0, 107229, 3734, '돌풍의 웬투스 아처');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (119, 3711, 0, 107228, 3735, '질풍의 웬투스');
GO

INSERT INTO [dbo].[DT_AbnormalAttrChangeEquipAbnormal] ([mID], [mConditionAID], [mOriginAType], [mOriginAItemNo], [mEffectAID], [mDesc]) VALUES (120, 3712, 0, 107229, 3736, '질풍의 웬투스 아처');
GO

