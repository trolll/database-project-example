/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_DropGroup
Date                  : 2023-10-07 09:08:47
*/


INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (1, '6실버', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (2, '7실버', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (3, '12실버', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (4, '18실버', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (358, '데몬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (23, '초보방어구', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (24, '유즈아이템', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (26, '초보무기', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (359, '아스타로트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (29, '허수아비', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (31, '청개구리', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (32, '너구리', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (360, '루시퍼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (38, '기라카메로', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (39, '그램린', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (41, '딱정벌레', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (42, '토끼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (43, '오크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (44, '멧돼지', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (312, '스터지', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (46, '좀비', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (47, '노란줄 너구리', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (48, '혼돈의 스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (49, '홉그램린', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (50, '새끼거미', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (51, '회색그램린', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (52, '식인늑대', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (53, '거미', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (54, '고블린', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (55, '머맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (56, '구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (383, '루비그 유충', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (58, '오우거', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (59, '비홀더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (60, '코볼트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (61, '갈색곰', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (62, '동굴거미', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (63, '트롤전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (64, '오크아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (301, '고블린족장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (66, '드루이드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (67, '코카트리스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (68, '환각괴수', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (69, '흡혈박쥐', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (257, '감시자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (71, '스켈레톤아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (72, '리저드맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (73, '불딱정벌레', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (74, '파이어스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (75, '파이어스켈레톤아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (76, '라미아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (77, '와이번', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (78, '늪리저드맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (384, '루비그', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (80, '뱀파이어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (84, '소', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (82, '임프', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (85, '늪그램린', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (86, '이벤트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (87, '놀병사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (88, '오크전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (89, '경비병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (90, '코볼트아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (91, '놀아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (92, '용아병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (93, '타란튤라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (304, '미노타우르스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (95, '불의정령', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (97, '스켈레톤나이트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (98, '파이어샐러맨더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (99, '타이탄오크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (100, '뱀파이어남작', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (101, '스카', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (109, '자이언트머맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (110, '하이드라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (385, '늪 바실리스크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (371, '파주즈', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (372, '윈터와이트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (373, '불의정령 세트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (386, '헤즐링', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (117, '트로글다이트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (102, '빅스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (103, '쿠룬카', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (104, '포레스트코카트리스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (105, '버탈리언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (106, '버드퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (107, '루움', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (108, '골리앗버드이터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (116, '홉고블린', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (118, '초보섬유즈템', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (119, '에어데몬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (120, '파이어데몬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (387, '쿼드포드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (388, '킬러맨티스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (389, '흐그누이', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (124, '초보섬잡템', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (125, '방줌저드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (126, '하피', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (127, '스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (128, 'C급패키지', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (129, 'D급패키지', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (130, 'E급패키지', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (131, 'F급이상패키지', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (132, '파멸의스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (133, '트레이서', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (134, '매지컬머맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (135, '매지컬라미아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (136, '메테오스의하수인', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (137, '오크산적', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (138, '절망의스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (139, '오크약탈자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (140, '오크병사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (141, '오크주술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (142, '지옥의스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (143, '멸망의스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (144, '완더윗처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (145, '러스트뱀파이어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (146, '어둠의라미아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (147, '메테오스의호위병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (148, '메테오스의시녀', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (149, '놀산적', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (150, '놀창병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (151, '버닝구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (152, '놀패잔병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (153, '놀전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (154, '놀대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (155, '놀부대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (156, '어둠의감시자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (157, '미완의타란튤라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (158, '메테오스의추종자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (159, '사이키델릭', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (160, '트롤병사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (161, '트롤대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (162, '파이어울프', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (163, '트롤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (164, '다크트레이서', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (165, '나이트메어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (392, '메테오스의 정찰병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (167, '불지옥수문장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (168, '지옥불용아병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (169, '지옥불해골기사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (170, '자봉', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (171, '레서트롤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (172, '고블린채굴꾼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (173, '숲라미아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (174, '고블린정비병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (175, '식인고블린', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (176, '타이탄오크병사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (236, '동굴박쥐', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (237, '가고일', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (242, '코볼트채굴꾼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (243, '좀비개', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (244, '스켈레톤킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (245, '화타포스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (246, '헬게이트 가디언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (247, '블랙와이번', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (248, '망치머리', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (249, '구리조각', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (250, '철광석', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (251, '은조각', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (252, '군이벤트용', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (253, '루비', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (255, '에메랄드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (177, '타이탄오크전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (178, '타이탄오크아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (179, '타이탄오크대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (180, '투랑카의전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (181, '투랑카의돌격병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (182, '투랑카의호위병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (183, '투랑카의부대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (184, '연옥의드루이드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (185, '늪라미아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (186, '고블린슬레이어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (187, '검은줄무늬거미', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (188, '늪거미', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (189, '코모도리저드맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (190, '그물거미', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (191, '코모도리저드맨슬레이', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (192, '코모도리저드맨돌격병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (193, '네크로맨서', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (194, '박쥐', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (195, '감시자의눈', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (196, '커즈스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (197, '커즈스켈레톤아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (198, '레서구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (393, '메테오스의 쉐도우', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (200, '저주받은스켈레톤나이', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (201, '블러드스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (202, '사일런트구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (203, '헤이트구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (204, '산화된좀비', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (205, '부반쉬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (206, '광란의구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (207, '러스트라미아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (208, '혼스켈레톤아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (209, '혼스켈레톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (210, '나락의구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (211, '카밀의패잔병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (212, '뱀파이어퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (213, '레서서큐버스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (214, '커즈라미아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (215, '바이탈구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (216, '뱀파이어킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (217, '서큐버스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (218, '카샤카노', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (219, '바일런스켈베로스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (220, '언데드인형사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (221, '카이바', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (222, '로어구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (223, '소울드레인구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (224, '리치킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (225, '블러드스켈레톤아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (226, '어레스트뱀파이어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (227, '베어런뱀파이어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (228, '서먼구울', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (229, '블러드리치', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (230, '벨줴뷔트의성기사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (231, '시종장샤모스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (232, '알라스트로', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (233, '벨제뷔트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (234, '슬라임', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (238, '블러드스켈레톤나이트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (239, '카발로', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (240, '빅트롤전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (254, '사파이어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (394, '메테오스의 파괴자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (241, '게거미', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (256, '블랙유니콘', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (300, '아이시하이드라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (261, '여왕 마브', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (262, '블레이드 플린드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (263, '오우거 워리어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (264, '강철투구벌레', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (265, '자이언트바실리스크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (266, '킹 라쿰', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (299, '오우거 메이지', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (268, '만드라고라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (269, '플린드 아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (270, '레서 오우거', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (271, '스켈레톤 나이트 아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (272, '프로즌 샐러맨더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (273, '스톤오크아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (274, '스톤오크워리어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (275, '뿔거미', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (276, '버그맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (277, '고리쉬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (278, '프테라노돈', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (279, '투구벌레', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (280, '자이언트 호넷', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (281, '보가트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (282, '바실리스크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (283, '아이시 머맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (284, '플린드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (285, '레프리칸 족장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (286, '레프리칸 주술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (287, '레프리칸 전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (288, '코볼트약탈자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (289, '바룬용병대 오크졸병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (290, '바룬용병대 오크족장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (291, '바룬용병대 오크대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (292, '바룬용병대 오크전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (293, '거대 딱정벌레', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (294, '픽시', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (305, '거대전갈', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (306, '스톤골렘', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (307, '호문클로스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (308, '켈베로스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (309, '자이언트샤먼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (310, '자이언트전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (311, '자이언트족장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (313, '정화의사이클롭스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (314, '혼돈의사이클롭스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (315, '재생의사이클롭스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (316, '파괴의사이클롭스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (317, '트랜트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (318, '베이비트랜트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (319, '불도마뱀', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (320, '물도마뱀', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (321, '땅도마뱀', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (322, '바람도마뱀', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (323, '카벙클', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (324, '하이에나', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (325, '피에로', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (326, '마리오네트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (260, '5실버', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (297, '스펙터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (298, '고리쉬 퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (302, '자라탄', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (303, '베히모스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (327, '우르크하이', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (328, '블랙슬라드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (329, '맨티코어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (330, '드래고노이드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (331, '라바와이트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (332, '크라켄', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (333, '화이트슬라드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (334, '드래고노이드파이터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (335, '괴물게', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (336, '백곰', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (337, '펭귄', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (338, '비만펭귄', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (339, '황제펭귄', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (340, '미노타우르스워리어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (341, '스노우오크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (342, '스노우오크대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (343, '스노우오크궁수', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (344, '라이칸스로프', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (345, '라이칸스로프대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (346, '라이칸스로프궁수', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (347, '유적도굴꾼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (348, '아이언오크대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (349, '아이언오크궁수', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (350, '아이언오크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (351, '아타락시아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (352, '가스트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (353, '가스트처형자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (354, '세이렌', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (355, '레이번', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (356, '빛의베리얼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (357, '어둠의베리얼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (361, '빅풋', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (362, '글룸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (378, '이벤트용 꾸러미1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (379, '이벤트용 꾸러미2단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (390, '와이번 라이더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (391, '메테오스의 레인저', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (395, '메테오스의 주술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (396, '메테오스의 버서커', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (397, '메테오스의 수문장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (402, '111천년삼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (403, '111트리너비아', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (404, '111붉은 여우', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (405, '111플라밍고', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (406, '111스팅 레이', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (407, '111카필라타', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (408, '111크랩 피쉬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (409, '111아이언암 콩', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (410, '111바바리언 콩', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (411, '111아크리언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (412, '111트란잠 가디언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (413, '111롱보우 가디언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (414, '111차크람 가디언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (415, '111풀 플레이트 가디언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (416, '111샤먼 아크리언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (417, '111병사 아크리언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (418, '(이벤트드랍)네임드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (419, '111본보우 아크리언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (420, '111나가 샤먼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (421, '111나가 파이터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (422, '111카니발 마스크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (423, '111카니발 샤먼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (424, '111솔리더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (425, '111나가 퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (426, '111대장 아크리언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (427, '111파계 제천대성', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (428, '111로드 오브 아크리언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (363, '쉐이드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (364, '푸퍼맨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (365, '스파이크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (366, '아스트랄로돈', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (367, '블레이드댄서', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (368, '사커리저드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (398, '데스크로우', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (399, '데스크로우 샤먼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (400, '스컬 스파이더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (401, '도마뱀', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (380, '이벤트용 꾸러미3단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (381, '이벤트용 꾸러미4단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (382, '이벤트용 꾸러미5단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (429, '이벤트용 꾸러미0단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (430, '(이벤트드랍)이벤트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (431, '(이벤트드랍)보스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (432, '해외이벤트 1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (433, '해외이벤트 2단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (434, '해외이벤트 3단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (435, '해외이벤트 4단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (436, '해외이벤트 5단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (437, '222듀라한', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (438, '222리치 퀸', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (439, '222본 드래곤', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (440, '222좀비오우거 킹', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (441, '222쉐이드 웨폰', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (442, '222좀비오우거 전사', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (443, '222좀비오우거 아처', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (444, '222좀비오우거 주술사', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (445, '222좀비오우거 노예', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (446, '222좀비오우거 독수리', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (447, '222좀비하이에나', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (448, '<프리미엄>본 드래곤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (449, '<프리미엄>듀라한', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (450, '<프리미엄>좀비오우거 킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (451, '<프리미엄>이계의 화타포스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (452, '[오토]오크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (453, '[오토]오크아쳐', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (454, '[오토]오크전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (455, '[오토]놀병사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (456, '[오토]놀아처', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (457, '[중고랩]가디언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (494, '[반복퀘]전사의버려진', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (459, '[중고랩]차크라스톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (460, '[중고랩]호노크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (461, '[중로랩]소라게', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (462, '[중고랩]아크라의망령', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (463, '[중고랩]킹가디언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (464, '[중고랩]가재', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (465, '[중고랩]고스트감시자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (466, '[중고랩]고스트 노예', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (467, '[중고랩]대왕지네', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (468, '[중고랩]스니밀', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (469, '[중고랩]던전쥐', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (470, '[중고랩]도적총병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (471, '[중고랩]보물수호자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (472, '[중고랩]심해어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (473, '[중고랩]트롤노예', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (474, '[중고랩]캐스퍼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (475, '[중고랩]해적바이킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (476, '[중고랩]해적백정', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (477, '[중고랩]해적여자바이킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (478, '[중고랩]해적주술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (479, '[중고랩]블레어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (480, '[중고랩]헬키메라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (481, '[중고랩]해적상자1', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (482, '[중고랩]해적상자2', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (483, '[중고랩]해적상자3', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (484, '[중고랩]해적상자4', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (485, '[중고랩]해적상자5', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (486, '[이벤트]건방진토끼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (487, '[반복퀘]나르투의유적', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (488, '[반복퀘]버려진유적', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (489, '[반복퀘]암흑사제바르쿠쉬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (490, '[반복퀘]유물조사단바리오스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (491, '[093Q]소환이벤트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (492, '[중고랩]퀘스트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (498, '해외이벤트 1-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (499, '해외이벤트 2-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (500, '해외이벤트 3-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (501, '해외이벤트 4-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (502, '해외이벤트 5-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (503, '이벤트용 꾸러미0-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (504, '이벤트용 꾸러미1-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (505, '이벤트용 꾸러미2-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (506, '이벤트용 꾸러미3-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (507, '이벤트용 꾸러미4-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (508, '이벤트용 꾸러미5-1단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (509, '094Q한국소환이벤트 1급', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (510, '094Q한국소환이벤트 2급', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (511, '094Q한국소환이벤트 3급', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (512, '[카오스]황동석문지기', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (513, '[카오스]룬파수꾼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (528, '해외몬스터일반(이벤트2010_2q)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (529, '해외몬스터네임드(이벤트2010_2q)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (530, '해외몬스터보스(이벤트2010_2q)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (532, '[일루]변질된엘프', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (533, '[일루]머맨티릅스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (534, '[일루]머맨록사르', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (535, '[일루]드라그람돌', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (536, '[일루]드라그람군단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (537, '[일루]드라그람화염술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (538, '[일루]거머리스콜피온', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (539, '[일루]붉은가시팬서', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (540, '[일루]타락한와이번', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (541, '[일루]변질된그리폰', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (542, '[일루]스테고사우르스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (543, '[일루]황천의기사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (544, '[일루]황천의레인저', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (545, '[일루]황천의주술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (546, '[중고랩]던전쥐(퀘스트)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (547, '바포메트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (548, '리틀 고블린', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (549, '어린 오크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (550, '뿔거북 두꺼비', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (551, '기네아 와이번', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (554, '[만월]보병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (555, '[만월]사제', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (556, '[만월]궁병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (557, '[만월]전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (558, '[만월]정령', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (559, '[만월]식인귀', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (560, '[만월]나이트메어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (561, '[안식]정찰병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (562, '[안식]궁수', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (563, '[안식]암살자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (564, '[안식]창기병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (565, '[안식]광전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (566, '[안식]와이번', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (567, '[안식]파멸의에틴', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (568, '[안식]캐슬골렘', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (570, '[만월]기사단장라르카', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (571, '[안식]타나토스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (575, '[스팟]강철의 심판자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (576, '[스팟]혼돈의 파괴자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (577, '[스팟]타락한 지배자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (578, '룬 보스(중급)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (579, '룬 보스(고급)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (580, '룬 보스(메테탑)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (582, '(상층)공포의 나이트메어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (583, '(상층)블러드 트레이서', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (584, '(상층)푸른 드루이드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (585, '(상층)가디언의 사냥개', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (586, '(상층)가디언의 사냥꾼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (587, '(상층)절망의 네크로맨서', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (588, '(상층)언데드 마법사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (589, '(상층)글룸 워리어', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (743, '해외 할로윈 실버카드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (493, '[중고랩]퀘스트아이템', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (514, '[카오스]피조물주술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (531, '[일루]쿼드포드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (552, '고대의 감시자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (553, '[만월]장교', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (581, '룬(이프리트)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (590, '(상층)리치킹의 도살자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (591, '(상층)리치킹의 사제', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (592, '선물상자조각 0.13%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (593, '선물상자조각 0.18%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (594, '선물상자조각 0.24%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (595, '선물상자조각 0.28%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (596, '선물상자조각 0.35%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (597, '선물상자조각 0.39%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (598, '선물상자조각 0.44%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (599, '선물상자조각 0.59%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (600, '선물상자조각 0.7%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (601, '선물상자조각 0.88%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (602, '선물상자조각 1.17%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (603, '선물상자조각 2%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (604, '선물상자조각 4%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (605, '선물상자조각 7.5%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (606, '선물상자조각 25%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (607, '선물상자조각 50%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (608, '선물상자조각 100%', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (610, '러시아이벤트(열정1단계)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (611, '러시아이벤트(승리2단계)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (612, '러시아이벤트(영광3단계)', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (613, '(상층)1번 몬스터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (614, '(상층)2번 몬스터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (615, '(상층)4번 몬스터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (616, '(상층)3번 몬스터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (617, '(상층)5번 몬스터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (618, '(상층)공용아이템', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (626, '<프리미엄>데몬 스토커', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (627, '<프리미엄>여신 칼리', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (628, '<프리미엄>아스모데우스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (629, '<프리미엄>이계의 고리쉬 퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (630, '<프리미엄>이계의 스펙터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (631, '<프리미엄>아바돈', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (632, '<프리미엄>이계의 자이언트 전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (633, '<프리미엄>이계의 자이언트 샤먼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (634, '<프리미엄>이계의 좀비오우거 킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (635, '<프리미엄>이계의 본 드래곤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (636, '<프리미엄>이계의 헬게이트 가디언', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (637, '(상층)퀘스트_흑룡', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (638, '(상층)퀘스트_왕무', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (639, '러시아파계제천대성상자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (640, '러시아로드오브아크리언상자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (648, '(엘테르마을)오크전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (649, '(엘테르마을)오크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (650, '(엘테르마을)오크리더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (651, '(엘테르천사)헤이크단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (652, '(엘테르천사)엘더스톤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (653, '(엘테르천사)나투라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (654, '(엘테르천사)엘디온', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (655, '(엘테르천사)엘자드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (656, '(엘테르천사)베이크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (657, '(엘테르천사)광휘의엘윈더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (658, '(엘테르천사)광휘의엘로라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (659, '(엘테르천사)수호자앨리엇', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (660, '(엘테르천사)수호자앨리시스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (661, '(엘테르천사)수호자엘사라드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (662, '(엘테르악마)역병의엔더스트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (663, '(엘테르악마)역병의엔스칸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (664, '(엘테르악마)역병의엔마지', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (665, '(엘테르악마)둥지수호자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (666, '(엘테르악마)둥지파수꾼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (667, '(엘테르악마)원한의엔락', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (668, '(엘테르악마)원한의엔쥬크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (669, '(엘테르악마)원한의통곡', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (670, '(엘테르악마)핏빛의이블퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (671, '(엘테르악마)이블시드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (672, '(엘테르악마)핏빛의이블킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (673, '(엘테르중립)그라둔', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (674, '(엘테르중립)베일먼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (675, '(엘테르중립)골리엇', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (676, '(엘테르중립)파샤드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (677, '(엘테르중립)토우두', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (678, '(엘테르중립)스렌달', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (679, '(엘테르중립)켈크론', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (680, '(엘테르중립)에일크론', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (681, '(엘테르중립)프레크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (682, '(엘테르중립)로얄그리폰', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (683, '(엘테르중립)엘더버드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (684, '(엘테르보스)유피테르사념', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (685, '(엘테르보스)바알베크사령관', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (744, '해외 할로윈 골드카드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (749, '[메테오스]메테오스 드랍A', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (750, '[메테오스]블랙드래곤 드랍C', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (689, '<프리미엄>이계의 우르크하이', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (690, '<프리미엄>이계의 블랙 와이번', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (691, '<프리미엄>이계의 리치 퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (692, '<프리미엄>이계의 듀라한', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (693, '(유피던전1층)루나틱엘윈더', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (694, '(유피던전1층)고대의발키리', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (695, '(유피던전1층)신의하인', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (696, '(유피던전1층)카오틱엘르시르', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (697, '(바알던전1층)타락한이블퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (698, '(바알던전1층)악마암살자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (699, '(바알던전1층)각성한이블킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (700, '(바알던전1층)드라군나이트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (701, '유피테르의 열쇠', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (706, '(카오스)하급 및 중급 상자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (707, '(카오스)중급 및 고급 상자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (713, '(상층)퀘스트_화탑', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (714, '(상층)투사투랑카', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (710, '(카오스)큐브2배', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (711, '(카오스)큐브3배', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (712, '(카오스)큐브4배', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (715, '(필드이벤트)일반 A그룹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (716, '(필드이벤트)일반 B그룹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (717, '(필드이벤트)일반 C그룹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (718, '(필드이벤트)일반 D그룹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (719, '(필드이벤트)일반 E그룹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (720, '(필드이벤트)보스 A그룹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (721, '(필드이벤트)보스 B그룹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (722, '(필드이벤트)본 드래곤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (723, '(필드이벤트)리치 퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (724, '(필드이벤트)듀라한', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (725, '(필드이벤트)좀비오우거 킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (726, '(유피던전2층)분쇄자 엘크단', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (727, '(유피던전2층)파괴천사의 손', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (728, '(유피던전2층)소울크러쉬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (729, '(유피던전2층)아크엔젤', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (730, '(바알던전2층)전도자 엔스칸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (731, '(바알던전2층)지옥뱀 엔더스트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (732, '(바알던전2층)타락한 용의 꽃', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (733, '(바알던전2층)아크데몬', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (734, '빛나는 유피테르의 열쇠', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (735, '빛나는 바알베크의 열쇠', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (736, '<프리미엄>사무라이', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (737, '<프리미엄>신의 하인', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (738, '<프리미엄>글라디에이터', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (739, '<프리미엄>실라이론', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (740, '<프리미엄>헤라클레스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (741, '<프리미엄>나이트 오브 퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (742, '<프리미엄>노아스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (751, '[스팟1팀1]퀸오브나이트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (752, '[스팟1팀2]베르키오라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (753, '[스팟1팀3]러블리피어스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (754, '[스팟1팀4]알케미스트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (755, '[스팟1팀5]미드나잇에르메스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (756, '[스팟2팀1]헤라클래스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (757, '[스팟2팀2]아수라', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (758, '[스팟2팀3]버서커', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (759, '[스팟2팀4]몽크', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (760, '[스팟2팀5]헌터리바인저', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (761, '[스팟3팀1]엔젤리스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (762, '[스팟3팀2]데빌리스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (763, '[스팟3팀3]광대', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (764, '[스팟3팀4]칼리', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (765, '[스팟3팀5]스나이퍼빅스웰', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (766, '[신기]주시자의 망토 A', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (767, '[신기]투명 망토 A', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (768, '[신기]투명 망토 B', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (769, '[신기]투명 망토 C', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (770, '[신기]투명 망토 D', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (771, '[일루]암컷 스테고 사우르스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (784, '[메테오스]메테오스 드랍B', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (495, '[중고랩]해적상자6', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (496, '[중고랩]해적상자7', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (497, '[중고랩]해적상자8', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (702, '바알베크의 열쇠', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (745, '해외 할로윈 VIP카드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (786, '라르카 환영', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (515, '[카오스]대지의군주', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (516, '[카오스]뾰족뿔감시자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (517, '[카오스]위장한나방망루병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (518, '[카오스]맹독모쓰', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (519, '[카오스]맹독의군주', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (520, '[카오스]수룡문지기', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (521, '[카오스]네눈박이감시자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (522, '[카오스]심해의고룡', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (523, '[카오스]대해의정복자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (524, '[카오스]불꽃의총통', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (525, '[카오스]화염심문관', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (526, '[카오스]화염불꽃술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (527, '[카오스]불꽃의폭군', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (569, '바포메트의 환영', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (572, '유적지 정찰병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (573, '유적지 궁수', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (574, '유적지 부대장', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (641, '<프리미엄>언딘', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (642, '<프리미엄>레프리컨', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (643, '<프리미엄>리치 퀸', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (644, '<프리미엄>각성한 이블킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (645, '<프리미엄>이계의 리치 킹', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (646, '<프리미엄>이계의 벨제뷔트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (647, '<프리미엄>알케미스트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (746, '[메테오스]블랙드래곤 드랍A', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (747, '그리헨텔의 의지', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (772, '타나토스 방패병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (773, '타나토스 망치전사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (774, '타나토스 저격수', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (775, '타나토스 마법사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (776, '각성 타나토스', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (777, '라르카 근위기사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (778, '라르카 고위기사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (779, '라르카 대총병', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (780, '라르카 집정관', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (781, '각성 라르카', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (782, '타나토스 환영', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (748, '[메테오스]블랙드래곤 드랍B', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (787, '천공의 수호자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (788, '천공의 검', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (789, '수호비석', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (790, '하늘의 귀신', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (791, '암석거인', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (792, '대지의 수호자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (793, '대지의 검', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (794, '문어 주술사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (795, '어둠의 추적자', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (796, '코브라 병사', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (797, '공중던전 퀘스트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (798, '지하광장 퀘스트', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (799, '떠도는 나이트 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (800, '헤메는 나이트 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (801, '떠도는 어쌔신 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (802, '헤메는 어쌔신 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (803, '떠도는 서모너 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (804, '헤메는 서모너 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (805, '떠도는 엘프 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (806, '헤메는 엘프 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (807, '떠도는 레인저 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (808, '헤메는 레인저 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (809, '광폭화된 파샤드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (810, '광폭화된 토우드', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (811, '광폭화된 스렌달', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (812, '광폭화된 에일크론', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (813, '광폭화된 베일먼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (814, '광폭화된 골리엇', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (815, '광폭화된 켈크론', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (816, '고뇌하는 나이트 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (817, '혼란스러운 나이트 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (818, '고뇌하는 어쌔신 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (819, '혼란스러운 어쌔신 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (820, '고뇌하는 서모너 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (821, '혼란스러운 서모너 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (822, '고뇌하는 레인저 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (823, '혼란스러운 레인저 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (824, '고뇌하는 엘프 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (825, '혼란스러운 엘프 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (826, '방황하는 나이트 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (827, '침묵하는 나이트 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (828, '방황하는 어쌔신 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (829, '침묵하는  어쌔신 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (830, '방황하는 서모너 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (831, '침묵하는  서모너 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (832, '방황하는 레인저 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (833, '침묵하는 레인저 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (834, '방황하는 엘프 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (835, '침묵하는  엘프 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (836, '광폭화된 나이트 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (837, '분노한 나이트 영혼', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (842, '10주년 이벤트 (A,D)', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (843, '10주년 이벤트 (E,J)', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (840, '수료증 드랍', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (841, '시험장 공통 드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (844, '10주년 이벤트 (K,P)', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (845, '10주년 이벤트 (Q,S)', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (846, '10주년 이벤트 (네임드, 스팟)', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (847, '10주년 이벤트 (보스)', 1);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (848, '동굴 티폰 드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (849, '동굴 나가 드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (850, '암살단 암흑 드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (851, '암살단 칠흑 드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (852, '암흑 사제단 집행자 드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (853, '암흑 사제단 도살자 드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (854, '피괴 암흑 소환체 드랍', 0);
GO

INSERT INTO [dbo].[TP_DropGroup] ([DGroup], [DName], [DDropType]) VALUES (855, '수호 암흑 소환체 드랍', 0);
GO

