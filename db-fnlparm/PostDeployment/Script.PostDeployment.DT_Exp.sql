/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_Exp
Date                  : 2023-10-07 09:07:22
*/


INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (1, 130, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (2, 312, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (3, 499, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (4, 748, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (5, 1024, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (6, 1732, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (7, 2734, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (8, 4456, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (9, 6568, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (10, 8566, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (11, 11233, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (12, 15678, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (13, 21453, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (14, 28345, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (15, 37324, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (16, 45456, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (17, 54123, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (18, 64356, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (19, 77124, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (20, 92343, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (21, 111234, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (22, 141231, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (23, 181231, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (24, 221232, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (25, 274252, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (26, 318132, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (27, 365851, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (28, 420728, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (29, 479629, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (30, 546777, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (31, 617858, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (32, 698179, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (33, 781960, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (34, 875795, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (35, 972132, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (36, 1079066, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (37, 1197763, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (38, 1317539, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (39, 1449292, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (40, 2825777, 400);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (41, 3111713, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (42, 3418818, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (43, 3748113, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (44, 4100642, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (45, 4830002, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (46, 6338418, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (47, 9833681, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (48, 14833681, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (49, 20572838, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (50, 24851353, 150);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (51, 30056418, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (52, 46086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (53, 56086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (54, 66086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (55, 76086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (56, 86086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (57, 96086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (58, 106086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (59, 126086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (60, 156086423, 40);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (61, 196086423, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (62, 246086423, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (63, 282999386, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (64, 325449293, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (65, 374266687, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (66, 430406690, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (67, 494967693, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (68, 569212847, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (69, 654594774, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (70, 752783990, 12);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (71, 865701588, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (72, 995556826, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (73, 1144890349, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (74, 1328072805, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (75, 1540564454, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (76, 1787054767, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (77, 2072983530, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (78, 2404660895, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (79, 2861546465, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (80, 3405240293, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (81, 4052235949, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (82, 4822160779, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (83, 5738371327, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (84, 7000813019, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (85, 8540991883, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (86, 10420010097, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (87, 12712412318, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (88, 15509143028, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (89, 19386428785, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (90, 24233035981, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (91, 30291294976, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (92, 37864118720, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (93, 47330148400, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (94, 60582589952, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (95, 77545715139, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (96, 99258515378, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (97, 127050899684, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (98, 162625151596, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (99, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (100, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (101, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (102, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (103, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (104, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (105, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (106, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (107, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (108, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (109, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (110, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (111, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (112, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (113, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (114, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (115, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (116, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (117, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (118, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (119, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (120, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (121, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (122, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (123, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (124, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (125, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (126, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (127, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (128, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (129, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (130, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (131, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (132, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (133, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (134, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (135, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (136, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (137, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (138, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (139, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (140, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (141, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (142, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (143, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (144, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (145, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (146, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (147, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (148, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (149, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (150, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (151, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (152, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (153, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (154, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (155, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (156, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (157, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (158, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (159, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (160, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (161, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (162, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (163, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (164, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (165, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (166, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (167, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (168, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (169, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (170, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (171, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (172, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (173, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (174, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (175, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (176, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (177, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (178, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (179, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (180, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (181, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (182, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (183, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (184, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (185, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (186, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (187, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (188, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (189, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (190, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (191, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (192, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (193, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (194, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (195, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (196, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (197, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (198, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (199, 213038948591, 5);
GO

INSERT INTO [dbo].[DT_Exp] ([ELevel], [EExp], [ERestExpRate]) VALUES (200, 213038948591, 5);
GO

