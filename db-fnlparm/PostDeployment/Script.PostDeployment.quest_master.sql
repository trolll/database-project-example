/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : quest_master
Date                  : 2023-10-07 09:07:22
*/


INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (2, '制作暗红之装甲');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (3, '拜德.莱特的请求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (6, '内森的礼物');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (9, '斯塔克斯的新手指南');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (10, '阿尔洛玛的游戏指南');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (11, '博恩森的请求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (12, '与果狸的战争');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (13, '佩尼的请求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (14, '沃尔特的兔子大战争');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (20, '赛希尔的好奇心');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (21, '寻找兽骨之剑吧');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (23, '乌尔莉娅的道具指南');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (24, '托米的请求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (25, '制作哥布林金属护手');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (32, '消除捣蛋鬼哥布林');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (33, '消除僵尸');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (34, '消除亡者骸骨');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (42, '哈特的哈皮打猎要求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (43, '哈特的蜥蜴人打猎要求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (44, '哈特的兽人战士打猎要求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (45, '哈特的烈焰亡者骸骨打猎要求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (46, '哈特的最后测试');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (47, '制作深红之装甲2');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (52, '凯宾的信息');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (53, '骨头制作的骑士');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (54, '兽骨之剑');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (55, '熔岩铸剑');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (56, '制作熔岩铸剑');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (62, '托米的请求2');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (63, '托米的请求3');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (72, '寻找兽骨之剑2');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (73, '玛瑙剑');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (74, '玛瑙剑封印');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (75, '玛瑙剑封印解除');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (82, '制作哥布林金属靴子');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (83, '制作哥布林金属头盔');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (85, '伊芙利特封印');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (86, '封印磐石');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (87, '伊芙利特的复活');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (88, '古代遗书');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (90, '伊芙利特的讨伐准备');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (91, '折剑');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (92, '修理折剑');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (93, '一种不会熄灭的火光');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (94, '修理完毕的剑');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (95, '别的请求');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (96, '生气的恩特曼');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (97, '锤子(冶炼用）');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (98, '冶炼夹子');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (99, '小礼物');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (100, '制作超级加速秘药');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (101, '制作负重之秘药');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (102, '对黑暗祭司群的境界');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (103, '恶的气息');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (104, '强化石的秘密');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (105, '消除黑暗祭司监视者');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (106, '巴登地图');
GO

INSERT INTO [dbo].[quest_master] ([questid], [quest_nm_cn]) VALUES (107, '莱姆.金的指教');
GO

