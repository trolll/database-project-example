/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblItemIncSysPossibleItem
Date                  : 2023-10-07 09:09:31
*/


INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (239, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (239, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (239, 2, 0, 22.5, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (240, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (240, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (240, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (241, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (241, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (241, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (351, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (351, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (351, 2, 0, 7.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (353, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (353, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (353, 2, 0, 7.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (355, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (355, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (355, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (356, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (356, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (356, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (358, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (358, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (358, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (359, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (359, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (359, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (360, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (360, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (360, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (362, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (362, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (362, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (411, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (411, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (411, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (500, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (500, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (500, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (707, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (707, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (707, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (711, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (711, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (711, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (714, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (714, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (714, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (715, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (715, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (715, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (750, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (750, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (750, 2, 0, 22.5, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (751, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (751, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (751, 2, 0, 22.5, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (752, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (752, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (752, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (778, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (778, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (778, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (779, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (779, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (779, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (781, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (781, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (781, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (783, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (783, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (783, 2, 0, 22.5, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (802, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (802, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (802, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (813, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (813, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (813, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (814, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (814, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (814, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (815, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (815, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (815, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (816, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (816, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (816, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (817, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (817, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (817, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (819, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (819, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (819, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (845, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (845, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (845, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (847, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (847, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (847, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (865, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (865, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (865, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (901, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (901, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (901, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (918, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (918, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (918, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (919, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (919, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (919, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (920, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (920, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (920, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (921, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (921, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (921, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (922, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (922, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (922, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (923, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (923, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (923, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (924, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (924, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (924, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (925, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (925, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (925, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (951, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (951, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (951, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (952, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (952, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (952, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (953, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (953, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (953, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (954, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (954, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (954, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (955, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (955, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (955, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (956, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (956, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (956, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (957, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (957, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (957, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (958, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (958, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (958, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (959, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (959, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (959, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (960, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (960, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (960, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (961, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (961, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (961, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (962, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (962, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (962, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (963, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (963, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (963, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (964, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (964, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (964, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (965, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (965, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (965, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (966, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (966, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (966, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (967, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (967, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (967, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (968, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (968, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (968, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (969, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (969, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (969, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (970, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (970, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (970, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (971, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (971, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (971, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (972, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (972, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (972, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (973, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (973, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (973, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (974, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (974, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (974, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (975, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (975, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (975, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (976, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (976, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (976, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (999, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (999, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (999, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1000, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1000, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1000, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1001, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1001, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1001, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1002, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1002, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1002, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1034, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1034, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1034, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1050, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1050, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1050, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1051, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1051, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1051, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1052, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1052, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1052, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1053, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1053, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1053, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1164, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1164, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1164, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1170, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1170, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1170, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1210, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1210, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1210, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1211, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1211, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1211, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1288, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1288, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1288, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1289, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1289, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1289, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1290, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1290, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1290, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1291, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1291, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1291, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1384, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1384, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1384, 2, 0, 8.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1390, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1390, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1390, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1391, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1391, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1391, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1392, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1392, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1392, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1581, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1581, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1581, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1596, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1596, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1596, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1619, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1619, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1619, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1620, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1620, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1620, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1621, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1621, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1621, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1623, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1623, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1623, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1624, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1624, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1624, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1938, 0, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1938, 1, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1938, 2, 0, 11.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1940, 0, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1940, 1, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1940, 2, 0, 11.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1950, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1950, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (1950, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2047, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2047, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2047, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2048, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2048, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2048, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2049, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2049, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2049, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2052, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2052, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2052, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2428, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2428, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2428, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2445, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2445, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2445, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2446, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2446, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2446, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2490, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2490, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2490, 2, 0, 18.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2691, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2691, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2691, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2827, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2827, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (2827, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3297, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3297, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3297, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3298, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3298, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3298, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3299, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3299, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3299, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3390, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3390, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3390, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3498, 0, 0, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3498, 1, 0, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3498, 2, 0, 2.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3499, 0, 0, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3499, 1, 0, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3499, 2, 0, 2.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3500, 0, 0, 5.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3500, 1, 0, 5.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3500, 2, 0, 1.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3501, 0, 0, 5.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3501, 1, 0, 5.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3501, 2, 0, 1.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3973, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3973, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3973, 2, 0, 34.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3974, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3974, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3974, 2, 0, 34.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3975, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3975, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3975, 2, 0, 34.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3976, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3976, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3976, 2, 0, 34.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3977, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3977, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3977, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3978, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3978, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (3978, 2, 0, 26.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4056, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4056, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4056, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4057, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4057, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4057, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4058, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4058, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4058, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4059, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4059, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4059, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4388, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4388, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4388, 2, 0, 26.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4389, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4389, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4389, 2, 0, 19.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4829, 0, 0, 5.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4829, 1, 0, 5.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4829, 2, 0, 4.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4835, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4835, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4835, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4892, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4892, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4892, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4893, 0, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4893, 1, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4893, 2, 0, 11.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4894, 0, 0, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4894, 1, 0, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (4894, 2, 0, 8.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5243, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5243, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5243, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5244, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5244, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5244, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5245, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5245, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5245, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5246, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5246, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5246, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5514, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5514, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5514, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5519, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5519, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5519, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5520, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5520, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5520, 2, 0, 33.75, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5521, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5521, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5521, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5556, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5556, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5556, 2, 0, 26.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5571, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5571, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5571, 2, 0, 26.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5572, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5572, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5572, 2, 0, 19.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5573, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5573, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5573, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5574, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5574, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5574, 2, 0, 34.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5575, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5575, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5575, 2, 0, 34.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5576, 0, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5576, 1, 0, 45.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5576, 2, 0, 34.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5898, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5898, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5898, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5899, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5899, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5899, 2, 0, 19.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5900, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5900, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5900, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5902, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5902, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5902, 2, 0, 19.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5903, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5903, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5903, 2, 0, 19.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5904, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5904, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5904, 2, 0, 19.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5908, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5908, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5908, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5909, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5909, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5909, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5910, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5910, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5910, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5911, 0, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5911, 1, 0, 40.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5911, 2, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5913, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5913, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (5913, 2, 0, 19.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6181, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6181, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6181, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6182, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6182, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6182, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6183, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6183, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6183, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6231, 0, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6231, 1, 0, 25.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6231, 2, 0, 19.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6331, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6331, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6331, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6332, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6332, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6332, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6548, 0, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6548, 1, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6548, 2, 0, 11.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6549, 0, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6549, 1, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6549, 2, 0, 11.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6551, 0, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6551, 1, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6551, 2, 0, 11.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6630, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6630, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6630, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6631, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6631, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6631, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6632, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6632, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6632, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6633, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6633, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6633, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6634, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6634, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6634, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6635, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6635, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6635, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6636, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6636, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6636, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6637, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6637, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6637, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6638, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6638, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6638, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6639, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6639, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6639, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6640, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6640, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6640, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6641, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6641, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6641, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6642, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6642, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6642, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6643, 0, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6643, 1, 0, 30.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6643, 2, 0, 23.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6660, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6660, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6660, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6661, 0, 0, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6661, 1, 0, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6661, 2, 0, 8.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6662, 0, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6662, 1, 0, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6662, 2, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6663, 0, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6663, 1, 0, 15.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6663, 2, 0, 11.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6683, 2, 1, 100.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6684, 3, 1, 66.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6685, 4, 1, 44.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6686, 5, 1, 20.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (6687, 10, 2, 10.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7557, 3, 1, 80.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7558, 4, 1, 70.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7559, 5, 1, 70.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7560, 10, 2, 70.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7565, 0, 0, 100.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7565, 1, 0, 100.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7565, 2, 0, 100.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7566, 0, 0, 95.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7566, 1, 0, 95.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7566, 2, 0, 95.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7567, 0, 0, 90.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7567, 1, 0, 90.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7567, 2, 0, 90.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7568, 0, 0, 85.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7568, 1, 0, 85.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7568, 2, 0, 85.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (7572, 2, 1, 90.0, 0, 2);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (40609, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (40609, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (40609, 2, 0, 26.25, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (40610, 0, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (40610, 1, 0, 35.0, 0, 1);
GO

INSERT INTO [dbo].[TblItemIncSysPossibleItem] ([mIID], [mStatus], [mCubeType], [mProb], [mResource], [mKind]) VALUES (40610, 2, 0, 7.0, 0, 1);
GO

