/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblPopupGuideDialog
Date                  : 2023-10-07 09:09:29
*/


INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-12 18:48:00.000', 1, '角色移动', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[角色移动]
</p>
<p>
欢迎进入R2 Online. R2 Online的第一步!! 如何移动角色. 可以利用键盘
</p><p color=ff99ffcc> [W,A,S,D]</p><p> 移动角色. W是前进, S是后退, A是左转弯,D是右转弯, 按W键旁的R键 可以自动奔跑. 也可以点击鼠标左击来移动角色. 
</p>
</text>
</GUItext>', '2011-07-25 15:06:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-12 18:58:00.000', 2, '升级', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[等级提升]
</p>
<p>
恭喜您. 您的角色等级得到了提升. 在每次等级得到提升时角色的HP/MP等能力值也会得到提升
</p><p>且达到一定等级时 </p><p color=ffff9999>根据职业特性属性值也会得到提升</p><p>

</p>
</text>
</GUItext>
', '2011-07-12 19:00:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-12 20:46:00.000', 3, '战斗方法', '<GUIText ver=2>
<text no=0>
<p>
[冒险的开始]


- 角色移动

在开始新的冒险之前先向您介绍一下简单的操作方法.

在R2中您可以通过键盘上的[W,A,S,D]键进行移动.

W为前进, S为后退, A为左回转,D为右回转.按W键 

旁边的R键将会自动跑步.

且可以通过点击鼠标左键来进行角色的移动.



- 战斗方法

将鼠标移动至怪物上方后当鼠标箭头变成刀剑的摸样时

按住鼠标左键将会开始攻击. 

开始攻击怪物后将鼠标移动至画面下方正中央的刀剑摸样时

将进行自动攻击.

且R2里可以与其他玩家进行战斗.

在按住ctrl键的情况下用鼠标左键点击对方

或在聊天窗口内输入 /强制攻击 开 后用鼠标左键点击对方将可以进行攻击.

在与其他玩家进行对战中将其他玩家击杀时角色的倾向值也会进行变化. 



- 游戏帮助使用方法

为了让玩家更好更快的了解R2的各项系统功能

增加了游戏帮助功能.

玩家在游戏内按键盘上的F9键可以在游戏帮助内

搜索到各种关于游戏内容的帮助提示.



[帮助种类]

1. 职业说明

2. 道具

3. 赠送和快捷键设定

4. 物品购买

5. 战斗

6. 聊天和过滤器

7. 公会

8. 负重和饱腹度

9. 精灵魔法

10. 召唤师魔法

11. 技能书

12. 游戏命令

</p>
</text>
</GUIText>', '2011-07-20 22:22:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-12 20:53:00.000', 4, '帮助', '<GUIText ver=2>
<text no=0>
<p>
[艾斯本镇]


欢迎来到艾斯本镇.

艾斯本镇是属于复仇之地的一个村镇.

复仇之地总共由1个城和6个营地构成.

且是一个很多冒险家门前来拜访休息的村镇.

村镇内有技能书商人/特殊道具出售商人/铁匠等各种各样的 

NPC，玩家可以按"M"键来确定NPC们的位置并且加以使用. 

</p>
</text>
</GUIText>', '2013-05-31 20:15:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-12 21:21:00.000', 5, '道具获得与使用', '<GUIText ver=2>
<text no=0>
<p>
[复仇之城]


欢迎来到复仇之城.

复仇之城是每周星期六进行攻城战的攻城地区. 

在攻城战中获胜的公会将拥有城池. 

想确认拥有城池的公会可以按 ''M''键在

世界地图里查看到城池或营地的情报.




[攻城战]


参加条件 :拥有公会的每个人都可以参加攻城战.

时间 : 每周星期六19：00~21:00. 


攻城战规则 : 

1. 在攻城战结束的时间段内占领守护石的公会将拥有城池.

2. 在攻城战进行期间将每个城的守护塔最终进行占领的公会将获得城池.

3. 破坏守护塔后将出现10分钟的等待占领状态，在经过10分钟后才能被认可为占领的状态.

4. 在等待占领的状态下守护塔再次遭到破坏时

守护塔的所有权将会进行变更且再次进入到等待占领的状态当中.  

5. 在等待占领的状态下攻城战结束时该领地将会被认为没有占领者. 

像5一样处于没有被占领状态的营地/城池

叫做废墟化状态且可以通过世界地图进行确认. 

营地也会根据攻城战的规则进行变化.



</p>
</text>
</GUIText>', '2011-07-12 21:22:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-12 21:29:00.000', 6, '快捷栏使用方法', '<GUIText ver=2>
<text no=0>
<p>
[拜伦镇]


欢迎来到拜伦镇.

拜伦镇是属于拜伦领地的一个村镇.

拜伦领地由6个营地和1个城来构成的.

村镇内有杂货/武器/防具商人等各种 NPC

且可以按"M"键来确认NPC们的位置并且加以使用. 






</text>
</GUIText>', '2012-05-11 17:17:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 17:18:00.000', 7, '任务执行方法', '<GUIText ver=2>
<text no=0>
<p>
[拜伦城]


欢迎来到拜伦城.

拜伦城是每周星期六进行攻城战的攻城地区. 

在攻城战中获胜的公会将拥有城池. 

想确认拥有城池的公会可以按 ''M''键在

世界地图里查看到城池或营地的情报.




[攻城战]


参加条件 :拥有公会的每个人都可以参加攻城战.

时间 : 每周星期六19：00~21:00. 


攻城战规则 : 

1. 在攻城战结束的时间段内占领守护石的公会将拥有城池.

2. 在攻城战进行期间将每个城的守护塔最终进行占领的公会将获得城池.

3. 破坏守护塔后将出现10分钟的等待占领状态，在经过10分钟后才能被认可为占领的状态.

4. 在等待占领的状态下守护塔再次遭到破坏时

守护塔的所有权将会进行变更且再次进入到等待占领的状态当中.  

5. 在等待占领的状态下攻城战结束时该领地将会被认为没有占领者. 

像5一样处于没有被占领状态的营地/城池

叫做废墟化状态且可以通过世界地图进行确认. 

营地也会根据攻城战的规则进行变化.



</text>
</GUIText>', '2011-07-13 17:24:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 17:30:00.000', 8, '新技能', '<GUIText ver=2>
<text no=0>
<p>
[罗敦镇]


欢迎来到罗敦镇.

罗敦镇是属于罗敦领地的一个村镇.

罗敦领地由6个营地和1个城来构成的.

村镇内有杂货/武器/防具商人等各种 NPC

且可以按"M"键来确认NPC们的位置并且加以使用. 



</text>
</GUIText>', '2011-07-13 17:33:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 17:43:00.000', 9, '道具强化系统', '<GUIText ver=2>
<text no=0>
<p>
[罗敦城]


欢迎来到罗敦城.

罗敦城是每周星期六进行攻城战的攻城地区. 

在攻城战中获胜的公会将拥有城池. 

想确认拥有城池的公会可以按 ''M''键在

世界地图里查看到城池或营地的情报.




[攻城战]


参加条件 :拥有公会的每个人都可以参加攻城战.

时间 : 每周星期六19：00~21:00. 


攻城战规则 : 

1. 在攻城战结束的时间段内占领守护石的公会将拥有城池.

2. 在攻城战进行期间将每个城的守护塔最终进行占领的公会将获得城池.

3. 破坏守护塔后将出现10分钟的等待占领状态，在经过10分钟后才能被认可为占领的状态.

4. 在等待占领的状态下守护塔再次遭到破坏时

守护塔的所有权将会进行变更且再次进入到等待占领的状态当中.  

5. 在等待占领的状态下攻城战结束时该领地将会被认为没有占领者. 

像5一样处于没有被占领状态的营地/城池

叫做废墟化状态且可以通过世界地图进行确认. 

营地也会根据攻城战的规则进行变化.




</text>
</GUIText>', '2011-07-13 17:52:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 17:54:00.000', 10, '道具合成系统', '<GUIText ver=2>
<text no=0>
<p>
[流浪者镇]


欢迎来到流浪者镇.

与其他村镇拥有不一样的气氛的这个地方在出售

刺客职业可以使用的毒.

且仓库管理员和武器/杂货商人也在这里等待着需要他们帮助的冒险家.

玩家可以按"M"键来确认NPC们的位置并且加以使用. 


</text>
</GUIText>', '2011-07-13 18:00:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 18:19:00.000', 11, '角色死亡与惩罚', '<GUIText ver=2>
<text no=0>
<p>
[黑土镇]


欢迎来到黑土镇.

黑土镇是位于黑暗大陆的村镇.

黑暗大陆领地由1个城和6个营地构成.

村镇内有仓库管理员和武器/防具,杂货商人等各种NPC

玩家可以按"M"键来确认NPC们的位置并且加以使用. 



</text>
</GUIText>', '2011-07-29 12:48:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 18:36:00.000', 12, 'PvP战斗与规则', '<GUIText ver=2>
<text no=0>
<p>
[黑暗之城]


欢迎来到黑暗之城.

黑暗之城是每周星期六进行攻城战的攻城地区. 

在攻城战中获胜的公会将拥有城池. 

想确认拥有城池的公会可以按 ''M''键在

世界地图里查看到城池或营地的情报.




[攻城战]


参加条件 :拥有公会的每个人都可以参加攻城战.

时间 : 每周星期六19：00~21:00. 


攻城战规则 : 

1. 在攻城战结束的时间段内占领守护石的公会将拥有城池.

2. 在攻城战进行期间将每个城的守护塔最终进行占领的公会将获得城池.

3. 破坏守护塔后将出现10分钟的等待占领状态，在经过10分钟后才能被认可为占领的状态.

4. 在等待占领的状态下守护塔再次遭到破坏时

守护塔的所有权将会进行变更且再次进入到等待占领的状态当中.  

5. 在等待占领的状态下攻城战结束时该领地将会被认为没有占领者. 

像5一样处于没有被占领状态的营地/城池

叫做废墟化状态且可以通过世界地图进行确认. 

营地也会根据攻城战的规则进行变化.


</text>
</GUIText>', '2011-07-13 18:36:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 18:41:00.000', 13, '道具交易', '<GUIText ver=2>
<text no=0>
<p>
[变身项链卡片]

恭喜您获得了变身项链卡片.

变身项链卡片是通过位于村镇里的''幸运术士 卡肯''

可以交换到符合角色等级的变身项链的道具.

变身项链的使用期限是在进行兑换变身项链时随机决定.



[期限延长道具]

可以使用"黄金锤子" 或 "银色锤子" 道具

来延长道具的使用期限. 银色锤子可以在

特殊道具商人那里进行购买且可以用银币来进行购买.

而黄金锤子可以用跨服攻城战点数来进行购买.

银色锤子 - 1天 (只可使用1~3次)
黄金锤子 - 3/5/7天 (只可使用1~10次)

需要注意的是使用时间剩1小时以上24小时未满的

德拉克戒指和变身项链才可延长有效期限.


</p>

<m width=48 height=48 value=10001940></m>
<p>&nl;&nl;&nl;&nl;&nl;&nl;</p>
</text>
</GUIText>', '2011-07-14 16:27:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 18:55:00.000', 14, '点击技能树', '<GUIText ver=2>
<text no=0>
<p>
[变身系统]


玩家可以使用怪物掉落的变身法杖或变身项链等道具来进行怪物变身. 

根据变身的怪物种类有的变身有助于战斗且有的变身将不利于战斗. 

有助于战斗的变身拥有增加 HP/MP及攻击速度等非常好的效果.

根据角色的等级能够进行变身的怪物的种类将越来越多

且使用变身法杖时变身的怪物种类将随机选取

但在佩戴变身戒指后使用变身法杖时

可以在变身列表里面选择变身的怪物种类.

</p>
</text>
</GUIText>', '2011-07-13 18:57:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 19:01:00.000', 15, '公会系统', '<GUIText ver=2>
<text no=0>
<p>
[经验增幅之咒]


恭喜您获得了经验增幅之咒.

经验增幅之咒是可以让玩家在有效期限内进行狩猎时获得额外经验值的道具. 

根据不同种类的经验增幅之咒所额外获得的经验值数量也会不同.

想确认正在使用中的道具期限及种类可以按T 键进行确认.

</p>
</text>
</GUIText>', '2011-07-13 19:12:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 19:14:00.000', 16, '角色移动&吉内亚岛', '<GUIText ver=2>
<text no=0>
<p>
[德拉克卡片]


恭喜您获得了德拉克卡片.

德拉克卡片是通过位于村镇里的''幸运术士 卡肯''

可以交换到德拉克戒指的道具.

德拉克戒指的使用期限是在进行兑换德拉克戒指时随机决定.



[期限延长道具]

可以使用"黄金锤子" 或 "银色锤子" 道具

来延长道具的使用期限. 银色锤子可以在

特殊道具商人那里进行购买且可以用银币来进行购买.

而黄金锤子可以用跨服攻城战点数来进行购买.

银色锤子 - 1天 (只可使用1~3次)
黄金锤子 - 3/5/7天 (只可使用1~10次)

需要注意的是使用时间剩1小时以上24小时未满的

德拉克戒指和变身项链才可延长有效期限.


</p>

<m width=48 height=48 value=10001940></m>
<p>&nl;&nl;&nl;&nl;&nl;&nl;</p>
</text>
</GUIText>', '2011-07-27 15:51:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 19:15:00.000', 17, '角色移动&阿克拉岛', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[角色移动]
</p>
<p>
欢迎进入R2 Online. R2 Online的第一步!! 如何移动角色.

可以利用键盘</p><p color=ff99ffcc> [W,A,S,D]</p><p> 移动角色. W是前进, S是后退, A是左转弯,D是右转弯, 按W键旁的R键 可以自动奔跑. 也可以点击鼠标左击来移动角色.

</p>
<p color=ffff6600>[阿克拉岛]</p>
<p>

阿克拉岛是 为R2初步玩家的村，可以从NPC们学会关于游戏的基本知识. 各角色执行基础任务，就能达到11级.

达到11级后可以移动其他地区, 请通过打怪和执行任务来升级. 还有，阿克拉是专门为精灵和召唤师职业的空间，

骑士/游侠/刺客无法入内. 分布为，为初步玩家的低级怪物和为20~40级玩家的中高级怪物.想再次访问阿克拉岛的玩家们通过传送员随时可以访问.

</p>
</text>
</GUIText>', '2011-07-27 15:55:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 19:16:00.000', 18, '艾斯本镇', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[艾斯本镇]</p>
<p>
欢迎访问艾斯本镇.

艾斯本镇是属于复仇之地的村，周边栖宿着薄弱的怪物.

因很多冒险家总访问艾斯本镇，所以商业比别的村子发达.

包括技能书商人/特殊贩卖商人/铁匠/公会管理员/材料商人 等各种NPC, 流浪者镇农场也是通过艾斯本镇进入，可以称为在石墨岛领地中艾斯本镇是商业最活泼的村子.

请用快捷键M来确认村子内NPC信息.
</p>
</text>
</GUIText>', '2011-07-13 19:16:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-13 19:32:00.000', 19, '风车平原', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[风车平原]</p>
<p>
介绍执行低级别任务时必须得访问的风车平原.

风车平原分别于南边和北边, 都出现不同的怪物. 因怪物等级比较低 </p><p color=ffff9999>要升到10~20级</p><p>，还是比较容易的.

那么该介绍登场的怪物们了.

</p>
<p color=ffffff66>- 怪物情报(南地区) - 
</p>
<p color=ff99ffcc>* 哥布林: C等级, 被动,
</p>
<p color=ff99ffcc>* 捣蛋鬼哥布林: C等级, 被动
</p>
<p color=ff99ffcc>* 食人狼: C等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 棕熊: B等级, 被动
</p>
<p color=ffffff66>- 怪物情报(北地区) -
</p>
<p color=ff99ffcc>* 死亡克鲁: C等级, 被动
</p>
<p color=ff99ffcc>*死亡克鲁巫师: C等级, 主动, 被动
</p>
<p> 风车平原北地区是可以执行</p><p color=ffff9999>伊克伦道具</p><p>奖励的每日任务. 
</p>
</text>
</GUIText>', '2011-07-29 12:08:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 12:49:00.000', 20, '亡者之地', '<GUIText ver=2>
<text no=0>
<p>

[延长时效道具] 

可以使用 "黄金锤子" 或 "银色锤子"延长剩下的时间. 

银色锤子由特殊商人出手贩售,可以用银币购买. 

黄金锤子可以使用混沌点数购买,且可以延长一定的时效. 

银色锤子- 1天 (可以使用1~3次) 

黄金锤子- 3/5/7天 (可以使用1~10次)

可以延长使用时间剩下1小时以上24小时以內的德拉克戒指与变身项链 

请参考.




</p>
</text>
</GUIText>', '2011-07-14 12:49:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 12:50:00.000', 21, '豺狼人的营寨', '<GUIText ver=2>
<text no=0>
<p>

[延长时效道具] 

可以使用 "黄金锤子" 或 "银色锤子"延长剩下的时间. 

银色锤子由特殊商人出手贩售,可以用银币购买. 

黄金锤子可以使用混沌点数购买,且可以延长一定的时效. 

银色锤子- 1天 (可以使用1~3次) 

黄金锤子- 3/5/7天 (可以使用1~10次)

可以延长使用时间剩下1小时以上24小时以內的德拉克戒指与变身项链 

请参考.




</p>
</text>
</GUIText>', '2011-07-14 12:50:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 12:51:00.000', 22, '兽人营地', '<GUIText ver=2>
<text no=0>
<p>

[延长时效道具] 

可以使用 "黄金锤子" 或 "银色锤子"延长剩下的时间. 

银色锤子由特殊商人出手贩售,可以用银币购买. 

黄金锤子可以使用混沌点数购买,且可以延长一定的时效. 

银色锤子- 1天 (可以使用1~3次) 

黄金锤子- 3/5/7天 (可以使用1~10次)

可以延长使用时间剩下1小时以上24小时以內的德拉克戒指与变身项链 

请参考.




</p>
</text>
</GUIText>', '2011-07-14 12:52:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 12:53:00.000', 23, '半人鱼栖息地', '<GUIText ver=2>
<text no=0>
<p>

[延长时效道具] 

可以使用 "黄金锤子" 或 "银色锤子"延长剩下的时间. 

银色锤子由特殊商人出手贩售,可以用银币购买. 

黄金锤子可以使用混沌点数购买,且可以延长一定的时效. 

银色锤子- 1天 (可以使用1~3次) 

黄金锤子- 3/5/7天 (可以使用1~10次)

可以延长使用时间剩下1小时以上24小时以內的德拉克戒指与变身项链 

请参考.




</p>
</text>
</GUIText>', '2011-07-14 12:54:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 12:55:00.000', 24, '黑暗洞窟', '<GUIText ver=2>
<text no=0>
<p>
[祝福的强化精华碎片]. 

您获得了祝福的强化精华碎片. 

祝福的强化精华碎片可以通过组合成为祝福的

强化精华的道具且进行组合时总共需要3个碎片.

玩家可以通过组合快捷键''J''按键激活组合窗口

后将物品栏里的3个祝福的强化精华碎片用拖动

或双击的方式放入组合窗内进行组合从而获得祝

福的强化精华.组合祝福的强化精华的成功率为

100%且组合材料的碎片必须为3个.


</p>
</text>
</GUIText>', '2013-10-29 15:59:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 14:38:00.000', 25, '烈焰塔', '<GUIText ver=2>
<text no=0>
<p>
[特殊精华强化之咒] 

特殊精华强化之咒是强化特殊精华的必要道具.

特殊精华的强化可以一定几率成功,如果强化失

败特殊精华有可能会消失掉.

* 强化方法 

在拥有特殊精华强化之咒和特殊精华道具的状态

下跟装备强化的同一种方式进行强化.


</p>
</text>
</GUIText>', '2013-10-29 16:14:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 14:47:00.000', 26, '蜘蛛林', '<GUIText ver=2>
<text no=0>
<p>
[特殊精华变换之咒] 

特殊精华变换之咒是将持有的特殊精华的种类

变换成其他种类的触媒类型的道具. 

*变换方法* 

在拥有特殊精华变换之咒和特殊精华的状态下

通过NPC "<活动精华>赫伊特"进行交换.



</p>
</text>
</GUIText>', '2011-07-14 14:48:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 14:48:00.000', 27, '复仇之城', '<GUIText ver=2>
<text no=0>
<p>
[无视护甲] 

游侠职业的50级技能 

无视护甲跟其他职业的技能不同,是一个特殊的技能.

将原有的一定几率脱落对方防具的属性修改为根据自

身的远程攻击命中率影响到该技能的成功率的形式,对

方的防具虽然不再脱落但一定几率随机降低对方防御力.

且在使用技能的同时使用者本人可以获得暴击及命中率

上升,怪物系杀戮的追加效果从而将自身的攻击力最大化. 

*无视护甲技能效果* 

1) 给对方 

- 一定时间内降低对方随机数值的防御力 

2) 给自身 

- 提高自身的暴击几率 
- 提高自身的命中率 
- 给自身附加上怪物系狩猎效果





</p>
</text>
</GUIText>', '2011-07-14 14:48:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 14:49:00.000', 28, '黑土镇', '<GUIText ver=2>
<text no=0>
<p>
[嗜血肯布吉特之弓] 

拥有强烈的血红色气息的弓.

弓自身的性能很优秀且是唯一可以使用嗜血箭的强力武器. 

嗜血箭是一定几率降低对方药水恢复量的特殊箭矢.

嗜血箭是可以用‘嗜血晶石’和钢铁箭,以及一定的手续费

通过流浪者镇的‘胡克’进行制作.



</p>
</text>
</GUIText>
', '2011-07-14 14:49:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 15:33:00.000', 29, '地精营地', '<GUIText ver=2>
<text no=0>
<p>
[灵魂锁之弓] 

用珍贵材料制作而成的灵魂锁之弓是一个强力且可以使用

特殊箭矢的特殊弓箭.

灵魂锁之弓是唯一一把可以使用锁链箭的武器.

锁链箭是一定几率让被攻击对象的周围的敌人受到伤害的拥

有强大威力的箭矢. 

[锁链箭] 

锁链箭是可以用‘锗’和钢铁箭,以及一定的手续

费通过流浪者镇的‘胡克’进行制作.




</p>
</text>
</GUIText>

', '2011-07-14 15:34:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 16:30:00.000', 30, '不死地牢', '<GUIText ver=2>
<text no=0>
<p>
[嗜血箭] 

嗜血箭是唯一一个配合嗜血肯布吉特之弓使用的专用箭矢.

嗜血箭与一般箭矢不同拥有特殊的效果且可以配合毒瓶/猛毒瓶一起使用. 

受到嗜血箭的攻击时,将有一定几率降低通过药水恢复的HP恢复量. 

*制作方法 

嗜血箭是可以用‘嗜血晶石’和钢铁箭,以及一定的手续费通过流浪者镇

的‘胡克’进行制作.





</p>
</text>
</GUIText>', '2011-07-14 16:31:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 16:52:00.000', 31, '黑龙沼泽', '<GUIText ver=2>
<text no=0>
<p>
[嗜血晶石]

嗜血晶石是制作嗜血肯布吉特之弓专用弓箭嗜血箭的核心材料。

嗜血箭不同于一般的弓箭有特殊的功能，同时还可以实用毒瓶/猛毒瓶。

对攻击目标HP药水恢复量降低的DBUFF状态。

*制作方法*

到流浪者镇找‘胡克’便可以制作，制作材料有‘嗜血晶石’和钢铁箭，

支付的一定的手续费。





</p>
</text>
</GUIText>', '2011-07-14 16:54:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 17:00:00.000', 32, '帝王墓穴', '<GUIText ver=2>
<text no=0>
<p>
[锁链箭] 

锁链箭是灵魂锁之弓专用箭矢，锁链箭不同于一般弓箭有独特的效果，

也可以正常使用毒瓶/猛毒瓶一起使用. 

锁链箭是一定几率让被攻击对象的周围的敌人受到伤害的拥有强大威力的箭矢. 

*制作方法

锁链箭是可以用‘锗’和钢铁箭,以及一定的手续费通过流浪者镇

的‘胡克’进行制作.




</p>
</text>
</GUIText>
', '2011-07-14 17:01:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 17:05:00.000', 33, '混沌神殿', '<GUIText ver=2>
<text no=0>
<p>
[锁链箭] 

锁链箭是灵魂锁之弓专用箭矢，锁链箭不同于一般弓箭有独特的效果，

也可以正常使用毒瓶/猛毒瓶一起使用. 

锁链箭是一定几率让被攻击对象的周围的敌人受到伤害的拥有强大威力的箭矢. 

*制作方法

锁链箭是可以用‘锗’和钢铁箭,以及一定的手续费通过流浪者镇

的‘胡克’进行制作.




</p>
</text>
</GUIText>
', '2011-07-14 17:05:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 17:09:00.000', 34, '极乐之巢', '<GUIText ver=2>
<text no=0>
<p>
[锁链箭] 

锁链箭是灵魂锁之弓专用箭矢，锁链箭不同于一般弓箭有独特的效果，

也可以正常使用毒瓶/猛毒瓶一起使用. 

锁链箭是一定几率让被攻击对象的周围的敌人受到伤害的拥有强大威力的箭矢. 

*制作方法

锁链箭是可以用‘锗’和钢铁箭,以及一定的手续费通过流浪者镇

的‘胡克’进行制作.




</p>
</text>
</GUIText>
', '2011-07-14 17:09:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-18 18:30:00.000', 35, '精灵神殿', '<GUIText ver=2>
<text no=0>
<p>
[锗] 

锗是坚硬并且锋利，受到强烈冲击可反弹的神秘的矿石。

这种矿石在''荣光圣殿''被精灵所发现，并做为制作武器的材料。

锁链箭是一定几率让被攻击对象的周围的敌人受到伤害的拥有强大威力的箭矢. 

*制作方法

锁链箭是可以用‘锗’和钢铁箭,以及一定的手续费通过流浪者镇

的‘胡克’进行制作.







</p>
</text>
</GUIText>
', '2011-07-18 18:30:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 17:38:00.000', 36, '秩序神殿', '<GUIText ver=2>
<text no=0>
<p>
[荣光圣殿]

您所在位置为荣光圣殿，荣光圣殿通过特定的结界才可进入的一个神秘区域。

荣光圣殿里有无法使用传送之咒、传送卷轴等传送有关的任何道具，

并且也无法使用骑乘德拉克，如神秘德拉克等。

但是通过''封印奇美拉卷轴''和神秘德拉克合成后，可获得在圣殿内骑乘的

华丽的新骑乘德拉克。

在这里可以获得武器、首饰、很多新道具，神秘圣殿带你挑战和探索。





</p>
</text>
</GUIText>', '2011-07-14 17:38:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 17:39:00.000', 37, '黑暗之城', '<GUIText ver=2>
<text no=0>
<p>
[技能列表系统]

恭喜您达到11级了!角色达到11级就可以使用技能列表，并且学习各种技能。

技能列表窗口可以通过快捷键''ALT键+ K''激活，您可以任意学习个人技能。

11级开始随着等级的提升获得技能点，等级每提升1级可获得1点技能点。

40级前(未满40级)只能学习通用技能，40级后可开始学习职业专属技能。


*快捷键 

K：可激活已学习的技能，在此界面的技能图标拖到快捷栏里可使用技能。

Ctrl+K:激活公会技能列表。

ALT+K:激活个人技能列表




</p>
</text>
</GUIText>
', '2011-07-14 17:39:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 17:42:00.000', 38, '拜伦镇', '<GUIText ver=2>
<text no=0>
<p>
[职业技能列表] 

恭喜您达到35级了!角色达到35级就可以使用职业技能列表了。

技能列表窗口可以通过快捷键''ALT键+ K''激活，您可以任意学习个人技能。

职业技能列表每个职业有2种技能树，根据您的需求学习技能和发展技能树。

有些技能是必须完成指定任务后才可以学习。


*快捷键

 K：可激活已学习的技能，在此界面的技能图标拖到快捷栏里可使用技能。

Ctrl+K:激活公会技能列表。

ALT+K:激活个人技能列表。





</p>
</text>
</GUIText>

', '2011-07-14 17:42:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 17:45:00.000', 39, '德拉克农场', '<GUIText ver=2>
<text no=0>
<p>
[技能列表]

恭喜你达到60级了!角色的等级11~60级期间，每升一级可获得技能点，

之后无法获得技能点。快捷键''ALT键+ K''激活，您可以任意学习个人技能。

职业技能列表每个职业有2种技能树，根据您的需求学习技能和发展技能树。


*快捷键 

K：可激活已学习的技能，在此界面的技能图标拖到快捷栏里可使用技能。

Ctrl+K:激活公会技能列表。

ALT+K:激活个人技能列表




</p>
</text>
</GUIText>
', '2011-07-14 17:45:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 17:51:00.000', 40, '石锤阵', '<GUIText ver=2>
<text no=0>
<p>
[礼物箱]

礼物箱里有待领取的物品，保管箱物品先按''I''键，

在角色信息背包窗口的右下方，有一个礼盒装图标，

点击即可查阅礼物箱信息。


</p>
</text>
</GUIText>
', '2011-07-14 17:51:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 18:07:00.000', 41, '妖精岭', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[妖精岭]
</p>
<p>
妖精岭是伊芙利特为了讨伐精灵移住到石墨岛的精灵界的妖精们而建设的暂住之所.

这里出现下级妖精- 小仙子, 一个月一次出现小仙子的队长 ‘玛波女王’. 给魅力的妖精们许下愿望的话, 妖精实现该愿望同时也要给付出代价. 有传闻也有可能以许愿人的性命做出代价，所以许愿的时候需要慎重.

妖精岭是邻近拜伦镇的</p><p color=ffff9999>营地</p><p>山坡上面有守护石, 请挑战营地战.

</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 小仙子: F等级, 被动
</p>
<p color=ff99ffcc>* 玛波女王:  被动
</p>
</text>
</GUIText>', '2011-07-14 18:07:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 18:19:00.000', 42, '古城那勒图', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[古城那勒图]
</p>
<p> 欢迎访问古城那勒图.

位置于拜伦领地和罗敦领地境界的古城那勒图是存留着莱普力卡种族文明的都市.

莱普力卡种族是文明发达的种族，有名的是坚固而生态的建筑.之所以他们设计的建筑物出色的别的种族是无法模仿，为了抢夺建筑样式也经常被侵略.

为了侦察外部的侵略，他们在削壁搭建建筑物，在深坑里创造基地.

古城那勒图虽是拜伦领地 </p><p color=ffff9999>营地</p><p>地区, 但位于离村子最远的地方.所以营地战斗时请通过/记忆命令确保有利的位置.

狩猎区难度是 </p><p color=ffff9999>30级初，中期</p><p>狩猎难度较低.

</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 莱普力卡巫师: D等级, 被动
</p>
<p color=ff99ffcc>* 莱普力卡战士: D等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 莱普力卡族长: D等级, 主动, 变身时被动
</p>
</text>
</GUIText>', '2011-07-14 18:19:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 18:28:00.000', 43, '圣甲虫谷', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[圣甲虫谷]
</p>
<p>
欢迎访问圣甲虫谷.

虫子们的天国-圣甲虫谷是药材商们为了夺取材料而频繁侵略的地方.

属于拜伦领地的副本之一 是</p><p color=ffff9999>1 ~ 3层</p><p的结构</p><p color=ffff9999>可以进行营地战的地区.
</p>
<p>

大部分登场的是相似于虫子样貌的怪物, 谷最下称-3层里守着 ‘毒刺蝎女皇’BOSS.

狩猎场难度是 </p><p color=ffff9999>20级后期~40级初期</p><p>可以各种级别的玩家一起游戏的地区.

可以掉落多样的材料和宝石,卷轴.

</p>
<p color=ffffff66>- 怪物情报(1层) -
</p>
<p color=ff99ffcc>* 巨大的圣甲虫: D等级, 被动
</p>
<p color=ff99ffcc>* 盔甲虫: E等级, 被动
</p>
<p color=ff99ffcc>* 巨大的黄蜂, F等级, 主动, 变身时被动
</p>
<p color=ffffff66>- 怪物情报(2层) -
</p>
<p color=ff99ffcc>*巨大的黄蜂, F等级, 主动, 变身时被动 
</p>
<p color=ff99ffcc>* 毒刺蝎: G等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 潜行者: I等级, 主动, 变身时被动
</p>
<p color=ffffff66>- 怪物情报(3层) -
</p>
<p color=ff99ffcc>* 苍蝇人: H等级, 主动, 探测透明
</p>
<p color=ff99ffcc>* 斯巴克: J等级, 主动 探测透明
</p>
<p color=ff99ffcc>* 毒刺蝎女皇: BOSS, 主动, 探测透明
</p>
</text>
</GUIText>', '2011-07-14 18:28:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 18:59:00.000', 44, '埃吉尔洞窟', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[埃吉尔洞窟]
</p>
<p>
欢迎访问拜伦领地的第二个副本-埃吉尔洞窟.

这地区也跟圣甲虫谷一样</p><p color=ffff9999>可以进行营地战的地区</p><p> </p><p color=ffff9999>1~3层</p><p>的结构.

狩猎场的难度是比圣甲虫谷稍微高一点, 出现高一等的怪物. 埃吉尔洞窟怪物都是主动，所以为了安全最好是保持变身状态.

</p><p color=ffffff66>- 怪物情报(1层) - 
</p>
<p color=ff99ffcc>* 河豚人: E等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 海神: F等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 冰霜沙罗曼蛇: G等级, 主动
</p>
<p color=ffffff66>- 怪物情报(2层) -
</p>
<p color=ff99ffcc>* 博加特: G等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 骸骨高级弓箭手: H等级, 主动, 探测透明 
</p>
<p color=ffffff66>- 怪物情报(3层) -
</p>
<p color=ff99ffcc>* 翼龙: F等级, 主动
</p>
<p color=ff99ffcc>* 骸骨高级弓箭手: H等级, 主动, 探测透明
</p>
<p color=ff99ffcc>* 阿斯特拉鲁顿: L等级, 主动, 探测透明
</p> <p color=ff99ffcc>* 斯佩特: BOSS, 主动, 探测透明
</p>
</text>
</GUIText>', '2011-07-14 18:59:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:04:00.000', 45, '哈斯特祭坛', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[哈斯特祭坛]
</p>
<p>
哈斯特祭坛是为了安慰混血豺狼人的祖先-翼龙的灵魂而搭建的祭坛.

在这里出没的混血豺狼人是从天上到陆地后适应陆地而进化的鸟类.在高地带搭建了基地.

传闻道混血豺狼人的首长 ‘混血豺狼人之嗜血者’为了不被外部种族侵略，安慰灵魂祭那天, 就出现在祭坛上.

狩猎场难度是 </p><p color=ffff9999>30~ 40级</p><p>祭坛周边触摸的怪物较多，有利于升级.

</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 混血豺狼人: F等级, 被动
</p>
<p color=ff99ffcc>* 混血豺狼人弓箭手: F等级, 主动, 变身时被动
</p>
</text>
</GUIText>', '2011-07-14 19:04:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:04:00.000', 46, '拜伦城', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[拜伦城]
</p>
<p>
欢迎访问拜伦城.

石墨岛的第三个镇-拜伦城是可以通过攻城来占领的战场地区，位于拜伦镇北边岛的中间.

要进入拜伦城的话，过狭窄的桥墩后从连接到绝壁边的入口. 是难以进入的.

通过地图（M）可以查看现领有的工会.
</p>
</text>
</GUIText>', '2011-07-14 19:04:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:09:00.000', 47, '罗敦镇', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[罗敦镇]
</p> <p> 欢迎访问石墨岛的第四个镇-罗敦镇.
</p>
<p color=ffff9999>罗敦地区之存在一个城，没有营地是这个城的特征</p><p>.

大部分是崎岖的地形，出没较强的怪物. 而比较冷僻但，勇猛的冒险家经常来往的地区.

适合</p><p color=ffff9999>40级 </p><p>以上的角色. 狩猎时需要的道具都可以在这里的NPC处购买.

只能在罗敦镇杂货商店处购买的‘传送卷轴’是有利于战斗中使用，所以到罗敦镇时请通过杂货商NPC处购买.

</p>
<m width=36 height=36 value=10001481></m> <p color=ffffff66>&nl; 传送卷轴 &nl;&nl; &nl;</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 土狼: E等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 卡蓬克: F等级, 被动
</p>
<p color=ff99ffcc>* 巨大的天蝎: G等级, 被动
</p>
<p color=ff99ffcc>* 巨大的火焰恶魔: M等级, 主动
</p>
</text>
</GUIText>', '2011-07-14 19:09:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:12:00.000', 48, '深红峡谷', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[深红峡谷]
</p>
<p>
欢迎光临, 这里是在罗敦镇最早遇到的狩猎场-深红峡谷.

罗敦镇绝壁地下的峡谷周边驻扎着狼人和派西魔斯.

狩猎场难度是 </p><p color=ffff9999>40~40后期</p><p>怪物数虽不多，但都是主动性.

</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 狼人: G等级, 主动, 变身时被动 
</p>
<p color=ff99ffcc>* 狼人弓箭手: H等级, 主动
</p>
<p color=ff99ffcc>* 派西魔斯: L等级, 主动
</p>
</text>
</GUIText>', '2011-07-14 19:12:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:14:00.000', 49, '维拉梯田', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[维拉梯田]
</p>
<p>
维拉梯田位于罗敦领地北边的山区，驻扎着狼人种族.

浪人们经常密集在积水的火山口附近.

</p><p color=ffff9999>40级初期</p><p> 玩家适合狩猎

</p> <p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 巨大的天蝎: G等级, 被动
</p>
<p color=ff99ffcc>* 狼人: G等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 狼人弓箭手: H等级, 主动
</p>
<p color=ff99ffcc>* 浪人队长: K等级, 主动, 探测透明
</p>
<p color=ff99ffcc>* 恶魔战士史雷得: G等级, 主动, 变身时被动, 探测透明
</p>
</text>
</GUIText>', '2011-07-14 19:14:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:20:00.000', 50, '牛角迷宫', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[牛角迷宫]
</p>
<p>
牛角迷宫是以前罗敦领主- 巴尔卡马里休斯搭建的迷宫.

除了巴尔卡马里休斯，在罗敦居住的居民们也能使用，但是治安不好，现在被怪物占领。

牛角迷宫中间有个小神殿，听说偶尔会出现牛头人的首长 ‘牛头人勇士’. 牛角迷宫虽属于罗敦领地，但邻近黑暗大陆.推荐从黑暗领地移动.

狩猎场难度是 </p><p color=ffff9999>40级初期~50级初期</p><p>是属于中，高级别玩家的狩猎场.

</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 地狱犬: K等级, 主动, 探测透明
</p>
<p color=ff99ffcc>* 牛头人: L等级, 主动
</p>
<p color=ff99ffcc>* 牛头人勇士: 主动, 探测透明
</p>
</text>
</GUIText>', '2011-07-14 19:26:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:22:00.000', 51, '时间之湖', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[时间之湖]
</p>
<p>
时间之湖周边能看到的海螺样的岩石充分表示出这里驻扎怪物的特征.

这里驻扎着克拉肯和怪物蟹等小生物，偶尔也会出现想盗取它们宝石的遗迹盗取者.

狩猎难度是</p><p color=ffff9999>30级后期~40级初期</p><p>在罗敦领地狩猎场中属于较低的狩猎场.

</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 遗迹盗取者: F等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 克拉肯: G等级, 被动
</p>
<p color=ff99ffcc>* 怪物蟹: G等级, 被动
</p>
<p color=ff99ffcc>* 海妖塞壬: 主动, 探测透明
</p>
</text>
</GUIText>', '2013-10-29 15:59:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:25:00.000', 52, '扎坦盆地', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[扎坦盆地]
</p>
<p>
长满了枯木和仙人掌的扎坦盆地里经常出没巨型龟和树人.

这地区怪物大部分比较温顺，碰到人类也不会攻击.

</p><p color=ffff9999>40级别</p><p>玩家适合狩猎, 这里的 ‘巨型龟’石墨岛里唯一能掉落‘力量腰带’的怪物.

</p> <m width=36 height=36 value=10000259></m> <p color=ffffff66>&nl; 力量腰带 &nl;&nl; &nl;</p>
<p color=ffffff66>- 怪物情报 -
</p> <p color=ff99ffcc>* 婴儿树人: E等级, 被动</p>
<p color=ff99ffcc>* 树人: H等级, 被动 </p> <p color=ff99ffcc>* 巨型龟: J等级, 被动 </p> </text> </GUIText>', '2011-07-14 19:25:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:28:00.000', 53, '巨人的安息地', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[巨人的安息地]
</p>
<p> 
欢迎访问巨人的安息地. 这里是巨人生活的地区. 巨人不想别的种族侵犯自己的地区，而选择绝壁地区来驻扎.

罗敦领地中难度最高的狩猎场. </p><p color=ffff9999>40级后期~ 50级初期</p><p>玩家适合狩猎 ‘巨人战士’可以掉落提升角色负重的‘巨人腰带’. 

</p>
<m width=36 height=36 value=10000713></m> <p color=ffffff66>&nl; 巨人腰带 &nl;&nl; &nl;</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 巨人巫师: L等级, 主动, 探测透明
</p>
<p color=ff99ffcc>* 巨人战士: N等级, 主动, 探测透明
</p>
</text>
</GUIText>', '2011-07-14 19:28:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:30:00.000', 54, '城镇废墟', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[城镇废墟]
</p>
<p>
罗敦城的初代城主- 需要建设村子. 因罗敦城里的原住民编辑各处不好管理，怪物们和其他领地种族的侵略频繁. 而在罗敦领地的中央，邻近水源的地方搭建了一个镇.

让编辑各处的原住民都移住到罗敦镇，在这过程中生成很多废弃的村子. 现在钢铁兽人种族占领了其中最大的村子. 

狩猎场难度是 </p><p color=ffff9999>40级别</p><p>威胁性的怪物较多.

</p>
<p color=ffffff66>- 怪物情报 -
</p>
<p color=ff99ffcc>* 土狼: E等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 钢铁兽人: G等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 钢铁兽人弓箭手: G等级, 主动
</p>
<p color=ff99ffcc>* 钢铁兽人队长: J等级, 主动, 探测透明
</p>
</text>
</GUIText>', '2011-07-14 19:30:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:33:00.000', 55, '埃塔斯洞窟', '<GUIText ver=2>
<text no=0>
<p color=ffff6600>[埃塔斯洞窟]
</p>
<p>
欢迎访问埃塔斯洞窟.

位于罗敦镇北边的无名洞窟 - 埃塔斯是覆盖着冰和雪的地区. 

无名洞窟 - 埃塔斯 </p><p color=ffff9999>区分与外部和内部</p><p>, 外部出没企鹅，内部出没水蜥蜴, 普特, 恶魔史雷得等不常见的怪物 . 

这地区的 </p><p color=ffff9999>大部分怪物体力较少，但攻击力强 </p><p>. 狩猎场难度是 </p><p color=ffff9999>40级后期</p><p>玩家适合狩猎. 

</p>
<p color=ffffff66>- 怪物情报(外部) -
</p>
<p color=ff99ffcc>* 肥企鹅: G等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 企鹅: G等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 白熊: I等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 雪地兽人: J等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 帝企鹅: K等级, 主动
</p>
<p color=ff99ffcc>* 雪地兽人弓箭手: K等级, 主动
</p>
<p color=ffffff66>- 怪物情报(内部) -
</p>
<p color=ff99ffcc>* 水蜥蜴: J等级, 主动, 探测透明
</p>
<p color=ff99ffcc>* 普特: K等级, 主动, 变身时被动
</p>
<p color=ff99ffcc>* 恶魔史雷得: L等级, 主动, 探测透明
</p>
<p color=ff99ffcc>* 雪地兽人队长: N等级, 主动, 探测透明
</p>
<p color=ff99ffcc>* 阿特拉茜娅: BOSS, 主动, 探测透明
</p>
</text>
</GUIText>', '2011-07-14 19:33:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:37:00.000', 56, '古代祭坛', '<GUIText ver=2> <text no=0> <p color=ffff6600>[古代祭坛] </p> <p> 古代祭坛是罗敦领地最初的副本 </p><p color=ffff9999>1~3层</p><p>的结构. 其他领地的副本可通过副本入口直接进入, </p><p color=ffff9999>因古代祭坛位于罗敦城内部，通过罗敦城传送门进入 </p><p>比各领地的副本难度较高，都是主动怪物. </p> <p color=ffffff66>- 怪物情报(1层) - </p> <p color=ff99ffcc>* 匹埃罗: J等级, 主动 </p> <p color=ff99ffcc>* 牵线木偶: J等级, 主动 </p> <p color=ff99ffcc>* 水晶傀儡: L等级, 主动 </p> <p color=ff99ffcc>* 混沌的独眼兽: M等级, 主动 </p> <p color=ff99ffcc>* 莱本: M等级, 主动, 探测透明 </p> <p color=ffffff66>- 怪物情报(2层) - </p> <p color=ff99ffcc>* 光明之葬: K等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 再生的独眼兽: L等级, 主动 </p> <p color=ff99ffcc>* 半兽人: M等级, 主动, 变身时被动 </p> <p color=ff99ffcc>* 龟龙: M等级, 变身时被动</p> <p color=ffffff66>- 怪物情报(3层) - </p> <p color=ff99ffcc>* 黑暗之葬: L等级, 主动, 变身时被动, 探测透明 </p> <p color=ff99ffcc>* 水晶傀儡战士: L等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 破坏之独眼兽: M等级, 主动 </p> <p color=ff99ffcc>* 丑陋的侏儒: N等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 半兽人刽子手: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 奥克斯: BOSS, 主动, 探测透明 </p> </text> </GUIText>', '2011-07-14 19:50:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:34:00.000', 57, '罗敦城', '<GUIText ver=2> <text no=0> <p color=ffff6600>[罗敦城] </p> <p> 罗敦城是属于罗敦领地，石墨岛的第四个 </p><p color=ffff9999>工程地区</p><p>通过罗敦城传送门可进入，但城门较窄人数多的话很难同时进入. 相反守城的地区不守城门的话，很容易被侵略. 攻城的时候请充分适当利用传送门周边平地. </p> </text> </GUIText>', '2011-07-14 19:34:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 19:53:00.000', 58, '华利弗地下城', '<GUIText ver=2> <text no=0> <p color=ffff6600>[华利弗地下城] </p> <p> 祝贺您终于提升了40多级. 属于复仇之地的华利弗地下城是为提升等级而被搭建的. </p><p color=ffff9999>40~ 59级玩家可进入的华利弗地下城是以提升等级的目的设计的. 狩猎时获得的经验值很多，但几乎没有掉落的物品. </p><p>执行连接副本路口交叉路NPC任务时，有可能低价购买药水. 请务必要执行能获得古堡的勋章的任务. </p> <p color=ffffff66>- 怪物情报 - </p> <p color=ff99ffcc>* 长螯蟹: J等级, 被动 </p> <p color=ff99ffcc>* 查克拉之石: K等级, 被动 </p> <p color=ff99ffcc>* 浩努克: K等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 阿克拉亡灵: K等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 保卫者: L等级, 主动 </p> <p color=ff99ffcc>* 守卫首领: M等级, 主动, 探测透明 </p> <p>游戏愉快. </p> </text> </GUIText>', '2011-07-14 19:53:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 20:02:00.000', 59, '米泰欧斯洞窟', '<GUIText ver=2> <text no=0> <p color=ffff6600>[米泰欧斯洞窟] </p> <p> 欢迎访问米泰欧斯洞窟. 米泰欧斯洞窟是曾经败给塔莉埃尔的米泰欧斯领自己的军团驻扎的地方.各处都有强悍的怪物. 米泰欧斯洞窟 </p><p color=ffff9999>分为外部，内部，封印之塔</p><p>怪物难度是从外部 → 内部 → 封印之塔顺序越来越强 . </p><p color=ffff9999>推荐60级以上，拥有充分装备的玩家</p><p>执行技能点任务时，必须要访问的地方. 和同僚们一起组队战斗会轻松点. </p><p color=ffffff66>/搜索删除 </p><p>用以上命令. </p> <p color=ffffff66>- 怪物情报(外部) - </p> <p color=ff99ffcc>* 卢比伽幼虫: M等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 屠杀者: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 小翼龙: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 喝格奴役: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 沼泽蜥蜴王: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 卢比伽: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 米泰欧斯的侦察兵: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 米泰欧斯的暗影: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 奎特普特: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 双足翼龙拉伊德: 活动, 主动, 探测透明</p> <p color=ffffff66>- 怪物情报(内部) - </p> <p color=ff99ffcc>* 米泰欧斯的诅咒师: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 米泰欧斯的侦察兵: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 米泰欧斯的弓箭手: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 米泰欧斯的暗影: O等级, 主动, 变身时被动 </p> <p color=ff99ffcc>* 米泰欧斯的狂战士: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 米泰欧斯的破坏者: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 米泰欧斯的守门人: BOSS, 被动 </p> <p color=ffffff66>- 怪物情报(封印之塔) - </p> <p color=ff99ffcc>* 伪装的飞蛾监视兵: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 血之咒术师: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 火焰术士: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 火焰审问官: P等级, 主动, 变身时被动 </p> <p color=ff99ffcc>* 黄铜石看门人: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 四眼监视者: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 狩猎者: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 猛毒莫斯: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 烈火统领: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 尖角监视者: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 水龙看门人: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 深海青龙: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 大地君主: BOSS, 主动, 探测透明 </p> <p color=ff99ffcc>* 大海征服者: BOSS, 主动, 探测透明 </p> <p color=ff99ffcc>* 猛毒君主: BOSS, 主动, 探测透明 </p> <p color=ff99ffcc>* 烈焰暴君: BOSS, 主动, 探测透明 </p> </text> </GUIText>', '2011-07-14 20:03:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-04 12:39:00.000', 60, '海盗洞窟', '<GUIText ver=2> <text no=0> <p color=ffff6600>[海盗洞窟] </p> <p> 欢迎访问海盗洞窟. 可通过罗敦领地境界的被封印的洞窟NPC ‘洛利肯’进入 </p><p color=ffff9999>60级以上玩家可进入的高级别副本</p><p>这洞窟内部有藏有宝物的房间, 该房间钥匙可通过海盗洞窟2层的怪物获得. </p> <p color=ffffff66>- 进入秘密房间道具 - </p> <m width=36 height=36 value=10003468></m> <p>&nl; 洛利肯的戒指: 洛利肯的房 &nl;&nl;</p> <m width=36 height=36 value=10003469></m> <p>&nl; 卡斯帕的戒指: 卡斯帕的房 &nl;&nl;</p> <m width=36 height=36 value=10003470></m> <p>&nl; 布莱尔的项链: 布莱尔的房 &nl;&nl;</p> <m width=36 height=36 value=10003471></m> <p>&nl; 奴隶监视者的鞭子: 奴隶的房 &nl;&nl;</p> <p> 洛利肯/卡斯帕/布莱尔/奴隶的房各个都存在海盗箱子, ‘海盗箱子的钥匙’可通过海盗洞窟2层怪物掉落. </p> <m width=36 height=36 value=10003472></m> <p>&nl;海盗箱子的钥匙 &nl;&nl; &nl;</p> <p color=ffffff66>- 怪物情报(1层) - </p> <p color=ff99ffcc>* 蝲蛄: P等级, 被动 </p> <p color=ff99ffcc>* 蜈蚣王: P等级 被动 </p> <p color=ff99ffcc>* 深海鱼: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 斯尼米尔: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 盗贼总兵: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 海盗: Q等级, 主动 </p> <p color=ff99ffcc>* 女海盗: Q等级, 主动 </p> <p color=ff99ffcc>* 恶魔奴隶: Q等级, 主动, 探测透明 </p> <p color=ffffff66>- 怪物情报(2层) - </p> <p color=ff99ffcc>* 盗贼总兵: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 海盗诅咒师: Q等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 海盗兵: Q等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 恶魔监视者, Q等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 诱饵奴隶: Q等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 卡斯帕: R等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 海尔梅拉: R等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 布莱尔: R等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 宝物守护者, R等级, 主动, 探测透明 </p> </text> </GUIText>', '2011-07-14 20:08:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-14 20:15:00.000', 61, '荣光圣殿', '<GUIText ver=2> <text no=0> <p color=ffff6600>[荣光圣殿] </p> <p> 欢迎访问荣光圣殿. </p><p color=ffff9999>可通过古代祭坛3层NPC [汉德里温拉佩特]移动，这NPC是消灭完 ‘奥克斯’一分钟后出现. 荣光圣殿是关于奥克斯的出现来决定开放与否的时段形副本，奥克斯再次出现的情况下荣光圣殿里的全部玩家都会被归还到罗敦镇. </p><p>荣光圣殿全部地区不可乘坐德拉克, </p><p color=ffff9999>只能乘坐德拉克</p><p>所以，为了快速移动需要获得封印的奇美拉卷轴. 以外也能获得武器，首饰等高级道具. </p> <p color=ffffff66>- 制作道具情报 - </p> <p> 1.把以下三个道具登陆到制作窗(J)后点击 [制作] , 可获得大马士革长斧道具. </p> <m width=36 height=36 value=10001627></m> <p color=ffffff66>&nl; 大马士革长斧 &nl;&nl;</p> <m width=36 height=36 value=10006185></m> <p>&nl; 古代大马士革钢铁 &nl;&nl;</p> <m width=36 height=36 value=10006186></m> <p>&nl; 长斧的斧把 &nl;&nl;</p> <m width=36 height=36 value=10006187></m> <p>&nl; 大马士革钢铁合成剂 &nl;&nl;&nl;</p> <p> 2. 把一下道具登录到制作窗(J)后点击 [制作]，可获得?灵魂锁之弓. </p> <m width=36 height=36 value=10005653></m> <p color=ffffff66>&nl; ?灵魂锁之弓 &nl;&nl;</p> <m width=36 height=36 value=10006188></m> <p>&nl; 坚固的把手 &nl;&nl;</p> <m width=36 height=36 value=10006189></m> <p>&nl; 绷紧的弓弦 &nl;&nl;</p> <m width=36 height=36 value=10006190></m> <p>&nl; 星陨石弓身 &nl;&nl;&nl;</p> <p> 3.把一下三个道具登录到制作窗(J)后点击[制作]，可获得肯布吉特之弓. </p> <m width=36 height=36 value=10005667></m> <p color=ffffff66>&nl; 嗜血肯布吉特之弓&nl;&nl;</p> <m width=36 height=36 value=10006188></m> <p>&nl; 坚固的把手 &nl;&nl;</p> <m width=36 height=36 value=10006189></m> <p>&nl; 绷紧的弓弦  &nl;&nl;</p> <m width=36 height=36 value=10006191></m> <p>&nl; 血淋淋的弓身 &nl;&nl;&nl;</p> <p color=ffffff66>- 怪物情报 - </p> <p color=ff99ffcc>* 半人鱼 卢萨勒: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 半人鱼 提鲁斯: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 变质的精灵: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 变异的奎特普特: O等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 堕落的双足翼龙: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 嗜血天蝎: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 红色尖刺 潘瑟: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 变质的格雷芬特: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 雄性剑龙: P等级 主动, 探测透明 </p> <p color=ff99ffcc>* 德拉古兰 火焰术士: P等级, 主动 探测透明 </p> <p color=ff99ffcc>* 德拉古兰 军团兵: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 德拉古兰 岩石看门人: P等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 黄泉咒术师: Q等级, 主动, 探测透明 </p> <p color=ff99ffcc>* 黄泉骑士: Q等级, 主动, 探测透明</p> <p color=ff99ffcc>* 黄泉弓箭手: Q等级, 主动, 探测透明 </p> </text> </GUIText>', '2011-07-14 20:55:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 11:12:00.000', 62, '攻城/营地系统', '<GUIText ver=2> <text no=0> <p color=ffff6600>[攻城/营地系统] </p> <p> 您好，开始介绍 R2 Online大规模PvP 战斗 攻城/营地系统. </p><p color=ffffff66>1. 参加条件 : </p><p>只要加入了工会，就都能参加营地攻城战. </p><p color=ffffff66>2. 时间 : </p><p>每周星期日20~22点 有营地攻城战. </p><p color=ffffff66>3. 规则</p><p> 1) 攻城战结束的时候占有守护石的工会赢得该城. 2) 攻城战开始的时候最终占领各城的守护塔的工会赢得该城. 3) 破坏守护塔后10分钟会成为标记状态，10分钟以后才正式认证为占领状态. 4) 标记状态下守护塔再次被破坏时所有权会变更，重新开始标记状态. 5) 标记状态下到攻城战结束点时，将决定为该领地没有拥有者. </p><p color=ffff9999>* 5)一样把没有拥有者状态的营地/城称为废弃状态，可在世界地图确认. </p><p> 营地战规则也和攻城战一样. </p> </text> </GUIText>', '2011-07-29 14:50:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 11:16:00.000', 63, '攻城/营地战斗', '<GUIText ver=2> <text no=0> <p color=ffff6600>[攻城/营地战斗] </p> <p> 今天进行攻城/营地战斗. 请体验R2特有的攻城战斗. </p><p color=ffff9999> - 以城为中心的小营地!! - 占领时获得的特殊技能列表!! - 关于攻城/营地战同时爆发防御/攻击组织化 </p><p> 相进一步了解现在游戏中的服务器攻城/营地占领状况，就请登陆官方网站-在R2，战争记录里确认. </p> </text> </GUIText>', '2011-07-15 11:16:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 11:17:00.000', 64, '公会技能树', '<GUIText ver=2> <text no=0> <p color=ffff6600>[公会技能列表] </p> <p> 现在开始介绍公会技能列表. 基本的理路相似于角色技能列表, 但一名工会成员只能开发一个技能?. </p><p color=ffffff66>*快捷键: Ctrl+K </p> <p color=ffff9999>1. 公会技能列表分成''公会通用技能列表'', ''公会营地城池技能列表'', ''公会特殊技能列表'' .</p> <p> ① 公会通用技能列表是全部工会基本都能开发的技能列表. ② 公会营地城池技能列表是占领营地的工会可开发的技能列表. ③ 公会特殊技能列表能追加强化现开发完的技能列表, 都是期间制技能. </p> <p color=ffff9999>2. 要开发公会技能需要技能点. </p> <p> ① 可用公会技能点开发工会技能. 特殊列表的情况下消耗特定道具. ② 学会公会技能点的条件是. - 招募工会成员: 每一名工会成员提供1点. - 占领营地: 每次占领营地提供5点. 最大能应用2个营地. - 占领城: 每次占领城池提供20点?.不可重复应用. ③ 按有成员退出工会或者攻城战结果来减少技能点时，开发的技能可能会破坏. </p> <p color=ffff9999>3. 要强化开发的技能，需要获得经验值.</p> <p> ① 要强化更高级别技能，需要技能开发者获得经验值. ② 经验值可获得狩猎添加，也可通过公会荣誉勋章(工会)获得. </p> <p color=ffff9999>4. 一名工会成员可开发一个工会技能.</p> <p> ① 没有开发过的工会成员才能开发公会技能. ② 开发工会技能的成员退出工会时，开发过的技能会被破坏. - 接着以被破坏技能条件的其他技能都不可激活. - 不可激活的技能没有任何的效果，条天充足时重新激活. - 没点工会技能的成员退出公会时，也有可能因技能点不足而破坏?. </p> <p color=ffff9999>5. 可以初始化公会技能列表.</p> <p> ① 使用遗忘之粉（工会）来初始化公会技能列表. ② 公会会长才能初始化公会技能列表. ③ 在艾斯本镇的[技能列表介绍员]艾莉莎处可购买遗忘之粉（工会）. ④ 初始化时全部技能初始化到开发以前的状态, 技能里使用过的点数会全部返还. </p> </text> </GUIText>', '2011-07-15 11:27:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 11:29:00.000', 65, '跨服团队战', '<GUIText ver=2> <text no=0> <p color=ffff6600>[跨服团队战] </p> <p> 恭喜您达成15级. 现在可以参加跨服团队战. </p><p color=ffff9999>团队战是分开两队进行 PvP 的小规模战斗项目，一个队伍最多可参加50名玩家</p><p>进行时间是</p><p color=ffff9999>0点,2点,4点,6点,8点,10点,12点,14点,16点,18点,20点,22点 每2小时进行</p><p>进行5分钟前可通过团队战NPC申请, 不参加的玩家可以通过观战模式来观战. 按申请人员数，竞技场数也会不同, 随机决定竞技场. 胜利的队伍会获得‘荣誉币’确保一定数量荣誉币后可通过 NPC交换奖励道具. </p> <m width=36 height=36 value=10001046></m> <p color=ffffff66>&nl; 荣誉币&nl;&nl; &nl;</p> <p color=ffffff66>- 荣誉币获得条件 - </p> <p> 1. 队伍胜利时获得1个 2. 击杀数最多时获得1个 3. 最多助攻获得1个 4. 最高分数获得1个. 做多击杀数， 最多助攻, 最高分数玩家是队伍胜利时能获得2个荣誉币. 详细内容请通过团队战NPC. 请您游戏愉快. </p> </text> </GUIText>', '2011-07-15 11:48:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 11:54:00.000', 66, '团队战场奖励', '<GUIText ver=2> <text no=0> <p color=ffff6600>[团队战奖励] </p> <p> 用一定数量‘荣誉币’可获团队战奖励, 通过NPC来交换想要的 ‘角斗士宝箱’. </p><p color=ffffff66>- 宝箱种类 - </p> <m width=36 height=36 value=10005891></m> <p>&nl; 角斗士宝箱(武器)&nl;&nl;</p> <m width=36 height=36 value=10005893></m> <p>&nl; 老练的角斗士宝箱（武器）&nl;&nl;</p> <m width=36 height=36 value=10005892></m> <p>&nl; 角斗士宝箱(防具)&nl;&nl;</p> <m width=36 height=36 value=10005894></m> <p>&nl; 老练的角斗士宝箱（防具）&nl;&nl; &nl;</p> <p>各宝箱里获得的道具只能通过团队战获得的职业别武器/防具道具，  +0~+9为止可用一般武器强化卷轴来强化, </p><p color=ffff9999>+10 以上开始用 ‘荣誉卷轴’强化. </p> <p color=ffffff66>- 荣誉卷轴种类 - </p> <m width=36 height=36 value=10005240></m> <p>&nl; 荣誉的武器强化卷轴&nl;&nl;</p> <m width=36 height=36 value=10005241></m> <p>&nl; 荣誉的防具强化卷轴&nl;&nl; &nl;</p> <p color=ffff9999>荣誉卷轴可在各镇的古董商NPC交换</p><p>访问 NPC时可获得更详细的情报. </p> </text> </GUIText>', '2011-07-15 11:55:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 11:56:00.000', 67, '拍卖行', '<GUIText ver=2> <text no=0> <p color=ffff6600>[拍卖行] </p> <p> 拍卖行与个人商店和交换不同，不在线也可交易的系统. 拍卖行使用方法如下. - 点击拍卖行NPC后选择[开启拍卖行]. -在 [拍卖行目录]可购买其他玩家登陆的道具. - 想在拍卖行登陆道具时，选择[拍卖行登陆] 后登陆道具. - 登陆道具到拍卖行时支付一定金额的手续费，可登录24小时. - 登陆到的道具出售时，可提取除去手续费5%的金额. - 交易没达成时，可回收登陆的道具但手续费不可回收. 基本在拍卖行可登陆的道具数是最多三个，使用高级委托券时最多可登陆8个道具. </p> <m width=36 height=36 value=10006230></m> <p>&nl; 高级委托券&nl;&nl; &nl;</p> </text> </GUIText>', '2011-07-15 11:56:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 11:58:00.000', 68, '经验之咒', '<GUIText ver=2> <text no=0> <p color=ffff6600>[经验之咒] </p> <p> 使用了经验之咒. </p> <m width=36 height=36 value=10002836></m> <p>&nl; 经验之咒&nl;&nl; &nl;</p> <p>经验之咒是 </p><p color=ffff9999>追加获得在限定期间狩猎获得的经验值</p><p>按经验之咒获得不同量的经验值. 使用中的道具期限和种类点击</p><p color=ffffff66>快捷键 T</p><p>确认. </p> </text> </GUIText>', '2011-07-15 11:59:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 12:03:00.000', 69, '德拉克卡片', '<GUIText ver=2> <text no=0> <p color=ffff6600>[德拉克卡片] </p> <p> 使用了德拉克卡片. </p> <m width=36 height=36 value=10001938></m> <p>&nl; 德拉克卡片&nl;&nl; &nl;</p> <p>可通过</p><p color=ffff9999><幸运术士>凯根NPC交换德拉克戒指</p><p> </p><m width=36 height=36 value=10001941></m> <p>&nl; 战斗德拉克&nl;&nl; &nl;</p> <p color=ffff9999>交换时戒指随机获得使用期限10日, 14日, 21日, 30日 </p><p>按期限道具特征</p><p color=ffff9999>获得时开始倒计时</p><p>请参考. </p> </text> </GUIText>', '2011-07-15 12:04:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 12:09:00.000', 70, '期限延长道具', '<GUIText ver=2> <text no=0> <p color=ffff6600>[期限延长道具] </p> <p color=ffff9999>通过使用"黄金锤子" 或 "银色锤子" 来延长剩下的期限</p><p></p><p color=ffff9999>银色锤子道具可在杂货商人NPC处用银币购买</p><p></p><p color=ffff9999>黄金锤子道具可用混沌战场点数购买 </p><p>. 按锤子的种类不同，延长次数和期限也不同. </p> <m width=36 height=36 value=10004891></m> <p>&nl; 银色锤子 - 1日 (延长1~3回)&nl;&nl;</p> <m width=36 height=36 value=10004894></m> <p>&nl; 黄金锤子 - 3/5/7日 (延长1~10回)&nl;&nl; &nl;</p> <p>锤子类道具 </p><p color=ffffff66>只能用在使用期限1~24小时未满时的德拉克戒指和变身项链里使用</p><p> </p><p color=ffffff66>使用时被绑定，无法交易</p><p>请参考. </p> </text> </GUIText>', '2011-07-15 12:09:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 12:13:00.000', 71, '祝福的强化精华碎片', '<GUIText ver=2> <text no=0> <p color=ffff6600>[祝福的强化精华碎片] </p> <p> 学会了祝福的强化精华碎片. </p> <m width=36 height=36 value=10005608></m> <p>&nl; 祝福的强化精华碎片&nl;&nl;&nl;</p> <p>祝福的强化精华碎片 </p><p color=ffff9999>通过制作可变更为祝福的强化精华，需要3个碎片</p><p>先通过快捷键 ''J'' 激活制作窗，双击背包里的三个祝福的强化精华碎片后点击制作就能获得祝福的强化精华. 没有失败率，必须要用3个祝福的强化精华碎片. </p> </text> </GUIText>', '2011-07-15 12:13:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 12:30:00.000', 72, '特殊精华强化之咒', '<GUIText ver=2> <text no=0> <p color=ffff6600>[特殊精华强化之咒] </p> <p> 特殊精华强化之咒是特殊精华强化时需要的道具. </p> <m width=36 height=36 value=10005573></m> <p>&nl; 特殊精华强化之咒&nl;&nl;&nl;</p> <p>特殊精华强化之咒 </p><p color=ffff9999>有一定几率成功</p><p>失败时, 特殊精华有可能会消失. </p><p color=ffffff66>- 强化方法 - </p><p>和一般武器/防具强化方法一样. 双击强化之咒后点击要强化的道具. </p> <p color=ffffff66>- 可以强化的特殊精华 - </p> <m width=36 height=36 value=10040600></m> <p>&nl; 幸运精华 Lv1 ~ Lv2&nl;&nl;</p> <m width=36 height=36 value=10040603></m> <p>&nl; 变身强化精华 Lv1 ~ Lv2&nl;&nl;</p> <m width=36 height=36 value=10040606></m> <p>&nl; 英雄力量精华 Lv1 ~ Lv2&nl;&nl;</p> <m width=36 height=36 value=10040607></m> <p>&nl; 猎人敏捷精华 Lv1 ~ Lv2&nl;&nl;</p> <m width=36 height=36 value=10040608></m> <p>&nl; 贤者智慧精华 Lv1 ~ Lv2&nl;&nl;</p> </text> </GUIText>', '2011-07-15 12:34:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 12:39:00.000', 73, '特殊精华转换之咒', '<GUIText ver=2> <text no=0> <p color=ffff6600>[特殊精华转换之咒] </p> <p> 特殊精华转换之咒 </p><p color=ffff9999>变换持有的属性精华种类时需要的道具</p><p></p><p color=ffffff66>- 变换方法- </p><p>确保特殊精华转换之咒和特殊精华道具时通过NPC "<活动精华>赫伊特"交换. </p> <p color=ffffff66>- 可以变换的属性精华 - </p> <m width=36 height=36 value=10040606></m> <p>&nl; 英雄力量精华 Lv1 ~ Lv3&nl;&nl;</p> <m width=36 height=36 value=10040607></m> <p>&nl; 猎人敏捷精华 Lv1 ~ Lv3&nl;&nl;</p> <m width=36 height=36 value=10040608></m> <p>&nl; 贤者智慧精华 Lv1 ~ Lv3&nl;&nl;</p> <p>* 例) 英雄力量精华 Lv2 → 变换猎人敏捷精华 Lv2 </p> </text> </GUIText>', '2011-07-15 12:41:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-15 12:43:00.000', 74, '表情符号', '<GUIText ver=2> <text no=0> <p color=ffff6600>[表情符号] </p> <p> 不在聊天窗输入，用可爱的表情符号也可以简单表达自己的想法. 通过快捷键发表情符号, 表情符号快捷键如下. </p> <p color=ff99ffcc>数字1 - 打招呼 数字2 - 感谢 数字3 - 道歉 数字4 - 帮助 数字5 - 愤怒 数字6 - 悲伤 数字7 - 哀求 数字8 - 祝贺 数字9 - 慌张 数字0 - 否定 </p> <p color=ffff9999>使用表情符号时，角色头脑上边出现标志，出现效果因和系统文字</p><p>. 表情符号 </p><p color=ffff9999>每5秒施行一次</p><p>正在施行表情符号的话，5秒后可再次施行.</p> </text> </GUIText>', '2011-07-15 12:43:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-18 10:54:00.000', 75, '变身技能书', '<GUIText ver=2> <text no=0> <p color=ffff6600>[变身技能书] </p> <p> 以任务奖励获得的变身技能书与变身项链和怪物变身不同，是新的变身类型. 使用完成任务后获得的</p><p color=ffff9999>变身技能书，学习技能后</p><p>首先 </p><p color=ffff9999>使用变身技能再装备变身项链</p><p>的话变身为学习技能的怪物. 按和变身项链同样装备的角色级别设定，按任务类型奖励的道具也有很多种. </p> </text> </GUIText>', '2011-07-18 10:54:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-18 10:55:00.000', 76, '休眠经验恢复剂', '<GUIText ver=2> <text no=0> <p color=ffff6600>[休眠经验恢复剂] </p> <p> 获得了休眠经验恢复剂. </p> <m width=36 height=36 value=10005898></m> <p>&nl; 休眠经验恢复剂&nl;&nl; &nl;</p> <p>使用该道具之前先介绍休眠经验值. 首先, 通过位于下段的UI可确认休眠经验. 边缘的经验条表示公会技能和公会议事厅里获得的公会议事厅休眠经验值。圆形经验条表示休眠经验值</p><p color=ffff9999>圆形休眠经验值是退出系统后开始计算，一开始是堆积灰色-非激活状态，使用休眠经验恢复剂时变色-处于激活状态. </p><p>这状态上应用休眠经验. 如果 </p><p color=ffff9999>两种休眠经验都被激活的话，先消耗通过休眠经验恢复剂激活的休眠经验</p><p>按狩猎时获得的经验值消耗休眠经验. </p> </text> </GUIText>', '2011-07-18 10:55:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-18 10:57:00.000', 77, '负重与饱食度', '<GUIText ver=2> <text no=0> <p color=ffff6600>[负重与饱食度] </p> <p> 您好, R2 Online 这次给您介绍负重与饱食度. </p> <p color=ffffff66>- ?? - </p> <p>负重是 </p><p color=ffff9999>角色最多能携带的数值</p><p>负重以在游戏画面HP上面的数字显示, 超过一定数值后会受到惩罚. </p><p color=ffff9999> - 70% 以上: 以淡红色显示, HP/MP 不可自然回复 - 100% 以上: 以红色显示, HP/MP 不可自然回复, 不能获得道具(不包括银币), 不能使用攻击和魔法</p> <p color=ffffff66>- 饱食度 - </p> <p>饱食度是</p><p color=ffff9999>显示角色饥饿的数值</p><p>按数值的状态 </p><p color=ffff9999>应用攻击力增加/减少</p><p>把鼠标挪到又上段肉图片上边时，以数字显示. </p><p color=ffff9999>吃(面包, 肉类)食物可充满饱食度. </p> <p color=ff99ffcc> - 饱食度 0: 攻击力 -1 - 饱食度 1~69: 一般状态(数值渐渐减少) - 饱食度 70~100: 攻击力 +1(数值减少较快) </p> <p>角色死亡时饱食度下降到0.充满饱食度的食品-面包</p><p color=ffff9999>可在杂货商NPC处购买</p><p>请参考. </p> </text> </GUIText>', '2011-07-18 10:57:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-18 11:03:00.000', 78, '精华系统', '<GUIText ver=2> <text no=0> <p color=ffff6600>[精华系统] </p> <p> 祝贺您获得了精华. </p><p color=ffff9999>精华是凝缩MP的道具，给角色能力值较大的影响. </p> <p color=ffff9999>分生命，灵魂，熟练，守护，破坏，特殊精华</p><p>??, 都持有不同的效果. </p><p color=ffff9999>精华等级分为一般, 稀有, 史诗, 传说.按等级所应用的能力值也不同</p><p>精华是 </p><p color=ffff9999>可以进化/强化道具，按等级和强化数值所应用的能力值不同 </p><p>现在开始介绍各精华的效果种类. </p> <m width=36 height=36 value=10040119></m> <p>&nl; 生命精华: HP增加, HP回复, 药水回复 &nl;&nl;</p> <m width=36 height=36 value=10040239></m> <p>&nl; 灵魂精华: MP增加, MP增加, 减少使用技能时消耗MP&nl;&nl;</p> <m width=36 height=36 value=10040599></m> <p>&nl; 熟练精华: 能力值增加, 负重增加, 暴击追加伤害&nl;&nl;</p> <m width=36 height=36 value=10040479></m> <p>&nl; 守护精华: 防御力增加, 减少暴击伤害, 减少暴击几率&nl;&nl;</p> <m width=36 height=36 value=10040359></m> <p>&nl; 破坏精华: 攻击力增加, 命中率增加, 暴击几率增加&nl;&nl; &nl;</p> <p> 从背包(快捷键: Tab, I)移动到栏里后双击装备道具. 收集4个同样种类, 同样级别的精华后可进化到下一个级别. 装备同样强化, 同样级别,同样等级精华时发动经验值增加套装效果. </p> </text> </GUIText>', '2011-07-18 11:05:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-18 11:07:00.000', 79, '道具确认', '<GUIText ver=2> <text no=0> <p color=ffff6600>[道具确认] </p> <p> 获得了道具确认药水. </p> <m width=36 height=36 value=10000355></m> <p>&nl; 道具确认药水&nl;&nl; &nl;</p> <p>怪物掉落的道具大部分都是无法确认状态. 为了交易道具，需要用 ‘道具确认药水’把道具变更为确认状态. 使用方法是 双击‘确认魔法药水’后再点击要确认的道具. 为正确获取道具情报，道具确认是非常重要的. 道具一般分为 </p><p color=ffff9999>永久诅咒（红色）, </p><p color=ffff9999>诅咒（红色）, </p><p>一般（白色）, </p><p color=ffffff66>祝福（黄色） </p><p> - 装备被诅咒道具时将无法卸下. 使用 ‘解咒药水’，可以解除被诅咒状态. - 如果用诅咒的武器强化卷轴强化时，道具破坏率提高. </p> <m width=36 height=36 value=10000356></m> <p>&nl; 解咒药水&nl;&nl; &nl;</p> <p>道具确认药水和解咒药水可通过狩猎获得，或通过杂货商NPC购买. </p> </text> </GUIText>', '2011-07-18 11:10:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2011-07-18 11:11:00.000', 80, '黄金宝箱和钥匙', '<GUIText ver=2> <text no=0> <p color=ffff6600>[黄金宝箱和钥匙] </p> <p> 获得了‘黄金宝箱’. 黄金宝箱就像一种彩票. 要确认奖励内容，需要 ‘黄金宝箱钥匙’.拿这两个道具去各镇的 </p><p color=ffff9999><黄金宝箱>欧谱诺NPC处</p><p>可随机获得一个奖励内容. </p> <m width=36 height=36 value=10001622></m> <p>&nl; 黄金宝箱&nl;&nl;</p> <m width=36 height=36 value=10001621></m> 黄金宝箱和钥匙 <p>&nl; 黄金宝箱钥匙&nl;&nl;&nl;</p> <p>从黄金宝箱可获得的物品多种多样-强化卷轴, 宝石, 技能书, 武器, 防具, 材料等可随机获得.确认箱子前要慎重. 祝您好运. </p> </text> </GUIText>', '2011-07-18 11:12:00.000');
GO

INSERT INTO [dbo].[TblPopupGuideDialog] ([mRegDate], [mGuideNo], [mSubject], [mDialog], [mUptDate]) VALUES ('2012-10-31 17:10:00.000', 124, '                              ', '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', '2012-10-31 17:10:00.000');
GO

