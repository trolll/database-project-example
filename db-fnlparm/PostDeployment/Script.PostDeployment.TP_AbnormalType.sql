/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_AbnormalType
Date                  : 2023-10-07 09:08:46
*/


INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (1, '混乱', 1021, 0, 'Abnormal00.dds', 32, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (2, '麻痹', 1038, 0, 'Abnormal00.dds', 64, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (3, '中毒', 1037, 0, 'Abnormal00.dds', 96, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (4, '疾病', 1036, 0, 'Abnormal00.dds', 128, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (5, '流血', 1002, 0, 'Abnormal00.dds', 160, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (6, '销毁 药水/卷轴', 1023, 0, 'Abnormal00.dds', 192, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (7, '诅咒', 1034, 0, 'Abnormal00.dds', 224, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (8, '粘着', 1018, 0, 'Abnormal00.dds', 256, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (9, '龙之恐惧', 0, 0, 'Abnormal00.dds', 288, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (10, '治疗', 1040, 1, 'Abnormal00.dds', 320, 0, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (11, '恢复饱腹度', 1104, 1, 'Abnormal00.dds', 352, 0, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (12, '攻击速度上升', 1042, 1, 'Abnormal00.dds', 384, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (13, '移动速度加快', 1041, 1, 'Abnormal00.dds', 416, 0, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (14, '解毒药水', 1045, 1, 'Abnormal00.dds', 448, 0, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (15, '恐惧', 1025, 0, 'Abnormal00.dds', 480, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (16, '沉默', 1032, 0, 'Abnormal00.dds', 0, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (17, '提升力量', 1095, 1, 'Abnormal00.dds', 32, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (18, '变身中', 1847, 1, 'Abnormal00.dds', 64, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (19, '怪物召唤', 1049, 0, 'Abnormal00.dds', 96, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (20, '强化火焰抗性', 0, 1, 'Abnormal00.dds', 128, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (21, '道具祝福变换', 0, 0, 'Abnormal00.dds', 160, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (22, '复活', 1047, 1, 'Abnormal00.dds', 192, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (23, '强化冰霜抗性', 0, 1, 'Abnormal00.dds', 224, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (24, '敏捷上升', 1244, 1, 'Abnormal00.dds', 256, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (25, '智力上升', 1243, 1, 'Abnormal00.dds', 288, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (26, '魔法盾', 1060, 1, 'Abnormal00.dds', 320, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (27, '石化皮肤', 1076, 1, 'Abnormal00.dds', 352, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (28, '强化酸性抗性', 0, 1, 'Abnormal00.dds', 384, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (29, '强化雷击抗性', 0, 1, 'Abnormal00.dds', 416, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (30, '祝福光环', 1046, 1, 'Abnormal00.dds', 448, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (31, '心灵传送术', 1048, 0, 'Abnormal00.dds', 480, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (32, '鉴定', 0, 0, 'Abnormal00.dds', 0, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (33, '武器强化', 0, 1, 'Abnormal00.dds', 32, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (34, '防具强化', 0, 1, 'Abnormal00.dds', 64, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (35, '回城', 1048, 1, 'Abnormal00.dds', 96, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (36, '解咒', 1062, 0, 'Abnormal00.dds', 128, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (37, '侦察', 0, 1, 'Abnormal00.dds', 160, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (38, '隐身状态', 0, 1, 'Abnormal00.dds', 192, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (39, '传送术操纵', 0, 1, 'Abnormal00.dds', 224, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (40, '变身操纵', 0, 1, 'Abnormal00.dds', 256, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (41, '制造食物', 0, 0, 'Abnormal00.dds', 288, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (42, '魔法驱散', 1062, 0, 'Abnormal00.dds', 320, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (43, '攻击力上升', 1044, 1, 'Abnormal00.dds', 352, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (44, '神圣铠甲', 1069, 1, 'Abnormal00.dds', 384, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (45, '元素铠甲', 1067, 1, 'Abnormal00.dds', 416, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (46, '德拉克骑乘中', 1094, 1, 'Abnormal00.dds', 448, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (47, '愤怒', 1039, 1, 'Abnormal00.dds', 480, 64, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (48, '经验值上升', 1043, 1, 'Abnormal00.dds', 0, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (49, '命中率提高', 1046, 1, 'Abnormal00.dds', 32, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (50, '物体变换中', 0, 1, 'Abnormal00.dds', 64, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (51, '烟花', 1097, 0, 'Abnormal00.dds', 96, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (52, '移动速度减慢', 1107, 0, 'Abnormal00.dds', 128, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (53, '攻击力上升(网吧)', 1040, 1, 'Abnormal00.dds', 160, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (54, '防御力上升(网吧)', 1040, 1, 'Abnormal00.dds', 192, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (55, '近战攻击下降', 0, 0, 'Abnormal00.dds', 224, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (56, '远程攻击下降', 0, 0, 'Abnormal00.dds', 256, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (57, '防御力下降', 4207, 0, 'Abnormal00.dds', 288, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (58, '远程攻击命中率增加', 1233, 1, 'Abnormal00.dds', 320, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (59, '近战攻击命中率增加', 1236, 1, 'Abnormal00.dds', 352, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (60, '魔法命中率增加', 1234, 1, 'Abnormal00.dds', 384, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (61, '近战攻击力上升', 1232, 1, 'Abnormal00.dds', 416, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (62, '远程攻击力上升', 1237, 1, 'Abnormal00.dds', 448, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (63, '魔法攻击力上升', 1238, 1, 'Abnormal00.dds', 480, 96, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (64, '生命恢复速度加快(网吧)', 1040, 1, 'Abnormal00.dds', 0, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (65, '法力恢复速度加快', 1242, 1, 'Abnormal00.dds', 32, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (66, '攻击力上升(活动)', 1040, 1, 'Abnormal00.dds', 64, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (67, '防御力增加', 1257, 1, 'Abnormal00.dds', 96, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (68, '重量增加', 1146, 1, 'Abnormal00.dds', 128, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (69, '重击', 0, 0, 'Abnormal00.dds', 160, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (70, '猛力攻击', 0, 1, 'Abnormal00.dds', 192, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (71, '痛苦麻痹', 0, 1, 'Abnormal00.dds', 224, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (72, '暴走', 1130, 1, 'Abnormal00.dds', 256, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (73, '瞄准射击', 1132, 0, 'Abnormal00.dds', 288, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (74, '振荡射击', 1107, 0, 'Abnormal00.dds', 320, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (75, '音速屏障', 0, 1, 'Abnormal00.dds', 352, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (76, '疾速射击', 0, 1, 'Abnormal00.dds', 384, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (77, '生命恢复速度加快', 1250, 1, 'Abnormal00.dds', 416, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (78, 'DV增加(范围)', 0, 1, 'Abnormal00.dds', 448, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (79, 'PV增加(范围)', 0, 0, 'Abnormal00.dds', 480, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (80, '生命恢复量增加(范围)', 0, 1, 'Abnormal00.dds', 0, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (81, '法力恢复量增加(范围)', 0, 1, 'Abnormal00.dds', 32, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (82, '法力增减', 1142, 0, 'Abnormal00.dds', 64, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (83, '凝神术', 0, 1, 'Abnormal00.dds', 96, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (84, '拯救', 1139, 1, 'Abnormal00.dds', 128, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (85, '魔法恢复', 0, 1, 'Abnormal00.dds', 160, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (86, '毒', 1037, 0, 'Abnormal00.dds', 192, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (87, '猛毒', 1804, 0, 'Abnormal00.dds', 224, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (88, '回城(公会议事厅)', 0, 0, 'Abnormal00.dds', 256, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (89, '回城(公会礼堂)', 0, 0, 'Abnormal00.dds', 288, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (90, '活动', 1169, 0, 'Abnormal00.dds', 320, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (91, '超级加速秘药', 1277, 1, 'Abnormal00.dds', 352, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (92, '封印石', 1144, 0, 'Abnormal00.dds', 384, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (93, '疾行', 0, 1, 'Abnormal00.dds', 416, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (94, '隐身术', 0, 1, 'Abnormal00.dds', 448, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (95, '侦察', 1201, 0, 'Abnormal00.dds', 480, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (96, '回避', 0, 1, 'Abnormal00.dds', 0, 192, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (97, '毒牙', 0, 0, 'Abnormal00.dds', 32, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (98, '暗杀', 1182, 1, 'Abnormal00.dds', 64, 192, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (99, '昏厥攻击', 1200, 0, 'Abnormal00.dds', 96, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (100, '无敌', 1206, 0, 'Abnormal00.dds', 128, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (101, '曼陀铃之毒', 1205, 0, 'Abnormal00.dds', 160, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (102, '狼蛛毒', 1107, 0, 'Abnormal00.dds', 192, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (103, '使用毒囊', 1805, 0, 'Abnormal00.dds', 224, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (104, '杀人犯', 1207, 0, 'Abnormal00.dds', 256, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (105, '地震', 0, 0, 'Abnormal00.dds', 288, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (106, '烈焰', 0, 0, 'Abnormal00.dds', 320, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (107, '蓄力攻击', 0, 0, 'Abnormal00.dds', 352, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (108, '增加暴击机率', 1253, 1, 'Abnormal00.dds', 384, 192, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (109, '自杀', 1254, 0, 'Abnormal00.dds', 416, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (110, '初始敌人列表', 0, 0, 'Abnormal00.dds', 448, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (111, '驱逐', 0, 0, 'Abnormal00.dds', 480, 192, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (112, 'spawn spot 传送点', 0, 0, 'Abnormal00.dds', 0, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (113, '检测出非法程序使用者', 1288, 0, 'Abnormal00.dds', 32, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (114, '减少法力消耗量', 0, 0, 'Abnormal00.dds', 64, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (115, '狩猎时获得经验值增加', 0, 0, 'Abnormal00.dds', 96, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (116, '狩猎时掉宝率增加', 0, 0, 'Abnormal00.dds', 128, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (117, '生命绽放', 1366, 0, 'Abnormal00.dds', 160, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (118, '无视护甲', 1369, 0, 'Abnormal00.dds', 192, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (119, '漂浮术', 1373, 1, 'Abnormal00.dds', 224, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (120, '魔法凝聚', 0, 0, 'Abnormal00.dds', 256, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (121, '要害攻击', 0, 1, 'Abnormal00.dds', 288, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (122, '强力攻击', 0, 0, 'Abnormal00.dds', 320, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (123, '置换生命力', 1477, 0, 'Abnormal00.dds', 352, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (124, '魔力强化', 0, 1, 'Abnormal00.dds', 384, 224, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (125, '时光流逝', 0, 1, 'Abnormal00.dds', 416, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (126, '生命之流', 0, 1, 'Abnormal00.dds', 448, 224, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (127, '无法攻击', 0, 0, 'Abnormal00.dds', 480, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (128, '召唤召唤兽', 0, 0, 'Abnormal00.dds', 0, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (129, '召唤兽攻击', 0, 0, 'Abnormal00.dds', 32, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (130, '防御图腾', 1424, 1, 'Abnormal00.dds', 64, 256, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (131, '生命图腾', 1428, 1, 'Abnormal00.dds', 96, 256, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (132, '地狱烈焰', 1435, 0, 'Abnormal00.dds', 128, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (133, '深渊', 1439, 0, 'Abnormal00.dds', 160, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (134, '选择性异常状态', 0, 0, 'Abnormal00.dds', 192, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (135, '陨石术_无力感', 1444, 0, 'Abnormal00.dds', 224, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (136, '陨石术_迟钝感', 1445, 0, 'Abnormal00.dds', 256, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (137, '陨石术_神经冲击', 1446, 0, 'Abnormal00.dds', 288, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (138, '陨石术_混乱', 1447, 0, 'Abnormal00.dds', 320, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (139, '陨石术_混沌', 1448, 0, 'Abnormal00.dds', 352, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (140, '返回待机状态', 1488, 1, 'Abnormal00.dds', 384, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (141, '守护祝福', 1540, 1, 'Abnormal00.dds', 416, 256, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (142, '生命增幅', 0, 1, 'Abnormal00.dds', 448, 256, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (143, '防御提升', 0, 1, 'Abnormal00.dds', 480, 256, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (144, '攻击速度提升', 0, 1, 'Abnormal00.dds', 0, 288, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (145, '速度提升', 0, 1, 'Abnormal00.dds', 32, 288, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (146, '魔法防御', 0, 1, 'Abnormal00.dds', 64, 288, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (147, '驱散魔法', 0, 0, 'Abnormal00.dds', 96, 288, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (148, '净化卷轴', 0, 0, 'Abnormal00.dds', 128, 288, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (149, '回避伤害', 0, 1, 'Abnormal00.dds', 160, 288, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (150, '闪电突击', 0, 0, 'Abnormal00.dds', 192, 288, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (151, '大地恐怖', 1518, 0, 'Abnormal00.dds', 224, 288, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (152, '减少伤害', 0, 1, 'Abnormal00.dds', 256, 288, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (153, '电光盾', 1557, 1, 'Abnormal00.dds', 288, 288, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (154, '水盾', 1559, 1, 'Abnormal00.dds', 320, 288, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (155, '反射光环', 1604, 1, 'Abnormal00.dds', 352, 288, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (156, '吸血触摸', 1548, 0, 'Abnormal00.dds', 384, 288, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (157, '名誉点数保护', 0, 0, 'Abnormal00.dds', 416, 288, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (158, '名誉点数增幅', 0, 0, 'Abnormal00.dds', 448, 288, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (159, '大地震动', 0, 0, 'Abnormal00.dds', 480, 288, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (160, '混沌战场套装效果', 0, 0, 'Abnormal00.dds', 0, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (161, '奥利爱德图腾', 1610, 0, 'Abnormal00.dds', 32, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (162, '反射', 0, 1, 'Abnormal00.dds', 64, 320, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (163, 'HP,MP 上限增加', 1242, 1, 'Abnormal00.dds', 96, 320, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (164, '负重增加(II)', 0, 1, 'Abnormal00.dds', 128, 320, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (165, '传送被封印', 0, 0, 'Abnormal00.dds', 160, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (166, '传送被封印 无法使用', 0, 0, 'Abnormal00.dds', 192, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (167, '变身用漂浮术', 1373, 1, 'Abnormal00.dds', 224, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (168, '施蛊成功', 1581, 0, 'Abnormal00.dds', 256, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (169, '怪物复活', 0, 0, 'Abnormal00.dds', 288, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (170, '透明披风感知', 0, 1, 'Abnormal00.dds', 320, 320, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (171, '运营透明', 0, 0, 'Abnormal00.dds', 352, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (172, '解除选择性状态', 1045, 0, 'Abnormal00.dds', 384, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (173, '正在施蛊', 0, 0, 'Abnormal00.dds', 416, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (174, '怪物复活成功', 1583, 0, 'Abnormal00.dds', 448, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (175, '激活访问NPC', 1692, 0, 'Abnormal00.dds', 480, 320, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (176, '守护者', 0, 0, 'Abnormal00.dds', 0, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (177, '加减声望值', 0, 0, 'Abnormal00.dds', 32, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (178, '攻击速度提升', 0, 1, 'Abnormal00.dds', 64, 352, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (179, '', 0, 1, 'Abnormal00.dds', 96, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (180, '精灵的祝福', 0, 1, 'Abnormal00.dds', 128, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (181, '秩序的祝福', 0, 1, 'Abnormal00.dds', 160, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (182, '石锤镇的祝福', 0, 1, 'Abnormal00.dds', 192, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (183, '妖精的祝福', 0, 1, 'Abnormal00.dds', 224, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (184, '那勒图的祝福', 0, 1, 'Abnormal00.dds', 256, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (185, '遗弃的村镇的祝福', 0, 1, 'Abnormal00.dds', 288, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (186, '扎拉坦的祝福', 0, 1, 'Abnormal00.dds', 320, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (187, '红色峡谷的祝福', 0, 1, 'Abnormal00.dds', 352, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (188, '技能列表专用 能力值 增加', 0, 0, 'Abnormal00.dds', 384, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (189, '强化现有技能', 0, 0, 'Abnormal00.dds', 416, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (190, '特殊技能抵抗', 0, 0, 'Abnormal00.dds', 448, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (191, '列表用 特殊技能', 0, 0, 'Abnormal00.dds', 480, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (192, '自动攻击', 0, 0, 'Abnormal00.dds', 0, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (193, '专属技能生成', 0, 0, 'Abnormal00.dds', 32, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (194, '目标锁定', 0, 0, 'Abnormal00.dds', 32, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (195, '召唤', 0, 0, 'Abnormal00.dds', 96, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (196, '最后的抵抗', 0, 1, 'Abnormal00.dds', 96, 384, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (197, '魔法抵抗', 0, 0, 'Abnormal00.dds', 128, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (198, '火焰箭', 1872, 0, 'Abnormal00.dds', 160, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (199, '爆炸', 0, 0, 'Abnormal00.dds', 224, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (200, '钢铁陷阱', 1886, 0, 'Abnormal00.dds', 256, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (201, '粘性陷阱', 1887, 0, 'Abnormal00.dds', 256, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (202, '法力值转换HP', 0, 1, 'Abnormal00.dds', 288, 384, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (203, '法力值转换攻击力', 0, 1, 'Abnormal00.dds', 320, 384, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (204, '魔力吸食', 0, 0, 'Abnormal00.dds', 384, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (205, '解毒', 0, 0, 'Abnormal00.dds', 416, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (206, '消灭', 0, 1, 'Abnormal00.dds', 416, 384, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (207, '狂暴', 0, 1, 'Abnormal00.dds', 448, 384, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (208, '攻击图腾', 1882, 1, 'Abnormal00.dds', 480, 384, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (209, '防御图腾', 1879, 1, 'Abnormal00.dds', 0, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (210, '连锁技能', 0, 0, 'Abnormal00.dds', 64, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (211, '防御力瞬间增加', 1257, 1, 'Abnormal00.dds', 96, 128, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (212, '目标锁定的BUFF状态', 0, 0, 'Abnormal00.dds', 32, 384, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (213, '装备型 状态异常', 0, 1, 'Abnormal00.dds', 96, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (214, '药剂恢复量变动', 1894, 0, 'Abnormal00.dds', 128, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (215, '陷阱的透明状态', 0, 0, 'Abnormal00.dds', 224, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (216, '法力转换的状态', 0, 1, 'Abnormal00.dds', 256, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (217, '攻击图腾的效果状态', 0, 1, 'Abnormal00.dds', 288, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (218, '防御图腾的效果状态', 0, 1, 'Abnormal00.dds', 320, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (219, '侵蚀火焰抵抗状态', 0, 0, 'Abnormal00.dds', 352, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (220, '英雄爆竹', 0, 1, 'Abnormal00.dds', 192, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (221, '猎人爆竹', 0, 1, 'Abnormal00.dds', 224, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (222, '贤者爆竹', 0, 1, 'Abnormal00.dds', 256, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (223, '普尔菲因的恩惠', 0, 1, 'Abnormal00.dds', 160, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (224, '暴击几率瞬间增加', 1253, 1, 'Abnormal00.dds', 384, 192, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (225, '暴击几率技能增加', 1253, 1, 'Abnormal00.dds', 384, 192, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (226, '网吧经验值增加', 0, 0, 'Abnormal00.dds', 288, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (227, '高级经验值增加', 0, 0, 'Abnormal00.dds', 320, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (228, '元素爆炸', 1950, 0, 'Abnormal00.dds', 352, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (229, '元素抵抗', 1951, 1, 'Abnormal00.dds', 384, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (230, '元素膨胀', 1955, 1, 'Abnormal00.dds', 416, 416, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (231, '元素爆发', 1952, 0, 'Abnormal00.dds', 448, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (232, '火焰环绕', 1938, 0, 'Abnormal00.dds', 0, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (233, '寒气环绕', 1939, 0, 'Abnormal00.dds', 480, 416, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (234, '侵蚀环绕', 1941, 0, 'Abnormal00.dds', 32, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (235, '闪电环绕', 1940, 0, 'Abnormal00.dds', 96, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (236, '火焰爆炸', 0, 0, 'Abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (237, '寒气爆炸', 0, 0, 'Abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (238, '侵蚀爆炸', 0, 0, 'Abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (239, '闪电爆炸', 0, 0, 'Abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (240, '欧谱诺的祝福', 1046, 1, 'Abnormal00.dds', 32, 448, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (241, '减少目标的暴击', 0, 1, 'abnormal00.dds', 0, 0, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (242, '指定异常状态（巴普麦特专用）', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (243, '召唤魔像（巴普麦特专用）', 1972, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (244, '施展动作（巴普麦特专用）', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (245, '失败动作（巴普麦特专用）', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (246, '成功动作（巴普麦特专用）', 1974, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (247, '施展攻击技能（巴普麦特专用）', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (248, '中毒', 0, 1, 'Abnormal00.dds', 320, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (249, '免罪符', 1067, 0, 'Abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (250, '休眠经验值激活', 0, 0, 'Abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (251, '4大精灵的灵魂', 0, 1, 'abnormal00.dds', 288, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (252, '杀戮追加数值应用', 0, 0, 'Abnormal01.dds', 160, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (253, '防护追加数值应用', 0, 0, 'Abnormal01.dds', 160, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (254, '透明披风', 0, 0, 'Abnormal00.dds', 384, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (255, '注视者披风', 0, 0, 'Abnormal00.dds', 416, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (256, '所有攻击', 0, 0, 'abnormal00.dds', 448, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (257, '致命一击', 0, 0, 'abnormal00.dds', 480, 448, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (258, '疾速攻击', 0, 0, 'abnormal00.dds', 0, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (259, '集中攻击', 0, 0, 'abnormal00.dds', 32, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (260, '疾速反击', 0, 0, 'abnormal00.dds', 64, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (261, '集中反击', 0, 0, 'abnormal00.dds', 96, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (262, '庇护', 0, 0, 'abnormal00.dds', 128, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (263, '闪避', 0, 0, 'abnormal00.dds', 160, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (264, '奋发', 0, 0, 'abnormal00.dds', 192, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (265, '减少持续伤害', 0, 0, 'abnormal00.dds', 224, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (266, '潜行', 0, 0, 'abnormal00.dds', 256, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (267, '疾速', 0, 0, 'abnormal00.dds', 288, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (268, '近程额外防御', 0, 0, 'abnormal00.dds', 320, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (269, '勇气', 0, 0, 'Abnormal00.dds', 352, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (270, '安息状态', 0, 1, 'abnormal00.dds', 448, 352, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (271, '古代记忆', 4096, 1, 'abnormal00.dds', 384, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (272, '净化气息', 1045, 0, 'abnormal00.dds', 448, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (273, '土豪服务', 0, 0, 'Abnormal01.dds', 32, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (274, '鲁普的红色气息', 4157, 1, 'Abnormal01.dds', 128, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (275, '鲁普的蓝色气息', 4159, 1, 'Abnormal01.dds', 160, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (276, '猎人的祝福', 4161, 0, 'Abnormal01.dds', 192, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (277, 'MP消耗量增减', 0, 0, 'Abnormal00.dds', 64, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (278, '可增加暴击率状态', 0, 0, 'Abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (279, '额外增加暴击率', 0, 0, 'Abnormal01.dds', 320, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (280, '疾风图腾', 4171, 1, 'Abnormal01.dds', 256, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (281, '毒瓶生成', 0, 0, 'Abnormal00.dds', 224, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (282, '神圣铠甲', 0, 0, 'Abnormal01.dds', 128, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (283, '死亡之手', 0, 0, 'Abnormal01.dds', 160, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (284, '雷神庇护', 0, 0, 'Abnormal01.dds', 0, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (285, '巴普麦特的血气', 0, 0, 'Abnormal01.dds', 384, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (286, '音速屏障强化', 0, 0, 'Abnormal00.dds', 160, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (287, '攻击速度减少', 0, 0, 'Abnormal01.dds', 384, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (288, '变身操纵等级', 4260, 0, 'Abnormal01.dds', 416, 32, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (289, '强力魔法师的秘药', 1559, 1, 'Abnormal01.dds', 448, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (290, '实习魔法师的秘药', 1559, 1, 'Abnormal01.dds', 480, 32, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (291, '征服的披风', 4271, 0, 'abnormal00.dds', 448, 480, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (292, '天空之城刻印', 4259, 0, 'Abnormal01.dds', 0, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (293, '使用战币福袋', 0, 0, 'Abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (294, '非目标使用技能', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (295, '米泰欧斯使用技能提示', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (296, '米泰欧斯技能准备(米泰欧斯专用)', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (297, '米泰欧斯技能施放(米泰欧斯专用)', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (298, '米泰欧斯技能发动(米泰欧斯专用)', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (299, '米泰欧斯弱化(米泰欧斯专用)', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (300, '近距离伤害完美回避', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (301, '米泰欧斯飞行(米泰欧斯专用)', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (302, '米泰欧斯无敌(米泰欧斯专用)', 0, 0, 'abnormal00.dds', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (303, '隐身限制', 0, 0, 'Abnormal01.dds', 32, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (304, '无法移动', 0, 0, 'Abnormal01.dds', 64, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (305, '回避增加', 0, 0, 'Abnormal01.dds', 96, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (306, '持续伤害', 1037, 0, 'Abnormal01.dds', 128, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (307, '无视护甲 - 内心的破坏', 1369, 0, 'Abnormal00.dds', 192, 224, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (308, '反射光环 - 魔力扩张', 4614, 1, '', 0, 0, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (309, '召唤奥利爱德图腾 - 吸收强化', 4618, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (310, '群体拯救 - 暴风的时间', 4604, 1, 'Abnormal00.dds', 128, 160, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (311, '漂浮术 - 回避增加', 4606, 1, 'Abnormal01.dds', 96, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (312, '暗杀技能- 巨鹰之眼', 4609, 1, '', 0, 0, 1);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (313, '装备了宠物', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (314, '宠物亲密度药水道具', 1104, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (315, '亲密度 力量增加', 0, 1, 'abnormal01.dds', 32, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (316, '亲密度 智力增加', 0, 1, 'abnormal01.dds', 96, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (317, '亲密度 敏捷增加', 0, 1, 'abnormal01.dds', 64, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (318, '亲密度 药水恢复', 0, 1, 'abnormal01.dds', 256, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (319, '亲密度 近距离伤害减少', 0, 1, 'abnormal01.dds', 160, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (320, '亲密度 远距离伤害减少', 0, 1, 'abnormal01.dds', 192, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (321, '亲密度 魔法伤害减少', 0, 1, 'abnormal01.dds', 224, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (322, '亲密度 暴击增加', 0, 1, 'abnormal01.dds', 288, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (323, '亲密度 暴击伤害增加', 0, 1, 'abnormal01.dds', 320, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (324, '亲密度伤害减少', 0, 1, 'abnormal01.dds', 352, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (325, '亲密度 魔力消耗减少', 0, 1, 'abnormal01.dds', 384, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (326, '亲密度 魔法命中增加', 0, 1, 'abnormal01.dds', 416, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (327, '亲密度 特效添加', 4593, 1, 'abnormal01.dds', 480, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (328, '亲密度 经验值增加、', 0, 1, 'abnormal01.dds', 0, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (329, '亲密度德拉克外形变更', 0, 1, 'abnormal01.dds', 448, 64, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (330, '可变亲密度 药水恢复', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (331, '盈月突袭怪物 无敌', 4636, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (332, '盈月突袭怪物 恢复', 4637, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (333, '可变亲密度 近距离伤害减少', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (334, '可变亲密度 远距离伤害减少', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (335, '可变亲密度 魔法伤害减少', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (336, '可变亲密度 暴击率增加', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (337, '可变亲密度 暴击伤害增加', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (338, '可变亲密度伤害减少', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (339, '可变亲密度 魔力消耗减少', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (340, '可变亲密度 魔法命中增加', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (341, '可变亲密度 特效添加', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (342, '可变亲密度 经验值增加', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (343, '可变亲密度 德拉克外形变更', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (344, '狩猎时获得经验值增加', 0, 1, 'Abnormal01.dds', 288, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (345, '条件状态异常
', 0, 1, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (346, '光的祈护
', 4838, 1, 'abnormal01.dds', 128, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (347, '骑士的力量
', 0, 1, 'abnormal01.dds', 320, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (348, '骑士的韧性
', 0, 1, 'abnormal01.dds', 352, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (349, '游侠的 力量
', 0, 1, 'abnormal01.dds', 384, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (350, '游侠的韧性
', 0, 1, 'abnormal01.dds', 416, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (351, '刺客的力量
', 0, 1, 'abnormal01.dds', 448, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (352, '刺客的韧性
', 0, 1, 'abnormal01.dds', 480, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (353, '召唤师的力量
', 0, 1, 'abnormal01.dds', 0, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (354, '召唤师的韧性', 0, 1, 'abnormal01.dds', 32, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (355, '精灵的力量
', 0, 1, 'abnormal01.dds', 64, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (356, '精灵的韧性
', 0, 1, 'abnormal01.dds', 96, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (357, '灵魂试炼
', 1045, 0, 'abnormal01.dds', 192, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (358, '宠物力量
', 0, 0, 'abnormal01.dds', 160, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (359, '宠物力场确认
', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (360, '[活动] 烟花', 4910, 0, 'Abnormal00.dds', 96, 96, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (361, '竞技场角色无敌
', 4902, 0, 'abnormal01.dds', 224, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (362, '竞技场城门无敌
', 0, 0, 'abnormal01.dds', 224, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (363, '竞技场守护石无敌
', 4903, 0, 'abnormal01.dds', 224, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (364, '竞技场药水使用时恢复量减少1
', 0, 0, 'abnormal01.dds', 320, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (365, '竞技场药水使用时恢复量减少2
', 0, 0, 'abnormal01.dds', 352, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (366, '竞技场药水使用时恢复量减少3
', 0, 0, 'abnormal01.dds', 384, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (367, '竞技场药水使用时恢复量减少4
', 0, 0, 'abnormal01.dds', 416, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (368, '竞技场药水使用时恢复量减少5
', 0, 0, 'abnormal01.dds', 448, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (369, '连接
', 0, 0, '', 0, 0, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (370, '宠物反力场
', 5064, 0, 'Abnormal01.dds', 480, 128, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (371, '宠物力量
', 5063, 0, 'Abnormal01.dds', 0, 160, 0);
GO

INSERT INTO [dbo].[TP_AbnormalType] ([AType], [AName], [AEffect], [ARemovable], [AFileName], [AIconX], [AIconY], [ACopyable]) VALUES (372, '宠物力量确认
', 0, 0, 'Abnormal00.dds', 0, 0, 0);
GO

