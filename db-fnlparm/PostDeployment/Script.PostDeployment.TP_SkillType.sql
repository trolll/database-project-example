/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_SkillType
Date                  : 2023-10-07 09:08:48
*/


INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (1, '아이템전용');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (2, '스킬트리용');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (3, '마법책');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (4, '전사레인져스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (5, '스팟상점용');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (6, '몬스터용 스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (7, '포션류');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (8, '매터리얼 전용');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (9, '카오스배틀');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (10, '아이템 장착');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (11, '버프 물약용 스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (12, '변신 전용');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (13, '퀘스트보상스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (14, '길드스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (15, '캐릭터 공통 스킬트리');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (16, '나이트 스킬트리');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (22, '서번트 스킬트리');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (23, '특성 강화 스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (24, '소유형 변신');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (25, '서번트 친밀도 스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (17, '레인저 스킬트리');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (18, '엘프 스킬트리');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (19, '어쌔신 스킬트리');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (20, '서모너 스킬트리');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (21, '스킬팩 생성');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (26, '서번트 변신 스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (27, '서번트 모임장 스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (28, '서번트 합성 친밀도 스킬');
GO

INSERT INTO [dbo].[TP_SkillType] ([SType], [SName]) VALUES (29, '서번트 합성 변신 스킬');
GO

