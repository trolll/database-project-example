/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblEventObj
Date                  : 2023-10-07 09:09:30
*/


SET IDENTITY_INSERT [dbo].[TblEventObj] ON;

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (1, 7, 0, 136);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (2, 7, 0, 137);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (3, 7, 1, 1979);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (4, 8, 0, 137);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (5, 9, 0, 928);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (6, 10, 0, 929);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (7, 10, 1, 1622);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (8, 11, 0, 137);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (9, 11, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (19, 11, 1, 158);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (18, 11, 1, 202);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (17, 11, 1, 204);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (13, 11, 1, 720);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (12, 11, 1, 755);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (10, 11, 1, 867);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (11, 11, 1, 868);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (15, 11, 1, 1005);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (16, 11, 1, 1007);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (14, 11, 1, 1009);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (20, 12, 0, 945);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (21, 13, 0, 937);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (22, 14, 0, 845);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (31, 15, 0, 717);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (32, 15, 0, 959);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (33, 15, 0, 960);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (34, 15, 0, 961);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (35, 15, 0, 962);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (36, 15, 0, 963);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (23, 15, 1, 1043);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (24, 15, 1, 2557);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (25, 15, 1, 2558);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (26, 15, 1, 2559);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (27, 15, 1, 2560);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (28, 15, 1, 2561);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (29, 15, 1, 2562);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (30, 15, 1, 2563);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (37, 16, 0, 137);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (38, 16, 1, 1979);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (42, 17, 0, 1067);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (39, 17, 1, 2816);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (40, 17, 1, 2817);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (41, 17, 1, 2818);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (43, 18, 0, 965);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (44, 18, 1, 2568);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (45, 18, 1, 2569);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (46, 19, 0, 1072);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (47, 19, 1, 2868);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (48, 19, 1, 2869);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (49, 19, 1, 2870);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (50, 19, 1, 2871);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (52, 21, 0, 137);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (53, 21, 1, 3381);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (54, 21, 1, 3382);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (55, 21, 1, 3383);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (56, 21, 1, 3384);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (57, 21, 1, 3385);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (61, 22, 0, 1180);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (62, 22, 0, 1181);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (63, 22, 0, 1182);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (64, 22, 0, 1183);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (60, 23, 0, 1068);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (65, 24, 1, 4188);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (66, 24, 1, 4189);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (67, 24, 1, 4190);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (68, 24, 1, 4191);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (69, 24, 1, 4192);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (70, 25, 1, 4193);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (71, 25, 1, 4194);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (72, 25, 1, 4195);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (73, 25, 1, 4196);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (74, 25, 1, 4197);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (80, 26, 0, 1279);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (75, 26, 1, 4183);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (76, 26, 1, 4184);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (77, 26, 1, 4185);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (78, 26, 1, 4186);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (79, 26, 1, 4187);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (86, 27, 0, 1276);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (87, 27, 1, 5457);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (88, 27, 1, 5460);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (89, 27, 1, 5461);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (90, 27, 1, 5464);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (91, 28, 1, 4364);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (97, 29, 0, 1276);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (98, 29, 1, 5457);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (99, 29, 1, 5460);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (100, 29, 1, 5461);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (101, 29, 1, 5464);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (102, 30, 1, 4364);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (103, 31, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (105, 31, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (104, 31, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (106, 32, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (108, 32, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (107, 32, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (109, 33, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (111, 33, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (110, 33, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (112, 34, 0, 937);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (113, 34, 0, 945);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (115, 34, 0, 1068);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (114, 34, 0, 1180);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (116, 35, 0, 1614);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (117, 35, 0, 1616);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (118, 35, 0, 1618);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (119, 35, 1, 1982);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (120, 35, 1, 4473);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (121, 35, 1, 4474);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (122, 35, 1, 4475);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (123, 35, 1, 4476);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (124, 35, 1, 4477);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (125, 35, 1, 4478);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (126, 36, 1, 4826);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (127, 36, 1, 4827);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (128, 37, 0, 1654);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (129, 38, 0, 1654);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (130, 39, 0, 1654);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (131, 40, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (133, 40, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (132, 40, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (134, 41, 0, 1614);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (135, 41, 0, 1616);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (136, 41, 0, 1618);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (137, 41, 1, 1982);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (138, 41, 1, 4473);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (139, 41, 1, 4474);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (140, 41, 1, 4475);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (141, 41, 1, 4476);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (142, 41, 1, 4477);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (143, 41, 1, 4478);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (144, 42, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (145, 42, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (146, 43, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (147, 43, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (148, 43, 1, 4857);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (149, 43, 1, 4858);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (150, 44, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (152, 44, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (151, 44, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (153, 45, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (154, 45, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (155, 46, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (156, 46, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (157, 46, 1, 4857);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (158, 46, 1, 4858);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (159, 47, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (161, 47, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (160, 47, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (162, 48, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (164, 48, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (163, 48, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (165, 49, 0, 1614);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (166, 49, 0, 1616);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (167, 49, 0, 1618);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (168, 49, 1, 1982);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (169, 49, 1, 4473);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (170, 49, 1, 4474);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (171, 49, 1, 4475);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (172, 49, 1, 4476);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (173, 49, 1, 4477);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (174, 49, 1, 4478);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (175, 50, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (176, 50, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (177, 51, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (178, 51, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (179, 52, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (180, 52, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (181, 53, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (182, 53, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (183, 53, 1, 4857);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (184, 53, 1, 4858);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (185, 54, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (186, 54, 1, 4010);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (187, 54, 1, 4857);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (188, 54, 1, 4858);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (189, 55, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (191, 55, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (190, 55, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (192, 56, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (194, 56, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (193, 56, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (195, 57, 0, 1924);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (196, 58, 0, 1873);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (197, 59, 0, 1873);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (198, 59, 1, 5688);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (199, 59, 1, 5689);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (200, 60, 0, 2038);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (201, 60, 1, 6323);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (202, 60, 1, 6324);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (203, 61, 0, 1614);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (204, 61, 0, 1616);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (205, 61, 0, 1618);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (221, 61, 1, 1982);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (206, 61, 1, 4473);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (207, 61, 1, 4475);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (208, 61, 1, 4476);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (209, 61, 1, 4477);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (210, 61, 1, 4478);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (211, 61, 1, 4514);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (212, 62, 0, 1614);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (213, 62, 0, 1616);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (214, 62, 0, 1618);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (222, 62, 1, 1982);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (215, 62, 1, 4473);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (216, 62, 1, 4475);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (217, 62, 1, 4476);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (218, 62, 1, 4477);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (219, 62, 1, 4478);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (220, 62, 1, 4514);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (223, 63, 0, 1924);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (224, 64, 0, 1873);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (225, 64, 1, 5688);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (226, 64, 1, 5689);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (227, 65, 0, 1345);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (228, 65, 0, 1347);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (229, 65, 0, 1349);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (230, 65, 0, 1351);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (231, 65, 0, 2344);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (232, 66, 0, 1924);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (233, 67, 0, 1873);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (234, 67, 1, 5688);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (235, 67, 1, 5689);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (236, 68, 0, 1345);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (237, 68, 0, 1347);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (238, 68, 0, 1349);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (239, 68, 0, 1351);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (240, 68, 0, 2344);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (241, 69, 0, 2103);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (242, 70, 0, 2103);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (243, 71, 0, 2103);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (244, 72, 0, 1289);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (245, 72, 0, 1290);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (246, 73, 0, 1614);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (247, 73, 0, 1616);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (248, 73, 0, 1618);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (249, 73, 1, 1982);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (250, 73, 1, 4473);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (251, 73, 1, 4474);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (252, 73, 1, 4475);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (253, 73, 1, 4476);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (254, 73, 1, 4477);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (255, 73, 1, 4478);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (256, 74, 0, 2579);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (257, 75, 0, 2688);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (258, 75, 0, 2689);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (259, 76, 0, 2578);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (260, 77, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (261, 77, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (262, 78, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (263, 78, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (264, 78, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (265, 79, 0, 2578);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (266, 80, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (267, 80, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (268, 81, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (269, 81, 1, 157);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (270, 81, 1, 4859);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (271, 82, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (273, 84, 0, 2748);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (274, 85, 0, 723);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (275, 85, 0, 724);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (276, 85, 0, 725);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (277, 85, 0, 941);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (278, 86, 0, 1288);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (281, 86, 0, 1308);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (280, 86, 0, 1309);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (279, 86, 0, 1310);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (282, 86, 0, 1311);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (283, 86, 0, 1312);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (284, 86, 0, 2672);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (285, 86, 0, 2673);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (286, 86, 0, 2674);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (287, 86, 0, 2675);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (288, 86, 0, 2676);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (289, 86, 0, 2677);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (290, 86, 0, 2678);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (291, 86, 0, 2679);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (292, 86, 0, 2680);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (293, 86, 0, 2681);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (294, 87, 0, 856);
GO

INSERT INTO [dbo].[TblEventObj] ([mID], [mEventID], [mObjType], [mObjID]) VALUES (295, 88, 0, 2806);
GO

SET IDENTITY_INSERT [dbo].[TblEventObj] OFF;