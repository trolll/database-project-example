/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblUserQuest
Date                  : 2023-10-07 09:09:29
*/


INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (5, 1, 0, '2007-05-28 05:41:55.983');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (6, 1, 0, '2007-06-07 18:40:51.253');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (14, 1, 0, '2007-06-05 01:52:12.860');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (21, 2, 0, '2007-11-09 11:05:23.403');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (30, 2, 0, '2007-11-09 14:42:14.607');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (32, 1, 0, '2007-06-10 23:55:16.003');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (35, 1, 0, '2007-06-15 03:33:58.827');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (36, 1, 0, '2007-06-13 02:08:19.513');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (55, 2, 0, '2007-11-09 13:09:18.700');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (59, 2, 0, '2007-11-09 10:27:44.937');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (60, 2, 0, '2007-11-09 10:27:52.577');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (63, 2, 0, '2007-11-09 12:32:00.280');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (65, 2, 0, '2007-11-09 10:27:51.060');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (67, 2, 0, '2007-11-09 14:36:11.030');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (73, 1, 0, '2007-11-07 17:21:00.403');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (74, 2, 0, '2007-11-09 10:28:28.950');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (76, 2, 0, '2007-11-09 12:31:13.700');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (354, 2, 0, '2008-09-28 14:35:22.683');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (416, 2, 0, '2007-11-09 13:28:59.280');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (420, 2, 0, '2007-11-09 11:03:59.467');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (423, 1, 0, '2007-10-05 09:14:14.340');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (423, 2, 0, '2007-09-27 15:31:59.343');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (429, 1, 0, '2007-11-09 17:15:16.683');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (429, 2, 0, '2007-11-09 15:18:38.763');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (432, 1, 0, '2007-11-13 09:43:42.730');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (436, 1, 0, '2007-11-14 11:05:21.043');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (436, 2, 0, '2007-11-09 11:07:34.250');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (437, 2, 0, '2007-11-09 15:12:15.217');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (438, 1, 0, '2007-09-18 13:07:26.653');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (438, 2, 0, '2007-09-27 15:22:05.670');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (439, 1, 0, '2007-11-06 16:47:39.120');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (439, 2, 0, '2007-11-09 12:02:08.500');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (441, 1, 0, '2007-11-06 17:48:42.077');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (441, 2, 0, '2007-09-27 15:22:35.357');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (444, 2, 0, '2007-09-27 16:40:07.107');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (457, 1, 0, '2007-11-04 16:26:31.930');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (458, 2, 0, '2007-11-09 11:52:34.623');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (459, 2, 0, '2007-09-27 15:59:02.793');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (463, 2, 0, '2007-11-08 16:48:36.607');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (464, 2, 0, '2007-11-09 15:15:31.263');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (466, 2, 0, '2007-09-28 14:36:04.843');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (471, 1, 0, '2007-11-07 11:17:04.750');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (474, 2, 0, '2007-09-27 15:22:18.327');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (476, 2, 0, '2007-11-09 15:03:31.980');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (477, 1, 0, '2007-11-02 11:01:02.963');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (477, 2, 0, '2007-09-27 15:26:02.343');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (478, 1, 0, '2007-09-18 18:38:30.043');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (478, 2, 0, '2007-09-19 11:28:34.967');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (484, 1, 0, '2007-11-11 15:41:09.903');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (484, 2, 0, '2007-11-09 15:15:22.170');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (488, 1, 0, '2007-11-02 15:35:31.900');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (491, 1, 0, '2007-09-29 15:10:49.200');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (491, 2, 0, '2007-10-08 17:41:17.607');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (546, 1, 0, '2007-11-13 15:17:19.310');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (1367, 2, 0, '2008-09-28 14:35:16.840');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (1720, 2, 0, '2008-09-28 14:42:53.777');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (4113, 2, 0, '2008-09-28 14:36:10.853');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (7506, 2, 0, '2008-09-28 14:37:06.680');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (17535, 2, 0, '2008-09-28 14:35:56.947');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (32211, 2, 0, '2008-09-28 14:35:23.557');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (56527, 2, 0, '2008-09-28 14:43:00.773');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (62720, 2, 0, '2008-09-28 14:45:52.613');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (78586, 2, 0, '2008-09-28 14:35:40.870');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (89845, 2, 0, '2008-09-28 14:35:34.527');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (97706, 2, 0, '2008-09-28 14:44:49.867');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (110412, 2, 0, '2008-09-28 14:44:48.850');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (113374, 2, 0, '2008-09-28 14:40:36.700');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (124339, 2, 0, '2008-09-28 14:42:38.150');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (145929, 2, 0, '2008-09-28 14:35:38.010');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (146481, 2, 0, '2008-09-28 14:35:39.700');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (147043, 2, 0, '2008-09-28 14:42:39.480');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (148497, 2, 0, '2008-09-28 14:41:03.013');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (148753, 2, 0, '2008-09-28 14:39:20.563');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (178157, 2, 0, '2008-09-28 14:35:17.120');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (192927, 2, 0, '2008-09-28 14:42:05.387');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (199422, 2, 0, '2008-09-28 14:35:23.497');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (237892, 2, 0, '2008-09-28 14:36:16.383');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (242976, 2, 0, '2008-09-28 14:41:30.560');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (244724, 2, 0, '2008-09-28 14:42:41.387');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (256798, 2, 0, '2008-09-28 14:36:00.167');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (259609, 2, 0, '2008-09-28 14:35:13.790');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (282762, 2, 0, '2008-09-28 14:35:22.730');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (308420, 2, 0, '2008-09-28 14:42:08.120');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (323865, 2, 0, '2008-09-28 14:35:47.493');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (331925, 2, 0, '2008-09-28 14:39:51.640');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (337455, 2, 0, '2008-09-28 14:35:38.790');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (411511, 2, 0, '2008-09-28 14:40:42.013');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (456060, 2, 0, '2008-09-28 14:35:37.027');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (567716, 2, 0, '2008-09-28 14:45:38.833');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (629309, 2, 0, '2008-09-28 14:36:13.070');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (794762, 2, 0, '2008-09-28 14:35:17.963');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (860356, 2, 0, '2008-09-28 14:42:30.887');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (864591, 2, 0, '2008-09-28 14:35:16.510');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (870193, 2, 0, '2008-09-28 14:42:43.493');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (871833, 2, 0, '2008-09-28 14:36:49.583');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (885979, 2, 0, '2008-09-28 14:42:42.713');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (899021, 2, 0, '2008-09-28 14:35:10.400');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (899022, 2, 0, '2008-09-28 14:35:20.057');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (913163, 2, 0, '2008-09-28 14:42:35.200');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (950116, 2, 0, '2008-09-28 14:42:30.340');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (961686, 2, 0, '2008-09-28 14:41:50.417');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (963964, 2, 0, '2008-09-28 14:39:28.877');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (974191, 2, 0, '2008-09-28 14:35:48.417');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (977295, 2, 0, '2008-09-28 14:37:33.350');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (981971, 2, 0, '2008-09-28 14:38:27.550');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (994442, 2, 0, '2008-09-28 14:46:14.487');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (1002837, 2, 0, '2008-09-28 14:42:31.747');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (1005795, 2, 0, '2008-09-28 14:35:29.747');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (1009553, 2, 0, '2008-09-28 14:35:10.557');
GO

INSERT INTO [dbo].[TblUserQuest] ([mUserNo], [mQuestNo], [mValue], [mUpdateDate]) VALUES (1016871, 2, 0, '2008-09-28 14:42:18.887');
GO

