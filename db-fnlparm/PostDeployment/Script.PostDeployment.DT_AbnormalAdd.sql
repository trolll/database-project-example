/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_AbnormalAdd
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (1, 1, 1, 1);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (2, 1, 2, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (3, 1, 3, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (4, 1, 4, 5);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (5, 2, 1, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (6, 2, 2, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (7, 2, 3, 5);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (8, 2, 4, 6);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (10, 3, 1, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (11, 3, 2, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (12, 3, 3, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (13, 3, 4, 6);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (14, 4, 1, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (15, 4, 2, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (16, 4, 3, 1);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (17, 4, 4, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (18, 5, 1, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (19, 5, 2, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (20, 5, 3, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (21, 5, 4, 5);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (22, 6, 1, 1);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (23, 6, 2, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (24, 6, 3, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (25, 6, 4, 5);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (26, 7, 1, 1);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (27, 7, 2, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (28, 7, 3, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (29, 7, 4, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (30, 7, 1, 1);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (31, 7, 2, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (32, 7, 3, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (33, 7, 4, 6);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (34, 8, 1, 1);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (35, 8, 2, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (36, 8, 3, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (37, 8, 4, 6);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (38, 9, 1, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (39, 9, 2, 5);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (40, 9, 3, 6);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (41, 9, 4, 8);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (42, 23, 1, 1);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (43, 23, 2, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (44, 23, 3, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (45, 23, 4, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (46, 15, 1, 1);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (47, 15, 2, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (48, 15, 3, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (49, 15, 4, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (50, 16, 1, 2);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (52, 16, 3, 4);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (53, 16, 4, 5);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (54, 16, 2, 3);
GO

INSERT INTO [dbo].[DT_AbnormalAdd] ([AID], [AType], [ALevel], [APercent]) VALUES (57, 17, 1, 100);
GO

