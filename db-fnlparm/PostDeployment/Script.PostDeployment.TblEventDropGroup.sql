/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblEventDropGroup
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (842, 1, 0.10000000149011612);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (842, 2, 0.10000000149011612);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (842, 3, 0.10000000149011612);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (842, 4, 0.10000000149011612);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (843, 5, 1.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (843, 6, 1.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (843, 7, 1.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (843, 8, 1.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (843, 9, 1.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (843, 10, 1.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (844, 11, 3.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (844, 12, 3.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (844, 13, 3.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (844, 14, 3.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (844, 15, 3.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (844, 16, 3.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (845, 17, 5.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (845, 18, 5.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (845, 19, 5.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (846, 28, 5.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (847, 29, 100.0);
GO

INSERT INTO [dbo].[TblEventDropGroup] ([DGroup], [MClass], [DPercent]) VALUES (846, 32, 40.0);
GO

