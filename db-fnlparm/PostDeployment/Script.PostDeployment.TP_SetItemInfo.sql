/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_SetItemInfo
Date                  : 2023-10-07 09:08:50
*/


INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (1, '*5+0 稀有的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (2, '*5+1 稀有的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (3, '*5+2 稀有的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (4, '*5+3 稀有的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (5, '*5+4 稀有的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (6, '*5+5 稀有的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (7, '*5+0 稀有的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (8, '*5+1 稀有的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (9, '*5+2 稀有的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (10, '*5+3 稀有的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (11, '*5+4 稀有的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (12, '*5+5 稀有的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (13, '*5+0 稀有的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (14, '*5+1 稀有的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (15, '*5+2 稀有的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (16, '*5+3 稀有的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (17, '*5+4 稀有的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (18, '*5+5 稀有的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (19, '*5+0 稀有的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (20, '*5+1 稀有的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (21, '*5+2 稀有的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (22, '*5+3 稀有的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (23, '*5+4 稀有的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (24, '*5+5 稀有的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (25, '*5+0 稀有的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (26, '*5+1 稀有的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (27, '*5+2 稀有的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (28, '*5+3 稀有的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (29, '*5+4 稀有的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (30, '*5+5 稀有的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (31, '*2+0 史诗的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (32, '*2+1 史诗的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (33, '*2+2 史诗的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (34, '*2+3 史诗的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (35, '*2+4 史诗的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (36, '*2+5 史诗的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (37, '*2+0 史诗的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (38, '*2+1 史诗的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (39, '*2+2 史诗的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (40, '*2+3 史诗的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (41, '*2+4 史诗的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (42, '*2+5 史诗的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (43, '*2+0 史诗的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (44, '*2+1 史诗的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (45, '*2+2 史诗的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (46, '*2+3 史诗的精华套装  Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (47, '*2+4 史诗的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (48, '*2+5 史诗的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (49, '*2+0 史诗的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (50, '*2+1 史诗的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (51, '*2+2 史诗的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (52, '*2+3 史诗的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (53, '*2+4 史诗的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (54, '*2+5 史诗的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (55, '*2+0 史诗的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (56, '*2+1 史诗的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (57, '*2+2 史诗的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (58, '*2+3 史诗的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (59, '*2+4 史诗的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (60, '*2+5 史诗的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (61, '*6+0 传说的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (62, '*6+1 传说的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (63, '*6+2 传说的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (64, '*6+3 传说的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (65, '*6+4 传说的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (66, '*6+5 传说的精华套装 Lv1');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (67, '*6+0 传说的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (68, '*6+1 传说的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (69, '*6+2 传说的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (70, '*6+3 传说的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (71, '*6+4 传说的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (72, '*6+5 传说的精华套装 Lv2');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (73, '*6+0 传说的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (74, '*6+1 传说的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (75, '*6+2 传说的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (76, '*6+3 传说的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (77, '*6+4 传说的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (78, '*6+5 传说的精华套装 Lv3');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (79, '*6+0 传说的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (80, '*6+1 传说的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (81, '*6+2 传说的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (82, '*6+3 传说的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (83, '*6+4 传说的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (84, '*6+5 传说的精华套装 Lv4');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (85, '*6+0 传说的精华套装  Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (86, '*6+1 传说的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (87, '*6+2 传说的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (88, '*6+3 传说的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (89, '*6+4 传说的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (90, '*6+5 传说的精华套装 Lv5');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (91, '*8体验用套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (92, '战场 Lv1 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (93, '战场 Lv2 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (94, '战场 Lv3 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (95, '黑暗 Lv1 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (96, '黑暗 Lv2 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (97, '黑暗 Lv3 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (98, '光 Lv1 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (99, '光 Lv2 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (100, '光 Lv3 套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (101, '英雄套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (102, '传说套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (103, '名誉时间制套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (104, '*7破坏者的黑色套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (105, '*1破坏者的红色套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (106, '*5破坏者的蓝色套装');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (108, '英雄套 Lv2 ');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (109, '英雄套 Lv3 ');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (110, '传说套 Lv2 ');
GO

INSERT INTO [dbo].[TP_SetItemInfo] ([mSetType], [mSetName]) VALUES (111, '传说套 Lv3 ');
GO

