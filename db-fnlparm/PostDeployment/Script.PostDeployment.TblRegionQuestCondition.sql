/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblRegionQuestCondition
Date                  : 2023-10-07 09:09:32
*/


INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 73, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 87, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 105, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 106, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 107, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 124, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 197, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 399, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 400, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 417, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 456, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 457, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 466, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 468, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 469, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 470, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 493, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2501, 577, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 101, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 131, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 402, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 404, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 437, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 452, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 507, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 515, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 522, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2502, 818, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 37, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 88, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 125, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 404, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 411, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 437, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 439, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 440, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 441, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 442, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 443, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 444, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 465, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 479, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 480, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 540, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2503, 550, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 81, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 92, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 104, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 406, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 418, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 448, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 449, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 450, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 451, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 538, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 821, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 827, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2504, 835, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 91, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 516, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 519, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 533, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 534, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 535, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 536, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 820, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2505, 825, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2506, 84, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2506, 406, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2506, 655, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2506, 658, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2506, 692, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2506, 693, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2506, 821, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2506, 824, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 90, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 108, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 121, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 501, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 520, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 523, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 527, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 738, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 739, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 741, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2507, 819, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2508, 40, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2508, 129, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2508, 656, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2508, 657, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2508, 660, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2508, 661, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2508, 662, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2508, 823, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2509, 1288, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2509, 1308, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2509, 1309, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2509, 1310, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2509, 1311, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2509, 1312, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 45, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 96, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 103, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 110, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 111, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 112, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 397, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 472, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 473, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 477, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 659, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 735, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 736, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 737, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2510, 742, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1798, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1799, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1800, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1801, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1802, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1803, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1804, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1805, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1806, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1807, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1808, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1809, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1810, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1811, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1812, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1871, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1881, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2511, 1882, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 869, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 870, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 871, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 872, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 874, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 875, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 876, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 877, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 878, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 879, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 881, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 882, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 883, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2512, 895, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 539, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1633, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1634, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1635, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1636, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1637, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1638, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1639, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1640, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1641, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1642, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1643, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1644, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1645, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1646, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1647, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 1648, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2513, 2526, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1313, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1314, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1316, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1317, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1318, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1319, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1320, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1321, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1334, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1335, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1336, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1337, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1338, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1339, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1340, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2514, 1341, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1983, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1984, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1985, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1986, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1987, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1988, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1989, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1990, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1991, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1992, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1993, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1994, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1995, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1996, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1997, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1998, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 1999, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 2000, 1, 2, 5, 5, 0, 10);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 2047, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 2048, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2515, 2049, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2516, 2447, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2516, 2448, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2516, 2449, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2516, 2450, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2516, 2451, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2517, 2076, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2517, 2077, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2517, 2078, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2517, 2079, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2517, 2080, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2518, 2071, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2518, 2072, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2518, 2073, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2518, 2074, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2518, 2075, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2519, 2140, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2519, 2141, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2519, 2142, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2519, 2143, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2519, 2144, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2520, 2145, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2520, 2146, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2520, 2147, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2520, 2148, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2520, 2149, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2521, 2454, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2521, 2455, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2521, 2456, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2521, 2457, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2521, 2498, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2521, 2499, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2521, 2500, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2521, 2501, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2522, 2458, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2522, 2459, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2522, 2460, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2522, 2461, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2522, 2502, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2522, 2503, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2522, 2504, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2522, 2505, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2523, 2610, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2523, 2611, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2523, 2612, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2523, 2613, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2523, 2614, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2523, 2615, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2523, 2616, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2523, 2617, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2524, 2672, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2524, 2673, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2524, 2674, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2524, 2675, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2524, 2676, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2525, 2677, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2525, 2678, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2525, 2679, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2525, 2680, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2525, 2681, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2692, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2693, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2694, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2695, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2696, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2697, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2698, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2699, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2700, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2701, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2702, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2703, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2704, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2705, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2706, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2707, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2526, 2708, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2709, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2710, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2711, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2712, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2713, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2714, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2715, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2716, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2717, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2718, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2719, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2720, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2721, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2722, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2723, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2724, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2725, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2726, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2727, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2728, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2729, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2527, 2730, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2528, 2858, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2528, 2859, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2528, 2860, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2528, 2861, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2529, 2860, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2529, 2861, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2529, 2862, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2529, 2863, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2529, 2864, 0, 3, 300, 300, 300, 900);
GO

INSERT INTO [dbo].[TblRegionQuestCondition] ([mQuestNo], [mParmID], [mBoss], [mStepCnt], [mStep1], [mStep2], [mStep3], [mTotalCnt]) VALUES (2529, 2865, 0, 3, 300, 300, 300, 900);
GO

