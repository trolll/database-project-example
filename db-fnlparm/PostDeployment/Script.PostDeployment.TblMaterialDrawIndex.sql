/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblMaterialDrawIndex
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (7, 7, 1, 1, 100.0, 'IID : 40609', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (8, 8, 1, 1, 100.0, 'IID : 2798', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (9, 9, 1, 1, 100.0, 'IID : 2799', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (10, 10, 1, 1, 100.0, 'IID : 2800', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (11, 11, 1, 1, 100.0, 'IID : 2801', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (12, 12, 1, 1, 100.0, '스킬 박스', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (13, 13, 1, 1, 100.0, '고급 스킬 박스', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (14, 14, 1, 1, 100.0, '물음표 박스', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (15, 15, 0, 1, 100.0, '사라만다 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (16, 16, 0, 1, 100.0, '이그니스 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (17, 17, 0, 1, 100.0, '실프 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (18, 18, 0, 1, 100.0, '실라이론 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (19, 19, 0, 1, 100.0, '노임 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (20, 20, 0, 1, 100.0, '노이아넨 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (21, 21, 0, 1, 100.0, '운디네 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (22, 22, 0, 1, 100.0, '운다임 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (23, 23, 0, 1, 100.0, '일렉트릭 쇼크 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (24, 24, 0, 1, 100.0, '체인 일렉트릭 쇼크', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (25, 25, 0, 1, 100.0, '뱀파이어릭 터치 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (26, 26, 0, 1, 100.0, '뱀파이어릭 존', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (27, 27, 0, 1, 100.0, '대지의 공포 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (28, 28, 0, 1, 100.0, '죽음의 대지', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (29, 29, 0, 1, 100.0, '대기의 혼란 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (30, 30, 0, 1, 100.0, '혼탁한 대기', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (31, 31, 0, 1, 100.0, '라이트닝 쉴드 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (32, 32, 0, 1, 100.0, '라이트닝 아머', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (33, 33, 0, 1, 100.0, '워터 쉴드 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (34, 34, 0, 1, 100.0, '워터 아머', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (35, 35, 0, 1, 100.0, '리듀스 대미지 35', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (36, 36, 0, 1, 100.0, '리듀스 대미지 35+', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (37, 37, 0, 1, 100.0, '캔슬 스크롤 C', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (38, 38, 0, 1, 100.0, '캔슬 스크롤 C+', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (39, 39, 1, 1, 100.0, '무게의 룬', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (40, 40, 1, 1, 100.0, '에메랄드 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (41, 41, 1, 1, 100.0, '다이아몬드 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (42, 42, 0, 1, 100.0, '이프리트 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (43, 43, 0, 1, 100.0, '퓨리 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (44, 44, 0, 1, 100.0, '휠카셀 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (45, 45, 0, 1, 100.0, '슈리엘 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (46, 46, 0, 1, 100.0, '레프리컨 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (47, 47, 0, 1, 100.0, '에리스 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (48, 48, 0, 1, 100.0, '노아스 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (49, 49, 0, 1, 100.0, '시아페 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (50, 50, 0, 1, 100.0, '릴리언스 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (51, 51, 0, 1, 100.0, '엘라임 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (52, 52, 0, 1, 100.0, '레이커 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (53, 53, 0, 1, 100.0, '에리세드 아바타', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (54, 54, 0, 1, 100.0, '일렉트릭 쇼크 Lv3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (55, 55, 0, 1, 100.0, '썬더볼트 쇼크', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (56, 56, 0, 1, 100.0, '체인 일렉트릭 쇼크 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (57, 57, 0, 1, 100.0, '뱀파이어릭 터치 Lv3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (58, 58, 0, 1, 100.0, '뱀파이어 퀸 터치', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (59, 59, 0, 1, 100.0, '뱀파이어릭 존 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (60, 60, 0, 1, 100.0, '대지의 공포 Lv3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (61, 61, 0, 1, 100.0, '대지의 혼돈', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (62, 62, 0, 1, 100.0, '죽음의 대지 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (63, 63, 0, 1, 100.0, '대기의 혼란 Lv3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (64, 64, 0, 1, 100.0, '대기의 혼돈', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (65, 65, 0, 1, 100.0, '혼탁한 대기 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (66, 66, 0, 1, 100.0, '라이트닝 쉴드 Lv3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (67, 67, 0, 1, 100.0, '라이트닝 프로텍트', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (68, 68, 0, 1, 100.0, '라이트닝 아머 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (69, 69, 0, 1, 100.0, '워터 쉴드 Lv3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (70, 70, 0, 1, 100.0, '워터 프로텍트', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (71, 71, 0, 1, 100.0, '워터 아머 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (72, 72, 0, 1, 100.0, '리듀스 대미지 45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (73, 73, 0, 1, 100.0, '리듀스 대미지 45+', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (74, 74, 0, 1, 100.0, '리듀스 대미지 45++', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (75, 75, 0, 1, 100.0, '캔슬 스크롤 D', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (76, 76, 0, 1, 100.0, '캔슬 스크롤 D+', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (77, 77, 0, 1, 100.0, '캔슬 스크롤 D++', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (78, 78, 0, 1, 100.0, '전장의 강화 주문서', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (79, 79, 0, 1, 100.0, '영웅의 강화 주문서', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (80, 80, 0, 1, 100.0, '전설의 강화 주문서', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (81, 81, 1, 1, 100.0, '태양의 아바타 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (82, 82, 1, 1, 100.0, '하늘의 아바타 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (83, 83, 1, 1, 100.0, '대지의 아바타 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (84, 84, 1, 1, 100.0, '대해의 아바타 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (85, 85, 1, 1, 100.0, '(이벤트)R2 고대 글자판', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (86, 86, 1, 1, 100.0, '(할로윈)호박', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (87, 87, 1, 1, 100.0, '(할로윈)눈 달린 호박', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (88, 88, 1, 1, 100.0, '(할로윈)머리 달린 호박', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (89, 89, 1, 1, 100.0, '(할로윈) 할로윈 호박', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (90, 90, 1, 1, 100.0, '(조각이벤트)두번째조각제련', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (91, 91, 1, 1, 30.0, '(조각이벤트)멜론1개제련', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (92, 92, 1, 1, 5.0, '(조각이벤트)방어구강화주문서', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (93, 93, 1, 1, 60.0, '(조각이벤트)두번째조각과무기강화주문서', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (94, 94, 1, 1, 1.0, '(조각이벤트)금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (95, 95, 0, 100, 100.0, '(길드토너먼트보상)레인보우 황금주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (96, 96, 0, 100, 100.0, '(길드토너먼트보상)레인보우 행운 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (97, 97, 1, 1, 100.0, '(이벤트)행운의 퍼니 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (98, 98, 0, 100, 100.0, '(길드토너먼트보상)[빨강색]레인보우 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (99, 99, 0, 100, 100.0, '(길드토너먼트보상)[주황색]레인보우 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (100, 100, 0, 100, 100.0, '(길드토너먼트보상)[노랑색]레인보우 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (101, 101, 0, 100, 100.0, '(길드토너먼트보상)[초록색]레인보우 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (102, 102, 0, 100, 100.0, '(길드토너먼트보상)[파랑색]레인보우 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (103, 103, 0, 100, 100.0, '(길드토너먼트보상)[남색]레인보우 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (104, 104, 0, 100, 100.0, '(길드토너먼트보상)[보라색]레인보우 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (105, 105, 0, 100, 100.0, '행운의 선물 Lv2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (106, 106, 0, 100, 100.0, '행운의 선물 Lv3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (107, 107, 0, 100, 100.0, '행운의 선물 Lv4', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (108, 108, 1, 1, 90.0, '(조각이벤트)세번째조각및웅담제련', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (109, 109, 1, 1, 30.0, '(조각이벤트)달걀', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (110, 110, 1, 1, 100.0, '(조각이벤트)고대글자판조합', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (111, 111, 0, 100, 100.0, '행운의 선물 LV5', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (112, 112, 0, 100, 100.0, '행운의 선물 LV6', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (113, 113, 0, 100, 100.0, '행운의 선물 LV7', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (114, 114, 0, 100, 100.0, '행운의 선물 LV8', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (115, 115, 0, 100, 100.0, '행운의 선물 LV9', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (116, 116, 0, 100, 100.0, '행운의 선물 LV10', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (117, 117, 0, 12, 100.0, '행운의 선물 LV11, (글로벌) 행운의 선물 Lv15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (118, 118, 0, 100, 100.0, '행운의 선물 LV12', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (119, 119, 0, 100, 100.0, '행운의 선물 LV13', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (120, 120, 0, 100, 100.0, '행운의 선물 LV14', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (121, 121, 0, 100, 100.0, '행운의 선물 LV15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (122, 122, 0, 10, 100.0, '행운의 선물 LV16, (글로벌) 행운의 선물 Lv20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (123, 123, 0, 100, 100.0, '행운의 선물 LV17', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (124, 124, 0, 100, 100.0, '행운의 선물 LV18', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (125, 125, 0, 100, 100.0, '행운의 선물 LV19', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (126, 126, 0, 100, 100.0, '행운의 선물 LV20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (127, 127, 0, 10, 100.0, '행운의 선물 LV21, (글로벌) 행운의 선물 Lv25', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (128, 128, 0, 100, 100.0, '행운의 선물 LV22', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (129, 129, 0, 100, 100.0, '행운의 선물 LV23', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (130, 130, 0, 100, 100.0, '행운의 선물 LV24', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (131, 131, 0, 100, 100.0, '행운의 선물 LV25', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (132, 132, 0, 10, 100.0, '행운의 선물 LV26, (글로벌) 행운의 선물 Lv30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (133, 133, 0, 100, 100.0, '행운의 선물 LV27', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (134, 134, 0, 100, 100.0, '행운의 선물 LV28', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (135, 135, 0, 100, 100.0, '행운의 선물 LV29', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (136, 136, 0, 100, 100.0, '행운의 선물 LV30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (137, 137, 0, 10, 100.0, '행운의 선물 LV31', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (138, 138, 0, 100, 100.0, '행운의 선물 LV32', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (139, 139, 0, 100, 100.0, '행운의 선물 LV33', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (140, 140, 0, 100, 100.0, '행운의 선물 LV34', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (141, 141, 0, 100, 100.0, '행운의 선물 LV35', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (142, 142, 0, 10, 100.0, '행운의 선물 LV36', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (143, 143, 0, 100, 100.0, '행운의 선물 LV37', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (144, 144, 0, 100, 100.0, '행운의 선물 LV38', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (145, 145, 0, 100, 100.0, '행운의 선물 LV39', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (146, 146, 0, 100, 100.0, '행운의 선물 LV40', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (147, 147, 0, 10, 100.0, '행운의 선물 LV41', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (148, 148, 0, 100, 100.0, '행운의 선물 LV42', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (149, 149, 0, 100, 100.0, '행운의 선물 LV43', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (150, 150, 0, 100, 100.0, '행운의 선물 LV44', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (151, 151, 0, 100, 100.0, '행운의 선물 LV45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (152, 152, 0, 10, 100.0, '행운의 선물 LV46', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (153, 153, 0, 100, 100.0, '행운의 선물 LV47', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (154, 154, 0, 100, 100.0, '행운의 선물 LV48', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (155, 155, 0, 10, 100.0, '행운의 선물 LV49', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (156, 156, 0, 100, 100.0, '행운의 선물 LV50', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (157, 157, 0, 100, 100.0, '행운의 선물 LV51', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (158, 158, 0, 13, 100.0, '행운의 선물 LV52', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (159, 159, 0, 100, 100.0, '행운의 선물 LV53', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (160, 160, 0, 100, 100.0, '행운의 선물 LV54', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (161, 161, 0, 10, 100.0, '행운의 선물 LV55', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (162, 162, 0, 100, 100.0, '행운의 선물 LV56', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (163, 163, 0, 100, 100.0, '행운의 선물 LV57', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (164, 164, 0, 10, 100.0, '행운의 선물 LV58', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (165, 165, 0, 100, 100.0, '행운의 선물 LV59', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (166, 166, 0, 100, 100.0, '행운의 선물 LV60', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (167, 167, 0, 11, 100.0, '행운의 선물 LV61', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (168, 168, 0, 100, 100.0, '행운의 선물 LV62', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (169, 169, 0, 10, 100.0, '행운의 선물 LV63', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (170, 170, 0, 100, 100.0, '행운의 선물 LV64', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (171, 171, 0, 10, 100.0, '행운의 선물 LV65', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (172, 172, 0, 100, 100.0, '행운의 선물 LV66', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (173, 173, 0, 10, 100.0, '행운의 선물 LV67', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (174, 174, 0, 100, 100.0, '행운의 선물 LV68', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (175, 175, 0, 10, 100.0, '행운의 선물 LV69', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (176, 176, 0, 100, 100.0, '행운의 선물 LV70', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (177, 177, 0, 10, 100.0, '행운의 선물 LV71', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (178, 178, 0, 100, 100.0, '행운의 선물 LV72', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (179, 179, 0, 10, 100.0, '행운의 선물 LV73', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (180, 180, 0, 10, 100.0, '행운의 선물 LV74', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (181, 181, 0, 10, 100.0, '행운의 선물 LV75', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (182, 182, 0, 10, 100.0, '행운의 선물 LV76', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (183, 183, 0, 10, 100.0, '행운의 선물 LV77', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (184, 184, 0, 10, 100.0, '행운의 선물 LV78', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (185, 185, 0, 10, 100.0, '행운의 선물 LV79', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (186, 186, 0, 10, 100.0, '행운의 선물 LV80', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (187, 187, 0, 10, 100.0, '행운의 선물 LV81', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (188, 188, 0, 100, 100.0, '행운의 선물 LV82', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (189, 189, 0, 100, 100.0, '행운의 선물 LV83', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (190, 190, 0, 100, 100.0, '행운의 선물 LV84', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (191, 191, 0, 100, 100.0, '행운의 선물 LV85', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (192, 192, 0, 100, 100.0, '행운의 선물 LV86', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (193, 193, 0, 100, 100.0, '행운의 선물 LV87', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (194, 194, 0, 100, 100.0, '행운의 선물 LV88', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (195, 195, 0, 100, 100.0, '행운의 선물 LV89', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (196, 196, 0, 100, 100.0, '행운의 선물 LV90', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (197, 197, 0, 100, 100.0, '행운의 선물 LV91', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (198, 198, 0, 100, 100.0, '행운의 선물 LV92', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (199, 199, 0, 100, 100.0, '행운의 선물 LV93', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (200, 200, 0, 100, 100.0, '행운의 선물 LV94', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (201, 201, 0, 100, 100.0, '행운의 선물 LV95', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (202, 202, 0, 100, 100.0, '행운의 선물 LV96', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (203, 203, 0, 100, 100.0, '행운의 선물 LV97', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (204, 204, 0, 100, 100.0, '행운의 선물 LV98', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (205, 205, 0, 100, 100.0, '행운의 선물 LV99', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (206, 206, 0, 100, 100.0, '행운의 선물 LV100', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (207, 207, 0, 100, 100.0, '행운의 선물 LV100을 뽑은 결과물', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (208, 208, 1, 1, 100.0, '(할로윈)눈달린 호박 만들기', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (209, 209, 1, 1, 100.0, '(할로윈)머리달린 호박 만들기', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (210, 210, 1, 1, 100.0, '(할로윈)할로윈 호박 만들기', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (211, 211, 0, 100, 100.0, '(길드토너먼트)레인보우 선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (212, 212, 0, 100, 100.0, '[093Q소환몬스터]행운의 무지개 도시락', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (213, 213, 0, 100, 100.0, '[093Q소환몬스터]행운의 최고급 다이아몬드', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (214, 214, 0, 100, 100.0, '[093Q소환몬스터]행운의 황금 망치', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (215, 215, 0, 100, 100.0, '[093Q소환몬스터]행운의 유리 구두', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (216, 216, 0, 100, 100.0, '[093Q소환몬스터]행운의 루비 반지', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (217, 217, 0, 100, 100.0, '[093Q소환몬스터]행운의 블루사파이어 목걸이', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (218, 218, 0, 100, 100.0, '[093Q소환몬스터]행운의 황금알', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (219, 219, 0, 100, 100.0, '[093Q소환몬스터]행운의 무지개 우산', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (220, 220, 1, 1, 100.0, '카오스 배틀 체력 회복 포션 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (221, 221, 1, 1, 100.0, '카오스 배틀 마나 회복 포션 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (222, 222, 1, 1, 100.0, '보물상자 리뉴얼(보물 상자)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (223, 223, 1, 1, 100.0, '황금 보물상자 리뉴얼(황금 보물 상자)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (224, 224, 1, 1, 100.0, '진카사강화(카사로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (225, 225, 1, 1, 100.0, '진실라페강화(실라페로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (226, 226, 1, 1, 100.0, '진노움강화(노움으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (227, 227, 1, 1, 100.0, '진언딘강화(언딘으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (228, 228, 1, 1, 100.0, '사라만다강화(진카사로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (229, 229, 1, 1, 100.0, '실프강화(진실라페로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (230, 230, 1, 1, 100.0, '노임강화(진노움으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (231, 231, 1, 1, 100.0, '운디네강화(진언딘으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (232, 232, 1, 1, 100.0, '진사라만다강화(사라만다로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (233, 233, 1, 1, 100.0, '진실프강화(실프로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (234, 234, 1, 1, 100.0, '진노임강화(노임으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (235, 235, 1, 1, 100.0, '진운디네강화(운디네로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (236, 236, 1, 1, 100.0, '이그니스강화(진사라만다로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (237, 237, 1, 1, 100.0, '실라이론강화(진실프로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (238, 238, 1, 1, 100.0, '노이아넨강화(진노임으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (239, 239, 1, 1, 100.0, '운다임강화(진운디네로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (240, 240, 1, 1, 100.0, '이프리트강화(이그니스로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (241, 241, 1, 1, 100.0, '슈리엘강화(실라이론으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (242, 242, 1, 1, 100.0, '노아스강화(노이아넨으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (243, 243, 1, 1, 100.0, '엘라임강화(운다임으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (244, 244, 1, 1, 100.0, '퓨리강화(이프리트로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (245, 245, 1, 1, 100.0, '레프리컨강화(슈리엘로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (246, 246, 1, 1, 100.0, '시아페강화(노아스로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (247, 247, 1, 1, 100.0, '레이커강화(엘라임으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (248, 248, 1, 1, 100.0, '휠카셀강화(퓨리로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (249, 249, 1, 1, 100.0, '에리스강화(레프리컨으로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (250, 250, 1, 1, 100.0, '릴리언스강화(시아페로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (251, 251, 1, 1, 100.0, '에리세드강화(레이커로)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (252, 252, 0, 3, 100.0, '용암에도 견디는 주문서 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (253, 253, 1, 1, 100.0, '용암에도 견디는 스킬북 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (254, 254, 1, 1, 100.0, '용암에도 견디는 무기 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (255, 255, 1, 1, 100.0, '용암에도 견디는 방어구 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (256, 256, 1, 1, 100.0, '용암에도 견디는 악세서리 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (257, 257, 0, 3, 100.0, '용암에도 견디는 광석 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (258, 258, 1, 1, 100.0, '태양의 아바타 상자 (체험용)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (259, 259, 1, 1, 100.0, '하늘의 아바타 상자 (체험용)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (260, 260, 1, 1, 100.0, '대지의 아바타 상자 (체험용)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (261, 261, 1, 1, 100.0, '대해의 아바타 상자 (체험용)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (262, 262, 1, 1, 100.0, '아바타 강화 상자(1개)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (263, 263, 1, 1, 100.0, '아바타 강화 상자(10개)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (264, 264, 1, 1, 100.0, '경험치 증폭의 룬 상자 (1.5배 6시간)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (265, 265, 1, 1, 100.0, '경험치 증폭의 룬 상자 (1.3배 6시간)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (266, 266, 1, 1, 100.0, '장비 가챠 상자 1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (267, 267, 1, 1, 100.0, '장비 가챠 상자 2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (268, 268, 1, 1, 100.0, '장비 가챠 상자 3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (269, 269, 1, 1, 100.0, '장비 가챠 상자 4', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (270, 270, 1, 1, 100.0, '장비 가챠 상자 5', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (271, 271, 1, 1, 100.0, '장비 가챠 상자 6', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (272, 272, 1, 1, 100.0, '장비 가챠 상자 7', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (273, 273, 1, 1, 100.0, '장비 가챠 상자 8', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (274, 274, 1, 1, 100.0, '장비 가챠 상자 9', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (275, 275, 1, 1, 100.0, '장비 가챠 상자 10', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (276, 276, 1, 1, 100.0, '장비 가챠 상자 11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (277, 277, 1, 1, 100.0, '장비 가챠 상자 12', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (278, 278, 1, 1, 100.0, '장비 가챠 상자 13', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (279, 279, 1, 1, 100.0, '장비 가챠 상자 14', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (280, 280, 1, 1, 100.0, '장비 가챠 상자 15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (281, 281, 1, 1, 100.0, '장비 가챠 상자 16', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (282, 282, 1, 1, 100.0, '장비 가챠 상자 17', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (283, 283, 1, 1, 100.0, '장비 가챠 상자 18', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (284, 284, 1, 1, 100.0, '장비 가챠 상자 19', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (285, 285, 1, 1, 100.0, '장비 가챠 상자 20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (286, 286, 1, 1, 100.0, '산타의선물상자(초급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (287, 287, 1, 1, 100.0, '산타의선물상자(중급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (288, 288, 1, 1, 100.0, '산타의선물상자(상급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (289, 289, 0, 2, 100.0, '카야가 전해준 여비 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (290, 290, 1, 1, 100.0, '카노의 마음이 담긴 선물 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (291, 291, 1, 1, 100.0, '제인의 마음이 담긴 선물 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (292, 292, 1, 1, 100.0, '카야의 마음이 담긴 선물 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (293, 293, 1, 1, 100.0, '작은 복 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (294, 294, 1, 1, 100.0, '제인의 약간 큰 복 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (295, 295, 1, 1, 100.0, '제인의 아주 큰 복 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (296, 296, 1, 1, 100.0, '카노의 약간 큰 복 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (297, 297, 1, 1, 100.0, '카노의 아주 큰 복 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (298, 298, 1, 1, 100.0, '카야의 마음이 담긴 복 주머니', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (299, 299, 1, 1, 100.0, '카오스 보급 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (300, 300, 1, 1, 100.0, '프리미엄 길드 모집 티켓(7일) 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (301, 301, 1, 1, 35.0, '특수 변신 막대', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (302, 302, 1, 1, 100.0, '발렌타인 데이 선물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (303, 303, 1, 1, 50.0, '[조합]아름다운 꽃다발', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (304, 304, 1, 1, 10.0, '[조합]세련된 꽃다발', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (305, 305, 1, 1, 100.0, '[포장지]아름다운 꽃다발 포장지', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (306, 306, 1, 1, 100.0, '[포장지]세련된 꽃다발', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (307, 307, 1, 1, 100.0, '정복의상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (308, 308, 0, 1, 100.0, '제우스의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (309, 309, 0, 1, 100.0, '헤라의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (310, 310, 0, 1, 100.0, '포세이돈의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (311, 311, 0, 1, 100.0, '데메테르의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (312, 312, 0, 1, 100.0, '아테나의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (313, 313, 0, 1, 100.0, '아폴론의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (314, 314, 0, 1, 100.0, '아르테미스의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (315, 315, 0, 1, 100.0, '아레스의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (316, 316, 0, 1, 100.0, '헤파이스토스의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (317, 317, 0, 1, 100.0, '아프로디테의 황금상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (318, 318, 0, 1, 100.0, '환상의 선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (319, 319, 0, 1, 100.0, '축구공 선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (320, 320, 1, 1, 100.0, '풍만한 종자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (321, 321, 1, 1, 100.0, '불풍만한 종자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (322, 322, 1, 1, 100.0, '단오 참석권', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (323, 323, 1, 1, 100.0, '고기종자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (324, 324, 0, 1, 100.0, '축복의 강화 메터리얼 조각', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (325, 325, 1, 1, 100.0, '혼돈의 무기 상자[나이트용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (326, 326, 1, 1, 100.0, '혼돈의 무기 상자[엘프용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (327, 327, 1, 1, 100.0, '혼돈의 무기 상자[레인저용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (328, 328, 1, 1, 100.0, '혼돈의 무기 상자[어쌔신용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (329, 329, 1, 1, 100.0, '혼돈의 무기 상자[서모너용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (330, 330, 1, 1, 100.0, '혼돈의 투구 상자[나이트용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (331, 331, 1, 1, 100.0, '혼돈의 투구 상자[엘프/레인저/서모너용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (332, 332, 1, 1, 100.0, '혼돈의 마스크 상자[어쌔신용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (333, 333, 1, 1, 100.0, '혼돈의 중갑 상자[나이트용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (334, 334, 1, 1, 100.0, '혼돈의 경갑 상자[엘프/레인저/서모너용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (335, 335, 1, 1, 100.0, '혼돈의 메일 상자[어쌔신용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (336, 336, 1, 1, 100.0, '혼돈의 장갑 상자[나이트용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (337, 337, 1, 1, 100.0, '혼돈의 장갑 상자[엘프/레인저/서모너용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (338, 338, 1, 1, 100.0, '혼돈의 글러브 상자[어쌔신용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (339, 339, 1, 1, 100.0, '혼돈의 부츠 상자[나이트용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (340, 340, 1, 1, 100.0, '혼돈의 부츠 상자[엘프/레인저/서모너용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (341, 341, 1, 1, 100.0, '혼돈의 부츠 상자[어쎄신용]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (342, 342, 1, 1, 100.0, 'R2 더블카드', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (343, 343, 1, 1, 100.0, '글레디에이터의 상자(무기)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (344, 344, 1, 1, 100.0, '노련한 글레디에이터의 상자(무기)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (345, 345, 1, 1, 100.0, '신성한 무기 가루 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (346, 346, 1, 1, 100.0, '글레디에이터의 상자(방어구)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (347, 347, 1, 1, 100.0, '노련한 글레디에이터의 상자(방어구)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (348, 348, 1, 1, 100.0, '경험치의 룬 상자[59레벨]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (349, 349, 1, 1, 100.0, '경험치의 룬 상자[70레벨]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (350, 350, 1, 1, 100.0, '경험치의 룬 상자[80레벨]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (351, 351, 1, 1, 100.0, '바포메트의 상자(무기)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (352, 352, 1, 1, 100.0, '바포메트의 상자(방어구)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (353, 353, 1, 1, 100.0, '바포메트의 상자(전리품)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (354, 354, 1, 1, 100.0, '바포메트의 상자(주문서)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (355, 355, 1, 1, 100.0, '혼돈의 망토 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (356, 356, 1, 1, 100.0, '혼돈의 반지 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (357, 357, 1, 1, 100.0, '혼돈의 목걸이 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (358, 358, 1, 1, 100.0, '혼돈의 벨트 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (359, 359, 1, 1, 100.0, '혼돈의 대형방패 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (360, 360, 1, 1, 100.0, '혼돈의 중형방패 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (361, 361, 1, 1, 100.0, '혼돈의 강철총알 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (362, 362, 1, 1, 100.0, '혼돈의 마법가루 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (363, 363, 1, 1, 100.0, '혼돈의 팔찌 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (364, 364, 1, 1, 100.0, '조합구슬[고급]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (365, 365, 1, 1, 100.0, '일본 목걸이 가챠 상자 I', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (366, 366, 1, 1, 100.0, '일본 목걸이 가챠 상자 II', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (367, 367, 1, 1, 100.0, '일본 목걸이 가챠 상자 III', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (368, 368, 1, 1, 100.0, '일본 목걸이 가챠 상자 IV', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (369, 369, 1, 1, 100.0, '일본 목걸이 가챠 상자 V ', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (370, 370, 1, 1, 100.0, '일본 목걸이 가챠 상자 VI', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (371, 371, 1, 1, 100.0, '프리미엄 위탁권 세트', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (372, 372, 1, 1, 100.0, '혼돈의 변신 스크롤 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (373, 373, 1, 1, 100.0, '벼락 행운 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (374, 374, 1, 1, 100.0, '무기용 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (375, 375, 1, 1, 100.0, '투구용 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (376, 376, 1, 1, 100.0, '갑옷용 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (377, 377, 1, 1, 100.0, '장갑용 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (378, 378, 1, 1, 100.0, '신발용 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (379, 379, 1, 1, 100.0, '망토용 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (380, 380, 1, 1, 100.0, '보조장비용 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (381, 381, 1, 1, 100.0, '+0 달빛 서린 마력의 검', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (382, 382, 1, 1, 100.0, '+0 라르카 명중의 검', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (383, 383, 1, 1, 100.0, '+0 만월의 방패', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (384, 384, 1, 1, 100.0, '+0 안식의 망토', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (385, 385, 1, 1, 100.0, '슬롯 제거 연금서 상자[10개]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (386, 386, 1, 1, 100.0, '룬 환원 연금서 상자[10개]', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (387, 387, 1, 1, 100.0, '무기용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (388, 388, 1, 1, 100.0, '무기용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (389, 389, 1, 1, 100.0, '투구용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (390, 390, 1, 1, 100.0, '투구용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (391, 391, 1, 1, 100.0, '갑옷용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (392, 392, 1, 1, 100.0, '갑옷용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (393, 393, 1, 1, 100.0, '장갑용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (394, 394, 1, 1, 100.0, '장갑용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (395, 395, 1, 1, 100.0, '신발용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (396, 396, 1, 1, 100.0, '신발용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (397, 397, 1, 1, 100.0, '망토용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (398, 398, 1, 1, 100.0, '망토용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (399, 399, 1, 1, 100.0, '보조장비용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (400, 400, 1, 1, 100.0, '보조장비용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (401, 401, 1, 1, 100.0, '경험치 증가 아이템상자(중급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (402, 402, 1, 1, 100.0, '경험치 증가 아이템상자(고급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (403, 403, 1, 1, 100.0, '행운의 매터리얼 상자(중급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (404, 404, 1, 1, 100.0, '행운의 매터리얼 상자(고급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (405, 405, 1, 1, 100.0, '원소의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (406, 406, 1, 1, 100.0, '철의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (407, 407, 1, 1, 100.0, '무기 강화 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (408, 408, 1, 1, 100.0, '방어구 강화 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (409, 409, 1, 1, 100.0, '선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (410, 410, 1, 1, 100.0, '선물꾸러미', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (411, 411, 1, 1, 100.0, '아스타로드의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (412, 412, 1, 1, 100.0, '루시퍼의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (413, 413, 1, 1, 100.0, '스펙터의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (414, 414, 1, 1, 100.0, '아타락시아의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (415, 415, 1, 1, 100.0, '우루크하이의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (416, 416, 1, 1, 100.0, '벨제뷔트의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (417, 417, 1, 1, 100.0, '이프리트의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (418, 418, 1, 1, 100.0, '블랙 와이번의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (419, 419, 1, 1, 100.0, '헬게이트 가디언의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (420, 420, 1, 1, 100.0, '화타포스의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (421, 421, 1, 1, 100.0, '고리쉬 퀸의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (422, 422, 1, 1, 100.0, '리치 킹의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (423, 423, 1, 1, 100.0, '파계 제천대성의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (424, 424, 1, 1, 100.0, '로드 오브 아크리언의 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (425, 425, 1, 1, 100.0, '드라코 반지용 중급 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (426, 426, 1, 1, 100.0, '드라코 반지용 고급 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (427, 427, 1, 1, 100.0, '변신 목걸이용 중급 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (428, 428, 1, 1, 100.0, '변신 목걸이용 고급 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (429, 429, 1, 1, 100.0, '반지용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (430, 430, 1, 1, 100.0, '반지용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (431, 431, 1, 1, 100.0, '목걸이용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (432, 432, 1, 1, 100.0, '목걸이용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (433, 433, 1, 1, 100.0, '벨트용 중급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (434, 434, 1, 1, 100.0, '벨트용 고급 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (435, 435, 1, 1, 100.0, '변신목걸이용 특수 룬 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (436, 436, 1, 1, 100.0, '우세의 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (437, 437, 1, 1, 100.0, '묵직한 선물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (438, 438, 1, 1, 100.0, '가벼운 선물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (439, 439, 1, 1, 100.0, '뜯겨진 선물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (440, 440, 1, 1, 100.0, '무기용 프리미엄 룬 상자(RU) ', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (441, 441, 1, 1, 100.0, '갑옷용 프리미엄 룬 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (442, 442, 1, 1, 100.0, '투구용 프리미엄 룬 상자(RU', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (443, 443, 1, 1, 100.0, '장갑용 프리미엄 룬 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (444, 444, 1, 1, 100.0, '신발용 프리미엄 룬 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (445, 445, 1, 1, 100.0, '망토용 프리미엄 룬 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (446, 446, 1, 1, 100.0, '보조장비용 프리미엄 룬 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (447, 447, 1, 1, 100.0, '하급 퀘스트 선물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (448, 448, 1, 1, 100.0, '중급 퀘스트 선물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (449, 449, 1, 1, 100.0, '상급 퀘스트 선물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (450, 450, 0, 2, 100.0, '지배의상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (451, 451, 1, 1, 100.0, '하급 의뢰 보상 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (452, 452, 1, 1, 100.0, '중급 의뢰 보상 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (453, 453, 1, 1, 100.0, '상급 의뢰 보상 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (454, 454, 1, 1, 100.0, '변신 목걸이용 중급 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (455, 455, 1, 1, 100.0, '변신 목걸이용 고급 상자(RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (456, 456, 0, 6, 100.0, '하급 의뢰 보상 상자2 (RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (457, 457, 0, 6, 100.0, '중급 의뢰 보상 상자2 (RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (458, 458, 0, 6, 100.0, '상급 의뢰 보상 상자2 (RU)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (459, 459, 1, 1, 100.0, '하급 카오스 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (460, 460, 1, 1, 100.0, '중급 카오스 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (461, 461, 1, 1, 100.0, '고급 카오스 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (462, 462, 1, 1, 100.0, '하급 큐브 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (463, 463, 1, 1, 100.0, '중급 큐브 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (464, 464, 1, 1, 100.0, '고급 큐브 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (465, 465, 1, 1, 100.0, '최고급 큐브 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (466, 466, 0, 3, 100.0, '[RU] 유료 상자1 (30일 세트)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (467, 467, 0, 3, 100.0, '[RU] 유료 상자1 (7일 세트)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (468, 468, 0, 6, 100.0, '[RU] 유료 상자2 (30일 세트)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (469, 469, 0, 6, 100.0, '[RU] 유료 상자2 (7일 세트)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (470, 470, 0, 14, 100.0, '[RU] 이크론상자 세트(59렙이하)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (471, 471, 0, 9, 100.0, '[RU] 이크론상자 세트(40렙이상)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (472, 472, 1, 1, 100.0, '[RU] 신성 참 세트', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (473, 473, 1, 1, 100.0, '행운의 매터리얼 상자 중급 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (474, 474, 1, 1, 100.0, '행운의 매터리얼 상자 고급 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (475, 475, 1, 1, 100.0, '행운의 매터리얼 상자 중급 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (476, 476, 1, 1, 100.0, '행운의 매터리얼 상자 고급 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (477, 477, 1, 1, 100.0, '변신강화 매터리얼 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (478, 478, 1, 1, 100.0, '변신강화 매터리얼 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (479, 479, 1, 1, 100.0, '영웅의 힘 매터리얼 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (480, 480, 1, 1, 100.0, '영웅의 힘 매터리얼 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (481, 481, 1, 1, 100.0, '헌터의 민첩 매터리얼 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (482, 482, 1, 1, 100.0, '헌터의 민첩 매터리얼 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (483, 483, 1, 1, 100.0, '현자의 지혜 매터리얼 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (484, 484, 1, 1, 100.0, '현자의 지혜 매터리얼 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (485, 485, 0, 5, 100.0, '파괴자의 매터리얼 상자 (이벤트)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (486, 486, 0, 3, 100.0, '고급 보급 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (487, 487, 0, 2, 100.0, '천연 보급 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (488, 488, 0, 3, 100.0, '고급 폭죽 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (489, 489, 1, 1, 100.0, '장비 수호신 보물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (490, 490, 1, 1, 100.0, '강화 수호신 보물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (508, 508, 0, 9, 100.0, '변신목걸이룬 Ⅲ 종합 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (509, 509, 0, 7, 100.0, '변신목걸이룬 Ⅳ 종합 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (510, 510, 0, 9, 100.0, '변신목걸이룬 V 종합 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (511, 511, 0, 7, 100.0, '드라코반지룬 Ⅳ 종합 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (512, 512, 0, 7, 100.0, '드라코반지룬 V 종합 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (513, 513, 0, 4, 100.0, '벨트 룬 Ⅱ 공격 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (514, 514, 0, 5, 100.0, '벨트 룬 Ⅱ 수렵 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (515, 515, 0, 4, 100.0, '벨트 룬 Ⅲ 공격 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (516, 516, 0, 5, 100.0, '벨트 룬 Ⅲ 수렵 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (517, 517, 0, 4, 100.0, '반지 룬 Ⅱ 공격 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (518, 518, 0, 4, 100.0, '반지 룬 Ⅱ 크리티컬 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (519, 519, 0, 6, 100.0, '반지 룬 Ⅱ 수렵 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (520, 520, 0, 4, 100.0, '반지 룬 Ⅲ 공격 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (521, 521, 0, 4, 100.0, '반지 룬 Ⅲ 크리티컬 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (522, 522, 0, 6, 100.0, '반지 룬 Ⅲ 수렵 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (523, 523, 0, 8, 100.0, '나이트 컴백 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (524, 524, 0, 8, 100.0, '레인저 컴백 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (525, 525, 0, 8, 100.0, '어쌔신 컴백 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (526, 526, 0, 8, 100.0, '엘프 컴백 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (527, 527, 0, 8, 100.0, '서모너 컴백 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (528, 528, 1, 1, 100.0, '요스트의 선물', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (529, 529, 1, 1, 100.0, '프리미엄 보물 상자 1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (530, 530, 1, 1, 100.0, '프리미엄 보물 상자 2', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (531, 531, 1, 1, 100.0, '프리미엄 보물 상자 3', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (532, 532, 1, 1, 100.0, '프리미엄 보물 상자 4', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (533, 533, 1, 1, 100.0, '프리미엄 보물 상자 5', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (534, 534, 1, 1, 100.0, '프리미엄 보물 상자 6', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (535, 535, 1, 1, 100.0, '프리미엄 보물 상자 7', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (536, 536, 1, 1, 100.0, '프리미엄 보물 상자 8', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (537, 537, 1, 1, 100.0, '프리미엄 보물 상자 9', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (538, 538, 1, 1, 100.0, '프리미엄 보물 상자 10', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (539, 539, 1, 1, 100.0, '프리미엄 보물 상자 11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (540, 540, 1, 1, 100.0, '프리미엄 보물 상자 12', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (541, 541, 1, 1, 100.0, '프리미엄 보물 상자 13', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (542, 542, 1, 1, 100.0, '프리미엄 보물 상자 14', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (543, 543, 1, 1, 100.0, '프리미엄 보물 상자 15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (544, 544, 1, 1, 100.0, '프리미엄 보물 상자 16', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (545, 545, 1, 1, 100.0, '프리미엄 보물 상자 17', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (546, 546, 1, 1, 100.0, '프리미엄 보물 상자 18', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (547, 547, 1, 1, 100.0, '프리미엄 보물 상자 19', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (548, 548, 1, 1, 100.0, '프리미엄 보물 상자 20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (549, 549, 1, 1, 100.0, '프리미엄 보물 상자 21', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (550, 550, 1, 1, 100.0, '프리미엄 보물 상자 22', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (551, 551, 1, 1, 100.0, '프리미엄 보물 상자 23', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (552, 552, 1, 1, 100.0, '프리미엄 보물 상자 24', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (553, 553, 1, 1, 100.0, '프리미엄 보물 상자 25', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (554, 554, 1, 1, 100.0, '프리미엄 보물 상자 26', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (555, 555, 1, 1, 100.0, '프리미엄 보물 상자 27', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (556, 556, 1, 1, 100.0, '프리미엄 보물 상자 28', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (557, 557, 1, 1, 100.0, '프리미엄 보물 상자 29', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (558, 558, 1, 1, 100.0, '프리미엄 보물 상자 30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (559, 559, 1, 1, 100.0, '프리미엄 보물 상자 31', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (560, 560, 1, 1, 100.0, '프리미엄 보물 상자 32', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (561, 561, 1, 1, 100.0, '프리미엄 보물 상자 33', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (562, 562, 0, 6, 100.0, '나이트 보급상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (563, 563, 0, 6, 100.0, '어쌔신 보급상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (564, 564, 0, 6, 100.0, '레인저 보급상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (565, 565, 0, 6, 100.0, '엘프 보급상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (566, 566, 0, 6, 100.0, '서모너 보급상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (567, 567, 0, 7, 100.0, '나이트 보급상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (568, 568, 0, 8, 100.0, '어쌔신 보급상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (569, 569, 0, 7, 100.0, '레인저 보급상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (570, 570, 0, 7, 100.0, '엘프 보급상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (571, 571, 0, 8, 100.0, '서모너 보급상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (572, 572, 0, 4, 100.0, '보급상자 Lv20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (573, 573, 0, 4, 100.0, '보급상자 Lv25', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (574, 574, 0, 5, 100.0, '보급상자 Lv30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (575, 575, 0, 4, 100.0, '보급상자 Lv35', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (576, 576, 0, 9, 100.0, '보급상자 Lv40', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (577, 577, 0, 4, 100.0, '보급상자 Lv45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (578, 578, 0, 4, 100.0, '보급상자 Lv50', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (579, 579, 0, 4, 100.0, '보급상자 Lv55', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (580, 580, 0, 6, 100.0, '보급상자 Lv60', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (581, 581, 0, 4, 100.0, '보급상자 Lv62', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (582, 582, 0, 4, 100.0, '보급상자 Lv64', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (583, 583, 0, 4, 100.0, '보급상자 Lv66', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (584, 584, 0, 4, 100.0, '보급상자 Lv68', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (585, 585, 0, 4, 100.0, '보급상자 Lv70', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (586, 586, 1, 1, 100.0, '카오스 크라입텍스 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (587, 587, 0, 4, 100.0, '정복 드라코 상자 (7일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (588, 588, 0, 4, 100.0, '정복 드라코 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (589, 589, 0, 4, 100.0, '1+1 배틀 드라코 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (590, 590, 1, 1, 100.0, '배틀 드라코 행운 상자 (30일)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (591, 591, 1, 1, 100.0, '[RU] 카오스 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (592, 592, 1, 1, 100.0, '[RU] R2 더블 카드', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (593, 593, 1, 1, 100.0, '블랙 드래곤의 액세서리 상자(중급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (594, 594, 1, 1, 100.0, '[RU] 드라코 랜덤 박스', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (595, 595, 1, 1, 100.0, '[RU] 메테리얼 랜덤 박스', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (596, 596, 1, 1, 100.0, '[RU] 룬 랜덤 박스', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (597, 597, 0, 5, 100.0, '[RU] 나이트 레벨업 상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (598, 598, 0, 5, 100.0, '[RU] 어쌔신 레벨업 상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (599, 599, 0, 5, 100.0, '[RU] 레인저 레벨업 상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (600, 600, 0, 5, 100.0, '[RU] 엘프 레벨업 상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (601, 601, 0, 5, 100.0, '[RU] 서모너 레벨업 상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (602, 602, 0, 7, 100.0, '[RU] 나이트 레벨업 상자 Lv10', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (603, 603, 0, 7, 100.0, '[RU] 어쌔신 레벨업 상자 Lv10', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (604, 604, 0, 7, 100.0, '[RU] 레인저 레벨업 상자 Lv10', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (605, 605, 0, 7, 100.0, '[RU] 엘프 레벨업 상자 Lv10', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (606, 606, 0, 7, 100.0, '[RU] 서모너 레벨업 상자 Lv10', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (607, 607, 0, 10, 100.0, '[RU] 나이트 레벨업 상자 Lv15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (608, 608, 0, 10, 100.0, '[RU] 어쌔신 레벨업 상자 Lv15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (609, 609, 0, 10, 100.0, '[RU] 레인저 레벨업 상자 Lv15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (610, 610, 0, 10, 100.0, '[RU] 엘프 레벨업 상자 Lv15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (611, 611, 0, 10, 100.0, '[RU] 서모너 레벨업 상자 Lv15', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (612, 612, 0, 6, 100.0, '[RU] 나이트 레벨업 상자 Lv20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (613, 613, 0, 6, 100.0, '[RU] 어쌔신 레벨업 상자 Lv20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (614, 614, 0, 6, 100.0, '[RU] 레인저 레벨업 상자 Lv20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (615, 615, 0, 6, 100.0, '[RU] 엘프 레벨업 상자 Lv20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (616, 616, 0, 6, 100.0, '[RU] 서모너 레벨업 상자 Lv20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (617, 617, 0, 4, 100.0, '[RU] 나이트 레벨업 상자 Lv30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (618, 618, 0, 4, 100.0, '[RU] 어쌔신 레벨업 상자 Lv30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (619, 619, 0, 4, 100.0, '[RU] 레인저 레벨업 상자 Lv30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (620, 620, 0, 4, 100.0, '[RU] 엘프 레벨업 상자 Lv30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (621, 621, 0, 4, 100.0, '[RU] 서모너 레벨업 상자 Lv30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (622, 622, 0, 9, 100.0, '[RU] 나이트 레벨업 상자 Lv40', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (623, 623, 0, 9, 100.0, '[RU] 어쌔신 레벨업 상자 Lv40', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (624, 624, 0, 9, 100.0, '[RU]  레인저 레벨업 상자 Lv40', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (625, 625, 0, 9, 100.0, '[RU] 엘프 레벨업 상자 Lv40', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (626, 626, 0, 9, 100.0, '[RU] 서모너 레벨업 상자 Lv40', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (627, 627, 0, 5, 100.0, '[RU] 나이트 레벨업 상자 Lv45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (628, 628, 0, 5, 100.0, '[RU] 어쌔신 레벨업 상자 Lv45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (629, 629, 0, 5, 100.0, '[RU] 레인저 레벨업 상자 Lv45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (630, 630, 0, 5, 100.0, '[RU] 엘프 레벨업 상자 Lv45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (631, 631, 0, 5, 100.0, '[RU] 서모너 레벨업 상자 Lv45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (632, 632, 0, 5, 100.0, '[RU] 나이트 레벨업 상자 Lv50', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (633, 633, 0, 5, 100.0, '[RU] 어쌔신 레벨업 상자 Lv50', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (634, 634, 0, 5, 100.0, '[RU] 레인저 레벨업 상자 Lv50', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (635, 635, 0, 5, 100.0, '[RU] 엘프 레벨업 상자 Lv50', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (636, 636, 0, 5, 100.0, '[RU] 서모너 레벨업 상자 Lv50', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (637, 637, 0, 4, 100.0, '[RU] 나이트 레벨업 상자 Lv54', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (638, 638, 0, 4, 100.0, '[RU] 어쌔신 레벨업 상자 Lv54', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (639, 639, 0, 4, 100.0, '[RU] 레인저 레벨업 상자 Lv54', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (640, 640, 0, 4, 100.0, '[RU] 엘프 레벨업 상자 Lv54', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (641, 641, 0, 4, 100.0, '[RU] 서모너 레벨업 상자 Lv54', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (642, 642, 0, 5, 100.0, '[RU] 나이트 레벨업 상자 Lv60', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (643, 643, 0, 5, 100.0, '[RU] 어쌔신 레벨업 상자 Lv60', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (644, 644, 0, 5, 100.0, '[RU] 레인저 레벨업 상자 Lv60', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (645, 645, 0, 5, 100.0, '[RU] 엘프 레벨업 상자 Lv60', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (646, 646, 0, 5, 100.0, '[RU] 서모너 레벨업 상자 Lv60', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (647, 647, 0, 5, 100.0, '[RU] 나이트 레벨업 상자 Lv62', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (648, 648, 0, 5, 100.0, '[RU] 어쌔신 레벨업 상자 Lv62', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (649, 649, 0, 5, 100.0, '[RU] 레인저 레벨업 상자 Lv62', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (650, 650, 0, 5, 100.0, '[RU] 엘프 레벨업 상자 Lv62', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (651, 651, 0, 5, 100.0, '[RU] 서모너 레벨업 상자 Lv62', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (652, 652, 0, 5, 100.0, '[RU] 나이트 레벨업 상자 Lv64', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (653, 653, 0, 5, 100.0, '[RU] 어쌔신 레벨업 상자 Lv64', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (654, 654, 0, 5, 100.0, '[RU] 레인저 레벨업 상자 Lv64', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (655, 655, 0, 5, 100.0, '[RU] 엘프 레벨업 상자 Lv64', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (656, 656, 0, 5, 100.0, '[RU] 서모너 레벨업 상자 Lv64', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (657, 657, 0, 5, 100.0, '[RU] 나이트 레벨업 상자 Lv66', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (658, 658, 0, 5, 100.0, '[RU] 어쌔신 레벨업 상자 Lv66', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (659, 659, 0, 5, 100.0, '[RU] 레인저 레벨업 상자 Lv66', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (660, 660, 0, 5, 100.0, '[RU] 엘프 레벨업 상자 Lv66', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (661, 661, 0, 5, 100.0, '[RU] 서모너 레벨업 상자 Lv66', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (662, 662, 0, 5, 100.0, '[RU] 나이트 레벨업 상자 Lv68', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (663, 663, 0, 5, 100.0, '[RU] 어쌔신 레벨업 상자 Lv68', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (664, 664, 0, 5, 100.0, '[RU] 레인저 레벨업 상자 Lv68', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (665, 665, 0, 5, 100.0, '[RU] 엘프 레벨업 상자 Lv68', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (666, 666, 0, 5, 100.0, '[RU] 서모너 레벨업 상자 Lv68', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (667, 667, 0, 6, 100.0, '[RU] 나이트 레벨업 상자 Lv69', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (668, 668, 0, 6, 100.0, '[RU] 어쌔신 레벨업 상자 Lv69', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (669, 669, 0, 6, 100.0, '[RU] 레인저 레벨업 상자 Lv69', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (670, 670, 0, 6, 100.0, '[RU] 엘프 레벨업 상자 Lv69', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (671, 671, 0, 6, 100.0, '[RU] 서모너 레벨업 상자 Lv69', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (672, 672, 0, 1, 100.0, '변신 스킬북 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (673, 673, 0, 5, 100.0, '보급상자 Lv71', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (674, 674, 0, 6, 100.0, '보급상자 Lv72', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (675, 675, 0, 5, 100.0, '보급상자 Lv73', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (676, 676, 0, 5, 100.0, '보급상자 Lv74', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (677, 677, 0, 5, 100.0, '보급상자 Lv75', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (678, 678, 0, 5, 100.0, '보급상자 Lv76', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (679, 679, 0, 6, 100.0, '보급상자 Lv77', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (680, 680, 0, 6, 100.0, '보급상자 Lv78', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (681, 681, 0, 5, 100.0, '보급상자 Lv79', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (682, 682, 0, 6, 100.0, '보급상자 Lv80', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (683, 683, 0, 5, 100.0, '보급상자 Lv81', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (684, 684, 0, 10, 100.0, '레벨업 상자 Lv20', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (685, 685, 0, 10, 100.0, '레벨업 상자 Lv25', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (686, 686, 0, 10, 100.0, '레벨업 상자 Lv30', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (687, 687, 0, 10, 100.0, '레벨업 상자 Lv40', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (688, 688, 0, 10, 100.0, '레벨업 상자 Lv45', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (689, 689, 0, 10, 100.0, '레벨업 상자 Lv50', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (690, 690, 0, 10, 100.0, '레벨업 상자 Lv55', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (691, 691, 0, 10, 100.0, '레벨업 상자 Lv60', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (692, 692, 0, 10, 100.0, '레벨업 상자 Lv62', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (693, 693, 0, 10, 100.0, '레벨업 상자 Lv64', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (694, 694, 0, 10, 100.0, '레벨업 상자 Lv66', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (695, 695, 0, 10, 100.0, '레벨업 상자 Lv68', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (696, 696, 0, 10, 100.0, '레벨업 상자 Lv70', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (697, 697, 0, 10, 100.0, '레벨업 상자 Lv71', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (698, 698, 0, 10, 100.0, '레벨업 상자 Lv72', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (699, 699, 0, 10, 100.0, '레벨업 상자 Lv73', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (700, 700, 0, 10, 100.0, '레벨업 상자 Lv74', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (701, 701, 0, 10, 100.0, '레벨업 상자 Lv75', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (702, 702, 0, 10, 100.0, '레벨업 상자 Lv76', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (703, 703, 0, 10, 100.0, '레벨업 상자 Lv77', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (704, 704, 0, 10, 100.0, '레벨업 상자 Lv78', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (705, 705, 0, 10, 100.0, '레벨업 상자 Lv79', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (706, 706, 0, 10, 100.0, '레벨업 상자 Lv80', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (707, 707, 1, 1, 100.0, '바포메트의 상자(악세서리)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (708, 708, 0, 5, 100.0, '이프리트의 보물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (709, 709, 0, 5, 100.0, '바포메트의 보물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (710, 710, 0, 5, 100.0, '메테오스의 보물 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (711, 711, 0, 4, 100.0, '이계의 무기 상자(고급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (712, 712, 0, 4, 100.0, '이계의 방어구 상자(고급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (713, 713, 0, 4, 100.0, '이계의 악세사리 상자(고급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (714, 714, 0, 3, 100.0, '이계의 무기 상자(중급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (715, 715, 0, 3, 100.0, '이계의 방어구 상자(중급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (716, 716, 0, 3, 100.0, '이계의 악세사리 상자(중급)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (717, 717, 1, 1, 100.0, '메테오스의 상자(무기)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (718, 718, 1, 1, 100.0, '메테오스의 상자(방어구)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (719, 719, 1, 1, 100.0, '메테오스의 상자(주문서)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (720, 720, 1, 1, 100.0, '메테오스의 상자(전리품)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (721, 721, 0, 10, 100.0, '레벨업 상자 Lv35', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (722, 722, 0, 6, 100.0, '보급상자 Lv1', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (723, 723, 0, 6, 100.0, '나이트(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (724, 724, 0, 6, 100.0, '나이트(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (725, 725, 0, 6, 100.0, '어쌔신(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (726, 726, 0, 6, 100.0, '어쌔신(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (727, 727, 0, 6, 100.0, '레인저(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (728, 728, 0, 6, 100.0, '레인저(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (729, 729, 0, 6, 100.0, '서모너(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (730, 730, 0, 6, 100.0, '서모너(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (731, 731, 0, 6, 100.0, '엘프(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (732, 732, 0, 6, 100.0, '엘프(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (733, 733, 0, 6, 100.0, '보급상자 Lv82', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (734, 734, 0, 7, 100.0, '보급상자 Lv83', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (735, 735, 0, 6, 100.0, '보급상자 Lv84', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (736, 736, 0, 6, 100.0, '보급상자 Lv85', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (737, 737, 0, 8, 100.0, '보급상자 Lv86', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (738, 738, 0, 6, 100.0, '보급상자 Lv87', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (739, 739, 0, 7, 100.0, '보급상자 Lv88', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (740, 740, 0, 9, 100.0, '보급상자 Lv89', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (741, 741, 0, 5, 100.0, '보급상자 Lv90', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (742, 742, 0, 4, 100.0, '지존 신비상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (743, 743, 1, 1, 100.0, '변신 스킬북 상자 (II)', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (744, 744, 1, 1, 100.0, '영혼의 시험장 물품 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (745, 745, 1, 1, 100.0, '영혼의 시험장 방어구 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (746, 746, 1, 1, 100.0, '영혼의 시험장 무기 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (747, 747, 0, 2, 100.0, '보급상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (748, 748, 2, 1, 100.0, '10주년 이벤트 나이트 선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (749, 749, 2, 1, 100.0, '10주년 이벤트 레인저 선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (750, 750, 2, 1, 100.0, '10주년 이벤트 엘프 선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (751, 751, 2, 1, 100.0, '10주년 이벤트 어쌔신 선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (752, 752, 2, 1, 100.0, '10주년 이벤트 서모너 선물상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (753, 753, 1, 1, 100.0, '3등급 아레나 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (754, 754, 1, 2, 100.0, '2등급 아레나 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (755, 755, 1, 2, 100.0, '1등급 아레나 상자', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (756, 756, 0, 6, 100.0, '나이트(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (757, 757, 0, 6, 100.0, '나이트(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (758, 758, 0, 6, 100.0, '어쌔신(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (759, 759, 0, 6, 100.0, '어쌔신(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (760, 760, 0, 6, 100.0, '레인저(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (761, 761, 0, 6, 100.0, '레인저(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (762, 762, 0, 6, 100.0, '서모너(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (763, 763, 0, 6, 100.0, '서모너(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (764, 764, 0, 6, 100.0, '엘프(I) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (765, 765, 0, 6, 100.0, '엘프(II) 장비상자 Lv11', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (766, 766, 2, 1, 100.0, '빨간색 채집 상자 A', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (767, 767, 2, 1, 100.0, '빨간색 채집 상자 B', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (768, 768, 2, 1, 100.0, '파란색 채집 상자 A', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (769, 769, 2, 1, 100.0, '파란색 채집 상자 B', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (770, 770, 2, 1, 100.0, '노란색 채집 상자 A', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (771, 771, 2, 1, 100.0, '노란색 채집 상자 B', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (772, 772, 2, 1, 100.0, '보라색 채집 상자 A', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (773, 773, 2, 1, 100.0, '보라색 채집 상자 B', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (774, 774, 2, 1, 100.0, '주황색 채집 상자 A', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (775, 775, 2, 1, 100.0, '주황색 채집 상자 B', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (776, 776, 2, 1, 100.0, '초록색 채집 상자 A', 0);
GO

INSERT INTO [dbo].[TblMaterialDrawIndex] ([MDID], [MDRD], [mResType], [mMaxResCnt], [mSuccess], [mDesc], [mAddQuestionMark]) VALUES (777, 777, 2, 1, 100.0, '초록색 채집 상자 B', 0);
GO

