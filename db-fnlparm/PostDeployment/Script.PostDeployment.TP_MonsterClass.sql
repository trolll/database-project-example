/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_MonsterClass
Date                  : 2023-10-07 09:08:48
*/


INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (1, 'A');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (3, 'C');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (4, 'D');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (5, 'E');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (6, 'F');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (7, 'G');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (8, 'H');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (9, 'I');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (10, 'J');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (11, 'K');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (12, 'L');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (13, 'M');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (14, 'N');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (15, 'O');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (16, 'P');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (17, 'Q');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (18, 'R');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (19, 'S');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (20, 'T');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (21, 'U');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (22, 'V');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (2, 'B');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (24, 'W');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (25, 'X');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (26, '특수몬스터');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (27, '이벤트');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (28, '네임드');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (29, '보스');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (31, 's');
GO

INSERT INTO [dbo].[TP_MonsterClass] ([MClass], [MName]) VALUES (32, '스팟 습격자');
GO

