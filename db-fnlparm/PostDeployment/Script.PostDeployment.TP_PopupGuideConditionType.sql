/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_PopupGuideConditionType
Date                  : 2023-10-07 09:09:30
*/


INSERT INTO [dbo].[TP_PopupGuideConditionType] ([mConType], [mDesc], [mAParm], [mBParm], [mCParm]) VALUES (1, '?? ?', '?? Level', NULL, NULL);
GO

INSERT INTO [dbo].[TP_PopupGuideConditionType] ([mConType], [mDesc], [mAParm], [mBParm], [mCParm]) VALUES (2, '?? ??', '??(EPlace)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_PopupGuideConditionType] ([mConType], [mDesc], [mAParm], [mBParm], [mCParm]) VALUES (3, '???? ???', '???? ??(TP_AbnormalType.AType)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_PopupGuideConditionType] ([mConType], [mDesc], [mAParm], [mBParm], [mCParm]) VALUES (4, '??? ???', '?? ??? ID(DT_Item.IID)', NULL, NULL);
GO

INSERT INTO [dbo].[TP_PopupGuideConditionType] ([mConType], [mDesc], [mAParm], [mBParm], [mCParm]) VALUES (5, '?? ?? ??', '??(WeekDay)(0~6)', '?? ??(0~23)', '?? ??(1~24)');
GO

INSERT INTO [dbo].[TP_PopupGuideConditionType] ([mConType], [mDesc], [mAParm], [mBParm], [mCParm]) VALUES (6, '???, ??? ??', '1:???, 2:???', NULL, NULL);
GO

