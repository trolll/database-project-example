/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblQuestRefMonster
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (2, 165);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (42, 165);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (43, 165);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (44, 165);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (45, 165);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (46, 165);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (47, 240);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (56, 321);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (85, 622);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (86, 311);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (87, 814);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (88, 814);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (90, 135);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (92, 570);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (93, 160);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (94, 160);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (95, 570);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (96, 600);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (97, 816);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (98, 816);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (99, 570);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (111, 677);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (112, 756);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (113, 754);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (114, 758);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (115, 760);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (116, 953);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (117, 953);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (118, 953);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (119, 953);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (120, 953);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (125, 1049);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (126, 1028);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (127, 1028);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (128, 1049);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (129, 1029);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (130, 1029);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (131, 1049);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (132, 1052);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (133, 1052);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (134, 1049);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (135, 1036);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (136, 1036);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (157, 953);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (158, 953);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (160, 1291);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (161, 1291);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (162, 1291);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (163, 1291);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (164, 1292);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (165, 1292);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (166, 1292);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (167, 1292);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (168, 1293);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (169, 1293);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (170, 1295);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (171, 1295);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (172, 1295);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (173, 1295);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (188, 1451);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (189, 1451);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (190, 1451);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (191, 1452);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (192, 1452);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (193, 1452);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (194, 1453);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (195, 1453);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (196, 1453);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (197, 1454);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (198, 1454);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (199, 1454);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (201, 1457);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (202, 1457);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (203, 1457);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (204, 1458);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (205, 1458);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (206, 1459);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (207, 1461);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (208, 1461);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (209, 1461);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (211, 1462);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (213, 1463);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (214, 1464);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (215, 1464);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (216, 1465);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (217, 1465);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (218, 1466);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (219, 1468);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (220, 1470);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (221, 1479);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (222, 1479);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (223, 1479);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (224, 1471);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (225, 1471);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (226, 1471);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (227, 1474);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (228, 1476);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (229, 1479);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (233, 1481);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (234, 1481);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (235, 1481);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (236, 1482);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (237, 1482);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (238, 1482);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (239, 1483);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (241, 1484);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (242, 1484);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (243, 1484);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (244, 1487);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (244, 1488);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (245, 1485);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (246, 1485);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (247, 1485);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (248, 1485);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (249, 1456);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (250, 1462);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (251, 1462);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (301, 1710);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (302, 1711);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (303, 1712);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (304, 1713);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (311, 1696);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (312, 1703);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (314, 1705);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (315, 1701);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (316, 1706);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (318, 1700);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (319, 1702);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (320, 1699);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (321, 1709);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (322, 1704);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (323, 1708);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (324, 1707);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (325, 1698);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (331, 1720);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (332, 1720);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (333, 1721);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (334, 1721);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (335, 1722);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (336, 1722);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (337, 1723);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (338, 1723);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (382, 680);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (386, 1741);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (387, 1741);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (388, 1741);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (389, 1742);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (390, 1742);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (391, 1742);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (392, 1743);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (393, 1743);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (394, 1744);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (395, 1744);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (396, 1745);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (397, 1745);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (398, 1746);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (399, 1746);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (400, 1291);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (401, 1292);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (402, 1293);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (403, 1749);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (404, 1749);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (405, 1749);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (406, 1307);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (407, 1307);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (408, 1307);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (409, 1307);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (410, 1297);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (411, 1297);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (412, 1297);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (413, 1296);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (414, 1296);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (415, 1296);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (416, 1296);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (417, 1296);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (418, 1750);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (419, 1750);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (420, 1750);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (421, 1747);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (422, 1747);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (423, 1747);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (424, 1747);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (425, 1748);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (426, 1748);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (427, 1748);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (428, 1748);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (430, 1489);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (431, 1489);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (432, 1490);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (433, 1490);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (436, 1782);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (437, 1783);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (438, 1784);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (439, 1785);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (440, 1786);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (441, 1787);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (442, 1788);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (443, 1789);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (444, 1790);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (445, 1791);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (446, 1792);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (447, 1793);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (448, 1794);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (449, 1795);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (450, 1796);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (451, 1797);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (452, 2805);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (453, 2805);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (467, 1819);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (468, 1820);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (469, 1821);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (470, 1866);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (471, 1813);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (472, 1813);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (473, 1813);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (474, 1814);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (475, 1814);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (476, 1814);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (477, 1815);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (478, 1815);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (479, 1815);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (480, 1815);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (481, 264);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (482, 264);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (483, 264);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (484, 279);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (485, 279);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (486, 279);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (487, 288);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (488, 288);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (489, 288);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (490, 297);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (491, 297);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (492, 297);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (493, 294);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (494, 678);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (495, 678);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (496, 678);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (497, 675);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (498, 675);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (499, 675);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (500, 675);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (501, 750);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (502, 750);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (503, 750);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (504, 750);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (505, 751);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (506, 751);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (507, 751);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (508, 751);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (509, 708);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (510, 708);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (511, 708);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (512, 626);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (513, 626);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (514, 707);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (515, 707);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (516, 707);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (517, 275);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (518, 275);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (519, 275);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (520, 623);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (521, 623);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (522, 624);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (523, 624);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (524, 624);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (525, 625);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (526, 625);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (527, 948);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (528, 948);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (529, 948);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (530, 766);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (531, 766);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (532, 766);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (533, 1818);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (534, 1891);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (535, 1892);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (536, 1893);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (540, 425);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (541, 425);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (542, 425);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (543, 425);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (544, 2012);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (545, 2012);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (546, 2012);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (547, 1719);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (548, 1719);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (549, 1719);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (550, 1719);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (551, 1719);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (552, 1835);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (553, 1835);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (554, 1835);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (555, 1835);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (556, 1835);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (557, 2013);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (558, 2013);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (559, 2013);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (560, 2014);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (561, 2014);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (562, 2024);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (563, 2024);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (564, 2024);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (565, 2030);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (566, 2030);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (567, 2030);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (568, 2025);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (569, 2026);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (570, 2027);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (571, 2028);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (572, 2031);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (573, 2032);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (574, 2033);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (575, 2034);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (576, 162);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (576, 333);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (576, 682);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (576, 748);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (576, 1066);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (578, 375);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (579, 376);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (580, 375);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (581, 352);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (582, 350);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (583, 349);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (584, 382);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (585, 378);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (586, 343);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (587, 381);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (588, 384);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (589, 374);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (590, 339);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (591, 341);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (592, 383);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (593, 379);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (594, 353);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (595, 345);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (596, 387);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (597, 351);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (598, 340);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (599, 372);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (600, 386);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (601, 338);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (602, 338);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (603, 1039);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (604, 1045);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (605, 1046);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (606, 1044);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (607, 1027);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (608, 1030);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (609, 1035);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (610, 1041);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (611, 1042);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (612, 1076);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (613, 1043);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (614, 717);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (615, 375);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (616, 375);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (617, 375);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (618, 382);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (619, 378);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (620, 343);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (621, 352);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (622, 350);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (623, 2064);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (624, 2066);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (625, 2064);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (626, 2064);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (627, 2064);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (628, 341);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (629, 383);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (630, 379);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (631, 2064);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (632, 384);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (633, 374);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (634, 2065);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (635, 2067);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (636, 2065);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (637, 2065);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (638, 2065);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (639, 340);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (640, 372);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (641, 386);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (642, 2065);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (643, 345);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (644, 387);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (645, 1039);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (646, 1045);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (647, 1046);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (648, 1035);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (649, 1041);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (650, 1042);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (651, 1076);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (652, 1046);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (653, 1044);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (654, 1027);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (655, 2061);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (656, 2062);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (657, 2063);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (658, 2081);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (659, 2082);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (660, 2082);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (661, 2084);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (662, 765);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (663, 668);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (664, 155);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (665, 155);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (666, 166);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (667, 166);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (668, 1751);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (669, 2092);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (670, 2092);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (671, 1752);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (672, 1752);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (673, 2093);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (674, 2093);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (675, 1753);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (676, 1753);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (677, 2094);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (678, 2094);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (679, 1754);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (680, 2096);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (681, 2096);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (682, 2096);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (683, 2097);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (684, 2098);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (685, 2098);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (686, 2098);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (687, 2099);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (688, 2085);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (689, 2085);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (690, 2083);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (691, 2086);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (692, 2095);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (693, 2095);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (694, 1755);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (695, 1755);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (696, 1756);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (697, 1756);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (698, 2106);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (699, 2106);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (700, 2106);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (701, 2107);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (702, 2107);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (703, 1758);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (704, 1758);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (705, 1757);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (706, 1757);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (707, 1759);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (708, 1759);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (709, 2108);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (710, 2108);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (711, 1755);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (712, 1762);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (713, 1762);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (714, 1762);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (715, 1763);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (716, 1763);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (717, 2109);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (718, 2109);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (719, 2110);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (720, 2110);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (721, 1764);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (722, 1764);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (723, 2111);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (724, 2112);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (725, 2112);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (726, 2111);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (727, 1765);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (728, 1765);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (729, 761);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (730, 761);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (731, 1766);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (732, 1766);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (733, 1772);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (734, 2113);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (735, 2113);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (736, 1769);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (737, 2114);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (738, 2114);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (739, 2115);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (740, 1770);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (741, 2115);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (742, 2116);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (743, 2116);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (744, 2117);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (745, 2117);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (746, 1722);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (747, 2124);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (748, 2124);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (749, 2124);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (750, 2125);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (751, 2126);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (752, 2126);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (753, 2126);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (754, 2127);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (755, 2118);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (756, 2119);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (757, 2119);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (758, 2121);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (759, 2122);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (760, 2122);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (761, 2120);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (762, 2123);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (763, 2128);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (764, 2128);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (765, 2128);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (766, 2129);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (767, 2129);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (768, 2130);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (769, 2130);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (770, 2131);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (771, 2131);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (772, 2131);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (773, 2132);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (774, 2132);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (775, 2132);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (776, 2131);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (777, 2133);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (778, 2133);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (779, 2133);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (780, 2134);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (781, 2134);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (782, 2134);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (783, 2135);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (784, 2135);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (785, 2136);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (786, 2136);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (787, 2136);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (788, 2137);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (789, 2137);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (790, 2137);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (791, 2138);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (792, 2138);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (793, 2138);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (794, 2138);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (795, 2139);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (796, 2139);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (797, 2231);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (798, 2231);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (799, 2232);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (800, 2232);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (801, 2233);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (802, 2233);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (803, 2234);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (804, 2234);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (805, 2235);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (806, 2235);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (807, 2231);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (808, 2236);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (809, 2236);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (810, 2237);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (811, 2237);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (812, 2238);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (813, 2238);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (814, 2239);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (815, 2231);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (816, 2240);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (817, 2240);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (818, 2241);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (819, 2241);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (820, 2242);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (821, 2242);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (822, 2243);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (823, 2244);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (824, 2245);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (825, 2246);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (826, 2247);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (827, 2248);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (828, 2249);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (829, 2250);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (830, 2251);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (831, 2314);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (832, 2474);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (833, 2475);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (834, 2475);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (835, 2476);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (836, 2477);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (837, 2477);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (838, 2477);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (839, 2478);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (840, 2479);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (841, 2480);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (842, 2480);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (843, 2480);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (844, 2481);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (845, 2482);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (846, 2482);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (847, 2482);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (848, 2483);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (849, 2484);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (850, 2484);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (851, 2484);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (852, 2485);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (853, 2486);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (854, 2486);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (855, 2486);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (856, 2579);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (857, 2607);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (858, 2608);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (859, 2608);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (860, 2608);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (861, 2608);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (862, 2609);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (863, 2609);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (864, 2608);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (865, 2579);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (866, 2625);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (867, 2625);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (868, 2625);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (869, 2625);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (870, 2625);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (871, 2624);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (872, 2624);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (873, 2624);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (874, 2624);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (875, 2624);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (876, 2682);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (877, 2682);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (878, 2682);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (879, 2682);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (880, 2682);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (881, 2683);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (882, 2683);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (883, 2683);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (884, 2683);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (885, 2683);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (886, 2683);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (887, 2684);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (888, 2684);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (889, 2684);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (890, 2684);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (891, 2684);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (892, 2685);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (893, 2685);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (894, 2685);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (895, 2685);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (896, 2685);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (897, 2685);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (898, 2690);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (899, 2857);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (900, 1691);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (901, 1691);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (902, 1691);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (903, 1692);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (904, 1692);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (905, 1693);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (906, 1694);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (907, 1694);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (908, 1692);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (909, 1692);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (910, 2105);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (911, 2105);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (912, 2105);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (913, 2105);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (914, 2105);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (1000, 2691);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (1001, 2691);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (1002, 2691);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (1003, 2691);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (1004, 2691);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (1005, 2748);
GO

INSERT INTO [dbo].[TblQuestRefMonster] ([mQuestNo], [mMonsterID]) VALUES (1006, 2870);
GO

