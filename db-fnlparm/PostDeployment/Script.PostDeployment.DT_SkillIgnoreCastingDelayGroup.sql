/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_SkillIgnoreCastingDelayGroup
Date                  : 2023-10-07 09:09:28
*/


INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (1, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (1, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (2, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (2, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (3, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (3, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (4, 1);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (4, 2);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (4, 6);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (4, 7);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (4, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (4, 10);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (4, 11);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (5, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (5, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (6, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (6, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (7, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (7, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (8, 1);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (8, 2);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (8, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (8, 5);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (8, 6);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (8, 7);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (8, 11);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 1);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 2);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 3);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 5);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 6);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 7);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 9);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 10);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (9, 11);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (10, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (10, 8);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (11, 4);
GO

INSERT INTO [dbo].[DT_SkillIgnoreCastingDelayGroup] ([SGroupNo], [SIgnoreGroup]) VALUES (11, 8);
GO

