/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblSpecialScopeSkill
Date                  : 2023-10-07 09:09:33
*/


INSERT INTO [dbo].[TblSpecialScopeSkill] ([mSkillNo], [mType], [mIsSpecialRadius], [mParamA], [mParamB], [mParamC]) VALUES (2554, 4, 1, 250.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblSpecialScopeSkill] ([mSkillNo], [mType], [mIsSpecialRadius], [mParamA], [mParamB], [mParamC]) VALUES (2558, 2, 0, 0.0, 30.0, 0.0);
GO

INSERT INTO [dbo].[TblSpecialScopeSkill] ([mSkillNo], [mType], [mIsSpecialRadius], [mParamA], [mParamB], [mParamC]) VALUES (2560, 1, 0, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblSpecialScopeSkill] ([mSkillNo], [mType], [mIsSpecialRadius], [mParamA], [mParamB], [mParamC]) VALUES (2580, 1, 0, 0.0, 0.0, 0.0);
GO

INSERT INTO [dbo].[TblSpecialScopeSkill] ([mSkillNo], [mType], [mIsSpecialRadius], [mParamA], [mParamB], [mParamC]) VALUES (2583, 2, 0, 0.0, 30.0, 0.0);
GO

INSERT INTO [dbo].[TblSpecialScopeSkill] ([mSkillNo], [mType], [mIsSpecialRadius], [mParamA], [mParamB], [mParamC]) VALUES (2585, 2, 0, 0.0, 90.0, 0.0);
GO

INSERT INTO [dbo].[TblSpecialScopeSkill] ([mSkillNo], [mType], [mIsSpecialRadius], [mParamA], [mParamB], [mParamC]) VALUES (2591, 4, 1, 250.0, 0.0, 0.0);
GO

