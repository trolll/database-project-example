/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_AchieveItemLevel
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (1, 0);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (2, 1732);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (3, 3811);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (4, 6236);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (5, 9008);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (6, 12127);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (7, 15592);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (8, 19404);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (9, 23562);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (10, 28067);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (11, 32918);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (12, 38116);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (13, 43660);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (14, 49551);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (15, 55789);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (16, 62373);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (17, 69304);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (18, 76581);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (19, 84205);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (20, 92175);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (21, 100492);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (22, 109156);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (23, 118166);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (24, 127523);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (25, 137226);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (26, 147276);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (27, 157672);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (28, 168415);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (29, 179504);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (30, 190940);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (31, 203936);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (32, 217368);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (33, 231236);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (34, 245540);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (35, 260280);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (36, 275456);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (37, 291068);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (38, 307116);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (39, 323600);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (40, 340520);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (41, 357876);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (42, 375668);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (43, 393897);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (44, 412562);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (45, 431663);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (46, 451200);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (47, 471173);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (48, 491582);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (49, 512427);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (50, 533708);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (51, 555425);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (52, 577578);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (53, 600167);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (54, 623193);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (55, 646655);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (56, 670553);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (57, 694887);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (58, 719657);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (59, 744863);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (60, 770505);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (61, 799385);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (62, 828987);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (63, 859311);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (64, 890357);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (65, 922125);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (66, 954615);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (67, 987827);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (68, 1021761);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (69, 1056417);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (70, 1091795);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (71, 1127895);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (72, 1164717);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (73, 1202261);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (74, 1240527);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (75, 1279515);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (76, 1319225);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (77, 1359657);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (78, 1400811);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (79, 1442687);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (80, 1485285);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (81, 1528605);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (82, 1572647);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (83, 1617411);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (84, 1662897);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (85, 1709105);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (86, 1756035);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (87, 1803687);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (88, 1852061);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (89, 1901157);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (90, 1950975);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (91, 2001515);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (92, 2052777);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (93, 2104761);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (94, 2157467);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (95, 2210895);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (96, 2265045);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (97, 2319917);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (98, 2375511);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (99, 2431827);
GO

INSERT INTO [dbo].[TP_AchieveItemLevel] ([mLevel], [mExp]) VALUES (100, 2488865);
GO

