/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblPlace
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (0, '未指定区域', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (1, '修炼之窟', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (2, '吉内亚村', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (3, '港口', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (4, '黑暗祭司峡谷', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (5, '西部海滩', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (6, '失落的精灵遗迹', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (7, '霍顿风车坊', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (8, '修炼之窟 1层', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (9, '艾斯本镇', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (10, '艾斯本港口', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (11, '风车平原', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (12, '复仇之城', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (13, '不死地牢入口', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (14, '豺狼人的营寨', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (15, '侏儒林地', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (16, '亡者之地', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (17, '烈焰塔入口', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (18, '蜘蛛林', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (19, '黑暗洞窟入口', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (20, '半人鱼栖息地', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (21, '兽人营地', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (22, '拉贡河', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (23, '黑暗洞窟 1层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (24, '黑暗洞窟 2层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (25, '黑暗洞窟 3层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (26, '黑暗洞窟 4层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (27, '黑暗洞窟 5层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (28, '黑暗洞窟 6层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (29, '黑暗洞窟 7层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (30, '艾斯本镇道具商店', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (31, '艾斯本镇武器商店', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (32, '烈焰塔 1层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (33, '烈焰塔 2层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (34, '烈焰塔 3层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (35, '烈焰塔 4层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (36, '烈焰塔 5层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (37, '烈焰塔 6层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (38, '烈焰塔 7层', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (39, '黑土镇', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (40, '黑暗之城', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (41, '精灵神殿', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (42, '混沌神殿', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (43, '秩序神殿', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (44, '黑龙沼泽入口', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (45, '流浪者镇', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (46, '帝王墓穴 ', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (47, '失落的港口', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (48, '流浪者镇竞技场', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (49, '地精营地', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (50, '极乐之巢', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (51, '黑龙沼泽 1层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (52, '黑龙沼泽 2层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (53, '黑龙沼泽 3层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (54, '黑龙沼泽 4层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (55, '黑龙沼泽 5层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (56, '黑龙沼泽 6层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (57, '黑龙沼泽 7层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (58, '不死地牢 入口', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (59, '不死地牢 1层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (60, '不死地牢 2层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (61, '不死地牢 3层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (62, '不死地牢 4层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (63, '不死地牢 5层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (64, '不死地牢 6层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (65, '不死地牢 7层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (66, '帝王墓穴 1层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (67, '帝王墓穴 2层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (68, '帝王墓穴 3层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (69, '帝王墓穴 4层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (70, '帝王墓穴 5层', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (71, '复仇之城竞技场', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (72, '黑暗之城竞技场', 'Black_land');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (73, '拜伦镇', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (74, '拜伦城', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (75, '圣甲虫谷', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (76, '埃吉尔洞窟', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (77, '古城那勒图', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (78, '妖精岭', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (79, '石锤镇', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (80, '图兰湾', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (81, '哈斯特祭坛', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (82, '德拉克农场', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (83, '拜伦竞技场', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (84, '圣甲虫谷1层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (85, '圣甲虫谷2层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (86, '圣甲虫谷3层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (87, '圣甲虫谷4层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (88, '圣甲虫谷5层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (89, '埃吉尔洞窟1层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (90, '埃吉尔洞窟2层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (91, '埃吉尔洞窟3层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (92, '埃吉尔洞窟4层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (93, '埃吉尔洞窟5层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (94, '罗敦镇', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (95, '罗敦城', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (96, '???', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (97, '公会小天地（黑暗大陆）', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (98, '公会小天地（拜伦）', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (99, '宴会厅 (小) (复仇之地)', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (100, '宴会厅 (小) (黑暗大陆)', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (101, '宴会厅 (小) (拜伦)', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (102, '宴会厅 (大) (复仇之地)', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (103, '宴会厅 (大) (黑暗大陆)', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (104, '宴会厅 (大) (拜伦)', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (105, '团队战竞技场0~30', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (106, '团队战竞技场 31~44', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (107, '团队战竞技场 45~Over', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (108, '封印的磐石', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (109, '地狱之门', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (110, '运营者决斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (111, '特瑞斯火山', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (112, '罗敦城传送门', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (113, '维拉梯田', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (114, '深红峡谷', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (115, '无名洞窟', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (116, '扎坦盆地', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (117, '时间之湖', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (118, '巨人的安息地', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (119, '牛角迷宫', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (120, '罗敦镇露营地', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (121, '无名洞窟外部', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (122, '无名洞窟内部', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (123, '古代祭坛－1层', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (124, '古代祭坛－2层', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (125, '古代祭坛－3层', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (126, '城镇废墟', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (127, '流放地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (128, '咨询室', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (129, '宽敞的在团队战场 初级', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (130, '宽敞的在团队战场 中级', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (131, '宽敞的在团队战场 上级', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (132, '活动团队战场 1', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (133, '活动团队战场 2', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (134, '活动团队战场 3', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (135, '米泰欧斯稀有 - 外部', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (136, '米泰欧斯稀有 - 外部营帐', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (137, '米泰欧斯稀有 - 内部', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (138, '米泰欧斯神殿', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (139, '团队战竞技场 45~Over', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (140, '最上级冠军团队战', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (141, '最上级塔楼团队战', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (142, '阿克拉岛', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (143, '阿克拉镇', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (144, '仪式祭坛', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (145, '原始森林', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (146, '樵童的海滨', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (147, '结界缝隙', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (148, '次元森林', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (149, '蛇的海滨', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (150, '华利弗森林', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (151, '华利弗地下城入口', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (152, '野性的树海', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (153, '意志之战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (154, '意志之战场攻击营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (155, '意志之战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (156, '斗志之战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (157, '斗志之战场攻击营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (158, '斗志之战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (159, '寂静之战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (160, '寂静之战场攻击营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (161, '寂静之战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (162, '智慧之战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (163, '智慧之战场攻击营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (164, '智慧之战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (165, '华利弗地下城入口（复仇之地方向）', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (166, '华利弗地下城', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (167, '被封印的洞窟', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (168, '海盗洞窟 1层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (169, '海盗洞窟 2层', 'Byron');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (170, '米泰欧斯之塔', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (171, '大地的房间', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (172, '大气的房间', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (173, '大海的房间', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (174, '太阳的房间', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (175, '米泰欧斯最上层', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (176, '高级公会议事厅', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (177, '稀有公会议事厅', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (178, '精致的公会议事厅', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (179, '超豪华公会议事厅', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (180, '第1公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (181, '第1古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (182, '第1古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (183, '第2公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (184, '第2古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (185, '第2古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (186, '第3公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (187, '第3古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (188, '第3古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (189, '第4公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (190, '第4古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (191, '第4古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (192, '第5公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (193, '第5古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (194, '第5古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (195, '第6公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (196, '第6古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (197, '第6古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (198, '第7公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (199, '第7古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (200, '第7古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (201, '第8公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (202, '第8古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (203, '第8古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (204, '第9公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (205, '第9古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (206, '第9古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (207, '第10公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (208, '第10古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (209, '第10古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (210, '第11公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (211, '第11古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (212, '第11古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (213, '第12公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (214, '第12古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (215, '第12古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (216, '第13公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (217, '第13古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (218, '第13古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (219, '第14公会战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (220, '第14古代竞技场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (221, '第14古代角斗场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (222, '意志之战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (223, '斗志之战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (224, '寂静之战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (225, '智慧之战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (226, '荣光圣殿', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (227, '荣光圣殿营地区域', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (228, '变质的沼泽', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (229, '德拉古兰的巢穴', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (230, '变种动物岩石地带', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (231, '混乱洞窟', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (232, '巴普麦特魔磷桥', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (233, '巴普麦特巢穴', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (234, '岩石峡谷基地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (235, '风车镇', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (236, '阴影之镇', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (237, '吉内亚港口', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (238, '吉内亚北部海岸', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (239, '吉内亚东部海岸', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (240, '吉内亚南部海岸', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (241, '古代精灵都市', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (242, '尤金教皇北部结界', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (243, '尤金教皇东部结界', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (244, '尤金教皇南部结界', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (245, '古代精灵的痕迹', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (246, '风车农场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (247, '岩石峡谷湖畔', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (248, '盈月遗迹', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (249, '废弃的盈月遗迹', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (250, '遗忘的草丛', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (251, '遗忘的巨像', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (252, '遗忘的高原', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (253, '潜伏者袭击地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (254, '盈月之桥', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (255, '冷森森的前线基地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (256, '拉尔卡追击壁垒区域', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (257, '拉尔卡追击聚集地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (258, '拉尔卡祭坛', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (259, '盈月之壁', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (260, '盈月遗迹入口区域', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (261, '勇气战场', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (262, '勇气战场攻击营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (263, '勇气战场守备营地', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (264, '勇气战场守备安全区域', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (265, '鲁普团队战', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (266, '第1怪物狩猎', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (267, '第2怪物狩猎', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (268, '第3怪物狩猎', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (269, '第4怪物狩猎', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (270, '第5怪物狩猎', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (271, '第6怪物狩猎', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (272, '第7怪物狩猎', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (273, '第8怪物狩猎', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (274, '重建的贝利恩都市', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (275, '贝利恩决斗场', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (276, '天空之城亚特兰', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (277, '皇冠刻印地区', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (278, '亚特兰小天地', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (279, '亚特兰桥墩', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (280, '天使的天籁草丛', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (281, '魔力草丛', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (282, '光辉草丛', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (283, '朱庇特圣所', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (284, '军团巢穴', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (285, '疾病的踪迹', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (286, '死者废墟', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (287, '巴贝克祭坛', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (288, '被禁止的密林', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (289, '宁静的荒地', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (290, '天籁草原', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (291, '荒野峡谷', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (292, '天空之城艾泰尔', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (293, '天空之城艾泰尔入口', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (294, '朱庇特副本入口', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (295, '巴贝克副本入口', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (296, '朱庇特副本一层', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (297, '巴贝克副本一层', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (298, '朱庇特副本二层', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (299, '巴贝克副本二层', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (300, '米泰欧斯巢穴', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (301, '盈月地平线
', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (302, '盈月之湖
', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (303, '华利弗地下城 70
', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (304, '华利弗地下城 80
', 'Fourier');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (305, '阿贝尔森的要塞
', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (306, '第1激战
', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (307, '阿贝尔森的要塞入口
', 'Elther');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (308, '第1 Boss战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (309, '第2 Boss战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (310, '第3 Boss战', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (311, '第4 Boss战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (312, '第5 Boss战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (313, '第6 Boss战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (314, '第1激战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (315, '第2激战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (316, '第3激战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (317, '第4激战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (318, '第5激战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (319, '第6激战
', 'Unknown');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (320, '暗黑祭司祭坛外部
', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (321, '暗黑祭司祭坛内部
', 'Loden');
GO

INSERT INTO [dbo].[TblPlace] ([mPlaceNo], [mPlaceNm], [mTerritoryNm]) VALUES (322, '暗黑祭司祭坛内部
', 'Loden');
GO

