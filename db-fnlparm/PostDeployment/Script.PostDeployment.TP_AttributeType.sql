/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_AttributeType
Date                  : 2023-10-07 09:08:46
*/


INSERT INTO [dbo].[TP_AttributeType] ([AType], [AName]) VALUES (1, '화염');
GO

INSERT INTO [dbo].[TP_AttributeType] ([AType], [AName]) VALUES (2, '냉기');
GO

INSERT INTO [dbo].[TP_AttributeType] ([AType], [AName]) VALUES (3, '전격');
GO

INSERT INTO [dbo].[TP_AttributeType] ([AType], [AName]) VALUES (4, '산');
GO

INSERT INTO [dbo].[TP_AttributeType] ([AType], [AName]) VALUES (5, '이블파워');
GO

INSERT INTO [dbo].[TP_AttributeType] ([AType], [AName]) VALUES (6, '홀리파워');
GO

INSERT INTO [dbo].[TP_AttributeType] ([AType], [AName]) VALUES (7, '무속성');
GO

