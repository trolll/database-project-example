/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblServantCombineAddAbility
Date                  : 2023-10-07 09:09:33
*/


INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (7, 0, 0, 3, 0, 0, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (7, 0, 1, 4, 0, 0, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (7, 1, 0, 4, 0, 0, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (7, 1, 1, 5, 0, 0, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (8, 0, 0, 3, 0, 0, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (8, 0, 1, 4, 0, 0, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (8, 1, 0, 4, 0, 0, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (8, 1, 1, 5, 0, 0, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (9, 0, 0, 0, 0, 3, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (9, 0, 1, 0, 0, 4, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (9, 1, 0, 0, 0, 4, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (9, 1, 1, 0, 0, 5, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (10, 0, 0, 0, 0, 3, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (10, 0, 1, 0, 0, 4, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (10, 1, 0, 0, 0, 4, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (10, 1, 1, 0, 0, 5, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (11, 0, 0, 2, 2, 2, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (11, 0, 1, 2, 2, 2, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (11, 1, 0, 2, 2, 2, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (11, 1, 1, 2, 2, 2, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (12, 0, 0, 2, 2, 2, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (12, 0, 1, 2, 2, 2, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (12, 1, 0, 2, 2, 2, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (12, 1, 1, 2, 2, 2, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (13, 0, 0, 2, 0, 2, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (13, 0, 1, 3, 0, 3, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (13, 1, 0, 3, 0, 3, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (13, 1, 1, 4, 0, 4, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (14, 0, 0, 2, 0, 2, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (14, 0, 1, 3, 0, 3, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (14, 1, 0, 3, 0, 3, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (14, 1, 1, 4, 0, 4, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (15, 0, 0, 2, 2, 0, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (15, 0, 1, 3, 3, 0, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (15, 1, 0, 3, 3, 0, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (15, 1, 1, 4, 4, 0, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (16, 0, 0, 2, 2, 0, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (16, 0, 1, 3, 3, 0, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (16, 1, 0, 3, 3, 0, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (16, 1, 1, 4, 4, 0, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (17, 0, 0, 0, 2, 2, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (17, 0, 1, 0, 3, 3, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (17, 1, 0, 0, 3, 3, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (17, 1, 1, 0, 4, 4, 3, 5);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (18, 0, 0, 0, 2, 2, 1, 3);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (18, 0, 1, 0, 3, 3, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (18, 1, 0, 0, 3, 3, 2, 4);
GO

INSERT INTO [dbo].[TblServantCombineAddAbility] ([SStuffType], [SIsCoreGold], [SIsStuffGold], [SStrMax], [SDexMax], [SIntMax], [STotalMin], [STotalMax]) VALUES (18, 1, 1, 0, 4, 4, 3, 5);
GO

