/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TblWorldMapInfo
Date                  : 2023-10-07 09:09:29
*/


INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (0, 1, 1, 0, 'ePlaceNo, // 지정되지않은지역');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (1, 1, 1, 0, 'ePlaceTrainingCave, // 수련동굴  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (2, 1, 1, 0, 'ePlaceGuineaTown, // 기네아마을  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (3, 1, 1, 0, 'ePlaceGuineaDock, // 선착장.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (4, 1, 1, 0, 'ePlaceValleyDarkPriests, // 암흑사제의 협곡.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (5, 1, 1, 0, 'ePlaceSerpentBeach, // 서펜트해변.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (6, 1, 1, 0, 'ePlaceElvenRuin, // 버려진 엘프유적.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (7, 1, 1, 0, 'ePlaceWindmillHoldenFamily, // 홀덴의 풍차.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (8, 1, 1, 0, 'ePlaceTraningCave1, // 수련동굴 1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (9, 1, 1, 0, 'ePlaceAshburnTown, // 애쉬번마을  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (10, 1, 1, 0, 'ePlaceAshburnPort, // 애쉬번항구  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (11, 1, 1, 0, 'ePlaceWindmillPlain, // 풍차평원  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (12, 1, 1, 0, 'ePlaceFurieCastle, // 퓨리에성(성)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (13, 0, 0, 0, 'ePlaceNoUse1, // 사용안함.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (14, 1, 1, 0, 'ePlaceFastnessOfKnolls, // 놀의 산적아지트(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (15, 1, 1, 0, 'ePlaceGremlinsWood, // 그렘린 숲  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (16, 1, 1, 0, 'ePlaceLandOfTheDead, // 망자의 대지  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (17, 1, 1, 0, 'ePlaceTowerOfFlame, // 화염의 탑입구.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (18, 1, 1, 0, 'ePlaceSpidersForest, // 거미숲(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (19, 1, 1, 0, 'ePlaceDarkCave, // 어두운 동굴입구.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (20, 1, 1, 0, 'ePlaceMermanGround, // 머맨서식지(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (21, 1, 1, 0, 'ePlaceOrcCamp, // 오크캠프(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (22, 1, 1, 0, 'ePlaceLargonRiver, // 라르곤 강  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (23, 1, 1, 0, 'ePlaceDarkCave1, // 어두운 동굴 1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (24, 1, 1, 0, 'ePlaceDarkCave2, // 어두운 동굴 2층.(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (25, 1, 1, 0, 'ePlaceDarkCave3, // 어두운 동굴 3층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (26, 1, 1, 0, 'ePlaceDarkCave4, // 어두운 동굴 4층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (27, 0, 0, 0, 'ePlaceStrangeDungeon, // 이상한 던전.');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (28, 1, 1, 0, 'ePlaceDarkCave5, // 어두운 동굴 5층.');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (29, 0, 0, 0, 'ePlaceDarkCave6, // 어두운 동굴 6층.');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (30, 0, 0, 0, 'ePlaceItemShopAshburnTown, // 애쉬번마을 아이템 상점.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (31, 0, 0, 0, 'ePlaceWeaponShopAshburnTown,//  무기 상점.   ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (32, 1, 1, 0, 'ePlaceTowerOfFlame1, // 화염의탑 1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (33, 1, 1, 0, 'ePlaceTowerOfFlame2, // 화염의탑 2층.(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (34, 1, 1, 0, 'ePlaceTowerOfFlame3, // 화염의탑 3층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (35, 1, 1, 0, 'ePlaceTowerOfFlame4, // 화염의탑 4층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (36, 1, 1, 0, 'ePlaceTowerOfFlame5, // 화염의탑 5층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (37, 0, 0, 0, 'ePlaceTowerOfFlame6, // 화염의탑 6층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (38, 0, 0, 0, 'ePlaceTowerOfFlame7, // 화염의탑 7층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (39, 1, 1, 0, 'ePlaceBlacklandTown, // 블랙랜드마을  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (40, 1, 1, 0, 'ePlaceBlacklandCastle, // 블랙랜드성(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (41, 1, 1, 0, 'ePlaceElfTemple, // 엘프신전  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (42, 1, 1, 0, 'ePlaceSanctuaryOfChaos, // 혼돈의신전(블랙유니콘)(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (43, 1, 1, 0, 'ePlaceSanctuaryOfOrder, // 질서의신전(유니콘)(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (44, 1, 1, 0, 'ePlaceSwampOfBlackDragon, // 흑룡의 늪입구.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (45, 1, 1, 0, 'ePlaceVillageOfStraggler, // 낙오자의 마을.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (46, 1, 1, 0, 'ePlaceKingTomb, // 왕의무덤(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (47, 1, 1, 0, 'ePlaceDismissedPort, // 버려진 선착장.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (48, 1, 1, 0, 'ePlaceRaceCourse, // 낙오자마을 경주장.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (49, 1, 1, 0, 'ePlaceKoboldCamp, // 코볼트 캠프.(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (50, 1, 1, 0, 'ePlaceHarpiNest, // 하피의 둥지.(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (51, 1, 1, 0, 'ePlaceSwampBlackDragon1, // 흑룡의늪 1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (52, 1, 1, 0, 'ePlaceSwampBlackDragon2, // 흑룡의늪 2층.(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (53, 1, 1, 0, 'ePlaceSwampBlackDragon3, // 흑룡의늪 3층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (54, 1, 1, 0, 'ePlaceSwampBlackDragon4, // 흑룡의늪 4층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (55, 1, 1, 0, 'ePlaceSwampBlackDragon5, // 흑룡의늪 5층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (56, 0, 0, 0, 'ePlaceSwampBlackDragon6, // 흑룡의늪 6층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (57, 0, 0, 0, 'ePlaceSwampBlackDragon7, // 흑룡의늪 7층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (58, 1, 1, 0, 'ePlaceUndeadDungeon, // 언데드던젼입구.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (59, 1, 1, 0, 'ePlaceUndeadDungeon1, // 언데드던젼 1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (60, 1, 1, 0, 'ePlaceUndeadDungeon2, // 언데드던젼 2층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (61, 1, 1, 0, 'ePlaceUndeadDungeon3, // 언데드던젼 3층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (62, 1, 1, 0, 'ePlaceUndeadDungeon4, // 언데드던젼 4층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (63, 0, 0, 0, 'ePlaceUndeadDungeon5, // 언데드던젼 5층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (64, 0, 0, 0, 'ePlaceUndeadDungeon6, // 언데드던젼 6층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (65, 0, 0, 0, 'ePlaceUndeadDungeon7, // 언데드던젼 7층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (66, 1, 1, 0, 'ePlaceKingTomb1, // 왕의무덤 1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (67, 1, 1, 0, 'ePlaceKingTomb2, // 왕의무덤 2층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (68, 1, 1, 0, 'ePlaceKingTomb3, // 왕의무덤 3층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (69, 1, 1, 0, 'ePlaceKingTomb4, // 왕의무덤 4층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (70, 1, 1, 0, 'ePlaceKingTomb5, // 왕의무덤 5층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (71, 1, 1, 0, 'ePlacePvpZoneAtFurie, // 푸리에결투장.');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (72, 1, 1, 0, 'ePlacePvpZoneAtBlackland, // 블랙랜드결투장.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (73, 1, 1, 0, 'ePlaceByronTown, // 바이런 마을  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (74, 1, 1, 0, 'ePlaceByronCastle, // 바이런 성(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (75, 1, 1, 0, 'ePlacePitsOfGiantBeetle, // 딱정벌레구덩이  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (76, 1, 1, 0, 'ePlaceAegirUnderwaterCave, // 에기르의 수중동굴.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (77, 1, 1, 0, 'ePlaceAncientRuinOfKhnartuu,// 고대도시 나르투(spot)   ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (78, 1, 1, 0, 'ePlaceHillOfFairies, // 요정의언덕(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (79, 1, 1, 0, 'ePlaceStoneHammer, // 스톤해머(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (80, 1, 1, 0, 'ePlaceTolanBay, // 툴란만.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (81, 1, 1, 0, 'ePlaceAltarOfHaast, // 하스트의 제단.(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (82, 1, 1, 0, 'ePlaceDracoFarm, // 드라코농장.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (83, 1, 1, 0, 'ePlacePvpZoneAtByron, // 바이런결투장.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (84, 1, 1, 0, 'ePlacePitsOfGiantBeetle1, // 딱정벌레구덩이1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (85, 1, 1, 0, 'ePlacePitsOfGiantBeetle2, // 딱정벌레구덩이2층.(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (86, 1, 1, 0, 'ePlacePitsOfGiantBeetle3, // 딱정벌레구덩이3층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (87, 0, 0, 0, 'ePlacePitsOfGiantBeetle4, // 딱정벌레구덩이4층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (88, 0, 0, 0, 'ePlacePitsOfGiantBeetle5, // 딱정벌레구덩이5층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (89, 1, 1, 0, 'ePlaceAegirUnderwaterCave1, // 에기르의 수중동굴1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (90, 1, 1, 0, 'ePlaceAegirUnderwaterCave2, // 에기르의 수중동굴2층.(spot)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (91, 1, 1, 0, 'ePlaceAegirUnderwaterCave3, // 에기르의 수중동굴3층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (92, 1, 1, 0, 'ePlaceAegirUnderwaterCave4, // 에기르의 수중동굴4층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (93, 0, 0, 0, 'ePlaceAegirUnderwaterCave5, // 에기르의 수중동굴5층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (94, 1, 1, 0, 'ePlaceRodenTown, // 로덴마을  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (95, 0, 0, 0, 'ePlaceRodenCastle, // 로덴성(프라임성)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (96, 0, 0, 0, 'ePlaceGuildAgit1, // 길드아지트 (푸리에)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (97, 0, 0, 0, 'ePlaceGuildAgit2, // 길드아지트 (블랙랜드)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (98, 0, 0, 0, 'ePlaceGuildAgit3, // 길드아지트 (바이런)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (99, 0, 0, 0, 'ePlaceBanquetHallSmall1, // 연회장 (소) (푸리에)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (100, 0, 0, 0, 'ePlaceBanquetHallSmall2, // 연회장 (소) (블랙랜  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (101, 0, 0, 0, 'ePlaceBanquetHallSmall3, // 연회장 (소) (바이런)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (102, 0, 0, 0, 'ePlaceBanquetHallLarge1, // 연회장 (대) (푸리에)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (103, 0, 0, 0, 'ePlaceBanquetHallLarge2, // 연회장 (대) (블랙랜  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (104, 0, 0, 0, 'ePlaceBanquetHallLarge3, // 연회장 (대) (바이런)  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (105, 0, 0, 0, 'ePlaceTeamBattleArena0, // 팀 배틀 경기장 0~30  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (106, 0, 0, 0, 'ePlaceTeamBattleArena30, // 팀 배틀 경기장 31~44  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (107, 0, 0, 0, 'ePlaceTeamBattleArena45Over,// 팀 배틀 경기장 45Over   ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (108, 1, 1, 0, 'ePlaceRockOfSeal, // 봉인의 반석  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (109, 0, 0, 0, 'ePlaceHellGate, // 헬게이트  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (110, 1, 1, 0, 'ePlaceTeamBattleArenaOp, // 운영자 대전 결투장.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (111, 1, 1, 0, 'ePlaceTellares,  // 텔라레스 화산 ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (112, 1, 1, 0, 'ePlaceRodenPrimeCastlePortal, // 로덴 프라임성 포탈  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (113, 1, 1, 0, 'ePlaceVellainTerrace, // 벨라인테라스  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (114, 1, 1, 0, 'ePlaceRedValley, // 붉은계곡  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (115, 1, 1, 0, 'ePlaceErtesCave, // 에르테스 동굴  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (116, 1, 1, 0, 'ePlaceZaratanBasin, // 자라탄 분지  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (117, 1, 1, 0, 'ePlaceLakeOfTime, // 시간의 호수  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (118, 1, 1, 0, 'ePlaceRefugeOfGiants, // 거인병의 안식처  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (119, 1, 1, 0, 'ePlaceMazeOfMinotaur, // 마녹의 미궁  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (120, 1, 1, 0, 'ePlaceRodenTownCampsite, // 로덴마을 야영지  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (121, 1, 1, 0, 'ePlaceErtesCaveOutside, // 에르테스 동굴 외부  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (122, 1, 1, 0, 'ePlaceErtesCaveInside, // 에르테스 동굴 내부  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (123, 1, 1, 0, 'ePlaceAncientStairs1, // 고대의계단 던젼 1층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (124, 1, 1, 0, 'ePlaceAncientStairs2, // 고대의계단 던젼 2층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (125, 1, 1, 0, 'ePlaceAncientStairs3, // 고대의계단 던젼 3층.  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (126, 1, 1, 0, 'ePlaceTownOfDumper, // 버려진 마을  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (127, 0, 0, 0, 'ePlaceExile,  // 유배지. ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (128, 0, 0, 0, 'ePlaceConsultRoom, // 상담실  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (129, 0, 0, 0, 'ePlaceTeamBattleArenaWide0, // 넓은 크루컴뱃존 초급  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (130, 0, 0, 0, 'ePlaceTeamBattleArenaWide30, // 넓은 크루컴뱃존 중급  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (131, 0, 0, 0, 'ePlaceTeamBattleArenaWide45Over, // 넓은 크루컴뱃존 상급  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (132, 0, 0, 0, 'ePlaceEventPrimeCrewCombat, // 이벤트 크루컴뱃 경기장 1  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (133, 0, 0, 0, 'ePlaceEventHonorCrewCombat, // 이벤트 크루컴뱃 경기장 2  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (134, 0, 0, 0, 'ePlaceEventSpritCrewCombat, // 이벤트 크루컴뱃 경기장 3  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (135, 0, 0, 0, 'ePlaceEntranceToMeteosLair, // 메테오스 레어 - 외부  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (136, 0, 0, 0, 'ePlaceTheOutsideCamp, // 메테오스 레어 - 외부 캠프  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (137, 0, 0, 0, 'ePlaceMeteosLair, // 메테오스 레어 - 내부  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (138, 1, 1, 0, 'ePlaceSanctuaryOfMeteos, // 메테오스 신전  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (139, 0, 0, 0, 'ePlaceTeamBattleArena54Over, // 팀배틀 경기장 54 Over  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (140, 0, 0, 0, 'ePlaceChampionCombat, // 최상급 챔피온 크루컴뱃  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (141, 0, 0, 0, 'ePlaceTowerCrewCombat, // 최상급 타워 크루컴뱃  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (142, 1, 1, 1, 'ePlaceAccra,  // 아크라 섬 ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (143, 1, 1, 0, 'ePlaceAccraTown, // 아크라 마을  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (144, 1, 1, 0, 'ePlaceAltarOfRitual, // 의식의 제단  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (145, 1, 1, 0, 'ePlacePrimitiveForest, // 원초의 숲  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (146, 1, 1, 0, 'ePlaceShoreOfSettler, // 초동의 해변  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (147, 1, 1, 0, 'ePlaceDimensionDoor, // 결계의 틈  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (148, 1, 1, 0, 'ePlaceForestOfDimension, // 차원의 숲  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (149, 1, 1, 0, 'ePlaceShoreOfSerpents, // 뱀의 해변  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (150, 1, 1, 0, 'ePlaceValeforForest, // 발레포르의 숲  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (151, 1, 1, 0, 'ePlaceEnteranceOfValefor, // 발레포르 던전 입구  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (152, 1, 1, 0, 'ePlaceFoliageOfWildness, // 야성의 수해  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (153, 1, 1, 0, 'ePlaceBattleFieldOfWill, // 의지의 전장  ');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (154, 1, 1, 0, 'ePlaceBattleFieldOfWillAttackCamp, // 의지의 전장 공격 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (155, 1, 1, 0, 'ePlaceBattleFieldOfWillDefenseCamp, // 의지의 전장 수비 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (156, 1, 1, 0, 'ePlaceBattleFieldOfSprit, // 투지의 전장');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (157, 1, 1, 0, 'ePlaceBattleFieldOfSpritAttackCamp, // 투지의 전장 공격 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (158, 1, 1, 0, 'ePlaceBattleFieldOfSpritDefenseCamp, // 투지의 전장 수비 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (159, 1, 1, 0, 'ePlaceBattleFieldOfCalm, // 고요의 전장');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (160, 1, 1, 0, 'ePlaceBattleFieldOfCalmAttackCamp, // 고요의 전장 공격 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (161, 1, 1, 0, 'ePlaceBattleFieldOfCalmDefenseCamp, // 고요의 전장 수비 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (162, 1, 1, 0, 'ePlaceBattleFieldOfWisdom, // 지혜의 전장');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (163, 1, 1, 0, 'ePlaceBattleFieldOfWisdomAttackCamp, // 지혜의 전장 공격 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (164, 1, 1, 0, 'ePlaceBattleFieldOfWisdomDefenseCamp, // 지혜의 전장 수비 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (165, 1, 1, 0, 'ePlaceEnteranceOfValefor2,// 발레포르 던전 입구(푸리에 영지 방면)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (166, 1, 1, 0, 'ePlaceValefor,// 발레포르 던전');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (167, 1, 1, 0, 'ePlaceSealedCave,// 봉인된 동굴');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (168, 0, 0, 0, 'ePlaceCaveOfPrimordialPirates1,// 원시해적의 동굴1층');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (169, 0, 0, 0, 'ePlaceCaveOfPrimordialPirates2,// 원시해적의 동굴2층');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (170, 1, 1, 0, '	ePlaceTowerOfMeteos,					// 메테오스의 탑			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (171, 1, 1, 0, '	ePlaceRoomOfLand,						// 대지의 방		');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (172, 1, 1, 0, '	ePlaceRoomOfAir,						// 대기의 방		');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (173, 1, 1, 0, '	ePlaceRoomOfOcean,						// 대해의 방		');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (174, 1, 1, 0, '	ePlaceRoomOfSun,						// 태양의 방		');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (175, 1, 1, 0, '	ePlaceTowerOfMeteosTheTopFloor,			// 메테오스의 탑 최상층					');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (180, 1, 1, 0, '	ePlace1thGuildBattleField,				// 제 1 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (181, 1, 1, 0, '	ePlace1thTheColosseum,					// 제 1 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (182, 1, 1, 0, '	ePlace1thTheColosseumBattleZone,		// 제 1 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (183, 1, 1, 0, '	ePlace2thGuildBattleField,				// 제 2 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (184, 1, 1, 0, '	ePlace2thTheColosseum,					// 제 2 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (185, 1, 1, 0, '	ePlace2thTheColosseumBattleZone,		// 제 2 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (186, 1, 1, 0, '	ePlace3thGuildBattleField,				// 제 3 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (187, 1, 1, 0, '	ePlace3thTheColosseum,					// 제 3 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (188, 1, 1, 0, '	ePlace3thTheColosseumBattleZone,		// 제 3 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (189, 1, 1, 0, '	ePlace4thGuildBattleField,				// 제 4 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (190, 1, 1, 0, '	ePlace4thTheColosseum,					// 제 4 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (191, 1, 1, 0, '	ePlace4thTheColosseumBattleZone,		// 제 4 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (192, 1, 1, 0, '	ePlace5thGuildBattleField,				// 제 5 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (193, 1, 1, 0, '	ePlace5thTheColosseum,					// 제 5 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (194, 1, 1, 0, '	ePlace5thTheColosseumBattleZone,		// 제 5 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (195, 1, 1, 0, '	ePlace6thGuildBattleField,				// 제 6 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (196, 1, 1, 0, '	ePlace6thTheColosseum,					// 제 6 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (197, 1, 1, 0, '	ePlace6thTheColosseumBattleZone,		// 제 6 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (198, 1, 1, 0, '	ePlace7thGuildBattleField,				// 제 7 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (199, 1, 1, 0, '	ePlace7thTheColosseum,					// 제 7 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (200, 1, 1, 0, '	ePlace7thTheColosseumBattleZone,		// 제 7 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (201, 1, 1, 0, '	ePlace8thGuildBattleField,				// 제 8 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (202, 1, 1, 0, '	ePlace8thTheColosseum,					// 제 8 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (203, 1, 1, 0, '	ePlace8thTheColosseumBattleZone,		// 제 8 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (204, 1, 1, 0, '	ePlace9thGuildBattleField,				// 제 9 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (205, 1, 1, 0, '	ePlace9thTheColosseum,					// 제 9 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (206, 1, 1, 0, '	ePlace9thTheColosseumBattleZone,		// 제 9 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (207, 1, 1, 0, '	ePlace10thGuildBattleField,				// 제 10 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (208, 1, 1, 0, '	ePlace10thTheColosseum,					// 제 10 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (209, 1, 1, 0, '	ePlace10thTheColosseumBattleZone,		// 제 10 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (210, 1, 1, 0, '	ePlace11thGuildBattleField,				// 제 11 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (211, 1, 1, 0, '	ePlace11thTheColosseum,					// 제 11 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (212, 1, 1, 0, '	ePlace11thTheColosseumBattleZone,		// 제 11 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (213, 1, 1, 0, '	ePlace12thGuildBattleField,				// 제 12 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (214, 1, 1, 0, '	ePlace12thTheColosseum,					// 제 12 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (215, 1, 1, 0, '	ePlace12thTheColosseumBattleZone,		// 제 12 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (216, 1, 1, 0, '	ePlace13thGuildBattleField,				// 제 13 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (217, 1, 1, 0, '	ePlace13thTheColosseum,					// 제 13 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (218, 1, 1, 0, '	ePlace13thTheColosseumBattleZone,		// 제 13 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (219, 1, 1, 0, '	ePlace14thGuildBattleField,				// 제 14 길드 전장				');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (220, 1, 1, 0, '	ePlace14thTheColosseum,					// 제 14 콜로세움			');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (221, 1, 1, 0, '	ePlace14thTheColosseumBattleZone,		// 제 14 콜로세움 결투장						');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (226, 1, 1, 0, 'ePlaceShrineOfIllumina.	// 일루미나의 성지');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (227, 1, 1, 0, 'ePlaceCampOfIllumina.	// 일루미나 캠프 지역');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (228, 1, 1, 0, 'ePlaceSpoiledSwamp.		// 변질된 늪');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (229, 1, 1, 0, 'ePlaceNestOfDragram.		// 드라그람의 둥지');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (230, 1, 1, 0, 'ePlaceMutantRockArea.	// 변종 동물 암석 지대');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (231, 1, 1, 0, 'ePlaceCaveOfConfusion.	// 혼란의 동굴');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (232, 1, 1, 0, 'ePlaceBaphometScaleBridge.	// 바포메트 비늘다리');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (233, 0, 0, 0, 'ePlaceNestOfBaphomet. // 바포메트의 둥지');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (234, 1, 1, 0, 'ePlaceRockCanyonBase. // 바위 협곡 기지');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (235, 1, 1, 0, 'ePlaceWindmillVillage. // 바람의 풍차 마을');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (236, 1, 1, 0, 'ePlaceShadowVillage. // 그림자 마을');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (237, 1, 1, 0, 'ePlaceGuineaPort. // 기네아 항구');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (238, 1, 1, 0, 'ePlaceGuineaNorthSeaside. // 기네아 북부 해안');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (239, 1, 1, 0, 'ePlaceGuineaEastSeaside. // 기네아 동부 해안');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (240, 1, 1, 0, 'ePlaceGuineaSouthSeaside. // 기네아 남부 해안');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (241, 1, 1, 0, 'ePlaceCityOfAncientElf. // 고대 엘프의 도시');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (242, 1, 1, 0, 'ePlaceNorthRestrictionOfEuvgenh. // 유게네스의 북부 결계터');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (243, 1, 1, 0, 'ePlaceEastRestrictionOfEuvgenh. // 유게네스의 동부 결계터');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (244, 1, 1, 0, 'ePlaceSouthRestrictionOfEuvgenh. // 유게네스의 남부 결계터');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (245, 1, 1, 0, 'ePlaceVestigeOfAncientElf. // 고대 엘프의 흔적');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (246, 1, 1, 0, 'ePlaceWindmillFarm. // 풍차 농장');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (247, 1, 1, 0, 'ePlaceRockCanyonLakeside. // 바위 협곡 호숫가');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (248, 1, 1, 0, 'ePlaceHistoricSiteOfFullMoon. // 만월의 유적지');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (249, 1, 1, 0, 'ePlaceCityOfForlornFullMoon. // 버려진 만월의 도시');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (250, 1, 1, 0, 'ePlaceForgottenGrove. // 잊혀진 숲');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (251, 1, 1, 0, 'ePlaceForgottenStatus. // 잊혀진 거상');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (252, 1, 1, 0, 'ePlaceForgottenPlateau. // 잊혀진 고원');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (253, 1, 1, 0, 'ePlaceAttackAreaOfRecluse. // 은둔자의 습격지');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (254, 1, 1, 0, 'ePlaceForgottenBridge. // 만월의 다리');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (255, 1, 1, 0, 'ePlaceGloomyOutpost. // 으스스한 전초기지');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (256, 1, 1, 0, 'ePlaceLerkaFollowProtectiveWall. // 레르카 추종 방벽지대');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (257, 1, 1, 0, 'ePlaceLerkaFollowStagingArea. // 레르카 추종 집결지');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (258, 1, 1, 0, 'ePlaceAltarOfLerka. // 레르카의 제단');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (259, 1, 1, 0, 'ePlaceCliffOfFullMoon. // 만월의 절벽');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (260, 1, 1, 0, 'ePlaceFullMoonEntrance. // 만월의 유적지 입구지역');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (261, 1, 1, 0, 'ePlaceBattleFieldOfCourage, // 용기의 전장');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (262, 1, 1, 0, 'ePlaceBattleFieldOfCourageAttackCamp, // 용기의 전장 공격 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (263, 1, 1, 0, 'ePlaceBattleFieldOfCourageDefenseCamp, // 용기의 전장 수비 캠프');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (264, 1, 1, 0, 'ePlaceBattleFieldOfCourageSafetyDefenseCamp, // 용기의 전장 수비 안전지대');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (265, 0, 0, 0, 'ePlaceLoopCrewCombat,	// 루프 크루컴뱃');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (266, 0, 0, 0, 'ePlace1thMonsterMatch, // 제1 몬스터 매치');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (267, 0, 0, 0, 'ePlace2thMonsterMatch, // 제2 몬스터 매치');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (268, 0, 0, 0, 'ePlace3thMonsterMatch, // 제3 몬스터 매치');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (269, 0, 0, 0, 'ePlace4thMonsterMatch, // 제4 몬스터 매치');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (270, 0, 0, 0, 'ePlace5thMonsterMatch, // 제5 몬스터 매치');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (271, 0, 0, 0, 'ePlace6thMonsterMatch, // 제6 몬스터 매치');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (272, 0, 0, 0, 'ePlace7thMonsterMatch, // 제7 몬스터 매치');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (273, 0, 0, 0, 'ePlace8thMonsterMatch, // 제8 몬스터 매치');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (274, 1, 1, 0, 'ePlaceReconstructionOfCity,				// 재건된 아벨리온 도시');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (275, 1, 1, 0, 'ePlacePvpZoneAtAveliyon,				// 아벨레온 결투장');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (276, 1, 1, 0, 'ePlaceEtelriumCastle,					// 천공의 성 에텔리움');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (277, 1, 1, 0, 'ePlaceImprintPlaceOfCrown,				// 왕관의 각인지(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (278, 0, 0, 0, 'ePlaceEtelriumHideout,					// 에텔리움 아지트');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (279, 1, 1, 0, 'ePlaceEtelriumBridge,					// 에텔리움 교각');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (280, 1, 1, 0, 'ePlaceSongforestOfAngel,				// 천사의 노래숲(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (281, 1, 1, 0, 'ePlaceForestOfMagic,					// 마력의 숲(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (282, 1, 1, 0, 'ePlaceForestOfBrilliance,				// 광휘의 숲(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (283, 1, 1, 0, 'ePlaceSanctuaryOfUpiter,				// 유피테르 성소(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (284, 1, 1, 0, 'ePlaceDrgonnestOfCorps,					// 군단의 용둥지(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (285, 1, 1, 0, 'ePlaceTraceOfPlague,					// 역병의 발자취(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (286, 1, 1, 0, 'ePlaceDeadbodyRuin,						// 죽은자의 폐허(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (287, 1, 1, 0, 'ePlaceAltarOfBaalbek,					// 바알베크 제단(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (288, 1, 1, 0, 'ePlaceProhibitedJungle,					// 금지된 밀림(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (289, 1, 1, 0, 'ePlaceSilentWasteland,					// 고요한 황무지(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (290, 1, 1, 0, 'ePlaceSkysongGrassland,					// 하늘노래 초원(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (291, 1, 1, 0, 'ePlaceWildCanyon,						// 황야의 협곡(spot)');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (292, 1, 1, 1, 'ePlaceIslandSky,						// 천공의 섬');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (293, 1, 1, 1, 'ePlaceIslandSkyGate,						// 천공의 섬 입구');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (294, 1, 1, 0, 'ePlaceUpiterDungeonGate. //유피테르던전 입구');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (295, 1, 1, 0, 'ePlaceBaalbekDungeonGate. //바알베크던전 입구');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (296, 1, 1, 0, 'ePlaceUpiterDungeon1, // 유피테르던전 1층');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (297, 1, 1, 0, 'ePlaceBaalbekDungeon1, // 바알베크던전 1층');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (298, 1, 1, 0, 'ePlaceUpiterDungeon2, // 유피테르던전 2층');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (299, 1, 1, 0, 'ePlaceBaalbekDungeon2, // 바알베크던전 2층');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (303, 1, 1, 0, 'ePlace70LevelsOfDungeon,// 발레포르 던전 70');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (304, 1, 1, 0, 'ePlace80LevelsOfDungeon,// 발레포르 던전 80');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (305, 1, 1, 0, 'ePlaceFortOfAbelruth,// 아벨루스의 요새');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (306, 0, 0, 0, 'ePlaceTrialOfTheSoul,// 아벨루스의 훈련소');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (307, 1, 1, 0, 'ePlaceFortOfAbelruthGate,// 아벨루스의 요새 입구');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (320, 1, 1, 0, 'ePlaceExteriorOfDarkPriestTemple //암흑사제 사원 외부');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (321, 1, 1, 0, 'ePlaceInteriorOfDarkPriestTemple //암흑사제 사원 내부');
GO

INSERT INTO [dbo].[TblWorldMapInfo] ([mMapNo], [mMapVisible], [mPartyVisible], [mListVisible], [mDesc]) VALUES (322, 1, 1, 0, 'ePlaceEntranceOfDarkPriestTemple //암흑사제 사원 입구');
GO

