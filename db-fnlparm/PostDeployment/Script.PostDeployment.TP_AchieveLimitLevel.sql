/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : TP_AchieveLimitLevel
Date                  : 2023-10-07 09:09:27
*/


INSERT INTO [dbo].[TP_AchieveLimitLevel] ([mLimitID], [mSRange], [mERange], [mProb]) VALUES (1, 11, 50, 30.0);
GO

INSERT INTO [dbo].[TP_AchieveLimitLevel] ([mLimitID], [mSRange], [mERange], [mProb]) VALUES (2, 51, 60, 65.0);
GO

INSERT INTO [dbo].[TP_AchieveLimitLevel] ([mLimitID], [mSRange], [mERange], [mProb]) VALUES (3, 61, 75, 4.94);
GO

INSERT INTO [dbo].[TP_AchieveLimitLevel] ([mLimitID], [mSRange], [mERange], [mProb]) VALUES (4, 76, 90, 0.05);
GO

INSERT INTO [dbo].[TP_AchieveLimitLevel] ([mLimitID], [mSRange], [mERange], [mProb]) VALUES (5, 91, 100, 0.01);
GO

