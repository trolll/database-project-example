/*
Source Server         : 192.168.1.210
Source Server Type    : SQL Server
Source Host           : 192.168.1.210:1433
Source Database       : FNLParm
Source Table          : DT_SkillTreeNodeItem
Date                  : 2023-10-07 09:08:49
*/


INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (3, 1, 167, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (4, 1, 168, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (5, 1, 169, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (6, 1, 170, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (7, 1, 171, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (8, 67, 350, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (9, 67, 351, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (10, 67, 352, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (11, 7, 172, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (12, 7, 173, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (13, 7, 174, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (14, 7, 175, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (15, 7, 176, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (16, 14, 177, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (17, 14, 178, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (18, 14, 179, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (19, 14, 180, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (20, 14, 181, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (21, 68, 353, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (24, 68, 354, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (25, 68, 355, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (26, 68, 356, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (27, 68, 357, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (28, 15, 182, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (29, 15, 183, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (30, 16, 184, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (31, 69, 358, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (32, 17, 185, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (34, 17, 186, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (35, 69, 359, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (36, 17, 187, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (37, 69, 360, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (38, 69, 361, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (39, 69, 362, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (40, 70, 363, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (41, 70, 364, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (42, 70, 365, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (43, 71, 366, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (44, 71, 367, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (45, 71, 368, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (46, 71, 369, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (47, 71, 370, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (48, 72, 371, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (49, 17, 188, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (50, 72, 372, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (51, 72, 373, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (52, 17, 189, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (53, 73, 374, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (54, 73, 375, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (55, 73, 376, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (56, 18, 190, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (57, 74, 377, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (58, 74, 378, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (59, 74, 379, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (60, 74, 380, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (61, 74, 381, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (62, 75, 382, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (63, 75, 383, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (64, 75, 384, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (65, 75, 385, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (66, 75, 386, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (67, 77, 387, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (68, 77, 388, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (69, 77, 389, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (70, 77, 390, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (71, 19, 191, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (72, 5, 192, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (73, 5, 193, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (74, 5, 194, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (75, 5, 195, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (76, 5, 196, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (77, 11, 197, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (78, 11, 198, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (79, 11, 199, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (80, 11, 200, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (81, 11, 201, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (82, 6, 202, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (83, 6, 203, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (84, 6, 204, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (85, 78, 391, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (86, 78, 392, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (87, 78, 393, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (88, 78, 394, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (89, 78, 395, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (90, 12, 205, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (91, 12, 206, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (92, 12, 207, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (93, 20, 208, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (94, 20, 209, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (95, 20, 210, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (96, 21, 211, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (97, 21, 212, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (98, 21, 213, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (99, 23, 214, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (100, 23, 215, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (101, 23, 216, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (102, 79, 396, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (103, 79, 397, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (104, 79, 398, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (105, 79, 399, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (106, 79, 400, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (107, 80, 401, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (108, 80, 402, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (109, 80, 403, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (110, 81, 404, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (111, 81, 405, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (112, 81, 406, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (113, 82, 407, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (114, 82, 408, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (115, 82, 409, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (116, 82, 410, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (117, 82, 411, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (118, 83, 412, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (119, 83, 413, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (120, 83, 414, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (121, 83, 415, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (122, 83, 416, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (123, 32, 247, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (124, 32, 248, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (125, 32, 249, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (126, 84, 417, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (127, 84, 418, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (128, 84, 419, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (129, 84, 420, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (130, 84, 421, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (131, 85, 422, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (132, 85, 423, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (133, 85, 424, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (134, 85, 425, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (135, 85, 426, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (136, 33, 250, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (137, 33, 251, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (138, 86, 427, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (139, 86, 428, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (140, 33, 252, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (141, 86, 429, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (142, 86, 430, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (143, 86, 431, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (144, 34, 253, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (145, 87, 432, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (146, 34, 254, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (147, 34, 255, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (148, 35, 256, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (149, 35, 257, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (150, 35, 258, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (151, 35, 259, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (152, 35, 260, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (153, 36, 261, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (154, 36, 262, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (155, 36, 263, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (156, 36, 264, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (157, 36, 265, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (158, 37, 266, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (159, 37, 267, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (160, 37, 268, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (161, 37, 269, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (163, 88, 433, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (164, 88, 434, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (165, 88, 435, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (166, 37, 270, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (167, 89, 436, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (168, 89, 437, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (169, 89, 438, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (170, 89, 439, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (171, 89, 440, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (172, 90, 441, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (174, 90, 442, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (175, 90, 443, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (176, 90, 444, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (177, 90, 445, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (178, 91, 446, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (179, 91, 447, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (180, 91, 448, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (181, 92, 449, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (182, 92, 450, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (183, 92, 451, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (184, 92, 452, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (185, 92, 453, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (186, 93, 454, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (187, 93, 455, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (188, 93, 456, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (189, 93, 457, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (190, 93, 458, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (194, 96, 462, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (195, 96, 463, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (196, 96, 464, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (197, 97, 465, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (198, 97, 466, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (199, 97, 467, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (200, 97, 468, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (201, 97, 469, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (207, 38, 271, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (208, 38, 272, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (209, 38, 273, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (210, 39, 274, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (211, 39, 275, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (212, 39, 276, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (213, 40, 277, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (214, 40, 278, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (215, 40, 279, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (216, 51, 300, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (217, 51, 301, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (218, 51, 302, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (219, 51, 303, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (220, 51, 304, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (221, 99, 475, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (222, 99, 476, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (223, 99, 477, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (224, 99, 478, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (225, 99, 479, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (226, 100, 480, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (227, 100, 481, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (228, 100, 482, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (229, 100, 483, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (230, 100, 484, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (231, 101, 485, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (232, 101, 486, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (233, 101, 487, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (234, 53, 305, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (235, 103, 488, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (236, 103, 489, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (237, 53, 306, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (238, 103, 490, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (239, 104, 491, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (240, 53, 307, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (241, 104, 492, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (242, 104, 493, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (243, 53, 308, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (244, 104, 494, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (245, 104, 495, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (246, 53, 309, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (247, 52, 310, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (248, 52, 311, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (249, 105, 496, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (250, 52, 312, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (251, 105, 497, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (252, 52, 313, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (253, 52, 314, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (254, 105, 498, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (255, 105, 499, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (256, 105, 500, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (257, 106, 501, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (258, 106, 502, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (259, 106, 503, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (260, 106, 504, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (261, 106, 505, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (262, 107, 506, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (263, 107, 507, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (264, 107, 508, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (265, 107, 509, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (266, 107, 510, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (267, 108, 511, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (268, 108, 512, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (269, 108, 513, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (270, 108, 514, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (271, 108, 515, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (273, 56, 315, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (274, 56, 316, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (275, 56, 317, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (276, 56, 318, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (277, 56, 319, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (278, 57, 320, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (279, 57, 321, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (280, 57, 322, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (281, 57, 323, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (282, 57, 324, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (283, 60, 325, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (284, 60, 326, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (285, 60, 327, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (286, 60, 328, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (287, 60, 329, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (288, 61, 330, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (289, 61, 331, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (290, 61, 332, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (291, 62, 333, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (292, 62, 334, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (293, 62, 335, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (294, 63, 336, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (295, 63, 337, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (296, 63, 338, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (297, 63, 339, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (298, 63, 340, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (299, 64, 341, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (300, 64, 342, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (301, 64, 343, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (302, 65, 344, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (303, 65, 345, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (304, 65, 346, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (305, 66, 347, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (306, 66, 348, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (307, 66, 349, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (308, 109, 516, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (309, 109, 517, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (311, 110, 518, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (312, 110, 519, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (313, 110, 520, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (314, 110, 521, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (315, 110, 522, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (316, 111, 523, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (317, 111, 524, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (318, 111, 525, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (319, 111, 526, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (320, 111, 527, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (321, 112, 528, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (322, 112, 529, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (323, 113, 530, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (324, 113, 531, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (325, 113, 532, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (326, 113, 533, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (327, 113, 534, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (328, 114, 535, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (329, 114, 536, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (330, 114, 537, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (331, 114, 538, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (332, 114, 539, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (333, 115, 540, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (334, 115, 541, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (335, 115, 542, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (336, 116, 543, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (337, 116, 544, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (338, 116, 545, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (339, 117, 546, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (340, 117, 547, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (341, 117, 548, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (342, 118, 549, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (343, 118, 550, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (344, 118, 551, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (345, 118, 552, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (346, 118, 553, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (347, 119, 554, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (348, 119, 555, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (349, 119, 556, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (350, 120, 518, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (351, 120, 519, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (352, 120, 520, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (353, 120, 521, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (354, 120, 522, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (355, 121, 557, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (356, 121, 558, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (357, 121, 559, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (358, 122, 560, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (359, 122, 561, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (360, 122, 562, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (361, 123, 530, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (362, 123, 531, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (363, 123, 532, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (364, 123, 533, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (366, 123, 534, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (367, 124, 563, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (368, 124, 564, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (369, 124, 565, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (370, 125, 566, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (371, 125, 567, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (372, 125, 568, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (373, 126, 569, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (374, 126, 570, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (375, 126, 571, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (376, 126, 572, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (377, 126, 573, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (378, 127, 574, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (379, 127, 575, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (380, 127, 576, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (381, 127, 577, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (382, 127, 578, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (383, 128, 579, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (384, 128, 580, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (385, 128, 581, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (386, 129, 582, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (387, 129, 583, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (388, 129, 584, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (389, 129, 585, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (390, 129, 586, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (391, 130, 587, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (392, 130, 588, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (393, 130, 589, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (394, 130, 590, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (395, 130, 591, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (396, 131, 592, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (397, 131, 593, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (398, 131, 594, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (399, 131, 595, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (400, 131, 596, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (401, 132, 353, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (402, 132, 354, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (403, 132, 355, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (404, 132, 356, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (405, 132, 357, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (406, 136, 366, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (407, 136, 367, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (408, 136, 368, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (409, 136, 369, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (410, 136, 370, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (411, 138, 377, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (412, 138, 378, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (413, 138, 379, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (414, 138, 380, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (415, 138, 381, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (416, 133, 597, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (417, 133, 598, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (418, 133, 599, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (419, 135, 600, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (420, 135, 601, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (421, 135, 602, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (422, 135, 603, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (423, 135, 604, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (424, 137, 605, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (425, 137, 606, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (426, 137, 607, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (427, 137, 608, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (428, 137, 609, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (429, 139, 610, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (430, 139, 611, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (431, 139, 612, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (432, 139, 613, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (434, 139, 614, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (435, 140, 615, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (436, 140, 616, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (437, 140, 617, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (438, 140, 618, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (439, 140, 619, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (440, 141, 620, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (441, 141, 621, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (442, 141, 622, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (443, 141, 623, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (444, 141, 624, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (455, 146, 625, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (456, 146, 626, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (457, 146, 627, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (458, 146, 628, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (459, 144, 629, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (460, 144, 630, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (461, 144, 631, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (462, 144, 632, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (463, 144, 633, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (464, 143, 634, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (465, 143, 635, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (466, 143, 636, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (467, 147, 637, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (468, 147, 638, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (469, 147, 639, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (470, 147, 640, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (471, 147, 641, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (472, 148, 642, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (473, 148, 643, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (474, 148, 644, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (475, 149, 645, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (476, 150, 646, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (477, 150, 647, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (478, 150, 648, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (479, 151, 518, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (480, 151, 519, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (481, 151, 520, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (482, 151, 521, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (483, 151, 522, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (484, 154, 530, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (485, 154, 531, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (486, 154, 532, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (487, 154, 533, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (488, 154, 534, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (489, 152, 649, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (490, 152, 650, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (491, 152, 651, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (492, 152, 652, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (493, 152, 653, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (494, 153, 654, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (495, 153, 655, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (496, 153, 656, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (497, 155, 657, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (498, 155, 658, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (499, 155, 659, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (505, 157, 665, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (506, 157, 666, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (507, 157, 667, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (508, 157, 668, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (509, 157, 669, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (510, 158, 670, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (511, 158, 671, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (512, 158, 672, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (513, 159, 673, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (514, 160, 674, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (515, 160, 675, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (516, 160, 676, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (517, 161, 518, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (518, 161, 519, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (519, 161, 520, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (520, 161, 521, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (521, 161, 522, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (522, 163, 530, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (523, 163, 531, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (524, 163, 532, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (525, 163, 533, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (526, 163, 534, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (527, 162, 677, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (528, 164, 678, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (529, 164, 679, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (530, 164, 680, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (531, 165, 681, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (532, 165, 682, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (533, 165, 683, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (534, 165, 684, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (535, 165, 685, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (536, 166, 686, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (537, 166, 687, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (538, 166, 688, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (539, 167, 689, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (540, 167, 690, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (541, 167, 691, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (542, 167, 692, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (543, 167, 693, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (544, 168, 694, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (545, 168, 695, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (546, 168, 696, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (547, 168, 697, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (548, 168, 698, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (550, 24, 217, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (551, 24, 218, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (552, 24, 219, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (553, 24, 220, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (554, 24, 221, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (555, 26, 222, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (556, 26, 223, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (557, 26, 224, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (558, 26, 225, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (559, 26, 226, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (560, 28, 227, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (561, 28, 228, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (562, 28, 229, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (563, 28, 230, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (564, 28, 231, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (565, 29, 232, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (566, 29, 233, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (567, 29, 234, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (568, 29, 235, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (569, 29, 236, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (570, 30, 237, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (571, 30, 238, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (572, 30, 239, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (573, 30, 240, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (574, 30, 241, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (575, 31, 242, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (576, 31, 243, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (577, 31, 244, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (578, 31, 245, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (579, 31, 246, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (580, 41, 280, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (581, 41, 281, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (582, 42, 282, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (583, 42, 283, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (584, 43, 284, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (585, 43, 285, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (586, 44, 286, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (587, 44, 287, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (588, 45, 288, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (589, 45, 289, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (590, 46, 290, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (591, 46, 291, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (592, 47, 292, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (593, 47, 293, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (594, 48, 294, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (595, 48, 295, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (596, 49, 296, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (597, 49, 297, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (598, 50, 298, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (599, 50, 299, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (600, 169, 459, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (601, 169, 460, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (602, 169, 461, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (603, 170, 470, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (604, 170, 471, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (605, 170, 472, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (606, 170, 473, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (607, 170, 474, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (608, 171, 808, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (609, 171, 809, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (610, 171, 810, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (611, 172, 811, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (612, 173, 812, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (613, 173, 813, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (614, 173, 814, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (615, 173, 815, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (616, 173, 816, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (617, 175, 817, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (618, 175, 818, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (619, 175, 819, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (620, 176, 820, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (621, 176, 821, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (622, 176, 822, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (623, 178, 823, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (624, 178, 824, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (625, 178, 825, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (626, 180, 826, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (627, 181, 827, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (628, 181, 828, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (629, 181, 829, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (630, 183, 830, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (631, 183, 831, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (632, 183, 832, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (633, 186, 833, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (634, 187, 834, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (635, 187, 835, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (636, 187, 836, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (637, 188, 837, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (638, 188, 838, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (639, 188, 839, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (640, 188, 840, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (641, 188, 841, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (642, 189, 842, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (643, 189, 843, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (644, 189, 844, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (645, 190, 845, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (646, 190, 846, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (647, 190, 847, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (648, 191, 848, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (649, 191, 849, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (650, 191, 850, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (651, 192, 851, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (652, 193, 852, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (653, 193, 853, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (654, 193, 854, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (655, 194, 855, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (656, 196, 856, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (657, 196, 857, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (658, 196, 858, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (659, 198, 859, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (660, 200, 860, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (661, 200, 861, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (662, 200, 862, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (663, 201, 863, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (664, 201, 864, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (665, 201, 865, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (666, 203, 866, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (667, 203, 867, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (668, 203, 868, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (669, 204, 869, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (670, 204, 870, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (671, 204, 871, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (672, 204, 950, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (673, 204, 951, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (674, 205, 872, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (675, 205, 873, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (676, 205, 874, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (677, 206, 875, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (678, 207, 876, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (679, 207, 877, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (680, 207, 878, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (681, 208, 879, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (682, 208, 880, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (683, 209, 882, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (684, 209, 883, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (685, 209, 884, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (686, 210, 885, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (687, 210, 886, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (688, 210, 887, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (689, 210, 888, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (690, 210, 889, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (691, 212, 890, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (692, 212, 891, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (693, 212, 892, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (694, 212, 893, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (695, 212, 894, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (696, 214, 895, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (697, 217, 896, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (698, 216, 897, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (699, 220, 898, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (700, 220, 899, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (701, 220, 900, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (702, 222, 901, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (706, 224, 905, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (707, 224, 906, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (708, 224, 907, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (709, 224, 908, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (710, 224, 909, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (711, 225, 910, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (712, 225, 911, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (713, 225, 912, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (714, 225, 913, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (715, 225, 914, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (716, 226, 915, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (717, 226, 916, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (718, 226, 917, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (719, 226, 918, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (720, 226, 919, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (721, 229, 920, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (722, 230, 921, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (723, 230, 922, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (724, 230, 923, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (725, 231, 924, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (726, 231, 925, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (727, 231, 926, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (729, 233, 928, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (730, 233, 929, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (731, 233, 930, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (732, 235, 931, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (733, 235, 932, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (734, 235, 933, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (735, 236, 934, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (736, 237, 935, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (737, 237, 936, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (738, 237, 937, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (739, 238, 938, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (740, 238, 939, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (741, 238, 940, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (742, 239, 941, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (743, 239, 942, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (744, 239, 943, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (745, 239, 944, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (746, 239, 945, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (747, 240, 946, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (748, 240, 947, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (749, 240, 948, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (750, 241, 949, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (751, 244, 902, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (752, 244, 903, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (753, 244, 904, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (754, 245, 957, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (755, 245, 958, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (756, 245, 959, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (757, 246, 1004, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (758, 246, 1005, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (759, 247, 1006, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (760, 247, 1007, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (761, 248, 1008, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (762, 248, 1009, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (763, 248, 1010, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (764, 249, 1025, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (765, 249, 1026, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (766, 250, 1027, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (767, 250, 1028, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (768, 251, 1029, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (769, 251, 1030, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (770, 251, 1031, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (771, 252, 1011, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (772, 252, 1012, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (773, 253, 1013, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (774, 253, 1014, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (775, 254, 1015, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (776, 254, 1016, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (777, 254, 1017, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (778, 255, 1032, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (779, 255, 1033, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (780, 256, 1034, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (781, 256, 1035, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (782, 257, 1036, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (783, 257, 1037, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (784, 257, 1038, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (785, 258, 1018, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (786, 258, 1019, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (787, 259, 1020, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (788, 259, 1021, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (789, 260, 1022, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (790, 260, 1023, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (791, 260, 1024, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (792, 261, 1039, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (793, 261, 1040, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (794, 262, 1041, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (795, 262, 1042, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (796, 263, 1043, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (797, 263, 1044, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (798, 263, 1045, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (799, 264, 1046, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (800, 264, 1047, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (801, 265, 1048, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (802, 265, 1049, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (803, 266, 1062, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (804, 266, 1063, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (805, 267, 1064, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (806, 267, 1065, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (807, 268, 1050, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (808, 268, 1051, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (809, 269, 1052, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (810, 269, 1053, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (811, 270, 1066, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (812, 270, 1067, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (813, 271, 1068, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (814, 271, 1069, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (815, 272, 1054, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (816, 272, 1055, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (817, 273, 1056, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (818, 273, 1057, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (819, 274, 1070, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (820, 274, 1071, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (821, 275, 1072, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (822, 275, 1073, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (823, 276, 1058, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (824, 276, 1059, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (825, 277, 1060, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (826, 277, 1061, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (827, 278, 1062, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (828, 278, 1063, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (829, 279, 1066, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (830, 279, 1067, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (831, 280, 1046, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (832, 280, 1047, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (833, 281, 1054, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (834, 281, 1055, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (835, 282, 1062, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (836, 282, 1063, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (837, 283, 1070, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (838, 283, 1071, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (839, 284, 1050, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (840, 284, 1051, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (841, 285, 1054, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (842, 285, 1055, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (843, 286, 1066, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (844, 286, 1067, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (845, 287, 1070, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (846, 287, 1071, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (847, 288, 1096, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (848, 288, 1097, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (849, 288, 1098, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (850, 289, 1099, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (851, 289, 1100, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (852, 289, 1101, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (853, 289, 1102, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (854, 290, 1109, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (855, 290, 1110, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (856, 290, 1111, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (857, 291, 1112, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (858, 291, 1113, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (859, 291, 1114, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (860, 291, 1115, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (861, 292, 1122, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (862, 292, 1123, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (863, 292, 1124, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (864, 293, 1125, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (865, 293, 1126, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (866, 293, 1127, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (867, 293, 1128, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (868, 294, 1135, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (869, 294, 1136, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (870, 294, 1137, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (871, 295, 1138, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (872, 295, 1139, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (873, 295, 1140, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (874, 295, 1141, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (875, 296, 1148, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (876, 296, 1149, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (877, 296, 1150, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (878, 297, 1151, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (879, 297, 1152, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (880, 297, 1153, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (881, 297, 1154, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (882, 298, 1161, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (883, 298, 1162, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (884, 298, 1163, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (885, 299, 1164, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (886, 299, 1165, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (887, 299, 1166, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (888, 299, 1167, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (889, 300, 1174, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (890, 300, 1175, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (891, 300, 1176, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (892, 301, 1177, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (893, 301, 1178, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (894, 301, 1179, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (895, 301, 1180, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (896, 302, 1109, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (897, 302, 1110, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (898, 302, 1187, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (899, 303, 1112, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (900, 303, 1113, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (901, 303, 1114, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (902, 303, 1115, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (903, 304, 1096, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (904, 304, 1097, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (905, 304, 1190, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (906, 305, 1099, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (907, 305, 1100, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (908, 305, 1101, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (909, 305, 1102, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (910, 306, 1109, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (911, 306, 1110, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (912, 306, 1187, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (913, 307, 1112, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (914, 307, 1113, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (915, 307, 1114, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (916, 307, 1115, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (917, 308, 1122, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (918, 308, 1123, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (919, 308, 1195, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (920, 309, 1125, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (921, 309, 1126, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (922, 309, 1127, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (923, 309, 1128, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (924, 310, 1135, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (925, 310, 1136, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (926, 310, 1188, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (927, 311, 1138, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (928, 311, 1139, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (929, 311, 1140, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (930, 311, 1141, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (931, 312, 1103, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (932, 312, 1104, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (933, 312, 1105, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (934, 313, 1106, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (935, 313, 1107, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (936, 313, 1108, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (937, 314, 1116, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (938, 314, 1117, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (939, 314, 1118, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (940, 315, 1119, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (941, 315, 1120, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (942, 315, 1121, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (943, 316, 1129, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (944, 316, 1130, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (945, 316, 1131, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (946, 317, 1132, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (947, 317, 1133, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (948, 317, 1134, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (949, 318, 1142, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (950, 318, 1143, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (951, 318, 1144, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (952, 319, 1145, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (953, 319, 1146, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (954, 319, 1147, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (955, 320, 1155, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (956, 320, 1156, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (957, 320, 1157, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (958, 321, 1158, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (959, 321, 1159, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (960, 321, 1160, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (961, 322, 1168, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (962, 322, 1169, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (963, 322, 1170, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (964, 323, 1171, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (965, 323, 1172, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (966, 323, 1173, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (967, 324, 1181, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (968, 324, 1182, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (969, 324, 1183, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (970, 325, 1184, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (971, 325, 1185, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (972, 325, 1186, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (973, 326, 1135, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (974, 326, 1136, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (975, 326, 1188, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (976, 327, 1138, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (977, 327, 1139, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (978, 327, 1189, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (979, 328, 1148, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (980, 328, 1149, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (981, 328, 1191, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (982, 329, 1151, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (983, 329, 1152, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (984, 329, 1192, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (985, 330, 1161, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (986, 330, 1162, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (987, 330, 1193, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (988, 331, 1164, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (989, 331, 1165, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (990, 331, 1194, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (991, 332, 1148, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (992, 332, 1149, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (993, 332, 1191, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (994, 333, 1151, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (995, 333, 1152, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (996, 333, 1192, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (997, 334, 1161, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (998, 334, 1162, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (999, 334, 1193, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1000, 335, 1164, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1001, 335, 1165, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1002, 335, 1194, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1003, 234, 927, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1004, 336, 660, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1005, 336, 661, 2);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1006, 336, 662, 3);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1007, 336, 663, 4);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1008, 336, 664, 5);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1019, 347, 1257, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1020, 348, 1258, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1021, 349, 1259, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1022, 350, 1260, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1023, 351, 1261, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1024, 352, 1262, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1025, 353, 1263, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1026, 354, 1264, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1027, 355, 1265, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1028, 356, 1266, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1029, 357, 1267, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1030, 358, 1268, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1031, 359, 1269, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1032, 360, 1270, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1033, 361, 1271, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1034, 362, 1272, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1035, 363, 1273, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1036, 364, 1274, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1037, 365, 1275, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1038, 366, 1276, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1039, 367, 1277, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1040, 368, 1278, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1041, 369, 1279, 1);
GO

INSERT INTO [dbo].[DT_SkillTreeNodeItem] ([mSTNIID], [mSTNID], [mSPID], [mLevel]) VALUES (1042, 370, 1280, 1);
GO

